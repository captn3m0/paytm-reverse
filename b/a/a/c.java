package b.a.a;

import android.os.Looper;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;

public class c
{
  public static String a = "Event";
  static volatile c b;
  private static final d c = new d();
  private static final Map<Class<?>, List<Class<?>>> d = new HashMap();
  private final Map<Class<?>, CopyOnWriteArrayList<m>> e = new HashMap();
  private final Map<Object, List<Class<?>>> f = new HashMap();
  private final Map<Class<?>, Object> g = new ConcurrentHashMap();
  private final ThreadLocal<a> h = new ThreadLocal()
  {
    protected c.a a()
    {
      return new c.a();
    }
  };
  private final f i = new f(this, Looper.getMainLooper(), 10);
  private final b j = new b(this);
  private final a k = new a(this);
  private final l l;
  private final ExecutorService m;
  private final boolean n;
  private final boolean o;
  private final boolean p;
  private final boolean q;
  private final boolean r;
  private final boolean s;
  
  public c()
  {
    this(c);
  }
  
  c(d paramd)
  {
    this.l = new l(paramd.h);
    this.o = paramd.a;
    this.p = paramd.b;
    this.q = paramd.c;
    this.r = paramd.d;
    this.n = paramd.e;
    this.s = paramd.f;
    this.m = paramd.g;
  }
  
  public static c a()
  {
    if (b == null) {}
    try
    {
      if (b == null) {
        b = new c();
      }
      return b;
    }
    finally {}
  }
  
  private List<Class<?>> a(Class<?> paramClass)
  {
    synchronized (d)
    {
      Object localObject2 = (List)d.get(paramClass);
      Object localObject1 = localObject2;
      if (localObject2 == null)
      {
        localObject2 = new ArrayList();
        for (localObject1 = paramClass; localObject1 != null; localObject1 = ((Class)localObject1).getSuperclass())
        {
          ((List)localObject2).add(localObject1);
          a((List)localObject2, ((Class)localObject1).getInterfaces());
        }
        d.put(paramClass, localObject2);
        localObject1 = localObject2;
      }
      return (List<Class<?>>)localObject1;
    }
  }
  
  private void a(m paramm, Object paramObject, Throwable paramThrowable)
  {
    if ((paramObject instanceof j)) {
      if (this.o)
      {
        Log.e(a, "SubscriberExceptionEvent subscriber " + paramm.a.getClass() + " threw an exception", paramThrowable);
        paramm = (j)paramObject;
        Log.e(a, "Initial event " + paramm.c + " caused exception in " + paramm.d, paramm.b);
      }
    }
    do
    {
      return;
      if (this.n) {
        throw new e("Invoking subscriber failed", paramThrowable);
      }
      if (this.o) {
        Log.e(a, "Could not dispatch event: " + paramObject.getClass() + " to subscribing class " + paramm.a.getClass(), paramThrowable);
      }
    } while (!this.q);
    c(new j(this, paramThrowable, paramObject, paramm.a));
  }
  
  private void a(m paramm, Object paramObject, boolean paramBoolean)
  {
    switch (2.a[paramm.b.b.ordinal()])
    {
    default: 
      throw new IllegalStateException("Unknown thread mode: " + paramm.b.b);
    case 1: 
      a(paramm, paramObject);
      return;
    case 2: 
      if (paramBoolean)
      {
        a(paramm, paramObject);
        return;
      }
      this.i.a(paramm, paramObject);
      return;
    case 3: 
      if (paramBoolean)
      {
        this.j.a(paramm, paramObject);
        return;
      }
      a(paramm, paramObject);
      return;
    }
    this.k.a(paramm, paramObject);
  }
  
  private void a(Object paramObject, a parama)
    throws Error
  {
    Class localClass = paramObject.getClass();
    boolean bool1 = false;
    if (this.s)
    {
      List localList = a(localClass);
      int i2 = localList.size();
      int i1 = 0;
      for (;;)
      {
        bool2 = bool1;
        if (i1 >= i2) {
          break;
        }
        bool1 |= a(paramObject, parama, (Class)localList.get(i1));
        i1 += 1;
      }
    }
    boolean bool2 = a(paramObject, parama, localClass);
    if (!bool2)
    {
      if (this.p) {
        Log.d(a, "No subscribers registered for event " + localClass);
      }
      if ((this.r) && (localClass != g.class) && (localClass != j.class)) {
        c(new g(this, paramObject));
      }
    }
  }
  
  private void a(Object arg1, k paramk, boolean paramBoolean, int paramInt)
  {
    Class localClass = paramk.c;
    Object localObject = (CopyOnWriteArrayList)this.e.get(localClass);
    m localm = new m(???, paramk, paramInt);
    if (localObject == null)
    {
      paramk = new CopyOnWriteArrayList();
      this.e.put(localClass, paramk);
      int i1 = paramk.size();
      paramInt = 0;
      if (paramInt <= i1)
      {
        if ((paramInt != i1) && (localm.c <= ((m)paramk.get(paramInt)).c)) {
          break label268;
        }
        paramk.add(paramInt, localm);
      }
      localObject = (List)this.f.get(???);
      paramk = (k)localObject;
      if (localObject == null)
      {
        paramk = new ArrayList();
        this.f.put(???, paramk);
      }
      paramk.add(localClass);
      if (!paramBoolean) {}
    }
    for (;;)
    {
      synchronized (this.g)
      {
        paramk = this.g.get(localClass);
        if (paramk != null)
        {
          if (Looper.getMainLooper() == Looper.myLooper())
          {
            paramBoolean = true;
            a(localm, paramk, paramBoolean);
          }
        }
        else
        {
          return;
          paramk = (k)localObject;
          if (!((CopyOnWriteArrayList)localObject).contains(localm)) {
            break;
          }
          throw new e("Subscriber " + ???.getClass() + " already registered to event " + localClass);
          label268:
          paramInt += 1;
        }
      }
      paramBoolean = false;
    }
  }
  
  private void a(Object paramObject, Class<?> paramClass)
  {
    paramClass = (List)this.e.get(paramClass);
    if (paramClass != null)
    {
      int i2 = paramClass.size();
      int i1 = 0;
      while (i1 < i2)
      {
        m localm = (m)paramClass.get(i1);
        int i4 = i1;
        int i3 = i2;
        if (localm.a == paramObject)
        {
          localm.d = false;
          paramClass.remove(i1);
          i4 = i1 - 1;
          i3 = i2 - 1;
        }
        i1 = i4 + 1;
        i2 = i3;
      }
    }
  }
  
  private void a(Object paramObject, boolean paramBoolean, int paramInt)
  {
    try
    {
      Iterator localIterator = this.l.a(paramObject.getClass()).iterator();
      while (localIterator.hasNext()) {
        a(paramObject, (k)localIterator.next(), paramBoolean, paramInt);
      }
    }
    finally {}
  }
  
  static void a(List<Class<?>> paramList, Class<?>[] paramArrayOfClass)
  {
    int i2 = paramArrayOfClass.length;
    int i1 = 0;
    while (i1 < i2)
    {
      Class<?> localClass = paramArrayOfClass[i1];
      if (!paramList.contains(localClass))
      {
        paramList.add(localClass);
        a(paramList, localClass.getInterfaces());
      }
      i1 += 1;
    }
  }
  
  private boolean a(Object paramObject, a parama, Class<?> paramClass)
  {
    boolean bool2 = false;
    try
    {
      paramClass = (CopyOnWriteArrayList)this.e.get(paramClass);
      bool1 = bool2;
      if (paramClass == null) {
        break label116;
      }
      bool1 = bool2;
      if (paramClass.isEmpty()) {
        break label116;
      }
      paramClass = paramClass.iterator();
    }
    finally
    {
      try
      {
        do
        {
          m localm;
          a(localm, paramObject, parama.c);
          bool1 = parama.f;
          parama.e = null;
          parama.d = null;
          parama.f = false;
        } while (!bool1);
        boolean bool1 = true;
        return bool1;
      }
      finally
      {
        parama.e = null;
        parama.d = null;
        parama.f = false;
      }
      paramObject = finally;
    }
    if (paramClass.hasNext())
    {
      localm = (m)paramClass.next();
      parama.e = paramObject;
      parama.d = localm;
    }
  }
  
  void a(h paramh)
  {
    Object localObject = paramh.a;
    m localm = paramh.b;
    h.a(paramh);
    if (localm.d) {
      a(localm, localObject);
    }
  }
  
  void a(m paramm, Object paramObject)
  {
    try
    {
      paramm.b.a.invoke(paramm.a, new Object[] { paramObject });
      return;
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      a(paramm, paramObject, localInvocationTargetException.getCause());
      return;
    }
    catch (IllegalAccessException paramm)
    {
      throw new IllegalStateException("Unexpected exception", paramm);
    }
  }
  
  public void a(Object paramObject)
  {
    a(paramObject, true, 0);
  }
  
  ExecutorService b()
  {
    return this.m;
  }
  
  public void b(Object paramObject)
  {
    try
    {
      Object localObject = (List)this.f.get(paramObject);
      if (localObject != null)
      {
        localObject = ((List)localObject).iterator();
        while (((Iterator)localObject).hasNext()) {
          a(paramObject, (Class)((Iterator)localObject).next());
        }
        this.f.remove(paramObject);
      }
    }
    finally {}
    for (;;)
    {
      return;
      Log.w(a, "Subscriber to unregister was not registered before: " + paramObject.getClass());
    }
  }
  
  /* Error */
  public void c(Object paramObject)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 73	b/a/a/c:h	Ljava/lang/ThreadLocal;
    //   4: invokevirtual 415	java/lang/ThreadLocal:get	()Ljava/lang/Object;
    //   7: checkcast 10	b/a/a/c$a
    //   10: astore_3
    //   11: aload_3
    //   12: getfield 417	b/a/a/c$a:a	Ljava/util/List;
    //   15: astore 4
    //   17: aload 4
    //   19: aload_1
    //   20: invokeinterface 163 2 0
    //   25: pop
    //   26: aload_3
    //   27: getfield 418	b/a/a/c$a:b	Z
    //   30: ifne +96 -> 126
    //   33: invokestatic 90	android/os/Looper:getMainLooper	()Landroid/os/Looper;
    //   36: invokestatic 321	android/os/Looper:myLooper	()Landroid/os/Looper;
    //   39: if_acmpne +33 -> 72
    //   42: iconst_1
    //   43: istore_2
    //   44: aload_3
    //   45: iload_2
    //   46: putfield 367	b/a/a/c$a:c	Z
    //   49: aload_3
    //   50: iconst_1
    //   51: putfield 418	b/a/a/c$a:b	Z
    //   54: aload_3
    //   55: getfield 368	b/a/a/c$a:f	Z
    //   58: ifeq +19 -> 77
    //   61: new 229	b/a/a/e
    //   64: dup
    //   65: ldc_w 420
    //   68: invokespecial 331	b/a/a/e:<init>	(Ljava/lang/String;)V
    //   71: athrow
    //   72: iconst_0
    //   73: istore_2
    //   74: goto -30 -> 44
    //   77: aload 4
    //   79: invokeinterface 421 1 0
    //   84: ifne +32 -> 116
    //   87: aload_0
    //   88: aload 4
    //   90: iconst_0
    //   91: invokeinterface 336 2 0
    //   96: aload_3
    //   97: invokespecial 423	b/a/a/c:a	(Ljava/lang/Object;Lb/a/a/c$a;)V
    //   100: goto -23 -> 77
    //   103: astore_1
    //   104: aload_3
    //   105: iconst_0
    //   106: putfield 418	b/a/a/c$a:b	Z
    //   109: aload_3
    //   110: iconst_0
    //   111: putfield 367	b/a/a/c$a:c	Z
    //   114: aload_1
    //   115: athrow
    //   116: aload_3
    //   117: iconst_0
    //   118: putfield 418	b/a/a/c$a:b	Z
    //   121: aload_3
    //   122: iconst_0
    //   123: putfield 367	b/a/a/c$a:c	Z
    //   126: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	127	0	this	c
    //   0	127	1	paramObject	Object
    //   43	31	2	bool	boolean
    //   10	112	3	locala	a
    //   15	74	4	localList	List
    // Exception table:
    //   from	to	target	type
    //   77	100	103	finally
  }
  
  public boolean d(Object paramObject)
  {
    synchronized (this.g)
    {
      Class localClass = paramObject.getClass();
      if (paramObject.equals(this.g.get(localClass)))
      {
        this.g.remove(localClass);
        return true;
      }
      return false;
    }
  }
  
  static final class a
  {
    final List<Object> a = new ArrayList();
    boolean b;
    boolean c;
    m d;
    Object e;
    boolean f;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/b/a/a/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */