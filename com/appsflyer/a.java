package com.appsflyer;

import android.util.Log;

public class a
{
  public static final String a = "AppsFlyer_" + AppsFlyerLib.a + "." + AppsFlyerLib.b;
  
  public static void a(String paramString)
  {
    if (a()) {
      Log.i(a, paramString);
    }
  }
  
  public static void a(String paramString, Throwable paramThrowable)
  {
    if (a()) {
      Log.e(a, paramString, paramThrowable);
    }
  }
  
  private static boolean a()
  {
    return e.a().e();
  }
  
  public static void b(String paramString)
  {
    if (a()) {
      Log.d(a, paramString);
    }
  }
  
  private static boolean b()
  {
    return e.a().f();
  }
  
  public static void c(String paramString)
  {
    if (a()) {
      Log.w(a, paramString);
    }
  }
  
  public static void d(String paramString)
  {
    if (!b()) {
      Log.d(a, paramString);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/appsflyer/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */