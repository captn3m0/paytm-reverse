package com.appsflyer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class e
{
  private static e a = new e();
  private Map<String, Object> b = new HashMap();
  private boolean c;
  private boolean d;
  private String e;
  
  public static e a()
  {
    return a;
  }
  
  public String a(Context paramContext)
  {
    if (this.e != null) {
      return this.e;
    }
    if (a("AF_REFERRER") != null) {
      return a("AF_REFERRER");
    }
    return paramContext.getSharedPreferences("appsflyer-data", 0).getString("referrer", null);
  }
  
  public String a(String paramString)
  {
    return (String)this.b.get(paramString);
  }
  
  public void a(String paramString1, String paramString2)
  {
    this.b.put(paramString1, paramString2);
  }
  
  public void a(String paramString, boolean paramBoolean)
  {
    this.b.put(paramString, Boolean.toString(paramBoolean));
  }
  
  protected void a(boolean paramBoolean)
  {
    this.d = paramBoolean;
  }
  
  protected void b()
  {
    this.c = true;
  }
  
  @SuppressLint({"CommitPrefEdits"})
  public void b(Context paramContext)
  {
    String str = new JSONObject(this.b).toString();
    paramContext = paramContext.getSharedPreferences("appsflyer-data", 0).edit();
    paramContext.putString("savedProperties", str);
    if (Build.VERSION.SDK_INT >= 9)
    {
      paramContext.apply();
      return;
    }
    paramContext.commit();
  }
  
  protected void b(String paramString)
  {
    a("AF_REFERRER", paramString);
    this.e = paramString;
  }
  
  public void b(boolean paramBoolean)
  {
    a("shouldLog", paramBoolean);
  }
  
  public boolean b(String paramString, boolean paramBoolean)
  {
    paramString = a(paramString);
    if (paramString == null) {
      return paramBoolean;
    }
    return Boolean.valueOf(paramString).booleanValue();
  }
  
  public void c(Context paramContext)
  {
    paramContext = paramContext.getSharedPreferences("appsflyer-data", 0).getString("savedProperties", null);
    if (paramContext != null) {
      try
      {
        paramContext = new JSONObject(paramContext);
        Iterator localIterator = paramContext.keys();
        while (localIterator.hasNext())
        {
          String str = (String)localIterator.next();
          if (this.b.get(str) == null) {
            this.b.put(str, paramContext.getString(str));
          }
        }
        return;
      }
      catch (JSONException paramContext)
      {
        a.a("Failed loading properties", paramContext);
      }
    }
  }
  
  protected boolean c()
  {
    return this.d;
  }
  
  protected void d()
  {
    this.d = true;
  }
  
  public boolean e()
  {
    return b("shouldLog", true);
  }
  
  public boolean f()
  {
    return b("disableLogs", false);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/appsflyer/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */