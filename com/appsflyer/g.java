package com.appsflyer;

import android.content.Context;
import android.os.AsyncTask;
import java.util.Map;
import org.json.JSONObject;

public class g
  extends AsyncTask<String, Void, String>
{
  public Map<String, String> a;
  private String b = null;
  private boolean c = false;
  private String d;
  private Context e;
  
  public g(Context paramContext)
  {
    this.e = paramContext;
  }
  
  /* Error */
  protected String a(String... paramVarArgs)
  {
    // Byte code:
    //   0: new 38	java/net/URL
    //   3: dup
    //   4: aload_1
    //   5: iconst_0
    //   6: aaload
    //   7: invokespecial 41	java/net/URL:<init>	(Ljava/lang/String;)V
    //   10: astore_1
    //   11: new 43	java/lang/StringBuilder
    //   14: dup
    //   15: invokespecial 44	java/lang/StringBuilder:<init>	()V
    //   18: ldc 46
    //   20: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   23: aload_1
    //   24: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   27: ldc 55
    //   29: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   32: aload_0
    //   33: getfield 57	com/appsflyer/g:a	Ljava/util/Map;
    //   36: invokevirtual 63	java/lang/Object:toString	()Ljava/lang/String;
    //   39: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   42: invokevirtual 64	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   45: invokestatic 68	com/appsflyer/a:a	(Ljava/lang/String;)V
    //   48: aload_1
    //   49: invokevirtual 72	java/net/URL:openConnection	()Ljava/net/URLConnection;
    //   52: checkcast 74	javax/net/ssl/HttpsURLConnection
    //   55: astore_1
    //   56: aload_1
    //   57: sipush 30000
    //   60: invokevirtual 78	javax/net/ssl/HttpsURLConnection:setReadTimeout	(I)V
    //   63: aload_1
    //   64: sipush 30000
    //   67: invokevirtual 81	javax/net/ssl/HttpsURLConnection:setConnectTimeout	(I)V
    //   70: aload_1
    //   71: ldc 83
    //   73: invokevirtual 86	javax/net/ssl/HttpsURLConnection:setRequestMethod	(Ljava/lang/String;)V
    //   76: aload_1
    //   77: iconst_1
    //   78: invokevirtual 90	javax/net/ssl/HttpsURLConnection:setDoInput	(Z)V
    //   81: aload_1
    //   82: iconst_1
    //   83: invokevirtual 93	javax/net/ssl/HttpsURLConnection:setDoOutput	(Z)V
    //   86: aload_1
    //   87: ldc 95
    //   89: ldc 97
    //   91: invokevirtual 101	javax/net/ssl/HttpsURLConnection:setRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   94: aload_1
    //   95: invokevirtual 105	javax/net/ssl/HttpsURLConnection:getOutputStream	()Ljava/io/OutputStream;
    //   98: astore_2
    //   99: new 107	java/io/BufferedWriter
    //   102: dup
    //   103: new 109	java/io/OutputStreamWriter
    //   106: dup
    //   107: aload_2
    //   108: ldc 111
    //   110: invokespecial 114	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;Ljava/lang/String;)V
    //   113: invokespecial 117	java/io/BufferedWriter:<init>	(Ljava/io/Writer;)V
    //   116: astore_3
    //   117: aload_3
    //   118: aload_0
    //   119: getfield 119	com/appsflyer/g:d	Ljava/lang/String;
    //   122: invokevirtual 122	java/io/BufferedWriter:write	(Ljava/lang/String;)V
    //   125: aload_3
    //   126: invokevirtual 125	java/io/BufferedWriter:flush	()V
    //   129: aload_3
    //   130: invokevirtual 128	java/io/BufferedWriter:close	()V
    //   133: aload_2
    //   134: invokevirtual 131	java/io/OutputStream:close	()V
    //   137: aload_1
    //   138: invokevirtual 134	javax/net/ssl/HttpsURLConnection:connect	()V
    //   141: aload_1
    //   142: invokevirtual 138	javax/net/ssl/HttpsURLConnection:getResponseCode	()I
    //   145: sipush 200
    //   148: if_icmpne +85 -> 233
    //   151: new 140	java/io/BufferedReader
    //   154: dup
    //   155: new 142	java/io/InputStreamReader
    //   158: dup
    //   159: aload_1
    //   160: invokevirtual 146	javax/net/ssl/HttpsURLConnection:getInputStream	()Ljava/io/InputStream;
    //   163: invokespecial 149	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   166: invokespecial 152	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   169: astore_1
    //   170: aload_1
    //   171: invokevirtual 155	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   174: astore_2
    //   175: aload_2
    //   176: ifnull +62 -> 238
    //   179: aload_0
    //   180: new 43	java/lang/StringBuilder
    //   183: dup
    //   184: invokespecial 44	java/lang/StringBuilder:<init>	()V
    //   187: aload_0
    //   188: getfield 22	com/appsflyer/g:b	Ljava/lang/String;
    //   191: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   194: aload_2
    //   195: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   198: invokevirtual 64	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   201: putfield 22	com/appsflyer/g:b	Ljava/lang/String;
    //   204: goto -34 -> 170
    //   207: astore_1
    //   208: new 43	java/lang/StringBuilder
    //   211: dup
    //   212: invokespecial 44	java/lang/StringBuilder:<init>	()V
    //   215: ldc -99
    //   217: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   220: aload_1
    //   221: invokevirtual 158	java/net/MalformedURLException:toString	()Ljava/lang/String;
    //   224: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   227: invokevirtual 64	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   230: invokestatic 68	com/appsflyer/a:a	(Ljava/lang/String;)V
    //   233: aload_0
    //   234: getfield 22	com/appsflyer/g:b	Ljava/lang/String;
    //   237: areturn
    //   238: ldc -96
    //   240: invokestatic 68	com/appsflyer/a:a	(Ljava/lang/String;)V
    //   243: goto -10 -> 233
    //   246: astore_1
    //   247: new 43	java/lang/StringBuilder
    //   250: dup
    //   251: invokespecial 44	java/lang/StringBuilder:<init>	()V
    //   254: ldc -94
    //   256: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   259: aload_1
    //   260: invokevirtual 163	java/net/ProtocolException:toString	()Ljava/lang/String;
    //   263: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   266: invokevirtual 64	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   269: invokestatic 68	com/appsflyer/a:a	(Ljava/lang/String;)V
    //   272: goto -39 -> 233
    //   275: astore_1
    //   276: new 43	java/lang/StringBuilder
    //   279: dup
    //   280: invokespecial 44	java/lang/StringBuilder:<init>	()V
    //   283: ldc -91
    //   285: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   288: aload_1
    //   289: invokevirtual 166	java/io/IOException:toString	()Ljava/lang/String;
    //   292: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   295: invokevirtual 64	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   298: invokestatic 68	com/appsflyer/a:a	(Ljava/lang/String;)V
    //   301: goto -68 -> 233
    //   304: astore_1
    //   305: new 43	java/lang/StringBuilder
    //   308: dup
    //   309: invokespecial 44	java/lang/StringBuilder:<init>	()V
    //   312: ldc -88
    //   314: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   317: aload_1
    //   318: invokevirtual 169	java/lang/Exception:toString	()Ljava/lang/String;
    //   321: invokevirtual 50	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   324: invokevirtual 64	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   327: invokestatic 68	com/appsflyer/a:a	(Ljava/lang/String;)V
    //   330: goto -97 -> 233
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	333	0	this	g
    //   0	333	1	paramVarArgs	String[]
    //   98	97	2	localObject	Object
    //   116	14	3	localBufferedWriter	java.io.BufferedWriter
    // Exception table:
    //   from	to	target	type
    //   0	170	207	java/net/MalformedURLException
    //   170	175	207	java/net/MalformedURLException
    //   179	204	207	java/net/MalformedURLException
    //   238	243	207	java/net/MalformedURLException
    //   0	170	246	java/net/ProtocolException
    //   170	175	246	java/net/ProtocolException
    //   179	204	246	java/net/ProtocolException
    //   238	243	246	java/net/ProtocolException
    //   0	170	275	java/io/IOException
    //   170	175	275	java/io/IOException
    //   179	204	275	java/io/IOException
    //   238	243	275	java/io/IOException
    //   0	170	304	java/lang/Exception
    //   170	175	304	java/lang/Exception
    //   179	204	304	java/lang/Exception
    //   238	243	304	java/lang/Exception
  }
  
  protected void a(String paramString)
  {
    if (this.c)
    {
      a.a("Connection error");
      return;
    }
    a.a("Connection call succeeded");
  }
  
  protected void onCancelled() {}
  
  protected void onPreExecute()
  {
    JSONObject localJSONObject = new JSONObject(this.a);
    if (localJSONObject != null) {
      this.d = localJSONObject.toString();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/appsflyer/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */