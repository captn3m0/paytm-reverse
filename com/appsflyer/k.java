package com.appsflyer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Random;
import java.util.UUID;

public class k
{
  private static String a = null;
  
  public static String a(Context paramContext)
  {
    for (;;)
    {
      Object localObject;
      try
      {
        if (a == null)
        {
          localObject = c(paramContext);
          if (localObject != null) {
            a = (String)localObject;
          }
        }
        else
        {
          paramContext = a;
          return paramContext;
        }
        localObject = new File(paramContext.getFilesDir(), "AF_INSTALLATION");
        try
        {
          if (!((File)localObject).exists())
          {
            a = b(paramContext);
            a(paramContext, a);
          }
        }
        catch (Exception paramContext)
        {
          throw new RuntimeException(paramContext);
        }
        a = a((File)localObject);
      }
      finally {}
      ((File)localObject).delete();
    }
  }
  
  private static String a(File paramFile)
    throws IOException
  {
    paramFile = new RandomAccessFile(paramFile, "r");
    byte[] arrayOfByte = new byte[(int)paramFile.length()];
    paramFile.readFully(arrayOfByte);
    paramFile.close();
    return new String(arrayOfByte);
  }
  
  @SuppressLint({"CommitPrefEdits"})
  private static void a(Context paramContext, String paramString)
    throws PackageManager.NameNotFoundException
  {
    paramContext = paramContext.getSharedPreferences("appsflyer-data", 0).edit();
    paramContext.putString("AF_INSTALLATION", paramString);
    if (Build.VERSION.SDK_INT >= 9)
    {
      paramContext.apply();
      return;
    }
    paramContext.commit();
  }
  
  private static String b(Context paramContext)
    throws PackageManager.NameNotFoundException
  {
    paramContext = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 0);
    if (Build.VERSION.SDK_INT >= 9) {
      return paramContext.firstInstallTime + "-" + Math.abs(new Random().nextLong());
    }
    return UUID.randomUUID().toString();
  }
  
  private static String c(Context paramContext)
  {
    return paramContext.getSharedPreferences("appsflyer-data", 0).getString("AF_INSTALLATION", null);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/appsflyer/k.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */