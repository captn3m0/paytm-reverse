package com.appsflyer;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class i
  implements Application.ActivityLifecycleCallbacks
{
  private static i a;
  private boolean b = false;
  private boolean c = true;
  private Handler d = new Handler();
  private List<a> e = new CopyOnWriteArrayList();
  private Runnable f;
  
  public static i a()
  {
    if (a == null) {
      throw new IllegalStateException("Foreground is not initialised - invoke at least once with parameter init/get");
    }
    return a;
  }
  
  public static i a(Application paramApplication)
  {
    if (a == null)
    {
      a = new i();
      if (Build.VERSION.SDK_INT >= 14) {
        paramApplication.registerActivityLifecycleCallbacks(a);
      }
    }
    return a;
  }
  
  public void a(a parama)
  {
    this.e.add(parama);
  }
  
  public void onActivityCreated(Activity paramActivity, Bundle paramBundle) {}
  
  public void onActivityDestroyed(Activity paramActivity) {}
  
  public void onActivityPaused(final Activity paramActivity)
  {
    this.c = true;
    if (this.f != null) {
      this.d.removeCallbacks(this.f);
    }
    this.d.postDelayed(new Runnable()
    {
      public void run()
      {
        if ((i.a(i.this)) && (i.b(i.this)))
        {
          i.a(i.this, false);
          Iterator localIterator = i.c(i.this).iterator();
          while (localIterator.hasNext())
          {
            i.a locala = (i.a)localIterator.next();
            try
            {
              locala.b(paramActivity);
            }
            catch (Exception localException)
            {
              a.a("Listener threw exception! ", localException);
            }
          }
        }
      }
    }, 500L);
  }
  
  public void onActivityResumed(Activity paramActivity)
  {
    int i = 0;
    this.c = false;
    if (!this.b) {
      i = 1;
    }
    this.b = true;
    if (this.f != null) {
      this.d.removeCallbacks(this.f);
    }
    if (i != 0)
    {
      Iterator localIterator = this.e.iterator();
      while (localIterator.hasNext())
      {
        a locala = (a)localIterator.next();
        try
        {
          locala.a(paramActivity);
        }
        catch (Exception localException)
        {
          a.a("Listener threw exception! ", localException);
        }
      }
    }
  }
  
  public void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {}
  
  public void onActivityStarted(Activity paramActivity) {}
  
  public void onActivityStopped(Activity paramActivity) {}
  
  public static abstract interface a
  {
    public abstract void a(Activity paramActivity);
    
    public abstract void b(Activity paramActivity);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/appsflyer/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */