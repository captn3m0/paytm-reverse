package com.appsflyer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Process;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.iid.InstanceID;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONException;
import org.json.JSONObject;

public class AppsFlyerLib
  extends BroadcastReceiver
{
  public static final String a = "4.3.8".substring(0, "4.3.8".indexOf("."));
  public static final String b = "4.3.8".substring("4.3.8".indexOf(".") + 1);
  public static final String c = "AppsFlyer_" + b;
  public static final String d = "https://t.appsflyer.com/api/v" + a + "/androidevent?buildnumber=" + b + "&app_id=";
  public static final String e = "https://events.appsflyer.com/api/v" + a + "/androidevent?buildnumber=" + b + "&app_id=";
  private static final String f = "https://register.appsflyer.com/api/v" + a + "/androidevent?buildnumber=" + b + "&app_id=";
  private static final List<String> g = Arrays.asList(new String[] { "is_cache" });
  private static String h;
  private static String i;
  private static c j = null;
  private static d k = null;
  private static boolean l = false;
  private static long m;
  private static ScheduledExecutorService n = null;
  private static long o = System.currentTimeMillis();
  private static AppsFlyerLib p = new AppsFlyerLib();
  private i.a q;
  private long r;
  
  private int a(Context paramContext, String paramString, boolean paramBoolean)
  {
    paramContext = paramContext.getSharedPreferences("appsflyer-data", 0);
    int i2 = paramContext.getInt(paramString, 0);
    int i1 = i2;
    if (paramBoolean)
    {
      i1 = i2 + 1;
      paramContext = paramContext.edit();
      paramContext.putInt(paramString, i1);
      a(paramContext);
    }
    return i1;
  }
  
  private long a(Context paramContext, boolean paramBoolean)
  {
    long l1 = paramContext.getSharedPreferences("appsflyer-data", 0).getLong("AppsFlyerTimePassedSincePrevLaunch", 0L);
    long l2 = System.currentTimeMillis();
    if (l1 > 0L) {}
    for (l1 = l2 - l1;; l1 = -1L)
    {
      if (paramBoolean) {
        a(paramContext, "AppsFlyerTimePassedSincePrevLaunch", l2);
      }
      return l1 / 1000L;
    }
  }
  
  public static AppsFlyerLib a()
  {
    return p;
  }
  
  private String a(SimpleDateFormat paramSimpleDateFormat, Context paramContext)
  {
    String str = paramContext.getSharedPreferences("appsflyer-data", 0).getString("appsFlyerFirstInstall", null);
    Object localObject = str;
    if (str == null)
    {
      if (!i(paramContext)) {
        break label84;
      }
      a.b("AppsFlyer: first launch detected");
    }
    label84:
    for (paramSimpleDateFormat = paramSimpleDateFormat.format(new Date());; paramSimpleDateFormat = "")
    {
      a(paramContext, "appsFlyerFirstInstall", paramSimpleDateFormat);
      localObject = paramSimpleDateFormat;
      a.a("AppsFlyer: first launch date: " + (String)localObject);
      return (String)localObject;
    }
  }
  
  private Map<String, String> a(Context paramContext, String paramString)
  {
    LinkedHashMap localLinkedHashMap = new LinkedHashMap();
    String[] arrayOfString = paramString.split("&");
    int i1 = 0;
    int i4 = arrayOfString.length;
    int i2 = 0;
    if (i2 < i4)
    {
      String str2 = arrayOfString[i2];
      int i5 = str2.indexOf("=");
      label65:
      int i3;
      if (i5 > 0)
      {
        str1 = str2.substring(0, i5);
        i3 = i1;
        paramString = str1;
        if (!localLinkedHashMap.containsKey(str1))
        {
          if (!str1.equals("c")) {
            break label173;
          }
          paramString = "campaign";
          label96:
          localLinkedHashMap.put(paramString, new String());
          i3 = i1;
        }
        if ((i5 <= 0) || (str2.length() <= i5 + 1)) {
          break label214;
        }
      }
      label173:
      label214:
      for (String str1 = str2.substring(i5 + 1);; str1 = null)
      {
        localLinkedHashMap.put(paramString, str1);
        i2 += 1;
        i1 = i3;
        break;
        str1 = str2;
        break label65;
        if (str1.equals("pid"))
        {
          paramString = "media_source";
          break label96;
        }
        paramString = str1;
        if (!str1.equals("af_prt")) {
          break label96;
        }
        i1 = 1;
        paramString = "agency";
        break label96;
      }
    }
    try
    {
      if (!localLinkedHashMap.containsKey("install_time"))
      {
        long l1 = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 0).firstInstallTime;
        localLinkedHashMap.put("install_time", new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date(l1)));
      }
      if (!localLinkedHashMap.containsKey("af_status")) {
        localLinkedHashMap.put("af_status", "Non-organic");
      }
      if (i1 != 0) {
        localLinkedHashMap.remove("media_source");
      }
      return localLinkedHashMap;
    }
    catch (Exception paramContext)
    {
      for (;;)
      {
        a.c("Could not fetch install time");
      }
    }
  }
  
  private void a(Application paramApplication)
  {
    if (this.q == null)
    {
      e.a().c(paramApplication.getApplicationContext());
      if (Build.VERSION.SDK_INT >= 14)
      {
        i.a(paramApplication);
        this.q = new i.a()
        {
          public void a(Activity paramAnonymousActivity)
          {
            a.a("onBecameForeground");
            AppsFlyerLib.this.a(paramAnonymousActivity, null, null);
          }
          
          public void b(Activity paramAnonymousActivity)
          {
            a.a("onBecameBackground");
            a.a("callStatsBackground background call");
            AppsFlyerLib.a(AppsFlyerLib.this, paramAnonymousActivity.getApplicationContext());
          }
        };
        i.a().a(this.q);
      }
    }
    else
    {
      return;
    }
    a.a("SDK<14 call trackAppLaunch manually");
    a(paramApplication.getApplicationContext(), null, null);
  }
  
  private void a(Context paramContext, String paramString, int paramInt)
  {
    paramContext = paramContext.getSharedPreferences("appsflyer-data", 0).edit();
    paramContext.putInt(paramString, paramInt);
    a(paramContext);
  }
  
  private void a(Context paramContext, String paramString, long paramLong)
  {
    paramContext = paramContext.getSharedPreferences("appsflyer-data", 0).edit();
    paramContext.putLong(paramString, paramLong);
    a(paramContext);
  }
  
  private void a(Context paramContext, String paramString1, String paramString2)
  {
    paramContext = paramContext.getSharedPreferences("appsflyer-data", 0).edit();
    paramContext.putString(paramString1, paramString2);
    a(paramContext);
  }
  
  private void a(Context paramContext, String paramString1, String paramString2, String paramString3)
  {
    if (e.a().b("shouldMonitor", false))
    {
      Intent localIntent = new Intent("com.appsflyer.MonitorBroadcast");
      localIntent.setPackage("com.appsflyer.nightvision");
      localIntent.putExtra("message", paramString2);
      localIntent.putExtra("value", paramString3);
      localIntent.putExtra("packageName", "true");
      localIntent.putExtra("pid", new Integer(Process.myPid()));
      localIntent.putExtra("eventIdentifier", paramString1);
      localIntent.putExtra("sdk", a + '.' + b);
      paramContext.sendBroadcast(localIntent);
    }
  }
  
  private void a(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean)
  {
    ScheduledExecutorService localScheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
    localScheduledExecutorService.schedule(new c(paramContext, paramString1, paramString2, paramString3, paramString4, paramBoolean, localScheduledExecutorService, null), 5L, TimeUnit.MILLISECONDS);
  }
  
  private static void a(Context paramContext, HashMap<String, String> paramHashMap)
  {
    Intent localIntent = new Intent("com.appsflyer.testIntgrationBroadcast");
    localIntent.putExtra("params", paramHashMap);
    paramContext.sendBroadcast(localIntent);
  }
  
  private void a(Context paramContext, Map<String, String> paramMap)
  {
    if (e.a().b("deviceTrackingDisabled", false))
    {
      paramMap.put("deviceTrackingDisabled", "true");
      return;
    }
    SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("appsflyer-data", 0);
    boolean bool = e.a().b("collectIMEI", true);
    String str1 = localSharedPreferences.getString("imeiCached", null);
    Object localObject1 = null;
    if (bool) {
      if (h(paramContext))
      {
        try
        {
          localObject2 = (TelephonyManager)paramContext.getSystemService("phone");
          localObject2 = (String)localObject2.getClass().getMethod("getDeviceId", new Class[0]).invoke(localObject2, new Object[0]);
          if (localObject2 == null) {
            break label240;
          }
          localObject1 = localObject2;
        }
        catch (Exception localException1)
        {
          for (;;)
          {
            Object localObject2;
            label152:
            a.a("WARNING: READ_PHONE_STATE is missing");
          }
        }
        if (localObject1 == null) {
          break label309;
        }
        a(paramContext, "imeiCached", (String)localObject1);
        paramMap.put("imei", localObject1);
        bool = e.a().b("collectAndroidId", true);
        str1 = localSharedPreferences.getString("androidIdCached", null);
        localObject1 = null;
        if ((bool) && (!h(paramContext))) {
          break label348;
        }
      }
    }
    for (;;)
    {
      try
      {
        localObject2 = Settings.Secure.getString(paramContext.getContentResolver(), "android_id");
        if (localObject2 == null) {
          continue;
        }
        localObject1 = localObject2;
      }
      catch (Exception localException2)
      {
        label240:
        label309:
        String str2;
        label348:
        continue;
      }
      if (localObject1 == null) {
        continue;
      }
      a(paramContext, "androidIdCached", (String)localObject1);
      paramMap.put("android_id", localObject1);
      return;
      if (h != null)
      {
        str1 = h;
        localObject1 = str1;
        break;
      }
      if (str1 == null) {
        break;
      }
      localObject1 = str1;
      break;
      if (h == null) {
        break;
      }
      localObject1 = h;
      break;
      if (h == null) {
        break;
      }
      localObject1 = h;
      break;
      a.a("IMEI was not collected.");
      break label152;
      if (i != null)
      {
        str2 = i;
        localObject1 = str2;
      }
      else if (str2 != null)
      {
        localObject1 = str2;
        continue;
        if (i != null)
        {
          localObject1 = i;
          continue;
          if (i != null) {
            localObject1 = i;
          }
        }
      }
    }
    a.a("Android ID was not collected.");
  }
  
  private void a(Context paramContext, Map<String, String> paramMap, String paramString1, String paramString2)
  {
    paramContext = paramContext.getSharedPreferences("appsflyer-data", 0);
    SharedPreferences.Editor localEditor = paramContext.edit();
    try
    {
      String str = paramContext.getString("prev_event_name", null);
      if (str != null)
      {
        JSONObject localJSONObject = new JSONObject();
        localJSONObject.put("prev_event_timestamp", paramContext.getLong("prev_event_timestamp", -1L) + "");
        localJSONObject.put("prev_event_value", paramContext.getString("prev_event_value", null));
        localJSONObject.put("prev_event_name", str);
        paramMap.put("prev_event", localJSONObject.toString());
      }
      localEditor.putString("prev_event_name", paramString1);
      localEditor.putString("prev_event_value", paramString2);
      localEditor.putLong("prev_event_timestamp", System.currentTimeMillis());
      a(localEditor);
      return;
    }
    catch (Exception paramContext)
    {
      a.a("Error while processing previous event.", paramContext);
    }
  }
  
  @SuppressLint({"CommitPrefEdits"})
  private void a(SharedPreferences.Editor paramEditor)
  {
    if (Build.VERSION.SDK_INT >= 9)
    {
      paramEditor.apply();
      return;
    }
    paramEditor.commit();
  }
  
  private void a(String paramString1, String paramString2, Context paramContext)
  {
    if (paramContext != null) {}
    try
    {
      if (e(paramContext.getPackageName())) {
        h.a().a(paramString1 + paramString2);
      }
      return;
    }
    catch (Exception paramString1)
    {
      a.a(paramString1.toString());
    }
  }
  
  private void a(String paramString1, String paramString2, String paramString3, WeakReference<Context> paramWeakReference, String paramString4, boolean paramBoolean)
    throws IOException
  {
    URL localURL = new URL(paramString1);
    a.a("url: " + localURL.toString());
    a("call server.", "\n" + localURL.toString() + "\nPOST:" + paramString2, (Context)paramWeakReference.get());
    l.b("data: " + paramString2);
    a((Context)paramWeakReference.get(), c, "EVENT_DATA", paramString2);
    try
    {
      a(localURL, paramString2, paramString3, paramWeakReference, paramString4, paramBoolean);
      return;
    }
    catch (IOException localIOException)
    {
      if (e.a().b("useHttpFallback", false))
      {
        a("https failed: " + localIOException.getLocalizedMessage(), "", (Context)paramWeakReference.get());
        a(new URL(paramString1.replace("https:", "http:")), paramString2, paramString3, paramWeakReference, paramString4, paramBoolean);
        return;
      }
      a.a("failed to send requeset to server. " + localIOException.getLocalizedMessage());
      a((Context)paramWeakReference.get(), c, "ERROR", localIOException.getLocalizedMessage());
      throw localIOException;
    }
  }
  
  /* Error */
  private void a(URL paramURL, String paramString1, String paramString2, WeakReference<Context> paramWeakReference, String paramString3, boolean paramBoolean)
    throws IOException
  {
    // Byte code:
    //   0: aload 4
    //   2: invokevirtual 596	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
    //   5: checkcast 138	android/content/Context
    //   8: astore 13
    //   10: aconst_null
    //   11: astore 10
    //   13: aload_1
    //   14: invokevirtual 636	java/net/URL:openConnection	()Ljava/net/URLConnection;
    //   17: checkcast 638	java/net/HttpURLConnection
    //   20: astore 11
    //   22: aload 11
    //   24: astore 10
    //   26: aload 11
    //   28: ldc_w 640
    //   31: invokevirtual 643	java/net/HttpURLConnection:setRequestMethod	(Ljava/lang/String;)V
    //   34: aload 11
    //   36: astore 10
    //   38: aload_2
    //   39: invokevirtual 647	java/lang/String:getBytes	()[B
    //   42: arraylength
    //   43: istore 7
    //   45: aload 11
    //   47: astore 10
    //   49: aload 11
    //   51: ldc_w 649
    //   54: new 71	java/lang/StringBuilder
    //   57: dup
    //   58: invokespecial 74	java/lang/StringBuilder:<init>	()V
    //   61: iload 7
    //   63: invokevirtual 652	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   66: ldc -29
    //   68: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   71: invokevirtual 84	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   74: invokevirtual 656	java/net/HttpURLConnection:setRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   77: aload 11
    //   79: astore 10
    //   81: aload 11
    //   83: ldc_w 658
    //   86: ldc_w 660
    //   89: invokevirtual 656	java/net/HttpURLConnection:setRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   92: aload 11
    //   94: astore 10
    //   96: aload 11
    //   98: sipush 10000
    //   101: invokevirtual 663	java/net/HttpURLConnection:setConnectTimeout	(I)V
    //   104: aload 11
    //   106: astore 10
    //   108: aload 11
    //   110: iconst_1
    //   111: invokevirtual 667	java/net/HttpURLConnection:setDoOutput	(Z)V
    //   114: aconst_null
    //   115: astore_1
    //   116: new 669	java/io/OutputStreamWriter
    //   119: dup
    //   120: aload 11
    //   122: invokevirtual 673	java/net/HttpURLConnection:getOutputStream	()Ljava/io/OutputStream;
    //   125: invokespecial 676	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;)V
    //   128: astore 12
    //   130: aload 12
    //   132: aload_2
    //   133: invokevirtual 679	java/io/OutputStreamWriter:write	(Ljava/lang/String;)V
    //   136: aload 12
    //   138: ifnull +12 -> 150
    //   141: aload 11
    //   143: astore 10
    //   145: aload 12
    //   147: invokevirtual 682	java/io/OutputStreamWriter:close	()V
    //   150: aload 11
    //   152: astore 10
    //   154: aload 11
    //   156: invokevirtual 685	java/net/HttpURLConnection:getResponseCode	()I
    //   159: istore 7
    //   161: aload 11
    //   163: astore 10
    //   165: new 71	java/lang/StringBuilder
    //   168: dup
    //   169: invokespecial 74	java/lang/StringBuilder:<init>	()V
    //   172: ldc_w 687
    //   175: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   178: iload 7
    //   180: invokevirtual 652	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   183: invokevirtual 84	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   186: invokestatic 689	com/appsflyer/a:d	(Ljava/lang/String;)V
    //   189: aload 11
    //   191: astore 10
    //   193: aload_0
    //   194: aload 13
    //   196: getstatic 86	com/appsflyer/AppsFlyerLib:c	Ljava/lang/String;
    //   199: ldc_w 691
    //   202: iload 7
    //   204: invokestatic 693	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   207: invokespecial 607	com/appsflyer/AppsFlyerLib:a	(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   210: aload 11
    //   212: astore 10
    //   214: aload_0
    //   215: ldc_w 695
    //   218: iload 7
    //   220: invokestatic 693	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   223: aload 13
    //   225: invokespecial 598	com/appsflyer/AppsFlyerLib:a	(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    //   228: aload 11
    //   230: astore 10
    //   232: aload 13
    //   234: ldc -120
    //   236: iconst_0
    //   237: invokevirtual 142	android/content/Context:getSharedPreferences	(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    //   240: astore_1
    //   241: iload 7
    //   243: sipush 200
    //   246: if_icmpne +65 -> 311
    //   249: aload 5
    //   251: ifnull +17 -> 268
    //   254: aload 11
    //   256: astore 10
    //   258: invokestatic 700	com/appsflyer/a/a:a	()Lcom/appsflyer/a/a;
    //   261: aload 5
    //   263: aload 13
    //   265: invokevirtual 703	com/appsflyer/a/a:a	(Ljava/lang/String;Landroid/content/Context;)V
    //   268: aload 11
    //   270: astore 10
    //   272: aload 4
    //   274: invokevirtual 596	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
    //   277: ifnull +34 -> 311
    //   280: aload 5
    //   282: ifnonnull +29 -> 311
    //   285: aload 11
    //   287: astore 10
    //   289: aload_0
    //   290: aload 13
    //   292: ldc_w 705
    //   295: ldc_w 396
    //   298: invokespecial 221	com/appsflyer/AppsFlyerLib:a	(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    //   301: aload 11
    //   303: astore 10
    //   305: aload_0
    //   306: aload 13
    //   308: invokespecial 707	com/appsflyer/AppsFlyerLib:m	(Landroid/content/Context;)V
    //   311: aload 11
    //   313: astore 10
    //   315: aload_1
    //   316: ldc_w 709
    //   319: iconst_0
    //   320: invokeinterface 148 3 0
    //   325: istore 7
    //   327: aload 11
    //   329: astore 10
    //   331: aload_1
    //   332: ldc_w 711
    //   335: lconst_0
    //   336: invokeinterface 174 4 0
    //   341: lstore 8
    //   343: lload 8
    //   345: lconst_0
    //   346: lcmp
    //   347: ifeq +48 -> 395
    //   350: aload 11
    //   352: astore 10
    //   354: invokestatic 126	java/lang/System:currentTimeMillis	()J
    //   357: lload 8
    //   359: lsub
    //   360: ldc2_w 712
    //   363: lcmp
    //   364: ifle +31 -> 395
    //   367: aload 11
    //   369: astore 10
    //   371: aload_0
    //   372: aload 13
    //   374: ldc_w 715
    //   377: aconst_null
    //   378: invokespecial 221	com/appsflyer/AppsFlyerLib:a	(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    //   381: aload 11
    //   383: astore 10
    //   385: aload_0
    //   386: aload 13
    //   388: ldc_w 711
    //   391: lconst_0
    //   392: invokespecial 177	com/appsflyer/AppsFlyerLib:a	(Landroid/content/Context;Ljava/lang/String;J)V
    //   395: aload 11
    //   397: astore 10
    //   399: aload_1
    //   400: ldc_w 715
    //   403: aconst_null
    //   404: invokeinterface 199 3 0
    //   409: ifnonnull +111 -> 520
    //   412: aload_3
    //   413: ifnull +107 -> 520
    //   416: iload 6
    //   418: ifeq +102 -> 520
    //   421: aload 11
    //   423: astore 10
    //   425: getstatic 114	com/appsflyer/AppsFlyerLib:j	Lcom/appsflyer/c;
    //   428: ifnull +92 -> 520
    //   431: iload 7
    //   433: iconst_5
    //   434: if_icmpgt +86 -> 520
    //   437: aload 11
    //   439: astore 10
    //   441: invokestatic 427	java/util/concurrent/Executors:newSingleThreadScheduledExecutor	()Ljava/util/concurrent/ScheduledExecutorService;
    //   444: astore_1
    //   445: aload 11
    //   447: astore 10
    //   449: aload_1
    //   450: new 19	com/appsflyer/AppsFlyerLib$d
    //   453: dup
    //   454: aload_0
    //   455: aload 13
    //   457: invokevirtual 716	android/content/Context:getApplicationContext	()Landroid/content/Context;
    //   460: aload_3
    //   461: aload_1
    //   462: invokespecial 719	com/appsflyer/AppsFlyerLib$d:<init>	(Lcom/appsflyer/AppsFlyerLib;Landroid/content/Context;Ljava/lang/String;Ljava/util/concurrent/ScheduledExecutorService;)V
    //   465: ldc2_w 720
    //   468: getstatic 438	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   471: invokeinterface 444 5 0
    //   476: pop
    //   477: aload 11
    //   479: ifnull +8 -> 487
    //   482: aload 11
    //   484: invokevirtual 724	java/net/HttpURLConnection:disconnect	()V
    //   487: return
    //   488: astore_2
    //   489: aload_1
    //   490: ifnull +11 -> 501
    //   493: aload 11
    //   495: astore 10
    //   497: aload_1
    //   498: invokevirtual 682	java/io/OutputStreamWriter:close	()V
    //   501: aload 11
    //   503: astore 10
    //   505: aload_2
    //   506: athrow
    //   507: astore_1
    //   508: aload 10
    //   510: ifnull +8 -> 518
    //   513: aload 10
    //   515: invokevirtual 724	java/net/HttpURLConnection:disconnect	()V
    //   518: aload_1
    //   519: athrow
    //   520: aload_3
    //   521: ifnonnull +16 -> 537
    //   524: aload 11
    //   526: astore 10
    //   528: ldc_w 726
    //   531: invokestatic 309	com/appsflyer/a:c	(Ljava/lang/String;)V
    //   534: goto -57 -> 477
    //   537: iload 6
    //   539: ifeq -62 -> 477
    //   542: aload 11
    //   544: astore 10
    //   546: getstatic 114	com/appsflyer/AppsFlyerLib:j	Lcom/appsflyer/c;
    //   549: ifnull -72 -> 477
    //   552: aload 11
    //   554: astore 10
    //   556: aload_1
    //   557: ldc_w 715
    //   560: aconst_null
    //   561: invokeinterface 199 3 0
    //   566: ifnull -89 -> 477
    //   569: aload 11
    //   571: astore 10
    //   573: aload_0
    //   574: aload 13
    //   576: ldc_w 728
    //   579: iconst_0
    //   580: invokespecial 164	com/appsflyer/AppsFlyerLib:a	(Landroid/content/Context;Ljava/lang/String;Z)I
    //   583: istore 7
    //   585: iload 7
    //   587: iconst_1
    //   588: if_icmple -111 -> 477
    //   591: aload 11
    //   593: astore 10
    //   595: aload_0
    //   596: aload 13
    //   598: invokevirtual 731	com/appsflyer/AppsFlyerLib:a	(Landroid/content/Context;)Ljava/util/Map;
    //   601: astore_1
    //   602: aload_1
    //   603: ifnull -126 -> 477
    //   606: aload 11
    //   608: astore 10
    //   610: getstatic 114	com/appsflyer/AppsFlyerLib:j	Lcom/appsflyer/c;
    //   613: aload_1
    //   614: invokeinterface 736 2 0
    //   619: goto -142 -> 477
    //   622: astore_1
    //   623: goto -146 -> 477
    //   626: astore_2
    //   627: aload 12
    //   629: astore_1
    //   630: goto -141 -> 489
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	633	0	this	AppsFlyerLib
    //   0	633	1	paramURL	URL
    //   0	633	2	paramString1	String
    //   0	633	3	paramString2	String
    //   0	633	4	paramWeakReference	WeakReference<Context>
    //   0	633	5	paramString3	String
    //   0	633	6	paramBoolean	boolean
    //   43	546	7	i1	int
    //   341	17	8	l1	long
    //   11	598	10	localObject	Object
    //   20	587	11	localHttpURLConnection	java.net.HttpURLConnection
    //   128	500	12	localOutputStreamWriter	java.io.OutputStreamWriter
    //   8	589	13	localContext	Context
    // Exception table:
    //   from	to	target	type
    //   116	130	488	finally
    //   13	22	507	finally
    //   26	34	507	finally
    //   38	45	507	finally
    //   49	77	507	finally
    //   81	92	507	finally
    //   96	104	507	finally
    //   108	114	507	finally
    //   145	150	507	finally
    //   154	161	507	finally
    //   165	189	507	finally
    //   193	210	507	finally
    //   214	228	507	finally
    //   232	241	507	finally
    //   258	268	507	finally
    //   272	280	507	finally
    //   289	301	507	finally
    //   305	311	507	finally
    //   315	327	507	finally
    //   331	343	507	finally
    //   354	367	507	finally
    //   371	381	507	finally
    //   385	395	507	finally
    //   399	412	507	finally
    //   425	431	507	finally
    //   441	445	507	finally
    //   449	477	507	finally
    //   497	501	507	finally
    //   505	507	507	finally
    //   528	534	507	finally
    //   546	552	507	finally
    //   556	569	507	finally
    //   573	585	507	finally
    //   595	602	507	finally
    //   610	619	507	finally
    //   595	602	622	com/appsflyer/f
    //   610	619	622	com/appsflyer/f
    //   130	136	626	finally
  }
  
  private String b(Context paramContext, String paramString)
    throws PackageManager.NameNotFoundException
  {
    SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("appsflyer-data", 0);
    if (localSharedPreferences.contains("CACHED_CHANNEL")) {
      return localSharedPreferences.getString("CACHED_CHANNEL", null);
    }
    a(paramContext, "CACHED_CHANNEL", paramString);
    return paramString;
  }
  
  /* Error */
  private void b(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean)
  {
    // Byte code:
    //   0: invokestatic 324	com/appsflyer/e:a	()Lcom/appsflyer/e;
    //   3: aload_1
    //   4: invokevirtual 748	com/appsflyer/e:b	(Landroid/content/Context;)V
    //   7: new 71	java/lang/StringBuilder
    //   10: dup
    //   11: invokespecial 74	java/lang/StringBuilder:<init>	()V
    //   14: ldc_w 750
    //   17: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   20: aload_1
    //   21: invokevirtual 473	java/lang/Object:getClass	()Ljava/lang/Class;
    //   24: invokevirtual 753	java/lang/Class:getName	()Ljava/lang/String;
    //   27: invokevirtual 754	java/lang/String:toString	()Ljava/lang/String;
    //   30: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   33: invokevirtual 84	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   36: invokestatic 225	com/appsflyer/a:a	(Ljava/lang/String;)V
    //   39: aload_3
    //   40: ifnonnull +1967 -> 2007
    //   43: iconst_1
    //   44: istore 8
    //   46: new 756	java/util/HashMap
    //   49: dup
    //   50: invokespecial 757	java/util/HashMap:<init>	()V
    //   53: astore 14
    //   55: aload 14
    //   57: ldc_w 759
    //   60: new 211	java/util/Date
    //   63: dup
    //   64: invokespecial 212	java/util/Date:<init>	()V
    //   67: invokevirtual 762	java/util/Date:getTime	()J
    //   70: invokestatic 767	java/lang/Long:toString	(J)Ljava/lang/String;
    //   73: invokeinterface 258 3 0
    //   78: pop
    //   79: aload_0
    //   80: ldc_w 769
    //   83: ldc -29
    //   85: aload_1
    //   86: invokespecial 598	com/appsflyer/AppsFlyerLib:a	(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    //   89: new 71	java/lang/StringBuilder
    //   92: dup
    //   93: invokespecial 74	java/lang/StringBuilder:<init>	()V
    //   96: ldc_w 771
    //   99: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   102: astore 15
    //   104: iload 8
    //   106: ifeq +1907 -> 2013
    //   109: ldc_w 773
    //   112: astore 13
    //   114: aload 15
    //   116: aload 13
    //   118: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   121: invokevirtual 84	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   124: invokestatic 225	com/appsflyer/a:a	(Ljava/lang/String;)V
    //   127: iload 8
    //   129: ifeq +1890 -> 2019
    //   132: ldc_w 773
    //   135: astore 13
    //   137: aload_0
    //   138: ldc_w 775
    //   141: aload 13
    //   143: aload_1
    //   144: invokespecial 598	com/appsflyer/AppsFlyerLib:a	(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    //   147: getstatic 86	com/appsflyer/AppsFlyerLib:c	Ljava/lang/String;
    //   150: astore 15
    //   152: iload 8
    //   154: ifeq +1871 -> 2025
    //   157: ldc_w 773
    //   160: astore 13
    //   162: aload_0
    //   163: aload_1
    //   164: aload 15
    //   166: ldc_w 777
    //   169: aload 13
    //   171: invokespecial 607	com/appsflyer/AppsFlyerLib:a	(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   174: invokestatic 700	com/appsflyer/a/a:a	()Lcom/appsflyer/a/a;
    //   177: aload_1
    //   178: invokevirtual 779	com/appsflyer/a/a:a	(Landroid/content/Context;)V
    //   181: aload_1
    //   182: invokevirtual 276	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
    //   185: aload_1
    //   186: invokevirtual 279	android/content/Context:getPackageName	()Ljava/lang/String;
    //   189: sipush 4096
    //   192: invokevirtual 285	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    //   195: getfield 783	android/content/pm/PackageInfo:requestedPermissions	[Ljava/lang/String;
    //   198: invokestatic 110	java/util/Arrays:asList	([Ljava/lang/Object;)Ljava/util/List;
    //   201: astore 13
    //   203: aload 13
    //   205: ldc_w 785
    //   208: invokeinterface 789 2 0
    //   213: ifne +19 -> 232
    //   216: ldc_w 791
    //   219: invokestatic 309	com/appsflyer/a:c	(Ljava/lang/String;)V
    //   222: aload_0
    //   223: aload_1
    //   224: aconst_null
    //   225: ldc_w 793
    //   228: aconst_null
    //   229: invokespecial 607	com/appsflyer/AppsFlyerLib:a	(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   232: aload 13
    //   234: ldc_w 795
    //   237: invokeinterface 789 2 0
    //   242: ifne +9 -> 251
    //   245: ldc_w 797
    //   248: invokestatic 309	com/appsflyer/a:c	(Ljava/lang/String;)V
    //   251: aload 13
    //   253: ldc_w 799
    //   256: invokeinterface 789 2 0
    //   261: ifne +9 -> 270
    //   264: ldc_w 801
    //   267: invokestatic 309	com/appsflyer/a:c	(Ljava/lang/String;)V
    //   270: new 71	java/lang/StringBuilder
    //   273: dup
    //   274: invokespecial 74	java/lang/StringBuilder:<init>	()V
    //   277: astore 15
    //   279: iload 8
    //   281: ifeq +1491 -> 1772
    //   284: getstatic 94	com/appsflyer/AppsFlyerLib:d	Ljava/lang/String;
    //   287: astore 13
    //   289: aload 15
    //   291: aload 13
    //   293: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   296: aload_1
    //   297: invokevirtual 279	android/content/Context:getPackageName	()Ljava/lang/String;
    //   300: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   303: pop
    //   304: iload 6
    //   306: ifeq +17 -> 323
    //   309: aload 14
    //   311: ldc_w 803
    //   314: ldc_w 805
    //   317: invokeinterface 258 3 0
    //   322: pop
    //   323: aload 14
    //   325: ldc_w 807
    //   328: getstatic 812	android/os/Build:BRAND	Ljava/lang/String;
    //   331: invokeinterface 258 3 0
    //   336: pop
    //   337: aload 14
    //   339: ldc_w 814
    //   342: getstatic 817	android/os/Build:DEVICE	Ljava/lang/String;
    //   345: invokeinterface 258 3 0
    //   350: pop
    //   351: aload 14
    //   353: ldc_w 819
    //   356: getstatic 822	android/os/Build:PRODUCT	Ljava/lang/String;
    //   359: invokeinterface 258 3 0
    //   364: pop
    //   365: aload 14
    //   367: ldc_w 413
    //   370: getstatic 339	android/os/Build$VERSION:SDK_INT	I
    //   373: invokestatic 693	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   376: invokeinterface 258 3 0
    //   381: pop
    //   382: aload 14
    //   384: ldc_w 824
    //   387: getstatic 827	android/os/Build:MODEL	Ljava/lang/String;
    //   390: invokeinterface 258 3 0
    //   395: pop
    //   396: aload 14
    //   398: ldc_w 829
    //   401: getstatic 832	android/os/Build:TYPE	Ljava/lang/String;
    //   404: invokeinterface 258 3 0
    //   409: pop
    //   410: iload 8
    //   412: ifeq +1368 -> 1780
    //   415: aload_0
    //   416: aload_1
    //   417: invokespecial 202	com/appsflyer/AppsFlyerLib:i	(Landroid/content/Context;)Z
    //   420: ifeq +18 -> 438
    //   423: aload 14
    //   425: ldc_w 834
    //   428: aload_0
    //   429: invokespecial 836	com/appsflyer/AppsFlyerLib:j	()Ljava/lang/String;
    //   432: invokeinterface 258 3 0
    //   437: pop
    //   438: aload_0
    //   439: ldc_w 838
    //   442: invokevirtual 841	com/appsflyer/AppsFlyerLib:c	(Ljava/lang/String;)Ljava/lang/String;
    //   445: astore 13
    //   447: aload 13
    //   449: ifnull +16 -> 465
    //   452: aload 14
    //   454: ldc_w 843
    //   457: aload 13
    //   459: invokeinterface 258 3 0
    //   464: pop
    //   465: aload_1
    //   466: invokevirtual 276	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
    //   469: aload_1
    //   470: invokevirtual 279	android/content/Context:getPackageName	()Ljava/lang/String;
    //   473: invokevirtual 846	android/content/pm/PackageManager:getInstallerPackageName	(Ljava/lang/String;)Ljava/lang/String;
    //   476: astore 13
    //   478: aload 13
    //   480: ifnull +16 -> 496
    //   483: aload 14
    //   485: ldc_w 848
    //   488: aload 13
    //   490: invokeinterface 258 3 0
    //   495: pop
    //   496: invokestatic 324	com/appsflyer/e:a	()Lcom/appsflyer/e;
    //   499: ldc_w 850
    //   502: invokevirtual 852	com/appsflyer/e:a	(Ljava/lang/String;)Ljava/lang/String;
    //   505: astore 13
    //   507: aload 13
    //   509: ifnull +24 -> 533
    //   512: aload 13
    //   514: invokevirtual 262	java/lang/String:length	()I
    //   517: ifle +16 -> 533
    //   520: aload 14
    //   522: ldc_w 850
    //   525: aload 13
    //   527: invokeinterface 258 3 0
    //   532: pop
    //   533: aload_0
    //   534: aload_1
    //   535: invokespecial 855	com/appsflyer/AppsFlyerLib:n	(Landroid/content/Context;)Ljava/lang/String;
    //   538: astore 13
    //   540: aload_0
    //   541: aload_1
    //   542: aload 13
    //   544: invokespecial 191	com/appsflyer/AppsFlyerLib:b	(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    //   547: astore 16
    //   549: aload 16
    //   551: ifnull +16 -> 567
    //   554: aload 14
    //   556: ldc_w 857
    //   559: aload 16
    //   561: invokeinterface 258 3 0
    //   566: pop
    //   567: aload 16
    //   569: ifnull +1425 -> 1994
    //   572: aload 16
    //   574: aload 13
    //   576: invokevirtual 251	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   579: ifeq +6 -> 585
    //   582: goto +1412 -> 1994
    //   585: aload 14
    //   587: ldc_w 859
    //   590: aload 13
    //   592: invokeinterface 258 3 0
    //   597: pop
    //   598: aload_0
    //   599: aload_1
    //   600: invokespecial 861	com/appsflyer/AppsFlyerLib:j	(Landroid/content/Context;)Ljava/lang/String;
    //   603: astore 13
    //   605: aload 13
    //   607: ifnull +19 -> 626
    //   610: aload 14
    //   612: ldc_w 863
    //   615: aload 13
    //   617: invokevirtual 866	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   620: invokeinterface 258 3 0
    //   625: pop
    //   626: aload_0
    //   627: aload_1
    //   628: invokespecial 868	com/appsflyer/AppsFlyerLib:l	(Landroid/content/Context;)Ljava/lang/String;
    //   631: astore 13
    //   633: aload 13
    //   635: ifnull +19 -> 654
    //   638: aload 14
    //   640: ldc_w 870
    //   643: aload 13
    //   645: invokevirtual 866	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   648: invokeinterface 258 3 0
    //   653: pop
    //   654: aload_0
    //   655: aload_1
    //   656: invokespecial 872	com/appsflyer/AppsFlyerLib:k	(Landroid/content/Context;)Ljava/lang/String;
    //   659: astore 13
    //   661: aload 13
    //   663: ifnull +19 -> 682
    //   666: aload 14
    //   668: ldc_w 874
    //   671: aload 13
    //   673: invokevirtual 866	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   676: invokeinterface 258 3 0
    //   681: pop
    //   682: aload_2
    //   683: ifnull +13 -> 696
    //   686: aload_2
    //   687: astore 13
    //   689: aload_2
    //   690: invokevirtual 262	java/lang/String:length	()I
    //   693: ifne +12 -> 705
    //   696: aload_0
    //   697: ldc_w 876
    //   700: invokevirtual 841	com/appsflyer/AppsFlyerLib:c	(Ljava/lang/String;)Ljava/lang/String;
    //   703: astore 13
    //   705: aload 13
    //   707: ifnull +1096 -> 1803
    //   710: aload 13
    //   712: invokevirtual 262	java/lang/String:length	()I
    //   715: ifle +1088 -> 1803
    //   718: aload 14
    //   720: ldc_w 878
    //   723: aload 13
    //   725: invokeinterface 258 3 0
    //   730: pop
    //   731: aload 13
    //   733: invokevirtual 262	java/lang/String:length	()I
    //   736: bipush 8
    //   738: if_icmple +22 -> 760
    //   741: aload 14
    //   743: ldc_w 880
    //   746: aload 13
    //   748: iconst_0
    //   749: bipush 8
    //   751: invokevirtual 62	java/lang/String:substring	(II)Ljava/lang/String;
    //   754: invokeinterface 258 3 0
    //   759: pop
    //   760: aload_0
    //   761: invokevirtual 882	com/appsflyer/AppsFlyerLib:b	()Ljava/lang/String;
    //   764: astore_2
    //   765: aload_2
    //   766: ifnull +15 -> 781
    //   769: aload 14
    //   771: ldc_w 884
    //   774: aload_2
    //   775: invokeinterface 258 3 0
    //   780: pop
    //   781: invokestatic 324	com/appsflyer/e:a	()Lcom/appsflyer/e;
    //   784: ldc_w 886
    //   787: invokevirtual 852	com/appsflyer/e:a	(Ljava/lang/String;)Ljava/lang/String;
    //   790: astore_2
    //   791: aload_2
    //   792: ifnull +1036 -> 1828
    //   795: aload 14
    //   797: ldc_w 888
    //   800: aload_2
    //   801: invokeinterface 258 3 0
    //   806: pop
    //   807: aload_3
    //   808: ifnull +33 -> 841
    //   811: aload 14
    //   813: ldc_w 890
    //   816: aload_3
    //   817: invokeinterface 258 3 0
    //   822: pop
    //   823: aload 4
    //   825: ifnull +16 -> 841
    //   828: aload 14
    //   830: ldc_w 892
    //   833: aload 4
    //   835: invokeinterface 258 3 0
    //   840: pop
    //   841: aload_0
    //   842: ldc_w 894
    //   845: invokevirtual 841	com/appsflyer/AppsFlyerLib:c	(Ljava/lang/String;)Ljava/lang/String;
    //   848: ifnull +21 -> 869
    //   851: aload 14
    //   853: ldc_w 894
    //   856: aload_0
    //   857: ldc_w 894
    //   860: invokevirtual 841	com/appsflyer/AppsFlyerLib:c	(Ljava/lang/String;)Ljava/lang/String;
    //   863: invokeinterface 258 3 0
    //   868: pop
    //   869: aload_0
    //   870: ldc_w 896
    //   873: invokevirtual 841	com/appsflyer/AppsFlyerLib:c	(Ljava/lang/String;)Ljava/lang/String;
    //   876: astore_2
    //   877: aload_2
    //   878: ifnull +52 -> 930
    //   881: aload_2
    //   882: invokevirtual 262	java/lang/String:length	()I
    //   885: iconst_3
    //   886: if_icmpeq +32 -> 918
    //   889: new 71	java/lang/StringBuilder
    //   892: dup
    //   893: invokespecial 74	java/lang/StringBuilder:<init>	()V
    //   896: ldc_w 898
    //   899: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   902: aload_2
    //   903: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   906: ldc_w 900
    //   909: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   912: invokevirtual 84	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   915: invokestatic 309	com/appsflyer/a:c	(Ljava/lang/String;)V
    //   918: aload 14
    //   920: ldc_w 902
    //   923: aload_2
    //   924: invokeinterface 258 3 0
    //   929: pop
    //   930: aload_0
    //   931: ldc_w 904
    //   934: invokevirtual 841	com/appsflyer/AppsFlyerLib:c	(Ljava/lang/String;)Ljava/lang/String;
    //   937: astore_2
    //   938: aload_2
    //   939: ifnull +15 -> 954
    //   942: aload 14
    //   944: ldc_w 906
    //   947: aload_2
    //   948: invokeinterface 258 3 0
    //   953: pop
    //   954: aload 14
    //   956: ldc_w 908
    //   959: aload_0
    //   960: aload_1
    //   961: invokevirtual 910	com/appsflyer/AppsFlyerLib:b	(Landroid/content/Context;)Z
    //   964: invokestatic 915	java/lang/Boolean:toString	(Z)Ljava/lang/String;
    //   967: invokeinterface 258 3 0
    //   972: pop
    //   973: invokestatic 324	com/appsflyer/e:a	()Lcom/appsflyer/e;
    //   976: ldc_w 917
    //   979: iconst_1
    //   980: invokevirtual 373	com/appsflyer/e:b	(Ljava/lang/String;Z)Z
    //   983: ifeq +28 -> 1011
    //   986: aload_0
    //   987: aload_1
    //   988: invokevirtual 497	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   991: invokevirtual 920	com/appsflyer/AppsFlyerLib:a	(Landroid/content/ContentResolver;)Ljava/lang/String;
    //   994: astore_2
    //   995: aload_2
    //   996: ifnull +15 -> 1011
    //   999: aload 14
    //   1001: ldc_w 922
    //   1004: aload_2
    //   1005: invokeinterface 258 3 0
    //   1010: pop
    //   1011: aload_0
    //   1012: aload_1
    //   1013: aload 14
    //   1015: invokespecial 924	com/appsflyer/AppsFlyerLib:a	(Landroid/content/Context;Ljava/util/Map;)V
    //   1018: aload_1
    //   1019: invokestatic 928	com/appsflyer/k:a	(Landroid/content/Context;)Ljava/lang/String;
    //   1022: astore_2
    //   1023: aload_2
    //   1024: ifnull +15 -> 1039
    //   1027: aload 14
    //   1029: ldc_w 930
    //   1032: aload_2
    //   1033: invokeinterface 258 3 0
    //   1038: pop
    //   1039: aload 14
    //   1041: ldc_w 932
    //   1044: invokestatic 938	java/util/Locale:getDefault	()Ljava/util/Locale;
    //   1047: invokevirtual 941	java/util/Locale:getDisplayLanguage	()Ljava/lang/String;
    //   1050: invokeinterface 258 3 0
    //   1055: pop
    //   1056: aload 14
    //   1058: ldc_w 943
    //   1061: invokestatic 938	java/util/Locale:getDefault	()Ljava/util/Locale;
    //   1064: invokevirtual 946	java/util/Locale:getLanguage	()Ljava/lang/String;
    //   1067: invokeinterface 258 3 0
    //   1072: pop
    //   1073: aload 14
    //   1075: ldc_w 948
    //   1078: invokestatic 938	java/util/Locale:getDefault	()Ljava/util/Locale;
    //   1081: invokevirtual 951	java/util/Locale:getCountry	()Ljava/lang/String;
    //   1084: invokeinterface 258 3 0
    //   1089: pop
    //   1090: aload_1
    //   1091: ldc_w 461
    //   1094: invokevirtual 465	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
    //   1097: checkcast 467	android/telephony/TelephonyManager
    //   1100: astore_2
    //   1101: aload 14
    //   1103: ldc_w 953
    //   1106: aload_2
    //   1107: invokevirtual 956	android/telephony/TelephonyManager:getSimOperatorName	()Ljava/lang/String;
    //   1110: invokeinterface 258 3 0
    //   1115: pop
    //   1116: aload 14
    //   1118: ldc_w 958
    //   1121: aload_2
    //   1122: invokevirtual 961	android/telephony/TelephonyManager:getNetworkOperatorName	()Ljava/lang/String;
    //   1125: invokeinterface 258 3 0
    //   1130: pop
    //   1131: aload 14
    //   1133: ldc_w 963
    //   1136: aload_0
    //   1137: aload_1
    //   1138: invokespecial 965	com/appsflyer/AppsFlyerLib:o	(Landroid/content/Context;)Ljava/lang/String;
    //   1141: invokeinterface 258 3 0
    //   1146: pop
    //   1147: invokestatic 324	com/appsflyer/e:a	()Lcom/appsflyer/e;
    //   1150: ldc_w 967
    //   1153: iconst_1
    //   1154: invokevirtual 373	com/appsflyer/e:b	(Ljava/lang/String;Z)Z
    //   1157: ifeq +24 -> 1181
    //   1160: aload_0
    //   1161: invokevirtual 969	com/appsflyer/AppsFlyerLib:c	()Ljava/lang/String;
    //   1164: astore_2
    //   1165: aload_2
    //   1166: ifnull +15 -> 1181
    //   1169: aload 14
    //   1171: ldc_w 971
    //   1174: aload_2
    //   1175: invokeinterface 258 3 0
    //   1180: pop
    //   1181: aload_0
    //   1182: aload_1
    //   1183: aload 14
    //   1185: invokespecial 973	com/appsflyer/AppsFlyerLib:b	(Landroid/content/Context;Ljava/util/Map;)V
    //   1188: aload_0
    //   1189: aload_1
    //   1190: aload 14
    //   1192: invokespecial 975	com/appsflyer/AppsFlyerLib:c	(Landroid/content/Context;Ljava/util/Map;)V
    //   1195: new 214	java/text/SimpleDateFormat
    //   1198: dup
    //   1199: ldc_w 977
    //   1202: getstatic 981	java/util/Locale:US	Ljava/util/Locale;
    //   1205: invokespecial 984	java/text/SimpleDateFormat:<init>	(Ljava/lang/String;Ljava/util/Locale;)V
    //   1208: astore_2
    //   1209: getstatic 339	android/os/Build$VERSION:SDK_INT	I
    //   1212: istore 7
    //   1214: iload 7
    //   1216: bipush 9
    //   1218: if_icmplt +40 -> 1258
    //   1221: aload 14
    //   1223: ldc_w 986
    //   1226: aload_2
    //   1227: new 211	java/util/Date
    //   1230: dup
    //   1231: aload_1
    //   1232: invokevirtual 276	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
    //   1235: aload_1
    //   1236: invokevirtual 279	android/content/Context:getPackageName	()Ljava/lang/String;
    //   1239: iconst_0
    //   1240: invokevirtual 285	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    //   1243: getfield 290	android/content/pm/PackageInfo:firstInstallTime	J
    //   1246: invokespecial 297	java/util/Date:<init>	(J)V
    //   1249: invokevirtual 218	java/text/SimpleDateFormat:format	(Ljava/util/Date;)Ljava/lang/String;
    //   1252: invokeinterface 258 3 0
    //   1257: pop
    //   1258: aload_1
    //   1259: invokevirtual 276	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
    //   1262: aload_1
    //   1263: invokevirtual 279	android/content/Context:getPackageName	()Ljava/lang/String;
    //   1266: iconst_0
    //   1267: invokevirtual 285	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    //   1270: astore_3
    //   1271: aload_1
    //   1272: ldc -120
    //   1274: iconst_0
    //   1275: invokevirtual 142	android/content/Context:getSharedPreferences	(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    //   1278: ldc_w 988
    //   1281: iconst_0
    //   1282: invokeinterface 148 3 0
    //   1287: istore 7
    //   1289: aload_3
    //   1290: getfield 990	android/content/pm/PackageInfo:versionCode	I
    //   1293: iload 7
    //   1295: if_icmple +24 -> 1319
    //   1298: aload_0
    //   1299: aload_1
    //   1300: ldc_w 709
    //   1303: iconst_0
    //   1304: invokespecial 556	com/appsflyer/AppsFlyerLib:a	(Landroid/content/Context;Ljava/lang/String;I)V
    //   1307: aload_0
    //   1308: aload_1
    //   1309: ldc_w 988
    //   1312: aload_3
    //   1313: getfield 990	android/content/pm/PackageInfo:versionCode	I
    //   1316: invokespecial 556	com/appsflyer/AppsFlyerLib:a	(Landroid/content/Context;Ljava/lang/String;I)V
    //   1319: aload 14
    //   1321: ldc_w 992
    //   1324: aload_3
    //   1325: getfield 990	android/content/pm/PackageInfo:versionCode	I
    //   1328: invokestatic 693	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   1331: invokeinterface 258 3 0
    //   1336: pop
    //   1337: aload 14
    //   1339: ldc_w 994
    //   1342: aload_3
    //   1343: getfield 997	android/content/pm/PackageInfo:versionName	Ljava/lang/String;
    //   1346: invokeinterface 258 3 0
    //   1351: pop
    //   1352: getstatic 339	android/os/Build$VERSION:SDK_INT	I
    //   1355: bipush 9
    //   1357: if_icmplt +80 -> 1437
    //   1360: aload_3
    //   1361: getfield 290	android/content/pm/PackageInfo:firstInstallTime	J
    //   1364: lstore 9
    //   1366: aload_3
    //   1367: getfield 1000	android/content/pm/PackageInfo:lastUpdateTime	J
    //   1370: lstore 11
    //   1372: aload 14
    //   1374: ldc_w 1002
    //   1377: aload_2
    //   1378: new 211	java/util/Date
    //   1381: dup
    //   1382: lload 9
    //   1384: invokespecial 297	java/util/Date:<init>	(J)V
    //   1387: invokevirtual 218	java/text/SimpleDateFormat:format	(Ljava/util/Date;)Ljava/lang/String;
    //   1390: invokeinterface 258 3 0
    //   1395: pop
    //   1396: aload 14
    //   1398: ldc_w 1004
    //   1401: aload_2
    //   1402: new 211	java/util/Date
    //   1405: dup
    //   1406: lload 11
    //   1408: invokespecial 297	java/util/Date:<init>	(J)V
    //   1411: invokevirtual 218	java/text/SimpleDateFormat:format	(Ljava/util/Date;)Ljava/lang/String;
    //   1414: invokeinterface 258 3 0
    //   1419: pop
    //   1420: aload 14
    //   1422: ldc_w 1006
    //   1425: aload_0
    //   1426: aload_2
    //   1427: aload_1
    //   1428: invokespecial 1008	com/appsflyer/AppsFlyerLib:a	(Ljava/text/SimpleDateFormat;Landroid/content/Context;)Ljava/lang/String;
    //   1431: invokeinterface 258 3 0
    //   1436: pop
    //   1437: aload 5
    //   1439: invokevirtual 262	java/lang/String:length	()I
    //   1442: ifle +16 -> 1458
    //   1445: aload 14
    //   1447: ldc_w 1010
    //   1450: aload 5
    //   1452: invokeinterface 258 3 0
    //   1457: pop
    //   1458: aload_1
    //   1459: ldc -120
    //   1461: iconst_0
    //   1462: invokevirtual 142	android/content/Context:getSharedPreferences	(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    //   1465: ldc_w 715
    //   1468: aconst_null
    //   1469: invokeinterface 199 3 0
    //   1474: astore_2
    //   1475: aload_2
    //   1476: ifnull +22 -> 1498
    //   1479: aload_2
    //   1480: invokevirtual 262	java/lang/String:length	()I
    //   1483: ifle +15 -> 1498
    //   1486: aload 14
    //   1488: ldc_w 1012
    //   1491: aload_2
    //   1492: invokeinterface 258 3 0
    //   1497: pop
    //   1498: invokestatic 324	com/appsflyer/e:a	()Lcom/appsflyer/e;
    //   1501: ldc_w 1014
    //   1504: invokevirtual 852	com/appsflyer/e:a	(Ljava/lang/String;)Ljava/lang/String;
    //   1507: astore_2
    //   1508: aload_2
    //   1509: ifnull +15 -> 1524
    //   1512: aload 14
    //   1514: ldc_w 1016
    //   1517: aload_2
    //   1518: invokeinterface 258 3 0
    //   1523: pop
    //   1524: iload 8
    //   1526: ifeq +163 -> 1689
    //   1529: aload_1
    //   1530: instanceof 1018
    //   1533: ifeq +156 -> 1689
    //   1536: aload_1
    //   1537: checkcast 1018	android/app/Activity
    //   1540: invokevirtual 1022	android/app/Activity:getIntent	()Landroid/content/Intent;
    //   1543: astore_2
    //   1544: aload_2
    //   1545: ifnull +144 -> 1689
    //   1548: aload_2
    //   1549: invokevirtual 1025	android/content/Intent:getAction	()Ljava/lang/String;
    //   1552: ldc_w 1027
    //   1555: if_acmpne +134 -> 1689
    //   1558: aload_2
    //   1559: invokevirtual 1031	android/content/Intent:getData	()Landroid/net/Uri;
    //   1562: astore 4
    //   1564: aload 14
    //   1566: ldc_w 1033
    //   1569: aload 4
    //   1571: invokevirtual 1036	android/net/Uri:toString	()Ljava/lang/String;
    //   1574: invokeinterface 258 3 0
    //   1579: pop
    //   1580: aload 4
    //   1582: ldc_w 1033
    //   1585: invokevirtual 1039	android/net/Uri:getQueryParameter	(Ljava/lang/String;)Ljava/lang/String;
    //   1588: ifnull +342 -> 1930
    //   1591: aload_0
    //   1592: aload_1
    //   1593: aload 4
    //   1595: invokevirtual 1042	android/net/Uri:getQuery	()Ljava/lang/String;
    //   1598: invokevirtual 754	java/lang/String:toString	()Ljava/lang/String;
    //   1601: invokespecial 1044	com/appsflyer/AppsFlyerLib:a	(Landroid/content/Context;Ljava/lang/String;)Ljava/util/Map;
    //   1604: astore_3
    //   1605: aload 4
    //   1607: invokevirtual 1047	android/net/Uri:getPath	()Ljava/lang/String;
    //   1610: ifnull +18 -> 1628
    //   1613: aload_3
    //   1614: ldc_w 1049
    //   1617: aload 4
    //   1619: invokevirtual 1047	android/net/Uri:getPath	()Ljava/lang/String;
    //   1622: invokeinterface 258 3 0
    //   1627: pop
    //   1628: aload_3
    //   1629: astore_2
    //   1630: aload 4
    //   1632: invokevirtual 1052	android/net/Uri:getScheme	()Ljava/lang/String;
    //   1635: ifnull +20 -> 1655
    //   1638: aload_3
    //   1639: ldc_w 1054
    //   1642: aload 4
    //   1644: invokevirtual 1052	android/net/Uri:getScheme	()Ljava/lang/String;
    //   1647: invokeinterface 258 3 0
    //   1652: pop
    //   1653: aload_3
    //   1654: astore_2
    //   1655: aload_0
    //   1656: aload_1
    //   1657: ldc_w 1056
    //   1660: new 520	org/json/JSONObject
    //   1663: dup
    //   1664: aload_2
    //   1665: invokespecial 1058	org/json/JSONObject:<init>	(Ljava/util/Map;)V
    //   1668: invokevirtual 534	org/json/JSONObject:toString	()Ljava/lang/String;
    //   1671: invokespecial 221	com/appsflyer/AppsFlyerLib:a	(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    //   1674: getstatic 114	com/appsflyer/AppsFlyerLib:j	Lcom/appsflyer/c;
    //   1677: ifnull +12 -> 1689
    //   1680: getstatic 114	com/appsflyer/AppsFlyerLib:j	Lcom/appsflyer/c;
    //   1683: aload_2
    //   1684: invokeinterface 1060 2 0
    //   1689: aload_0
    //   1690: invokespecial 1062	com/appsflyer/AppsFlyerLib:i	()Z
    //   1693: ifeq +47 -> 1740
    //   1696: ldc_w 1064
    //   1699: aload 5
    //   1701: invokevirtual 251	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1704: ifeq +36 -> 1740
    //   1707: aload 14
    //   1709: ldc_w 1066
    //   1712: ldc_w 396
    //   1715: invokeinterface 258 3 0
    //   1720: pop
    //   1721: aload_1
    //   1722: aload 14
    //   1724: checkcast 756	java/util/HashMap
    //   1727: invokestatic 1068	com/appsflyer/AppsFlyerLib:a	(Landroid/content/Context;Ljava/util/HashMap;)V
    //   1730: ldc_w 1070
    //   1733: invokestatic 225	com/appsflyer/a:a	(Ljava/lang/String;)V
    //   1736: aload_0
    //   1737: invokespecial 1072	com/appsflyer/AppsFlyerLib:h	()V
    //   1740: ldc_w 1074
    //   1743: invokestatic 225	com/appsflyer/a:a	(Ljava/lang/String;)V
    //   1746: new 22	com/appsflyer/AppsFlyerLib$e
    //   1749: dup
    //   1750: aload_0
    //   1751: aload 15
    //   1753: invokevirtual 84	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1756: aload 14
    //   1758: aload_1
    //   1759: invokevirtual 716	android/content/Context:getApplicationContext	()Landroid/content/Context;
    //   1762: iload 8
    //   1764: aconst_null
    //   1765: invokespecial 1077	com/appsflyer/AppsFlyerLib$e:<init>	(Lcom/appsflyer/AppsFlyerLib;Ljava/lang/String;Ljava/util/Map;Landroid/content/Context;ZLcom/appsflyer/AppsFlyerLib$1;)V
    //   1768: invokevirtual 1080	com/appsflyer/AppsFlyerLib$e:run	()V
    //   1771: return
    //   1772: getstatic 98	com/appsflyer/AppsFlyerLib:e	Ljava/lang/String;
    //   1775: astore 13
    //   1777: goto -1488 -> 289
    //   1780: aload_0
    //   1781: aload_1
    //   1782: aload 14
    //   1784: aload_3
    //   1785: aload 4
    //   1787: invokespecial 1082	com/appsflyer/AppsFlyerLib:a	(Landroid/content/Context;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    //   1790: goto -1352 -> 438
    //   1793: astore_1
    //   1794: aload_1
    //   1795: invokevirtual 1083	java/lang/Throwable:getLocalizedMessage	()Ljava/lang/String;
    //   1798: aload_1
    //   1799: invokestatic 539	com/appsflyer/a:a	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   1802: return
    //   1803: ldc_w 1085
    //   1806: invokestatic 225	com/appsflyer/a:a	(Ljava/lang/String;)V
    //   1809: aload_0
    //   1810: aload_1
    //   1811: getstatic 86	com/appsflyer/AppsFlyerLib:c	Ljava/lang/String;
    //   1814: ldc_w 1087
    //   1817: aconst_null
    //   1818: invokespecial 607	com/appsflyer/AppsFlyerLib:a	(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   1821: ldc_w 1089
    //   1824: invokestatic 225	com/appsflyer/a:a	(Ljava/lang/String;)V
    //   1827: return
    //   1828: aload_0
    //   1829: ldc_w 1091
    //   1832: invokevirtual 841	com/appsflyer/AppsFlyerLib:c	(Ljava/lang/String;)Ljava/lang/String;
    //   1835: astore_2
    //   1836: aload_2
    //   1837: ifnull -1030 -> 807
    //   1840: aload 14
    //   1842: ldc_w 1093
    //   1845: aload_2
    //   1846: invokestatic 1096	com/appsflyer/j:a	(Ljava/lang/String;)Ljava/lang/String;
    //   1849: invokeinterface 258 3 0
    //   1854: pop
    //   1855: goto -1048 -> 807
    //   1858: astore_2
    //   1859: new 71	java/lang/StringBuilder
    //   1862: dup
    //   1863: invokespecial 74	java/lang/StringBuilder:<init>	()V
    //   1866: ldc_w 1098
    //   1869: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1872: ldc_w 1098
    //   1875: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1878: ldc_w 1100
    //   1881: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1884: aload_2
    //   1885: invokevirtual 1103	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   1888: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1891: invokevirtual 84	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1894: invokestatic 225	com/appsflyer/a:a	(Ljava/lang/String;)V
    //   1897: goto -858 -> 1039
    //   1900: astore_2
    //   1901: new 71	java/lang/StringBuilder
    //   1904: dup
    //   1905: invokespecial 74	java/lang/StringBuilder:<init>	()V
    //   1908: ldc_w 1105
    //   1911: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1914: aload_2
    //   1915: invokevirtual 1106	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   1918: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1921: invokevirtual 84	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1924: invokestatic 225	com/appsflyer/a:a	(Ljava/lang/String;)V
    //   1927: goto -780 -> 1147
    //   1930: new 756	java/util/HashMap
    //   1933: dup
    //   1934: invokespecial 757	java/util/HashMap:<init>	()V
    //   1937: astore_2
    //   1938: aload_2
    //   1939: ldc_w 1108
    //   1942: aload 4
    //   1944: invokevirtual 1036	android/net/Uri:toString	()Ljava/lang/String;
    //   1947: invokeinterface 258 3 0
    //   1952: pop
    //   1953: goto -298 -> 1655
    //   1956: astore_2
    //   1957: goto -520 -> 1437
    //   1960: astore_2
    //   1961: goto -524 -> 1437
    //   1964: astore_3
    //   1965: goto -707 -> 1258
    //   1968: astore_2
    //   1969: goto -838 -> 1131
    //   1972: astore_2
    //   1973: goto -883 -> 1090
    //   1976: astore_2
    //   1977: goto -904 -> 1073
    //   1980: astore_2
    //   1981: goto -925 -> 1056
    //   1984: astore 13
    //   1986: goto -1490 -> 496
    //   1989: astore 13
    //   1991: goto -1721 -> 270
    //   1994: aload 16
    //   1996: ifnonnull -1398 -> 598
    //   1999: aload 13
    //   2001: ifnull -1403 -> 598
    //   2004: goto -1419 -> 585
    //   2007: iconst_0
    //   2008: istore 8
    //   2010: goto -1964 -> 46
    //   2013: aload_3
    //   2014: astore 13
    //   2016: goto -1902 -> 114
    //   2019: aload_3
    //   2020: astore 13
    //   2022: goto -1885 -> 137
    //   2025: aload_3
    //   2026: astore 13
    //   2028: goto -1866 -> 162
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	2031	0	this	AppsFlyerLib
    //   0	2031	1	paramContext	Context
    //   0	2031	2	paramString1	String
    //   0	2031	3	paramString2	String
    //   0	2031	4	paramString3	String
    //   0	2031	5	paramString4	String
    //   0	2031	6	paramBoolean	boolean
    //   1212	84	7	i1	int
    //   44	1965	8	bool	boolean
    //   1364	19	9	l1	long
    //   1370	37	11	l2	long
    //   112	1664	13	localObject1	Object
    //   1984	1	13	localException1	Exception
    //   1989	11	13	localException2	Exception
    //   2014	13	13	str1	String
    //   53	1788	14	localHashMap	HashMap
    //   102	1650	15	localObject2	Object
    //   547	1448	16	str2	String
    // Exception table:
    //   from	to	target	type
    //   79	104	1793	java/lang/Throwable
    //   114	127	1793	java/lang/Throwable
    //   137	152	1793	java/lang/Throwable
    //   162	181	1793	java/lang/Throwable
    //   181	232	1793	java/lang/Throwable
    //   232	251	1793	java/lang/Throwable
    //   251	270	1793	java/lang/Throwable
    //   270	279	1793	java/lang/Throwable
    //   284	289	1793	java/lang/Throwable
    //   289	304	1793	java/lang/Throwable
    //   309	323	1793	java/lang/Throwable
    //   323	410	1793	java/lang/Throwable
    //   415	438	1793	java/lang/Throwable
    //   438	447	1793	java/lang/Throwable
    //   452	465	1793	java/lang/Throwable
    //   465	478	1793	java/lang/Throwable
    //   483	496	1793	java/lang/Throwable
    //   496	507	1793	java/lang/Throwable
    //   512	533	1793	java/lang/Throwable
    //   533	549	1793	java/lang/Throwable
    //   554	567	1793	java/lang/Throwable
    //   572	582	1793	java/lang/Throwable
    //   585	598	1793	java/lang/Throwable
    //   598	605	1793	java/lang/Throwable
    //   610	626	1793	java/lang/Throwable
    //   626	633	1793	java/lang/Throwable
    //   638	654	1793	java/lang/Throwable
    //   654	661	1793	java/lang/Throwable
    //   666	682	1793	java/lang/Throwable
    //   689	696	1793	java/lang/Throwable
    //   696	705	1793	java/lang/Throwable
    //   710	760	1793	java/lang/Throwable
    //   760	765	1793	java/lang/Throwable
    //   769	781	1793	java/lang/Throwable
    //   781	791	1793	java/lang/Throwable
    //   795	807	1793	java/lang/Throwable
    //   811	823	1793	java/lang/Throwable
    //   828	841	1793	java/lang/Throwable
    //   841	869	1793	java/lang/Throwable
    //   869	877	1793	java/lang/Throwable
    //   881	918	1793	java/lang/Throwable
    //   918	930	1793	java/lang/Throwable
    //   930	938	1793	java/lang/Throwable
    //   942	954	1793	java/lang/Throwable
    //   954	995	1793	java/lang/Throwable
    //   999	1011	1793	java/lang/Throwable
    //   1011	1018	1793	java/lang/Throwable
    //   1018	1023	1793	java/lang/Throwable
    //   1027	1039	1793	java/lang/Throwable
    //   1039	1056	1793	java/lang/Throwable
    //   1056	1073	1793	java/lang/Throwable
    //   1073	1090	1793	java/lang/Throwable
    //   1090	1131	1793	java/lang/Throwable
    //   1147	1165	1793	java/lang/Throwable
    //   1169	1181	1793	java/lang/Throwable
    //   1181	1214	1793	java/lang/Throwable
    //   1221	1258	1793	java/lang/Throwable
    //   1258	1319	1793	java/lang/Throwable
    //   1319	1437	1793	java/lang/Throwable
    //   1437	1458	1793	java/lang/Throwable
    //   1458	1475	1793	java/lang/Throwable
    //   1479	1498	1793	java/lang/Throwable
    //   1498	1508	1793	java/lang/Throwable
    //   1512	1524	1793	java/lang/Throwable
    //   1529	1544	1793	java/lang/Throwable
    //   1548	1628	1793	java/lang/Throwable
    //   1630	1653	1793	java/lang/Throwable
    //   1655	1689	1793	java/lang/Throwable
    //   1689	1740	1793	java/lang/Throwable
    //   1740	1771	1793	java/lang/Throwable
    //   1772	1777	1793	java/lang/Throwable
    //   1780	1790	1793	java/lang/Throwable
    //   1803	1827	1793	java/lang/Throwable
    //   1828	1836	1793	java/lang/Throwable
    //   1840	1855	1793	java/lang/Throwable
    //   1859	1897	1793	java/lang/Throwable
    //   1901	1927	1793	java/lang/Throwable
    //   1930	1953	1793	java/lang/Throwable
    //   1018	1023	1858	java/lang/Exception
    //   1027	1039	1858	java/lang/Exception
    //   1131	1147	1900	java/lang/Throwable
    //   1258	1319	1956	java/lang/NoSuchFieldError
    //   1319	1437	1956	java/lang/NoSuchFieldError
    //   1258	1319	1960	android/content/pm/PackageManager$NameNotFoundException
    //   1319	1437	1960	android/content/pm/PackageManager$NameNotFoundException
    //   1221	1258	1964	java/lang/Exception
    //   1090	1131	1968	java/lang/Exception
    //   1073	1090	1972	java/lang/Exception
    //   1056	1073	1976	java/lang/Exception
    //   1039	1056	1980	java/lang/Exception
    //   465	478	1984	java/lang/Exception
    //   483	496	1984	java/lang/Exception
    //   181	232	1989	java/lang/Exception
    //   232	251	1989	java/lang/Exception
    //   251	270	1989	java/lang/Exception
  }
  
  private void b(Context paramContext, Map<String, String> paramMap)
  {
    boolean bool4 = true;
    Object localObject8 = null;
    Object localObject1 = null;
    Object localObject6 = null;
    Object localObject3 = null;
    boolean bool5 = false;
    boolean bool6 = false;
    boolean bool1 = false;
    Object localObject10 = null;
    Object localObject5 = localObject8;
    Object localObject7 = localObject6;
    boolean bool2 = bool5;
    boolean bool3 = bool6;
    for (;;)
    {
      try
      {
        Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient");
        localObject5 = localObject8;
        localObject7 = localObject6;
        bool2 = bool5;
        bool3 = bool6;
        Object localObject9 = AdvertisingIdClient.b(paramContext);
        if (localObject9 == null) {
          continue;
        }
        localObject5 = localObject8;
        localObject7 = localObject6;
        bool2 = bool5;
        bool3 = bool6;
        localObject8 = ((AdvertisingIdClient.Info)localObject9).a();
        localObject5 = localObject8;
        localObject7 = localObject6;
        bool2 = bool5;
        bool3 = bool6;
        if (((AdvertisingIdClient.Info)localObject9).b()) {
          continue;
        }
        bool1 = true;
        localObject5 = localObject8;
        localObject7 = localObject6;
        bool2 = bool5;
        bool3 = bool6;
        localObject9 = Boolean.toString(bool1);
        bool2 = true;
        bool3 = true;
        bool5 = true;
        if (localObject8 != null)
        {
          localObject1 = localObject8;
          localObject3 = localObject9;
          bool1 = bool5;
          localObject6 = localObject10;
          localObject5 = localObject8;
          localObject7 = localObject9;
          if (((String)localObject8).length() != 0) {}
        }
        else
        {
          localObject6 = "emptyOrNull";
          bool1 = bool5;
          localObject3 = localObject9;
          localObject1 = localObject8;
        }
      }
      catch (ClassNotFoundException localClassNotFoundException)
      {
        a.a("WARNING: Google Play services SDK jar is missing.");
        localObject2 = localObject5;
        localObject3 = localObject7;
        bool1 = bool2;
        localObject6 = localObject10;
        if (!e.a().b("enableGpsFallback", true)) {
          continue;
        }
        localObject2 = localObject5;
        try
        {
          localObject8 = b.a(paramContext);
          localObject2 = localObject5;
          localObject3 = localObject7;
          bool1 = bool2;
          localObject6 = localObject10;
          if (localObject8 == null) {
            continue;
          }
          localObject2 = localObject5;
          localObject3 = ((b.a)localObject8).a();
          localObject2 = localObject3;
          if (((b.a)localObject8).b()) {
            continue;
          }
          bool1 = bool4;
          localObject2 = localObject3;
          localObject5 = Boolean.toString(bool1);
          paramContext = (Context)localObject5;
          localObject2 = localObject3;
          localObject3 = paramContext;
          bool1 = bool2;
          localObject6 = localObject10;
          continue;
          bool1 = false;
          continue;
        }
        catch (Exception localException1)
        {
          a("GAID", "\tgot error: " + localException1.getMessage(), paramContext);
          localObject4 = localObject7;
          bool1 = bool2;
          localObject6 = localObject10;
        }
      }
      catch (Exception localException2)
      {
        localObject6 = localException2.getClass().getSimpleName();
        Object localObject2 = e.a().a("advertiserId");
        Object localObject4 = e.a().a("advertiserIdEnabled");
        if (localException2.getLocalizedMessage() == null) {
          break label553;
        }
      }
      if (localObject6 != null) {
        paramMap.put("gaidError", localObject6);
      }
      if ((localObject1 != null) && (localObject3 != null))
      {
        paramMap.put("advertiserId", localObject1);
        paramMap.put("advertiserIdEnabled", localObject3);
        e.a().a("advertiserId", (String)localObject1);
        e.a().a("advertiserIdEnabled", (String)localObject3);
        paramMap.put("isGaidWithGps", String.valueOf(bool1));
      }
      return;
      bool1 = false;
      continue;
      localObject6 = "gpsAdInfo-null";
    }
    a.a(localException2.getLocalizedMessage());
    for (;;)
    {
      a("Could not fetch advertiser id: ", localException2.getLocalizedMessage(), paramContext);
      bool1 = bool3;
      break;
      label553:
      a.a(localException2.toString());
    }
  }
  
  private void c(Context paramContext, Map<String, String> paramMap)
  {
    try
    {
      Class.forName("com.unity3d.player.UnityPlayer");
      paramMap.put("platformextension", "android_unity");
      return;
    }
    catch (ClassNotFoundException paramContext)
    {
      paramMap.put("platformextension", "android_native");
      return;
    }
    catch (Exception paramContext) {}
  }
  
  private void d(final Context paramContext)
  {
    final String str = e.a().a("GCM_PROJECT_ID");
    if ((str != null) && (e.a().a("GCM_TOKEN") == null)) {
      new Thread(new Runnable()
      {
        public void run()
        {
          try
          {
            Object localObject = InstanceID.b(paramContext);
            String str = ((InstanceID)localObject).a(str, "GCM", null);
            a.a("token=" + str);
            e.a().a("GCM_TOKEN", str);
            localObject = ((InstanceID)localObject).b();
            a.a("instance id=" + (String)localObject);
            e.a().a("GCM_INSTANCE_ID", (String)localObject);
            AppsFlyerLib.b(AppsFlyerLib.this, paramContext);
            return;
          }
          catch (Exception localException)
          {
            a.a("Could not load registration ID", localException);
            return;
          }
          catch (Error localError)
          {
            a.a("Caught Exception", localError);
          }
        }
      }).start();
    }
  }
  
  private void e(Context paramContext)
  {
    a.a("app went to background");
    e.a().b(paramContext);
    long l1 = System.currentTimeMillis();
    long l2 = o;
    HashMap localHashMap = new HashMap();
    String str = c("AppsFlyerKey");
    localHashMap.put("app_id", paramContext.getPackageName());
    localHashMap.put("devkey", str);
    localHashMap.put("uid", c(paramContext));
    localHashMap.put("time_in_app", String.valueOf((l1 - l2) / 1000L));
    localHashMap.put("statType", "user_closed_app");
    localHashMap.put("platform", "Android");
    localHashMap.put("launch_counter", Integer.toString(a(paramContext, "appsFlyerCount", false)));
    localHashMap.put("gcd_conversion_data_timing", Long.toString(paramContext.getSharedPreferences("appsflyer-data", 0).getLong("appsflyerGetConversionDataTiming", 0L)));
    if (e.a().b("collectFingerPrint", true))
    {
      str = c();
      if (str != null) {
        localHashMap.put("deviceFingerPrintId", str);
      }
    }
    try
    {
      paramContext = new g(paramContext);
      paramContext.a = localHashMap;
      paramContext.execute(new String[] { "https://stats.appsflyer.com/stats" });
      return;
    }
    catch (Throwable paramContext) {}
  }
  
  private boolean e(String paramString)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (paramString.length() > 12)
    {
      bool1 = bool2;
      if ("com.appsflyer".equals(paramString.toLowerCase().substring(0, 13))) {
        bool1 = true;
      }
    }
    return bool1;
  }
  
  private Map<String, String> f(String paramString)
  {
    HashMap localHashMap = new HashMap();
    try
    {
      JSONObject localJSONObject = new JSONObject(paramString);
      Iterator localIterator = localJSONObject.keys();
      for (;;)
      {
        paramString = localHashMap;
        if (!localIterator.hasNext()) {
          break;
        }
        paramString = (String)localIterator.next();
        if (!g.contains(paramString)) {
          localHashMap.put(paramString, localJSONObject.getString(paramString));
        }
      }
      return paramString;
    }
    catch (JSONException paramString)
    {
      a.c(paramString.getMessage());
      paramString = null;
    }
  }
  
  private void f(Context paramContext)
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("devkey", c("AppsFlyerKey"));
    localHashMap.put("uid", c(paramContext));
    localHashMap.put("af_gcm_token", e.a().a("GCM_TOKEN"));
    localHashMap.put("advertiserId", e.a().a("advertiserId"));
    localHashMap.put("af_google_instance_id", e.a().a("GCM_INSTANCE_ID"));
    localHashMap.put("launch_counter", Integer.toString(a(paramContext, "appsFlyerCount", false)));
    localHashMap.put("sdk", Integer.toString(Build.VERSION.SDK_INT));
    try
    {
      long l1 = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 0).firstInstallTime;
      localHashMap.put("install_date", new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date(l1)));
      Object localObject;
      if (e.a().b("collectFingerPrint", true))
      {
        localObject = c();
        if (localObject != null) {
          localHashMap.put("deviceFingerPrintId", localObject);
        }
      }
      try
      {
        localObject = new g(paramContext);
        ((g)localObject).a = localHashMap;
        ((g)localObject).execute(new String[] { f + paramContext.getPackageName() });
        return;
      }
      catch (Throwable paramContext) {}
    }
    catch (NoSuchFieldError localNoSuchFieldError)
    {
      for (;;) {}
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      for (;;) {}
    }
  }
  
  private int g(String paramString)
  {
    try
    {
      Class.forName(paramString);
      return 1;
    }
    catch (ClassNotFoundException paramString) {}
    return 0;
  }
  
  private void g()
  {
    a.a("Test mode started..");
    this.r = System.currentTimeMillis();
  }
  
  private boolean g(Context paramContext)
  {
    boolean bool = false;
    try
    {
      int i1 = GoogleApiAvailability.a().a(paramContext);
      if (i1 == 0) {
        bool = true;
      }
      return bool;
    }
    catch (Throwable paramContext)
    {
      a.a("WARNING: Google play services is unavailable.");
    }
    return false;
  }
  
  private void h()
  {
    a.a("Test mode ended!");
    this.r = 0L;
  }
  
  private boolean h(Context paramContext)
  {
    return (Build.VERSION.SDK_INT < 19) || (!g(paramContext));
  }
  
  private boolean i()
  {
    return System.currentTimeMillis() - this.r <= 30000L;
  }
  
  private boolean i(Context paramContext)
  {
    boolean bool = false;
    if (!paramContext.getSharedPreferences("appsflyer-data", 0).contains("appsFlyerCount")) {
      bool = true;
    }
    return bool;
  }
  
  private String j()
  {
    return g("com.tune.Tune") + g("com.adjust.sdk.Adjust") + g("com.kochava.android.tracker.Feature") + g("io.branch.referral.Branch") + g("com.apsalar.sdk.Apsalar") + g("com.localytics.android.Localytics") + g("com.tenjin.android.TenjinSDK") + g("com.talkingdata.sdk.TalkingDataSDK") + g("it.partytrack.sdk.Track") + g("jp.appAdForce.android.LtvManager");
  }
  
  private String j(Context paramContext)
  {
    String str = null;
    SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("appsflyer-data", 0);
    if (localSharedPreferences.contains("INSTALL_STORE")) {
      return localSharedPreferences.getString("INSTALL_STORE", null);
    }
    if (i(paramContext)) {
      str = k(paramContext);
    }
    a(paramContext, "INSTALL_STORE", str);
    return str;
  }
  
  private String k(Context paramContext)
  {
    try
    {
      paramContext = paramContext.getPackageManager().getApplicationInfo(paramContext.getPackageName(), 128).metaData;
      if (paramContext != null)
      {
        paramContext = paramContext.get("AF_STORE");
        if (paramContext != null)
        {
          if ((paramContext instanceof String)) {
            return (String)paramContext;
          }
          paramContext = paramContext.toString();
          return paramContext;
        }
      }
    }
    catch (Exception paramContext)
    {
      a.a("Could not find AF_STORE value in the manifest", paramContext);
    }
    return null;
  }
  
  private String l(Context paramContext)
  {
    Object localObject1 = paramContext.getSharedPreferences("appsflyer-data", 0);
    if (((SharedPreferences)localObject1).contains("preInstallName")) {
      return ((SharedPreferences)localObject1).getString("preInstallName", null);
    }
    boolean bool = i(paramContext);
    localObject3 = null;
    localObject1 = localObject3;
    if (bool) {}
    for (;;)
    {
      try
      {
        localObject4 = paramContext.getPackageManager().getApplicationInfo(paramContext.getPackageName(), 128).metaData;
        localObject1 = localObject3;
        if (localObject4 != null)
        {
          localObject4 = ((Bundle)localObject4).get("AF_PRE_INSTALL_NAME");
          localObject1 = localObject3;
          if (localObject4 != null)
          {
            if (!(localObject4 instanceof String)) {
              continue;
            }
            localObject1 = (String)localObject4;
          }
        }
      }
      catch (Exception localException)
      {
        Object localObject4;
        a.a("Could not find AF_PRE_INSTALL_NAME value in the manifest", localException);
        Object localObject2 = localObject3;
        continue;
      }
      a(paramContext, "preInstallName", (String)localObject1);
      return (String)localObject1;
      localObject1 = localObject4.toString();
    }
  }
  
  private void m(Context paramContext)
  {
    if ((l) || (System.currentTimeMillis() - m < 15000L)) {}
    while (n != null) {
      return;
    }
    n = Executors.newSingleThreadScheduledExecutor();
    n.schedule(new b(paramContext), 1L, TimeUnit.SECONDS);
  }
  
  private String n(Context paramContext)
  {
    String str = e.a().a("channel");
    if (str == null) {
      try
      {
        paramContext = paramContext.getPackageManager().getApplicationInfo(paramContext.getPackageName(), 128).metaData;
        if (paramContext != null)
        {
          paramContext = paramContext.get("CHANNEL");
          if (paramContext != null)
          {
            if ((paramContext instanceof String)) {
              return (String)paramContext;
            }
            paramContext = paramContext.toString();
            return paramContext;
          }
        }
      }
      catch (Exception paramContext)
      {
        a.a("Could not load CHANNEL value", paramContext);
      }
    }
    return str;
  }
  
  private String o(Context paramContext)
  {
    paramContext = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
    if (paramContext != null)
    {
      if (paramContext.getType() == 1) {
        return "WIFI";
      }
      if (paramContext.getType() == 0) {
        return "MOBILE";
      }
    }
    return "unknown";
  }
  
  /* Error */
  public String a(android.content.ContentResolver paramContentResolver)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 1360
    //   4: invokestatic 1364	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   7: iconst_1
    //   8: anewarray 54	java/lang/String
    //   11: dup
    //   12: iconst_0
    //   13: ldc_w 1366
    //   16: aastore
    //   17: aconst_null
    //   18: aconst_null
    //   19: aconst_null
    //   20: invokevirtual 1372	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   23: astore 4
    //   25: aconst_null
    //   26: astore_1
    //   27: aload 4
    //   29: ifnull +15 -> 44
    //   32: aload 4
    //   34: invokeinterface 1377 1 0
    //   39: istore_2
    //   40: iload_2
    //   41: ifne +17 -> 58
    //   44: aload 4
    //   46: ifnull +10 -> 56
    //   49: aload 4
    //   51: invokeinterface 1378 1 0
    //   56: aconst_null
    //   57: areturn
    //   58: aload 4
    //   60: aload 4
    //   62: ldc_w 1366
    //   65: invokeinterface 1381 2 0
    //   70: invokeinterface 1383 2 0
    //   75: astore_3
    //   76: aload_3
    //   77: astore_1
    //   78: aload_1
    //   79: astore_3
    //   80: aload 4
    //   82: ifnull +12 -> 94
    //   85: aload 4
    //   87: invokeinterface 1378 1 0
    //   92: aload_1
    //   93: astore_3
    //   94: aload_3
    //   95: areturn
    //   96: astore_3
    //   97: new 71	java/lang/StringBuilder
    //   100: dup
    //   101: invokespecial 74	java/lang/StringBuilder:<init>	()V
    //   104: ldc_w 1385
    //   107: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   110: aload_3
    //   111: invokevirtual 1388	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   114: invokevirtual 84	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   117: invokestatic 309	com/appsflyer/a:c	(Ljava/lang/String;)V
    //   120: aload_1
    //   121: astore_3
    //   122: aload 4
    //   124: ifnull -30 -> 94
    //   127: aload 4
    //   129: invokeinterface 1378 1 0
    //   134: aload_1
    //   135: astore_3
    //   136: goto -42 -> 94
    //   139: astore_3
    //   140: aload_1
    //   141: astore_3
    //   142: goto -48 -> 94
    //   145: astore_1
    //   146: aload 4
    //   148: ifnull +10 -> 158
    //   151: aload 4
    //   153: invokeinterface 1378 1 0
    //   158: aload_1
    //   159: athrow
    //   160: astore_1
    //   161: aconst_null
    //   162: areturn
    //   163: astore_3
    //   164: aload_1
    //   165: astore_3
    //   166: goto -72 -> 94
    //   169: astore_3
    //   170: goto -12 -> 158
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	173	0	this	AppsFlyerLib
    //   0	173	1	paramContentResolver	android.content.ContentResolver
    //   39	2	2	bool	boolean
    //   75	20	3	localObject	Object
    //   96	15	3	localException1	Exception
    //   121	15	3	localContentResolver1	android.content.ContentResolver
    //   139	1	3	localException2	Exception
    //   141	1	3	localContentResolver2	android.content.ContentResolver
    //   163	1	3	localException3	Exception
    //   165	1	3	localContentResolver3	android.content.ContentResolver
    //   169	1	3	localException4	Exception
    //   23	129	4	localCursor	android.database.Cursor
    // Exception table:
    //   from	to	target	type
    //   32	40	96	java/lang/Exception
    //   58	76	96	java/lang/Exception
    //   127	134	139	java/lang/Exception
    //   32	40	145	finally
    //   58	76	145	finally
    //   97	120	145	finally
    //   49	56	160	java/lang/Exception
    //   85	92	163	java/lang/Exception
    //   151	158	169	java/lang/Exception
  }
  
  public Map<String, String> a(Context paramContext)
    throws f
  {
    SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("appsflyer-data", 0);
    String str = e.a().a(paramContext);
    if ((str != null) && (str.length() > 0) && (str.contains("af_tranid"))) {
      return a(paramContext, str);
    }
    paramContext = localSharedPreferences.getString("attributionId", null);
    if ((paramContext != null) && (paramContext.length() > 0)) {
      return f(paramContext);
    }
    throw new f();
  }
  
  public void a(Application paramApplication, String paramString)
  {
    a(paramApplication);
    d(paramApplication.getApplicationContext());
    a("AppsFlyerKey", paramString);
    l.a(paramString);
  }
  
  public void a(Context paramContext, c paramc)
  {
    if (paramc == null) {
      return;
    }
    j = paramc;
  }
  
  public void a(Context paramContext, String paramString, Map<String, Object> paramMap)
  {
    Object localObject = paramMap;
    if (paramMap == null) {
      localObject = new HashMap();
    }
    localObject = new JSONObject((Map)localObject);
    paramMap = e.a().a(paramContext);
    localObject = ((JSONObject)localObject).toString();
    if (paramMap == null) {
      paramMap = "";
    }
    for (;;)
    {
      a(paramContext, null, paramString, (String)localObject, paramMap, true);
      return;
    }
  }
  
  public void a(String paramString)
  {
    e.a().a("GCM_PROJECT_ID", paramString);
  }
  
  protected void a(String paramString1, String paramString2)
  {
    e.a().a(paramString1, paramString2);
  }
  
  public void a(boolean paramBoolean)
  {
    a("collectIMEI", Boolean.toString(paramBoolean));
  }
  
  public String b()
  {
    return c("AppUserId");
  }
  
  public void b(String paramString)
  {
    i = paramString;
  }
  
  public boolean b(Context paramContext)
  {
    boolean bool = false;
    try
    {
      int i1 = paramContext.getPackageManager().getApplicationInfo(paramContext.getPackageName(), 0).flags;
      if ((i1 & 0x1) != 0) {
        bool = true;
      }
      return bool;
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      a.a("Could not check if app is pre installed", paramContext);
    }
    return false;
  }
  
  public String c()
  {
    String str1 = "35" + Build.BOARD.length() % 10 + Build.BRAND.length() % 10 + Build.CPU_ABI.length() % 10 + Build.DEVICE.length() % 10 + Build.MANUFACTURER.length() % 10 + Build.MODEL.length() % 10 + Build.PRODUCT.length() % 10;
    try
    {
      String str2 = Build.class.getField("SERIAL").get(null).toString();
      str2 = new UUID(str1.hashCode(), str2.hashCode()).toString();
      return str2;
    }
    catch (Exception localException) {}
    return new UUID(str1.hashCode(), "serial".hashCode()).toString();
  }
  
  public String c(Context paramContext)
  {
    return k.a(paramContext);
  }
  
  public String c(String paramString)
  {
    return e.a().a(paramString);
  }
  
  public void d(String paramString)
  {
    e.a().a("currencyCode", paramString);
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    String str = paramIntent.getStringExtra("shouldMonitor");
    if (str != null)
    {
      a.a("Turning on monitoring.");
      e.a().a("shouldMonitor", str.equals("true"));
      a(paramContext, null, "START_TRACKING", paramContext.getPackageName());
    }
    do
    {
      do
      {
        return;
        a.a("****** onReceive called *******");
        a("******* onReceive: ", "", paramContext);
        e.a().b();
        str = paramIntent.getStringExtra("referrer");
        a.a("Play store referrer: " + str);
      } while (str == null);
      paramIntent = paramIntent.getStringExtra("TestIntegrationMode");
      if ((paramIntent != null) && (paramIntent.equals("AppsFlyer_Test")))
      {
        paramIntent = paramContext.getSharedPreferences("appsflyer-data", 0).edit();
        paramIntent.clear();
        a(paramIntent);
        e.a().a(false);
        g();
      }
      a("onReceive called. referrer: ", str, paramContext);
      a(paramContext, "referrer", str);
      e.a().b(str);
    } while (!e.a().c());
    a.a("onReceive: isLaunchCalled");
    a(paramContext, null, null, null, str, false);
  }
  
  private abstract class a
    implements Runnable
  {
    protected WeakReference<Context> a = null;
    private String c;
    private ScheduledExecutorService d;
    private AtomicInteger e = new AtomicInteger(0);
    
    public a(Context paramContext, String paramString, ScheduledExecutorService paramScheduledExecutorService)
    {
      this.a = new WeakReference(paramContext);
      this.c = paramString;
      this.d = paramScheduledExecutorService;
    }
    
    public abstract String a();
    
    protected abstract void a(String paramString, int paramInt);
    
    protected abstract void a(Map<String, String> paramMap);
    
    /* Error */
    public void run()
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 42	com/appsflyer/AppsFlyerLib$a:c	Ljava/lang/String;
      //   4: ifnull +13 -> 17
      //   7: aload_0
      //   8: getfield 42	com/appsflyer/AppsFlyerLib$a:c	Ljava/lang/String;
      //   11: invokevirtual 59	java/lang/String:length	()I
      //   14: ifne +4 -> 18
      //   17: return
      //   18: aload_0
      //   19: getfield 35	com/appsflyer/AppsFlyerLib$a:e	Ljava/util/concurrent/atomic/AtomicInteger;
      //   22: invokevirtual 62	java/util/concurrent/atomic/AtomicInteger:incrementAndGet	()I
      //   25: pop
      //   26: aconst_null
      //   27: astore 10
      //   29: aconst_null
      //   30: astore 9
      //   32: aload 9
      //   34: astore 7
      //   36: aload 10
      //   38: astore 6
      //   40: aload_0
      //   41: getfield 28	com/appsflyer/AppsFlyerLib$a:a	Ljava/lang/ref/WeakReference;
      //   44: invokevirtual 66	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
      //   47: checkcast 68	android/content/Context
      //   50: astore 12
      //   52: aload 12
      //   54: ifnonnull +23 -> 77
      //   57: aload_0
      //   58: getfield 35	com/appsflyer/AppsFlyerLib$a:e	Ljava/util/concurrent/atomic/AtomicInteger;
      //   61: invokevirtual 71	java/util/concurrent/atomic/AtomicInteger:decrementAndGet	()I
      //   64: pop
      //   65: iconst_0
      //   66: ifeq -49 -> 17
      //   69: new 73	java/lang/NullPointerException
      //   72: dup
      //   73: invokespecial 74	java/lang/NullPointerException:<init>	()V
      //   76: athrow
      //   77: aload 9
      //   79: astore 7
      //   81: aload 10
      //   83: astore 6
      //   85: invokestatic 80	java/lang/System:currentTimeMillis	()J
      //   88: lstore 4
      //   90: aload 9
      //   92: astore 7
      //   94: aload 10
      //   96: astore 6
      //   98: aload_0
      //   99: getfield 23	com/appsflyer/AppsFlyerLib$a:b	Lcom/appsflyer/AppsFlyerLib;
      //   102: aload 12
      //   104: aload_0
      //   105: getfield 23	com/appsflyer/AppsFlyerLib$a:b	Lcom/appsflyer/AppsFlyerLib;
      //   108: aload 12
      //   110: invokestatic 83	com/appsflyer/AppsFlyerLib:c	(Lcom/appsflyer/AppsFlyerLib;Landroid/content/Context;)Ljava/lang/String;
      //   113: invokestatic 86	com/appsflyer/AppsFlyerLib:a	(Lcom/appsflyer/AppsFlyerLib;Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
      //   116: astore 11
      //   118: ldc 88
      //   120: astore 8
      //   122: aload 11
      //   124: ifnull +33 -> 157
      //   127: aload 9
      //   129: astore 7
      //   131: aload 10
      //   133: astore 6
      //   135: new 90	java/lang/StringBuilder
      //   138: dup
      //   139: invokespecial 91	java/lang/StringBuilder:<init>	()V
      //   142: ldc 93
      //   144: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   147: aload 11
      //   149: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   152: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   155: astore 8
      //   157: aload 9
      //   159: astore 7
      //   161: aload 10
      //   163: astore 6
      //   165: new 90	java/lang/StringBuilder
      //   168: dup
      //   169: invokespecial 91	java/lang/StringBuilder:<init>	()V
      //   172: aload_0
      //   173: invokevirtual 102	com/appsflyer/AppsFlyerLib$a:a	()Ljava/lang/String;
      //   176: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   179: aload 12
      //   181: invokevirtual 105	android/content/Context:getPackageName	()Ljava/lang/String;
      //   184: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   187: aload 8
      //   189: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   192: ldc 107
      //   194: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   197: aload_0
      //   198: getfield 42	com/appsflyer/AppsFlyerLib$a:c	Ljava/lang/String;
      //   201: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   204: ldc 109
      //   206: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   209: aload_0
      //   210: getfield 23	com/appsflyer/AppsFlyerLib$a:b	Lcom/appsflyer/AppsFlyerLib;
      //   213: aload 12
      //   215: invokevirtual 112	com/appsflyer/AppsFlyerLib:c	(Landroid/content/Context;)Ljava/lang/String;
      //   218: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   221: astore 8
      //   223: aload 9
      //   225: astore 7
      //   227: aload 10
      //   229: astore 6
      //   231: new 90	java/lang/StringBuilder
      //   234: dup
      //   235: invokespecial 91	java/lang/StringBuilder:<init>	()V
      //   238: ldc 114
      //   240: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   243: aload 8
      //   245: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   248: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   251: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   254: invokestatic 119	com/appsflyer/l:b	(Ljava/lang/String;)V
      //   257: aload 9
      //   259: astore 7
      //   261: aload 10
      //   263: astore 6
      //   265: new 121	java/net/URL
      //   268: dup
      //   269: aload 8
      //   271: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   274: invokespecial 123	java/net/URL:<init>	(Ljava/lang/String;)V
      //   277: invokevirtual 127	java/net/URL:openConnection	()Ljava/net/URLConnection;
      //   280: checkcast 129	java/net/HttpURLConnection
      //   283: astore 10
      //   285: aload 10
      //   287: astore 7
      //   289: aload 10
      //   291: astore 6
      //   293: aload 10
      //   295: ldc -125
      //   297: invokevirtual 134	java/net/HttpURLConnection:setRequestMethod	(Ljava/lang/String;)V
      //   300: aload 10
      //   302: astore 7
      //   304: aload 10
      //   306: astore 6
      //   308: aload 10
      //   310: sipush 10000
      //   313: invokevirtual 137	java/net/HttpURLConnection:setConnectTimeout	(I)V
      //   316: aload 10
      //   318: astore 7
      //   320: aload 10
      //   322: astore 6
      //   324: aload 10
      //   326: ldc -117
      //   328: ldc -115
      //   330: invokevirtual 145	java/net/HttpURLConnection:setRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
      //   333: aload 10
      //   335: astore 7
      //   337: aload 10
      //   339: astore 6
      //   341: aload 10
      //   343: invokevirtual 148	java/net/HttpURLConnection:connect	()V
      //   346: aload 10
      //   348: astore 7
      //   350: aload 10
      //   352: astore 6
      //   354: aload 10
      //   356: invokevirtual 151	java/net/HttpURLConnection:getResponseCode	()I
      //   359: sipush 200
      //   362: if_icmpne +655 -> 1017
      //   365: aload 10
      //   367: astore 7
      //   369: aload 10
      //   371: astore 6
      //   373: invokestatic 80	java/lang/System:currentTimeMillis	()J
      //   376: lstore_2
      //   377: aload 10
      //   379: astore 7
      //   381: aload 10
      //   383: astore 6
      //   385: aload_0
      //   386: getfield 23	com/appsflyer/AppsFlyerLib$a:b	Lcom/appsflyer/AppsFlyerLib;
      //   389: aload 12
      //   391: ldc -103
      //   393: lload_2
      //   394: lload 4
      //   396: lsub
      //   397: ldc2_w 154
      //   400: ldiv
      //   401: invokestatic 158	com/appsflyer/AppsFlyerLib:a	(Lcom/appsflyer/AppsFlyerLib;Landroid/content/Context;Ljava/lang/String;J)V
      //   404: aconst_null
      //   405: astore 11
      //   407: aload 10
      //   409: astore 7
      //   411: aload 10
      //   413: astore 6
      //   415: new 90	java/lang/StringBuilder
      //   418: dup
      //   419: invokespecial 91	java/lang/StringBuilder:<init>	()V
      //   422: astore 13
      //   424: aconst_null
      //   425: astore 6
      //   427: new 160	java/io/InputStreamReader
      //   430: dup
      //   431: aload 10
      //   433: invokevirtual 164	java/net/HttpURLConnection:getInputStream	()Ljava/io/InputStream;
      //   436: invokespecial 167	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
      //   439: astore 8
      //   441: new 169	java/io/BufferedReader
      //   444: dup
      //   445: aload 8
      //   447: invokespecial 172	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
      //   450: astore 9
      //   452: aload 9
      //   454: invokevirtual 175	java/io/BufferedReader:readLine	()Ljava/lang/String;
      //   457: astore 6
      //   459: aload 6
      //   461: ifnull +144 -> 605
      //   464: aload 13
      //   466: aload 6
      //   468: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   471: bipush 10
      //   473: invokevirtual 178	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
      //   476: pop
      //   477: goto -25 -> 452
      //   480: astore 6
      //   482: aload 9
      //   484: astore 11
      //   486: aload 6
      //   488: astore 9
      //   490: aload 11
      //   492: ifnull +16 -> 508
      //   495: aload 10
      //   497: astore 7
      //   499: aload 10
      //   501: astore 6
      //   503: aload 11
      //   505: invokevirtual 180	java/io/BufferedReader:close	()V
      //   508: aload 8
      //   510: ifnull +16 -> 526
      //   513: aload 10
      //   515: astore 7
      //   517: aload 10
      //   519: astore 6
      //   521: aload 8
      //   523: invokevirtual 181	java/io/InputStreamReader:close	()V
      //   526: aload 10
      //   528: astore 7
      //   530: aload 10
      //   532: astore 6
      //   534: aload 9
      //   536: athrow
      //   537: astore 8
      //   539: aload 7
      //   541: astore 6
      //   543: invokestatic 184	com/appsflyer/AppsFlyerLib:d	()Lcom/appsflyer/c;
      //   546: ifnull +17 -> 563
      //   549: aload 7
      //   551: astore 6
      //   553: aload_0
      //   554: aload 8
      //   556: invokevirtual 187	java/lang/Throwable:getMessage	()Ljava/lang/String;
      //   559: iconst_0
      //   560: invokevirtual 189	com/appsflyer/AppsFlyerLib$a:a	(Ljava/lang/String;I)V
      //   563: aload 7
      //   565: astore 6
      //   567: aload 8
      //   569: invokevirtual 187	java/lang/Throwable:getMessage	()Ljava/lang/String;
      //   572: aload 8
      //   574: invokestatic 194	com/appsflyer/a:a	(Ljava/lang/String;Ljava/lang/Throwable;)V
      //   577: aload_0
      //   578: getfield 35	com/appsflyer/AppsFlyerLib$a:e	Ljava/util/concurrent/atomic/AtomicInteger;
      //   581: invokevirtual 71	java/util/concurrent/atomic/AtomicInteger:decrementAndGet	()I
      //   584: pop
      //   585: aload 7
      //   587: ifnull +8 -> 595
      //   590: aload 7
      //   592: invokevirtual 197	java/net/HttpURLConnection:disconnect	()V
      //   595: aload_0
      //   596: getfield 44	com/appsflyer/AppsFlyerLib$a:d	Ljava/util/concurrent/ScheduledExecutorService;
      //   599: invokeinterface 202 1 0
      //   604: return
      //   605: aload 9
      //   607: ifnull +16 -> 623
      //   610: aload 10
      //   612: astore 7
      //   614: aload 10
      //   616: astore 6
      //   618: aload 9
      //   620: invokevirtual 180	java/io/BufferedReader:close	()V
      //   623: aload 8
      //   625: ifnull +16 -> 641
      //   628: aload 10
      //   630: astore 7
      //   632: aload 10
      //   634: astore 6
      //   636: aload 8
      //   638: invokevirtual 181	java/io/InputStreamReader:close	()V
      //   641: aload 10
      //   643: astore 7
      //   645: aload 10
      //   647: astore 6
      //   649: new 90	java/lang/StringBuilder
      //   652: dup
      //   653: invokespecial 91	java/lang/StringBuilder:<init>	()V
      //   656: ldc -52
      //   658: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   661: aload 13
      //   663: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   666: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   669: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   672: invokestatic 119	com/appsflyer/l:b	(Ljava/lang/String;)V
      //   675: aload 10
      //   677: astore 7
      //   679: aload 10
      //   681: astore 6
      //   683: aload 13
      //   685: invokevirtual 205	java/lang/StringBuilder:length	()I
      //   688: ifle +253 -> 941
      //   691: aload 12
      //   693: ifnull +248 -> 941
      //   696: aload 10
      //   698: astore 7
      //   700: aload 10
      //   702: astore 6
      //   704: aload_0
      //   705: getfield 23	com/appsflyer/AppsFlyerLib$a:b	Lcom/appsflyer/AppsFlyerLib;
      //   708: aload 13
      //   710: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   713: invokestatic 208	com/appsflyer/AppsFlyerLib:a	(Lcom/appsflyer/AppsFlyerLib;Ljava/lang/String;)Ljava/util/Map;
      //   716: astore 8
      //   718: aload 10
      //   720: astore 7
      //   722: aload 10
      //   724: astore 6
      //   726: aload 8
      //   728: ldc -46
      //   730: invokeinterface 215 2 0
      //   735: checkcast 55	java/lang/String
      //   738: astore 9
      //   740: aload 9
      //   742: ifnull +43 -> 785
      //   745: aload 10
      //   747: astore 7
      //   749: aload 10
      //   751: astore 6
      //   753: ldc -39
      //   755: aload 9
      //   757: invokevirtual 221	java/lang/String:equals	(Ljava/lang/Object;)Z
      //   760: ifeq +25 -> 785
      //   763: aload 10
      //   765: astore 7
      //   767: aload 10
      //   769: astore 6
      //   771: aload_0
      //   772: getfield 23	com/appsflyer/AppsFlyerLib$a:b	Lcom/appsflyer/AppsFlyerLib;
      //   775: aload 12
      //   777: ldc -33
      //   779: invokestatic 80	java/lang/System:currentTimeMillis	()J
      //   782: invokestatic 158	com/appsflyer/AppsFlyerLib:a	(Lcom/appsflyer/AppsFlyerLib;Landroid/content/Context;Ljava/lang/String;J)V
      //   785: aload 10
      //   787: astore 7
      //   789: aload 10
      //   791: astore 6
      //   793: new 225	org/json/JSONObject
      //   796: dup
      //   797: aload 8
      //   799: invokespecial 227	org/json/JSONObject:<init>	(Ljava/util/Map;)V
      //   802: invokevirtual 228	org/json/JSONObject:toString	()Ljava/lang/String;
      //   805: astore 11
      //   807: aload 11
      //   809: ifnull +153 -> 962
      //   812: aload 10
      //   814: astore 7
      //   816: aload 10
      //   818: astore 6
      //   820: aload_0
      //   821: getfield 23	com/appsflyer/AppsFlyerLib$a:b	Lcom/appsflyer/AppsFlyerLib;
      //   824: aload 12
      //   826: ldc -26
      //   828: aload 11
      //   830: invokestatic 233	com/appsflyer/AppsFlyerLib:a	(Lcom/appsflyer/AppsFlyerLib;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
      //   833: aload 10
      //   835: astore 7
      //   837: aload 10
      //   839: astore 6
      //   841: new 90	java/lang/StringBuilder
      //   844: dup
      //   845: invokespecial 91	java/lang/StringBuilder:<init>	()V
      //   848: ldc -21
      //   850: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   853: aload 9
      //   855: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   858: ldc -19
      //   860: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   863: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   866: invokestatic 238	com/appsflyer/a:b	(Ljava/lang/String;)V
      //   869: aload 10
      //   871: astore 7
      //   873: aload 10
      //   875: astore 6
      //   877: invokestatic 184	com/appsflyer/AppsFlyerLib:d	()Lcom/appsflyer/c;
      //   880: ifnull +61 -> 941
      //   883: aload 10
      //   885: astore 7
      //   887: aload 10
      //   889: astore 6
      //   891: aload_0
      //   892: getfield 35	com/appsflyer/AppsFlyerLib$a:e	Ljava/util/concurrent/atomic/AtomicInteger;
      //   895: invokevirtual 241	java/util/concurrent/atomic/AtomicInteger:intValue	()I
      //   898: istore_1
      //   899: iload_1
      //   900: iconst_1
      //   901: if_icmpgt +40 -> 941
      //   904: aload 10
      //   906: astore 7
      //   908: aload 10
      //   910: astore 6
      //   912: aload_0
      //   913: getfield 23	com/appsflyer/AppsFlyerLib$a:b	Lcom/appsflyer/AppsFlyerLib;
      //   916: aload 12
      //   918: invokevirtual 244	com/appsflyer/AppsFlyerLib:a	(Landroid/content/Context;)Ljava/util/Map;
      //   921: astore 9
      //   923: aload 9
      //   925: astore 8
      //   927: aload 10
      //   929: astore 7
      //   931: aload 10
      //   933: astore 6
      //   935: aload_0
      //   936: aload 8
      //   938: invokevirtual 246	com/appsflyer/AppsFlyerLib$a:a	(Ljava/util/Map;)V
      //   941: aload_0
      //   942: getfield 35	com/appsflyer/AppsFlyerLib$a:e	Ljava/util/concurrent/atomic/AtomicInteger;
      //   945: invokevirtual 71	java/util/concurrent/atomic/AtomicInteger:decrementAndGet	()I
      //   948: pop
      //   949: aload 10
      //   951: ifnull -356 -> 595
      //   954: aload 10
      //   956: invokevirtual 197	java/net/HttpURLConnection:disconnect	()V
      //   959: goto -364 -> 595
      //   962: aload 10
      //   964: astore 7
      //   966: aload 10
      //   968: astore 6
      //   970: aload_0
      //   971: getfield 23	com/appsflyer/AppsFlyerLib$a:b	Lcom/appsflyer/AppsFlyerLib;
      //   974: aload 12
      //   976: ldc -26
      //   978: aload 13
      //   980: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   983: invokestatic 233	com/appsflyer/AppsFlyerLib:a	(Lcom/appsflyer/AppsFlyerLib;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
      //   986: goto -153 -> 833
      //   989: astore 7
      //   991: aload_0
      //   992: getfield 35	com/appsflyer/AppsFlyerLib$a:e	Ljava/util/concurrent/atomic/AtomicInteger;
      //   995: invokevirtual 71	java/util/concurrent/atomic/AtomicInteger:decrementAndGet	()I
      //   998: pop
      //   999: aload 6
      //   1001: ifnull +8 -> 1009
      //   1004: aload 6
      //   1006: invokevirtual 197	java/net/HttpURLConnection:disconnect	()V
      //   1009: aload 7
      //   1011: athrow
      //   1012: astore 6
      //   1014: goto -87 -> 927
      //   1017: aload 10
      //   1019: astore 7
      //   1021: aload 10
      //   1023: astore 6
      //   1025: invokestatic 184	com/appsflyer/AppsFlyerLib:d	()Lcom/appsflyer/c;
      //   1028: ifnull +43 -> 1071
      //   1031: aload 10
      //   1033: astore 7
      //   1035: aload 10
      //   1037: astore 6
      //   1039: aload_0
      //   1040: new 90	java/lang/StringBuilder
      //   1043: dup
      //   1044: invokespecial 91	java/lang/StringBuilder:<init>	()V
      //   1047: ldc -8
      //   1049: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1052: aload 10
      //   1054: invokevirtual 151	java/net/HttpURLConnection:getResponseCode	()I
      //   1057: invokevirtual 251	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
      //   1060: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   1063: aload 10
      //   1065: invokevirtual 151	java/net/HttpURLConnection:getResponseCode	()I
      //   1068: invokevirtual 189	com/appsflyer/AppsFlyerLib$a:a	(Ljava/lang/String;I)V
      //   1071: aload 10
      //   1073: astore 7
      //   1075: aload 10
      //   1077: astore 6
      //   1079: new 90	java/lang/StringBuilder
      //   1082: dup
      //   1083: invokespecial 91	java/lang/StringBuilder:<init>	()V
      //   1086: ldc -3
      //   1088: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1091: aload 10
      //   1093: invokevirtual 151	java/net/HttpURLConnection:getResponseCode	()I
      //   1096: invokevirtual 251	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
      //   1099: ldc -1
      //   1101: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1104: aload 8
      //   1106: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   1109: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   1112: invokestatic 119	com/appsflyer/l:b	(Ljava/lang/String;)V
      //   1115: goto -174 -> 941
      //   1118: astore 9
      //   1120: aload 6
      //   1122: astore 8
      //   1124: goto -634 -> 490
      //   1127: astore 9
      //   1129: goto -639 -> 490
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	1132	0	this	a
      //   898	4	1	i	int
      //   376	18	2	l1	long
      //   88	307	4	l2	long
      //   38	429	6	localObject1	Object
      //   480	7	6	localObject2	Object
      //   501	504	6	localObject3	Object
      //   1012	1	6	localf	f
      //   1023	98	6	localHttpURLConnection1	java.net.HttpURLConnection
      //   34	931	7	localObject4	Object
      //   989	21	7	localObject5	Object
      //   1019	55	7	localHttpURLConnection2	java.net.HttpURLConnection
      //   120	402	8	localObject6	Object
      //   537	100	8	localThrowable	Throwable
      //   716	407	8	localObject7	Object
      //   30	894	9	localObject8	Object
      //   1118	1	9	localObject9	Object
      //   1127	1	9	localObject10	Object
      //   27	1065	10	localHttpURLConnection3	java.net.HttpURLConnection
      //   116	713	11	localObject11	Object
      //   50	925	12	localContext	Context
      //   422	557	13	localStringBuilder	StringBuilder
      // Exception table:
      //   from	to	target	type
      //   452	459	480	finally
      //   464	477	480	finally
      //   40	52	537	java/lang/Throwable
      //   85	90	537	java/lang/Throwable
      //   98	118	537	java/lang/Throwable
      //   135	157	537	java/lang/Throwable
      //   165	223	537	java/lang/Throwable
      //   231	257	537	java/lang/Throwable
      //   265	285	537	java/lang/Throwable
      //   293	300	537	java/lang/Throwable
      //   308	316	537	java/lang/Throwable
      //   324	333	537	java/lang/Throwable
      //   341	346	537	java/lang/Throwable
      //   354	365	537	java/lang/Throwable
      //   373	377	537	java/lang/Throwable
      //   385	404	537	java/lang/Throwable
      //   415	424	537	java/lang/Throwable
      //   503	508	537	java/lang/Throwable
      //   521	526	537	java/lang/Throwable
      //   534	537	537	java/lang/Throwable
      //   618	623	537	java/lang/Throwable
      //   636	641	537	java/lang/Throwable
      //   649	675	537	java/lang/Throwable
      //   683	691	537	java/lang/Throwable
      //   704	718	537	java/lang/Throwable
      //   726	740	537	java/lang/Throwable
      //   753	763	537	java/lang/Throwable
      //   771	785	537	java/lang/Throwable
      //   793	807	537	java/lang/Throwable
      //   820	833	537	java/lang/Throwable
      //   841	869	537	java/lang/Throwable
      //   877	883	537	java/lang/Throwable
      //   891	899	537	java/lang/Throwable
      //   912	923	537	java/lang/Throwable
      //   935	941	537	java/lang/Throwable
      //   970	986	537	java/lang/Throwable
      //   1025	1031	537	java/lang/Throwable
      //   1039	1071	537	java/lang/Throwable
      //   1079	1115	537	java/lang/Throwable
      //   40	52	989	finally
      //   85	90	989	finally
      //   98	118	989	finally
      //   135	157	989	finally
      //   165	223	989	finally
      //   231	257	989	finally
      //   265	285	989	finally
      //   293	300	989	finally
      //   308	316	989	finally
      //   324	333	989	finally
      //   341	346	989	finally
      //   354	365	989	finally
      //   373	377	989	finally
      //   385	404	989	finally
      //   415	424	989	finally
      //   503	508	989	finally
      //   521	526	989	finally
      //   534	537	989	finally
      //   543	549	989	finally
      //   553	563	989	finally
      //   567	577	989	finally
      //   618	623	989	finally
      //   636	641	989	finally
      //   649	675	989	finally
      //   683	691	989	finally
      //   704	718	989	finally
      //   726	740	989	finally
      //   753	763	989	finally
      //   771	785	989	finally
      //   793	807	989	finally
      //   820	833	989	finally
      //   841	869	989	finally
      //   877	883	989	finally
      //   891	899	989	finally
      //   912	923	989	finally
      //   935	941	989	finally
      //   970	986	989	finally
      //   1025	1031	989	finally
      //   1039	1071	989	finally
      //   1079	1115	989	finally
      //   912	923	1012	com/appsflyer/f
      //   427	441	1118	finally
      //   441	452	1127	finally
    }
  }
  
  private class b
    implements Runnable
  {
    private WeakReference<Context> b = null;
    
    public b(Context paramContext)
    {
      this.b = new WeakReference(paramContext);
    }
    
    /* Error */
    public void run()
    {
      // Byte code:
      //   0: invokestatic 35	com/appsflyer/AppsFlyerLib:e	()Z
      //   3: ifeq +4 -> 7
      //   6: return
      //   7: invokestatic 41	java/lang/System:currentTimeMillis	()J
      //   10: invokestatic 44	com/appsflyer/AppsFlyerLib:a	(J)J
      //   13: pop2
      //   14: aload_0
      //   15: getfield 22	com/appsflyer/AppsFlyerLib$b:b	Ljava/lang/ref/WeakReference;
      //   18: ifnull -12 -> 6
      //   21: iconst_1
      //   22: invokestatic 47	com/appsflyer/AppsFlyerLib:b	(Z)Z
      //   25: pop
      //   26: aload_0
      //   27: getfield 17	com/appsflyer/AppsFlyerLib$b:a	Lcom/appsflyer/AppsFlyerLib;
      //   30: ldc 49
      //   32: invokevirtual 53	com/appsflyer/AppsFlyerLib:c	(Ljava/lang/String;)Ljava/lang/String;
      //   35: astore 6
      //   37: aload_0
      //   38: getfield 22	com/appsflyer/AppsFlyerLib$b:b	Ljava/lang/ref/WeakReference;
      //   41: astore 5
      //   43: aload 5
      //   45: monitorenter
      //   46: invokestatic 58	com/appsflyer/a/a:a	()Lcom/appsflyer/a/a;
      //   49: aload_0
      //   50: getfield 22	com/appsflyer/AppsFlyerLib$b:b	Ljava/lang/ref/WeakReference;
      //   53: invokevirtual 62	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
      //   56: checkcast 64	android/content/Context
      //   59: invokevirtual 67	com/appsflyer/a/a:b	(Landroid/content/Context;)Ljava/util/List;
      //   62: invokeinterface 73 1 0
      //   67: astore 7
      //   69: aload 7
      //   71: invokeinterface 78 1 0
      //   76: ifeq +163 -> 239
      //   79: aload 7
      //   81: invokeinterface 81 1 0
      //   86: checkcast 83	com/appsflyer/a/b
      //   89: astore 8
      //   91: new 85	java/lang/StringBuilder
      //   94: dup
      //   95: invokespecial 86	java/lang/StringBuilder:<init>	()V
      //   98: ldc 88
      //   100: invokevirtual 92	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   103: aload 8
      //   105: invokevirtual 95	com/appsflyer/a/b:c	()Ljava/lang/String;
      //   108: invokevirtual 92	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   111: invokevirtual 98	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   114: invokestatic 103	com/appsflyer/a:a	(Ljava/lang/String;)V
      //   117: invokestatic 41	java/lang/System:currentTimeMillis	()J
      //   120: lstore_1
      //   121: aload 8
      //   123: invokevirtual 106	com/appsflyer/a/b:d	()Ljava/lang/String;
      //   126: bipush 10
      //   128: invokestatic 112	java/lang/Long:parseLong	(Ljava/lang/String;I)J
      //   131: lstore_3
      //   132: aload_0
      //   133: getfield 17	com/appsflyer/AppsFlyerLib$b:a	Lcom/appsflyer/AppsFlyerLib;
      //   136: new 85	java/lang/StringBuilder
      //   139: dup
      //   140: invokespecial 86	java/lang/StringBuilder:<init>	()V
      //   143: aload 8
      //   145: invokevirtual 95	com/appsflyer/a/b:c	()Ljava/lang/String;
      //   148: invokevirtual 92	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   151: ldc 114
      //   153: invokevirtual 92	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   156: lload_1
      //   157: lload_3
      //   158: lsub
      //   159: ldc2_w 115
      //   162: ldiv
      //   163: invokestatic 119	java/lang/Long:toString	(J)Ljava/lang/String;
      //   166: invokevirtual 92	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   169: invokevirtual 98	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   172: aload 8
      //   174: invokevirtual 121	com/appsflyer/a/b:b	()Ljava/lang/String;
      //   177: aload 6
      //   179: aload_0
      //   180: getfield 22	com/appsflyer/AppsFlyerLib$b:b	Ljava/lang/ref/WeakReference;
      //   183: aload 8
      //   185: invokevirtual 106	com/appsflyer/a/b:d	()Ljava/lang/String;
      //   188: iconst_0
      //   189: invokestatic 124	com/appsflyer/AppsFlyerLib:a	(Lcom/appsflyer/AppsFlyerLib;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ref/WeakReference;Ljava/lang/String;Z)V
      //   192: goto -123 -> 69
      //   195: astore 8
      //   197: ldc 126
      //   199: invokestatic 103	com/appsflyer/a:a	(Ljava/lang/String;)V
      //   202: goto -133 -> 69
      //   205: astore 6
      //   207: aload 5
      //   209: monitorexit
      //   210: aload 6
      //   212: athrow
      //   213: astore 5
      //   215: ldc -128
      //   217: invokestatic 103	com/appsflyer/a:a	(Ljava/lang/String;)V
      //   220: iconst_0
      //   221: invokestatic 47	com/appsflyer/AppsFlyerLib:b	(Z)Z
      //   224: pop
      //   225: invokestatic 132	com/appsflyer/AppsFlyerLib:f	()Ljava/util/concurrent/ScheduledExecutorService;
      //   228: invokeinterface 137 1 0
      //   233: aconst_null
      //   234: invokestatic 140	com/appsflyer/AppsFlyerLib:a	(Ljava/util/concurrent/ScheduledExecutorService;)Ljava/util/concurrent/ScheduledExecutorService;
      //   237: pop
      //   238: return
      //   239: aload 5
      //   241: monitorexit
      //   242: iconst_0
      //   243: invokestatic 47	com/appsflyer/AppsFlyerLib:b	(Z)Z
      //   246: pop
      //   247: goto -22 -> 225
      //   250: astore 5
      //   252: iconst_0
      //   253: invokestatic 47	com/appsflyer/AppsFlyerLib:b	(Z)Z
      //   256: pop
      //   257: aload 5
      //   259: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	260	0	this	b
      //   120	37	1	l1	long
      //   131	27	3	l2	long
      //   213	27	5	localException1	Exception
      //   250	8	5	localObject1	Object
      //   35	143	6	str	String
      //   205	6	6	localObject2	Object
      //   67	13	7	localIterator	Iterator
      //   89	95	8	localb	com.appsflyer.a.b
      //   195	1	8	localException2	Exception
      // Exception table:
      //   from	to	target	type
      //   117	192	195	java/lang/Exception
      //   46	69	205	finally
      //   69	117	205	finally
      //   117	192	205	finally
      //   197	202	205	finally
      //   207	210	205	finally
      //   239	242	205	finally
      //   26	46	213	java/lang/Exception
      //   210	213	213	java/lang/Exception
      //   26	46	250	finally
      //   210	213	250	finally
      //   215	220	250	finally
    }
  }
  
  private class c
    implements Runnable
  {
    private Context b;
    private String c;
    private String d;
    private String e;
    private String f;
    private ExecutorService g;
    private boolean h;
    
    private c(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean, ExecutorService paramExecutorService)
    {
      this.b = paramContext;
      this.c = paramString1;
      this.d = paramString2;
      this.e = paramString3;
      this.f = paramString4;
      this.h = paramBoolean;
      this.g = paramExecutorService;
    }
    
    public void run()
    {
      AppsFlyerLib.a(AppsFlyerLib.this, this.b, this.c, this.d, this.e, this.f, this.h);
      this.g.shutdown();
    }
  }
  
  private class d
    extends AppsFlyerLib.a
  {
    public d(Context paramContext, String paramString, ScheduledExecutorService paramScheduledExecutorService)
    {
      super(paramContext, paramString, paramScheduledExecutorService);
    }
    
    public String a()
    {
      return "https://api.appsflyer.com/install_data/v3/";
    }
    
    protected void a(String paramString, int paramInt)
    {
      AppsFlyerLib.d().a(paramString);
      if ((paramInt >= 400) && (paramInt < 500))
      {
        paramInt = ((Context)this.a.get()).getSharedPreferences("appsflyer-data", 0).getInt("appsflyerConversionDataRequestRetries", 0);
        AppsFlyerLib.a(AppsFlyerLib.this, (Context)this.a.get(), "appsflyerConversionDataRequestRetries", paramInt + 1);
      }
    }
    
    protected void a(Map<String, String> paramMap)
    {
      AppsFlyerLib.d().a(paramMap);
      ((Context)this.a.get()).getSharedPreferences("appsflyer-data", 0);
      AppsFlyerLib.a(AppsFlyerLib.this, (Context)this.a.get(), "appsflyerConversionDataRequestRetries", 0);
    }
  }
  
  private class e
    implements Runnable
  {
    Map<String, String> a;
    boolean b;
    private String d;
    private WeakReference<Context> e = null;
    
    private e(Map<String, String> paramMap, Context paramContext, boolean paramBoolean)
    {
      this.d = paramMap;
      this.a = paramContext;
      this.e = new WeakReference(paramBoolean);
      boolean bool;
      this.b = bool;
    }
    
    public void run()
    {
      String str2 = null;
      String str1 = str2;
      for (;;)
      {
        try
        {
          localObject1 = (Context)this.e.get();
          bool1 = false;
          if (localObject1 != null)
          {
            str1 = str2;
            localObject2 = e.a().a((Context)localObject1);
            if (localObject2 != null)
            {
              str1 = str2;
              if (((String)localObject2).length() > 0)
              {
                str1 = str2;
                if (this.a.get("referrer") == null)
                {
                  str1 = str2;
                  this.a.put("referrer", localObject2);
                }
              }
            }
            str1 = str2;
            boolean bool2 = "true".equals(((Context)localObject1).getSharedPreferences("appsflyer-data", 0).getString("sentSuccessfully", ""));
            str1 = str2;
            localObject2 = (String)this.a.get("eventName");
            str1 = str2;
            localObject3 = AppsFlyerLib.this;
            if (localObject2 != null) {
              break label606;
            }
            bool1 = true;
            str1 = str2;
            int i = AppsFlyerLib.a((AppsFlyerLib)localObject3, (Context)localObject1, "appsFlyerCount", bool1);
            str1 = str2;
            this.a.put("counter", Integer.toString(i));
            str1 = str2;
            localObject3 = this.a;
            str1 = str2;
            localObject4 = AppsFlyerLib.this;
            if (localObject2 == null) {
              break label611;
            }
            bool1 = true;
            str1 = str2;
            ((Map)localObject3).put("iaecounter", Integer.toString(AppsFlyerLib.a((AppsFlyerLib)localObject4, (Context)localObject1, "appsFlyerInAppEventCount", bool1)));
            str1 = str2;
            this.a.put("timepassedsincelastlaunch", Long.toString(AppsFlyerLib.a(AppsFlyerLib.this, (Context)localObject1, true)));
            bool1 = bool2;
            str1 = str2;
            if (this.b)
            {
              bool1 = bool2;
              if (i == 1)
              {
                str1 = str2;
                e.a().d();
                bool1 = bool2;
              }
            }
          }
          str1 = str2;
          localObject1 = this.a;
          if (bool1) {
            break label616;
          }
          bool1 = true;
          str1 = str2;
          ((Map)localObject1).put("isFirstCall", Boolean.toString(bool1));
          str1 = str2;
          localObject1 = (String)this.a.get("appsflyerKey");
          if (localObject1 != null)
          {
            str1 = str2;
            if (((String)localObject1).length() != 0) {}
          }
          else
          {
            str1 = str2;
            a.b("Not sending data yet, waiting for dev key");
            return;
          }
          str1 = str2;
          localObject2 = new j().a(this.a);
          str1 = str2;
          this.a.put("af_v", localObject2);
          str1 = str2;
          str2 = new JSONObject(this.a).toString();
          str1 = str2;
          localObject2 = AppsFlyerLib.this;
          str1 = str2;
          localObject3 = this.d;
          str1 = str2;
          localObject4 = this.e;
          str1 = str2;
          if (!this.b) {
            continue;
          }
          str1 = str2;
          if (AppsFlyerLib.d() == null) {
            continue;
          }
          bool1 = true;
        }
        catch (IOException localIOException)
        {
          Object localObject1;
          Object localObject2;
          Object localObject3;
          Object localObject4;
          if ((str1 == null) || (this.e == null) || (this.d.contains("&isCachedRequest=true&timeincache="))) {
            break label605;
          }
          a.a(localIOException.getMessage(), localIOException);
          com.appsflyer.a.a.a().a(new com.appsflyer.a.b(this.d, str1, AppsFlyerLib.b), (Context)this.e.get());
          return;
          bool1 = false;
          continue;
        }
        catch (Throwable localThrowable)
        {
          a.a(localThrowable.getMessage(), localThrowable);
        }
        str1 = str2;
        AppsFlyerLib.a((AppsFlyerLib)localObject2, (String)localObject3, str2, (String)localObject1, (WeakReference)localObject4, null, bool1);
        return;
        label605:
        return;
        label606:
        boolean bool1 = false;
        continue;
        label611:
        bool1 = false;
        continue;
        label616:
        bool1 = false;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/appsflyer/AppsFlyerLib.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */