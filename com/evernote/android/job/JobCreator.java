package com.evernote.android.job;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.support.annotation.NonNull;

public abstract interface JobCreator
{
  public abstract a a(String paramString);
  
  public static abstract class AddJobCreatorReceiver
    extends BroadcastReceiver
  {
    protected abstract void a(@NonNull Context paramContext, @NonNull d paramd);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/JobCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */