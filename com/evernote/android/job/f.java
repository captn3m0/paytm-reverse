package com.evernote.android.job;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.evernote.android.job.a.a.b;
import com.evernote.android.job.a.e;
import java.util.concurrent.TimeUnit;
import net.a.a.a.a;

public final class f
{
  public static final a a = a.b;
  public static final c b = c.a;
  public static final long c = TimeUnit.MINUTES.toMillis(15L);
  public static final long d = TimeUnit.MINUTES.toMillis(5L);
  private static final net.a.a.a.c e = new com.evernote.android.job.a.d("JobRequest");
  private final b f;
  private final com.evernote.android.job.a.c g;
  private int h;
  private long i;
  private boolean j;
  private boolean k;
  
  private f(b paramb)
  {
    this.f = paramb;
    if (b.a(paramb)) {}
    for (paramb = com.evernote.android.job.a.c.d;; paramb = d.a().d())
    {
      this.g = paramb;
      return;
    }
  }
  
  static long a()
  {
    if (d.a().b().b()) {
      return TimeUnit.MINUTES.toMillis(1L);
    }
    return c;
  }
  
  static f a(Cursor paramCursor)
    throws Exception
  {
    boolean bool2 = true;
    f localf = new b(paramCursor, null).a();
    localf.h = paramCursor.getInt(paramCursor.getColumnIndex("numFailures"));
    localf.i = paramCursor.getLong(paramCursor.getColumnIndex("scheduledAt"));
    if (paramCursor.getInt(paramCursor.getColumnIndex("isTransient")) > 0)
    {
      bool1 = true;
      localf.j = bool1;
      if (paramCursor.getInt(paramCursor.getColumnIndex("flexSupport")) <= 0) {
        break label126;
      }
    }
    label126:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      localf.k = bool1;
      e.a(localf.h, "failure count can't be negative");
      e.a(localf.i, "scheduled at can't be negative");
      return localf;
      bool1 = false;
      break;
    }
  }
  
  static long b()
  {
    if (d.a().b().b()) {
      return TimeUnit.SECONDS.toMillis(30L);
    }
    return d;
  }
  
  ContentValues A()
  {
    ContentValues localContentValues = new ContentValues();
    b.a(this.f, localContentValues);
    localContentValues.put("numFailures", Integer.valueOf(this.h));
    localContentValues.put("scheduledAt", Long.valueOf(this.i));
    localContentValues.put("isTransient", Boolean.valueOf(this.j));
    localContentValues.put("flexSupport", Boolean.valueOf(this.k));
    return localContentValues;
  }
  
  int a(boolean paramBoolean1, boolean paramBoolean2)
  {
    f localf = new b(this, paramBoolean2, null).a();
    if (paramBoolean1) {
      this.h += 1;
    }
    return localf.x();
  }
  
  void a(long paramLong)
  {
    this.i = paramLong;
  }
  
  void a(boolean paramBoolean)
  {
    this.k = paramBoolean;
  }
  
  void b(boolean paramBoolean)
  {
    this.j = paramBoolean;
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("isTransient", Boolean.valueOf(this.j));
    d.a().e().a(this, localContentValues);
  }
  
  public int c()
  {
    return b.b(this.f);
  }
  
  @NonNull
  public String d()
  {
    return b.c(this.f);
  }
  
  public long e()
  {
    return b.d(this.f);
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    paramObject = (f)paramObject;
    return this.f.equals(((f)paramObject).f);
  }
  
  public long f()
  {
    return b.e(this.f);
  }
  
  public a g()
  {
    return b.f(this.f);
  }
  
  public long h()
  {
    return b.g(this.f);
  }
  
  public int hashCode()
  {
    return this.f.hashCode();
  }
  
  public boolean i()
  {
    return j() > 0L;
  }
  
  public long j()
  {
    return b.h(this.f);
  }
  
  public long k()
  {
    return b.i(this.f);
  }
  
  public boolean l()
  {
    return b.j(this.f);
  }
  
  public boolean m()
  {
    return b.k(this.f);
  }
  
  public boolean n()
  {
    return b.l(this.f);
  }
  
  public c o()
  {
    return b.m(this.f);
  }
  
  public boolean p()
  {
    return b.n(this.f);
  }
  
  public boolean q()
  {
    return b.o(this.f);
  }
  
  public boolean r()
  {
    return b.a(this.f);
  }
  
  long s()
  {
    if (i()) {
      return 0L;
    }
    long l;
    switch (1.a[g().ordinal()])
    {
    default: 
      throw new IllegalStateException("not implemented");
    case 1: 
      l = this.h * h();
    }
    for (;;)
    {
      return Math.min(l, TimeUnit.HOURS.toMillis(5L));
      if (this.h == 0) {
        l = 0L;
      } else {
        l = (h() * Math.pow(2.0D, this.h - 1));
      }
    }
  }
  
  com.evernote.android.job.a.c t()
  {
    return this.g;
  }
  
  public String toString()
  {
    return "request{id=" + c() + ", tag=" + d() + '}';
  }
  
  long u()
  {
    return this.i;
  }
  
  boolean v()
  {
    return this.j;
  }
  
  boolean w()
  {
    return this.k;
  }
  
  public int x()
  {
    d.a().a(this);
    return c();
  }
  
  public b y()
  {
    d.a().cancel(c());
    b localb = new b(this, false, null);
    this.j = false;
    if (!i())
    {
      long l = System.currentTimeMillis() - this.i;
      localb.a(Math.max(1L, e() - l), Math.max(1L, f() - l));
    }
    return localb;
  }
  
  void z()
  {
    this.h += 1;
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("numFailures", Integer.valueOf(this.h));
    d.a().e().a(this, localContentValues);
  }
  
  public static enum a
  {
    private a() {}
  }
  
  public static final class b
  {
    private final int a;
    private final String b;
    private long c;
    private long d;
    private long e;
    private f.a f;
    private long g;
    private long h;
    private boolean i;
    private boolean j;
    private boolean k;
    private boolean l;
    private f.c m;
    private b n;
    private String o;
    private boolean p;
    private boolean q;
    
    private b(Cursor paramCursor)
      throws Exception
    {
      this.a = paramCursor.getInt(paramCursor.getColumnIndex("_id"));
      this.b = paramCursor.getString(paramCursor.getColumnIndex("tag"));
      this.c = paramCursor.getLong(paramCursor.getColumnIndex("startMs"));
      this.d = paramCursor.getLong(paramCursor.getColumnIndex("endMs"));
      this.e = paramCursor.getLong(paramCursor.getColumnIndex("backoffMs"));
      try
      {
        this.f = f.a.valueOf(paramCursor.getString(paramCursor.getColumnIndex("backoffPolicy")));
        this.g = paramCursor.getLong(paramCursor.getColumnIndex("intervalMs"));
        this.h = paramCursor.getLong(paramCursor.getColumnIndex("flexMs"));
        if (paramCursor.getInt(paramCursor.getColumnIndex("requirementsEnforced")) > 0)
        {
          bool1 = true;
          this.i = bool1;
          if (paramCursor.getInt(paramCursor.getColumnIndex("requiresCharging")) <= 0) {
            break label338;
          }
          bool1 = true;
          this.j = bool1;
          if (paramCursor.getInt(paramCursor.getColumnIndex("requiresDeviceIdle")) <= 0) {
            break label343;
          }
          bool1 = true;
          this.k = bool1;
          if (paramCursor.getInt(paramCursor.getColumnIndex("exact")) <= 0) {
            break label348;
          }
          bool1 = true;
          this.l = bool1;
        }
      }
      catch (Throwable localThrowable1)
      {
        try
        {
          for (;;)
          {
            this.m = f.c.valueOf(paramCursor.getString(paramCursor.getColumnIndex("networkType")));
            this.o = paramCursor.getString(paramCursor.getColumnIndex("extras"));
            if (paramCursor.getInt(paramCursor.getColumnIndex("persisted")) <= 0) {
              break;
            }
            bool1 = bool2;
            this.p = bool1;
            return;
            localThrowable1 = localThrowable1;
            f.B().b(localThrowable1);
            this.f = f.a;
            continue;
            bool1 = false;
            continue;
            label338:
            bool1 = false;
            continue;
            label343:
            bool1 = false;
          }
          label348:
          bool1 = false;
        }
        catch (Throwable localThrowable2)
        {
          for (;;)
          {
            f.B().b(localThrowable2);
            this.m = f.b;
            continue;
            boolean bool1 = false;
          }
        }
      }
    }
    
    private b(f paramf, boolean paramBoolean)
    {
      if (paramBoolean) {}
      for (int i1 = d.a().e().a();; i1 = paramf.c())
      {
        this.a = i1;
        this.b = paramf.d();
        this.c = paramf.e();
        this.d = paramf.f();
        this.e = paramf.h();
        this.f = paramf.g();
        this.g = paramf.j();
        this.h = paramf.k();
        this.i = paramf.l();
        this.j = paramf.m();
        this.k = paramf.n();
        this.l = paramf.r();
        this.m = paramf.o();
        this.n = f.a(paramf).n;
        this.o = f.a(paramf).o;
        this.p = paramf.p();
        return;
      }
    }
    
    public b(@NonNull String paramString)
    {
      this.b = ((String)e.a(paramString));
      this.a = d.a().e().a();
      this.c = -1L;
      this.d = -1L;
      this.e = 30000L;
      this.f = f.a;
      this.m = f.b;
    }
    
    private void a(ContentValues paramContentValues)
    {
      paramContentValues.put("_id", Integer.valueOf(this.a));
      paramContentValues.put("tag", this.b);
      paramContentValues.put("startMs", Long.valueOf(this.c));
      paramContentValues.put("endMs", Long.valueOf(this.d));
      paramContentValues.put("backoffMs", Long.valueOf(this.e));
      paramContentValues.put("backoffPolicy", this.f.toString());
      paramContentValues.put("intervalMs", Long.valueOf(this.g));
      paramContentValues.put("flexMs", Long.valueOf(this.h));
      paramContentValues.put("requirementsEnforced", Boolean.valueOf(this.i));
      paramContentValues.put("requiresCharging", Boolean.valueOf(this.j));
      paramContentValues.put("requiresDeviceIdle", Boolean.valueOf(this.k));
      paramContentValues.put("exact", Boolean.valueOf(this.l));
      paramContentValues.put("networkType", this.m.toString());
      if (this.n != null) {
        paramContentValues.put("extras", this.n.a());
      }
      for (;;)
      {
        paramContentValues.put("persisted", Boolean.valueOf(this.p));
        return;
        if (!TextUtils.isEmpty(this.o)) {
          paramContentValues.put("extras", this.o);
        }
      }
    }
    
    public b a(long paramLong)
    {
      return b(paramLong, paramLong);
    }
    
    public b a(long paramLong1, long paramLong2)
    {
      this.c = e.b(paramLong1, "startMs must be greater than 0");
      this.d = e.a(paramLong2, paramLong1, Long.MAX_VALUE, "endMs");
      if (this.c > 6148914691236517204L)
      {
        a.a("startMs reduced from %d days to %d days", new Object[] { Long.valueOf(TimeUnit.MILLISECONDS.toDays(this.c)), Long.valueOf(TimeUnit.MILLISECONDS.toDays(6148914691236517204L)) });
        this.c = 6148914691236517204L;
      }
      if (this.d > 6148914691236517204L)
      {
        a.a("endMs reduced from %d days to %d days", new Object[] { Long.valueOf(TimeUnit.MILLISECONDS.toDays(this.d)), Long.valueOf(TimeUnit.MILLISECONDS.toDays(6148914691236517204L)) });
        this.d = 6148914691236517204L;
      }
      return this;
    }
    
    public b a(@Nullable f.c paramc)
    {
      this.m = paramc;
      return this;
    }
    
    public b a(boolean paramBoolean)
    {
      this.i = paramBoolean;
      return this;
    }
    
    public f a()
    {
      e.a(this.a, "id can't be negative");
      e.a(this.b);
      e.b(this.e, "backoffMs must be > 0");
      e.a(this.f);
      e.a(this.m);
      if (this.g > 0L)
      {
        e.a(this.g, f.a(), Long.MAX_VALUE, "intervalMs");
        e.a(this.h, f.b(), this.g, "flexMs");
        if ((this.g < f.c) || (this.h < f.d)) {
          f.B().c("AllowSmallerIntervals enabled, this will crash on Android N and later, interval %d (minimum is %d), flex %d (minimum is %d)", new Object[] { Long.valueOf(this.g), Long.valueOf(f.c), Long.valueOf(this.h), Long.valueOf(f.d) });
        }
      }
      if ((this.l) && (this.g > 0L)) {
        throw new IllegalArgumentException("Can't call setExact() on a periodic job.");
      }
      if ((this.l) && (this.c != this.d)) {
        throw new IllegalArgumentException("Can't call setExecutionWindow() for an exact job.");
      }
      if ((this.l) && ((this.i) || (this.k) || (this.j) || (!f.b.equals(this.m)))) {
        throw new IllegalArgumentException("Can't require any condition for an exact job.");
      }
      if ((this.g <= 0L) && ((this.c == -1L) || (this.d == -1L))) {
        throw new IllegalArgumentException("You're trying to build a job with no constraints, this is not allowed.");
      }
      if ((this.g > 0L) && ((this.c != -1L) || (this.d != -1L))) {
        throw new IllegalArgumentException("Can't call setExecutionWindow() on a periodic job.");
      }
      if ((this.g > 0L) && ((this.e != 30000L) || (!f.a.equals(this.f)))) {
        throw new IllegalArgumentException("A periodic job will not respect any back-off policy, so calling setBackoffCriteria() with setPeriodic() is an error.");
      }
      if ((this.g <= 0L) && ((this.c > 3074457345618258602L) || (this.d > 3074457345618258602L))) {
        a.b("Attention: your execution window is too large. This could result in undesired behavior, see https://github.com/evernote/android-job/blob/master/FAQ.md");
      }
      return new f(this, null);
    }
    
    public b b(long paramLong1, long paramLong2)
    {
      this.g = e.a(paramLong1, f.a(), Long.MAX_VALUE, "intervalMs");
      this.h = e.a(paramLong2, f.b(), this.g, "flexMs");
      return this;
    }
    
    public b b(boolean paramBoolean)
    {
      if ((paramBoolean) && (!com.evernote.android.job.a.f.a(d.a().h()))) {
        throw new IllegalStateException("Does not have RECEIVE_BOOT_COMPLETED permission, which is mandatory for this feature");
      }
      this.p = paramBoolean;
      return this;
    }
    
    public b c(boolean paramBoolean)
    {
      this.q = paramBoolean;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      if (this == paramObject) {}
      do
      {
        return true;
        if ((paramObject == null) || (getClass() != paramObject.getClass())) {
          return false;
        }
        paramObject = (b)paramObject;
      } while (this.a == ((b)paramObject).a);
      return false;
    }
    
    public int hashCode()
    {
      return this.a;
    }
  }
  
  public static enum c
  {
    private c() {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */