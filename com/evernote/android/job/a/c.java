package com.evernote.android.job.a;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import com.evernote.android.job.e;

public enum c
{
  private e f;
  private final boolean g;
  private final boolean h;
  
  private c(boolean paramBoolean1, boolean paramBoolean2)
  {
    this.g = paramBoolean1;
    this.h = paramBoolean2;
  }
  
  @NonNull
  public static c a(Context paramContext, boolean paramBoolean)
  {
    if (a.a(paramContext)) {
      return a;
    }
    if (b.a(paramContext)) {
      return b;
    }
    if ((paramBoolean) && (e.a(paramContext))) {
      return e;
    }
    if (c.a(paramContext)) {
      return c;
    }
    return d;
  }
  
  public boolean a()
  {
    return this.g;
  }
  
  public boolean a(Context paramContext)
  {
    switch (1.a[ordinal()])
    {
    default: 
      throw new IllegalStateException("not implemented");
    case 1: 
      if (Build.VERSION.SDK_INT < 24) {
        break;
      }
    case 4: 
    case 2: 
    case 3: 
      do
      {
        do
        {
          return true;
          return false;
        } while (Build.VERSION.SDK_INT >= 21);
        return false;
      } while (Build.VERSION.SDK_INT >= 19);
      return false;
    }
    return b.a(paramContext);
  }
  
  @NonNull
  public e b(Context paramContext)
  {
    switch (1.a[ordinal()])
    {
    default: 
      throw new IllegalStateException("not implemented");
    case 1: 
      return new com.evernote.android.job.c.a(paramContext);
    case 2: 
      return new com.evernote.android.job.v21.a(paramContext);
    case 3: 
      return new com.evernote.android.job.b.a(paramContext);
    case 4: 
      return new com.evernote.android.job.v14.a(paramContext);
    }
    return new com.evernote.android.job.gcm.a(paramContext);
  }
  
  public boolean b()
  {
    return this.h;
  }
  
  @NonNull
  public e c(Context paramContext)
  {
    try
    {
      if (this.f == null) {
        this.f = b(paramContext);
      }
      paramContext = this.f;
      return paramContext;
    }
    finally {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/a/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */