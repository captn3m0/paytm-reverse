package com.evernote.android.job.a;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build.VERSION;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.v4.net.ConnectivityManagerCompat;
import android.telephony.TelephonyManager;
import com.evernote.android.job.f.c;

public final class a
{
  @TargetApi(17)
  public static boolean a(Context paramContext)
  {
    paramContext = paramContext.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
    if (paramContext == null) {}
    int i;
    do
    {
      return false;
      i = paramContext.getIntExtra("plugged", 0);
    } while ((i != 1) && (i != 2) && ((Build.VERSION.SDK_INT < 17) || (i != 4)));
    return true;
  }
  
  public static boolean b(Context paramContext)
  {
    boolean bool1 = true;
    boolean bool2 = false;
    paramContext = (PowerManager)paramContext.getSystemService("power");
    if (Build.VERSION.SDK_INT >= 23) {
      if (!paramContext.isDeviceIdleMode())
      {
        bool1 = bool2;
        if (paramContext.isInteractive()) {}
      }
      else
      {
        bool1 = true;
      }
    }
    do
    {
      do
      {
        return bool1;
        if (Build.VERSION.SDK_INT < 20) {
          break;
        }
      } while (!paramContext.isInteractive());
      return false;
    } while (!paramContext.isScreenOn());
    return false;
  }
  
  @NonNull
  public static f.c c(Context paramContext)
  {
    ConnectivityManager localConnectivityManager = (ConnectivityManager)paramContext.getSystemService("connectivity");
    NetworkInfo localNetworkInfo = localConnectivityManager.getActiveNetworkInfo();
    if ((localNetworkInfo == null) || (!localNetworkInfo.isConnectedOrConnecting())) {
      return f.c.a;
    }
    paramContext = (TelephonyManager)paramContext.getSystemService("phone");
    if ((paramContext != null) && (paramContext.isNetworkRoaming())) {
      return f.c.b;
    }
    if (ConnectivityManagerCompat.a(localConnectivityManager)) {
      return f.c.d;
    }
    return f.c.c;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */