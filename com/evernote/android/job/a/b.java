package com.evernote.android.job.a;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import com.evernote.android.job.gcm.PlatformGcmService;
import com.google.android.gms.common.GoogleApiAvailability;
import java.util.Iterator;
import java.util.List;

final class b
{
  private static final boolean a;
  private static int b = -1;
  private static boolean c;
  
  static
  {
    try
    {
      Class.forName("com.google.android.gms.gcm.GcmNetworkManager");
      bool = true;
    }
    catch (Throwable localThrowable)
    {
      for (;;)
      {
        boolean bool = false;
      }
    }
    a = bool;
  }
  
  private static void a(Context paramContext, boolean paramBoolean)
  {
    PackageManager localPackageManager;
    try
    {
      localPackageManager = paramContext.getPackageManager();
      paramContext = new ComponentName(paramContext, com.evernote.android.job.gcm.a.class.getPackage().getName() + ".PlatformGcmService");
      switch (localPackageManager.getComponentEnabledSetting(paramContext))
      {
      case 1: 
        if (paramBoolean) {
          return;
        }
        localPackageManager.setComponentEnabledSetting(paramContext, 2, 1);
        net.a.a.a.a.a("GCM service disabled");
        return;
      }
    }
    catch (Throwable paramContext)
    {
      net.a.a.a.a.b(paramContext);
      return;
    }
    if (paramBoolean)
    {
      localPackageManager.setComponentEnabledSetting(paramContext, 1, 1);
      net.a.a.a.a.a("GCM service enabled");
      return;
    }
  }
  
  public static boolean a(Context paramContext)
  {
    try
    {
      if (!c)
      {
        c = true;
        a(paramContext, a);
      }
      if ((a) && (GoogleApiAvailability.a().a(paramContext) == 0))
      {
        int i = b(paramContext);
        if (i == 0) {
          return true;
        }
      }
      return false;
    }
    catch (Throwable paramContext)
    {
      net.a.a.a.a.a(paramContext);
    }
    return false;
  }
  
  private static boolean a(List<ResolveInfo> paramList)
  {
    if ((paramList == null) || (paramList.isEmpty())) {}
    ResolveInfo localResolveInfo;
    do
    {
      while (!paramList.hasNext())
      {
        return false;
        paramList = paramList.iterator();
      }
      localResolveInfo = (ResolveInfo)paramList.next();
    } while ((localResolveInfo.serviceInfo == null) || (!"com.google.android.gms.permission.BIND_NETWORK_TASK_SERVICE".equals(localResolveInfo.serviceInfo.permission)) || (!localResolveInfo.serviceInfo.exported));
    return true;
  }
  
  private static int b(Context paramContext)
  {
    if (b < 0)
    {
      try
      {
        if (b >= 0) {
          break label111;
        }
        Intent localIntent = new Intent(paramContext, PlatformGcmService.class);
        int i;
        if (!a(paramContext.getPackageManager().queryIntentServices(localIntent, 0)))
        {
          b = 1;
          i = b;
          return i;
        }
        localIntent = new Intent("com.google.android.gms.gcm.ACTION_TASK_READY");
        localIntent.setPackage(paramContext.getPackageName());
        if (!a(paramContext.getPackageManager().queryIntentServices(localIntent, 0)))
        {
          b = 1;
          i = b;
          return i;
        }
      }
      finally {}
      b = 0;
    }
    label111:
    return b;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/a/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */