package com.evernote.android.job.a.a;

import com.evernote.android.job.a.d;
import java.util.HashMap;
import java.util.Map;
import net.a.a.a.c;

public final class b
{
  private static final c a = new d("PersistableBundleCompat");
  private final Map<String, Object> b;
  
  public b()
  {
    this(new HashMap());
  }
  
  private b(Map<String, Object> paramMap)
  {
    this.b = paramMap;
  }
  
  /* Error */
  @android.support.annotation.NonNull
  public String a()
  {
    // Byte code:
    //   0: new 44	java/io/ByteArrayOutputStream
    //   3: dup
    //   4: invokespecial 45	java/io/ByteArrayOutputStream:<init>	()V
    //   7: astore_2
    //   8: aload_0
    //   9: getfield 32	com/evernote/android/job/a/a/b:b	Ljava/util/Map;
    //   12: aload_2
    //   13: invokestatic 50	com/evernote/android/job/a/a/c:a	(Ljava/util/Map;Ljava/io/OutputStream;)V
    //   16: aload_2
    //   17: ldc 52
    //   19: invokevirtual 56	java/io/ByteArrayOutputStream:toString	(Ljava/lang/String;)Ljava/lang/String;
    //   22: astore_1
    //   23: aload_2
    //   24: invokevirtual 59	java/io/ByteArrayOutputStream:close	()V
    //   27: aload_1
    //   28: areturn
    //   29: astore_1
    //   30: getstatic 21	com/evernote/android/job/a/a/b:a	Lnet/a/a/a/c;
    //   33: aload_1
    //   34: invokevirtual 64	net/a/a/a/c:b	(Ljava/lang/Throwable;)V
    //   37: aload_2
    //   38: invokevirtual 59	java/io/ByteArrayOutputStream:close	()V
    //   41: ldc 66
    //   43: areturn
    //   44: astore_1
    //   45: ldc 66
    //   47: areturn
    //   48: astore_1
    //   49: getstatic 21	com/evernote/android/job/a/a/b:a	Lnet/a/a/a/c;
    //   52: aload_1
    //   53: invokevirtual 64	net/a/a/a/c:b	(Ljava/lang/Throwable;)V
    //   56: aload_2
    //   57: invokevirtual 59	java/io/ByteArrayOutputStream:close	()V
    //   60: ldc 66
    //   62: areturn
    //   63: astore_1
    //   64: ldc 66
    //   66: areturn
    //   67: astore_1
    //   68: aload_2
    //   69: invokevirtual 59	java/io/ByteArrayOutputStream:close	()V
    //   72: aload_1
    //   73: athrow
    //   74: astore_1
    //   75: goto -45 -> 30
    //   78: astore_2
    //   79: aload_1
    //   80: areturn
    //   81: astore_2
    //   82: goto -10 -> 72
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	85	0	this	b
    //   22	6	1	str	String
    //   29	5	1	localXmlPullParserException	org.xmlpull.v1.XmlPullParserException
    //   44	1	1	localIOException1	java.io.IOException
    //   48	5	1	localError	Error
    //   63	1	1	localIOException2	java.io.IOException
    //   67	6	1	localObject	Object
    //   74	6	1	localIOException3	java.io.IOException
    //   7	62	2	localByteArrayOutputStream	java.io.ByteArrayOutputStream
    //   78	1	2	localIOException4	java.io.IOException
    //   81	1	2	localIOException5	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   8	23	29	org/xmlpull/v1/XmlPullParserException
    //   37	41	44	java/io/IOException
    //   8	23	48	java/lang/Error
    //   56	60	63	java/io/IOException
    //   8	23	67	finally
    //   30	37	67	finally
    //   49	56	67	finally
    //   8	23	74	java/io/IOException
    //   23	27	78	java/io/IOException
    //   68	72	81	java/io/IOException
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/a/a/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */