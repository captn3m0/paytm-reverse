package com.evernote.android.job.a;

import android.text.TextUtils;
import java.util.Locale;

public final class e
{
  public static int a(int paramInt, String paramString)
  {
    if (paramInt < 0) {
      throw new IllegalArgumentException(paramString);
    }
    return paramInt;
  }
  
  public static long a(long paramLong1, long paramLong2, long paramLong3, String paramString)
  {
    if (paramLong1 < paramLong2) {
      throw new IllegalArgumentException(String.format(Locale.US, "%s is out of range of [%d, %d] (too low)", new Object[] { paramString, Long.valueOf(paramLong2), Long.valueOf(paramLong3) }));
    }
    if (paramLong1 > paramLong3) {
      throw new IllegalArgumentException(String.format(Locale.US, "%s is out of range of [%d, %d] (too high)", new Object[] { paramString, Long.valueOf(paramLong2), Long.valueOf(paramLong3) }));
    }
    return paramLong1;
  }
  
  public static long a(long paramLong, String paramString)
  {
    if (paramLong < 0L) {
      throw new IllegalArgumentException(paramString);
    }
    return paramLong;
  }
  
  public static <T extends CharSequence> T a(T paramT)
  {
    if (TextUtils.isEmpty(paramT)) {
      throw new IllegalArgumentException();
    }
    return paramT;
  }
  
  public static <T> T a(T paramT)
  {
    if (paramT == null) {
      throw new NullPointerException();
    }
    return paramT;
  }
  
  public static <T> T a(T paramT, Object paramObject)
  {
    if (paramT == null) {
      throw new NullPointerException(String.valueOf(paramObject));
    }
    return paramT;
  }
  
  public static long b(long paramLong, String paramString)
  {
    if (paramLong <= 0L) {
      throw new IllegalArgumentException(paramString);
    }
    return paramLong;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/a/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */