package com.evernote.android.job;

import com.evernote.android.job.a.d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.a.a.a.c;

class b
{
  private static final c a = new d("JobCreatorHolder");
  private final List<JobCreator> b = new ArrayList();
  private final Object c = new Object();
  
  public a a(String paramString)
  {
    Object localObject1 = null;
    Object localObject2 = null;
    synchronized (this.c)
    {
      int i = this.b.size();
      if (i == 0)
      {
        a.b("no JobCreator added");
        return null;
      }
      if (i == 1)
      {
        localObject2 = (JobCreator)this.b.get(0);
        if (localObject2 != null) {
          return ((JobCreator)localObject2).a(paramString);
        }
      }
      else
      {
        localObject1 = new ArrayList(this.b);
      }
    }
    if (localObject1 != null)
    {
      localObject1 = ((ArrayList)localObject1).iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = ((JobCreator)((Iterator)localObject1).next()).a(paramString);
        if (localObject2 != null) {
          return (a)localObject2;
        }
      }
    }
    return null;
  }
  
  public void a(JobCreator paramJobCreator)
  {
    synchronized (this.c)
    {
      this.b.add(paramJobCreator);
      return;
    }
  }
  
  public boolean a()
  {
    synchronized (this.c)
    {
      boolean bool = this.b.isEmpty();
      return bool;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */