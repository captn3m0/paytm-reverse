package com.evernote.android.job;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import com.evernote.android.job.a.d;
import java.lang.ref.WeakReference;
import net.a.a.a.c;

public abstract class a
{
  private static final c a = new d("Job");
  private a b;
  private WeakReference<Context> c;
  private Context d;
  private boolean e;
  private long f = -1L;
  private b g = b.b;
  
  private boolean k()
  {
    if (!e().d().l()) {}
    do
    {
      return true;
      if (!b())
      {
        a.b("Job requires charging, reschedule");
        return false;
      }
      if (!c())
      {
        a.b("Job requires device to be idle, reschedule");
        return false;
      }
    } while (d());
    a.c("Job requires network to be %s, but was %s", new Object[] { e().d().o(), com.evernote.android.job.a.a.c(f()) });
    return false;
  }
  
  final b a()
  {
    for (;;)
    {
      try
      {
        b localb1;
        if (k())
        {
          this.g = a(e());
          localb1 = this.g;
          return localb1;
        }
        if (e().c())
        {
          localb1 = b.b;
          this.g = localb1;
        }
        else
        {
          b localb2 = b.c;
        }
      }
      finally
      {
        this.f = System.currentTimeMillis();
      }
    }
  }
  
  @NonNull
  @WorkerThread
  protected abstract b a(a parama);
  
  final a a(Context paramContext)
  {
    this.c = new WeakReference(paramContext);
    this.d = paramContext.getApplicationContext();
    return this;
  }
  
  final a a(f paramf)
  {
    this.b = new a(paramf, null);
    return this;
  }
  
  @WorkerThread
  protected void a(int paramInt) {}
  
  protected boolean b()
  {
    return (!e().d().m()) || (com.evernote.android.job.a.a.a(f()));
  }
  
  protected boolean c()
  {
    return (!e().d().n()) || (com.evernote.android.job.a.a.b(f()));
  }
  
  public final void cancel()
  {
    if (!h()) {
      this.e = true;
    }
  }
  
  protected boolean d()
  {
    boolean bool = false;
    f.c localc1 = e().d().o();
    if (localc1 == f.c.a) {}
    f.c localc2;
    do
    {
      do
      {
        return true;
        localc2 = com.evernote.android.job.a.a.c(f());
        switch (1.a[localc1.ordinal()])
        {
        default: 
          throw new IllegalStateException("not implemented");
        }
      } while (localc2 != f.c.a);
      return false;
      if ((localc2 == f.c.d) || (localc2 == f.c.c)) {
        bool = true;
      }
      return bool;
    } while (localc2 == f.c.c);
    return false;
  }
  
  @NonNull
  protected final a e()
  {
    return this.b;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    paramObject = (a)paramObject;
    return this.b.equals(((a)paramObject).b);
  }
  
  @NonNull
  protected final Context f()
  {
    Context localContext2 = (Context)this.c.get();
    Context localContext1 = localContext2;
    if (localContext2 == null) {
      localContext1 = this.d;
    }
    return localContext1;
  }
  
  protected final boolean g()
  {
    return this.e;
  }
  
  public final boolean h()
  {
    return this.f > 0L;
  }
  
  public int hashCode()
  {
    return this.b.hashCode();
  }
  
  final long i()
  {
    return this.f;
  }
  
  final b j()
  {
    return this.g;
  }
  
  public String toString()
  {
    return "job{id=" + this.b.a() + ", finished=" + h() + ", result=" + this.g + ", canceled=" + this.e + ", periodic=" + this.b.c() + ", class=" + getClass().getSimpleName() + ", tag=" + this.b.b() + '}';
  }
  
  protected static final class a
  {
    private final f a;
    
    private a(@NonNull f paramf)
    {
      this.a = paramf;
    }
    
    public int a()
    {
      return this.a.c();
    }
    
    public String b()
    {
      return this.a.d();
    }
    
    public boolean c()
    {
      return this.a.i();
    }
    
    f d()
    {
      return this.a;
    }
    
    public boolean equals(Object paramObject)
    {
      if (this == paramObject) {
        return true;
      }
      if ((paramObject == null) || (getClass() != paramObject.getClass())) {
        return false;
      }
      paramObject = (a)paramObject;
      return this.a.equals(((a)paramObject).a);
    }
    
    public int hashCode()
    {
      return this.a.hashCode();
    }
  }
  
  public static enum b
  {
    private b() {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */