package com.evernote.android.job.v14;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.support.annotation.Nullable;
import com.evernote.android.job.a.d;
import com.evernote.android.job.e;
import com.evernote.android.job.e.a;
import net.a.a.a.c;

public class a
  implements e
{
  protected final Context a;
  protected final c b;
  private AlarmManager c;
  
  public a(Context paramContext)
  {
    this(paramContext, "JobProxy14");
  }
  
  protected a(Context paramContext, String paramString)
  {
    this.a = paramContext;
    this.b = new d(paramString);
  }
  
  private void f(com.evernote.android.job.f paramf)
  {
    this.b.a("Scheduled alarm, %s, delay %s, exact %b", new Object[] { paramf, com.evernote.android.job.a.f.a(e.a.c(paramf)), Boolean.valueOf(paramf.r()) });
  }
  
  protected int a(boolean paramBoolean)
  {
    int i = 134217728;
    if (!paramBoolean) {
      i = 0x8000000 | 0x40000000;
    }
    return i;
  }
  
  @Nullable
  protected AlarmManager a()
  {
    if (this.c == null) {
      this.c = ((AlarmManager)this.a.getSystemService("alarm"));
    }
    if (this.c == null) {
      this.b.c("AlarmManager is null");
    }
    return this.c;
  }
  
  protected PendingIntent a(int paramInt1, int paramInt2)
  {
    Object localObject = PlatformAlarmReceiver.a(this.a, paramInt1);
    try
    {
      localObject = PendingIntent.getBroadcast(this.a, paramInt1, (Intent)localObject, paramInt2);
      return (PendingIntent)localObject;
    }
    catch (Exception localException)
    {
      this.b.b(localException);
    }
    return null;
  }
  
  protected PendingIntent a(com.evernote.android.job.f paramf, int paramInt)
  {
    return a(paramf.c(), paramInt);
  }
  
  protected PendingIntent a(com.evernote.android.job.f paramf, boolean paramBoolean)
  {
    return a(paramf, a(paramBoolean));
  }
  
  public void a(com.evernote.android.job.f paramf)
  {
    PendingIntent localPendingIntent = a(paramf, false);
    AlarmManager localAlarmManager = a();
    if (localAlarmManager == null) {
      return;
    }
    try
    {
      if (paramf.r())
      {
        b(paramf, localAlarmManager, localPendingIntent);
        return;
      }
    }
    catch (Exception paramf)
    {
      this.b.b(paramf);
      return;
    }
    a(paramf, localAlarmManager, localPendingIntent);
  }
  
  protected void a(com.evernote.android.job.f paramf, AlarmManager paramAlarmManager, PendingIntent paramPendingIntent)
  {
    paramAlarmManager.set(1, e(paramf), paramPendingIntent);
    f(paramf);
  }
  
  public void b(com.evernote.android.job.f paramf)
  {
    PendingIntent localPendingIntent = a(paramf, true);
    AlarmManager localAlarmManager = a();
    if (localAlarmManager != null) {
      localAlarmManager.setRepeating(0, System.currentTimeMillis() + paramf.j(), paramf.j(), localPendingIntent);
    }
    this.b.a("Scheduled repeating alarm, %s, interval %s", new Object[] { paramf, com.evernote.android.job.a.f.a(paramf.j()) });
  }
  
  protected void b(com.evernote.android.job.f paramf, AlarmManager paramAlarmManager, PendingIntent paramPendingIntent)
  {
    long l = e(paramf);
    if (Build.VERSION.SDK_INT >= 23) {
      paramAlarmManager.setExactAndAllowWhileIdle(0, l, paramPendingIntent);
    }
    for (;;)
    {
      f(paramf);
      return;
      if (Build.VERSION.SDK_INT >= 19) {
        paramAlarmManager.setExact(0, l, paramPendingIntent);
      } else {
        paramAlarmManager.set(0, l, paramPendingIntent);
      }
    }
  }
  
  public void c(com.evernote.android.job.f paramf)
  {
    PendingIntent localPendingIntent = a(paramf, false);
    AlarmManager localAlarmManager = a();
    if (localAlarmManager == null) {
      return;
    }
    try
    {
      c(paramf, localAlarmManager, localPendingIntent);
      return;
    }
    catch (Exception paramf)
    {
      this.b.b(paramf);
    }
  }
  
  protected void c(com.evernote.android.job.f paramf, AlarmManager paramAlarmManager, PendingIntent paramPendingIntent)
  {
    paramAlarmManager.set(1, System.currentTimeMillis() + e.a.f(paramf), paramPendingIntent);
    this.b.a("Scheduled repeating alarm (flex support), %s, interval %s, flex %s", new Object[] { paramf, com.evernote.android.job.a.f.a(paramf.j()), com.evernote.android.job.a.f.a(paramf.k()) });
  }
  
  public void cancel(int paramInt)
  {
    AlarmManager localAlarmManager = a();
    if (localAlarmManager != null) {}
    try
    {
      localAlarmManager.cancel(a(paramInt, a(true)));
      localAlarmManager.cancel(a(paramInt, a(false)));
      return;
    }
    catch (Exception localException)
    {
      this.b.b(localException);
    }
  }
  
  public boolean d(com.evernote.android.job.f paramf)
  {
    return a(paramf, 536870912) != null;
  }
  
  protected long e(com.evernote.android.job.f paramf)
  {
    return System.currentTimeMillis() + e.a.c(paramf);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/v14/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */