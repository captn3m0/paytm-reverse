package com.evernote.android.job.v14;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import com.evernote.android.job.a.d;
import com.evernote.android.job.a.f;
import net.a.a.a.c;

public class PlatformAlarmReceiver
  extends WakefulBroadcastReceiver
{
  private static final c a = new d("PlatformAlarmReceiver");
  
  static Intent a(Context paramContext, int paramInt)
  {
    return new Intent(paramContext, PlatformAlarmReceiver.class).putExtra("EXTRA_JOB_ID", paramInt);
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if ((paramIntent == null) || (!paramIntent.hasExtra("EXTRA_JOB_ID"))) {
      return;
    }
    paramIntent = PlatformAlarmService.a(paramContext, paramIntent.getIntExtra("EXTRA_JOB_ID", -1));
    if (f.b(paramContext)) {
      try
      {
        a(paramContext, paramIntent);
        return;
      }
      catch (Exception paramContext)
      {
        a.b(paramContext);
        return;
      }
    }
    paramContext.startService(paramIntent);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/v14/PlatformAlarmReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */