package com.evernote.android.job.v14;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import com.evernote.android.job.e.a;
import com.evernote.android.job.f;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import net.a.a.a.a;

public class PlatformAlarmService
  extends IntentService
{
  private static final ExecutorService a = ;
  
  public PlatformAlarmService()
  {
    super(PlatformAlarmService.class.getSimpleName());
  }
  
  static Intent a(Context paramContext, int paramInt)
  {
    paramContext = new Intent(paramContext, PlatformAlarmService.class);
    paramContext.putExtra("EXTRA_JOB_ID", paramInt);
    return paramContext;
  }
  
  protected void onHandleIntent(final Intent paramIntent)
  {
    if (paramIntent == null) {
      a.a("Delivered intent is null");
    }
    final e.a locala;
    final f localf;
    do
    {
      return;
      locala = new e.a(this, paramIntent.getIntExtra("EXTRA_JOB_ID", -1));
      localf = locala.a(true);
    } while (localf == null);
    a.execute(new Runnable()
    {
      public void run()
      {
        locala.g(localf);
        try
        {
          PlatformAlarmReceiver.a(paramIntent);
          return;
        }
        catch (Exception localException) {}
      }
    });
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/v14/PlatformAlarmService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */