package com.evernote.android.job.b;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import com.evernote.android.job.e.a;
import net.a.a.a.c;

@TargetApi(19)
public class a
  extends com.evernote.android.job.v14.a
{
  public a(Context paramContext)
  {
    super(paramContext, "JobProxy19");
  }
  
  protected void a(com.evernote.android.job.f paramf, AlarmManager paramAlarmManager, PendingIntent paramPendingIntent)
  {
    paramAlarmManager.setWindow(1, System.currentTimeMillis() + e.a.a(paramf), e.a.b(paramf) - e.a.a(paramf), paramPendingIntent);
    this.b.a("Schedule alarm, %s, start %s, end %s", new Object[] { paramf, com.evernote.android.job.a.f.a(e.a.a(paramf)), com.evernote.android.job.a.f.a(e.a.b(paramf)) });
  }
  
  protected void c(com.evernote.android.job.f paramf, AlarmManager paramAlarmManager, PendingIntent paramPendingIntent)
  {
    paramAlarmManager.setWindow(1, System.currentTimeMillis() + e.a.d(paramf), e.a.e(paramf) - e.a.d(paramf), paramPendingIntent);
    this.b.a("Scheduled repeating alarm (flex support), %s, start %s, end %s, flex %s", new Object[] { paramf, com.evernote.android.job.a.f.a(e.a.d(paramf)), com.evernote.android.job.a.f.a(e.a.e(paramf)), com.evernote.android.job.a.f.a(paramf.k()) });
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/b/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */