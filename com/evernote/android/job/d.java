package com.evernote.android.job;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build.VERSION;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public final class d
{
  private static final Package a = d.class.getPackage();
  private static final net.a.a.a.c b = new com.evernote.android.job.a.d("JobManager");
  @SuppressLint({"StaticFieldLeak"})
  private static volatile d c;
  private final Context d;
  private final b e;
  private final g f;
  private final c g;
  private final a h;
  private com.evernote.android.job.a.c i;
  
  private d(Context paramContext)
  {
    this.d = paramContext;
    this.e = new b();
    this.f = new g(paramContext);
    this.g = new c();
    this.h = new a(null);
    a(com.evernote.android.job.a.c.a(this.d, this.h.a()));
    j();
  }
  
  public static d a()
  {
    if (c == null) {
      try
      {
        if (c == null) {
          throw new IllegalStateException("You need to call create() at least once to create the singleton");
        }
      }
      finally {}
    }
    return c;
  }
  
  public static d a(@NonNull Context paramContext)
  {
    if (c == null) {}
    try
    {
      if (c == null)
      {
        com.evernote.android.job.a.e.a(paramContext, "Context cannot be null");
        if (a != null) {
          net.a.a.a.b.a(a.getName(), new com.evernote.android.job.a.d());
        }
        Context localContext = paramContext;
        if (paramContext.getApplicationContext() != null) {
          localContext = paramContext.getApplicationContext();
        }
        c = new d(localContext);
        if (!com.evernote.android.job.a.f.b(localContext)) {
          net.a.a.a.a.b("No wake lock permission");
        }
        if (!com.evernote.android.job.a.f.a(localContext)) {
          net.a.a.a.a.b("No boot permission");
        }
        b(localContext);
      }
      return c;
    }
    finally {}
  }
  
  private boolean a(@Nullable a parama)
  {
    if ((parama != null) && (!parama.h()) && (!parama.g()))
    {
      b.b("Cancel running %s", new Object[] { parama });
      parama.cancel();
      return true;
    }
    return false;
  }
  
  private e b(com.evernote.android.job.a.c paramc)
  {
    return paramc.c(this.d);
  }
  
  private static void b(@NonNull Context paramContext)
  {
    Object localObject1 = new Intent("com.evernote.android.job.ADD_JOB_CREATOR");
    Object localObject2 = paramContext.getPackageManager().queryBroadcastReceivers((Intent)localObject1, 0);
    localObject1 = paramContext.getPackageName();
    localObject2 = ((List)localObject2).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      ActivityInfo localActivityInfo = ((ResolveInfo)((Iterator)localObject2).next()).activityInfo;
      if ((localActivityInfo != null) && (!localActivityInfo.exported) && (((String)localObject1).equals(localActivityInfo.packageName)) && (!TextUtils.isEmpty(localActivityInfo.name))) {
        try
        {
          ((JobCreator.AddJobCreatorReceiver)Class.forName(localActivityInfo.name).newInstance()).a(paramContext, c);
        }
        catch (Exception localException) {}
      }
    }
  }
  
  private boolean b(@Nullable f paramf)
  {
    if (paramf != null)
    {
      b.b("Found pending job %s, canceling", new Object[] { paramf });
      c(paramf).cancel(paramf.c());
      e().b(paramf);
      return true;
    }
    return false;
  }
  
  private int c(@Nullable String paramString)
  {
    int j = 0;
    Iterator localIterator = this.f.a(paramString, true).iterator();
    while (localIterator.hasNext()) {
      if (b((f)localIterator.next())) {
        j += 1;
      }
    }
    if (TextUtils.isEmpty(paramString)) {}
    for (paramString = c();; paramString = a(paramString))
    {
      paramString = paramString.iterator();
      while (paramString.hasNext()) {
        if (a((a)paramString.next())) {
          j += 1;
        }
      }
    }
    return j;
  }
  
  private e c(f paramf)
  {
    return b(paramf.t());
  }
  
  private void j()
  {
    new Thread()
    {
      public void run()
      {
        int i;
        for (;;)
        {
          try
          {
            SystemClock.sleep(10000L);
            Set localSet = d.a(d.this).a(null, true);
            int j = 0;
            Iterator localIterator = localSet.iterator();
            if (localIterator.hasNext())
            {
              f localf = (f)localIterator.next();
              if (localf.v())
              {
                if (d.this.a(localf.c()) == null)
                {
                  i = 1;
                  if (i == 0) {
                    continue;
                  }
                  localf.y().a().x();
                  j += 1;
                }
              }
              else
              {
                if (d.a(d.this, localf).d(localf)) {
                  break label180;
                }
                i = 1;
                break;
              }
            }
            else
            {
              d.i().a("Reschedule %d jobs of %d jobs", new Object[] { Integer.valueOf(j), Integer.valueOf(localSet.size()) });
              return;
            }
          }
          finally
          {
            h.a(this.a);
          }
          i = 0;
        }
        for (;;)
        {
          break;
          label180:
          i = 0;
        }
      }
    }.start();
  }
  
  public a a(int paramInt)
  {
    return this.g.a(paramInt);
  }
  
  f a(int paramInt, boolean paramBoolean)
  {
    f localf2 = this.f.a(paramInt);
    f localf1 = localf2;
    if (!paramBoolean)
    {
      localf1 = localf2;
      if (localf2 != null)
      {
        localf1 = localf2;
        if (localf2.v()) {
          localf1 = null;
        }
      }
    }
    return localf1;
  }
  
  @NonNull
  public Set<a> a(@NonNull String paramString)
  {
    return this.g.a(paramString);
  }
  
  public void a(JobCreator paramJobCreator)
  {
    this.e.a(paramJobCreator);
  }
  
  protected void a(com.evernote.android.job.a.c paramc)
  {
    this.i = paramc;
  }
  
  public void a(@NonNull f paramf)
  {
    if (this.e.a()) {
      b.b("you haven't registered a JobCreator with addJobCreator(), it's likely that your job never will be executed");
    }
    if (paramf.q()) {
      b(paramf.d());
    }
    e.a.a(this.d, paramf.c());
    Object localObject = paramf.t();
    boolean bool2 = paramf.i();
    if ((bool2) && (((com.evernote.android.job.a.c)localObject).b()) && (paramf.k() < paramf.j())) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      if ((localObject == com.evernote.android.job.a.c.e) && (!this.h.a())) {
        b.b("GCM API disabled, but used nonetheless");
      }
      paramf.a(System.currentTimeMillis());
      paramf.a(bool1);
      this.f.a(paramf);
      localObject = b((com.evernote.android.job.a.c)localObject);
      if (!bool2) {
        break label169;
      }
      if (!bool1) {
        break;
      }
      ((e)localObject).c(paramf);
      return;
    }
    ((e)localObject).b(paramf);
    return;
    label169:
    ((e)localObject).a(paramf);
  }
  
  public int b(@NonNull String paramString)
  {
    return c(paramString);
  }
  
  public a b()
  {
    return this.h;
  }
  
  @NonNull
  public Set<a> c()
  {
    return this.g.a();
  }
  
  public boolean cancel(int paramInt)
  {
    boolean bool1 = b(a(paramInt, true));
    boolean bool2 = a(a(paramInt));
    e.a.a(this.d, paramInt);
    return bool1 | bool2;
  }
  
  public com.evernote.android.job.a.c d()
  {
    return this.i;
  }
  
  g e()
  {
    return this.f;
  }
  
  c f()
  {
    return this.g;
  }
  
  b g()
  {
    return this.e;
  }
  
  Context h()
  {
    return this.d;
  }
  
  public final class a
  {
    private boolean b = true;
    private boolean c = true;
    private boolean d = false;
    
    private a() {}
    
    public boolean a()
    {
      return this.c;
    }
    
    public boolean b()
    {
      return (this.d) && (Build.VERSION.SDK_INT < 24);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */