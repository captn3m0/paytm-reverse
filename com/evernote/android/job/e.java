package com.evernote.android.job;

import android.app.Service;
import android.content.Context;
import android.support.annotation.NonNull;

public abstract interface e
{
  public abstract void a(f paramf);
  
  public abstract void b(f paramf);
  
  public abstract void c(f paramf);
  
  public abstract void cancel(int paramInt);
  
  public abstract boolean d(f paramf);
  
  public static final class a
  {
    private final Context a;
    private final int b;
    private final net.a.a.a.c c;
    private final d d;
    
    public a(@NonNull Service paramService, int paramInt)
    {
      this(paramService, paramService.getClass().getSimpleName(), paramInt);
    }
    
    a(@NonNull Context paramContext, String paramString, int paramInt)
    {
      this.a = paramContext;
      this.b = paramInt;
      this.c = new com.evernote.android.job.a.d(paramString);
      this.d = d.a(paramContext);
    }
    
    private static long a(long paramLong1, long paramLong2)
    {
      int j = 1;
      long l = paramLong1 + paramLong2;
      int i;
      if ((paramLong1 ^ paramLong2) < 0L)
      {
        i = 1;
        if ((paramLong1 ^ l) < 0L) {
          break label45;
        }
      }
      for (;;)
      {
        return a(l, j | i);
        i = 0;
        break;
        label45:
        j = 0;
      }
    }
    
    private static long a(long paramLong, boolean paramBoolean)
    {
      if (paramBoolean) {
        return paramLong;
      }
      return Long.MAX_VALUE;
    }
    
    public static long a(f paramf)
    {
      return a(paramf.e(), paramf.s());
    }
    
    public static void a(Context paramContext, int paramInt)
    {
      com.evernote.android.job.a.c[] arrayOfc = com.evernote.android.job.a.c.values();
      int j = arrayOfc.length;
      int i = 0;
      for (;;)
      {
        com.evernote.android.job.a.c localc;
        if (i < j)
        {
          localc = arrayOfc[i];
          if (!localc.a(paramContext)) {}
        }
        try
        {
          localc.c(paramContext).cancel(paramInt);
          i += 1;
          continue;
          return;
        }
        catch (Exception localException)
        {
          for (;;) {}
        }
      }
    }
    
    public static long b(f paramf)
    {
      return a(paramf.f(), paramf.s());
    }
    
    private void b(boolean paramBoolean)
    {
      if (paramBoolean) {
        a(this.a, this.b);
      }
    }
    
    public static long c(f paramf)
    {
      return a(a(paramf), (b(paramf) - a(paramf)) / 2L);
    }
    
    public static long d(f paramf)
    {
      return Math.max(1L, paramf.j() - paramf.k());
    }
    
    public static long e(f paramf)
    {
      return paramf.j();
    }
    
    public static long f(f paramf)
    {
      return a(d(paramf), (e(paramf) - d(paramf)) / 2L);
    }
    
    public f a(boolean paramBoolean)
    {
      f localf = this.d.a(this.b, true);
      Object localObject = this.d.a(this.b);
      int i;
      if ((localf != null) && (localf.i()))
      {
        i = 1;
        if ((localObject == null) || (((a)localObject).h())) {
          break label89;
        }
        this.c.a("Job %d is already running, %s", new Object[] { Integer.valueOf(this.b), localf });
        localObject = null;
      }
      label89:
      do
      {
        return (f)localObject;
        i = 0;
        break;
        if ((localObject != null) && (i == 0))
        {
          this.c.a("Job %d already finished, %s", new Object[] { Integer.valueOf(this.b), localf });
          b(paramBoolean);
          return null;
        }
        if ((localObject != null) && (System.currentTimeMillis() - ((a)localObject).i() < 2000L))
        {
          this.c.a("Job %d is periodic and just finished, %s", new Object[] { Integer.valueOf(this.b), localf });
          return null;
        }
        if ((localf != null) && (localf.v()))
        {
          this.c.a("Request %d is transient, %s", new Object[] { Integer.valueOf(this.b), localf });
          return null;
        }
        localObject = localf;
      } while (localf != null);
      this.c.a("Request for ID %d was null", new Object[] { Integer.valueOf(this.b) });
      b(paramBoolean);
      return null;
    }
    
    /* Error */
    @NonNull
    public a.b g(@NonNull f paramf)
    {
      // Byte code:
      //   0: invokestatic 155	java/lang/System:currentTimeMillis	()J
      //   3: lstore_2
      //   4: aload_1
      //   5: invokevirtual 177	com/evernote/android/job/f:u	()J
      //   8: lstore 4
      //   10: aload_1
      //   11: invokevirtual 128	com/evernote/android/job/f:i	()Z
      //   14: ifeq +211 -> 225
      //   17: getstatic 183	java/util/Locale:US	Ljava/util/Locale;
      //   20: ldc -71
      //   22: iconst_2
      //   23: anewarray 4	java/lang/Object
      //   26: dup
      //   27: iconst_0
      //   28: aload_1
      //   29: invokevirtual 105	com/evernote/android/job/f:j	()J
      //   32: invokestatic 190	com/evernote/android/job/a/f:a	(J)Ljava/lang/String;
      //   35: aastore
      //   36: dup
      //   37: iconst_1
      //   38: aload_1
      //   39: invokevirtual 108	com/evernote/android/job/f:k	()J
      //   42: invokestatic 190	com/evernote/android/job/a/f:a	(J)Ljava/lang/String;
      //   45: aastore
      //   46: invokestatic 196	java/lang/String:format	(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
      //   49: astore 6
      //   51: invokestatic 202	android/os/Looper:myLooper	()Landroid/os/Looper;
      //   54: invokestatic 205	android/os/Looper:getMainLooper	()Landroid/os/Looper;
      //   57: if_acmpne +12 -> 69
      //   60: aload_0
      //   61: getfield 46	com/evernote/android/job/e$a:c	Lnet/a/a/a/c;
      //   64: ldc -49
      //   66: invokevirtual 209	net/a/a/a/c:b	(Ljava/lang/String;)V
      //   69: aload_0
      //   70: getfield 46	com/evernote/android/job/e$a:c	Lnet/a/a/a/c;
      //   73: ldc -45
      //   75: iconst_3
      //   76: anewarray 4	java/lang/Object
      //   79: dup
      //   80: iconst_0
      //   81: aload_1
      //   82: aastore
      //   83: dup
      //   84: iconst_1
      //   85: lload_2
      //   86: lload 4
      //   88: lsub
      //   89: invokestatic 190	com/evernote/android/job/a/f:a	(J)Ljava/lang/String;
      //   92: aastore
      //   93: dup
      //   94: iconst_2
      //   95: aload 6
      //   97: aastore
      //   98: invokevirtual 146	net/a/a/a/c:a	(Ljava/lang/String;[Ljava/lang/Object;)V
      //   101: aload_0
      //   102: getfield 53	com/evernote/android/job/e$a:d	Lcom/evernote/android/job/d;
      //   105: invokevirtual 214	com/evernote/android/job/d:f	()Lcom/evernote/android/job/c;
      //   108: astore 9
      //   110: aconst_null
      //   111: astore 7
      //   113: aconst_null
      //   114: astore 6
      //   116: aload_0
      //   117: getfield 53	com/evernote/android/job/e$a:d	Lcom/evernote/android/job/d;
      //   120: invokevirtual 217	com/evernote/android/job/d:g	()Lcom/evernote/android/job/b;
      //   123: aload_1
      //   124: invokevirtual 219	com/evernote/android/job/f:d	()Ljava/lang/String;
      //   127: invokevirtual 224	com/evernote/android/job/b:a	(Ljava/lang/String;)Lcom/evernote/android/job/a;
      //   130: astore 8
      //   132: aload 8
      //   134: astore 6
      //   136: aload 8
      //   138: astore 7
      //   140: aload_1
      //   141: invokevirtual 128	com/evernote/android/job/f:i	()Z
      //   144: ifne +16 -> 160
      //   147: aload 8
      //   149: astore 6
      //   151: aload 8
      //   153: astore 7
      //   155: aload_1
      //   156: iconst_1
      //   157: invokevirtual 225	com/evernote/android/job/f:b	(Z)V
      //   160: aload 8
      //   162: astore 6
      //   164: aload 8
      //   166: astore 7
      //   168: aload 9
      //   170: aload_0
      //   171: getfield 37	com/evernote/android/job/e$a:a	Landroid/content/Context;
      //   174: aload_1
      //   175: aload 8
      //   177: invokevirtual 230	com/evernote/android/job/c:a	(Landroid/content/Context;Lcom/evernote/android/job/f;Lcom/evernote/android/job/a;)Ljava/util/concurrent/Future;
      //   180: astore 9
      //   182: aload 9
      //   184: ifnonnull +151 -> 335
      //   187: aload 8
      //   189: astore 6
      //   191: aload 8
      //   193: astore 7
      //   195: getstatic 235	com/evernote/android/job/a$b:b	Lcom/evernote/android/job/a$b;
      //   198: astore 8
      //   200: aload_1
      //   201: invokevirtual 128	com/evernote/android/job/f:i	()Z
      //   204: ifne +99 -> 303
      //   207: aload_0
      //   208: getfield 53	com/evernote/android/job/e$a:d	Lcom/evernote/android/job/d;
      //   211: invokevirtual 238	com/evernote/android/job/d:e	()Lcom/evernote/android/job/g;
      //   214: aload_1
      //   215: invokevirtual 243	com/evernote/android/job/g:b	(Lcom/evernote/android/job/f;)V
      //   218: aload 8
      //   220: astore 6
      //   222: aload 6
      //   224: areturn
      //   225: aload_1
      //   226: invokevirtual 247	com/evernote/android/job/f:t	()Lcom/evernote/android/job/a/c;
      //   229: invokevirtual 249	com/evernote/android/job/a/c:a	()Z
      //   232: ifeq +40 -> 272
      //   235: getstatic 183	java/util/Locale:US	Ljava/util/Locale;
      //   238: ldc -5
      //   240: iconst_2
      //   241: anewarray 4	java/lang/Object
      //   244: dup
      //   245: iconst_0
      //   246: aload_1
      //   247: invokestatic 98	com/evernote/android/job/e$a:a	(Lcom/evernote/android/job/f;)J
      //   250: invokestatic 190	com/evernote/android/job/a/f:a	(J)Ljava/lang/String;
      //   253: aastore
      //   254: dup
      //   255: iconst_1
      //   256: aload_1
      //   257: invokestatic 100	com/evernote/android/job/e$a:b	(Lcom/evernote/android/job/f;)J
      //   260: invokestatic 190	com/evernote/android/job/a/f:a	(J)Ljava/lang/String;
      //   263: aastore
      //   264: invokestatic 196	java/lang/String:format	(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
      //   267: astore 6
      //   269: goto -218 -> 51
      //   272: new 253	java/lang/StringBuilder
      //   275: dup
      //   276: invokespecial 254	java/lang/StringBuilder:<init>	()V
      //   279: ldc_w 256
      //   282: invokevirtual 260	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   285: aload_1
      //   286: invokestatic 262	com/evernote/android/job/e$a:c	(Lcom/evernote/android/job/f;)J
      //   289: invokestatic 190	com/evernote/android/job/a/f:a	(J)Ljava/lang/String;
      //   292: invokevirtual 260	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   295: invokevirtual 265	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   298: astore 6
      //   300: goto -249 -> 51
      //   303: aload 8
      //   305: astore 6
      //   307: aload_1
      //   308: invokevirtual 268	com/evernote/android/job/f:w	()Z
      //   311: ifeq -89 -> 222
      //   314: aload_0
      //   315: getfield 53	com/evernote/android/job/e$a:d	Lcom/evernote/android/job/d;
      //   318: invokevirtual 238	com/evernote/android/job/d:e	()Lcom/evernote/android/job/g;
      //   321: aload_1
      //   322: invokevirtual 243	com/evernote/android/job/g:b	(Lcom/evernote/android/job/f;)V
      //   325: aload_1
      //   326: iconst_0
      //   327: iconst_0
      //   328: invokevirtual 271	com/evernote/android/job/f:a	(ZZ)I
      //   331: pop
      //   332: aload 8
      //   334: areturn
      //   335: aload 8
      //   337: astore 6
      //   339: aload 8
      //   341: astore 7
      //   343: aload 9
      //   345: invokeinterface 277 1 0
      //   350: checkcast 232	com/evernote/android/job/a$b
      //   353: astore 9
      //   355: aload 8
      //   357: astore 6
      //   359: aload 8
      //   361: astore 7
      //   363: aload_0
      //   364: getfield 46	com/evernote/android/job/e$a:c	Lnet/a/a/a/c;
      //   367: ldc_w 279
      //   370: iconst_2
      //   371: anewarray 4	java/lang/Object
      //   374: dup
      //   375: iconst_0
      //   376: aload_1
      //   377: aastore
      //   378: dup
      //   379: iconst_1
      //   380: aload 9
      //   382: aastore
      //   383: invokevirtual 146	net/a/a/a/c:a	(Ljava/lang/String;[Ljava/lang/Object;)V
      //   386: aload_1
      //   387: invokevirtual 128	com/evernote/android/job/f:i	()Z
      //   390: ifne +17 -> 407
      //   393: aload_0
      //   394: getfield 53	com/evernote/android/job/e$a:d	Lcom/evernote/android/job/d;
      //   397: invokevirtual 238	com/evernote/android/job/d:e	()Lcom/evernote/android/job/g;
      //   400: aload_1
      //   401: invokevirtual 243	com/evernote/android/job/g:b	(Lcom/evernote/android/job/f;)V
      //   404: aload 9
      //   406: areturn
      //   407: aload 9
      //   409: astore 6
      //   411: aload_1
      //   412: invokevirtual 268	com/evernote/android/job/f:w	()Z
      //   415: ifeq -193 -> 222
      //   418: aload_0
      //   419: getfield 53	com/evernote/android/job/e$a:d	Lcom/evernote/android/job/d;
      //   422: invokevirtual 238	com/evernote/android/job/d:e	()Lcom/evernote/android/job/g;
      //   425: aload_1
      //   426: invokevirtual 243	com/evernote/android/job/g:b	(Lcom/evernote/android/job/f;)V
      //   429: aload_1
      //   430: iconst_0
      //   431: iconst_0
      //   432: invokevirtual 271	com/evernote/android/job/f:a	(ZZ)I
      //   435: pop
      //   436: aload 9
      //   438: areturn
      //   439: astore 7
      //   441: aload 6
      //   443: astore 8
      //   445: aload 7
      //   447: astore 6
      //   449: aload_0
      //   450: getfield 46	com/evernote/android/job/e$a:c	Lnet/a/a/a/c;
      //   453: aload 6
      //   455: invokevirtual 282	net/a/a/a/c:b	(Ljava/lang/Throwable;)V
      //   458: aload 8
      //   460: ifnull +26 -> 486
      //   463: aload 8
      //   465: invokevirtual 284	com/evernote/android/job/a:cancel	()V
      //   468: aload_0
      //   469: getfield 46	com/evernote/android/job/e$a:c	Lnet/a/a/a/c;
      //   472: ldc_w 286
      //   475: iconst_1
      //   476: anewarray 4	java/lang/Object
      //   479: dup
      //   480: iconst_0
      //   481: aload_1
      //   482: aastore
      //   483: invokevirtual 288	net/a/a/a/c:d	(Ljava/lang/String;[Ljava/lang/Object;)V
      //   486: getstatic 235	com/evernote/android/job/a$b:b	Lcom/evernote/android/job/a$b;
      //   489: astore 7
      //   491: aload_1
      //   492: invokevirtual 128	com/evernote/android/job/f:i	()Z
      //   495: ifne +17 -> 512
      //   498: aload_0
      //   499: getfield 53	com/evernote/android/job/e$a:d	Lcom/evernote/android/job/d;
      //   502: invokevirtual 238	com/evernote/android/job/d:e	()Lcom/evernote/android/job/g;
      //   505: aload_1
      //   506: invokevirtual 243	com/evernote/android/job/g:b	(Lcom/evernote/android/job/f;)V
      //   509: aload 7
      //   511: areturn
      //   512: aload 7
      //   514: astore 6
      //   516: aload_1
      //   517: invokevirtual 268	com/evernote/android/job/f:w	()Z
      //   520: ifeq -298 -> 222
      //   523: aload_0
      //   524: getfield 53	com/evernote/android/job/e$a:d	Lcom/evernote/android/job/d;
      //   527: invokevirtual 238	com/evernote/android/job/d:e	()Lcom/evernote/android/job/g;
      //   530: aload_1
      //   531: invokevirtual 243	com/evernote/android/job/g:b	(Lcom/evernote/android/job/f;)V
      //   534: aload_1
      //   535: iconst_0
      //   536: iconst_0
      //   537: invokevirtual 271	com/evernote/android/job/f:a	(ZZ)I
      //   540: pop
      //   541: aload 7
      //   543: areturn
      //   544: astore 6
      //   546: aload_1
      //   547: invokevirtual 128	com/evernote/android/job/f:i	()Z
      //   550: ifne +17 -> 567
      //   553: aload_0
      //   554: getfield 53	com/evernote/android/job/e$a:d	Lcom/evernote/android/job/d;
      //   557: invokevirtual 238	com/evernote/android/job/d:e	()Lcom/evernote/android/job/g;
      //   560: aload_1
      //   561: invokevirtual 243	com/evernote/android/job/g:b	(Lcom/evernote/android/job/f;)V
      //   564: aload 6
      //   566: athrow
      //   567: aload_1
      //   568: invokevirtual 268	com/evernote/android/job/f:w	()Z
      //   571: ifeq -7 -> 564
      //   574: aload_0
      //   575: getfield 53	com/evernote/android/job/e$a:d	Lcom/evernote/android/job/d;
      //   578: invokevirtual 238	com/evernote/android/job/d:e	()Lcom/evernote/android/job/g;
      //   581: aload_1
      //   582: invokevirtual 243	com/evernote/android/job/g:b	(Lcom/evernote/android/job/f;)V
      //   585: aload_1
      //   586: iconst_0
      //   587: iconst_0
      //   588: invokevirtual 271	com/evernote/android/job/f:a	(ZZ)I
      //   591: pop
      //   592: goto -28 -> 564
      //   595: astore 6
      //   597: aload 7
      //   599: astore 8
      //   601: goto -152 -> 449
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	604	0	this	a
      //   0	604	1	paramf	f
      //   3	83	2	l1	long
      //   8	79	4	l2	long
      //   49	466	6	localObject1	Object
      //   544	21	6	localObject2	Object
      //   595	1	6	localExecutionException	java.util.concurrent.ExecutionException
      //   111	251	7	localObject3	Object
      //   439	7	7	localInterruptedException	InterruptedException
      //   489	109	7	localb	a.b
      //   130	470	8	localObject4	Object
      //   108	329	9	localObject5	Object
      // Exception table:
      //   from	to	target	type
      //   116	132	439	java/lang/InterruptedException
      //   140	147	439	java/lang/InterruptedException
      //   155	160	439	java/lang/InterruptedException
      //   168	182	439	java/lang/InterruptedException
      //   195	200	439	java/lang/InterruptedException
      //   343	355	439	java/lang/InterruptedException
      //   363	386	439	java/lang/InterruptedException
      //   116	132	544	finally
      //   140	147	544	finally
      //   155	160	544	finally
      //   168	182	544	finally
      //   195	200	544	finally
      //   343	355	544	finally
      //   363	386	544	finally
      //   449	458	544	finally
      //   463	486	544	finally
      //   486	491	544	finally
      //   116	132	595	java/util/concurrent/ExecutionException
      //   140	147	595	java/util/concurrent/ExecutionException
      //   155	160	595	java/util/concurrent/ExecutionException
      //   168	182	595	java/util/concurrent/ExecutionException
      //   195	200	595	java/util/concurrent/ExecutionException
      //   343	355	595	java/util/concurrent/ExecutionException
      //   363	386	595	java/util/concurrent/ExecutionException
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */