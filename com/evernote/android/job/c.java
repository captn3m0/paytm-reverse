package com.evernote.android.job;

import android.content.Context;
import android.os.PowerManager.WakeLock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.LruCache;
import android.util.SparseArray;
import com.evernote.android.job.a.d;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

class c
{
  private static final net.a.a.a.c a = new d("JobExecutor");
  private static final long b = TimeUnit.MINUTES.toMillis(3L);
  private final ExecutorService c = Executors.newCachedThreadPool();
  private final SparseArray<a> d = new SparseArray();
  private final LruCache<Integer, a> e = new LruCache(20);
  
  private void a(a parama)
  {
    try
    {
      int i = parama.e().a();
      this.d.remove(i);
      this.e.a(Integer.valueOf(i), parama);
      return;
    }
    finally
    {
      parama = finally;
      throw parama;
    }
  }
  
  /* Error */
  public a a(int paramInt)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 62	com/evernote/android/job/c:d	Landroid/util/SparseArray;
    //   6: iload_1
    //   7: invokevirtual 102	android/util/SparseArray:get	(I)Ljava/lang/Object;
    //   10: checkcast 72	com/evernote/android/job/a
    //   13: astore_2
    //   14: aload_2
    //   15: ifnull +7 -> 22
    //   18: aload_0
    //   19: monitorexit
    //   20: aload_2
    //   21: areturn
    //   22: aload_0
    //   23: getfield 69	com/evernote/android/job/c:e	Landroid/support/v4/util/LruCache;
    //   26: iload_1
    //   27: invokestatic 89	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   30: invokevirtual 105	android/support/v4/util/LruCache:a	(Ljava/lang/Object;)Ljava/lang/Object;
    //   33: checkcast 72	com/evernote/android/job/a
    //   36: astore_2
    //   37: goto -19 -> 18
    //   40: astore_2
    //   41: aload_0
    //   42: monitorexit
    //   43: aload_2
    //   44: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	45	0	this	c
    //   0	45	1	paramInt	int
    //   13	24	2	locala	a
    //   40	4	2	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   2	14	40	finally
    //   22	37	40	finally
  }
  
  public Set<a> a()
  {
    try
    {
      Set localSet = a(null);
      return localSet;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public Set<a> a(String paramString)
  {
    for (;;)
    {
      HashSet localHashSet;
      int i;
      try
      {
        localHashSet = new HashSet();
        i = 0;
        if (i < this.d.size())
        {
          localObject = (a)this.d.valueAt(i);
          if ((paramString != null) && (!paramString.equals(((a)localObject).e().b()))) {
            break label148;
          }
          localHashSet.add(localObject);
          break label148;
        }
        Object localObject = this.e.c().values().iterator();
        if (((Iterator)localObject).hasNext())
        {
          a locala = (a)((Iterator)localObject).next();
          if ((paramString != null) && (!paramString.equals(locala.e().b()))) {
            continue;
          }
          localHashSet.add(locala);
          continue;
        }
      }
      finally {}
      return localHashSet;
      label148:
      i += 1;
    }
  }
  
  public Future<a.b> a(@NonNull Context paramContext, @NonNull f paramf, @Nullable a parama)
  {
    Object localObject = null;
    if (parama == null) {}
    for (;;)
    {
      try
      {
        a.c("JobCreator returned null for tag %s", new Object[] { paramf.d() });
        paramContext = (Context)localObject;
        return paramContext;
      }
      finally {}
      if (parama.h()) {
        throw new IllegalStateException("Job for tag %s was already run, a creator should always create a new Job instance");
      }
      parama.a(paramContext).a(paramf);
      a.b("Executing %s, context %s", new Object[] { paramf, paramContext.getClass().getSimpleName() });
      this.d.put(paramf.c(), parama);
      paramContext = this.c.submit(new a(parama, null));
    }
  }
  
  private final class a
    implements Callable<a.b>
  {
    private final a b;
    private final PowerManager.WakeLock c;
    
    private a(a parama)
    {
      this.b = parama;
      this.c = h.a(this.b.f(), "JobExecutor", c.b());
    }
    
    private void a(a.b paramb)
    {
      f localf = this.b.e().d();
      if ((!localf.i()) && (a.b.c.equals(paramb)))
      {
        i = localf.a(true, true);
        this.b.a(i);
      }
      while ((!localf.i()) || (a.b.a.equals(paramb)))
      {
        int i;
        return;
      }
      localf.z();
    }
    
    private a.b b()
    {
      try
      {
        a.b localb = this.b.a();
        c.c().b("Finished %s", new Object[] { this.b });
        a(localb);
        return localb;
      }
      catch (Throwable localThrowable)
      {
        c.c().a(localThrowable, "Crashed %s", new Object[] { this.b });
      }
      return this.b.j();
    }
    
    /* Error */
    public a.b a()
      throws java.lang.Exception
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 24	com/evernote/android/job/c$a:b	Lcom/evernote/android/job/a;
      //   4: invokevirtual 30	com/evernote/android/job/a:f	()Landroid/content/Context;
      //   7: aload_0
      //   8: getfield 42	com/evernote/android/job/c$a:c	Landroid/os/PowerManager$WakeLock;
      //   11: invokestatic 35	com/evernote/android/job/c:b	()J
      //   14: invokestatic 113	com/evernote/android/job/h:a	(Landroid/content/Context;Landroid/os/PowerManager$WakeLock;J)Z
      //   17: pop
      //   18: aload_0
      //   19: invokespecial 115	com/evernote/android/job/c$a:b	()Lcom/evernote/android/job/a$b;
      //   22: astore_1
      //   23: aload_0
      //   24: getfield 19	com/evernote/android/job/c$a:a	Lcom/evernote/android/job/c;
      //   27: aload_0
      //   28: getfield 24	com/evernote/android/job/c$a:b	Lcom/evernote/android/job/a;
      //   31: invokestatic 117	com/evernote/android/job/c:a	(Lcom/evernote/android/job/c;Lcom/evernote/android/job/a;)V
      //   34: aload_0
      //   35: getfield 42	com/evernote/android/job/c$a:c	Landroid/os/PowerManager$WakeLock;
      //   38: ifnull +13 -> 51
      //   41: aload_0
      //   42: getfield 42	com/evernote/android/job/c$a:c	Landroid/os/PowerManager$WakeLock;
      //   45: invokevirtual 122	android/os/PowerManager$WakeLock:isHeld	()Z
      //   48: ifne +22 -> 70
      //   51: invokestatic 91	com/evernote/android/job/c:c	()Lnet/a/a/a/c;
      //   54: ldc 124
      //   56: iconst_1
      //   57: anewarray 5	java/lang/Object
      //   60: dup
      //   61: iconst_0
      //   62: aload_0
      //   63: getfield 24	com/evernote/android/job/c$a:b	Lcom/evernote/android/job/a;
      //   66: aastore
      //   67: invokevirtual 126	net/a/a/a/c:c	(Ljava/lang/String;[Ljava/lang/Object;)V
      //   70: aload_0
      //   71: getfield 42	com/evernote/android/job/c$a:c	Landroid/os/PowerManager$WakeLock;
      //   74: invokestatic 129	com/evernote/android/job/h:a	(Landroid/os/PowerManager$WakeLock;)V
      //   77: aload_1
      //   78: areturn
      //   79: astore_1
      //   80: aload_0
      //   81: getfield 19	com/evernote/android/job/c$a:a	Lcom/evernote/android/job/c;
      //   84: aload_0
      //   85: getfield 24	com/evernote/android/job/c$a:b	Lcom/evernote/android/job/a;
      //   88: invokestatic 117	com/evernote/android/job/c:a	(Lcom/evernote/android/job/c;Lcom/evernote/android/job/a;)V
      //   91: aload_0
      //   92: getfield 42	com/evernote/android/job/c$a:c	Landroid/os/PowerManager$WakeLock;
      //   95: ifnull +13 -> 108
      //   98: aload_0
      //   99: getfield 42	com/evernote/android/job/c$a:c	Landroid/os/PowerManager$WakeLock;
      //   102: invokevirtual 122	android/os/PowerManager$WakeLock:isHeld	()Z
      //   105: ifne +22 -> 127
      //   108: invokestatic 91	com/evernote/android/job/c:c	()Lnet/a/a/a/c;
      //   111: ldc 124
      //   113: iconst_1
      //   114: anewarray 5	java/lang/Object
      //   117: dup
      //   118: iconst_0
      //   119: aload_0
      //   120: getfield 24	com/evernote/android/job/c$a:b	Lcom/evernote/android/job/a;
      //   123: aastore
      //   124: invokevirtual 126	net/a/a/a/c:c	(Ljava/lang/String;[Ljava/lang/Object;)V
      //   127: aload_0
      //   128: getfield 42	com/evernote/android/job/c$a:c	Landroid/os/PowerManager$WakeLock;
      //   131: invokestatic 129	com/evernote/android/job/h:a	(Landroid/os/PowerManager$WakeLock;)V
      //   134: aload_1
      //   135: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	136	0	this	a
      //   22	56	1	localb	a.b
      //   79	56	1	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   0	23	79	finally
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */