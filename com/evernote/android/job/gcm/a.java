package com.evernote.android.job.gcm;

import android.content.Context;
import android.support.annotation.NonNull;
import com.evernote.android.job.a.d;
import com.evernote.android.job.e;
import com.evernote.android.job.e.a;
import com.evernote.android.job.f.c;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.OneoffTask;
import com.google.android.gms.gcm.OneoffTask.Builder;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.gms.gcm.PeriodicTask.Builder;
import com.google.android.gms.gcm.Task.Builder;
import net.a.a.a.c;

public class a
  implements e
{
  private static final c a = new d("JobProxyGcm");
  private final GcmNetworkManager b;
  
  public a(Context paramContext)
  {
    this.b = GcmNetworkManager.a(paramContext);
  }
  
  protected int a(@NonNull f.c paramc)
  {
    int i = 1;
    switch (1.a[paramc.ordinal()])
    {
    default: 
      throw new IllegalStateException("not implemented");
    case 1: 
      i = 2;
    case 3: 
    case 4: 
      return i;
    }
    return 0;
  }
  
  protected <T extends Task.Builder> T a(T paramT, com.evernote.android.job.f paramf)
  {
    paramT.b(e(paramf)).b(PlatformGcmService.class).e(true).b(a(paramf.o())).d(paramf.p()).f(paramf.m());
    return paramT;
  }
  
  protected String a(int paramInt)
  {
    return String.valueOf(paramInt);
  }
  
  public void a(com.evernote.android.job.f paramf)
  {
    long l1 = e.a.a(paramf);
    long l2 = l1 / 1000L;
    long l3 = e.a.b(paramf);
    long l4 = Math.max(l3 / 1000L, 1L + l2);
    OneoffTask localOneoffTask = ((OneoffTask.Builder)a(new OneoffTask.Builder(), paramf)).a(l2, l4).b();
    this.b.a(localOneoffTask);
    a.a("Scheduled OneoffTask, %s, start %s, end %s", new Object[] { paramf, com.evernote.android.job.a.f.a(l1), com.evernote.android.job.a.f.a(l3) });
  }
  
  public void b(com.evernote.android.job.f paramf)
  {
    PeriodicTask localPeriodicTask = ((PeriodicTask.Builder)a(new PeriodicTask.Builder(), paramf)).a(paramf.j() / 1000L).b(paramf.k() / 1000L).b();
    this.b.a(localPeriodicTask);
    a.a("Scheduled PeriodicTask, %s, interval %s, flex %s", new Object[] { paramf, com.evernote.android.job.a.f.a(paramf.j()), com.evernote.android.job.a.f.a(paramf.k()) });
  }
  
  public void c(com.evernote.android.job.f paramf)
  {
    a.b("plantPeriodicFlexSupport called although flex is supported");
    long l1 = e.a.d(paramf);
    long l2 = e.a.e(paramf);
    OneoffTask localOneoffTask = ((OneoffTask.Builder)a(new OneoffTask.Builder(), paramf)).a(l1 / 1000L, l2 / 1000L).b();
    this.b.a(localOneoffTask);
    a.a("Scheduled periodic (flex support), %s, start %s, end %s, flex %s", new Object[] { paramf, com.evernote.android.job.a.f.a(l1), com.evernote.android.job.a.f.a(l2), com.evernote.android.job.a.f.a(paramf.k()) });
  }
  
  public void cancel(int paramInt)
  {
    this.b.a(a(paramInt), PlatformGcmService.class);
  }
  
  public boolean d(com.evernote.android.job.f paramf)
  {
    return true;
  }
  
  protected String e(com.evernote.android.job.f paramf)
  {
    return a(paramf.c());
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/gcm/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */