package com.evernote.android.job.c;

import android.annotation.TargetApi;
import android.app.job.JobInfo.Builder;
import android.app.job.JobScheduler;
import android.content.Context;
import android.support.annotation.NonNull;
import com.evernote.android.job.f;
import com.evernote.android.job.f.c;
import net.a.a.a.c;

@TargetApi(24)
public class a
  extends com.evernote.android.job.v21.a
{
  public a(Context paramContext)
  {
    super(paramContext, "JobProxy24");
  }
  
  protected int a(@NonNull f.c paramc)
  {
    switch (1.a[paramc.ordinal()])
    {
    default: 
      return super.a(paramc);
    }
    return 3;
  }
  
  protected JobInfo.Builder b(JobInfo.Builder paramBuilder, long paramLong1, long paramLong2)
  {
    return paramBuilder.setPeriodic(paramLong1, paramLong2);
  }
  
  public void c(f paramf)
  {
    this.b.b("plantPeriodicFlexSupport called although flex is supported");
    super.c(paramf);
  }
  
  public boolean d(f paramf)
  {
    boolean bool = false;
    try
    {
      paramf = a().getPendingJob(paramf.c());
      if (paramf != null) {
        bool = true;
      }
      return bool;
    }
    catch (Exception paramf)
    {
      this.b.b(paramf);
    }
    return false;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/c/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */