package com.evernote.android.job;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.support.v4.util.LruCache;
import android.text.TextUtils;
import com.evernote.android.job.a.d;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import net.a.a.a.c;

class g
{
  private static final c a = new d("JobStorage");
  private final SharedPreferences b;
  private final a c;
  private final AtomicInteger d;
  private final b e;
  private SQLiteDatabase f;
  
  public g(Context paramContext)
  {
    this.b = paramContext.getSharedPreferences("evernote_jobs", 0);
    this.c = new a();
    this.d = new AtomicInteger(this.b.getInt("JOB_ID_COUNTER", 0));
    this.e = new b(paramContext);
  }
  
  private f a(int paramInt, boolean paramBoolean)
  {
    Object localObject5 = null;
    f localf = null;
    Object localObject4 = "_id=?";
    if (!paramBoolean)
    {
      localObject2 = localf;
      localObject1 = localObject5;
    }
    try
    {
      localObject4 = "_id=?" + " AND isTransient<=0";
      localObject2 = localf;
      localObject1 = localObject5;
      localObject4 = b().query("jobs", null, (String)localObject4, new String[] { String.valueOf(paramInt) }, null, null, null);
      localObject2 = localObject4;
      localObject1 = localObject4;
      if (((Cursor)localObject4).moveToFirst())
      {
        localObject2 = localObject4;
        localObject1 = localObject4;
        localf = f.a((Cursor)localObject4);
        return localf;
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localObject1 = localObject2;
        a.a(localException, "could not load id %d", new Object[] { Integer.valueOf(paramInt) });
        if (localObject2 != null) {
          ((Cursor)localObject2).close();
        }
      }
    }
    finally
    {
      if (localObject1 == null) {
        break label191;
      }
      ((Cursor)localObject1).close();
    }
    return null;
  }
  
  private SQLiteDatabase b()
  {
    if (this.f == null) {}
    try
    {
      if (this.f == null) {
        this.f = this.e.getWritableDatabase();
      }
      return this.f;
    }
    finally {}
  }
  
  private void c(f paramf)
  {
    this.c.a(Integer.valueOf(paramf.c()), paramf);
  }
  
  private void d(f paramf)
  {
    try
    {
      ContentValues localContentValues = paramf.A();
      b().insert("jobs", null, localContentValues);
      return;
    }
    catch (Exception localException)
    {
      a.a(localException, "could not store %s", new Object[] { paramf });
    }
  }
  
  public int a()
  {
    try
    {
      int j = this.d.incrementAndGet();
      int i = j;
      if (j < 0)
      {
        i = 1;
        this.d.set(1);
      }
      this.b.edit().putInt("JOB_ID_COUNTER", i).apply();
      return i;
    }
    finally {}
  }
  
  public f a(int paramInt)
  {
    try
    {
      f localf = (f)this.c.a(Integer.valueOf(paramInt));
      return localf;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public Set<f> a(@Nullable String paramString, boolean paramBoolean)
  {
    Object localObject3 = null;
    for (;;)
    {
      try
      {
        HashSet localHashSet = new HashSet();
        Object localObject4 = null;
        Integer localInteger = null;
        Object localObject2 = localInteger;
        Object localObject1 = localObject4;
        try
        {
          if (!TextUtils.isEmpty(paramString)) {
            break label315;
          }
          if (!paramBoolean) {
            continue;
          }
          paramString = (String)localObject3;
        }
        catch (Exception paramString)
        {
          localObject1 = localObject2;
          a.a(paramString, "could not load all jobs", new Object[0]);
          if (localObject2 == null) {
            continue;
          }
          ((Cursor)localObject2).close();
          return localHashSet;
          paramString = "ifnull(isTransient, 0)<=0";
          break label309;
          localObject2 = localInteger;
          localObject1 = localObject4;
          String str = (String)localObject3 + "tag=?";
          localObject2 = localInteger;
          localObject1 = localObject4;
          localObject3 = new String[1];
          localObject3[0] = paramString;
          paramString = str;
          continue;
          localObject2 = paramString;
          localObject1 = paramString;
          localHashSet.add(f.a(paramString));
          continue;
        }
        finally
        {
          if (localObject1 == null) {
            continue;
          }
          ((Cursor)localObject1).close();
        }
        localObject2 = localInteger;
        localObject1 = localObject4;
        paramString = b().query("jobs", null, paramString, (String[])localObject3, null, null, null);
        localObject2 = paramString;
        localObject1 = paramString;
        localObject3 = new HashMap(this.c.c());
        localObject2 = paramString;
        localObject1 = paramString;
        if (paramString.moveToNext())
        {
          localObject2 = paramString;
          localObject1 = paramString;
          localInteger = Integer.valueOf(paramString.getInt(paramString.getColumnIndex("_id")));
          localObject2 = paramString;
          localObject1 = paramString;
          if (((HashMap)localObject3).containsKey(localInteger))
          {
            localObject2 = paramString;
            localObject1 = paramString;
            localHashSet.add(((HashMap)localObject3).get(localInteger));
            continue;
          }
        }
        if (paramString == null) {
          continue;
        }
      }
      finally {}
      paramString.close();
      continue;
      label309:
      localObject3 = null;
      continue;
      label315:
      if (paramBoolean) {
        localObject3 = "";
      } else {
        localObject3 = "ifnull(isTransient, 0)<=0 AND ";
      }
    }
  }
  
  public void a(f paramf)
  {
    try
    {
      c(paramf);
      d(paramf);
      return;
    }
    finally
    {
      paramf = finally;
      throw paramf;
    }
  }
  
  /* Error */
  public void a(f paramf, ContentValues paramContentValues)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokespecial 238	com/evernote/android/job/g:c	(Lcom/evernote/android/job/f;)V
    //   7: aload_0
    //   8: invokespecial 92	com/evernote/android/job/g:b	()Landroid/database/sqlite/SQLiteDatabase;
    //   11: ldc 94
    //   13: aload_2
    //   14: ldc 76
    //   16: iconst_1
    //   17: anewarray 96	java/lang/String
    //   20: dup
    //   21: iconst_0
    //   22: aload_1
    //   23: invokevirtual 144	com/evernote/android/job/f:c	()I
    //   26: invokestatic 100	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   29: aastore
    //   30: invokevirtual 245	android/database/sqlite/SQLiteDatabase:update	(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   33: pop
    //   34: aload_0
    //   35: monitorexit
    //   36: return
    //   37: astore_2
    //   38: getstatic 32	com/evernote/android/job/g:a	Lnet/a/a/a/c;
    //   41: aload_2
    //   42: ldc -9
    //   44: iconst_1
    //   45: anewarray 4	java/lang/Object
    //   48: dup
    //   49: iconst_0
    //   50: aload_1
    //   51: aastore
    //   52: invokevirtual 132	net/a/a/a/c:a	(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    //   55: goto -21 -> 34
    //   58: astore_1
    //   59: aload_0
    //   60: monitorexit
    //   61: aload_1
    //   62: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	63	0	this	g
    //   0	63	1	paramf	f
    //   0	63	2	paramContentValues	ContentValues
    // Exception table:
    //   from	to	target	type
    //   7	34	37	java/lang/Exception
    //   2	7	58	finally
    //   7	34	58	finally
    //   38	55	58	finally
  }
  
  /* Error */
  public void b(f paramf)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 51	com/evernote/android/job/g:c	Lcom/evernote/android/job/g$a;
    //   6: aload_1
    //   7: invokevirtual 144	com/evernote/android/job/f:c	()I
    //   10: invokestatic 127	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   13: invokevirtual 249	com/evernote/android/job/g$a:b	(Ljava/lang/Object;)Ljava/lang/Object;
    //   16: pop
    //   17: aload_0
    //   18: invokespecial 92	com/evernote/android/job/g:b	()Landroid/database/sqlite/SQLiteDatabase;
    //   21: ldc 94
    //   23: ldc 76
    //   25: iconst_1
    //   26: anewarray 96	java/lang/String
    //   29: dup
    //   30: iconst_0
    //   31: aload_1
    //   32: invokevirtual 144	com/evernote/android/job/f:c	()I
    //   35: invokestatic 100	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   38: aastore
    //   39: invokevirtual 253	android/database/sqlite/SQLiteDatabase:delete	(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    //   42: pop
    //   43: aload_0
    //   44: monitorexit
    //   45: return
    //   46: astore_2
    //   47: getstatic 32	com/evernote/android/job/g:a	Lnet/a/a/a/c;
    //   50: aload_2
    //   51: ldc -1
    //   53: iconst_1
    //   54: anewarray 4	java/lang/Object
    //   57: dup
    //   58: iconst_0
    //   59: aload_1
    //   60: aastore
    //   61: invokevirtual 132	net/a/a/a/c:a	(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    //   64: goto -21 -> 43
    //   67: astore_1
    //   68: aload_0
    //   69: monitorexit
    //   70: aload_1
    //   71: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	72	0	this	g
    //   0	72	1	paramf	f
    //   46	5	2	localException	Exception
    // Exception table:
    //   from	to	target	type
    //   17	43	46	java/lang/Exception
    //   2	17	67	finally
    //   17	43	67	finally
    //   47	64	67	finally
  }
  
  private class a
    extends LruCache<Integer, f>
  {
    public a()
    {
      super();
    }
    
    protected f a(Integer paramInteger)
    {
      return g.a(g.this, paramInteger.intValue(), true);
    }
  }
  
  private class b
    extends SQLiteOpenHelper
  {
    public b(Context paramContext)
    {
      super("evernote_jobs.db", null, 3);
    }
    
    private void a(SQLiteDatabase paramSQLiteDatabase)
    {
      paramSQLiteDatabase.execSQL("create table jobs (_id integer primary key, tag text not null, startMs integer, endMs integer, backoffMs integer, backoffPolicy text not null, intervalMs integer, requirementsEnforced integer, requiresCharging integer, requiresDeviceIdle integer, exact integer, networkType text not null, extras text, persisted integer, numFailures integer, scheduledAt integer, isTransient integer, flexMs integer, flexSupport integer);");
    }
    
    private void b(SQLiteDatabase paramSQLiteDatabase)
    {
      paramSQLiteDatabase.execSQL("alter table jobs add column isTransient integer;");
    }
    
    private void c(SQLiteDatabase paramSQLiteDatabase)
    {
      paramSQLiteDatabase.execSQL("alter table jobs add column flexMs integer;");
      paramSQLiteDatabase.execSQL("alter table jobs add column flexSupport integer;");
      ContentValues localContentValues = new ContentValues();
      localContentValues.put("intervalMs", Long.valueOf(f.c));
      paramSQLiteDatabase.update("jobs", localContentValues, "intervalMs>0 AND intervalMs<" + f.c, new String[0]);
      paramSQLiteDatabase.execSQL("update jobs set flexMs = intervalMs;");
    }
    
    public void onCreate(SQLiteDatabase paramSQLiteDatabase)
    {
      a(paramSQLiteDatabase);
    }
    
    public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
    {
      while (paramInt1 < paramInt2) {
        switch (paramInt1)
        {
        default: 
          throw new IllegalStateException("not implemented");
        case 1: 
          b(paramSQLiteDatabase);
          paramInt1 += 1;
          break;
        case 2: 
          c(paramSQLiteDatabase);
          paramInt1 += 1;
        }
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */