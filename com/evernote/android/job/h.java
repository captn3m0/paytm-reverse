package com.evernote.android.job;

import android.content.Context;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.evernote.android.job.a.d;
import com.evernote.android.job.a.f;
import net.a.a.a.c;

final class h
{
  private static final c a = new d("WakeLockUtil");
  
  @Nullable
  public static PowerManager.WakeLock a(@NonNull Context paramContext, @NonNull String paramString, long paramLong)
  {
    paramString = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(1, paramString);
    paramString.setReferenceCounted(false);
    if (a(paramContext, paramString, paramLong)) {
      return paramString;
    }
    return null;
  }
  
  public static void a(@Nullable PowerManager.WakeLock paramWakeLock)
  {
    if (paramWakeLock != null) {}
    try
    {
      if (paramWakeLock.isHeld()) {
        paramWakeLock.release();
      }
      return;
    }
    catch (Exception paramWakeLock)
    {
      a.b(paramWakeLock);
    }
  }
  
  public static boolean a(@NonNull Context paramContext, @Nullable PowerManager.WakeLock paramWakeLock, long paramLong)
  {
    if ((paramWakeLock != null) && (!paramWakeLock.isHeld()) && (f.b(paramContext))) {
      try
      {
        paramWakeLock.acquire(paramLong);
        return true;
      }
      catch (Exception paramContext)
      {
        a.b(paramContext);
      }
    }
    return false;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */