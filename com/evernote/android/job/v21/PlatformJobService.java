package com.evernote.android.job.v21;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import com.evernote.android.job.a;
import com.evernote.android.job.e.a;
import com.evernote.android.job.f;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import net.a.a.a.c;

@TargetApi(21)
public class PlatformJobService
  extends JobService
{
  private static final c a = new com.evernote.android.job.a.d("PlatformJobService");
  private static final ExecutorService b = Executors.newCachedThreadPool();
  
  public boolean onStartJob(final JobParameters paramJobParameters)
  {
    final e.a locala = new e.a(this, paramJobParameters.getJobId());
    final f localf = locala.a(true);
    if (localf == null) {
      return false;
    }
    b.execute(new Runnable()
    {
      public void run()
      {
        try
        {
          locala.g(localf);
          return;
        }
        finally
        {
          PlatformJobService.this.jobFinished(paramJobParameters, false);
        }
      }
    });
    return true;
  }
  
  public boolean onStopJob(JobParameters paramJobParameters)
  {
    a locala = com.evernote.android.job.d.a().a(paramJobParameters.getJobId());
    if (locala != null)
    {
      locala.cancel();
      a.a("Called onStopJob for %s", new Object[] { locala });
      return false;
    }
    a.a("Called onStopJob, job %d not found", new Object[] { Integer.valueOf(paramJobParameters.getJobId()) });
    return false;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/v21/PlatformJobService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */