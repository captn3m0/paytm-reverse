package com.evernote.android.job.v21;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.app.job.JobInfo.Builder;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.support.annotation.NonNull;
import com.evernote.android.job.a.d;
import com.evernote.android.job.e;
import com.evernote.android.job.e.a;
import com.evernote.android.job.f.c;
import java.util.Iterator;
import java.util.List;
import net.a.a.a.c;

@TargetApi(21)
public class a
  implements e
{
  protected final Context a;
  protected final c b;
  
  public a(Context paramContext)
  {
    this(paramContext, "JobProxy21");
  }
  
  protected a(Context paramContext, String paramString)
  {
    this.a = paramContext;
    this.b = new d(paramString);
  }
  
  protected static String a(int paramInt)
  {
    if (paramInt == 1) {
      return "success";
    }
    return "failure";
  }
  
  protected final int a(JobInfo paramJobInfo)
  {
    try
    {
      int i = a().schedule(paramJobInfo);
      return i;
    }
    catch (Exception paramJobInfo)
    {
      this.b.b(paramJobInfo);
    }
    return 0;
  }
  
  protected int a(@NonNull f.c paramc)
  {
    int i = 2;
    switch (1.a[paramc.ordinal()])
    {
    default: 
      throw new IllegalStateException("not implemented");
    case 1: 
      i = 0;
    case 3: 
    case 4: 
      return i;
    }
    return 1;
  }
  
  protected JobInfo.Builder a(JobInfo.Builder paramBuilder, long paramLong1, long paramLong2)
  {
    return paramBuilder.setMinimumLatency(paramLong1).setOverrideDeadline(paramLong2);
  }
  
  protected final JobScheduler a()
  {
    return (JobScheduler)this.a.getSystemService("jobscheduler");
  }
  
  public void a(com.evernote.android.job.f paramf)
  {
    long l1 = e.a.a(paramf);
    long l2 = e.a.b(paramf);
    int i = a(a(e(paramf), l1, l2).build());
    this.b.a("Schedule one-off jobInfo %s, %s, start %s, end %s", new Object[] { a(i), paramf, com.evernote.android.job.a.f.a(l1), com.evernote.android.job.a.f.a(l2) });
  }
  
  protected JobInfo.Builder b(JobInfo.Builder paramBuilder, long paramLong1, long paramLong2)
  {
    return paramBuilder.setPeriodic(paramLong1);
  }
  
  public void b(com.evernote.android.job.f paramf)
  {
    long l1 = paramf.j();
    long l2 = paramf.k();
    int i = a(b(e(paramf), l1, l2).build());
    this.b.a("Schedule periodic jobInfo %s, %s, interval %s, flex %s", new Object[] { a(i), paramf, com.evernote.android.job.a.f.a(l1), com.evernote.android.job.a.f.a(l2) });
  }
  
  public void c(com.evernote.android.job.f paramf)
  {
    long l1 = e.a.d(paramf);
    long l2 = e.a.e(paramf);
    int i = a(a(e(paramf), l1, l2).build());
    this.b.a("Schedule periodic (flex support) jobInfo %s, %s, start %s, end %s, flex %s", new Object[] { a(i), paramf, com.evernote.android.job.a.f.a(l1), com.evernote.android.job.a.f.a(l2), com.evernote.android.job.a.f.a(paramf.k()) });
  }
  
  public void cancel(int paramInt)
  {
    try
    {
      a().cancel(paramInt);
      return;
    }
    catch (Exception localException)
    {
      this.b.b(localException);
    }
  }
  
  public boolean d(com.evernote.android.job.f paramf)
  {
    List localList;
    try
    {
      localList = a().getAllPendingJobs();
      if ((localList == null) || (localList.isEmpty())) {
        return false;
      }
    }
    catch (Exception paramf)
    {
      this.b.b(paramf);
      return false;
    }
    int i = paramf.c();
    paramf = localList.iterator();
    do
    {
      if (!paramf.hasNext()) {
        break;
      }
    } while (((JobInfo)paramf.next()).getId() != i);
    return true;
  }
  
  protected JobInfo.Builder e(com.evernote.android.job.f paramf)
  {
    return new JobInfo.Builder(paramf.c(), new ComponentName(this.a, PlatformJobService.class)).setRequiresCharging(paramf.m()).setRequiresDeviceIdle(paramf.n()).setRequiredNetworkType(a(paramf.o())).setPersisted(paramf.p());
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/evernote/android/job/v21/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */