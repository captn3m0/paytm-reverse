package com.andexert.expandablelayout.library;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Transformation;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

public class ExpandableLayout
  extends RelativeLayout
{
  private Boolean a = Boolean.valueOf(false);
  private Boolean b = Boolean.valueOf(false);
  private Integer c;
  private FrameLayout d;
  private FrameLayout e;
  private Animation f;
  
  public ExpandableLayout(Context paramContext)
  {
    super(paramContext);
  }
  
  public ExpandableLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a(paramContext, paramAttributeSet);
  }
  
  public ExpandableLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    a(paramContext, paramAttributeSet);
  }
  
  private void a(Context paramContext, AttributeSet paramAttributeSet)
  {
    View localView = View.inflate(paramContext, a.b.view_expandable, this);
    this.e = ((FrameLayout)localView.findViewById(a.a.view_expandable_headerlayout));
    paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, a.c.ExpandableLayout);
    int i = paramAttributeSet.getResourceId(a.c.ExpandableLayout_el_headerLayout, -1);
    int j = paramAttributeSet.getResourceId(a.c.ExpandableLayout_el_contentLayout, -1);
    this.d = ((FrameLayout)localView.findViewById(a.a.view_expandable_contentLayout));
    if ((i == -1) || (j == -1)) {
      throw new IllegalArgumentException("HeaderLayout and ContentLayout cannot be null!");
    }
    if (isInEditMode()) {
      return;
    }
    this.c = Integer.valueOf(paramAttributeSet.getInt(a.c.ExpandableLayout_el_duration, getContext().getResources().getInteger(17694720)));
    localView = View.inflate(paramContext, i, null);
    localView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
    this.e.addView(localView);
    paramContext = View.inflate(paramContext, j, null);
    paramContext.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
    this.d.addView(paramContext);
    this.d.setVisibility(8);
    this.e.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (!ExpandableLayout.a(ExpandableLayout.this).booleanValue())
        {
          if (ExpandableLayout.b(ExpandableLayout.this).getVisibility() != 0) {
            break label83;
          }
          ExpandableLayout.a(ExpandableLayout.this, ExpandableLayout.b(ExpandableLayout.this));
        }
        for (;;)
        {
          ExpandableLayout.a(ExpandableLayout.this, Boolean.valueOf(true));
          new Handler().postDelayed(new Runnable()
          {
            public void run()
            {
              ExpandableLayout.a(ExpandableLayout.this, Boolean.valueOf(false));
            }
          }, ExpandableLayout.c(ExpandableLayout.this).intValue());
          return;
          label83:
          ExpandableLayout.b(ExpandableLayout.this, ExpandableLayout.b(ExpandableLayout.this));
        }
      }
    });
    paramAttributeSet.recycle();
  }
  
  private void a(final View paramView)
  {
    paramView.measure(-1, -2);
    final int i = paramView.getMeasuredHeight();
    paramView.getLayoutParams().height = 0;
    paramView.setVisibility(0);
    this.f = new Animation()
    {
      protected void applyTransformation(float paramAnonymousFloat, Transformation paramAnonymousTransformation)
      {
        if (paramAnonymousFloat == 1.0F) {
          ExpandableLayout.b(ExpandableLayout.this, Boolean.valueOf(true));
        }
        paramAnonymousTransformation = paramView.getLayoutParams();
        if (paramAnonymousFloat == 1.0F) {}
        for (int i = -2;; i = (int)(i * paramAnonymousFloat))
        {
          paramAnonymousTransformation.height = i;
          paramView.requestLayout();
          return;
        }
      }
      
      public boolean willChangeBounds()
      {
        return true;
      }
    };
    this.f.setDuration(this.c.intValue());
    paramView.startAnimation(this.f);
  }
  
  private void b(final View paramView)
  {
    this.f = new Animation()
    {
      protected void applyTransformation(float paramAnonymousFloat, Transformation paramAnonymousTransformation)
      {
        if (paramAnonymousFloat == 1.0F)
        {
          paramView.setVisibility(8);
          ExpandableLayout.b(ExpandableLayout.this, Boolean.valueOf(false));
          return;
        }
        paramView.getLayoutParams().height = (this.b - (int)(this.b * paramAnonymousFloat));
        paramView.requestLayout();
      }
      
      public boolean willChangeBounds()
      {
        return true;
      }
    };
    this.f.setDuration(this.c.intValue());
    paramView.startAnimation(this.f);
  }
  
  public void a()
  {
    if (this.d == null) {
      return;
    }
    try
    {
      this.f.cancel();
      this.d.getLayoutParams().height = 0;
      this.d.requestLayout();
      this.d.setVisibility(8);
      this.b = Boolean.valueOf(false);
      this.a = Boolean.valueOf(false);
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  public Boolean b()
  {
    return this.b;
  }
  
  public void c()
  {
    this.a = Boolean.valueOf(false);
    this.d.getLayoutParams().height = 1;
    this.d.requestLayout();
    this.b = Boolean.valueOf(false);
  }
  
  public Boolean d()
  {
    return this.a;
  }
  
  public FrameLayout getContentLayout()
  {
    return this.d;
  }
  
  public FrameLayout getHeaderLayout()
  {
    return this.e;
  }
  
  public void setAnimationRunning(boolean paramBoolean)
  {
    this.a = Boolean.valueOf(paramBoolean);
  }
  
  public void setLayoutAnimationListener(Animation.AnimationListener paramAnimationListener)
  {
    this.f.setAnimationListener(paramAnimationListener);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/andexert/expandablelayout/library/ExpandableLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */