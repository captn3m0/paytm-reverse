package com.a.a;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.widget.ScrollView;
import android.widget.TextView;
import io.a.a.a.a.b.j.a;
import io.a.a.a.a.b.j.b;
import io.a.a.a.a.b.n;
import io.a.a.a.a.c.d;
import io.a.a.a.a.f.b;
import io.a.a.a.a.g.p;
import io.a.a.a.a.g.q.b;
import io.a.a.a.a.g.t;
import io.a.a.a.h;
import io.a.a.a.j;
import java.io.File;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@d(a={com.a.a.c.a.class})
public class f
  extends h<Void>
  implements io.a.a.a.i
{
  private final long f = System.currentTimeMillis();
  private final ConcurrentHashMap<String, String> g = new ConcurrentHashMap();
  private final Collection<h<Boolean>> h;
  private g i;
  private i j;
  private String k = null;
  private String l = null;
  private String m = null;
  private String n;
  private String o;
  private String p;
  private String q;
  private String r;
  private float s;
  private boolean t;
  private final o u;
  private io.a.a.a.a.e.e v;
  private final ExecutorService w;
  private com.a.a.c.a x;
  
  public f()
  {
    this(1.0F, null, null, false);
  }
  
  f(float paramFloat, g paramg, o paramo, boolean paramBoolean)
  {
    this(paramFloat, paramg, paramo, paramBoolean, n.a("Crashlytics Exception Handler"));
  }
  
  f(float paramFloat, g paramg, o paramo, boolean paramBoolean, ExecutorService paramExecutorService)
  {
    this.s = paramFloat;
    this.i = paramg;
    this.u = paramo;
    this.t = paramBoolean;
    this.w = paramExecutorService;
    this.h = Collections.unmodifiableCollection(Arrays.asList(new h[] { new com.a.a.a.a(), new com.a.a.b.a() }));
  }
  
  private void E()
  {
    Object localObject = new io.a.a.a.a.c.g()
    {
      public Void a()
        throws Exception
      {
        return f.this.b();
      }
      
      public io.a.a.a.a.c.e b()
      {
        return io.a.a.a.a.c.e.d;
      }
    };
    Iterator localIterator = D().iterator();
    while (localIterator.hasNext()) {
      ((io.a.a.a.a.c.g)localObject).a((io.a.a.a.a.c.l)localIterator.next());
    }
    localObject = A().e().submit((Callable)localObject);
    io.a.a.a.c.g().a("Fabric", "Crashlytics detected incomplete initialization on previous app launch. Will initialize synchronously.");
    try
    {
      ((Future)localObject).get(4L, TimeUnit.SECONDS);
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
      io.a.a.a.c.g().d("Fabric", "Crashlytics was interrupted during initialization.", localInterruptedException);
      return;
    }
    catch (ExecutionException localExecutionException)
    {
      io.a.a.a.c.g().d("Fabric", "Problem encountered during Crashlytics initialization.", localExecutionException);
      return;
    }
    catch (TimeoutException localTimeoutException)
    {
      io.a.a.a.c.g().d("Fabric", "Crashlytics timed out during initialization.", localTimeoutException);
    }
  }
  
  private static boolean F()
  {
    f localf = f();
    return (localf == null) || (localf.t);
  }
  
  private int a(float paramFloat, int paramInt)
  {
    return (int)(paramInt * paramFloat);
  }
  
  public static void a(int paramInt, String paramString1, String paramString2)
  {
    b(paramInt, paramString1, paramString2);
    io.a.a.a.c.g().a(paramInt, "" + paramString1, "" + paramString2, true);
  }
  
  public static void a(String paramString)
  {
    if (F()) {
      return;
    }
    f().k = e(paramString);
  }
  
  public static void a(Throwable paramThrowable)
  {
    if (F()) {}
    f localf;
    do
    {
      return;
      localf = f();
    } while (!a("prior to logging exceptions.", localf));
    if (paramThrowable == null)
    {
      io.a.a.a.c.g().a(5, "Fabric", "Crashlytics is ignoring a request to log a null exception.");
      return;
    }
    localf.j.a(Thread.currentThread(), paramThrowable);
  }
  
  private boolean a(final Activity paramActivity, final io.a.a.a.a.g.o paramo)
  {
    final m localm = new m(paramActivity, paramo);
    final a locala = new a(null);
    paramActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramActivity);
        Object localObject = new DialogInterface.OnClickListener()
        {
          public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int)
          {
            f.5.this.b.a(true);
            paramAnonymous2DialogInterface.dismiss();
          }
        };
        float f = paramActivity.getResources().getDisplayMetrics().density;
        int i = f.a(f.this, f, 5);
        TextView localTextView = new TextView(paramActivity);
        localTextView.setAutoLinkMask(15);
        localTextView.setText(localm.b());
        localTextView.setTextAppearance(paramActivity, 16973892);
        localTextView.setPadding(i, i, i, i);
        localTextView.setFocusable(false);
        ScrollView localScrollView = new ScrollView(paramActivity);
        localScrollView.setPadding(f.a(f.this, f, 14), f.a(f.this, f, 2), f.a(f.this, f, 10), f.a(f.this, f, 12));
        localScrollView.addView(localTextView);
        localBuilder.setView(localScrollView).setTitle(localm.a()).setCancelable(false).setNeutralButton(localm.c(), (DialogInterface.OnClickListener)localObject);
        if (paramo.d)
        {
          localObject = new DialogInterface.OnClickListener()
          {
            public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int)
            {
              f.5.this.b.a(false);
              paramAnonymous2DialogInterface.dismiss();
            }
          };
          localBuilder.setNegativeButton(localm.e(), (DialogInterface.OnClickListener)localObject);
        }
        if (paramo.f)
        {
          localObject = new DialogInterface.OnClickListener()
          {
            public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int)
            {
              f.this.a(true);
              f.5.this.b.a(true);
              paramAnonymous2DialogInterface.dismiss();
            }
          };
          localBuilder.setPositiveButton(localm.d(), (DialogInterface.OnClickListener)localObject);
        }
        localBuilder.show();
      }
    });
    io.a.a.a.c.g().a("Fabric", "Waiting for user opt-in.");
    locala.b();
    return locala.a();
  }
  
  private static boolean a(String paramString, f paramf)
  {
    if ((paramf == null) || (paramf.j == null))
    {
      io.a.a.a.c.g().d("Fabric", "Crashlytics must be initialized by calling Fabric.with(Context) " + paramString, null);
      return false;
    }
    return true;
  }
  
  private static void b(int paramInt, String paramString1, String paramString2)
  {
    if (F()) {}
    f localf;
    do
    {
      return;
      localf = f();
    } while (!a("prior to logging messages.", localf));
    long l1 = System.currentTimeMillis();
    long l2 = localf.f;
    localf.j.a(l1 - l2, c(paramInt, paramString1, paramString2));
  }
  
  public static void b(String paramString)
  {
    if (F()) {
      return;
    }
    f().l = e(paramString);
  }
  
  private boolean b(Context paramContext)
  {
    return io.a.a.a.a.b.i.a(paramContext, "com.crashlytics.RequireBuildId", true);
  }
  
  private static String c(int paramInt, String paramString1, String paramString2)
  {
    return io.a.a.a.a.b.i.b(paramInt) + "/" + paramString1 + " " + paramString2;
  }
  
  static void c(String paramString)
  {
    com.a.a.a.a locala = (com.a.a.a.a)io.a.a.a.c.a(com.a.a.a.a.class);
    if (locala != null) {
      locala.a(new j.b(paramString));
    }
  }
  
  static void d(String paramString)
  {
    com.a.a.a.a locala = (com.a.a.a.a)io.a.a.a.c.a(com.a.a.a.a.class);
    if (locala != null) {
      locala.a(new j.a(paramString));
    }
  }
  
  private static String e(String paramString)
  {
    String str = paramString;
    if (paramString != null)
    {
      paramString = paramString.trim();
      str = paramString;
      if (paramString.length() > 1024) {
        str = paramString.substring(0, 1024);
      }
    }
    return str;
  }
  
  public static f f()
  {
    try
    {
      f localf = (f)io.a.a.a.c.a(f.class);
      return localf;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      io.a.a.a.c.g().d("Fabric", "Crashlytics must be initialized by calling Fabric.with(Context) prior to calling Crashlytics.getInstance()", null);
      throw localIllegalStateException;
    }
  }
  
  a a(String paramString, boolean paramBoolean)
  {
    return new a(paramString, paramBoolean);
  }
  
  k a(t paramt)
  {
    if (paramt != null) {
      return new l(this, l(), paramt.a.d, this.v);
    }
    return null;
  }
  
  @SuppressLint({"CommitPrefEdits"})
  void a(boolean paramBoolean)
  {
    io.a.a.a.a.f.c localc = new io.a.a.a.a.f.c(this);
    localc.a(localc.b().putBoolean("always_send_reports_opt_in", paramBoolean));
  }
  
  protected boolean a()
  {
    return a(super.z());
  }
  
  /* Error */
  boolean a(Context paramContext)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 105	com/a/a/f:t	Z
    //   4: ifeq +5 -> 9
    //   7: iconst_0
    //   8: ireturn
    //   9: aload_1
    //   10: invokestatic 422	io/a/a/a/c:h	()Z
    //   13: invokestatic 427	io/a/a/a/a/b/g:a	(Landroid/content/Context;Z)Ljava/lang/String;
    //   16: astore 5
    //   18: aload 5
    //   20: ifnonnull +5 -> 25
    //   23: iconst_0
    //   24: ireturn
    //   25: aload_0
    //   26: new 429	io/a/a/a/a/e/b
    //   29: dup
    //   30: invokestatic 183	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   33: invokespecial 432	io/a/a/a/a/e/b:<init>	(Lio/a/a/a/j;)V
    //   36: putfield 381	com/a/a/f:v	Lio/a/a/a/a/e/e;
    //   39: aload_0
    //   40: getfield 103	com/a/a/f:u	Lcom/a/a/o;
    //   43: ifnonnull +296 -> 339
    //   46: aload_0
    //   47: getfield 381	com/a/a/f:v	Lio/a/a/a/a/e/e;
    //   50: aconst_null
    //   51: invokeinterface 437 2 0
    //   56: invokestatic 183	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   59: ldc -71
    //   61: new 230	java/lang/StringBuilder
    //   64: dup
    //   65: invokespecial 231	java/lang/StringBuilder:<init>	()V
    //   68: ldc_w 439
    //   71: invokevirtual 237	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   74: aload_0
    //   75: invokevirtual 441	com/a/a/f:d	()Ljava/lang/String;
    //   78: invokevirtual 237	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   81: invokevirtual 241	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   84: invokeinterface 443 3 0
    //   89: aload_0
    //   90: aload_1
    //   91: invokevirtual 448	android/content/Context:getPackageName	()Ljava/lang/String;
    //   94: putfield 450	com/a/a/f:o	Ljava/lang/String;
    //   97: aload_0
    //   98: aload_0
    //   99: invokevirtual 454	com/a/a/f:y	()Lio/a/a/a/a/b/o;
    //   102: invokevirtual 458	io/a/a/a/a/b/o:h	()Ljava/lang/String;
    //   105: putfield 460	com/a/a/f:p	Ljava/lang/String;
    //   108: invokestatic 183	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   111: ldc -71
    //   113: new 230	java/lang/StringBuilder
    //   116: dup
    //   117: invokespecial 231	java/lang/StringBuilder:<init>	()V
    //   120: ldc_w 462
    //   123: invokevirtual 237	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   126: aload_0
    //   127: getfield 460	com/a/a/f:p	Ljava/lang/String;
    //   130: invokevirtual 237	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   133: invokevirtual 241	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   136: invokeinterface 192 3 0
    //   141: aload_1
    //   142: invokevirtual 466	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
    //   145: aload_0
    //   146: getfield 450	com/a/a/f:o	Ljava/lang/String;
    //   149: iconst_0
    //   150: invokevirtual 472	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    //   153: astore 4
    //   155: aload_0
    //   156: aload 4
    //   158: getfield 478	android/content/pm/PackageInfo:versionCode	I
    //   161: invokestatic 482	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   164: putfield 484	com/a/a/f:q	Ljava/lang/String;
    //   167: aload 4
    //   169: getfield 487	android/content/pm/PackageInfo:versionName	Ljava/lang/String;
    //   172: ifnonnull +197 -> 369
    //   175: ldc_w 489
    //   178: astore 4
    //   180: aload_0
    //   181: aload 4
    //   183: putfield 491	com/a/a/f:r	Ljava/lang/String;
    //   186: aload_0
    //   187: aload_1
    //   188: invokestatic 494	io/a/a/a/a/b/i:m	(Landroid/content/Context;)Ljava/lang/String;
    //   191: putfield 496	com/a/a/f:n	Ljava/lang/String;
    //   194: aload_0
    //   195: invokevirtual 454	com/a/a/f:y	()Lio/a/a/a/a/b/o;
    //   198: invokevirtual 498	io/a/a/a/a/b/o:m	()Ljava/lang/String;
    //   201: pop
    //   202: aload_0
    //   203: aload_0
    //   204: getfield 496	com/a/a/f:n	Ljava/lang/String;
    //   207: aload_0
    //   208: aload_1
    //   209: invokespecial 500	com/a/a/f:b	(Landroid/content/Context;)Z
    //   212: invokevirtual 502	com/a/a/f:a	(Ljava/lang/String;Z)Lcom/a/a/a;
    //   215: aload 5
    //   217: aload_0
    //   218: getfield 450	com/a/a/f:o	Ljava/lang/String;
    //   221: invokevirtual 503	com/a/a/a:a	(Ljava/lang/String;Ljava/lang/String;)V
    //   224: iconst_0
    //   225: istore_3
    //   226: iload_3
    //   227: istore_2
    //   228: invokestatic 183	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   231: ldc -71
    //   233: ldc_w 505
    //   236: invokeinterface 192 3 0
    //   241: iload_3
    //   242: istore_2
    //   243: aload_0
    //   244: new 270	com/a/a/i
    //   247: dup
    //   248: invokestatic 509	java/lang/Thread:getDefaultUncaughtExceptionHandler	()Ljava/lang/Thread$UncaughtExceptionHandler;
    //   251: aload_0
    //   252: getfield 101	com/a/a/f:i	Lcom/a/a/g;
    //   255: aload_0
    //   256: getfield 107	com/a/a/f:w	Ljava/util/concurrent/ExecutorService;
    //   259: aload_0
    //   260: getfield 496	com/a/a/f:n	Ljava/lang/String;
    //   263: aload_0
    //   264: invokevirtual 454	com/a/a/f:y	()Lio/a/a/a/a/b/o;
    //   267: aload_0
    //   268: invokespecial 512	com/a/a/i:<init>	(Ljava/lang/Thread$UncaughtExceptionHandler;Lcom/a/a/g;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;Lio/a/a/a/a/b/o;Lcom/a/a/f;)V
    //   271: putfield 262	com/a/a/f:j	Lcom/a/a/i;
    //   274: iload_3
    //   275: istore_2
    //   276: aload_0
    //   277: getfield 262	com/a/a/f:j	Lcom/a/a/i;
    //   280: invokevirtual 514	com/a/a/i:j	()Z
    //   283: istore_3
    //   284: iload_3
    //   285: istore_2
    //   286: aload_0
    //   287: getfield 262	com/a/a/f:j	Lcom/a/a/i;
    //   290: invokevirtual 515	com/a/a/i:b	()V
    //   293: iload_3
    //   294: istore_2
    //   295: aload_0
    //   296: getfield 262	com/a/a/f:j	Lcom/a/a/i;
    //   299: invokestatic 519	java/lang/Thread:setDefaultUncaughtExceptionHandler	(Ljava/lang/Thread$UncaughtExceptionHandler;)V
    //   302: iload_3
    //   303: istore_2
    //   304: invokestatic 183	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   307: ldc -71
    //   309: ldc_w 521
    //   312: invokeinterface 192 3 0
    //   317: iload_3
    //   318: istore_2
    //   319: iload_2
    //   320: ifeq +114 -> 434
    //   323: aload_0
    //   324: invokevirtual 522	com/a/a/f:z	()Landroid/content/Context;
    //   327: invokestatic 524	io/a/a/a/a/b/i:n	(Landroid/content/Context;)Z
    //   330: ifeq +104 -> 434
    //   333: aload_0
    //   334: invokespecial 526	com/a/a/f:E	()V
    //   337: iconst_0
    //   338: ireturn
    //   339: aload_0
    //   340: getfield 381	com/a/a/f:v	Lio/a/a/a/a/e/e;
    //   343: new 9	com/a/a/f$1
    //   346: dup
    //   347: aload_0
    //   348: invokespecial 527	com/a/a/f$1:<init>	(Lcom/a/a/f;)V
    //   351: invokeinterface 437 2 0
    //   356: goto -300 -> 56
    //   359: astore_1
    //   360: new 529	io/a/a/a/a/c/m
    //   363: dup
    //   364: aload_1
    //   365: invokespecial 531	io/a/a/a/a/c/m:<init>	(Ljava/lang/Throwable;)V
    //   368: athrow
    //   369: aload 4
    //   371: getfield 487	android/content/pm/PackageInfo:versionName	Ljava/lang/String;
    //   374: astore 4
    //   376: goto -196 -> 180
    //   379: astore 4
    //   381: invokestatic 183	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   384: ldc -71
    //   386: ldc_w 533
    //   389: aload 4
    //   391: invokeinterface 212 4 0
    //   396: goto -202 -> 194
    //   399: astore_1
    //   400: invokestatic 183	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   403: ldc -71
    //   405: ldc_w 535
    //   408: aload_1
    //   409: invokeinterface 212 4 0
    //   414: iconst_0
    //   415: ireturn
    //   416: astore_1
    //   417: invokestatic 183	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   420: ldc -71
    //   422: ldc_w 537
    //   425: aload_1
    //   426: invokeinterface 212 4 0
    //   431: goto -112 -> 319
    //   434: iconst_1
    //   435: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	436	0	this	f
    //   0	436	1	paramContext	Context
    //   227	93	2	bool1	boolean
    //   225	93	3	bool2	boolean
    //   153	222	4	localObject	Object
    //   379	11	4	localException	Exception
    //   16	200	5	str	String
    // Exception table:
    //   from	to	target	type
    //   25	56	359	com/a/a/h
    //   56	89	359	com/a/a/h
    //   89	175	359	com/a/a/h
    //   180	194	359	com/a/a/h
    //   194	224	359	com/a/a/h
    //   228	241	359	com/a/a/h
    //   243	274	359	com/a/a/h
    //   276	284	359	com/a/a/h
    //   286	293	359	com/a/a/h
    //   295	302	359	com/a/a/h
    //   304	317	359	com/a/a/h
    //   323	337	359	com/a/a/h
    //   339	356	359	com/a/a/h
    //   369	376	359	com/a/a/h
    //   381	396	359	com/a/a/h
    //   417	431	359	com/a/a/h
    //   89	175	379	java/lang/Exception
    //   180	194	379	java/lang/Exception
    //   369	376	379	java/lang/Exception
    //   25	56	399	java/lang/Exception
    //   56	89	399	java/lang/Exception
    //   194	224	399	java/lang/Exception
    //   323	337	399	java/lang/Exception
    //   339	356	399	java/lang/Exception
    //   381	396	399	java/lang/Exception
    //   417	431	399	java/lang/Exception
    //   228	241	416	java/lang/Exception
    //   243	274	416	java/lang/Exception
    //   276	284	416	java/lang/Exception
    //   286	293	416	java/lang/Exception
    //   295	302	416	java/lang/Exception
    //   304	317	416	java/lang/Exception
  }
  
  protected Void b()
  {
    this.j.h();
    this.j.g();
    int i3 = 1;
    i2 = 1;
    i1 = i3;
    for (;;)
    {
      try
      {
        Object localObject1 = io.a.a.a.a.g.q.a().b();
        if (localObject1 == null)
        {
          i1 = i3;
          io.a.a.a.c.g().c("Fabric", "Received null settings, skipping initialization!");
          return null;
        }
        i1 = i3;
        if (((t)localObject1).d.c)
        {
          i3 = 0;
          i2 = 0;
          i1 = i3;
          this.j.d();
          i1 = i3;
          localObject1 = a((t)localObject1);
          if (localObject1 == null) {
            continue;
          }
          i1 = i3;
          new q((k)localObject1).a(this.s);
        }
      }
      catch (Exception localException1)
      {
        io.a.a.a.c.g().d("Fabric", "Error dealing with settings", localException1);
        i2 = i1;
        continue;
      }
      finally
      {
        this.j.i();
      }
      if (i2 != 0) {}
      try
      {
        io.a.a.a.c.g().a("Fabric", "Crash reporting disabled.");
        this.j.i();
        return null;
      }
      catch (Exception localException2)
      {
        io.a.a.a.c.g().d("Fabric", "Problem encountered during Crashlytics initialization.", localException2);
        this.j.i();
        return null;
      }
      i1 = i3;
      io.a.a.a.c.g().c("Fabric", "Unable to create a call to upload reports.");
    }
  }
  
  public String c()
  {
    return "com.crashlytics.sdk.android:crashlytics";
  }
  
  public String d()
  {
    return "2.2.1.32";
  }
  
  public Collection<? extends h> e()
  {
    return this.h;
  }
  
  Map<String, String> g()
  {
    return Collections.unmodifiableMap(this.g);
  }
  
  String h()
  {
    return this.o;
  }
  
  String i()
  {
    return this.p;
  }
  
  String j()
  {
    return this.r;
  }
  
  String k()
  {
    return this.q;
  }
  
  String l()
  {
    return io.a.a.a.a.b.i.b(f().z(), "com.crashlytics.ApiEndpoint");
  }
  
  i m()
  {
    return this.j;
  }
  
  String n()
  {
    if (y().a()) {
      return this.k;
    }
    return null;
  }
  
  String o()
  {
    if (y().a()) {
      return this.l;
    }
    return null;
  }
  
  String p()
  {
    if (y().a()) {
      return this.m;
    }
    return null;
  }
  
  com.a.a.c.a.c q()
  {
    com.a.a.c.a.c localc = null;
    if (this.x != null) {
      localc = this.x.a();
    }
    return localc;
  }
  
  File r()
  {
    return new io.a.a.a.a.f.a(this).a();
  }
  
  boolean s()
  {
    ((Boolean)io.a.a.a.a.g.q.a().a(new q.b()
    {
      public Boolean a(t paramAnonymoust)
      {
        boolean bool = false;
        if (paramAnonymoust.d.a)
        {
          if (!f.this.t()) {
            bool = true;
          }
          return Boolean.valueOf(bool);
        }
        return Boolean.valueOf(false);
      }
    }, Boolean.valueOf(false))).booleanValue();
  }
  
  boolean t()
  {
    return new io.a.a.a.a.f.c(this).a().getBoolean("always_send_reports_opt_in", false);
  }
  
  boolean u()
  {
    ((Boolean)io.a.a.a.a.g.q.a().a(new q.b()
    {
      public Boolean a(t paramAnonymoust)
      {
        boolean bool2 = true;
        Activity localActivity = f.this.A().b();
        boolean bool1 = bool2;
        if (localActivity != null)
        {
          bool1 = bool2;
          if (!localActivity.isFinishing())
          {
            bool1 = bool2;
            if (f.this.s()) {
              bool1 = f.a(f.this, localActivity, paramAnonymoust.c);
            }
          }
        }
        return Boolean.valueOf(bool1);
      }
    }, Boolean.valueOf(true))).booleanValue();
  }
  
  p v()
  {
    t localt = io.a.a.a.a.g.q.a().b();
    if (localt == null) {
      return null;
    }
    return localt.b;
  }
  
  private class a
  {
    private boolean b = false;
    private final CountDownLatch c = new CountDownLatch(1);
    
    private a() {}
    
    void a(boolean paramBoolean)
    {
      this.b = paramBoolean;
      this.c.countDown();
    }
    
    boolean a()
    {
      return this.b;
    }
    
    void b()
    {
      try
      {
        this.c.await();
        return;
      }
      catch (InterruptedException localInterruptedException) {}
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/a/a/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */