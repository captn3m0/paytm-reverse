package com.a.a;

import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Environment;
import android.os.Looper;
import io.a.a.a.a.b.o;
import io.a.a.a.a.b.o.a;
import io.a.a.a.a.b.q.c;
import io.a.a.a.a.g.p;
import io.a.a.a.a.g.t;
import io.a.a.a.j;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class i
  implements Thread.UncaughtExceptionHandler
{
  static final FilenameFilter a = new FilenameFilter()
  {
    public boolean accept(File paramAnonymousFile, String paramAnonymousString)
    {
      return (paramAnonymousString.length() == ".cls".length() + 35) && (paramAnonymousString.endsWith(".cls"));
    }
  };
  static final Comparator<File> b = new Comparator()
  {
    public int a(File paramAnonymousFile1, File paramAnonymousFile2)
    {
      return paramAnonymousFile2.getName().compareTo(paramAnonymousFile1.getName());
    }
  };
  static final Comparator<File> c = new Comparator()
  {
    public int a(File paramAnonymousFile1, File paramAnonymousFile2)
    {
      return paramAnonymousFile1.getName().compareTo(paramAnonymousFile2.getName());
    }
  };
  static final FilenameFilter d = new FilenameFilter()
  {
    public boolean accept(File paramAnonymousFile, String paramAnonymousString)
    {
      return i.k().matcher(paramAnonymousString).matches();
    }
  };
  private static final Pattern e = Pattern.compile("([\\d|A-Z|a-z]{12}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{12}).+");
  private static final Map<String, String> f = Collections.singletonMap("X-CRASHLYTICS-SEND-FLAGS", "1");
  private static final b g = b.a("0");
  private StackTraceElement[] A;
  private final f B;
  private final AtomicInteger h = new AtomicInteger(0);
  private final AtomicBoolean i = new AtomicBoolean(false);
  private final int j;
  private final Thread.UncaughtExceptionHandler k;
  private final File l;
  private final File m;
  private final AtomicBoolean n;
  private final String o;
  private final BroadcastReceiver p;
  private final BroadcastReceiver q;
  private final b r;
  private final b s;
  private final ExecutorService t;
  private final o u;
  private ActivityManager.RunningAppProcessInfo v;
  private io.a.a.a.a.b.q w;
  private boolean x;
  private Thread[] y;
  private List<StackTraceElement[]> z;
  
  i(Thread.UncaughtExceptionHandler paramUncaughtExceptionHandler, g paramg, ExecutorService paramExecutorService, String paramString, o paramo, f paramf)
  {
    this.k = paramUncaughtExceptionHandler;
    this.t = paramExecutorService;
    this.u = paramo;
    this.B = paramf;
    this.n = new AtomicBoolean(false);
    this.l = paramf.r();
    this.m = new File(this.l, "initialization_marker");
    this.o = String.format(Locale.US, "Crashlytics Android SDK/%s", new Object[] { paramf.d() });
    this.j = 8;
    a(paramg);
    this.r = b.a(paramf.h());
    if (paramString == null) {}
    for (paramUncaughtExceptionHandler = null;; paramUncaughtExceptionHandler = b.a(paramString.replace("-", "")))
    {
      this.s = paramUncaughtExceptionHandler;
      this.q = new BroadcastReceiver()
      {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
          i.a(i.this, true);
        }
      };
      paramUncaughtExceptionHandler = new IntentFilter("android.intent.action.ACTION_POWER_CONNECTED");
      this.p = new BroadcastReceiver()
      {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
          i.a(i.this, false);
        }
      };
      paramg = new IntentFilter("android.intent.action.ACTION_POWER_DISCONNECTED");
      paramExecutorService = paramf.z();
      paramExecutorService.registerReceiver(this.q, paramUncaughtExceptionHandler);
      paramExecutorService.registerReceiver(this.p, paramg);
      this.i.set(true);
      return;
    }
  }
  
  private int a(float paramFloat, int paramInt1, boolean paramBoolean, int paramInt2, long paramLong1, long paramLong2)
  {
    return 0 + e.b(1, paramFloat) + e.h(2, paramInt1) + e.b(3, paramBoolean) + e.f(4, paramInt2) + e.b(5, paramLong1) + e.b(6, paramLong2);
  }
  
  private int a(int paramInt1, b paramb1, b paramb2, int paramInt2, long paramLong1, long paramLong2, boolean paramBoolean, Map<o.a, String> paramMap, int paramInt3, b paramb3, b paramb4)
  {
    int i1 = e.b(1, paramb1);
    int i2 = e.g(3, paramInt1);
    if (paramb2 == null) {}
    for (paramInt1 = 0;; paramInt1 = e.b(4, paramb2))
    {
      paramInt1 = 0 + i1 + i2 + paramInt1 + e.f(5, paramInt2) + e.b(6, paramLong1) + e.b(7, paramLong2) + e.b(10, paramBoolean);
      paramInt2 = paramInt1;
      if (paramMap == null) {
        break;
      }
      paramb1 = paramMap.entrySet().iterator();
      for (;;)
      {
        paramInt2 = paramInt1;
        if (!paramb1.hasNext()) {
          break;
        }
        paramb2 = (Map.Entry)paramb1.next();
        paramInt2 = a((o.a)paramb2.getKey(), (String)paramb2.getValue());
        paramInt1 += e.j(11) + e.l(paramInt2) + paramInt2;
      }
    }
    i1 = e.f(12, paramInt3);
    if (paramb3 == null)
    {
      paramInt1 = 0;
      if (paramb4 != null) {
        break label203;
      }
    }
    label203:
    for (paramInt3 = 0;; paramInt3 = e.b(14, paramb4))
    {
      return paramInt2 + i1 + paramInt1 + paramInt3;
      paramInt1 = e.b(13, paramb3);
      break;
    }
  }
  
  private int a(b paramb)
  {
    return e.b(1, paramb);
  }
  
  private int a(b paramb1, b paramb2, b paramb3, b paramb4, int paramInt)
  {
    int i1 = e.b(1, paramb1);
    int i2 = e.b(2, paramb2);
    int i3 = e.b(3, paramb3);
    int i4 = s();
    return 0 + i1 + i2 + i3 + (e.j(5) + e.l(i4) + i4) + e.b(6, paramb4) + e.g(10, paramInt);
  }
  
  private int a(b paramb1, b paramb2, boolean paramBoolean)
  {
    return 0 + e.g(1, 3) + e.b(2, paramb1) + e.b(3, paramb2) + e.b(4, paramBoolean);
  }
  
  private int a(o.a parama, String paramString)
  {
    return e.g(1, parama.h) + e.b(2, b.a(paramString));
  }
  
  private int a(StackTraceElement paramStackTraceElement, boolean paramBoolean)
  {
    int i3 = 2;
    int i2;
    if (paramStackTraceElement.isNativeMethod())
    {
      i1 = 0 + e.b(1, Math.max(paramStackTraceElement.getLineNumber(), 0));
      i2 = i1 + e.b(2, b.a(paramStackTraceElement.getClassName() + "." + paramStackTraceElement.getMethodName()));
      i1 = i2;
      if (paramStackTraceElement.getFileName() != null) {
        i1 = i2 + e.b(3, b.a(paramStackTraceElement.getFileName()));
      }
      i2 = i1;
      if (!paramStackTraceElement.isNativeMethod())
      {
        i2 = i1;
        if (paramStackTraceElement.getLineNumber() > 0) {
          i2 = i1 + e.b(4, paramStackTraceElement.getLineNumber());
        }
      }
      if (!paramBoolean) {
        break label152;
      }
    }
    label152:
    for (int i1 = i3;; i1 = 0)
    {
      return i2 + e.f(5, i1);
      i1 = 0 + e.b(1, 0L);
      break;
    }
  }
  
  private int a(String paramString1, String paramString2)
  {
    int i1 = e.b(1, b.a(paramString1));
    paramString1 = paramString2;
    if (paramString2 == null) {
      paramString1 = "";
    }
    return i1 + e.b(2, b.a(paramString1));
  }
  
  private int a(Thread paramThread, Throwable paramThrowable, String paramString, long paramLong1, Map<String, String> paramMap, float paramFloat, int paramInt1, boolean paramBoolean, int paramInt2, long paramLong2, long paramLong3, b paramb)
  {
    int i1 = e.b(1, paramLong1);
    int i2 = e.b(2, b.a(paramString));
    int i3 = a(paramThread, paramThrowable, paramMap);
    int i4 = e.j(3);
    int i5 = e.l(i3);
    paramInt1 = a(paramFloat, paramInt1, paramBoolean, paramInt2, paramLong2, paramLong3);
    paramInt2 = 0 + i1 + i2 + (i4 + i5 + i3) + (e.j(5) + e.l(paramInt1) + paramInt1);
    paramInt1 = paramInt2;
    if (paramb != null)
    {
      paramInt1 = a(paramb);
      paramInt1 = paramInt2 + (e.j(6) + e.l(paramInt1) + paramInt1);
    }
    return paramInt1;
  }
  
  private int a(Thread paramThread, Throwable paramThrowable, Map<String, String> paramMap)
  {
    int i1 = b(paramThread, paramThrowable);
    int i2 = 0 + (e.j(1) + e.l(i1) + i1);
    i1 = i2;
    if (paramMap != null)
    {
      paramThread = paramMap.entrySet().iterator();
      for (;;)
      {
        i1 = i2;
        if (!paramThread.hasNext()) {
          break;
        }
        paramThrowable = (Map.Entry)paramThread.next();
        i1 = a((String)paramThrowable.getKey(), (String)paramThrowable.getValue());
        i2 += e.j(2) + e.l(i1) + i1;
      }
    }
    i2 = i1;
    if (this.v != null) {
      if (this.v.importance == 100) {
        break label174;
      }
    }
    label174:
    for (boolean bool = true;; bool = false)
    {
      i2 = i1 + e.b(3, bool);
      return i2 + e.f(4, this.B.z().getResources().getConfiguration().orientation);
    }
  }
  
  private int a(Thread paramThread, StackTraceElement[] paramArrayOfStackTraceElement, int paramInt, boolean paramBoolean)
  {
    int i1 = e.b(1, b.a(paramThread.getName())) + e.f(2, paramInt);
    int i2 = paramArrayOfStackTraceElement.length;
    paramInt = 0;
    while (paramInt < i2)
    {
      int i3 = a(paramArrayOfStackTraceElement[paramInt], paramBoolean);
      i1 += e.j(3) + e.l(i3) + i3;
      paramInt += 1;
    }
    return i1;
  }
  
  private int a(Throwable paramThrowable, int paramInt)
  {
    int i2 = 0 + e.b(1, b.a(paramThrowable.getClass().getName()));
    Object localObject = paramThrowable.getLocalizedMessage();
    int i1 = i2;
    if (localObject != null) {
      i1 = i2 + e.b(3, b.a((String)localObject));
    }
    localObject = paramThrowable.getStackTrace();
    int i3 = localObject.length;
    i2 = 0;
    while (i2 < i3)
    {
      int i4 = a(localObject[i2], true);
      i1 += e.j(4) + e.l(i4) + i4;
      i2 += 1;
    }
    paramThrowable = paramThrowable.getCause();
    i2 = i1;
    if (paramThrowable != null)
    {
      if (paramInt < this.j)
      {
        paramInt = a(paramThrowable, paramInt + 1);
        i2 = i1 + (e.j(6) + e.l(paramInt) + paramInt);
      }
    }
    else {
      return i2;
    }
    paramInt = 0;
    while (paramThrowable != null)
    {
      paramThrowable = paramThrowable.getCause();
      paramInt += 1;
    }
    return i1 + e.f(7, paramInt);
  }
  
  private <T> T a(Callable<T> paramCallable)
  {
    try
    {
      if (Looper.getMainLooper() == Looper.myLooper()) {
        return (T)this.t.submit(paramCallable).get(4L, TimeUnit.SECONDS);
      }
      paramCallable = this.t.submit(paramCallable).get();
      return paramCallable;
    }
    catch (RejectedExecutionException paramCallable)
    {
      io.a.a.a.c.g().a("Fabric", "Executor is shut down because we're handling a fatal crash.");
      return null;
    }
    catch (Exception paramCallable)
    {
      io.a.a.a.c.g().d("Fabric", "Failed to execute task.", paramCallable);
    }
    return null;
  }
  
  private String a(File paramFile)
  {
    return paramFile.getName().substring(0, 35);
  }
  
  private static String a(Throwable paramThrowable)
  {
    paramThrowable = paramThrowable.getLocalizedMessage();
    if (paramThrowable == null) {
      return null;
    }
    return paramThrowable.replaceAll("(\r\n|\n|\f)", " ");
  }
  
  private Future<?> a(final Runnable paramRunnable)
  {
    try
    {
      paramRunnable = this.t.submit(new Runnable()
      {
        public void run()
        {
          try
          {
            paramRunnable.run();
            return;
          }
          catch (Exception localException)
          {
            io.a.a.a.c.g().d("Fabric", "Failed to execute task.", localException);
          }
        }
      });
      return paramRunnable;
    }
    catch (RejectedExecutionException paramRunnable)
    {
      io.a.a.a.c.g().a("Fabric", "Executor is shut down because we're handling a fatal crash.");
    }
    return null;
  }
  
  private void a(int paramInt)
  {
    HashSet localHashSet = new HashSet();
    File[] arrayOfFile = e();
    Arrays.sort(arrayOfFile, b);
    int i1 = Math.min(paramInt, arrayOfFile.length);
    paramInt = 0;
    while (paramInt < i1)
    {
      localHashSet.add(a(arrayOfFile[paramInt]));
      paramInt += 1;
    }
    arrayOfFile = a(new a(null));
    i1 = arrayOfFile.length;
    paramInt = 0;
    while (paramInt < i1)
    {
      File localFile = arrayOfFile[paramInt];
      String str = localFile.getName();
      Matcher localMatcher = e.matcher(str);
      localMatcher.matches();
      if (!localHashSet.contains(localMatcher.group(1)))
      {
        io.a.a.a.c.g().a("Fabric", "Trimming open session file: " + str);
        localFile.delete();
      }
      paramInt += 1;
    }
  }
  
  /* Error */
  private void a(com.a.a.c.a.c paramc)
    throws IOException
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 5
    //   3: aconst_null
    //   4: astore 10
    //   6: aconst_null
    //   7: astore 4
    //   9: aconst_null
    //   10: astore 6
    //   12: aconst_null
    //   13: astore 11
    //   15: aconst_null
    //   16: astore 8
    //   18: aconst_null
    //   19: astore 7
    //   21: aconst_null
    //   22: astore 9
    //   24: aload 11
    //   26: astore_2
    //   27: aload 10
    //   29: astore_3
    //   30: aload_0
    //   31: invokespecial 570	com/a/a/i:m	()Ljava/lang/String;
    //   34: astore 12
    //   36: aload 12
    //   38: ifnull +84 -> 122
    //   41: aload 11
    //   43: astore_2
    //   44: aload 10
    //   46: astore_3
    //   47: new 572	com/a/a/d
    //   50: dup
    //   51: aload_0
    //   52: getfield 185	com/a/a/i:l	Ljava/io/File;
    //   55: new 350	java/lang/StringBuilder
    //   58: dup
    //   59: invokespecial 351	java/lang/StringBuilder:<init>	()V
    //   62: aload 12
    //   64: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   67: ldc_w 574
    //   70: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   73: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   76: invokespecial 575	com/a/a/d:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   79: astore 4
    //   81: aload 8
    //   83: astore_2
    //   84: aload 7
    //   86: astore 5
    //   88: aload 4
    //   90: invokestatic 578	com/a/a/e:a	(Ljava/io/OutputStream;)Lcom/a/a/e;
    //   93: astore_3
    //   94: aload_3
    //   95: astore_2
    //   96: aload_3
    //   97: astore 5
    //   99: aload_1
    //   100: aload_3
    //   101: invokestatic 583	com/a/a/n:a	(Lcom/a/a/c/a/c;Lcom/a/a/e;)V
    //   104: aload 4
    //   106: astore_1
    //   107: aload_3
    //   108: ldc_w 585
    //   111: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   114: aload_1
    //   115: ldc_w 592
    //   118: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   121: return
    //   122: aload 11
    //   124: astore_2
    //   125: aload 10
    //   127: astore_3
    //   128: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   131: ldc_w 481
    //   134: ldc_w 597
    //   137: aconst_null
    //   138: invokeinterface 493 4 0
    //   143: aload 9
    //   145: astore_3
    //   146: aload 4
    //   148: astore_1
    //   149: goto -42 -> 107
    //   152: astore_1
    //   153: aload 5
    //   155: astore 4
    //   157: aload_1
    //   158: astore 5
    //   160: aload 6
    //   162: astore_1
    //   163: aload_1
    //   164: astore_2
    //   165: aload 4
    //   167: astore_3
    //   168: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   171: ldc_w 481
    //   174: ldc_w 599
    //   177: aload 5
    //   179: invokeinterface 493 4 0
    //   184: aload_1
    //   185: astore_2
    //   186: aload 4
    //   188: astore_3
    //   189: aload_0
    //   190: aload 5
    //   192: aload 4
    //   194: invokespecial 602	com/a/a/i:a	(Ljava/lang/Throwable;Ljava/io/OutputStream;)V
    //   197: aload_1
    //   198: ldc_w 585
    //   201: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   204: aload 4
    //   206: ldc_w 592
    //   209: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   212: return
    //   213: astore_1
    //   214: aload_2
    //   215: ldc_w 585
    //   218: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   221: aload_3
    //   222: ldc_w 592
    //   225: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   228: aload_1
    //   229: athrow
    //   230: astore_1
    //   231: aload 4
    //   233: astore_3
    //   234: goto -20 -> 214
    //   237: astore_2
    //   238: aload 5
    //   240: astore_1
    //   241: aload_2
    //   242: astore 5
    //   244: goto -81 -> 163
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	247	0	this	i
    //   0	247	1	paramc	com.a.a.c.a.c
    //   26	189	2	localObject1	Object
    //   237	5	2	localException	Exception
    //   29	205	3	localObject2	Object
    //   7	225	4	localObject3	Object
    //   1	242	5	localObject4	Object
    //   10	151	6	localObject5	Object
    //   19	66	7	localObject6	Object
    //   16	66	8	localObject7	Object
    //   22	122	9	localObject8	Object
    //   4	122	10	localObject9	Object
    //   13	110	11	localObject10	Object
    //   34	29	12	str	String
    // Exception table:
    //   from	to	target	type
    //   30	36	152	java/lang/Exception
    //   47	81	152	java/lang/Exception
    //   128	143	152	java/lang/Exception
    //   30	36	213	finally
    //   47	81	213	finally
    //   128	143	213	finally
    //   168	184	213	finally
    //   189	197	213	finally
    //   88	94	230	finally
    //   99	104	230	finally
    //   88	94	237	java/lang/Exception
    //   99	104	237	java/lang/Exception
  }
  
  private void a(d paramd)
  {
    if (paramd != null) {}
    try
    {
      paramd.a();
      return;
    }
    catch (IOException paramd)
    {
      io.a.a.a.c.g().d("Fabric", "Error closing session file stream in the presence of an exception", paramd);
    }
  }
  
  private void a(e parame, float paramFloat, int paramInt1, boolean paramBoolean, int paramInt2, long paramLong1, long paramLong2)
    throws Exception
  {
    parame.i(5, 2);
    parame.k(a(paramFloat, paramInt1, paramBoolean, paramInt2, paramLong1, paramLong2));
    parame.a(1, paramFloat);
    parame.d(2, paramInt1);
    parame.a(3, paramBoolean);
    parame.b(4, paramInt2);
    parame.a(5, paramLong1);
    parame.a(6, paramLong2);
  }
  
  private void a(e parame, int paramInt, StackTraceElement paramStackTraceElement, boolean paramBoolean)
    throws Exception
  {
    int i1 = 4;
    parame.i(paramInt, 2);
    parame.k(a(paramStackTraceElement, paramBoolean));
    if (paramStackTraceElement.isNativeMethod())
    {
      parame.a(1, Math.max(paramStackTraceElement.getLineNumber(), 0));
      parame.a(2, b.a(paramStackTraceElement.getClassName() + "." + paramStackTraceElement.getMethodName()));
      if (paramStackTraceElement.getFileName() != null) {
        parame.a(3, b.a(paramStackTraceElement.getFileName()));
      }
      if ((!paramStackTraceElement.isNativeMethod()) && (paramStackTraceElement.getLineNumber() > 0)) {
        parame.a(4, paramStackTraceElement.getLineNumber());
      }
      if (!paramBoolean) {
        break label146;
      }
    }
    label146:
    for (paramInt = i1;; paramInt = 0)
    {
      parame.b(5, paramInt);
      return;
      parame.a(1, 0L);
      break;
    }
  }
  
  private void a(e parame, b paramb)
    throws Exception
  {
    if (paramb != null)
    {
      parame.i(6, 2);
      parame.k(a(paramb));
      parame.a(1, paramb);
    }
  }
  
  /* Error */
  private void a(e parame, File paramFile)
    throws IOException
  {
    // Byte code:
    //   0: aload_2
    //   1: invokevirtual 636	java/io/File:exists	()Z
    //   4: ifeq +87 -> 91
    //   7: aload_2
    //   8: invokevirtual 640	java/io/File:length	()J
    //   11: l2i
    //   12: newarray <illegal type>
    //   14: astore 6
    //   16: aconst_null
    //   17: astore 5
    //   19: new 642	java/io/FileInputStream
    //   22: dup
    //   23: aload_2
    //   24: invokespecial 645	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   27: astore_2
    //   28: iconst_0
    //   29: istore_3
    //   30: iload_3
    //   31: aload 6
    //   33: arraylength
    //   34: if_icmpge +30 -> 64
    //   37: aload_2
    //   38: aload 6
    //   40: iload_3
    //   41: aload 6
    //   43: arraylength
    //   44: iload_3
    //   45: isub
    //   46: invokevirtual 649	java/io/FileInputStream:read	([BII)I
    //   49: istore 4
    //   51: iload 4
    //   53: iflt +11 -> 64
    //   56: iload_3
    //   57: iload 4
    //   59: iadd
    //   60: istore_3
    //   61: goto -31 -> 30
    //   64: aload_2
    //   65: ldc_w 651
    //   68: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   71: aload_1
    //   72: aload 6
    //   74: invokevirtual 654	com/a/a/e:a	([B)V
    //   77: return
    //   78: astore_2
    //   79: aload 5
    //   81: astore_1
    //   82: aload_1
    //   83: ldc_w 651
    //   86: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   89: aload_2
    //   90: athrow
    //   91: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   94: ldc_w 481
    //   97: new 350	java/lang/StringBuilder
    //   100: dup
    //   101: invokespecial 351	java/lang/StringBuilder:<init>	()V
    //   104: ldc_w 656
    //   107: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   110: aload_2
    //   111: invokevirtual 496	java/io/File:getName	()Ljava/lang/String;
    //   114: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   117: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   120: aconst_null
    //   121: invokeinterface 493 4 0
    //   126: return
    //   127: astore 5
    //   129: aload_2
    //   130: astore_1
    //   131: aload 5
    //   133: astore_2
    //   134: goto -52 -> 82
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	137	0	this	i
    //   0	137	1	parame	e
    //   0	137	2	paramFile	File
    //   29	32	3	i1	int
    //   49	11	4	i2	int
    //   17	63	5	localObject1	Object
    //   127	5	5	localObject2	Object
    //   14	59	6	arrayOfByte	byte[]
    // Exception table:
    //   from	to	target	type
    //   19	28	78	finally
    //   30	51	127	finally
  }
  
  private void a(e parame, String paramString)
    throws IOException
  {
    String[] arrayOfString = new String[4];
    arrayOfString[0] = "SessionUser";
    arrayOfString[1] = "SessionApp";
    arrayOfString[2] = "SessionOS";
    arrayOfString[3] = "SessionDevice";
    int i2 = arrayOfString.length;
    int i1 = 0;
    if (i1 < i2)
    {
      String str = arrayOfString[i1];
      File[] arrayOfFile = a(new b(paramString + str));
      if (arrayOfFile.length == 0) {
        io.a.a.a.c.g().d("Fabric", "Can't find " + str + " data for session ID " + paramString, null);
      }
      for (;;)
      {
        i1 += 1;
        break;
        io.a.a.a.c.g().a("Fabric", "Collecting " + str + " data for session ID " + paramString);
        a(parame, arrayOfFile[0]);
      }
    }
  }
  
  private void a(e parame, Thread paramThread, Throwable paramThrowable)
    throws Exception
  {
    parame.i(1, 2);
    parame.k(b(paramThread, paramThrowable));
    a(parame, paramThread, this.A, 4, true);
    int i2 = this.y.length;
    int i1 = 0;
    while (i1 < i2)
    {
      a(parame, this.y[i1], (StackTraceElement[])this.z.get(i1), 0, false);
      i1 += 1;
    }
    a(parame, paramThrowable, 1, 2);
    parame.i(3, 2);
    parame.k(u());
    parame.a(1, g);
    parame.a(2, g);
    parame.a(3, 0L);
    parame.i(4, 2);
    parame.k(t());
    parame.a(1, 0L);
    parame.a(2, 0L);
    parame.a(3, this.r);
    if (this.s != null) {
      parame.a(4, this.s);
    }
  }
  
  private void a(e parame, Thread paramThread, Throwable paramThrowable, Map<String, String> paramMap)
    throws Exception
  {
    parame.i(3, 2);
    parame.k(a(paramThread, paramThrowable, paramMap));
    a(parame, paramThread, paramThrowable);
    if ((paramMap != null) && (!paramMap.isEmpty())) {
      a(parame, paramMap);
    }
    if (this.v != null) {
      if (this.v.importance == 100) {
        break label98;
      }
    }
    label98:
    for (boolean bool = true;; bool = false)
    {
      parame.a(3, bool);
      parame.b(4, this.B.z().getResources().getConfiguration().orientation);
      return;
    }
  }
  
  private void a(e parame, Thread paramThread, StackTraceElement[] paramArrayOfStackTraceElement, int paramInt, boolean paramBoolean)
    throws Exception
  {
    parame.i(1, 2);
    parame.k(a(paramThread, paramArrayOfStackTraceElement, paramInt, paramBoolean));
    parame.a(1, b.a(paramThread.getName()));
    parame.b(2, paramInt);
    int i1 = paramArrayOfStackTraceElement.length;
    paramInt = 0;
    while (paramInt < i1)
    {
      a(parame, 3, paramArrayOfStackTraceElement[paramInt], paramBoolean);
      paramInt += 1;
    }
  }
  
  private void a(e parame, Throwable paramThrowable, int paramInt1, int paramInt2)
    throws Exception
  {
    parame.i(paramInt2, 2);
    parame.k(a(paramThrowable, 1));
    parame.a(1, b.a(paramThrowable.getClass().getName()));
    Object localObject = paramThrowable.getLocalizedMessage();
    if (localObject != null) {
      parame.a(3, b.a((String)localObject));
    }
    localObject = paramThrowable.getStackTrace();
    int i1 = localObject.length;
    paramInt2 = 0;
    while (paramInt2 < i1)
    {
      a(parame, 4, localObject[paramInt2], true);
      paramInt2 += 1;
    }
    paramThrowable = paramThrowable.getCause();
    if (paramThrowable != null)
    {
      if (paramInt1 < this.j) {
        a(parame, paramThrowable, paramInt1 + 1, 6);
      }
    }
    else {
      return;
    }
    paramInt1 = 0;
    while (paramThrowable != null)
    {
      paramThrowable = paramThrowable.getCause();
      paramInt1 += 1;
    }
    parame.b(7, paramInt1);
  }
  
  private void a(e parame, Map<String, String> paramMap)
    throws Exception
  {
    Iterator localIterator = paramMap.entrySet().iterator();
    while (localIterator.hasNext())
    {
      paramMap = (Map.Entry)localIterator.next();
      parame.i(2, 2);
      parame.k(a((String)paramMap.getKey(), (String)paramMap.getValue()));
      parame.a(1, b.a((String)paramMap.getKey()));
      String str = (String)paramMap.getValue();
      paramMap = str;
      if (str == null) {
        paramMap = "";
      }
      parame.a(2, b.a(paramMap));
    }
  }
  
  private void a(e parame, File[] paramArrayOfFile, String paramString)
  {
    Arrays.sort(paramArrayOfFile, io.a.a.a.a.b.i.a);
    int i2 = paramArrayOfFile.length;
    int i1 = 0;
    for (;;)
    {
      if (i1 < i2)
      {
        File localFile = paramArrayOfFile[i1];
        try
        {
          io.a.a.a.c.g().a("Fabric", String.format(Locale.US, "Found Non Fatal for session ID %s in %s ", new Object[] { paramString, localFile.getName() }));
          a(parame, localFile);
          i1 += 1;
        }
        catch (Exception localException)
        {
          for (;;)
          {
            io.a.a.a.c.g().d("Fabric", "Error writting non-fatal to session.", localException);
          }
        }
      }
    }
  }
  
  private void a(g paramg)
  {
    io.a.a.a.c.g().a("Fabric", "Checking for previous crash marker.");
    File localFile = new File(this.B.r(), "crash_marker");
    if (localFile.exists())
    {
      localFile.delete();
      if (paramg == null) {}
    }
    try
    {
      paramg.a();
      return;
    }
    catch (Exception paramg)
    {
      io.a.a.a.c.g().d("Fabric", "Exception thrown by CrashlyticsListener while notifying of previous crash.", paramg);
    }
  }
  
  /* Error */
  private void a(File paramFile, String paramString, int paramInt)
  {
    // Byte code:
    //   0: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   3: ldc_w 481
    //   6: new 350	java/lang/StringBuilder
    //   9: dup
    //   10: invokespecial 351	java/lang/StringBuilder:<init>	()V
    //   13: ldc_w 738
    //   16: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   19: aload_2
    //   20: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   23: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   26: invokeinterface 488 3 0
    //   31: aload_0
    //   32: new 51	com/a/a/i$b
    //   35: dup
    //   36: new 350	java/lang/StringBuilder
    //   39: dup
    //   40: invokespecial 351	java/lang/StringBuilder:<init>	()V
    //   43: aload_2
    //   44: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   47: ldc_w 574
    //   50: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   53: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   56: invokespecial 666	com/a/a/i$b:<init>	(Ljava/lang/String;)V
    //   59: invokespecial 544	com/a/a/i:a	(Ljava/io/FilenameFilter;)[Ljava/io/File;
    //   62: astore 14
    //   64: aload 14
    //   66: ifnull +543 -> 609
    //   69: aload 14
    //   71: arraylength
    //   72: ifle +537 -> 609
    //   75: iconst_1
    //   76: istore 4
    //   78: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   81: ldc_w 481
    //   84: getstatic 200	java/util/Locale:US	Ljava/util/Locale;
    //   87: ldc_w 740
    //   90: iconst_2
    //   91: anewarray 4	java/lang/Object
    //   94: dup
    //   95: iconst_0
    //   96: aload_2
    //   97: aastore
    //   98: dup
    //   99: iconst_1
    //   100: iload 4
    //   102: invokestatic 746	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   105: aastore
    //   106: invokestatic 211	java/lang/String:format	(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   109: invokeinterface 488 3 0
    //   114: aload_0
    //   115: new 51	com/a/a/i$b
    //   118: dup
    //   119: new 350	java/lang/StringBuilder
    //   122: dup
    //   123: invokespecial 351	java/lang/StringBuilder:<init>	()V
    //   126: aload_2
    //   127: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   130: ldc_w 748
    //   133: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   136: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   139: invokespecial 666	com/a/a/i$b:<init>	(Ljava/lang/String;)V
    //   142: invokespecial 544	com/a/a/i:a	(Ljava/io/FilenameFilter;)[Ljava/io/File;
    //   145: astore 10
    //   147: aload 10
    //   149: ifnull +466 -> 615
    //   152: aload 10
    //   154: arraylength
    //   155: ifle +460 -> 615
    //   158: iconst_1
    //   159: istore 5
    //   161: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   164: ldc_w 481
    //   167: getstatic 200	java/util/Locale:US	Ljava/util/Locale;
    //   170: ldc_w 750
    //   173: iconst_2
    //   174: anewarray 4	java/lang/Object
    //   177: dup
    //   178: iconst_0
    //   179: aload_2
    //   180: aastore
    //   181: dup
    //   182: iconst_1
    //   183: iload 5
    //   185: invokestatic 746	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   188: aastore
    //   189: invokestatic 211	java/lang/String:format	(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   192: invokeinterface 488 3 0
    //   197: iload 4
    //   199: ifne +8 -> 207
    //   202: iload 5
    //   204: ifeq +557 -> 761
    //   207: aconst_null
    //   208: astore 7
    //   210: aconst_null
    //   211: astore 9
    //   213: aconst_null
    //   214: astore 6
    //   216: aconst_null
    //   217: astore 13
    //   219: aconst_null
    //   220: astore 12
    //   222: aconst_null
    //   223: astore 11
    //   225: new 572	com/a/a/d
    //   228: dup
    //   229: aload_0
    //   230: getfield 185	com/a/a/i:l	Ljava/io/File;
    //   233: aload_2
    //   234: invokespecial 575	com/a/a/d:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   237: astore 8
    //   239: aload 13
    //   241: astore 6
    //   243: aload 12
    //   245: astore 7
    //   247: aload 8
    //   249: invokestatic 578	com/a/a/e:a	(Ljava/io/OutputStream;)Lcom/a/a/e;
    //   252: astore 9
    //   254: aload 9
    //   256: astore 6
    //   258: aload 9
    //   260: astore 7
    //   262: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   265: ldc_w 481
    //   268: new 350	java/lang/StringBuilder
    //   271: dup
    //   272: invokespecial 351	java/lang/StringBuilder:<init>	()V
    //   275: ldc_w 752
    //   278: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   281: aload_2
    //   282: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   285: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   288: invokeinterface 488 3 0
    //   293: aload 9
    //   295: astore 6
    //   297: aload 9
    //   299: astore 7
    //   301: aload_0
    //   302: aload 9
    //   304: aload_1
    //   305: invokespecial 674	com/a/a/i:a	(Lcom/a/a/e;Ljava/io/File;)V
    //   308: aload 9
    //   310: astore 6
    //   312: aload 9
    //   314: astore 7
    //   316: aload 9
    //   318: iconst_4
    //   319: new 754	java/util/Date
    //   322: dup
    //   323: invokespecial 755	java/util/Date:<init>	()V
    //   326: invokevirtual 758	java/util/Date:getTime	()J
    //   329: ldc2_w 759
    //   332: ldiv
    //   333: invokevirtual 627	com/a/a/e:a	(IJ)V
    //   336: aload 9
    //   338: astore 6
    //   340: aload 9
    //   342: astore 7
    //   344: aload 9
    //   346: iconst_5
    //   347: iload 4
    //   349: invokevirtual 622	com/a/a/e:a	(IZ)V
    //   352: aload 9
    //   354: astore 6
    //   356: aload 9
    //   358: astore 7
    //   360: aload_0
    //   361: aload 9
    //   363: aload_2
    //   364: invokespecial 762	com/a/a/i:a	(Lcom/a/a/e;Ljava/lang/String;)V
    //   367: iload 5
    //   369: ifeq +130 -> 499
    //   372: aload 10
    //   374: astore_1
    //   375: aload 9
    //   377: astore 6
    //   379: aload 9
    //   381: astore 7
    //   383: aload 10
    //   385: arraylength
    //   386: iload_3
    //   387: if_icmple +96 -> 483
    //   390: aload 9
    //   392: astore 6
    //   394: aload 9
    //   396: astore 7
    //   398: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   401: ldc_w 481
    //   404: getstatic 200	java/util/Locale:US	Ljava/util/Locale;
    //   407: ldc_w 764
    //   410: iconst_1
    //   411: anewarray 4	java/lang/Object
    //   414: dup
    //   415: iconst_0
    //   416: iload_3
    //   417: invokestatic 769	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   420: aastore
    //   421: invokestatic 211	java/lang/String:format	(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   424: invokeinterface 488 3 0
    //   429: aload 9
    //   431: astore 6
    //   433: aload 9
    //   435: astore 7
    //   437: aload_0
    //   438: aload_2
    //   439: iload_3
    //   440: invokespecial 772	com/a/a/i:a	(Ljava/lang/String;I)V
    //   443: aload 9
    //   445: astore 6
    //   447: aload 9
    //   449: astore 7
    //   451: aload_0
    //   452: new 51	com/a/a/i$b
    //   455: dup
    //   456: new 350	java/lang/StringBuilder
    //   459: dup
    //   460: invokespecial 351	java/lang/StringBuilder:<init>	()V
    //   463: aload_2
    //   464: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   467: ldc_w 748
    //   470: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   473: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   476: invokespecial 666	com/a/a/i$b:<init>	(Ljava/lang/String;)V
    //   479: invokespecial 544	com/a/a/i:a	(Ljava/io/FilenameFilter;)[Ljava/io/File;
    //   482: astore_1
    //   483: aload 9
    //   485: astore 6
    //   487: aload 9
    //   489: astore 7
    //   491: aload_0
    //   492: aload 9
    //   494: aload_1
    //   495: aload_2
    //   496: invokespecial 774	com/a/a/i:a	(Lcom/a/a/e;[Ljava/io/File;Ljava/lang/String;)V
    //   499: iload 4
    //   501: ifeq +21 -> 522
    //   504: aload 9
    //   506: astore 6
    //   508: aload 9
    //   510: astore 7
    //   512: aload_0
    //   513: aload 9
    //   515: aload 14
    //   517: iconst_0
    //   518: aaload
    //   519: invokespecial 674	com/a/a/i:a	(Lcom/a/a/e;Ljava/io/File;)V
    //   522: aload 9
    //   524: astore 6
    //   526: aload 9
    //   528: astore 7
    //   530: aload 9
    //   532: bipush 11
    //   534: iconst_1
    //   535: invokevirtual 624	com/a/a/e:b	(II)V
    //   538: aload 9
    //   540: astore 6
    //   542: aload 9
    //   544: astore 7
    //   546: aload 9
    //   548: bipush 12
    //   550: iconst_3
    //   551: invokevirtual 776	com/a/a/e:c	(II)V
    //   554: aload 9
    //   556: ldc_w 778
    //   559: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   562: iconst_0
    //   563: ifeq +58 -> 621
    //   566: aload_0
    //   567: aload 8
    //   569: invokespecial 780	com/a/a/i:a	(Lcom/a/a/d;)V
    //   572: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   575: ldc_w 481
    //   578: new 350	java/lang/StringBuilder
    //   581: dup
    //   582: invokespecial 351	java/lang/StringBuilder:<init>	()V
    //   585: ldc_w 782
    //   588: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   591: aload_2
    //   592: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   595: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   598: invokeinterface 488 3 0
    //   603: aload_0
    //   604: aload_2
    //   605: invokespecial 784	com/a/a/i:a	(Ljava/lang/String;)V
    //   608: return
    //   609: iconst_0
    //   610: istore 4
    //   612: goto -534 -> 78
    //   615: iconst_0
    //   616: istore 5
    //   618: goto -457 -> 161
    //   621: aload 8
    //   623: ldc_w 786
    //   626: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   629: goto -57 -> 572
    //   632: astore_1
    //   633: aload 9
    //   635: astore 8
    //   637: aload_1
    //   638: astore 9
    //   640: aload 11
    //   642: astore_1
    //   643: aload_1
    //   644: astore 6
    //   646: aload 8
    //   648: astore 7
    //   650: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   653: ldc_w 481
    //   656: new 350	java/lang/StringBuilder
    //   659: dup
    //   660: invokespecial 351	java/lang/StringBuilder:<init>	()V
    //   663: ldc_w 788
    //   666: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   669: aload_2
    //   670: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   673: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   676: aload 9
    //   678: invokeinterface 493 4 0
    //   683: aload_1
    //   684: astore 6
    //   686: aload 8
    //   688: astore 7
    //   690: aload_0
    //   691: aload 9
    //   693: aload 8
    //   695: invokespecial 602	com/a/a/i:a	(Ljava/lang/Throwable;Ljava/io/OutputStream;)V
    //   698: aload_1
    //   699: ldc_w 778
    //   702: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   705: iconst_1
    //   706: ifeq +12 -> 718
    //   709: aload_0
    //   710: aload 8
    //   712: invokespecial 780	com/a/a/i:a	(Lcom/a/a/d;)V
    //   715: goto -143 -> 572
    //   718: aload 8
    //   720: ldc_w 786
    //   723: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   726: goto -154 -> 572
    //   729: astore_1
    //   730: aload 6
    //   732: ldc_w 778
    //   735: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   738: iconst_0
    //   739: ifeq +11 -> 750
    //   742: aload_0
    //   743: aload 7
    //   745: invokespecial 780	com/a/a/i:a	(Lcom/a/a/d;)V
    //   748: aload_1
    //   749: athrow
    //   750: aload 7
    //   752: ldc_w 786
    //   755: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   758: goto -10 -> 748
    //   761: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   764: ldc_w 481
    //   767: new 350	java/lang/StringBuilder
    //   770: dup
    //   771: invokespecial 351	java/lang/StringBuilder:<init>	()V
    //   774: ldc_w 790
    //   777: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   780: aload_2
    //   781: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   784: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   787: invokeinterface 488 3 0
    //   792: goto -220 -> 572
    //   795: astore_1
    //   796: aload 8
    //   798: astore 7
    //   800: goto -70 -> 730
    //   803: astore 9
    //   805: aload 7
    //   807: astore_1
    //   808: goto -165 -> 643
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	811	0	this	i
    //   0	811	1	paramFile	File
    //   0	811	2	paramString	String
    //   0	811	3	paramInt	int
    //   76	535	4	bool1	boolean
    //   159	458	5	bool2	boolean
    //   214	517	6	localObject1	Object
    //   208	598	7	localObject2	Object
    //   237	560	8	localObject3	Object
    //   211	481	9	localObject4	Object
    //   803	1	9	localException	Exception
    //   145	239	10	arrayOfFile1	File[]
    //   223	418	11	localObject5	Object
    //   220	24	12	localObject6	Object
    //   217	23	13	localObject7	Object
    //   62	454	14	arrayOfFile2	File[]
    // Exception table:
    //   from	to	target	type
    //   225	239	632	java/lang/Exception
    //   225	239	729	finally
    //   650	683	729	finally
    //   690	698	729	finally
    //   247	254	795	finally
    //   262	293	795	finally
    //   301	308	795	finally
    //   316	336	795	finally
    //   344	352	795	finally
    //   360	367	795	finally
    //   383	390	795	finally
    //   398	429	795	finally
    //   437	443	795	finally
    //   451	483	795	finally
    //   491	499	795	finally
    //   512	522	795	finally
    //   530	538	795	finally
    //   546	554	795	finally
    //   247	254	803	java/lang/Exception
    //   262	293	803	java/lang/Exception
    //   301	308	803	java/lang/Exception
    //   316	336	803	java/lang/Exception
    //   344	352	803	java/lang/Exception
    //   360	367	803	java/lang/Exception
    //   383	390	803	java/lang/Exception
    //   398	429	803	java/lang/Exception
    //   437	443	803	java/lang/Exception
    //   451	483	803	java/lang/Exception
    //   491	499	803	java/lang/Exception
    //   512	522	803	java/lang/Exception
    //   530	538	803	java/lang/Exception
    //   546	554	803	java/lang/Exception
  }
  
  private void a(String paramString)
  {
    paramString = b(paramString);
    int i2 = paramString.length;
    int i1 = 0;
    while (i1 < i2)
    {
      paramString[i1].delete();
      i1 += 1;
    }
  }
  
  private void a(String paramString, int paramInt)
  {
    s.a(this.l, new b(paramString + "SessionEvent"), paramInt, c);
  }
  
  private void a(Throwable paramThrowable, OutputStream paramOutputStream)
  {
    if (paramOutputStream != null) {
      b(paramThrowable, paramOutputStream);
    }
  }
  
  private void a(Throwable paramThrowable, Writer paramWriter)
  {
    int i1 = 1;
    if (paramThrowable != null) {}
    for (;;)
    {
      String str;
      try
      {
        localObject = a(paramThrowable);
        if (localObject == null) {
          break label175;
        }
      }
      catch (Exception paramThrowable)
      {
        int i2;
        int i3;
        io.a.a.a.c.g().d("Fabric", "Could not write stack trace", paramThrowable);
      }
      paramWriter.write(str + paramThrowable.getClass().getName() + ": " + (String)localObject + "\n");
      i2 = 0;
      Object localObject = paramThrowable.getStackTrace();
      i3 = localObject.length;
      i1 = 0;
      if (i1 < i3)
      {
        str = localObject[i1];
        paramWriter.write("\tat " + str.toString() + "\n");
        i1 += 1;
      }
      else
      {
        paramThrowable = paramThrowable.getCause();
        i1 = i2;
        break;
        return;
        for (;;)
        {
          if (i1 == 0) {
            break label183;
          }
          str = "";
          break;
          label175:
          localObject = "";
        }
        label183:
        str = "Caused by: ";
      }
    }
  }
  
  private void a(Date paramDate, e parame, Thread paramThread, Throwable paramThrowable, String paramString, boolean paramBoolean)
    throws Exception
  {
    Object localObject1 = this.B.z();
    long l1 = paramDate.getTime() / 1000L;
    float f1 = io.a.a.a.a.b.i.c((Context)localObject1);
    int i2 = io.a.a.a.a.b.i.a((Context)localObject1, this.x);
    boolean bool = io.a.a.a.a.b.i.d((Context)localObject1);
    int i3 = ((Context)localObject1).getResources().getConfiguration().orientation;
    long l2 = io.a.a.a.a.b.i.b() - io.a.a.a.a.b.i.b((Context)localObject1);
    long l3 = io.a.a.a.a.b.i.b(Environment.getDataDirectory().getPath());
    this.v = io.a.a.a.a.b.i.a(this.B.h(), (Context)localObject1);
    this.z = new LinkedList();
    this.A = paramThrowable.getStackTrace();
    if (paramBoolean)
    {
      paramDate = Thread.getAllStackTraces();
      this.y = new Thread[paramDate.size()];
      int i1 = 0;
      paramDate = paramDate.entrySet().iterator();
      while (paramDate.hasNext())
      {
        localObject2 = (Map.Entry)paramDate.next();
        this.y[i1] = ((Thread)((Map.Entry)localObject2).getKey());
        this.z.add(((Map.Entry)localObject2).getValue());
        i1 += 1;
      }
    }
    this.y = new Thread[0];
    Object localObject2 = a(this.w);
    if (localObject2 == null) {
      io.a.a.a.c.g().a("Fabric", "No log data to include with this event.");
    }
    io.a.a.a.a.b.i.a(this.w, "There was a problem closing the Crashlytics log file.");
    this.w = null;
    if (!io.a.a.a.a.b.i.a((Context)localObject1, "com.crashlytics.CollectCustomKeys", true)) {
      paramDate = new TreeMap();
    }
    for (;;)
    {
      parame.i(10, 2);
      parame.k(a(paramThread, paramThrowable, paramString, l1, paramDate, f1, i2, bool, i3, l2, l3, (b)localObject2));
      parame.a(1, l1);
      parame.a(2, b.a(paramString));
      a(parame, paramThread, paramThrowable, paramDate);
      a(parame, f1, i2, bool, i3, l2, l3);
      a(parame, (b)localObject2);
      return;
      localObject1 = this.B.g();
      paramDate = (Date)localObject1;
      if (localObject1 != null)
      {
        paramDate = (Date)localObject1;
        if (((Map)localObject1).size() > 1) {
          paramDate = new TreeMap((Map)localObject1);
        }
      }
    }
  }
  
  /* Error */
  private void a(Date paramDate, String paramString)
    throws Exception
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: aconst_null
    //   4: astore 7
    //   6: aconst_null
    //   7: astore_3
    //   8: aconst_null
    //   9: astore 9
    //   11: aconst_null
    //   12: astore 8
    //   14: aconst_null
    //   15: astore 6
    //   17: new 572	com/a/a/d
    //   20: dup
    //   21: aload_0
    //   22: getfield 176	com/a/a/i:B	Lcom/a/a/f;
    //   25: invokevirtual 183	com/a/a/f:r	()Ljava/io/File;
    //   28: new 350	java/lang/StringBuilder
    //   31: dup
    //   32: invokespecial 351	java/lang/StringBuilder:<init>	()V
    //   35: aload_2
    //   36: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   39: ldc_w 894
    //   42: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   45: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   48: invokespecial 575	com/a/a/d:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   51: astore 5
    //   53: aload 9
    //   55: astore_3
    //   56: aload 8
    //   58: astore 4
    //   60: aload 5
    //   62: invokestatic 578	com/a/a/e:a	(Ljava/io/OutputStream;)Lcom/a/a/e;
    //   65: astore 6
    //   67: aload 6
    //   69: astore_3
    //   70: aload 6
    //   72: astore 4
    //   74: aload 6
    //   76: iconst_1
    //   77: aload_0
    //   78: getfield 213	com/a/a/i:o	Ljava/lang/String;
    //   81: invokestatic 149	com/a/a/b:a	(Ljava/lang/String;)Lcom/a/a/b;
    //   84: invokevirtual 631	com/a/a/e:a	(ILcom/a/a/b;)V
    //   87: aload 6
    //   89: astore_3
    //   90: aload 6
    //   92: astore 4
    //   94: aload 6
    //   96: iconst_2
    //   97: aload_2
    //   98: invokestatic 149	com/a/a/b:a	(Ljava/lang/String;)Lcom/a/a/b;
    //   101: invokevirtual 631	com/a/a/e:a	(ILcom/a/a/b;)V
    //   104: aload 6
    //   106: astore_3
    //   107: aload 6
    //   109: astore 4
    //   111: aload 6
    //   113: iconst_3
    //   114: aload_1
    //   115: invokevirtual 758	java/util/Date:getTime	()J
    //   118: ldc2_w 759
    //   121: ldiv
    //   122: invokevirtual 627	com/a/a/e:a	(IJ)V
    //   125: aload 6
    //   127: ldc_w 585
    //   130: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   133: aload 5
    //   135: ldc_w 896
    //   138: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   141: return
    //   142: astore 5
    //   144: aload 7
    //   146: astore_2
    //   147: aload 6
    //   149: astore_1
    //   150: aload_1
    //   151: astore_3
    //   152: aload_2
    //   153: astore 4
    //   155: aload_0
    //   156: aload 5
    //   158: aload_2
    //   159: invokespecial 602	com/a/a/i:a	(Ljava/lang/Throwable;Ljava/io/OutputStream;)V
    //   162: aload_1
    //   163: astore_3
    //   164: aload_2
    //   165: astore 4
    //   167: aload 5
    //   169: athrow
    //   170: astore_1
    //   171: aload_3
    //   172: ldc_w 585
    //   175: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   178: aload 4
    //   180: ldc_w 896
    //   183: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   186: aload_1
    //   187: athrow
    //   188: astore_1
    //   189: aload 5
    //   191: astore 4
    //   193: goto -22 -> 171
    //   196: astore_3
    //   197: aload 5
    //   199: astore_2
    //   200: aload 4
    //   202: astore_1
    //   203: aload_3
    //   204: astore 5
    //   206: goto -56 -> 150
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	209	0	this	i
    //   0	209	1	paramDate	Date
    //   0	209	2	paramString	String
    //   7	165	3	localObject1	Object
    //   196	8	3	localException1	Exception
    //   1	200	4	localObject2	Object
    //   51	83	5	locald	d
    //   142	56	5	localException2	Exception
    //   204	1	5	localException3	Exception
    //   15	133	6	locale	e
    //   4	141	7	localObject3	Object
    //   12	45	8	localObject4	Object
    //   9	45	9	localObject5	Object
    // Exception table:
    //   from	to	target	type
    //   17	53	142	java/lang/Exception
    //   17	53	170	finally
    //   155	162	170	finally
    //   167	170	170	finally
    //   60	67	188	finally
    //   74	87	188	finally
    //   94	104	188	finally
    //   111	125	188	finally
    //   60	67	196	java/lang/Exception
    //   74	87	196	java/lang/Exception
    //   94	104	196	java/lang/Exception
    //   111	125	196	java/lang/Exception
  }
  
  private void a(Date paramDate, Thread paramThread, Throwable paramThrowable)
    throws Exception
  {
    b(paramDate, paramThread, paramThrowable);
    o();
    n();
    f();
    if (!this.B.s()) {
      v();
    }
  }
  
  private File[] a(FilenameFilter paramFilenameFilter)
  {
    return b(this.l.listFiles(paramFilenameFilter));
  }
  
  private int b(Thread paramThread, Throwable paramThrowable)
  {
    int i1 = a(paramThread, this.A, 4, true);
    i1 = 0 + (e.j(1) + e.l(i1) + i1);
    int i3 = this.y.length;
    int i2 = 0;
    while (i2 < i3)
    {
      i4 = a(this.y[i2], (StackTraceElement[])this.z.get(i2), 0, false);
      i1 += e.j(1) + e.l(i4) + i4;
      i2 += 1;
    }
    i2 = a(paramThrowable, 1);
    i3 = e.j(2);
    int i4 = e.l(i2);
    int i5 = u();
    int i6 = e.j(3);
    int i7 = e.l(i5);
    int i8 = t();
    return i1 + (i3 + i4 + i2) + (i6 + i7 + i5) + (e.j(3) + e.l(i8) + i8);
  }
  
  private <T> Future<T> b(final Callable<T> paramCallable)
  {
    try
    {
      paramCallable = this.t.submit(new Callable()
      {
        public T call()
          throws Exception
        {
          try
          {
            Object localObject = paramCallable.call();
            return (T)localObject;
          }
          catch (Exception localException)
          {
            io.a.a.a.c.g().d("Fabric", "Failed to execute task.", localException);
          }
          return null;
        }
      });
      return paramCallable;
    }
    catch (RejectedExecutionException paramCallable)
    {
      io.a.a.a.c.g().a("Fabric", "Executor is shut down because we're handling a fatal crash.");
    }
    return null;
  }
  
  /* Error */
  private void b(Throwable paramThrowable, OutputStream paramOutputStream)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aconst_null
    //   3: astore 4
    //   5: new 925	java/io/PrintWriter
    //   8: dup
    //   9: aload_2
    //   10: invokespecial 928	java/io/PrintWriter:<init>	(Ljava/io/OutputStream;)V
    //   13: astore_2
    //   14: aload_0
    //   15: aload_1
    //   16: aload_2
    //   17: invokespecial 930	com/a/a/i:a	(Ljava/lang/Throwable;Ljava/io/Writer;)V
    //   20: aload_2
    //   21: ldc_w 932
    //   24: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   27: return
    //   28: astore_2
    //   29: aload 4
    //   31: astore_1
    //   32: aload_1
    //   33: astore_3
    //   34: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   37: ldc_w 481
    //   40: ldc_w 934
    //   43: aload_2
    //   44: invokeinterface 493 4 0
    //   49: aload_1
    //   50: ldc_w 932
    //   53: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   56: return
    //   57: astore_1
    //   58: aload_3
    //   59: ldc_w 932
    //   62: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   65: aload_1
    //   66: athrow
    //   67: astore_1
    //   68: aload_2
    //   69: astore_3
    //   70: goto -12 -> 58
    //   73: astore_3
    //   74: aload_2
    //   75: astore_1
    //   76: aload_3
    //   77: astore_2
    //   78: goto -46 -> 32
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	81	0	this	i
    //   0	81	1	paramThrowable	Throwable
    //   0	81	2	paramOutputStream	OutputStream
    //   1	69	3	localObject1	Object
    //   73	4	3	localException	Exception
    //   3	27	4	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   5	14	28	java/lang/Exception
    //   5	14	57	finally
    //   34	49	57	finally
    //   14	20	67	finally
    //   14	20	73	java/lang/Exception
  }
  
  /* Error */
  private void b(Date paramDate, Thread paramThread, Throwable paramThrowable)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 8
    //   3: aconst_null
    //   4: astore 12
    //   6: aconst_null
    //   7: astore 6
    //   9: aconst_null
    //   10: astore 7
    //   12: aconst_null
    //   13: astore 13
    //   15: aconst_null
    //   16: astore 10
    //   18: aconst_null
    //   19: astore 9
    //   21: aconst_null
    //   22: astore 11
    //   24: aload 13
    //   26: astore 4
    //   28: aload 12
    //   30: astore 5
    //   32: new 187	java/io/File
    //   35: dup
    //   36: aload_0
    //   37: getfield 185	com/a/a/i:l	Ljava/io/File;
    //   40: ldc_w 723
    //   43: invokespecial 192	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   46: invokevirtual 937	java/io/File:createNewFile	()Z
    //   49: pop
    //   50: aload 13
    //   52: astore 4
    //   54: aload 12
    //   56: astore 5
    //   58: aload_0
    //   59: invokespecial 570	com/a/a/i:m	()Ljava/lang/String;
    //   62: astore 14
    //   64: aload 14
    //   66: ifnull +113 -> 179
    //   69: aload 13
    //   71: astore 4
    //   73: aload 12
    //   75: astore 5
    //   77: aload 14
    //   79: invokestatic 939	com/a/a/f:d	(Ljava/lang/String;)V
    //   82: aload 13
    //   84: astore 4
    //   86: aload 12
    //   88: astore 5
    //   90: new 572	com/a/a/d
    //   93: dup
    //   94: aload_0
    //   95: getfield 185	com/a/a/i:l	Ljava/io/File;
    //   98: new 350	java/lang/StringBuilder
    //   101: dup
    //   102: invokespecial 351	java/lang/StringBuilder:<init>	()V
    //   105: aload 14
    //   107: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   110: ldc_w 574
    //   113: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   116: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   119: invokespecial 575	com/a/a/d:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   122: astore 6
    //   124: aload 10
    //   126: astore 4
    //   128: aload 9
    //   130: astore 7
    //   132: aload 6
    //   134: invokestatic 578	com/a/a/e:a	(Ljava/io/OutputStream;)Lcom/a/a/e;
    //   137: astore 5
    //   139: aload 5
    //   141: astore 4
    //   143: aload 5
    //   145: astore 7
    //   147: aload_0
    //   148: aload_1
    //   149: aload 5
    //   151: aload_2
    //   152: aload_3
    //   153: ldc_w 941
    //   156: iconst_1
    //   157: invokespecial 943	com/a/a/i:a	(Ljava/util/Date;Lcom/a/a/e;Ljava/lang/Thread;Ljava/lang/Throwable;Ljava/lang/String;Z)V
    //   160: aload 6
    //   162: astore_1
    //   163: aload 5
    //   165: ldc_w 585
    //   168: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   171: aload_1
    //   172: ldc_w 592
    //   175: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   178: return
    //   179: aload 13
    //   181: astore 4
    //   183: aload 12
    //   185: astore 5
    //   187: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   190: ldc_w 481
    //   193: ldc_w 945
    //   196: aconst_null
    //   197: invokeinterface 493 4 0
    //   202: aload 11
    //   204: astore 5
    //   206: aload 6
    //   208: astore_1
    //   209: goto -46 -> 163
    //   212: astore_2
    //   213: aload 8
    //   215: astore_1
    //   216: aload 7
    //   218: astore 4
    //   220: aload_1
    //   221: astore 5
    //   223: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   226: ldc_w 481
    //   229: ldc_w 947
    //   232: aload_2
    //   233: invokeinterface 493 4 0
    //   238: aload 7
    //   240: astore 4
    //   242: aload_1
    //   243: astore 5
    //   245: aload_0
    //   246: aload_2
    //   247: aload_1
    //   248: invokespecial 602	com/a/a/i:a	(Ljava/lang/Throwable;Ljava/io/OutputStream;)V
    //   251: aload 7
    //   253: ldc_w 585
    //   256: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   259: aload_1
    //   260: ldc_w 592
    //   263: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   266: return
    //   267: astore_1
    //   268: aload 4
    //   270: ldc_w 585
    //   273: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   276: aload 5
    //   278: ldc_w 592
    //   281: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   284: aload_1
    //   285: athrow
    //   286: astore_1
    //   287: aload 6
    //   289: astore 5
    //   291: goto -23 -> 268
    //   294: astore_2
    //   295: aload 6
    //   297: astore_1
    //   298: goto -82 -> 216
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	301	0	this	i
    //   0	301	1	paramDate	Date
    //   0	301	2	paramThread	Thread
    //   0	301	3	paramThrowable	Throwable
    //   26	243	4	localObject1	Object
    //   30	260	5	localObject2	Object
    //   7	289	6	locald	d
    //   10	242	7	localObject3	Object
    //   1	213	8	localObject4	Object
    //   19	110	9	localObject5	Object
    //   16	109	10	localObject6	Object
    //   22	181	11	localObject7	Object
    //   4	180	12	localObject8	Object
    //   13	167	13	localObject9	Object
    //   62	44	14	str	String
    // Exception table:
    //   from	to	target	type
    //   32	50	212	java/lang/Exception
    //   58	64	212	java/lang/Exception
    //   77	82	212	java/lang/Exception
    //   90	124	212	java/lang/Exception
    //   187	202	212	java/lang/Exception
    //   32	50	267	finally
    //   58	64	267	finally
    //   77	82	267	finally
    //   90	124	267	finally
    //   187	202	267	finally
    //   223	238	267	finally
    //   245	251	267	finally
    //   132	139	286	finally
    //   147	160	286	finally
    //   132	139	294	java/lang/Exception
    //   147	160	294	java/lang/Exception
  }
  
  private File[] b(String paramString)
  {
    return a(new c(paramString));
  }
  
  private File[] b(File[] paramArrayOfFile)
  {
    File[] arrayOfFile = paramArrayOfFile;
    if (paramArrayOfFile == null) {
      arrayOfFile = new File[0];
    }
    return arrayOfFile;
  }
  
  /* Error */
  private void c(String paramString)
    throws Exception
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 8
    //   3: aconst_null
    //   4: astore 5
    //   6: aconst_null
    //   7: astore 7
    //   9: aconst_null
    //   10: astore 4
    //   12: aconst_null
    //   13: astore 11
    //   15: aconst_null
    //   16: astore 10
    //   18: aconst_null
    //   19: astore 9
    //   21: new 572	com/a/a/d
    //   24: dup
    //   25: aload_0
    //   26: getfield 185	com/a/a/i:l	Ljava/io/File;
    //   29: new 350	java/lang/StringBuilder
    //   32: dup
    //   33: invokespecial 351	java/lang/StringBuilder:<init>	()V
    //   36: aload_1
    //   37: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   40: ldc_w 659
    //   43: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   46: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   49: invokespecial 575	com/a/a/d:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   52: astore 6
    //   54: aload 11
    //   56: astore_1
    //   57: aload 10
    //   59: astore 4
    //   61: aload 6
    //   63: invokestatic 578	com/a/a/e:a	(Ljava/io/OutputStream;)Lcom/a/a/e;
    //   66: astore 5
    //   68: aload 5
    //   70: astore_1
    //   71: aload 5
    //   73: astore 4
    //   75: aload_0
    //   76: getfield 176	com/a/a/i:B	Lcom/a/a/f;
    //   79: invokevirtual 950	com/a/a/f:n	()Ljava/lang/String;
    //   82: astore 9
    //   84: aload 5
    //   86: astore_1
    //   87: aload 5
    //   89: astore 4
    //   91: aload_0
    //   92: getfield 176	com/a/a/i:B	Lcom/a/a/f;
    //   95: invokevirtual 952	com/a/a/f:p	()Ljava/lang/String;
    //   98: astore 11
    //   100: aload 5
    //   102: astore_1
    //   103: aload 5
    //   105: astore 4
    //   107: aload_0
    //   108: getfield 176	com/a/a/i:B	Lcom/a/a/f;
    //   111: invokevirtual 954	com/a/a/f:o	()Ljava/lang/String;
    //   114: astore 10
    //   116: aload 9
    //   118: ifnonnull +30 -> 148
    //   121: aload 11
    //   123: ifnonnull +25 -> 148
    //   126: aload 10
    //   128: ifnonnull +20 -> 148
    //   131: aload 5
    //   133: ldc_w 956
    //   136: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   139: aload 6
    //   141: ldc_w 958
    //   144: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   147: return
    //   148: aload 9
    //   150: astore 7
    //   152: aload 9
    //   154: ifnonnull +8 -> 162
    //   157: ldc_w 257
    //   160: astore 7
    //   162: aload 5
    //   164: astore_1
    //   165: aload 5
    //   167: astore 4
    //   169: aload 7
    //   171: invokestatic 149	com/a/a/b:a	(Ljava/lang/String;)Lcom/a/a/b;
    //   174: astore 9
    //   176: aload 11
    //   178: ifnonnull +171 -> 349
    //   181: aconst_null
    //   182: astore 7
    //   184: goto +279 -> 463
    //   187: aload 5
    //   189: astore_1
    //   190: aload 5
    //   192: astore 4
    //   194: iconst_0
    //   195: iconst_1
    //   196: aload 9
    //   198: invokestatic 282	com/a/a/e:b	(ILcom/a/a/b;)I
    //   201: iadd
    //   202: istore_3
    //   203: iload_3
    //   204: istore_2
    //   205: aload 7
    //   207: ifnull +19 -> 226
    //   210: aload 5
    //   212: astore_1
    //   213: aload 5
    //   215: astore 4
    //   217: iload_3
    //   218: iconst_2
    //   219: aload 7
    //   221: invokestatic 282	com/a/a/e:b	(ILcom/a/a/b;)I
    //   224: iadd
    //   225: istore_2
    //   226: iload_2
    //   227: istore_3
    //   228: aload 8
    //   230: ifnull +19 -> 249
    //   233: aload 5
    //   235: astore_1
    //   236: aload 5
    //   238: astore 4
    //   240: iload_2
    //   241: iconst_3
    //   242: aload 8
    //   244: invokestatic 282	com/a/a/e:b	(ILcom/a/a/b;)I
    //   247: iadd
    //   248: istore_3
    //   249: aload 5
    //   251: astore_1
    //   252: aload 5
    //   254: astore 4
    //   256: aload 5
    //   258: bipush 6
    //   260: iconst_2
    //   261: invokevirtual 612	com/a/a/e:i	(II)V
    //   264: aload 5
    //   266: astore_1
    //   267: aload 5
    //   269: astore 4
    //   271: aload 5
    //   273: iload_3
    //   274: invokevirtual 614	com/a/a/e:k	(I)V
    //   277: aload 5
    //   279: astore_1
    //   280: aload 5
    //   282: astore 4
    //   284: aload 5
    //   286: iconst_1
    //   287: aload 9
    //   289: invokevirtual 631	com/a/a/e:a	(ILcom/a/a/b;)V
    //   292: aload 7
    //   294: ifnull +18 -> 312
    //   297: aload 5
    //   299: astore_1
    //   300: aload 5
    //   302: astore 4
    //   304: aload 5
    //   306: iconst_2
    //   307: aload 7
    //   309: invokevirtual 631	com/a/a/e:a	(ILcom/a/a/b;)V
    //   312: aload 8
    //   314: ifnull +18 -> 332
    //   317: aload 5
    //   319: astore_1
    //   320: aload 5
    //   322: astore 4
    //   324: aload 5
    //   326: iconst_3
    //   327: aload 8
    //   329: invokevirtual 631	com/a/a/e:a	(ILcom/a/a/b;)V
    //   332: aload 5
    //   334: ldc_w 956
    //   337: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   340: aload 6
    //   342: ldc_w 958
    //   345: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   348: return
    //   349: aload 5
    //   351: astore_1
    //   352: aload 5
    //   354: astore 4
    //   356: aload 11
    //   358: invokestatic 149	com/a/a/b:a	(Ljava/lang/String;)Lcom/a/a/b;
    //   361: astore 7
    //   363: goto +100 -> 463
    //   366: aload 5
    //   368: astore_1
    //   369: aload 5
    //   371: astore 4
    //   373: aload 10
    //   375: invokestatic 149	com/a/a/b:a	(Ljava/lang/String;)Lcom/a/a/b;
    //   378: astore 8
    //   380: goto -193 -> 187
    //   383: astore_1
    //   384: aload 7
    //   386: astore 6
    //   388: aload_1
    //   389: astore 7
    //   391: aload 9
    //   393: astore_1
    //   394: aload_1
    //   395: astore 4
    //   397: aload 6
    //   399: astore 5
    //   401: aload_0
    //   402: aload 7
    //   404: aload 6
    //   406: invokespecial 602	com/a/a/i:a	(Ljava/lang/Throwable;Ljava/io/OutputStream;)V
    //   409: aload_1
    //   410: astore 4
    //   412: aload 6
    //   414: astore 5
    //   416: aload 7
    //   418: athrow
    //   419: astore 6
    //   421: aload 4
    //   423: astore_1
    //   424: aload_1
    //   425: ldc_w 956
    //   428: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   431: aload 5
    //   433: ldc_w 958
    //   436: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   439: aload 6
    //   441: athrow
    //   442: astore 4
    //   444: aload 6
    //   446: astore 5
    //   448: aload 4
    //   450: astore 6
    //   452: goto -28 -> 424
    //   455: astore 7
    //   457: aload 4
    //   459: astore_1
    //   460: goto -66 -> 394
    //   463: aload 10
    //   465: ifnonnull -99 -> 366
    //   468: goto -281 -> 187
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	471	0	this	i
    //   0	471	1	paramString	String
    //   204	44	2	i1	int
    //   202	72	3	i2	int
    //   10	412	4	localObject1	Object
    //   442	16	4	localObject2	Object
    //   4	443	5	localObject3	Object
    //   52	361	6	localObject4	Object
    //   419	26	6	localObject5	Object
    //   450	1	6	localObject6	Object
    //   7	410	7	localObject7	Object
    //   455	1	7	localException	Exception
    //   1	378	8	localb	b
    //   19	373	9	localObject8	Object
    //   16	448	10	str1	String
    //   13	344	11	str2	String
    // Exception table:
    //   from	to	target	type
    //   21	54	383	java/lang/Exception
    //   21	54	419	finally
    //   401	409	419	finally
    //   416	419	419	finally
    //   61	68	442	finally
    //   75	84	442	finally
    //   91	100	442	finally
    //   107	116	442	finally
    //   169	176	442	finally
    //   194	203	442	finally
    //   217	226	442	finally
    //   240	249	442	finally
    //   256	264	442	finally
    //   271	277	442	finally
    //   284	292	442	finally
    //   304	312	442	finally
    //   324	332	442	finally
    //   356	363	442	finally
    //   373	380	442	finally
    //   61	68	455	java/lang/Exception
    //   75	84	455	java/lang/Exception
    //   91	100	455	java/lang/Exception
    //   107	116	455	java/lang/Exception
    //   169	176	455	java/lang/Exception
    //   194	203	455	java/lang/Exception
    //   217	226	455	java/lang/Exception
    //   240	249	455	java/lang/Exception
    //   256	264	455	java/lang/Exception
    //   271	277	455	java/lang/Exception
    //   284	292	455	java/lang/Exception
    //   304	312	455	java/lang/Exception
    //   324	332	455	java/lang/Exception
    //   356	363	455	java/lang/Exception
    //   373	380	455	java/lang/Exception
  }
  
  /* Error */
  private void c(Date paramDate, Thread paramThread, Throwable paramThrowable)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 570	com/a/a/i:m	()Ljava/lang/String;
    //   4: astore 12
    //   6: aload 12
    //   8: ifnull +311 -> 319
    //   11: aload 12
    //   13: invokestatic 960	com/a/a/f:c	(Ljava/lang/String;)V
    //   16: aconst_null
    //   17: astore 6
    //   19: aconst_null
    //   20: astore 8
    //   22: aconst_null
    //   23: astore 11
    //   25: aconst_null
    //   26: astore 10
    //   28: aconst_null
    //   29: astore 9
    //   31: aconst_null
    //   32: astore 7
    //   34: aload 11
    //   36: astore 4
    //   38: aload 6
    //   40: astore 5
    //   42: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   45: ldc_w 481
    //   48: new 350	java/lang/StringBuilder
    //   51: dup
    //   52: invokespecial 351	java/lang/StringBuilder:<init>	()V
    //   55: ldc_w 962
    //   58: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   61: aload_3
    //   62: invokevirtual 965	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   65: ldc_w 967
    //   68: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   71: aload_2
    //   72: invokevirtual 413	java/lang/Thread:getName	()Ljava/lang/String;
    //   75: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   78: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   81: invokeinterface 488 3 0
    //   86: aload 11
    //   88: astore 4
    //   90: aload 6
    //   92: astore 5
    //   94: aload_0
    //   95: getfield 161	com/a/a/i:h	Ljava/util/concurrent/atomic/AtomicInteger;
    //   98: invokevirtual 970	java/util/concurrent/atomic/AtomicInteger:getAndIncrement	()I
    //   101: invokestatic 972	io/a/a/a/a/b/i:a	(I)Ljava/lang/String;
    //   104: astore 13
    //   106: aload 11
    //   108: astore 4
    //   110: aload 6
    //   112: astore 5
    //   114: new 350	java/lang/StringBuilder
    //   117: dup
    //   118: invokespecial 351	java/lang/StringBuilder:<init>	()V
    //   121: aload 12
    //   123: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   126: ldc_w 748
    //   129: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   132: aload 13
    //   134: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   137: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   140: astore 13
    //   142: aload 11
    //   144: astore 4
    //   146: aload 6
    //   148: astore 5
    //   150: new 572	com/a/a/d
    //   153: dup
    //   154: aload_0
    //   155: getfield 185	com/a/a/i:l	Ljava/io/File;
    //   158: aload 13
    //   160: invokespecial 575	com/a/a/d:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   163: astore 6
    //   165: aload 10
    //   167: astore 4
    //   169: aload 9
    //   171: astore 5
    //   173: aload 6
    //   175: invokestatic 578	com/a/a/e:a	(Ljava/io/OutputStream;)Lcom/a/a/e;
    //   178: astore 7
    //   180: aload 7
    //   182: astore 4
    //   184: aload 7
    //   186: astore 5
    //   188: aload_0
    //   189: aload_1
    //   190: aload 7
    //   192: aload_2
    //   193: aload_3
    //   194: ldc_w 974
    //   197: iconst_0
    //   198: invokespecial 943	com/a/a/i:a	(Ljava/util/Date;Lcom/a/a/e;Ljava/lang/Thread;Ljava/lang/Throwable;Ljava/lang/String;Z)V
    //   201: aload 7
    //   203: ldc_w 976
    //   206: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   209: aload 6
    //   211: ldc_w 978
    //   214: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   217: aload_0
    //   218: aload 12
    //   220: bipush 64
    //   222: invokespecial 772	com/a/a/i:a	(Ljava/lang/String;I)V
    //   225: return
    //   226: astore_3
    //   227: aload 8
    //   229: astore_2
    //   230: aload 7
    //   232: astore_1
    //   233: aload_1
    //   234: astore 4
    //   236: aload_2
    //   237: astore 5
    //   239: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   242: ldc_w 481
    //   245: ldc_w 980
    //   248: aload_3
    //   249: invokeinterface 493 4 0
    //   254: aload_1
    //   255: astore 4
    //   257: aload_2
    //   258: astore 5
    //   260: aload_0
    //   261: aload_3
    //   262: aload_2
    //   263: invokespecial 602	com/a/a/i:a	(Ljava/lang/Throwable;Ljava/io/OutputStream;)V
    //   266: aload_1
    //   267: ldc_w 976
    //   270: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   273: aload_2
    //   274: ldc_w 978
    //   277: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   280: goto -63 -> 217
    //   283: astore_1
    //   284: aload 4
    //   286: ldc_w 976
    //   289: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   292: aload 5
    //   294: ldc_w 978
    //   297: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   300: aload_1
    //   301: athrow
    //   302: astore_1
    //   303: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   306: ldc_w 481
    //   309: ldc_w 982
    //   312: aload_1
    //   313: invokeinterface 493 4 0
    //   318: return
    //   319: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   322: ldc_w 481
    //   325: ldc_w 984
    //   328: aconst_null
    //   329: invokeinterface 493 4 0
    //   334: return
    //   335: astore_1
    //   336: aload 6
    //   338: astore 5
    //   340: goto -56 -> 284
    //   343: astore_3
    //   344: aload 6
    //   346: astore_2
    //   347: aload 5
    //   349: astore_1
    //   350: goto -117 -> 233
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	353	0	this	i
    //   0	353	1	paramDate	Date
    //   0	353	2	paramThread	Thread
    //   0	353	3	paramThrowable	Throwable
    //   36	249	4	localObject1	Object
    //   40	308	5	localObject2	Object
    //   17	328	6	locald	d
    //   32	199	7	locale	e
    //   20	208	8	localObject3	Object
    //   29	141	9	localObject4	Object
    //   26	140	10	localObject5	Object
    //   23	120	11	localObject6	Object
    //   4	215	12	str1	String
    //   104	55	13	str2	String
    // Exception table:
    //   from	to	target	type
    //   42	86	226	java/lang/Exception
    //   94	106	226	java/lang/Exception
    //   114	142	226	java/lang/Exception
    //   150	165	226	java/lang/Exception
    //   42	86	283	finally
    //   94	106	283	finally
    //   114	142	283	finally
    //   150	165	283	finally
    //   239	254	283	finally
    //   260	266	283	finally
    //   217	225	302	java/lang/Exception
    //   173	180	335	finally
    //   188	201	335	finally
    //   173	180	343	java/lang/Exception
    //   188	201	343	java/lang/Exception
  }
  
  /* Error */
  private void d(String paramString)
    throws Exception
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: aconst_null
    //   4: astore 6
    //   6: aconst_null
    //   7: astore_3
    //   8: aconst_null
    //   9: astore 9
    //   11: aconst_null
    //   12: astore 8
    //   14: aconst_null
    //   15: astore 7
    //   17: new 572	com/a/a/d
    //   20: dup
    //   21: aload_0
    //   22: getfield 176	com/a/a/i:B	Lcom/a/a/f;
    //   25: invokevirtual 183	com/a/a/f:r	()Ljava/io/File;
    //   28: new 350	java/lang/StringBuilder
    //   31: dup
    //   32: invokespecial 351	java/lang/StringBuilder:<init>	()V
    //   35: aload_1
    //   36: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   39: ldc_w 661
    //   42: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   45: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   48: invokespecial 575	com/a/a/d:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   51: astore 5
    //   53: aload 9
    //   55: astore_1
    //   56: aload 8
    //   58: astore_3
    //   59: aload 5
    //   61: invokestatic 578	com/a/a/e:a	(Ljava/io/OutputStream;)Lcom/a/a/e;
    //   64: astore 4
    //   66: aload 4
    //   68: astore_1
    //   69: aload 4
    //   71: astore_3
    //   72: aload_0
    //   73: getfield 176	com/a/a/i:B	Lcom/a/a/f;
    //   76: invokevirtual 220	com/a/a/f:h	()Ljava/lang/String;
    //   79: invokestatic 149	com/a/a/b:a	(Ljava/lang/String;)Lcom/a/a/b;
    //   82: astore 6
    //   84: aload 4
    //   86: astore_1
    //   87: aload 4
    //   89: astore_3
    //   90: aload_0
    //   91: getfield 176	com/a/a/i:B	Lcom/a/a/f;
    //   94: invokevirtual 989	com/a/a/f:k	()Ljava/lang/String;
    //   97: invokestatic 149	com/a/a/b:a	(Ljava/lang/String;)Lcom/a/a/b;
    //   100: astore 7
    //   102: aload 4
    //   104: astore_1
    //   105: aload 4
    //   107: astore_3
    //   108: aload_0
    //   109: getfield 176	com/a/a/i:B	Lcom/a/a/f;
    //   112: invokevirtual 991	com/a/a/f:j	()Ljava/lang/String;
    //   115: invokestatic 149	com/a/a/b:a	(Ljava/lang/String;)Lcom/a/a/b;
    //   118: astore 8
    //   120: aload 4
    //   122: astore_1
    //   123: aload 4
    //   125: astore_3
    //   126: aload_0
    //   127: getfield 174	com/a/a/i:u	Lio/a/a/a/a/b/o;
    //   130: invokevirtual 995	io/a/a/a/a/b/o:b	()Ljava/lang/String;
    //   133: invokestatic 149	com/a/a/b:a	(Ljava/lang/String;)Lcom/a/a/b;
    //   136: astore 9
    //   138: aload 4
    //   140: astore_1
    //   141: aload 4
    //   143: astore_3
    //   144: aload_0
    //   145: getfield 176	com/a/a/i:B	Lcom/a/a/f;
    //   148: invokevirtual 997	com/a/a/f:i	()Ljava/lang/String;
    //   151: invokestatic 1002	io/a/a/a/a/b/l:a	(Ljava/lang/String;)Lio/a/a/a/a/b/l;
    //   154: invokevirtual 1004	io/a/a/a/a/b/l:a	()I
    //   157: istore_2
    //   158: aload 4
    //   160: astore_1
    //   161: aload 4
    //   163: astore_3
    //   164: aload 4
    //   166: bipush 7
    //   168: iconst_2
    //   169: invokevirtual 612	com/a/a/e:i	(II)V
    //   172: aload 4
    //   174: astore_1
    //   175: aload 4
    //   177: astore_3
    //   178: aload 4
    //   180: aload_0
    //   181: aload 6
    //   183: aload 7
    //   185: aload 8
    //   187: aload 9
    //   189: iload_2
    //   190: invokespecial 1006	com/a/a/i:a	(Lcom/a/a/b;Lcom/a/a/b;Lcom/a/a/b;Lcom/a/a/b;I)I
    //   193: invokevirtual 614	com/a/a/e:k	(I)V
    //   196: aload 4
    //   198: astore_1
    //   199: aload 4
    //   201: astore_3
    //   202: aload 4
    //   204: iconst_1
    //   205: aload 6
    //   207: invokevirtual 631	com/a/a/e:a	(ILcom/a/a/b;)V
    //   210: aload 4
    //   212: astore_1
    //   213: aload 4
    //   215: astore_3
    //   216: aload 4
    //   218: iconst_2
    //   219: aload 7
    //   221: invokevirtual 631	com/a/a/e:a	(ILcom/a/a/b;)V
    //   224: aload 4
    //   226: astore_1
    //   227: aload 4
    //   229: astore_3
    //   230: aload 4
    //   232: iconst_3
    //   233: aload 8
    //   235: invokevirtual 631	com/a/a/e:a	(ILcom/a/a/b;)V
    //   238: aload 4
    //   240: astore_1
    //   241: aload 4
    //   243: astore_3
    //   244: aload 4
    //   246: iconst_5
    //   247: iconst_2
    //   248: invokevirtual 612	com/a/a/e:i	(II)V
    //   251: aload 4
    //   253: astore_1
    //   254: aload 4
    //   256: astore_3
    //   257: aload 4
    //   259: aload_0
    //   260: invokespecial 331	com/a/a/i:s	()I
    //   263: invokevirtual 614	com/a/a/e:k	(I)V
    //   266: aload 4
    //   268: astore_1
    //   269: aload 4
    //   271: astore_3
    //   272: aload 4
    //   274: iconst_1
    //   275: aload_0
    //   276: getfield 176	com/a/a/i:B	Lcom/a/a/f;
    //   279: invokevirtual 244	com/a/a/f:z	()Landroid/content/Context;
    //   282: invokestatic 1011	io/a/a/a/a/b/g:a	(Landroid/content/Context;)Ljava/lang/String;
    //   285: invokevirtual 1014	com/a/a/e:a	(ILjava/lang/String;)V
    //   288: aload 4
    //   290: astore_1
    //   291: aload 4
    //   293: astore_3
    //   294: aload 4
    //   296: bipush 6
    //   298: aload 9
    //   300: invokevirtual 631	com/a/a/e:a	(ILcom/a/a/b;)V
    //   303: aload 4
    //   305: astore_1
    //   306: aload 4
    //   308: astore_3
    //   309: aload 4
    //   311: bipush 10
    //   313: iload_2
    //   314: invokevirtual 776	com/a/a/e:c	(II)V
    //   317: aload 4
    //   319: ldc_w 1016
    //   322: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   325: aload 5
    //   327: ldc_w 1018
    //   330: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   333: return
    //   334: astore_1
    //   335: aload 6
    //   337: astore 5
    //   339: aload_1
    //   340: astore 6
    //   342: aload 7
    //   344: astore_1
    //   345: aload_1
    //   346: astore_3
    //   347: aload 5
    //   349: astore 4
    //   351: aload_0
    //   352: aload 6
    //   354: aload 5
    //   356: invokespecial 602	com/a/a/i:a	(Ljava/lang/Throwable;Ljava/io/OutputStream;)V
    //   359: aload_1
    //   360: astore_3
    //   361: aload 5
    //   363: astore 4
    //   365: aload 6
    //   367: athrow
    //   368: astore 5
    //   370: aload_3
    //   371: astore_1
    //   372: aload 5
    //   374: astore_3
    //   375: aload_1
    //   376: ldc_w 1016
    //   379: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   382: aload 4
    //   384: ldc_w 1018
    //   387: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   390: aload_3
    //   391: athrow
    //   392: astore_3
    //   393: aload 5
    //   395: astore 4
    //   397: goto -22 -> 375
    //   400: astore 6
    //   402: aload_3
    //   403: astore_1
    //   404: goto -59 -> 345
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	407	0	this	i
    //   0	407	1	paramString	String
    //   157	157	2	i1	int
    //   7	384	3	localObject1	Object
    //   392	11	3	localObject2	Object
    //   1	395	4	localObject3	Object
    //   51	311	5	localObject4	Object
    //   368	26	5	localObject5	Object
    //   4	362	6	localObject6	Object
    //   400	1	6	localException	Exception
    //   15	328	7	localb1	b
    //   12	222	8	localb2	b
    //   9	290	9	localb3	b
    // Exception table:
    //   from	to	target	type
    //   17	53	334	java/lang/Exception
    //   17	53	368	finally
    //   351	359	368	finally
    //   365	368	368	finally
    //   59	66	392	finally
    //   72	84	392	finally
    //   90	102	392	finally
    //   108	120	392	finally
    //   126	138	392	finally
    //   144	158	392	finally
    //   164	172	392	finally
    //   178	196	392	finally
    //   202	210	392	finally
    //   216	224	392	finally
    //   230	238	392	finally
    //   244	251	392	finally
    //   257	266	392	finally
    //   272	288	392	finally
    //   294	303	392	finally
    //   309	317	392	finally
    //   59	66	400	java/lang/Exception
    //   72	84	400	java/lang/Exception
    //   90	102	400	java/lang/Exception
    //   108	120	400	java/lang/Exception
    //   126	138	400	java/lang/Exception
    //   144	158	400	java/lang/Exception
    //   164	172	400	java/lang/Exception
    //   178	196	400	java/lang/Exception
    //   202	210	400	java/lang/Exception
    //   216	224	400	java/lang/Exception
    //   230	238	400	java/lang/Exception
    //   244	251	400	java/lang/Exception
    //   257	266	400	java/lang/Exception
    //   272	288	400	java/lang/Exception
    //   294	303	400	java/lang/Exception
    //   309	317	400	java/lang/Exception
  }
  
  /* Error */
  private void e(String paramString)
    throws Exception
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 5
    //   3: aconst_null
    //   4: astore 6
    //   6: aconst_null
    //   7: astore 10
    //   9: aconst_null
    //   10: astore 9
    //   12: aconst_null
    //   13: astore 8
    //   15: aconst_null
    //   16: astore 7
    //   18: aload 10
    //   20: astore_3
    //   21: aload 5
    //   23: astore 4
    //   25: aload_0
    //   26: getfield 176	com/a/a/i:B	Lcom/a/a/f;
    //   29: invokevirtual 244	com/a/a/f:z	()Landroid/content/Context;
    //   32: astore 11
    //   34: aload 10
    //   36: astore_3
    //   37: aload 5
    //   39: astore 4
    //   41: new 572	com/a/a/d
    //   44: dup
    //   45: aload_0
    //   46: getfield 176	com/a/a/i:B	Lcom/a/a/f;
    //   49: invokevirtual 183	com/a/a/f:r	()Ljava/io/File;
    //   52: new 350	java/lang/StringBuilder
    //   55: dup
    //   56: invokespecial 351	java/lang/StringBuilder:<init>	()V
    //   59: aload_1
    //   60: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   63: ldc_w 663
    //   66: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   69: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   72: invokespecial 575	com/a/a/d:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   75: astore 5
    //   77: aload 9
    //   79: astore_1
    //   80: aload 8
    //   82: astore_3
    //   83: aload 5
    //   85: invokestatic 578	com/a/a/e:a	(Ljava/io/OutputStream;)Lcom/a/a/e;
    //   88: astore 4
    //   90: aload 4
    //   92: astore_1
    //   93: aload 4
    //   95: astore_3
    //   96: getstatic 1024	android/os/Build$VERSION:RELEASE	Ljava/lang/String;
    //   99: invokestatic 149	com/a/a/b:a	(Ljava/lang/String;)Lcom/a/a/b;
    //   102: astore 6
    //   104: aload 4
    //   106: astore_1
    //   107: aload 4
    //   109: astore_3
    //   110: getstatic 1027	android/os/Build$VERSION:CODENAME	Ljava/lang/String;
    //   113: invokestatic 149	com/a/a/b:a	(Ljava/lang/String;)Lcom/a/a/b;
    //   116: astore 7
    //   118: aload 4
    //   120: astore_1
    //   121: aload 4
    //   123: astore_3
    //   124: aload 11
    //   126: invokestatic 1029	io/a/a/a/a/b/i:g	(Landroid/content/Context;)Z
    //   129: istore_2
    //   130: aload 4
    //   132: astore_1
    //   133: aload 4
    //   135: astore_3
    //   136: aload 4
    //   138: bipush 8
    //   140: iconst_2
    //   141: invokevirtual 612	com/a/a/e:i	(II)V
    //   144: aload 4
    //   146: astore_1
    //   147: aload 4
    //   149: astore_3
    //   150: aload 4
    //   152: aload_0
    //   153: aload 6
    //   155: aload 7
    //   157: iload_2
    //   158: invokespecial 1031	com/a/a/i:a	(Lcom/a/a/b;Lcom/a/a/b;Z)I
    //   161: invokevirtual 614	com/a/a/e:k	(I)V
    //   164: aload 4
    //   166: astore_1
    //   167: aload 4
    //   169: astore_3
    //   170: aload 4
    //   172: iconst_1
    //   173: iconst_3
    //   174: invokevirtual 776	com/a/a/e:c	(II)V
    //   177: aload 4
    //   179: astore_1
    //   180: aload 4
    //   182: astore_3
    //   183: aload 4
    //   185: iconst_2
    //   186: aload 6
    //   188: invokevirtual 631	com/a/a/e:a	(ILcom/a/a/b;)V
    //   191: aload 4
    //   193: astore_1
    //   194: aload 4
    //   196: astore_3
    //   197: aload 4
    //   199: iconst_3
    //   200: aload 7
    //   202: invokevirtual 631	com/a/a/e:a	(ILcom/a/a/b;)V
    //   205: aload 4
    //   207: astore_1
    //   208: aload 4
    //   210: astore_3
    //   211: aload 4
    //   213: iconst_4
    //   214: iload_2
    //   215: invokevirtual 622	com/a/a/e:a	(IZ)V
    //   218: aload 4
    //   220: ldc_w 1033
    //   223: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   226: aload 5
    //   228: ldc_w 1035
    //   231: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   234: return
    //   235: astore_1
    //   236: aload 6
    //   238: astore 5
    //   240: aload_1
    //   241: astore 6
    //   243: aload 7
    //   245: astore_1
    //   246: aload_1
    //   247: astore_3
    //   248: aload 5
    //   250: astore 4
    //   252: aload_0
    //   253: aload 6
    //   255: aload 5
    //   257: invokespecial 602	com/a/a/i:a	(Ljava/lang/Throwable;Ljava/io/OutputStream;)V
    //   260: aload_1
    //   261: astore_3
    //   262: aload 5
    //   264: astore 4
    //   266: aload 6
    //   268: athrow
    //   269: astore 5
    //   271: aload_3
    //   272: astore_1
    //   273: aload_1
    //   274: ldc_w 1033
    //   277: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   280: aload 4
    //   282: ldc_w 1035
    //   285: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   288: aload 5
    //   290: athrow
    //   291: astore_3
    //   292: aload 5
    //   294: astore 4
    //   296: aload_3
    //   297: astore 5
    //   299: goto -26 -> 273
    //   302: astore 6
    //   304: aload_3
    //   305: astore_1
    //   306: goto -60 -> 246
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	309	0	this	i
    //   0	309	1	paramString	String
    //   129	86	2	bool	boolean
    //   20	252	3	localObject1	Object
    //   291	14	3	localObject2	Object
    //   23	272	4	localObject3	Object
    //   1	262	5	localObject4	Object
    //   269	24	5	localObject5	Object
    //   297	1	5	localObject6	Object
    //   4	263	6	localObject7	Object
    //   302	1	6	localException	Exception
    //   16	228	7	localb	b
    //   13	68	8	localObject8	Object
    //   10	68	9	localObject9	Object
    //   7	28	10	localObject10	Object
    //   32	93	11	localContext	Context
    // Exception table:
    //   from	to	target	type
    //   25	34	235	java/lang/Exception
    //   41	77	235	java/lang/Exception
    //   25	34	269	finally
    //   41	77	269	finally
    //   252	260	269	finally
    //   266	269	269	finally
    //   83	90	291	finally
    //   96	104	291	finally
    //   110	118	291	finally
    //   124	130	291	finally
    //   136	144	291	finally
    //   150	164	291	finally
    //   170	177	291	finally
    //   183	191	291	finally
    //   197	205	291	finally
    //   211	218	291	finally
    //   83	90	302	java/lang/Exception
    //   96	104	302	java/lang/Exception
    //   110	118	302	java/lang/Exception
    //   124	130	302	java/lang/Exception
    //   136	144	302	java/lang/Exception
    //   150	164	302	java/lang/Exception
    //   170	177	302	java/lang/Exception
    //   183	191	302	java/lang/Exception
    //   197	205	302	java/lang/Exception
    //   211	218	302	java/lang/Exception
  }
  
  /* Error */
  private void f(String paramString)
    throws Exception
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 12
    //   3: aconst_null
    //   4: astore 14
    //   6: aconst_null
    //   7: astore 13
    //   9: aconst_null
    //   10: astore 17
    //   12: aconst_null
    //   13: astore 15
    //   15: aconst_null
    //   16: astore 16
    //   18: aload 17
    //   20: astore 10
    //   22: aload 12
    //   24: astore 11
    //   26: aload_0
    //   27: getfield 176	com/a/a/i:B	Lcom/a/a/f;
    //   30: invokevirtual 244	com/a/a/f:z	()Landroid/content/Context;
    //   33: astore 18
    //   35: aload 17
    //   37: astore 10
    //   39: aload 12
    //   41: astore 11
    //   43: new 572	com/a/a/d
    //   46: dup
    //   47: aload_0
    //   48: getfield 176	com/a/a/i:B	Lcom/a/a/f;
    //   51: invokevirtual 183	com/a/a/f:r	()Ljava/io/File;
    //   54: new 350	java/lang/StringBuilder
    //   57: dup
    //   58: invokespecial 351	java/lang/StringBuilder:<init>	()V
    //   61: aload_1
    //   62: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   65: ldc_w 665
    //   68: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   71: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   74: invokespecial 575	com/a/a/d:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   77: astore 12
    //   79: aload 16
    //   81: astore_1
    //   82: aload 15
    //   84: astore 10
    //   86: aload 12
    //   88: invokestatic 578	com/a/a/e:a	(Ljava/io/OutputStream;)Lcom/a/a/e;
    //   91: astore 11
    //   93: aload 11
    //   95: astore_1
    //   96: aload 11
    //   98: astore 10
    //   100: new 1037	android/os/StatFs
    //   103: dup
    //   104: invokestatic 841	android/os/Environment:getDataDirectory	()Ljava/io/File;
    //   107: invokevirtual 844	java/io/File:getPath	()Ljava/lang/String;
    //   110: invokespecial 1038	android/os/StatFs:<init>	(Ljava/lang/String;)V
    //   113: astore 16
    //   115: aload 11
    //   117: astore_1
    //   118: aload 11
    //   120: astore 10
    //   122: invokestatic 1039	io/a/a/a/a/b/i:a	()I
    //   125: istore_2
    //   126: aload 11
    //   128: astore_1
    //   129: aload 11
    //   131: astore 10
    //   133: aload_0
    //   134: getstatic 1044	android/os/Build:MODEL	Ljava/lang/String;
    //   137: invokespecial 1046	com/a/a/i:g	(Ljava/lang/String;)Lcom/a/a/b;
    //   140: astore 15
    //   142: aload 11
    //   144: astore_1
    //   145: aload 11
    //   147: astore 10
    //   149: aload_0
    //   150: getstatic 1049	android/os/Build:MANUFACTURER	Ljava/lang/String;
    //   153: invokespecial 1046	com/a/a/i:g	(Ljava/lang/String;)Lcom/a/a/b;
    //   156: astore 13
    //   158: aload 11
    //   160: astore_1
    //   161: aload 11
    //   163: astore 10
    //   165: aload_0
    //   166: getstatic 1052	android/os/Build:PRODUCT	Ljava/lang/String;
    //   169: invokespecial 1046	com/a/a/i:g	(Ljava/lang/String;)Lcom/a/a/b;
    //   172: astore 14
    //   174: aload 11
    //   176: astore_1
    //   177: aload 11
    //   179: astore 10
    //   181: invokestatic 1058	java/lang/Runtime:getRuntime	()Ljava/lang/Runtime;
    //   184: invokevirtual 1061	java/lang/Runtime:availableProcessors	()I
    //   187: istore_3
    //   188: aload 11
    //   190: astore_1
    //   191: aload 11
    //   193: astore 10
    //   195: invokestatic 833	io/a/a/a/a/b/i:b	()J
    //   198: lstore 5
    //   200: aload 11
    //   202: astore_1
    //   203: aload 11
    //   205: astore 10
    //   207: aload 16
    //   209: invokevirtual 1064	android/os/StatFs:getBlockCount	()I
    //   212: i2l
    //   213: aload 16
    //   215: invokevirtual 1067	android/os/StatFs:getBlockSize	()I
    //   218: i2l
    //   219: lmul
    //   220: lstore 7
    //   222: aload 11
    //   224: astore_1
    //   225: aload 11
    //   227: astore 10
    //   229: aload 18
    //   231: invokestatic 1069	io/a/a/a/a/b/i:f	(Landroid/content/Context;)Z
    //   234: istore 9
    //   236: aload 11
    //   238: astore_1
    //   239: aload 11
    //   241: astore 10
    //   243: aload_0
    //   244: getfield 174	com/a/a/i:u	Lio/a/a/a/a/b/o;
    //   247: invokevirtual 1071	io/a/a/a/a/b/o:f	()Ljava/lang/String;
    //   250: invokestatic 149	com/a/a/b:a	(Ljava/lang/String;)Lcom/a/a/b;
    //   253: astore 16
    //   255: aload 11
    //   257: astore_1
    //   258: aload 11
    //   260: astore 10
    //   262: aload_0
    //   263: getfield 174	com/a/a/i:u	Lio/a/a/a/a/b/o;
    //   266: invokevirtual 1072	io/a/a/a/a/b/o:g	()Ljava/util/Map;
    //   269: astore 17
    //   271: aload 11
    //   273: astore_1
    //   274: aload 11
    //   276: astore 10
    //   278: aload 18
    //   280: invokestatic 1075	io/a/a/a/a/b/i:h	(Landroid/content/Context;)I
    //   283: istore 4
    //   285: aload 11
    //   287: astore_1
    //   288: aload 11
    //   290: astore 10
    //   292: aload 11
    //   294: bipush 9
    //   296: iconst_2
    //   297: invokevirtual 612	com/a/a/e:i	(II)V
    //   300: aload 11
    //   302: astore_1
    //   303: aload 11
    //   305: astore 10
    //   307: aload 11
    //   309: aload_0
    //   310: iload_2
    //   311: aload 16
    //   313: aload 15
    //   315: iload_3
    //   316: lload 5
    //   318: lload 7
    //   320: iload 9
    //   322: aload 17
    //   324: iload 4
    //   326: aload 13
    //   328: aload 14
    //   330: invokespecial 1077	com/a/a/i:a	(ILcom/a/a/b;Lcom/a/a/b;IJJZLjava/util/Map;ILcom/a/a/b;Lcom/a/a/b;)I
    //   333: invokevirtual 614	com/a/a/e:k	(I)V
    //   336: aload 11
    //   338: astore_1
    //   339: aload 11
    //   341: astore 10
    //   343: aload 11
    //   345: iconst_1
    //   346: aload 16
    //   348: invokevirtual 631	com/a/a/e:a	(ILcom/a/a/b;)V
    //   351: aload 11
    //   353: astore_1
    //   354: aload 11
    //   356: astore 10
    //   358: aload 11
    //   360: iconst_3
    //   361: iload_2
    //   362: invokevirtual 776	com/a/a/e:c	(II)V
    //   365: aload 11
    //   367: astore_1
    //   368: aload 11
    //   370: astore 10
    //   372: aload 11
    //   374: iconst_4
    //   375: aload 15
    //   377: invokevirtual 631	com/a/a/e:a	(ILcom/a/a/b;)V
    //   380: aload 11
    //   382: astore_1
    //   383: aload 11
    //   385: astore 10
    //   387: aload 11
    //   389: iconst_5
    //   390: iload_3
    //   391: invokevirtual 624	com/a/a/e:b	(II)V
    //   394: aload 11
    //   396: astore_1
    //   397: aload 11
    //   399: astore 10
    //   401: aload 11
    //   403: bipush 6
    //   405: lload 5
    //   407: invokevirtual 627	com/a/a/e:a	(IJ)V
    //   410: aload 11
    //   412: astore_1
    //   413: aload 11
    //   415: astore 10
    //   417: aload 11
    //   419: bipush 7
    //   421: lload 7
    //   423: invokevirtual 627	com/a/a/e:a	(IJ)V
    //   426: aload 11
    //   428: astore_1
    //   429: aload 11
    //   431: astore 10
    //   433: aload 11
    //   435: bipush 10
    //   437: iload 9
    //   439: invokevirtual 622	com/a/a/e:a	(IZ)V
    //   442: aload 11
    //   444: astore_1
    //   445: aload 11
    //   447: astore 10
    //   449: aload 17
    //   451: invokeinterface 290 1 0
    //   456: invokeinterface 296 1 0
    //   461: astore 15
    //   463: aload 11
    //   465: astore_1
    //   466: aload 11
    //   468: astore 10
    //   470: aload 15
    //   472: invokeinterface 302 1 0
    //   477: ifeq +174 -> 651
    //   480: aload 11
    //   482: astore_1
    //   483: aload 11
    //   485: astore 10
    //   487: aload 15
    //   489: invokeinterface 306 1 0
    //   494: checkcast 308	java/util/Map$Entry
    //   497: astore 16
    //   499: aload 11
    //   501: astore_1
    //   502: aload 11
    //   504: astore 10
    //   506: aload 11
    //   508: bipush 11
    //   510: iconst_2
    //   511: invokevirtual 612	com/a/a/e:i	(II)V
    //   514: aload 11
    //   516: astore_1
    //   517: aload 11
    //   519: astore 10
    //   521: aload 11
    //   523: aload_0
    //   524: aload 16
    //   526: invokeinterface 311 1 0
    //   531: checkcast 313	io/a/a/a/a/b/o$a
    //   534: aload 16
    //   536: invokeinterface 316 1 0
    //   541: checkcast 207	java/lang/String
    //   544: invokespecial 319	com/a/a/i:a	(Lio/a/a/a/a/b/o$a;Ljava/lang/String;)I
    //   547: invokevirtual 614	com/a/a/e:k	(I)V
    //   550: aload 11
    //   552: astore_1
    //   553: aload 11
    //   555: astore 10
    //   557: aload 11
    //   559: iconst_1
    //   560: aload 16
    //   562: invokeinterface 311 1 0
    //   567: checkcast 313	io/a/a/a/a/b/o$a
    //   570: getfield 334	io/a/a/a/a/b/o$a:h	I
    //   573: invokevirtual 776	com/a/a/e:c	(II)V
    //   576: aload 11
    //   578: astore_1
    //   579: aload 11
    //   581: astore 10
    //   583: aload 11
    //   585: iconst_2
    //   586: aload 16
    //   588: invokeinterface 316 1 0
    //   593: checkcast 207	java/lang/String
    //   596: invokestatic 149	com/a/a/b:a	(Ljava/lang/String;)Lcom/a/a/b;
    //   599: invokevirtual 631	com/a/a/e:a	(ILcom/a/a/b;)V
    //   602: goto -139 -> 463
    //   605: astore 13
    //   607: aload_1
    //   608: astore 10
    //   610: aload 12
    //   612: astore 11
    //   614: aload_0
    //   615: aload 13
    //   617: aload 12
    //   619: invokespecial 602	com/a/a/i:a	(Ljava/lang/Throwable;Ljava/io/OutputStream;)V
    //   622: aload_1
    //   623: astore 10
    //   625: aload 12
    //   627: astore 11
    //   629: aload 13
    //   631: athrow
    //   632: astore_1
    //   633: aload 10
    //   635: ldc_w 1079
    //   638: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   641: aload 11
    //   643: ldc_w 1081
    //   646: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   649: aload_1
    //   650: athrow
    //   651: aload 11
    //   653: astore_1
    //   654: aload 11
    //   656: astore 10
    //   658: aload 11
    //   660: bipush 12
    //   662: iload 4
    //   664: invokevirtual 624	com/a/a/e:b	(II)V
    //   667: aload 13
    //   669: ifnull +19 -> 688
    //   672: aload 11
    //   674: astore_1
    //   675: aload 11
    //   677: astore 10
    //   679: aload 11
    //   681: bipush 13
    //   683: aload 13
    //   685: invokevirtual 631	com/a/a/e:a	(ILcom/a/a/b;)V
    //   688: aload 14
    //   690: ifnull +19 -> 709
    //   693: aload 11
    //   695: astore_1
    //   696: aload 11
    //   698: astore 10
    //   700: aload 11
    //   702: bipush 14
    //   704: aload 14
    //   706: invokevirtual 631	com/a/a/e:a	(ILcom/a/a/b;)V
    //   709: aload 11
    //   711: ldc_w 1079
    //   714: invokestatic 590	io/a/a/a/a/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   717: aload 12
    //   719: ldc_w 1081
    //   722: invokestatic 595	io/a/a/a/a/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   725: return
    //   726: astore_1
    //   727: aload 12
    //   729: astore 11
    //   731: goto -98 -> 633
    //   734: astore 10
    //   736: aload 13
    //   738: astore_1
    //   739: aload 10
    //   741: astore 13
    //   743: aload 14
    //   745: astore 12
    //   747: goto -140 -> 607
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	750	0	this	i
    //   0	750	1	paramString	String
    //   125	237	2	i1	int
    //   187	204	3	i2	int
    //   283	380	4	i3	int
    //   198	208	5	l1	long
    //   220	202	7	l2	long
    //   234	204	9	bool	boolean
    //   20	679	10	localObject1	Object
    //   734	6	10	localException1	Exception
    //   24	706	11	localObject2	Object
    //   1	745	12	localObject3	Object
    //   7	320	13	localb1	b
    //   605	132	13	localException2	Exception
    //   741	1	13	localObject4	Object
    //   4	740	14	localb2	b
    //   13	475	15	localObject5	Object
    //   16	571	16	localObject6	Object
    //   10	440	17	localMap	Map
    //   33	246	18	localContext	Context
    // Exception table:
    //   from	to	target	type
    //   86	93	605	java/lang/Exception
    //   100	115	605	java/lang/Exception
    //   122	126	605	java/lang/Exception
    //   133	142	605	java/lang/Exception
    //   149	158	605	java/lang/Exception
    //   165	174	605	java/lang/Exception
    //   181	188	605	java/lang/Exception
    //   195	200	605	java/lang/Exception
    //   207	222	605	java/lang/Exception
    //   229	236	605	java/lang/Exception
    //   243	255	605	java/lang/Exception
    //   262	271	605	java/lang/Exception
    //   278	285	605	java/lang/Exception
    //   292	300	605	java/lang/Exception
    //   307	336	605	java/lang/Exception
    //   343	351	605	java/lang/Exception
    //   358	365	605	java/lang/Exception
    //   372	380	605	java/lang/Exception
    //   387	394	605	java/lang/Exception
    //   401	410	605	java/lang/Exception
    //   417	426	605	java/lang/Exception
    //   433	442	605	java/lang/Exception
    //   449	463	605	java/lang/Exception
    //   470	480	605	java/lang/Exception
    //   487	499	605	java/lang/Exception
    //   506	514	605	java/lang/Exception
    //   521	550	605	java/lang/Exception
    //   557	576	605	java/lang/Exception
    //   583	602	605	java/lang/Exception
    //   658	667	605	java/lang/Exception
    //   679	688	605	java/lang/Exception
    //   700	709	605	java/lang/Exception
    //   26	35	632	finally
    //   43	79	632	finally
    //   614	622	632	finally
    //   629	632	632	finally
    //   86	93	726	finally
    //   100	115	726	finally
    //   122	126	726	finally
    //   133	142	726	finally
    //   149	158	726	finally
    //   165	174	726	finally
    //   181	188	726	finally
    //   195	200	726	finally
    //   207	222	726	finally
    //   229	236	726	finally
    //   243	255	726	finally
    //   262	271	726	finally
    //   278	285	726	finally
    //   292	300	726	finally
    //   307	336	726	finally
    //   343	351	726	finally
    //   358	365	726	finally
    //   372	380	726	finally
    //   387	394	726	finally
    //   401	410	726	finally
    //   417	426	726	finally
    //   433	442	726	finally
    //   449	463	726	finally
    //   470	480	726	finally
    //   487	499	726	finally
    //   506	514	726	finally
    //   521	550	726	finally
    //   557	576	726	finally
    //   583	602	726	finally
    //   658	667	726	finally
    //   679	688	726	finally
    //   700	709	726	finally
    //   26	35	734	java/lang/Exception
    //   43	79	734	java/lang/Exception
  }
  
  private b g(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return b.a(paramString);
  }
  
  private String m()
  {
    File[] arrayOfFile = a(new b("BeginSession"));
    Arrays.sort(arrayOfFile, b);
    if (arrayOfFile.length > 0) {
      return a(arrayOfFile[0]);
    }
    return null;
  }
  
  private void n()
    throws Exception
  {
    Date localDate = new Date();
    String str = new c(this.u).toString();
    io.a.a.a.c.g().a("Fabric", "Opening an new session with ID " + str);
    a(localDate, str);
    d(str);
    e(str);
    f(str);
  }
  
  private void o()
    throws Exception
  {
    a(8);
    Object localObject = m();
    if (localObject != null)
    {
      c((String)localObject);
      localObject = this.B.v();
      if (localObject != null)
      {
        int i2 = ((p)localObject).c;
        io.a.a.a.c.g().a("Fabric", "Closing all open sessions.");
        localObject = e();
        if ((localObject != null) && (localObject.length > 0))
        {
          int i3 = localObject.length;
          int i1 = 0;
          while (i1 < i3)
          {
            File localFile = localObject[i1];
            String str = a(localFile);
            io.a.a.a.c.g().a("Fabric", "Closing session: " + str);
            a(localFile, str, i2);
            i1 += 1;
          }
        }
      }
      else
      {
        io.a.a.a.c.g().a("Fabric", "Unable to close session. Settings are not loaded.");
      }
      return;
    }
    io.a.a.a.c.g().a("Fabric", "No open sessions exist.");
  }
  
  private File[] p()
  {
    return a(a);
  }
  
  private void q()
  {
    File localFile = new File(this.B.r(), "invalidClsFiles");
    if (localFile.exists())
    {
      if (localFile.isDirectory())
      {
        File[] arrayOfFile = localFile.listFiles();
        int i2 = arrayOfFile.length;
        int i1 = 0;
        while (i1 < i2)
        {
          arrayOfFile[i1].delete();
          i1 += 1;
        }
      }
      localFile.delete();
    }
  }
  
  private boolean r()
  {
    if (!io.a.a.a.a.b.i.a(this.B.z(), "com.crashlytics.CollectCustomLogs", true))
    {
      io.a.a.a.c.g().a("Fabric", "Preferences requested no custom logs. Aborting log file creation.");
      return false;
    }
    io.a.a.a.a.b.i.a(this.w, "Could not close log file: " + this.w);
    localObject3 = null;
    try
    {
      Object localObject1 = "crashlytics-userlog-" + UUID.randomUUID().toString() + ".temp";
      localObject1 = new File(this.B.r(), (String)localObject1);
      io.a.a.a.c.g().d("Fabric", "Could not create log file: " + localObject3, localException1);
    }
    catch (Exception localException1)
    {
      try
      {
        this.w = new io.a.a.a.a.b.q((File)localObject1);
        ((File)localObject1).delete();
        return true;
      }
      catch (Exception localException2)
      {
        for (;;)
        {
          localObject3 = localException1;
          Object localObject2 = localException2;
        }
      }
      localException1 = localException1;
    }
    return false;
  }
  
  private int s()
  {
    return 0 + e.b(1, b.a(io.a.a.a.a.b.g.a(this.B.z(), io.a.a.a.c.h())));
  }
  
  private int t()
  {
    int i2 = 0 + e.b(1, 0L) + e.b(2, 0L) + e.b(3, this.r);
    int i1 = i2;
    if (this.s != null) {
      i1 = i2 + e.b(4, this.s);
    }
    return i1;
  }
  
  private int u()
  {
    return 0 + e.b(1, g) + e.b(2, g) + e.b(3, 0L);
  }
  
  private void v()
  {
    File[] arrayOfFile = p();
    int i2 = arrayOfFile.length;
    int i1 = 0;
    while (i1 < i2)
    {
      a(new Runnable()
      {
        public void run()
        {
          if (io.a.a.a.a.b.i.n(i.e(i.this).z()))
          {
            io.a.a.a.c.g().a("Fabric", "Attempting to send crash report at time of crash...");
            Object localObject = io.a.a.a.a.g.q.a().b();
            localObject = i.e(i.this).a((t)localObject);
            if (localObject != null) {
              new q((k)localObject).a(new r(this.a, i.l()));
            }
          }
        }
      });
      i1 += 1;
    }
  }
  
  b a(io.a.a.a.a.b.q paramq)
  {
    if (paramq == null) {
      return null;
    }
    final int[] arrayOfInt = new int[1];
    arrayOfInt[0] = 0;
    final byte[] arrayOfByte = new byte[paramq.a()];
    try
    {
      paramq.a(new q.c()
      {
        public void a(InputStream paramAnonymousInputStream, int paramAnonymousInt)
          throws IOException
        {
          try
          {
            paramAnonymousInputStream.read(arrayOfByte, arrayOfInt[0], paramAnonymousInt);
            int[] arrayOfInt = arrayOfInt;
            arrayOfInt[0] += paramAnonymousInt;
            return;
          }
          finally
          {
            paramAnonymousInputStream.close();
          }
        }
      });
      return b.a(arrayOfByte, 0, arrayOfInt[0]);
    }
    catch (IOException paramq)
    {
      for (;;)
      {
        io.a.a.a.c.g().d("Fabric", "A problem occurred while reading the Crashlytics log file.", paramq);
      }
    }
  }
  
  void a(final long paramLong, String paramString)
  {
    b(new Callable()
    {
      public Void a()
        throws Exception
      {
        if (!i.a(i.this).get())
        {
          if (i.b(i.this) == null) {
            i.c(i.this);
          }
          i.this.a(i.b(i.this), 65536, paramLong, this.b);
        }
        return null;
      }
    });
  }
  
  void a(io.a.a.a.a.b.q paramq, int paramInt, long paramLong, String paramString)
  {
    if (paramq == null) {}
    for (;;)
    {
      return;
      String str = paramString;
      if (paramString == null) {
        str = "null";
      }
      try
      {
        int i1 = paramInt / 4;
        paramString = str;
        if (str.length() > i1) {
          paramString = "..." + str.substring(str.length() - i1);
        }
        paramString = paramString.replaceAll("\r", " ").replaceAll("\n", " ");
        paramq.a(String.format(Locale.US, "%d %s%n", new Object[] { Long.valueOf(paramLong), paramString }).getBytes("UTF-8"));
        while ((!paramq.b()) && (paramq.a() > paramInt)) {
          paramq.c();
        }
        return;
      }
      catch (IOException paramq)
      {
        io.a.a.a.c.g().d("Fabric", "There was a problem writing to the Crashlytics log.", paramq);
      }
    }
  }
  
  void a(final Thread paramThread, final Throwable paramThrowable)
  {
    a(new Runnable()
    {
      public void run()
      {
        if (!i.a(i.this).get()) {
          i.b(i.this, this.a, paramThread, paramThrowable);
        }
      }
    });
  }
  
  void a(File[] paramArrayOfFile)
  {
    q();
    int i3 = paramArrayOfFile.length;
    int i1 = 0;
    while (i1 < i3)
    {
      final Object localObject = paramArrayOfFile[i1];
      io.a.a.a.c.g().a("Fabric", "Found invalid session part file: " + localObject);
      localObject = a((File)localObject);
      FilenameFilter local7 = new FilenameFilter()
      {
        public boolean accept(File paramAnonymousFile, String paramAnonymousString)
        {
          return paramAnonymousString.startsWith(localObject);
        }
      };
      io.a.a.a.c.g().a("Fabric", "Deleting all part files for invalid session: " + (String)localObject);
      localObject = a(local7);
      int i4 = localObject.length;
      int i2 = 0;
      while (i2 < i4)
      {
        local7 = localObject[i2];
        io.a.a.a.c.g().a("Fabric", "Deleting session file: " + local7);
        local7.delete();
        i2 += 1;
      }
      i1 += 1;
    }
  }
  
  boolean a()
  {
    return this.n.get();
  }
  
  void b()
  {
    b(new Callable()
    {
      public Void a()
        throws Exception
      {
        if (!i.this.c()) {
          i.d(i.this);
        }
        return null;
      }
    });
  }
  
  boolean c()
  {
    return e().length > 0;
  }
  
  boolean d()
  {
    ((Boolean)a(new Callable()
    {
      public Boolean a()
        throws Exception
      {
        if (!i.a(i.this).get())
        {
          com.a.a.c.a.c localc = i.e(i.this).q();
          if (localc != null) {
            i.a(i.this, localc);
          }
          i.f(i.this);
          i.d(i.this);
          io.a.a.a.c.g().a("Fabric", "Open sessions were closed and a new session was opened.");
          return Boolean.valueOf(true);
        }
        io.a.a.a.c.g().a("Fabric", "Skipping session finalization because a crash has already occurred.");
        return Boolean.valueOf(false);
      }
    })).booleanValue();
  }
  
  File[] e()
  {
    return a(new b("BeginSession"));
  }
  
  void f()
  {
    s.a(this.l, a, 4, c);
  }
  
  void g()
  {
    a(new Runnable()
    {
      public void run()
      {
        i.this.a(i.a(i.this, d.a));
      }
    });
  }
  
  void h()
  {
    a(new Callable()
    {
      public Void a()
        throws Exception
      {
        i.g(i.this).createNewFile();
        io.a.a.a.c.g().a("Fabric", "Initialization marker file created.");
        return null;
      }
    });
  }
  
  void i()
  {
    b(new Callable()
    {
      public Boolean a()
        throws Exception
      {
        try
        {
          boolean bool = i.g(i.this).delete();
          io.a.a.a.c.g().a("Fabric", "Initialization marker file removed: " + bool);
          return Boolean.valueOf(bool);
        }
        catch (Exception localException)
        {
          io.a.a.a.c.g().d("Fabric", "Problem encountered deleting Crashlytics initialization marker.", localException);
        }
        return Boolean.valueOf(false);
      }
    });
  }
  
  boolean j()
  {
    ((Boolean)a(new Callable()
    {
      public Boolean a()
        throws Exception
      {
        return Boolean.valueOf(i.g(i.this).exists());
      }
    })).booleanValue();
  }
  
  /* Error */
  public void uncaughtException(final Thread paramThread, final Throwable paramThrowable)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 178	com/a/a/i:n	Ljava/util/concurrent/atomic/AtomicBoolean;
    //   6: iconst_1
    //   7: invokevirtual 253	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
    //   10: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   13: ldc_w 481
    //   16: new 350	java/lang/StringBuilder
    //   19: dup
    //   20: invokespecial 351	java/lang/StringBuilder:<init>	()V
    //   23: ldc_w 1239
    //   26: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   29: aload_2
    //   30: invokevirtual 965	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   33: ldc_w 967
    //   36: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   39: aload_1
    //   40: invokevirtual 413	java/lang/Thread:getName	()Ljava/lang/String;
    //   43: invokevirtual 358	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   46: invokevirtual 366	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   49: invokeinterface 488 3 0
    //   54: aload_0
    //   55: getfield 168	com/a/a/i:i	Ljava/util/concurrent/atomic/AtomicBoolean;
    //   58: iconst_1
    //   59: invokevirtual 1243	java/util/concurrent/atomic/AtomicBoolean:getAndSet	(Z)Z
    //   62: ifne +41 -> 103
    //   65: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   68: ldc_w 481
    //   71: ldc_w 1245
    //   74: invokeinterface 488 3 0
    //   79: aload_0
    //   80: getfield 176	com/a/a/i:B	Lcom/a/a/f;
    //   83: invokevirtual 244	com/a/a/f:z	()Landroid/content/Context;
    //   86: astore_3
    //   87: aload_3
    //   88: aload_0
    //   89: getfield 229	com/a/a/i:q	Landroid/content/BroadcastReceiver;
    //   92: invokevirtual 1249	android/content/Context:unregisterReceiver	(Landroid/content/BroadcastReceiver;)V
    //   95: aload_3
    //   96: aload_0
    //   97: getfield 239	com/a/a/i:p	Landroid/content/BroadcastReceiver;
    //   100: invokevirtual 1249	android/content/Context:unregisterReceiver	(Landroid/content/BroadcastReceiver;)V
    //   103: aload_0
    //   104: new 26	com/a/a/i$18
    //   107: dup
    //   108: aload_0
    //   109: new 754	java/util/Date
    //   112: dup
    //   113: invokespecial 755	java/util/Date:<init>	()V
    //   116: aload_1
    //   117: aload_2
    //   118: invokespecial 1250	com/a/a/i$18:<init>	(Lcom/a/a/i;Ljava/util/Date;Ljava/lang/Thread;Ljava/lang/Throwable;)V
    //   121: invokespecial 1229	com/a/a/i:a	(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    //   124: pop
    //   125: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   128: ldc_w 481
    //   131: ldc_w 1252
    //   134: invokeinterface 488 3 0
    //   139: aload_0
    //   140: getfield 170	com/a/a/i:k	Ljava/lang/Thread$UncaughtExceptionHandler;
    //   143: aload_1
    //   144: aload_2
    //   145: invokeinterface 1254 3 0
    //   150: aload_0
    //   151: getfield 178	com/a/a/i:n	Ljava/util/concurrent/atomic/AtomicBoolean;
    //   154: iconst_0
    //   155: invokevirtual 253	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
    //   158: aload_0
    //   159: monitorexit
    //   160: return
    //   161: astore_3
    //   162: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   165: ldc_w 481
    //   168: ldc_w 1256
    //   171: aload_3
    //   172: invokeinterface 493 4 0
    //   177: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   180: ldc_w 481
    //   183: ldc_w 1252
    //   186: invokeinterface 488 3 0
    //   191: aload_0
    //   192: getfield 170	com/a/a/i:k	Ljava/lang/Thread$UncaughtExceptionHandler;
    //   195: aload_1
    //   196: aload_2
    //   197: invokeinterface 1254 3 0
    //   202: aload_0
    //   203: getfield 178	com/a/a/i:n	Ljava/util/concurrent/atomic/AtomicBoolean;
    //   206: iconst_0
    //   207: invokevirtual 253	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
    //   210: goto -52 -> 158
    //   213: astore_1
    //   214: aload_0
    //   215: monitorexit
    //   216: aload_1
    //   217: athrow
    //   218: astore_3
    //   219: invokestatic 479	io/a/a/a/c:g	()Lio/a/a/a/j;
    //   222: ldc_w 481
    //   225: ldc_w 1252
    //   228: invokeinterface 488 3 0
    //   233: aload_0
    //   234: getfield 170	com/a/a/i:k	Ljava/lang/Thread$UncaughtExceptionHandler;
    //   237: aload_1
    //   238: aload_2
    //   239: invokeinterface 1254 3 0
    //   244: aload_0
    //   245: getfield 178	com/a/a/i:n	Ljava/util/concurrent/atomic/AtomicBoolean;
    //   248: iconst_0
    //   249: invokevirtual 253	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
    //   252: aload_3
    //   253: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	254	0	this	i
    //   0	254	1	paramThread	Thread
    //   0	254	2	paramThrowable	Throwable
    //   86	10	3	localContext	Context
    //   161	11	3	localException	Exception
    //   218	35	3	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   10	103	161	java/lang/Exception
    //   103	125	161	java/lang/Exception
    //   2	10	213	finally
    //   125	158	213	finally
    //   177	210	213	finally
    //   219	254	213	finally
    //   10	103	218	finally
    //   103	125	218	finally
    //   162	177	218	finally
  }
  
  private static class a
    implements FilenameFilter
  {
    public boolean accept(File paramFile, String paramString)
    {
      return (!i.a.accept(paramFile, paramString)) && (i.k().matcher(paramString).matches());
    }
  }
  
  static class b
    implements FilenameFilter
  {
    private final String a;
    
    public b(String paramString)
    {
      this.a = paramString;
    }
    
    public boolean accept(File paramFile, String paramString)
    {
      return (paramString.contains(this.a)) && (!paramString.endsWith(".cls_temp"));
    }
  }
  
  static class c
    implements FilenameFilter
  {
    private final String a;
    
    public c(String paramString)
    {
      this.a = paramString;
    }
    
    public boolean accept(File paramFile, String paramString)
    {
      if (paramString.equals(this.a + ".cls")) {}
      while ((!paramString.contains(this.a)) || (paramString.endsWith(".cls_temp"))) {
        return false;
      }
      return true;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/a/a/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */