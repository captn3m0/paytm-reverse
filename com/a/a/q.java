package com.a.a;

import io.a.a.a.a.b.g;
import io.a.a.a.a.b.h;
import io.a.a.a.c;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

class q
{
  static final Map<String, String> a = Collections.singletonMap("X-CRASHLYTICS-INVALID-SESSION", "1");
  private static final FilenameFilter b = new FilenameFilter()
  {
    public boolean accept(File paramAnonymousFile, String paramAnonymousString)
    {
      return (paramAnonymousString.endsWith(".cls")) && (!paramAnonymousString.contains("Session"));
    }
  };
  private static final short[] c = { 10, 20, 30, 60, 120, 300 };
  private final Object d = new Object();
  private final k e;
  private Thread f;
  
  public q(k paramk)
  {
    if (paramk == null) {
      throw new IllegalArgumentException("createReportCall must not be null.");
    }
    this.e = paramk;
  }
  
  List<p> a()
  {
    c.g().a("Fabric", "Checking for crash reports...");
    synchronized (this.d)
    {
      File[] arrayOfFile = f.f().r().listFiles(b);
      ??? = new LinkedList();
      int j = arrayOfFile.length;
      int i = 0;
      if (i < j)
      {
        File localFile = arrayOfFile[i];
        c.g().a("Fabric", "Found crash report " + localFile.getPath());
        ((List)???).add(new r(localFile));
        i += 1;
      }
    }
    if (((List)???).isEmpty()) {
      c.g().a("Fabric", "No reports found.");
    }
    return (List<p>)???;
  }
  
  public void a(float paramFloat)
  {
    try
    {
      if (this.f == null)
      {
        this.f = new Thread(new a(paramFloat), "Crashlytics Report Uploader");
        this.f.start();
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  boolean a(p paramp)
  {
    boolean bool2 = false;
    synchronized (this.d)
    {
      try
      {
        localObject1 = new j(g.a(f.f().z(), c.h()), paramp);
        boolean bool3 = this.e.a((j)localObject1);
        io.a.a.a.j localj = c.g();
        StringBuilder localStringBuilder = new StringBuilder().append("Crashlytics report upload ");
        if (!bool3) {
          break label123;
        }
        localObject1 = "complete: ";
        localj.b("Fabric", (String)localObject1 + paramp.b());
        bool1 = bool2;
        if (bool3)
        {
          paramp.a();
          bool1 = true;
        }
      }
      catch (Exception localException)
      {
        for (;;)
        {
          Object localObject1;
          label123:
          c.g().d("Fabric", "Error occurred sending report " + paramp, localException);
          boolean bool1 = bool2;
        }
      }
      return bool1;
      localObject1 = "FAILED: ";
    }
  }
  
  private class a
    extends h
  {
    private final float b;
    
    a(float paramFloat)
    {
      this.b = paramFloat;
    }
    
    private void b()
    {
      c.g().a("Fabric", "Starting report processing in " + this.b + " second(s)...");
      if (this.b > 0.0F) {}
      Object localObject2;
      i locali;
      List localList;
      break label194;
      label86:
      Object localObject1;
      for (;;)
      {
        try
        {
          Thread.sleep((this.b * 1000.0F));
          localObject2 = f.f();
          locali = ((f)localObject2).m();
          localList = q.this.a();
          if (locali.a()) {
            return;
          }
        }
        catch (InterruptedException localInterruptedException1)
        {
          Thread.currentThread().interrupt();
          return;
        }
        if ((localInterruptedException1.isEmpty()) || (((f)localObject2).u())) {
          break;
        }
        c.g().a("Fabric", "User declined to send. Removing " + localInterruptedException1.size() + " Report(s).");
        localObject1 = localInterruptedException1.iterator();
        while (((Iterator)localObject1).hasNext()) {
          ((p)((Iterator)localObject1).next()).a();
        }
      }
      int i = 0;
      for (;;)
      {
        label194:
        if ((((List)localObject1).isEmpty()) || (f.f().m().a())) {
          break label86;
        }
        c.g().a("Fabric", "Attempting to send " + ((List)localObject1).size() + " report(s)");
        localObject1 = ((List)localObject1).iterator();
        while (((Iterator)localObject1).hasNext())
        {
          localObject2 = (p)((Iterator)localObject1).next();
          q.this.a((p)localObject2);
        }
        localObject2 = q.this.a();
        localObject1 = localObject2;
        if (((List)localObject2).isEmpty()) {
          break;
        }
        long l = q.b()[Math.min(i, q.b().length - 1)];
        c.g().a("Fabric", "Report submisson: scheduling delayed retry in " + l + " seconds");
        try
        {
          Thread.sleep(1000L * l);
          i += 1;
          localObject1 = localObject2;
        }
        catch (InterruptedException localInterruptedException2)
        {
          Thread.currentThread().interrupt();
        }
      }
    }
    
    public void a()
    {
      try
      {
        b();
        q.a(q.this, null);
        return;
      }
      catch (Exception localException)
      {
        for (;;)
        {
          c.g().d("Fabric", "An unexpected error occurred while attempting to upload crash reports.", localException);
        }
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/a/a/q.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */