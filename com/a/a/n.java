package com.a.a;

import com.a.a.c.a.a;
import com.a.a.c.a.c;
import com.a.a.c.a.d;
import com.a.a.c.a.e.a;
import java.io.IOException;

class n
{
  private static final d a = new d("", "", 0L);
  private static final h[] b = new h[0];
  private static final k[] c = new k[0];
  private static final g[] d = new g[0];
  private static final b[] e = new b[0];
  private static final c[] f = new c[0];
  
  private static e a(c paramc)
    throws IOException
  {
    if (paramc.b != null) {}
    for (Object localObject = paramc.b;; localObject = a)
    {
      localObject = new a(new f(new j((d)localObject), a(paramc.c), a(paramc.d)), a(paramc.e));
      d locald = new d();
      return new e(paramc.a, "ndk-crash", (a)localObject, locald);
    }
  }
  
  private static i a(a[] paramArrayOfa)
  {
    if (paramArrayOfa != null) {}
    for (b[] arrayOfb = new b[paramArrayOfa.length];; arrayOfb = e)
    {
      int i = 0;
      while (i < arrayOfb.length)
      {
        arrayOfb[i] = new b(paramArrayOfa[i]);
        i += 1;
      }
    }
    return new i(arrayOfb);
  }
  
  private static i a(com.a.a.c.a.b[] paramArrayOfb)
  {
    if (paramArrayOfb != null) {}
    for (c[] arrayOfc = new c[paramArrayOfb.length];; arrayOfc = f)
    {
      int i = 0;
      while (i < arrayOfc.length)
      {
        arrayOfc[i] = new c(paramArrayOfb[i]);
        i += 1;
      }
    }
    return new i(arrayOfc);
  }
  
  private static i a(e.a[] paramArrayOfa)
  {
    if (paramArrayOfa != null) {}
    for (g[] arrayOfg = new g[paramArrayOfa.length];; arrayOfg = d)
    {
      int i = 0;
      while (i < arrayOfg.length)
      {
        arrayOfg[i] = new g(paramArrayOfa[i]);
        i += 1;
      }
    }
    return new i(arrayOfg);
  }
  
  private static i a(com.a.a.c.a.e[] paramArrayOfe)
  {
    if (paramArrayOfe != null) {}
    for (k[] arrayOfk = new k[paramArrayOfe.length];; arrayOfk = c)
    {
      int i = 0;
      while (i < arrayOfk.length)
      {
        com.a.a.c.a.e locale = paramArrayOfe[i];
        arrayOfk[i] = new k(locale, a(locale.b));
        i += 1;
      }
    }
    return new i(arrayOfk);
  }
  
  public static void a(c paramc, e parame)
    throws IOException
  {
    a(paramc).b(parame);
  }
  
  private static final class a
    extends n.h
  {
    public a(n.f paramf, n.i parami)
    {
      super(new n.h[] { paramf, parami });
    }
  }
  
  private static final class b
    extends n.h
  {
    private final long a;
    private final long b;
    private final String c;
    private final String d;
    
    public b(a parama)
    {
      super(new n.h[0]);
      this.a = parama.a;
      this.b = parama.b;
      this.c = parama.c;
      this.d = parama.d;
    }
    
    public int a()
    {
      int i = e.b(1, this.a);
      int j = e.b(2, this.b);
      return e.b(3, b.a(this.c)) + i + j + e.b(4, b.a(this.d));
    }
    
    public void a(e parame)
      throws IOException
    {
      parame.a(1, this.a);
      parame.a(2, this.b);
      parame.a(3, b.a(this.c));
      parame.a(4, b.a(this.d));
    }
  }
  
  private static final class c
    extends n.h
  {
    private final String a;
    private final String b;
    
    public c(com.a.a.c.a.b paramb)
    {
      super(new n.h[0]);
      this.a = paramb.a;
      this.b = paramb.b;
    }
    
    public int a()
    {
      int i = e.b(1, b.a(this.a));
      if (this.b == null) {}
      for (String str = "";; str = this.b) {
        return i + e.b(2, b.a(str));
      }
    }
    
    public void a(e parame)
      throws IOException
    {
      parame.a(1, b.a(this.a));
      if (this.b == null) {}
      for (String str = "";; str = this.b)
      {
        parame.a(2, b.a(str));
        return;
      }
    }
  }
  
  private static final class d
    extends n.h
  {
    public d()
    {
      super(new n.h[0]);
    }
    
    public int a()
    {
      return 0 + e.b(1, 0.0F) + e.e(2, 0) + e.b(3, false) + e.f(4, 0) + e.b(5, 0L) + e.b(6, 0L);
    }
    
    public void a(e parame)
      throws IOException
    {
      parame.a(1, 0.0F);
      parame.a(2, 0);
      parame.a(3, false);
      parame.b(4, 0);
      parame.a(5, 0L);
      parame.a(6, 0L);
    }
  }
  
  private static final class e
    extends n.h
  {
    private final long a;
    private final String b;
    
    public e(long paramLong, String paramString, n.a parama, n.d paramd)
    {
      super(new n.h[] { parama, paramd });
      this.a = paramLong;
      this.b = paramString;
    }
    
    public int a()
    {
      return e.b(1, this.a) + e.b(2, b.a(this.b));
    }
    
    public void a(e parame)
      throws IOException
    {
      parame.a(1, this.a);
      parame.a(2, b.a(this.b));
    }
  }
  
  private static final class f
    extends n.h
  {
    public f(n.j paramj, n.i parami1, n.i parami2)
    {
      super(new n.h[] { parami1, paramj, parami2 });
    }
  }
  
  private static final class g
    extends n.h
  {
    private final long a;
    private final String b;
    private final String c;
    private final long d;
    private final int e;
    
    public g(e.a parama)
    {
      super(new n.h[0]);
      this.a = parama.a;
      this.b = parama.b;
      this.c = parama.c;
      this.d = parama.d;
      this.e = parama.e;
    }
    
    public int a()
    {
      return e.b(1, this.a) + e.b(2, b.a(this.b)) + e.b(3, b.a(this.c)) + e.b(4, this.d) + e.f(5, this.e);
    }
    
    public void a(e parame)
      throws IOException
    {
      parame.a(1, this.a);
      parame.a(2, b.a(this.b));
      parame.a(3, b.a(this.c));
      parame.a(4, this.d);
      parame.b(5, this.e);
    }
  }
  
  private static abstract class h
  {
    private final int a;
    private final h[] b;
    
    public h(int paramInt, h... paramVarArgs)
    {
      this.a = paramInt;
      if (paramVarArgs != null) {}
      for (;;)
      {
        this.b = paramVarArgs;
        return;
        paramVarArgs = n.a();
      }
    }
    
    public int a()
    {
      return 0;
    }
    
    public void a(e parame)
      throws IOException
    {}
    
    public int b()
    {
      int i = c();
      return i + e.l(i) + e.j(this.a);
    }
    
    public void b(e parame)
      throws IOException
    {
      parame.i(this.a, 2);
      parame.k(c());
      a(parame);
      h[] arrayOfh = this.b;
      int j = arrayOfh.length;
      int i = 0;
      while (i < j)
      {
        arrayOfh[i].b(parame);
        i += 1;
      }
    }
    
    public int c()
    {
      int j = a();
      h[] arrayOfh = this.b;
      int k = arrayOfh.length;
      int i = 0;
      while (i < k)
      {
        j += arrayOfh[i].b();
        i += 1;
      }
      return j;
    }
  }
  
  private static final class i
    extends n.h
  {
    private final n.h[] a;
    
    public i(n.h... paramVarArgs)
    {
      super(new n.h[0]);
      this.a = paramVarArgs;
    }
    
    public int b()
    {
      int j = 0;
      n.h[] arrayOfh = this.a;
      int k = arrayOfh.length;
      int i = 0;
      while (i < k)
      {
        j += arrayOfh[i].b();
        i += 1;
      }
      return j;
    }
    
    public void b(e parame)
      throws IOException
    {
      n.h[] arrayOfh = this.a;
      int j = arrayOfh.length;
      int i = 0;
      while (i < j)
      {
        arrayOfh[i].b(parame);
        i += 1;
      }
    }
  }
  
  private static final class j
    extends n.h
  {
    private final String a;
    private final String b;
    private final long c;
    
    public j(d paramd)
    {
      super(new n.h[0]);
      this.a = paramd.a;
      this.b = paramd.b;
      this.c = paramd.c;
    }
    
    public int a()
    {
      return e.b(1, b.a(this.a)) + e.b(2, b.a(this.b)) + e.b(3, this.c);
    }
    
    public void a(e parame)
      throws IOException
    {
      parame.a(1, b.a(this.a));
      parame.a(2, b.a(this.b));
      parame.a(3, this.c);
    }
  }
  
  private static final class k
    extends n.h
  {
    private final int a;
    
    public k(com.a.a.c.a.e parame, n.i parami)
    {
      super(new n.h[] { parami });
      this.a = parame.a;
    }
    
    public int a()
    {
      return e.f(2, this.a);
    }
    
    public void a(e parame)
      throws IOException
    {
      parame.b(2, this.a);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/a/a/n.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */