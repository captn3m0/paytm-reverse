package com.a.a;

import io.a.a.a.c;
import io.a.a.a.j;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

class r
  implements p
{
  private final File a;
  private final Map<String, String> b;
  
  public r(File paramFile)
  {
    this(paramFile, Collections.emptyMap());
  }
  
  public r(File paramFile, Map<String, String> paramMap)
  {
    this.a = paramFile;
    this.b = new HashMap(paramMap);
    if (this.a.length() == 0L) {
      this.b.putAll(q.a);
    }
  }
  
  public boolean a()
  {
    c.g().a("Fabric", "Removing report at " + this.a.getPath());
    return this.a.delete();
  }
  
  public String b()
  {
    return d().getName();
  }
  
  public String c()
  {
    String str = b();
    return str.substring(0, str.lastIndexOf('.'));
  }
  
  public File d()
  {
    return this.a;
  }
  
  public Map<String, String> e()
  {
    return Collections.unmodifiableMap(this.b);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/a/a/r.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */