package com.a.a.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import io.a.a.a.a.b.g;
import io.a.a.a.a.b.k;
import io.a.a.a.a.b.o;
import io.a.a.a.a.e.e;
import io.a.a.a.j;

class c
{
  private final Context a;
  private final a b;
  private final o c;
  private final io.a.a.a.a.g.f d;
  private final b e;
  private final io.a.a.a.a.f.b f;
  private final k g;
  private final e h;
  
  public c(Context paramContext, a parama, o paramo, io.a.a.a.a.g.f paramf, b paramb, io.a.a.a.a.f.b paramb1, k paramk, e parame)
  {
    this.a = paramContext;
    this.b = parama;
    this.c = paramo;
    this.d = paramf;
    this.e = paramb;
    this.f = paramb1;
    this.g = paramk;
    this.h = parame;
  }
  
  public void a()
  {
    long l1 = this.g.a();
    long l2 = this.d.b * 1000;
    io.a.a.a.c.g().a("Beta", "Check for updates delay: " + l2);
    long l3 = this.f.a().getLong("last_update_check", 0L);
    io.a.a.a.c.g().a("Beta", "Check for updates last check time: " + l3);
    l2 = l3 + l2;
    io.a.a.a.c.g().a("Beta", "Check for updates current time: " + l1 + ", next check time: " + l2);
    if (l1 >= l2) {
      try
      {
        io.a.a.a.c.g().a("Beta", "Performing update check");
        String str1 = g.a(this.a);
        String str2 = this.c.a(str1, this.e.d);
        new d(this.b, this.b.f(), this.d.a, this.h, new f()).a(str1, str2, this.e);
        return;
      }
      finally
      {
        this.f.b().putLong("last_update_check", l1).commit();
      }
    }
    io.a.a.a.c.g().a("Beta", "Check for updates next check time was not passed");
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/a/a/b/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */