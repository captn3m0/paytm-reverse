package com.a.a.b;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.Build.VERSION;
import android.text.TextUtils;
import io.a.a.a.a.b.i;
import io.a.a.a.a.b.m;
import io.a.a.a.a.b.o;
import io.a.a.a.a.b.o.a;
import io.a.a.a.a.b.s;
import io.a.a.a.a.g.f;
import io.a.a.a.a.g.q;
import io.a.a.a.a.g.t;
import io.a.a.a.h;
import io.a.a.a.j;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class a
  extends h<Boolean>
  implements m
{
  private final io.a.a.a.a.a.b<String> f = new io.a.a.a.a.a.b();
  private final g g = new g();
  
  private b a(Context paramContext)
  {
    Object localObject3 = null;
    Object localObject2 = null;
    Object localObject4 = null;
    Object localObject5 = null;
    Object localObject1 = localObject4;
    for (;;)
    {
      try
      {
        localInputStream = paramContext.getAssets().open("crashlytics-build.properties");
        paramContext = (Context)localObject5;
        if (localInputStream != null)
        {
          localObject1 = localObject4;
          localObject2 = localInputStream;
          localObject3 = localInputStream;
          paramContext = b.a(localInputStream);
          localObject1 = paramContext;
          localObject2 = localInputStream;
          localObject3 = localInputStream;
          io.a.a.a.c.g().a("Beta", paramContext.d + " build properties: " + paramContext.b + " (" + paramContext.a + ")" + " - " + paramContext.c);
        }
        localObject3 = paramContext;
      }
      catch (Exception paramContext)
      {
        InputStream localInputStream;
        localObject3 = localObject2;
        io.a.a.a.c.g().d("Beta", "Error reading Beta build properties", paramContext);
        localObject3 = localIOException1;
        if (localObject2 == null) {
          continue;
        }
        try
        {
          ((InputStream)localObject2).close();
          return localIOException1;
        }
        catch (IOException paramContext)
        {
          io.a.a.a.c.g().d("Beta", "Error closing Beta build properties asset", paramContext);
          return localIOException1;
        }
      }
      finally
      {
        if (localObject3 == null) {
          break label218;
        }
      }
      try
      {
        localInputStream.close();
        localObject3 = paramContext;
        return (b)localObject3;
      }
      catch (IOException localIOException1)
      {
        io.a.a.a.c.g().d("Beta", "Error closing Beta build properties asset", localIOException1);
        return paramContext;
      }
    }
    try
    {
      ((InputStream)localObject3).close();
      label218:
      throw paramContext;
    }
    catch (IOException localIOException2)
    {
      for (;;)
      {
        io.a.a.a.c.g().d("Beta", "Error closing Beta build properties asset", localIOException2);
      }
    }
  }
  
  private String a(Context paramContext, String paramString)
  {
    if (a(paramString, Build.VERSION.SDK_INT))
    {
      io.a.a.a.c.g().a("Beta", "App was installed by Beta. Getting device token");
      try
      {
        paramContext = (String)this.f.a(paramContext, this.g);
        boolean bool = "".equals(paramContext);
        if (bool) {
          return null;
        }
        return paramContext;
      }
      catch (Exception paramContext)
      {
        io.a.a.a.c.g().d("Beta", "Failed to load the Beta device token", paramContext);
        return null;
      }
    }
    io.a.a.a.c.g().a("Beta", "App was not installed by Beta. Skipping device token");
    return null;
  }
  
  private void a(Context paramContext, o paramo, f paramf, b paramb)
  {
    new c(paramContext, this, paramo, paramf, paramb, new io.a.a.a.a.f.c(io.a.a.a.c.a(a.class)), new s(), new io.a.a.a.a.e.b(io.a.a.a.c.g())).a();
  }
  
  private f g()
  {
    t localt = q.a().b();
    if (localt != null) {
      return localt.f;
    }
    return null;
  }
  
  boolean a(f paramf, b paramb)
  {
    return (paramf != null) && (!TextUtils.isEmpty(paramf.a)) && (paramb != null);
  }
  
  @TargetApi(11)
  boolean a(String paramString, int paramInt)
  {
    if (paramInt < 11) {
      return paramString == null;
    }
    return "io.crash.air".equals(paramString);
  }
  
  protected Boolean b()
  {
    io.a.a.a.c.g().a("Beta", "Beta kit initializing...");
    Context localContext = z();
    o localo = y();
    if (TextUtils.isEmpty(a(localContext, localo.h())))
    {
      io.a.a.a.c.g().a("Beta", "A Beta device token was not found for this app");
      return Boolean.valueOf(false);
    }
    io.a.a.a.c.g().a("Beta", "Beta device token is present, checking for app updates.");
    f localf = g();
    b localb = a(localContext);
    if (a(localf, localb)) {
      a(localContext, localo, localf, localb);
    }
    return Boolean.valueOf(true);
  }
  
  public String c()
  {
    return "com.crashlytics.sdk.android:beta";
  }
  
  public String d()
  {
    return "1.1.1.32";
  }
  
  public Map<o.a, String> e()
  {
    String str = y().h();
    str = a(z(), str);
    HashMap localHashMap = new HashMap();
    if (!TextUtils.isEmpty(str)) {
      localHashMap.put(o.a.c, str);
    }
    return localHashMap;
  }
  
  String f()
  {
    return i.b(z(), "com.crashlytics.ApiEndpoint");
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/a/a/b/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */