package com.a.a.b;

import io.a.a.a.a.b.a;
import io.a.a.a.h;
import io.a.a.a.j;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

class d
  extends a
{
  private final f b;
  
  public d(h paramh, String paramString1, String paramString2, io.a.a.a.a.e.e parame, f paramf)
  {
    super(paramh, paramString1, paramString2, parame, io.a.a.a.a.e.c.a);
    this.b = paramf;
  }
  
  private io.a.a.a.a.e.d a(io.a.a.a.a.e.d paramd, String paramString1, String paramString2)
  {
    return paramd.a("Accept", "application/json").a("User-Agent", "Crashlytics Android SDK/" + this.a.d()).a("X-CRASHLYTICS-DEVELOPER-TOKEN", "bca6990fc3c15a8105800c0673517a4b579634a1").a("X-CRASHLYTICS-API-CLIENT-TYPE", "android").a("X-CRASHLYTICS-API-CLIENT-VERSION", this.a.d()).a("X-CRASHLYTICS-API-KEY", paramString1).a("X-CRASHLYTICS-D", paramString2);
  }
  
  private Map<String, String> a(b paramb)
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("build_version", paramb.a);
    localHashMap.put("display_version", paramb.b);
    localHashMap.put("instance", paramb.c);
    localHashMap.put("source", "3");
    return localHashMap;
  }
  
  public e a(String paramString1, String paramString2, b paramb)
  {
    Object localObject4 = null;
    Object localObject3 = null;
    localObject2 = localObject3;
    localObject1 = localObject4;
    try
    {
      Map localMap = a(paramb);
      localObject2 = localObject3;
      localObject1 = localObject4;
      paramb = a(localMap);
      localObject2 = paramb;
      localObject1 = paramb;
      paramString1 = a(paramb, paramString1, paramString2);
      localObject2 = paramString1;
      localObject1 = paramString1;
      io.a.a.a.c.g().a("Beta", "Checking for updates from " + a());
      localObject2 = paramString1;
      localObject1 = paramString1;
      io.a.a.a.c.g().a("Beta", "Checking for updates query params are: " + localMap);
      localObject2 = paramString1;
      localObject1 = paramString1;
      if (paramString1.c())
      {
        localObject2 = paramString1;
        localObject1 = paramString1;
        io.a.a.a.c.g().a("Beta", "Checking for updates was successful");
        localObject2 = paramString1;
        localObject1 = paramString1;
        paramString2 = new JSONObject(paramString1.e());
        localObject2 = paramString1;
        localObject1 = paramString1;
        paramString2 = this.b.a(paramString2);
        if (paramString1 != null)
        {
          paramString1 = paramString1.b("X-REQUEST-ID");
          io.a.a.a.c.g().a("Fabric", "Checking for updates request ID: " + paramString1);
        }
        return paramString2;
      }
      localObject2 = paramString1;
      localObject1 = paramString1;
      io.a.a.a.c.g().d("Beta", "Checking for updates failed. Response code: " + paramString1.b());
      if (paramString1 != null)
      {
        paramString1 = paramString1.b("X-REQUEST-ID");
        io.a.a.a.c.g().a("Fabric", "Checking for updates request ID: " + paramString1);
      }
    }
    catch (Exception paramString1)
    {
      for (;;)
      {
        localObject1 = localObject2;
        io.a.a.a.c.g().d("Beta", "Error while checking for updates from " + a(), paramString1);
        if (localObject2 != null)
        {
          paramString1 = ((io.a.a.a.a.e.d)localObject2).b("X-REQUEST-ID");
          io.a.a.a.c.g().a("Fabric", "Checking for updates request ID: " + paramString1);
        }
      }
    }
    finally
    {
      if (localObject1 == null) {
        break label436;
      }
      paramString2 = ((io.a.a.a.a.e.d)localObject1).b("X-REQUEST-ID");
      io.a.a.a.c.g().a("Fabric", "Checking for updates request ID: " + paramString2);
    }
    return null;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/a/a/b/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */