package com.a.a;

import android.util.Log;
import io.a.a.a.a.b.i;
import io.a.a.a.c;
import io.a.a.a.j;

class a
{
  private final String a;
  private final boolean b;
  
  public a(String paramString, boolean paramBoolean)
  {
    this.a = paramString;
    this.b = paramBoolean;
  }
  
  public void a(String paramString1, String paramString2)
  {
    if ((i.c(this.a)) && (this.b))
    {
      paramString1 = b(paramString1, paramString2);
      Log.e("Fabric", ".");
      Log.e("Fabric", ".     |  | ");
      Log.e("Fabric", ".     |  |");
      Log.e("Fabric", ".     |  |");
      Log.e("Fabric", ".   \\ |  | /");
      Log.e("Fabric", ".    \\    /");
      Log.e("Fabric", ".     \\  /");
      Log.e("Fabric", ".      \\/");
      Log.e("Fabric", ".");
      Log.e("Fabric", paramString1);
      Log.e("Fabric", ".");
      Log.e("Fabric", ".      /\\");
      Log.e("Fabric", ".     /  \\");
      Log.e("Fabric", ".    /    \\");
      Log.e("Fabric", ".   / |  | \\");
      Log.e("Fabric", ".     |  |");
      Log.e("Fabric", ".     |  |");
      Log.e("Fabric", ".     |  |");
      Log.e("Fabric", ".");
      throw new h(paramString1);
    }
    if (!this.b) {
      c.g().a("Fabric", "Configured not to require a build ID.");
    }
  }
  
  protected String b(String paramString1, String paramString2)
  {
    return "This app relies on Crashlytics. Please sign up for access at https://fabric.io/sign_up,\ninstall an Android build tool and ask a team member to invite you to this app's organization.";
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/a/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */