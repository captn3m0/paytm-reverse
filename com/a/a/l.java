package com.a.a;

import io.a.a.a.a.b.a;
import io.a.a.a.a.b.r;
import io.a.a.a.a.e.d;
import io.a.a.a.a.e.e;
import io.a.a.a.h;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class l
  extends a
  implements k
{
  public l(h paramh, String paramString1, String paramString2, e parame)
  {
    super(paramh, paramString1, paramString2, parame, io.a.a.a.a.e.c.b);
  }
  
  private d a(d paramd, j paramj)
  {
    paramd = paramd.a("X-CRASHLYTICS-API-KEY", paramj.a).a("X-CRASHLYTICS-API-CLIENT-TYPE", "android").a("X-CRASHLYTICS-API-CLIENT-VERSION", f.f().d());
    paramj = paramj.b.e().entrySet().iterator();
    while (paramj.hasNext()) {
      paramd = paramd.a((Map.Entry)paramj.next());
    }
    return paramd;
  }
  
  private d b(d paramd, j paramj)
  {
    paramj = paramj.b;
    return paramd.a("report[file]", paramj.b(), "application/octet-stream", paramj.d()).e("report[identifier]", paramj.c());
  }
  
  public boolean a(j paramj)
  {
    paramj = b(a(b(), paramj), paramj);
    io.a.a.a.c.g().a("Fabric", "Sending report to: " + a());
    int i = paramj.b();
    io.a.a.a.c.g().a("Fabric", "Create report request ID: " + paramj.b("X-REQUEST-ID"));
    io.a.a.a.c.g().a("Fabric", "Result was: " + i);
    return r.a(i) == 0;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/a/a/l.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */