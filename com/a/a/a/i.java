package com.a.a.a;

import android.app.Activity;
import java.util.Collections;
import java.util.Map;

final class i
{
  public final j a;
  public final long b;
  public final a c;
  public final Map<String, String> d;
  private String e;
  
  private i(j paramj, long paramLong, a parama, Map<String, String> paramMap)
  {
    this.a = paramj;
    this.b = paramLong;
    this.c = parama;
    this.d = paramMap;
  }
  
  public static i a(j paramj, a parama, Activity paramActivity)
  {
    return a(paramj, parama, Collections.singletonMap("activity", paramActivity.getClass().getName()));
  }
  
  public static i a(j paramj, a parama, Map<String, String> paramMap)
  {
    return new i(paramj, System.currentTimeMillis(), parama, paramMap);
  }
  
  public static i a(j paramj, String paramString)
  {
    paramString = Collections.singletonMap("sessionId", paramString);
    return a(paramj, a.h, paramString);
  }
  
  public static i b(j paramj, String paramString)
  {
    paramString = Collections.singletonMap("sessionId", paramString);
    return a(paramj, a.i, paramString);
  }
  
  public String toString()
  {
    if (this.e == null) {
      this.e = ("[" + getClass().getSimpleName() + ": appBundleId=" + this.a.a + ", executionId=" + this.a.b + ", installationId=" + this.a.c + ", androidId=" + this.a.d + ", advertisingId=" + this.a.e + ", betaDeviceToken=" + this.a.f + ", buildId=" + this.a.g + ", osVersion=" + this.a.h + ", deviceModel=" + this.a.i + ", appVersionCode=" + this.a.j + ", appVersionName=" + this.a.k + ", timestamp=" + this.b + ", type=" + this.c + ", details=" + this.d.toString() + "]");
    }
    return this.e;
  }
  
  static enum a
  {
    private a() {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/a/a/a/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */