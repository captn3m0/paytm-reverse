package com.a.a.a;

import android.app.Activity;
import android.content.Context;
import android.os.Looper;
import io.a.a.a.a.b.n;
import io.a.a.a.a.g.b;
import java.util.HashMap;
import java.util.concurrent.ScheduledExecutorService;

class g
  extends io.a.a.a.a.d.e<i>
{
  private final j d;
  
  public g(Context paramContext, j paramj, f paramf, io.a.a.a.a.e.e parame)
  {
    this(paramContext, paramj, paramf, n.b("Crashlytics SAM"), parame);
  }
  
  g(Context paramContext, j paramj, f paramf, ScheduledExecutorService paramScheduledExecutorService, io.a.a.a.a.e.e parame)
  {
    super(paramContext, new e(paramContext, paramScheduledExecutorService, paramf, parame), paramf, paramScheduledExecutorService);
    this.d = paramj;
  }
  
  private void a(i.a parama, Activity paramActivity, boolean paramBoolean)
  {
    a(i.a(this.d, parama, paramActivity), paramBoolean);
  }
  
  public void a(Activity paramActivity)
  {
    a(i.a.a, paramActivity, false);
  }
  
  void a(final b paramb, final String paramString)
  {
    b(new Runnable()
    {
      public void run()
      {
        try
        {
          ((h)g.c(g.this)).a(paramb, paramString);
          return;
        }
        catch (Exception localException)
        {
          io.a.a.a.a.b.i.a(a.b().z(), "Crashlytics failed to set analytics settings data.", localException);
        }
      }
    });
  }
  
  public void a(final String paramString)
  {
    if (Looper.myLooper() == Looper.getMainLooper()) {
      throw new IllegalStateException("onCrash called from main thread!!!");
    }
    a(new Runnable()
    {
      public void run()
      {
        try
        {
          g.b(g.this).a(i.b(g.a(g.this), paramString));
          return;
        }
        catch (Exception localException)
        {
          io.a.a.a.a.b.i.a(a.b().z(), "Crashlytics failed to record crash event", localException);
        }
      }
    });
  }
  
  public void b()
  {
    a(i.a(this.d, i.a.j, new HashMap()), true);
  }
  
  public void b(Activity paramActivity)
  {
    a(i.a.g, paramActivity, false);
  }
  
  public void b(String paramString)
  {
    a(i.a(this.d, paramString), false);
  }
  
  protected io.a.a.a.a.d.i<i> c()
  {
    return new d();
  }
  
  public void c(Activity paramActivity)
  {
    a(i.a.e, paramActivity, false);
  }
  
  public void d(Activity paramActivity)
  {
    a(i.a.c, paramActivity, false);
  }
  
  public void e(Activity paramActivity)
  {
    a(i.a.d, paramActivity, false);
  }
  
  public void f(Activity paramActivity)
  {
    a(i.a.b, paramActivity, false);
  }
  
  public void g(Activity paramActivity)
  {
    a(i.a.f, paramActivity, false);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/a/a/a/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */