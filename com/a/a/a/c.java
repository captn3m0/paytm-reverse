package com.a.a.a;

import io.a.a.a.a.b.i;
import io.a.a.a.a.b.r;
import io.a.a.a.a.d.k;
import io.a.a.a.a.e.d;
import io.a.a.a.a.e.e;
import io.a.a.a.h;
import java.io.File;
import java.util.Iterator;
import java.util.List;

class c
  extends io.a.a.a.a.b.a
  implements k
{
  private final String b;
  
  public c(h paramh, String paramString1, String paramString2, e parame, String paramString3)
  {
    this(paramh, paramString1, paramString2, parame, paramString3, io.a.a.a.a.e.c.b);
  }
  
  c(h paramh, String paramString1, String paramString2, e parame, String paramString3, io.a.a.a.a.e.c paramc)
  {
    super(paramh, paramString1, paramString2, parame, paramc);
    this.b = paramString3;
  }
  
  private d a(d paramd, String paramString)
  {
    return paramd.a("X-CRASHLYTICS-API-CLIENT-TYPE", "android").a("X-CRASHLYTICS-API-CLIENT-VERSION", a.b().d()).a("X-CRASHLYTICS-API-KEY", paramString);
  }
  
  private d a(d paramd, List<File> paramList)
  {
    int i = 0;
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      File localFile = (File)paramList.next();
      i.a(a.b().z(), "Adding analytics session file " + localFile.getName() + " to multipart POST");
      paramd.a("session_analytics_file_" + i, localFile.getName(), "application/vnd.crashlytics.android.events", localFile);
      i += 1;
    }
    return paramd;
  }
  
  public boolean a(List<File> paramList)
  {
    d locald = a(a(b(), this.b), paramList);
    i.a(a.b().z(), "Sending " + paramList.size() + " analytics files to " + a());
    int i = locald.b();
    i.a(a.b().z(), "Response code for analytics file send is " + i);
    return r.a(i) == 0;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/a/a/a/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */