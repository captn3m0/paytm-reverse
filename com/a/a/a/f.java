package com.a.a.a;

import android.content.Context;
import io.a.a.a.a.d.d;
import io.a.a.a.a.d.g;
import io.a.a.a.a.g.b;
import java.io.IOException;
import java.util.UUID;

class f
  extends d
{
  private b h;
  
  f(Context paramContext, k paramk, io.a.a.a.a.b.k paramk1, g paramg)
    throws IOException
  {
    this(paramContext, paramk, paramk1, paramg, 100);
  }
  
  f(Context paramContext, k paramk, io.a.a.a.a.b.k paramk1, g paramg, int paramInt)
    throws IOException
  {
    super(paramContext, paramk, paramk1, paramg, paramInt);
  }
  
  protected String a()
  {
    UUID localUUID = UUID.randomUUID();
    return "sa" + "_" + localUUID.toString() + "_" + this.c.a() + ".tap";
  }
  
  void a(b paramb)
  {
    this.h = paramb;
  }
  
  protected int b()
  {
    if (this.h == null) {
      return 8000;
    }
    return this.h.c;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/a/a/a/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */