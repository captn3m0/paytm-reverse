package com.a.a.a;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import io.a.a.a.a.b.i;
import io.a.a.a.a.b.j.a;
import io.a.a.a.a.b.j.b;
import io.a.a.a.a.b.o;
import io.a.a.a.a.b.o.a;
import io.a.a.a.a.b.s;
import io.a.a.a.a.d.l;
import io.a.a.a.a.g.m;
import io.a.a.a.a.g.q;
import io.a.a.a.a.g.t;
import io.a.a.a.h;
import java.io.File;
import java.util.Map;
import java.util.UUID;

public class a
  extends h<Boolean>
{
  private String f;
  private String g;
  private io.a.a.a.a.f.b h;
  private long i;
  private g j;
  
  @TargetApi(14)
  private void a(Context paramContext)
  {
    try
    {
      f localf = new f(paramContext, new k(), new s(), new l(z(), i(), "session_analytics.tap", "session_analytics_to_send"));
      Object localObject3 = y();
      Object localObject4 = ((o)localObject3).g();
      Object localObject1 = paramContext.getPackageName();
      Object localObject2 = ((o)localObject3).b();
      String str1 = (String)((Map)localObject4).get(o.a.d);
      String str2 = (String)((Map)localObject4).get(o.a.g);
      localObject4 = (String)((Map)localObject4).get(o.a.c);
      String str3 = i.m(paramContext);
      String str4 = ((o)localObject3).d();
      localObject3 = ((o)localObject3).e();
      localObject1 = new j((String)localObject1, UUID.randomUUID().toString(), (String)localObject2, str1, str2, (String)localObject4, str3, str4, (String)localObject3, this.f, this.g);
      localObject2 = (Application)z().getApplicationContext();
      if ((localObject2 != null) && (Build.VERSION.SDK_INT >= 14)) {}
      for (this.j = new b((Application)localObject2, (j)localObject1, localf, new io.a.a.a.a.e.b(io.a.a.a.c.g())); a(this.i); this.j = new g(paramContext, (j)localObject1, localf, new io.a.a.a.a.e.b(io.a.a.a.c.g())))
      {
        io.a.a.a.c.g().a("Answers", "First launch");
        g();
        return;
      }
      return;
    }
    catch (Exception localException)
    {
      i.a(paramContext, "Crashlytics failed to initialize session analytics.", localException);
    }
  }
  
  public static a b()
  {
    return (a)io.a.a.a.c.a(a.class);
  }
  
  public void a(j.a parama)
  {
    if (this.j != null) {
      this.j.a(parama.a());
    }
  }
  
  public void a(j.b paramb)
  {
    if (this.j != null) {
      this.j.b(paramb.a());
    }
  }
  
  @SuppressLint({"NewApi"})
  protected boolean a()
  {
    try
    {
      this.h = new io.a.a.a.a.f.c(io.a.a.a.c.a(a.class));
      Context localContext = z();
      PackageInfo localPackageInfo = localContext.getPackageManager().getPackageInfo(localContext.getPackageName(), 0);
      this.f = Integer.toString(localPackageInfo.versionCode);
      if (localPackageInfo.versionName == null) {}
      for (String str = "0.0";; str = localPackageInfo.versionName)
      {
        this.g = str;
        if (Build.VERSION.SDK_INT < 9) {
          break;
        }
        this.i = localPackageInfo.firstInstallTime;
        break label135;
      }
      this.i = new File(localContext.getPackageManager().getApplicationInfo(localContext.getPackageName(), 0).sourceDir).lastModified();
    }
    catch (Exception localException)
    {
      io.a.a.a.c.g().d("Answers", "Error setting up app properties", localException);
      return false;
    }
    label135:
    return true;
  }
  
  boolean a(long paramLong)
  {
    return (!h()) && (b(paramLong));
  }
  
  boolean b(long paramLong)
  {
    return System.currentTimeMillis() - paramLong < 3600000L;
  }
  
  public String c()
  {
    return "com.crashlytics.sdk.android:answers";
  }
  
  public String d()
  {
    return "1.1.1.32";
  }
  
  protected Boolean e()
  {
    Context localContext = z();
    a(localContext);
    try
    {
      t localt = q.a().b();
      if (localt == null) {
        return Boolean.valueOf(false);
      }
      if (localt.d.d)
      {
        this.j.a(localt.e, f());
        return Boolean.valueOf(true);
      }
      i.a(localContext, "Disabling analytics collection based on settings flag value.");
      this.j.a();
      return Boolean.valueOf(false);
    }
    catch (Exception localException)
    {
      io.a.a.a.c.g().d("Answers", "Error dealing with settings", localException);
    }
    return Boolean.valueOf(false);
  }
  
  String f()
  {
    return i.b(z(), "com.crashlytics.ApiEndpoint");
  }
  
  @SuppressLint({"CommitPrefEdits"})
  void g()
  {
    if (this.j != null)
    {
      this.j.b();
      this.h.a(this.h.b().putBoolean("analytics_launched", true));
    }
  }
  
  boolean h()
  {
    return this.h.a().getBoolean("analytics_launched", false);
  }
  
  File i()
  {
    return new io.a.a.a.a.f.a(this).a();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/a/a/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */