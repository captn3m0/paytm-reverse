package com.a.a.a;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import io.a.a.a.a.b.i;
import io.a.a.a.a.b.n;
import io.a.a.a.a.e.e;
import java.util.concurrent.ScheduledExecutorService;

@TargetApi(14)
class b
  extends g
{
  private final Application d;
  private final Application.ActivityLifecycleCallbacks e = new Application.ActivityLifecycleCallbacks()
  {
    public void onActivityCreated(Activity paramAnonymousActivity, Bundle paramAnonymousBundle)
    {
      b.this.a(paramAnonymousActivity);
    }
    
    public void onActivityDestroyed(Activity paramAnonymousActivity)
    {
      b.this.b(paramAnonymousActivity);
    }
    
    public void onActivityPaused(Activity paramAnonymousActivity)
    {
      b.this.c(paramAnonymousActivity);
    }
    
    public void onActivityResumed(Activity paramAnonymousActivity)
    {
      b.this.d(paramAnonymousActivity);
    }
    
    public void onActivitySaveInstanceState(Activity paramAnonymousActivity, Bundle paramAnonymousBundle)
    {
      b.this.e(paramAnonymousActivity);
    }
    
    public void onActivityStarted(Activity paramAnonymousActivity)
    {
      b.this.f(paramAnonymousActivity);
    }
    
    public void onActivityStopped(Activity paramAnonymousActivity)
    {
      b.this.g(paramAnonymousActivity);
    }
  };
  
  public b(Application paramApplication, j paramj, f paramf, e parame)
  {
    this(paramApplication, paramj, paramf, n.b("Crashlytics Trace Manager"), parame);
  }
  
  b(Application paramApplication, j paramj, f paramf, ScheduledExecutorService paramScheduledExecutorService, e parame)
  {
    super(paramApplication, paramj, paramf, paramScheduledExecutorService, parame);
    this.d = paramApplication;
    i.a(a.b().z(), "Registering activity lifecycle callbacks for session analytics.");
    paramApplication.registerActivityLifecycleCallbacks(this.e);
  }
  
  public void a()
  {
    i.a(a.b().z(), "Unregistering activity lifecycle callbacks for session analytics");
    this.d.unregisterActivityLifecycleCallbacks(this.e);
    super.a();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/a/a/a/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */