package com.a.a.a;

import android.annotation.TargetApi;
import android.os.Build.VERSION;
import io.a.a.a.a.d.c;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

class k
  implements c<i>
{
  private JSONObject a(Map<String, String> paramMap)
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject();
    paramMap = paramMap.entrySet().iterator();
    while (paramMap.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)paramMap.next();
      localJSONObject.put((String)localEntry.getKey(), localEntry.getValue());
    }
    return localJSONObject;
  }
  
  public byte[] a(i parami)
    throws IOException
  {
    return b(parami).toString().getBytes("UTF-8");
  }
  
  @TargetApi(9)
  public JSONObject b(i parami)
    throws IOException
  {
    try
    {
      JSONObject localJSONObject = new JSONObject();
      j localj = parami.a;
      localJSONObject.put("appBundleId", localj.a);
      localJSONObject.put("executionId", localj.b);
      localJSONObject.put("installationId", localj.c);
      localJSONObject.put("androidId", localj.d);
      localJSONObject.put("advertisingId", localj.e);
      localJSONObject.put("betaDeviceToken", localj.f);
      localJSONObject.put("buildId", localj.g);
      localJSONObject.put("osVersion", localj.h);
      localJSONObject.put("deviceModel", localj.i);
      localJSONObject.put("appVersionCode", localj.j);
      localJSONObject.put("appVersionName", localj.k);
      localJSONObject.put("timestamp", parami.b);
      localJSONObject.put("type", parami.c.toString());
      localJSONObject.put("details", a(parami.d));
      return localJSONObject;
    }
    catch (JSONException parami)
    {
      if (Build.VERSION.SDK_INT >= 9) {
        throw new IOException(parami.getMessage(), parami);
      }
      throw new IOException(parami.getMessage());
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/a/a/a/k.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */