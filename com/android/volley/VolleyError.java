package com.android.volley;

import android.net.Uri;

public class VolleyError
  extends Exception
{
  private String mAlertMessage;
  private String mAlertTitle;
  private String mUrl;
  public final NetworkResponse networkResponse;
  private String uniqueReference;
  
  public VolleyError()
  {
    this.networkResponse = null;
  }
  
  public VolleyError(NetworkResponse paramNetworkResponse)
  {
    this.networkResponse = paramNetworkResponse;
  }
  
  public VolleyError(NetworkResponse paramNetworkResponse, String paramString)
  {
    super(paramString);
    this.networkResponse = paramNetworkResponse;
  }
  
  public VolleyError(String paramString)
  {
    super(paramString);
    this.networkResponse = null;
  }
  
  public VolleyError(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
    this.networkResponse = null;
  }
  
  public VolleyError(Throwable paramThrowable)
  {
    super(paramThrowable);
    this.networkResponse = null;
  }
  
  public VolleyError(Throwable paramThrowable, String paramString)
  {
    super(paramThrowable);
    this.networkResponse = null;
  }
  
  public String getAlertMessage()
  {
    return this.mAlertMessage;
  }
  
  public String getAlertTitle()
  {
    return this.mAlertTitle;
  }
  
  public String getFullUrl()
  {
    return this.mUrl;
  }
  
  public String getUniqueReference()
  {
    return this.uniqueReference;
  }
  
  public String getUrl()
  {
    try
    {
      Object localObject = Uri.parse(this.mUrl.toString());
      localObject = ((Uri)localObject).getHost() + ((Uri)localObject).getPath();
      return (String)localObject;
    }
    catch (Exception localException) {}
    return this.mUrl;
  }
  
  public void setAlertMessage(String paramString)
  {
    this.mAlertMessage = paramString;
  }
  
  public void setUniqueReference(String paramString)
  {
    this.uniqueReference = paramString;
  }
  
  public void setUrl(String paramString)
  {
    this.mUrl = paramString;
  }
  
  public void setmAlertTitle(String paramString)
  {
    this.mAlertTitle = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/android/volley/VolleyError.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */