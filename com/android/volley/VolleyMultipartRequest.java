package com.android.volley;

import android.content.Context;
import com.android.volley.toolbox.HttpHeaderParser;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLConnection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class VolleyMultipartRequest
  extends Request<String>
{
  private final String BOUNDARY = "WebKitFormBoundarynrHfHHBqL64QHPve";
  private final int MY_SOCKET_TIMEOUT_MS = 50000;
  private final String boundary = "WebKitFormBoundarynrHfHHBqL64QHPve";
  private Context context;
  DataOutputStream dataOutputStream;
  private Map<String, File> file;
  private Map<String, String> headerPart;
  private final String lineEnd = "\r\n";
  private final Response.ErrorListener mErrorListener;
  private final Response.Listener<String> mListener;
  private final String mMimeType = "multipart/form-data; boundary=WebKitFormBoundarynrHfHHBqL64QHPve";
  private Map<String, String> mStringPart;
  private final String twoHyphens = "--";
  
  public VolleyMultipartRequest(Context paramContext, int paramInt, String paramString, Response.ErrorListener paramErrorListener, Response.Listener<String> paramListener, Map<String, File> paramMap, Map<String, String> paramMap1)
  {
    super(paramInt, paramString, paramErrorListener);
    this.mListener = paramListener;
    this.mErrorListener = paramErrorListener;
    this.context = paramContext;
    this.file = paramMap;
    this.mStringPart = paramMap1;
    setRetryPolicy(new DefaultRetryPolicy(50000, 1, 1.0F));
  }
  
  public VolleyMultipartRequest(Context paramContext, String paramString, Response.ErrorListener paramErrorListener, Response.Listener<String> paramListener, Map<String, File> paramMap, Map<String, String> paramMap1, Map<String, String> paramMap2)
  {
    super(1, paramString, paramErrorListener);
    this.mListener = paramListener;
    this.mErrorListener = paramErrorListener;
    this.context = paramContext;
    this.file = paramMap;
    this.mStringPart = paramMap1;
    this.headerPart = paramMap2;
    setRetryPolicy(new DefaultRetryPolicy(50000, 1, 1.0F));
  }
  
  private void addFilePart(String paramString, File paramFile)
    throws IOException
  {
    PrintWriter localPrintWriter = new PrintWriter(this.dataOutputStream, true);
    String str = "";
    if (paramFile != null) {
      str = paramFile.getName();
    }
    localPrintWriter.append("--WebKitFormBoundarynrHfHHBqL64QHPve").append("\r\n");
    localPrintWriter.append("Content-Disposition: form-data; name=\"" + paramString + "\"; filename=\"" + str + "\"").append("\r\n");
    localPrintWriter.append("Content-Type: " + URLConnection.guessContentTypeFromName(str)).append("\r\n");
    localPrintWriter.append("Content-Transfer-Encoding: binary").append("\r\n");
    localPrintWriter.append("\r\n");
    localPrintWriter.flush();
    if (paramFile != null)
    {
      paramString = new FileInputStream(paramFile);
      paramFile = new byte['က'];
      for (;;)
      {
        int i = paramString.read(paramFile);
        if (i == -1) {
          break;
        }
        this.dataOutputStream.write(paramFile, 0, i);
      }
      this.dataOutputStream.flush();
      paramString.close();
    }
    localPrintWriter.append("\r\n");
    localPrintWriter.flush();
  }
  
  private void buildPart(String paramString1, String paramString2)
    throws IOException
  {
    this.dataOutputStream.writeBytes("--WebKitFormBoundarynrHfHHBqL64QHPve\r\n");
    this.dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + paramString1 + "\"" + "\r\n");
    this.dataOutputStream.writeBytes("Content-Type: text/plain; charset=UTF-8\r\n");
    this.dataOutputStream.writeBytes("\r\n");
    this.dataOutputStream.writeBytes(paramString2 + "\r\n");
  }
  
  public void deliverError(VolleyError paramVolleyError)
  {
    this.mErrorListener.onErrorResponse(paramVolleyError);
  }
  
  protected void deliverResponse(String paramString)
  {
    this.mListener.onResponse(paramString);
  }
  
  public byte[] getBody()
    throws AuthFailureError
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    this.dataOutputStream = new DataOutputStream(localByteArrayOutputStream);
    Map.Entry localEntry;
    try
    {
      Iterator localIterator1 = this.mStringPart.entrySet().iterator();
      while (localIterator1.hasNext())
      {
        localEntry = (Map.Entry)localIterator1.next();
        buildPart((String)localEntry.getKey(), (String)localEntry.getValue());
        continue;
        return localByteArrayOutputStream.toByteArray();
      }
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
    for (;;)
    {
      Iterator localIterator2 = this.file.entrySet().iterator();
      while (localIterator2.hasNext())
      {
        localEntry = (Map.Entry)localIterator2.next();
        addFilePart((String)localEntry.getKey(), (File)localEntry.getValue());
      }
      this.dataOutputStream.writeBytes("--WebKitFormBoundarynrHfHHBqL64QHPve--\r\n");
    }
  }
  
  public String getBodyContentType()
  {
    return "multipart/form-data; boundary=WebKitFormBoundarynrHfHHBqL64QHPve";
  }
  
  public Map<String, String> getHeaders()
    throws AuthFailureError
  {
    Map localMap = this.headerPart;
    Object localObject;
    if (localMap != null)
    {
      localObject = localMap;
      if (!localMap.equals(Collections.emptyMap())) {}
    }
    else
    {
      localObject = new HashMap();
    }
    ((Map)localObject).put("Connection", "keep-alive");
    ((Map)localObject).put("Content-Type", "multipart/form-data; boundary=WebKitFormBoundarynrHfHHBqL64QHPve");
    return (Map<String, String>)localObject;
  }
  
  protected Response<String> parseNetworkResponse(NetworkResponse paramNetworkResponse)
  {
    try
    {
      Response localResponse = Response.success(new String(paramNetworkResponse.data, HttpHeaderParser.parseCharset(paramNetworkResponse.headers)), getCacheEntry());
      return localResponse;
    }
    catch (UnsupportedEncodingException paramNetworkResponse)
    {
      return Response.error(new ParseError(paramNetworkResponse));
    }
    catch (Exception localException) {}
    return Response.error(new VolleyError(paramNetworkResponse));
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/android/volley/VolleyMultipartRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */