package com.android.volley;

public class ServerError
  extends VolleyError
{
  public ServerError() {}
  
  public ServerError(NetworkResponse paramNetworkResponse)
  {
    super(paramNetworkResponse);
  }
  
  public ServerError(NetworkResponse paramNetworkResponse, String paramString)
  {
    super(paramNetworkResponse, paramString);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/android/volley/ServerError.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */