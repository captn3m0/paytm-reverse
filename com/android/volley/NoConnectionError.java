package com.android.volley;

public class NoConnectionError
  extends NetworkError
{
  public NoConnectionError() {}
  
  public NoConnectionError(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
  
  public NoConnectionError(Throwable paramThrowable, String paramString)
  {
    super(paramThrowable, paramString);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/android/volley/NoConnectionError.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */