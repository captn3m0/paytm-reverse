package com.android.volley.toolbox;

import android.os.SystemClock;
import com.android.volley.AuthFailureError;
import com.android.volley.Cache.Entry;
import com.android.volley.Network;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.cookie.DateUtils;

public class BasicNetwork
  implements Network
{
  protected static final boolean DEBUG = VolleyLog.DEBUG;
  private static int DEFAULT_POOL_SIZE = 4096;
  private static int SLOW_REQUEST_THRESHOLD_MS = 3000;
  protected final HttpStack mHttpStack;
  protected final ByteArrayPool mPool;
  
  public BasicNetwork(HttpStack paramHttpStack)
  {
    this(paramHttpStack, new ByteArrayPool(DEFAULT_POOL_SIZE));
  }
  
  public BasicNetwork(HttpStack paramHttpStack, ByteArrayPool paramByteArrayPool)
  {
    this.mHttpStack = paramHttpStack;
    this.mPool = paramByteArrayPool;
  }
  
  private void addCacheHeaders(Map<String, String> paramMap, Cache.Entry paramEntry)
  {
    if (paramEntry == null) {}
    do
    {
      return;
      if (paramEntry.etag != null) {
        paramMap.put("If-None-Match", paramEntry.etag);
      }
    } while (paramEntry.serverDate <= 0L);
    paramMap.put("If-Modified-Since", DateUtils.formatDate(new Date(paramEntry.serverDate)));
  }
  
  private static void attemptRetryOnException(String paramString, Request<?> paramRequest, VolleyError paramVolleyError)
    throws VolleyError
  {
    RetryPolicy localRetryPolicy = paramRequest.getRetryPolicy();
    int i = paramRequest.getTimeoutMs();
    try
    {
      localRetryPolicy.retry(paramVolleyError);
      paramRequest.addMarker(String.format("%s-retry [timeout=%s]", new Object[] { paramString, Integer.valueOf(i) }));
      return;
    }
    catch (VolleyError paramVolleyError)
    {
      paramRequest.addMarker(String.format("%s-timeout-giveup [timeout=%s]", new Object[] { paramString, Integer.valueOf(i) }));
      throw paramVolleyError;
    }
  }
  
  private static Map<String, String> convertHeaders(Header[] paramArrayOfHeader)
  {
    HashMap localHashMap = new HashMap();
    int i = 0;
    while (i < paramArrayOfHeader.length)
    {
      localHashMap.put(paramArrayOfHeader[i].getName(), paramArrayOfHeader[i].getValue());
      i += 1;
    }
    return localHashMap;
  }
  
  private byte[] entityToBytes(HttpEntity paramHttpEntity)
    throws IOException, ServerError
  {
    PoolingByteArrayOutputStream localPoolingByteArrayOutputStream = new PoolingByteArrayOutputStream(this.mPool, (int)paramHttpEntity.getContentLength());
    byte[] arrayOfByte2 = null;
    Object localObject1 = null;
    byte[] arrayOfByte1 = arrayOfByte2;
    try
    {
      InputStream localInputStream = paramHttpEntity.getContent();
      if (localInputStream == null)
      {
        arrayOfByte1 = arrayOfByte2;
        localObject1 = localInputStream;
        throw new ServerError();
      }
    }
    finally {}
    try
    {
      paramHttpEntity.consumeContent();
      if (localObject1 != null) {
        ((InputStream)localObject1).close();
      }
    }
    catch (IOException paramHttpEntity)
    {
      for (;;)
      {
        boolean bool;
        byte[] arrayOfByte3;
        VolleyLog.v("Error occured when calling consumingContent", new Object[0]);
      }
    }
    this.mPool.returnBuf(arrayOfByte1);
    localPoolingByteArrayOutputStream.close();
    throw ((Throwable)localObject2);
    arrayOfByte1 = arrayOfByte2;
    localObject1 = localObject2;
    bool = ((InputStream)localObject2).markSupported();
    arrayOfByte1 = arrayOfByte2;
    localObject1 = localObject2;
    arrayOfByte2 = this.mPool.getBuf(1024);
    for (;;)
    {
      arrayOfByte1 = arrayOfByte2;
      localObject1 = localObject2;
      int i = ((InputStream)localObject2).read(arrayOfByte2);
      if (i == -1) {
        break;
      }
      arrayOfByte1 = arrayOfByte2;
      localObject1 = localObject2;
      localPoolingByteArrayOutputStream.write(arrayOfByte2, 0, i);
    }
    if (bool)
    {
      arrayOfByte1 = arrayOfByte2;
      localObject1 = localObject2;
      ((InputStream)localObject2).reset();
    }
    arrayOfByte1 = arrayOfByte2;
    localObject1 = localObject2;
    arrayOfByte3 = localPoolingByteArrayOutputStream.toByteArray();
    try
    {
      paramHttpEntity.consumeContent();
      if (localObject2 != null) {
        ((InputStream)localObject2).close();
      }
    }
    catch (IOException paramHttpEntity)
    {
      for (;;)
      {
        VolleyLog.v("Error occured when calling consumingContent", new Object[0]);
      }
    }
    this.mPool.returnBuf(arrayOfByte2);
    localPoolingByteArrayOutputStream.close();
    return arrayOfByte3;
  }
  
  private void logSlowRequests(long paramLong, Request<?> paramRequest, byte[] paramArrayOfByte, StatusLine paramStatusLine)
  {
    if ((DEBUG) && (paramLong > SLOW_REQUEST_THRESHOLD_MS)) {
      if (paramArrayOfByte == null) {
        break label82;
      }
    }
    label82:
    for (paramArrayOfByte = Integer.valueOf(paramArrayOfByte.length);; paramArrayOfByte = "null")
    {
      VolleyLog.d("HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]", new Object[] { paramRequest, Long.valueOf(paramLong), paramArrayOfByte, Integer.valueOf(paramStatusLine.getStatusCode()), Integer.valueOf(paramRequest.getRetryPolicy().getCurrentRetryCount()) });
      return;
    }
  }
  
  protected void logError(String paramString1, String paramString2, long paramLong)
  {
    VolleyLog.v("HTTP ERROR(%s) %d ms to fetch %s", new Object[] { paramString1, Long.valueOf(SystemClock.elapsedRealtime() - paramLong), paramString2 });
  }
  
  public NetworkResponse performRequest(Request<?> paramRequest)
    throws VolleyError
  {
    long l = SystemClock.elapsedRealtime();
    for (;;)
    {
      HttpResponse localHttpResponse2 = null;
      Object localObject5 = null;
      Object localObject1 = new HashMap();
      Object localObject3 = localObject5;
      HttpResponse localHttpResponse1 = localHttpResponse2;
      Object localObject4 = localObject1;
      int i;
      try
      {
        localObject6 = new HashMap();
        localObject3 = localObject5;
        localHttpResponse1 = localHttpResponse2;
        localObject4 = localObject1;
        addCacheHeaders((Map)localObject6, paramRequest.getCacheEntry());
        localObject3 = localObject5;
        localHttpResponse1 = localHttpResponse2;
        localObject4 = localObject1;
        localHttpResponse2 = this.mHttpStack.performRequest(paramRequest, (Map)localObject6);
        localObject3 = localObject5;
        localHttpResponse1 = localHttpResponse2;
        localObject4 = localObject1;
        localStatusLine = localHttpResponse2.getStatusLine();
        localObject3 = localObject5;
        localHttpResponse1 = localHttpResponse2;
        localObject4 = localObject1;
        i = localStatusLine.getStatusCode();
        localObject3 = localObject5;
        localHttpResponse1 = localHttpResponse2;
        localObject4 = localObject1;
        localObject6 = convertHeaders(localHttpResponse2.getAllHeaders());
        if (i == 304)
        {
          localObject3 = localObject5;
          localHttpResponse1 = localHttpResponse2;
          localObject4 = localObject6;
          if (paramRequest.getCacheEntry() == null) {}
          for (localObject1 = null;; localObject1 = paramRequest.getCacheEntry().data)
          {
            localObject3 = localObject5;
            localHttpResponse1 = localHttpResponse2;
            localObject4 = localObject6;
            return new NetworkResponse(304, (byte[])localObject1, (Map)localObject6, true);
            localObject3 = localObject5;
            localHttpResponse1 = localHttpResponse2;
            localObject4 = localObject6;
          }
        }
        localObject3 = localObject5;
        localHttpResponse1 = localHttpResponse2;
        localObject4 = localObject6;
        if (localHttpResponse2.getEntity() == null) {
          break label403;
        }
        localObject3 = localObject5;
        localHttpResponse1 = localHttpResponse2;
        localObject4 = localObject6;
        localObject1 = entityToBytes(localHttpResponse2.getEntity());
      }
      catch (SocketTimeoutException paramRequest)
      {
        Object localObject6;
        for (;;)
        {
          StatusLine localStatusLine;
          throw new NoConnectionError(paramRequest, "SocketTimeoutException" + paramRequest.getMessage());
          localObject3 = localObject5;
          localHttpResponse1 = localHttpResponse2;
          localObject4 = localObject6;
          localObject1 = new byte[0];
        }
        localObject3 = localObject1;
        localHttpResponse1 = localHttpResponse2;
        localObject4 = localObject6;
        localObject1 = new NetworkResponse(i, (byte[])localObject1, (Map)localObject6, false);
        return (NetworkResponse)localObject1;
      }
      catch (ConnectTimeoutException paramRequest)
      {
        throw new NoConnectionError(paramRequest, "ConnectTimeoutException" + paramRequest.getMessage());
      }
      catch (MalformedURLException localMalformedURLException)
      {
        throw new RuntimeException("Bad URL " + paramRequest.getUrl(), localMalformedURLException);
      }
      catch (IOException localIOException1)
      {
        if (localHttpResponse1 == null) {
          break label642;
        }
        i = localHttpResponse1.getStatusLine().getStatusCode();
        if ((i != 301) && (i != 302)) {
          break label652;
        }
        VolleyLog.e("Request at %s has been redirected to %s", new Object[] { paramRequest.getOriginUrl(), paramRequest.getUrl() });
        if (localObject3 == null) {
          break label751;
        }
        localObject2 = new NetworkResponse(i, (byte[])localObject3, (Map)localObject4, false);
        if ((i != 401) && (i != 403)) {
          break label679;
        }
        attemptRetryOnException("auth", paramRequest, new AuthFailureError((NetworkResponse)localObject2));
        throw new ServerError((NetworkResponse)localObject2, "ServerError");
        throw new NoConnectionError((Throwable)localObject2);
      }
      localObject3 = localObject1;
      localHttpResponse1 = localHttpResponse2;
      localObject4 = localObject6;
      logSlowRequests(SystemClock.elapsedRealtime() - l, paramRequest, (byte[])localObject1, localStatusLine);
      if ((i == 301) || (i == 302))
      {
        localObject3 = localObject1;
        localHttpResponse1 = localHttpResponse2;
        localObject4 = localObject6;
        paramRequest.setRedirectUrl((String)((Map)localObject6).get("Location"));
        localObject3 = localObject1;
        localHttpResponse1 = localHttpResponse2;
        localObject4 = localObject6;
        throw new IOException();
      }
      for (;;)
      {
        label403:
        label642:
        label652:
        VolleyLog.e("Unexpected response code %d for %s", new Object[] { Integer.valueOf(i), paramRequest.getUrl() });
      }
      label679:
      if ((i != 301) && (i != 302)) {
        break;
      }
      Object localObject2 = new HashMap();
      addCacheHeaders((Map)localObject2, paramRequest.getCacheEntry());
      try
      {
        this.mHttpStack.performRequest(paramRequest, (Map)localObject2);
      }
      catch (IOException localIOException2)
      {
        localIOException2.printStackTrace();
      }
    }
    throw new ServerError(localIOException2, "ServerError");
    label751:
    throw new NetworkError(null, "NetworkError");
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/android/volley/toolbox/BasicNetwork.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */