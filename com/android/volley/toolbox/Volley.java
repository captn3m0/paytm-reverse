package com.android.volley.toolbox;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.http.AndroidHttpClient;
import android.os.Build.VERSION;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import java.io.File;

public class Volley
{
  private static final String DEFAULT_CACHE_DIR = "volley";
  private static final String DEFAULT_DISK_CACHE_DIR = "images";
  private static final int DEFAULT_DISK_USAGE_BYTES = 5242880;
  
  private static File getCacheRoot(Context paramContext)
  {
    File localFile2 = paramContext.getExternalCacheDir();
    File localFile1 = localFile2;
    if (localFile2 == null)
    {
      VolleyLog.wtf("Can't find External Cache Dir, switching to application specific cache directory", new Object[0]);
      localFile1 = paramContext.getCacheDir();
    }
    return localFile1;
  }
  
  public static RequestQueue newRequestQueue(Context paramContext, HttpStack paramHttpStack, boolean paramBoolean)
  {
    File localFile;
    Object localObject;
    if (paramBoolean)
    {
      localFile = new File(getCacheRoot(paramContext), "images");
      localObject = "volley/0";
    }
    try
    {
      String str = paramContext.getPackageName();
      paramContext = paramContext.getPackageManager().getPackageInfo(str, 0);
      paramContext = str + "/" + paramContext.versionCode;
      localObject = paramContext;
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      label92:
      label163:
      for (;;) {}
    }
    paramContext = paramHttpStack;
    if (paramHttpStack == null)
    {
      if (Build.VERSION.SDK_INT >= 9) {
        paramContext = new HurlStack();
      }
    }
    else
    {
      paramContext = new BasicNetwork(paramContext);
      if (!paramBoolean) {
        break label163;
      }
    }
    for (paramContext = new RequestQueue(new DiskBasedCache(localFile, 5242880), paramContext);; paramContext = new RequestQueue(new DiskBasedCache(localFile), paramContext))
    {
      paramContext.start();
      return paramContext;
      localFile = new File(paramContext.getCacheDir(), "volley");
      break;
      paramContext = new HttpClientStack(AndroidHttpClient.newInstance((String)localObject));
      break label92;
    }
  }
  
  public static RequestQueue newRequestQueue(Context paramContext, boolean paramBoolean)
  {
    return newRequestQueue(paramContext, null, paramBoolean);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/android/volley/toolbox/Volley.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */