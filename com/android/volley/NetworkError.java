package com.android.volley;

public class NetworkError
  extends VolleyError
{
  public NetworkError() {}
  
  public NetworkError(NetworkResponse paramNetworkResponse)
  {
    super(paramNetworkResponse);
  }
  
  public NetworkError(NetworkResponse paramNetworkResponse, String paramString)
  {
    super(paramNetworkResponse, paramString);
  }
  
  public NetworkError(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
  
  public NetworkError(Throwable paramThrowable, String paramString)
  {
    super(paramThrowable, paramString);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/android/volley/NetworkError.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */