package com.squareup.a;

import android.content.Context;
import android.net.Uri;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.CacheControl;
import com.squareup.okhttp.CacheControl.Builder;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request.Builder;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class s
  implements j
{
  private final OkHttpClient a;
  
  public s(Context paramContext)
  {
    this(ae.b(paramContext));
  }
  
  public s(Context paramContext, long paramLong)
  {
    this(ae.b(paramContext), paramLong);
  }
  
  public s(OkHttpClient paramOkHttpClient)
  {
    this.a = paramOkHttpClient;
  }
  
  public s(File paramFile)
  {
    this(paramFile, ae.a(paramFile));
  }
  
  public s(File paramFile, long paramLong)
  {
    this(a());
    try
    {
      this.a.setCache(new Cache(paramFile, paramLong));
      return;
    }
    catch (IOException paramFile) {}
  }
  
  private static OkHttpClient a()
  {
    OkHttpClient localOkHttpClient = new OkHttpClient();
    localOkHttpClient.setConnectTimeout(15000L, TimeUnit.MILLISECONDS);
    localOkHttpClient.setReadTimeout(20000L, TimeUnit.MILLISECONDS);
    localOkHttpClient.setWriteTimeout(20000L, TimeUnit.MILLISECONDS);
    return localOkHttpClient;
  }
  
  public j.a a(Uri paramUri, int paramInt)
    throws IOException
  {
    Object localObject = null;
    if (paramInt != 0) {
      if (!q.c(paramInt)) {
        break label116;
      }
    }
    for (localObject = CacheControl.FORCE_CACHE;; localObject = ((CacheControl.Builder)localObject).build())
    {
      paramUri = new Request.Builder().url(paramUri.toString());
      if (localObject != null) {
        paramUri.cacheControl((CacheControl)localObject);
      }
      paramUri = this.a.newCall(paramUri.build()).execute();
      int i = paramUri.code();
      if (i < 300) {
        break;
      }
      paramUri.body().close();
      throw new j.b(i + " " + paramUri.message(), paramInt, i);
      label116:
      localObject = new CacheControl.Builder();
      if (!q.a(paramInt)) {
        ((CacheControl.Builder)localObject).noCache();
      }
      if (!q.b(paramInt)) {
        ((CacheControl.Builder)localObject).noStore();
      }
    }
    if (paramUri.cacheResponse() != null) {}
    for (boolean bool = true;; bool = false)
    {
      paramUri = paramUri.body();
      return new j.a(paramUri.byteStream(), bool, paramUri.contentLength());
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/squareup/a/s.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */