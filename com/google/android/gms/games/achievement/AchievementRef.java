package com.google.android.gms.games.achievement;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.common.internal.zzb;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerRef;

public final class AchievementRef
  extends zzc
  implements Achievement
{
  AchievementRef(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
  }
  
  public String b()
  {
    return e("external_achievement_id");
  }
  
  public int c()
  {
    return c("type");
  }
  
  public String d()
  {
    return e("name");
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String e()
  {
    return e("description");
  }
  
  public Uri f()
  {
    return h("unlocked_icon_image_uri");
  }
  
  public Uri g()
  {
    return h("revealed_icon_image_uri");
  }
  
  public String getRevealedImageUrl()
  {
    return e("revealed_icon_image_url");
  }
  
  public String getUnlockedImageUrl()
  {
    return e("unlocked_icon_image_url");
  }
  
  public int h()
  {
    boolean bool = true;
    if (c() == 1) {}
    for (;;)
    {
      zzb.a(bool);
      return c("total_steps");
      bool = false;
    }
  }
  
  public String i()
  {
    boolean bool = true;
    if (c() == 1) {}
    for (;;)
    {
      zzb.a(bool);
      return e("formatted_total_steps");
      bool = false;
    }
  }
  
  public Player j()
  {
    return new PlayerRef(this.a, this.b);
  }
  
  public int k()
  {
    return c("state");
  }
  
  public int l()
  {
    boolean bool = true;
    if (c() == 1) {}
    for (;;)
    {
      zzb.a(bool);
      return c("current_steps");
      bool = false;
    }
  }
  
  public String m()
  {
    boolean bool = true;
    if (c() == 1) {}
    for (;;)
    {
      zzb.a(bool);
      return e("formatted_current_steps");
      bool = false;
    }
  }
  
  public long n()
  {
    return b("last_updated_timestamp");
  }
  
  public long o()
  {
    if ((!a_("instance_xp_value")) || (i("instance_xp_value"))) {
      return b("definition_xp_value");
    }
    return b("instance_xp_value");
  }
  
  public Achievement p()
  {
    return new AchievementEntity(this);
  }
  
  public String toString()
  {
    return AchievementEntity.b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((AchievementEntity)p()).writeToParcel(paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/achievement/AchievementRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */