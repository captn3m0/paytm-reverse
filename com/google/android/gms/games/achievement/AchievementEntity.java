package com.google.android.gms.games.achievement;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzb;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;

public final class AchievementEntity
  implements SafeParcelable, Achievement
{
  public static final Parcelable.Creator<AchievementEntity> CREATOR = new AchievementEntityCreator();
  private final int a;
  private final String b;
  private final int c;
  private final String d;
  private final String e;
  private final Uri f;
  private final String g;
  private final Uri h;
  private final String i;
  private final int j;
  private final String k;
  private final PlayerEntity l;
  private final int m;
  private final int n;
  private final String o;
  private final long p;
  private final long q;
  
  AchievementEntity(int paramInt1, String paramString1, int paramInt2, String paramString2, String paramString3, Uri paramUri1, String paramString4, Uri paramUri2, String paramString5, int paramInt3, String paramString6, PlayerEntity paramPlayerEntity, int paramInt4, int paramInt5, String paramString7, long paramLong1, long paramLong2)
  {
    this.a = paramInt1;
    this.b = paramString1;
    this.c = paramInt2;
    this.d = paramString2;
    this.e = paramString3;
    this.f = paramUri1;
    this.g = paramString4;
    this.h = paramUri2;
    this.i = paramString5;
    this.j = paramInt3;
    this.k = paramString6;
    this.l = paramPlayerEntity;
    this.m = paramInt4;
    this.n = paramInt5;
    this.o = paramString7;
    this.p = paramLong1;
    this.q = paramLong2;
  }
  
  public AchievementEntity(Achievement paramAchievement)
  {
    this.a = 1;
    this.b = paramAchievement.b();
    this.c = paramAchievement.c();
    this.d = paramAchievement.d();
    this.e = paramAchievement.e();
    this.f = paramAchievement.f();
    this.g = paramAchievement.getUnlockedImageUrl();
    this.h = paramAchievement.g();
    this.i = paramAchievement.getRevealedImageUrl();
    this.l = ((PlayerEntity)paramAchievement.j().a());
    this.m = paramAchievement.k();
    this.p = paramAchievement.n();
    this.q = paramAchievement.o();
    if (paramAchievement.c() == 1)
    {
      this.j = paramAchievement.h();
      this.k = paramAchievement.i();
      this.n = paramAchievement.l();
    }
    for (this.o = paramAchievement.m();; this.o = null)
    {
      zzb.a(this.b);
      zzb.a(this.e);
      return;
      this.j = 0;
      this.k = null;
      this.n = 0;
    }
  }
  
  static int a(Achievement paramAchievement)
  {
    int i2;
    int i1;
    if (paramAchievement.c() == 1)
    {
      i2 = paramAchievement.l();
      i1 = paramAchievement.h();
    }
    for (;;)
    {
      return zzw.a(new Object[] { paramAchievement.b(), paramAchievement.d(), Integer.valueOf(paramAchievement.c()), paramAchievement.e(), Long.valueOf(paramAchievement.o()), Integer.valueOf(paramAchievement.k()), Long.valueOf(paramAchievement.n()), paramAchievement.j(), Integer.valueOf(i2), Integer.valueOf(i1) });
      i1 = 0;
      i2 = 0;
    }
  }
  
  static boolean a(Achievement paramAchievement, Object paramObject)
  {
    boolean bool3 = true;
    boolean bool2;
    if (!(paramObject instanceof Achievement)) {
      bool2 = false;
    }
    do
    {
      return bool2;
      bool2 = bool3;
    } while (paramAchievement == paramObject);
    paramObject = (Achievement)paramObject;
    boolean bool1;
    if (paramAchievement.c() == 1)
    {
      bool2 = zzw.a(Integer.valueOf(((Achievement)paramObject).l()), Integer.valueOf(paramAchievement.l()));
      bool1 = zzw.a(Integer.valueOf(((Achievement)paramObject).h()), Integer.valueOf(paramAchievement.h()));
    }
    for (;;)
    {
      if ((zzw.a(((Achievement)paramObject).b(), paramAchievement.b())) && (zzw.a(((Achievement)paramObject).d(), paramAchievement.d())) && (zzw.a(Integer.valueOf(((Achievement)paramObject).c()), Integer.valueOf(paramAchievement.c()))) && (zzw.a(((Achievement)paramObject).e(), paramAchievement.e())) && (zzw.a(Long.valueOf(((Achievement)paramObject).o()), Long.valueOf(paramAchievement.o()))) && (zzw.a(Integer.valueOf(((Achievement)paramObject).k()), Integer.valueOf(paramAchievement.k()))) && (zzw.a(Long.valueOf(((Achievement)paramObject).n()), Long.valueOf(paramAchievement.n()))) && (zzw.a(((Achievement)paramObject).j(), paramAchievement.j())) && (bool2))
      {
        bool2 = bool3;
        if (bool1) {
          break;
        }
      }
      return false;
      bool1 = true;
      bool2 = true;
    }
  }
  
  static String b(Achievement paramAchievement)
  {
    zzw.zza localzza = zzw.a(paramAchievement).a("Id", paramAchievement.b()).a("Type", Integer.valueOf(paramAchievement.c())).a("Name", paramAchievement.d()).a("Description", paramAchievement.e()).a("Player", paramAchievement.j()).a("State", Integer.valueOf(paramAchievement.k()));
    if (paramAchievement.c() == 1)
    {
      localzza.a("CurrentSteps", Integer.valueOf(paramAchievement.l()));
      localzza.a("TotalSteps", Integer.valueOf(paramAchievement.h()));
    }
    return localzza.toString();
  }
  
  public String b()
  {
    return this.b;
  }
  
  public int c()
  {
    return this.c;
  }
  
  public String d()
  {
    return this.d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String e()
  {
    return this.e;
  }
  
  public boolean equals(Object paramObject)
  {
    return a(this, paramObject);
  }
  
  public Uri f()
  {
    return this.f;
  }
  
  public Uri g()
  {
    return this.h;
  }
  
  public String getRevealedImageUrl()
  {
    return this.i;
  }
  
  public String getUnlockedImageUrl()
  {
    return this.g;
  }
  
  public int h()
  {
    boolean bool = true;
    if (c() == 1) {}
    for (;;)
    {
      zzb.a(bool);
      return q();
      bool = false;
    }
  }
  
  public int hashCode()
  {
    return a(this);
  }
  
  public String i()
  {
    boolean bool = true;
    if (c() == 1) {}
    for (;;)
    {
      zzb.a(bool);
      return r();
      bool = false;
    }
  }
  
  public Player j()
  {
    return this.l;
  }
  
  public int k()
  {
    return this.m;
  }
  
  public int l()
  {
    boolean bool = true;
    if (c() == 1) {}
    for (;;)
    {
      zzb.a(bool);
      return s();
      bool = false;
    }
  }
  
  public String m()
  {
    boolean bool = true;
    if (c() == 1) {}
    for (;;)
    {
      zzb.a(bool);
      return t();
      bool = false;
    }
  }
  
  public long n()
  {
    return this.p;
  }
  
  public long o()
  {
    return this.q;
  }
  
  public int p()
  {
    return this.a;
  }
  
  public int q()
  {
    return this.j;
  }
  
  public String r()
  {
    return this.k;
  }
  
  public int s()
  {
    return this.n;
  }
  
  public String t()
  {
    return this.o;
  }
  
  public String toString()
  {
    return b(this);
  }
  
  public Achievement u()
  {
    return this;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    AchievementEntityCreator.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/achievement/AchievementEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */