package com.google.android.gms.games.achievement;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.games.PlayerEntity;

public class AchievementEntityCreator
  implements Parcelable.Creator<AchievementEntity>
{
  static void a(AchievementEntity paramAchievementEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramAchievementEntity.b(), false);
    zzb.a(paramParcel, 2, paramAchievementEntity.c());
    zzb.a(paramParcel, 3, paramAchievementEntity.d(), false);
    zzb.a(paramParcel, 4, paramAchievementEntity.e(), false);
    zzb.a(paramParcel, 5, paramAchievementEntity.f(), paramInt, false);
    zzb.a(paramParcel, 6, paramAchievementEntity.getUnlockedImageUrl(), false);
    zzb.a(paramParcel, 7, paramAchievementEntity.g(), paramInt, false);
    zzb.a(paramParcel, 8, paramAchievementEntity.getRevealedImageUrl(), false);
    zzb.a(paramParcel, 9, paramAchievementEntity.q());
    zzb.a(paramParcel, 10, paramAchievementEntity.r(), false);
    zzb.a(paramParcel, 11, paramAchievementEntity.j(), paramInt, false);
    zzb.a(paramParcel, 12, paramAchievementEntity.k());
    zzb.a(paramParcel, 13, paramAchievementEntity.s());
    zzb.a(paramParcel, 14, paramAchievementEntity.t(), false);
    zzb.a(paramParcel, 15, paramAchievementEntity.n());
    zzb.a(paramParcel, 16, paramAchievementEntity.o());
    zzb.a(paramParcel, 1000, paramAchievementEntity.p());
    zzb.a(paramParcel, i);
  }
  
  public AchievementEntity a(Parcel paramParcel)
  {
    int i1 = zza.b(paramParcel);
    int n = 0;
    String str7 = null;
    int m = 0;
    String str6 = null;
    String str5 = null;
    Uri localUri2 = null;
    String str4 = null;
    Uri localUri1 = null;
    String str3 = null;
    int k = 0;
    String str2 = null;
    PlayerEntity localPlayerEntity = null;
    int j = 0;
    int i = 0;
    String str1 = null;
    long l2 = 0L;
    long l1 = 0L;
    while (paramParcel.dataPosition() < i1)
    {
      int i2 = zza.a(paramParcel);
      switch (zza.a(i2))
      {
      default: 
        zza.b(paramParcel, i2);
        break;
      case 1: 
        str7 = zza.p(paramParcel, i2);
        break;
      case 2: 
        m = zza.g(paramParcel, i2);
        break;
      case 3: 
        str6 = zza.p(paramParcel, i2);
        break;
      case 4: 
        str5 = zza.p(paramParcel, i2);
        break;
      case 5: 
        localUri2 = (Uri)zza.a(paramParcel, i2, Uri.CREATOR);
        break;
      case 6: 
        str4 = zza.p(paramParcel, i2);
        break;
      case 7: 
        localUri1 = (Uri)zza.a(paramParcel, i2, Uri.CREATOR);
        break;
      case 8: 
        str3 = zza.p(paramParcel, i2);
        break;
      case 9: 
        k = zza.g(paramParcel, i2);
        break;
      case 10: 
        str2 = zza.p(paramParcel, i2);
        break;
      case 11: 
        localPlayerEntity = (PlayerEntity)zza.a(paramParcel, i2, PlayerEntity.CREATOR);
        break;
      case 12: 
        j = zza.g(paramParcel, i2);
        break;
      case 13: 
        i = zza.g(paramParcel, i2);
        break;
      case 14: 
        str1 = zza.p(paramParcel, i2);
        break;
      case 15: 
        l2 = zza.i(paramParcel, i2);
        break;
      case 16: 
        l1 = zza.i(paramParcel, i2);
        break;
      case 1000: 
        n = zza.g(paramParcel, i2);
      }
    }
    if (paramParcel.dataPosition() != i1) {
      throw new zza.zza("Overread allowed size end=" + i1, paramParcel);
    }
    return new AchievementEntity(n, str7, m, str6, str5, localUri2, str4, localUri1, str3, k, str2, localPlayerEntity, j, i, str1, l2, l1);
  }
  
  public AchievementEntity[] a(int paramInt)
  {
    return new AchievementEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/achievement/AchievementEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */