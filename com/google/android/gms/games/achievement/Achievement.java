package com.google.android.gms.games.achievement;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.data.Freezable;
import com.google.android.gms.games.Player;

public abstract interface Achievement
  extends Parcelable, Freezable<Achievement>
{
  public abstract String b();
  
  public abstract int c();
  
  public abstract String d();
  
  public abstract String e();
  
  public abstract Uri f();
  
  public abstract Uri g();
  
  @Deprecated
  @KeepName
  public abstract String getRevealedImageUrl();
  
  @Deprecated
  @KeepName
  public abstract String getUnlockedImageUrl();
  
  public abstract int h();
  
  public abstract String i();
  
  public abstract Player j();
  
  public abstract int k();
  
  public abstract int l();
  
  public abstract String m();
  
  public abstract long n();
  
  public abstract long o();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/achievement/Achievement.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */