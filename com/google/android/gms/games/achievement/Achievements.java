package com.google.android.gms.games.achievement;

import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;

public abstract interface Achievements
{
  public static abstract interface LoadAchievementsResult
    extends Releasable, Result
  {}
  
  public static abstract interface UpdateAchievementResult
    extends Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/achievement/Achievements.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */