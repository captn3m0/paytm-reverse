package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;

public final class GameRef
  extends zzc
  implements Game
{
  public GameRef(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
  }
  
  public String b()
  {
    return e("external_game_id");
  }
  
  public String c()
  {
    return e("display_name");
  }
  
  public String d()
  {
    return e("primary_category");
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String e()
  {
    return e("secondary_category");
  }
  
  public boolean equals(Object paramObject)
  {
    return GameEntity.a(this, paramObject);
  }
  
  public String f()
  {
    return e("game_description");
  }
  
  public String g()
  {
    return e("developer_name");
  }
  
  public String getFeaturedImageUrl()
  {
    return e("featured_image_url");
  }
  
  public String getHiResImageUrl()
  {
    return e("game_hi_res_image_url");
  }
  
  public String getIconImageUrl()
  {
    return e("game_icon_image_url");
  }
  
  public Uri h()
  {
    return h("game_icon_image_uri");
  }
  
  public int hashCode()
  {
    return GameEntity.a(this);
  }
  
  public Uri i()
  {
    return h("game_hi_res_image_uri");
  }
  
  public Uri j()
  {
    return h("featured_image_uri");
  }
  
  public boolean k()
  {
    return d("play_enabled_game");
  }
  
  public boolean l()
  {
    return d("muted");
  }
  
  public boolean m()
  {
    return d("identity_sharing_confirmed");
  }
  
  public boolean n()
  {
    return c("installed") > 0;
  }
  
  public String o()
  {
    return e("package_name");
  }
  
  public int p()
  {
    return c("gameplay_acl_status");
  }
  
  public int q()
  {
    return c("achievement_total_count");
  }
  
  public int r()
  {
    return c("leaderboard_count");
  }
  
  public boolean s()
  {
    return c("real_time_support") > 0;
  }
  
  public boolean t()
  {
    return c("turn_based_support") > 0;
  }
  
  public String toString()
  {
    return GameEntity.b(this);
  }
  
  public boolean u()
  {
    return c("snapshots_enabled") > 0;
  }
  
  public String v()
  {
    return e("theme_color");
  }
  
  public boolean w()
  {
    return c("gamepad_support") > 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((GameEntity)x()).writeToParcel(paramParcel, paramInt);
  }
  
  public Game x()
  {
    return new GameEntity(this);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/GameRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */