package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.games.internal.player.MostRecentGameInfoEntity;

public class PlayerEntityCreator
  implements Parcelable.Creator<PlayerEntity>
{
  static void a(PlayerEntity paramPlayerEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramPlayerEntity.b(), false);
    zzb.a(paramParcel, 2, paramPlayerEntity.c(), false);
    zzb.a(paramParcel, 3, paramPlayerEntity.g(), paramInt, false);
    zzb.a(paramParcel, 4, paramPlayerEntity.h(), paramInt, false);
    zzb.a(paramParcel, 5, paramPlayerEntity.i());
    zzb.a(paramParcel, 6, paramPlayerEntity.k());
    zzb.a(paramParcel, 7, paramPlayerEntity.j());
    zzb.a(paramParcel, 8, paramPlayerEntity.getIconImageUrl(), false);
    zzb.a(paramParcel, 9, paramPlayerEntity.getHiResImageUrl(), false);
    zzb.a(paramParcel, 14, paramPlayerEntity.m(), false);
    zzb.a(paramParcel, 15, paramPlayerEntity.o(), paramInt, false);
    zzb.a(paramParcel, 16, paramPlayerEntity.n(), paramInt, false);
    zzb.a(paramParcel, 1000, paramPlayerEntity.r());
    zzb.a(paramParcel, 19, paramPlayerEntity.f());
    zzb.a(paramParcel, 18, paramPlayerEntity.l());
    zzb.a(paramParcel, 21, paramPlayerEntity.e(), false);
    zzb.a(paramParcel, 20, paramPlayerEntity.d(), false);
    zzb.a(paramParcel, 23, paramPlayerEntity.getBannerImageLandscapeUrl(), false);
    zzb.a(paramParcel, 22, paramPlayerEntity.p(), paramInt, false);
    zzb.a(paramParcel, 25, paramPlayerEntity.getBannerImagePortraitUrl(), false);
    zzb.a(paramParcel, 24, paramPlayerEntity.q(), paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public PlayerEntity a(Parcel paramParcel)
  {
    int k = zza.b(paramParcel);
    int j = 0;
    String str9 = null;
    String str8 = null;
    Uri localUri4 = null;
    Uri localUri3 = null;
    long l2 = 0L;
    int i = 0;
    long l1 = 0L;
    String str7 = null;
    String str6 = null;
    String str5 = null;
    MostRecentGameInfoEntity localMostRecentGameInfoEntity = null;
    PlayerLevelInfo localPlayerLevelInfo = null;
    boolean bool2 = false;
    boolean bool1 = false;
    String str4 = null;
    String str3 = null;
    Uri localUri2 = null;
    String str2 = null;
    Uri localUri1 = null;
    String str1 = null;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        str9 = zza.p(paramParcel, m);
        break;
      case 2: 
        str8 = zza.p(paramParcel, m);
        break;
      case 3: 
        localUri4 = (Uri)zza.a(paramParcel, m, Uri.CREATOR);
        break;
      case 4: 
        localUri3 = (Uri)zza.a(paramParcel, m, Uri.CREATOR);
        break;
      case 5: 
        l2 = zza.i(paramParcel, m);
        break;
      case 6: 
        i = zza.g(paramParcel, m);
        break;
      case 7: 
        l1 = zza.i(paramParcel, m);
        break;
      case 8: 
        str7 = zza.p(paramParcel, m);
        break;
      case 9: 
        str6 = zza.p(paramParcel, m);
        break;
      case 14: 
        str5 = zza.p(paramParcel, m);
        break;
      case 15: 
        localMostRecentGameInfoEntity = (MostRecentGameInfoEntity)zza.a(paramParcel, m, MostRecentGameInfoEntity.CREATOR);
        break;
      case 16: 
        localPlayerLevelInfo = (PlayerLevelInfo)zza.a(paramParcel, m, PlayerLevelInfo.CREATOR);
        break;
      case 1000: 
        j = zza.g(paramParcel, m);
        break;
      case 19: 
        bool1 = zza.c(paramParcel, m);
        break;
      case 18: 
        bool2 = zza.c(paramParcel, m);
        break;
      case 21: 
        str3 = zza.p(paramParcel, m);
        break;
      case 20: 
        str4 = zza.p(paramParcel, m);
        break;
      case 23: 
        str2 = zza.p(paramParcel, m);
        break;
      case 22: 
        localUri2 = (Uri)zza.a(paramParcel, m, Uri.CREATOR);
        break;
      case 25: 
        str1 = zza.p(paramParcel, m);
        break;
      case 24: 
        localUri1 = (Uri)zza.a(paramParcel, m, Uri.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new PlayerEntity(j, str9, str8, localUri4, localUri3, l2, i, l1, str7, str6, str5, localMostRecentGameInfoEntity, localPlayerLevelInfo, bool2, bool1, str4, str3, localUri2, str2, localUri1, str1);
  }
  
  public PlayerEntity[] a(int paramInt)
  {
    return new PlayerEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/PlayerEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */