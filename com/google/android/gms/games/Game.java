package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.data.Freezable;

public abstract interface Game
  extends Parcelable, Freezable<Game>
{
  public abstract String b();
  
  public abstract String c();
  
  public abstract String d();
  
  public abstract String e();
  
  public abstract String f();
  
  public abstract String g();
  
  @Deprecated
  @KeepName
  public abstract String getFeaturedImageUrl();
  
  @Deprecated
  @KeepName
  public abstract String getHiResImageUrl();
  
  @Deprecated
  @KeepName
  public abstract String getIconImageUrl();
  
  public abstract Uri h();
  
  public abstract Uri i();
  
  public abstract Uri j();
  
  public abstract boolean k();
  
  public abstract boolean l();
  
  public abstract boolean m();
  
  public abstract boolean n();
  
  public abstract String o();
  
  public abstract int p();
  
  public abstract int q();
  
  public abstract int r();
  
  public abstract boolean s();
  
  public abstract boolean t();
  
  public abstract boolean u();
  
  public abstract String v();
  
  public abstract boolean w();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/Game.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */