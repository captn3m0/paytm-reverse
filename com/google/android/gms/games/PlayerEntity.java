package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.zzb;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import com.google.android.gms.games.internal.player.MostRecentGameInfo;
import com.google.android.gms.games.internal.player.MostRecentGameInfoEntity;

public final class PlayerEntity
  extends GamesDowngradeableSafeParcel
  implements Player
{
  public static final Parcelable.Creator<PlayerEntity> CREATOR = new PlayerEntityCreatorCompat();
  private final int a;
  private String b;
  private String c;
  private final Uri d;
  private final Uri e;
  private final long f;
  private final int g;
  private final long h;
  private final String i;
  private final String j;
  private final String k;
  private final MostRecentGameInfoEntity l;
  private final PlayerLevelInfo m;
  private final boolean n;
  private final boolean o;
  private final String p;
  private final String q;
  private final Uri r;
  private final String s;
  private final Uri t;
  private final String u;
  
  PlayerEntity(int paramInt1, String paramString1, String paramString2, Uri paramUri1, Uri paramUri2, long paramLong1, int paramInt2, long paramLong2, String paramString3, String paramString4, String paramString5, MostRecentGameInfoEntity paramMostRecentGameInfoEntity, PlayerLevelInfo paramPlayerLevelInfo, boolean paramBoolean1, boolean paramBoolean2, String paramString6, String paramString7, Uri paramUri3, String paramString8, Uri paramUri4, String paramString9)
  {
    this.a = paramInt1;
    this.b = paramString1;
    this.c = paramString2;
    this.d = paramUri1;
    this.i = paramString3;
    this.e = paramUri2;
    this.j = paramString4;
    this.f = paramLong1;
    this.g = paramInt2;
    this.h = paramLong2;
    this.k = paramString5;
    this.n = paramBoolean1;
    this.l = paramMostRecentGameInfoEntity;
    this.m = paramPlayerLevelInfo;
    this.o = paramBoolean2;
    this.p = paramString6;
    this.q = paramString7;
    this.r = paramUri3;
    this.s = paramString8;
    this.t = paramUri4;
    this.u = paramString9;
  }
  
  public PlayerEntity(Player paramPlayer)
  {
    this(paramPlayer, true);
  }
  
  public PlayerEntity(Player paramPlayer, boolean paramBoolean)
  {
    this.a = 13;
    Object localObject1;
    if (paramBoolean)
    {
      localObject1 = paramPlayer.b();
      this.b = ((String)localObject1);
      this.c = paramPlayer.c();
      this.d = paramPlayer.g();
      this.i = paramPlayer.getIconImageUrl();
      this.e = paramPlayer.h();
      this.j = paramPlayer.getHiResImageUrl();
      this.f = paramPlayer.i();
      this.g = paramPlayer.k();
      this.h = paramPlayer.j();
      this.k = paramPlayer.m();
      this.n = paramPlayer.l();
      localObject1 = paramPlayer.o();
      if (localObject1 != null) {
        break label267;
      }
      localObject1 = localObject2;
      label143:
      this.l = ((MostRecentGameInfoEntity)localObject1);
      this.m = paramPlayer.n();
      this.o = paramPlayer.f();
      this.p = paramPlayer.d();
      this.q = paramPlayer.e();
      this.r = paramPlayer.p();
      this.s = paramPlayer.getBannerImageLandscapeUrl();
      this.t = paramPlayer.q();
      this.u = paramPlayer.getBannerImagePortraitUrl();
      if (paramBoolean) {
        zzb.a(this.b);
      }
      zzb.a(this.c);
      if (this.f <= 0L) {
        break label279;
      }
    }
    label267:
    label279:
    for (paramBoolean = true;; paramBoolean = false)
    {
      zzb.a(paramBoolean);
      return;
      localObject1 = null;
      break;
      localObject1 = new MostRecentGameInfoEntity((MostRecentGameInfo)localObject1);
      break label143;
    }
  }
  
  static int a(Player paramPlayer)
  {
    return zzw.a(new Object[] { paramPlayer.b(), paramPlayer.c(), Boolean.valueOf(paramPlayer.f()), paramPlayer.g(), paramPlayer.h(), Long.valueOf(paramPlayer.i()), paramPlayer.m(), paramPlayer.n(), paramPlayer.d(), paramPlayer.e(), paramPlayer.p(), paramPlayer.q() });
  }
  
  static boolean a(Player paramPlayer, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof Player)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramPlayer == paramObject);
      paramObject = (Player)paramObject;
      if ((!zzw.a(((Player)paramObject).b(), paramPlayer.b())) || (!zzw.a(((Player)paramObject).c(), paramPlayer.c())) || (!zzw.a(Boolean.valueOf(((Player)paramObject).f()), Boolean.valueOf(paramPlayer.f()))) || (!zzw.a(((Player)paramObject).g(), paramPlayer.g())) || (!zzw.a(((Player)paramObject).h(), paramPlayer.h())) || (!zzw.a(Long.valueOf(((Player)paramObject).i()), Long.valueOf(paramPlayer.i()))) || (!zzw.a(((Player)paramObject).m(), paramPlayer.m())) || (!zzw.a(((Player)paramObject).n(), paramPlayer.n())) || (!zzw.a(((Player)paramObject).d(), paramPlayer.d())) || (!zzw.a(((Player)paramObject).e(), paramPlayer.e())) || (!zzw.a(((Player)paramObject).p(), paramPlayer.p()))) {
        break;
      }
      bool1 = bool2;
    } while (zzw.a(((Player)paramObject).q(), paramPlayer.q()));
    return false;
  }
  
  static String b(Player paramPlayer)
  {
    return zzw.a(paramPlayer).a("PlayerId", paramPlayer.b()).a("DisplayName", paramPlayer.c()).a("HasDebugAccess", Boolean.valueOf(paramPlayer.f())).a("IconImageUri", paramPlayer.g()).a("IconImageUrl", paramPlayer.getIconImageUrl()).a("HiResImageUri", paramPlayer.h()).a("HiResImageUrl", paramPlayer.getHiResImageUrl()).a("RetrievedTimestamp", Long.valueOf(paramPlayer.i())).a("Title", paramPlayer.m()).a("LevelInfo", paramPlayer.n()).a("GamerTag", paramPlayer.d()).a("Name", paramPlayer.e()).a("BannerImageLandscapeUri", paramPlayer.p()).a("BannerImageLandscapeUrl", paramPlayer.getBannerImageLandscapeUrl()).a("BannerImagePortraitUri", paramPlayer.q()).a("BannerImagePortraitUrl", paramPlayer.getBannerImagePortraitUrl()).toString();
  }
  
  public String b()
  {
    return this.b;
  }
  
  public String c()
  {
    return this.c;
  }
  
  public String d()
  {
    return this.p;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String e()
  {
    return this.q;
  }
  
  public boolean equals(Object paramObject)
  {
    return a(this, paramObject);
  }
  
  public boolean f()
  {
    return this.o;
  }
  
  public Uri g()
  {
    return this.d;
  }
  
  public String getBannerImageLandscapeUrl()
  {
    return this.s;
  }
  
  public String getBannerImagePortraitUrl()
  {
    return this.u;
  }
  
  public String getHiResImageUrl()
  {
    return this.j;
  }
  
  public String getIconImageUrl()
  {
    return this.i;
  }
  
  public Uri h()
  {
    return this.e;
  }
  
  public int hashCode()
  {
    return a(this);
  }
  
  public long i()
  {
    return this.f;
  }
  
  public long j()
  {
    return this.h;
  }
  
  public int k()
  {
    return this.g;
  }
  
  public boolean l()
  {
    return this.n;
  }
  
  public String m()
  {
    return this.k;
  }
  
  public PlayerLevelInfo n()
  {
    return this.m;
  }
  
  public MostRecentGameInfo o()
  {
    return this.l;
  }
  
  public Uri p()
  {
    return this.r;
  }
  
  public Uri q()
  {
    return this.t;
  }
  
  public int r()
  {
    return this.a;
  }
  
  public Player s()
  {
    return this;
  }
  
  public String toString()
  {
    return b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    Object localObject2 = null;
    if (!k_())
    {
      PlayerEntityCreator.a(this, paramParcel, paramInt);
      return;
    }
    paramParcel.writeString(this.b);
    paramParcel.writeString(this.c);
    if (this.d == null)
    {
      localObject1 = null;
      paramParcel.writeString((String)localObject1);
      if (this.e != null) {
        break label82;
      }
    }
    label82:
    for (Object localObject1 = localObject2;; localObject1 = this.e.toString())
    {
      paramParcel.writeString((String)localObject1);
      paramParcel.writeLong(this.f);
      return;
      localObject1 = this.d.toString();
      break;
    }
  }
  
  static final class PlayerEntityCreatorCompat
    extends PlayerEntityCreator
  {
    public PlayerEntity a(Parcel paramParcel)
    {
      if ((PlayerEntity.a(PlayerEntity.t())) || (PlayerEntity.b(PlayerEntity.class.getCanonicalName()))) {
        return super.a(paramParcel);
      }
      String str1 = paramParcel.readString();
      String str2 = paramParcel.readString();
      Object localObject1 = paramParcel.readString();
      Object localObject2 = paramParcel.readString();
      if (localObject1 == null)
      {
        localObject1 = null;
        if (localObject2 != null) {
          break label111;
        }
      }
      label111:
      for (localObject2 = null;; localObject2 = Uri.parse((String)localObject2))
      {
        return new PlayerEntity(13, str1, str2, (Uri)localObject1, (Uri)localObject2, paramParcel.readLong(), -1, -1L, null, null, null, null, null, true, false, paramParcel.readString(), paramParcel.readString(), null, null, null, null);
        localObject1 = Uri.parse((String)localObject1);
        break;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/PlayerEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */