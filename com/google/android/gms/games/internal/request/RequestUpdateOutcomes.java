package com.google.android.gms.games.internal.request;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.internal.constants.RequestUpdateResultOutcome;
import java.util.HashMap;

public final class RequestUpdateOutcomes
{
  private static final String[] a = { "requestId", "outcome" };
  private final int b;
  private final HashMap<String, Integer> c;
  
  private RequestUpdateOutcomes(int paramInt, HashMap<String, Integer> paramHashMap)
  {
    this.b = paramInt;
    this.c = paramHashMap;
  }
  
  public static RequestUpdateOutcomes a(DataHolder paramDataHolder)
  {
    Builder localBuilder = new Builder();
    localBuilder.a(paramDataHolder.e());
    int j = paramDataHolder.g();
    int i = 0;
    while (i < j)
    {
      int k = paramDataHolder.a(i);
      localBuilder.a(paramDataHolder.c("requestId", i, k), paramDataHolder.b("outcome", i, k));
      i += 1;
    }
    return localBuilder.a();
  }
  
  public static final class Builder
  {
    private HashMap<String, Integer> a = new HashMap();
    private int b = 0;
    
    public Builder a(int paramInt)
    {
      this.b = paramInt;
      return this;
    }
    
    public Builder a(String paramString, int paramInt)
    {
      if (RequestUpdateResultOutcome.a(paramInt)) {
        this.a.put(paramString, Integer.valueOf(paramInt));
      }
      return this;
    }
    
    public RequestUpdateOutcomes a()
    {
      return new RequestUpdateOutcomes(this.b, this.a, null);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/internal/request/RequestUpdateOutcomes.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */