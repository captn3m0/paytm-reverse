package com.google.android.gms.games.internal.events;

import android.os.Handler;
import android.os.Looper;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class EventIncrementCache
{
  private Handler a;
  final Object b = new Object();
  private boolean c;
  private HashMap<String, AtomicInteger> d;
  private int e;
  
  public EventIncrementCache(Looper paramLooper, int paramInt)
  {
    this.a = new Handler(paramLooper);
    this.d = new HashMap();
    this.e = paramInt;
  }
  
  private void b()
  {
    synchronized (this.b)
    {
      this.c = false;
      a();
      return;
    }
  }
  
  public void a()
  {
    synchronized (this.b)
    {
      Iterator localIterator = this.d.entrySet().iterator();
      if (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        a((String)localEntry.getKey(), ((AtomicInteger)localEntry.getValue()).get());
      }
    }
    this.d.clear();
  }
  
  protected abstract void a(String paramString, int paramInt);
  
  public void b(String paramString, int paramInt)
  {
    synchronized (this.b)
    {
      if (!this.c)
      {
        this.c = true;
        this.a.postDelayed(new Runnable()
        {
          public void run()
          {
            EventIncrementCache.a(EventIncrementCache.this);
          }
        }, this.e);
      }
      AtomicInteger localAtomicInteger2 = (AtomicInteger)this.d.get(paramString);
      AtomicInteger localAtomicInteger1 = localAtomicInteger2;
      if (localAtomicInteger2 == null)
      {
        localAtomicInteger1 = new AtomicInteger();
        this.d.put(paramString, localAtomicInteger1);
      }
      localAtomicInteger1.addAndGet(paramInt);
      return;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/internal/events/EventIncrementCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */