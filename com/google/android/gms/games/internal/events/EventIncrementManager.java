package com.google.android.gms.games.internal.events;

import java.util.concurrent.atomic.AtomicReference;

public abstract class EventIncrementManager
{
  private final AtomicReference<EventIncrementCache> a = new AtomicReference();
  
  protected abstract EventIncrementCache a();
  
  public void a(String paramString, int paramInt)
  {
    EventIncrementCache localEventIncrementCache2 = (EventIncrementCache)this.a.get();
    EventIncrementCache localEventIncrementCache1 = localEventIncrementCache2;
    if (localEventIncrementCache2 == null)
    {
      localEventIncrementCache2 = a();
      localEventIncrementCache1 = localEventIncrementCache2;
      if (!this.a.compareAndSet(null, localEventIncrementCache2)) {
        localEventIncrementCache1 = (EventIncrementCache)this.a.get();
      }
    }
    localEventIncrementCache1.b(paramString, paramInt);
  }
  
  public void b()
  {
    EventIncrementCache localEventIncrementCache = (EventIncrementCache)this.a.get();
    if (localEventIncrementCache != null) {
      localEventIncrementCache.a();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/internal/events/EventIncrementManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */