package com.google.android.gms.games.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Display;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import com.google.android.gms.internal.zzne;
import java.lang.ref.WeakReference;

public class PopupManager
{
  protected GamesClientImpl a;
  protected PopupLocationInfo b;
  
  private PopupManager(GamesClientImpl paramGamesClientImpl, int paramInt)
  {
    this.a = paramGamesClientImpl;
    a(paramInt);
  }
  
  public static PopupManager a(GamesClientImpl paramGamesClientImpl, int paramInt)
  {
    if (zzne.b()) {
      return new PopupManagerHCMR1(paramGamesClientImpl, paramInt);
    }
    return new PopupManager(paramGamesClientImpl, paramInt);
  }
  
  public void a()
  {
    this.a.a(this.b.a, this.b.a());
  }
  
  protected void a(int paramInt)
  {
    this.b = new PopupLocationInfo(paramInt, new Binder(), null);
  }
  
  public void a(View paramView) {}
  
  public Bundle b()
  {
    return this.b.a();
  }
  
  public IBinder c()
  {
    return this.b.a;
  }
  
  public PopupLocationInfo d()
  {
    return this.b;
  }
  
  public static final class PopupLocationInfo
  {
    public IBinder a;
    public int b;
    public int c = -1;
    public int d = 0;
    public int e = 0;
    public int f = 0;
    public int g = 0;
    
    private PopupLocationInfo(int paramInt, IBinder paramIBinder)
    {
      this.b = paramInt;
      this.a = paramIBinder;
    }
    
    public Bundle a()
    {
      Bundle localBundle = new Bundle();
      localBundle.putInt("popupLocationInfo.gravity", this.b);
      localBundle.putInt("popupLocationInfo.displayId", this.c);
      localBundle.putInt("popupLocationInfo.left", this.d);
      localBundle.putInt("popupLocationInfo.top", this.e);
      localBundle.putInt("popupLocationInfo.right", this.f);
      localBundle.putInt("popupLocationInfo.bottom", this.g);
      return localBundle;
    }
  }
  
  @TargetApi(12)
  private static final class PopupManagerHCMR1
    extends PopupManager
    implements View.OnAttachStateChangeListener, ViewTreeObserver.OnGlobalLayoutListener
  {
    private WeakReference<View> c;
    private boolean d = false;
    
    protected PopupManagerHCMR1(GamesClientImpl paramGamesClientImpl, int paramInt)
    {
      super(paramInt, null);
    }
    
    @TargetApi(17)
    private void b(View paramView)
    {
      int j = -1;
      int i = j;
      if (zzne.f())
      {
        localObject = paramView.getDisplay();
        i = j;
        if (localObject != null) {
          i = ((Display)localObject).getDisplayId();
        }
      }
      Object localObject = paramView.getWindowToken();
      int[] arrayOfInt = new int[2];
      paramView.getLocationInWindow(arrayOfInt);
      j = paramView.getWidth();
      int k = paramView.getHeight();
      this.b.c = i;
      this.b.a = ((IBinder)localObject);
      this.b.d = arrayOfInt[0];
      this.b.e = arrayOfInt[1];
      this.b.f = (arrayOfInt[0] + j);
      this.b.g = (arrayOfInt[1] + k);
      if (this.d)
      {
        a();
        this.d = false;
      }
    }
    
    public void a()
    {
      if (this.b.a != null)
      {
        super.a();
        return;
      }
      if (this.c != null) {}
      for (boolean bool = true;; bool = false)
      {
        this.d = bool;
        return;
      }
    }
    
    protected void a(int paramInt)
    {
      this.b = new PopupManager.PopupLocationInfo(paramInt, null, null);
    }
    
    @TargetApi(16)
    public void a(View paramView)
    {
      this.a.h();
      Object localObject2;
      Object localObject1;
      if (this.c != null)
      {
        localObject2 = (View)this.c.get();
        Context localContext = this.a.q();
        localObject1 = localObject2;
        if (localObject2 == null)
        {
          localObject1 = localObject2;
          if ((localContext instanceof Activity)) {
            localObject1 = ((Activity)localContext).getWindow().getDecorView();
          }
        }
        if (localObject1 != null)
        {
          ((View)localObject1).removeOnAttachStateChangeListener(this);
          localObject1 = ((View)localObject1).getViewTreeObserver();
          if (!zzne.e()) {
            break label186;
          }
          ((ViewTreeObserver)localObject1).removeOnGlobalLayoutListener(this);
        }
      }
      for (;;)
      {
        this.c = null;
        localObject2 = this.a.q();
        localObject1 = paramView;
        if (paramView == null)
        {
          localObject1 = paramView;
          if ((localObject2 instanceof Activity))
          {
            localObject1 = ((Activity)localObject2).findViewById(16908290);
            paramView = (View)localObject1;
            if (localObject1 == null) {
              paramView = ((Activity)localObject2).getWindow().getDecorView();
            }
            GamesLog.a("PopupManager", "You have not specified a View to use as content view for popups. Falling back to the Activity content view. Note that this may not work as expected in multi-screen environments");
            localObject1 = paramView;
          }
        }
        if (localObject1 == null) {
          break;
        }
        b((View)localObject1);
        this.c = new WeakReference(localObject1);
        ((View)localObject1).addOnAttachStateChangeListener(this);
        ((View)localObject1).getViewTreeObserver().addOnGlobalLayoutListener(this);
        return;
        label186:
        ((ViewTreeObserver)localObject1).removeGlobalOnLayoutListener(this);
      }
      GamesLog.b("PopupManager", "No content view usable to display popups. Popups will not be displayed in response to this client's calls. Use setViewForPopups() to set your content view.");
    }
    
    public void onGlobalLayout()
    {
      if (this.c == null) {}
      View localView;
      do
      {
        return;
        localView = (View)this.c.get();
      } while (localView == null);
      b(localView);
    }
    
    public void onViewAttachedToWindow(View paramView)
    {
      b(paramView);
    }
    
    public void onViewDetachedFromWindow(View paramView)
    {
      this.a.h();
      paramView.removeOnAttachStateChangeListener(this);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/internal/PopupManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */