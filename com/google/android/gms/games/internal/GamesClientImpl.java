package com.google.android.gms.games.internal;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.view.View;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.GoogleApiClient.zza;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zzb;
import com.google.android.gms.common.api.internal.zze;
import com.google.android.gms.common.api.internal.zzq;
import com.google.android.gms.common.api.internal.zzq.zzb;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.BinderWrapper;
import com.google.android.gms.common.internal.zzb;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.games.GameBuffer;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.Games.BaseGamesApiMethodImpl;
import com.google.android.gms.games.Games.GamesOptions;
import com.google.android.gms.games.Games.GetServerAuthCodeResult;
import com.google.android.gms.games.Games.GetTokenResult;
import com.google.android.gms.games.Games.LoadExperimentsResult;
import com.google.android.gms.games.GamesMetadata.LoadGameInstancesResult;
import com.google.android.gms.games.GamesMetadata.LoadGameSearchSuggestionsResult;
import com.google.android.gms.games.GamesMetadata.LoadGamesResult;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.Notifications.ContactSettingLoadResult;
import com.google.android.gms.games.Notifications.GameMuteStatusChangeResult;
import com.google.android.gms.games.Notifications.GameMuteStatusLoadResult;
import com.google.android.gms.games.Notifications.InboxCountResult;
import com.google.android.gms.games.OnNearbyPlayerDetectedListener;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerBuffer;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.Players.LoadPlayersResult;
import com.google.android.gms.games.Players.LoadProfileSettingsResult;
import com.google.android.gms.games.Players.LoadXpForGameCategoriesResult;
import com.google.android.gms.games.Players.LoadXpStreamResult;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements.LoadAchievementsResult;
import com.google.android.gms.games.achievement.Achievements.UpdateAchievementResult;
import com.google.android.gms.games.appcontent.AppContents.LoadAppContentResult;
import com.google.android.gms.games.event.EventBuffer;
import com.google.android.gms.games.event.Events.LoadEventsResult;
import com.google.android.gms.games.internal.events.EventIncrementCache;
import com.google.android.gms.games.internal.events.EventIncrementManager;
import com.google.android.gms.games.internal.experience.ExperienceEventBuffer;
import com.google.android.gms.games.internal.game.Acls.LoadAclResult;
import com.google.android.gms.games.internal.game.GameInstanceBuffer;
import com.google.android.gms.games.internal.game.GameSearchSuggestionBuffer;
import com.google.android.gms.games.internal.request.RequestUpdateOutcomes;
import com.google.android.gms.games.leaderboard.LeaderboardBuffer;
import com.google.android.gms.games.leaderboard.LeaderboardEntity;
import com.google.android.gms.games.leaderboard.LeaderboardScoreBuffer;
import com.google.android.gms.games.leaderboard.LeaderboardScoreBufferHeader;
import com.google.android.gms.games.leaderboard.LeaderboardScoreEntity;
import com.google.android.gms.games.leaderboard.Leaderboards.LeaderboardMetadataResult;
import com.google.android.gms.games.leaderboard.Leaderboards.LoadPlayerScoreResult;
import com.google.android.gms.games.leaderboard.Leaderboards.LoadScoresResult;
import com.google.android.gms.games.leaderboard.Leaderboards.SubmitScoreResult;
import com.google.android.gms.games.leaderboard.ScoreSubmissionData;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.InvitationBuffer;
import com.google.android.gms.games.multiplayer.Invitations.LoadInvitationsResult;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.games.multiplayer.ParticipantResult;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessageReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMultiplayer.ReliableMessageSentCallback;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomBuffer;
import com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateListener;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateListener;
import com.google.android.gms.games.multiplayer.turnbased.LoadMatchesResponse;
import com.google.android.gms.games.multiplayer.turnbased.OnTurnBasedMatchUpdateReceivedListener;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchBuffer;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchConfig;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.CancelMatchResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.InitiateMatchResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.LeaveMatchResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.LoadMatchResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.LoadMatchesResult;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer.UpdateMatchResult;
import com.google.android.gms.games.quest.Milestone;
import com.google.android.gms.games.quest.Quest;
import com.google.android.gms.games.quest.QuestBuffer;
import com.google.android.gms.games.quest.QuestUpdateListener;
import com.google.android.gms.games.quest.Quests.AcceptQuestResult;
import com.google.android.gms.games.quest.Quests.ClaimMilestoneResult;
import com.google.android.gms.games.quest.Quests.LoadQuestsResult;
import com.google.android.gms.games.request.GameRequest;
import com.google.android.gms.games.request.GameRequestBuffer;
import com.google.android.gms.games.request.OnRequestReceivedListener;
import com.google.android.gms.games.request.Requests.LoadRequestSummariesResult;
import com.google.android.gms.games.request.Requests.LoadRequestsResult;
import com.google.android.gms.games.request.Requests.SendRequestResult;
import com.google.android.gms.games.request.Requests.UpdateRequestsResult;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotContents;
import com.google.android.gms.games.snapshot.SnapshotContentsEntity;
import com.google.android.gms.games.snapshot.SnapshotEntity;
import com.google.android.gms.games.snapshot.SnapshotMetadata;
import com.google.android.gms.games.snapshot.SnapshotMetadataBuffer;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;
import com.google.android.gms.games.snapshot.SnapshotMetadataChangeEntity;
import com.google.android.gms.games.snapshot.SnapshotMetadataEntity;
import com.google.android.gms.games.snapshot.Snapshots.CommitSnapshotResult;
import com.google.android.gms.games.snapshot.Snapshots.DeleteSnapshotResult;
import com.google.android.gms.games.snapshot.Snapshots.LoadSnapshotsResult;
import com.google.android.gms.games.snapshot.Snapshots.OpenSnapshotResult;
import com.google.android.gms.games.stats.PlayerStats;
import com.google.android.gms.games.stats.Stats.LoadPlayerStatsResult;
import com.google.android.gms.games.video.VideoBuffer;
import com.google.android.gms.games.video.VideoCapabilities;
import com.google.android.gms.games.video.VideoConfiguration;
import com.google.android.gms.games.video.Videos.ListVideosResult;
import com.google.android.gms.games.video.Videos.VideoAvailableResult;
import com.google.android.gms.games.video.Videos.VideoCapabilitiesResult;
import com.google.android.gms.signin.internal.zzh;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public final class GamesClientImpl
  extends zzj<IGamesService>
{
  EventIncrementManager a = new EventIncrementManager()
  {
    public EventIncrementCache a()
    {
      return new GamesClientImpl.GameClientEventIncrementCache(GamesClientImpl.this);
    }
  };
  private final String e;
  private PlayerEntity f;
  private GameEntity g;
  private final PopupManager h;
  private boolean i = false;
  private final Binder j;
  private final long k;
  private final Games.GamesOptions l;
  
  public GamesClientImpl(Context paramContext, Looper paramLooper, com.google.android.gms.common.internal.zzf paramzzf, Games.GamesOptions paramGamesOptions, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramContext, paramLooper, 1, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    this.e = paramzzf.h();
    this.j = new Binder();
    this.h = PopupManager.a(this, paramzzf.d());
    a(paramzzf.j());
    this.k = hashCode();
    this.l = paramGamesOptions;
  }
  
  private void a(RemoteException paramRemoteException)
  {
    GamesLog.a("GamesClientImpl", "service died", paramRemoteException);
  }
  
  private static Room b(DataHolder paramDataHolder)
  {
    RoomBuffer localRoomBuffer = new RoomBuffer(paramDataHolder);
    paramDataHolder = null;
    try
    {
      if (localRoomBuffer.a() > 0) {
        paramDataHolder = (Room)((Room)localRoomBuffer.a(0)).a();
      }
      return paramDataHolder;
    }
    finally
    {
      localRoomBuffer.release();
    }
  }
  
  private void i()
  {
    this.f = null;
    this.g = null;
  }
  
  protected IGamesService a(IBinder paramIBinder)
  {
    return IGamesService.Stub.a(paramIBinder);
  }
  
  protected String a()
  {
    return "com.google.android.gms.games.service.START";
  }
  
  protected Set<Scope> a(Set<Scope> paramSet)
  {
    Scope localScope1 = new Scope("https://www.googleapis.com/auth/games");
    Scope localScope2 = new Scope("https://www.googleapis.com/auth/games.firstparty");
    Iterator localIterator = paramSet.iterator();
    int m = 0;
    boolean bool = false;
    Scope localScope3;
    if (localIterator.hasNext())
    {
      localScope3 = (Scope)localIterator.next();
      if (localScope3.equals(localScope1)) {
        bool = true;
      }
    }
    for (;;)
    {
      break;
      if (localScope3.equals(localScope2))
      {
        m = 1;
        continue;
        if (m != 0)
        {
          if (!bool) {}
          for (bool = true;; bool = false)
          {
            zzx.a(bool, "Cannot have both %s and %s!", new Object[] { "https://www.googleapis.com/auth/games", "https://www.googleapis.com/auth/games.firstparty" });
            return paramSet;
          }
        }
        zzx.a(bool, "Games APIs requires %s to function.", new Object[] { "https://www.googleapis.com/auth/games" });
        return paramSet;
      }
    }
  }
  
  protected void a(int paramInt1, IBinder paramIBinder, Bundle paramBundle, int paramInt2)
  {
    if ((paramInt1 == 0) && (paramBundle != null))
    {
      paramBundle.setClassLoader(GamesClientImpl.class.getClassLoader());
      this.i = paramBundle.getBoolean("show_welcome_popup");
      this.f = ((PlayerEntity)paramBundle.getParcelable("com.google.android.gms.games.current_player"));
      this.g = ((GameEntity)paramBundle.getParcelable("com.google.android.gms.games.current_game"));
    }
    super.a(paramInt1, paramIBinder, paramBundle, paramInt2);
  }
  
  public void a(IBinder paramIBinder, Bundle paramBundle)
  {
    if (k()) {}
    try
    {
      ((IGamesService)v()).a(paramIBinder, paramBundle);
      return;
    }
    catch (RemoteException paramIBinder)
    {
      a(paramIBinder);
    }
  }
  
  public void a(View paramView)
  {
    this.h.a(paramView);
  }
  
  public void a(ConnectionResult paramConnectionResult)
  {
    super.a(paramConnectionResult);
    this.i = false;
  }
  
  public void a(GoogleApiClient.zza paramzza)
  {
    i();
    super.a(paramzza);
  }
  
  public void a(zza.zzb<Games.GetTokenResult> paramzzb)
    throws RemoteException
  {
    ((IGamesService)v()).k(new GetAuthTokenBinderCallbacks(paramzzb));
  }
  
  public void a(zza.zzb<Invitations.LoadInvitationsResult> paramzzb, int paramInt)
    throws RemoteException
  {
    ((IGamesService)v()).a(new InvitationsLoadedBinderCallback(paramzzb), paramInt);
  }
  
  public void a(zza.zzb<Requests.LoadRequestsResult> paramzzb, int paramInt1, int paramInt2, int paramInt3)
    throws RemoteException
  {
    ((IGamesService)v()).a(new RequestsLoadedBinderCallbacks(paramzzb), paramInt1, paramInt2, paramInt3);
  }
  
  public void a(zza.zzb<AppContents.LoadAppContentResult> paramzzb, int paramInt, String paramString, String[] paramArrayOfString, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)v()).a(new AppContentLoadedBinderCallbacks(paramzzb), paramInt, paramString, paramArrayOfString, paramBoolean);
  }
  
  public void a(zza.zzb<Players.LoadPlayersResult> paramzzb, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException
  {
    ((IGamesService)v()).a(new PlayersLoadedBinderCallback(paramzzb), paramInt, paramBoolean1, paramBoolean2);
  }
  
  public void a(zza.zzb<TurnBasedMultiplayer.LoadMatchesResult> paramzzb, int paramInt, int[] paramArrayOfInt)
    throws RemoteException
  {
    ((IGamesService)v()).a(new TurnBasedMatchesLoadedBinderCallbacks(paramzzb), paramInt, paramArrayOfInt);
  }
  
  public void a(zza.zzb<Leaderboards.LoadScoresResult> paramzzb, LeaderboardScoreBuffer paramLeaderboardScoreBuffer, int paramInt1, int paramInt2)
    throws RemoteException
  {
    ((IGamesService)v()).a(new LeaderboardScoresLoadedBinderCallback(paramzzb), paramLeaderboardScoreBuffer.c().a(), paramInt1, paramInt2);
  }
  
  public void a(zza.zzb<TurnBasedMultiplayer.InitiateMatchResult> paramzzb, TurnBasedMatchConfig paramTurnBasedMatchConfig)
    throws RemoteException
  {
    ((IGamesService)v()).a(new TurnBasedMatchInitiatedBinderCallbacks(paramzzb), paramTurnBasedMatchConfig.a(), paramTurnBasedMatchConfig.b(), paramTurnBasedMatchConfig.c(), paramTurnBasedMatchConfig.d());
  }
  
  public void a(zza.zzb<Snapshots.CommitSnapshotResult> paramzzb, Snapshot paramSnapshot, SnapshotMetadataChange paramSnapshotMetadataChange)
    throws RemoteException
  {
    SnapshotContents localSnapshotContents = paramSnapshot.c();
    if (!localSnapshotContents.c()) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.a(bool, "Snapshot already closed");
      Object localObject = paramSnapshotMetadataChange.a();
      if (localObject != null) {
        ((BitmapTeleporter)localObject).a(q().getCacheDir());
      }
      localObject = localSnapshotContents.a();
      localSnapshotContents.b();
      ((IGamesService)v()).a(new SnapshotCommittedBinderCallbacks(paramzzb), paramSnapshot.b().d(), (SnapshotMetadataChangeEntity)paramSnapshotMetadataChange, (Contents)localObject);
      return;
    }
  }
  
  public void a(zza.zzb<Achievements.UpdateAchievementResult> paramzzb, String paramString)
    throws RemoteException
  {
    if (paramzzb == null) {}
    for (paramzzb = null;; paramzzb = new AchievementUpdatedBinderCallback(paramzzb))
    {
      ((IGamesService)v()).a(paramzzb, paramString, this.h.c(), this.h.b());
      return;
    }
  }
  
  public void a(zza.zzb<Achievements.UpdateAchievementResult> paramzzb, String paramString, int paramInt)
    throws RemoteException
  {
    if (paramzzb == null) {}
    for (paramzzb = null;; paramzzb = new AchievementUpdatedBinderCallback(paramzzb))
    {
      ((IGamesService)v()).a(paramzzb, paramString, paramInt, this.h.c(), this.h.b());
      return;
    }
  }
  
  public void a(zza.zzb<Leaderboards.LoadScoresResult> paramzzb, String paramString, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)v()).a(new LeaderboardScoresLoadedBinderCallback(paramzzb), paramString, paramInt1, paramInt2, paramInt3, paramBoolean);
  }
  
  public void a(zza.zzb<Players.LoadPlayersResult> paramzzb, String paramString, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException
  {
    int m = -1;
    switch (paramString.hashCode())
    {
    }
    for (;;)
    {
      switch (m)
      {
      default: 
        throw new IllegalArgumentException("Invalid player collection: " + paramString);
        if (paramString.equals("played_with")) {
          m = 0;
        }
        break;
      }
    }
    ((IGamesService)v()).d(new PlayersLoadedBinderCallback(paramzzb), paramString, paramInt, paramBoolean1, paramBoolean2);
  }
  
  public void a(zza.zzb<TurnBasedMultiplayer.LoadMatchesResult> paramzzb, String paramString, int paramInt, int[] paramArrayOfInt)
    throws RemoteException
  {
    ((IGamesService)v()).a(new TurnBasedMatchesLoadedBinderCallbacks(paramzzb), paramString, paramInt, paramArrayOfInt);
  }
  
  public void a(zza.zzb<Leaderboards.SubmitScoreResult> paramzzb, String paramString1, long paramLong, String paramString2)
    throws RemoteException
  {
    if (paramzzb == null) {}
    for (paramzzb = null;; paramzzb = new SubmitScoreBinderCallbacks(paramzzb))
    {
      ((IGamesService)v()).a(paramzzb, paramString1, paramLong, paramString2);
      return;
    }
  }
  
  public void a(zza.zzb<TurnBasedMultiplayer.LeaveMatchResult> paramzzb, String paramString1, String paramString2)
    throws RemoteException
  {
    ((IGamesService)v()).c(new TurnBasedMatchLeftBinderCallbacks(paramzzb), paramString1, paramString2);
  }
  
  public void a(zza.zzb<Leaderboards.LoadPlayerScoreResult> paramzzb, String paramString1, String paramString2, int paramInt1, int paramInt2)
    throws RemoteException
  {
    ((IGamesService)v()).a(new PlayerLeaderboardScoreLoadedBinderCallback(paramzzb), paramString1, paramString2, paramInt1, paramInt2);
  }
  
  public void a(zza.zzb<Requests.LoadRequestsResult> paramzzb, String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3)
    throws RemoteException
  {
    ((IGamesService)v()).a(new RequestsLoadedBinderCallbacks(paramzzb), paramString1, paramString2, paramInt1, paramInt2, paramInt3);
  }
  
  public void a(zza.zzb<Leaderboards.LoadScoresResult> paramzzb, String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)v()).a(new LeaderboardScoresLoadedBinderCallback(paramzzb), paramString1, paramString2, paramInt1, paramInt2, paramInt3, paramBoolean);
  }
  
  public void a(zza.zzb<Players.LoadPlayersResult> paramzzb, String paramString1, String paramString2, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException
  {
    int m = -1;
    switch (paramString1.hashCode())
    {
    }
    for (;;)
    {
      switch (m)
      {
      default: 
        throw new IllegalArgumentException("Invalid player collection: " + paramString1);
        if (paramString1.equals("circled"))
        {
          m = 0;
          continue;
          if (paramString1.equals("played_with"))
          {
            m = 1;
            continue;
            if (paramString1.equals("nearby")) {
              m = 2;
            }
          }
        }
        break;
      }
    }
    ((IGamesService)v()).a(new PlayersLoadedBinderCallback(paramzzb), paramString1, paramString2, paramInt, paramBoolean1, paramBoolean2);
  }
  
  public void a(zza.zzb<Snapshots.OpenSnapshotResult> paramzzb, String paramString1, String paramString2, SnapshotMetadataChange paramSnapshotMetadataChange, SnapshotContents paramSnapshotContents)
    throws RemoteException
  {
    if (!paramSnapshotContents.c()) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.a(bool, "SnapshotContents already closed");
      Object localObject = paramSnapshotMetadataChange.a();
      if (localObject != null) {
        ((BitmapTeleporter)localObject).a(q().getCacheDir());
      }
      localObject = paramSnapshotContents.a();
      paramSnapshotContents.b();
      ((IGamesService)v()).a(new SnapshotOpenedBinderCallbacks(paramzzb), paramString1, paramString2, (SnapshotMetadataChangeEntity)paramSnapshotMetadataChange, (Contents)localObject);
      return;
    }
  }
  
  public void a(zza.zzb<Leaderboards.LeaderboardMetadataResult> paramzzb, String paramString1, String paramString2, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)v()).b(new LeaderboardsLoadedBinderCallback(paramzzb), paramString1, paramString2, paramBoolean);
  }
  
  public void a(zza.zzb<Quests.LoadQuestsResult> paramzzb, String paramString1, String paramString2, boolean paramBoolean, String[] paramArrayOfString)
    throws RemoteException
  {
    this.a.b();
    ((IGamesService)v()).a(new QuestsLoadedBinderCallbacks(paramzzb), paramString1, paramString2, paramArrayOfString, paramBoolean);
  }
  
  public void a(zza.zzb<Quests.LoadQuestsResult> paramzzb, String paramString1, String paramString2, int[] paramArrayOfInt, int paramInt, boolean paramBoolean)
    throws RemoteException
  {
    this.a.b();
    ((IGamesService)v()).a(new QuestsLoadedBinderCallbacks(paramzzb), paramString1, paramString2, paramArrayOfInt, paramInt, paramBoolean);
  }
  
  public void a(zza.zzb<Requests.UpdateRequestsResult> paramzzb, String paramString1, String paramString2, String[] paramArrayOfString)
    throws RemoteException
  {
    ((IGamesService)v()).a(new RequestsUpdatedBinderCallbacks(paramzzb), paramString1, paramString2, paramArrayOfString);
  }
  
  public void a(zza.zzb<Players.LoadPlayersResult> paramzzb, String paramString, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)v()).f(new PlayersLoadedBinderCallback(paramzzb), paramString, paramBoolean);
  }
  
  public void a(zza.zzb<Snapshots.OpenSnapshotResult> paramzzb, String paramString, boolean paramBoolean, int paramInt)
    throws RemoteException
  {
    ((IGamesService)v()).a(new SnapshotOpenedBinderCallbacks(paramzzb), paramString, paramBoolean, paramInt);
  }
  
  public void a(zza.zzb<TurnBasedMultiplayer.UpdateMatchResult> paramzzb, String paramString1, byte[] paramArrayOfByte, String paramString2, ParticipantResult[] paramArrayOfParticipantResult)
    throws RemoteException
  {
    ((IGamesService)v()).a(new TurnBasedMatchUpdatedBinderCallbacks(paramzzb), paramString1, paramArrayOfByte, paramString2, paramArrayOfParticipantResult);
  }
  
  public void a(zza.zzb<TurnBasedMultiplayer.UpdateMatchResult> paramzzb, String paramString, byte[] paramArrayOfByte, ParticipantResult[] paramArrayOfParticipantResult)
    throws RemoteException
  {
    ((IGamesService)v()).a(new TurnBasedMatchUpdatedBinderCallbacks(paramzzb), paramString, paramArrayOfByte, paramArrayOfParticipantResult);
  }
  
  public void a(zza.zzb<Requests.SendRequestResult> paramzzb, String paramString, String[] paramArrayOfString, int paramInt1, byte[] paramArrayOfByte, int paramInt2)
    throws RemoteException
  {
    ((IGamesService)v()).a(new RequestSentBinderCallbacks(paramzzb), paramString, paramArrayOfString, paramInt1, paramArrayOfByte, paramInt2);
  }
  
  public void a(zza.zzb<Players.LoadPlayersResult> paramzzb, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)v()).c(new PlayersLoadedBinderCallback(paramzzb), paramBoolean);
  }
  
  public void a(zza.zzb<Status> paramzzb, boolean paramBoolean, Bundle paramBundle)
    throws RemoteException
  {
    ((IGamesService)v()).a(new ContactSettingsUpdatedBinderCallback(paramzzb), paramBoolean, paramBundle);
  }
  
  public void a(zza.zzb<Events.LoadEventsResult> paramzzb, boolean paramBoolean, String... paramVarArgs)
    throws RemoteException
  {
    this.a.b();
    ((IGamesService)v()).a(new EventsLoadedBinderCallback(paramzzb), paramBoolean, paramVarArgs);
  }
  
  public void a(zza.zzb<Quests.LoadQuestsResult> paramzzb, int[] paramArrayOfInt, int paramInt, boolean paramBoolean)
    throws RemoteException
  {
    this.a.b();
    ((IGamesService)v()).a(new QuestsLoadedBinderCallbacks(paramzzb), paramArrayOfInt, paramInt, paramBoolean);
  }
  
  public void a(zza.zzb<Players.LoadPlayersResult> paramzzb, String[] paramArrayOfString)
    throws RemoteException
  {
    ((IGamesService)v()).c(new PlayersLoadedBinderCallback(paramzzb), paramArrayOfString);
  }
  
  public void a(Games.BaseGamesApiMethodImpl<Status> paramBaseGamesApiMethodImpl, String paramString1, String paramString2, VideoConfiguration paramVideoConfiguration)
    throws RemoteException
  {
    ((IGamesService)v()).a(new StartRecordingBinderCallback(paramBaseGamesApiMethodImpl), paramString1, paramString2, paramVideoConfiguration);
  }
  
  public void a(@NonNull IGamesService paramIGamesService)
  {
    super.a(paramIGamesService);
    if (this.i)
    {
      this.h.a();
      this.i = false;
    }
    if (!this.l.a) {
      b(paramIGamesService);
    }
  }
  
  public void a(String paramString, int paramInt)
  {
    this.a.a(paramString, paramInt);
  }
  
  public void a(String paramString, zza.zzb<Games.GetServerAuthCodeResult> paramzzb)
    throws RemoteException
  {
    zzx.a(paramString, "Please provide a valid serverClientId");
    ((IGamesService)v()).a(paramString, new GetServerAuthCodeBinderCallbacks(paramzzb));
  }
  
  protected Bundle a_()
  {
    String str = q().getResources().getConfiguration().locale.toString();
    Bundle localBundle = this.l.a();
    localBundle.putString("com.google.android.gms.games.key.gamePackageName", this.e);
    localBundle.putString("com.google.android.gms.games.key.desiredLocale", str);
    localBundle.putParcelable("com.google.android.gms.games.key.popupWindowToken", new BinderWrapper(this.h.c()));
    localBundle.putInt("com.google.android.gms.games.key.API_VERSION", 3);
    localBundle.putBundle("com.google.android.gms.games.key.signInOptions", zzh.a(t()));
    return localBundle;
  }
  
  protected String b()
  {
    return "com.google.android.gms.games.internal.IGamesService";
  }
  
  public void b(zza.zzb<GamesMetadata.LoadGamesResult> paramzzb)
    throws RemoteException
  {
    ((IGamesService)v()).d(new GamesLoadedBinderCallback(paramzzb));
  }
  
  public void b(zza.zzb<Players.LoadPlayersResult> paramzzb, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException
  {
    ((IGamesService)v()).b(new PlayersLoadedBinderCallback(paramzzb), paramInt, paramBoolean1, paramBoolean2);
  }
  
  public void b(zza.zzb<Achievements.UpdateAchievementResult> paramzzb, String paramString)
    throws RemoteException
  {
    if (paramzzb == null) {}
    for (paramzzb = null;; paramzzb = new AchievementUpdatedBinderCallback(paramzzb))
    {
      ((IGamesService)v()).b(paramzzb, paramString, this.h.c(), this.h.b());
      return;
    }
  }
  
  public void b(zza.zzb<Achievements.UpdateAchievementResult> paramzzb, String paramString, int paramInt)
    throws RemoteException
  {
    if (paramzzb == null) {}
    for (paramzzb = null;; paramzzb = new AchievementUpdatedBinderCallback(paramzzb))
    {
      ((IGamesService)v()).b(paramzzb, paramString, paramInt, this.h.c(), this.h.b());
      return;
    }
  }
  
  public void b(zza.zzb<Leaderboards.LoadScoresResult> paramzzb, String paramString, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)v()).b(new LeaderboardScoresLoadedBinderCallback(paramzzb), paramString, paramInt1, paramInt2, paramInt3, paramBoolean);
  }
  
  public void b(zza.zzb<Players.LoadPlayersResult> paramzzb, String paramString, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException
  {
    ((IGamesService)v()).b(new PlayersLoadedBinderCallback(paramzzb), paramString, paramInt, paramBoolean1, paramBoolean2);
  }
  
  public void b(zza.zzb<Quests.ClaimMilestoneResult> paramzzb, String paramString1, String paramString2)
    throws RemoteException
  {
    this.a.b();
    ((IGamesService)v()).f(new QuestMilestoneClaimBinderCallbacks(paramzzb, paramString2), paramString1, paramString2);
  }
  
  public void b(zza.zzb<Leaderboards.LoadScoresResult> paramzzb, String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)v()).b(new LeaderboardScoresLoadedBinderCallback(paramzzb), paramString1, paramString2, paramInt1, paramInt2, paramInt3, paramBoolean);
  }
  
  public void b(zza.zzb<Players.LoadPlayersResult> paramzzb, String paramString1, String paramString2, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException
  {
    ((IGamesService)v()).b(new PlayersLoadedBinderCallback(paramzzb), paramString1, paramString2, paramInt, paramBoolean1, paramBoolean2);
  }
  
  public void b(zza.zzb<Achievements.LoadAchievementsResult> paramzzb, String paramString1, String paramString2, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)v()).a(new AchievementsLoadedBinderCallback(paramzzb), paramString1, paramString2, paramBoolean);
  }
  
  public void b(zza.zzb<Leaderboards.LeaderboardMetadataResult> paramzzb, String paramString, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)v()).c(new LeaderboardsLoadedBinderCallback(paramzzb), paramString, paramBoolean);
  }
  
  public void b(zza.zzb<Leaderboards.LeaderboardMetadataResult> paramzzb, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)v()).b(new LeaderboardsLoadedBinderCallback(paramzzb), paramBoolean);
  }
  
  public void b(zza.zzb<Quests.LoadQuestsResult> paramzzb, boolean paramBoolean, String[] paramArrayOfString)
    throws RemoteException
  {
    this.a.b();
    ((IGamesService)v()).a(new QuestsLoadedBinderCallbacks(paramzzb), paramArrayOfString, paramBoolean);
  }
  
  public void b(zza.zzb<Requests.UpdateRequestsResult> paramzzb, String[] paramArrayOfString)
    throws RemoteException
  {
    ((IGamesService)v()).a(new RequestsUpdatedBinderCallbacks(paramzzb), paramArrayOfString);
  }
  
  public void b(IGamesService paramIGamesService)
  {
    try
    {
      paramIGamesService.a(new PopupLocationInfoBinderCallbacks(this.h), this.k);
      return;
    }
    catch (RemoteException paramIGamesService)
    {
      a(paramIGamesService);
    }
  }
  
  public void c(zza.zzb<Status> paramzzb)
    throws RemoteException
  {
    this.a.b();
    ((IGamesService)v()).a(new SignOutCompleteBinderCallbacks(paramzzb));
  }
  
  public void c(zza.zzb<Players.LoadPlayersResult> paramzzb, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException
  {
    ((IGamesService)v()).c(new PlayersLoadedBinderCallback(paramzzb), paramInt, paramBoolean1, paramBoolean2);
  }
  
  public void c(zza.zzb<TurnBasedMultiplayer.InitiateMatchResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)v()).l(new TurnBasedMatchInitiatedBinderCallbacks(paramzzb), paramString);
  }
  
  public void c(zza.zzb<Players.LoadXpStreamResult> paramzzb, String paramString, int paramInt)
    throws RemoteException
  {
    ((IGamesService)v()).b(new PlayerXpStreamLoadedBinderCallback(paramzzb), paramString, paramInt);
  }
  
  public void c(zza.zzb<TurnBasedMultiplayer.InitiateMatchResult> paramzzb, String paramString1, String paramString2)
    throws RemoteException
  {
    ((IGamesService)v()).d(new TurnBasedMatchInitiatedBinderCallbacks(paramzzb), paramString1, paramString2);
  }
  
  public void c(zza.zzb<Snapshots.LoadSnapshotsResult> paramzzb, String paramString1, String paramString2, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)v()).c(new SnapshotsLoadedBinderCallbacks(paramzzb), paramString1, paramString2, paramBoolean);
  }
  
  public void c(zza.zzb<Leaderboards.LeaderboardMetadataResult> paramzzb, String paramString, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)v()).d(new LeaderboardsLoadedBinderCallback(paramzzb), paramString, paramBoolean);
  }
  
  public void c(zza.zzb<Achievements.LoadAchievementsResult> paramzzb, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)v()).a(new AchievementsLoadedBinderCallback(paramzzb), paramBoolean);
  }
  
  public void c(zza.zzb<Requests.UpdateRequestsResult> paramzzb, String[] paramArrayOfString)
    throws RemoteException
  {
    ((IGamesService)v()).b(new RequestsUpdatedBinderCallbacks(paramzzb), paramArrayOfString);
  }
  
  public void d(zza.zzb<Videos.VideoCapabilitiesResult> paramzzb)
    throws RemoteException
  {
    ((IGamesService)v()).m(new VideoRecordingCapabilitiesBinderCallback(paramzzb));
  }
  
  public void d(zza.zzb<Players.LoadPlayersResult> paramzzb, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws RemoteException
  {
    ((IGamesService)v()).e(new PlayersLoadedBinderCallback(paramzzb), paramInt, paramBoolean1, paramBoolean2);
  }
  
  public void d(zza.zzb<TurnBasedMultiplayer.InitiateMatchResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)v()).m(new TurnBasedMatchInitiatedBinderCallbacks(paramzzb), paramString);
  }
  
  public void d(zza.zzb<Players.LoadXpStreamResult> paramzzb, String paramString, int paramInt)
    throws RemoteException
  {
    ((IGamesService)v()).c(new PlayerXpStreamLoadedBinderCallback(paramzzb), paramString, paramInt);
  }
  
  public void d(zza.zzb<TurnBasedMultiplayer.InitiateMatchResult> paramzzb, String paramString1, String paramString2)
    throws RemoteException
  {
    ((IGamesService)v()).e(new TurnBasedMatchInitiatedBinderCallbacks(paramzzb), paramString1, paramString2);
  }
  
  public void d(zza.zzb<Notifications.GameMuteStatusChangeResult> paramzzb, String paramString, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)v()).a(new GameMuteStatusChangedBinderCallback(paramzzb), paramString, paramBoolean);
  }
  
  public void d(zza.zzb<Events.LoadEventsResult> paramzzb, boolean paramBoolean)
    throws RemoteException
  {
    this.a.b();
    ((IGamesService)v()).f(new EventsLoadedBinderCallback(paramzzb), paramBoolean);
  }
  
  public void e(zza.zzb<Videos.VideoAvailableResult> paramzzb)
    throws RemoteException
  {
    ((IGamesService)v()).n(new VideoRecordingAvailableBinderCallback(paramzzb));
  }
  
  public void e(zza.zzb<TurnBasedMultiplayer.LeaveMatchResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)v()).o(new TurnBasedMatchLeftBinderCallbacks(paramzzb), paramString);
  }
  
  public void e(zza.zzb<Invitations.LoadInvitationsResult> paramzzb, String paramString, int paramInt)
    throws RemoteException
  {
    ((IGamesService)v()).b(new InvitationsLoadedBinderCallback(paramzzb), paramString, paramInt, false);
  }
  
  public void e(zza.zzb<Stats.LoadPlayerStatsResult> paramzzb, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)v()).i(new PlayerStatsLoadedBinderCallbacks(paramzzb), paramBoolean);
  }
  
  public void f()
  {
    this.i = false;
    if (k()) {}
    try
    {
      IGamesService localIGamesService = (IGamesService)v();
      localIGamesService.c();
      this.a.b();
      localIGamesService.a(this.k);
      super.f();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        GamesLog.a("GamesClientImpl", "Failed to notify client disconnect.");
      }
    }
  }
  
  public void f(zza.zzb<Videos.ListVideosResult> paramzzb)
    throws RemoteException
  {
    ((IGamesService)v()).l(new ListVideosBinderCallback(paramzzb));
  }
  
  public void f(zza.zzb<TurnBasedMultiplayer.CancelMatchResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)v()).n(new TurnBasedMatchCanceledBinderCallbacks(paramzzb), paramString);
  }
  
  public void f(zza.zzb<Requests.LoadRequestSummariesResult> paramzzb, String paramString, int paramInt)
    throws RemoteException
  {
    ((IGamesService)v()).a(new RequestSummariesLoadedBinderCallbacks(paramzzb), paramString, paramInt);
  }
  
  public void f(zza.zzb<Snapshots.LoadSnapshotsResult> paramzzb, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)v()).d(new SnapshotsLoadedBinderCallbacks(paramzzb), paramBoolean);
  }
  
  public void g(zza.zzb<Acls.LoadAclResult> paramzzb)
    throws RemoteException
  {
    ((IGamesService)v()).h(new NotifyAclLoadedBinderCallback(paramzzb));
  }
  
  public void g(zza.zzb<TurnBasedMultiplayer.LoadMatchResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)v()).p(new TurnBasedMatchLoadedBinderCallbacks(paramzzb), paramString);
  }
  
  public void g(zza.zzb<Players.LoadProfileSettingsResult> paramzzb, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)v()).g(new ProfileSettingsLoadedBinderCallback(paramzzb), paramBoolean);
  }
  
  public void h()
  {
    if (k()) {}
    try
    {
      ((IGamesService)v()).c();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      a(localRemoteException);
    }
  }
  
  public void h(zza.zzb<Notifications.InboxCountResult> paramzzb)
    throws RemoteException
  {
    ((IGamesService)v()).t(new InboxCountsLoadedBinderCallback(paramzzb), null);
  }
  
  public void h(zza.zzb<Quests.AcceptQuestResult> paramzzb, String paramString)
    throws RemoteException
  {
    this.a.b();
    ((IGamesService)v()).u(new QuestAcceptedBinderCallbacks(paramzzb), paramString);
  }
  
  public void h(zza.zzb<Status> paramzzb, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)v()).h(new ProfileSettingsUpdatedBinderCallback(paramzzb), paramBoolean);
  }
  
  public void i(zza.zzb<Games.LoadExperimentsResult> paramzzb)
    throws RemoteException
  {
    ((IGamesService)v()).o(new ExperimentsLoadedBinderCallback(paramzzb));
  }
  
  public void i(zza.zzb<Snapshots.DeleteSnapshotResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)v()).r(new SnapshotDeletedBinderCallbacks(paramzzb), paramString);
  }
  
  public void i(zza.zzb<Notifications.ContactSettingLoadResult> paramzzb, boolean paramBoolean)
    throws RemoteException
  {
    ((IGamesService)v()).e(new ContactSettingsLoadedBinderCallback(paramzzb), paramBoolean);
  }
  
  public void j(zza.zzb<GamesMetadata.LoadGameInstancesResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)v()).f(new GameInstancesLoadedBinderCallback(paramzzb), paramString);
  }
  
  public void k(zza.zzb<GamesMetadata.LoadGameSearchSuggestionsResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)v()).q(new GameSearchSuggestionsLoadedBinderCallback(paramzzb), paramString);
  }
  
  public void l(zza.zzb<Players.LoadXpForGameCategoriesResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)v()).s(new PlayerXpForGameCategoriesLoadedBinderCallback(paramzzb), paramString);
  }
  
  public boolean l()
  {
    return true;
  }
  
  public void m(zza.zzb<Invitations.LoadInvitationsResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)v()).k(new InvitationsLoadedBinderCallback(paramzzb), paramString);
  }
  
  public Bundle m_()
  {
    try
    {
      Bundle localBundle = ((IGamesService)v()).b();
      if (localBundle != null) {
        localBundle.setClassLoader(GamesClientImpl.class.getClassLoader());
      }
      return localBundle;
    }
    catch (RemoteException localRemoteException)
    {
      a(localRemoteException);
    }
    return null;
  }
  
  public void n(zza.zzb<Status> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)v()).j(new NotifyAclUpdatedBinderCallback(paramzzb), paramString);
  }
  
  public void o(zza.zzb<Notifications.GameMuteStatusLoadResult> paramzzb, String paramString)
    throws RemoteException
  {
    ((IGamesService)v()).i(new GameMuteStatusLoadedBinderCallback(paramzzb), paramString);
  }
  
  private static abstract class AbstractPeerStatusNotifier
    extends GamesClientImpl.AbstractRoomStatusNotifier
  {
    private final ArrayList<String> a = new ArrayList();
    
    AbstractPeerStatusNotifier(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      super();
      int i = 0;
      int j = paramArrayOfString.length;
      while (i < j)
      {
        this.a.add(paramArrayOfString[i]);
        i += 1;
      }
    }
    
    protected void a(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom)
    {
      a(paramRoomStatusUpdateListener, paramRoom, this.a);
    }
    
    protected abstract void a(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom, ArrayList<String> paramArrayList);
  }
  
  private static abstract class AbstractRoomNotifier
    extends zze<RoomUpdateListener>
  {
    AbstractRoomNotifier(DataHolder paramDataHolder)
    {
      super();
    }
    
    protected void a(RoomUpdateListener paramRoomUpdateListener, DataHolder paramDataHolder)
    {
      a(paramRoomUpdateListener, GamesClientImpl.a(paramDataHolder), paramDataHolder.e());
    }
    
    protected abstract void a(RoomUpdateListener paramRoomUpdateListener, Room paramRoom, int paramInt);
  }
  
  private static abstract class AbstractRoomStatusNotifier
    extends zze<RoomStatusUpdateListener>
  {
    AbstractRoomStatusNotifier(DataHolder paramDataHolder)
    {
      super();
    }
    
    protected void a(RoomStatusUpdateListener paramRoomStatusUpdateListener, DataHolder paramDataHolder)
    {
      a(paramRoomStatusUpdateListener, GamesClientImpl.a(paramDataHolder));
    }
    
    protected abstract void a(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom);
  }
  
  private static final class AcceptQuestResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Quests.AcceptQuestResult
  {
    private final Quest c;
    
    /* Error */
    AcceptQuestResultImpl(DataHolder paramDataHolder)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_1
      //   2: invokespecial 15	com/google/android/gms/games/internal/GamesClientImpl$GamesDataHolderResult:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: new 17	com/google/android/gms/games/quest/QuestBuffer
      //   8: dup
      //   9: aload_1
      //   10: invokespecial 18	com/google/android/gms/games/quest/QuestBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   13: astore_1
      //   14: aload_1
      //   15: invokevirtual 22	com/google/android/gms/games/quest/QuestBuffer:a	()I
      //   18: ifle +27 -> 45
      //   21: aload_0
      //   22: new 24	com/google/android/gms/games/quest/QuestEntity
      //   25: dup
      //   26: aload_1
      //   27: iconst_0
      //   28: invokevirtual 27	com/google/android/gms/games/quest/QuestBuffer:a	(I)Ljava/lang/Object;
      //   31: checkcast 29	com/google/android/gms/games/quest/Quest
      //   34: invokespecial 32	com/google/android/gms/games/quest/QuestEntity:<init>	(Lcom/google/android/gms/games/quest/Quest;)V
      //   37: putfield 34	com/google/android/gms/games/internal/GamesClientImpl$AcceptQuestResultImpl:c	Lcom/google/android/gms/games/quest/Quest;
      //   40: aload_1
      //   41: invokevirtual 38	com/google/android/gms/games/quest/QuestBuffer:release	()V
      //   44: return
      //   45: aload_0
      //   46: aconst_null
      //   47: putfield 34	com/google/android/gms/games/internal/GamesClientImpl$AcceptQuestResultImpl:c	Lcom/google/android/gms/games/quest/Quest;
      //   50: goto -10 -> 40
      //   53: astore_2
      //   54: aload_1
      //   55: invokevirtual 38	com/google/android/gms/games/quest/QuestBuffer:release	()V
      //   58: aload_2
      //   59: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	60	0	this	AcceptQuestResultImpl
      //   0	60	1	paramDataHolder	DataHolder
      //   53	6	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   14	40	53	finally
      //   45	50	53	finally
    }
  }
  
  private static final class AchievementUpdatedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Achievements.UpdateAchievementResult> a;
    
    AchievementUpdatedBinderCallback(zza.zzb<Achievements.UpdateAchievementResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void b(int paramInt, String paramString)
    {
      this.a.a(new GamesClientImpl.UpdateAchievementResultImpl(paramInt, paramString));
    }
  }
  
  private static final class AchievementsLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Achievements.LoadAchievementsResult> a;
    
    AchievementsLoadedBinderCallback(zza.zzb<Achievements.LoadAchievementsResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void a(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.LoadAchievementsResultImpl(paramDataHolder));
    }
  }
  
  private static final class AppContentLoadedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<AppContents.LoadAppContentResult> a;
    
    public AppContentLoadedBinderCallbacks(zza.zzb<AppContents.LoadAppContentResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void a(DataHolder[] paramArrayOfDataHolder)
    {
      this.a.a(new GamesClientImpl.LoadAppContentsResultImpl(paramArrayOfDataHolder));
    }
  }
  
  private static final class CancelMatchResultImpl
    implements TurnBasedMultiplayer.CancelMatchResult
  {
    private final Status a;
    private final String b;
    
    CancelMatchResultImpl(Status paramStatus, String paramString)
    {
      this.a = paramStatus;
      this.b = paramString;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
  
  private static final class ClaimMilestoneResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Quests.ClaimMilestoneResult
  {
    private final Milestone c;
    private final Quest d;
    
    /* Error */
    ClaimMilestoneResultImpl(DataHolder paramDataHolder, String paramString)
    {
      // Byte code:
      //   0: iconst_0
      //   1: istore_3
      //   2: aload_0
      //   3: aload_1
      //   4: invokespecial 18	com/google/android/gms/games/internal/GamesClientImpl$GamesDataHolderResult:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   7: new 20	com/google/android/gms/games/quest/QuestBuffer
      //   10: dup
      //   11: aload_1
      //   12: invokespecial 21	com/google/android/gms/games/quest/QuestBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   15: astore_1
      //   16: aload_1
      //   17: invokevirtual 25	com/google/android/gms/games/quest/QuestBuffer:a	()I
      //   20: ifle +108 -> 128
      //   23: aload_0
      //   24: new 27	com/google/android/gms/games/quest/QuestEntity
      //   27: dup
      //   28: aload_1
      //   29: iconst_0
      //   30: invokevirtual 30	com/google/android/gms/games/quest/QuestBuffer:a	(I)Ljava/lang/Object;
      //   33: checkcast 32	com/google/android/gms/games/quest/Quest
      //   36: invokespecial 35	com/google/android/gms/games/quest/QuestEntity:<init>	(Lcom/google/android/gms/games/quest/Quest;)V
      //   39: putfield 37	com/google/android/gms/games/internal/GamesClientImpl$ClaimMilestoneResultImpl:d	Lcom/google/android/gms/games/quest/Quest;
      //   42: aload_0
      //   43: getfield 37	com/google/android/gms/games/internal/GamesClientImpl$ClaimMilestoneResultImpl:d	Lcom/google/android/gms/games/quest/Quest;
      //   46: invokeinterface 41 1 0
      //   51: astore 5
      //   53: aload 5
      //   55: invokeinterface 46 1 0
      //   60: istore 4
      //   62: iload_3
      //   63: iload 4
      //   65: if_icmpge +53 -> 118
      //   68: aload 5
      //   70: iload_3
      //   71: invokeinterface 49 2 0
      //   76: checkcast 51	com/google/android/gms/games/quest/Milestone
      //   79: invokeinterface 55 1 0
      //   84: aload_2
      //   85: invokevirtual 61	java/lang/String:equals	(Ljava/lang/Object;)Z
      //   88: ifeq +23 -> 111
      //   91: aload_0
      //   92: aload 5
      //   94: iload_3
      //   95: invokeinterface 49 2 0
      //   100: checkcast 51	com/google/android/gms/games/quest/Milestone
      //   103: putfield 63	com/google/android/gms/games/internal/GamesClientImpl$ClaimMilestoneResultImpl:c	Lcom/google/android/gms/games/quest/Milestone;
      //   106: aload_1
      //   107: invokevirtual 67	com/google/android/gms/games/quest/QuestBuffer:release	()V
      //   110: return
      //   111: iload_3
      //   112: iconst_1
      //   113: iadd
      //   114: istore_3
      //   115: goto -53 -> 62
      //   118: aload_0
      //   119: aconst_null
      //   120: putfield 63	com/google/android/gms/games/internal/GamesClientImpl$ClaimMilestoneResultImpl:c	Lcom/google/android/gms/games/quest/Milestone;
      //   123: aload_1
      //   124: invokevirtual 67	com/google/android/gms/games/quest/QuestBuffer:release	()V
      //   127: return
      //   128: aload_0
      //   129: aconst_null
      //   130: putfield 63	com/google/android/gms/games/internal/GamesClientImpl$ClaimMilestoneResultImpl:c	Lcom/google/android/gms/games/quest/Milestone;
      //   133: aload_0
      //   134: aconst_null
      //   135: putfield 37	com/google/android/gms/games/internal/GamesClientImpl$ClaimMilestoneResultImpl:d	Lcom/google/android/gms/games/quest/Quest;
      //   138: goto -15 -> 123
      //   141: astore_2
      //   142: aload_1
      //   143: invokevirtual 67	com/google/android/gms/games/quest/QuestBuffer:release	()V
      //   146: aload_2
      //   147: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	148	0	this	ClaimMilestoneResultImpl
      //   0	148	1	paramDataHolder	DataHolder
      //   0	148	2	paramString	String
      //   1	114	3	i	int
      //   60	6	4	j	int
      //   51	42	5	localList	List
      // Exception table:
      //   from	to	target	type
      //   16	62	141	finally
      //   68	106	141	finally
      //   118	123	141	finally
      //   128	138	141	finally
    }
  }
  
  private static final class CommitSnapshotResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Snapshots.CommitSnapshotResult
  {
    private final SnapshotMetadata c;
    
    /* Error */
    CommitSnapshotResultImpl(DataHolder paramDataHolder)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_1
      //   2: invokespecial 15	com/google/android/gms/games/internal/GamesClientImpl$GamesDataHolderResult:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: new 17	com/google/android/gms/games/snapshot/SnapshotMetadataBuffer
      //   8: dup
      //   9: aload_1
      //   10: invokespecial 18	com/google/android/gms/games/snapshot/SnapshotMetadataBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   13: astore_1
      //   14: aload_1
      //   15: invokevirtual 22	com/google/android/gms/games/snapshot/SnapshotMetadataBuffer:a	()I
      //   18: ifle +24 -> 42
      //   21: aload_0
      //   22: new 24	com/google/android/gms/games/snapshot/SnapshotMetadataEntity
      //   25: dup
      //   26: aload_1
      //   27: iconst_0
      //   28: invokevirtual 28	com/google/android/gms/games/snapshot/SnapshotMetadataBuffer:b	(I)Lcom/google/android/gms/games/snapshot/SnapshotMetadata;
      //   31: invokespecial 31	com/google/android/gms/games/snapshot/SnapshotMetadataEntity:<init>	(Lcom/google/android/gms/games/snapshot/SnapshotMetadata;)V
      //   34: putfield 33	com/google/android/gms/games/internal/GamesClientImpl$CommitSnapshotResultImpl:c	Lcom/google/android/gms/games/snapshot/SnapshotMetadata;
      //   37: aload_1
      //   38: invokevirtual 37	com/google/android/gms/games/snapshot/SnapshotMetadataBuffer:release	()V
      //   41: return
      //   42: aload_0
      //   43: aconst_null
      //   44: putfield 33	com/google/android/gms/games/internal/GamesClientImpl$CommitSnapshotResultImpl:c	Lcom/google/android/gms/games/snapshot/SnapshotMetadata;
      //   47: goto -10 -> 37
      //   50: astore_2
      //   51: aload_1
      //   52: invokevirtual 37	com/google/android/gms/games/snapshot/SnapshotMetadataBuffer:release	()V
      //   55: aload_2
      //   56: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	57	0	this	CommitSnapshotResultImpl
      //   0	57	1	paramDataHolder	DataHolder
      //   50	6	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   14	37	50	finally
      //   42	47	50	finally
    }
  }
  
  private static final class ConnectedToRoomNotifier
    extends GamesClientImpl.AbstractRoomStatusNotifier
  {
    ConnectedToRoomNotifier(DataHolder paramDataHolder)
    {
      super();
    }
    
    public void a(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom)
    {
      paramRoomStatusUpdateListener.c(paramRoom);
    }
  }
  
  private static final class ContactSettingLoadResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Notifications.ContactSettingLoadResult
  {
    ContactSettingLoadResultImpl(DataHolder paramDataHolder)
    {
      super();
    }
  }
  
  private static final class ContactSettingsLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Notifications.ContactSettingLoadResult> a;
    
    ContactSettingsLoadedBinderCallback(zza.zzb<Notifications.ContactSettingLoadResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void B(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.ContactSettingLoadResultImpl(paramDataHolder));
    }
  }
  
  private static final class ContactSettingsUpdatedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Status> a;
    
    ContactSettingsUpdatedBinderCallback(zza.zzb<Status> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void b(int paramInt)
    {
      this.a.a(GamesStatusCodes.a(paramInt));
    }
  }
  
  private static final class DeleteSnapshotResultImpl
    implements Snapshots.DeleteSnapshotResult
  {
    private final Status a;
    private final String b;
    
    DeleteSnapshotResultImpl(int paramInt, String paramString)
    {
      this.a = GamesStatusCodes.a(paramInt);
      this.b = paramString;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
  
  private static final class DisconnectedFromRoomNotifier
    extends GamesClientImpl.AbstractRoomStatusNotifier
  {
    DisconnectedFromRoomNotifier(DataHolder paramDataHolder)
    {
      super();
    }
    
    public void a(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom)
    {
      paramRoomStatusUpdateListener.d(paramRoom);
    }
  }
  
  private static final class EventsLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Events.LoadEventsResult> a;
    
    EventsLoadedBinderCallback(zza.zzb<Events.LoadEventsResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void b(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.LoadEventResultImpl(paramDataHolder));
    }
  }
  
  private static final class ExperimentsLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Games.LoadExperimentsResult> a;
    
    ExperimentsLoadedBinderCallback(zza.zzb<Games.LoadExperimentsResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void a(int paramInt, long[] paramArrayOfLong)
    {
      this.a.a(new GamesClientImpl.LoadExperimentsResultImpl(paramInt, paramArrayOfLong));
    }
  }
  
  private class GameClientEventIncrementCache
    extends EventIncrementCache
  {
    public GameClientEventIncrementCache()
    {
      super(1000);
    }
    
    protected void a(String paramString, int paramInt)
    {
      try
      {
        if (GamesClientImpl.this.k())
        {
          ((IGamesService)GamesClientImpl.this.v()).e(paramString, paramInt);
          return;
        }
        GamesLog.b("GamesClientImpl", "Unable to increment event " + paramString + " by " + paramInt + " because the games client is no longer connected");
        return;
      }
      catch (RemoteException paramString)
      {
        GamesClientImpl.a(GamesClientImpl.this, paramString);
      }
    }
  }
  
  private static final class GameInstancesLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<GamesMetadata.LoadGameInstancesResult> a;
    
    GameInstancesLoadedBinderCallback(zza.zzb<GamesMetadata.LoadGameInstancesResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void i(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.LoadGameInstancesResultImpl(paramDataHolder));
    }
  }
  
  private static final class GameMuteStatusChangeResultImpl
    implements Notifications.GameMuteStatusChangeResult
  {
    private final Status a;
    private final String b;
    private final boolean c;
    
    public GameMuteStatusChangeResultImpl(int paramInt, String paramString, boolean paramBoolean)
    {
      this.a = GamesStatusCodes.a(paramInt);
      this.b = paramString;
      this.c = paramBoolean;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
  
  private static final class GameMuteStatusChangedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Notifications.GameMuteStatusChangeResult> a;
    
    GameMuteStatusChangedBinderCallback(zza.zzb<Notifications.GameMuteStatusChangeResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void a(int paramInt, String paramString, boolean paramBoolean)
    {
      this.a.a(new GamesClientImpl.GameMuteStatusChangeResultImpl(paramInt, paramString, paramBoolean));
    }
  }
  
  private static final class GameMuteStatusLoadResultImpl
    implements Notifications.GameMuteStatusLoadResult
  {
    private final Status a;
    private final String b;
    private final boolean c;
    
    /* Error */
    public GameMuteStatusLoadResultImpl(DataHolder paramDataHolder)
    {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial 20	java/lang/Object:<init>	()V
      //   4: aload_0
      //   5: aload_1
      //   6: invokevirtual 26	com/google/android/gms/common/data/DataHolder:e	()I
      //   9: invokestatic 31	com/google/android/gms/games/GamesStatusCodes:a	(I)Lcom/google/android/gms/common/api/Status;
      //   12: putfield 33	com/google/android/gms/games/internal/GamesClientImpl$GameMuteStatusLoadResultImpl:a	Lcom/google/android/gms/common/api/Status;
      //   15: aload_1
      //   16: invokevirtual 36	com/google/android/gms/common/data/DataHolder:g	()I
      //   19: ifle +32 -> 51
      //   22: aload_0
      //   23: aload_1
      //   24: ldc 38
      //   26: iconst_0
      //   27: iconst_0
      //   28: invokevirtual 41	com/google/android/gms/common/data/DataHolder:c	(Ljava/lang/String;II)Ljava/lang/String;
      //   31: putfield 43	com/google/android/gms/games/internal/GamesClientImpl$GameMuteStatusLoadResultImpl:b	Ljava/lang/String;
      //   34: aload_0
      //   35: aload_1
      //   36: ldc 45
      //   38: iconst_0
      //   39: iconst_0
      //   40: invokevirtual 49	com/google/android/gms/common/data/DataHolder:d	(Ljava/lang/String;II)Z
      //   43: putfield 51	com/google/android/gms/games/internal/GamesClientImpl$GameMuteStatusLoadResultImpl:c	Z
      //   46: aload_1
      //   47: invokevirtual 54	com/google/android/gms/common/data/DataHolder:i	()V
      //   50: return
      //   51: aload_0
      //   52: aconst_null
      //   53: putfield 43	com/google/android/gms/games/internal/GamesClientImpl$GameMuteStatusLoadResultImpl:b	Ljava/lang/String;
      //   56: aload_0
      //   57: iconst_0
      //   58: putfield 51	com/google/android/gms/games/internal/GamesClientImpl$GameMuteStatusLoadResultImpl:c	Z
      //   61: goto -15 -> 46
      //   64: astore_2
      //   65: aload_1
      //   66: invokevirtual 54	com/google/android/gms/common/data/DataHolder:i	()V
      //   69: aload_2
      //   70: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	71	0	this	GameMuteStatusLoadResultImpl
      //   0	71	1	paramDataHolder	DataHolder
      //   64	6	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   4	46	64	finally
      //   51	61	64	finally
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
  
  private static final class GameMuteStatusLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Notifications.GameMuteStatusLoadResult> a;
    
    GameMuteStatusLoadedBinderCallback(zza.zzb<Notifications.GameMuteStatusLoadResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void z(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.GameMuteStatusLoadResultImpl(paramDataHolder));
    }
  }
  
  private static final class GameSearchSuggestionsLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<GamesMetadata.LoadGameSearchSuggestionsResult> a;
    
    GameSearchSuggestionsLoadedBinderCallback(zza.zzb<GamesMetadata.LoadGameSearchSuggestionsResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void j(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.LoadGameSearchSuggestionsResultImpl(paramDataHolder));
    }
  }
  
  private static abstract class GamesDataHolderResult
    extends com.google.android.gms.common.api.internal.zzf
  {
    protected GamesDataHolderResult(DataHolder paramDataHolder)
    {
      super(GamesStatusCodes.a(paramDataHolder.e()));
    }
  }
  
  private static final class GamesLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<GamesMetadata.LoadGamesResult> a;
    
    GamesLoadedBinderCallback(zza.zzb<GamesMetadata.LoadGamesResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void g(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.LoadGamesResultImpl(paramDataHolder));
    }
  }
  
  private static final class GetAuthTokenBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Games.GetTokenResult> a;
    
    public GetAuthTokenBinderCallbacks(zza.zzb<Games.GetTokenResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void a(int paramInt, String paramString)
    {
      Status localStatus = GamesStatusCodes.a(paramInt);
      this.a.a(new GamesClientImpl.GetTokenResultImpl(localStatus, paramString));
    }
  }
  
  private static final class GetServerAuthCodeBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Games.GetServerAuthCodeResult> a;
    
    public GetServerAuthCodeBinderCallbacks(zza.zzb<Games.GetServerAuthCodeResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void a(int paramInt, String paramString)
    {
      Status localStatus = GamesStatusCodes.a(paramInt);
      this.a.a(new GamesClientImpl.GetServerAuthCodeResultImpl(localStatus, paramString));
    }
  }
  
  private static final class GetServerAuthCodeResultImpl
    implements Games.GetServerAuthCodeResult
  {
    private final Status a;
    private final String b;
    
    GetServerAuthCodeResultImpl(Status paramStatus, String paramString)
    {
      this.a = paramStatus;
      this.b = paramString;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
  
  private static final class GetTokenResultImpl
    implements Games.GetTokenResult
  {
    private final Status a;
    private final String b;
    
    GetTokenResultImpl(Status paramStatus, String paramString)
    {
      this.a = paramStatus;
      this.b = paramString;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
  
  private static final class InboxCountResultImpl
    implements Notifications.InboxCountResult
  {
    private final Status a;
    private final Bundle b;
    
    InboxCountResultImpl(Status paramStatus, Bundle paramBundle)
    {
      this.a = paramStatus;
      this.b = paramBundle;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
  
  private static final class InboxCountsLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Notifications.InboxCountResult> a;
    
    InboxCountsLoadedBinderCallback(zza.zzb<Notifications.InboxCountResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void e(int paramInt, Bundle paramBundle)
    {
      paramBundle.setClassLoader(getClass().getClassLoader());
      Status localStatus = GamesStatusCodes.a(paramInt);
      this.a.a(new GamesClientImpl.InboxCountResultImpl(localStatus, paramBundle));
    }
  }
  
  private static final class InitiateMatchResultImpl
    extends GamesClientImpl.TurnBasedMatchResult
    implements TurnBasedMultiplayer.InitiateMatchResult
  {
    InitiateMatchResultImpl(DataHolder paramDataHolder)
    {
      super();
    }
  }
  
  private static final class InvitationReceivedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzq<OnInvitationReceivedListener> a;
    
    public void a(String paramString)
    {
      this.a.a(new GamesClientImpl.InvitationRemovedNotifier(paramString));
    }
    
    public void l(DataHolder paramDataHolder)
    {
      InvitationBuffer localInvitationBuffer = new InvitationBuffer(paramDataHolder);
      paramDataHolder = null;
      try
      {
        if (localInvitationBuffer.a() > 0) {
          paramDataHolder = (Invitation)((Invitation)localInvitationBuffer.a(0)).a();
        }
        localInvitationBuffer.release();
        if (paramDataHolder != null) {
          this.a.a(new GamesClientImpl.InvitationReceivedNotifier(paramDataHolder));
        }
        return;
      }
      finally
      {
        localInvitationBuffer.release();
      }
    }
  }
  
  private static final class InvitationReceivedNotifier
    implements zzq.zzb<OnInvitationReceivedListener>
  {
    private final Invitation a;
    
    InvitationReceivedNotifier(Invitation paramInvitation)
    {
      this.a = paramInvitation;
    }
    
    public void a() {}
    
    public void a(OnInvitationReceivedListener paramOnInvitationReceivedListener)
    {
      paramOnInvitationReceivedListener.a(this.a);
    }
  }
  
  private static final class InvitationRemovedNotifier
    implements zzq.zzb<OnInvitationReceivedListener>
  {
    private final String a;
    
    InvitationRemovedNotifier(String paramString)
    {
      this.a = paramString;
    }
    
    public void a() {}
    
    public void a(OnInvitationReceivedListener paramOnInvitationReceivedListener)
    {
      paramOnInvitationReceivedListener.a(this.a);
    }
  }
  
  private static final class InvitationsLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Invitations.LoadInvitationsResult> a;
    
    InvitationsLoadedBinderCallback(zza.zzb<Invitations.LoadInvitationsResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void k(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.LoadInvitationsResultImpl(paramDataHolder));
    }
  }
  
  private static final class JoinedRoomNotifier
    extends GamesClientImpl.AbstractRoomNotifier
  {
    public JoinedRoomNotifier(DataHolder paramDataHolder)
    {
      super();
    }
    
    public void a(RoomUpdateListener paramRoomUpdateListener, Room paramRoom, int paramInt)
    {
      paramRoomUpdateListener.b(paramInt, paramRoom);
    }
  }
  
  private static final class LeaderboardMetadataResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Leaderboards.LeaderboardMetadataResult
  {
    private final LeaderboardBuffer c;
    
    LeaderboardMetadataResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.c = new LeaderboardBuffer(paramDataHolder);
    }
  }
  
  private static final class LeaderboardScoresLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Leaderboards.LoadScoresResult> a;
    
    LeaderboardScoresLoadedBinderCallback(zza.zzb<Leaderboards.LoadScoresResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void a(DataHolder paramDataHolder1, DataHolder paramDataHolder2)
    {
      this.a.a(new GamesClientImpl.LoadScoresResultImpl(paramDataHolder1, paramDataHolder2));
    }
  }
  
  private static final class LeaderboardsLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Leaderboards.LeaderboardMetadataResult> a;
    
    LeaderboardsLoadedBinderCallback(zza.zzb<Leaderboards.LeaderboardMetadataResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void c(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.LeaderboardMetadataResultImpl(paramDataHolder));
    }
  }
  
  private static final class LeaveMatchResultImpl
    extends GamesClientImpl.TurnBasedMatchResult
    implements TurnBasedMultiplayer.LeaveMatchResult
  {
    LeaveMatchResultImpl(DataHolder paramDataHolder)
    {
      super();
    }
  }
  
  private static final class LeftRoomNotifier
    implements zzq.zzb<RoomUpdateListener>
  {
    private final int a;
    private final String b;
    
    LeftRoomNotifier(int paramInt, String paramString)
    {
      this.a = paramInt;
      this.b = paramString;
    }
    
    public void a() {}
    
    public void a(RoomUpdateListener paramRoomUpdateListener)
    {
      paramRoomUpdateListener.a(this.a, this.b);
    }
  }
  
  private static final class ListVideosBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Videos.ListVideosResult> a;
    
    ListVideosBinderCallback(zza.zzb<Videos.ListVideosResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void Q(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.ListVideosResultImpl(paramDataHolder));
    }
  }
  
  public static final class ListVideosResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Videos.ListVideosResult
  {
    private final VideoBuffer c;
    
    public ListVideosResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.c = new VideoBuffer(paramDataHolder);
    }
  }
  
  private static final class LoadAchievementsResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Achievements.LoadAchievementsResult
  {
    private final AchievementBuffer c;
    
    LoadAchievementsResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.c = new AchievementBuffer(paramDataHolder);
    }
  }
  
  private static final class LoadAclResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Acls.LoadAclResult
  {
    LoadAclResultImpl(DataHolder paramDataHolder)
    {
      super();
    }
  }
  
  private static final class LoadAppContentsResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements AppContents.LoadAppContentResult
  {
    private final ArrayList<DataHolder> c;
    
    LoadAppContentsResultImpl(DataHolder[] paramArrayOfDataHolder)
    {
      super();
      this.c = new ArrayList(Arrays.asList(paramArrayOfDataHolder));
    }
  }
  
  private static final class LoadEventResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Events.LoadEventsResult
  {
    private final EventBuffer c;
    
    LoadEventResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.c = new EventBuffer(paramDataHolder);
    }
  }
  
  private static final class LoadExperimentsResultImpl
    implements Games.LoadExperimentsResult
  {
    private final Status a;
    private final Set<Long> b;
    
    LoadExperimentsResultImpl(int paramInt, long[] paramArrayOfLong)
    {
      this.a = new Status(paramInt);
      this.b = new HashSet();
      paramInt = 0;
      while (paramInt < paramArrayOfLong.length)
      {
        this.b.add(Long.valueOf(paramArrayOfLong[paramInt]));
        paramInt += 1;
      }
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
  
  private static final class LoadGameInstancesResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements GamesMetadata.LoadGameInstancesResult
  {
    private final GameInstanceBuffer c;
    
    LoadGameInstancesResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.c = new GameInstanceBuffer(paramDataHolder);
    }
  }
  
  private static final class LoadGameSearchSuggestionsResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements GamesMetadata.LoadGameSearchSuggestionsResult
  {
    private final GameSearchSuggestionBuffer c;
    
    LoadGameSearchSuggestionsResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.c = new GameSearchSuggestionBuffer(paramDataHolder);
    }
  }
  
  private static final class LoadGamesResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements GamesMetadata.LoadGamesResult
  {
    private final GameBuffer c;
    
    LoadGamesResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.c = new GameBuffer(paramDataHolder);
    }
  }
  
  private static final class LoadInvitationsResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Invitations.LoadInvitationsResult
  {
    private final InvitationBuffer c;
    
    LoadInvitationsResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.c = new InvitationBuffer(paramDataHolder);
    }
  }
  
  private static final class LoadMatchResultImpl
    extends GamesClientImpl.TurnBasedMatchResult
    implements TurnBasedMultiplayer.LoadMatchResult
  {
    LoadMatchResultImpl(DataHolder paramDataHolder)
    {
      super();
    }
  }
  
  private static final class LoadMatchesResultImpl
    implements TurnBasedMultiplayer.LoadMatchesResult
  {
    private final Status a;
    private final LoadMatchesResponse b;
    
    LoadMatchesResultImpl(Status paramStatus, Bundle paramBundle)
    {
      this.a = paramStatus;
      this.b = new LoadMatchesResponse(paramBundle);
    }
    
    public Status getStatus()
    {
      return this.a;
    }
    
    public void release()
    {
      this.b.a();
    }
  }
  
  private static final class LoadPlayerScoreResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Leaderboards.LoadPlayerScoreResult
  {
    private final LeaderboardScoreEntity c;
    
    /* Error */
    LoadPlayerScoreResultImpl(DataHolder paramDataHolder)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_1
      //   2: invokespecial 15	com/google/android/gms/games/internal/GamesClientImpl$GamesDataHolderResult:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: new 17	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer
      //   8: dup
      //   9: aload_1
      //   10: invokespecial 18	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   13: astore_1
      //   14: aload_1
      //   15: invokevirtual 22	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer:a	()I
      //   18: ifle +25 -> 43
      //   21: aload_0
      //   22: aload_1
      //   23: iconst_0
      //   24: invokevirtual 26	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer:b	(I)Lcom/google/android/gms/games/leaderboard/LeaderboardScore;
      //   27: invokeinterface 31 1 0
      //   32: checkcast 33	com/google/android/gms/games/leaderboard/LeaderboardScoreEntity
      //   35: putfield 35	com/google/android/gms/games/internal/GamesClientImpl$LoadPlayerScoreResultImpl:c	Lcom/google/android/gms/games/leaderboard/LeaderboardScoreEntity;
      //   38: aload_1
      //   39: invokevirtual 39	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer:release	()V
      //   42: return
      //   43: aload_0
      //   44: aconst_null
      //   45: putfield 35	com/google/android/gms/games/internal/GamesClientImpl$LoadPlayerScoreResultImpl:c	Lcom/google/android/gms/games/leaderboard/LeaderboardScoreEntity;
      //   48: goto -10 -> 38
      //   51: astore_2
      //   52: aload_1
      //   53: invokevirtual 39	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer:release	()V
      //   56: aload_2
      //   57: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	58	0	this	LoadPlayerScoreResultImpl
      //   0	58	1	paramDataHolder	DataHolder
      //   51	6	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   14	38	51	finally
      //   43	48	51	finally
    }
  }
  
  private static final class LoadPlayerStatsResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Stats.LoadPlayerStatsResult
  {
    private final PlayerStats c;
    
    /* Error */
    LoadPlayerStatsResultImpl(DataHolder paramDataHolder)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_1
      //   2: invokespecial 15	com/google/android/gms/games/internal/GamesClientImpl$GamesDataHolderResult:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: new 17	com/google/android/gms/games/stats/PlayerStatsBuffer
      //   8: dup
      //   9: aload_1
      //   10: invokespecial 18	com/google/android/gms/games/stats/PlayerStatsBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   13: astore_1
      //   14: aload_1
      //   15: invokevirtual 22	com/google/android/gms/games/stats/PlayerStatsBuffer:a	()I
      //   18: ifle +24 -> 42
      //   21: aload_0
      //   22: new 24	com/google/android/gms/games/stats/PlayerStatsEntity
      //   25: dup
      //   26: aload_1
      //   27: iconst_0
      //   28: invokevirtual 28	com/google/android/gms/games/stats/PlayerStatsBuffer:b	(I)Lcom/google/android/gms/games/stats/PlayerStats;
      //   31: invokespecial 31	com/google/android/gms/games/stats/PlayerStatsEntity:<init>	(Lcom/google/android/gms/games/stats/PlayerStats;)V
      //   34: putfield 33	com/google/android/gms/games/internal/GamesClientImpl$LoadPlayerStatsResultImpl:c	Lcom/google/android/gms/games/stats/PlayerStats;
      //   37: aload_1
      //   38: invokevirtual 37	com/google/android/gms/games/stats/PlayerStatsBuffer:release	()V
      //   41: return
      //   42: aload_0
      //   43: aconst_null
      //   44: putfield 33	com/google/android/gms/games/internal/GamesClientImpl$LoadPlayerStatsResultImpl:c	Lcom/google/android/gms/games/stats/PlayerStats;
      //   47: goto -10 -> 37
      //   50: astore_2
      //   51: aload_1
      //   52: invokevirtual 37	com/google/android/gms/games/stats/PlayerStatsBuffer:release	()V
      //   55: aload_2
      //   56: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	57	0	this	LoadPlayerStatsResultImpl
      //   0	57	1	paramDataHolder	DataHolder
      //   50	6	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   14	37	50	finally
      //   42	47	50	finally
    }
  }
  
  private static final class LoadPlayersResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Players.LoadPlayersResult
  {
    private final PlayerBuffer c;
    
    LoadPlayersResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.c = new PlayerBuffer(paramDataHolder);
    }
  }
  
  private static final class LoadProfileSettingsResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Players.LoadProfileSettingsResult
  {
    private final boolean c;
    private final boolean d;
    
    /* Error */
    LoadProfileSettingsResultImpl(DataHolder paramDataHolder)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_1
      //   2: invokespecial 16	com/google/android/gms/games/internal/GamesClientImpl$GamesDataHolderResult:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: aload_1
      //   6: invokevirtual 22	com/google/android/gms/common/data/DataHolder:g	()I
      //   9: ifle +38 -> 47
      //   12: aload_1
      //   13: iconst_0
      //   14: invokevirtual 26	com/google/android/gms/common/data/DataHolder:a	(I)I
      //   17: istore_2
      //   18: aload_0
      //   19: aload_1
      //   20: ldc 28
      //   22: iconst_0
      //   23: iload_2
      //   24: invokevirtual 31	com/google/android/gms/common/data/DataHolder:d	(Ljava/lang/String;II)Z
      //   27: putfield 33	com/google/android/gms/games/internal/GamesClientImpl$LoadProfileSettingsResultImpl:c	Z
      //   30: aload_0
      //   31: aload_1
      //   32: ldc 35
      //   34: iconst_0
      //   35: iload_2
      //   36: invokevirtual 31	com/google/android/gms/common/data/DataHolder:d	(Ljava/lang/String;II)Z
      //   39: putfield 37	com/google/android/gms/games/internal/GamesClientImpl$LoadProfileSettingsResultImpl:d	Z
      //   42: aload_1
      //   43: invokevirtual 41	com/google/android/gms/common/data/DataHolder:i	()V
      //   46: return
      //   47: aload_0
      //   48: iconst_1
      //   49: putfield 33	com/google/android/gms/games/internal/GamesClientImpl$LoadProfileSettingsResultImpl:c	Z
      //   52: aload_0
      //   53: iconst_0
      //   54: putfield 37	com/google/android/gms/games/internal/GamesClientImpl$LoadProfileSettingsResultImpl:d	Z
      //   57: goto -15 -> 42
      //   60: astore_3
      //   61: aload_1
      //   62: invokevirtual 41	com/google/android/gms/common/data/DataHolder:i	()V
      //   65: aload_3
      //   66: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	67	0	this	LoadProfileSettingsResultImpl
      //   0	67	1	paramDataHolder	DataHolder
      //   17	19	2	i	int
      //   60	6	3	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   5	42	60	finally
      //   47	57	60	finally
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
  
  private static final class LoadQuestsResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Quests.LoadQuestsResult
  {
    private final DataHolder c;
    
    LoadQuestsResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.c = paramDataHolder;
    }
  }
  
  private static final class LoadRequestSummariesResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Requests.LoadRequestSummariesResult
  {
    LoadRequestSummariesResultImpl(DataHolder paramDataHolder)
    {
      super();
    }
  }
  
  private static final class LoadRequestsResultImpl
    implements Requests.LoadRequestsResult
  {
    private final Status a;
    private final Bundle b;
    
    LoadRequestsResultImpl(Status paramStatus, Bundle paramBundle)
    {
      this.a = paramStatus;
      this.b = paramBundle;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
    
    public void release()
    {
      Iterator localIterator = this.b.keySet().iterator();
      while (localIterator.hasNext())
      {
        Object localObject = (String)localIterator.next();
        localObject = (DataHolder)this.b.getParcelable((String)localObject);
        if (localObject != null) {
          ((DataHolder)localObject).i();
        }
      }
    }
  }
  
  private static final class LoadScoresResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Leaderboards.LoadScoresResult
  {
    private final LeaderboardEntity c;
    private final LeaderboardScoreBuffer d;
    
    /* Error */
    LoadScoresResultImpl(DataHolder paramDataHolder1, DataHolder paramDataHolder2)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_2
      //   2: invokespecial 18	com/google/android/gms/games/internal/GamesClientImpl$GamesDataHolderResult:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: new 20	com/google/android/gms/games/leaderboard/LeaderboardBuffer
      //   8: dup
      //   9: aload_1
      //   10: invokespecial 21	com/google/android/gms/games/leaderboard/LeaderboardBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   13: astore_1
      //   14: aload_1
      //   15: invokevirtual 25	com/google/android/gms/games/leaderboard/LeaderboardBuffer:a	()I
      //   18: ifle +40 -> 58
      //   21: aload_0
      //   22: aload_1
      //   23: iconst_0
      //   24: invokevirtual 28	com/google/android/gms/games/leaderboard/LeaderboardBuffer:a	(I)Ljava/lang/Object;
      //   27: checkcast 30	com/google/android/gms/games/leaderboard/Leaderboard
      //   30: invokeinterface 33 1 0
      //   35: checkcast 35	com/google/android/gms/games/leaderboard/LeaderboardEntity
      //   38: putfield 37	com/google/android/gms/games/internal/GamesClientImpl$LoadScoresResultImpl:c	Lcom/google/android/gms/games/leaderboard/LeaderboardEntity;
      //   41: aload_1
      //   42: invokevirtual 41	com/google/android/gms/games/leaderboard/LeaderboardBuffer:release	()V
      //   45: aload_0
      //   46: new 43	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer
      //   49: dup
      //   50: aload_2
      //   51: invokespecial 44	com/google/android/gms/games/leaderboard/LeaderboardScoreBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   54: putfield 46	com/google/android/gms/games/internal/GamesClientImpl$LoadScoresResultImpl:d	Lcom/google/android/gms/games/leaderboard/LeaderboardScoreBuffer;
      //   57: return
      //   58: aload_0
      //   59: aconst_null
      //   60: putfield 37	com/google/android/gms/games/internal/GamesClientImpl$LoadScoresResultImpl:c	Lcom/google/android/gms/games/leaderboard/LeaderboardEntity;
      //   63: goto -22 -> 41
      //   66: astore_2
      //   67: aload_1
      //   68: invokevirtual 41	com/google/android/gms/games/leaderboard/LeaderboardBuffer:release	()V
      //   71: aload_2
      //   72: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	73	0	this	LoadScoresResultImpl
      //   0	73	1	paramDataHolder1	DataHolder
      //   0	73	2	paramDataHolder2	DataHolder
      // Exception table:
      //   from	to	target	type
      //   14	41	66	finally
      //   58	63	66	finally
    }
  }
  
  private static final class LoadSnapshotsResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Snapshots.LoadSnapshotsResult
  {
    LoadSnapshotsResultImpl(DataHolder paramDataHolder)
    {
      super();
    }
  }
  
  private static final class LoadXpForGameCategoriesResultImpl
    implements Players.LoadXpForGameCategoriesResult
  {
    private final Status a;
    private final List<String> b;
    private final Bundle c;
    
    LoadXpForGameCategoriesResultImpl(Status paramStatus, Bundle paramBundle)
    {
      this.a = paramStatus;
      this.b = paramBundle.getStringArrayList("game_category_list");
      this.c = paramBundle;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
  
  private static final class LoadXpStreamResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Players.LoadXpStreamResult
  {
    private final ExperienceEventBuffer c;
    
    LoadXpStreamResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.c = new ExperienceEventBuffer(paramDataHolder);
    }
  }
  
  private static final class MatchRemovedNotifier
    implements zzq.zzb<OnTurnBasedMatchUpdateReceivedListener>
  {
    private final String a;
    
    MatchRemovedNotifier(String paramString)
    {
      this.a = paramString;
    }
    
    public void a() {}
    
    public void a(OnTurnBasedMatchUpdateReceivedListener paramOnTurnBasedMatchUpdateReceivedListener)
    {
      paramOnTurnBasedMatchUpdateReceivedListener.a(this.a);
    }
  }
  
  private static final class MatchUpdateReceivedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzq<OnTurnBasedMatchUpdateReceivedListener> a;
    
    public void c(String paramString)
    {
      this.a.a(new GamesClientImpl.MatchRemovedNotifier(paramString));
    }
    
    public void r(DataHolder paramDataHolder)
    {
      TurnBasedMatchBuffer localTurnBasedMatchBuffer = new TurnBasedMatchBuffer(paramDataHolder);
      paramDataHolder = null;
      try
      {
        if (localTurnBasedMatchBuffer.a() > 0) {
          paramDataHolder = (TurnBasedMatch)((TurnBasedMatch)localTurnBasedMatchBuffer.a(0)).a();
        }
        localTurnBasedMatchBuffer.release();
        if (paramDataHolder != null) {
          this.a.a(new GamesClientImpl.MatchUpdateReceivedNotifier(paramDataHolder));
        }
        return;
      }
      finally
      {
        localTurnBasedMatchBuffer.release();
      }
    }
  }
  
  private static final class MatchUpdateReceivedNotifier
    implements zzq.zzb<OnTurnBasedMatchUpdateReceivedListener>
  {
    private final TurnBasedMatch a;
    
    MatchUpdateReceivedNotifier(TurnBasedMatch paramTurnBasedMatch)
    {
      this.a = paramTurnBasedMatch;
    }
    
    public void a() {}
    
    public void a(OnTurnBasedMatchUpdateReceivedListener paramOnTurnBasedMatchUpdateReceivedListener)
    {
      paramOnTurnBasedMatchUpdateReceivedListener.a(this.a);
    }
  }
  
  private static final class MessageReceivedNotifier
    implements zzq.zzb<RealTimeMessageReceivedListener>
  {
    private final RealTimeMessage a;
    
    MessageReceivedNotifier(RealTimeMessage paramRealTimeMessage)
    {
      this.a = paramRealTimeMessage;
    }
    
    public void a() {}
    
    public void a(RealTimeMessageReceivedListener paramRealTimeMessageReceivedListener)
    {
      paramRealTimeMessageReceivedListener.a(this.a);
    }
  }
  
  private static final class NearbyPlayerDetectedNotifier
    implements zzq.zzb<OnNearbyPlayerDetectedListener>
  {
    private final Player a;
    
    public void a() {}
    
    public void a(OnNearbyPlayerDetectedListener paramOnNearbyPlayerDetectedListener)
    {
      paramOnNearbyPlayerDetectedListener.a(this.a);
    }
  }
  
  private static final class NotifyAclLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Acls.LoadAclResult> a;
    
    NotifyAclLoadedBinderCallback(zza.zzb<Acls.LoadAclResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void A(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.LoadAclResultImpl(paramDataHolder));
    }
  }
  
  private static final class NotifyAclUpdatedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Status> a;
    
    NotifyAclUpdatedBinderCallback(zza.zzb<Status> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void a(int paramInt)
    {
      this.a.a(GamesStatusCodes.a(paramInt));
    }
  }
  
  private static final class OpenSnapshotResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Snapshots.OpenSnapshotResult
  {
    private final Snapshot c;
    private final String d;
    private final Snapshot e;
    private final Contents f;
    private final SnapshotContents g;
    
    OpenSnapshotResultImpl(DataHolder paramDataHolder, Contents paramContents)
    {
      this(paramDataHolder, null, paramContents, null, null);
    }
    
    OpenSnapshotResultImpl(DataHolder paramDataHolder, String paramString, Contents paramContents1, Contents paramContents2, Contents paramContents3)
    {
      super();
      SnapshotMetadataBuffer localSnapshotMetadataBuffer = new SnapshotMetadataBuffer(paramDataHolder);
      for (;;)
      {
        try
        {
          if (localSnapshotMetadataBuffer.a() == 0)
          {
            this.c = null;
            this.e = null;
            localSnapshotMetadataBuffer.release();
            this.d = paramString;
            this.f = paramContents3;
            this.g = new SnapshotContentsEntity(paramContents3);
            return;
          }
          if (localSnapshotMetadataBuffer.a() != 1) {
            break label144;
          }
          if (paramDataHolder.e() != 4004)
          {
            zzb.a(bool);
            this.c = new SnapshotEntity(new SnapshotMetadataEntity(localSnapshotMetadataBuffer.b(0)), new SnapshotContentsEntity(paramContents1));
            this.e = null;
            continue;
          }
          bool = false;
        }
        finally
        {
          localSnapshotMetadataBuffer.release();
        }
        continue;
        label144:
        this.c = new SnapshotEntity(new SnapshotMetadataEntity(localSnapshotMetadataBuffer.b(0)), new SnapshotContentsEntity(paramContents1));
        this.e = new SnapshotEntity(new SnapshotMetadataEntity(localSnapshotMetadataBuffer.b(1)), new SnapshotContentsEntity(paramContents2));
      }
    }
  }
  
  private static final class P2PConnectedNotifier
    implements zzq.zzb<RoomStatusUpdateListener>
  {
    private final String a;
    
    P2PConnectedNotifier(String paramString)
    {
      this.a = paramString;
    }
    
    public void a() {}
    
    public void a(RoomStatusUpdateListener paramRoomStatusUpdateListener)
    {
      paramRoomStatusUpdateListener.a(this.a);
    }
  }
  
  private static final class P2PDisconnectedNotifier
    implements zzq.zzb<RoomStatusUpdateListener>
  {
    private final String a;
    
    P2PDisconnectedNotifier(String paramString)
    {
      this.a = paramString;
    }
    
    public void a() {}
    
    public void a(RoomStatusUpdateListener paramRoomStatusUpdateListener)
    {
      paramRoomStatusUpdateListener.b(this.a);
    }
  }
  
  private static final class PeerConnectedNotifier
    extends GamesClientImpl.AbstractPeerStatusNotifier
  {
    PeerConnectedNotifier(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      super(paramArrayOfString);
    }
    
    protected void a(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom, ArrayList<String> paramArrayList)
    {
      paramRoomStatusUpdateListener.e(paramRoom, paramArrayList);
    }
  }
  
  private static final class PeerDeclinedNotifier
    extends GamesClientImpl.AbstractPeerStatusNotifier
  {
    PeerDeclinedNotifier(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      super(paramArrayOfString);
    }
    
    protected void a(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom, ArrayList<String> paramArrayList)
    {
      paramRoomStatusUpdateListener.b(paramRoom, paramArrayList);
    }
  }
  
  private static final class PeerDisconnectedNotifier
    extends GamesClientImpl.AbstractPeerStatusNotifier
  {
    PeerDisconnectedNotifier(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      super(paramArrayOfString);
    }
    
    protected void a(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom, ArrayList<String> paramArrayList)
    {
      paramRoomStatusUpdateListener.f(paramRoom, paramArrayList);
    }
  }
  
  private static final class PeerInvitedToRoomNotifier
    extends GamesClientImpl.AbstractPeerStatusNotifier
  {
    PeerInvitedToRoomNotifier(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      super(paramArrayOfString);
    }
    
    protected void a(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom, ArrayList<String> paramArrayList)
    {
      paramRoomStatusUpdateListener.a(paramRoom, paramArrayList);
    }
  }
  
  private static final class PeerJoinedRoomNotifier
    extends GamesClientImpl.AbstractPeerStatusNotifier
  {
    PeerJoinedRoomNotifier(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      super(paramArrayOfString);
    }
    
    protected void a(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom, ArrayList<String> paramArrayList)
    {
      paramRoomStatusUpdateListener.c(paramRoom, paramArrayList);
    }
  }
  
  private static final class PeerLeftRoomNotifier
    extends GamesClientImpl.AbstractPeerStatusNotifier
  {
    PeerLeftRoomNotifier(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      super(paramArrayOfString);
    }
    
    protected void a(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom, ArrayList<String> paramArrayList)
    {
      paramRoomStatusUpdateListener.d(paramRoom, paramArrayList);
    }
  }
  
  private static final class PlayerLeaderboardScoreLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Leaderboards.LoadPlayerScoreResult> a;
    
    PlayerLeaderboardScoreLoadedBinderCallback(zza.zzb<Leaderboards.LoadPlayerScoreResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void C(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.LoadPlayerScoreResultImpl(paramDataHolder));
    }
  }
  
  private static final class PlayerStatsLoadedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Stats.LoadPlayerStatsResult> a;
    
    public PlayerStatsLoadedBinderCallbacks(zza.zzb<Stats.LoadPlayerStatsResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void P(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.LoadPlayerStatsResultImpl(paramDataHolder));
    }
  }
  
  private static final class PlayerXpForGameCategoriesLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Players.LoadXpForGameCategoriesResult> a;
    
    PlayerXpForGameCategoriesLoadedBinderCallback(zza.zzb<Players.LoadXpForGameCategoriesResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void d(int paramInt, Bundle paramBundle)
    {
      paramBundle.setClassLoader(getClass().getClassLoader());
      Status localStatus = GamesStatusCodes.a(paramInt);
      this.a.a(new GamesClientImpl.LoadXpForGameCategoriesResultImpl(localStatus, paramBundle));
    }
  }
  
  static final class PlayerXpStreamLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Players.LoadXpStreamResult> a;
    
    PlayerXpStreamLoadedBinderCallback(zza.zzb<Players.LoadXpStreamResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void N(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.LoadXpStreamResultImpl(paramDataHolder));
    }
  }
  
  private static final class PlayersLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Players.LoadPlayersResult> a;
    
    PlayersLoadedBinderCallback(zza.zzb<Players.LoadPlayersResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void e(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.LoadPlayersResultImpl(paramDataHolder));
    }
    
    public void f(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.LoadPlayersResultImpl(paramDataHolder));
    }
  }
  
  private static final class PopupLocationInfoBinderCallbacks
    extends AbstractGamesClient
  {
    private final PopupManager a;
    
    public PopupLocationInfoBinderCallbacks(PopupManager paramPopupManager)
    {
      this.a = paramPopupManager;
    }
    
    public PopupLocationInfoParcelable a()
    {
      return new PopupLocationInfoParcelable(this.a.d());
    }
  }
  
  static final class ProfileSettingsLoadedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Players.LoadProfileSettingsResult> a;
    
    ProfileSettingsLoadedBinderCallback(zza.zzb<Players.LoadProfileSettingsResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void O(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.LoadProfileSettingsResultImpl(paramDataHolder));
    }
  }
  
  private static final class ProfileSettingsUpdatedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Status> a;
    
    ProfileSettingsUpdatedBinderCallback(zza.zzb<Status> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void c(int paramInt)
    {
      this.a.a(GamesStatusCodes.a(paramInt));
    }
  }
  
  private static final class QuestAcceptedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Quests.AcceptQuestResult> a;
    
    public QuestAcceptedBinderCallbacks(zza.zzb<Quests.AcceptQuestResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void J(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.AcceptQuestResultImpl(paramDataHolder));
    }
  }
  
  private static final class QuestCompletedNotifier
    implements zzq.zzb<QuestUpdateListener>
  {
    private final Quest a;
    
    QuestCompletedNotifier(Quest paramQuest)
    {
      this.a = paramQuest;
    }
    
    public void a() {}
    
    public void a(QuestUpdateListener paramQuestUpdateListener)
    {
      paramQuestUpdateListener.a(this.a);
    }
  }
  
  private static final class QuestMilestoneClaimBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Quests.ClaimMilestoneResult> a;
    private final String b;
    
    public QuestMilestoneClaimBinderCallbacks(zza.zzb<Quests.ClaimMilestoneResult> paramzzb, String paramString)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
      this.b = ((String)zzx.a(paramString, "MilestoneId must not be null"));
    }
    
    public void I(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.ClaimMilestoneResultImpl(paramDataHolder, this.b));
    }
  }
  
  private static final class QuestUpdateBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzq<QuestUpdateListener> a;
    
    private Quest R(DataHolder paramDataHolder)
    {
      QuestBuffer localQuestBuffer = new QuestBuffer(paramDataHolder);
      paramDataHolder = null;
      try
      {
        if (localQuestBuffer.a() > 0) {
          paramDataHolder = (Quest)((Quest)localQuestBuffer.a(0)).a();
        }
        return paramDataHolder;
      }
      finally
      {
        localQuestBuffer.release();
      }
    }
    
    public void K(DataHolder paramDataHolder)
    {
      paramDataHolder = R(paramDataHolder);
      if (paramDataHolder != null) {
        this.a.a(new GamesClientImpl.QuestCompletedNotifier(paramDataHolder));
      }
    }
  }
  
  private static final class QuestsLoadedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Quests.LoadQuestsResult> a;
    
    public QuestsLoadedBinderCallbacks(zza.zzb<Quests.LoadQuestsResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void M(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.LoadQuestsResultImpl(paramDataHolder));
    }
  }
  
  private static final class RealTimeMessageSentNotifier
    implements zzq.zzb<RealTimeMultiplayer.ReliableMessageSentCallback>
  {
    private final int a;
    private final String b;
    private final int c;
    
    RealTimeMessageSentNotifier(int paramInt1, int paramInt2, String paramString)
    {
      this.a = paramInt1;
      this.c = paramInt2;
      this.b = paramString;
    }
    
    public void a() {}
    
    public void a(RealTimeMultiplayer.ReliableMessageSentCallback paramReliableMessageSentCallback)
    {
      if (paramReliableMessageSentCallback != null) {
        paramReliableMessageSentCallback.a(this.a, this.c, this.b);
      }
    }
  }
  
  private static final class RealTimeReliableMessageBinderCallbacks
    extends AbstractGamesCallbacks
  {
    final zzq<RealTimeMultiplayer.ReliableMessageSentCallback> a;
    
    public void a(int paramInt1, int paramInt2, String paramString)
    {
      if (this.a != null) {
        this.a.a(new GamesClientImpl.RealTimeMessageSentNotifier(paramInt1, paramInt2, paramString));
      }
    }
  }
  
  private static final class RequestReceivedBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zzq<OnRequestReceivedListener> a;
    
    public void b(String paramString)
    {
      this.a.a(new GamesClientImpl.RequestRemovedNotifier(paramString));
    }
    
    public void m(DataHolder paramDataHolder)
    {
      GameRequestBuffer localGameRequestBuffer = new GameRequestBuffer(paramDataHolder);
      paramDataHolder = null;
      try
      {
        if (localGameRequestBuffer.a() > 0) {
          paramDataHolder = (GameRequest)((GameRequest)localGameRequestBuffer.a(0)).a();
        }
        localGameRequestBuffer.release();
        if (paramDataHolder != null) {
          this.a.a(new GamesClientImpl.RequestReceivedNotifier(paramDataHolder));
        }
        return;
      }
      finally
      {
        localGameRequestBuffer.release();
      }
    }
  }
  
  private static final class RequestReceivedNotifier
    implements zzq.zzb<OnRequestReceivedListener>
  {
    private final GameRequest a;
    
    RequestReceivedNotifier(GameRequest paramGameRequest)
    {
      this.a = paramGameRequest;
    }
    
    public void a() {}
    
    public void a(OnRequestReceivedListener paramOnRequestReceivedListener)
    {
      paramOnRequestReceivedListener.a(this.a);
    }
  }
  
  private static final class RequestRemovedNotifier
    implements zzq.zzb<OnRequestReceivedListener>
  {
    private final String a;
    
    RequestRemovedNotifier(String paramString)
    {
      this.a = paramString;
    }
    
    public void a() {}
    
    public void a(OnRequestReceivedListener paramOnRequestReceivedListener)
    {
      paramOnRequestReceivedListener.a(this.a);
    }
  }
  
  private static final class RequestSentBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Requests.SendRequestResult> a;
    
    public RequestSentBinderCallbacks(zza.zzb<Requests.SendRequestResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void E(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.SendRequestResultImpl(paramDataHolder));
    }
  }
  
  private static final class RequestSummariesLoadedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Requests.LoadRequestSummariesResult> a;
    
    public RequestSummariesLoadedBinderCallbacks(zza.zzb<Requests.LoadRequestSummariesResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void F(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.LoadRequestSummariesResultImpl(paramDataHolder));
    }
  }
  
  private static final class RequestsLoadedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Requests.LoadRequestsResult> a;
    
    public RequestsLoadedBinderCallbacks(zza.zzb<Requests.LoadRequestsResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void b(int paramInt, Bundle paramBundle)
    {
      paramBundle.setClassLoader(getClass().getClassLoader());
      Status localStatus = GamesStatusCodes.a(paramInt);
      this.a.a(new GamesClientImpl.LoadRequestsResultImpl(localStatus, paramBundle));
    }
  }
  
  private static final class RequestsUpdatedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Requests.UpdateRequestsResult> a;
    
    public RequestsUpdatedBinderCallbacks(zza.zzb<Requests.UpdateRequestsResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void D(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.UpdateRequestsResultImpl(paramDataHolder));
    }
  }
  
  private static final class RoomAutoMatchingNotifier
    extends GamesClientImpl.AbstractRoomStatusNotifier
  {
    RoomAutoMatchingNotifier(DataHolder paramDataHolder)
    {
      super();
    }
    
    public void a(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom)
    {
      paramRoomStatusUpdateListener.b(paramRoom);
    }
  }
  
  private static final class RoomBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zzq<? extends RoomUpdateListener> a;
    private final zzq<? extends RoomStatusUpdateListener> b;
    private final zzq<RealTimeMessageReceivedListener> c;
    
    public void a(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      if (this.b != null) {
        this.b.a(new GamesClientImpl.PeerInvitedToRoomNotifier(paramDataHolder, paramArrayOfString));
      }
    }
    
    public void a(RealTimeMessage paramRealTimeMessage)
    {
      if (this.c != null) {
        this.c.a(new GamesClientImpl.MessageReceivedNotifier(paramRealTimeMessage));
      }
    }
    
    public void b(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      if (this.b != null) {
        this.b.a(new GamesClientImpl.PeerJoinedRoomNotifier(paramDataHolder, paramArrayOfString));
      }
    }
    
    public void c(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      if (this.b != null) {
        this.b.a(new GamesClientImpl.PeerLeftRoomNotifier(paramDataHolder, paramArrayOfString));
      }
    }
    
    public void d(int paramInt, String paramString)
    {
      this.a.a(new GamesClientImpl.LeftRoomNotifier(paramInt, paramString));
    }
    
    public void d(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      if (this.b != null) {
        this.b.a(new GamesClientImpl.PeerDeclinedNotifier(paramDataHolder, paramArrayOfString));
      }
    }
    
    public void d(String paramString)
    {
      if (this.b != null) {
        this.b.a(new GamesClientImpl.P2PConnectedNotifier(paramString));
      }
    }
    
    public void e(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      if (this.b != null) {
        this.b.a(new GamesClientImpl.PeerConnectedNotifier(paramDataHolder, paramArrayOfString));
      }
    }
    
    public void e(String paramString)
    {
      if (this.b != null) {
        this.b.a(new GamesClientImpl.P2PDisconnectedNotifier(paramString));
      }
    }
    
    public void f(DataHolder paramDataHolder, String[] paramArrayOfString)
    {
      if (this.b != null) {
        this.b.a(new GamesClientImpl.PeerDisconnectedNotifier(paramDataHolder, paramArrayOfString));
      }
    }
    
    public void s(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.RoomCreatedNotifier(paramDataHolder));
    }
    
    public void t(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.JoinedRoomNotifier(paramDataHolder));
    }
    
    public void u(DataHolder paramDataHolder)
    {
      if (this.b != null) {
        this.b.a(new GamesClientImpl.RoomConnectingNotifier(paramDataHolder));
      }
    }
    
    public void v(DataHolder paramDataHolder)
    {
      if (this.b != null) {
        this.b.a(new GamesClientImpl.RoomAutoMatchingNotifier(paramDataHolder));
      }
    }
    
    public void w(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.RoomConnectedNotifier(paramDataHolder));
    }
    
    public void x(DataHolder paramDataHolder)
    {
      if (this.b != null) {
        this.b.a(new GamesClientImpl.ConnectedToRoomNotifier(paramDataHolder));
      }
    }
    
    public void y(DataHolder paramDataHolder)
    {
      if (this.b != null) {
        this.b.a(new GamesClientImpl.DisconnectedFromRoomNotifier(paramDataHolder));
      }
    }
  }
  
  private static final class RoomConnectedNotifier
    extends GamesClientImpl.AbstractRoomNotifier
  {
    RoomConnectedNotifier(DataHolder paramDataHolder)
    {
      super();
    }
    
    public void a(RoomUpdateListener paramRoomUpdateListener, Room paramRoom, int paramInt)
    {
      paramRoomUpdateListener.c(paramInt, paramRoom);
    }
  }
  
  private static final class RoomConnectingNotifier
    extends GamesClientImpl.AbstractRoomStatusNotifier
  {
    RoomConnectingNotifier(DataHolder paramDataHolder)
    {
      super();
    }
    
    public void a(RoomStatusUpdateListener paramRoomStatusUpdateListener, Room paramRoom)
    {
      paramRoomStatusUpdateListener.a(paramRoom);
    }
  }
  
  private static final class RoomCreatedNotifier
    extends GamesClientImpl.AbstractRoomNotifier
  {
    public RoomCreatedNotifier(DataHolder paramDataHolder)
    {
      super();
    }
    
    public void a(RoomUpdateListener paramRoomUpdateListener, Room paramRoom, int paramInt)
    {
      paramRoomUpdateListener.a(paramInt, paramRoom);
    }
  }
  
  private static final class SendRequestResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Requests.SendRequestResult
  {
    private final GameRequest c;
    
    /* Error */
    SendRequestResultImpl(DataHolder paramDataHolder)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_1
      //   2: invokespecial 15	com/google/android/gms/games/internal/GamesClientImpl$GamesDataHolderResult:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: new 17	com/google/android/gms/games/request/GameRequestBuffer
      //   8: dup
      //   9: aload_1
      //   10: invokespecial 18	com/google/android/gms/games/request/GameRequestBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   13: astore_1
      //   14: aload_1
      //   15: invokevirtual 22	com/google/android/gms/games/request/GameRequestBuffer:a	()I
      //   18: ifle +28 -> 46
      //   21: aload_0
      //   22: aload_1
      //   23: iconst_0
      //   24: invokevirtual 25	com/google/android/gms/games/request/GameRequestBuffer:a	(I)Ljava/lang/Object;
      //   27: checkcast 27	com/google/android/gms/games/request/GameRequest
      //   30: invokeinterface 30 1 0
      //   35: checkcast 27	com/google/android/gms/games/request/GameRequest
      //   38: putfield 32	com/google/android/gms/games/internal/GamesClientImpl$SendRequestResultImpl:c	Lcom/google/android/gms/games/request/GameRequest;
      //   41: aload_1
      //   42: invokevirtual 36	com/google/android/gms/games/request/GameRequestBuffer:release	()V
      //   45: return
      //   46: aload_0
      //   47: aconst_null
      //   48: putfield 32	com/google/android/gms/games/internal/GamesClientImpl$SendRequestResultImpl:c	Lcom/google/android/gms/games/request/GameRequest;
      //   51: goto -10 -> 41
      //   54: astore_2
      //   55: aload_1
      //   56: invokevirtual 36	com/google/android/gms/games/request/GameRequestBuffer:release	()V
      //   59: aload_2
      //   60: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	61	0	this	SendRequestResultImpl
      //   0	61	1	paramDataHolder	DataHolder
      //   54	6	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   14	41	54	finally
      //   46	51	54	finally
    }
  }
  
  private static final class SignOutCompleteBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Status> a;
    
    public SignOutCompleteBinderCallbacks(zza.zzb<Status> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void a()
    {
      Status localStatus = GamesStatusCodes.a(0);
      this.a.a(localStatus);
    }
  }
  
  private static final class SnapshotCommittedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Snapshots.CommitSnapshotResult> a;
    
    public SnapshotCommittedBinderCallbacks(zza.zzb<Snapshots.CommitSnapshotResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void H(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.CommitSnapshotResultImpl(paramDataHolder));
    }
  }
  
  static final class SnapshotDeletedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Snapshots.DeleteSnapshotResult> a;
    
    public SnapshotDeletedBinderCallbacks(zza.zzb<Snapshots.DeleteSnapshotResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void e(int paramInt, String paramString)
    {
      this.a.a(new GamesClientImpl.DeleteSnapshotResultImpl(paramInt, paramString));
    }
  }
  
  private static final class SnapshotOpenedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Snapshots.OpenSnapshotResult> a;
    
    public SnapshotOpenedBinderCallbacks(zza.zzb<Snapshots.OpenSnapshotResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void a(DataHolder paramDataHolder, Contents paramContents)
    {
      this.a.a(new GamesClientImpl.OpenSnapshotResultImpl(paramDataHolder, paramContents));
    }
    
    public void a(DataHolder paramDataHolder, String paramString, Contents paramContents1, Contents paramContents2, Contents paramContents3)
    {
      this.a.a(new GamesClientImpl.OpenSnapshotResultImpl(paramDataHolder, paramString, paramContents1, paramContents2, paramContents3));
    }
  }
  
  private static final class SnapshotsLoadedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Snapshots.LoadSnapshotsResult> a;
    
    public SnapshotsLoadedBinderCallbacks(zza.zzb<Snapshots.LoadSnapshotsResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void G(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.LoadSnapshotsResultImpl(paramDataHolder));
    }
  }
  
  private static final class StartRecordingBinderCallback
    extends AbstractGamesCallbacks
  {
    private final Games.BaseGamesApiMethodImpl<Status> a;
    
    StartRecordingBinderCallback(Games.BaseGamesApiMethodImpl<Status> paramBaseGamesApiMethodImpl)
    {
      this.a = ((Games.BaseGamesApiMethodImpl)zzx.a(paramBaseGamesApiMethodImpl, "Holder must not be null"));
    }
    
    public void d(int paramInt)
    {
      this.a.zza(new Status(paramInt));
    }
  }
  
  private static final class SubmitScoreBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Leaderboards.SubmitScoreResult> a;
    
    public SubmitScoreBinderCallbacks(zza.zzb<Leaderboards.SubmitScoreResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void d(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.SubmitScoreResultImpl(paramDataHolder));
    }
  }
  
  private static final class SubmitScoreResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Leaderboards.SubmitScoreResult
  {
    private final ScoreSubmissionData c;
    
    public SubmitScoreResultImpl(DataHolder paramDataHolder)
    {
      super();
      try
      {
        this.c = new ScoreSubmissionData(paramDataHolder);
        return;
      }
      finally
      {
        paramDataHolder.i();
      }
    }
  }
  
  private static final class TurnBasedMatchCanceledBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<TurnBasedMultiplayer.CancelMatchResult> a;
    
    public TurnBasedMatchCanceledBinderCallbacks(zza.zzb<TurnBasedMultiplayer.CancelMatchResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void c(int paramInt, String paramString)
    {
      Status localStatus = GamesStatusCodes.a(paramInt);
      this.a.a(new GamesClientImpl.CancelMatchResultImpl(localStatus, paramString));
    }
  }
  
  private static final class TurnBasedMatchInitiatedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<TurnBasedMultiplayer.InitiateMatchResult> a;
    
    public TurnBasedMatchInitiatedBinderCallbacks(zza.zzb<TurnBasedMultiplayer.InitiateMatchResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void o(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.InitiateMatchResultImpl(paramDataHolder));
    }
  }
  
  private static final class TurnBasedMatchLeftBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<TurnBasedMultiplayer.LeaveMatchResult> a;
    
    public TurnBasedMatchLeftBinderCallbacks(zza.zzb<TurnBasedMultiplayer.LeaveMatchResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void q(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.LeaveMatchResultImpl(paramDataHolder));
    }
  }
  
  private static final class TurnBasedMatchLoadedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<TurnBasedMultiplayer.LoadMatchResult> a;
    
    public TurnBasedMatchLoadedBinderCallbacks(zza.zzb<TurnBasedMultiplayer.LoadMatchResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void n(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.LoadMatchResultImpl(paramDataHolder));
    }
  }
  
  private static abstract class TurnBasedMatchResult
    extends GamesClientImpl.GamesDataHolderResult
  {
    final TurnBasedMatch c;
    
    /* Error */
    TurnBasedMatchResult(DataHolder paramDataHolder)
    {
      // Byte code:
      //   0: aload_0
      //   1: aload_1
      //   2: invokespecial 13	com/google/android/gms/games/internal/GamesClientImpl$GamesDataHolderResult:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   5: new 15	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchBuffer
      //   8: dup
      //   9: aload_1
      //   10: invokespecial 16	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchBuffer:<init>	(Lcom/google/android/gms/common/data/DataHolder;)V
      //   13: astore_1
      //   14: aload_1
      //   15: invokevirtual 20	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchBuffer:a	()I
      //   18: ifle +28 -> 46
      //   21: aload_0
      //   22: aload_1
      //   23: iconst_0
      //   24: invokevirtual 23	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchBuffer:a	(I)Ljava/lang/Object;
      //   27: checkcast 25	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch
      //   30: invokeinterface 28 1 0
      //   35: checkcast 25	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch
      //   38: putfield 30	com/google/android/gms/games/internal/GamesClientImpl$TurnBasedMatchResult:c	Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;
      //   41: aload_1
      //   42: invokevirtual 34	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchBuffer:release	()V
      //   45: return
      //   46: aload_0
      //   47: aconst_null
      //   48: putfield 30	com/google/android/gms/games/internal/GamesClientImpl$TurnBasedMatchResult:c	Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;
      //   51: goto -10 -> 41
      //   54: astore_2
      //   55: aload_1
      //   56: invokevirtual 34	com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchBuffer:release	()V
      //   59: aload_2
      //   60: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	61	0	this	TurnBasedMatchResult
      //   0	61	1	paramDataHolder	DataHolder
      //   54	6	2	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   14	41	54	finally
      //   46	51	54	finally
    }
  }
  
  private static final class TurnBasedMatchUpdatedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<TurnBasedMultiplayer.UpdateMatchResult> a;
    
    public TurnBasedMatchUpdatedBinderCallbacks(zza.zzb<TurnBasedMultiplayer.UpdateMatchResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void p(DataHolder paramDataHolder)
    {
      this.a.a(new GamesClientImpl.UpdateMatchResultImpl(paramDataHolder));
    }
  }
  
  private static final class TurnBasedMatchesLoadedBinderCallbacks
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<TurnBasedMultiplayer.LoadMatchesResult> a;
    
    public TurnBasedMatchesLoadedBinderCallbacks(zza.zzb<TurnBasedMultiplayer.LoadMatchesResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void a(int paramInt, Bundle paramBundle)
    {
      paramBundle.setClassLoader(getClass().getClassLoader());
      Status localStatus = GamesStatusCodes.a(paramInt);
      this.a.a(new GamesClientImpl.LoadMatchesResultImpl(localStatus, paramBundle));
    }
  }
  
  private static final class UpdateAchievementResultImpl
    implements Achievements.UpdateAchievementResult
  {
    private final Status a;
    private final String b;
    
    UpdateAchievementResultImpl(int paramInt, String paramString)
    {
      this.a = GamesStatusCodes.a(paramInt);
      this.b = paramString;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
  
  private static final class UpdateMatchResultImpl
    extends GamesClientImpl.TurnBasedMatchResult
    implements TurnBasedMultiplayer.UpdateMatchResult
  {
    UpdateMatchResultImpl(DataHolder paramDataHolder)
    {
      super();
    }
  }
  
  private static final class UpdateRequestsResultImpl
    extends GamesClientImpl.GamesDataHolderResult
    implements Requests.UpdateRequestsResult
  {
    private final RequestUpdateOutcomes c;
    
    UpdateRequestsResultImpl(DataHolder paramDataHolder)
    {
      super();
      this.c = RequestUpdateOutcomes.a(paramDataHolder);
    }
  }
  
  public static final class VideoAvailableResultImpl
    implements Videos.VideoAvailableResult
  {
    private final Status a;
    private final boolean b;
    
    VideoAvailableResultImpl(Status paramStatus, boolean paramBoolean)
    {
      this.a = paramStatus;
      this.b = paramBoolean;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
  
  public static final class VideoCapabilitiesResultImpl
    implements Videos.VideoCapabilitiesResult
  {
    private final Status a;
    private final VideoCapabilities b;
    
    VideoCapabilitiesResultImpl(Status paramStatus, VideoCapabilities paramVideoCapabilities)
    {
      this.a = paramStatus;
      this.b = paramVideoCapabilities;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
  
  private static final class VideoRecordingAvailableBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Videos.VideoAvailableResult> a;
    
    VideoRecordingAvailableBinderCallback(zza.zzb<Videos.VideoAvailableResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void a(int paramInt, boolean paramBoolean)
    {
      this.a.a(new GamesClientImpl.VideoAvailableResultImpl(new Status(paramInt), paramBoolean));
    }
  }
  
  private static final class VideoRecordingCapabilitiesBinderCallback
    extends AbstractGamesCallbacks
  {
    private final zza.zzb<Videos.VideoCapabilitiesResult> a;
    
    VideoRecordingCapabilitiesBinderCallback(zza.zzb<Videos.VideoCapabilitiesResult> paramzzb)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb, "Holder must not be null"));
    }
    
    public void a(int paramInt, VideoCapabilities paramVideoCapabilities)
    {
      this.a.a(new GamesClientImpl.VideoCapabilitiesResultImpl(new Status(paramInt), paramVideoCapabilities));
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/internal/GamesClientImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */