package com.google.android.gms.games.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class PopupLocationInfoParcelableCreator
  implements Parcelable.Creator<PopupLocationInfoParcelable>
{
  static void a(PopupLocationInfoParcelable paramPopupLocationInfoParcelable, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramPopupLocationInfoParcelable.b(), false);
    zzb.a(paramParcel, 1000, paramPopupLocationInfoParcelable.a());
    zzb.a(paramParcel, 2, paramPopupLocationInfoParcelable.c(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public PopupLocationInfoParcelable a(Parcel paramParcel)
  {
    IBinder localIBinder = null;
    int j = zza.b(paramParcel);
    int i = 0;
    Bundle localBundle = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localBundle = zza.r(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localIBinder = zza.q(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new PopupLocationInfoParcelable(i, localBundle, localIBinder);
  }
  
  public PopupLocationInfoParcelable[] a(int paramInt)
  {
    return new PopupLocationInfoParcelable[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/internal/PopupLocationInfoParcelableCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */