package com.google.android.gms.games.internal.data;

import android.net.Uri;
import android.net.Uri.Builder;

public final class GamesDataChangeUris
{
  public static final Uri a = c.buildUpon().appendPath("invitations").build();
  public static final Uri b = c.buildUpon().appendEncodedPath("players").build();
  private static final Uri c = Uri.parse("content://com.google.android.gms.games/").buildUpon().appendPath("data_change").build();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/internal/data/GamesDataChangeUris.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */