package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class MostRecentGameInfoEntityCreator
  implements Parcelable.Creator<MostRecentGameInfoEntity>
{
  static void a(MostRecentGameInfoEntity paramMostRecentGameInfoEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramMostRecentGameInfoEntity.b(), false);
    zzb.a(paramParcel, 1000, paramMostRecentGameInfoEntity.h());
    zzb.a(paramParcel, 2, paramMostRecentGameInfoEntity.c(), false);
    zzb.a(paramParcel, 3, paramMostRecentGameInfoEntity.d());
    zzb.a(paramParcel, 4, paramMostRecentGameInfoEntity.e(), paramInt, false);
    zzb.a(paramParcel, 5, paramMostRecentGameInfoEntity.f(), paramInt, false);
    zzb.a(paramParcel, 6, paramMostRecentGameInfoEntity.g(), paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public MostRecentGameInfoEntity a(Parcel paramParcel)
  {
    Uri localUri1 = null;
    int j = zza.b(paramParcel);
    int i = 0;
    long l = 0L;
    Uri localUri2 = null;
    Uri localUri3 = null;
    String str1 = null;
    String str2 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        str2 = zza.p(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str1 = zza.p(paramParcel, k);
        break;
      case 3: 
        l = zza.i(paramParcel, k);
        break;
      case 4: 
        localUri3 = (Uri)zza.a(paramParcel, k, Uri.CREATOR);
        break;
      case 5: 
        localUri2 = (Uri)zza.a(paramParcel, k, Uri.CREATOR);
        break;
      case 6: 
        localUri1 = (Uri)zza.a(paramParcel, k, Uri.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new MostRecentGameInfoEntity(i, str2, str1, l, localUri3, localUri2, localUri1);
  }
  
  public MostRecentGameInfoEntity[] a(int paramInt)
  {
    return new MostRecentGameInfoEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/internal/player/MostRecentGameInfoEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */