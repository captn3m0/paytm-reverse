package com.google.android.gms.games.internal.player;

import android.text.TextUtils;

public final class PlayerColumnNames
{
  public final String A;
  public final String B;
  public final String C;
  public final String D;
  public final String E;
  public final String F;
  public final String a;
  public final String b;
  public final String c;
  public final String d;
  public final String e;
  public final String f;
  public final String g;
  public final String h;
  public final String i;
  public final String j;
  public final String k;
  public final String l;
  public final String m;
  public final String n;
  public final String o;
  public final String p;
  public final String q;
  public final String r;
  public final String s;
  public final String t;
  public final String u;
  public final String v;
  public final String w;
  public final String x;
  public final String y;
  public final String z;
  
  public PlayerColumnNames(String paramString)
  {
    if (TextUtils.isEmpty(paramString))
    {
      this.a = "external_player_id";
      this.b = "profile_name";
      this.c = "profile_icon_image_uri";
      this.d = "profile_icon_image_url";
      this.e = "profile_hi_res_image_uri";
      this.f = "profile_hi_res_image_url";
      this.g = "last_updated";
      this.h = "is_in_circles";
      this.i = "played_with_timestamp";
      this.j = "current_xp_total";
      this.k = "current_level";
      this.l = "current_level_min_xp";
      this.m = "current_level_max_xp";
      this.n = "next_level";
      this.o = "next_level_max_xp";
      this.p = "last_level_up_timestamp";
      this.q = "player_title";
      this.r = "has_all_public_acls";
      this.s = "is_profile_visible";
      this.t = "most_recent_external_game_id";
      this.u = "most_recent_game_name";
      this.v = "most_recent_activity_timestamp";
      this.w = "most_recent_game_icon_uri";
      this.x = "most_recent_game_hi_res_uri";
      this.y = "most_recent_game_featured_uri";
      this.z = "has_debug_access";
      this.A = "gamer_tag";
      this.B = "real_name";
      this.C = "banner_image_landscape_uri";
      this.D = "banner_image_landscape_url";
      this.E = "banner_image_portrait_uri";
      this.F = "banner_image_portrait_url";
      return;
    }
    this.a = (paramString + "external_player_id");
    this.b = (paramString + "profile_name");
    this.c = (paramString + "profile_icon_image_uri");
    this.d = (paramString + "profile_icon_image_url");
    this.e = (paramString + "profile_hi_res_image_uri");
    this.f = (paramString + "profile_hi_res_image_url");
    this.g = (paramString + "last_updated");
    this.h = (paramString + "is_in_circles");
    this.i = (paramString + "played_with_timestamp");
    this.j = (paramString + "current_xp_total");
    this.k = (paramString + "current_level");
    this.l = (paramString + "current_level_min_xp");
    this.m = (paramString + "current_level_max_xp");
    this.n = (paramString + "next_level");
    this.o = (paramString + "next_level_max_xp");
    this.p = (paramString + "last_level_up_timestamp");
    this.q = (paramString + "player_title");
    this.r = (paramString + "has_all_public_acls");
    this.s = (paramString + "is_profile_visible");
    this.t = (paramString + "most_recent_external_game_id");
    this.u = (paramString + "most_recent_game_name");
    this.v = (paramString + "most_recent_activity_timestamp");
    this.w = (paramString + "most_recent_game_icon_uri");
    this.x = (paramString + "most_recent_game_hi_res_uri");
    this.y = (paramString + "most_recent_game_featured_uri");
    this.z = (paramString + "has_debug_access");
    this.A = (paramString + "gamer_tag");
    this.B = (paramString + "real_name");
    this.C = (paramString + "banner_image_landscape_uri");
    this.D = (paramString + "banner_image_landscape_url");
    this.E = (paramString + "banner_image_portrait_uri");
    this.F = (paramString + "banner_image_portrait_url");
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/internal/player/PlayerColumnNames.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */