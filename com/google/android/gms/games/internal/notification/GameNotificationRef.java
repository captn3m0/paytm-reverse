package com.google.android.gms.games.internal.notification;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;

public final class GameNotificationRef
  extends zzc
  implements GameNotification
{
  GameNotificationRef(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
  }
  
  public long a()
  {
    return b("_id");
  }
  
  public String c()
  {
    return e("notification_id");
  }
  
  public int d()
  {
    return c("type");
  }
  
  public String e()
  {
    return e("ticker");
  }
  
  public String f()
  {
    return e("title");
  }
  
  public String g()
  {
    return e("text");
  }
  
  public String h()
  {
    return e("coalesced_text");
  }
  
  public boolean i()
  {
    return c("acknowledged") > 0;
  }
  
  public boolean j()
  {
    return c("alert_level") == 0;
  }
  
  public String toString()
  {
    return zzw.a(this).a("Id", Long.valueOf(a())).a("NotificationId", c()).a("Type", Integer.valueOf(d())).a("Title", f()).a("Ticker", e()).a("Text", g()).a("CoalescedText", h()).a("isAcknowledged", Boolean.valueOf(i())).a("isSilent", Boolean.valueOf(j())).toString();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/internal/notification/GameNotificationRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */