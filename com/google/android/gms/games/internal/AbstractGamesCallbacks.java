package com.google.android.gms.games.internal;

import android.os.Bundle;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.video.VideoCapabilities;

public abstract class AbstractGamesCallbacks
  extends IGamesCallbacks.Stub
{
  public void A(DataHolder paramDataHolder) {}
  
  public void B(DataHolder paramDataHolder) {}
  
  public void C(DataHolder paramDataHolder) {}
  
  public void D(DataHolder paramDataHolder) {}
  
  public void E(DataHolder paramDataHolder) {}
  
  public void F(DataHolder paramDataHolder) {}
  
  public void G(DataHolder paramDataHolder) {}
  
  public void H(DataHolder paramDataHolder) {}
  
  public void I(DataHolder paramDataHolder) {}
  
  public void J(DataHolder paramDataHolder) {}
  
  public void K(DataHolder paramDataHolder) {}
  
  public void L(DataHolder paramDataHolder) {}
  
  public void M(DataHolder paramDataHolder) {}
  
  public void N(DataHolder paramDataHolder) {}
  
  public void O(DataHolder paramDataHolder) {}
  
  public void P(DataHolder paramDataHolder) {}
  
  public void Q(DataHolder paramDataHolder) {}
  
  public void a() {}
  
  public void a(int paramInt) {}
  
  public void a(int paramInt1, int paramInt2, String paramString) {}
  
  public void a(int paramInt, Bundle paramBundle) {}
  
  public void a(int paramInt, VideoCapabilities paramVideoCapabilities) {}
  
  public void a(int paramInt, String paramString) {}
  
  public void a(int paramInt, String paramString, boolean paramBoolean) {}
  
  public void a(int paramInt, boolean paramBoolean) {}
  
  public void a(int paramInt, long[] paramArrayOfLong) {}
  
  public void a(DataHolder paramDataHolder) {}
  
  public void a(DataHolder paramDataHolder1, DataHolder paramDataHolder2) {}
  
  public void a(DataHolder paramDataHolder, Contents paramContents) {}
  
  public void a(DataHolder paramDataHolder, String paramString, Contents paramContents1, Contents paramContents2, Contents paramContents3) {}
  
  public void a(DataHolder paramDataHolder, String[] paramArrayOfString) {}
  
  public void a(RealTimeMessage paramRealTimeMessage) {}
  
  public void a(String paramString) {}
  
  public void a(DataHolder[] paramArrayOfDataHolder) {}
  
  public void b(int paramInt) {}
  
  public void b(int paramInt, Bundle paramBundle) {}
  
  public void b(int paramInt, String paramString) {}
  
  public void b(DataHolder paramDataHolder) {}
  
  public void b(DataHolder paramDataHolder, String[] paramArrayOfString) {}
  
  public void b(String paramString) {}
  
  public void c(int paramInt) {}
  
  public void c(int paramInt, Bundle paramBundle) {}
  
  public void c(int paramInt, String paramString) {}
  
  public void c(DataHolder paramDataHolder) {}
  
  public void c(DataHolder paramDataHolder, String[] paramArrayOfString) {}
  
  public void c(String paramString) {}
  
  public void d(int paramInt) {}
  
  public void d(int paramInt, Bundle paramBundle) {}
  
  public void d(int paramInt, String paramString) {}
  
  public void d(DataHolder paramDataHolder) {}
  
  public void d(DataHolder paramDataHolder, String[] paramArrayOfString) {}
  
  public void d(String paramString) {}
  
  public void e(int paramInt, Bundle paramBundle) {}
  
  public void e(int paramInt, String paramString) {}
  
  public void e(DataHolder paramDataHolder) {}
  
  public void e(DataHolder paramDataHolder, String[] paramArrayOfString) {}
  
  public void e(String paramString) {}
  
  public void f(DataHolder paramDataHolder) {}
  
  public void f(DataHolder paramDataHolder, String[] paramArrayOfString) {}
  
  public void g(DataHolder paramDataHolder) {}
  
  public void h(DataHolder paramDataHolder) {}
  
  public void i(DataHolder paramDataHolder) {}
  
  public void j(DataHolder paramDataHolder) {}
  
  public void k(DataHolder paramDataHolder) {}
  
  public void l(DataHolder paramDataHolder) {}
  
  public void m(DataHolder paramDataHolder) {}
  
  public void n(DataHolder paramDataHolder) {}
  
  public void o(DataHolder paramDataHolder) {}
  
  public void p(DataHolder paramDataHolder) {}
  
  public void q(DataHolder paramDataHolder) {}
  
  public void r(DataHolder paramDataHolder) {}
  
  public void s(DataHolder paramDataHolder) {}
  
  public void t(DataHolder paramDataHolder) {}
  
  public void u(DataHolder paramDataHolder) {}
  
  public void v(DataHolder paramDataHolder) {}
  
  public void w(DataHolder paramDataHolder) {}
  
  public void x(DataHolder paramDataHolder) {}
  
  public void y(DataHolder paramDataHolder) {}
  
  public void z(DataHolder paramDataHolder) {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/internal/AbstractGamesCallbacks.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */