package com.google.android.gms.games.internal;

import com.google.android.gms.common.internal.zzo;

public final class GamesLog
{
  private static final zzo a = new zzo("Games");
  
  public static void a(String paramString1, String paramString2)
  {
    a.b(paramString1, paramString2);
  }
  
  public static void a(String paramString1, String paramString2, Throwable paramThrowable)
  {
    a.a(paramString1, paramString2, paramThrowable);
  }
  
  public static void b(String paramString1, String paramString2)
  {
    a.c(paramString1, paramString2);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/internal/GamesLog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */