package com.google.android.gms.games.internal.game;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class GameBadgeEntityCreator
  implements Parcelable.Creator<GameBadgeEntity>
{
  static void a(GameBadgeEntity paramGameBadgeEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramGameBadgeEntity.b());
    zzb.a(paramParcel, 1000, paramGameBadgeEntity.f());
    zzb.a(paramParcel, 2, paramGameBadgeEntity.c(), false);
    zzb.a(paramParcel, 3, paramGameBadgeEntity.d(), false);
    zzb.a(paramParcel, 4, paramGameBadgeEntity.e(), paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public GameBadgeEntity a(Parcel paramParcel)
  {
    int i = 0;
    Uri localUri = null;
    int k = zza.b(paramParcel);
    String str1 = null;
    String str2 = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        i = zza.g(paramParcel, m);
        break;
      case 1000: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        str2 = zza.p(paramParcel, m);
        break;
      case 3: 
        str1 = zza.p(paramParcel, m);
        break;
      case 4: 
        localUri = (Uri)zza.a(paramParcel, m, Uri.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new GameBadgeEntity(j, i, str2, str1, localUri);
  }
  
  public GameBadgeEntity[] a(int paramInt)
  {
    return new GameBadgeEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/internal/game/GameBadgeEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */