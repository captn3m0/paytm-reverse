package com.google.android.gms.games.internal.game;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.data.Freezable;

public abstract interface GameBadge
  extends Parcelable, Freezable<GameBadge>
{
  public abstract int b();
  
  public abstract String c();
  
  public abstract String d();
  
  public abstract Uri e();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/internal/game/GameBadge.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */