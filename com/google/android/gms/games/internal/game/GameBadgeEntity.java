package com.google.android.gms.games.internal.game;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;

public final class GameBadgeEntity
  extends GamesDowngradeableSafeParcel
  implements GameBadge
{
  public static final GameBadgeEntityCreator CREATOR = new GameBadgeEntityCreatorCompat();
  private final int a;
  private int b;
  private String c;
  private String d;
  private Uri e;
  
  GameBadgeEntity(int paramInt1, int paramInt2, String paramString1, String paramString2, Uri paramUri)
  {
    this.a = paramInt1;
    this.b = paramInt2;
    this.c = paramString1;
    this.d = paramString2;
    this.e = paramUri;
  }
  
  public GameBadgeEntity(GameBadge paramGameBadge)
  {
    this.a = 1;
    this.b = paramGameBadge.b();
    this.c = paramGameBadge.c();
    this.d = paramGameBadge.d();
    this.e = paramGameBadge.e();
  }
  
  static int a(GameBadge paramGameBadge)
  {
    return zzw.a(new Object[] { Integer.valueOf(paramGameBadge.b()), paramGameBadge.c(), paramGameBadge.d(), paramGameBadge.e() });
  }
  
  static boolean a(GameBadge paramGameBadge, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof GameBadge)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramGameBadge == paramObject);
      paramObject = (GameBadge)paramObject;
      if (!zzw.a(Integer.valueOf(((GameBadge)paramObject).b()), paramGameBadge.c())) {
        break;
      }
      bool1 = bool2;
    } while (zzw.a(((GameBadge)paramObject).d(), paramGameBadge.e()));
    return false;
  }
  
  static String b(GameBadge paramGameBadge)
  {
    return zzw.a(paramGameBadge).a("Type", Integer.valueOf(paramGameBadge.b())).a("Title", paramGameBadge.c()).a("Description", paramGameBadge.d()).a("IconImageUri", paramGameBadge.e()).toString();
  }
  
  public int b()
  {
    return this.b;
  }
  
  public String c()
  {
    return this.c;
  }
  
  public String d()
  {
    return this.d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Uri e()
  {
    return this.e;
  }
  
  public boolean equals(Object paramObject)
  {
    return a(this, paramObject);
  }
  
  public int f()
  {
    return this.a;
  }
  
  public GameBadge g()
  {
    return this;
  }
  
  public int hashCode()
  {
    return a(this);
  }
  
  public String toString()
  {
    return b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    if (!k_())
    {
      GameBadgeEntityCreator.a(this, paramParcel, paramInt);
      return;
    }
    paramParcel.writeInt(this.b);
    paramParcel.writeString(this.c);
    paramParcel.writeString(this.d);
    if (this.e == null) {}
    for (String str = null;; str = this.e.toString())
    {
      paramParcel.writeString(str);
      return;
    }
  }
  
  static final class GameBadgeEntityCreatorCompat
    extends GameBadgeEntityCreator
  {
    public GameBadgeEntity a(Parcel paramParcel)
    {
      if ((GameBadgeEntity.a(GameBadgeEntity.h())) || (GameBadgeEntity.b(GameBadgeEntity.class.getCanonicalName()))) {
        return super.a(paramParcel);
      }
      int i = paramParcel.readInt();
      String str1 = paramParcel.readString();
      String str2 = paramParcel.readString();
      paramParcel = paramParcel.readString();
      if (paramParcel == null) {}
      for (paramParcel = null;; paramParcel = Uri.parse(paramParcel)) {
        return new GameBadgeEntity(1, i, str1, str2, paramParcel);
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/internal/game/GameBadgeEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */