package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games.BaseGamesApiMethodImpl;
import com.google.android.gms.games.GamesMetadata;
import com.google.android.gms.games.GamesMetadata.LoadGameInstancesResult;
import com.google.android.gms.games.GamesMetadata.LoadGameSearchSuggestionsResult;
import com.google.android.gms.games.GamesMetadata.LoadGamesResult;
import com.google.android.gms.games.internal.GamesClientImpl;

public final class GamesMetadataImpl
  implements GamesMetadata
{
  private static abstract class LoadGameInstancesImpl
    extends Games.BaseGamesApiMethodImpl<GamesMetadata.LoadGameInstancesResult>
  {
    public GamesMetadata.LoadGameInstancesResult a(final Status paramStatus)
    {
      new GamesMetadata.LoadGameInstancesResult()
      {
        public Status getStatus()
        {
          return paramStatus;
        }
        
        public void release() {}
      };
    }
  }
  
  private static abstract class LoadGameSearchSuggestionsImpl
    extends Games.BaseGamesApiMethodImpl<GamesMetadata.LoadGameSearchSuggestionsResult>
  {
    public GamesMetadata.LoadGameSearchSuggestionsResult a(final Status paramStatus)
    {
      new GamesMetadata.LoadGameSearchSuggestionsResult()
      {
        public Status getStatus()
        {
          return paramStatus;
        }
        
        public void release() {}
      };
    }
  }
  
  private static abstract class LoadGamesImpl
    extends Games.BaseGamesApiMethodImpl<GamesMetadata.LoadGamesResult>
  {
    public GamesMetadata.LoadGamesResult a(final Status paramStatus)
    {
      new GamesMetadata.LoadGamesResult()
      {
        public Status getStatus()
        {
          return paramStatus;
        }
        
        public void release() {}
      };
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/internal/api/GamesMetadataImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */