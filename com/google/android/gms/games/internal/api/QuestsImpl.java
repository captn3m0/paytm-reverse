package com.google.android.gms.games.internal.api;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games.BaseGamesApiMethodImpl;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.quest.Quests;
import com.google.android.gms.games.quest.Quests.AcceptQuestResult;
import com.google.android.gms.games.quest.Quests.ClaimMilestoneResult;
import com.google.android.gms.games.quest.Quests.LoadQuestsResult;

public final class QuestsImpl
  implements Quests
{
  private static abstract class AcceptImpl
    extends Games.BaseGamesApiMethodImpl<Quests.AcceptQuestResult>
  {
    public Quests.AcceptQuestResult a(final Status paramStatus)
    {
      new Quests.AcceptQuestResult()
      {
        public Status getStatus()
        {
          return paramStatus;
        }
      };
    }
  }
  
  private static abstract class ClaimImpl
    extends Games.BaseGamesApiMethodImpl<Quests.ClaimMilestoneResult>
  {
    public Quests.ClaimMilestoneResult a(final Status paramStatus)
    {
      new Quests.ClaimMilestoneResult()
      {
        public Status getStatus()
        {
          return paramStatus;
        }
      };
    }
  }
  
  private static abstract class LoadsImpl
    extends Games.BaseGamesApiMethodImpl<Quests.LoadQuestsResult>
  {
    public Quests.LoadQuestsResult a(final Status paramStatus)
    {
      new Quests.LoadQuestsResult()
      {
        public Status getStatus()
        {
          return paramStatus;
        }
        
        public void release() {}
      };
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/internal/api/QuestsImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */