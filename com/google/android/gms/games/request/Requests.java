package com.google.android.gms.games.request;

import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;

public abstract interface Requests
{
  public static abstract interface LoadRequestSummariesResult
    extends Releasable, Result
  {}
  
  public static abstract interface LoadRequestsResult
    extends Releasable, Result
  {}
  
  public static abstract interface SendRequestResult
    extends Result
  {}
  
  public static abstract interface UpdateRequestsResult
    extends Releasable, Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/request/Requests.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */