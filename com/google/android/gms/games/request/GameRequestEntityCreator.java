package com.google.android.gms.games.request;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.PlayerEntity;
import java.util.ArrayList;

public class GameRequestEntityCreator
  implements Parcelable.Creator<GameRequestEntity>
{
  static void a(GameRequestEntity paramGameRequestEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramGameRequestEntity.e(), paramInt, false);
    zzb.a(paramParcel, 1000, paramGameRequestEntity.b());
    zzb.a(paramParcel, 2, paramGameRequestEntity.f(), paramInt, false);
    zzb.a(paramParcel, 3, paramGameRequestEntity.h(), false);
    zzb.a(paramParcel, 4, paramGameRequestEntity.d(), false);
    zzb.c(paramParcel, 5, paramGameRequestEntity.n(), false);
    zzb.a(paramParcel, 7, paramGameRequestEntity.i());
    zzb.a(paramParcel, 9, paramGameRequestEntity.j());
    zzb.a(paramParcel, 10, paramGameRequestEntity.k());
    zzb.a(paramParcel, 11, paramGameRequestEntity.c(), false);
    zzb.a(paramParcel, 12, paramGameRequestEntity.l());
    zzb.a(paramParcel, i);
  }
  
  public GameRequestEntity a(Parcel paramParcel)
  {
    int m = zza.b(paramParcel);
    int k = 0;
    GameEntity localGameEntity = null;
    PlayerEntity localPlayerEntity = null;
    byte[] arrayOfByte = null;
    String str = null;
    ArrayList localArrayList = null;
    int j = 0;
    long l2 = 0L;
    long l1 = 0L;
    Bundle localBundle = null;
    int i = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        localGameEntity = (GameEntity)zza.a(paramParcel, n, GameEntity.CREATOR);
        break;
      case 1000: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        localPlayerEntity = (PlayerEntity)zza.a(paramParcel, n, PlayerEntity.CREATOR);
        break;
      case 3: 
        arrayOfByte = zza.s(paramParcel, n);
        break;
      case 4: 
        str = zza.p(paramParcel, n);
        break;
      case 5: 
        localArrayList = zza.c(paramParcel, n, PlayerEntity.CREATOR);
        break;
      case 7: 
        j = zza.g(paramParcel, n);
        break;
      case 9: 
        l2 = zza.i(paramParcel, n);
        break;
      case 10: 
        l1 = zza.i(paramParcel, n);
        break;
      case 11: 
        localBundle = zza.r(paramParcel, n);
        break;
      case 12: 
        i = zza.g(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new GameRequestEntity(k, localGameEntity, localPlayerEntity, arrayOfByte, str, localArrayList, j, l2, l1, localBundle, i);
  }
  
  public GameRequestEntity[] a(int paramInt)
  {
    return new GameRequestEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/request/GameRequestEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */