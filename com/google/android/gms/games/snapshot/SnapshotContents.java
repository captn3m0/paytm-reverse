package com.google.android.gms.games.snapshot;

import android.os.Parcelable;
import com.google.android.gms.drive.Contents;

public abstract interface SnapshotContents
  extends Parcelable
{
  public abstract Contents a();
  
  public abstract void b();
  
  public abstract boolean c();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/snapshot/SnapshotContents.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */