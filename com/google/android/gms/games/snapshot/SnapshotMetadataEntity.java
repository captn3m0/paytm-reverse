package com.google.android.gms.games.snapshot;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;

public final class SnapshotMetadataEntity
  implements SafeParcelable, SnapshotMetadata
{
  public static final Parcelable.Creator<SnapshotMetadataEntity> CREATOR = new SnapshotMetadataEntityCreator();
  private final int a;
  private final GameEntity b;
  private final PlayerEntity c;
  private final String d;
  private final Uri e;
  private final String f;
  private final String g;
  private final String h;
  private final long i;
  private final long j;
  private final float k;
  private final String l;
  private final boolean m;
  private final long n;
  private final String o;
  
  SnapshotMetadataEntity(int paramInt, GameEntity paramGameEntity, PlayerEntity paramPlayerEntity, String paramString1, Uri paramUri, String paramString2, String paramString3, String paramString4, long paramLong1, long paramLong2, float paramFloat, String paramString5, boolean paramBoolean, long paramLong3, String paramString6)
  {
    this.a = paramInt;
    this.b = paramGameEntity;
    this.c = paramPlayerEntity;
    this.d = paramString1;
    this.e = paramUri;
    this.f = paramString2;
    this.k = paramFloat;
    this.g = paramString3;
    this.h = paramString4;
    this.i = paramLong1;
    this.j = paramLong2;
    this.l = paramString5;
    this.m = paramBoolean;
    this.n = paramLong3;
    this.o = paramString6;
  }
  
  public SnapshotMetadataEntity(SnapshotMetadata paramSnapshotMetadata)
  {
    this.a = 6;
    this.b = new GameEntity(paramSnapshotMetadata.b());
    this.c = new PlayerEntity(paramSnapshotMetadata.c());
    this.d = paramSnapshotMetadata.d();
    this.e = paramSnapshotMetadata.e();
    this.f = paramSnapshotMetadata.getCoverImageUrl();
    this.k = paramSnapshotMetadata.f();
    this.g = paramSnapshotMetadata.h();
    this.h = paramSnapshotMetadata.i();
    this.i = paramSnapshotMetadata.j();
    this.j = paramSnapshotMetadata.k();
    this.l = paramSnapshotMetadata.g();
    this.m = paramSnapshotMetadata.l();
    this.n = paramSnapshotMetadata.m();
    this.o = paramSnapshotMetadata.n();
  }
  
  static int a(SnapshotMetadata paramSnapshotMetadata)
  {
    return zzw.a(new Object[] { paramSnapshotMetadata.b(), paramSnapshotMetadata.c(), paramSnapshotMetadata.d(), paramSnapshotMetadata.e(), Float.valueOf(paramSnapshotMetadata.f()), paramSnapshotMetadata.h(), paramSnapshotMetadata.i(), Long.valueOf(paramSnapshotMetadata.j()), Long.valueOf(paramSnapshotMetadata.k()), paramSnapshotMetadata.g(), Boolean.valueOf(paramSnapshotMetadata.l()), Long.valueOf(paramSnapshotMetadata.m()), paramSnapshotMetadata.n() });
  }
  
  static boolean a(SnapshotMetadata paramSnapshotMetadata, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof SnapshotMetadata)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramSnapshotMetadata == paramObject);
      paramObject = (SnapshotMetadata)paramObject;
      if ((!zzw.a(((SnapshotMetadata)paramObject).b(), paramSnapshotMetadata.b())) || (!zzw.a(((SnapshotMetadata)paramObject).c(), paramSnapshotMetadata.c())) || (!zzw.a(((SnapshotMetadata)paramObject).d(), paramSnapshotMetadata.d())) || (!zzw.a(((SnapshotMetadata)paramObject).e(), paramSnapshotMetadata.e())) || (!zzw.a(Float.valueOf(((SnapshotMetadata)paramObject).f()), Float.valueOf(paramSnapshotMetadata.f()))) || (!zzw.a(((SnapshotMetadata)paramObject).h(), paramSnapshotMetadata.h())) || (!zzw.a(((SnapshotMetadata)paramObject).i(), paramSnapshotMetadata.i())) || (!zzw.a(Long.valueOf(((SnapshotMetadata)paramObject).j()), Long.valueOf(paramSnapshotMetadata.j()))) || (!zzw.a(Long.valueOf(((SnapshotMetadata)paramObject).k()), Long.valueOf(paramSnapshotMetadata.k()))) || (!zzw.a(((SnapshotMetadata)paramObject).g(), paramSnapshotMetadata.g())) || (!zzw.a(Boolean.valueOf(((SnapshotMetadata)paramObject).l()), Boolean.valueOf(paramSnapshotMetadata.l()))) || (!zzw.a(Long.valueOf(((SnapshotMetadata)paramObject).m()), Long.valueOf(paramSnapshotMetadata.m())))) {
        break;
      }
      bool1 = bool2;
    } while (zzw.a(((SnapshotMetadata)paramObject).n(), paramSnapshotMetadata.n()));
    return false;
  }
  
  static String b(SnapshotMetadata paramSnapshotMetadata)
  {
    return zzw.a(paramSnapshotMetadata).a("Game", paramSnapshotMetadata.b()).a("Owner", paramSnapshotMetadata.c()).a("SnapshotId", paramSnapshotMetadata.d()).a("CoverImageUri", paramSnapshotMetadata.e()).a("CoverImageUrl", paramSnapshotMetadata.getCoverImageUrl()).a("CoverImageAspectRatio", Float.valueOf(paramSnapshotMetadata.f())).a("Description", paramSnapshotMetadata.i()).a("LastModifiedTimestamp", Long.valueOf(paramSnapshotMetadata.j())).a("PlayedTime", Long.valueOf(paramSnapshotMetadata.k())).a("UniqueName", paramSnapshotMetadata.g()).a("ChangePending", Boolean.valueOf(paramSnapshotMetadata.l())).a("ProgressValue", Long.valueOf(paramSnapshotMetadata.m())).a("DeviceName", paramSnapshotMetadata.n()).toString();
  }
  
  public Game b()
  {
    return this.b;
  }
  
  public Player c()
  {
    return this.c;
  }
  
  public String d()
  {
    return this.d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Uri e()
  {
    return this.e;
  }
  
  public boolean equals(Object paramObject)
  {
    return a(this, paramObject);
  }
  
  public float f()
  {
    return this.k;
  }
  
  public String g()
  {
    return this.l;
  }
  
  public String getCoverImageUrl()
  {
    return this.f;
  }
  
  public String h()
  {
    return this.g;
  }
  
  public int hashCode()
  {
    return a(this);
  }
  
  public String i()
  {
    return this.h;
  }
  
  public long j()
  {
    return this.i;
  }
  
  public long k()
  {
    return this.j;
  }
  
  public boolean l()
  {
    return this.m;
  }
  
  public long m()
  {
    return this.n;
  }
  
  public String n()
  {
    return this.o;
  }
  
  public int o()
  {
    return this.a;
  }
  
  public SnapshotMetadata p()
  {
    return this;
  }
  
  public String toString()
  {
    return b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    SnapshotMetadataEntityCreator.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/snapshot/SnapshotMetadataEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */