package com.google.android.gms.games.snapshot;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;

public final class SnapshotMetadataChangeEntity
  extends SnapshotMetadataChange
  implements SafeParcelable
{
  public static final SnapshotMetadataChangeCreator CREATOR = new SnapshotMetadataChangeCreator();
  private final int b;
  private final String c;
  private final Long d;
  private final Uri e;
  private BitmapTeleporter f;
  private final Long g;
  
  SnapshotMetadataChangeEntity()
  {
    this(5, null, null, null, null, null);
  }
  
  SnapshotMetadataChangeEntity(int paramInt, String paramString, Long paramLong1, BitmapTeleporter paramBitmapTeleporter, Uri paramUri, Long paramLong2)
  {
    this.b = paramInt;
    this.c = paramString;
    this.d = paramLong1;
    this.f = paramBitmapTeleporter;
    this.e = paramUri;
    this.g = paramLong2;
    if (this.f != null) {
      if (this.e == null) {
        zzx.a(bool1, "Cannot set both a URI and an image");
      }
    }
    while (this.e == null) {
      for (;;)
      {
        return;
        bool1 = false;
      }
    }
    if (this.f == null) {}
    for (bool1 = bool2;; bool1 = false)
    {
      zzx.a(bool1, "Cannot set both a URI and an image");
      return;
    }
  }
  
  public BitmapTeleporter a()
  {
    return this.f;
  }
  
  public int b()
  {
    return this.b;
  }
  
  public String c()
  {
    return this.c;
  }
  
  public Long d()
  {
    return this.d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Long e()
  {
    return this.g;
  }
  
  public Uri f()
  {
    return this.e;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    SnapshotMetadataChangeCreator.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/snapshot/SnapshotMetadataChangeEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */