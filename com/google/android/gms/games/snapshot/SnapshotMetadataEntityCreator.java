package com.google.android.gms.games.snapshot;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.PlayerEntity;

public class SnapshotMetadataEntityCreator
  implements Parcelable.Creator<SnapshotMetadataEntity>
{
  static void a(SnapshotMetadataEntity paramSnapshotMetadataEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramSnapshotMetadataEntity.b(), paramInt, false);
    zzb.a(paramParcel, 1000, paramSnapshotMetadataEntity.o());
    zzb.a(paramParcel, 2, paramSnapshotMetadataEntity.c(), paramInt, false);
    zzb.a(paramParcel, 3, paramSnapshotMetadataEntity.d(), false);
    zzb.a(paramParcel, 5, paramSnapshotMetadataEntity.e(), paramInt, false);
    zzb.a(paramParcel, 6, paramSnapshotMetadataEntity.getCoverImageUrl(), false);
    zzb.a(paramParcel, 7, paramSnapshotMetadataEntity.h(), false);
    zzb.a(paramParcel, 8, paramSnapshotMetadataEntity.i(), false);
    zzb.a(paramParcel, 9, paramSnapshotMetadataEntity.j());
    zzb.a(paramParcel, 10, paramSnapshotMetadataEntity.k());
    zzb.a(paramParcel, 11, paramSnapshotMetadataEntity.f());
    zzb.a(paramParcel, 12, paramSnapshotMetadataEntity.g(), false);
    zzb.a(paramParcel, 13, paramSnapshotMetadataEntity.l());
    zzb.a(paramParcel, 14, paramSnapshotMetadataEntity.m());
    zzb.a(paramParcel, 15, paramSnapshotMetadataEntity.n(), false);
    zzb.a(paramParcel, i);
  }
  
  public SnapshotMetadataEntity a(Parcel paramParcel)
  {
    int j = zza.b(paramParcel);
    int i = 0;
    GameEntity localGameEntity = null;
    PlayerEntity localPlayerEntity = null;
    String str6 = null;
    Uri localUri = null;
    String str5 = null;
    String str4 = null;
    String str3 = null;
    long l3 = 0L;
    long l2 = 0L;
    float f = 0.0F;
    String str2 = null;
    boolean bool = false;
    long l1 = 0L;
    String str1 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localGameEntity = (GameEntity)zza.a(paramParcel, k, GameEntity.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localPlayerEntity = (PlayerEntity)zza.a(paramParcel, k, PlayerEntity.CREATOR);
        break;
      case 3: 
        str6 = zza.p(paramParcel, k);
        break;
      case 5: 
        localUri = (Uri)zza.a(paramParcel, k, Uri.CREATOR);
        break;
      case 6: 
        str5 = zza.p(paramParcel, k);
        break;
      case 7: 
        str4 = zza.p(paramParcel, k);
        break;
      case 8: 
        str3 = zza.p(paramParcel, k);
        break;
      case 9: 
        l3 = zza.i(paramParcel, k);
        break;
      case 10: 
        l2 = zza.i(paramParcel, k);
        break;
      case 11: 
        f = zza.l(paramParcel, k);
        break;
      case 12: 
        str2 = zza.p(paramParcel, k);
        break;
      case 13: 
        bool = zza.c(paramParcel, k);
        break;
      case 14: 
        l1 = zza.i(paramParcel, k);
        break;
      case 15: 
        str1 = zza.p(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new SnapshotMetadataEntity(i, localGameEntity, localPlayerEntity, str6, localUri, str5, str4, str3, l3, l2, f, str2, bool, l1, str1);
  }
  
  public SnapshotMetadataEntity[] a(int paramInt)
  {
    return new SnapshotMetadataEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/snapshot/SnapshotMetadataEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */