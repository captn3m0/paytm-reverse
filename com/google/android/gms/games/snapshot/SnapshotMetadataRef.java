package com.google.android.gms.games.snapshot;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameRef;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerRef;

public final class SnapshotMetadataRef
  extends zzc
  implements SnapshotMetadata
{
  private final Game c;
  private final Player d;
  
  public SnapshotMetadataRef(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
    this.c = new GameRef(paramDataHolder, paramInt);
    this.d = new PlayerRef(paramDataHolder, paramInt);
  }
  
  public Game b()
  {
    return this.c;
  }
  
  public Player c()
  {
    return this.d;
  }
  
  public String d()
  {
    return e("external_snapshot_id");
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Uri e()
  {
    return h("cover_icon_image_uri");
  }
  
  public boolean equals(Object paramObject)
  {
    return SnapshotMetadataEntity.a(this, paramObject);
  }
  
  public float f()
  {
    float f1 = f("cover_icon_image_height");
    float f2 = f("cover_icon_image_width");
    if (f1 == 0.0F) {
      return 0.0F;
    }
    return f2 / f1;
  }
  
  public String g()
  {
    return e("unique_name");
  }
  
  public String getCoverImageUrl()
  {
    return e("cover_icon_image_url");
  }
  
  public String h()
  {
    return e("title");
  }
  
  public int hashCode()
  {
    return SnapshotMetadataEntity.a(this);
  }
  
  public String i()
  {
    return e("description");
  }
  
  public long j()
  {
    return b("last_modified_timestamp");
  }
  
  public long k()
  {
    return b("duration");
  }
  
  public boolean l()
  {
    return c("pending_change_count") > 0;
  }
  
  public long m()
  {
    return b("progress_value");
  }
  
  public String n()
  {
    return e("device_name");
  }
  
  public SnapshotMetadata o()
  {
    return new SnapshotMetadataEntity(this);
  }
  
  public String toString()
  {
    return SnapshotMetadataEntity.b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((SnapshotMetadataEntity)o()).writeToParcel(paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/snapshot/SnapshotMetadataRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */