package com.google.android.gms.games.snapshot;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class SnapshotMetadataChangeCreator
  implements Parcelable.Creator<SnapshotMetadataChangeEntity>
{
  static void a(SnapshotMetadataChangeEntity paramSnapshotMetadataChangeEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramSnapshotMetadataChangeEntity.c(), false);
    zzb.a(paramParcel, 1000, paramSnapshotMetadataChangeEntity.b());
    zzb.a(paramParcel, 2, paramSnapshotMetadataChangeEntity.d(), false);
    zzb.a(paramParcel, 4, paramSnapshotMetadataChangeEntity.f(), paramInt, false);
    zzb.a(paramParcel, 5, paramSnapshotMetadataChangeEntity.a(), paramInt, false);
    zzb.a(paramParcel, 6, paramSnapshotMetadataChangeEntity.e(), false);
    zzb.a(paramParcel, i);
  }
  
  public SnapshotMetadataChangeEntity a(Parcel paramParcel)
  {
    Long localLong1 = null;
    int j = zza.b(paramParcel);
    int i = 0;
    Uri localUri = null;
    BitmapTeleporter localBitmapTeleporter = null;
    Long localLong2 = null;
    String str = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        str = zza.p(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localLong2 = zza.j(paramParcel, k);
        break;
      case 4: 
        localUri = (Uri)zza.a(paramParcel, k, Uri.CREATOR);
        break;
      case 5: 
        localBitmapTeleporter = (BitmapTeleporter)zza.a(paramParcel, k, BitmapTeleporter.CREATOR);
        break;
      case 6: 
        localLong1 = zza.j(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new SnapshotMetadataChangeEntity(i, str, localLong2, localBitmapTeleporter, localUri, localLong1);
  }
  
  public SnapshotMetadataChangeEntity[] a(int paramInt)
  {
    return new SnapshotMetadataChangeEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/snapshot/SnapshotMetadataChangeCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */