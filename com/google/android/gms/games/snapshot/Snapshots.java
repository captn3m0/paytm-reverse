package com.google.android.gms.games.snapshot;

import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;

public abstract interface Snapshots
{
  public static abstract interface CommitSnapshotResult
    extends Result
  {}
  
  public static abstract interface DeleteSnapshotResult
    extends Result
  {}
  
  public static abstract interface LoadSnapshotsResult
    extends Releasable, Result
  {}
  
  public static abstract interface OpenSnapshotResult
    extends Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/snapshot/Snapshots.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */