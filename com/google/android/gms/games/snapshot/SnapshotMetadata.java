package com.google.android.gms.games.snapshot;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.data.Freezable;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.Player;

public abstract interface SnapshotMetadata
  extends Parcelable, Freezable<SnapshotMetadata>
{
  public abstract Game b();
  
  public abstract Player c();
  
  public abstract String d();
  
  public abstract Uri e();
  
  public abstract float f();
  
  public abstract String g();
  
  @Deprecated
  @KeepName
  public abstract String getCoverImageUrl();
  
  public abstract String h();
  
  public abstract String i();
  
  public abstract long j();
  
  public abstract long k();
  
  public abstract boolean l();
  
  public abstract long m();
  
  public abstract String n();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/snapshot/SnapshotMetadata.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */