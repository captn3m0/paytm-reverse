package com.google.android.gms.games.leaderboard;

import android.net.Uri;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import java.util.ArrayList;

public final class LeaderboardEntity
  implements Leaderboard
{
  private final String a;
  private final String b;
  private final Uri c;
  private final int d;
  private final ArrayList<LeaderboardVariantEntity> e;
  private final Game f;
  private final String g;
  
  public LeaderboardEntity(Leaderboard paramLeaderboard)
  {
    this.a = paramLeaderboard.b();
    this.b = paramLeaderboard.c();
    this.c = paramLeaderboard.d();
    this.g = paramLeaderboard.getIconImageUrl();
    this.d = paramLeaderboard.e();
    Object localObject = paramLeaderboard.g();
    if (localObject == null) {}
    for (localObject = null;; localObject = new GameEntity((Game)localObject))
    {
      this.f = ((Game)localObject);
      paramLeaderboard = paramLeaderboard.f();
      int j = paramLeaderboard.size();
      this.e = new ArrayList(j);
      int i = 0;
      while (i < j)
      {
        this.e.add((LeaderboardVariantEntity)((LeaderboardVariant)paramLeaderboard.get(i)).a());
        i += 1;
      }
    }
  }
  
  static int a(Leaderboard paramLeaderboard)
  {
    return zzw.a(new Object[] { paramLeaderboard.b(), paramLeaderboard.c(), paramLeaderboard.d(), Integer.valueOf(paramLeaderboard.e()), paramLeaderboard.f() });
  }
  
  static boolean a(Leaderboard paramLeaderboard, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof Leaderboard)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramLeaderboard == paramObject);
      paramObject = (Leaderboard)paramObject;
      if ((!zzw.a(((Leaderboard)paramObject).b(), paramLeaderboard.b())) || (!zzw.a(((Leaderboard)paramObject).c(), paramLeaderboard.c())) || (!zzw.a(((Leaderboard)paramObject).d(), paramLeaderboard.d())) || (!zzw.a(Integer.valueOf(((Leaderboard)paramObject).e()), Integer.valueOf(paramLeaderboard.e())))) {
        break;
      }
      bool1 = bool2;
    } while (zzw.a(((Leaderboard)paramObject).f(), paramLeaderboard.f()));
    return false;
  }
  
  static String b(Leaderboard paramLeaderboard)
  {
    return zzw.a(paramLeaderboard).a("LeaderboardId", paramLeaderboard.b()).a("DisplayName", paramLeaderboard.c()).a("IconImageUri", paramLeaderboard.d()).a("IconImageUrl", paramLeaderboard.getIconImageUrl()).a("ScoreOrder", Integer.valueOf(paramLeaderboard.e())).a("Variants", paramLeaderboard.f()).toString();
  }
  
  public String b()
  {
    return this.a;
  }
  
  public String c()
  {
    return this.b;
  }
  
  public Uri d()
  {
    return this.c;
  }
  
  public int e()
  {
    return this.d;
  }
  
  public boolean equals(Object paramObject)
  {
    return a(this, paramObject);
  }
  
  public ArrayList<LeaderboardVariant> f()
  {
    return new ArrayList(this.e);
  }
  
  public Game g()
  {
    return this.f;
  }
  
  public String getIconImageUrl()
  {
    return this.g;
  }
  
  public Leaderboard h()
  {
    return this;
  }
  
  public int hashCode()
  {
    return a(this);
  }
  
  public String toString()
  {
    return b(this);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/leaderboard/LeaderboardEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */