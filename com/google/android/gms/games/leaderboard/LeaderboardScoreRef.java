package com.google.android.gms.games.leaderboard;

import android.net.Uri;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerRef;

public final class LeaderboardScoreRef
  extends zzc
  implements LeaderboardScore
{
  private final PlayerRef c;
  
  LeaderboardScoreRef(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
    this.c = new PlayerRef(paramDataHolder, paramInt);
  }
  
  public long b()
  {
    return b("rank");
  }
  
  public String c()
  {
    return e("display_rank");
  }
  
  public String d()
  {
    return e("display_score");
  }
  
  public long e()
  {
    return b("raw_score");
  }
  
  public boolean equals(Object paramObject)
  {
    return LeaderboardScoreEntity.a(this, paramObject);
  }
  
  public long f()
  {
    return b("achieved_timestamp");
  }
  
  public String g()
  {
    if (i("external_player_id")) {
      return e("default_display_name");
    }
    return this.c.c();
  }
  
  public String getScoreHolderHiResImageUrl()
  {
    if (i("external_player_id")) {
      return null;
    }
    return this.c.getHiResImageUrl();
  }
  
  public String getScoreHolderIconImageUrl()
  {
    if (i("external_player_id")) {
      return e("default_display_image_url");
    }
    return this.c.getIconImageUrl();
  }
  
  public Uri h()
  {
    if (i("external_player_id")) {
      return h("default_display_image_uri");
    }
    return this.c.g();
  }
  
  public int hashCode()
  {
    return LeaderboardScoreEntity.a(this);
  }
  
  public Uri i()
  {
    if (i("external_player_id")) {
      return null;
    }
    return this.c.h();
  }
  
  public Player j()
  {
    if (i("external_player_id")) {
      return null;
    }
    return this.c;
  }
  
  public String k()
  {
    return e("score_tag");
  }
  
  public LeaderboardScore l()
  {
    return new LeaderboardScoreEntity(this);
  }
  
  public String toString()
  {
    return LeaderboardScoreEntity.b(this);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/leaderboard/LeaderboardScoreRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */