package com.google.android.gms.games.leaderboard;

import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;

public abstract interface Leaderboards
{
  public static abstract interface LeaderboardMetadataResult
    extends Releasable, Result
  {}
  
  public static abstract interface LoadPlayerScoreResult
    extends Result
  {}
  
  public static abstract interface LoadScoresResult
    extends Releasable, Result
  {}
  
  public static abstract interface SubmitScoreResult
    extends Releasable, Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/leaderboard/Leaderboards.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */