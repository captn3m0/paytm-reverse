package com.google.android.gms.games.leaderboard;

import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.games.internal.constants.LeaderboardCollection;
import com.google.android.gms.games.internal.constants.TimeSpan;

public final class LeaderboardVariantEntity
  implements LeaderboardVariant
{
  private final int a;
  private final int b;
  private final boolean c;
  private final long d;
  private final String e;
  private final long f;
  private final String g;
  private final String h;
  private final long i;
  private final String j;
  private final String k;
  private final String l;
  
  public LeaderboardVariantEntity(LeaderboardVariant paramLeaderboardVariant)
  {
    this.a = paramLeaderboardVariant.b();
    this.b = paramLeaderboardVariant.c();
    this.c = paramLeaderboardVariant.d();
    this.d = paramLeaderboardVariant.e();
    this.e = paramLeaderboardVariant.f();
    this.f = paramLeaderboardVariant.g();
    this.g = paramLeaderboardVariant.h();
    this.h = paramLeaderboardVariant.i();
    this.i = paramLeaderboardVariant.j();
    this.j = paramLeaderboardVariant.k();
    this.k = paramLeaderboardVariant.l();
    this.l = paramLeaderboardVariant.m();
  }
  
  static int a(LeaderboardVariant paramLeaderboardVariant)
  {
    return zzw.a(new Object[] { Integer.valueOf(paramLeaderboardVariant.b()), Integer.valueOf(paramLeaderboardVariant.c()), Boolean.valueOf(paramLeaderboardVariant.d()), Long.valueOf(paramLeaderboardVariant.e()), paramLeaderboardVariant.f(), Long.valueOf(paramLeaderboardVariant.g()), paramLeaderboardVariant.h(), Long.valueOf(paramLeaderboardVariant.j()), paramLeaderboardVariant.k(), paramLeaderboardVariant.m(), paramLeaderboardVariant.l() });
  }
  
  static boolean a(LeaderboardVariant paramLeaderboardVariant, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof LeaderboardVariant)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramLeaderboardVariant == paramObject);
      paramObject = (LeaderboardVariant)paramObject;
      if ((!zzw.a(Integer.valueOf(((LeaderboardVariant)paramObject).b()), Integer.valueOf(paramLeaderboardVariant.b()))) || (!zzw.a(Integer.valueOf(((LeaderboardVariant)paramObject).c()), Integer.valueOf(paramLeaderboardVariant.c()))) || (!zzw.a(Boolean.valueOf(((LeaderboardVariant)paramObject).d()), Boolean.valueOf(paramLeaderboardVariant.d()))) || (!zzw.a(Long.valueOf(((LeaderboardVariant)paramObject).e()), Long.valueOf(paramLeaderboardVariant.e()))) || (!zzw.a(((LeaderboardVariant)paramObject).f(), paramLeaderboardVariant.f())) || (!zzw.a(Long.valueOf(((LeaderboardVariant)paramObject).g()), Long.valueOf(paramLeaderboardVariant.g()))) || (!zzw.a(((LeaderboardVariant)paramObject).h(), paramLeaderboardVariant.h())) || (!zzw.a(Long.valueOf(((LeaderboardVariant)paramObject).j()), Long.valueOf(paramLeaderboardVariant.j()))) || (!zzw.a(((LeaderboardVariant)paramObject).k(), paramLeaderboardVariant.k())) || (!zzw.a(((LeaderboardVariant)paramObject).m(), paramLeaderboardVariant.m()))) {
        break;
      }
      bool1 = bool2;
    } while (zzw.a(((LeaderboardVariant)paramObject).l(), paramLeaderboardVariant.l()));
    return false;
  }
  
  static String b(LeaderboardVariant paramLeaderboardVariant)
  {
    zzw.zza localzza = zzw.a(paramLeaderboardVariant).a("TimeSpan", TimeSpan.a(paramLeaderboardVariant.b())).a("Collection", LeaderboardCollection.a(paramLeaderboardVariant.c()));
    if (paramLeaderboardVariant.d())
    {
      localObject = Long.valueOf(paramLeaderboardVariant.e());
      localzza = localzza.a("RawPlayerScore", localObject);
      if (!paramLeaderboardVariant.d()) {
        break label191;
      }
      localObject = paramLeaderboardVariant.f();
      label76:
      localzza = localzza.a("DisplayPlayerScore", localObject);
      if (!paramLeaderboardVariant.d()) {
        break label197;
      }
      localObject = Long.valueOf(paramLeaderboardVariant.g());
      label103:
      localzza = localzza.a("PlayerRank", localObject);
      if (!paramLeaderboardVariant.d()) {
        break label203;
      }
    }
    label191:
    label197:
    label203:
    for (Object localObject = paramLeaderboardVariant.h();; localObject = "none")
    {
      return localzza.a("DisplayPlayerRank", localObject).a("NumScores", Long.valueOf(paramLeaderboardVariant.j())).a("TopPageNextToken", paramLeaderboardVariant.k()).a("WindowPageNextToken", paramLeaderboardVariant.m()).a("WindowPagePrevToken", paramLeaderboardVariant.l()).toString();
      localObject = "none";
      break;
      localObject = "none";
      break label76;
      localObject = "none";
      break label103;
    }
  }
  
  public int b()
  {
    return this.a;
  }
  
  public int c()
  {
    return this.b;
  }
  
  public boolean d()
  {
    return this.c;
  }
  
  public long e()
  {
    return this.d;
  }
  
  public boolean equals(Object paramObject)
  {
    return a(this, paramObject);
  }
  
  public String f()
  {
    return this.e;
  }
  
  public long g()
  {
    return this.f;
  }
  
  public String h()
  {
    return this.g;
  }
  
  public int hashCode()
  {
    return a(this);
  }
  
  public String i()
  {
    return this.h;
  }
  
  public long j()
  {
    return this.i;
  }
  
  public String k()
  {
    return this.j;
  }
  
  public String l()
  {
    return this.k;
  }
  
  public String m()
  {
    return this.l;
  }
  
  public LeaderboardVariant n()
  {
    return this;
  }
  
  public String toString()
  {
    return b(this);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/leaderboard/LeaderboardVariantEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */