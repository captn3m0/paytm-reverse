package com.google.android.gms.games.leaderboard;

import android.net.Uri;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;

public final class LeaderboardScoreEntity
  implements LeaderboardScore
{
  private final long a;
  private final String b;
  private final String c;
  private final long d;
  private final long e;
  private final String f;
  private final Uri g;
  private final Uri h;
  private final PlayerEntity i;
  private final String j;
  private final String k;
  private final String l;
  
  public LeaderboardScoreEntity(LeaderboardScore paramLeaderboardScore)
  {
    this.a = paramLeaderboardScore.b();
    this.b = ((String)zzx.a(paramLeaderboardScore.c()));
    this.c = ((String)zzx.a(paramLeaderboardScore.d()));
    this.d = paramLeaderboardScore.e();
    this.e = paramLeaderboardScore.f();
    this.f = paramLeaderboardScore.g();
    this.g = paramLeaderboardScore.h();
    this.h = paramLeaderboardScore.i();
    Object localObject = paramLeaderboardScore.j();
    if (localObject == null) {}
    for (localObject = null;; localObject = (PlayerEntity)((Player)localObject).a())
    {
      this.i = ((PlayerEntity)localObject);
      this.j = paramLeaderboardScore.k();
      this.k = paramLeaderboardScore.getScoreHolderIconImageUrl();
      this.l = paramLeaderboardScore.getScoreHolderHiResImageUrl();
      return;
    }
  }
  
  static int a(LeaderboardScore paramLeaderboardScore)
  {
    return zzw.a(new Object[] { Long.valueOf(paramLeaderboardScore.b()), paramLeaderboardScore.c(), Long.valueOf(paramLeaderboardScore.e()), paramLeaderboardScore.d(), Long.valueOf(paramLeaderboardScore.f()), paramLeaderboardScore.g(), paramLeaderboardScore.h(), paramLeaderboardScore.i(), paramLeaderboardScore.j() });
  }
  
  static boolean a(LeaderboardScore paramLeaderboardScore, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof LeaderboardScore)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramLeaderboardScore == paramObject);
      paramObject = (LeaderboardScore)paramObject;
      if ((!zzw.a(Long.valueOf(((LeaderboardScore)paramObject).b()), Long.valueOf(paramLeaderboardScore.b()))) || (!zzw.a(((LeaderboardScore)paramObject).c(), paramLeaderboardScore.c())) || (!zzw.a(Long.valueOf(((LeaderboardScore)paramObject).e()), Long.valueOf(paramLeaderboardScore.e()))) || (!zzw.a(((LeaderboardScore)paramObject).d(), paramLeaderboardScore.d())) || (!zzw.a(Long.valueOf(((LeaderboardScore)paramObject).f()), Long.valueOf(paramLeaderboardScore.f()))) || (!zzw.a(((LeaderboardScore)paramObject).g(), paramLeaderboardScore.g())) || (!zzw.a(((LeaderboardScore)paramObject).h(), paramLeaderboardScore.h())) || (!zzw.a(((LeaderboardScore)paramObject).i(), paramLeaderboardScore.i())) || (!zzw.a(((LeaderboardScore)paramObject).j(), paramLeaderboardScore.j()))) {
        break;
      }
      bool1 = bool2;
    } while (zzw.a(((LeaderboardScore)paramObject).k(), paramLeaderboardScore.k()));
    return false;
  }
  
  static String b(LeaderboardScore paramLeaderboardScore)
  {
    zzw.zza localzza = zzw.a(paramLeaderboardScore).a("Rank", Long.valueOf(paramLeaderboardScore.b())).a("DisplayRank", paramLeaderboardScore.c()).a("Score", Long.valueOf(paramLeaderboardScore.e())).a("DisplayScore", paramLeaderboardScore.d()).a("Timestamp", Long.valueOf(paramLeaderboardScore.f())).a("DisplayName", paramLeaderboardScore.g()).a("IconImageUri", paramLeaderboardScore.h()).a("IconImageUrl", paramLeaderboardScore.getScoreHolderIconImageUrl()).a("HiResImageUri", paramLeaderboardScore.i()).a("HiResImageUrl", paramLeaderboardScore.getScoreHolderHiResImageUrl());
    if (paramLeaderboardScore.j() == null) {}
    for (Object localObject = null;; localObject = paramLeaderboardScore.j()) {
      return localzza.a("Player", localObject).a("ScoreTag", paramLeaderboardScore.k()).toString();
    }
  }
  
  public long b()
  {
    return this.a;
  }
  
  public String c()
  {
    return this.b;
  }
  
  public String d()
  {
    return this.c;
  }
  
  public long e()
  {
    return this.d;
  }
  
  public boolean equals(Object paramObject)
  {
    return a(this, paramObject);
  }
  
  public long f()
  {
    return this.e;
  }
  
  public String g()
  {
    if (this.i == null) {
      return this.f;
    }
    return this.i.c();
  }
  
  public String getScoreHolderHiResImageUrl()
  {
    if (this.i == null) {
      return this.l;
    }
    return this.i.getHiResImageUrl();
  }
  
  public String getScoreHolderIconImageUrl()
  {
    if (this.i == null) {
      return this.k;
    }
    return this.i.getIconImageUrl();
  }
  
  public Uri h()
  {
    if (this.i == null) {
      return this.g;
    }
    return this.i.g();
  }
  
  public int hashCode()
  {
    return a(this);
  }
  
  public Uri i()
  {
    if (this.i == null) {
      return this.h;
    }
    return this.i.h();
  }
  
  public Player j()
  {
    return this.i;
  }
  
  public String k()
  {
    return this.j;
  }
  
  public LeaderboardScore l()
  {
    return this;
  }
  
  public String toString()
  {
    return b(this);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/leaderboard/LeaderboardScoreEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */