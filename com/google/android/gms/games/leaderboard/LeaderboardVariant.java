package com.google.android.gms.games.leaderboard;

import com.google.android.gms.common.data.Freezable;

public abstract interface LeaderboardVariant
  extends Freezable<LeaderboardVariant>
{
  public abstract int b();
  
  public abstract int c();
  
  public abstract boolean d();
  
  public abstract long e();
  
  public abstract String f();
  
  public abstract long g();
  
  public abstract String h();
  
  public abstract String i();
  
  public abstract long j();
  
  public abstract String k();
  
  public abstract String l();
  
  public abstract String m();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/leaderboard/LeaderboardVariant.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */