package com.google.android.gms.games.leaderboard;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;

public final class LeaderboardVariantRef
  extends zzc
  implements LeaderboardVariant
{
  LeaderboardVariantRef(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
  }
  
  public int b()
  {
    return c("timespan");
  }
  
  public int c()
  {
    return c("collection");
  }
  
  public boolean d()
  {
    return !i("player_raw_score");
  }
  
  public long e()
  {
    if (i("player_raw_score")) {
      return -1L;
    }
    return b("player_raw_score");
  }
  
  public boolean equals(Object paramObject)
  {
    return LeaderboardVariantEntity.a(this, paramObject);
  }
  
  public String f()
  {
    return e("player_display_score");
  }
  
  public long g()
  {
    if (i("player_rank")) {
      return -1L;
    }
    return b("player_rank");
  }
  
  public String h()
  {
    return e("player_display_rank");
  }
  
  public int hashCode()
  {
    return LeaderboardVariantEntity.a(this);
  }
  
  public String i()
  {
    return e("player_score_tag");
  }
  
  public long j()
  {
    if (i("total_scores")) {
      return -1L;
    }
    return b("total_scores");
  }
  
  public String k()
  {
    return e("top_page_token_next");
  }
  
  public String l()
  {
    return e("window_page_token_prev");
  }
  
  public String m()
  {
    return e("window_page_token_next");
  }
  
  public LeaderboardVariant n()
  {
    return new LeaderboardVariantEntity(this);
  }
  
  public String toString()
  {
    return LeaderboardVariantEntity.b(this);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/leaderboard/LeaderboardVariantRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */