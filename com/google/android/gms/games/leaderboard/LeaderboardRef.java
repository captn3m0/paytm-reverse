package com.google.android.gms.games.leaderboard;

import android.net.Uri;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameRef;
import java.util.ArrayList;

public final class LeaderboardRef
  extends zzc
  implements Leaderboard
{
  private final int c;
  private final Game d;
  
  LeaderboardRef(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    super(paramDataHolder, paramInt1);
    this.c = paramInt2;
    this.d = new GameRef(paramDataHolder, paramInt1);
  }
  
  public String b()
  {
    return e("external_leaderboard_id");
  }
  
  public String c()
  {
    return e("name");
  }
  
  public Uri d()
  {
    return h("board_icon_image_uri");
  }
  
  public int e()
  {
    return c("score_order");
  }
  
  public boolean equals(Object paramObject)
  {
    return LeaderboardEntity.a(this, paramObject);
  }
  
  public ArrayList<LeaderboardVariant> f()
  {
    ArrayList localArrayList = new ArrayList(this.c);
    int i = 0;
    while (i < this.c)
    {
      localArrayList.add(new LeaderboardVariantRef(this.a, this.b + i));
      i += 1;
    }
    return localArrayList;
  }
  
  public Game g()
  {
    return this.d;
  }
  
  public String getIconImageUrl()
  {
    return e("board_icon_image_url");
  }
  
  public Leaderboard h()
  {
    return new LeaderboardEntity(this);
  }
  
  public int hashCode()
  {
    return LeaderboardEntity.a(this);
  }
  
  public String toString()
  {
    return LeaderboardEntity.b(this);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/leaderboard/LeaderboardRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */