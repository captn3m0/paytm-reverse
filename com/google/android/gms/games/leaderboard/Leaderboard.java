package com.google.android.gms.games.leaderboard;

import android.net.Uri;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.data.Freezable;
import com.google.android.gms.games.Game;
import java.util.ArrayList;

public abstract interface Leaderboard
  extends Freezable<Leaderboard>
{
  public abstract String b();
  
  public abstract String c();
  
  public abstract Uri d();
  
  public abstract int e();
  
  public abstract ArrayList<LeaderboardVariant> f();
  
  public abstract Game g();
  
  @Deprecated
  @KeepName
  public abstract String getIconImageUrl();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/leaderboard/Leaderboard.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */