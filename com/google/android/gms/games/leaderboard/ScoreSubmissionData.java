package com.google.android.gms.games.leaderboard;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.games.internal.constants.TimeSpan;
import java.util.HashMap;

public final class ScoreSubmissionData
{
  private static final String[] a = { "leaderboardId", "playerId", "timeSpan", "hasResult", "rawScore", "formattedScore", "newBest", "scoreTag" };
  private String b;
  private String c;
  private int d;
  private HashMap<Integer, Result> e;
  
  public ScoreSubmissionData(DataHolder paramDataHolder)
  {
    this.d = paramDataHolder.e();
    this.e = new HashMap();
    int j = paramDataHolder.g();
    if (j == 3) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.b(bool);
      int i = 0;
      while (i < j)
      {
        int k = paramDataHolder.a(i);
        if (i == 0)
        {
          this.b = paramDataHolder.c("leaderboardId", i, k);
          this.c = paramDataHolder.c("playerId", i, k);
        }
        if (paramDataHolder.d("hasResult", i, k)) {
          a(new Result(paramDataHolder.a("rawScore", i, k), paramDataHolder.c("formattedScore", i, k), paramDataHolder.c("scoreTag", i, k), paramDataHolder.d("newBest", i, k)), paramDataHolder.b("timeSpan", i, k));
        }
        i += 1;
      }
    }
  }
  
  private void a(Result paramResult, int paramInt)
  {
    this.e.put(Integer.valueOf(paramInt), paramResult);
  }
  
  public String toString()
  {
    zzw.zza localzza = zzw.a(this).a("PlayerId", this.c).a("StatusCode", Integer.valueOf(this.d));
    int i = 0;
    if (i < 3)
    {
      Object localObject = (Result)this.e.get(Integer.valueOf(i));
      localzza.a("TimesSpan", TimeSpan.a(i));
      if (localObject == null) {}
      for (localObject = "null";; localObject = ((Result)localObject).toString())
      {
        localzza.a("Result", localObject);
        i += 1;
        break;
      }
    }
    return localzza.toString();
  }
  
  public static final class Result
  {
    public final long a;
    public final String b;
    public final String c;
    public final boolean d;
    
    public Result(long paramLong, String paramString1, String paramString2, boolean paramBoolean)
    {
      this.a = paramLong;
      this.b = paramString1;
      this.c = paramString2;
      this.d = paramBoolean;
    }
    
    public String toString()
    {
      return zzw.a(this).a("RawScore", Long.valueOf(this.a)).a("FormattedScore", this.b).a("ScoreTag", this.c).a("NewBest", Boolean.valueOf(this.d)).toString();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/leaderboard/ScoreSubmissionData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */