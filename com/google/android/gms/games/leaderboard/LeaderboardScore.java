package com.google.android.gms.games.leaderboard;

import android.net.Uri;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.data.Freezable;
import com.google.android.gms.games.Player;

public abstract interface LeaderboardScore
  extends Freezable<LeaderboardScore>
{
  public abstract long b();
  
  public abstract String c();
  
  public abstract String d();
  
  public abstract long e();
  
  public abstract long f();
  
  public abstract String g();
  
  @Deprecated
  @KeepName
  public abstract String getScoreHolderHiResImageUrl();
  
  @Deprecated
  @KeepName
  public abstract String getScoreHolderIconImageUrl();
  
  public abstract Uri h();
  
  public abstract Uri i();
  
  public abstract Player j();
  
  public abstract String k();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/leaderboard/LeaderboardScore.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */