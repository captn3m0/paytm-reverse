package com.google.android.gms.games;

import com.google.android.gms.common.api.Result;

public abstract interface Notifications
{
  public static abstract interface ContactSettingLoadResult
    extends Result
  {}
  
  public static abstract interface GameMuteStatusChangeResult
    extends Result
  {}
  
  public static abstract interface GameMuteStatusLoadResult
    extends Result
  {}
  
  public static abstract interface InboxCountResult
    extends Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/Notifications.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */