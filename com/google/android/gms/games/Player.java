package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.data.Freezable;
import com.google.android.gms.games.internal.player.MostRecentGameInfo;

public abstract interface Player
  extends Parcelable, Freezable<Player>
{
  public abstract String b();
  
  public abstract String c();
  
  public abstract String d();
  
  public abstract String e();
  
  public abstract boolean f();
  
  public abstract Uri g();
  
  @Deprecated
  @KeepName
  public abstract String getBannerImageLandscapeUrl();
  
  @Deprecated
  @KeepName
  public abstract String getBannerImagePortraitUrl();
  
  @Deprecated
  @KeepName
  public abstract String getHiResImageUrl();
  
  @Deprecated
  @KeepName
  public abstract String getIconImageUrl();
  
  public abstract Uri h();
  
  public abstract long i();
  
  public abstract long j();
  
  public abstract int k();
  
  public abstract boolean l();
  
  public abstract String m();
  
  public abstract PlayerLevelInfo n();
  
  public abstract MostRecentGameInfo o();
  
  public abstract Uri p();
  
  public abstract Uri q();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/Player.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */