package com.google.android.gms.games;

import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;

public abstract interface Players
{
  public static abstract interface LoadPlayersResult
    extends Releasable, Result
  {}
  
  public static abstract interface LoadProfileSettingsResult
    extends Result
  {}
  
  public static abstract interface LoadXpForGameCategoriesResult
    extends Result
  {}
  
  public static abstract interface LoadXpForGamesResult
    extends Result
  {}
  
  public static abstract interface LoadXpStreamResult
    extends Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/Players.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */