package com.google.android.gms.games.multiplayer;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class ParticipantResultCreator
  implements Parcelable.Creator<ParticipantResult>
{
  static void a(ParticipantResult paramParticipantResult, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramParticipantResult.b(), false);
    zzb.a(paramParcel, 1000, paramParticipantResult.a());
    zzb.a(paramParcel, 2, paramParticipantResult.c());
    zzb.a(paramParcel, 3, paramParticipantResult.d());
    zzb.a(paramParcel, paramInt);
  }
  
  public ParticipantResult a(Parcel paramParcel)
  {
    int k = 0;
    int m = zza.b(paramParcel);
    String str = null;
    int j = 0;
    int i = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        str = zza.p(paramParcel, n);
        break;
      case 1000: 
        i = zza.g(paramParcel, n);
        break;
      case 2: 
        j = zza.g(paramParcel, n);
        break;
      case 3: 
        k = zza.g(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new ParticipantResult(i, str, j, k);
  }
  
  public ParticipantResult[] a(int paramInt)
  {
    return new ParticipantResult[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/multiplayer/ParticipantResultCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */