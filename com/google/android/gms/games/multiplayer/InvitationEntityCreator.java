package com.google.android.gms.games.multiplayer;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.games.GameEntity;
import java.util.ArrayList;

public class InvitationEntityCreator
  implements Parcelable.Creator<InvitationEntity>
{
  static void a(InvitationEntity paramInvitationEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramInvitationEntity.d(), paramInt, false);
    zzb.a(paramParcel, 1000, paramInvitationEntity.k());
    zzb.a(paramParcel, 2, paramInvitationEntity.e(), false);
    zzb.a(paramParcel, 3, paramInvitationEntity.g());
    zzb.a(paramParcel, 4, paramInvitationEntity.h());
    zzb.a(paramParcel, 5, paramInvitationEntity.f(), paramInt, false);
    zzb.c(paramParcel, 6, paramInvitationEntity.l(), false);
    zzb.a(paramParcel, 7, paramInvitationEntity.i());
    zzb.a(paramParcel, 8, paramInvitationEntity.j());
    zzb.a(paramParcel, i);
  }
  
  public InvitationEntity a(Parcel paramParcel)
  {
    ArrayList localArrayList = null;
    int i = 0;
    int n = zza.b(paramParcel);
    long l = 0L;
    int j = 0;
    ParticipantEntity localParticipantEntity = null;
    int k = 0;
    String str = null;
    GameEntity localGameEntity = null;
    int m = 0;
    while (paramParcel.dataPosition() < n)
    {
      int i1 = zza.a(paramParcel);
      switch (zza.a(i1))
      {
      default: 
        zza.b(paramParcel, i1);
        break;
      case 1: 
        localGameEntity = (GameEntity)zza.a(paramParcel, i1, GameEntity.CREATOR);
        break;
      case 1000: 
        m = zza.g(paramParcel, i1);
        break;
      case 2: 
        str = zza.p(paramParcel, i1);
        break;
      case 3: 
        l = zza.i(paramParcel, i1);
        break;
      case 4: 
        k = zza.g(paramParcel, i1);
        break;
      case 5: 
        localParticipantEntity = (ParticipantEntity)zza.a(paramParcel, i1, ParticipantEntity.CREATOR);
        break;
      case 6: 
        localArrayList = zza.c(paramParcel, i1, ParticipantEntity.CREATOR);
        break;
      case 7: 
        j = zza.g(paramParcel, i1);
        break;
      case 8: 
        i = zza.g(paramParcel, i1);
      }
    }
    if (paramParcel.dataPosition() != n) {
      throw new zza.zza("Overread allowed size end=" + n, paramParcel);
    }
    return new InvitationEntity(m, localGameEntity, str, l, k, localParticipantEntity, localArrayList, j, i);
  }
  
  public InvitationEntity[] a(int paramInt)
  {
    return new InvitationEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/multiplayer/InvitationEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */