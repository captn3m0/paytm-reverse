package com.google.android.gms.games.multiplayer;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.games.PlayerEntity;

public class ParticipantEntityCreator
  implements Parcelable.Creator<ParticipantEntity>
{
  static void a(ParticipantEntity paramParticipantEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramParticipantEntity.i(), false);
    zzb.a(paramParcel, 1000, paramParticipantEntity.l());
    zzb.a(paramParcel, 2, paramParticipantEntity.f(), false);
    zzb.a(paramParcel, 3, paramParticipantEntity.g(), paramInt, false);
    zzb.a(paramParcel, 4, paramParticipantEntity.h(), paramInt, false);
    zzb.a(paramParcel, 5, paramParticipantEntity.b());
    zzb.a(paramParcel, 6, paramParticipantEntity.c(), false);
    zzb.a(paramParcel, 7, paramParticipantEntity.e());
    zzb.a(paramParcel, 8, paramParticipantEntity.j(), paramInt, false);
    zzb.a(paramParcel, 9, paramParticipantEntity.d());
    zzb.a(paramParcel, 10, paramParticipantEntity.k(), paramInt, false);
    zzb.a(paramParcel, 11, paramParticipantEntity.getIconImageUrl(), false);
    zzb.a(paramParcel, 12, paramParticipantEntity.getHiResImageUrl(), false);
    zzb.a(paramParcel, i);
  }
  
  public ParticipantEntity a(Parcel paramParcel)
  {
    int m = zza.b(paramParcel);
    int k = 0;
    String str5 = null;
    String str4 = null;
    Uri localUri2 = null;
    Uri localUri1 = null;
    int j = 0;
    String str3 = null;
    boolean bool = false;
    PlayerEntity localPlayerEntity = null;
    int i = 0;
    ParticipantResult localParticipantResult = null;
    String str2 = null;
    String str1 = null;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        str5 = zza.p(paramParcel, n);
        break;
      case 1000: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        str4 = zza.p(paramParcel, n);
        break;
      case 3: 
        localUri2 = (Uri)zza.a(paramParcel, n, Uri.CREATOR);
        break;
      case 4: 
        localUri1 = (Uri)zza.a(paramParcel, n, Uri.CREATOR);
        break;
      case 5: 
        j = zza.g(paramParcel, n);
        break;
      case 6: 
        str3 = zza.p(paramParcel, n);
        break;
      case 7: 
        bool = zza.c(paramParcel, n);
        break;
      case 8: 
        localPlayerEntity = (PlayerEntity)zza.a(paramParcel, n, PlayerEntity.CREATOR);
        break;
      case 9: 
        i = zza.g(paramParcel, n);
        break;
      case 10: 
        localParticipantResult = (ParticipantResult)zza.a(paramParcel, n, ParticipantResult.CREATOR);
        break;
      case 11: 
        str2 = zza.p(paramParcel, n);
        break;
      case 12: 
        str1 = zza.p(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new ParticipantEntity(k, str5, str4, localUri2, localUri1, j, str3, bool, localPlayerEntity, i, localParticipantResult, str2, str1);
  }
  
  public ParticipantEntity[] a(int paramInt)
  {
    return new ParticipantEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/multiplayer/ParticipantEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */