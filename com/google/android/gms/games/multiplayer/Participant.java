package com.google.android.gms.games.multiplayer;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.data.Freezable;
import com.google.android.gms.games.Player;

public abstract interface Participant
  extends Parcelable, Freezable<Participant>
{
  public abstract int b();
  
  public abstract String c();
  
  public abstract int d();
  
  public abstract boolean e();
  
  public abstract String f();
  
  public abstract Uri g();
  
  @Deprecated
  @KeepName
  public abstract String getHiResImageUrl();
  
  @Deprecated
  @KeepName
  public abstract String getIconImageUrl();
  
  public abstract Uri h();
  
  public abstract String i();
  
  public abstract Player j();
  
  public abstract ParticipantResult k();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/multiplayer/Participant.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */