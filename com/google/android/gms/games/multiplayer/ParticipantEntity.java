package com.google.android.gms.games.multiplayer;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;

public final class ParticipantEntity
  extends GamesDowngradeableSafeParcel
  implements Participant
{
  public static final Parcelable.Creator<ParticipantEntity> CREATOR = new ParticipantEntityCreatorCompat();
  private final int a;
  private final String b;
  private final String c;
  private final Uri d;
  private final Uri e;
  private final int f;
  private final String g;
  private final boolean h;
  private final PlayerEntity i;
  private final int j;
  private final ParticipantResult k;
  private final String l;
  private final String m;
  
  ParticipantEntity(int paramInt1, String paramString1, String paramString2, Uri paramUri1, Uri paramUri2, int paramInt2, String paramString3, boolean paramBoolean, PlayerEntity paramPlayerEntity, int paramInt3, ParticipantResult paramParticipantResult, String paramString4, String paramString5)
  {
    this.a = paramInt1;
    this.b = paramString1;
    this.c = paramString2;
    this.d = paramUri1;
    this.e = paramUri2;
    this.f = paramInt2;
    this.g = paramString3;
    this.h = paramBoolean;
    this.i = paramPlayerEntity;
    this.j = paramInt3;
    this.k = paramParticipantResult;
    this.l = paramString4;
    this.m = paramString5;
  }
  
  public ParticipantEntity(Participant paramParticipant)
  {
    this.a = 3;
    this.b = paramParticipant.i();
    this.c = paramParticipant.f();
    this.d = paramParticipant.g();
    this.e = paramParticipant.h();
    this.f = paramParticipant.b();
    this.g = paramParticipant.c();
    this.h = paramParticipant.e();
    Object localObject = paramParticipant.j();
    if (localObject == null) {}
    for (localObject = null;; localObject = new PlayerEntity((Player)localObject))
    {
      this.i = ((PlayerEntity)localObject);
      this.j = paramParticipant.d();
      this.k = paramParticipant.k();
      this.l = paramParticipant.getIconImageUrl();
      this.m = paramParticipant.getHiResImageUrl();
      return;
    }
  }
  
  static int a(Participant paramParticipant)
  {
    return zzw.a(new Object[] { paramParticipant.j(), Integer.valueOf(paramParticipant.b()), paramParticipant.c(), Boolean.valueOf(paramParticipant.e()), paramParticipant.f(), paramParticipant.g(), paramParticipant.h(), Integer.valueOf(paramParticipant.d()), paramParticipant.k(), paramParticipant.i() });
  }
  
  static boolean a(Participant paramParticipant, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof Participant)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramParticipant == paramObject);
      paramObject = (Participant)paramObject;
      if ((!zzw.a(((Participant)paramObject).j(), paramParticipant.j())) || (!zzw.a(Integer.valueOf(((Participant)paramObject).b()), Integer.valueOf(paramParticipant.b()))) || (!zzw.a(((Participant)paramObject).c(), paramParticipant.c())) || (!zzw.a(Boolean.valueOf(((Participant)paramObject).e()), Boolean.valueOf(paramParticipant.e()))) || (!zzw.a(((Participant)paramObject).f(), paramParticipant.f())) || (!zzw.a(((Participant)paramObject).g(), paramParticipant.g())) || (!zzw.a(((Participant)paramObject).h(), paramParticipant.h())) || (!zzw.a(Integer.valueOf(((Participant)paramObject).d()), Integer.valueOf(paramParticipant.d()))) || (!zzw.a(((Participant)paramObject).k(), paramParticipant.k()))) {
        break;
      }
      bool1 = bool2;
    } while (zzw.a(((Participant)paramObject).i(), paramParticipant.i()));
    return false;
  }
  
  static String b(Participant paramParticipant)
  {
    return zzw.a(paramParticipant).a("ParticipantId", paramParticipant.i()).a("Player", paramParticipant.j()).a("Status", Integer.valueOf(paramParticipant.b())).a("ClientAddress", paramParticipant.c()).a("ConnectedToRoom", Boolean.valueOf(paramParticipant.e())).a("DisplayName", paramParticipant.f()).a("IconImage", paramParticipant.g()).a("IconImageUrl", paramParticipant.getIconImageUrl()).a("HiResImage", paramParticipant.h()).a("HiResImageUrl", paramParticipant.getHiResImageUrl()).a("Capabilities", Integer.valueOf(paramParticipant.d())).a("Result", paramParticipant.k()).toString();
  }
  
  public int b()
  {
    return this.f;
  }
  
  public String c()
  {
    return this.g;
  }
  
  public int d()
  {
    return this.j;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean e()
  {
    return this.h;
  }
  
  public boolean equals(Object paramObject)
  {
    return a(this, paramObject);
  }
  
  public String f()
  {
    if (this.i == null) {
      return this.c;
    }
    return this.i.c();
  }
  
  public Uri g()
  {
    if (this.i == null) {
      return this.d;
    }
    return this.i.g();
  }
  
  public String getHiResImageUrl()
  {
    if (this.i == null) {
      return this.m;
    }
    return this.i.getHiResImageUrl();
  }
  
  public String getIconImageUrl()
  {
    if (this.i == null) {
      return this.l;
    }
    return this.i.getIconImageUrl();
  }
  
  public Uri h()
  {
    if (this.i == null) {
      return this.e;
    }
    return this.i.h();
  }
  
  public int hashCode()
  {
    return a(this);
  }
  
  public String i()
  {
    return this.b;
  }
  
  public Player j()
  {
    return this.i;
  }
  
  public ParticipantResult k()
  {
    return this.k;
  }
  
  public int l()
  {
    return this.a;
  }
  
  public Participant m()
  {
    return this;
  }
  
  public String toString()
  {
    return b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    Object localObject2 = null;
    int i1 = 0;
    if (!k_())
    {
      ParticipantEntityCreator.a(this, paramParcel, paramInt);
      return;
    }
    paramParcel.writeString(this.b);
    paramParcel.writeString(this.c);
    Object localObject1;
    if (this.d == null)
    {
      localObject1 = null;
      label46:
      paramParcel.writeString((String)localObject1);
      if (this.e != null) {
        break label143;
      }
      localObject1 = localObject2;
      label63:
      paramParcel.writeString((String)localObject1);
      paramParcel.writeInt(this.f);
      paramParcel.writeString(this.g);
      if (!this.h) {
        break label155;
      }
      n = 1;
      label94:
      paramParcel.writeInt(n);
      if (this.i != null) {
        break label160;
      }
    }
    label143:
    label155:
    label160:
    for (int n = i1;; n = 1)
    {
      paramParcel.writeInt(n);
      if (this.i == null) {
        break;
      }
      this.i.writeToParcel(paramParcel, paramInt);
      return;
      localObject1 = this.d.toString();
      break label46;
      localObject1 = this.e.toString();
      break label63;
      n = 0;
      break label94;
    }
  }
  
  static final class ParticipantEntityCreatorCompat
    extends ParticipantEntityCreator
  {
    public ParticipantEntity a(Parcel paramParcel)
    {
      int i = 1;
      if ((ParticipantEntity.a(ParticipantEntity.n())) || (ParticipantEntity.b(ParticipantEntity.class.getCanonicalName()))) {
        return super.a(paramParcel);
      }
      String str1 = paramParcel.readString();
      String str2 = paramParcel.readString();
      Object localObject1 = paramParcel.readString();
      Object localObject2;
      label68:
      int j;
      String str3;
      boolean bool;
      if (localObject1 == null)
      {
        localObject1 = null;
        localObject2 = paramParcel.readString();
        if (localObject2 != null) {
          break label151;
        }
        localObject2 = null;
        j = paramParcel.readInt();
        str3 = paramParcel.readString();
        if (paramParcel.readInt() <= 0) {
          break label161;
        }
        bool = true;
        label89:
        if (paramParcel.readInt() <= 0) {
          break label167;
        }
        label96:
        if (i == 0) {
          break label172;
        }
      }
      label151:
      label161:
      label167:
      label172:
      for (paramParcel = (PlayerEntity)PlayerEntity.CREATOR.createFromParcel(paramParcel);; paramParcel = null)
      {
        return new ParticipantEntity(3, str1, str2, (Uri)localObject1, (Uri)localObject2, j, str3, bool, paramParcel, 7, null, null, null);
        localObject1 = Uri.parse((String)localObject1);
        break;
        localObject2 = Uri.parse((String)localObject2);
        break label68;
        bool = false;
        break label89;
        i = 0;
        break label96;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/multiplayer/ParticipantEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */