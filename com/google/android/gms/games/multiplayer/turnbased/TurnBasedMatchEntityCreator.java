package com.google.android.gms.games.multiplayer.turnbased;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.multiplayer.ParticipantEntity;
import java.util.ArrayList;

public class TurnBasedMatchEntityCreator
  implements Parcelable.Creator<TurnBasedMatchEntity>
{
  static void a(TurnBasedMatchEntity paramTurnBasedMatchEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramTurnBasedMatchEntity.b(), paramInt, false);
    zzb.a(paramParcel, 2, paramTurnBasedMatchEntity.c(), false);
    zzb.a(paramParcel, 3, paramTurnBasedMatchEntity.d(), false);
    zzb.a(paramParcel, 4, paramTurnBasedMatchEntity.e());
    zzb.a(paramParcel, 5, paramTurnBasedMatchEntity.j(), false);
    zzb.a(paramParcel, 6, paramTurnBasedMatchEntity.k());
    zzb.a(paramParcel, 7, paramTurnBasedMatchEntity.m(), false);
    zzb.a(paramParcel, 8, paramTurnBasedMatchEntity.f());
    zzb.a(paramParcel, 10, paramTurnBasedMatchEntity.i());
    zzb.a(paramParcel, 11, paramTurnBasedMatchEntity.o());
    zzb.a(paramParcel, 12, paramTurnBasedMatchEntity.n(), false);
    zzb.c(paramParcel, 13, paramTurnBasedMatchEntity.l(), false);
    zzb.a(paramParcel, 14, paramTurnBasedMatchEntity.p(), false);
    zzb.a(paramParcel, 15, paramTurnBasedMatchEntity.q(), false);
    zzb.a(paramParcel, 17, paramTurnBasedMatchEntity.s(), false);
    zzb.a(paramParcel, 16, paramTurnBasedMatchEntity.r());
    zzb.a(paramParcel, 1000, paramTurnBasedMatchEntity.w());
    zzb.a(paramParcel, 19, paramTurnBasedMatchEntity.u());
    zzb.a(paramParcel, 18, paramTurnBasedMatchEntity.g());
    zzb.a(paramParcel, 21, paramTurnBasedMatchEntity.v(), false);
    zzb.a(paramParcel, 20, paramTurnBasedMatchEntity.h(), false);
    zzb.a(paramParcel, i);
  }
  
  public TurnBasedMatchEntity a(Parcel paramParcel)
  {
    int i2 = zza.b(paramParcel);
    int i1 = 0;
    GameEntity localGameEntity = null;
    String str7 = null;
    String str6 = null;
    long l2 = 0L;
    String str5 = null;
    long l1 = 0L;
    String str4 = null;
    int n = 0;
    int m = 0;
    int k = 0;
    byte[] arrayOfByte2 = null;
    ArrayList localArrayList = null;
    String str3 = null;
    byte[] arrayOfByte1 = null;
    int j = 0;
    Bundle localBundle = null;
    int i = 0;
    boolean bool = false;
    String str2 = null;
    String str1 = null;
    while (paramParcel.dataPosition() < i2)
    {
      int i3 = zza.a(paramParcel);
      switch (zza.a(i3))
      {
      default: 
        zza.b(paramParcel, i3);
        break;
      case 1: 
        localGameEntity = (GameEntity)zza.a(paramParcel, i3, GameEntity.CREATOR);
        break;
      case 2: 
        str7 = zza.p(paramParcel, i3);
        break;
      case 3: 
        str6 = zza.p(paramParcel, i3);
        break;
      case 4: 
        l2 = zza.i(paramParcel, i3);
        break;
      case 5: 
        str5 = zza.p(paramParcel, i3);
        break;
      case 6: 
        l1 = zza.i(paramParcel, i3);
        break;
      case 7: 
        str4 = zza.p(paramParcel, i3);
        break;
      case 8: 
        n = zza.g(paramParcel, i3);
        break;
      case 10: 
        m = zza.g(paramParcel, i3);
        break;
      case 11: 
        k = zza.g(paramParcel, i3);
        break;
      case 12: 
        arrayOfByte2 = zza.s(paramParcel, i3);
        break;
      case 13: 
        localArrayList = zza.c(paramParcel, i3, ParticipantEntity.CREATOR);
        break;
      case 14: 
        str3 = zza.p(paramParcel, i3);
        break;
      case 15: 
        arrayOfByte1 = zza.s(paramParcel, i3);
        break;
      case 17: 
        localBundle = zza.r(paramParcel, i3);
        break;
      case 16: 
        j = zza.g(paramParcel, i3);
        break;
      case 1000: 
        i1 = zza.g(paramParcel, i3);
        break;
      case 19: 
        bool = zza.c(paramParcel, i3);
        break;
      case 18: 
        i = zza.g(paramParcel, i3);
        break;
      case 21: 
        str1 = zza.p(paramParcel, i3);
        break;
      case 20: 
        str2 = zza.p(paramParcel, i3);
      }
    }
    if (paramParcel.dataPosition() != i2) {
      throw new zza.zza("Overread allowed size end=" + i2, paramParcel);
    }
    return new TurnBasedMatchEntity(i1, localGameEntity, str7, str6, l2, str5, l1, str4, n, m, k, arrayOfByte2, localArrayList, str3, arrayOfByte1, j, localBundle, i, bool, str2, str1);
  }
  
  public TurnBasedMatchEntity[] a(int paramInt)
  {
    return new TurnBasedMatchEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */