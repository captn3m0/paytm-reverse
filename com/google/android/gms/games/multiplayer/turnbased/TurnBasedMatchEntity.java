package com.google.android.gms.games.multiplayer.turnbased;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.ParticipantEntity;
import java.util.ArrayList;

public final class TurnBasedMatchEntity
  implements SafeParcelable, TurnBasedMatch
{
  public static final Parcelable.Creator<TurnBasedMatchEntity> CREATOR = new TurnBasedMatchEntityCreator();
  private final int a;
  private final GameEntity b;
  private final String c;
  private final String d;
  private final long e;
  private final String f;
  private final long g;
  private final String h;
  private final int i;
  private final int j;
  private final int k;
  private final byte[] l;
  private final ArrayList<ParticipantEntity> m;
  private final String n;
  private final byte[] o;
  private final int p;
  private final Bundle q;
  private final int r;
  private final boolean s;
  private final String t;
  private final String u;
  
  TurnBasedMatchEntity(int paramInt1, GameEntity paramGameEntity, String paramString1, String paramString2, long paramLong1, String paramString3, long paramLong2, String paramString4, int paramInt2, int paramInt3, int paramInt4, byte[] paramArrayOfByte1, ArrayList<ParticipantEntity> paramArrayList, String paramString5, byte[] paramArrayOfByte2, int paramInt5, Bundle paramBundle, int paramInt6, boolean paramBoolean, String paramString6, String paramString7)
  {
    this.a = paramInt1;
    this.b = paramGameEntity;
    this.c = paramString1;
    this.d = paramString2;
    this.e = paramLong1;
    this.f = paramString3;
    this.g = paramLong2;
    this.h = paramString4;
    this.i = paramInt2;
    this.r = paramInt6;
    this.j = paramInt3;
    this.k = paramInt4;
    this.l = paramArrayOfByte1;
    this.m = paramArrayList;
    this.n = paramString5;
    this.o = paramArrayOfByte2;
    this.p = paramInt5;
    this.q = paramBundle;
    this.s = paramBoolean;
    this.t = paramString6;
    this.u = paramString7;
  }
  
  public TurnBasedMatchEntity(TurnBasedMatch paramTurnBasedMatch)
  {
    this.a = 2;
    this.b = new GameEntity(paramTurnBasedMatch.b());
    this.c = paramTurnBasedMatch.c();
    this.d = paramTurnBasedMatch.d();
    this.e = paramTurnBasedMatch.e();
    this.f = paramTurnBasedMatch.j();
    this.g = paramTurnBasedMatch.k();
    this.h = paramTurnBasedMatch.m();
    this.i = paramTurnBasedMatch.f();
    this.r = paramTurnBasedMatch.g();
    this.j = paramTurnBasedMatch.i();
    this.k = paramTurnBasedMatch.o();
    this.n = paramTurnBasedMatch.p();
    this.p = paramTurnBasedMatch.r();
    this.q = paramTurnBasedMatch.s();
    this.s = paramTurnBasedMatch.u();
    this.t = paramTurnBasedMatch.h();
    this.u = paramTurnBasedMatch.v();
    byte[] arrayOfByte = paramTurnBasedMatch.n();
    if (arrayOfByte == null)
    {
      this.l = null;
      arrayOfByte = paramTurnBasedMatch.q();
      if (arrayOfByte != null) {
        break label313;
      }
      this.o = null;
    }
    for (;;)
    {
      paramTurnBasedMatch = paramTurnBasedMatch.l();
      int i2 = paramTurnBasedMatch.size();
      this.m = new ArrayList(i2);
      int i1 = 0;
      while (i1 < i2)
      {
        this.m.add((ParticipantEntity)((Participant)paramTurnBasedMatch.get(i1)).a());
        i1 += 1;
      }
      this.l = new byte[arrayOfByte.length];
      System.arraycopy(arrayOfByte, 0, this.l, 0, arrayOfByte.length);
      break;
      label313:
      this.o = new byte[arrayOfByte.length];
      System.arraycopy(arrayOfByte, 0, this.o, 0, arrayOfByte.length);
    }
  }
  
  static int a(TurnBasedMatch paramTurnBasedMatch)
  {
    return zzw.a(new Object[] { paramTurnBasedMatch.b(), paramTurnBasedMatch.c(), paramTurnBasedMatch.d(), Long.valueOf(paramTurnBasedMatch.e()), paramTurnBasedMatch.j(), Long.valueOf(paramTurnBasedMatch.k()), paramTurnBasedMatch.m(), Integer.valueOf(paramTurnBasedMatch.f()), Integer.valueOf(paramTurnBasedMatch.g()), paramTurnBasedMatch.h(), Integer.valueOf(paramTurnBasedMatch.i()), Integer.valueOf(paramTurnBasedMatch.o()), paramTurnBasedMatch.l(), paramTurnBasedMatch.p(), Integer.valueOf(paramTurnBasedMatch.r()), paramTurnBasedMatch.s(), Integer.valueOf(paramTurnBasedMatch.t()), Boolean.valueOf(paramTurnBasedMatch.u()) });
  }
  
  static boolean a(TurnBasedMatch paramTurnBasedMatch, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof TurnBasedMatch)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramTurnBasedMatch == paramObject);
      paramObject = (TurnBasedMatch)paramObject;
      if ((!zzw.a(((TurnBasedMatch)paramObject).b(), paramTurnBasedMatch.b())) || (!zzw.a(((TurnBasedMatch)paramObject).c(), paramTurnBasedMatch.c())) || (!zzw.a(((TurnBasedMatch)paramObject).d(), paramTurnBasedMatch.d())) || (!zzw.a(Long.valueOf(((TurnBasedMatch)paramObject).e()), Long.valueOf(paramTurnBasedMatch.e()))) || (!zzw.a(((TurnBasedMatch)paramObject).j(), paramTurnBasedMatch.j())) || (!zzw.a(Long.valueOf(((TurnBasedMatch)paramObject).k()), Long.valueOf(paramTurnBasedMatch.k()))) || (!zzw.a(((TurnBasedMatch)paramObject).m(), paramTurnBasedMatch.m())) || (!zzw.a(Integer.valueOf(((TurnBasedMatch)paramObject).f()), Integer.valueOf(paramTurnBasedMatch.f()))) || (!zzw.a(Integer.valueOf(((TurnBasedMatch)paramObject).g()), Integer.valueOf(paramTurnBasedMatch.g()))) || (!zzw.a(((TurnBasedMatch)paramObject).h(), paramTurnBasedMatch.h())) || (!zzw.a(Integer.valueOf(((TurnBasedMatch)paramObject).i()), Integer.valueOf(paramTurnBasedMatch.i()))) || (!zzw.a(Integer.valueOf(((TurnBasedMatch)paramObject).o()), Integer.valueOf(paramTurnBasedMatch.o()))) || (!zzw.a(((TurnBasedMatch)paramObject).l(), paramTurnBasedMatch.l())) || (!zzw.a(((TurnBasedMatch)paramObject).p(), paramTurnBasedMatch.p())) || (!zzw.a(Integer.valueOf(((TurnBasedMatch)paramObject).r()), Integer.valueOf(paramTurnBasedMatch.r()))) || (!zzw.a(((TurnBasedMatch)paramObject).s(), paramTurnBasedMatch.s())) || (!zzw.a(Integer.valueOf(((TurnBasedMatch)paramObject).t()), Integer.valueOf(paramTurnBasedMatch.t())))) {
        break;
      }
      bool1 = bool2;
    } while (zzw.a(Boolean.valueOf(((TurnBasedMatch)paramObject).u()), Boolean.valueOf(paramTurnBasedMatch.u())));
    return false;
  }
  
  static String b(TurnBasedMatch paramTurnBasedMatch)
  {
    return zzw.a(paramTurnBasedMatch).a("Game", paramTurnBasedMatch.b()).a("MatchId", paramTurnBasedMatch.c()).a("CreatorId", paramTurnBasedMatch.d()).a("CreationTimestamp", Long.valueOf(paramTurnBasedMatch.e())).a("LastUpdaterId", paramTurnBasedMatch.j()).a("LastUpdatedTimestamp", Long.valueOf(paramTurnBasedMatch.k())).a("PendingParticipantId", paramTurnBasedMatch.m()).a("MatchStatus", Integer.valueOf(paramTurnBasedMatch.f())).a("TurnStatus", Integer.valueOf(paramTurnBasedMatch.g())).a("Description", paramTurnBasedMatch.h()).a("Variant", Integer.valueOf(paramTurnBasedMatch.i())).a("Data", paramTurnBasedMatch.n()).a("Version", Integer.valueOf(paramTurnBasedMatch.o())).a("Participants", paramTurnBasedMatch.l()).a("RematchId", paramTurnBasedMatch.p()).a("PreviousData", paramTurnBasedMatch.q()).a("MatchNumber", Integer.valueOf(paramTurnBasedMatch.r())).a("AutoMatchCriteria", paramTurnBasedMatch.s()).a("AvailableAutoMatchSlots", Integer.valueOf(paramTurnBasedMatch.t())).a("LocallyModified", Boolean.valueOf(paramTurnBasedMatch.u())).a("DescriptionParticipantId", paramTurnBasedMatch.v()).toString();
  }
  
  public Game b()
  {
    return this.b;
  }
  
  public String c()
  {
    return this.c;
  }
  
  public String d()
  {
    return this.d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public long e()
  {
    return this.e;
  }
  
  public boolean equals(Object paramObject)
  {
    return a(this, paramObject);
  }
  
  public int f()
  {
    return this.i;
  }
  
  public int g()
  {
    return this.r;
  }
  
  public String h()
  {
    return this.t;
  }
  
  public int hashCode()
  {
    return a(this);
  }
  
  public int i()
  {
    return this.j;
  }
  
  public String j()
  {
    return this.f;
  }
  
  public long k()
  {
    return this.g;
  }
  
  public ArrayList<Participant> l()
  {
    return new ArrayList(this.m);
  }
  
  public String m()
  {
    return this.h;
  }
  
  public byte[] n()
  {
    return this.l;
  }
  
  public int o()
  {
    return this.k;
  }
  
  public String p()
  {
    return this.n;
  }
  
  public byte[] q()
  {
    return this.o;
  }
  
  public int r()
  {
    return this.p;
  }
  
  public Bundle s()
  {
    return this.q;
  }
  
  public int t()
  {
    if (this.q == null) {
      return 0;
    }
    return this.q.getInt("max_automatch_players");
  }
  
  public String toString()
  {
    return b(this);
  }
  
  public boolean u()
  {
    return this.s;
  }
  
  public String v()
  {
    return this.u;
  }
  
  public int w()
  {
    return this.a;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    TurnBasedMatchEntityCreator.a(this, paramParcel, paramInt);
  }
  
  public TurnBasedMatch x()
  {
    return this;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */