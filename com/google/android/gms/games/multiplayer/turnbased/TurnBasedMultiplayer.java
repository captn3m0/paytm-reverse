package com.google.android.gms.games.multiplayer.turnbased;

import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;

public abstract interface TurnBasedMultiplayer
{
  public static abstract interface CancelMatchResult
    extends Result
  {}
  
  public static abstract interface InitiateMatchResult
    extends Result
  {}
  
  public static abstract interface LeaveMatchResult
    extends Result
  {}
  
  public static abstract interface LoadMatchResult
    extends Result
  {}
  
  public static abstract interface LoadMatchesResult
    extends Releasable, Result
  {}
  
  public static abstract interface UpdateMatchResult
    extends Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/multiplayer/turnbased/TurnBasedMultiplayer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */