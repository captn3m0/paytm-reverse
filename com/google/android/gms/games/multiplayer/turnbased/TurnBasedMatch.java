package com.google.android.gms.games.multiplayer.turnbased;

import android.os.Bundle;
import android.os.Parcelable;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.data.Freezable;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.multiplayer.Participatable;

public abstract interface TurnBasedMatch
  extends Parcelable, Freezable<TurnBasedMatch>, Participatable
{
  @KeepName
  public static final int[] MATCH_TURN_STATUS_ALL = { 0, 1, 2, 3 };
  
  public abstract Game b();
  
  public abstract String c();
  
  public abstract String d();
  
  public abstract long e();
  
  public abstract int f();
  
  public abstract int g();
  
  public abstract String h();
  
  public abstract int i();
  
  public abstract String j();
  
  public abstract long k();
  
  public abstract String m();
  
  public abstract byte[] n();
  
  public abstract int o();
  
  public abstract String p();
  
  public abstract byte[] q();
  
  public abstract int r();
  
  public abstract Bundle s();
  
  public abstract int t();
  
  public abstract boolean u();
  
  public abstract String v();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */