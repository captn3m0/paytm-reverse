package com.google.android.gms.games.multiplayer.turnbased;

public abstract interface OnTurnBasedMatchUpdateReceivedListener
{
  public abstract void a(TurnBasedMatch paramTurnBasedMatch);
  
  public abstract void a(String paramString);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/multiplayer/turnbased/OnTurnBasedMatchUpdateReceivedListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */