package com.google.android.gms.games.multiplayer.turnbased;

import android.os.Bundle;

public final class TurnBasedMatchConfigImpl
  extends TurnBasedMatchConfig
{
  private final int a;
  private final String[] b;
  private final Bundle c;
  private final int d;
  
  public int a()
  {
    return this.a;
  }
  
  public int b()
  {
    return this.d;
  }
  
  public String[] c()
  {
    return this.b;
  }
  
  public Bundle d()
  {
    return this.c;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/multiplayer/turnbased/TurnBasedMatchConfigImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */