package com.google.android.gms.games.multiplayer;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerRef;

public final class ParticipantRef
  extends zzc
  implements Participant
{
  private final PlayerRef c;
  
  public ParticipantRef(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
    this.c = new PlayerRef(paramDataHolder, paramInt);
  }
  
  public int b()
  {
    return c("player_status");
  }
  
  public String c()
  {
    return e("client_address");
  }
  
  public int d()
  {
    return c("capabilities");
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean e()
  {
    return c("connected") > 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return ParticipantEntity.a(this, paramObject);
  }
  
  public String f()
  {
    if (i("external_player_id")) {
      return e("default_display_name");
    }
    return this.c.c();
  }
  
  public Uri g()
  {
    if (i("external_player_id")) {
      return h("default_display_image_uri");
    }
    return this.c.g();
  }
  
  public String getHiResImageUrl()
  {
    if (i("external_player_id")) {
      return e("default_display_hi_res_image_url");
    }
    return this.c.getHiResImageUrl();
  }
  
  public String getIconImageUrl()
  {
    if (i("external_player_id")) {
      return e("default_display_image_url");
    }
    return this.c.getIconImageUrl();
  }
  
  public Uri h()
  {
    if (i("external_player_id")) {
      return h("default_display_hi_res_image_uri");
    }
    return this.c.h();
  }
  
  public int hashCode()
  {
    return ParticipantEntity.a(this);
  }
  
  public String i()
  {
    return e("external_participant_id");
  }
  
  public Player j()
  {
    if (i("external_player_id")) {
      return null;
    }
    return this.c;
  }
  
  public ParticipantResult k()
  {
    if (i("result_type")) {
      return null;
    }
    int i = c("result_type");
    int j = c("placing");
    return new ParticipantResult(i(), i, j);
  }
  
  public Participant l()
  {
    return new ParticipantEntity(this);
  }
  
  public String toString()
  {
    return ParticipantEntity.b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((ParticipantEntity)l()).writeToParcel(paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/multiplayer/ParticipantRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */