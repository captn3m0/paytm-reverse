package com.google.android.gms.games.multiplayer.realtime;

public abstract interface RealTimeMultiplayer
{
  public static abstract interface ReliableMessageSentCallback
  {
    public abstract void a(int paramInt1, int paramInt2, String paramString);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/multiplayer/realtime/RealTimeMultiplayer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */