package com.google.android.gms.games.multiplayer.realtime;

import java.util.List;

public abstract interface RoomStatusUpdateListener
{
  public abstract void a(Room paramRoom);
  
  public abstract void a(Room paramRoom, List<String> paramList);
  
  public abstract void a(String paramString);
  
  public abstract void b(Room paramRoom);
  
  public abstract void b(Room paramRoom, List<String> paramList);
  
  public abstract void b(String paramString);
  
  public abstract void c(Room paramRoom);
  
  public abstract void c(Room paramRoom, List<String> paramList);
  
  public abstract void d(Room paramRoom);
  
  public abstract void d(Room paramRoom, List<String> paramList);
  
  public abstract void e(Room paramRoom, List<String> paramList);
  
  public abstract void f(Room paramRoom, List<String> paramList);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/multiplayer/realtime/RoomStatusUpdateListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */