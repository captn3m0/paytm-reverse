package com.google.android.gms.games.multiplayer.realtime;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.games.multiplayer.ParticipantEntity;
import java.util.ArrayList;

public class RoomEntityCreator
  implements Parcelable.Creator<RoomEntity>
{
  static void a(RoomEntity paramRoomEntity, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramRoomEntity.b(), false);
    zzb.a(paramParcel, 1000, paramRoomEntity.j());
    zzb.a(paramParcel, 2, paramRoomEntity.c(), false);
    zzb.a(paramParcel, 3, paramRoomEntity.d());
    zzb.a(paramParcel, 4, paramRoomEntity.e());
    zzb.a(paramParcel, 5, paramRoomEntity.f(), false);
    zzb.a(paramParcel, 6, paramRoomEntity.g());
    zzb.a(paramParcel, 7, paramRoomEntity.h(), false);
    zzb.c(paramParcel, 8, paramRoomEntity.l(), false);
    zzb.a(paramParcel, 9, paramRoomEntity.i());
    zzb.a(paramParcel, paramInt);
  }
  
  public RoomEntity a(Parcel paramParcel)
  {
    int i = 0;
    ArrayList localArrayList = null;
    int n = zza.b(paramParcel);
    long l = 0L;
    Bundle localBundle = null;
    int j = 0;
    String str1 = null;
    int k = 0;
    String str2 = null;
    String str3 = null;
    int m = 0;
    while (paramParcel.dataPosition() < n)
    {
      int i1 = zza.a(paramParcel);
      switch (zza.a(i1))
      {
      default: 
        zza.b(paramParcel, i1);
        break;
      case 1: 
        str3 = zza.p(paramParcel, i1);
        break;
      case 1000: 
        m = zza.g(paramParcel, i1);
        break;
      case 2: 
        str2 = zza.p(paramParcel, i1);
        break;
      case 3: 
        l = zza.i(paramParcel, i1);
        break;
      case 4: 
        k = zza.g(paramParcel, i1);
        break;
      case 5: 
        str1 = zza.p(paramParcel, i1);
        break;
      case 6: 
        j = zza.g(paramParcel, i1);
        break;
      case 7: 
        localBundle = zza.r(paramParcel, i1);
        break;
      case 8: 
        localArrayList = zza.c(paramParcel, i1, ParticipantEntity.CREATOR);
        break;
      case 9: 
        i = zza.g(paramParcel, i1);
      }
    }
    if (paramParcel.dataPosition() != n) {
      throw new zza.zza("Overread allowed size end=" + n, paramParcel);
    }
    return new RoomEntity(m, str3, str2, l, k, str1, j, localBundle, localArrayList, i);
  }
  
  public RoomEntity[] a(int paramInt)
  {
    return new RoomEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/multiplayer/realtime/RoomEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */