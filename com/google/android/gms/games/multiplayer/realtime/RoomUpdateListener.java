package com.google.android.gms.games.multiplayer.realtime;

public abstract interface RoomUpdateListener
{
  public abstract void a(int paramInt, Room paramRoom);
  
  public abstract void a(int paramInt, String paramString);
  
  public abstract void b(int paramInt, Room paramRoom);
  
  public abstract void c(int paramInt, Room paramRoom);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/multiplayer/realtime/RoomUpdateListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */