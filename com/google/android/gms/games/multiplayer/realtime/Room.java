package com.google.android.gms.games.multiplayer.realtime;

import android.os.Bundle;
import android.os.Parcelable;
import com.google.android.gms.common.data.Freezable;
import com.google.android.gms.games.multiplayer.Participatable;

public abstract interface Room
  extends Parcelable, Freezable<Room>, Participatable
{
  public abstract String b();
  
  public abstract String c();
  
  public abstract long d();
  
  public abstract int e();
  
  public abstract String f();
  
  public abstract int g();
  
  public abstract Bundle h();
  
  public abstract int i();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/multiplayer/realtime/Room.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */