package com.google.android.gms.games.multiplayer;

import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameRef;
import java.util.ArrayList;

public final class InvitationRef
  extends zzc
  implements Invitation
{
  private final Game c;
  private final ParticipantRef d;
  private final ArrayList<Participant> e;
  
  InvitationRef(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    super(paramDataHolder, paramInt1);
    this.c = new GameRef(paramDataHolder, paramInt1);
    this.e = new ArrayList(paramInt2);
    String str = e("external_inviter_id");
    paramInt1 = 0;
    paramDataHolder = null;
    while (paramInt1 < paramInt2)
    {
      ParticipantRef localParticipantRef = new ParticipantRef(this.a, this.b + paramInt1);
      if (localParticipantRef.i().equals(str)) {
        paramDataHolder = localParticipantRef;
      }
      this.e.add(localParticipantRef);
      paramInt1 += 1;
    }
    this.d = ((ParticipantRef)zzx.a(paramDataHolder, "Must have a valid inviter!"));
  }
  
  public Invitation c()
  {
    return new InvitationEntity(this);
  }
  
  public Game d()
  {
    return this.c;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String e()
  {
    return e("external_invitation_id");
  }
  
  public boolean equals(Object paramObject)
  {
    return InvitationEntity.a(this, paramObject);
  }
  
  public Participant f()
  {
    return this.d;
  }
  
  public long g()
  {
    return Math.max(b("creation_timestamp"), b("last_modified_timestamp"));
  }
  
  public int h()
  {
    return c("type");
  }
  
  public int hashCode()
  {
    return InvitationEntity.a(this);
  }
  
  public int i()
  {
    return c("variant");
  }
  
  public int j()
  {
    if (!d("has_automatch_criteria")) {
      return 0;
    }
    return c("automatch_max_players");
  }
  
  public ArrayList<Participant> l()
  {
    return this.e;
  }
  
  public String toString()
  {
    return InvitationEntity.b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((InvitationEntity)c()).writeToParcel(paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/multiplayer/InvitationRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */