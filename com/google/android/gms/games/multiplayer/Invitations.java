package com.google.android.gms.games.multiplayer;

import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;

public abstract interface Invitations
{
  public static abstract interface LoadInvitationsResult
    extends Releasable, Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/multiplayer/Invitations.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */