package com.google.android.gms.games.quest;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.data.Freezable;
import com.google.android.gms.games.Game;
import java.util.List;

public abstract interface Quest
  extends Parcelable, Freezable<Quest>
{
  @KeepName
  public static final int[] QUEST_STATE_ALL = { 1, 2, 3, 4, 6, 5 };
  @KeepName
  public static final String[] QUEST_STATE_NON_TERMINAL = { Integer.toString(1), Integer.toString(2), Integer.toString(3) };
  
  public abstract String b();
  
  public abstract String c();
  
  public abstract String d();
  
  public abstract Uri e();
  
  public abstract Uri f();
  
  public abstract List<Milestone> g();
  
  @Deprecated
  @KeepName
  public abstract String getBannerImageUrl();
  
  @Deprecated
  @KeepName
  public abstract String getIconImageUrl();
  
  public abstract Game h();
  
  public abstract int i();
  
  public abstract int j();
  
  public abstract long k();
  
  public abstract long l();
  
  public abstract long m();
  
  public abstract long n();
  
  public abstract long o();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/quest/Quest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */