package com.google.android.gms.games.quest;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.games.GameEntity;
import java.util.ArrayList;

public class QuestEntityCreator
  implements Parcelable.Creator<QuestEntity>
{
  static void a(QuestEntity paramQuestEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramQuestEntity.h(), paramInt, false);
    zzb.a(paramParcel, 2, paramQuestEntity.b(), false);
    zzb.a(paramParcel, 3, paramQuestEntity.k());
    zzb.a(paramParcel, 4, paramQuestEntity.e(), paramInt, false);
    zzb.a(paramParcel, 5, paramQuestEntity.getBannerImageUrl(), false);
    zzb.a(paramParcel, 6, paramQuestEntity.d(), false);
    zzb.a(paramParcel, 7, paramQuestEntity.l());
    zzb.a(paramParcel, 8, paramQuestEntity.m());
    zzb.a(paramParcel, 9, paramQuestEntity.f(), paramInt, false);
    zzb.a(paramParcel, 10, paramQuestEntity.getIconImageUrl(), false);
    zzb.a(paramParcel, 12, paramQuestEntity.c(), false);
    zzb.a(paramParcel, 13, paramQuestEntity.n());
    zzb.a(paramParcel, 14, paramQuestEntity.o());
    zzb.a(paramParcel, 15, paramQuestEntity.i());
    zzb.c(paramParcel, 17, paramQuestEntity.g(), false);
    zzb.a(paramParcel, 16, paramQuestEntity.j());
    zzb.a(paramParcel, 1000, paramQuestEntity.p());
    zzb.a(paramParcel, i);
  }
  
  public QuestEntity a(Parcel paramParcel)
  {
    int m = zza.b(paramParcel);
    int k = 0;
    GameEntity localGameEntity = null;
    String str5 = null;
    long l5 = 0L;
    Uri localUri2 = null;
    String str4 = null;
    String str3 = null;
    long l4 = 0L;
    long l3 = 0L;
    Uri localUri1 = null;
    String str2 = null;
    String str1 = null;
    long l2 = 0L;
    long l1 = 0L;
    int j = 0;
    int i = 0;
    ArrayList localArrayList = null;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        localGameEntity = (GameEntity)zza.a(paramParcel, n, GameEntity.CREATOR);
        break;
      case 2: 
        str5 = zza.p(paramParcel, n);
        break;
      case 3: 
        l5 = zza.i(paramParcel, n);
        break;
      case 4: 
        localUri2 = (Uri)zza.a(paramParcel, n, Uri.CREATOR);
        break;
      case 5: 
        str4 = zza.p(paramParcel, n);
        break;
      case 6: 
        str3 = zza.p(paramParcel, n);
        break;
      case 7: 
        l4 = zza.i(paramParcel, n);
        break;
      case 8: 
        l3 = zza.i(paramParcel, n);
        break;
      case 9: 
        localUri1 = (Uri)zza.a(paramParcel, n, Uri.CREATOR);
        break;
      case 10: 
        str2 = zza.p(paramParcel, n);
        break;
      case 12: 
        str1 = zza.p(paramParcel, n);
        break;
      case 13: 
        l2 = zza.i(paramParcel, n);
        break;
      case 14: 
        l1 = zza.i(paramParcel, n);
        break;
      case 15: 
        j = zza.g(paramParcel, n);
        break;
      case 17: 
        localArrayList = zza.c(paramParcel, n, MilestoneEntity.CREATOR);
        break;
      case 16: 
        i = zza.g(paramParcel, n);
        break;
      case 1000: 
        k = zza.g(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new QuestEntity(k, localGameEntity, str5, l5, localUri2, str4, str3, l4, l3, localUri1, str2, str1, l2, l1, j, i, localArrayList);
  }
  
  public QuestEntity[] a(int paramInt)
  {
    return new QuestEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/quest/QuestEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */