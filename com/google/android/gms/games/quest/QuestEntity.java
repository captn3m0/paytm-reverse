package com.google.android.gms.games.quest;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import java.util.ArrayList;
import java.util.List;

public final class QuestEntity
  implements SafeParcelable, Quest
{
  public static final Parcelable.Creator<QuestEntity> CREATOR = new QuestEntityCreator();
  private final int a;
  private final GameEntity b;
  private final String c;
  private final long d;
  private final Uri e;
  private final String f;
  private final String g;
  private final long h;
  private final long i;
  private final Uri j;
  private final String k;
  private final String l;
  private final long m;
  private final long n;
  private final int o;
  private final int p;
  private final ArrayList<MilestoneEntity> q;
  
  QuestEntity(int paramInt1, GameEntity paramGameEntity, String paramString1, long paramLong1, Uri paramUri1, String paramString2, String paramString3, long paramLong2, long paramLong3, Uri paramUri2, String paramString4, String paramString5, long paramLong4, long paramLong5, int paramInt2, int paramInt3, ArrayList<MilestoneEntity> paramArrayList)
  {
    this.a = paramInt1;
    this.b = paramGameEntity;
    this.c = paramString1;
    this.d = paramLong1;
    this.e = paramUri1;
    this.f = paramString2;
    this.g = paramString3;
    this.h = paramLong2;
    this.i = paramLong3;
    this.j = paramUri2;
    this.k = paramString4;
    this.l = paramString5;
    this.m = paramLong4;
    this.n = paramLong5;
    this.o = paramInt2;
    this.p = paramInt3;
    this.q = paramArrayList;
  }
  
  public QuestEntity(Quest paramQuest)
  {
    this.a = 2;
    this.b = new GameEntity(paramQuest.h());
    this.c = paramQuest.b();
    this.d = paramQuest.k();
    this.g = paramQuest.d();
    this.e = paramQuest.e();
    this.f = paramQuest.getBannerImageUrl();
    this.h = paramQuest.l();
    this.j = paramQuest.f();
    this.k = paramQuest.getIconImageUrl();
    this.i = paramQuest.m();
    this.l = paramQuest.c();
    this.m = paramQuest.n();
    this.n = paramQuest.o();
    this.o = paramQuest.i();
    this.p = paramQuest.j();
    paramQuest = paramQuest.g();
    int i2 = paramQuest.size();
    this.q = new ArrayList(i2);
    int i1 = 0;
    while (i1 < i2)
    {
      this.q.add((MilestoneEntity)((Milestone)paramQuest.get(i1)).a());
      i1 += 1;
    }
  }
  
  static int a(Quest paramQuest)
  {
    return zzw.a(new Object[] { paramQuest.h(), paramQuest.b(), Long.valueOf(paramQuest.k()), paramQuest.e(), paramQuest.d(), Long.valueOf(paramQuest.l()), paramQuest.f(), Long.valueOf(paramQuest.m()), paramQuest.g(), paramQuest.c(), Long.valueOf(paramQuest.n()), Long.valueOf(paramQuest.o()), Integer.valueOf(paramQuest.i()) });
  }
  
  static boolean a(Quest paramQuest, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof Quest)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramQuest == paramObject);
      paramObject = (Quest)paramObject;
      if ((!zzw.a(((Quest)paramObject).h(), paramQuest.h())) || (!zzw.a(((Quest)paramObject).b(), paramQuest.b())) || (!zzw.a(Long.valueOf(((Quest)paramObject).k()), Long.valueOf(paramQuest.k()))) || (!zzw.a(((Quest)paramObject).e(), paramQuest.e())) || (!zzw.a(((Quest)paramObject).d(), paramQuest.d())) || (!zzw.a(Long.valueOf(((Quest)paramObject).l()), Long.valueOf(paramQuest.l()))) || (!zzw.a(((Quest)paramObject).f(), paramQuest.f())) || (!zzw.a(Long.valueOf(((Quest)paramObject).m()), Long.valueOf(paramQuest.m()))) || (!zzw.a(((Quest)paramObject).g(), paramQuest.g())) || (!zzw.a(((Quest)paramObject).c(), paramQuest.c())) || (!zzw.a(Long.valueOf(((Quest)paramObject).n()), Long.valueOf(paramQuest.n()))) || (!zzw.a(Long.valueOf(((Quest)paramObject).o()), Long.valueOf(paramQuest.o())))) {
        break;
      }
      bool1 = bool2;
    } while (zzw.a(Integer.valueOf(((Quest)paramObject).i()), Integer.valueOf(paramQuest.i())));
    return false;
  }
  
  static String b(Quest paramQuest)
  {
    return zzw.a(paramQuest).a("Game", paramQuest.h()).a("QuestId", paramQuest.b()).a("AcceptedTimestamp", Long.valueOf(paramQuest.k())).a("BannerImageUri", paramQuest.e()).a("BannerImageUrl", paramQuest.getBannerImageUrl()).a("Description", paramQuest.d()).a("EndTimestamp", Long.valueOf(paramQuest.l())).a("IconImageUri", paramQuest.f()).a("IconImageUrl", paramQuest.getIconImageUrl()).a("LastUpdatedTimestamp", Long.valueOf(paramQuest.m())).a("Milestones", paramQuest.g()).a("Name", paramQuest.c()).a("NotifyTimestamp", Long.valueOf(paramQuest.n())).a("StartTimestamp", Long.valueOf(paramQuest.o())).a("State", Integer.valueOf(paramQuest.i())).toString();
  }
  
  public String b()
  {
    return this.c;
  }
  
  public String c()
  {
    return this.l;
  }
  
  public String d()
  {
    return this.g;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Uri e()
  {
    return this.e;
  }
  
  public boolean equals(Object paramObject)
  {
    return a(this, paramObject);
  }
  
  public Uri f()
  {
    return this.j;
  }
  
  public List<Milestone> g()
  {
    return new ArrayList(this.q);
  }
  
  public String getBannerImageUrl()
  {
    return this.f;
  }
  
  public String getIconImageUrl()
  {
    return this.k;
  }
  
  public Game h()
  {
    return this.b;
  }
  
  public int hashCode()
  {
    return a(this);
  }
  
  public int i()
  {
    return this.o;
  }
  
  public int j()
  {
    return this.p;
  }
  
  public long k()
  {
    return this.d;
  }
  
  public long l()
  {
    return this.h;
  }
  
  public long m()
  {
    return this.i;
  }
  
  public long n()
  {
    return this.m;
  }
  
  public long o()
  {
    return this.n;
  }
  
  public int p()
  {
    return this.a;
  }
  
  public Quest q()
  {
    return this;
  }
  
  public String toString()
  {
    return b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    QuestEntityCreator.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/quest/QuestEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */