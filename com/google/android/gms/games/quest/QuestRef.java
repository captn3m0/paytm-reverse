package com.google.android.gms.games.quest;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameRef;
import java.util.ArrayList;
import java.util.List;

public final class QuestRef
  extends zzc
  implements Quest
{
  private final Game c;
  private final int d;
  
  QuestRef(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    super(paramDataHolder, paramInt1);
    this.c = new GameRef(paramDataHolder, paramInt1);
    this.d = paramInt2;
  }
  
  public String b()
  {
    return e("external_quest_id");
  }
  
  public String c()
  {
    return e("quest_name");
  }
  
  public String d()
  {
    return e("quest_description");
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Uri e()
  {
    return h("quest_banner_image_uri");
  }
  
  public boolean equals(Object paramObject)
  {
    return QuestEntity.a(this, paramObject);
  }
  
  public Uri f()
  {
    return h("quest_icon_image_uri");
  }
  
  public List<Milestone> g()
  {
    ArrayList localArrayList = new ArrayList(this.d);
    int i = 0;
    while (i < this.d)
    {
      localArrayList.add(new MilestoneRef(this.a, this.b + i));
      i += 1;
    }
    return localArrayList;
  }
  
  public String getBannerImageUrl()
  {
    return e("quest_banner_image_url");
  }
  
  public String getIconImageUrl()
  {
    return e("quest_icon_image_url");
  }
  
  public Game h()
  {
    return this.c;
  }
  
  public int hashCode()
  {
    return QuestEntity.a(this);
  }
  
  public int i()
  {
    return c("quest_state");
  }
  
  public int j()
  {
    return c("quest_type");
  }
  
  public long k()
  {
    return b("accepted_ts");
  }
  
  public long l()
  {
    return b("quest_end_ts");
  }
  
  public long m()
  {
    return b("quest_last_updated_ts");
  }
  
  public long n()
  {
    return b("notification_ts");
  }
  
  public long o()
  {
    return b("quest_start_ts");
  }
  
  public Quest p()
  {
    return new QuestEntity(this);
  }
  
  public String toString()
  {
    return QuestEntity.b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((QuestEntity)p()).writeToParcel(paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/quest/QuestRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */