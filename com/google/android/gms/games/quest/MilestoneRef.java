package com.google.android.gms.games.quest;

import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.games.internal.GamesLog;

public final class MilestoneRef
  extends zzc
  implements Milestone
{
  MilestoneRef(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
  }
  
  private long i()
  {
    return b("initial_value");
  }
  
  public String b()
  {
    return e("external_milestone_id");
  }
  
  public long c()
  {
    long l2 = 0L;
    long l1;
    switch (e())
    {
    default: 
      l1 = 0L;
      if (l1 < 0L)
      {
        GamesLog.b("MilestoneRef", "Current progress should never be negative");
        l1 = l2;
      }
      break;
    }
    for (;;)
    {
      l2 = l1;
      if (l1 > f())
      {
        GamesLog.b("MilestoneRef", "Current progress should never exceed target progress");
        l2 = f();
      }
      return l2;
      l1 = f();
      break;
      l1 = 0L;
      break;
      long l3 = b("current_value");
      l1 = l3;
      if (b("quest_state") == 6L) {
        break;
      }
      l1 = l3 - i();
      break;
    }
  }
  
  public String d()
  {
    return e("external_event_id");
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public int e()
  {
    return c("milestone_state");
  }
  
  public boolean equals(Object paramObject)
  {
    return MilestoneEntity.a(this, paramObject);
  }
  
  public long f()
  {
    return b("target_value");
  }
  
  public byte[] g()
  {
    return g("completion_reward_data");
  }
  
  public Milestone h()
  {
    return new MilestoneEntity(this);
  }
  
  public int hashCode()
  {
    return MilestoneEntity.a(this);
  }
  
  public String toString()
  {
    return MilestoneEntity.b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((MilestoneEntity)h()).writeToParcel(paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/quest/MilestoneRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */