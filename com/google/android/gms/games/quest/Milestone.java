package com.google.android.gms.games.quest;

import android.os.Parcelable;
import com.google.android.gms.common.data.Freezable;

public abstract interface Milestone
  extends Parcelable, Freezable<Milestone>
{
  public abstract String b();
  
  public abstract long c();
  
  public abstract String d();
  
  public abstract int e();
  
  public abstract long f();
  
  public abstract byte[] g();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/quest/Milestone.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */