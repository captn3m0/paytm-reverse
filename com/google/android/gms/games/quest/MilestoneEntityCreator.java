package com.google.android.gms.games.quest;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class MilestoneEntityCreator
  implements Parcelable.Creator<MilestoneEntity>
{
  static void a(MilestoneEntity paramMilestoneEntity, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramMilestoneEntity.b(), false);
    zzb.a(paramParcel, 1000, paramMilestoneEntity.h());
    zzb.a(paramParcel, 2, paramMilestoneEntity.c());
    zzb.a(paramParcel, 3, paramMilestoneEntity.f());
    zzb.a(paramParcel, 4, paramMilestoneEntity.g(), false);
    zzb.a(paramParcel, 5, paramMilestoneEntity.e());
    zzb.a(paramParcel, 6, paramMilestoneEntity.d(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public MilestoneEntity a(Parcel paramParcel)
  {
    long l1 = 0L;
    int i = 0;
    String str1 = null;
    int k = zza.b(paramParcel);
    byte[] arrayOfByte = null;
    long l2 = 0L;
    String str2 = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        str2 = zza.p(paramParcel, m);
        break;
      case 1000: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        l2 = zza.i(paramParcel, m);
        break;
      case 3: 
        l1 = zza.i(paramParcel, m);
        break;
      case 4: 
        arrayOfByte = zza.s(paramParcel, m);
        break;
      case 5: 
        i = zza.g(paramParcel, m);
        break;
      case 6: 
        str1 = zza.p(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new MilestoneEntity(j, str2, l2, l1, arrayOfByte, i, str1);
  }
  
  public MilestoneEntity[] a(int paramInt)
  {
    return new MilestoneEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/quest/MilestoneEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */