package com.google.android.gms.games.quest;

import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;

public abstract interface Quests
{
  public static final int[] a = { 1, 2, 3, 4, 101, 5, 102, 6, 103 };
  
  public static abstract interface AcceptQuestResult
    extends Result
  {}
  
  public static abstract interface ClaimMilestoneResult
    extends Result
  {}
  
  public static abstract interface LoadQuestsResult
    extends Releasable, Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/quest/Quests.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */