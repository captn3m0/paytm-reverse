package com.google.android.gms.games.event;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerRef;

public final class EventRef
  extends zzc
  implements Event
{
  EventRef(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
  }
  
  public String b()
  {
    return e("external_event_id");
  }
  
  public String c()
  {
    return e("name");
  }
  
  public String d()
  {
    return e("description");
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Uri e()
  {
    return h("icon_image_uri");
  }
  
  public boolean equals(Object paramObject)
  {
    return EventEntity.a(this, paramObject);
  }
  
  public Player f()
  {
    return new PlayerRef(this.a, this.b);
  }
  
  public long g()
  {
    return b("value");
  }
  
  public String getIconImageUrl()
  {
    return e("icon_image_url");
  }
  
  public String h()
  {
    return e("formatted_value");
  }
  
  public int hashCode()
  {
    return EventEntity.a(this);
  }
  
  public boolean i()
  {
    return d("visibility");
  }
  
  public Event j()
  {
    return new EventEntity(this);
  }
  
  public String toString()
  {
    return EventEntity.b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((EventEntity)j()).writeToParcel(paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/event/EventRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */