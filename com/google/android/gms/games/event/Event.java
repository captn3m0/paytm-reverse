package com.google.android.gms.games.event;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.data.Freezable;
import com.google.android.gms.games.Player;

public abstract interface Event
  extends Parcelable, Freezable<Event>
{
  public abstract String b();
  
  public abstract String c();
  
  public abstract String d();
  
  public abstract Uri e();
  
  public abstract Player f();
  
  public abstract long g();
  
  @Deprecated
  @KeepName
  public abstract String getIconImageUrl();
  
  public abstract String h();
  
  public abstract boolean i();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/event/Event.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */