package com.google.android.gms.games.event;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;

public final class EventEntity
  implements SafeParcelable, Event
{
  public static final EventEntityCreator CREATOR = new EventEntityCreator();
  private final int a;
  private final String b;
  private final String c;
  private final String d;
  private final Uri e;
  private final String f;
  private final PlayerEntity g;
  private final long h;
  private final String i;
  private final boolean j;
  
  EventEntity(int paramInt, String paramString1, String paramString2, String paramString3, Uri paramUri, String paramString4, Player paramPlayer, long paramLong, String paramString5, boolean paramBoolean)
  {
    this.a = paramInt;
    this.b = paramString1;
    this.c = paramString2;
    this.d = paramString3;
    this.e = paramUri;
    this.f = paramString4;
    this.g = new PlayerEntity(paramPlayer);
    this.h = paramLong;
    this.i = paramString5;
    this.j = paramBoolean;
  }
  
  public EventEntity(Event paramEvent)
  {
    this.a = 1;
    this.b = paramEvent.b();
    this.c = paramEvent.c();
    this.d = paramEvent.d();
    this.e = paramEvent.e();
    this.f = paramEvent.getIconImageUrl();
    this.g = ((PlayerEntity)paramEvent.f().a());
    this.h = paramEvent.g();
    this.i = paramEvent.h();
    this.j = paramEvent.i();
  }
  
  static int a(Event paramEvent)
  {
    return zzw.a(new Object[] { paramEvent.b(), paramEvent.c(), paramEvent.d(), paramEvent.e(), paramEvent.getIconImageUrl(), paramEvent.f(), Long.valueOf(paramEvent.g()), paramEvent.h(), Boolean.valueOf(paramEvent.i()) });
  }
  
  static boolean a(Event paramEvent, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof Event)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramEvent == paramObject);
      paramObject = (Event)paramObject;
      if ((!zzw.a(((Event)paramObject).b(), paramEvent.b())) || (!zzw.a(((Event)paramObject).c(), paramEvent.c())) || (!zzw.a(((Event)paramObject).d(), paramEvent.d())) || (!zzw.a(((Event)paramObject).e(), paramEvent.e())) || (!zzw.a(((Event)paramObject).getIconImageUrl(), paramEvent.getIconImageUrl())) || (!zzw.a(((Event)paramObject).f(), paramEvent.f())) || (!zzw.a(Long.valueOf(((Event)paramObject).g()), Long.valueOf(paramEvent.g()))) || (!zzw.a(((Event)paramObject).h(), paramEvent.h()))) {
        break;
      }
      bool1 = bool2;
    } while (zzw.a(Boolean.valueOf(((Event)paramObject).i()), Boolean.valueOf(paramEvent.i())));
    return false;
  }
  
  static String b(Event paramEvent)
  {
    return zzw.a(paramEvent).a("Id", paramEvent.b()).a("Name", paramEvent.c()).a("Description", paramEvent.d()).a("IconImageUri", paramEvent.e()).a("IconImageUrl", paramEvent.getIconImageUrl()).a("Player", paramEvent.f()).a("Value", Long.valueOf(paramEvent.g())).a("FormattedValue", paramEvent.h()).a("isVisible", Boolean.valueOf(paramEvent.i())).toString();
  }
  
  public String b()
  {
    return this.b;
  }
  
  public String c()
  {
    return this.c;
  }
  
  public String d()
  {
    return this.d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Uri e()
  {
    return this.e;
  }
  
  public boolean equals(Object paramObject)
  {
    return a(this, paramObject);
  }
  
  public Player f()
  {
    return this.g;
  }
  
  public long g()
  {
    return this.h;
  }
  
  public String getIconImageUrl()
  {
    return this.f;
  }
  
  public String h()
  {
    return this.i;
  }
  
  public int hashCode()
  {
    return a(this);
  }
  
  public boolean i()
  {
    return this.j;
  }
  
  public int j()
  {
    return this.a;
  }
  
  public Event k()
  {
    return this;
  }
  
  public String toString()
  {
    return b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    EventEntityCreator.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/event/EventEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */