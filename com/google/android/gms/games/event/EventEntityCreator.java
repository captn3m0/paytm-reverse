package com.google.android.gms.games.event;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.games.PlayerEntity;

public class EventEntityCreator
  implements Parcelable.Creator<EventEntity>
{
  static void a(EventEntity paramEventEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramEventEntity.b(), false);
    zzb.a(paramParcel, 1000, paramEventEntity.j());
    zzb.a(paramParcel, 2, paramEventEntity.c(), false);
    zzb.a(paramParcel, 3, paramEventEntity.d(), false);
    zzb.a(paramParcel, 4, paramEventEntity.e(), paramInt, false);
    zzb.a(paramParcel, 5, paramEventEntity.getIconImageUrl(), false);
    zzb.a(paramParcel, 6, paramEventEntity.f(), paramInt, false);
    zzb.a(paramParcel, 7, paramEventEntity.g());
    zzb.a(paramParcel, 8, paramEventEntity.h(), false);
    zzb.a(paramParcel, 9, paramEventEntity.i());
    zzb.a(paramParcel, i);
  }
  
  public EventEntity a(Parcel paramParcel)
  {
    boolean bool = false;
    String str1 = null;
    int j = zza.b(paramParcel);
    long l = 0L;
    PlayerEntity localPlayerEntity = null;
    String str2 = null;
    Uri localUri = null;
    String str3 = null;
    String str4 = null;
    String str5 = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        str5 = zza.p(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str4 = zza.p(paramParcel, k);
        break;
      case 3: 
        str3 = zza.p(paramParcel, k);
        break;
      case 4: 
        localUri = (Uri)zza.a(paramParcel, k, Uri.CREATOR);
        break;
      case 5: 
        str2 = zza.p(paramParcel, k);
        break;
      case 6: 
        localPlayerEntity = (PlayerEntity)zza.a(paramParcel, k, PlayerEntity.CREATOR);
        break;
      case 7: 
        l = zza.i(paramParcel, k);
        break;
      case 8: 
        str1 = zza.p(paramParcel, k);
        break;
      case 9: 
        bool = zza.c(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new EventEntity(i, str5, str4, str3, localUri, str2, localPlayerEntity, l, str1, bool);
  }
  
  public EventEntity[] a(int paramInt)
  {
    return new EventEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/event/EventEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */