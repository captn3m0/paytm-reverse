package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;

public final class GameEntity
  extends GamesDowngradeableSafeParcel
  implements Game
{
  public static final Parcelable.Creator<GameEntity> CREATOR = new GameEntityCreatorCompat();
  private final int a;
  private final String b;
  private final String c;
  private final String d;
  private final String e;
  private final String f;
  private final String g;
  private final Uri h;
  private final Uri i;
  private final Uri j;
  private final boolean k;
  private final boolean l;
  private final String m;
  private final int n;
  private final int o;
  private final int p;
  private final boolean q;
  private final boolean r;
  private final String s;
  private final String t;
  private final String u;
  private final boolean v;
  private final boolean w;
  private final boolean x;
  private final String y;
  private final boolean z;
  
  GameEntity(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, Uri paramUri1, Uri paramUri2, Uri paramUri3, boolean paramBoolean1, boolean paramBoolean2, String paramString7, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean3, boolean paramBoolean4, String paramString8, String paramString9, String paramString10, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, String paramString11, boolean paramBoolean8)
  {
    this.a = paramInt1;
    this.b = paramString1;
    this.c = paramString2;
    this.d = paramString3;
    this.e = paramString4;
    this.f = paramString5;
    this.g = paramString6;
    this.h = paramUri1;
    this.s = paramString8;
    this.i = paramUri2;
    this.t = paramString9;
    this.j = paramUri3;
    this.u = paramString10;
    this.k = paramBoolean1;
    this.l = paramBoolean2;
    this.m = paramString7;
    this.n = paramInt2;
    this.o = paramInt3;
    this.p = paramInt4;
    this.q = paramBoolean3;
    this.r = paramBoolean4;
    this.v = paramBoolean5;
    this.w = paramBoolean6;
    this.x = paramBoolean7;
    this.y = paramString11;
    this.z = paramBoolean8;
  }
  
  public GameEntity(Game paramGame)
  {
    this.a = 7;
    this.b = paramGame.b();
    this.d = paramGame.d();
    this.e = paramGame.e();
    this.f = paramGame.f();
    this.g = paramGame.g();
    this.c = paramGame.c();
    this.h = paramGame.h();
    this.s = paramGame.getIconImageUrl();
    this.i = paramGame.i();
    this.t = paramGame.getHiResImageUrl();
    this.j = paramGame.j();
    this.u = paramGame.getFeaturedImageUrl();
    this.k = paramGame.k();
    this.l = paramGame.n();
    this.m = paramGame.o();
    this.n = paramGame.p();
    this.o = paramGame.q();
    this.p = paramGame.r();
    this.q = paramGame.s();
    this.r = paramGame.t();
    this.v = paramGame.l();
    this.w = paramGame.m();
    this.x = paramGame.u();
    this.y = paramGame.v();
    this.z = paramGame.w();
  }
  
  static int a(Game paramGame)
  {
    return zzw.a(new Object[] { paramGame.b(), paramGame.c(), paramGame.d(), paramGame.e(), paramGame.f(), paramGame.g(), paramGame.h(), paramGame.i(), paramGame.j(), Boolean.valueOf(paramGame.k()), Boolean.valueOf(paramGame.n()), paramGame.o(), Integer.valueOf(paramGame.p()), Integer.valueOf(paramGame.q()), Integer.valueOf(paramGame.r()), Boolean.valueOf(paramGame.s()), Boolean.valueOf(paramGame.t()), Boolean.valueOf(paramGame.l()), Boolean.valueOf(paramGame.m()), Boolean.valueOf(paramGame.u()), paramGame.v(), Boolean.valueOf(paramGame.w()) });
  }
  
  static boolean a(Game paramGame, Object paramObject)
  {
    boolean bool2 = true;
    if (!(paramObject instanceof Game)) {
      bool1 = false;
    }
    do
    {
      return bool1;
      bool1 = bool2;
    } while (paramGame == paramObject);
    paramObject = (Game)paramObject;
    boolean bool3;
    if ((zzw.a(((Game)paramObject).b(), paramGame.b())) && (zzw.a(((Game)paramObject).c(), paramGame.c())) && (zzw.a(((Game)paramObject).d(), paramGame.d())) && (zzw.a(((Game)paramObject).e(), paramGame.e())) && (zzw.a(((Game)paramObject).f(), paramGame.f())) && (zzw.a(((Game)paramObject).g(), paramGame.g())) && (zzw.a(((Game)paramObject).h(), paramGame.h())) && (zzw.a(((Game)paramObject).i(), paramGame.i())) && (zzw.a(((Game)paramObject).j(), paramGame.j())) && (zzw.a(Boolean.valueOf(((Game)paramObject).k()), Boolean.valueOf(paramGame.k()))) && (zzw.a(Boolean.valueOf(((Game)paramObject).n()), Boolean.valueOf(paramGame.n()))) && (zzw.a(((Game)paramObject).o(), paramGame.o())) && (zzw.a(Integer.valueOf(((Game)paramObject).p()), Integer.valueOf(paramGame.p()))) && (zzw.a(Integer.valueOf(((Game)paramObject).q()), Integer.valueOf(paramGame.q()))) && (zzw.a(Integer.valueOf(((Game)paramObject).r()), Integer.valueOf(paramGame.r()))) && (zzw.a(Boolean.valueOf(((Game)paramObject).s()), Boolean.valueOf(paramGame.s()))))
    {
      bool3 = ((Game)paramObject).t();
      if ((!paramGame.t()) || (!zzw.a(Boolean.valueOf(((Game)paramObject).l()), Boolean.valueOf(paramGame.l()))) || (!zzw.a(Boolean.valueOf(((Game)paramObject).m()), Boolean.valueOf(paramGame.m())))) {
        break label501;
      }
    }
    label501:
    for (boolean bool1 = true;; bool1 = false)
    {
      if ((zzw.a(Boolean.valueOf(bool3), Boolean.valueOf(bool1))) && (zzw.a(Boolean.valueOf(((Game)paramObject).u()), Boolean.valueOf(paramGame.u()))) && (zzw.a(((Game)paramObject).v(), paramGame.v())))
      {
        bool1 = bool2;
        if (zzw.a(Boolean.valueOf(((Game)paramObject).w()), Boolean.valueOf(paramGame.w()))) {
          break;
        }
      }
      return false;
    }
  }
  
  static String b(Game paramGame)
  {
    return zzw.a(paramGame).a("ApplicationId", paramGame.b()).a("DisplayName", paramGame.c()).a("PrimaryCategory", paramGame.d()).a("SecondaryCategory", paramGame.e()).a("Description", paramGame.f()).a("DeveloperName", paramGame.g()).a("IconImageUri", paramGame.h()).a("IconImageUrl", paramGame.getIconImageUrl()).a("HiResImageUri", paramGame.i()).a("HiResImageUrl", paramGame.getHiResImageUrl()).a("FeaturedImageUri", paramGame.j()).a("FeaturedImageUrl", paramGame.getFeaturedImageUrl()).a("PlayEnabledGame", Boolean.valueOf(paramGame.k())).a("InstanceInstalled", Boolean.valueOf(paramGame.n())).a("InstancePackageName", paramGame.o()).a("AchievementTotalCount", Integer.valueOf(paramGame.q())).a("LeaderboardCount", Integer.valueOf(paramGame.r())).a("RealTimeMultiplayerEnabled", Boolean.valueOf(paramGame.s())).a("TurnBasedMultiplayerEnabled", Boolean.valueOf(paramGame.t())).a("AreSnapshotsEnabled", Boolean.valueOf(paramGame.u())).a("ThemeColor", paramGame.v()).a("HasGamepadSupport", Boolean.valueOf(paramGame.w())).toString();
  }
  
  public String b()
  {
    return this.b;
  }
  
  public String c()
  {
    return this.c;
  }
  
  public String d()
  {
    return this.d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String e()
  {
    return this.e;
  }
  
  public boolean equals(Object paramObject)
  {
    return a(this, paramObject);
  }
  
  public String f()
  {
    return this.f;
  }
  
  public String g()
  {
    return this.g;
  }
  
  public String getFeaturedImageUrl()
  {
    return this.u;
  }
  
  public String getHiResImageUrl()
  {
    return this.t;
  }
  
  public String getIconImageUrl()
  {
    return this.s;
  }
  
  public Uri h()
  {
    return this.h;
  }
  
  public int hashCode()
  {
    return a(this);
  }
  
  public Uri i()
  {
    return this.i;
  }
  
  public Uri j()
  {
    return this.j;
  }
  
  public boolean k()
  {
    return this.k;
  }
  
  public boolean l()
  {
    return this.v;
  }
  
  public boolean m()
  {
    return this.w;
  }
  
  public boolean n()
  {
    return this.l;
  }
  
  public String o()
  {
    return this.m;
  }
  
  public int p()
  {
    return this.n;
  }
  
  public int q()
  {
    return this.o;
  }
  
  public int r()
  {
    return this.p;
  }
  
  public boolean s()
  {
    return this.q;
  }
  
  public boolean t()
  {
    return this.r;
  }
  
  public String toString()
  {
    return b(this);
  }
  
  public boolean u()
  {
    return this.x;
  }
  
  public String v()
  {
    return this.y;
  }
  
  public boolean w()
  {
    return this.z;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int i1 = 1;
    Object localObject2 = null;
    if (!k_())
    {
      GameEntityCreator.a(this, paramParcel, paramInt);
      return;
    }
    paramParcel.writeString(this.b);
    paramParcel.writeString(this.c);
    paramParcel.writeString(this.d);
    paramParcel.writeString(this.e);
    paramParcel.writeString(this.f);
    paramParcel.writeString(this.g);
    Object localObject1;
    if (this.h == null)
    {
      localObject1 = null;
      paramParcel.writeString((String)localObject1);
      if (this.i != null) {
        break label189;
      }
      localObject1 = null;
      label93:
      paramParcel.writeString((String)localObject1);
      if (this.j != null) {
        break label201;
      }
      localObject1 = localObject2;
      label110:
      paramParcel.writeString((String)localObject1);
      if (!this.k) {
        break label213;
      }
      paramInt = 1;
      label125:
      paramParcel.writeInt(paramInt);
      if (!this.l) {
        break label218;
      }
    }
    label189:
    label201:
    label213:
    label218:
    for (paramInt = i1;; paramInt = 0)
    {
      paramParcel.writeInt(paramInt);
      paramParcel.writeString(this.m);
      paramParcel.writeInt(this.n);
      paramParcel.writeInt(this.o);
      paramParcel.writeInt(this.p);
      return;
      localObject1 = this.h.toString();
      break;
      localObject1 = this.i.toString();
      break label93;
      localObject1 = this.j.toString();
      break label110;
      paramInt = 0;
      break label125;
    }
  }
  
  public int x()
  {
    return this.a;
  }
  
  public Game y()
  {
    return this;
  }
  
  static final class GameEntityCreatorCompat
    extends GameEntityCreator
  {
    public GameEntity a(Parcel paramParcel)
    {
      if ((GameEntity.a(GameEntity.z())) || (GameEntity.b(GameEntity.class.getCanonicalName()))) {
        return super.a(paramParcel);
      }
      String str1 = paramParcel.readString();
      String str2 = paramParcel.readString();
      String str3 = paramParcel.readString();
      String str4 = paramParcel.readString();
      String str5 = paramParcel.readString();
      String str6 = paramParcel.readString();
      Object localObject1 = paramParcel.readString();
      Object localObject2;
      label90:
      Object localObject3;
      label104:
      boolean bool1;
      if (localObject1 == null)
      {
        localObject1 = null;
        localObject2 = paramParcel.readString();
        if (localObject2 != null) {
          break label188;
        }
        localObject2 = null;
        localObject3 = paramParcel.readString();
        if (localObject3 != null) {
          break label198;
        }
        localObject3 = null;
        if (paramParcel.readInt() <= 0) {
          break label208;
        }
        bool1 = true;
        label113:
        if (paramParcel.readInt() <= 0) {
          break label213;
        }
      }
      label188:
      label198:
      label208:
      label213:
      for (boolean bool2 = true;; bool2 = false)
      {
        return new GameEntity(7, str1, str2, str3, str4, str5, str6, (Uri)localObject1, (Uri)localObject2, (Uri)localObject3, bool1, bool2, paramParcel.readString(), paramParcel.readInt(), paramParcel.readInt(), paramParcel.readInt(), false, false, null, null, null, false, false, false, null, false);
        localObject1 = Uri.parse((String)localObject1);
        break;
        localObject2 = Uri.parse((String)localObject2);
        break label90;
        localObject3 = Uri.parse((String)localObject3);
        break label104;
        bool1 = false;
        break label113;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/GameEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */