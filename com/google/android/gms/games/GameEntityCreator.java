package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class GameEntityCreator
  implements Parcelable.Creator<GameEntity>
{
  static void a(GameEntity paramGameEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramGameEntity.b(), false);
    zzb.a(paramParcel, 2, paramGameEntity.c(), false);
    zzb.a(paramParcel, 3, paramGameEntity.d(), false);
    zzb.a(paramParcel, 4, paramGameEntity.e(), false);
    zzb.a(paramParcel, 5, paramGameEntity.f(), false);
    zzb.a(paramParcel, 6, paramGameEntity.g(), false);
    zzb.a(paramParcel, 7, paramGameEntity.h(), paramInt, false);
    zzb.a(paramParcel, 8, paramGameEntity.i(), paramInt, false);
    zzb.a(paramParcel, 9, paramGameEntity.j(), paramInt, false);
    zzb.a(paramParcel, 10, paramGameEntity.k());
    zzb.a(paramParcel, 11, paramGameEntity.n());
    zzb.a(paramParcel, 12, paramGameEntity.o(), false);
    zzb.a(paramParcel, 13, paramGameEntity.p());
    zzb.a(paramParcel, 14, paramGameEntity.q());
    zzb.a(paramParcel, 15, paramGameEntity.r());
    zzb.a(paramParcel, 17, paramGameEntity.t());
    zzb.a(paramParcel, 16, paramGameEntity.s());
    zzb.a(paramParcel, 1000, paramGameEntity.x());
    zzb.a(paramParcel, 19, paramGameEntity.getHiResImageUrl(), false);
    zzb.a(paramParcel, 18, paramGameEntity.getIconImageUrl(), false);
    zzb.a(paramParcel, 21, paramGameEntity.l());
    zzb.a(paramParcel, 20, paramGameEntity.getFeaturedImageUrl(), false);
    zzb.a(paramParcel, 23, paramGameEntity.u());
    zzb.a(paramParcel, 22, paramGameEntity.m());
    zzb.a(paramParcel, 25, paramGameEntity.w());
    zzb.a(paramParcel, 24, paramGameEntity.v(), false);
    zzb.a(paramParcel, i);
  }
  
  public GameEntity a(Parcel paramParcel)
  {
    int n = zza.b(paramParcel);
    int m = 0;
    String str11 = null;
    String str10 = null;
    String str9 = null;
    String str8 = null;
    String str7 = null;
    String str6 = null;
    Uri localUri3 = null;
    Uri localUri2 = null;
    Uri localUri1 = null;
    boolean bool8 = false;
    boolean bool7 = false;
    String str5 = null;
    int k = 0;
    int j = 0;
    int i = 0;
    boolean bool6 = false;
    boolean bool5 = false;
    String str4 = null;
    String str3 = null;
    String str2 = null;
    boolean bool4 = false;
    boolean bool3 = false;
    boolean bool2 = false;
    String str1 = null;
    boolean bool1 = false;
    while (paramParcel.dataPosition() < n)
    {
      int i1 = zza.a(paramParcel);
      switch (zza.a(i1))
      {
      default: 
        zza.b(paramParcel, i1);
        break;
      case 1: 
        str11 = zza.p(paramParcel, i1);
        break;
      case 2: 
        str10 = zza.p(paramParcel, i1);
        break;
      case 3: 
        str9 = zza.p(paramParcel, i1);
        break;
      case 4: 
        str8 = zza.p(paramParcel, i1);
        break;
      case 5: 
        str7 = zza.p(paramParcel, i1);
        break;
      case 6: 
        str6 = zza.p(paramParcel, i1);
        break;
      case 7: 
        localUri3 = (Uri)zza.a(paramParcel, i1, Uri.CREATOR);
        break;
      case 8: 
        localUri2 = (Uri)zza.a(paramParcel, i1, Uri.CREATOR);
        break;
      case 9: 
        localUri1 = (Uri)zza.a(paramParcel, i1, Uri.CREATOR);
        break;
      case 10: 
        bool8 = zza.c(paramParcel, i1);
        break;
      case 11: 
        bool7 = zza.c(paramParcel, i1);
        break;
      case 12: 
        str5 = zza.p(paramParcel, i1);
        break;
      case 13: 
        k = zza.g(paramParcel, i1);
        break;
      case 14: 
        j = zza.g(paramParcel, i1);
        break;
      case 15: 
        i = zza.g(paramParcel, i1);
        break;
      case 17: 
        bool5 = zza.c(paramParcel, i1);
        break;
      case 16: 
        bool6 = zza.c(paramParcel, i1);
        break;
      case 1000: 
        m = zza.g(paramParcel, i1);
        break;
      case 19: 
        str3 = zza.p(paramParcel, i1);
        break;
      case 18: 
        str4 = zza.p(paramParcel, i1);
        break;
      case 21: 
        bool4 = zza.c(paramParcel, i1);
        break;
      case 20: 
        str2 = zza.p(paramParcel, i1);
        break;
      case 23: 
        bool2 = zza.c(paramParcel, i1);
        break;
      case 22: 
        bool3 = zza.c(paramParcel, i1);
        break;
      case 25: 
        bool1 = zza.c(paramParcel, i1);
        break;
      case 24: 
        str1 = zza.p(paramParcel, i1);
      }
    }
    if (paramParcel.dataPosition() != n) {
      throw new zza.zza("Overread allowed size end=" + n, paramParcel);
    }
    return new GameEntity(m, str11, str10, str9, str8, str7, str6, localUri3, localUri2, localUri1, bool8, bool7, str5, k, j, i, bool6, bool5, str4, str3, str2, bool4, bool3, bool2, str1, bool1);
  }
  
  public GameEntity[] a(int paramInt)
  {
    return new GameEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/GameEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */