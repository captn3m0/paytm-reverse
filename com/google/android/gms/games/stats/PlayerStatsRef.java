package com.google.android.gms.games.stats;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.common.internal.zzb;

public class PlayerStatsRef
  extends zzc
  implements PlayerStats
{
  private Bundle c;
  
  PlayerStatsRef(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
  }
  
  public float b()
  {
    return f("ave_session_length_minutes");
  }
  
  public float c()
  {
    return f("churn_probability");
  }
  
  public int d()
  {
    return c("days_since_last_played");
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public int e()
  {
    return c("num_purchases");
  }
  
  public boolean equals(Object paramObject)
  {
    return PlayerStatsEntity.a(this, paramObject);
  }
  
  public int f()
  {
    return c("num_sessions");
  }
  
  public float g()
  {
    return f("num_sessions_percentile");
  }
  
  public float h()
  {
    return f("spend_percentile");
  }
  
  public int hashCode()
  {
    return PlayerStatsEntity.a(this);
  }
  
  public float i()
  {
    if (!a_("spend_probability")) {
      return -1.0F;
    }
    return f("spend_probability");
  }
  
  public Bundle j()
  {
    int i = 0;
    if (this.c != null) {
      return this.c;
    }
    this.c = new Bundle();
    Object localObject2 = e("unknown_raw_keys");
    Object localObject1 = e("unknown_raw_values");
    if ((localObject2 != null) && (localObject1 != null))
    {
      localObject2 = ((String)localObject2).split(",");
      localObject1 = ((String)localObject1).split(",");
      if (localObject2.length <= localObject1.length) {}
      for (boolean bool = true;; bool = false)
      {
        zzb.a(bool, "Invalid raw arguments!");
        while (i < localObject2.length)
        {
          this.c.putString(localObject2[i], localObject1[i]);
          i += 1;
        }
      }
    }
    return this.c;
  }
  
  public PlayerStats k()
  {
    return new PlayerStatsEntity(this);
  }
  
  public String toString()
  {
    return PlayerStatsEntity.b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((PlayerStatsEntity)k()).writeToParcel(paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/stats/PlayerStatsRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */