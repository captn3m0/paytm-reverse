package com.google.android.gms.games.stats;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class PlayerStatsEntityCreator
  implements Parcelable.Creator<PlayerStatsEntity>
{
  static void a(PlayerStatsEntity paramPlayerStatsEntity, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramPlayerStatsEntity.b());
    zzb.a(paramParcel, 1000, paramPlayerStatsEntity.k());
    zzb.a(paramParcel, 2, paramPlayerStatsEntity.c());
    zzb.a(paramParcel, 3, paramPlayerStatsEntity.d());
    zzb.a(paramParcel, 4, paramPlayerStatsEntity.e());
    zzb.a(paramParcel, 5, paramPlayerStatsEntity.f());
    zzb.a(paramParcel, 6, paramPlayerStatsEntity.g());
    zzb.a(paramParcel, 7, paramPlayerStatsEntity.h());
    zzb.a(paramParcel, 8, paramPlayerStatsEntity.j(), false);
    zzb.a(paramParcel, 9, paramPlayerStatsEntity.i());
    zzb.a(paramParcel, paramInt);
  }
  
  public PlayerStatsEntity a(Parcel paramParcel)
  {
    int i = 0;
    float f1 = 0.0F;
    int n = zza.b(paramParcel);
    Bundle localBundle = null;
    float f2 = 0.0F;
    float f3 = 0.0F;
    int j = 0;
    int k = 0;
    float f4 = 0.0F;
    float f5 = 0.0F;
    int m = 0;
    while (paramParcel.dataPosition() < n)
    {
      int i1 = zza.a(paramParcel);
      switch (zza.a(i1))
      {
      default: 
        zza.b(paramParcel, i1);
        break;
      case 1: 
        f5 = zza.l(paramParcel, i1);
        break;
      case 1000: 
        m = zza.g(paramParcel, i1);
        break;
      case 2: 
        f4 = zza.l(paramParcel, i1);
        break;
      case 3: 
        k = zza.g(paramParcel, i1);
        break;
      case 4: 
        j = zza.g(paramParcel, i1);
        break;
      case 5: 
        i = zza.g(paramParcel, i1);
        break;
      case 6: 
        f3 = zza.l(paramParcel, i1);
        break;
      case 7: 
        f2 = zza.l(paramParcel, i1);
        break;
      case 8: 
        localBundle = zza.r(paramParcel, i1);
        break;
      case 9: 
        f1 = zza.l(paramParcel, i1);
      }
    }
    if (paramParcel.dataPosition() != n) {
      throw new zza.zza("Overread allowed size end=" + n, paramParcel);
    }
    return new PlayerStatsEntity(m, f5, f4, k, j, i, f3, f2, localBundle, f1);
  }
  
  public PlayerStatsEntity[] a(int paramInt)
  {
    return new PlayerStatsEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/stats/PlayerStatsEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */