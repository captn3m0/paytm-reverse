package com.google.android.gms.games.stats;

import android.os.Bundle;
import android.os.Parcelable;
import com.google.android.gms.common.data.Freezable;

public abstract interface PlayerStats
  extends Parcelable, Freezable<PlayerStats>
{
  public abstract float b();
  
  public abstract float c();
  
  public abstract int d();
  
  public abstract int e();
  
  public abstract int f();
  
  public abstract float g();
  
  public abstract float h();
  
  public abstract float i();
  
  public abstract Bundle j();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/stats/PlayerStats.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */