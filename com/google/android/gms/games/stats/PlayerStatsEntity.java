package com.google.android.gms.games.stats;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;

public class PlayerStatsEntity
  implements SafeParcelable, PlayerStats
{
  public static final Parcelable.Creator<PlayerStatsEntity> CREATOR = new PlayerStatsEntityCreator();
  private final int a;
  private final float b;
  private final float c;
  private final int d;
  private final int e;
  private final int f;
  private final float g;
  private final float h;
  private final Bundle i;
  private final float j;
  
  PlayerStatsEntity(int paramInt1, float paramFloat1, float paramFloat2, int paramInt2, int paramInt3, int paramInt4, float paramFloat3, float paramFloat4, Bundle paramBundle, float paramFloat5)
  {
    this.a = paramInt1;
    this.b = paramFloat1;
    this.c = paramFloat2;
    this.d = paramInt2;
    this.e = paramInt3;
    this.f = paramInt4;
    this.g = paramFloat3;
    this.h = paramFloat4;
    this.i = paramBundle;
    this.j = paramFloat5;
  }
  
  public PlayerStatsEntity(PlayerStats paramPlayerStats)
  {
    this.a = 3;
    this.b = paramPlayerStats.b();
    this.c = paramPlayerStats.c();
    this.d = paramPlayerStats.d();
    this.e = paramPlayerStats.e();
    this.f = paramPlayerStats.f();
    this.g = paramPlayerStats.g();
    this.h = paramPlayerStats.h();
    this.j = paramPlayerStats.i();
    this.i = paramPlayerStats.j();
  }
  
  static int a(PlayerStats paramPlayerStats)
  {
    return zzw.a(new Object[] { Float.valueOf(paramPlayerStats.b()), Float.valueOf(paramPlayerStats.c()), Integer.valueOf(paramPlayerStats.d()), Integer.valueOf(paramPlayerStats.e()), Integer.valueOf(paramPlayerStats.f()), Float.valueOf(paramPlayerStats.g()), Float.valueOf(paramPlayerStats.h()), Float.valueOf(paramPlayerStats.i()) });
  }
  
  static boolean a(PlayerStats paramPlayerStats, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof PlayerStats)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramPlayerStats == paramObject);
      paramObject = (PlayerStats)paramObject;
      if ((!zzw.a(Float.valueOf(((PlayerStats)paramObject).b()), Float.valueOf(paramPlayerStats.b()))) || (!zzw.a(Float.valueOf(((PlayerStats)paramObject).c()), Float.valueOf(paramPlayerStats.c()))) || (!zzw.a(Integer.valueOf(((PlayerStats)paramObject).d()), Integer.valueOf(paramPlayerStats.d()))) || (!zzw.a(Integer.valueOf(((PlayerStats)paramObject).e()), Integer.valueOf(paramPlayerStats.e()))) || (!zzw.a(Integer.valueOf(((PlayerStats)paramObject).f()), Integer.valueOf(paramPlayerStats.f()))) || (!zzw.a(Float.valueOf(((PlayerStats)paramObject).g()), Float.valueOf(paramPlayerStats.g()))) || (!zzw.a(Float.valueOf(((PlayerStats)paramObject).h()), Float.valueOf(paramPlayerStats.h())))) {
        break;
      }
      bool1 = bool2;
    } while (zzw.a(Float.valueOf(((PlayerStats)paramObject).i()), Float.valueOf(paramPlayerStats.i())));
    return false;
  }
  
  static String b(PlayerStats paramPlayerStats)
  {
    return zzw.a(paramPlayerStats).a("AverageSessionLength", Float.valueOf(paramPlayerStats.b())).a("ChurnProbability", Float.valueOf(paramPlayerStats.c())).a("DaysSinceLastPlayed", Integer.valueOf(paramPlayerStats.d())).a("NumberOfPurchases", Integer.valueOf(paramPlayerStats.e())).a("NumberOfSessions", Integer.valueOf(paramPlayerStats.f())).a("SessionPercentile", Float.valueOf(paramPlayerStats.g())).a("SpendPercentile", Float.valueOf(paramPlayerStats.h())).a("SpendProbability", Float.valueOf(paramPlayerStats.i())).toString();
  }
  
  public float b()
  {
    return this.b;
  }
  
  public float c()
  {
    return this.c;
  }
  
  public int d()
  {
    return this.d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public int e()
  {
    return this.e;
  }
  
  public boolean equals(Object paramObject)
  {
    return a(this, paramObject);
  }
  
  public int f()
  {
    return this.f;
  }
  
  public float g()
  {
    return this.g;
  }
  
  public float h()
  {
    return this.h;
  }
  
  public int hashCode()
  {
    return a(this);
  }
  
  public float i()
  {
    return this.j;
  }
  
  public Bundle j()
  {
    return this.i;
  }
  
  public int k()
  {
    return this.a;
  }
  
  public PlayerStats l()
  {
    return this;
  }
  
  public String toString()
  {
    return b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    PlayerStatsEntityCreator.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/stats/PlayerStatsEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */