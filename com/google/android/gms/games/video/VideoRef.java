package com.google.android.gms.games.video;

import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;

public final class VideoRef
  extends zzc
  implements Video
{
  VideoRef(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
  }
  
  public int b()
  {
    return c("duration");
  }
  
  public String c()
  {
    return e("filepath");
  }
  
  public long d()
  {
    return b("filesize");
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public long e()
  {
    return b("start_time");
  }
  
  public String f()
  {
    return e("package");
  }
  
  public Video g()
  {
    return new VideoEntity(this);
  }
  
  public String toString()
  {
    return VideoEntity.b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((VideoEntity)g()).writeToParcel(paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/video/VideoRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */