package com.google.android.gms.games.video;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzb;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;

public final class VideoEntity
  implements SafeParcelable, Video
{
  public static final Parcelable.Creator<VideoEntity> CREATOR = new VideoEntityCreator();
  private final int a;
  private final int b;
  private final String c;
  private final long d;
  private final long e;
  private final String f;
  
  VideoEntity(int paramInt1, int paramInt2, String paramString1, long paramLong1, long paramLong2, String paramString2)
  {
    this.a = paramInt1;
    this.b = paramInt2;
    this.c = paramString1;
    this.d = paramLong1;
    this.e = paramLong2;
    this.f = paramString2;
  }
  
  public VideoEntity(Video paramVideo)
  {
    this.a = 1;
    this.b = paramVideo.b();
    this.c = paramVideo.c();
    this.d = paramVideo.d();
    this.e = paramVideo.e();
    this.f = paramVideo.f();
    zzb.a(this.c);
    zzb.a(this.f);
  }
  
  static int a(Video paramVideo)
  {
    return zzw.a(new Object[] { Integer.valueOf(paramVideo.b()), paramVideo.c(), Long.valueOf(paramVideo.d()), Long.valueOf(paramVideo.e()), paramVideo.f() });
  }
  
  static boolean a(Video paramVideo, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof Video)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramVideo == paramObject);
      paramObject = (Video)paramObject;
      if ((!zzw.a(Integer.valueOf(((Video)paramObject).b()), Integer.valueOf(paramVideo.b()))) || (!zzw.a(((Video)paramObject).c(), paramVideo.c())) || (!zzw.a(Long.valueOf(((Video)paramObject).d()), Long.valueOf(paramVideo.d()))) || (!zzw.a(Long.valueOf(((Video)paramObject).e()), Long.valueOf(paramVideo.e())))) {
        break;
      }
      bool1 = bool2;
    } while (zzw.a(((Video)paramObject).f(), paramVideo.f()));
    return false;
  }
  
  static String b(Video paramVideo)
  {
    return zzw.a(paramVideo).a("Duration", Integer.valueOf(paramVideo.b())).a("File path", paramVideo.c()).a("File size", Long.valueOf(paramVideo.d())).a("Start time", Long.valueOf(paramVideo.e())).a("Package name", paramVideo.f()).toString();
  }
  
  public int b()
  {
    return this.b;
  }
  
  public String c()
  {
    return this.c;
  }
  
  public long d()
  {
    return this.d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public long e()
  {
    return this.e;
  }
  
  public boolean equals(Object paramObject)
  {
    return a(this, paramObject);
  }
  
  public String f()
  {
    return this.f;
  }
  
  public int g()
  {
    return this.a;
  }
  
  public Video h()
  {
    return this;
  }
  
  public int hashCode()
  {
    return a(this);
  }
  
  public String toString()
  {
    return b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    VideoEntityCreator.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/video/VideoEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */