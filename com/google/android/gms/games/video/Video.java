package com.google.android.gms.games.video;

import android.os.Parcelable;
import com.google.android.gms.common.data.Freezable;

public abstract interface Video
  extends Parcelable, Freezable<Video>
{
  public abstract int b();
  
  public abstract String c();
  
  public abstract long d();
  
  public abstract long e();
  
  public abstract String f();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/video/Video.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */