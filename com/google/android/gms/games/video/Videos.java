package com.google.android.gms.games.video;

import com.google.android.gms.common.api.Result;

public abstract interface Videos
{
  public static abstract interface ListVideosResult
    extends Result
  {}
  
  public static abstract interface VideoAvailableResult
    extends Result
  {}
  
  public static abstract interface VideoCapabilitiesResult
    extends Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/video/Videos.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */