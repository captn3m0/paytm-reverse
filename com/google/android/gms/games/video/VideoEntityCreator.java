package com.google.android.gms.games.video;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class VideoEntityCreator
  implements Parcelable.Creator<VideoEntity>
{
  static void a(VideoEntity paramVideoEntity, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramVideoEntity.b());
    zzb.a(paramParcel, 1000, paramVideoEntity.g());
    zzb.a(paramParcel, 2, paramVideoEntity.c(), false);
    zzb.a(paramParcel, 3, paramVideoEntity.d());
    zzb.a(paramParcel, 4, paramVideoEntity.e());
    zzb.a(paramParcel, 5, paramVideoEntity.f(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public VideoEntity a(Parcel paramParcel)
  {
    long l1 = 0L;
    String str1 = null;
    int i = 0;
    int k = zza.b(paramParcel);
    long l2 = 0L;
    String str2 = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        i = zza.g(paramParcel, m);
        break;
      case 1000: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        str2 = zza.p(paramParcel, m);
        break;
      case 3: 
        l2 = zza.i(paramParcel, m);
        break;
      case 4: 
        l1 = zza.i(paramParcel, m);
        break;
      case 5: 
        str1 = zza.p(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new VideoEntity(j, i, str2, l2, l1, str1);
  }
  
  public VideoEntity[] a(int paramInt)
  {
    return new VideoEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/video/VideoEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */