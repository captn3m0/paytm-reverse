package com.google.android.gms.games.video;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;

public final class VideoCapabilities
  implements SafeParcelable
{
  public static final Parcelable.Creator<VideoCapabilities> CREATOR = new VideoCapabilitiesCreator();
  private final int a;
  private final boolean b;
  private final boolean c;
  private final boolean d;
  private final boolean[] e;
  private final boolean[] f;
  
  public VideoCapabilities(int paramInt, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean[] paramArrayOfBoolean1, boolean[] paramArrayOfBoolean2)
  {
    this.a = paramInt;
    this.b = paramBoolean1;
    this.c = paramBoolean2;
    this.d = paramBoolean3;
    this.e = paramArrayOfBoolean1;
    this.f = paramArrayOfBoolean2;
  }
  
  public int a()
  {
    return this.a;
  }
  
  public boolean b()
  {
    return this.c;
  }
  
  public boolean c()
  {
    return this.b;
  }
  
  public boolean d()
  {
    return this.d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean[] e()
  {
    return this.e;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof VideoCapabilities)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (this == paramObject);
      paramObject = (VideoCapabilities)paramObject;
      if ((!zzw.a(((VideoCapabilities)paramObject).e(), e())) || (!zzw.a(((VideoCapabilities)paramObject).f(), f())) || (!zzw.a(Boolean.valueOf(((VideoCapabilities)paramObject).c()), Boolean.valueOf(c()))) || (!zzw.a(Boolean.valueOf(((VideoCapabilities)paramObject).b()), Boolean.valueOf(b())))) {
        break;
      }
      bool1 = bool2;
    } while (zzw.a(Boolean.valueOf(((VideoCapabilities)paramObject).d()), Boolean.valueOf(d())));
    return false;
  }
  
  public boolean[] f()
  {
    return this.f;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { e(), f(), Boolean.valueOf(c()), Boolean.valueOf(b()), Boolean.valueOf(d()) });
  }
  
  public String toString()
  {
    return zzw.a(this).a("SupportedCaptureModes", e()).a("SupportedQualityLevels", f()).a("CameraSupported", Boolean.valueOf(c())).a("MicSupported", Boolean.valueOf(b())).a("StorageWriteSupported", Boolean.valueOf(d())).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    VideoCapabilitiesCreator.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/video/VideoCapabilities.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */