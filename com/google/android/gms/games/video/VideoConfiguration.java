package com.google.android.gms.games.video;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;

public final class VideoConfiguration
  implements SafeParcelable
{
  public static final Parcelable.Creator<VideoConfiguration> CREATOR = new VideoConfigurationCreator();
  private final int a;
  private final int b;
  private final int c;
  private final String d;
  private final String e;
  
  public VideoConfiguration(int paramInt1, int paramInt2, int paramInt3, String paramString1, String paramString2)
  {
    this.a = paramInt1;
    zzx.b(a(paramInt2));
    zzx.b(b(paramInt3));
    this.b = paramInt2;
    this.c = paramInt3;
    if (paramInt3 == 1)
    {
      this.e = paramString2;
      this.d = paramString1;
      return;
    }
    if (paramString2 == null)
    {
      bool1 = true;
      zzx.b(bool1, "Stream key should be null when not streaming");
      if (paramString1 != null) {
        break label102;
      }
    }
    label102:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzx.b(bool1, "Stream url should be null when not streaming");
      this.e = null;
      this.d = null;
      return;
      bool1 = false;
      break;
    }
  }
  
  public static boolean a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return false;
    }
    return true;
  }
  
  public static boolean b(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return false;
    }
    return true;
  }
  
  public int a()
  {
    return this.a;
  }
  
  public int b()
  {
    return this.b;
  }
  
  public int c()
  {
    return this.c;
  }
  
  public String d()
  {
    return this.e;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String e()
  {
    return this.d;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    VideoConfigurationCreator.a(this, paramParcel, paramInt);
  }
  
  public static final class Builder {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/video/VideoConfiguration.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */