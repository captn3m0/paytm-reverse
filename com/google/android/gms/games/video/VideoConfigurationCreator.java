package com.google.android.gms.games.video;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class VideoConfigurationCreator
  implements Parcelable.Creator<VideoConfiguration>
{
  static void a(VideoConfiguration paramVideoConfiguration, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramVideoConfiguration.b());
    zzb.a(paramParcel, 1000, paramVideoConfiguration.a());
    zzb.a(paramParcel, 2, paramVideoConfiguration.c());
    zzb.a(paramParcel, 3, paramVideoConfiguration.e(), false);
    zzb.a(paramParcel, 4, paramVideoConfiguration.d(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public VideoConfiguration a(Parcel paramParcel)
  {
    String str1 = null;
    int i = 0;
    int m = zza.b(paramParcel);
    String str2 = null;
    int j = 0;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        j = zza.g(paramParcel, n);
        break;
      case 1000: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        i = zza.g(paramParcel, n);
        break;
      case 3: 
        str2 = zza.p(paramParcel, n);
        break;
      case 4: 
        str1 = zza.p(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new VideoConfiguration(k, j, i, str2, str1);
  }
  
  public VideoConfiguration[] a(int paramInt)
  {
    return new VideoConfiguration[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/video/VideoConfigurationCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */