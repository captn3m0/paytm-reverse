package com.google.android.gms.games.video;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class VideoCapabilitiesCreator
  implements Parcelable.Creator<VideoCapabilities>
{
  static void a(VideoCapabilities paramVideoCapabilities, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramVideoCapabilities.c());
    zzb.a(paramParcel, 1000, paramVideoCapabilities.a());
    zzb.a(paramParcel, 2, paramVideoCapabilities.b());
    zzb.a(paramParcel, 3, paramVideoCapabilities.d());
    zzb.a(paramParcel, 4, paramVideoCapabilities.e(), false);
    zzb.a(paramParcel, 5, paramVideoCapabilities.f(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public VideoCapabilities a(Parcel paramParcel)
  {
    boolean[] arrayOfBoolean1 = null;
    boolean bool1 = false;
    int j = zza.b(paramParcel);
    boolean[] arrayOfBoolean2 = null;
    boolean bool2 = false;
    boolean bool3 = false;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        bool3 = zza.c(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        bool2 = zza.c(paramParcel, k);
        break;
      case 3: 
        bool1 = zza.c(paramParcel, k);
        break;
      case 4: 
        arrayOfBoolean2 = zza.u(paramParcel, k);
        break;
      case 5: 
        arrayOfBoolean1 = zza.u(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new VideoCapabilities(i, bool3, bool2, bool1, arrayOfBoolean2, arrayOfBoolean1);
  }
  
  public VideoCapabilities[] a(int paramInt)
  {
    return new VideoCapabilities[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/video/VideoCapabilitiesCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */