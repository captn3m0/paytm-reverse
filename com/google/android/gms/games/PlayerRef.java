package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.games.internal.player.MostRecentGameInfo;
import com.google.android.gms.games.internal.player.MostRecentGameInfoRef;
import com.google.android.gms.games.internal.player.PlayerColumnNames;

public final class PlayerRef
  extends zzc
  implements Player
{
  private final PlayerColumnNames c;
  private final PlayerLevelInfo d;
  private final MostRecentGameInfoRef e;
  
  public PlayerRef(DataHolder paramDataHolder, int paramInt)
  {
    this(paramDataHolder, paramInt, null);
  }
  
  public PlayerRef(DataHolder paramDataHolder, int paramInt, String paramString)
  {
    super(paramDataHolder, paramInt);
    this.c = new PlayerColumnNames(paramString);
    this.e = new MostRecentGameInfoRef(paramDataHolder, paramInt, this.c);
    int i;
    if (s())
    {
      paramInt = c(this.c.k);
      i = c(this.c.n);
      paramString = new PlayerLevel(paramInt, b(this.c.l), b(this.c.m));
      if (paramInt == i) {
        break label178;
      }
    }
    label178:
    for (paramDataHolder = new PlayerLevel(i, b(this.c.m), b(this.c.o));; paramDataHolder = paramString)
    {
      this.d = new PlayerLevelInfo(b(this.c.j), b(this.c.p), paramString, paramDataHolder);
      return;
      this.d = null;
      return;
    }
  }
  
  private boolean s()
  {
    if (i(this.c.j)) {}
    while (b(this.c.j) == -1L) {
      return false;
    }
    return true;
  }
  
  public String b()
  {
    return e(this.c.a);
  }
  
  public String c()
  {
    return e(this.c.b);
  }
  
  public String d()
  {
    return e(this.c.A);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String e()
  {
    return e(this.c.B);
  }
  
  public boolean equals(Object paramObject)
  {
    return PlayerEntity.a(this, paramObject);
  }
  
  public boolean f()
  {
    return d(this.c.z);
  }
  
  public Uri g()
  {
    return h(this.c.c);
  }
  
  public String getBannerImageLandscapeUrl()
  {
    return e(this.c.D);
  }
  
  public String getBannerImagePortraitUrl()
  {
    return e(this.c.F);
  }
  
  public String getHiResImageUrl()
  {
    return e(this.c.f);
  }
  
  public String getIconImageUrl()
  {
    return e(this.c.d);
  }
  
  public Uri h()
  {
    return h(this.c.e);
  }
  
  public int hashCode()
  {
    return PlayerEntity.a(this);
  }
  
  public long i()
  {
    return b(this.c.g);
  }
  
  public long j()
  {
    if ((!a_(this.c.i)) || (i(this.c.i))) {
      return -1L;
    }
    return b(this.c.i);
  }
  
  public int k()
  {
    return c(this.c.h);
  }
  
  public boolean l()
  {
    return d(this.c.s);
  }
  
  public String m()
  {
    return e(this.c.q);
  }
  
  public PlayerLevelInfo n()
  {
    return this.d;
  }
  
  public MostRecentGameInfo o()
  {
    if (i(this.c.t)) {
      return null;
    }
    return this.e;
  }
  
  public Uri p()
  {
    return h(this.c.C);
  }
  
  public Uri q()
  {
    return h(this.c.E);
  }
  
  public Player r()
  {
    return new PlayerEntity(this);
  }
  
  public String toString()
  {
    return PlayerEntity.b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((PlayerEntity)r()).writeToParcel(paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/PlayerRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */