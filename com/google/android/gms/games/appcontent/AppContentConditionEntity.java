package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;

public final class AppContentConditionEntity
  implements SafeParcelable, AppContentCondition
{
  public static final AppContentConditionEntityCreator CREATOR = new AppContentConditionEntityCreator();
  private final int a;
  private final String b;
  private final String c;
  private final String d;
  private final Bundle e;
  
  AppContentConditionEntity(int paramInt, String paramString1, String paramString2, String paramString3, Bundle paramBundle)
  {
    this.a = paramInt;
    this.b = paramString1;
    this.c = paramString2;
    this.d = paramString3;
    this.e = paramBundle;
  }
  
  public AppContentConditionEntity(AppContentCondition paramAppContentCondition)
  {
    this.a = 1;
    this.b = paramAppContentCondition.b();
    this.c = paramAppContentCondition.c();
    this.d = paramAppContentCondition.d();
    this.e = paramAppContentCondition.e();
  }
  
  static int a(AppContentCondition paramAppContentCondition)
  {
    return zzw.a(new Object[] { paramAppContentCondition.b(), paramAppContentCondition.c(), paramAppContentCondition.d(), paramAppContentCondition.e() });
  }
  
  static boolean a(AppContentCondition paramAppContentCondition, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof AppContentCondition)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramAppContentCondition == paramObject);
      paramObject = (AppContentCondition)paramObject;
      if ((!zzw.a(((AppContentCondition)paramObject).b(), paramAppContentCondition.b())) || (!zzw.a(((AppContentCondition)paramObject).c(), paramAppContentCondition.c())) || (!zzw.a(((AppContentCondition)paramObject).d(), paramAppContentCondition.d()))) {
        break;
      }
      bool1 = bool2;
    } while (zzw.a(((AppContentCondition)paramObject).e(), paramAppContentCondition.e()));
    return false;
  }
  
  static String b(AppContentCondition paramAppContentCondition)
  {
    return zzw.a(paramAppContentCondition).a("DefaultValue", paramAppContentCondition.b()).a("ExpectedValue", paramAppContentCondition.c()).a("Predicate", paramAppContentCondition.d()).a("PredicateParameters", paramAppContentCondition.e()).toString();
  }
  
  public String b()
  {
    return this.b;
  }
  
  public String c()
  {
    return this.c;
  }
  
  public String d()
  {
    return this.d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Bundle e()
  {
    return this.e;
  }
  
  public boolean equals(Object paramObject)
  {
    return a(this, paramObject);
  }
  
  public int f()
  {
    return this.a;
  }
  
  public AppContentCondition g()
  {
    return this;
  }
  
  public int hashCode()
  {
    return a(this);
  }
  
  public String toString()
  {
    return b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    AppContentConditionEntityCreator.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentConditionEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */