package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcelable;
import com.google.android.gms.common.data.Freezable;
import java.util.List;

public abstract interface AppContentCard
  extends Parcelable, Freezable<AppContentCard>
{
  public abstract List<AppContentAction> b();
  
  public abstract List<AppContentAnnotation> c();
  
  public abstract List<AppContentCondition> d();
  
  public abstract String e();
  
  public abstract int f();
  
  public abstract String g();
  
  public abstract Bundle h();
  
  public abstract String i();
  
  public abstract String j();
  
  public abstract String k();
  
  public abstract int l();
  
  public abstract String m();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentCard.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */