package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcelable;
import com.google.android.gms.common.data.Freezable;
import java.util.List;

public abstract interface AppContentSection
  extends Parcelable, Freezable<AppContentSection>
{
  public abstract List<AppContentAction> b();
  
  public abstract List<AppContentAnnotation> c();
  
  public abstract List<AppContentCard> d();
  
  public abstract String e();
  
  public abstract String f();
  
  public abstract Bundle g();
  
  public abstract String h();
  
  public abstract String i();
  
  public abstract String j();
  
  public abstract String k();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentSection.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */