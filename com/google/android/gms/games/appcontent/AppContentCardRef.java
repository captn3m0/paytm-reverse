package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import java.util.ArrayList;
import java.util.List;

public final class AppContentCardRef
  extends MultiDataBufferRef
  implements AppContentCard
{
  AppContentCardRef(ArrayList<DataHolder> paramArrayList, int paramInt)
  {
    super(paramArrayList, 0, paramInt);
  }
  
  public List<AppContentAction> b()
  {
    return AppContentUtils.a(this.a, this.c, "card_actions", this.b);
  }
  
  public List<AppContentAnnotation> c()
  {
    return AppContentUtils.b(this.a, this.c, "card_annotations", this.b);
  }
  
  public List<AppContentCondition> d()
  {
    return AppContentUtils.c(this.a, this.c, "card_conditions", this.b);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String e()
  {
    return e("card_content_description");
  }
  
  public boolean equals(Object paramObject)
  {
    return AppContentCardEntity.a(this, paramObject);
  }
  
  public int f()
  {
    return c("card_current_steps");
  }
  
  public String g()
  {
    return e("card_description");
  }
  
  public Bundle h()
  {
    return AppContentUtils.d(this.a, this.c, "card_data", this.b);
  }
  
  public int hashCode()
  {
    return AppContentCardEntity.a(this);
  }
  
  public String i()
  {
    return e("card_id");
  }
  
  public String j()
  {
    return e("card_subtitle");
  }
  
  public String k()
  {
    return e("card_title");
  }
  
  public int l()
  {
    return c("card_total_steps");
  }
  
  public String m()
  {
    return e("card_type");
  }
  
  public AppContentCard n()
  {
    return new AppContentCardEntity(this);
  }
  
  public String toString()
  {
    return AppContentCardEntity.b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((AppContentCardEntity)n()).writeToParcel(paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentCardRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */