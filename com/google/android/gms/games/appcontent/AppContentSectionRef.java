package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import java.util.ArrayList;

public final class AppContentSectionRef
  extends MultiDataBufferRef
  implements AppContentSection
{
  private final int d;
  
  AppContentSectionRef(ArrayList<DataHolder> paramArrayList, int paramInt1, int paramInt2)
  {
    super(paramArrayList, 0, paramInt1);
    this.d = paramInt2;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String e()
  {
    return e("section_card_type");
  }
  
  public boolean equals(Object paramObject)
  {
    return AppContentSectionEntity.a(this, paramObject);
  }
  
  public String f()
  {
    return e("section_content_description");
  }
  
  public Bundle g()
  {
    return AppContentUtils.d(this.a, this.c, "section_data", this.b);
  }
  
  public String h()
  {
    return e("section_id");
  }
  
  public int hashCode()
  {
    return AppContentSectionEntity.a(this);
  }
  
  public String i()
  {
    return e("section_subtitle");
  }
  
  public String j()
  {
    return e("section_title");
  }
  
  public String k()
  {
    return e("section_type");
  }
  
  public ArrayList<AppContentAction> l()
  {
    return AppContentUtils.a(this.a, this.c, "section_actions", this.b);
  }
  
  public ArrayList<AppContentAnnotation> m()
  {
    return AppContentUtils.b(this.a, this.c, "section_annotations", this.b);
  }
  
  public ArrayList<AppContentCard> n()
  {
    ArrayList localArrayList = new ArrayList(this.d);
    int i = 0;
    while (i < this.d)
    {
      localArrayList.add(new AppContentCardRef(this.c, this.b + i));
      i += 1;
    }
    return localArrayList;
  }
  
  public AppContentSection o()
  {
    return new AppContentSectionEntity(this);
  }
  
  public String toString()
  {
    return AppContentSectionEntity.b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((AppContentSectionEntity)o()).writeToParcel(paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentSectionRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */