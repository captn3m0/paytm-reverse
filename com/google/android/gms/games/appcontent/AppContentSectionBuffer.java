package com.google.android.gms.games.appcontent;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzf;
import java.util.ArrayList;

public final class AppContentSectionBuffer
  extends zzf<AppContentSection>
{
  private final ArrayList<DataHolder> b;
  
  protected AppContentSection b(int paramInt1, int paramInt2)
  {
    return new AppContentSectionRef(this.b, paramInt1, paramInt2);
  }
  
  protected String c()
  {
    return "section_id";
  }
  
  protected String d()
  {
    return "card_id";
  }
  
  public void release()
  {
    super.release();
    int j = this.b.size();
    int i = 1;
    while (i < j)
    {
      DataHolder localDataHolder = (DataHolder)this.b.get(i);
      if (localDataHolder != null) {
        localDataHolder.i();
      }
      i += 1;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentSectionBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */