package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import java.util.ArrayList;
import java.util.List;

public final class AppContentSectionEntity
  implements SafeParcelable, AppContentSection
{
  public static final AppContentSectionEntityCreator CREATOR = new AppContentSectionEntityCreator();
  private final int a;
  private final ArrayList<AppContentActionEntity> b;
  private final ArrayList<AppContentCardEntity> c;
  private final String d;
  private final Bundle e;
  private final String f;
  private final String g;
  private final String h;
  private final String i;
  private final String j;
  private final ArrayList<AppContentAnnotationEntity> k;
  
  AppContentSectionEntity(int paramInt, ArrayList<AppContentActionEntity> paramArrayList, ArrayList<AppContentCardEntity> paramArrayList1, String paramString1, Bundle paramBundle, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, ArrayList<AppContentAnnotationEntity> paramArrayList2)
  {
    this.a = paramInt;
    this.b = paramArrayList;
    this.k = paramArrayList2;
    this.c = paramArrayList1;
    this.j = paramString6;
    this.d = paramString1;
    this.e = paramBundle;
    this.i = paramString5;
    this.f = paramString2;
    this.g = paramString3;
    this.h = paramString4;
  }
  
  public AppContentSectionEntity(AppContentSection paramAppContentSection)
  {
    this.a = 5;
    this.j = paramAppContentSection.e();
    this.d = paramAppContentSection.f();
    this.e = paramAppContentSection.g();
    this.i = paramAppContentSection.h();
    this.f = paramAppContentSection.i();
    this.g = paramAppContentSection.j();
    this.h = paramAppContentSection.k();
    List localList = paramAppContentSection.b();
    int i1 = localList.size();
    this.b = new ArrayList(i1);
    int m = 0;
    while (m < i1)
    {
      this.b.add((AppContentActionEntity)((AppContentAction)localList.get(m)).a());
      m += 1;
    }
    localList = paramAppContentSection.d();
    i1 = localList.size();
    this.c = new ArrayList(i1);
    m = 0;
    while (m < i1)
    {
      this.c.add((AppContentCardEntity)((AppContentCard)localList.get(m)).a());
      m += 1;
    }
    paramAppContentSection = paramAppContentSection.c();
    i1 = paramAppContentSection.size();
    this.k = new ArrayList(i1);
    m = n;
    while (m < i1)
    {
      this.k.add((AppContentAnnotationEntity)((AppContentAnnotation)paramAppContentSection.get(m)).a());
      m += 1;
    }
  }
  
  static int a(AppContentSection paramAppContentSection)
  {
    return zzw.a(new Object[] { paramAppContentSection.b(), paramAppContentSection.c(), paramAppContentSection.d(), paramAppContentSection.e(), paramAppContentSection.f(), paramAppContentSection.g(), paramAppContentSection.h(), paramAppContentSection.i(), paramAppContentSection.j(), paramAppContentSection.k() });
  }
  
  static boolean a(AppContentSection paramAppContentSection, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof AppContentSection)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramAppContentSection == paramObject);
      paramObject = (AppContentSection)paramObject;
      if ((!zzw.a(((AppContentSection)paramObject).b(), paramAppContentSection.b())) || (!zzw.a(((AppContentSection)paramObject).c(), paramAppContentSection.c())) || (!zzw.a(((AppContentSection)paramObject).d(), paramAppContentSection.d())) || (!zzw.a(((AppContentSection)paramObject).e(), paramAppContentSection.e())) || (!zzw.a(((AppContentSection)paramObject).f(), paramAppContentSection.f())) || (!zzw.a(((AppContentSection)paramObject).g(), paramAppContentSection.g())) || (!zzw.a(((AppContentSection)paramObject).h(), paramAppContentSection.h())) || (!zzw.a(((AppContentSection)paramObject).i(), paramAppContentSection.i())) || (!zzw.a(((AppContentSection)paramObject).j(), paramAppContentSection.j()))) {
        break;
      }
      bool1 = bool2;
    } while (zzw.a(((AppContentSection)paramObject).k(), paramAppContentSection.k()));
    return false;
  }
  
  static String b(AppContentSection paramAppContentSection)
  {
    return zzw.a(paramAppContentSection).a("Actions", paramAppContentSection.b()).a("Annotations", paramAppContentSection.c()).a("Cards", paramAppContentSection.d()).a("CardType", paramAppContentSection.e()).a("ContentDescription", paramAppContentSection.f()).a("Extras", paramAppContentSection.g()).a("Id", paramAppContentSection.h()).a("Subtitle", paramAppContentSection.i()).a("Title", paramAppContentSection.j()).a("Type", paramAppContentSection.k()).toString();
  }
  
  public List<AppContentAction> b()
  {
    return new ArrayList(this.b);
  }
  
  public List<AppContentAnnotation> c()
  {
    return new ArrayList(this.k);
  }
  
  public List<AppContentCard> d()
  {
    return new ArrayList(this.c);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String e()
  {
    return this.j;
  }
  
  public boolean equals(Object paramObject)
  {
    return a(this, paramObject);
  }
  
  public String f()
  {
    return this.d;
  }
  
  public Bundle g()
  {
    return this.e;
  }
  
  public String h()
  {
    return this.i;
  }
  
  public int hashCode()
  {
    return a(this);
  }
  
  public String i()
  {
    return this.f;
  }
  
  public String j()
  {
    return this.g;
  }
  
  public String k()
  {
    return this.h;
  }
  
  public int l()
  {
    return this.a;
  }
  
  public AppContentSection m()
  {
    return this;
  }
  
  public String toString()
  {
    return b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    AppContentSectionEntityCreator.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentSectionEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */