package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.internal.zzmn;
import java.util.ArrayList;

public final class AppContentUtils
{
  public static ArrayList<AppContentAction> a(DataHolder paramDataHolder, ArrayList<DataHolder> paramArrayList, String paramString, int paramInt)
  {
    ArrayList localArrayList = new ArrayList();
    a(paramDataHolder, 1, paramString, "action_id", paramInt, new AppContentRunner()
    {
      public void a(ArrayList<DataHolder> paramAnonymousArrayList, int paramAnonymousInt)
      {
        this.a.add(new AppContentActionRef(paramAnonymousArrayList, paramAnonymousInt));
      }
    }, paramArrayList);
    return localArrayList;
  }
  
  private static void a(DataHolder paramDataHolder, int paramInt1, String paramString1, String paramString2, int paramInt2, AppContentRunner paramAppContentRunner, ArrayList<DataHolder> paramArrayList)
  {
    DataHolder localDataHolder = (DataHolder)paramArrayList.get(paramInt1);
    paramDataHolder = paramDataHolder.c(paramString1, paramInt2, paramDataHolder.a(paramInt2));
    if (!TextUtils.isEmpty(paramDataHolder))
    {
      paramInt2 = localDataHolder.g();
      paramDataHolder = paramDataHolder.split(",");
      paramInt1 = 0;
      while (paramInt1 < paramInt2)
      {
        paramString1 = localDataHolder.c(paramString2, paramInt1, localDataHolder.a(paramInt1));
        if ((!TextUtils.isEmpty(paramString1)) && (zzmn.b(paramDataHolder, paramString1))) {
          paramAppContentRunner.a(paramArrayList, paramInt1);
        }
        paramInt1 += 1;
      }
    }
  }
  
  public static ArrayList<AppContentAnnotation> b(DataHolder paramDataHolder, ArrayList<DataHolder> paramArrayList, String paramString, int paramInt)
  {
    ArrayList localArrayList = new ArrayList();
    a(paramDataHolder, 2, paramString, "annotation_id", paramInt, new AppContentRunner()
    {
      public void a(ArrayList<DataHolder> paramAnonymousArrayList, int paramAnonymousInt)
      {
        this.a.add(new AppContentAnnotationRef(paramAnonymousArrayList, paramAnonymousInt));
      }
    }, paramArrayList);
    return localArrayList;
  }
  
  public static ArrayList<AppContentCondition> c(DataHolder paramDataHolder, ArrayList<DataHolder> paramArrayList, String paramString, int paramInt)
  {
    ArrayList localArrayList = new ArrayList();
    a(paramDataHolder, 4, paramString, "condition_id", paramInt, new AppContentRunner()
    {
      public void a(ArrayList<DataHolder> paramAnonymousArrayList, int paramAnonymousInt)
      {
        this.a.add(new AppContentConditionRef(paramAnonymousArrayList, paramAnonymousInt));
      }
    }, paramArrayList);
    return localArrayList;
  }
  
  public static Bundle d(DataHolder paramDataHolder, ArrayList<DataHolder> paramArrayList, String paramString, int paramInt)
  {
    final Bundle localBundle = new Bundle();
    a(paramDataHolder, 3, paramString, "tuple_id", paramInt, new AppContentRunner()
    {
      public void a(ArrayList<DataHolder> paramAnonymousArrayList, int paramAnonymousInt)
      {
        paramAnonymousArrayList = new AppContentTupleRef(this.a, paramAnonymousInt);
        localBundle.putString(paramAnonymousArrayList.b(), paramAnonymousArrayList.c());
      }
    }, paramArrayList);
    return localBundle;
  }
  
  private static abstract interface AppContentRunner
  {
    public abstract void a(ArrayList<DataHolder> paramArrayList, int paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */