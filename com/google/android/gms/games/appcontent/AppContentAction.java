package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcelable;
import com.google.android.gms.common.data.Freezable;
import java.util.List;

public abstract interface AppContentAction
  extends Parcelable, Freezable<AppContentAction>
{
  public abstract AppContentAnnotation b();
  
  public abstract List<AppContentCondition> c();
  
  public abstract String d();
  
  public abstract Bundle e();
  
  public abstract String f();
  
  public abstract String g();
  
  public abstract String h();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentAction.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */