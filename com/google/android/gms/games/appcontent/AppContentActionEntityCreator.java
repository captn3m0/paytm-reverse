package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class AppContentActionEntityCreator
  implements Parcelable.Creator<AppContentActionEntity>
{
  static void a(AppContentActionEntity paramAppContentActionEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.c(paramParcel, 1, paramAppContentActionEntity.c(), false);
    zzb.a(paramParcel, 1000, paramAppContentActionEntity.i());
    zzb.a(paramParcel, 2, paramAppContentActionEntity.d(), false);
    zzb.a(paramParcel, 3, paramAppContentActionEntity.e(), false);
    zzb.a(paramParcel, 6, paramAppContentActionEntity.h(), false);
    zzb.a(paramParcel, 7, paramAppContentActionEntity.f(), false);
    zzb.a(paramParcel, 8, paramAppContentActionEntity.b(), paramInt, false);
    zzb.a(paramParcel, 9, paramAppContentActionEntity.g(), false);
    zzb.a(paramParcel, i);
  }
  
  public AppContentActionEntity a(Parcel paramParcel)
  {
    String str1 = null;
    int j = zza.b(paramParcel);
    int i = 0;
    AppContentAnnotationEntity localAppContentAnnotationEntity = null;
    String str2 = null;
    String str3 = null;
    Bundle localBundle = null;
    String str4 = null;
    ArrayList localArrayList = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localArrayList = zza.c(paramParcel, k, AppContentConditionEntity.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str4 = zza.p(paramParcel, k);
        break;
      case 3: 
        localBundle = zza.r(paramParcel, k);
        break;
      case 6: 
        str3 = zza.p(paramParcel, k);
        break;
      case 7: 
        str2 = zza.p(paramParcel, k);
        break;
      case 8: 
        localAppContentAnnotationEntity = (AppContentAnnotationEntity)zza.a(paramParcel, k, AppContentAnnotationEntity.CREATOR);
        break;
      case 9: 
        str1 = zza.p(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new AppContentActionEntity(i, localArrayList, str4, localBundle, str3, str2, localAppContentAnnotationEntity, str1);
  }
  
  public AppContentActionEntity[] a(int paramInt)
  {
    return new AppContentActionEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentActionEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */