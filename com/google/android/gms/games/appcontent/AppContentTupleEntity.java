package com.google.android.gms.games.appcontent;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;

public final class AppContentTupleEntity
  implements SafeParcelable, AppContentTuple
{
  public static final AppContentTupleEntityCreator CREATOR = new AppContentTupleEntityCreator();
  private final int a;
  private final String b;
  private final String c;
  
  AppContentTupleEntity(int paramInt, String paramString1, String paramString2)
  {
    this.a = paramInt;
    this.b = paramString1;
    this.c = paramString2;
  }
  
  public AppContentTupleEntity(AppContentTuple paramAppContentTuple)
  {
    this.a = 1;
    this.b = paramAppContentTuple.b();
    this.c = paramAppContentTuple.c();
  }
  
  static int a(AppContentTuple paramAppContentTuple)
  {
    return zzw.a(new Object[] { paramAppContentTuple.b(), paramAppContentTuple.c() });
  }
  
  static boolean a(AppContentTuple paramAppContentTuple, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof AppContentTuple)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramAppContentTuple == paramObject);
      paramObject = (AppContentTuple)paramObject;
      if (!zzw.a(((AppContentTuple)paramObject).b(), paramAppContentTuple.b())) {
        break;
      }
      bool1 = bool2;
    } while (zzw.a(((AppContentTuple)paramObject).c(), paramAppContentTuple.c()));
    return false;
  }
  
  static String b(AppContentTuple paramAppContentTuple)
  {
    return zzw.a(paramAppContentTuple).a("Name", paramAppContentTuple.b()).a("Value", paramAppContentTuple.c()).toString();
  }
  
  public String b()
  {
    return this.b;
  }
  
  public String c()
  {
    return this.c;
  }
  
  public int d()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public AppContentTuple e()
  {
    return this;
  }
  
  public boolean equals(Object paramObject)
  {
    return a(this, paramObject);
  }
  
  public int hashCode()
  {
    return a(this);
  }
  
  public String toString()
  {
    return b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    AppContentTupleEntityCreator.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentTupleEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */