package com.google.android.gms.games.appcontent;

import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;

public final class AppContentTupleRef
  extends zzc
  implements AppContentTuple
{
  AppContentTupleRef(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
  }
  
  public String b()
  {
    return e("tuple_name");
  }
  
  public String c()
  {
    return e("tuple_value");
  }
  
  public AppContentTuple d()
  {
    return new AppContentTupleEntity(this);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return AppContentTupleEntity.a(this, paramObject);
  }
  
  public int hashCode()
  {
    return AppContentTupleEntity.a(this);
  }
  
  public String toString()
  {
    return AppContentTupleEntity.b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((AppContentTupleEntity)d()).writeToParcel(paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentTupleRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */