package com.google.android.gms.games.appcontent;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;

public final class AppContentAnnotationEntity
  implements SafeParcelable, AppContentAnnotation
{
  public static final AppContentAnnotationEntityCreator CREATOR = new AppContentAnnotationEntityCreator();
  private final int a;
  private final String b;
  private final Uri c;
  private final String d;
  private final String e;
  private final String f;
  private final String g;
  private final int h;
  private final int i;
  private final Bundle j;
  
  AppContentAnnotationEntity(int paramInt1, String paramString1, Uri paramUri, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt2, int paramInt3, Bundle paramBundle)
  {
    this.a = paramInt1;
    this.b = paramString1;
    this.e = paramString3;
    this.g = paramString5;
    this.h = paramInt2;
    this.c = paramUri;
    this.i = paramInt3;
    this.f = paramString4;
    this.j = paramBundle;
    this.d = paramString2;
  }
  
  public AppContentAnnotationEntity(AppContentAnnotation paramAppContentAnnotation)
  {
    this.a = 4;
    this.b = paramAppContentAnnotation.b();
    this.e = paramAppContentAnnotation.c();
    this.g = paramAppContentAnnotation.d();
    this.h = paramAppContentAnnotation.e();
    this.c = paramAppContentAnnotation.f();
    this.i = paramAppContentAnnotation.h();
    this.f = paramAppContentAnnotation.i();
    this.j = paramAppContentAnnotation.g();
    this.d = paramAppContentAnnotation.j();
  }
  
  static int a(AppContentAnnotation paramAppContentAnnotation)
  {
    return zzw.a(new Object[] { paramAppContentAnnotation.b(), paramAppContentAnnotation.c(), paramAppContentAnnotation.d(), Integer.valueOf(paramAppContentAnnotation.e()), paramAppContentAnnotation.f(), Integer.valueOf(paramAppContentAnnotation.h()), paramAppContentAnnotation.i(), paramAppContentAnnotation.g(), paramAppContentAnnotation.j() });
  }
  
  static boolean a(AppContentAnnotation paramAppContentAnnotation, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof AppContentAnnotation)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramAppContentAnnotation == paramObject);
      paramObject = (AppContentAnnotation)paramObject;
      if ((!zzw.a(((AppContentAnnotation)paramObject).b(), paramAppContentAnnotation.b())) || (!zzw.a(((AppContentAnnotation)paramObject).c(), paramAppContentAnnotation.c())) || (!zzw.a(((AppContentAnnotation)paramObject).d(), paramAppContentAnnotation.d())) || (!zzw.a(Integer.valueOf(((AppContentAnnotation)paramObject).e()), Integer.valueOf(paramAppContentAnnotation.e()))) || (!zzw.a(((AppContentAnnotation)paramObject).f(), paramAppContentAnnotation.f())) || (!zzw.a(Integer.valueOf(((AppContentAnnotation)paramObject).h()), Integer.valueOf(paramAppContentAnnotation.h()))) || (!zzw.a(((AppContentAnnotation)paramObject).i(), paramAppContentAnnotation.i())) || (!zzw.a(((AppContentAnnotation)paramObject).g(), paramAppContentAnnotation.g()))) {
        break;
      }
      bool1 = bool2;
    } while (zzw.a(((AppContentAnnotation)paramObject).j(), paramAppContentAnnotation.j()));
    return false;
  }
  
  static String b(AppContentAnnotation paramAppContentAnnotation)
  {
    return zzw.a(paramAppContentAnnotation).a("Description", paramAppContentAnnotation.b()).a("Id", paramAppContentAnnotation.c()).a("ImageDefaultId", paramAppContentAnnotation.d()).a("ImageHeight", Integer.valueOf(paramAppContentAnnotation.e())).a("ImageUri", paramAppContentAnnotation.f()).a("ImageWidth", Integer.valueOf(paramAppContentAnnotation.h())).a("LayoutSlot", paramAppContentAnnotation.i()).a("Modifiers", paramAppContentAnnotation.g()).a("Title", paramAppContentAnnotation.j()).toString();
  }
  
  public String b()
  {
    return this.b;
  }
  
  public String c()
  {
    return this.e;
  }
  
  public String d()
  {
    return this.g;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public int e()
  {
    return this.h;
  }
  
  public boolean equals(Object paramObject)
  {
    return a(this, paramObject);
  }
  
  public Uri f()
  {
    return this.c;
  }
  
  public Bundle g()
  {
    return this.j;
  }
  
  public int h()
  {
    return this.i;
  }
  
  public int hashCode()
  {
    return a(this);
  }
  
  public String i()
  {
    return this.f;
  }
  
  public String j()
  {
    return this.d;
  }
  
  public int k()
  {
    return this.a;
  }
  
  public AppContentAnnotation l()
  {
    return this;
  }
  
  public String toString()
  {
    return b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    AppContentAnnotationEntityCreator.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentAnnotationEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */