package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class AppContentSectionEntityCreator
  implements Parcelable.Creator<AppContentSectionEntity>
{
  static void a(AppContentSectionEntity paramAppContentSectionEntity, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.c(paramParcel, 1, paramAppContentSectionEntity.b(), false);
    zzb.a(paramParcel, 1000, paramAppContentSectionEntity.l());
    zzb.c(paramParcel, 3, paramAppContentSectionEntity.d(), false);
    zzb.a(paramParcel, 4, paramAppContentSectionEntity.f(), false);
    zzb.a(paramParcel, 5, paramAppContentSectionEntity.g(), false);
    zzb.a(paramParcel, 6, paramAppContentSectionEntity.i(), false);
    zzb.a(paramParcel, 7, paramAppContentSectionEntity.j(), false);
    zzb.a(paramParcel, 8, paramAppContentSectionEntity.k(), false);
    zzb.a(paramParcel, 9, paramAppContentSectionEntity.h(), false);
    zzb.a(paramParcel, 10, paramAppContentSectionEntity.e(), false);
    zzb.c(paramParcel, 14, paramAppContentSectionEntity.c(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public AppContentSectionEntity a(Parcel paramParcel)
  {
    ArrayList localArrayList1 = null;
    int j = zza.b(paramParcel);
    int i = 0;
    String str1 = null;
    String str2 = null;
    String str3 = null;
    String str4 = null;
    String str5 = null;
    Bundle localBundle = null;
    String str6 = null;
    ArrayList localArrayList2 = null;
    ArrayList localArrayList3 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localArrayList3 = zza.c(paramParcel, k, AppContentActionEntity.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 3: 
        localArrayList2 = zza.c(paramParcel, k, AppContentCardEntity.CREATOR);
        break;
      case 4: 
        str6 = zza.p(paramParcel, k);
        break;
      case 5: 
        localBundle = zza.r(paramParcel, k);
        break;
      case 6: 
        str5 = zza.p(paramParcel, k);
        break;
      case 7: 
        str4 = zza.p(paramParcel, k);
        break;
      case 8: 
        str3 = zza.p(paramParcel, k);
        break;
      case 9: 
        str2 = zza.p(paramParcel, k);
        break;
      case 10: 
        str1 = zza.p(paramParcel, k);
        break;
      case 14: 
        localArrayList1 = zza.c(paramParcel, k, AppContentAnnotationEntity.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new AppContentSectionEntity(i, localArrayList3, localArrayList2, str6, localBundle, str5, str4, str3, str2, str1, localArrayList1);
  }
  
  public AppContentSectionEntity[] a(int paramInt)
  {
    return new AppContentSectionEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentSectionEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */