package com.google.android.gms.games.appcontent;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class AppContentAnnotationEntityCreator
  implements Parcelable.Creator<AppContentAnnotationEntity>
{
  static void a(AppContentAnnotationEntity paramAppContentAnnotationEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramAppContentAnnotationEntity.b(), false);
    zzb.a(paramParcel, 1000, paramAppContentAnnotationEntity.k());
    zzb.a(paramParcel, 2, paramAppContentAnnotationEntity.f(), paramInt, false);
    zzb.a(paramParcel, 3, paramAppContentAnnotationEntity.j(), false);
    zzb.a(paramParcel, 5, paramAppContentAnnotationEntity.c(), false);
    zzb.a(paramParcel, 6, paramAppContentAnnotationEntity.i(), false);
    zzb.a(paramParcel, 7, paramAppContentAnnotationEntity.d(), false);
    zzb.a(paramParcel, 8, paramAppContentAnnotationEntity.e());
    zzb.a(paramParcel, 9, paramAppContentAnnotationEntity.h());
    zzb.a(paramParcel, 10, paramAppContentAnnotationEntity.g(), false);
    zzb.a(paramParcel, i);
  }
  
  public AppContentAnnotationEntity a(Parcel paramParcel)
  {
    int i = 0;
    Bundle localBundle = null;
    int m = zza.b(paramParcel);
    int j = 0;
    String str1 = null;
    String str2 = null;
    String str3 = null;
    String str4 = null;
    Uri localUri = null;
    String str5 = null;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        str5 = zza.p(paramParcel, n);
        break;
      case 1000: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        localUri = (Uri)zza.a(paramParcel, n, Uri.CREATOR);
        break;
      case 3: 
        str4 = zza.p(paramParcel, n);
        break;
      case 5: 
        str3 = zza.p(paramParcel, n);
        break;
      case 6: 
        str2 = zza.p(paramParcel, n);
        break;
      case 7: 
        str1 = zza.p(paramParcel, n);
        break;
      case 8: 
        j = zza.g(paramParcel, n);
        break;
      case 9: 
        i = zza.g(paramParcel, n);
        break;
      case 10: 
        localBundle = zza.r(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new AppContentAnnotationEntity(k, str5, localUri, str4, str3, str2, str1, j, i, localBundle);
  }
  
  public AppContentAnnotationEntity[] a(int paramInt)
  {
    return new AppContentAnnotationEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentAnnotationEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */