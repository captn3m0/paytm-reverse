package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import java.util.ArrayList;
import java.util.List;

public final class AppContentActionEntity
  implements SafeParcelable, AppContentAction
{
  public static final AppContentActionEntityCreator CREATOR = new AppContentActionEntityCreator();
  private final int a;
  private final ArrayList<AppContentConditionEntity> b;
  private final String c;
  private final Bundle d;
  private final String e;
  private final String f;
  private final AppContentAnnotationEntity g;
  private final String h;
  
  AppContentActionEntity(int paramInt, ArrayList<AppContentConditionEntity> paramArrayList, String paramString1, Bundle paramBundle, String paramString2, String paramString3, AppContentAnnotationEntity paramAppContentAnnotationEntity, String paramString4)
  {
    this.a = paramInt;
    this.g = paramAppContentAnnotationEntity;
    this.b = paramArrayList;
    this.c = paramString1;
    this.d = paramBundle;
    this.f = paramString3;
    this.h = paramString4;
    this.e = paramString2;
  }
  
  public AppContentActionEntity(AppContentAction paramAppContentAction)
  {
    this.a = 5;
    this.g = ((AppContentAnnotationEntity)paramAppContentAction.b().a());
    this.c = paramAppContentAction.d();
    this.d = paramAppContentAction.e();
    this.f = paramAppContentAction.f();
    this.h = paramAppContentAction.g();
    this.e = paramAppContentAction.h();
    paramAppContentAction = paramAppContentAction.c();
    int j = paramAppContentAction.size();
    this.b = new ArrayList(j);
    int i = 0;
    while (i < j)
    {
      this.b.add((AppContentConditionEntity)((AppContentCondition)paramAppContentAction.get(i)).a());
      i += 1;
    }
  }
  
  static int a(AppContentAction paramAppContentAction)
  {
    return zzw.a(new Object[] { paramAppContentAction.b(), paramAppContentAction.c(), paramAppContentAction.d(), paramAppContentAction.e(), paramAppContentAction.f(), paramAppContentAction.g(), paramAppContentAction.h() });
  }
  
  static boolean a(AppContentAction paramAppContentAction, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof AppContentAction)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramAppContentAction == paramObject);
      paramObject = (AppContentAction)paramObject;
      if ((!zzw.a(((AppContentAction)paramObject).b(), paramAppContentAction.b())) || (!zzw.a(((AppContentAction)paramObject).c(), paramAppContentAction.c())) || (!zzw.a(((AppContentAction)paramObject).d(), paramAppContentAction.d())) || (!zzw.a(((AppContentAction)paramObject).e(), paramAppContentAction.e())) || (!zzw.a(((AppContentAction)paramObject).f(), paramAppContentAction.f())) || (!zzw.a(((AppContentAction)paramObject).g(), paramAppContentAction.g()))) {
        break;
      }
      bool1 = bool2;
    } while (zzw.a(((AppContentAction)paramObject).h(), paramAppContentAction.h()));
    return false;
  }
  
  static String b(AppContentAction paramAppContentAction)
  {
    return zzw.a(paramAppContentAction).a("Annotation", paramAppContentAction.b()).a("Conditions", paramAppContentAction.c()).a("ContentDescription", paramAppContentAction.d()).a("Extras", paramAppContentAction.e()).a("Id", paramAppContentAction.f()).a("OverflowText", paramAppContentAction.g()).a("Type", paramAppContentAction.h()).toString();
  }
  
  public AppContentAnnotation b()
  {
    return this.g;
  }
  
  public List<AppContentCondition> c()
  {
    return new ArrayList(this.b);
  }
  
  public String d()
  {
    return this.c;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Bundle e()
  {
    return this.d;
  }
  
  public boolean equals(Object paramObject)
  {
    return a(this, paramObject);
  }
  
  public String f()
  {
    return this.f;
  }
  
  public String g()
  {
    return this.h;
  }
  
  public String h()
  {
    return this.e;
  }
  
  public int hashCode()
  {
    return a(this);
  }
  
  public int i()
  {
    return this.a;
  }
  
  public AppContentAction j()
  {
    return this;
  }
  
  public String toString()
  {
    return b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    AppContentActionEntityCreator.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentActionEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */