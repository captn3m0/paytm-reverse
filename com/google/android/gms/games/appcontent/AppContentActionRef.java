package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import java.util.ArrayList;
import java.util.List;

public final class AppContentActionRef
  extends MultiDataBufferRef
  implements AppContentAction
{
  AppContentActionRef(ArrayList<DataHolder> paramArrayList, int paramInt)
  {
    super(paramArrayList, 1, paramInt);
  }
  
  public AppContentAnnotation b()
  {
    ArrayList localArrayList = AppContentUtils.b(this.a, this.c, "action_annotation", this.b);
    if (localArrayList.size() == 1) {
      return (AppContentAnnotation)localArrayList.get(0);
    }
    return null;
  }
  
  public List<AppContentCondition> c()
  {
    return AppContentUtils.c(this.a, this.c, "action_conditions", this.b);
  }
  
  public String d()
  {
    return e("action_content_description");
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Bundle e()
  {
    return AppContentUtils.d(this.a, this.c, "action_data", this.b);
  }
  
  public boolean equals(Object paramObject)
  {
    return AppContentActionEntity.a(this, paramObject);
  }
  
  public String f()
  {
    return e("action_id");
  }
  
  public String g()
  {
    return e("overflow_text");
  }
  
  public String h()
  {
    return e("action_type");
  }
  
  public int hashCode()
  {
    return AppContentActionEntity.a(this);
  }
  
  public AppContentAction i()
  {
    return new AppContentActionEntity(this);
  }
  
  public String toString()
  {
    return AppContentActionEntity.b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((AppContentActionEntity)i()).writeToParcel(paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentActionRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */