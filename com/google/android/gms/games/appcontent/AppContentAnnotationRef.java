package com.google.android.gms.games.appcontent;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import java.util.ArrayList;

public final class AppContentAnnotationRef
  extends MultiDataBufferRef
  implements AppContentAnnotation
{
  AppContentAnnotationRef(ArrayList<DataHolder> paramArrayList, int paramInt)
  {
    super(paramArrayList, 2, paramInt);
  }
  
  public String b()
  {
    return e("annotation_description");
  }
  
  public String c()
  {
    return e("annotation_id");
  }
  
  public String d()
  {
    return e("annotation_image_default_id");
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public int e()
  {
    return c("annotation_image_height");
  }
  
  public boolean equals(Object paramObject)
  {
    return AppContentAnnotationEntity.a(this, paramObject);
  }
  
  public Uri f()
  {
    return h("annotation_image_uri");
  }
  
  public Bundle g()
  {
    return AppContentUtils.d(this.a, this.c, "annotation_modifiers", this.b);
  }
  
  public int h()
  {
    return c("annotation_image_width");
  }
  
  public int hashCode()
  {
    return AppContentAnnotationEntity.a(this);
  }
  
  public String i()
  {
    return e("annotation_layout_slot");
  }
  
  public String j()
  {
    return e("annotation_title");
  }
  
  public AppContentAnnotation k()
  {
    return new AppContentAnnotationEntity(this);
  }
  
  public String toString()
  {
    return AppContentAnnotationEntity.b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((AppContentAnnotationEntity)k()).writeToParcel(paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentAnnotationRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */