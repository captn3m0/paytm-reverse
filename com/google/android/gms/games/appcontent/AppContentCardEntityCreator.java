package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class AppContentCardEntityCreator
  implements Parcelable.Creator<AppContentCardEntity>
{
  static void a(AppContentCardEntity paramAppContentCardEntity, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.c(paramParcel, 1, paramAppContentCardEntity.b(), false);
    zzb.a(paramParcel, 1000, paramAppContentCardEntity.n());
    zzb.c(paramParcel, 2, paramAppContentCardEntity.c(), false);
    zzb.c(paramParcel, 3, paramAppContentCardEntity.d(), false);
    zzb.a(paramParcel, 4, paramAppContentCardEntity.e(), false);
    zzb.a(paramParcel, 5, paramAppContentCardEntity.f());
    zzb.a(paramParcel, 6, paramAppContentCardEntity.g(), false);
    zzb.a(paramParcel, 7, paramAppContentCardEntity.h(), false);
    zzb.a(paramParcel, 10, paramAppContentCardEntity.j(), false);
    zzb.a(paramParcel, 11, paramAppContentCardEntity.k(), false);
    zzb.a(paramParcel, 12, paramAppContentCardEntity.l());
    zzb.a(paramParcel, 13, paramAppContentCardEntity.m(), false);
    zzb.a(paramParcel, 14, paramAppContentCardEntity.i(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public AppContentCardEntity a(Parcel paramParcel)
  {
    int m = zza.b(paramParcel);
    int k = 0;
    ArrayList localArrayList3 = null;
    ArrayList localArrayList2 = null;
    ArrayList localArrayList1 = null;
    String str6 = null;
    int j = 0;
    String str5 = null;
    Bundle localBundle = null;
    String str4 = null;
    String str3 = null;
    int i = 0;
    String str2 = null;
    String str1 = null;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        localArrayList3 = zza.c(paramParcel, n, AppContentActionEntity.CREATOR);
        break;
      case 1000: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        localArrayList2 = zza.c(paramParcel, n, AppContentAnnotationEntity.CREATOR);
        break;
      case 3: 
        localArrayList1 = zza.c(paramParcel, n, AppContentConditionEntity.CREATOR);
        break;
      case 4: 
        str6 = zza.p(paramParcel, n);
        break;
      case 5: 
        j = zza.g(paramParcel, n);
        break;
      case 6: 
        str5 = zza.p(paramParcel, n);
        break;
      case 7: 
        localBundle = zza.r(paramParcel, n);
        break;
      case 10: 
        str4 = zza.p(paramParcel, n);
        break;
      case 11: 
        str3 = zza.p(paramParcel, n);
        break;
      case 12: 
        i = zza.g(paramParcel, n);
        break;
      case 13: 
        str2 = zza.p(paramParcel, n);
        break;
      case 14: 
        str1 = zza.p(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new AppContentCardEntity(k, localArrayList3, localArrayList2, localArrayList1, str6, j, str5, localBundle, str4, str3, i, str2, str1);
  }
  
  public AppContentCardEntity[] a(int paramInt)
  {
    return new AppContentCardEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentCardEntityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */