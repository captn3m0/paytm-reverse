package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import java.util.ArrayList;
import java.util.List;

public final class AppContentCardEntity
  implements SafeParcelable, AppContentCard
{
  public static final AppContentCardEntityCreator CREATOR = new AppContentCardEntityCreator();
  private final int a;
  private final ArrayList<AppContentActionEntity> b;
  private final ArrayList<AppContentAnnotationEntity> c;
  private final ArrayList<AppContentConditionEntity> d;
  private final String e;
  private final int f;
  private final String g;
  private final Bundle h;
  private final String i;
  private final String j;
  private final int k;
  private final String l;
  private final String m;
  
  AppContentCardEntity(int paramInt1, ArrayList<AppContentActionEntity> paramArrayList, ArrayList<AppContentAnnotationEntity> paramArrayList1, ArrayList<AppContentConditionEntity> paramArrayList2, String paramString1, int paramInt2, String paramString2, Bundle paramBundle, String paramString3, String paramString4, int paramInt3, String paramString5, String paramString6)
  {
    this.a = paramInt1;
    this.b = paramArrayList;
    this.c = paramArrayList1;
    this.d = paramArrayList2;
    this.e = paramString1;
    this.f = paramInt2;
    this.g = paramString2;
    this.h = paramBundle;
    this.m = paramString6;
    this.i = paramString3;
    this.j = paramString4;
    this.k = paramInt3;
    this.l = paramString5;
  }
  
  public AppContentCardEntity(AppContentCard paramAppContentCard)
  {
    this.a = 4;
    this.e = paramAppContentCard.e();
    this.f = paramAppContentCard.f();
    this.g = paramAppContentCard.g();
    this.h = paramAppContentCard.h();
    this.m = paramAppContentCard.i();
    this.j = paramAppContentCard.k();
    this.i = paramAppContentCard.j();
    this.k = paramAppContentCard.l();
    this.l = paramAppContentCard.m();
    List localList = paramAppContentCard.b();
    int i2 = localList.size();
    this.b = new ArrayList(i2);
    int n = 0;
    while (n < i2)
    {
      this.b.add((AppContentActionEntity)((AppContentAction)localList.get(n)).a());
      n += 1;
    }
    localList = paramAppContentCard.c();
    i2 = localList.size();
    this.c = new ArrayList(i2);
    n = 0;
    while (n < i2)
    {
      this.c.add((AppContentAnnotationEntity)((AppContentAnnotation)localList.get(n)).a());
      n += 1;
    }
    paramAppContentCard = paramAppContentCard.d();
    i2 = paramAppContentCard.size();
    this.d = new ArrayList(i2);
    n = i1;
    while (n < i2)
    {
      this.d.add((AppContentConditionEntity)((AppContentCondition)paramAppContentCard.get(n)).a());
      n += 1;
    }
  }
  
  static int a(AppContentCard paramAppContentCard)
  {
    return zzw.a(new Object[] { paramAppContentCard.b(), paramAppContentCard.c(), paramAppContentCard.d(), paramAppContentCard.e(), Integer.valueOf(paramAppContentCard.f()), paramAppContentCard.g(), paramAppContentCard.h(), paramAppContentCard.i(), paramAppContentCard.j(), paramAppContentCard.k(), Integer.valueOf(paramAppContentCard.l()), paramAppContentCard.m() });
  }
  
  static boolean a(AppContentCard paramAppContentCard, Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (!(paramObject instanceof AppContentCard)) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramAppContentCard == paramObject);
      paramObject = (AppContentCard)paramObject;
      if ((!zzw.a(((AppContentCard)paramObject).b(), paramAppContentCard.b())) || (!zzw.a(((AppContentCard)paramObject).c(), paramAppContentCard.c())) || (!zzw.a(((AppContentCard)paramObject).d(), paramAppContentCard.d())) || (!zzw.a(((AppContentCard)paramObject).e(), paramAppContentCard.e())) || (!zzw.a(Integer.valueOf(((AppContentCard)paramObject).f()), Integer.valueOf(paramAppContentCard.f()))) || (!zzw.a(((AppContentCard)paramObject).g(), paramAppContentCard.g())) || (!zzw.a(((AppContentCard)paramObject).h(), paramAppContentCard.h())) || (!zzw.a(((AppContentCard)paramObject).i(), paramAppContentCard.i())) || (!zzw.a(((AppContentCard)paramObject).j(), paramAppContentCard.j())) || (!zzw.a(((AppContentCard)paramObject).k(), paramAppContentCard.k())) || (!zzw.a(Integer.valueOf(((AppContentCard)paramObject).l()), Integer.valueOf(paramAppContentCard.l())))) {
        break;
      }
      bool1 = bool2;
    } while (zzw.a(((AppContentCard)paramObject).m(), paramAppContentCard.m()));
    return false;
  }
  
  static String b(AppContentCard paramAppContentCard)
  {
    return zzw.a(paramAppContentCard).a("Actions", paramAppContentCard.b()).a("Annotations", paramAppContentCard.c()).a("Conditions", paramAppContentCard.d()).a("ContentDescription", paramAppContentCard.e()).a("CurrentSteps", Integer.valueOf(paramAppContentCard.f())).a("Description", paramAppContentCard.g()).a("Extras", paramAppContentCard.h()).a("Id", paramAppContentCard.i()).a("Subtitle", paramAppContentCard.j()).a("Title", paramAppContentCard.k()).a("TotalSteps", Integer.valueOf(paramAppContentCard.l())).a("Type", paramAppContentCard.m()).toString();
  }
  
  public List<AppContentAction> b()
  {
    return new ArrayList(this.b);
  }
  
  public List<AppContentAnnotation> c()
  {
    return new ArrayList(this.c);
  }
  
  public List<AppContentCondition> d()
  {
    return new ArrayList(this.d);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String e()
  {
    return this.e;
  }
  
  public boolean equals(Object paramObject)
  {
    return a(this, paramObject);
  }
  
  public int f()
  {
    return this.f;
  }
  
  public String g()
  {
    return this.g;
  }
  
  public Bundle h()
  {
    return this.h;
  }
  
  public int hashCode()
  {
    return a(this);
  }
  
  public String i()
  {
    return this.m;
  }
  
  public String j()
  {
    return this.i;
  }
  
  public String k()
  {
    return this.j;
  }
  
  public int l()
  {
    return this.k;
  }
  
  public String m()
  {
    return this.l;
  }
  
  public int n()
  {
    return this.a;
  }
  
  public AppContentCard o()
  {
    return this;
  }
  
  public String toString()
  {
    return b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    AppContentCardEntityCreator.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentCardEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */