package com.google.android.gms.games.appcontent;

import android.os.Parcelable;
import com.google.android.gms.common.data.Freezable;

public abstract interface AppContentTuple
  extends Parcelable, Freezable<AppContentTuple>
{
  public abstract String b();
  
  public abstract String c();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentTuple.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */