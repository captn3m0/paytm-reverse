package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import java.util.ArrayList;

public final class AppContentConditionRef
  extends MultiDataBufferRef
  implements AppContentCondition
{
  AppContentConditionRef(ArrayList<DataHolder> paramArrayList, int paramInt)
  {
    super(paramArrayList, 4, paramInt);
  }
  
  public String b()
  {
    return e("condition_default_value");
  }
  
  public String c()
  {
    return e("condition_expected_value");
  }
  
  public String d()
  {
    return e("condition_predicate");
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Bundle e()
  {
    return AppContentUtils.d(this.a, this.c, "condition_predicate_parameters", this.b);
  }
  
  public boolean equals(Object paramObject)
  {
    return AppContentConditionEntity.a(this, paramObject);
  }
  
  public AppContentCondition f()
  {
    return new AppContentConditionEntity(this);
  }
  
  public int hashCode()
  {
    return AppContentConditionEntity.a(this);
  }
  
  public String toString()
  {
    return AppContentConditionEntity.b(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    ((AppContentConditionEntity)f()).writeToParcel(paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentConditionRef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */