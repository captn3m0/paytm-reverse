package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcelable;
import com.google.android.gms.common.data.Freezable;

public abstract interface AppContentCondition
  extends Parcelable, Freezable<AppContentCondition>
{
  public abstract String b();
  
  public abstract String c();
  
  public abstract String d();
  
  public abstract Bundle e();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/games/appcontent/AppContentCondition.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */