package com.google.android.gms.internal;

import android.os.ParcelFileDescriptor;
import com.google.android.gms.appdatasearch.GetRecentContextCall.Response;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zzb;

public abstract class zzkh<T>
  extends zzkg.zza
{
  protected zza.zzb<T> a;
  
  public zzkh(zza.zzb<T> paramzzb)
  {
    this.a = paramzzb;
  }
  
  public void a(GetRecentContextCall.Response paramResponse) {}
  
  public void a(Status paramStatus) {}
  
  public void a(Status paramStatus, ParcelFileDescriptor paramParcelFileDescriptor) {}
  
  public void a(Status paramStatus, boolean paramBoolean) {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzkh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */