package com.google.android.gms.internal;

import com.google.android.gms.auth.api.proxy.ProxyApi.ProxyResult;
import com.google.android.gms.auth.api.proxy.ProxyResponse;
import com.google.android.gms.common.api.Status;

class zzle
  implements ProxyApi.ProxyResult
{
  private Status a;
  private ProxyResponse b;
  
  public zzle(ProxyResponse paramProxyResponse)
  {
    this.b = paramProxyResponse;
    this.a = Status.a;
  }
  
  public zzle(Status paramStatus)
  {
    this.a = paramStatus;
  }
  
  public Status getStatus()
  {
    return this.a;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */