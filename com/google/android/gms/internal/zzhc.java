package com.google.android.gms.internal;

@zzhb
public class zzhc
{
  public final zzib a;
  public final zzbo b;
  public final zzhl c;
  public final zzek d;
  public final zzfy e;
  public final zzie f;
  public final zzhn g;
  public final zzhm h;
  public final zzhh i;
  
  zzhc(zzib paramzzib, zzbo paramzzbo, zzhl paramzzhl, zzek paramzzek, zzfy paramzzfy, zzie paramzzie, zzhn paramzzhn, zzhm paramzzhm, zzhh paramzzhh)
  {
    this.a = paramzzib;
    this.b = paramzzbo;
    this.c = paramzzhl;
    this.d = paramzzek;
    this.e = paramzzfy;
    this.f = paramzzie;
    this.g = paramzzhn;
    this.h = paramzzhm;
    this.i = paramzzhh;
  }
  
  public static zzhc a()
  {
    return new zzhc(new zzic(), new zzbn(), new zzho(), new zzel(), new zzfx(), new zzid(), new zzhq(), new zzhp(), null);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzhc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */