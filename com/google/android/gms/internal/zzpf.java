package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zzb;
import com.google.android.gms.fitness.SensorsApi;
import com.google.android.gms.fitness.data.zzl.zza;
import com.google.android.gms.fitness.request.DataSourcesRequest;
import com.google.android.gms.fitness.request.SensorRegistrationRequest;
import com.google.android.gms.fitness.request.SensorUnregistrationRequest;
import com.google.android.gms.fitness.result.DataSourcesResult;

public class zzpf
  implements SensorsApi
{
  private static abstract interface zza
  {
    public abstract void a();
  }
  
  private static class zzb
    extends zzoi.zza
  {
    private final zza.zzb<DataSourcesResult> a;
    
    private zzb(zza.zzb<DataSourcesResult> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void a(DataSourcesResult paramDataSourcesResult)
    {
      this.a.a(paramDataSourcesResult);
    }
  }
  
  private static class zzc
    extends zzow.zza
  {
    private final zza.zzb<Status> a;
    private final zzpf.zza b;
    
    private zzc(zza.zzb<Status> paramzzb, zzpf.zza paramzza)
    {
      this.a = paramzzb;
      this.b = paramzza;
    }
    
    public void a(Status paramStatus)
    {
      if ((this.b != null) && (paramStatus.d())) {
        this.b.a();
      }
      this.a.a(paramStatus);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzpf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */