package com.google.android.gms.internal;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class zznx
{
  private static final double a = 1000.0D / TimeUnit.SECONDS.toNanos(1L);
  private static final double b = 1000.0D / TimeUnit.SECONDS.toNanos(1L);
  private static final zznx e = new zznx();
  private final Map<String, Map<String, zza>> c;
  private final Map<String, zza> d;
  
  private zznx()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put(zznt.j.a, new zza(-90.0D, 90.0D, null));
    localHashMap.put(zznt.k.a, new zza(-180.0D, 180.0D, null));
    localHashMap.put(zznt.l.a, new zza(0.0D, 10000.0D, null));
    localHashMap.put(zznt.i.a, new zza(0.0D, 1000.0D, null));
    localHashMap.put(zznt.m.a, new zza(-100000.0D, 100000.0D, null));
    localHashMap.put(zznt.t.a, new zza(0.0D, 100.0D, null));
    localHashMap.put(zznt.b.a, new zza(0.0D, 100.0D, null));
    localHashMap.put(zznt.e.a, new zza(0.0D, 9.223372036854776E18D, null));
    localHashMap.put(zznt.q.a, new zza(0.0D, 10.0D, null));
    localHashMap.put(zznt.r.a, new zza(0.0D, 1000.0D, null));
    localHashMap.put(zznt.u.a, new zza(0.0D, 200000.0D, null));
    this.d = Collections.unmodifiableMap(localHashMap);
    localHashMap = new HashMap();
    localHashMap.put("com.google.step_count.delta", a(zznt.d.a, new zza(0.0D, a, null)));
    localHashMap.put("com.google.calories.consumed", a(zznt.x.a, new zza(0.0D, b, null)));
    localHashMap.put("com.google.calories.expended", a(zznt.x.a, new zza(0.0D, b, null)));
    this.c = Collections.unmodifiableMap(localHashMap);
  }
  
  private static <K, V> Map<K, V> a(K paramK, V paramV)
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put(paramK, paramV);
    return localHashMap;
  }
  
  public static class zza
  {
    private final double a;
    private final double b;
    
    private zza(double paramDouble1, double paramDouble2)
    {
      this.a = paramDouble1;
      this.b = paramDouble2;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zznx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */