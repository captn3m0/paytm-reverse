package com.google.android.gms.internal;

import android.util.Log;
import com.google.android.gms.ads.internal.util.client.zzb;

@zzhb
public final class zzin
  extends zzb
{
  public static boolean a()
  {
    return ((Boolean)zzbt.ak.c()).booleanValue();
  }
  
  private static boolean b()
  {
    return (a(2)) && (a());
  }
  
  public static void e(String paramString)
  {
    if (b()) {
      Log.v("Ads", paramString);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzin.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */