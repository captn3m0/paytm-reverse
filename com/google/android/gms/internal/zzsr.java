package com.google.android.gms.internal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class zzsr
  implements Cloneable
{
  private zzsp<?, ?> a;
  private Object b;
  private List<zzsw> c = new ArrayList();
  
  private byte[] c()
    throws IOException
  {
    byte[] arrayOfByte = new byte[a()];
    a(zzsn.a(arrayOfByte));
    return arrayOfByte;
  }
  
  int a()
  {
    int j;
    if (this.b != null)
    {
      j = this.a.a(this.b);
      return j;
    }
    Iterator localIterator = this.c.iterator();
    for (int i = 0;; i = ((zzsw)localIterator.next()).a() + i)
    {
      j = i;
      if (!localIterator.hasNext()) {
        break;
      }
    }
  }
  
  <T> T a(zzsp<?, T> paramzzsp)
  {
    if (this.b != null)
    {
      if (this.a != paramzzsp) {
        throw new IllegalStateException("Tried to getExtension with a differernt Extension.");
      }
    }
    else
    {
      this.a = paramzzsp;
      this.b = paramzzsp.a(this.c);
      this.c = null;
    }
    return (T)this.b;
  }
  
  void a(zzsn paramzzsn)
    throws IOException
  {
    if (this.b != null) {
      this.a.a(this.b, paramzzsn);
    }
    for (;;)
    {
      return;
      Iterator localIterator = this.c.iterator();
      while (localIterator.hasNext()) {
        ((zzsw)localIterator.next()).a(paramzzsn);
      }
    }
  }
  
  void a(zzsw paramzzsw)
  {
    this.c.add(paramzzsw);
  }
  
  public final zzsr b()
  {
    int i = 0;
    zzsr localzzsr = new zzsr();
    try
    {
      localzzsr.a = this.a;
      if (this.c == null) {
        localzzsr.c = null;
      }
      while (this.b == null)
      {
        return localzzsr;
        localzzsr.c.addAll(this.c);
      }
      if (!(this.b instanceof zzsu)) {
        break label92;
      }
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      throw new AssertionError(localCloneNotSupportedException);
    }
    localCloneNotSupportedException.b = ((zzsu)this.b).e();
    return localCloneNotSupportedException;
    label92:
    if ((this.b instanceof byte[]))
    {
      localCloneNotSupportedException.b = ((byte[])this.b).clone();
      return localCloneNotSupportedException;
    }
    Object localObject1;
    Object localObject2;
    if ((this.b instanceof byte[][]))
    {
      localObject1 = (byte[][])this.b;
      localObject2 = new byte[localObject1.length][];
      localCloneNotSupportedException.b = localObject2;
      i = 0;
      while (i < localObject1.length)
      {
        localObject2[i] = ((byte[])localObject1[i].clone());
        i += 1;
      }
    }
    if ((this.b instanceof boolean[]))
    {
      localCloneNotSupportedException.b = ((boolean[])this.b).clone();
      return localCloneNotSupportedException;
    }
    if ((this.b instanceof int[]))
    {
      localCloneNotSupportedException.b = ((int[])this.b).clone();
      return localCloneNotSupportedException;
    }
    if ((this.b instanceof long[]))
    {
      localCloneNotSupportedException.b = ((long[])this.b).clone();
      return localCloneNotSupportedException;
    }
    if ((this.b instanceof float[]))
    {
      localCloneNotSupportedException.b = ((float[])this.b).clone();
      return localCloneNotSupportedException;
    }
    if ((this.b instanceof double[]))
    {
      localCloneNotSupportedException.b = ((double[])this.b).clone();
      return localCloneNotSupportedException;
    }
    if ((this.b instanceof zzsu[]))
    {
      localObject1 = (zzsu[])this.b;
      localObject2 = new zzsu[localObject1.length];
      localCloneNotSupportedException.b = localObject2;
      while (i < localObject1.length)
      {
        localObject2[i] = localObject1[i].e();
        i += 1;
      }
    }
    return localCloneNotSupportedException;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1;
    if (paramObject == this) {
      bool1 = true;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (!(paramObject instanceof zzsr));
      paramObject = (zzsr)paramObject;
      if ((this.b == null) || (((zzsr)paramObject).b == null)) {
        break;
      }
      bool1 = bool2;
    } while (this.a != ((zzsr)paramObject).a);
    if (!this.a.b.isArray()) {
      return this.b.equals(((zzsr)paramObject).b);
    }
    if ((this.b instanceof byte[])) {
      return Arrays.equals((byte[])this.b, (byte[])((zzsr)paramObject).b);
    }
    if ((this.b instanceof int[])) {
      return Arrays.equals((int[])this.b, (int[])((zzsr)paramObject).b);
    }
    if ((this.b instanceof long[])) {
      return Arrays.equals((long[])this.b, (long[])((zzsr)paramObject).b);
    }
    if ((this.b instanceof float[])) {
      return Arrays.equals((float[])this.b, (float[])((zzsr)paramObject).b);
    }
    if ((this.b instanceof double[])) {
      return Arrays.equals((double[])this.b, (double[])((zzsr)paramObject).b);
    }
    if ((this.b instanceof boolean[])) {
      return Arrays.equals((boolean[])this.b, (boolean[])((zzsr)paramObject).b);
    }
    return Arrays.deepEquals((Object[])this.b, (Object[])((zzsr)paramObject).b);
    if ((this.c != null) && (((zzsr)paramObject).c != null)) {
      return this.c.equals(((zzsr)paramObject).c);
    }
    try
    {
      bool1 = Arrays.equals(c(), ((zzsr)paramObject).c());
      return bool1;
    }
    catch (IOException paramObject)
    {
      throw new IllegalStateException((Throwable)paramObject);
    }
  }
  
  public int hashCode()
  {
    try
    {
      int i = Arrays.hashCode(c());
      return i + 527;
    }
    catch (IOException localIOException)
    {
      throw new IllegalStateException(localIOException);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzsr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */