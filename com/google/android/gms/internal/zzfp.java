package com.google.android.gms.internal;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.webkit.URLUtil;
import com.google.android.gms.R.string;
import com.google.android.gms.ads.internal.zzr;
import java.util.Map;

@zzhb
public class zzfp
  extends zzfs
{
  private final Map<String, String> a;
  private final Context b;
  
  public zzfp(zzjp paramzzjp, Map<String, String> paramMap)
  {
    super(paramzzjp, "storePicture");
    this.a = paramMap;
    this.b = paramzzjp.f();
  }
  
  DownloadManager.Request a(String paramString1, String paramString2)
  {
    paramString1 = new DownloadManager.Request(Uri.parse(paramString1));
    paramString1.setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES, paramString2);
    zzr.g().a(paramString1);
    return paramString1;
  }
  
  String a(String paramString)
  {
    return Uri.parse(paramString).getLastPathSegment();
  }
  
  public void a()
  {
    if (this.b == null)
    {
      b("Activity context is not available");
      return;
    }
    if (!zzr.e().e(this.b).c())
    {
      b("Feature is not supported by the device.");
      return;
    }
    final String str1 = (String)this.a.get("iurl");
    if (TextUtils.isEmpty(str1))
    {
      b("Image url cannot be empty.");
      return;
    }
    if (!URLUtil.isValidUrl(str1))
    {
      b("Invalid image url: " + str1);
      return;
    }
    final String str2 = a(str1);
    if (!zzr.e().c(str2))
    {
      b("Image type not recognized: " + str2);
      return;
    }
    AlertDialog.Builder localBuilder = zzr.e().d(this.b);
    localBuilder.setTitle(zzr.h().a(R.string.store_picture_title, "Save image"));
    localBuilder.setMessage(zzr.h().a(R.string.store_picture_message, "Allow Ad to store image in Picture gallery?"));
    localBuilder.setPositiveButton(zzr.h().a(R.string.accept, "Accept"), new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        paramAnonymousDialogInterface = (DownloadManager)zzfp.a(zzfp.this).getSystemService("download");
        try
        {
          paramAnonymousDialogInterface.enqueue(zzfp.this.a(str1, str2));
          return;
        }
        catch (IllegalStateException paramAnonymousDialogInterface)
        {
          zzfp.this.b("Could not store picture.");
        }
      }
    });
    localBuilder.setNegativeButton(zzr.h().a(R.string.decline, "Decline"), new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        zzfp.this.b("User canceled the download.");
      }
    });
    localBuilder.create().show();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzfp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */