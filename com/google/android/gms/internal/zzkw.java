package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.Looper;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;

public class zzkw
  extends zzj<zzkx>
{
  public zzkw(Context paramContext, Looper paramLooper, zzf paramzzf, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramContext, paramLooper, 92, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
  }
  
  protected zzkx a(IBinder paramIBinder)
  {
    return zzkx.zza.a(paramIBinder);
  }
  
  protected String a()
  {
    return "com.google.android.gms.auth.api.consent.START";
  }
  
  protected String b()
  {
    return "com.google.android.gms.auth.api.consent.internal.IConsentService";
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzkw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */