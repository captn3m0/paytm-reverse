package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;

public class zzkm
  extends zzj<zzkp>
{
  private final String a;
  
  public zzkm(Context paramContext, Looper paramLooper, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener, zzf paramzzf)
  {
    super(paramContext, paramLooper, 77, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    this.a = paramzzf.h();
  }
  
  private Bundle h()
  {
    Bundle localBundle = new Bundle();
    localBundle.putString("authPackage", this.a);
    return localBundle;
  }
  
  protected zzkp a(IBinder paramIBinder)
  {
    return zzkp.zza.a(paramIBinder);
  }
  
  protected String a()
  {
    return "com.google.android.gms.appinvite.service.START";
  }
  
  public void a(zzko paramzzko)
  {
    try
    {
      ((zzkp)v()).a(paramzzko);
      return;
    }
    catch (RemoteException paramzzko) {}
  }
  
  public void a(zzko paramzzko, String paramString)
  {
    try
    {
      ((zzkp)v()).b(paramzzko, paramString);
      return;
    }
    catch (RemoteException paramzzko) {}
  }
  
  protected Bundle a_()
  {
    return h();
  }
  
  protected String b()
  {
    return "com.google.android.gms.appinvite.internal.IAppInviteService";
  }
  
  public void b(zzko paramzzko, String paramString)
  {
    try
    {
      ((zzkp)v()).a(paramzzko, paramString);
      return;
    }
    catch (RemoteException paramzzko) {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzkm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */