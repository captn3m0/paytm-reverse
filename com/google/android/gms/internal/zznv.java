package com.google.android.gms.internal;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources.NotFoundException;
import android.telephony.TelephonyManager;
import android.util.Log;

public class zznv
{
  private static int a = -1;
  
  public static boolean a(Context paramContext)
  {
    return b(paramContext) == 3;
  }
  
  public static int b(Context paramContext)
  {
    if (a == -1) {
      switch (c(paramContext))
      {
      case 9: 
      case 11: 
      case 12: 
      default: 
        if (!e(paramContext)) {
          break;
        }
      }
    }
    for (int i = 1;; i = 2)
    {
      a = i;
      for (;;)
      {
        return a;
        a = 3;
        continue;
        a = 0;
      }
    }
  }
  
  private static int c(Context paramContext)
  {
    return d(paramContext) % 1000 / 100 + 5;
  }
  
  private static int d(Context paramContext)
  {
    try
    {
      int i = paramContext.getPackageManager().getPackageInfo("com.google.android.gms", 0).versionCode;
      return i;
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      Log.w("Fitness", "Could not find package info for Google Play Services");
    }
    return -1;
  }
  
  private static boolean e(Context paramContext)
  {
    try
    {
      int i = ((TelephonyManager)paramContext.getSystemService("phone")).getPhoneType();
      return i != 0;
    }
    catch (Resources.NotFoundException localNotFoundException)
    {
      Log.wtf("Fitness", "Unable to determine type of device, assuming phone.  Version: " + d(paramContext), localNotFoundException);
    }
    return true;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zznv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */