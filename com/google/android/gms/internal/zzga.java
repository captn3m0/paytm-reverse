package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import org.json.JSONObject;

@zzhb
public class zzga
  extends Handler
{
  private final zzfz a;
  
  public zzga(Context paramContext)
  {
    this(new zzgb(localContext));
  }
  
  public zzga(zzfz paramzzfz)
  {
    this.a = paramzzfz;
  }
  
  private void a(JSONObject paramJSONObject)
  {
    try
    {
      this.a.a(paramJSONObject.getString("request_id"), paramJSONObject.getString("base_url"), paramJSONObject.getString("html"));
      return;
    }
    catch (Exception paramJSONObject) {}
  }
  
  public void handleMessage(Message paramMessage)
  {
    try
    {
      paramMessage = paramMessage.getData();
      if (paramMessage == null) {
        return;
      }
      paramMessage = new JSONObject(paramMessage.getString("data"));
      if ("fetch_html".equals(paramMessage.getString("message_name")))
      {
        a(paramMessage);
        return;
      }
    }
    catch (Exception paramMessage) {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzga.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */