package com.google.android.gms.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.People.LoadPeopleResult;
import com.google.android.gms.plus.Plus.zza;
import com.google.android.gms.plus.internal.zze;

public final class zzqz
  implements People
{
  private static abstract class zza
    extends Plus.zza<People.LoadPeopleResult>
  {
    public People.LoadPeopleResult a(final Status paramStatus)
    {
      new People.LoadPeopleResult()
      {
        public Status getStatus()
        {
          return paramStatus;
        }
        
        public void release() {}
      };
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzqz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */