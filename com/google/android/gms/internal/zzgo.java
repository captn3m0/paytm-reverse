package com.google.android.gms.internal;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View.MeasureSpec;
import android.webkit.WebView;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.zzr;

@zzhb
public class zzgo
  implements Runnable
{
  protected final zzjp a;
  protected boolean b;
  protected boolean c;
  private final Handler d;
  private final long e;
  private long f;
  private zzjq.zza g;
  private final int h;
  private final int i;
  
  public zzgo(zzjq.zza paramzza, zzjp paramzzjp, int paramInt1, int paramInt2)
  {
    this(paramzza, paramzzjp, paramInt1, paramInt2, 200L, 50L);
  }
  
  public zzgo(zzjq.zza paramzza, zzjp paramzzjp, int paramInt1, int paramInt2, long paramLong1, long paramLong2)
  {
    this.e = paramLong1;
    this.f = paramLong2;
    this.d = new Handler(Looper.getMainLooper());
    this.a = paramzzjp;
    this.g = paramzza;
    this.b = false;
    this.c = false;
    this.h = paramInt2;
    this.i = paramInt1;
  }
  
  public void a()
  {
    this.d.postDelayed(this, this.e);
  }
  
  public void a(AdResponseParcel paramAdResponseParcel)
  {
    a(paramAdResponseParcel, new zzjy(this, this.a, paramAdResponseParcel.q));
  }
  
  public void a(AdResponseParcel paramAdResponseParcel, zzjy paramzzjy)
  {
    this.a.setWebViewClient(paramzzjy);
    zzjp localzzjp = this.a;
    if (TextUtils.isEmpty(paramAdResponseParcel.b)) {}
    for (paramzzjy = null;; paramzzjy = zzr.e().a(paramAdResponseParcel.b))
    {
      localzzjp.loadDataWithBaseURL(paramzzjy, paramAdResponseParcel.c, "text/html", "UTF-8", null);
      return;
    }
  }
  
  public void b()
  {
    try
    {
      this.b = true;
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public boolean c()
  {
    try
    {
      boolean bool = this.b;
      return bool;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public boolean d()
  {
    return this.c;
  }
  
  public void run()
  {
    if ((this.a == null) || (c()))
    {
      this.g.a(this.a, true);
      return;
    }
    new zza(this.a.a()).execute(new Void[0]);
  }
  
  protected final class zza
    extends AsyncTask<Void, Void, Boolean>
  {
    private final WebView b;
    private Bitmap c;
    
    public zza(WebView paramWebView)
    {
      this.b = paramWebView;
    }
    
    protected Boolean a(Void... paramVarArgs)
    {
      for (;;)
      {
        int i;
        int m;
        try
        {
          int n = this.c.getWidth();
          int i1 = this.c.getHeight();
          if ((n == 0) || (i1 == 0))
          {
            paramVarArgs = Boolean.valueOf(false);
            return paramVarArgs;
          }
          i = 0;
          j = 0;
          int k;
          if (i < n)
          {
            k = 0;
            if (k >= i1) {
              break label139;
            }
            m = j;
            if (this.c.getPixel(i, k) != 0) {
              m = j + 1;
            }
          }
          else
          {
            if (j / (n * i1 / 100.0D) > 0.1D)
            {
              bool = true;
              paramVarArgs = Boolean.valueOf(bool);
              continue;
            }
            boolean bool = false;
            continue;
          }
          k += 10;
        }
        finally {}
        int j = m;
        continue;
        label139:
        i += 10;
      }
    }
    
    protected void a(Boolean paramBoolean)
    {
      zzgo.c(zzgo.this);
      if ((paramBoolean.booleanValue()) || (zzgo.this.c()) || (zzgo.d(zzgo.this) <= 0L))
      {
        zzgo.this.c = paramBoolean.booleanValue();
        zzgo.e(zzgo.this).a(zzgo.this.a, true);
      }
      while (zzgo.d(zzgo.this) <= 0L) {
        return;
      }
      if (zzin.a(2)) {
        zzin.a("Ad not detected, scheduling another run.");
      }
      zzgo.g(zzgo.this).postDelayed(zzgo.this, zzgo.f(zzgo.this));
    }
    
    protected void onPreExecute()
    {
      try
      {
        this.c = Bitmap.createBitmap(zzgo.a(zzgo.this), zzgo.b(zzgo.this), Bitmap.Config.ARGB_8888);
        this.b.setVisibility(0);
        this.b.measure(View.MeasureSpec.makeMeasureSpec(zzgo.a(zzgo.this), 0), View.MeasureSpec.makeMeasureSpec(zzgo.b(zzgo.this), 0));
        this.b.layout(0, 0, zzgo.a(zzgo.this), zzgo.b(zzgo.this));
        Canvas localCanvas = new Canvas(this.c);
        this.b.draw(localCanvas);
        this.b.invalidate();
        return;
      }
      finally
      {
        localObject = finally;
        throw ((Throwable)localObject);
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzgo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */