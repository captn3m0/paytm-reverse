package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.safetynet.AttestationData;
import com.google.android.gms.safetynet.SafeBrowsingData;
import com.google.android.gms.safetynet.SafetyNetApi;
import com.google.android.gms.safetynet.SafetyNetApi.AttestationResult;
import com.google.android.gms.safetynet.SafetyNetApi.SafeBrowsingResult;

public class zzre
  implements SafetyNetApi
{
  static class zza
    implements SafetyNetApi.AttestationResult
  {
    private final Status a;
    private final AttestationData b;
    
    public zza(Status paramStatus, AttestationData paramAttestationData)
    {
      this.a = paramStatus;
      this.b = paramAttestationData;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
  
  static abstract class zzb
    extends zzrb<SafetyNetApi.AttestationResult>
  {
    protected zzrc b;
    
    protected SafetyNetApi.AttestationResult a(Status paramStatus)
    {
      return new zzre.zza(paramStatus, null);
    }
  }
  
  static abstract class zzc
    extends zzrb<SafetyNetApi.SafeBrowsingResult>
  {
    protected zzrc c;
    
    protected SafetyNetApi.SafeBrowsingResult a(Status paramStatus)
    {
      return new zzre.zzd(paramStatus, null);
    }
  }
  
  static class zzd
    implements SafetyNetApi.SafeBrowsingResult
  {
    private Status a;
    private final SafeBrowsingData b;
    private String c;
    
    public zzd(Status paramStatus, SafeBrowsingData paramSafeBrowsingData)
    {
      this.a = paramStatus;
      this.b = paramSafeBrowsingData;
      this.c = null;
      if (this.b != null) {
        this.c = this.b.a();
      }
      while (!this.a.d()) {
        return;
      }
      this.a = new Status(8);
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzre.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */