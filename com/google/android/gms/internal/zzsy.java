package com.google.android.gms.internal;

import java.io.IOException;

public abstract interface zzsy
{
  public static final class zza
    extends zzsu
  {
    public String a;
    public zzsy.zzb[] b;
    
    public zza()
    {
      a();
    }
    
    public zza a()
    {
      this.a = null;
      this.b = zzsy.zzb.a();
      this.S = -1;
      return this;
    }
    
    public zza a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (zzsx.a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 10: 
          this.a = paramzzsm.i();
          break;
        case 18: 
          int j = zzsx.b(paramzzsm, 18);
          if (this.b == null) {}
          zzsy.zzb[] arrayOfzzb;
          for (i = 0;; i = this.b.length)
          {
            arrayOfzzb = new zzsy.zzb[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.b, 0, arrayOfzzb, 0, i);
              j = i;
            }
            while (j < arrayOfzzb.length - 1)
            {
              arrayOfzzb[j] = new zzsy.zzb();
              paramzzsm.a(arrayOfzzb[j]);
              paramzzsm.a();
              j += 1;
            }
          }
          arrayOfzzb[j] = new zzsy.zzb();
          paramzzsm.a(arrayOfzzb[j]);
          this.b = arrayOfzzb;
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (this.a != null) {
        paramzzsn.a(1, this.a);
      }
      if ((this.b != null) && (this.b.length > 0))
      {
        int i = 0;
        while (i < this.b.length)
        {
          zzsy.zzb localzzb = this.b[i];
          if (localzzb != null) {
            paramzzsn.a(2, localzzb);
          }
          i += 1;
        }
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b();
      int i = j;
      if (this.a != null) {
        i = j + zzsn.b(1, this.a);
      }
      j = i;
      if (this.b != null)
      {
        j = i;
        if (this.b.length > 0)
        {
          j = 0;
          while (j < this.b.length)
          {
            zzsy.zzb localzzb = this.b[j];
            int k = i;
            if (localzzb != null) {
              k = i + zzsn.c(2, localzzb);
            }
            j += 1;
            i = k;
          }
          j = i;
        }
      }
      return j;
    }
    
    public boolean equals(Object paramObject)
    {
      if (paramObject == this) {}
      do
      {
        return true;
        if (!(paramObject instanceof zza)) {
          return false;
        }
        paramObject = (zza)paramObject;
        if (this.a == null)
        {
          if (((zza)paramObject).a != null) {
            return false;
          }
        }
        else if (!this.a.equals(((zza)paramObject).a)) {
          return false;
        }
      } while (zzss.a(this.b, ((zza)paramObject).b));
      return false;
    }
    
    public int hashCode()
    {
      int j = getClass().getName().hashCode();
      if (this.a == null) {}
      for (int i = 0;; i = this.a.hashCode()) {
        return (i + (j + 527) * 31) * 31 + zzss.a(this.b);
      }
    }
  }
  
  public static final class zzb
    extends zzsu
  {
    private static volatile zzb[] d;
    public String a;
    public Integer b;
    public Boolean c;
    
    public zzb()
    {
      c();
    }
    
    public static zzb[] a()
    {
      if (d == null) {}
      synchronized (zzss.a)
      {
        if (d == null) {
          d = new zzb[0];
        }
        return d;
      }
    }
    
    public zzb a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (zzsx.a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 10: 
          this.a = paramzzsm.i();
          break;
        case 24: 
          i = paramzzsm.g();
          switch (i)
          {
          default: 
            break;
          case 1: 
          case 2: 
          case 3: 
          case 4: 
          case 5: 
          case 6: 
          case 7: 
            this.b = Integer.valueOf(i);
          }
          break;
        case 32: 
          this.c = Boolean.valueOf(paramzzsm.h());
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (this.a != null) {
        paramzzsn.a(1, this.a);
      }
      if (this.b != null) {
        paramzzsn.a(3, this.b.intValue());
      }
      if (this.c != null) {
        paramzzsn.a(4, this.c.booleanValue());
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b();
      int i = j;
      if (this.a != null) {
        i = j + zzsn.b(1, this.a);
      }
      j = i;
      if (this.b != null) {
        j = i + zzsn.c(3, this.b.intValue());
      }
      i = j;
      if (this.c != null) {
        i = j + zzsn.b(4, this.c.booleanValue());
      }
      return i;
    }
    
    public zzb c()
    {
      this.a = null;
      this.b = null;
      this.c = null;
      this.S = -1;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      if (paramObject == this) {}
      do
      {
        do
        {
          return true;
          if (!(paramObject instanceof zzb)) {
            return false;
          }
          paramObject = (zzb)paramObject;
          if (this.a == null)
          {
            if (((zzb)paramObject).a != null) {
              return false;
            }
          }
          else if (!this.a.equals(((zzb)paramObject).a)) {
            return false;
          }
          if (this.b == null)
          {
            if (((zzb)paramObject).b != null) {
              return false;
            }
          }
          else if (!this.b.equals(((zzb)paramObject).b)) {
            return false;
          }
          if (this.c != null) {
            break;
          }
        } while (((zzb)paramObject).c == null);
        return false;
      } while (this.c.equals(((zzb)paramObject).c));
      return false;
    }
    
    public int hashCode()
    {
      int k = 0;
      int m = getClass().getName().hashCode();
      int i;
      int j;
      if (this.a == null)
      {
        i = 0;
        if (this.b != null) {
          break label72;
        }
        j = 0;
        label32:
        if (this.c != null) {
          break label83;
        }
      }
      for (;;)
      {
        return (j + (i + (m + 527) * 31) * 31) * 31 + k;
        i = this.a.hashCode();
        break;
        label72:
        j = this.b.intValue();
        break label32;
        label83:
        k = this.c.hashCode();
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzsy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */