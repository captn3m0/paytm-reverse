package com.google.android.gms.internal;

import java.io.IOException;

public final class zzsx
{
  public static final int[] a = new int[0];
  public static final long[] b = new long[0];
  public static final float[] c = new float[0];
  public static final double[] d = new double[0];
  public static final boolean[] e = new boolean[0];
  public static final String[] f = new String[0];
  public static final byte[][] g = new byte[0][];
  public static final byte[] h = new byte[0];
  
  static int a(int paramInt)
  {
    return paramInt & 0x7;
  }
  
  static int a(int paramInt1, int paramInt2)
  {
    return paramInt1 << 3 | paramInt2;
  }
  
  public static boolean a(zzsm paramzzsm, int paramInt)
    throws IOException
  {
    return paramzzsm.b(paramInt);
  }
  
  public static int b(int paramInt)
  {
    return paramInt >>> 3;
  }
  
  public static final int b(zzsm paramzzsm, int paramInt)
    throws IOException
  {
    int i = 1;
    int j = paramzzsm.s();
    paramzzsm.b(paramInt);
    while (paramzzsm.a() == paramInt)
    {
      paramzzsm.b(paramInt);
      i += 1;
    }
    paramzzsm.f(j);
    return i;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzsx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */