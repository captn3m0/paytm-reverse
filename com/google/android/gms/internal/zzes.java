package com.google.android.gms.internal;

@zzhb
public final class zzes
{
  public final int a;
  public final zzen b;
  public final zzey c;
  public final String d;
  public final zzeq e;
  public final zzfa f;
  
  public zzes(int paramInt)
  {
    this(null, null, null, null, paramInt, null);
  }
  
  public zzes(zzen paramzzen, zzey paramzzey, String paramString, zzeq paramzzeq, int paramInt, zzfa paramzzfa)
  {
    this.b = paramzzen;
    this.c = paramzzey;
    this.d = paramString;
    this.e = paramzzeq;
    this.a = paramInt;
    this.f = paramzzfa;
  }
  
  public static abstract interface zza
  {
    public abstract void a(int paramInt);
    
    public abstract void a(int paramInt, zzfa paramzzfa);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzes.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */