package com.google.android.gms.internal;

import org.json.JSONException;
import org.json.JSONObject;

@zzhb
public class zzfq
{
  private final boolean a;
  private final boolean b;
  private final boolean c;
  private final boolean d;
  private final boolean e;
  
  private zzfq(zza paramzza)
  {
    this.a = zza.a(paramzza);
    this.b = zza.b(paramzza);
    this.c = zza.c(paramzza);
    this.d = zza.d(paramzza);
    this.e = zza.e(paramzza);
  }
  
  public JSONObject a()
  {
    try
    {
      JSONObject localJSONObject = new JSONObject().put("sms", this.a).put("tel", this.b).put("calendar", this.c).put("storePicture", this.d).put("inlineVideo", this.e);
      return localJSONObject;
    }
    catch (JSONException localJSONException)
    {
      zzin.b("Error occured while obtaining the MRAID capabilities.", localJSONException);
    }
    return null;
  }
  
  public static final class zza
  {
    private boolean a;
    private boolean b;
    private boolean c;
    private boolean d;
    private boolean e;
    
    public zza a(boolean paramBoolean)
    {
      this.a = paramBoolean;
      return this;
    }
    
    public zzfq a()
    {
      return new zzfq(this, null);
    }
    
    public zza b(boolean paramBoolean)
    {
      this.b = paramBoolean;
      return this;
    }
    
    public zza c(boolean paramBoolean)
    {
      this.c = paramBoolean;
      return this;
    }
    
    public zza d(boolean paramBoolean)
    {
      this.d = paramBoolean;
      return this;
    }
    
    public zza e(boolean paramBoolean)
    {
      this.e = paramBoolean;
      return this;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzfq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */