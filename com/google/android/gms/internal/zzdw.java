package com.google.android.gms.internal;

import android.os.Handler;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.client.zzp;
import com.google.android.gms.ads.internal.client.zzp.zza;
import com.google.android.gms.ads.internal.client.zzq;
import com.google.android.gms.ads.internal.client.zzq.zza;
import com.google.android.gms.ads.internal.client.zzw;
import com.google.android.gms.ads.internal.client.zzw.zza;
import com.google.android.gms.ads.internal.reward.client.zza;
import com.google.android.gms.ads.internal.reward.client.zzd;
import com.google.android.gms.ads.internal.reward.client.zzd.zza;
import com.google.android.gms.ads.internal.zzk;
import com.google.android.gms.ads.internal.zzr;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@zzhb
class zzdw
{
  private final List<zza> a = new LinkedList();
  
  void a(zzk paramzzk)
  {
    paramzzk.a(new zzq.zza()
    {
      public void a()
        throws RemoteException
      {
        zzdw.a(zzdw.this).add(new zzdw.zza()
        {
          public void a(zzdx paramAnonymous2zzdx)
            throws RemoteException
          {
            if (paramAnonymous2zzdx.a != null) {
              paramAnonymous2zzdx.a.a();
            }
            zzr.p().a();
          }
        });
      }
      
      public void a(final int paramAnonymousInt)
        throws RemoteException
      {
        zzdw.a(zzdw.this).add(new zzdw.zza()
        {
          public void a(zzdx paramAnonymous2zzdx)
            throws RemoteException
          {
            if (paramAnonymous2zzdx.a != null) {
              paramAnonymous2zzdx.a.a(paramAnonymousInt);
            }
          }
        });
        zzin.e("Pooled interstitial failed to load.");
      }
      
      public void b()
        throws RemoteException
      {
        zzdw.a(zzdw.this).add(new zzdw.zza()
        {
          public void a(zzdx paramAnonymous2zzdx)
            throws RemoteException
          {
            if (paramAnonymous2zzdx.a != null) {
              paramAnonymous2zzdx.a.b();
            }
          }
        });
      }
      
      public void c()
        throws RemoteException
      {
        zzdw.a(zzdw.this).add(new zzdw.zza()
        {
          public void a(zzdx paramAnonymous2zzdx)
            throws RemoteException
          {
            if (paramAnonymous2zzdx.a != null) {
              paramAnonymous2zzdx.a.c();
            }
          }
        });
        zzin.e("Pooled interstitial loaded.");
      }
      
      public void d()
        throws RemoteException
      {
        zzdw.a(zzdw.this).add(new zzdw.zza()
        {
          public void a(zzdx paramAnonymous2zzdx)
            throws RemoteException
          {
            if (paramAnonymous2zzdx.a != null) {
              paramAnonymous2zzdx.a.d();
            }
          }
        });
      }
    });
    paramzzk.a(new zzw.zza()
    {
      public void a(final String paramAnonymousString1, final String paramAnonymousString2)
        throws RemoteException
      {
        zzdw.a(zzdw.this).add(new zzdw.zza()
        {
          public void a(zzdx paramAnonymous2zzdx)
            throws RemoteException
          {
            if (paramAnonymous2zzdx.b != null) {
              paramAnonymous2zzdx.b.a(paramAnonymousString1, paramAnonymousString2);
            }
          }
        });
      }
    });
    paramzzk.a(new zzgd.zza()
    {
      public void a(final zzgc paramAnonymouszzgc)
        throws RemoteException
      {
        zzdw.a(zzdw.this).add(new zzdw.zza()
        {
          public void a(zzdx paramAnonymous2zzdx)
            throws RemoteException
          {
            if (paramAnonymous2zzdx.c != null) {
              paramAnonymous2zzdx.c.a(paramAnonymouszzgc);
            }
          }
        });
      }
    });
    paramzzk.a(new zzcf.zza()
    {
      public void a(final zzce paramAnonymouszzce)
        throws RemoteException
      {
        zzdw.a(zzdw.this).add(new zzdw.zza()
        {
          public void a(zzdx paramAnonymous2zzdx)
            throws RemoteException
          {
            if (paramAnonymous2zzdx.d != null) {
              paramAnonymous2zzdx.d.a(paramAnonymouszzce);
            }
          }
        });
      }
    });
    paramzzk.a(new zzp.zza()
    {
      public void a()
        throws RemoteException
      {
        zzdw.a(zzdw.this).add(new zzdw.zza()
        {
          public void a(zzdx paramAnonymous2zzdx)
            throws RemoteException
          {
            if (paramAnonymous2zzdx.e != null) {
              paramAnonymous2zzdx.e.a();
            }
          }
        });
      }
    });
    paramzzk.a(new zzd.zza()
    {
      public void a()
        throws RemoteException
      {
        zzdw.a(zzdw.this).add(new zzdw.zza()
        {
          public void a(zzdx paramAnonymous2zzdx)
            throws RemoteException
          {
            if (paramAnonymous2zzdx.f != null) {
              paramAnonymous2zzdx.f.a();
            }
          }
        });
      }
      
      public void a(final int paramAnonymousInt)
        throws RemoteException
      {
        zzdw.a(zzdw.this).add(new zzdw.zza()
        {
          public void a(zzdx paramAnonymous2zzdx)
            throws RemoteException
          {
            if (paramAnonymous2zzdx.f != null) {
              paramAnonymous2zzdx.f.a(paramAnonymousInt);
            }
          }
        });
      }
      
      public void a(final zza paramAnonymouszza)
        throws RemoteException
      {
        zzdw.a(zzdw.this).add(new zzdw.zza()
        {
          public void a(zzdx paramAnonymous2zzdx)
            throws RemoteException
          {
            if (paramAnonymous2zzdx.f != null) {
              paramAnonymous2zzdx.f.a(paramAnonymouszza);
            }
          }
        });
      }
      
      public void b()
        throws RemoteException
      {
        zzdw.a(zzdw.this).add(new zzdw.zza()
        {
          public void a(zzdx paramAnonymous2zzdx)
            throws RemoteException
          {
            if (paramAnonymous2zzdx.f != null) {
              paramAnonymous2zzdx.f.b();
            }
          }
        });
      }
      
      public void c()
        throws RemoteException
      {
        zzdw.a(zzdw.this).add(new zzdw.zza()
        {
          public void a(zzdx paramAnonymous2zzdx)
            throws RemoteException
          {
            if (paramAnonymous2zzdx.f != null) {
              paramAnonymous2zzdx.f.c();
            }
          }
        });
      }
      
      public void d()
        throws RemoteException
      {
        zzdw.a(zzdw.this).add(new zzdw.zza()
        {
          public void a(zzdx paramAnonymous2zzdx)
            throws RemoteException
          {
            if (paramAnonymous2zzdx.f != null) {
              paramAnonymous2zzdx.f.d();
            }
          }
        });
      }
      
      public void e()
        throws RemoteException
      {
        zzdw.a(zzdw.this).add(new zzdw.zza()
        {
          public void a(zzdx paramAnonymous2zzdx)
            throws RemoteException
          {
            if (paramAnonymous2zzdx.f != null) {
              paramAnonymous2zzdx.f.e();
            }
          }
        });
      }
    });
  }
  
  void a(final zzdx paramzzdx)
  {
    Handler localHandler = zzir.a;
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext()) {
      localHandler.post(new Runnable()
      {
        public void run()
        {
          try
          {
            this.a.a(paramzzdx);
            return;
          }
          catch (RemoteException localRemoteException)
          {
            zzin.d("Could not propagate interstitial ad event.", localRemoteException);
          }
        }
      });
    }
  }
  
  static abstract interface zza
  {
    public abstract void a(zzdx paramzzdx)
      throws RemoteException;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzdw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */