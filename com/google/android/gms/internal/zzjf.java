package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

@zzhb
public class zzjf
{
  public static <A, B> zzjg<B> a(final zzjg<A> paramzzjg, final zza<A, B> paramzza)
  {
    zzjd localzzjd = new zzjd();
    paramzzjg.a(new Runnable()
    {
      public void run()
      {
        try
        {
          this.a.b(paramzza.a(paramzzjg.get()));
          return;
        }
        catch (ExecutionException localExecutionException)
        {
          this.a.cancel(true);
          return;
        }
        catch (InterruptedException localInterruptedException)
        {
          for (;;) {}
        }
        catch (CancellationException localCancellationException)
        {
          for (;;) {}
        }
      }
    });
    return localzzjd;
  }
  
  public static <V> zzjg<List<V>> a(final List<zzjg<V>> paramList)
  {
    final zzjd localzzjd = new zzjd();
    final int i = paramList.size();
    AtomicInteger localAtomicInteger = new AtomicInteger(0);
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext()) {
      ((zzjg)localIterator.next()).a(new Runnable()
      {
        public void run()
        {
          if (this.a.incrementAndGet() >= i) {}
          try
          {
            localzzjd.b(zzjf.b(paramList));
            return;
          }
          catch (InterruptedException localInterruptedException)
          {
            zzin.d("Unable to convert list of futures to a future of list", localInterruptedException);
            return;
          }
          catch (ExecutionException localExecutionException)
          {
            for (;;) {}
          }
        }
      });
    }
    return localzzjd;
  }
  
  private static <V> List<V> c(List<zzjg<V>> paramList)
    throws ExecutionException, InterruptedException
  {
    ArrayList localArrayList = new ArrayList();
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      Object localObject = ((zzjg)paramList.next()).get();
      if (localObject != null) {
        localArrayList.add(localObject);
      }
    }
    return localArrayList;
  }
  
  public static abstract interface zza<D, R>
  {
    public abstract R a(D paramD);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzjf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */