package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zzb;
import com.google.android.gms.fitness.BleApi;
import com.google.android.gms.fitness.data.BleDevice;
import com.google.android.gms.fitness.request.ClaimBleDeviceRequest;
import com.google.android.gms.fitness.request.ListClaimedBleDevicesRequest;
import com.google.android.gms.fitness.request.StartBleScanRequest;
import com.google.android.gms.fitness.request.StopBleScanRequest;
import com.google.android.gms.fitness.request.UnclaimBleDeviceRequest;
import com.google.android.gms.fitness.result.BleDevicesResult;

public class zzpa
  implements BleApi
{
  private static class zza
    extends zzpj.zza
  {
    private final zza.zzb<BleDevicesResult> a;
    
    private zza(zza.zzb<BleDevicesResult> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void a(BleDevicesResult paramBleDevicesResult)
    {
      this.a.a(paramBleDevicesResult);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzpa.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */