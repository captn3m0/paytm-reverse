package com.google.android.gms.internal;

@zzhb
public class zzia
{
  private final zzey a;
  private final zzhx b;
  
  public zzia(zzey paramzzey, zzhw paramzzhw)
  {
    this.a = paramzzey;
    this.b = new zzhx(paramzzhw);
  }
  
  public zzey a()
  {
    return this.a;
  }
  
  public zzhx b()
  {
    return this.b;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzia.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */