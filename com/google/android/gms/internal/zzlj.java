package com.google.android.gms.internal;

import org.json.JSONException;
import org.json.JSONObject;

public final class zzlj
{
  private final String a;
  private final int b;
  private final String c;
  
  public zzlj(String paramString1, int paramInt, String paramString2)
  {
    this.a = paramString1;
    this.b = paramInt;
    this.c = paramString2;
  }
  
  public zzlj(JSONObject paramJSONObject)
    throws JSONException
  {
    this(paramJSONObject.optString("applicationName"), paramJSONObject.optInt("maxPlayers"), paramJSONObject.optString("version"));
  }
  
  public final String a()
  {
    return this.a;
  }
  
  public final int b()
  {
    return this.b;
  }
  
  public final String c()
  {
    return this.c;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzlj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */