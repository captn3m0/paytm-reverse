package com.google.android.gms.internal;

import com.google.android.gms.ads.formats.NativeAppInstallAd.OnAppInstallAdLoadedListener;

@zzhb
public class zzcw
  extends zzcr.zza
{
  private final NativeAppInstallAd.OnAppInstallAdLoadedListener a;
  
  public zzcw(NativeAppInstallAd.OnAppInstallAdLoadedListener paramOnAppInstallAdLoadedListener)
  {
    this.a = paramOnAppInstallAdLoadedListener;
  }
  
  public void a(zzcl paramzzcl)
  {
    this.a.a(b(paramzzcl));
  }
  
  zzcm b(zzcl paramzzcl)
  {
    return new zzcm(paramzzcl);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzcw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */