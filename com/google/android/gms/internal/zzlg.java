package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Intent;
import com.google.android.gms.auth.api.signin.internal.IdpTokenType;
import com.google.android.gms.auth.api.signin.zzd;
import com.google.android.gms.common.internal.zzx;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class zzlg
  implements zzlf
{
  protected final Activity a;
  protected final Set<String> b;
  private String c;
  private String d;
  private zzlf.zza e;
  
  protected zzlg(Activity paramActivity, List<String> paramList1, List<String> paramList2)
  {
    this.a = ((Activity)zzx.a(paramActivity));
    paramActivity = new HashSet((Collection)zzx.a(paramList1));
    paramActivity.addAll((Collection)zzx.a(paramList2));
    this.b = Collections.unmodifiableSet(paramActivity);
  }
  
  protected Intent a(IdpTokenType paramIdpTokenType, String paramString1, String paramString2)
  {
    zzx.a(paramIdpTokenType);
    zzx.a(paramString1);
    Intent localIntent = new Intent("com.google.android.gms.auth.VERIFY_ASSERTION");
    localIntent.putExtra("idpTokenType", paramIdpTokenType);
    localIntent.putExtra("idpToken", paramString1);
    localIntent.putExtra("pendingToken", paramString2);
    paramIdpTokenType = a();
    if (paramIdpTokenType != null) {
      localIntent.putExtra("idProvider", paramIdpTokenType.a());
    }
    return localIntent;
  }
  
  protected void a(String paramString)
  {
    this.c = paramString;
  }
  
  protected Set<String> b()
  {
    return this.b;
  }
  
  protected void b(zzlf.zza paramzza)
  {
    this.e = ((zzlf.zza)zzx.a(paramzza));
  }
  
  protected void b(String paramString)
  {
    this.d = paramString;
  }
  
  protected void b(String paramString1, String paramString2, zzlf.zza paramzza)
  {
    a(paramString1);
    b(paramString2);
    b(paramzza);
  }
  
  protected zzlf.zza c()
  {
    return this.e;
  }
  
  protected String d()
  {
    return this.d;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzlg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */