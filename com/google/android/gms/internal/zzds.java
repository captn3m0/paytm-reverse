package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzd;
import com.google.android.gms.ads.internal.zzr;
import com.google.android.gms.common.internal.zzb;
import java.util.Map;

@zzhb
public class zzds
  implements zzdf
{
  public void a(zzjp paramzzjp, Map<String, String> paramMap)
  {
    zzdq localzzdq = zzr.t();
    if (paramMap.containsKey("abort"))
    {
      if (!localzzdq.a(paramzzjp)) {
        zzin.d("Precache abort but no preload task running.");
      }
      return;
    }
    String str = (String)paramMap.get("src");
    if (str == null)
    {
      zzin.d("Precache video action is missing the src parameter.");
      return;
    }
    try
    {
      i = Integer.parseInt((String)paramMap.get("player"));
      if (paramMap.containsKey("mimetype"))
      {
        paramMap = (String)paramMap.get("mimetype");
        if (!localzzdq.b(paramzzjp)) {
          break label121;
        }
        zzin.d("Precache task already running.");
      }
    }
    catch (NumberFormatException localNumberFormatException)
    {
      int i;
      for (;;)
      {
        i = 0;
        continue;
        paramMap = "";
      }
      label121:
      zzb.a(paramzzjp.h());
      new zzdp(paramzzjp, paramzzjp.h().a.a(paramzzjp, i, paramMap), str).f();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzds.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */