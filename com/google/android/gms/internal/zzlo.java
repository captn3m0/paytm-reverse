package com.google.android.gms.internal;

import com.google.android.gms.cast.internal.zzf;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzlo
{
  private final String a;
  private final int b;
  private final JSONObject c;
  
  public zzlo(String paramString, int paramInt, JSONObject paramJSONObject)
  {
    this.a = paramString;
    this.b = paramInt;
    this.c = paramJSONObject;
  }
  
  public zzlo(JSONObject paramJSONObject)
    throws JSONException
  {
    this(paramJSONObject.optString("playerId"), paramJSONObject.optInt("playerState"), paramJSONObject.optJSONObject("playerData"));
  }
  
  public int a()
  {
    return this.b;
  }
  
  public JSONObject b()
  {
    return this.c;
  }
  
  public String c()
  {
    return this.a;
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject == null) || (!(paramObject instanceof zzlo))) {}
    do
    {
      return false;
      paramObject = (zzlo)paramObject;
    } while ((this.b != ((zzlo)paramObject).a()) || (!zzf.a(this.a, ((zzlo)paramObject).c())) || (!zznb.a(this.c, ((zzlo)paramObject).b())));
    return true;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzlo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */