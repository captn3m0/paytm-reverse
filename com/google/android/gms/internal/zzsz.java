package com.google.android.gms.internal;

import java.io.IOException;
import java.util.Arrays;

public abstract interface zzsz
{
  public static final class zza
    extends zzso<zza>
  {
    public String[] a;
    public String[] b;
    public int[] c;
    public long[] d;
    
    public zza()
    {
      a();
    }
    
    public zza a()
    {
      this.a = zzsx.f;
      this.b = zzsx.f;
      this.c = zzsx.a;
      this.d = zzsx.b;
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public zza a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        int j;
        Object localObject;
        int k;
        switch (i)
        {
        default: 
          if (a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 10: 
          j = zzsx.b(paramzzsm, 10);
          if (this.a == null) {}
          for (i = 0;; i = this.a.length)
          {
            localObject = new String[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.a, 0, localObject, 0, i);
              j = i;
            }
            while (j < localObject.length - 1)
            {
              localObject[j] = paramzzsm.i();
              paramzzsm.a();
              j += 1;
            }
          }
          localObject[j] = paramzzsm.i();
          this.a = ((String[])localObject);
          break;
        case 18: 
          j = zzsx.b(paramzzsm, 18);
          if (this.b == null) {}
          for (i = 0;; i = this.b.length)
          {
            localObject = new String[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.b, 0, localObject, 0, i);
              j = i;
            }
            while (j < localObject.length - 1)
            {
              localObject[j] = paramzzsm.i();
              paramzzsm.a();
              j += 1;
            }
          }
          localObject[j] = paramzzsm.i();
          this.b = ((String[])localObject);
          break;
        case 24: 
          j = zzsx.b(paramzzsm, 24);
          if (this.c == null) {}
          for (i = 0;; i = this.c.length)
          {
            localObject = new int[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.c, 0, localObject, 0, i);
              j = i;
            }
            while (j < localObject.length - 1)
            {
              localObject[j] = paramzzsm.g();
              paramzzsm.a();
              j += 1;
            }
          }
          localObject[j] = paramzzsm.g();
          this.c = ((int[])localObject);
          break;
        case 26: 
          k = paramzzsm.d(paramzzsm.m());
          i = paramzzsm.s();
          j = 0;
          while (paramzzsm.q() > 0)
          {
            paramzzsm.g();
            j += 1;
          }
          paramzzsm.f(i);
          if (this.c == null) {}
          for (i = 0;; i = this.c.length)
          {
            localObject = new int[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.c, 0, localObject, 0, i);
              j = i;
            }
            while (j < localObject.length)
            {
              localObject[j] = paramzzsm.g();
              j += 1;
            }
          }
          this.c = ((int[])localObject);
          paramzzsm.e(k);
          break;
        case 32: 
          j = zzsx.b(paramzzsm, 32);
          if (this.d == null) {}
          for (i = 0;; i = this.d.length)
          {
            localObject = new long[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.d, 0, localObject, 0, i);
              j = i;
            }
            while (j < localObject.length - 1)
            {
              localObject[j] = paramzzsm.f();
              paramzzsm.a();
              j += 1;
            }
          }
          localObject[j] = paramzzsm.f();
          this.d = ((long[])localObject);
          break;
        case 34: 
          k = paramzzsm.d(paramzzsm.m());
          i = paramzzsm.s();
          j = 0;
          while (paramzzsm.q() > 0)
          {
            paramzzsm.f();
            j += 1;
          }
          paramzzsm.f(i);
          if (this.d == null) {}
          for (i = 0;; i = this.d.length)
          {
            localObject = new long[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.d, 0, localObject, 0, i);
              j = i;
            }
            while (j < localObject.length)
            {
              localObject[j] = paramzzsm.f();
              j += 1;
            }
          }
          this.d = ((long[])localObject);
          paramzzsm.e(k);
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      int j = 0;
      int i;
      String str;
      if ((this.a != null) && (this.a.length > 0))
      {
        i = 0;
        while (i < this.a.length)
        {
          str = this.a[i];
          if (str != null) {
            paramzzsn.a(1, str);
          }
          i += 1;
        }
      }
      if ((this.b != null) && (this.b.length > 0))
      {
        i = 0;
        while (i < this.b.length)
        {
          str = this.b[i];
          if (str != null) {
            paramzzsn.a(2, str);
          }
          i += 1;
        }
      }
      if ((this.c != null) && (this.c.length > 0))
      {
        i = 0;
        while (i < this.c.length)
        {
          paramzzsn.a(3, this.c[i]);
          i += 1;
        }
      }
      if ((this.d != null) && (this.d.length > 0))
      {
        i = j;
        while (i < this.d.length)
        {
          paramzzsn.b(4, this.d[i]);
          i += 1;
        }
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int i2 = 0;
      int i1 = super.b();
      int j;
      int k;
      String str;
      int n;
      int m;
      if ((this.a != null) && (this.a.length > 0))
      {
        i = 0;
        j = 0;
        for (k = 0; i < this.a.length; k = m)
        {
          str = this.a[i];
          n = j;
          m = k;
          if (str != null)
          {
            m = k + 1;
            n = j + zzsn.b(str);
          }
          i += 1;
          j = n;
        }
      }
      for (int i = i1 + j + k * 1;; i = i1)
      {
        j = i;
        if (this.b != null)
        {
          j = i;
          if (this.b.length > 0)
          {
            j = 0;
            k = 0;
            for (m = 0; j < this.b.length; m = n)
            {
              str = this.b[j];
              i1 = k;
              n = m;
              if (str != null)
              {
                n = m + 1;
                i1 = k + zzsn.b(str);
              }
              j += 1;
              k = i1;
            }
            j = i + k + m * 1;
          }
        }
        i = j;
        if (this.c != null)
        {
          i = j;
          if (this.c.length > 0)
          {
            i = 0;
            k = 0;
            while (i < this.c.length)
            {
              k += zzsn.c(this.c[i]);
              i += 1;
            }
            i = j + k + this.c.length * 1;
          }
        }
        j = i;
        if (this.d != null)
        {
          j = i;
          if (this.d.length > 0)
          {
            k = 0;
            j = i2;
            while (j < this.d.length)
            {
              k += zzsn.e(this.d[j]);
              j += 1;
            }
            j = i + k + this.d.length * 1;
          }
        }
        return j;
      }
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  return bool1;
                  bool1 = bool2;
                } while (!(paramObject instanceof zza));
                paramObject = (zza)paramObject;
                bool1 = bool2;
              } while (!zzss.a(this.a, ((zza)paramObject).a));
              bool1 = bool2;
            } while (!zzss.a(this.b, ((zza)paramObject).b));
            bool1 = bool2;
          } while (!zzss.a(this.c, ((zza)paramObject).c));
          bool1 = bool2;
        } while (!zzss.a(this.d, ((zza)paramObject).d));
        if ((this.r != null) && (!this.r.b())) {
          break label127;
        }
        if (((zza)paramObject).r == null) {
          break;
        }
        bool1 = bool2;
      } while (!((zza)paramObject).r.b());
      return true;
      label127:
      return this.r.equals(((zza)paramObject).r);
    }
    
    public int hashCode()
    {
      int j = getClass().getName().hashCode();
      int k = zzss.a(this.a);
      int m = zzss.a(this.b);
      int n = zzss.a(this.c);
      int i1 = zzss.a(this.d);
      if ((this.r == null) || (this.r.b())) {}
      for (int i = 0;; i = this.r.hashCode()) {
        return i + (((((j + 527) * 31 + k) * 31 + m) * 31 + n) * 31 + i1) * 31;
      }
    }
  }
  
  public static final class zzb
    extends zzso<zzb>
  {
    public int a;
    public String b;
    public String c;
    
    public zzb()
    {
      a();
    }
    
    public zzb a()
    {
      this.a = 0;
      this.b = "";
      this.c = "";
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public zzb a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 8: 
          i = paramzzsm.g();
          switch (i)
          {
          default: 
            break;
          case 0: 
          case 1: 
          case 2: 
          case 3: 
          case 4: 
          case 5: 
          case 6: 
          case 7: 
          case 8: 
          case 9: 
          case 10: 
          case 11: 
          case 12: 
          case 13: 
          case 14: 
          case 15: 
          case 16: 
          case 17: 
          case 18: 
          case 19: 
          case 20: 
          case 21: 
          case 22: 
          case 23: 
          case 24: 
          case 25: 
          case 26: 
            this.a = i;
          }
          break;
        case 18: 
          this.b = paramzzsm.i();
          break;
        case 26: 
          this.c = paramzzsm.i();
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (this.a != 0) {
        paramzzsn.a(1, this.a);
      }
      if (!this.b.equals("")) {
        paramzzsn.a(2, this.b);
      }
      if (!this.c.equals("")) {
        paramzzsn.a(3, this.c);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b();
      int i = j;
      if (this.a != 0) {
        i = j + zzsn.c(1, this.a);
      }
      j = i;
      if (!this.b.equals("")) {
        j = i + zzsn.b(2, this.b);
      }
      i = j;
      if (!this.c.equals("")) {
        i = j + zzsn.b(3, this.c);
      }
      return i;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      label54:
      do
      {
        do
        {
          do
          {
            do
            {
              return bool1;
              bool1 = bool2;
            } while (!(paramObject instanceof zzb));
            paramObject = (zzb)paramObject;
            bool1 = bool2;
          } while (this.a != ((zzb)paramObject).a);
          if (this.b != null) {
            break;
          }
          bool1 = bool2;
        } while (((zzb)paramObject).b != null);
        if (this.c != null) {
          break label124;
        }
        bool1 = bool2;
      } while (((zzb)paramObject).c != null);
      for (;;)
      {
        if ((this.r == null) || (this.r.b()))
        {
          if (((zzb)paramObject).r != null)
          {
            bool1 = bool2;
            if (!((zzb)paramObject).r.b()) {
              break;
            }
          }
          return true;
          if (this.b.equals(((zzb)paramObject).b)) {
            break label54;
          }
          return false;
          label124:
          if (!this.c.equals(((zzb)paramObject).c)) {
            return false;
          }
        }
      }
      return this.r.equals(((zzb)paramObject).r);
    }
    
    public int hashCode()
    {
      int m = 0;
      int n = getClass().getName().hashCode();
      int i1 = this.a;
      int i;
      int j;
      if (this.b == null)
      {
        i = 0;
        if (this.c != null) {
          break label101;
        }
        j = 0;
        label39:
        k = m;
        if (this.r != null) {
          if (!this.r.b()) {
            break label112;
          }
        }
      }
      label101:
      label112:
      for (int k = m;; k = this.r.hashCode())
      {
        return (j + (i + ((n + 527) * 31 + i1) * 31) * 31) * 31 + k;
        i = this.b.hashCode();
        break;
        j = this.c.hashCode();
        break label39;
      }
    }
  }
  
  public static final class zzc
    extends zzso<zzc>
  {
    public byte[] a;
    public byte[][] b;
    public boolean c;
    
    public zzc()
    {
      a();
    }
    
    public zzc a()
    {
      this.a = zzsx.h;
      this.b = zzsx.g;
      this.c = false;
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public zzc a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 10: 
          this.a = paramzzsm.j();
          break;
        case 18: 
          int j = zzsx.b(paramzzsm, 18);
          if (this.b == null) {}
          byte[][] arrayOfByte;
          for (i = 0;; i = this.b.length)
          {
            arrayOfByte = new byte[j + i][];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.b, 0, arrayOfByte, 0, i);
              j = i;
            }
            while (j < arrayOfByte.length - 1)
            {
              arrayOfByte[j] = paramzzsm.j();
              paramzzsm.a();
              j += 1;
            }
          }
          arrayOfByte[j] = paramzzsm.j();
          this.b = arrayOfByte;
          break;
        case 24: 
          this.c = paramzzsm.h();
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (!Arrays.equals(this.a, zzsx.h)) {
        paramzzsn.a(1, this.a);
      }
      if ((this.b != null) && (this.b.length > 0))
      {
        int i = 0;
        while (i < this.b.length)
        {
          byte[] arrayOfByte = this.b[i];
          if (arrayOfByte != null) {
            paramzzsn.a(2, arrayOfByte);
          }
          i += 1;
        }
      }
      if (this.c) {
        paramzzsn.a(3, this.c);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int n = 0;
      int j = super.b();
      int i = j;
      if (!Arrays.equals(this.a, zzsx.h)) {
        i = j + zzsn.b(1, this.a);
      }
      j = i;
      if (this.b != null)
      {
        j = i;
        if (this.b.length > 0)
        {
          int k = 0;
          int m = 0;
          j = n;
          while (j < this.b.length)
          {
            byte[] arrayOfByte = this.b[j];
            int i1 = k;
            n = m;
            if (arrayOfByte != null)
            {
              n = m + 1;
              i1 = k + zzsn.c(arrayOfByte);
            }
            j += 1;
            k = i1;
            m = n;
          }
          j = i + k + m * 1;
        }
      }
      i = j;
      if (this.c) {
        i = j + zzsn.b(3, this.c);
      }
      return i;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                return bool1;
                bool1 = bool2;
              } while (!(paramObject instanceof zzc));
              paramObject = (zzc)paramObject;
              bool1 = bool2;
            } while (!Arrays.equals(this.a, ((zzc)paramObject).a));
            bool1 = bool2;
          } while (!zzss.a(this.b, ((zzc)paramObject).b));
          bool1 = bool2;
        } while (this.c != ((zzc)paramObject).c);
        if ((this.r != null) && (!this.r.b())) {
          break label108;
        }
        if (((zzc)paramObject).r == null) {
          break;
        }
        bool1 = bool2;
      } while (!((zzc)paramObject).r.b());
      return true;
      label108:
      return this.r.equals(((zzc)paramObject).r);
    }
    
    public int hashCode()
    {
      int k = getClass().getName().hashCode();
      int m = Arrays.hashCode(this.a);
      int n = zzss.a(this.b);
      int i;
      if (this.c)
      {
        i = 1231;
        if ((this.r != null) && (!this.r.b())) {
          break label94;
        }
      }
      label94:
      for (int j = 0;; j = this.r.hashCode())
      {
        return j + (i + (((k + 527) * 31 + m) * 31 + n) * 31) * 31;
        i = 1237;
        break;
      }
    }
  }
  
  public static final class zzd
    extends zzso<zzd>
  {
    public long a;
    public long b;
    public long c;
    public String d;
    public int e;
    public int f;
    public boolean g;
    public zzsz.zze[] h;
    public zzsz.zzb i;
    public byte[] j;
    public byte[] k;
    public byte[] l;
    public zzsz.zza m;
    public String n;
    public long o;
    public zzsz.zzc p;
    public byte[] q;
    public int s;
    public int[] t;
    public long u;
    
    public zzd()
    {
      a();
    }
    
    public zzd a()
    {
      this.a = 0L;
      this.b = 0L;
      this.c = 0L;
      this.d = "";
      this.e = 0;
      this.f = 0;
      this.g = false;
      this.h = zzsz.zze.a();
      this.i = null;
      this.j = zzsx.h;
      this.k = zzsx.h;
      this.l = zzsx.h;
      this.m = null;
      this.n = "";
      this.o = 180000L;
      this.p = null;
      this.q = zzsx.h;
      this.s = 0;
      this.t = zzsx.a;
      this.u = 0L;
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public zzd a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i1 = paramzzsm.a();
        int i2;
        Object localObject;
        switch (i1)
        {
        default: 
          if (a(paramzzsm, i1)) {}
          break;
        case 0: 
          return this;
        case 8: 
          this.a = paramzzsm.f();
          break;
        case 18: 
          this.d = paramzzsm.i();
          break;
        case 26: 
          i2 = zzsx.b(paramzzsm, 26);
          if (this.h == null) {}
          for (i1 = 0;; i1 = this.h.length)
          {
            localObject = new zzsz.zze[i2 + i1];
            i2 = i1;
            if (i1 != 0)
            {
              System.arraycopy(this.h, 0, localObject, 0, i1);
              i2 = i1;
            }
            while (i2 < localObject.length - 1)
            {
              localObject[i2] = new zzsz.zze();
              paramzzsm.a(localObject[i2]);
              paramzzsm.a();
              i2 += 1;
            }
          }
          localObject[i2] = new zzsz.zze();
          paramzzsm.a(localObject[i2]);
          this.h = ((zzsz.zze[])localObject);
          break;
        case 50: 
          this.j = paramzzsm.j();
          break;
        case 58: 
          if (this.m == null) {
            this.m = new zzsz.zza();
          }
          paramzzsm.a(this.m);
          break;
        case 66: 
          this.k = paramzzsm.j();
          break;
        case 74: 
          if (this.i == null) {
            this.i = new zzsz.zzb();
          }
          paramzzsm.a(this.i);
          break;
        case 80: 
          this.g = paramzzsm.h();
          break;
        case 88: 
          this.e = paramzzsm.g();
          break;
        case 96: 
          this.f = paramzzsm.g();
          break;
        case 106: 
          this.l = paramzzsm.j();
          break;
        case 114: 
          this.n = paramzzsm.i();
          break;
        case 120: 
          this.o = paramzzsm.l();
          break;
        case 130: 
          if (this.p == null) {
            this.p = new zzsz.zzc();
          }
          paramzzsm.a(this.p);
          break;
        case 136: 
          this.b = paramzzsm.f();
          break;
        case 146: 
          this.q = paramzzsm.j();
          break;
        case 152: 
          i1 = paramzzsm.g();
          switch (i1)
          {
          default: 
            break;
          case 0: 
          case 1: 
          case 2: 
            this.s = i1;
          }
          break;
        case 160: 
          i2 = zzsx.b(paramzzsm, 160);
          if (this.t == null) {}
          for (i1 = 0;; i1 = this.t.length)
          {
            localObject = new int[i2 + i1];
            i2 = i1;
            if (i1 != 0)
            {
              System.arraycopy(this.t, 0, localObject, 0, i1);
              i2 = i1;
            }
            while (i2 < localObject.length - 1)
            {
              localObject[i2] = paramzzsm.g();
              paramzzsm.a();
              i2 += 1;
            }
          }
          localObject[i2] = paramzzsm.g();
          this.t = ((int[])localObject);
          break;
        case 162: 
          int i3 = paramzzsm.d(paramzzsm.m());
          i1 = paramzzsm.s();
          i2 = 0;
          while (paramzzsm.q() > 0)
          {
            paramzzsm.g();
            i2 += 1;
          }
          paramzzsm.f(i1);
          if (this.t == null) {}
          for (i1 = 0;; i1 = this.t.length)
          {
            localObject = new int[i2 + i1];
            i2 = i1;
            if (i1 != 0)
            {
              System.arraycopy(this.t, 0, localObject, 0, i1);
              i2 = i1;
            }
            while (i2 < localObject.length)
            {
              localObject[i2] = paramzzsm.g();
              i2 += 1;
            }
          }
          this.t = ((int[])localObject);
          paramzzsm.e(i3);
          break;
        case 168: 
          this.c = paramzzsm.f();
          break;
        case 176: 
          this.u = paramzzsm.f();
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      int i2 = 0;
      if (this.a != 0L) {
        paramzzsn.b(1, this.a);
      }
      if (!this.d.equals("")) {
        paramzzsn.a(2, this.d);
      }
      int i1;
      if ((this.h != null) && (this.h.length > 0))
      {
        i1 = 0;
        while (i1 < this.h.length)
        {
          zzsz.zze localzze = this.h[i1];
          if (localzze != null) {
            paramzzsn.a(3, localzze);
          }
          i1 += 1;
        }
      }
      if (!Arrays.equals(this.j, zzsx.h)) {
        paramzzsn.a(6, this.j);
      }
      if (this.m != null) {
        paramzzsn.a(7, this.m);
      }
      if (!Arrays.equals(this.k, zzsx.h)) {
        paramzzsn.a(8, this.k);
      }
      if (this.i != null) {
        paramzzsn.a(9, this.i);
      }
      if (this.g) {
        paramzzsn.a(10, this.g);
      }
      if (this.e != 0) {
        paramzzsn.a(11, this.e);
      }
      if (this.f != 0) {
        paramzzsn.a(12, this.f);
      }
      if (!Arrays.equals(this.l, zzsx.h)) {
        paramzzsn.a(13, this.l);
      }
      if (!this.n.equals("")) {
        paramzzsn.a(14, this.n);
      }
      if (this.o != 180000L) {
        paramzzsn.c(15, this.o);
      }
      if (this.p != null) {
        paramzzsn.a(16, this.p);
      }
      if (this.b != 0L) {
        paramzzsn.b(17, this.b);
      }
      if (!Arrays.equals(this.q, zzsx.h)) {
        paramzzsn.a(18, this.q);
      }
      if (this.s != 0) {
        paramzzsn.a(19, this.s);
      }
      if ((this.t != null) && (this.t.length > 0))
      {
        i1 = i2;
        while (i1 < this.t.length)
        {
          paramzzsn.a(20, this.t[i1]);
          i1 += 1;
        }
      }
      if (this.c != 0L) {
        paramzzsn.b(21, this.c);
      }
      if (this.u != 0L) {
        paramzzsn.b(22, this.u);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int i4 = 0;
      int i1 = super.b();
      int i2 = i1;
      if (this.a != 0L) {
        i2 = i1 + zzsn.d(1, this.a);
      }
      i1 = i2;
      if (!this.d.equals("")) {
        i1 = i2 + zzsn.b(2, this.d);
      }
      i2 = i1;
      if (this.h != null)
      {
        i2 = i1;
        if (this.h.length > 0)
        {
          i2 = 0;
          while (i2 < this.h.length)
          {
            zzsz.zze localzze = this.h[i2];
            i3 = i1;
            if (localzze != null) {
              i3 = i1 + zzsn.c(3, localzze);
            }
            i2 += 1;
            i1 = i3;
          }
          i2 = i1;
        }
      }
      i1 = i2;
      if (!Arrays.equals(this.j, zzsx.h)) {
        i1 = i2 + zzsn.b(6, this.j);
      }
      i2 = i1;
      if (this.m != null) {
        i2 = i1 + zzsn.c(7, this.m);
      }
      i1 = i2;
      if (!Arrays.equals(this.k, zzsx.h)) {
        i1 = i2 + zzsn.b(8, this.k);
      }
      i2 = i1;
      if (this.i != null) {
        i2 = i1 + zzsn.c(9, this.i);
      }
      i1 = i2;
      if (this.g) {
        i1 = i2 + zzsn.b(10, this.g);
      }
      i2 = i1;
      if (this.e != 0) {
        i2 = i1 + zzsn.c(11, this.e);
      }
      i1 = i2;
      if (this.f != 0) {
        i1 = i2 + zzsn.c(12, this.f);
      }
      i2 = i1;
      if (!Arrays.equals(this.l, zzsx.h)) {
        i2 = i1 + zzsn.b(13, this.l);
      }
      i1 = i2;
      if (!this.n.equals("")) {
        i1 = i2 + zzsn.b(14, this.n);
      }
      i2 = i1;
      if (this.o != 180000L) {
        i2 = i1 + zzsn.e(15, this.o);
      }
      i1 = i2;
      if (this.p != null) {
        i1 = i2 + zzsn.c(16, this.p);
      }
      i2 = i1;
      if (this.b != 0L) {
        i2 = i1 + zzsn.d(17, this.b);
      }
      int i3 = i2;
      if (!Arrays.equals(this.q, zzsx.h)) {
        i3 = i2 + zzsn.b(18, this.q);
      }
      i1 = i3;
      if (this.s != 0) {
        i1 = i3 + zzsn.c(19, this.s);
      }
      i2 = i1;
      if (this.t != null)
      {
        i2 = i1;
        if (this.t.length > 0)
        {
          i3 = 0;
          i2 = i4;
          while (i2 < this.t.length)
          {
            i3 += zzsn.c(this.t[i2]);
            i2 += 1;
          }
          i2 = i1 + i3 + this.t.length * 2;
        }
      }
      i1 = i2;
      if (this.c != 0L) {
        i1 = i2 + zzsn.d(21, this.c);
      }
      i2 = i1;
      if (this.u != 0L) {
        i2 = i1 + zzsn.d(22, this.u);
      }
      return i2;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      label83:
      label154:
      label218:
      label234:
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  do
                  {
                    do
                    {
                      do
                      {
                        do
                        {
                          do
                          {
                            do
                            {
                              do
                              {
                                do
                                {
                                  do
                                  {
                                    do
                                    {
                                      do
                                      {
                                        return bool1;
                                        bool1 = bool2;
                                      } while (!(paramObject instanceof zzd));
                                      paramObject = (zzd)paramObject;
                                      bool1 = bool2;
                                    } while (this.a != ((zzd)paramObject).a);
                                    bool1 = bool2;
                                  } while (this.b != ((zzd)paramObject).b);
                                  bool1 = bool2;
                                } while (this.c != ((zzd)paramObject).c);
                                if (this.d != null) {
                                  break;
                                }
                                bool1 = bool2;
                              } while (((zzd)paramObject).d != null);
                              bool1 = bool2;
                            } while (this.e != ((zzd)paramObject).e);
                            bool1 = bool2;
                          } while (this.f != ((zzd)paramObject).f);
                          bool1 = bool2;
                        } while (this.g != ((zzd)paramObject).g);
                        bool1 = bool2;
                      } while (!zzss.a(this.h, ((zzd)paramObject).h));
                      if (this.i != null) {
                        break label377;
                      }
                      bool1 = bool2;
                    } while (((zzd)paramObject).i != null);
                    bool1 = bool2;
                  } while (!Arrays.equals(this.j, ((zzd)paramObject).j));
                  bool1 = bool2;
                } while (!Arrays.equals(this.k, ((zzd)paramObject).k));
                bool1 = bool2;
              } while (!Arrays.equals(this.l, ((zzd)paramObject).l));
              if (this.m != null) {
                break label393;
              }
              bool1 = bool2;
            } while (((zzd)paramObject).m != null);
            if (this.n != null) {
              break label409;
            }
            bool1 = bool2;
          } while (((zzd)paramObject).n != null);
          bool1 = bool2;
        } while (this.o != ((zzd)paramObject).o);
        if (this.p != null) {
          break label425;
        }
        bool1 = bool2;
      } while (((zzd)paramObject).p != null);
      label377:
      label393:
      label409:
      label425:
      while (this.p.equals(((zzd)paramObject).p))
      {
        bool1 = bool2;
        if (!Arrays.equals(this.q, ((zzd)paramObject).q)) {
          break;
        }
        bool1 = bool2;
        if (this.s != ((zzd)paramObject).s) {
          break;
        }
        bool1 = bool2;
        if (!zzss.a(this.t, ((zzd)paramObject).t)) {
          break;
        }
        bool1 = bool2;
        if (this.u != ((zzd)paramObject).u) {
          break;
        }
        if ((this.r != null) && (!this.r.b())) {
          break label441;
        }
        if (((zzd)paramObject).r != null)
        {
          bool1 = bool2;
          if (!((zzd)paramObject).r.b()) {
            break;
          }
        }
        return true;
        if (this.d.equals(((zzd)paramObject).d)) {
          break label83;
        }
        return false;
        if (this.i.equals(((zzd)paramObject).i)) {
          break label154;
        }
        return false;
        if (this.m.equals(((zzd)paramObject).m)) {
          break label218;
        }
        return false;
        if (this.n.equals(((zzd)paramObject).n)) {
          break label234;
        }
        return false;
      }
      return false;
      label441:
      return this.r.equals(((zzd)paramObject).r);
    }
    
    public int hashCode()
    {
      int i8 = 0;
      int i9 = getClass().getName().hashCode();
      int i10 = (int)(this.a ^ this.a >>> 32);
      int i11 = (int)(this.b ^ this.b >>> 32);
      int i12 = (int)(this.c ^ this.c >>> 32);
      int i1;
      int i13;
      int i14;
      int i2;
      label92:
      int i15;
      int i3;
      label110:
      int i16;
      int i17;
      int i18;
      int i4;
      label147:
      int i5;
      label157:
      int i19;
      int i6;
      label182:
      int i20;
      int i21;
      int i22;
      int i23;
      if (this.d == null)
      {
        i1 = 0;
        i13 = this.e;
        i14 = this.f;
        if (!this.g) {
          break label387;
        }
        i2 = 1231;
        i15 = zzss.a(this.h);
        if (this.i != null) {
          break label394;
        }
        i3 = 0;
        i16 = Arrays.hashCode(this.j);
        i17 = Arrays.hashCode(this.k);
        i18 = Arrays.hashCode(this.l);
        if (this.m != null) {
          break label405;
        }
        i4 = 0;
        if (this.n != null) {
          break label417;
        }
        i5 = 0;
        i19 = (int)(this.o ^ this.o >>> 32);
        if (this.p != null) {
          break label429;
        }
        i6 = 0;
        i20 = Arrays.hashCode(this.q);
        i21 = this.s;
        i22 = zzss.a(this.t);
        i23 = (int)(this.u ^ this.u >>> 32);
        i7 = i8;
        if (this.r != null) {
          if (!this.r.b()) {
            break label441;
          }
        }
      }
      label387:
      label394:
      label405:
      label417:
      label429:
      label441:
      for (int i7 = i8;; i7 = this.r.hashCode())
      {
        return (((((i6 + ((i5 + (i4 + ((((i3 + ((i2 + (((i1 + ((((i9 + 527) * 31 + i10) * 31 + i11) * 31 + i12) * 31) * 31 + i13) * 31 + i14) * 31) * 31 + i15) * 31) * 31 + i16) * 31 + i17) * 31 + i18) * 31) * 31) * 31 + i19) * 31) * 31 + i20) * 31 + i21) * 31 + i22) * 31 + i23) * 31 + i7;
        i1 = this.d.hashCode();
        break;
        i2 = 1237;
        break label92;
        i3 = this.i.hashCode();
        break label110;
        i4 = this.m.hashCode();
        break label147;
        i5 = this.n.hashCode();
        break label157;
        i6 = this.p.hashCode();
        break label182;
      }
    }
  }
  
  public static final class zze
    extends zzso<zze>
  {
    private static volatile zze[] c;
    public String a;
    public String b;
    
    public zze()
    {
      c();
    }
    
    public static zze[] a()
    {
      if (c == null) {}
      synchronized (zzss.a)
      {
        if (c == null) {
          c = new zze[0];
        }
        return c;
      }
    }
    
    public zze a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 10: 
          this.a = paramzzsm.i();
          break;
        case 18: 
          this.b = paramzzsm.i();
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (!this.a.equals("")) {
        paramzzsn.a(1, this.a);
      }
      if (!this.b.equals("")) {
        paramzzsn.a(2, this.b);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b();
      int i = j;
      if (!this.a.equals("")) {
        i = j + zzsn.b(1, this.a);
      }
      j = i;
      if (!this.b.equals("")) {
        j = i + zzsn.b(2, this.b);
      }
      return j;
    }
    
    public zze c()
    {
      this.a = "";
      this.b = "";
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      label41:
      do
      {
        do
        {
          do
          {
            return bool1;
            bool1 = bool2;
          } while (!(paramObject instanceof zze));
          paramObject = (zze)paramObject;
          if (this.a != null) {
            break;
          }
          bool1 = bool2;
        } while (((zze)paramObject).a != null);
        if (this.b != null) {
          break label111;
        }
        bool1 = bool2;
      } while (((zze)paramObject).b != null);
      for (;;)
      {
        if ((this.r == null) || (this.r.b()))
        {
          if (((zze)paramObject).r != null)
          {
            bool1 = bool2;
            if (!((zze)paramObject).r.b()) {
              break;
            }
          }
          return true;
          if (this.a.equals(((zze)paramObject).a)) {
            break label41;
          }
          return false;
          label111:
          if (!this.b.equals(((zze)paramObject).b)) {
            return false;
          }
        }
      }
      return this.r.equals(((zze)paramObject).r);
    }
    
    public int hashCode()
    {
      int m = 0;
      int n = getClass().getName().hashCode();
      int i;
      int j;
      if (this.a == null)
      {
        i = 0;
        if (this.b != null) {
          break label89;
        }
        j = 0;
        label33:
        k = m;
        if (this.r != null) {
          if (!this.r.b()) {
            break label100;
          }
        }
      }
      label89:
      label100:
      for (int k = m;; k = this.r.hashCode())
      {
        return (j + (i + (n + 527) * 31) * 31) * 31 + k;
        i = this.a.hashCode();
        break;
        j = this.b.hashCode();
        break label33;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzsz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */