package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.ads.internal.client.zzp;
import com.google.android.gms.ads.internal.client.zzq;
import com.google.android.gms.ads.internal.client.zzq.zza;
import com.google.android.gms.ads.internal.client.zzw;
import com.google.android.gms.ads.internal.reward.client.zzd;
import com.google.android.gms.ads.internal.zzk;
import com.google.android.gms.ads.internal.zzr;

@zzhb
class zzdx
{
  zzq a;
  zzw b;
  zzgd c;
  zzcf d;
  zzp e;
  zzd f;
  
  void a(zzk paramzzk)
  {
    if (this.a != null) {
      paramzzk.a(new zza(this.a));
    }
    if (this.b != null) {
      paramzzk.a(this.b);
    }
    if (this.c != null) {
      paramzzk.a(this.c);
    }
    if (this.d != null) {
      paramzzk.a(this.d);
    }
    if (this.e != null) {
      paramzzk.a(this.e);
    }
    if (this.f != null) {
      paramzzk.a(this.f);
    }
  }
  
  private class zza
    extends zzq.zza
  {
    zzq a;
    
    zza(zzq paramzzq)
    {
      this.a = paramzzq;
    }
    
    public void a()
      throws RemoteException
    {
      this.a.a();
      zzr.p().a();
    }
    
    public void a(int paramInt)
      throws RemoteException
    {
      this.a.a(paramInt);
    }
    
    public void b()
      throws RemoteException
    {
      this.a.b();
    }
    
    public void c()
      throws RemoteException
    {
      this.a.c();
    }
    
    public void d()
      throws RemoteException
    {
      this.a.d();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzdx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */