package com.google.android.gms.internal;

public abstract interface zzji<T>
{
  public abstract void a(zzc<T> paramzzc, zza paramzza);
  
  public abstract void a(T paramT);
  
  public static abstract interface zza
  {
    public abstract void a();
  }
  
  public static class zzb
    implements zzji.zza
  {
    public void a() {}
  }
  
  public static abstract interface zzc<T>
  {
    public abstract void a(T paramT);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzji.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */