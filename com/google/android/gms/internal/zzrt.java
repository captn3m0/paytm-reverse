package com.google.android.gms.internal;

import android.content.Context;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class zzrt
{
  public static final Integer a = Integer.valueOf(0);
  public static final Integer b = Integer.valueOf(1);
  private final Context c;
  private final ExecutorService d;
  
  public zzrt(Context paramContext)
  {
    this(paramContext, Executors.newSingleThreadExecutor());
  }
  
  zzrt(Context paramContext, ExecutorService paramExecutorService)
  {
    this.c = paramContext;
    this.d = paramExecutorService;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzrt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */