package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;

@zzhb
public abstract class zzhl
{
  public abstract void a(Context paramContext, zzhf paramzzhf, VersionInfoParcel paramVersionInfoParcel);
  
  protected void a(zzhf paramzzhf)
  {
    paramzzhf.c();
    if (paramzzhf.a() != null) {
      paramzzhf.a().a();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzhl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */