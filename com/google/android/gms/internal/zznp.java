package com.google.android.gms.internal;

import android.os.Bundle;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.internal.zzk;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

public class zznp
  extends zzk<DriveId>
{
  public static final zznp a = new zznp();
  
  private zznp()
  {
    super("driveId", Arrays.asList(new String[] { "sqlId", "resourceId", "mimeType" }), Arrays.asList(new String[] { "dbInstanceId" }), 4100000);
  }
  
  protected boolean b(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    Iterator localIterator = b().iterator();
    while (localIterator.hasNext()) {
      if (!paramDataHolder.a((String)localIterator.next())) {
        return false;
      }
    }
    return true;
  }
  
  protected DriveId d(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    long l1 = paramDataHolder.f().getLong("dbInstanceId");
    if ("application/vnd.google-apps.folder".equals(paramDataHolder.c(zznm.x.a(), paramInt1, paramInt2))) {}
    for (int i = 1;; i = 0)
    {
      String str = paramDataHolder.c("resourceId", paramInt1, paramInt2);
      long l2 = paramDataHolder.a("sqlId", paramInt1, paramInt2);
      paramDataHolder = str;
      if ("generated-android-null".equals(str)) {
        paramDataHolder = null;
      }
      return new DriveId(paramDataHolder, Long.valueOf(l2).longValue(), l1, i);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zznp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */