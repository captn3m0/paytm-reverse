package com.google.android.gms.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Account;
import com.google.android.gms.plus.Plus.zza;
import com.google.android.gms.plus.internal.zze;

public final class zzqv
  implements Account
{
  private static abstract class zza
    extends Plus.zza<Status>
  {
    public Status a(Status paramStatus)
    {
      return paramStatus;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzqv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */