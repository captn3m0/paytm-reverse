package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.measurement.zze;
import java.util.HashMap;
import java.util.Map;

public final class zzpq
  extends zze<zzpq>
{
  private String a;
  private String b;
  private String c;
  private String d;
  
  public String a()
  {
    return this.a;
  }
  
  public void a(zzpq paramzzpq)
  {
    if (!TextUtils.isEmpty(this.a)) {
      paramzzpq.a(this.a);
    }
    if (!TextUtils.isEmpty(this.b)) {
      paramzzpq.b(this.b);
    }
    if (!TextUtils.isEmpty(this.c)) {
      paramzzpq.c(this.c);
    }
    if (!TextUtils.isEmpty(this.d)) {
      paramzzpq.d(this.d);
    }
  }
  
  public void a(String paramString)
  {
    this.a = paramString;
  }
  
  public String b()
  {
    return this.b;
  }
  
  public void b(String paramString)
  {
    this.b = paramString;
  }
  
  public String c()
  {
    return this.c;
  }
  
  public void c(String paramString)
  {
    this.c = paramString;
  }
  
  public String d()
  {
    return this.d;
  }
  
  public void d(String paramString)
  {
    this.d = paramString;
  }
  
  public String toString()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("appName", this.a);
    localHashMap.put("appVersion", this.b);
    localHashMap.put("appId", this.c);
    localHashMap.put("appInstallerId", this.d);
    return a(localHashMap);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzpq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */