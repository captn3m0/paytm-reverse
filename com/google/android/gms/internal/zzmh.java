package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zzb;

public final class zzmh
  implements zzmg
{
  public PendingResult<Status> a(GoogleApiClient paramGoogleApiClient)
  {
    paramGoogleApiClient.b(new zzmi.zza(paramGoogleApiClient)
    {
      protected void a(zzmj paramAnonymouszzmj)
        throws RemoteException
      {
        ((zzml)paramAnonymouszzmj.v()).a(new zzmh.zza(this));
      }
    });
  }
  
  private static class zza
    extends zzme
  {
    private final zza.zzb<Status> a;
    
    public zza(zza.zzb<Status> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void a(int paramInt)
      throws RemoteException
    {
      this.a.a(new Status(paramInt));
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzmh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */