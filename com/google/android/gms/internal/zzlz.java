package com.google.android.gms.internal;

import android.os.Binder;

public abstract class zzlz<T>
{
  private static final Object c = new Object();
  private static zza d = null;
  private static int e = 0;
  private static String f = "com.google.android.providers.gsf.permission.READ_GSERVICES";
  protected final String a;
  protected final T b;
  private T g = null;
  
  protected zzlz(String paramString, T paramT)
  {
    this.a = paramString;
    this.b = paramT;
  }
  
  public static int a()
  {
    return e;
  }
  
  public static zzlz<Float> a(String paramString, Float paramFloat)
  {
    new zzlz(paramString, paramFloat)
    {
      protected Float b(String paramAnonymousString)
      {
        return zzlz.e().a(this.a, (Float)this.b);
      }
    };
  }
  
  public static zzlz<Integer> a(String paramString, Integer paramInteger)
  {
    new zzlz(paramString, paramInteger)
    {
      protected Integer b(String paramAnonymousString)
      {
        return zzlz.e().a(this.a, (Integer)this.b);
      }
    };
  }
  
  public static zzlz<Long> a(String paramString, Long paramLong)
  {
    new zzlz(paramString, paramLong)
    {
      protected Long b(String paramAnonymousString)
      {
        return zzlz.e().a(this.a, (Long)this.b);
      }
    };
  }
  
  public static zzlz<String> a(String paramString1, String paramString2)
  {
    new zzlz(paramString1, paramString2)
    {
      protected String b(String paramAnonymousString)
      {
        return zzlz.e().a(this.a, (String)this.b);
      }
    };
  }
  
  public static zzlz<Boolean> a(String paramString, boolean paramBoolean)
  {
    new zzlz(paramString, Boolean.valueOf(paramBoolean))
    {
      protected Boolean b(String paramAnonymousString)
      {
        return zzlz.e().a(this.a, (Boolean)this.b);
      }
    };
  }
  
  public static boolean b()
  {
    return d != null;
  }
  
  protected abstract T a(String paramString);
  
  public final T c()
  {
    if (this.g != null) {
      return (T)this.g;
    }
    return (T)a(this.a);
  }
  
  public final T d()
  {
    long l = Binder.clearCallingIdentity();
    try
    {
      Object localObject1 = c();
      return (T)localObject1;
    }
    finally
    {
      Binder.restoreCallingIdentity(l);
    }
  }
  
  private static abstract interface zza
  {
    public abstract Boolean a(String paramString, Boolean paramBoolean);
    
    public abstract Float a(String paramString, Float paramFloat);
    
    public abstract Integer a(String paramString, Integer paramInteger);
    
    public abstract Long a(String paramString, Long paramLong);
    
    public abstract String a(String paramString1, String paramString2);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzlz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */