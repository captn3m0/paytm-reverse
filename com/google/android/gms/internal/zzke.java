package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.measurement.zze;
import java.util.HashMap;
import java.util.Map;

public final class zzke
  extends zze<zzke>
{
  private String a;
  private String b;
  private String c;
  private String d;
  private boolean e;
  private String f;
  private boolean g;
  private double h;
  
  public String a()
  {
    return this.a;
  }
  
  public void a(double paramDouble)
  {
    if ((paramDouble >= 0.0D) && (paramDouble <= 100.0D)) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.b(bool, "Sample rate must be between 0% and 100%");
      this.h = paramDouble;
      return;
    }
  }
  
  public void a(zzke paramzzke)
  {
    if (!TextUtils.isEmpty(this.a)) {
      paramzzke.a(this.a);
    }
    if (!TextUtils.isEmpty(this.b)) {
      paramzzke.b(this.b);
    }
    if (!TextUtils.isEmpty(this.c)) {
      paramzzke.c(this.c);
    }
    if (!TextUtils.isEmpty(this.d)) {
      paramzzke.d(this.d);
    }
    if (this.e) {
      paramzzke.a(true);
    }
    if (!TextUtils.isEmpty(this.f)) {
      paramzzke.e(this.f);
    }
    if (this.g) {
      paramzzke.b(this.g);
    }
    if (this.h != 0.0D) {
      paramzzke.a(this.h);
    }
  }
  
  public void a(String paramString)
  {
    this.a = paramString;
  }
  
  public void a(boolean paramBoolean)
  {
    this.e = paramBoolean;
  }
  
  public String b()
  {
    return this.b;
  }
  
  public void b(String paramString)
  {
    this.b = paramString;
  }
  
  public void b(boolean paramBoolean)
  {
    this.g = paramBoolean;
  }
  
  public String c()
  {
    return this.c;
  }
  
  public void c(String paramString)
  {
    this.c = paramString;
  }
  
  public String d()
  {
    return this.d;
  }
  
  public void d(String paramString)
  {
    this.d = paramString;
  }
  
  public void e(String paramString)
  {
    this.f = paramString;
  }
  
  public boolean e()
  {
    return this.e;
  }
  
  public String f()
  {
    return this.f;
  }
  
  public boolean g()
  {
    return this.g;
  }
  
  public double h()
  {
    return this.h;
  }
  
  public String toString()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("hitType", this.a);
    localHashMap.put("clientId", this.b);
    localHashMap.put("userId", this.c);
    localHashMap.put("androidAdId", this.d);
    localHashMap.put("AdTargetingEnabled", Boolean.valueOf(this.e));
    localHashMap.put("sessionControl", this.f);
    localHashMap.put("nonInteraction", Boolean.valueOf(this.g));
    localHashMap.put("sampleRate", Double.valueOf(this.h));
    return a(localHashMap);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzke.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */