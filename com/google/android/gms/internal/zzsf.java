package com.google.android.gms.internal;

import android.app.Activity;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.dynamic.zzc;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.dynamic.zzg;
import com.google.android.gms.dynamic.zzg.zza;
import com.google.android.gms.wallet.fragment.WalletFragmentOptions;

public class zzsf
  extends zzg<zzsa>
{
  private static zzsf a;
  
  protected zzsf()
  {
    super("com.google.android.gms.wallet.dynamite.WalletDynamiteCreatorImpl");
  }
  
  public static zzrx a(Activity paramActivity, zzc paramzzc, WalletFragmentOptions paramWalletFragmentOptions, zzry paramzzry)
    throws GooglePlayServicesNotAvailableException
  {
    int i = GooglePlayServicesUtil.a(paramActivity);
    if (i != 0) {
      throw new GooglePlayServicesNotAvailableException(i);
    }
    try
    {
      paramActivity = ((zzsa)a().a(paramActivity)).a(zze.a(paramActivity), paramzzc, paramWalletFragmentOptions, paramzzry);
      return paramActivity;
    }
    catch (RemoteException paramActivity)
    {
      throw new RuntimeException(paramActivity);
    }
    catch (zzg.zza paramActivity)
    {
      throw new RuntimeException(paramActivity);
    }
  }
  
  private static zzsf a()
  {
    if (a == null) {
      a = new zzsf();
    }
    return a;
  }
  
  protected zzsa a(IBinder paramIBinder)
  {
    return zzsa.zza.a(paramIBinder);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzsf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */