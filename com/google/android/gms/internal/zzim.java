package com.google.android.gms.internal;

import java.util.concurrent.Future;

@zzhb
public abstract class zzim
  implements zzit<Future>
{
  private final Runnable a = new Runnable()
  {
    public final void run()
    {
      zzim.a(zzim.this, Thread.currentThread());
      zzim.this.a();
    }
  };
  private volatile Thread b;
  private boolean c;
  
  public zzim()
  {
    this.c = false;
  }
  
  public zzim(boolean paramBoolean)
  {
    this.c = paramBoolean;
  }
  
  public abstract void a();
  
  public abstract void b();
  
  public final void cancel()
  {
    b();
    if (this.b != null) {
      this.b.interrupt();
    }
  }
  
  public final Future f()
  {
    if (this.c) {
      return zziq.a(1, this.a);
    }
    return zziq.a(this.a);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzim.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */