package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.os.SystemClock;
import android.text.TextUtils;
import com.google.ads.mediation.AdUrlAdapter;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAdOptions.Builder;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.dynamic.zze;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

@zzhb
public final class zzer
  implements zzes.zza
{
  private final String a;
  private final zzex b;
  private final long c;
  private final zzeo d;
  private final zzen e;
  private final AdRequestParcel f;
  private final AdSizeParcel g;
  private final Context h;
  private final Object i = new Object();
  private final VersionInfoParcel j;
  private final boolean k;
  private final NativeAdOptionsParcel l;
  private final List<String> m;
  private final boolean n;
  private zzey o;
  private int p = -2;
  private zzfa q;
  
  public zzer(Context paramContext, String paramString, zzex paramzzex, zzeo paramzzeo, zzen paramzzen, AdRequestParcel paramAdRequestParcel, AdSizeParcel paramAdSizeParcel, VersionInfoParcel paramVersionInfoParcel, boolean paramBoolean1, boolean paramBoolean2, NativeAdOptionsParcel paramNativeAdOptionsParcel, List<String> paramList)
  {
    this.h = paramContext;
    this.b = paramzzex;
    this.e = paramzzen;
    if ("com.google.ads.mediation.customevent.CustomEventAdapter".equals(paramString))
    {
      this.a = a();
      this.d = paramzzeo;
      if (paramzzeo.b == -1L) {
        break label136;
      }
    }
    label136:
    for (long l1 = paramzzeo.b;; l1 = 10000L)
    {
      this.c = l1;
      this.f = paramAdRequestParcel;
      this.g = paramAdSizeParcel;
      this.j = paramVersionInfoParcel;
      this.k = paramBoolean1;
      this.n = paramBoolean2;
      this.l = paramNativeAdOptionsParcel;
      this.m = paramList;
      return;
      this.a = paramString;
      break;
    }
  }
  
  private String a()
  {
    try
    {
      if (!TextUtils.isEmpty(this.e.e))
      {
        if (this.b.b(this.e.e)) {
          return "com.google.android.gms.ads.mediation.customevent.CustomEventAdapter";
        }
        return "com.google.ads.mediation.customevent.CustomEventAdapter";
      }
    }
    catch (RemoteException localRemoteException)
    {
      zzin.d("Fail to determine the custom event's version, assuming the old one.");
    }
    return "com.google.ads.mediation.customevent.CustomEventAdapter";
  }
  
  private String a(String paramString)
  {
    if ((paramString == null) || (!d()) || (b(2))) {
      return paramString;
    }
    try
    {
      Object localObject = new JSONObject(paramString);
      ((JSONObject)localObject).remove("cpm_floor_cents");
      localObject = ((JSONObject)localObject).toString();
      return (String)localObject;
    }
    catch (JSONException localJSONException)
    {
      zzin.d("Could not remove field. Returning the original value");
    }
    return paramString;
  }
  
  private void a(long paramLong1, long paramLong2, long paramLong3, long paramLong4)
  {
    for (;;)
    {
      if (this.p != -2) {
        return;
      }
      b(paramLong1, paramLong2, paramLong3, paramLong4);
    }
  }
  
  private void a(zzeq paramzzeq)
  {
    if ("com.google.ads.mediation.AdUrlAdapter".equals(this.a))
    {
      Bundle localBundle = this.f.m.getBundle(this.a);
      localObject = localBundle;
      if (localBundle == null) {
        localObject = new Bundle();
      }
      ((Bundle)localObject).putString("sdk_less_network_id", this.e.b);
      this.f.m.putBundle(this.a, (Bundle)localObject);
    }
    Object localObject = a(this.e.h);
    try
    {
      if (this.j.d < 4100000)
      {
        if (this.g.e)
        {
          this.o.a(zze.a(this.h), this.f, (String)localObject, paramzzeq);
          return;
        }
        this.o.a(zze.a(this.h), this.g, this.f, (String)localObject, paramzzeq);
        return;
      }
    }
    catch (RemoteException paramzzeq)
    {
      zzin.d("Could not request ad from mediation adapter.", paramzzeq);
      a(5);
      return;
    }
    if (this.k)
    {
      this.o.a(zze.a(this.h), this.f, (String)localObject, this.e.a, paramzzeq, this.l, this.m);
      return;
    }
    if (this.g.e)
    {
      this.o.a(zze.a(this.h), this.f, (String)localObject, this.e.a, paramzzeq);
      return;
    }
    if (this.n)
    {
      if (this.e.k != null)
      {
        this.o.a(zze.a(this.h), this.f, (String)localObject, this.e.a, paramzzeq, new NativeAdOptionsParcel(b(this.e.o)), this.e.n);
        return;
      }
      this.o.a(zze.a(this.h), this.g, this.f, (String)localObject, this.e.a, paramzzeq);
      return;
    }
    this.o.a(zze.a(this.h), this.g, this.f, (String)localObject, this.e.a, paramzzeq);
  }
  
  private static NativeAdOptions b(String paramString)
  {
    NativeAdOptions.Builder localBuilder = new NativeAdOptions.Builder();
    if (paramString == null) {
      return localBuilder.a();
    }
    try
    {
      paramString = new JSONObject(paramString);
      localBuilder.b(paramString.optBoolean("multiple_images", false));
      localBuilder.a(paramString.optBoolean("only_urls", false));
      localBuilder.a(c(paramString.optString("native_image_orientation", "any")));
      return localBuilder.a();
    }
    catch (JSONException paramString)
    {
      for (;;)
      {
        zzin.d("Exception occurred when creating native ad options", paramString);
      }
    }
  }
  
  private zzfa b()
  {
    if ((this.p != 0) || (!d())) {
      return null;
    }
    try
    {
      if ((b(4)) && (this.q != null) && (this.q.a() != 0))
      {
        zzfa localzzfa = this.q;
        return localzzfa;
      }
    }
    catch (RemoteException localRemoteException)
    {
      zzin.d("Could not get cpm value from MediationResponseMetadata");
    }
    return c(e());
  }
  
  private void b(long paramLong1, long paramLong2, long paramLong3, long paramLong4)
  {
    long l1 = SystemClock.elapsedRealtime();
    paramLong1 = paramLong2 - (l1 - paramLong1);
    paramLong2 = paramLong4 - (l1 - paramLong3);
    if ((paramLong1 <= 0L) || (paramLong2 <= 0L))
    {
      zzin.c("Timed out waiting for adapter.");
      this.p = 3;
      return;
    }
    try
    {
      this.i.wait(Math.min(paramLong1, paramLong2));
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
      this.p = -1;
    }
  }
  
  private boolean b(int paramInt)
  {
    boolean bool = false;
    for (;;)
    {
      try
      {
        Bundle localBundle;
        if (this.k)
        {
          localBundle = this.o.l();
          if (localBundle != null)
          {
            if ((localBundle.getInt("capabilities", 0) & paramInt) == paramInt) {
              bool = true;
            }
          }
          else {
            return bool;
          }
        }
        else
        {
          if (this.g.e)
          {
            localBundle = this.o.k();
            continue;
          }
          localBundle = this.o.j();
          continue;
        }
        bool = false;
      }
      catch (RemoteException localRemoteException)
      {
        zzin.d("Could not get adapter info. Returning false");
        return false;
      }
    }
  }
  
  private static int c(String paramString)
  {
    if ("landscape".equals(paramString)) {
      return 2;
    }
    if ("portrait".equals(paramString)) {
      return 1;
    }
    return 0;
  }
  
  private zzey c()
  {
    zzin.c("Instantiating mediation adapter: " + this.a);
    if ((((Boolean)zzbt.av.c()).booleanValue()) && ("com.google.ads.mediation.admob.AdMobAdapter".equals(this.a))) {
      return new zzfe(new AdMobAdapter());
    }
    if ((((Boolean)zzbt.aw.c()).booleanValue()) && ("com.google.ads.mediation.AdUrlAdapter".equals(this.a))) {
      return new zzfe(new AdUrlAdapter());
    }
    try
    {
      zzey localzzey = this.b.a(this.a);
      return localzzey;
    }
    catch (RemoteException localRemoteException)
    {
      zzin.a("Could not instantiate mediation adapter: " + this.a, localRemoteException);
    }
    return null;
  }
  
  private static zzfa c(int paramInt)
  {
    new zzfa.zza()
    {
      public int a()
        throws RemoteException
      {
        return this.a;
      }
    };
  }
  
  private boolean d()
  {
    return this.d.j != -1;
  }
  
  private int e()
  {
    int i2;
    if (this.e.h == null)
    {
      i2 = 0;
      return i2;
    }
    try
    {
      JSONObject localJSONObject = new JSONObject(this.e.h);
      if ("com.google.ads.mediation.admob.AdMobAdapter".equals(this.a)) {
        return localJSONObject.optInt("cpm_cents", 0);
      }
    }
    catch (JSONException localJSONException)
    {
      zzin.d("Could not convert to json. Returning 0");
      return 0;
    }
    if (b(2)) {}
    for (int i1 = localJSONException.optInt("cpm_floor_cents", 0);; i1 = 0)
    {
      i2 = i1;
      if (i1 != 0) {
        break;
      }
      return localJSONException.optInt("penalized_average_cpm_cents", 0);
    }
  }
  
  public zzes a(long paramLong1, long paramLong2)
  {
    synchronized (this.i)
    {
      long l1 = SystemClock.elapsedRealtime();
      final Object localObject2 = new zzeq();
      zzir.a.post(new Runnable()
      {
        public void run()
        {
          synchronized (zzer.a(zzer.this))
          {
            if (zzer.b(zzer.this) != -2) {
              return;
            }
            zzer.a(zzer.this, zzer.c(zzer.this));
            if (zzer.d(zzer.this) == null)
            {
              zzer.this.a(4);
              return;
            }
          }
          if ((zzer.e(zzer.this)) && (!zzer.a(zzer.this, 1)))
          {
            zzin.d("Ignoring adapter " + zzer.f(zzer.this) + " as delayed" + " impression is not supported");
            zzer.this.a(2);
            return;
          }
          localObject2.a(zzer.this);
          zzer.a(zzer.this, localObject2);
        }
      });
      a(l1, this.c, paramLong1, paramLong2);
      localObject2 = new zzes(this.e, this.o, this.a, (zzeq)localObject2, this.p, b());
      return (zzes)localObject2;
    }
  }
  
  public void a(int paramInt)
  {
    synchronized (this.i)
    {
      this.p = paramInt;
      this.i.notify();
      return;
    }
  }
  
  public void a(int paramInt, zzfa paramzzfa)
  {
    synchronized (this.i)
    {
      this.p = paramInt;
      this.q = paramzzfa;
      this.i.notify();
      return;
    }
  }
  
  public void cancel()
  {
    synchronized (this.i)
    {
      try
      {
        if (this.o != null) {
          this.o.c();
        }
        this.p = -1;
        this.i.notify();
        return;
      }
      catch (RemoteException localRemoteException)
      {
        for (;;)
        {
          zzin.d("Could not destroy mediation adapter.", localRemoteException);
        }
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */