package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Api.ApiOptions.Optional;

public final class zzro
  implements Api.ApiOptions.Optional
{
  public static final zzro a = new zza().a();
  private final boolean b;
  private final boolean c;
  private final String d;
  private final boolean e;
  private final String f;
  private final boolean g;
  
  private zzro(boolean paramBoolean1, boolean paramBoolean2, String paramString1, boolean paramBoolean3, String paramString2, boolean paramBoolean4)
  {
    this.b = paramBoolean1;
    this.c = paramBoolean2;
    this.d = paramString1;
    this.e = paramBoolean3;
    this.g = paramBoolean4;
    this.f = paramString2;
  }
  
  public boolean a()
  {
    return this.b;
  }
  
  public boolean b()
  {
    return this.c;
  }
  
  public String c()
  {
    return this.d;
  }
  
  public boolean d()
  {
    return this.e;
  }
  
  @Nullable
  public String e()
  {
    return this.f;
  }
  
  public boolean f()
  {
    return this.g;
  }
  
  public static final class zza
  {
    private boolean a;
    private boolean b;
    private String c;
    private boolean d;
    private String e;
    private boolean f;
    
    public zzro a()
    {
      return new zzro(this.a, this.b, this.c, this.d, this.e, this.f, null);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzro.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */