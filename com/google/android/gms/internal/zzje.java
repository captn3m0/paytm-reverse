package com.google.android.gms.internal;

import java.util.concurrent.TimeUnit;

@zzhb
public class zzje<T>
  implements zzjg<T>
{
  private final T a;
  private final zzjh b;
  
  public zzje(T paramT)
  {
    this.a = paramT;
    this.b = new zzjh();
    this.b.a();
  }
  
  public void a(Runnable paramRunnable)
  {
    this.b.a(paramRunnable);
  }
  
  public boolean cancel(boolean paramBoolean)
  {
    return false;
  }
  
  public T get()
  {
    return (T)this.a;
  }
  
  public T get(long paramLong, TimeUnit paramTimeUnit)
  {
    return (T)this.a;
  }
  
  public boolean isCancelled()
  {
    return false;
  }
  
  public boolean isDone()
  {
    return true;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzje.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */