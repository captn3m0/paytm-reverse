package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.KeyguardManager;
import android.content.Context;
import android.graphics.Rect;
import android.os.PowerManager;
import android.os.Process;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

@zzhb
@TargetApi(14)
public class zzbf
  extends Thread
{
  private boolean a = false;
  private boolean b = false;
  private boolean c = false;
  private final Object d;
  private final zzbe e;
  private final zzbd f;
  private final zzha g;
  private final int h;
  private final int i;
  private final int j;
  private final int k;
  private final int l;
  
  public zzbf(zzbe paramzzbe, zzbd paramzzbd, zzha paramzzha)
  {
    this.e = paramzzbe;
    this.f = paramzzbd;
    this.g = paramzzha;
    this.d = new Object();
    this.i = ((Integer)zzbt.K.c()).intValue();
    this.j = ((Integer)zzbt.L.c()).intValue();
    this.k = ((Integer)zzbt.M.c()).intValue();
    this.l = ((Integer)zzbt.N.c()).intValue();
    this.h = ((Integer)zzbt.O.c()).intValue();
    setName("ContentFetchTask");
  }
  
  zza a(View paramView, zzbc paramzzbc)
  {
    int m = 0;
    if (paramView == null) {
      return new zza(0, 0);
    }
    boolean bool = paramView.getGlobalVisibleRect(new Rect());
    if (((paramView instanceof TextView)) && (!(paramView instanceof EditText)))
    {
      paramView = ((TextView)paramView).getText();
      if (!TextUtils.isEmpty(paramView))
      {
        paramzzbc.b(paramView.toString(), bool);
        return new zza(1, 0);
      }
      return new zza(0, 0);
    }
    if (((paramView instanceof WebView)) && (!(paramView instanceof zzjp)))
    {
      paramzzbc.f();
      if (a((WebView)paramView, paramzzbc, bool)) {
        return new zza(0, 1);
      }
      return new zza(0, 0);
    }
    if ((paramView instanceof ViewGroup))
    {
      paramView = (ViewGroup)paramView;
      int n = 0;
      int i1 = 0;
      while (m < paramView.getChildCount())
      {
        zza localzza = a(paramView.getChildAt(m), paramzzbc);
        i1 += localzza.a;
        n += localzza.b;
        m += 1;
      }
      return new zza(i1, n);
    }
    return new zza(0, 0);
  }
  
  public void a()
  {
    synchronized (this.d)
    {
      if (this.a)
      {
        zzin.a("Content hash thread already started, quiting...");
        return;
      }
      this.a = true;
      start();
      return;
    }
  }
  
  void a(Activity paramActivity)
  {
    if (paramActivity == null) {}
    Object localObject1;
    do
    {
      return;
      Object localObject2 = null;
      localObject1 = localObject2;
      if (paramActivity.getWindow() != null)
      {
        localObject1 = localObject2;
        if (paramActivity.getWindow().getDecorView() != null) {
          localObject1 = paramActivity.getWindow().getDecorView().findViewById(16908290);
        }
      }
    } while (localObject1 == null);
    a((View)localObject1);
  }
  
  void a(zzbc paramzzbc, WebView paramWebView, String paramString, boolean paramBoolean)
  {
    paramzzbc.e();
    try
    {
      if (!TextUtils.isEmpty(paramString))
      {
        paramString = new JSONObject(paramString).optString("text");
        if (TextUtils.isEmpty(paramWebView.getTitle())) {
          break label84;
        }
        paramzzbc.a(paramWebView.getTitle() + "\n" + paramString, paramBoolean);
      }
      while (paramzzbc.a())
      {
        this.f.b(paramzzbc);
        return;
        label84:
        paramzzbc.a(paramString, paramBoolean);
      }
      return;
    }
    catch (JSONException paramzzbc)
    {
      zzin.a("Json string may be malformed.");
      return;
    }
    catch (Throwable paramzzbc)
    {
      zzin.a("Failed to get webview content.", paramzzbc);
      this.g.a(paramzzbc, true);
    }
  }
  
  boolean a(ActivityManager.RunningAppProcessInfo paramRunningAppProcessInfo)
  {
    return paramRunningAppProcessInfo.importance == 100;
  }
  
  boolean a(Context paramContext)
  {
    paramContext = (PowerManager)paramContext.getSystemService("power");
    if (paramContext == null) {
      return false;
    }
    return paramContext.isScreenOn();
  }
  
  boolean a(final View paramView)
  {
    if (paramView == null) {
      return false;
    }
    paramView.post(new Runnable()
    {
      public void run()
      {
        zzbf.this.b(paramView);
      }
    });
    return true;
  }
  
  @TargetApi(19)
  boolean a(final WebView paramWebView, final zzbc paramzzbc, final boolean paramBoolean)
  {
    if (!zzne.h()) {
      return false;
    }
    paramzzbc.f();
    paramWebView.post(new Runnable()
    {
      ValueCallback<String> a = new ValueCallback()
      {
        public void a(String paramAnonymous2String)
        {
          zzbf.this.a(zzbf.2.this.b, zzbf.2.this.c, paramAnonymous2String, zzbf.2.this.d);
        }
      };
      
      public void run()
      {
        if (paramWebView.getSettings().getJavaScriptEnabled()) {}
        try
        {
          paramWebView.evaluateJavascript("(function() { return  {text:document.body.innerText}})();", this.a);
          return;
        }
        catch (Throwable localThrowable)
        {
          this.a.onReceiveValue("");
        }
      }
    });
    return true;
  }
  
  void b(View paramView)
  {
    try
    {
      zzbc localzzbc = new zzbc(this.i, this.j, this.k, this.l);
      paramView = a(paramView, localzzbc);
      localzzbc.g();
      if ((paramView.a == 0) && (paramView.b == 0)) {
        return;
      }
      if (((paramView.b != 0) || (localzzbc.i() != 0)) && ((paramView.b != 0) || (!this.f.a(localzzbc))))
      {
        this.f.c(localzzbc);
        return;
      }
    }
    catch (Exception paramView)
    {
      zzin.b("Exception in fetchContentOnUIThread", paramView);
      this.g.a(paramView, true);
    }
  }
  
  boolean b()
  {
    try
    {
      Context localContext = this.e.b();
      if (localContext == null) {
        return false;
      }
      Object localObject = (ActivityManager)localContext.getSystemService("activity");
      KeyguardManager localKeyguardManager = (KeyguardManager)localContext.getSystemService("keyguard");
      if ((localObject != null) && (localKeyguardManager != null))
      {
        localObject = ((ActivityManager)localObject).getRunningAppProcesses();
        if (localObject == null) {
          return false;
        }
        localObject = ((List)localObject).iterator();
        while (((Iterator)localObject).hasNext())
        {
          ActivityManager.RunningAppProcessInfo localRunningAppProcessInfo = (ActivityManager.RunningAppProcessInfo)((Iterator)localObject).next();
          if (Process.myPid() == localRunningAppProcessInfo.pid) {
            if ((a(localRunningAppProcessInfo)) && (!localKeyguardManager.inKeyguardRestrictedInputMode()))
            {
              boolean bool = a(localContext);
              if (bool) {
                return true;
              }
            }
          }
        }
        return false;
      }
    }
    catch (Throwable localThrowable)
    {
      return false;
    }
    return false;
  }
  
  public zzbc c()
  {
    return this.f.a();
  }
  
  public void d()
  {
    synchronized (this.d)
    {
      this.b = false;
      this.d.notifyAll();
      zzin.a("ContentFetchThread: wakeup");
      return;
    }
  }
  
  public void e()
  {
    synchronized (this.d)
    {
      this.b = true;
      zzin.a("ContentFetchThread: paused, mPause = " + this.b);
      return;
    }
  }
  
  public boolean f()
  {
    return this.b;
  }
  
  public void run()
  {
    while (!this.c) {
      try
      {
        if (b())
        {
          Activity localActivity = this.e.a();
          if (localActivity == null) {
            zzin.a("ContentFetchThread: no activity");
          }
        }
      }
      catch (Throwable localThrowable)
      {
        zzin.b("Error in ContentFetchTask", localThrowable);
        this.g.a(localThrowable, true);
        synchronized (this.d)
        {
          for (;;)
          {
            boolean bool = this.b;
            if (!bool) {
              break;
            }
            try
            {
              zzin.a("ContentFetchTask: waiting");
              this.d.wait();
            }
            catch (InterruptedException localInterruptedException) {}
          }
          a((Activity)???);
          for (;;)
          {
            Thread.sleep(this.h * 1000);
            break;
            zzin.a("ContentFetchTask: sleeping");
            e();
          }
        }
      }
    }
  }
  
  @zzhb
  class zza
  {
    final int a;
    final int b;
    
    zza(int paramInt1, int paramInt2)
    {
      this.a = paramInt1;
      this.b = paramInt2;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzbf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */