package com.google.android.gms.internal;

import java.util.Map;

@zzhb
public final class zzda
  implements zzdf
{
  private final zzdb a;
  
  public zzda(zzdb paramzzdb)
  {
    this.a = paramzzdb;
  }
  
  public void a(zzjp paramzzjp, Map<String, String> paramMap)
  {
    paramzzjp = (String)paramMap.get("name");
    if (paramzzjp == null)
    {
      zzin.d("App event with no name parameter.");
      return;
    }
    this.a.a(paramzzjp, (String)paramMap.get("info"));
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzda.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */