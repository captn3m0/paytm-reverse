package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import com.google.android.gms.ads.internal.request.AdResponseParcel;

@zzhb
public abstract class zzgq
  extends zzim
{
  protected final zzgr.zza a;
  protected final Context b;
  protected final Object c = new Object();
  protected final Object d = new Object();
  protected final zzif.zza e;
  protected AdResponseParcel f;
  
  protected zzgq(Context paramContext, zzif.zza paramzza, zzgr.zza paramzza1)
  {
    super(true);
    this.b = paramContext;
    this.e = paramzza;
    this.f = paramzza.b;
    this.a = paramzza1;
  }
  
  protected abstract zzif a(int paramInt);
  
  public void a()
  {
    for (;;)
    {
      int i;
      synchronized (this.c)
      {
        zzin.a("AdRendererBackgroundTask started.");
        i = this.e.e;
        try
        {
          a(SystemClock.elapsedRealtime());
          final zzif localzzif = a(i);
          zzir.a.post(new Runnable()
          {
            public void run()
            {
              synchronized (zzgq.this.c)
              {
                zzgq.this.a(localzzif);
                return;
              }
            }
          });
          return;
        }
        catch (zza localzza)
        {
          i = localzza.a();
          if (i == 3) {
            continue;
          }
        }
        if (i == -1)
        {
          zzin.c(localzza.getMessage());
          if (this.f == null)
          {
            this.f = new AdResponseParcel(i);
            zzir.a.post(new Runnable()
            {
              public void run()
              {
                zzgq.this.b();
              }
            });
          }
        }
        else
        {
          zzin.d(localzza.getMessage());
        }
      }
      this.f = new AdResponseParcel(i, this.f.k);
    }
  }
  
  protected abstract void a(long paramLong)
    throws zzgq.zza;
  
  protected void a(zzif paramzzif)
  {
    this.a.b(paramzzif);
  }
  
  public void b() {}
  
  protected static final class zza
    extends Exception
  {
    private final int a;
    
    public zza(String paramString, int paramInt)
    {
      super();
      this.a = paramInt;
    }
    
    public int a()
    {
      return this.a;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzgq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */