package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzr;
import java.util.Map;

@zzhb
public final class zzdd
  implements zzdf
{
  private long a(long paramLong)
  {
    return paramLong - zzr.i().a() + zzr.i().b();
  }
  
  private void b(zzjp paramzzjp, Map<String, String> paramMap)
  {
    String str2 = (String)paramMap.get("label");
    String str1 = (String)paramMap.get("start_label");
    paramMap = (String)paramMap.get("timestamp");
    if (TextUtils.isEmpty(str2))
    {
      zzin.d("No label given for CSI tick.");
      return;
    }
    if (TextUtils.isEmpty(paramMap))
    {
      zzin.d("No timestamp given for CSI tick.");
      return;
    }
    try
    {
      long l = a(Long.parseLong(paramMap));
      paramMap = str1;
      if (TextUtils.isEmpty(str1)) {
        paramMap = "native:view_load";
      }
      paramzzjp.x().a(str2, paramMap, l);
      return;
    }
    catch (NumberFormatException paramzzjp)
    {
      zzin.d("Malformed timestamp for CSI tick.", paramzzjp);
    }
  }
  
  private void c(zzjp paramzzjp, Map<String, String> paramMap)
  {
    paramMap = (String)paramMap.get("value");
    if (TextUtils.isEmpty(paramMap))
    {
      zzin.d("No value given for CSI experiment.");
      return;
    }
    paramzzjp = paramzzjp.x().a();
    if (paramzzjp == null)
    {
      zzin.d("No ticker for WebView, dropping experiment ID.");
      return;
    }
    paramzzjp.a("e", paramMap);
  }
  
  private void d(zzjp paramzzjp, Map<String, String> paramMap)
  {
    String str = (String)paramMap.get("name");
    paramMap = (String)paramMap.get("value");
    if (TextUtils.isEmpty(paramMap))
    {
      zzin.d("No value given for CSI extra.");
      return;
    }
    if (TextUtils.isEmpty(str))
    {
      zzin.d("No name given for CSI extra.");
      return;
    }
    paramzzjp = paramzzjp.x().a();
    if (paramzzjp == null)
    {
      zzin.d("No ticker for WebView, dropping extra parameter.");
      return;
    }
    paramzzjp.a(str, paramMap);
  }
  
  public void a(zzjp paramzzjp, Map<String, String> paramMap)
  {
    String str = (String)paramMap.get("action");
    if ("tick".equals(str)) {
      b(paramzzjp, paramMap);
    }
    do
    {
      return;
      if ("experiment".equals(str))
      {
        c(paramzzjp, paramMap);
        return;
      }
    } while (!"extra".equals(str));
    d(paramzzjp, paramMap);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzdd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */