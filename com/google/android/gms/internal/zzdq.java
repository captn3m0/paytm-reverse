package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzr;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@zzhb
public class zzdq
  implements Iterable<zzdp>
{
  private final List<zzdp> a = new LinkedList();
  
  private zzdp c(zzjp paramzzjp)
  {
    Iterator localIterator = zzr.t().iterator();
    while (localIterator.hasNext())
    {
      zzdp localzzdp = (zzdp)localIterator.next();
      if (localzzdp.a == paramzzjp) {
        return localzzdp;
      }
    }
    return null;
  }
  
  public void a(zzdp paramzzdp)
  {
    this.a.add(paramzzdp);
  }
  
  public boolean a(zzjp paramzzjp)
  {
    paramzzjp = c(paramzzjp);
    if (paramzzjp != null)
    {
      paramzzjp.b.a();
      return true;
    }
    return false;
  }
  
  public void b(zzdp paramzzdp)
  {
    this.a.remove(paramzzdp);
  }
  
  public boolean b(zzjp paramzzjp)
  {
    return c(paramzzjp) != null;
  }
  
  public Iterator<zzdp> iterator()
  {
    return this.a.iterator();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzdq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */