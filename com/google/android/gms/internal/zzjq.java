package com.google.android.gms.internal;

import android.content.Context;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zza;
import com.google.android.gms.ads.internal.overlay.AdLauncherIntentInfoParcel;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.ads.internal.overlay.zzg;
import com.google.android.gms.ads.internal.overlay.zzp;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzr;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

@zzhb
public class zzjq
  extends WebViewClient
{
  private static final String[] b = { "UNKNOWN", "HOST_LOOKUP", "UNSUPPORTED_AUTH_SCHEME", "AUTHENTICATION", "PROXY_AUTHENTICATION", "CONNECT", "IO", "TIMEOUT", "REDIRECT_LOOP", "UNSUPPORTED_SCHEME", "FAILED_SSL_HANDSHAKE", "BAD_URL", "FILE", "FILE_NOT_FOUND", "TOO_MANY_REQUESTS" };
  private static final String[] c = { "NOT_YET_VALID", "EXPIRED", "ID_MISMATCH", "UNTRUSTED", "DATE_INVALID", "INVALID" };
  protected zzjp a;
  private final HashMap<String, List<zzdf>> d = new HashMap();
  private final Object e = new Object();
  private zza f;
  private zzg g;
  private zza h;
  private zzdb i;
  private zzb j;
  private boolean k = false;
  private zzdh l;
  private zzdj m;
  private boolean n;
  private boolean o;
  private zzp p;
  private final zzfr q;
  private com.google.android.gms.ads.internal.zze r;
  private zzfn s;
  private zzft t;
  private boolean u;
  private boolean v;
  private boolean w;
  private int x;
  
  public zzjq(zzjp paramzzjp, boolean paramBoolean)
  {
    this(paramzzjp, paramBoolean, new zzfr(paramzzjp, paramzzjp.g(), new zzbl(paramzzjp.getContext())), null);
  }
  
  zzjq(zzjp paramzzjp, boolean paramBoolean, zzfr paramzzfr, zzfn paramzzfn)
  {
    this.a = paramzzjp;
    this.n = paramBoolean;
    this.q = paramzzfr;
    this.s = paramzzfn;
  }
  
  private String a(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return "";
    }
    paramString = Uri.parse(paramString);
    if (paramString.getHost() != null) {
      return paramString.getHost();
    }
    return "";
  }
  
  private void a(Context paramContext, String paramString1, String paramString2, String paramString3)
  {
    if (!((Boolean)zzbt.ao.c()).booleanValue()) {
      return;
    }
    Bundle localBundle = new Bundle();
    localBundle.putString("err", paramString1);
    localBundle.putString("code", paramString2);
    localBundle.putString("host", a(paramString3));
    zzr.e().a(paramContext, this.a.o().b, "gmob-apps", localBundle, true);
  }
  
  private static boolean b(Uri paramUri)
  {
    paramUri = paramUri.getScheme();
    return ("http".equalsIgnoreCase(paramUri)) || ("https".equalsIgnoreCase(paramUri));
  }
  
  private void h()
  {
    synchronized (this.e)
    {
      this.o = true;
      this.x += 1;
      e();
      return;
    }
  }
  
  private void i()
  {
    this.x -= 1;
    e();
  }
  
  private void j()
  {
    this.w = true;
    e();
  }
  
  public com.google.android.gms.ads.internal.zze a()
  {
    return this.r;
  }
  
  public void a(int paramInt1, int paramInt2)
  {
    if (this.s != null) {
      this.s.c(paramInt1, paramInt2);
    }
  }
  
  public void a(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    this.q.a(paramInt1, paramInt2);
    if (this.s != null) {
      this.s.a(paramInt1, paramInt2, paramBoolean);
    }
  }
  
  public void a(Uri paramUri)
  {
    Object localObject2 = paramUri.getPath();
    Object localObject1 = (List)this.d.get(localObject2);
    if (localObject1 != null)
    {
      paramUri = zzr.e().a(paramUri);
      if (zzin.a(2))
      {
        zzin.e("Received GMSG: " + (String)localObject2);
        localObject2 = paramUri.keySet().iterator();
        while (((Iterator)localObject2).hasNext())
        {
          String str = (String)((Iterator)localObject2).next();
          zzin.e("  " + str + ": " + (String)paramUri.get(str));
        }
      }
      localObject1 = ((List)localObject1).iterator();
      while (((Iterator)localObject1).hasNext()) {
        ((zzdf)((Iterator)localObject1).next()).a(this.a, paramUri);
      }
    }
    zzin.e("No GMSG handler found for GMSG: " + paramUri);
  }
  
  public void a(zza paramzza, zzg paramzzg, zzdb paramzzdb, zzp paramzzp, boolean paramBoolean, zzdh paramzzdh, zzdj paramzzdj, com.google.android.gms.ads.internal.zze paramzze, zzft paramzzft)
  {
    com.google.android.gms.ads.internal.zze localzze = paramzze;
    if (paramzze == null) {
      localzze = new com.google.android.gms.ads.internal.zze(false);
    }
    this.s = new zzfn(this.a, paramzzft);
    a("/appEvent", new zzda(paramzzdb));
    a("/backButton", zzde.k);
    a("/canOpenURLs", zzde.b);
    a("/canOpenIntents", zzde.c);
    a("/click", zzde.d);
    a("/close", zzde.e);
    a("/customClose", zzde.g);
    a("/instrument", zzde.n);
    a("/delayPageLoaded", new zzd(null));
    a("/httpTrack", zzde.h);
    a("/log", zzde.i);
    a("/mraid", new zzdl(localzze, this.s));
    a("/mraidLoaded", this.q);
    a("/open", new zzdm(paramzzdh, localzze, this.s));
    a("/precache", zzde.m);
    a("/touch", zzde.j);
    a("/video", zzde.l);
    a("/appStreaming", zzde.f);
    if (paramzzdj != null) {
      a("/setInterstitialProperties", new zzdi(paramzzdj));
    }
    this.f = paramzza;
    this.g = paramzzg;
    this.i = paramzzdb;
    this.l = paramzzdh;
    this.p = paramzzp;
    this.r = localzze;
    this.t = paramzzft;
    this.m = paramzzdj;
    a(paramBoolean);
  }
  
  public final void a(AdLauncherIntentInfoParcel paramAdLauncherIntentInfoParcel)
  {
    zzg localzzg = null;
    boolean bool = this.a.p();
    zza localzza;
    if ((bool) && (!this.a.k().e))
    {
      localzza = null;
      if (!bool) {
        break label75;
      }
    }
    for (;;)
    {
      a(new AdOverlayInfoParcel(paramAdLauncherIntentInfoParcel, localzza, localzzg, this.p, this.a.o()));
      return;
      localzza = this.f;
      break;
      label75:
      localzzg = this.g;
    }
  }
  
  public void a(AdOverlayInfoParcel paramAdOverlayInfoParcel)
  {
    boolean bool2 = false;
    if (this.s != null) {}
    for (boolean bool1 = this.s.b();; bool1 = false)
    {
      com.google.android.gms.ads.internal.overlay.zze localzze = zzr.c();
      Context localContext = this.a.getContext();
      if (!bool1) {
        bool2 = true;
      }
      localzze.a(localContext, paramAdOverlayInfoParcel, bool2);
      return;
    }
  }
  
  public void a(zzjp paramzzjp)
  {
    this.a = paramzzjp;
  }
  
  public void a(zza paramzza)
  {
    this.h = paramzza;
  }
  
  public void a(zzb paramzzb)
  {
    this.j = paramzzb;
  }
  
  public void a(String paramString, zzdf paramzzdf)
  {
    synchronized (this.e)
    {
      List localList = (List)this.d.get(paramString);
      Object localObject1 = localList;
      if (localList == null)
      {
        localObject1 = new CopyOnWriteArrayList();
        this.d.put(paramString, localObject1);
      }
      ((List)localObject1).add(paramzzdf);
      return;
    }
  }
  
  public void a(boolean paramBoolean)
  {
    this.k = paramBoolean;
  }
  
  public final void a(boolean paramBoolean, int paramInt)
  {
    if ((this.a.p()) && (!this.a.k().e)) {}
    for (zza localzza = null;; localzza = this.f)
    {
      a(new AdOverlayInfoParcel(localzza, this.g, this.p, this.a, paramBoolean, paramInt, this.a.o()));
      return;
    }
  }
  
  public final void a(boolean paramBoolean, int paramInt, String paramString)
  {
    Object localObject = null;
    boolean bool = this.a.p();
    zza localzza;
    if ((bool) && (!this.a.k().e))
    {
      localzza = null;
      if (!bool) {
        break label95;
      }
    }
    for (;;)
    {
      a(new AdOverlayInfoParcel(localzza, (zzg)localObject, this.i, this.p, this.a, paramBoolean, paramInt, paramString, this.a.o(), this.l));
      return;
      localzza = this.f;
      break;
      label95:
      localObject = new zzc(this.a, this.g);
    }
  }
  
  public final void a(boolean paramBoolean, int paramInt, String paramString1, String paramString2)
  {
    boolean bool = this.a.p();
    zza localzza;
    if ((bool) && (!this.a.k().e))
    {
      localzza = null;
      if (!bool) {
        break label97;
      }
    }
    label97:
    for (Object localObject = null;; localObject = new zzc(this.a, this.g))
    {
      a(new AdOverlayInfoParcel(localzza, (zzg)localObject, this.i, this.p, this.a, paramBoolean, paramInt, paramString1, paramString2, this.a.o(), this.l));
      return;
      localzza = this.f;
      break;
    }
  }
  
  public void b(String paramString, zzdf paramzzdf)
  {
    synchronized (this.e)
    {
      paramString = (List)this.d.get(paramString);
      if (paramString == null) {
        return;
      }
      paramString.remove(paramzzdf);
      return;
    }
  }
  
  public boolean b()
  {
    synchronized (this.e)
    {
      boolean bool = this.n;
      return bool;
    }
  }
  
  public boolean c()
  {
    synchronized (this.e)
    {
      boolean bool = this.o;
      return bool;
    }
  }
  
  public void d()
  {
    synchronized (this.e)
    {
      zzin.e("Loading blank page in WebView, 2...");
      this.u = true;
      this.a.a("about:blank");
      return;
    }
  }
  
  public final void e()
  {
    zza localzza;
    zzjp localzzjp;
    if ((this.h != null) && (((this.v) && (this.x <= 0)) || (this.w)))
    {
      localzza = this.h;
      localzzjp = this.a;
      if (this.w) {
        break label70;
      }
    }
    label70:
    for (boolean bool = true;; bool = false)
    {
      localzza.a(localzzjp, bool);
      this.h = null;
      this.a.z();
      return;
    }
  }
  
  public final void f()
  {
    synchronized (this.e)
    {
      this.d.clear();
      this.f = null;
      this.g = null;
      this.h = null;
      this.i = null;
      this.k = false;
      this.n = false;
      this.o = false;
      this.l = null;
      this.p = null;
      this.j = null;
      if (this.s != null)
      {
        this.s.a(true);
        this.s = null;
      }
      return;
    }
  }
  
  public final void g()
  {
    synchronized (this.e)
    {
      this.k = false;
      this.n = true;
      zzir.a(new Runnable()
      {
        public void run()
        {
          zzjq.this.a.y();
          zzd localzzd = zzjq.this.a.i();
          if (localzzd != null) {
            localzzd.m();
          }
          if (zzjq.d(zzjq.this) != null)
          {
            zzjq.d(zzjq.this).a();
            zzjq.a(zzjq.this, null);
          }
        }
      });
      return;
    }
  }
  
  public final void onLoadResource(WebView paramWebView, String paramString)
  {
    zzin.e("Loading resource: " + paramString);
    paramWebView = Uri.parse(paramString);
    if (("gmsg".equalsIgnoreCase(paramWebView.getScheme())) && ("mobileads.google.com".equalsIgnoreCase(paramWebView.getHost()))) {
      a(paramWebView);
    }
  }
  
  public final void onPageFinished(WebView arg1, String paramString)
  {
    synchronized (this.e)
    {
      if (this.u)
      {
        zzin.e("Blank page loaded, 1...");
        this.a.s();
        return;
      }
      this.v = true;
      e();
      return;
    }
  }
  
  public final void onReceivedError(WebView paramWebView, int paramInt, String paramString1, String paramString2)
  {
    if ((paramInt < 0) && (-paramInt - 1 < b.length)) {}
    for (String str = b[(-paramInt - 1)];; str = String.valueOf(paramInt))
    {
      a(this.a.getContext(), "http_err", str, paramString2);
      super.onReceivedError(paramWebView, paramInt, paramString1, paramString2);
      return;
    }
  }
  
  public final void onReceivedSslError(WebView paramWebView, SslErrorHandler paramSslErrorHandler, SslError paramSslError)
  {
    int i1;
    if (paramSslError != null)
    {
      i1 = paramSslError.getPrimaryError();
      if ((i1 < 0) || (i1 >= c.length)) {
        break label65;
      }
    }
    label65:
    for (String str = c[i1];; str = String.valueOf(i1))
    {
      a(this.a.getContext(), "ssl_err", str, zzr.g().a(paramSslError));
      super.onReceivedSslError(paramWebView, paramSslErrorHandler, paramSslError);
      return;
    }
  }
  
  public boolean shouldOverrideKeyEvent(WebView paramWebView, KeyEvent paramKeyEvent)
  {
    switch (paramKeyEvent.getKeyCode())
    {
    default: 
      return false;
    }
    return true;
  }
  
  public final boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    zzin.e("AdWebView shouldOverrideUrlLoading: " + paramString);
    Uri localUri = Uri.parse(paramString);
    if (("gmsg".equalsIgnoreCase(localUri.getScheme())) && ("mobileads.google.com".equalsIgnoreCase(localUri.getHost()))) {
      a(localUri);
    }
    for (;;)
    {
      return true;
      if ((this.k) && (paramWebView == this.a.a()) && (b(localUri)))
      {
        if ((this.f != null) && (((Boolean)zzbt.W.c()).booleanValue()))
        {
          this.f.e();
          this.f = null;
        }
        return super.shouldOverrideUrlLoading(paramWebView, paramString);
      }
      if (!this.a.a().willNotDraw())
      {
        try
        {
          zzan localzzan = this.a.n();
          paramWebView = localUri;
          if (localzzan != null)
          {
            paramWebView = localUri;
            if (localzzan.b(localUri)) {
              paramWebView = localzzan.a(localUri, this.a.getContext());
            }
          }
        }
        catch (zzao paramWebView)
        {
          for (;;)
          {
            zzin.d("Unable to append parameter to URL: " + paramString);
            paramWebView = localUri;
          }
          this.r.a(paramString);
        }
        if ((this.r == null) || (this.r.b())) {
          a(new AdLauncherIntentInfoParcel("android.intent.action.VIEW", paramWebView.toString(), null, null, null, null, null));
        }
      }
      else
      {
        zzin.d("AdWebView unable to handle URL: " + paramString);
      }
    }
  }
  
  public static abstract interface zza
  {
    public abstract void a(zzjp paramzzjp, boolean paramBoolean);
  }
  
  public static abstract interface zzb
  {
    public abstract void a();
  }
  
  private static class zzc
    implements zzg
  {
    private zzjp a;
    private zzg b;
    
    public zzc(zzjp paramzzjp, zzg paramzzg)
    {
      this.a = paramzzjp;
      this.b = paramzzg;
    }
    
    public void c_()
    {
      this.b.c_();
      this.a.c();
    }
    
    public void d_()
    {
      this.b.d_();
      this.a.d();
    }
    
    public void e_() {}
    
    public void g() {}
  }
  
  private class zzd
    implements zzdf
  {
    private zzd() {}
    
    public void a(zzjp paramzzjp, Map<String, String> paramMap)
    {
      if (paramMap.keySet().contains("start")) {
        zzjq.a(zzjq.this);
      }
      do
      {
        return;
        if (paramMap.keySet().contains("stop"))
        {
          zzjq.b(zzjq.this);
          return;
        }
      } while (!paramMap.keySet().contains("cancel"));
      zzjq.c(zzjq.this);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzjq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */