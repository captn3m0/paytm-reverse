package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.client.zza;
import com.google.android.gms.ads.internal.overlay.zzg;
import com.google.android.gms.ads.internal.overlay.zzp;
import com.google.android.gms.ads.internal.zze;

public abstract interface zzed
  extends zzeh
{
  public abstract void a();
  
  public abstract void a(zza paramzza, zzg paramzzg, zzdb paramzzdb, zzp paramzzp, boolean paramBoolean, zzdh paramzzdh, zzdj paramzzdj, zze paramzze, zzft paramzzft);
  
  public abstract void a(zza paramzza);
  
  public abstract void a(String paramString);
  
  public abstract zzei b();
  
  public abstract void b(String paramString);
  
  public abstract void c(String paramString);
  
  public static abstract interface zza
  {
    public abstract void a();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzed.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */