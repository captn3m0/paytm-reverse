package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zzb;
import com.google.android.gms.common.api.internal.zzq;
import com.google.android.gms.common.api.internal.zzq.zzb;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.nearby.connection.AppMetadata;
import com.google.android.gms.nearby.connection.Connections.ConnectionRequestListener;
import com.google.android.gms.nearby.connection.Connections.ConnectionResponseCallback;
import com.google.android.gms.nearby.connection.Connections.EndpointDiscoveryListener;
import com.google.android.gms.nearby.connection.Connections.MessageListener;
import com.google.android.gms.nearby.connection.Connections.StartAdvertisingResult;

public final class zzqk
  extends zzj<zzqn>
{
  private final long a = hashCode();
  
  public zzqk(Context paramContext, Looper paramLooper, zzf paramzzf, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramContext, paramLooper, 54, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
  }
  
  protected zzqn a(IBinder paramIBinder)
  {
    return zzqn.zza.a(paramIBinder);
  }
  
  protected String a()
  {
    return "com.google.android.gms.nearby.connection.service.START";
  }
  
  public void a(zza.zzb<Status> paramzzb, String paramString)
    throws RemoteException
  {
    ((zzqn)v()).a(new zzc(paramzzb), paramString, this.a);
  }
  
  public void a(zza.zzb<Status> paramzzb, String paramString, long paramLong, zzq<Connections.EndpointDiscoveryListener> paramzzq)
    throws RemoteException
  {
    ((zzqn)v()).a(new zzg(paramzzb, paramzzq), paramString, paramLong, this.a);
  }
  
  public void a(zza.zzb<Connections.StartAdvertisingResult> paramzzb, String paramString, AppMetadata paramAppMetadata, long paramLong, zzq<Connections.ConnectionRequestListener> paramzzq)
    throws RemoteException
  {
    ((zzqn)v()).a(new zze(paramzzb, paramzzq), paramString, paramAppMetadata, paramLong, this.a);
  }
  
  public void a(zza.zzb<Status> paramzzb, String paramString1, String paramString2, byte[] paramArrayOfByte, zzq<Connections.ConnectionResponseCallback> paramzzq, zzq<Connections.MessageListener> paramzzq1)
    throws RemoteException
  {
    ((zzqn)v()).a(new zzd(paramzzb, paramzzq, paramzzq1), paramString1, paramString2, paramArrayOfByte, this.a);
  }
  
  public void a(zza.zzb<Status> paramzzb, String paramString, byte[] paramArrayOfByte, zzq<Connections.MessageListener> paramzzq)
    throws RemoteException
  {
    ((zzqn)v()).a(new zza(paramzzb, paramzzq), paramString, paramArrayOfByte, this.a);
  }
  
  protected String b()
  {
    return "com.google.android.gms.nearby.internal.connection.INearbyConnectionService";
  }
  
  public void f()
  {
    if (k()) {}
    try
    {
      ((zzqn)v()).d(this.a);
      super.f();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        Log.w("NearbyConnectionsClient", "Failed to notify client disconnect.", localRemoteException);
      }
    }
  }
  
  private static final class zza
    extends zzqk.zzb
  {
    private final zza.zzb<Status> a;
    
    public zza(zza.zzb<Status> paramzzb, zzq<Connections.MessageListener> paramzzq)
    {
      super();
      this.a = ((zza.zzb)zzx.a(paramzzb));
    }
    
    public void d(int paramInt)
      throws RemoteException
    {
      this.a.a(new Status(paramInt));
    }
  }
  
  private static class zzb
    extends zzqj
  {
    private final zzq<Connections.MessageListener> a;
    
    zzb(zzq<Connections.MessageListener> paramzzq)
    {
      this.a = paramzzq;
    }
    
    public void a(final String paramString, final byte[] paramArrayOfByte, final boolean paramBoolean)
      throws RemoteException
    {
      this.a.a(new zzq.zzb()
      {
        public void a() {}
        
        public void a(Connections.MessageListener paramAnonymousMessageListener)
        {
          paramAnonymousMessageListener.a(paramString, paramArrayOfByte, paramBoolean);
        }
      });
    }
    
    public void c(final String paramString)
      throws RemoteException
    {
      this.a.a(new zzq.zzb()
      {
        public void a() {}
        
        public void a(Connections.MessageListener paramAnonymousMessageListener)
        {
          paramAnonymousMessageListener.a(paramString);
        }
      });
    }
  }
  
  private static class zzc
    extends zzqj
  {
    private final zza.zzb<Status> a;
    
    zzc(zza.zzb<Status> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void e(int paramInt)
      throws RemoteException
    {
      this.a.a(new Status(paramInt));
    }
  }
  
  private static final class zzd
    extends zzqk.zzb
  {
    private final zza.zzb<Status> a;
    private final zzq<Connections.ConnectionResponseCallback> b;
    
    public zzd(zza.zzb<Status> paramzzb, zzq<Connections.ConnectionResponseCallback> paramzzq, zzq<Connections.MessageListener> paramzzq1)
    {
      super();
      this.a = ((zza.zzb)zzx.a(paramzzb));
      this.b = ((zzq)zzx.a(paramzzq));
    }
    
    public void a(final String paramString, final int paramInt, final byte[] paramArrayOfByte)
      throws RemoteException
    {
      this.b.a(new zzq.zzb()
      {
        public void a() {}
        
        public void a(Connections.ConnectionResponseCallback paramAnonymousConnectionResponseCallback)
        {
          paramAnonymousConnectionResponseCallback.a(paramString, new Status(paramInt), paramArrayOfByte);
        }
      });
    }
    
    public void c(int paramInt)
      throws RemoteException
    {
      this.a.a(new Status(paramInt));
    }
  }
  
  private static final class zze
    extends zzqj
  {
    private final zza.zzb<Connections.StartAdvertisingResult> a;
    private final zzq<Connections.ConnectionRequestListener> b;
    
    zze(zza.zzb<Connections.StartAdvertisingResult> paramzzb, zzq<Connections.ConnectionRequestListener> paramzzq)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb));
      this.b = ((zzq)zzx.a(paramzzq));
    }
    
    public void a(int paramInt, String paramString)
      throws RemoteException
    {
      this.a.a(new zzqk.zzf(new Status(paramInt), paramString));
    }
    
    public void a(final String paramString1, final String paramString2, final String paramString3, final byte[] paramArrayOfByte)
      throws RemoteException
    {
      this.b.a(new zzq.zzb()
      {
        public void a() {}
        
        public void a(Connections.ConnectionRequestListener paramAnonymousConnectionRequestListener)
        {
          paramAnonymousConnectionRequestListener.a(paramString1, paramString2, paramString3, paramArrayOfByte);
        }
      });
    }
  }
  
  private static final class zzf
    implements Connections.StartAdvertisingResult
  {
    private final Status a;
    private final String b;
    
    zzf(Status paramStatus, String paramString)
    {
      this.a = paramStatus;
      this.b = paramString;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
  
  private static final class zzg
    extends zzqj
  {
    private final zza.zzb<Status> a;
    private final zzq<Connections.EndpointDiscoveryListener> b;
    
    zzg(zza.zzb<Status> paramzzb, zzq<Connections.EndpointDiscoveryListener> paramzzq)
    {
      this.a = ((zza.zzb)zzx.a(paramzzb));
      this.b = ((zzq)zzx.a(paramzzq));
    }
    
    public void a(int paramInt)
      throws RemoteException
    {
      this.a.a(new Status(paramInt));
    }
    
    public void a(final String paramString)
      throws RemoteException
    {
      this.b.a(new zzq.zzb()
      {
        public void a() {}
        
        public void a(Connections.EndpointDiscoveryListener paramAnonymousEndpointDiscoveryListener)
        {
          paramAnonymousEndpointDiscoveryListener.a(paramString);
        }
      });
    }
    
    public void a(final String paramString1, final String paramString2, final String paramString3, final String paramString4)
      throws RemoteException
    {
      this.b.a(new zzq.zzb()
      {
        public void a() {}
        
        public void a(Connections.EndpointDiscoveryListener paramAnonymousEndpointDiscoveryListener)
        {
          paramAnonymousEndpointDiscoveryListener.a(paramString1, paramString2, paramString3, paramString4);
        }
      });
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzqk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */