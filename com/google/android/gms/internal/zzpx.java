package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.measurement.zze;
import java.util.HashMap;
import java.util.Map;

public final class zzpx
  extends zze<zzpx>
{
  public String a;
  public String b;
  public String c;
  
  public String a()
  {
    return this.a;
  }
  
  public void a(zzpx paramzzpx)
  {
    if (!TextUtils.isEmpty(this.a)) {
      paramzzpx.a(this.a);
    }
    if (!TextUtils.isEmpty(this.b)) {
      paramzzpx.b(this.b);
    }
    if (!TextUtils.isEmpty(this.c)) {
      paramzzpx.c(this.c);
    }
  }
  
  public void a(String paramString)
  {
    this.a = paramString;
  }
  
  public String b()
  {
    return this.b;
  }
  
  public void b(String paramString)
  {
    this.b = paramString;
  }
  
  public String c()
  {
    return this.c;
  }
  
  public void c(String paramString)
  {
    this.c = paramString;
  }
  
  public String toString()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("network", this.a);
    localHashMap.put("action", this.b);
    localHashMap.put("target", this.c);
    return a(localHashMap);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzpx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */