package com.google.android.gms.internal;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class zznu
{
  public static final zzsy.zza A;
  public static final zzsy.zza B;
  public static final zzsy.zza C;
  public static final zzsy.zza D;
  public static final Set<String> E;
  public static final zzsy.zza F;
  public static final zzsy.zza G;
  public static final zzsy.zza H;
  public static final zzsy.zza I;
  public static final zzsy.zza J;
  public static final zzsy.zza K;
  public static final zzsy.zza L;
  public static final zzsy.zza M;
  public static final zzsy.zza N;
  public static final zzsy.zza O;
  public static final zzsy.zza P;
  public static final zzsy.zza Q;
  public static final zzsy.zza R;
  public static final zzsy.zza S;
  public static final zzsy.zza T;
  public static final zzsy.zza U;
  public static final zzsy.zza V;
  public static final String[] W;
  private static final Map<String, List<zzsy.zza>> X;
  private static final Map<zzsy.zza, zza> Y;
  public static final zzsy.zza a = a("com.google.step_count.delta", new zzsy.zzb[] { zznt.d });
  public static final zzsy.zza b = a("com.google.step_count.cumulative", new zzsy.zzb[] { zznt.d });
  public static final zzsy.zza c = a("com.google.step_count.cadence", new zzsy.zzb[] { zznt.v });
  public static final zzsy.zza d = a("com.google.activity.segment", new zzsy.zzb[] { zznt.a });
  public static final zzsy.zza e = a("com.google.floor_change", new zzsy.zzb[] { zznt.a, zznt.b, zznt.C, zznt.F });
  public static final zzsy.zza f = a("com.google.calories.consumed", new zzsy.zzb[] { zznt.x });
  public static final zzsy.zza g = a("com.google.calories.expended", new zzsy.zzb[] { zznt.x });
  public static final zzsy.zza h = a("com.google.calories.bmr", new zzsy.zzb[] { zznt.x });
  public static final zzsy.zza i = a("com.google.power.sample", new zzsy.zzb[] { zznt.y });
  public static final zzsy.zza j = a("com.google.activity.sample", new zzsy.zzb[] { zznt.a, zznt.b });
  public static final zzsy.zza k = a("com.google.accelerometer", new zzsy.zzb[] { zznt.U, zznt.V, zznt.W });
  public static final zzsy.zza l = a("com.google.sensor.events", new zzsy.zzb[] { zznt.Z, zznt.X, zznt.Y });
  public static final zzsy.zza m = a("com.google.internal.goal", new zzsy.zzb[] { zznt.o });
  public static final zzsy.zza n = a("com.google.heart_rate.bpm", new zzsy.zzb[] { zznt.i });
  public static final zzsy.zza o = a("com.google.location.sample", new zzsy.zzb[] { zznt.j, zznt.k, zznt.l, zznt.m });
  public static final zzsy.zza p = a("com.google.location.track", new zzsy.zzb[] { zznt.j, zznt.k, zznt.l, zznt.m });
  public static final zzsy.zza q = a("com.google.distance.delta", new zzsy.zzb[] { zznt.n });
  public static final zzsy.zza r = a("com.google.distance.cumulative", new zzsy.zzb[] { zznt.n });
  public static final zzsy.zza s = a("com.google.speed", new zzsy.zzb[] { zznt.u });
  public static final zzsy.zza t = a("com.google.cycling.wheel_revolution.cumulative", new zzsy.zzb[] { zznt.w });
  public static final zzsy.zza u = a("com.google.cycling.wheel_revolution.rpm", new zzsy.zzb[] { zznt.v });
  public static final zzsy.zza v = a("com.google.cycling.pedaling.cumulative", new zzsy.zzb[] { zznt.w });
  public static final zzsy.zza w = a("com.google.cycling.pedaling.cadence", new zzsy.zzb[] { zznt.v });
  public static final zzsy.zza x = a("com.google.height", new zzsy.zzb[] { zznt.q });
  public static final zzsy.zza y = a("com.google.weight", new zzsy.zzb[] { zznt.r });
  public static final zzsy.zza z = a("com.google.body.fat.percentage", new zzsy.zzb[] { zznt.t });
  
  static
  {
    A = a("com.google.body.waist.circumference", new zzsy.zzb[] { zznt.s });
    B = a("com.google.body.hip.circumference", new zzsy.zzb[] { zznt.s });
    C = a("com.google.nutrition", new zzsy.zzb[] { zznt.B, zznt.z, zznt.A });
    D = a("com.google.activity.exercise", new zzsy.zzb[] { zznt.I, zznt.J, zznt.e, zznt.L, zznt.K });
    E = Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[] { d.a, f.a, g.a, q.a, e.a, n.a, o.a, C.a, s.a, a.a, y.a })));
    F = a("com.google.activity.summary", new zzsy.zzb[] { zznt.a, zznt.e, zznt.M });
    G = a("com.google.floor_change.summary", new zzsy.zzb[] { zznt.g, zznt.h, zznt.D, zznt.E, zznt.G, zznt.H });
    H = a;
    I = q;
    J = f;
    K = g;
    L = a("com.google.heart_rate.summary", new zzsy.zzb[] { zznt.N, zznt.O, zznt.P });
    M = a("com.google.location.bounding_box", new zzsy.zzb[] { zznt.Q, zznt.R, zznt.S, zznt.T });
    N = a("com.google.power.summary", new zzsy.zzb[] { zznt.N, zznt.O, zznt.P });
    O = a("com.google.speed.summary", new zzsy.zzb[] { zznt.N, zznt.O, zznt.P });
    P = a("com.google.weight.summary", new zzsy.zzb[] { zznt.N, zznt.O, zznt.P });
    Q = a("com.google.calories.bmr.summary", new zzsy.zzb[] { zznt.N, zznt.O, zznt.P });
    R = a("com.google.body.fat.percentage.summary", new zzsy.zzb[] { zznt.N, zznt.O, zznt.P });
    S = a("com.google.body.hip.circumference.summary", new zzsy.zzb[] { zznt.N, zznt.O, zznt.P });
    T = a("com.google.body.waist.circumference.summary", new zzsy.zzb[] { zznt.N, zznt.O, zznt.P });
    U = a("com.google.nutrition.summary", new zzsy.zzb[] { zznt.B, zznt.z });
    V = a("com.google.internal.session", new zzsy.zzb[] { zznt.aa, zznt.a, zznt.ab, zznt.ac, zznt.ad });
    X = a();
    W = new String[] { "com.google.accelerometer", "com.google.activity.exercise", "com.google.activity.sample", "com.google.activity.segment", "com.google.activity.summary", "com.google.body.fat.percentage", "com.google.body.fat.percentage.summary", "com.google.body.hip.circumference", "com.google.body.hip.circumference.summary", "com.google.body.waist.circumference", "com.google.body.waist.circumference.summary", "com.google.calories.bmr", "com.google.calories.bmr.summary", "com.google.calories.consumed", "com.google.calories.expended", "com.google.cycling.pedaling.cadence", "com.google.cycling.pedaling.cumulative", "com.google.cycling.wheel_revolution.cumulative", "com.google.cycling.wheel_revolution.rpm", "com.google.distance.cumulative", "com.google.distance.delta", "com.google.floor_change", "com.google.floor_change.summary", "com.google.heart_rate.bpm", "com.google.heart_rate.summary", "com.google.height", "com.google.internal.goal", "com.google.internal.session", "com.google.location.bounding_box", "com.google.location.sample", "com.google.location.track", "com.google.nutrition", "com.google.nutrition.summary", "com.google.power.sample", "com.google.power.summary", "com.google.sensor.events", "com.google.speed", "com.google.speed.summary", "com.google.step_count.cadence", "com.google.step_count.cumulative", "com.google.step_count.delta", "com.google.weight", "com.google.weight.summary" };
    HashSet localHashSet1 = new HashSet();
    localHashSet1.add(b);
    localHashSet1.add(r);
    localHashSet1.add(v);
    HashSet localHashSet2 = new HashSet();
    localHashSet2.add(q);
    localHashSet2.add(a);
    localHashSet2.add(g);
    localHashSet2.add(f);
    localHashSet2.add(e);
    HashSet localHashSet3 = new HashSet();
    localHashSet3.add(z);
    localHashSet3.add(B);
    localHashSet3.add(A);
    localHashSet3.add(C);
    localHashSet3.add(x);
    localHashSet3.add(y);
    localHashSet3.add(n);
    HashMap localHashMap = new HashMap();
    a(localHashMap, localHashSet1, zza.a);
    a(localHashMap, localHashSet2, zza.b);
    a(localHashMap, localHashSet3, zza.c);
    Y = Collections.unmodifiableMap(localHashMap);
  }
  
  public static zzsy.zza a(String paramString, zzsy.zzb... paramVarArgs)
  {
    zzsy.zza localzza = new zzsy.zza();
    localzza.a = paramString;
    localzza.b = paramVarArgs;
    return localzza;
  }
  
  private static Map<String, List<zzsy.zza>> a()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put(d.a, Collections.singletonList(F));
    localHashMap.put(f.a, Collections.singletonList(J));
    localHashMap.put(g.a, Collections.singletonList(K));
    localHashMap.put(q.a, Collections.singletonList(I));
    localHashMap.put(e.a, Collections.singletonList(G));
    localHashMap.put(o.a, Collections.singletonList(M));
    localHashMap.put(i.a, Collections.singletonList(N));
    localHashMap.put(n.a, Collections.singletonList(L));
    localHashMap.put(s.a, Collections.singletonList(O));
    localHashMap.put(a.a, Collections.singletonList(H));
    localHashMap.put(y.a, Collections.singletonList(P));
    return localHashMap;
  }
  
  private static void a(Map<zzsy.zza, zza> paramMap, Collection<zzsy.zza> paramCollection, zza paramzza)
  {
    paramCollection = paramCollection.iterator();
    while (paramCollection.hasNext()) {
      paramMap.put((zzsy.zza)paramCollection.next(), paramzza);
    }
  }
  
  public static enum zza
  {
    private zza() {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zznu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */