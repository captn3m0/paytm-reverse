package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.safetynet.AttestationData;
import com.google.android.gms.safetynet.SafeBrowsingData;

public class zzra
  extends zzrc.zza
{
  public void a(Status paramStatus, AttestationData paramAttestationData)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(Status paramStatus, SafeBrowsingData paramSafeBrowsingData)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(String paramString)
    throws RemoteException
  {
    throw new UnsupportedOperationException();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzra.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */