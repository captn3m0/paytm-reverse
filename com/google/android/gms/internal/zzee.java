package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import java.util.concurrent.Future;

@zzhb
public class zzee
{
  private zzed a(Context paramContext, VersionInfoParcel paramVersionInfoParcel, final zza<zzed> paramzza, zzan paramzzan)
  {
    paramContext = new zzef(paramContext, paramVersionInfoParcel, paramzzan);
    paramzza.a = paramContext;
    paramContext.a(new zzed.zza()
    {
      public void a()
      {
        paramzza.b(paramzza.a);
      }
    });
    return paramContext;
  }
  
  public Future<zzed> a(final Context paramContext, final VersionInfoParcel paramVersionInfoParcel, final String paramString, final zzan paramzzan)
  {
    final zza localzza = new zza(null);
    zzir.a.post(new Runnable()
    {
      public void run()
      {
        zzee.a(zzee.this, paramContext, paramVersionInfoParcel, localzza, paramzzan).b(paramString);
      }
    });
    return localzza;
  }
  
  private static class zza<JavascriptEngine>
    extends zzjd<JavascriptEngine>
  {
    JavascriptEngine a;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzee.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */