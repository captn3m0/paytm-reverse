package com.google.android.gms.internal;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.auth.api.proxy.ProxyApi.ProxyResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zza;

abstract class zzlc
  extends zza.zza<ProxyApi.ProxyResult, zzkz>
{
  protected ProxyApi.ProxyResult a(Status paramStatus)
  {
    return new zzle(paramStatus);
  }
  
  protected abstract void a(Context paramContext, zzlb paramzzlb)
    throws RemoteException;
  
  protected final void a(zzkz paramzzkz)
    throws RemoteException
  {
    a(paramzzkz.q(), (zzlb)paramzzkz.v());
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzlc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */