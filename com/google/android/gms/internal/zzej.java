package com.google.android.gms.internal;

import java.util.AbstractMap.SimpleEntry;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONObject;

@zzhb
public class zzej
  implements zzei
{
  private final zzeh a;
  private final HashSet<AbstractMap.SimpleEntry<String, zzdf>> b;
  
  public zzej(zzeh paramzzeh)
  {
    this.a = paramzzeh;
    this.b = new HashSet();
  }
  
  public void a()
  {
    Iterator localIterator = this.b.iterator();
    while (localIterator.hasNext())
    {
      AbstractMap.SimpleEntry localSimpleEntry = (AbstractMap.SimpleEntry)localIterator.next();
      zzin.e("Unregistering eventhandler: " + ((zzdf)localSimpleEntry.getValue()).toString());
      this.a.b((String)localSimpleEntry.getKey(), (zzdf)localSimpleEntry.getValue());
    }
    this.b.clear();
  }
  
  public void a(String paramString, zzdf paramzzdf)
  {
    this.a.a(paramString, paramzzdf);
    this.b.add(new AbstractMap.SimpleEntry(paramString, paramzzdf));
  }
  
  public void a(String paramString1, String paramString2)
  {
    this.a.a(paramString1, paramString2);
  }
  
  public void a(String paramString, JSONObject paramJSONObject)
  {
    this.a.a(paramString, paramJSONObject);
  }
  
  public void b(String paramString, zzdf paramzzdf)
  {
    this.a.b(paramString, paramzzdf);
    this.b.remove(new AbstractMap.SimpleEntry(paramString, paramzzdf));
  }
  
  public void b(String paramString, JSONObject paramJSONObject)
  {
    this.a.b(paramString, paramJSONObject);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzej.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */