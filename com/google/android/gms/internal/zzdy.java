package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Base64;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.zzk;
import com.google.android.gms.ads.internal.zzr;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

@zzhb
public class zzdy
{
  private final Map<zzdz, zzea> a = new HashMap();
  private final LinkedList<zzdz> b = new LinkedList();
  private zzdv c;
  
  private static void a(String paramString, zzdz paramzzdz)
  {
    if (zzin.a(2)) {
      zzin.e(String.format(paramString, new Object[] { paramzzdz }));
    }
  }
  
  private String[] a(String paramString)
  {
    try
    {
      String[] arrayOfString = paramString.split("\000");
      int i = 0;
      for (;;)
      {
        paramString = arrayOfString;
        if (i >= arrayOfString.length) {
          break;
        }
        arrayOfString[i] = new String(Base64.decode(arrayOfString[i], 0), "UTF-8");
        i += 1;
      }
      return paramString;
    }
    catch (UnsupportedEncodingException paramString)
    {
      paramString = new String[0];
    }
  }
  
  private String e()
  {
    try
    {
      StringBuilder localStringBuilder = new StringBuilder();
      Iterator localIterator = this.b.iterator();
      while (localIterator.hasNext())
      {
        localStringBuilder.append(Base64.encodeToString(((zzdz)localIterator.next()).toString().getBytes("UTF-8"), 0));
        if (localIterator.hasNext()) {
          localStringBuilder.append("\000");
        }
      }
      str = localUnsupportedEncodingException.toString();
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      return "";
    }
    String str;
    return str;
  }
  
  zzea.zza a(AdRequestParcel paramAdRequestParcel, String paramString)
  {
    int i = new zzhj.zza(this.c.b()).a().m;
    zzdz localzzdz = new zzdz(paramAdRequestParcel, paramString, i);
    zzea localzzea = (zzea)this.a.get(localzzdz);
    if (localzzea == null)
    {
      a("Interstitial pool created at %s.", localzzdz);
      paramAdRequestParcel = new zzea(paramAdRequestParcel, paramString, i);
      this.a.put(localzzdz, paramAdRequestParcel);
    }
    for (;;)
    {
      this.b.remove(localzzdz);
      this.b.add(localzzdz);
      localzzdz.a();
      while (this.b.size() > ((Integer)zzbt.ag.c()).intValue())
      {
        paramString = (zzdz)this.b.remove();
        localzzea = (zzea)this.a.get(paramString);
        a("Evicting interstitial queue for %s.", paramString);
        while (localzzea.e() > 0) {
          localzzea.d().a.D();
        }
        this.a.remove(paramString);
      }
      while (paramAdRequestParcel.e() > 0)
      {
        paramString = paramAdRequestParcel.d();
        if ((paramString.e) && (zzr.i().a() - paramString.d > 1000L * ((Integer)zzbt.ai.c()).intValue()))
        {
          a("Expired interstitial at %s.", localzzdz);
        }
        else
        {
          a("Pooled interstitial returned at %s.", localzzdz);
          return paramString;
        }
      }
      return null;
      paramAdRequestParcel = localzzea;
    }
  }
  
  void a()
  {
    if (this.c == null) {
      return;
    }
    Iterator localIterator = this.a.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Object localObject = (Map.Entry)localIterator.next();
      zzdz localzzdz = (zzdz)((Map.Entry)localObject).getKey();
      localObject = (zzea)((Map.Entry)localObject).getValue();
      while (((zzea)localObject).e() < ((Integer)zzbt.ah.c()).intValue())
      {
        a("Pooling one interstitial for %s.", localzzdz);
        ((zzea)localObject).a(this.c);
      }
    }
    b();
  }
  
  void a(zzdv paramzzdv)
  {
    if (this.c == null)
    {
      this.c = paramzzdv;
      c();
    }
  }
  
  void b()
  {
    if (this.c == null) {
      return;
    }
    SharedPreferences.Editor localEditor = this.c.b().getSharedPreferences("com.google.android.gms.ads.internal.interstitial.InterstitialAdPool", 0).edit();
    localEditor.clear();
    Iterator localIterator = this.a.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Object localObject = (Map.Entry)localIterator.next();
      zzdz localzzdz = (zzdz)((Map.Entry)localObject).getKey();
      if (localzzdz.b())
      {
        localObject = new zzec((zzea)((Map.Entry)localObject).getValue()).a();
        localEditor.putString(localzzdz.toString(), (String)localObject);
        a("Saved interstitial queue for %s.", localzzdz);
      }
    }
    localEditor.putString("PoolKeys", e());
    localEditor.commit();
  }
  
  void c()
  {
    if (this.c == null) {}
    for (;;)
    {
      return;
      Object localObject2 = this.c.b().getSharedPreferences("com.google.android.gms.ads.internal.interstitial.InterstitialAdPool", 0);
      d();
      HashMap localHashMap = new HashMap();
      Iterator localIterator = ((SharedPreferences)localObject2).getAll().entrySet().iterator();
      label55:
      Object localObject1;
      if (localIterator.hasNext()) {
        localObject1 = (Map.Entry)localIterator.next();
      }
      try
      {
        if (((String)((Map.Entry)localObject1).getKey()).equals("PoolKeys")) {
          break label55;
        }
        Object localObject3 = new zzec((String)((Map.Entry)localObject1).getValue());
        localObject1 = new zzdz(((zzec)localObject3).a, ((zzec)localObject3).b, ((zzec)localObject3).c);
        if (this.a.containsKey(localObject1)) {
          break label55;
        }
        localObject3 = new zzea(((zzec)localObject3).a, ((zzec)localObject3).b, ((zzec)localObject3).c);
        this.a.put(localObject1, localObject3);
        localHashMap.put(((zzdz)localObject1).toString(), localObject1);
        a("Restored interstitial queue for %s.", (zzdz)localObject1);
      }
      catch (IOException localIOException)
      {
        zzin.d("Malformed preferences value for InterstitialAdPool.", localIOException);
        break label55;
        String[] arrayOfString = a(((SharedPreferences)localObject2).getString("PoolKeys", ""));
        int j = arrayOfString.length;
        int i = 0;
        while (i < j)
        {
          localObject2 = (zzdz)localHashMap.get(arrayOfString[i]);
          if (this.a.containsKey(localObject2)) {
            this.b.add(localObject2);
          }
          i += 1;
        }
      }
      catch (ClassCastException localClassCastException)
      {
        for (;;) {}
      }
    }
  }
  
  void d()
  {
    while (this.b.size() > 0)
    {
      zzdz localzzdz = (zzdz)this.b.remove();
      zzea localzzea = (zzea)this.a.get(localzzdz);
      a("Flushing interstitial queue for %s.", localzzdz);
      while (localzzea.e() > 0) {
        localzzea.d().a.D();
      }
      this.a.remove(localzzdz);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzdy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */