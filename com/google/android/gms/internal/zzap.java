package com.google.android.gms.internal;

public abstract interface zzap
{
  public abstract String a(byte[] paramArrayOfByte, boolean paramBoolean);
  
  public abstract byte[] a(String paramString, boolean paramBoolean)
    throws IllegalArgumentException;
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */