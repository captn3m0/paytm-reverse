package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.measurement.zze;
import java.util.HashMap;
import java.util.Map;

public final class zzpr
  extends zze<zzpr>
{
  private String a;
  private String b;
  private String c;
  private String d;
  private String e;
  private String f;
  private String g;
  private String h;
  private String i;
  private String j;
  
  public String a()
  {
    return this.a;
  }
  
  public void a(zzpr paramzzpr)
  {
    if (!TextUtils.isEmpty(this.a)) {
      paramzzpr.a(this.a);
    }
    if (!TextUtils.isEmpty(this.b)) {
      paramzzpr.b(this.b);
    }
    if (!TextUtils.isEmpty(this.c)) {
      paramzzpr.c(this.c);
    }
    if (!TextUtils.isEmpty(this.d)) {
      paramzzpr.d(this.d);
    }
    if (!TextUtils.isEmpty(this.e)) {
      paramzzpr.e(this.e);
    }
    if (!TextUtils.isEmpty(this.f)) {
      paramzzpr.f(this.f);
    }
    if (!TextUtils.isEmpty(this.g)) {
      paramzzpr.g(this.g);
    }
    if (!TextUtils.isEmpty(this.h)) {
      paramzzpr.h(this.h);
    }
    if (!TextUtils.isEmpty(this.i)) {
      paramzzpr.i(this.i);
    }
    if (!TextUtils.isEmpty(this.j)) {
      paramzzpr.j(this.j);
    }
  }
  
  public void a(String paramString)
  {
    this.a = paramString;
  }
  
  public String b()
  {
    return this.b;
  }
  
  public void b(String paramString)
  {
    this.b = paramString;
  }
  
  public String c()
  {
    return this.c;
  }
  
  public void c(String paramString)
  {
    this.c = paramString;
  }
  
  public String d()
  {
    return this.d;
  }
  
  public void d(String paramString)
  {
    this.d = paramString;
  }
  
  public String e()
  {
    return this.e;
  }
  
  public void e(String paramString)
  {
    this.e = paramString;
  }
  
  public String f()
  {
    return this.f;
  }
  
  public void f(String paramString)
  {
    this.f = paramString;
  }
  
  public String g()
  {
    return this.g;
  }
  
  public void g(String paramString)
  {
    this.g = paramString;
  }
  
  public String h()
  {
    return this.h;
  }
  
  public void h(String paramString)
  {
    this.h = paramString;
  }
  
  public String i()
  {
    return this.i;
  }
  
  public void i(String paramString)
  {
    this.i = paramString;
  }
  
  public String j()
  {
    return this.j;
  }
  
  public void j(String paramString)
  {
    this.j = paramString;
  }
  
  public String toString()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("name", this.a);
    localHashMap.put("source", this.b);
    localHashMap.put("medium", this.c);
    localHashMap.put("keyword", this.d);
    localHashMap.put("content", this.e);
    localHashMap.put("id", this.f);
    localHashMap.put("adNetworkId", this.g);
    localHashMap.put("gclid", this.h);
    localHashMap.put("dclid", this.i);
    localHashMap.put("aclid", this.j);
    return a(localHashMap);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzpr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */