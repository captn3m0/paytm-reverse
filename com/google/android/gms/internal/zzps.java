package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.measurement.zze;
import java.util.HashMap;
import java.util.Map;

public final class zzps
  extends zze<zzps>
{
  public int a;
  public int b;
  public int c;
  public int d;
  public int e;
  private String f;
  
  public int a()
  {
    return this.a;
  }
  
  public void a(int paramInt)
  {
    this.a = paramInt;
  }
  
  public void a(zzps paramzzps)
  {
    if (this.a != 0) {
      paramzzps.a(this.a);
    }
    if (this.b != 0) {
      paramzzps.b(this.b);
    }
    if (this.c != 0) {
      paramzzps.c(this.c);
    }
    if (this.d != 0) {
      paramzzps.d(this.d);
    }
    if (this.e != 0) {
      paramzzps.e(this.e);
    }
    if (!TextUtils.isEmpty(this.f)) {
      paramzzps.a(this.f);
    }
  }
  
  public void a(String paramString)
  {
    this.f = paramString;
  }
  
  public int b()
  {
    return this.b;
  }
  
  public void b(int paramInt)
  {
    this.b = paramInt;
  }
  
  public int c()
  {
    return this.c;
  }
  
  public void c(int paramInt)
  {
    this.c = paramInt;
  }
  
  public int d()
  {
    return this.d;
  }
  
  public void d(int paramInt)
  {
    this.d = paramInt;
  }
  
  public int e()
  {
    return this.e;
  }
  
  public void e(int paramInt)
  {
    this.e = paramInt;
  }
  
  public String f()
  {
    return this.f;
  }
  
  public String toString()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("language", this.f);
    localHashMap.put("screenColors", Integer.valueOf(this.a));
    localHashMap.put("screenWidth", Integer.valueOf(this.b));
    localHashMap.put("screenHeight", Integer.valueOf(this.c));
    localHashMap.put("viewportWidth", Integer.valueOf(this.d));
    localHashMap.put("viewportHeight", Integer.valueOf(this.e));
    return a(localHashMap);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzps.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */