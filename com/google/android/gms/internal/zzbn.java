package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import java.util.Collections;
import java.util.List;

@zzhb
public class zzbn
  implements zzbo
{
  public List<String> a(AdRequestInfoParcel paramAdRequestInfoParcel)
  {
    if (paramAdRequestInfoParcel.x == null) {
      return Collections.emptyList();
    }
    return paramAdRequestInfoParcel.x;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzbn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */