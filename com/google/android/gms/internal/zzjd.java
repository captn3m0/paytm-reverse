package com.google.android.gms.internal;

import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@zzhb
public class zzjd<T>
  implements zzjg<T>
{
  private final Object a = new Object();
  private T b = null;
  private boolean c = false;
  private boolean d = false;
  private final zzjh e = new zzjh();
  
  public void a(Runnable paramRunnable)
  {
    this.e.a(paramRunnable);
  }
  
  public void b(T paramT)
  {
    synchronized (this.a)
    {
      if (this.d) {
        return;
      }
      if (this.c) {
        throw new IllegalStateException("Provided CallbackFuture with multiple values.");
      }
    }
    this.c = true;
    this.b = paramT;
    this.a.notifyAll();
    this.e.a();
  }
  
  public void b(Runnable paramRunnable)
  {
    this.e.b(paramRunnable);
  }
  
  public boolean cancel(boolean paramBoolean)
  {
    if (!paramBoolean) {
      return false;
    }
    synchronized (this.a)
    {
      if (this.c) {
        return false;
      }
    }
    this.d = true;
    this.c = true;
    this.a.notifyAll();
    this.e.a();
    return true;
  }
  
  public T get()
  {
    synchronized (this.a)
    {
      boolean bool = this.c;
      if (bool) {}
    }
    try
    {
      this.a.wait();
      if (this.d)
      {
        throw new CancellationException("CallbackFuture was cancelled.");
        localObject2 = finally;
        throw ((Throwable)localObject2);
      }
      Object localObject3 = this.b;
      return (T)localObject3;
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;) {}
    }
  }
  
  public T get(long paramLong, TimeUnit paramTimeUnit)
    throws TimeoutException
  {
    synchronized (this.a)
    {
      boolean bool = this.c;
      if (!bool) {}
      try
      {
        paramLong = paramTimeUnit.toMillis(paramLong);
        if (paramLong != 0L) {
          this.a.wait(paramLong);
        }
      }
      catch (InterruptedException paramTimeUnit)
      {
        for (;;) {}
      }
      if (!this.c) {
        throw new TimeoutException("CallbackFuture timed out.");
      }
    }
    if (this.d) {
      throw new CancellationException("CallbackFuture was cancelled.");
    }
    paramTimeUnit = this.b;
    return paramTimeUnit;
  }
  
  public boolean isCancelled()
  {
    synchronized (this.a)
    {
      boolean bool = this.d;
      return bool;
    }
  }
  
  public boolean isDone()
  {
    synchronized (this.a)
    {
      boolean bool = this.c;
      return bool;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzjd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */