package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import org.json.JSONException;
import org.json.JSONObject;

@zzhb
public class zzba
  extends zzau
{
  private zzeg.zzd d;
  private boolean e;
  
  public zzba(final Context paramContext, AdSizeParcel paramAdSizeParcel, zzif paramzzif, VersionInfoParcel paramVersionInfoParcel, zzbb paramzzbb, zzeg paramzzeg)
  {
    super(paramContext, paramAdSizeParcel, paramzzif, paramVersionInfoParcel, paramzzbb);
    this.d = paramzzeg.b();
    try
    {
      paramContext = a(paramzzbb.c().a());
      this.d.a(new zzji.zzc()new zzji.zza
      {
        public void a(zzeh paramAnonymouszzeh)
        {
          zzba.this.a(paramContext);
        }
      }, new zzji.zza()
      {
        public void a() {}
      });
      this.d.a(new zzji.zzc()new zzji.zza
      {
        public void a(zzeh paramAnonymouszzeh)
        {
          zzba.a(zzba.this, true);
          zzba.this.a(paramAnonymouszzeh);
          zzba.this.a();
          zzba.this.b(false);
        }
      }, new zzji.zza()
      {
        public void a()
        {
          zzba.this.c();
        }
      });
      zzin.a("Tracking ad unit: " + this.b.d());
      return;
    }
    catch (RuntimeException paramContext)
    {
      for (;;)
      {
        zzin.b("Failure while processing active view data.", paramContext);
      }
    }
    catch (JSONException paramContext)
    {
      for (;;) {}
    }
  }
  
  protected void b(final JSONObject paramJSONObject)
  {
    this.d.a(new zzji.zzc()new zzji.zzb
    {
      public void a(zzeh paramAnonymouszzeh)
      {
        paramAnonymouszzeh.a("AFMA_updateActiveView", paramJSONObject);
      }
    }, new zzji.zzb());
  }
  
  protected void c()
  {
    synchronized (this.a)
    {
      super.c();
      this.d.a(new zzji.zzc()new zzji.zzb
      {
        public void a(zzeh paramAnonymouszzeh)
        {
          zzba.this.b(paramAnonymouszzeh);
        }
      }, new zzji.zzb());
      this.d.a();
      return;
    }
  }
  
  protected boolean j()
  {
    return this.e;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzba.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */