package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzr;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@zzhb
public class zzcb
{
  boolean a;
  private final List<zzbz> b = new LinkedList();
  private final Map<String, String> c = new LinkedHashMap();
  private final Object d = new Object();
  private String e;
  private zzbz f;
  private zzcb g;
  
  public zzcb(boolean paramBoolean, String paramString1, String paramString2)
  {
    this.a = paramBoolean;
    this.c.put("action", paramString1);
    this.c.put("ad_format", paramString2);
  }
  
  public zzbz a()
  {
    return a(zzr.i().b());
  }
  
  public zzbz a(long paramLong)
  {
    if (!this.a) {
      return null;
    }
    return new zzbz(paramLong, null, null);
  }
  
  public void a(zzcb paramzzcb)
  {
    synchronized (this.d)
    {
      this.g = paramzzcb;
      return;
    }
  }
  
  public void a(String paramString)
  {
    if (!this.a) {
      return;
    }
    synchronized (this.d)
    {
      this.e = paramString;
      return;
    }
  }
  
  public void a(String paramString1, String paramString2)
  {
    if ((!this.a) || (TextUtils.isEmpty(paramString2))) {}
    zzbv localzzbv;
    do
    {
      return;
      localzzbv = zzr.h().e();
    } while (localzzbv == null);
    synchronized (this.d)
    {
      localzzbv.a(paramString1).a(this.c, paramString1, paramString2);
      return;
    }
  }
  
  public boolean a(zzbz paramzzbz, long paramLong, String... paramVarArgs)
  {
    synchronized (this.d)
    {
      int j = paramVarArgs.length;
      int i = 0;
      while (i < j)
      {
        zzbz localzzbz = new zzbz(paramLong, paramVarArgs[i], paramzzbz);
        this.b.add(localzzbz);
        i += 1;
      }
      return true;
    }
  }
  
  public boolean a(zzbz paramzzbz, String... paramVarArgs)
  {
    if ((!this.a) || (paramzzbz == null)) {
      return false;
    }
    return a(paramzzbz, zzr.i().b(), paramVarArgs);
  }
  
  public void b()
  {
    synchronized (this.d)
    {
      this.f = a();
      return;
    }
  }
  
  public String c()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    synchronized (this.d)
    {
      Iterator localIterator = this.b.iterator();
      while (localIterator.hasNext())
      {
        zzbz localzzbz = (zzbz)localIterator.next();
        long l1 = localzzbz.a();
        String str2 = localzzbz.b();
        localzzbz = localzzbz.c();
        if ((localzzbz != null) && (l1 > 0L))
        {
          long l2 = localzzbz.a();
          localStringBuilder.append(str2).append('.').append(l1 - l2).append(',');
        }
      }
    }
    this.b.clear();
    if (!TextUtils.isEmpty(this.e)) {
      ((StringBuilder)localObject2).append(this.e);
    }
    for (;;)
    {
      String str1 = ((StringBuilder)localObject2).toString();
      return str1;
      if (str1.length() > 0) {
        str1.setLength(str1.length() - 1);
      }
    }
  }
  
  Map<String, String> d()
  {
    synchronized (this.d)
    {
      Object localObject2 = zzr.h().e();
      if ((localObject2 == null) || (this.g == null))
      {
        localObject2 = this.c;
        return (Map<String, String>)localObject2;
      }
      localObject2 = ((zzbv)localObject2).a(this.c, this.g.d());
      return (Map<String, String>)localObject2;
    }
  }
  
  public zzbz e()
  {
    synchronized (this.d)
    {
      zzbz localzzbz = this.f;
      return localzzbz;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzcb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */