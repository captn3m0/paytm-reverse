package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.widget.FrameLayout;
import com.google.android.gms.ads.internal.client.zzl;
import com.google.android.gms.ads.internal.client.zzn;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.dynamic.zzg;
import com.google.android.gms.dynamic.zzg.zza;

@zzhb
public class zzcv
  extends zzg<zzck>
{
  public zzcv()
  {
    super("com.google.android.gms.ads.NativeAdViewDelegateCreatorImpl");
  }
  
  private zzcj b(Context paramContext, FrameLayout paramFrameLayout1, FrameLayout paramFrameLayout2)
  {
    try
    {
      zzd localzzd = zze.a(paramContext);
      paramFrameLayout1 = zze.a(paramFrameLayout1);
      paramFrameLayout2 = zze.a(paramFrameLayout2);
      paramContext = zzcj.zza.a(((zzck)a(paramContext)).a(localzzd, paramFrameLayout1, paramFrameLayout2, 8487000));
      return paramContext;
    }
    catch (RemoteException paramContext)
    {
      zzb.d("Could not create remote NativeAdViewDelegate.", paramContext);
      return null;
    }
    catch (zzg.zza paramContext)
    {
      for (;;) {}
    }
  }
  
  public zzcj a(Context paramContext, FrameLayout paramFrameLayout1, FrameLayout paramFrameLayout2)
  {
    if (zzn.a().b(paramContext))
    {
      zzcj localzzcj = b(paramContext, paramFrameLayout1, paramFrameLayout2);
      paramContext = localzzcj;
      if (localzzcj != null) {}
    }
    else
    {
      zzb.a("Using NativeAdViewDelegate from the client jar.");
      paramContext = zzn.c().a(paramFrameLayout1, paramFrameLayout2);
    }
    return paramContext;
  }
  
  protected zzck a(IBinder paramIBinder)
  {
    return zzck.zza.a(paramIBinder);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzcv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */