package com.google.android.gms.internal;

import android.os.Handler;
import com.google.android.gms.ads.internal.zzr;

@zzhb
public class zzdp
  extends zzim
{
  final zzjp a;
  final zzdr b;
  private final String c;
  
  zzdp(zzjp paramzzjp, zzdr paramzzdr, String paramString)
  {
    this.a = paramzzjp;
    this.b = paramzzdr;
    this.c = paramString;
    zzr.t().a(this);
  }
  
  public void a()
  {
    try
    {
      this.b.a(this.c);
      return;
    }
    finally
    {
      zzir.a.post(new Runnable()
      {
        public void run()
        {
          zzr.t().b(zzdp.this);
        }
      });
    }
  }
  
  public void b()
  {
    this.b.a();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzdp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */