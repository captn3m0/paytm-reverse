package com.google.android.gms.internal;

public class zzoz
{
  private static final ThreadLocal<String> a = new ThreadLocal();
  
  public static String a(String paramString)
  {
    if (a()) {
      return paramString;
    }
    return a(paramString, (String)a.get());
  }
  
  public static String a(String paramString1, String paramString2)
  {
    if ((paramString1 == null) || (paramString2 == null)) {
      return paramString1;
    }
    byte[] arrayOfByte = new byte[paramString1.length() + paramString2.length()];
    System.arraycopy(paramString1.getBytes(), 0, arrayOfByte, 0, paramString1.length());
    System.arraycopy(paramString2.getBytes(), 0, arrayOfByte, paramString1.length(), paramString2.length());
    return Integer.toHexString(zznd.a(arrayOfByte, 0, arrayOfByte.length, 0));
  }
  
  public static boolean a()
  {
    String str = (String)a.get();
    return (str == null) || (str.startsWith("com.google"));
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzoz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */