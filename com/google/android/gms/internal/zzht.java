package com.google.android.gms.internal;

import android.os.Handler;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.reward.client.RewardedVideoAdRequestParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzb;
import com.google.android.gms.ads.internal.zzr;
import com.google.android.gms.ads.internal.zzs;
import com.google.android.gms.common.internal.zzx;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

@zzhb
public class zzht
  extends zzb
  implements zzhw
{
  private static final zzew l = new zzew();
  private final Map<String, zzia> m;
  private boolean n;
  
  private zzif.zza b(zzif.zza paramzza)
  {
    zzin.e("Creating mediation ad response for non-mediated rewarded ad.");
    try
    {
      Object localObject1 = zzhe.a(paramzza.b).toString();
      Object localObject2 = new JSONObject();
      ((JSONObject)localObject2).put("pubid", paramzza.a.e);
      localObject2 = ((JSONObject)localObject2).toString();
      localObject1 = new zzeo(Arrays.asList(new zzen[] { new zzen((String)localObject1, null, Arrays.asList(new String[] { "com.google.ads.mediation.admob.AdMobAdapter" }), null, null, Collections.emptyList(), Collections.emptyList(), (String)localObject2, null, Collections.emptyList(), Collections.emptyList(), null, null, null, null, null) }), -1L, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), "", -1L, 0, 1, null, 0, -1, -1L);
      return new zzif.zza(paramzza.a, paramzza.b, (zzeo)localObject1, paramzza.d, paramzza.e, paramzza.f, paramzza.g, paramzza.h);
    }
    catch (JSONException localJSONException)
    {
      zzin.b("Unable to generate ad state for non-mediated rewarded video.", localJSONException);
    }
    return c(paramzza);
  }
  
  private zzif.zza c(zzif.zza paramzza)
  {
    return new zzif.zza(paramzza.a, paramzza.b, null, paramzza.d, 0, paramzza.f, paramzza.g, paramzza.h);
  }
  
  public boolean A()
  {
    zzx.b("isLoaded must be called on the main UI thread.");
    return (this.f.g == null) && (this.f.h == null) && (this.f.j != null) && (!this.n);
  }
  
  public void B()
  {
    a(this.f.j, false);
    p();
  }
  
  public void C()
  {
    if ((this.f.j != null) && (this.f.j.n != null)) {
      zzr.r().a(this.f.c, this.f.e.b, this.f.j, this.f.b, false, this.f.j.n.i);
    }
    r();
  }
  
  public void D()
  {
    n();
  }
  
  public void E()
  {
    e();
  }
  
  public void F()
  {
    o();
  }
  
  public void a(RewardedVideoAdRequestParcel paramRewardedVideoAdRequestParcel)
  {
    zzx.b("loadAd must be called on the main UI thread.");
    if (TextUtils.isEmpty(paramRewardedVideoAdRequestParcel.c))
    {
      zzin.d("Invalid ad unit id. Aborting.");
      return;
    }
    this.n = false;
    this.f.b = paramRewardedVideoAdRequestParcel.c;
    super.a(paramRewardedVideoAdRequestParcel.b);
  }
  
  public void a(final zzif.zza paramzza, zzcb paramzzcb)
  {
    if (paramzza.e != -2)
    {
      zzir.a.post(new Runnable()
      {
        public void run()
        {
          zzht.this.b(new zzif(paramzza, null, null, null, null, null, null));
        }
      });
      return;
    }
    this.f.k = paramzza;
    if (paramzza.c == null) {
      this.f.k = b(paramzza);
    }
    this.f.D = 0;
    this.f.h = zzr.d().a(this.f.c, this.f.h(), this.f.k, this);
  }
  
  public boolean a(zzif paramzzif1, zzif paramzzif2)
  {
    return true;
  }
  
  public void b()
  {
    zzx.b("destroy must be called on the main UI thread.");
    Iterator localIterator = this.m.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      try
      {
        zzia localzzia = (zzia)this.m.get(str);
        if ((localzzia != null) && (localzzia.a() != null)) {
          localzzia.a().c();
        }
      }
      catch (RemoteException localRemoteException)
      {
        zzin.d("Fail to destroy adapter: " + str);
      }
    }
  }
  
  public void b(@Nullable RewardItemParcel paramRewardItemParcel)
  {
    if ((this.f.j != null) && (this.f.j.n != null)) {
      zzr.r().a(this.f.c, this.f.e.b, this.f.j, this.f.b, false, this.f.j.n.j);
    }
    RewardItemParcel localRewardItemParcel = paramRewardItemParcel;
    if (this.f.j != null)
    {
      localRewardItemParcel = paramRewardItemParcel;
      if (this.f.j.q != null)
      {
        localRewardItemParcel = paramRewardItemParcel;
        if (!TextUtils.isEmpty(this.f.j.q.h)) {
          localRewardItemParcel = new RewardItemParcel(this.f.j.q.h, this.f.j.q.i);
        }
      }
    }
    a(localRewardItemParcel);
  }
  
  public void b_()
  {
    zzx.b("resume must be called on the main UI thread.");
    Iterator localIterator = this.m.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      try
      {
        zzia localzzia = (zzia)this.m.get(str);
        if ((localzzia != null) && (localzzia.a() != null)) {
          localzzia.a().e();
        }
      }
      catch (RemoteException localRemoteException)
      {
        zzin.d("Fail to resume adapter: " + str);
      }
    }
  }
  
  @Nullable
  public zzia c(String paramString)
  {
    localObject1 = (zzia)this.m.get(paramString);
    Object localObject2 = localObject1;
    if (localObject1 == null) {}
    try
    {
      localObject2 = this.j;
      if ("com.google.ads.mediation.admob.AdMobAdapter".equals(paramString)) {}
      Object localObject3;
      for (localObject2 = l;; localObject2 = new zzia(((zzex)localObject2).a(paramString), this)) {}
    }
    catch (Exception localException1)
    {
      try
      {
        this.m.put(paramString, localObject2);
        return (zzia)localObject2;
      }
      catch (Exception localException2)
      {
        for (;;)
        {
          localObject1 = localException1;
          localObject3 = localException2;
        }
      }
      localException1 = localException1;
      zzin.d("Fail to instantiate adapter " + paramString, localException1);
      return (zzia)localObject1;
    }
  }
  
  public void d()
  {
    zzx.b("pause must be called on the main UI thread.");
    Iterator localIterator = this.m.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      try
      {
        zzia localzzia = (zzia)this.m.get(str);
        if ((localzzia != null) && (localzzia.a() != null)) {
          localzzia.a().d();
        }
      }
      catch (RemoteException localRemoteException)
      {
        zzin.d("Fail to pause adapter: " + str);
      }
    }
  }
  
  public void z()
  {
    zzx.b("showAd must be called on the main UI thread.");
    if (!A()) {
      zzin.d("The reward video has not loaded.");
    }
    zzia localzzia;
    do
    {
      return;
      this.n = true;
      localzzia = c(this.f.j.p);
    } while ((localzzia == null) || (localzzia.a() == null));
    try
    {
      localzzia.a().f();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzin.d("Could not call showVideo.", localRemoteException);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzht.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */