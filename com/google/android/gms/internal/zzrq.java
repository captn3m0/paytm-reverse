package com.google.android.gms.internal;

import java.io.IOException;

public abstract interface zzrq
{
  public static final class zza
    extends zzso<zza>
  {
    public long a;
    public zzaf.zzf b;
    public zzaf.zzj c;
    
    public zza()
    {
      a();
    }
    
    public static zza a(byte[] paramArrayOfByte)
      throws zzst
    {
      return (zza)zzsu.a(new zza(), paramArrayOfByte);
    }
    
    public zza a()
    {
      this.a = 0L;
      this.b = null;
      this.c = null;
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public zza a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 8: 
          this.a = paramzzsm.f();
          break;
        case 18: 
          if (this.b == null) {
            this.b = new zzaf.zzf();
          }
          paramzzsm.a(this.b);
          break;
        case 26: 
          if (this.c == null) {
            this.c = new zzaf.zzj();
          }
          paramzzsm.a(this.c);
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      paramzzsn.b(1, this.a);
      if (this.b != null) {
        paramzzsn.a(2, this.b);
      }
      if (this.c != null) {
        paramzzsn.a(3, this.c);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b() + zzsn.d(1, this.a);
      int i = j;
      if (this.b != null) {
        i = j + zzsn.c(2, this.b);
      }
      j = i;
      if (this.c != null) {
        j = i + zzsn.c(3, this.c);
      }
      return j;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      label55:
      do
      {
        do
        {
          do
          {
            do
            {
              return bool1;
              bool1 = bool2;
            } while (!(paramObject instanceof zza));
            paramObject = (zza)paramObject;
            bool1 = bool2;
          } while (this.a != ((zza)paramObject).a);
          if (this.b != null) {
            break;
          }
          bool1 = bool2;
        } while (((zza)paramObject).b != null);
        if (this.c != null) {
          break label125;
        }
        bool1 = bool2;
      } while (((zza)paramObject).c != null);
      for (;;)
      {
        if ((this.r == null) || (this.r.b()))
        {
          if (((zza)paramObject).r != null)
          {
            bool1 = bool2;
            if (!((zza)paramObject).r.b()) {
              break;
            }
          }
          return true;
          if (this.b.equals(((zza)paramObject).b)) {
            break label55;
          }
          return false;
          label125:
          if (!this.c.equals(((zza)paramObject).c)) {
            return false;
          }
        }
      }
      return this.r.equals(((zza)paramObject).r);
    }
    
    public int hashCode()
    {
      int m = 0;
      int n = getClass().getName().hashCode();
      int i1 = (int)(this.a ^ this.a >>> 32);
      int i;
      int j;
      if (this.b == null)
      {
        i = 0;
        if (this.c != null) {
          break label110;
        }
        j = 0;
        label48:
        k = m;
        if (this.r != null) {
          if (!this.r.b()) {
            break label121;
          }
        }
      }
      label110:
      label121:
      for (int k = m;; k = this.r.hashCode())
      {
        return (j + (i + ((n + 527) * 31 + i1) * 31) * 31) * 31 + k;
        i = this.b.hashCode();
        break;
        j = this.c.hashCode();
        break label48;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzrq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */