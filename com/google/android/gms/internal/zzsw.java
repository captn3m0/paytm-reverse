package com.google.android.gms.internal;

import java.io.IOException;
import java.util.Arrays;

final class zzsw
{
  final int a;
  final byte[] b;
  
  zzsw(int paramInt, byte[] paramArrayOfByte)
  {
    this.a = paramInt;
    this.b = paramArrayOfByte;
  }
  
  int a()
  {
    return 0 + zzsn.h(this.a) + this.b.length;
  }
  
  void a(zzsn paramzzsn)
    throws IOException
  {
    paramzzsn.g(this.a);
    paramzzsn.d(this.b);
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzsw)) {
        return false;
      }
      paramObject = (zzsw)paramObject;
    } while ((this.a == ((zzsw)paramObject).a) && (Arrays.equals(this.b, ((zzsw)paramObject).b)));
    return false;
  }
  
  public int hashCode()
  {
    return (this.a + 527) * 31 + Arrays.hashCode(this.b);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzsw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */