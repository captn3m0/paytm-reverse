package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.view.View;
import android.webkit.WebChromeClient.CustomViewCallback;

@zzhb
@TargetApi(14)
public final class zzjx
  extends zzjv
{
  public zzjx(zzjp paramzzjp)
  {
    super(paramzzjp);
  }
  
  public void onShowCustomView(View paramView, int paramInt, WebChromeClient.CustomViewCallback paramCustomViewCallback)
  {
    a(paramView, paramInt, paramCustomViewCallback);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzjx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */