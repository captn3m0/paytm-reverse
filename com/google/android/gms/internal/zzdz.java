package com.google.android.gms.internal;

import android.location.Location;
import android.os.Bundle;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

@zzhb
class zzdz
{
  private final Object[] a;
  private boolean b;
  
  zzdz(AdRequestParcel paramAdRequestParcel, String paramString, int paramInt)
  {
    this.a = a(paramAdRequestParcel, paramString, paramInt);
  }
  
  private static String a(Bundle paramBundle)
  {
    if (paramBundle == null) {
      return null;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    Collections.sort(new ArrayList(paramBundle.keySet()));
    Iterator localIterator = paramBundle.keySet().iterator();
    if (localIterator.hasNext())
    {
      Object localObject = paramBundle.get((String)localIterator.next());
      if (localObject == null) {
        localObject = "null";
      }
      for (;;)
      {
        localStringBuilder.append((String)localObject);
        break;
        if ((localObject instanceof Bundle)) {
          localObject = a((Bundle)localObject);
        } else {
          localObject = localObject.toString();
        }
      }
    }
    return localStringBuilder.toString();
  }
  
  private static Object[] a(AdRequestParcel paramAdRequestParcel, String paramString, int paramInt)
  {
    HashSet localHashSet = new HashSet(Arrays.asList(((String)zzbt.af.c()).split(",")));
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(paramString);
    if (localHashSet.contains("networkType")) {
      localArrayList.add(Integer.valueOf(paramInt));
    }
    if (localHashSet.contains("birthday")) {
      localArrayList.add(Long.valueOf(paramAdRequestParcel.b));
    }
    if (localHashSet.contains("extras")) {
      localArrayList.add(a(paramAdRequestParcel.c));
    }
    if (localHashSet.contains("gender")) {
      localArrayList.add(Integer.valueOf(paramAdRequestParcel.d));
    }
    if (localHashSet.contains("keywords"))
    {
      if (paramAdRequestParcel.e != null) {
        localArrayList.add(paramAdRequestParcel.e.toString());
      }
    }
    else
    {
      if (localHashSet.contains("isTestDevice")) {
        localArrayList.add(Boolean.valueOf(paramAdRequestParcel.f));
      }
      if (localHashSet.contains("tagForChildDirectedTreatment")) {
        localArrayList.add(Integer.valueOf(paramAdRequestParcel.g));
      }
      if (localHashSet.contains("manualImpressionsEnabled")) {
        localArrayList.add(Boolean.valueOf(paramAdRequestParcel.h));
      }
      if (localHashSet.contains("publisherProvidedId")) {
        localArrayList.add(paramAdRequestParcel.i);
      }
      if (localHashSet.contains("location"))
      {
        if (paramAdRequestParcel.k == null) {
          break label447;
        }
        localArrayList.add(paramAdRequestParcel.k.toString());
      }
      label289:
      if (localHashSet.contains("contentUrl")) {
        localArrayList.add(paramAdRequestParcel.l);
      }
      if (localHashSet.contains("networkExtras")) {
        localArrayList.add(a(paramAdRequestParcel.m));
      }
      if (localHashSet.contains("customTargeting")) {
        localArrayList.add(a(paramAdRequestParcel.n));
      }
      if (localHashSet.contains("categoryExclusions"))
      {
        if (paramAdRequestParcel.o == null) {
          break label457;
        }
        localArrayList.add(paramAdRequestParcel.o.toString());
      }
    }
    for (;;)
    {
      if (localHashSet.contains("requestAgent")) {
        localArrayList.add(paramAdRequestParcel.p);
      }
      if (localHashSet.contains("requestPackage")) {
        localArrayList.add(paramAdRequestParcel.q);
      }
      return localArrayList.toArray();
      localArrayList.add(null);
      break;
      label447:
      localArrayList.add(null);
      break label289;
      label457:
      localArrayList.add(null);
    }
  }
  
  void a()
  {
    this.b = true;
  }
  
  boolean b()
  {
    return this.b;
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof zzdz)) {
      return false;
    }
    paramObject = (zzdz)paramObject;
    return Arrays.equals(this.a, ((zzdz)paramObject).a);
  }
  
  public int hashCode()
  {
    return Arrays.hashCode(this.a);
  }
  
  public String toString()
  {
    return "[InterstitialAdPoolKey " + Arrays.toString(this.a) + "]";
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzdz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */