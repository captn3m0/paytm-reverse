package com.google.android.gms.internal;

import com.google.android.gms.drive.metadata.SearchableOrderedMetadataField;
import com.google.android.gms.drive.metadata.SortableMetadataField;
import com.google.android.gms.drive.metadata.internal.zzd;
import java.util.Date;

public class zzno
{
  public static final zza a = new zza("created", 4100000);
  public static final zzb b = new zzb("lastOpenedTime", 4300000);
  public static final zzd c = new zzd("modified", 4100000);
  public static final zzc d = new zzc("modifiedByMe", 4100000);
  public static final zzf e = new zzf("sharedWithMe", 4100000);
  public static final zze f = new zze("recency", 8000000);
  
  public static class zza
    extends zzd
    implements SortableMetadataField<Date>
  {
    public zza(String paramString, int paramInt)
    {
      super(paramInt);
    }
  }
  
  public static class zzb
    extends zzd
    implements SearchableOrderedMetadataField<Date>, SortableMetadataField<Date>
  {
    public zzb(String paramString, int paramInt)
    {
      super(paramInt);
    }
  }
  
  public static class zzc
    extends zzd
    implements SortableMetadataField<Date>
  {
    public zzc(String paramString, int paramInt)
    {
      super(paramInt);
    }
  }
  
  public static class zzd
    extends zzd
    implements SearchableOrderedMetadataField<Date>, SortableMetadataField<Date>
  {
    public zzd(String paramString, int paramInt)
    {
      super(paramInt);
    }
  }
  
  public static class zze
    extends zzd
    implements SortableMetadataField<Date>
  {
    public zze(String paramString, int paramInt)
    {
      super(paramInt);
    }
  }
  
  public static class zzf
    extends zzd
    implements SearchableOrderedMetadataField<Date>, SortableMetadataField<Date>
  {
    public zzf(String paramString, int paramInt)
    {
      super(paramInt);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzno.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */