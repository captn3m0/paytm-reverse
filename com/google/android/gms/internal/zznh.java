package com.google.android.gms.internal;

import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.zzx;
import java.util.Set;

public final class zznh
{
  public static String[] a(Set<Scope> paramSet)
  {
    zzx.a(paramSet, "scopes can't be null.");
    return a((Scope[])paramSet.toArray(new Scope[paramSet.size()]));
  }
  
  public static String[] a(Scope[] paramArrayOfScope)
  {
    zzx.a(paramArrayOfScope, "scopes can't be null.");
    String[] arrayOfString = new String[paramArrayOfScope.length];
    int i = 0;
    while (i < paramArrayOfScope.length)
    {
      arrayOfString[i] = paramArrayOfScope[i].a();
      i += 1;
    }
    return arrayOfString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zznh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */