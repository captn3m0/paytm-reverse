package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.Looper;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zza;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzx;

public class zzof
  extends zzny<zzoq>
{
  public zzof(Context paramContext, Looper paramLooper, zzf paramzzf, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramContext, paramLooper, 58, paramConnectionCallbacks, paramOnConnectionFailedListener, paramzzf);
  }
  
  protected zzoq a(IBinder paramIBinder)
  {
    return zzoq.zza.a(paramIBinder);
  }
  
  protected String a()
  {
    return "com.google.android.gms.fitness.SessionsApi";
  }
  
  protected String b()
  {
    return "com.google.android.gms.fitness.internal.IGoogleFitSessionsApi";
  }
  
  static abstract class zza<R extends Result>
    extends zza.zza<R, zzof>
  {}
  
  public static class zzb
    extends Api.zza<zzof, Api.ApiOptions.NoOptions>
  {
    public zzof a(Context paramContext, Looper paramLooper, zzf paramzzf, Api.ApiOptions.NoOptions paramNoOptions, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
    {
      return new zzof(paramContext, paramLooper, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    }
  }
  
  static abstract class zzc
    extends zzof.zza<Status>
  {
    protected Status a(Status paramStatus)
    {
      if (!paramStatus.d()) {}
      for (boolean bool = true;; bool = false)
      {
        zzx.b(bool);
        return paramStatus;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzof.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */