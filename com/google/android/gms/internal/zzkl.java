package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Intent;
import android.os.RemoteException;
import com.google.android.gms.appinvite.AppInviteApi;
import com.google.android.gms.appinvite.AppInviteInvitationResult;
import com.google.android.gms.appinvite.AppInviteReferral;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zza;

public class zzkl
  implements AppInviteApi
{
  static class zza
    extends zzko.zza
  {
    public void a(Status paramStatus)
      throws RemoteException
    {
      throw new UnsupportedOperationException();
    }
    
    public void a(Status paramStatus, Intent paramIntent)
    {
      throw new UnsupportedOperationException();
    }
  }
  
  static abstract class zzb<R extends Result>
    extends zza.zza<R, zzkm>
  {}
  
  final class zzc
    extends zzkl.zzb<Status>
  {
    private final String a;
    
    protected Status a(Status paramStatus)
    {
      return paramStatus;
    }
    
    protected void a(zzkm paramzzkm)
      throws RemoteException
    {
      paramzzkm.b(new zzkl.zza()
      {
        public void a(Status paramAnonymousStatus)
          throws RemoteException
        {
          zzkl.zzc.this.zza(paramAnonymousStatus);
        }
      }, this.a);
    }
  }
  
  final class zzd
    extends zzkl.zzb<Status>
  {
    private final String a;
    
    protected Status a(Status paramStatus)
    {
      return paramStatus;
    }
    
    protected void a(zzkm paramzzkm)
      throws RemoteException
    {
      paramzzkm.a(new zzkl.zza()
      {
        public void a(Status paramAnonymousStatus)
          throws RemoteException
        {
          zzkl.zzd.this.zza(paramAnonymousStatus);
        }
      }, this.a);
    }
  }
  
  final class zze
    extends zzkl.zzb<AppInviteInvitationResult>
  {
    private final Activity a;
    private final boolean b;
    private final Intent c;
    
    protected AppInviteInvitationResult a(Status paramStatus)
    {
      return new zzkn(paramStatus, new Intent());
    }
    
    protected void a(zzkm paramzzkm)
      throws RemoteException
    {
      if (AppInviteReferral.a(this.c))
      {
        zza(new zzkn(Status.a, this.c));
        return;
      }
      paramzzkm.a(new zzkl.zza()
      {
        public void a(Status paramAnonymousStatus, Intent paramAnonymousIntent)
        {
          zzkl.zze.this.zza(new zzkn(paramAnonymousStatus, paramAnonymousIntent));
          if ((AppInviteReferral.a(paramAnonymousIntent)) && (zzkl.zze.a(zzkl.zze.this)) && (zzkl.zze.b(zzkl.zze.this) != null)) {
            zzkl.zze.b(zzkl.zze.this).startActivity(paramAnonymousIntent);
          }
        }
      });
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzkl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */