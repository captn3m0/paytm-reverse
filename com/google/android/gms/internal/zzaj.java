package com.google.android.gms.internal;

import android.content.Context;
import android.view.MotionEvent;

public abstract interface zzaj
{
  public abstract String a(Context paramContext);
  
  public abstract String a(Context paramContext, String paramString);
  
  public abstract void a(int paramInt1, int paramInt2, int paramInt3);
  
  public abstract void a(MotionEvent paramMotionEvent);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzaj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */