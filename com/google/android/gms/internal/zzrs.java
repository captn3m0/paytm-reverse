package com.google.android.gms.internal;

import com.google.android.gms.tagmanager.zzbg;
import com.google.android.gms.tagmanager.zzdf;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class zzrs
{
  private static zzag.zza a(int paramInt, zzaf.zzf paramzzf, zzag.zza[] paramArrayOfzza, Set<Integer> paramSet)
    throws zzrs.zzg
  {
    int k = 0;
    int m = 0;
    int j = 0;
    if (paramSet.contains(Integer.valueOf(paramInt))) {
      a("Value cycle detected.  Current value reference: " + paramInt + "." + "  Previous value references: " + paramSet + ".");
    }
    zzag.zza localzza1 = (zzag.zza)a(paramzzf.c, paramInt, "values");
    if (paramArrayOfzza[paramInt] != null) {
      return paramArrayOfzza[paramInt];
    }
    Object localObject = null;
    paramSet.add(Integer.valueOf(paramInt));
    switch (localzza1.a)
    {
    }
    for (;;)
    {
      if (localObject == null) {
        a("Invalid value: " + localzza1);
      }
      paramArrayOfzza[paramInt] = localObject;
      paramSet.remove(Integer.valueOf(paramInt));
      return (zzag.zza)localObject;
      localObject = b(localzza1);
      zzag.zza localzza2 = a(localzza1);
      localzza2.c = new zzag.zza[((zzaf.zzh)localObject).b.length];
      int[] arrayOfInt = ((zzaf.zzh)localObject).b;
      k = arrayOfInt.length;
      int i = 0;
      for (;;)
      {
        localObject = localzza2;
        if (j >= k) {
          break;
        }
        m = arrayOfInt[j];
        localzza2.c[i] = a(m, paramzzf, paramArrayOfzza, paramSet);
        j += 1;
        i += 1;
      }
      localzza2 = a(localzza1);
      localObject = b(localzza1);
      if (((zzaf.zzh)localObject).c.length != ((zzaf.zzh)localObject).d.length) {
        a("Uneven map keys (" + ((zzaf.zzh)localObject).c.length + ") and map values (" + ((zzaf.zzh)localObject).d.length + ")");
      }
      localzza2.d = new zzag.zza[((zzaf.zzh)localObject).c.length];
      localzza2.e = new zzag.zza[((zzaf.zzh)localObject).c.length];
      arrayOfInt = ((zzaf.zzh)localObject).c;
      m = arrayOfInt.length;
      j = 0;
      i = 0;
      while (j < m)
      {
        int n = arrayOfInt[j];
        localzza2.d[i] = a(n, paramzzf, paramArrayOfzza, paramSet);
        j += 1;
        i += 1;
      }
      arrayOfInt = ((zzaf.zzh)localObject).d;
      m = arrayOfInt.length;
      i = 0;
      j = k;
      for (;;)
      {
        localObject = localzza2;
        if (j >= m) {
          break;
        }
        k = arrayOfInt[j];
        localzza2.e[i] = a(k, paramzzf, paramArrayOfzza, paramSet);
        j += 1;
        i += 1;
      }
      localObject = a(localzza1);
      ((zzag.zza)localObject).f = zzdf.zzg(a(b(localzza1).g, paramzzf, paramArrayOfzza, paramSet));
      continue;
      localzza2 = a(localzza1);
      localObject = b(localzza1);
      localzza2.j = new zzag.zza[((zzaf.zzh)localObject).f.length];
      arrayOfInt = ((zzaf.zzh)localObject).f;
      k = arrayOfInt.length;
      i = 0;
      j = m;
      for (;;)
      {
        localObject = localzza2;
        if (j >= k) {
          break;
        }
        m = arrayOfInt[j];
        localzza2.j[i] = a(m, paramzzf, paramArrayOfzza, paramSet);
        j += 1;
        i += 1;
      }
      localObject = localzza1;
    }
  }
  
  public static zzag.zza a(zzag.zza paramzza)
  {
    zzag.zza localzza = new zzag.zza();
    localzza.a = paramzza.a;
    localzza.k = ((int[])paramzza.k.clone());
    if (paramzza.l) {
      localzza.l = paramzza.l;
    }
    return localzza;
  }
  
  private static zza a(zzaf.zzb paramzzb, zzaf.zzf paramzzf, zzag.zza[] paramArrayOfzza, int paramInt)
    throws zzrs.zzg
  {
    zzb localzzb = zza.a();
    paramzzb = paramzzb.a;
    int i = paramzzb.length;
    paramInt = 0;
    if (paramInt < i)
    {
      int j = paramzzb[paramInt];
      Object localObject = (zzaf.zze)a(paramzzf.d, Integer.valueOf(j).intValue(), "properties");
      String str = (String)a(paramzzf.b, ((zzaf.zze)localObject).a, "keys");
      localObject = (zzag.zza)a(paramArrayOfzza, ((zzaf.zze)localObject).b, "values");
      if (zzae.cp.toString().equals(str)) {
        localzzb.a((zzag.zza)localObject);
      }
      for (;;)
      {
        paramInt += 1;
        break;
        localzzb.a(str, (zzag.zza)localObject);
      }
    }
    return localzzb.a();
  }
  
  public static zzc a(zzaf.zzf paramzzf)
    throws zzrs.zzg
  {
    int j = 0;
    Object localObject = new zzag.zza[paramzzf.c.length];
    int i = 0;
    while (i < paramzzf.c.length)
    {
      a(i, paramzzf, (zzag.zza[])localObject, new HashSet(0));
      i += 1;
    }
    zzd localzzd = zzc.a();
    ArrayList localArrayList1 = new ArrayList();
    i = 0;
    while (i < paramzzf.f.length)
    {
      localArrayList1.add(a(paramzzf.f[i], paramzzf, (zzag.zza[])localObject, i));
      i += 1;
    }
    ArrayList localArrayList2 = new ArrayList();
    i = 0;
    while (i < paramzzf.g.length)
    {
      localArrayList2.add(a(paramzzf.g[i], paramzzf, (zzag.zza[])localObject, i));
      i += 1;
    }
    ArrayList localArrayList3 = new ArrayList();
    i = 0;
    while (i < paramzzf.e.length)
    {
      zza localzza = a(paramzzf.e[i], paramzzf, (zzag.zza[])localObject, i);
      localzzd.a(localzza);
      localArrayList3.add(localzza);
      i += 1;
    }
    localObject = paramzzf.h;
    int k = localObject.length;
    i = j;
    while (i < k)
    {
      localzzd.a(a(localObject[i], localArrayList1, localArrayList3, localArrayList2, paramzzf));
      i += 1;
    }
    localzzd.a(paramzzf.l);
    localzzd.a(paramzzf.q);
    return localzzd.a();
  }
  
  private static zze a(zzaf.zzg paramzzg, List<zza> paramList1, List<zza> paramList2, List<zza> paramList3, zzaf.zzf paramzzf)
  {
    zzf localzzf = zze.a();
    int[] arrayOfInt = paramzzg.a;
    int j = arrayOfInt.length;
    int i = 0;
    while (i < j)
    {
      localzzf.a((zza)paramList3.get(Integer.valueOf(arrayOfInt[i]).intValue()));
      i += 1;
    }
    arrayOfInt = paramzzg.b;
    j = arrayOfInt.length;
    i = 0;
    while (i < j)
    {
      localzzf.b((zza)paramList3.get(Integer.valueOf(arrayOfInt[i]).intValue()));
      i += 1;
    }
    paramList3 = paramzzg.c;
    j = paramList3.length;
    i = 0;
    while (i < j)
    {
      localzzf.c((zza)paramList1.get(Integer.valueOf(paramList3[i]).intValue()));
      i += 1;
    }
    paramList3 = paramzzg.e;
    j = paramList3.length;
    i = 0;
    int k;
    while (i < j)
    {
      k = paramList3[i];
      localzzf.a(paramzzf.c[Integer.valueOf(k).intValue()].b);
      i += 1;
    }
    paramList3 = paramzzg.d;
    j = paramList3.length;
    i = 0;
    while (i < j)
    {
      localzzf.d((zza)paramList1.get(Integer.valueOf(paramList3[i]).intValue()));
      i += 1;
    }
    paramList1 = paramzzg.f;
    j = paramList1.length;
    i = 0;
    while (i < j)
    {
      k = paramList1[i];
      localzzf.b(paramzzf.c[Integer.valueOf(k).intValue()].b);
      i += 1;
    }
    paramList1 = paramzzg.g;
    j = paramList1.length;
    i = 0;
    while (i < j)
    {
      localzzf.e((zza)paramList2.get(Integer.valueOf(paramList1[i]).intValue()));
      i += 1;
    }
    paramList1 = paramzzg.i;
    j = paramList1.length;
    i = 0;
    while (i < j)
    {
      k = paramList1[i];
      localzzf.c(paramzzf.c[Integer.valueOf(k).intValue()].b);
      i += 1;
    }
    paramList1 = paramzzg.h;
    j = paramList1.length;
    i = 0;
    while (i < j)
    {
      localzzf.f((zza)paramList2.get(Integer.valueOf(paramList1[i]).intValue()));
      i += 1;
    }
    paramzzg = paramzzg.j;
    j = paramzzg.length;
    i = 0;
    while (i < j)
    {
      k = paramzzg[i];
      localzzf.d(paramzzf.c[Integer.valueOf(k).intValue()].b);
      i += 1;
    }
    return localzzf.a();
  }
  
  private static <T> T a(T[] paramArrayOfT, int paramInt, String paramString)
    throws zzrs.zzg
  {
    if ((paramInt < 0) || (paramInt >= paramArrayOfT.length)) {
      a("Index out of bounds detected: " + paramInt + " in " + paramString);
    }
    return paramArrayOfT[paramInt];
  }
  
  public static void a(InputStream paramInputStream, OutputStream paramOutputStream)
    throws IOException
  {
    byte[] arrayOfByte = new byte['Ѐ'];
    for (;;)
    {
      int i = paramInputStream.read(arrayOfByte);
      if (i == -1) {
        return;
      }
      paramOutputStream.write(arrayOfByte, 0, i);
    }
  }
  
  private static void a(String paramString)
    throws zzrs.zzg
  {
    zzbg.e(paramString);
    throw new zzg(paramString);
  }
  
  private static zzaf.zzh b(zzag.zza paramzza)
    throws zzrs.zzg
  {
    if ((zzaf.zzh)paramzza.a(zzaf.zzh.a) == null) {
      a("Expected a ServingValue and didn't get one. Value is: " + paramzza);
    }
    return (zzaf.zzh)paramzza.a(zzaf.zzh.a);
  }
  
  public static class zza
  {
    private final Map<String, zzag.zza> a;
    private final zzag.zza b;
    
    private zza(Map<String, zzag.zza> paramMap, zzag.zza paramzza)
    {
      this.a = paramMap;
      this.b = paramzza;
    }
    
    public static zzrs.zzb a()
    {
      return new zzrs.zzb(null);
    }
    
    public void a(String paramString, zzag.zza paramzza)
    {
      this.a.put(paramString, paramzza);
    }
    
    public Map<String, zzag.zza> b()
    {
      return Collections.unmodifiableMap(this.a);
    }
    
    public zzag.zza c()
    {
      return this.b;
    }
    
    public String toString()
    {
      return "Properties: " + b() + " pushAfterEvaluate: " + this.b;
    }
  }
  
  public static class zzb
  {
    private final Map<String, zzag.zza> a = new HashMap();
    private zzag.zza b;
    
    public zzrs.zza a()
    {
      return new zzrs.zza(this.a, this.b, null);
    }
    
    public zzb a(zzag.zza paramzza)
    {
      this.b = paramzza;
      return this;
    }
    
    public zzb a(String paramString, zzag.zza paramzza)
    {
      this.a.put(paramString, paramzza);
      return this;
    }
  }
  
  public static class zzc
  {
    private final List<zzrs.zze> a;
    private final Map<String, List<zzrs.zza>> b;
    private final String c;
    private final int d;
    
    private zzc(List<zzrs.zze> paramList, Map<String, List<zzrs.zza>> paramMap, String paramString, int paramInt)
    {
      this.a = Collections.unmodifiableList(paramList);
      this.b = Collections.unmodifiableMap(paramMap);
      this.c = paramString;
      this.d = paramInt;
    }
    
    public static zzrs.zzd a()
    {
      return new zzrs.zzd(null);
    }
    
    public List<zzrs.zze> b()
    {
      return this.a;
    }
    
    public String c()
    {
      return this.c;
    }
    
    public Map<String, List<zzrs.zza>> d()
    {
      return this.b;
    }
    
    public String toString()
    {
      return "Rules: " + b() + "  Macros: " + this.b;
    }
  }
  
  public static class zzd
  {
    private final List<zzrs.zze> a = new ArrayList();
    private final Map<String, List<zzrs.zza>> b = new HashMap();
    private String c = "";
    private int d = 0;
    
    public zzrs.zzc a()
    {
      return new zzrs.zzc(this.a, this.b, this.c, this.d, null);
    }
    
    public zzd a(int paramInt)
    {
      this.d = paramInt;
      return this;
    }
    
    public zzd a(zzrs.zza paramzza)
    {
      String str = zzdf.zzg((zzag.zza)paramzza.b().get(zzae.bm.toString()));
      List localList = (List)this.b.get(str);
      Object localObject = localList;
      if (localList == null)
      {
        localObject = new ArrayList();
        this.b.put(str, localObject);
      }
      ((List)localObject).add(paramzza);
      return this;
    }
    
    public zzd a(zzrs.zze paramzze)
    {
      this.a.add(paramzze);
      return this;
    }
    
    public zzd a(String paramString)
    {
      this.c = paramString;
      return this;
    }
  }
  
  public static class zze
  {
    private final List<zzrs.zza> a;
    private final List<zzrs.zza> b;
    private final List<zzrs.zza> c;
    private final List<zzrs.zza> d;
    private final List<zzrs.zza> e;
    private final List<zzrs.zza> f;
    private final List<String> g;
    private final List<String> h;
    private final List<String> i;
    private final List<String> j;
    
    private zze(List<zzrs.zza> paramList1, List<zzrs.zza> paramList2, List<zzrs.zza> paramList3, List<zzrs.zza> paramList4, List<zzrs.zza> paramList5, List<zzrs.zza> paramList6, List<String> paramList7, List<String> paramList8, List<String> paramList9, List<String> paramList10)
    {
      this.a = Collections.unmodifiableList(paramList1);
      this.b = Collections.unmodifiableList(paramList2);
      this.c = Collections.unmodifiableList(paramList3);
      this.d = Collections.unmodifiableList(paramList4);
      this.e = Collections.unmodifiableList(paramList5);
      this.f = Collections.unmodifiableList(paramList6);
      this.g = Collections.unmodifiableList(paramList7);
      this.h = Collections.unmodifiableList(paramList8);
      this.i = Collections.unmodifiableList(paramList9);
      this.j = Collections.unmodifiableList(paramList10);
    }
    
    public static zzrs.zzf a()
    {
      return new zzrs.zzf(null);
    }
    
    public List<zzrs.zza> b()
    {
      return this.a;
    }
    
    public List<zzrs.zza> c()
    {
      return this.b;
    }
    
    public List<zzrs.zza> d()
    {
      return this.c;
    }
    
    public List<zzrs.zza> e()
    {
      return this.d;
    }
    
    public List<zzrs.zza> f()
    {
      return this.e;
    }
    
    public List<String> g()
    {
      return this.g;
    }
    
    public List<String> h()
    {
      return this.h;
    }
    
    public List<String> i()
    {
      return this.i;
    }
    
    public List<String> j()
    {
      return this.j;
    }
    
    public List<zzrs.zza> k()
    {
      return this.f;
    }
    
    public String toString()
    {
      return "Positive predicates: " + b() + "  Negative predicates: " + c() + "  Add tags: " + d() + "  Remove tags: " + e() + "  Add macros: " + f() + "  Remove macros: " + k();
    }
  }
  
  public static class zzf
  {
    private final List<zzrs.zza> a = new ArrayList();
    private final List<zzrs.zza> b = new ArrayList();
    private final List<zzrs.zza> c = new ArrayList();
    private final List<zzrs.zza> d = new ArrayList();
    private final List<zzrs.zza> e = new ArrayList();
    private final List<zzrs.zza> f = new ArrayList();
    private final List<String> g = new ArrayList();
    private final List<String> h = new ArrayList();
    private final List<String> i = new ArrayList();
    private final List<String> j = new ArrayList();
    
    public zzrs.zze a()
    {
      return new zzrs.zze(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, null);
    }
    
    public zzf a(zzrs.zza paramzza)
    {
      this.a.add(paramzza);
      return this;
    }
    
    public zzf a(String paramString)
    {
      this.i.add(paramString);
      return this;
    }
    
    public zzf b(zzrs.zza paramzza)
    {
      this.b.add(paramzza);
      return this;
    }
    
    public zzf b(String paramString)
    {
      this.j.add(paramString);
      return this;
    }
    
    public zzf c(zzrs.zza paramzza)
    {
      this.c.add(paramzza);
      return this;
    }
    
    public zzf c(String paramString)
    {
      this.g.add(paramString);
      return this;
    }
    
    public zzf d(zzrs.zza paramzza)
    {
      this.d.add(paramzza);
      return this;
    }
    
    public zzf d(String paramString)
    {
      this.h.add(paramString);
      return this;
    }
    
    public zzf e(zzrs.zza paramzza)
    {
      this.e.add(paramzza);
      return this;
    }
    
    public zzf f(zzrs.zza paramzza)
    {
      this.f.add(paramzza);
      return this;
    }
  }
  
  public static class zzg
    extends Exception
  {
    public zzg(String paramString)
    {
      super();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzrs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */