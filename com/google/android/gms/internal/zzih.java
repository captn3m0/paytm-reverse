package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.security.NetworkSecurityPolicy;
import com.google.android.gms.ads.internal.purchase.zzi;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzr;
import com.google.android.gms.common.zze;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.Future;

@zzhb
public class zzih
  implements zzip.zzb
{
  private final Object a = new Object();
  private final String b;
  private final zzii c;
  private zzax d;
  private BigInteger e = BigInteger.ONE;
  private final HashSet<zzig> f = new HashSet();
  private final HashMap<String, zzik> g = new HashMap();
  private boolean h = false;
  private boolean i = true;
  private int j = 0;
  private boolean k = false;
  private Context l;
  private VersionInfoParcel m;
  private zzbv n = null;
  private boolean o = true;
  private zzbe p = null;
  private zzbf q = null;
  private zzbd r = null;
  private String s;
  private final LinkedList<Thread> t = new LinkedList();
  private final zzha u = null;
  private Boolean v = null;
  private String w;
  private boolean x = false;
  private boolean y = false;
  
  public zzih(zzir paramzzir)
  {
    this.b = paramzzir.c();
    this.c = new zzii(this.b);
  }
  
  public Bundle a(Context paramContext, zzij paramzzij, String paramString)
  {
    Bundle localBundle;
    synchronized (this.a)
    {
      localBundle = new Bundle();
      localBundle.putBundle("app", this.c.a(paramContext, paramString));
      paramContext = new Bundle();
      paramString = this.g.keySet().iterator();
      if (paramString.hasNext())
      {
        String str = (String)paramString.next();
        paramContext.putBundle(str, ((zzik)this.g.get(str)).a());
      }
    }
    localBundle.putBundle("slots", paramContext);
    paramContext = new ArrayList();
    paramString = this.f.iterator();
    while (paramString.hasNext()) {
      paramContext.add(((zzig)paramString.next()).d());
    }
    localBundle.putParcelableArrayList("ads", paramContext);
    paramzzij.a(this.f);
    this.f.clear();
    return localBundle;
  }
  
  public zzbf a(Context paramContext)
  {
    if ((!((Boolean)zzbt.J.c()).booleanValue()) || (!zzne.d()) || (b())) {
      return null;
    }
    synchronized (this.a)
    {
      if (this.p == null)
      {
        if (!(paramContext instanceof Activity)) {
          return null;
        }
        this.p = new zzbe((Application)paramContext.getApplicationContext(), (Activity)paramContext);
      }
      if (this.r == null) {
        this.r = new zzbd();
      }
      if (this.q == null) {
        this.q = new zzbf(this.p, this.r, new zzha(this.l, this.m, null, null));
      }
      this.q.a();
      paramContext = this.q;
      return paramContext;
    }
  }
  
  public String a()
  {
    return this.b;
  }
  
  public String a(int paramInt, String paramString)
  {
    if (this.m.e) {}
    for (Resources localResources = this.l.getResources(); localResources == null; localResources = zze.f(this.l)) {
      return paramString;
    }
    return localResources.getString(paramInt);
  }
  
  public Future a(Context paramContext, boolean paramBoolean)
  {
    synchronized (this.a)
    {
      if (paramBoolean != this.i)
      {
        this.i = paramBoolean;
        paramContext = zzip.a(paramContext, paramBoolean);
        return paramContext;
      }
      return null;
    }
  }
  
  public Future a(String paramString)
  {
    Object localObject = this.a;
    if (paramString != null) {}
    try
    {
      if (!paramString.equals(this.s))
      {
        this.s = paramString;
        paramString = zzip.a(this.l, paramString);
        return paramString;
      }
      return null;
    }
    finally {}
  }
  
  @TargetApi(23)
  public void a(Context paramContext, VersionInfoParcel paramVersionInfoParcel)
  {
    synchronized (this.a)
    {
      if (!this.k)
      {
        this.l = paramContext.getApplicationContext();
        this.m = paramVersionInfoParcel;
        zzip.a(paramContext, this);
        zzip.b(paramContext, this);
        zzip.c(paramContext, this);
        zzip.d(paramContext, this);
        a(Thread.currentThread());
        this.w = zzr.e().a(paramContext, paramVersionInfoParcel.b);
        if ((zzne.l()) && (!NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted())) {
          this.y = true;
        }
        this.d = new zzax(paramContext.getApplicationContext(), this.m, new zzeg(paramContext.getApplicationContext(), this.m, (String)zzbt.b.c()));
        n();
        zzr.o().a(this.l);
        this.k = true;
      }
      return;
    }
  }
  
  public void a(Bundle paramBundle)
  {
    synchronized (this.a)
    {
      if (paramBundle.containsKey("use_https")) {}
      for (boolean bool = paramBundle.getBoolean("use_https");; bool = this.i)
      {
        this.i = bool;
        if (!paramBundle.containsKey("webview_cache_version")) {
          break;
        }
        i1 = paramBundle.getInt("webview_cache_version");
        this.j = i1;
        if (paramBundle.containsKey("content_url_opted_out")) {
          a(paramBundle.getBoolean("content_url_opted_out"));
        }
        if (paramBundle.containsKey("content_url_hashes")) {
          this.s = paramBundle.getString("content_url_hashes");
        }
        return;
      }
      int i1 = this.j;
    }
  }
  
  public void a(zzig paramzzig)
  {
    synchronized (this.a)
    {
      this.f.add(paramzzig);
      return;
    }
  }
  
  public void a(Boolean paramBoolean)
  {
    synchronized (this.a)
    {
      this.v = paramBoolean;
      return;
    }
  }
  
  public void a(String paramString, zzik paramzzik)
  {
    synchronized (this.a)
    {
      this.g.put(paramString, paramzzik);
      return;
    }
  }
  
  public void a(Thread paramThread)
  {
    zzha.a(this.l, paramThread, this.m);
  }
  
  public void a(Throwable paramThrowable, boolean paramBoolean)
  {
    new zzha(this.l, this.m, null, null).a(paramThrowable, paramBoolean);
  }
  
  public void a(HashSet<zzig> paramHashSet)
  {
    synchronized (this.a)
    {
      this.f.addAll(paramHashSet);
      return;
    }
  }
  
  public void a(boolean paramBoolean)
  {
    synchronized (this.a)
    {
      if (this.o != paramBoolean) {
        zzip.b(this.l, paramBoolean);
      }
      this.o = paramBoolean;
      zzbf localzzbf = a(this.l);
      if ((localzzbf != null) && (!localzzbf.isAlive()))
      {
        zzin.c("start fetching content...");
        localzzbf.a();
      }
      return;
    }
  }
  
  public void b(boolean paramBoolean)
  {
    synchronized (this.a)
    {
      this.x = paramBoolean;
      return;
    }
  }
  
  public boolean b()
  {
    synchronized (this.a)
    {
      boolean bool = this.o;
      return bool;
    }
  }
  
  public String c()
  {
    synchronized (this.a)
    {
      String str = this.e.toString();
      this.e = this.e.add(BigInteger.ONE);
      return str;
    }
  }
  
  public zzii d()
  {
    synchronized (this.a)
    {
      zzii localzzii = this.c;
      return localzzii;
    }
  }
  
  public zzbv e()
  {
    synchronized (this.a)
    {
      zzbv localzzbv = this.n;
      return localzzbv;
    }
  }
  
  public boolean f()
  {
    synchronized (this.a)
    {
      boolean bool = this.h;
      this.h = true;
      return bool;
    }
  }
  
  public boolean g()
  {
    for (;;)
    {
      synchronized (this.a)
      {
        if (!this.i)
        {
          if (!this.y) {
            break label38;
          }
          break label33;
          return bool;
        }
      }
      label33:
      boolean bool = true;
      continue;
      label38:
      bool = false;
    }
  }
  
  public String h()
  {
    synchronized (this.a)
    {
      String str = this.w;
      return str;
    }
  }
  
  public String i()
  {
    synchronized (this.a)
    {
      String str = this.s;
      return str;
    }
  }
  
  public Boolean j()
  {
    synchronized (this.a)
    {
      Boolean localBoolean = this.v;
      return localBoolean;
    }
  }
  
  public zzax k()
  {
    return this.d;
  }
  
  public boolean l()
  {
    synchronized (this.a)
    {
      if (this.j < ((Integer)zzbt.aa.c()).intValue())
      {
        this.j = ((Integer)zzbt.aa.c()).intValue();
        zzip.a(this.l, this.j);
        return true;
      }
      return false;
    }
  }
  
  public boolean m()
  {
    synchronized (this.a)
    {
      boolean bool = this.x;
      return bool;
    }
  }
  
  void n()
  {
    zzbu localzzbu = new zzbu(this.l, this.m.b);
    try
    {
      this.n = zzr.j().a(localzzbu);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      zzin.d("Cannot initialize CSI reporter.", localIllegalArgumentException);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzih.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */