package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.measurement.zze;
import java.util.HashMap;
import java.util.Map;

public final class zzpu
  extends zze<zzpu>
{
  private String a;
  private String b;
  private String c;
  private long d;
  
  public String a()
  {
    return this.a;
  }
  
  public void a(long paramLong)
  {
    this.d = paramLong;
  }
  
  public void a(zzpu paramzzpu)
  {
    if (!TextUtils.isEmpty(this.a)) {
      paramzzpu.a(this.a);
    }
    if (!TextUtils.isEmpty(this.b)) {
      paramzzpu.b(this.b);
    }
    if (!TextUtils.isEmpty(this.c)) {
      paramzzpu.c(this.c);
    }
    if (this.d != 0L) {
      paramzzpu.a(this.d);
    }
  }
  
  public void a(String paramString)
  {
    this.a = paramString;
  }
  
  public String b()
  {
    return this.b;
  }
  
  public void b(String paramString)
  {
    this.b = paramString;
  }
  
  public String c()
  {
    return this.c;
  }
  
  public void c(String paramString)
  {
    this.c = paramString;
  }
  
  public long d()
  {
    return this.d;
  }
  
  public String toString()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("category", this.a);
    localHashMap.put("action", this.b);
    localHashMap.put("label", this.c);
    localHashMap.put("value", Long.valueOf(this.d));
    return a(localHashMap);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzpu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */