package com.google.android.gms.internal;

import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

public final class zzsi
{
  private static int a(String paramString, zzsj.zza.zza[] paramArrayOfzza)
  {
    int m = paramArrayOfzza.length;
    int i = 0;
    int j = 14;
    if (i < m)
    {
      zzsj.zza.zza localzza = paramArrayOfzza[i];
      int k;
      if (j == 14) {
        if ((localzza.a == 9) || (localzza.a == 2) || (localzza.a == 6)) {
          k = localzza.a;
        }
      }
      do
      {
        do
        {
          i += 1;
          j = k;
          break;
          k = j;
        } while (localzza.a == 14);
        throw new IllegalArgumentException("Unexpected TypedValue type: " + localzza.a + " for key " + paramString);
        k = j;
      } while (localzza.a == j);
      throw new IllegalArgumentException("The ArrayList elements should all be the same type, but ArrayList with key " + paramString + " contains items of type " + j + " and " + localzza.a);
    }
    return j;
  }
  
  static int a(List<Asset> paramList, Asset paramAsset)
  {
    paramList.add(paramAsset);
    return paramList.size() - 1;
  }
  
  public static zza a(DataMap paramDataMap)
  {
    zzsj localzzsj = new zzsj();
    ArrayList localArrayList = new ArrayList();
    localzzsj.a = a(paramDataMap, localArrayList);
    return new zza(localzzsj, localArrayList);
  }
  
  private static zzsj.zza.zza a(List<Asset> paramList, Object paramObject)
  {
    zzsj.zza.zza localzza1 = new zzsj.zza.zza();
    if (paramObject == null)
    {
      localzza1.a = 14;
      return localzza1;
    }
    localzza1.b = new zzsj.zza.zza.zza();
    if ((paramObject instanceof String))
    {
      localzza1.a = 2;
      localzza1.b.b = ((String)paramObject);
    }
    Object localObject2;
    Object localObject1;
    int i;
    Object localObject3;
    for (;;)
    {
      return localzza1;
      if ((paramObject instanceof Integer))
      {
        localzza1.a = 6;
        localzza1.b.f = ((Integer)paramObject).intValue();
      }
      else if ((paramObject instanceof Long))
      {
        localzza1.a = 5;
        localzza1.b.e = ((Long)paramObject).longValue();
      }
      else if ((paramObject instanceof Double))
      {
        localzza1.a = 3;
        localzza1.b.c = ((Double)paramObject).doubleValue();
      }
      else if ((paramObject instanceof Float))
      {
        localzza1.a = 4;
        localzza1.b.d = ((Float)paramObject).floatValue();
      }
      else if ((paramObject instanceof Boolean))
      {
        localzza1.a = 8;
        localzza1.b.h = ((Boolean)paramObject).booleanValue();
      }
      else if ((paramObject instanceof Byte))
      {
        localzza1.a = 7;
        localzza1.b.g = ((Byte)paramObject).byteValue();
      }
      else if ((paramObject instanceof byte[]))
      {
        localzza1.a = 1;
        localzza1.b.a = ((byte[])paramObject);
      }
      else if ((paramObject instanceof String[]))
      {
        localzza1.a = 11;
        localzza1.b.k = ((String[])paramObject);
      }
      else if ((paramObject instanceof long[]))
      {
        localzza1.a = 12;
        localzza1.b.l = ((long[])paramObject);
      }
      else if ((paramObject instanceof float[]))
      {
        localzza1.a = 15;
        localzza1.b.m = ((float[])paramObject);
      }
      else if ((paramObject instanceof Asset))
      {
        localzza1.a = 13;
        localzza1.b.n = a(paramList, (Asset)paramObject);
      }
      else
      {
        if (!(paramObject instanceof DataMap)) {
          break;
        }
        localzza1.a = 9;
        paramObject = (DataMap)paramObject;
        localObject2 = new TreeSet(((DataMap)paramObject).b());
        localObject1 = new zzsj.zza[((TreeSet)localObject2).size()];
        localObject2 = ((TreeSet)localObject2).iterator();
        i = 0;
        while (((Iterator)localObject2).hasNext())
        {
          localObject3 = (String)((Iterator)localObject2).next();
          localObject1[i] = new zzsj.zza();
          localObject1[i].a = ((String)localObject3);
          localObject1[i].b = a(paramList, ((DataMap)paramObject).a((String)localObject3));
          i += 1;
        }
        localzza1.b.i = ((zzsj.zza[])localObject1);
      }
    }
    int j;
    label587:
    zzsj.zza.zza localzza2;
    if ((paramObject instanceof ArrayList))
    {
      localzza1.a = 10;
      localObject2 = (ArrayList)paramObject;
      localObject3 = new zzsj.zza.zza[((ArrayList)localObject2).size()];
      paramObject = null;
      int k = ((ArrayList)localObject2).size();
      j = 0;
      i = 14;
      if (j < k)
      {
        localObject1 = ((ArrayList)localObject2).get(j);
        localzza2 = a(paramList, localObject1);
        if ((localzza2.a != 14) && (localzza2.a != 2) && (localzza2.a != 6) && (localzza2.a != 9)) {
          throw new IllegalArgumentException("The only ArrayList element types supported by DataBundleUtil are String, Integer, Bundle, and null, but this ArrayList contains a " + localObject1.getClass());
        }
        if ((i == 14) && (localzza2.a != 14))
        {
          i = localzza2.a;
          paramObject = localObject1;
        }
      }
    }
    for (;;)
    {
      localObject3[j] = localzza2;
      j += 1;
      break label587;
      if (localzza2.a != i)
      {
        throw new IllegalArgumentException("ArrayList elements must all be of the sameclass, but this one contains a " + paramObject.getClass() + " and a " + localObject1.getClass());
        localzza1.b.j = ((zzsj.zza.zza[])localObject3);
        break;
        throw new RuntimeException("newFieldValueFromValue: unexpected value " + paramObject.getClass().getSimpleName());
      }
    }
  }
  
  public static DataMap a(zza paramzza)
  {
    DataMap localDataMap = new DataMap();
    zzsj.zza[] arrayOfzza = paramzza.a.a;
    int j = arrayOfzza.length;
    int i = 0;
    while (i < j)
    {
      zzsj.zza localzza = arrayOfzza[i];
      a(paramzza.b, localDataMap, localzza.a, localzza.b);
      i += 1;
    }
    return localDataMap;
  }
  
  private static ArrayList a(List<Asset> paramList, zzsj.zza.zza.zza paramzza, int paramInt)
  {
    ArrayList localArrayList = new ArrayList(paramzza.j.length);
    paramzza = paramzza.j;
    int k = paramzza.length;
    int i = 0;
    if (i < k)
    {
      zzsj.zza[] arrayOfzza = paramzza[i];
      if (arrayOfzza.a == 14) {
        localArrayList.add(null);
      }
      for (;;)
      {
        i += 1;
        break;
        if (paramInt == 9)
        {
          DataMap localDataMap = new DataMap();
          arrayOfzza = arrayOfzza.b.i;
          int m = arrayOfzza.length;
          int j = 0;
          while (j < m)
          {
            zzsj.zza localzza = arrayOfzza[j];
            a(paramList, localDataMap, localzza.a, localzza.b);
            j += 1;
          }
          localArrayList.add(localDataMap);
        }
        else if (paramInt == 2)
        {
          localArrayList.add(arrayOfzza.b.b);
        }
        else
        {
          if (paramInt != 6) {
            break label191;
          }
          localArrayList.add(Integer.valueOf(arrayOfzza.b.f));
        }
      }
      label191:
      throw new IllegalArgumentException("Unexpected typeOfArrayList: " + paramInt);
    }
    return localArrayList;
  }
  
  private static void a(List<Asset> paramList, DataMap paramDataMap, String paramString, zzsj.zza.zza paramzza)
  {
    int i = paramzza.a;
    if (i == 14)
    {
      paramDataMap.a(paramString, null);
      return;
    }
    Object localObject1 = paramzza.b;
    if (i == 1)
    {
      paramDataMap.a(paramString, ((zzsj.zza.zza.zza)localObject1).a);
      return;
    }
    if (i == 11)
    {
      paramDataMap.a(paramString, ((zzsj.zza.zza.zza)localObject1).k);
      return;
    }
    if (i == 12)
    {
      paramDataMap.a(paramString, ((zzsj.zza.zza.zza)localObject1).l);
      return;
    }
    if (i == 15)
    {
      paramDataMap.a(paramString, ((zzsj.zza.zza.zza)localObject1).m);
      return;
    }
    if (i == 2)
    {
      paramDataMap.a(paramString, ((zzsj.zza.zza.zza)localObject1).b);
      return;
    }
    if (i == 3)
    {
      paramDataMap.a(paramString, ((zzsj.zza.zza.zza)localObject1).c);
      return;
    }
    if (i == 4)
    {
      paramDataMap.a(paramString, ((zzsj.zza.zza.zza)localObject1).d);
      return;
    }
    if (i == 5)
    {
      paramDataMap.a(paramString, ((zzsj.zza.zza.zza)localObject1).e);
      return;
    }
    if (i == 6)
    {
      paramDataMap.a(paramString, ((zzsj.zza.zza.zza)localObject1).f);
      return;
    }
    if (i == 7)
    {
      paramDataMap.a(paramString, (byte)((zzsj.zza.zza.zza)localObject1).g);
      return;
    }
    if (i == 8)
    {
      paramDataMap.a(paramString, ((zzsj.zza.zza.zza)localObject1).h);
      return;
    }
    if (i == 13)
    {
      if (paramList == null) {
        throw new RuntimeException("populateBundle: unexpected type for: " + paramString);
      }
      paramDataMap.a(paramString, (Asset)paramList.get((int)((zzsj.zza.zza.zza)localObject1).n));
      return;
    }
    if (i == 9)
    {
      paramzza = new DataMap();
      localObject1 = ((zzsj.zza.zza.zza)localObject1).i;
      int j = localObject1.length;
      i = 0;
      while (i < j)
      {
        Object localObject2 = localObject1[i];
        a(paramList, paramzza, ((zzsj.zza)localObject2).a, ((zzsj.zza)localObject2).b);
        i += 1;
      }
      paramDataMap.a(paramString, paramzza);
      return;
    }
    if (i == 10)
    {
      i = a(paramString, ((zzsj.zza.zza.zza)localObject1).j);
      paramList = a(paramList, (zzsj.zza.zza.zza)localObject1, i);
      if (i == 14)
      {
        paramDataMap.c(paramString, paramList);
        return;
      }
      if (i == 9)
      {
        paramDataMap.a(paramString, paramList);
        return;
      }
      if (i == 2)
      {
        paramDataMap.c(paramString, paramList);
        return;
      }
      if (i == 6)
      {
        paramDataMap.b(paramString, paramList);
        return;
      }
      throw new IllegalStateException("Unexpected typeOfArrayList: " + i);
    }
    throw new RuntimeException("populateBundle: unexpected type " + i);
  }
  
  private static zzsj.zza[] a(DataMap paramDataMap, List<Asset> paramList)
  {
    Object localObject1 = new TreeSet(paramDataMap.b());
    zzsj.zza[] arrayOfzza = new zzsj.zza[((TreeSet)localObject1).size()];
    localObject1 = ((TreeSet)localObject1).iterator();
    int i = 0;
    while (((Iterator)localObject1).hasNext())
    {
      String str = (String)((Iterator)localObject1).next();
      Object localObject2 = paramDataMap.a(str);
      arrayOfzza[i] = new zzsj.zza();
      arrayOfzza[i].a = str;
      arrayOfzza[i].b = a(paramList, localObject2);
      i += 1;
    }
    return arrayOfzza;
  }
  
  public static class zza
  {
    public final zzsj a;
    public final List<Asset> b;
    
    public zza(zzsj paramzzsj, List<Asset> paramList)
    {
      this.a = paramzzsj;
      this.b = paramList;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzsi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */