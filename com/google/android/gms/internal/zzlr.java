package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.CastRemoteDisplay.CastRemoteDisplaySessionCallbacks;
import com.google.android.gms.cast.internal.zzl;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;

public class zzlr
  extends zzj<zzlt>
  implements IBinder.DeathRecipient
{
  private static final zzl a = new zzl("CastRemoteDisplayClientImpl");
  private CastRemoteDisplay.CastRemoteDisplaySessionCallbacks e;
  private CastDevice f;
  
  public zzlr(Context paramContext, Looper paramLooper, zzf paramzzf, CastDevice paramCastDevice, CastRemoteDisplay.CastRemoteDisplaySessionCallbacks paramCastRemoteDisplaySessionCallbacks, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramContext, paramLooper, 83, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    a.b("instance created", new Object[0]);
    this.e = paramCastRemoteDisplaySessionCallbacks;
    this.f = paramCastDevice;
  }
  
  public zzlt a(IBinder paramIBinder)
  {
    return zzlt.zza.a(paramIBinder);
  }
  
  protected String a()
  {
    return "com.google.android.gms.cast.remote_display.service.START";
  }
  
  public void a(zzls paramzzls)
    throws RemoteException
  {
    a.b("stopRemoteDisplay", new Object[0]);
    ((zzlt)v()).a(paramzzls);
  }
  
  public void a(zzls paramzzls, int paramInt)
    throws RemoteException
  {
    ((zzlt)v()).a(paramzzls, paramInt);
  }
  
  public void a(zzls paramzzls, final zzlu paramzzlu, String paramString)
    throws RemoteException
  {
    a.b("startRemoteDisplay", new Object[0]);
    paramzzlu = new zzlu.zza()
    {
      public void a(int paramAnonymousInt)
        throws RemoteException
      {
        zzlr.h().b("onRemoteDisplayEnded", new Object[0]);
        if (paramzzlu != null) {
          paramzzlu.a(paramAnonymousInt);
        }
        if (zzlr.a(zzlr.this) != null) {
          zzlr.a(zzlr.this).a(new Status(paramAnonymousInt));
        }
      }
    };
    ((zzlt)v()).a(paramzzls, paramzzlu, this.f.b(), paramString);
  }
  
  protected String b()
  {
    return "com.google.android.gms.cast.remote_display.ICastRemoteDisplayService";
  }
  
  public void binderDied() {}
  
  /* Error */
  public void f()
  {
    // Byte code:
    //   0: getstatic 27	com/google/android/gms/internal/zzlr:a	Lcom/google/android/gms/cast/internal/zzl;
    //   3: ldc 94
    //   5: iconst_0
    //   6: anewarray 36	java/lang/Object
    //   9: invokevirtual 40	com/google/android/gms/cast/internal/zzl:b	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   12: aload_0
    //   13: aconst_null
    //   14: putfield 42	com/google/android/gms/internal/zzlr:e	Lcom/google/android/gms/cast/CastRemoteDisplay$CastRemoteDisplaySessionCallbacks;
    //   17: aload_0
    //   18: aconst_null
    //   19: putfield 44	com/google/android/gms/internal/zzlr:f	Lcom/google/android/gms/cast/CastDevice;
    //   22: aload_0
    //   23: invokevirtual 64	com/google/android/gms/internal/zzlr:v	()Landroid/os/IInterface;
    //   26: checkcast 66	com/google/android/gms/internal/zzlt
    //   29: invokeinterface 96 1 0
    //   34: aload_0
    //   35: invokespecial 98	com/google/android/gms/common/internal/zzj:f	()V
    //   38: return
    //   39: astore_1
    //   40: aload_0
    //   41: invokespecial 98	com/google/android/gms/common/internal/zzj:f	()V
    //   44: return
    //   45: astore_1
    //   46: aload_0
    //   47: invokespecial 98	com/google/android/gms/common/internal/zzj:f	()V
    //   50: aload_1
    //   51: athrow
    //   52: astore_1
    //   53: goto -13 -> 40
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	56	0	this	zzlr
    //   39	1	1	localRemoteException	RemoteException
    //   45	6	1	localObject	Object
    //   52	1	1	localIllegalStateException	IllegalStateException
    // Exception table:
    //   from	to	target	type
    //   22	34	39	android/os/RemoteException
    //   22	34	45	finally
    //   22	34	52	java/lang/IllegalStateException
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzlr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */