package com.google.android.gms.internal;

import android.content.Context;
import android.content.MutableContextWrapper;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzd;
import com.google.android.gms.ads.internal.zzk;

@zzhb
public class zzdv
{
  private MutableContextWrapper a;
  private final zzex b;
  private final VersionInfoParcel c;
  private final zzd d;
  
  zzdv(Context paramContext, zzex paramzzex, VersionInfoParcel paramVersionInfoParcel, zzd paramzzd)
  {
    this.a = new MutableContextWrapper(paramContext.getApplicationContext());
    this.b = paramzzex;
    this.c = paramVersionInfoParcel;
    this.d = paramzzd;
  }
  
  public zzk a(String paramString)
  {
    return new zzk(this.a, new AdSizeParcel(), paramString, this.b, this.c, this.d);
  }
  
  public zzdv a()
  {
    return new zzdv(this.a.getBaseContext(), this.b, this.c, this.d);
  }
  
  public MutableContextWrapper b()
  {
    return this.a;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzdv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */