package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.zzh.zza;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import java.util.Collections;
import java.util.List;
import org.json.JSONObject;

@zzhb
public class zzif
{
  public final String A;
  public final zzh.zza B;
  public boolean C = false;
  public boolean D = false;
  public final AdRequestParcel a;
  public final zzjp b;
  public final List<String> c;
  public final int d;
  public final List<String> e;
  public final List<String> f;
  public final int g;
  public final long h;
  public final String i;
  public final JSONObject j;
  public final boolean k;
  public boolean l;
  public final boolean m;
  public final zzen n;
  public final zzey o;
  public final String p;
  public final zzeo q;
  public final zzeq r;
  public final long s;
  public final AdSizeParcel t;
  public final long u;
  @Nullable
  public final RewardItemParcel v;
  @Nullable
  public final List<String> w;
  @Nullable
  public final List<String> x;
  public final long y;
  public final long z;
  
  public zzif(AdRequestParcel paramAdRequestParcel, zzjp paramzzjp, List<String> paramList1, int paramInt1, List<String> paramList2, List<String> paramList3, int paramInt2, long paramLong1, String paramString1, boolean paramBoolean1, zzen paramzzen, zzey paramzzey, String paramString2, zzeo paramzzeo, zzeq paramzzeq, long paramLong2, AdSizeParcel paramAdSizeParcel, long paramLong3, long paramLong4, long paramLong5, String paramString3, JSONObject paramJSONObject, zzh.zza paramzza, RewardItemParcel paramRewardItemParcel, List<String> paramList4, List<String> paramList5, boolean paramBoolean2)
  {
    this.a = paramAdRequestParcel;
    this.b = paramzzjp;
    this.c = a(paramList1);
    this.d = paramInt1;
    this.e = a(paramList2);
    this.f = a(paramList3);
    this.g = paramInt2;
    this.h = paramLong1;
    this.i = paramString1;
    this.m = paramBoolean1;
    this.n = paramzzen;
    this.o = paramzzey;
    this.p = paramString2;
    this.q = paramzzeo;
    this.r = paramzzeq;
    this.s = paramLong2;
    this.t = paramAdSizeParcel;
    this.u = paramLong3;
    this.y = paramLong4;
    this.z = paramLong5;
    this.A = paramString3;
    this.j = paramJSONObject;
    this.B = paramzza;
    this.v = paramRewardItemParcel;
    this.w = a(paramList4);
    this.x = a(paramList5);
    this.k = paramBoolean2;
  }
  
  public zzif(zza paramzza, zzjp paramzzjp, zzen paramzzen, zzey paramzzey, String paramString, zzeq paramzzeq, zzh.zza paramzza1)
  {
    this(paramzza.a.c, paramzzjp, paramzza.b.d, paramzza.e, paramzza.b.f, paramzza.b.j, paramzza.b.l, paramzza.b.k, paramzza.a.i, paramzza.b.h, paramzzen, paramzzey, paramString, paramzza.c, paramzzeq, paramzza.b.i, paramzza.d, paramzza.b.g, paramzza.f, paramzza.g, paramzza.b.o, paramzza.h, paramzza1, paramzza.b.D, paramzza.b.E, paramzza.b.E, paramzza.b.G);
  }
  
  @Nullable
  private static <T> List<T> a(@Nullable List<T> paramList)
  {
    if (paramList == null) {
      return null;
    }
    return Collections.unmodifiableList(paramList);
  }
  
  public boolean a()
  {
    if ((this.b == null) || (this.b.l() == null)) {
      return false;
    }
    return this.b.l().b();
  }
  
  @zzhb
  public static final class zza
  {
    public final AdRequestInfoParcel a;
    public final AdResponseParcel b;
    public final zzeo c;
    public final AdSizeParcel d;
    public final int e;
    public final long f;
    public final long g;
    public final JSONObject h;
    
    public zza(AdRequestInfoParcel paramAdRequestInfoParcel, AdResponseParcel paramAdResponseParcel, zzeo paramzzeo, AdSizeParcel paramAdSizeParcel, int paramInt, long paramLong1, long paramLong2, JSONObject paramJSONObject)
    {
      this.a = paramAdRequestInfoParcel;
      this.b = paramAdResponseParcel;
      this.c = paramzzeo;
      this.d = paramAdSizeParcel;
      this.e = paramInt;
      this.f = paramLong1;
      this.g = paramLong2;
      this.h = paramJSONObject;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzif.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */