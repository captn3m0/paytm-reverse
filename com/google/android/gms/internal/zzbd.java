package com.google.android.gms.internal;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@zzhb
public class zzbd
{
  private final Object a = new Object();
  private int b;
  private List<zzbc> c = new LinkedList();
  
  public zzbc a()
  {
    Object localObject1 = null;
    label146:
    label149:
    for (;;)
    {
      synchronized (this.a)
      {
        if (this.c.size() == 0)
        {
          zzin.a("Queue empty");
          return null;
        }
        if (this.c.size() >= 2)
        {
          int i = Integer.MIN_VALUE;
          Iterator localIterator = this.c.iterator();
          if (localIterator.hasNext())
          {
            zzbc localzzbc2 = (zzbc)localIterator.next();
            int j = localzzbc2.h();
            if (j <= i) {
              break label146;
            }
            localObject1 = localzzbc2;
            i = j;
            break label149;
          }
          this.c.remove(localObject1);
          return (zzbc)localObject1;
        }
      }
      zzbc localzzbc1 = (zzbc)this.c.get(0);
      localzzbc1.d();
      return localzzbc1;
    }
  }
  
  public boolean a(zzbc paramzzbc)
  {
    synchronized (this.a)
    {
      return this.c.contains(paramzzbc);
    }
  }
  
  public boolean b(zzbc paramzzbc)
  {
    synchronized (this.a)
    {
      Iterator localIterator = this.c.iterator();
      while (localIterator.hasNext())
      {
        zzbc localzzbc = (zzbc)localIterator.next();
        if ((paramzzbc != localzzbc) && (localzzbc.b().equals(paramzzbc.b())))
        {
          localIterator.remove();
          return true;
        }
      }
      return false;
    }
  }
  
  public void c(zzbc paramzzbc)
  {
    synchronized (this.a)
    {
      if (this.c.size() >= 10)
      {
        zzin.a("Queue is full, current size = " + this.c.size());
        this.c.remove(0);
      }
      int i = this.b;
      this.b = (i + 1);
      paramzzbc.a(i);
      this.c.add(paramzzbc);
      return;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzbd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */