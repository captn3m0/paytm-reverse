package com.google.android.gms.internal;

import android.content.Context;
import android.view.View;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.zzh;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.WeakHashMap;

@zzhb
public class zzax
  implements zzay
{
  private final Object a = new Object();
  private final WeakHashMap<zzif, zzau> b = new WeakHashMap();
  private final ArrayList<zzau> c = new ArrayList();
  private final Context d;
  private final VersionInfoParcel e;
  private final zzeg f;
  
  public zzax(Context paramContext, VersionInfoParcel paramVersionInfoParcel, zzeg paramzzeg)
  {
    this.d = paramContext.getApplicationContext();
    this.e = paramVersionInfoParcel;
    this.f = paramzzeg;
  }
  
  public zzau a(AdSizeParcel paramAdSizeParcel, zzif paramzzif)
  {
    return a(paramAdSizeParcel, paramzzif, paramzzif.b.b());
  }
  
  public zzau a(AdSizeParcel paramAdSizeParcel, zzif paramzzif, View paramView)
  {
    return a(paramAdSizeParcel, paramzzif, new zzau.zzd(paramView, paramzzif), null);
  }
  
  public zzau a(AdSizeParcel paramAdSizeParcel, zzif paramzzif, View paramView, zzeh paramzzeh)
  {
    return a(paramAdSizeParcel, paramzzif, new zzau.zzd(paramView, paramzzif), paramzzeh);
  }
  
  public zzau a(AdSizeParcel paramAdSizeParcel, zzif paramzzif, zzh paramzzh)
  {
    return a(paramAdSizeParcel, paramzzif, new zzau.zza(paramzzh), null);
  }
  
  public zzau a(AdSizeParcel paramAdSizeParcel, zzif paramzzif, zzbb paramzzbb, zzeh paramzzeh)
  {
    for (;;)
    {
      synchronized (this.a)
      {
        if (a(paramzzif))
        {
          paramAdSizeParcel = (zzau)this.b.get(paramzzif);
          return paramAdSizeParcel;
        }
        if (paramzzeh != null)
        {
          paramAdSizeParcel = new zzaz(this.d, paramAdSizeParcel, paramzzif, this.e, paramzzbb, paramzzeh);
          paramAdSizeParcel.a(this);
          this.b.put(paramzzif, paramAdSizeParcel);
          this.c.add(paramAdSizeParcel);
          return paramAdSizeParcel;
        }
      }
      paramAdSizeParcel = new zzba(this.d, paramAdSizeParcel, paramzzif, this.e, paramzzbb, this.f);
    }
  }
  
  public void a(zzau paramzzau)
  {
    synchronized (this.a)
    {
      if (!paramzzau.f())
      {
        this.c.remove(paramzzau);
        Iterator localIterator = this.b.entrySet().iterator();
        while (localIterator.hasNext()) {
          if (((Map.Entry)localIterator.next()).getValue() == paramzzau) {
            localIterator.remove();
          }
        }
      }
    }
  }
  
  public boolean a(zzif paramzzif)
  {
    for (;;)
    {
      synchronized (this.a)
      {
        paramzzif = (zzau)this.b.get(paramzzif);
        if ((paramzzif != null) && (paramzzif.f()))
        {
          bool = true;
          return bool;
        }
      }
      boolean bool = false;
    }
  }
  
  public void b(zzif paramzzif)
  {
    synchronized (this.a)
    {
      paramzzif = (zzau)this.b.get(paramzzif);
      if (paramzzif != null) {
        paramzzif.d();
      }
      return;
    }
  }
  
  public void c(zzif paramzzif)
  {
    synchronized (this.a)
    {
      paramzzif = (zzau)this.b.get(paramzzif);
      if (paramzzif != null) {
        paramzzif.n();
      }
      return;
    }
  }
  
  public void d(zzif paramzzif)
  {
    synchronized (this.a)
    {
      paramzzif = (zzau)this.b.get(paramzzif);
      if (paramzzif != null) {
        paramzzif.o();
      }
      return;
    }
  }
  
  public void e(zzif paramzzif)
  {
    synchronized (this.a)
    {
      paramzzif = (zzau)this.b.get(paramzzif);
      if (paramzzif != null) {
        paramzzif.p();
      }
      return;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzax.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */