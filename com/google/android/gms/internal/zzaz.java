package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import org.json.JSONObject;

@zzhb
public class zzaz
  extends zzau
{
  private final zzeh d;
  
  public zzaz(Context paramContext, AdSizeParcel paramAdSizeParcel, zzif paramzzif, VersionInfoParcel paramVersionInfoParcel, zzbb paramzzbb, zzeh paramzzeh)
  {
    super(paramContext, paramAdSizeParcel, paramzzif, paramVersionInfoParcel, paramzzbb);
    this.d = paramzzeh;
    a(this.d);
    a();
    b(false);
    zzin.a("Tracking ad unit: " + this.b.d());
  }
  
  protected void b(JSONObject paramJSONObject)
  {
    this.d.a("AFMA_updateActiveView", paramJSONObject);
  }
  
  protected void c()
  {
    synchronized (this.a)
    {
      super.c();
      b(this.d);
      return;
    }
  }
  
  public void d()
  {
    c();
  }
  
  protected boolean j()
  {
    return true;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzaz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */