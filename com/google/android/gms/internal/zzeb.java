package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzp;
import com.google.android.gms.ads.internal.client.zzq;
import com.google.android.gms.ads.internal.client.zzu.zza;
import com.google.android.gms.ads.internal.client.zzw;
import com.google.android.gms.ads.internal.client.zzx;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzk;
import com.google.android.gms.ads.internal.zzr;
import java.util.Set;

@zzhb
public class zzeb
  extends zzu.zza
{
  private String a;
  private zzdv b;
  private zzk c;
  private zzdx d;
  private zzgh e;
  private String f;
  
  public zzeb(Context paramContext, String paramString, zzex paramzzex, VersionInfoParcel paramVersionInfoParcel, com.google.android.gms.ads.internal.zzd paramzzd)
  {
    this(paramString, new zzdv(paramContext.getApplicationContext(), paramzzex, paramVersionInfoParcel, paramzzd));
  }
  
  public zzeb(String paramString, zzdv paramzzdv)
  {
    this.a = paramString;
    this.b = paramzzdv;
    this.d = new zzdx();
    zzr.p().a(paramzzdv);
  }
  
  private void m()
  {
    if ((this.c != null) && (this.e != null)) {
      this.c.a(this.e, this.f);
    }
  }
  
  public com.google.android.gms.dynamic.zzd a()
    throws RemoteException
  {
    if (this.c != null) {
      return this.c.a();
    }
    return null;
  }
  
  public void a(AdSizeParcel paramAdSizeParcel)
    throws RemoteException
  {
    if (this.c != null) {
      this.c.a(paramAdSizeParcel);
    }
  }
  
  public void a(zzp paramzzp)
    throws RemoteException
  {
    this.d.e = paramzzp;
    if (this.c != null) {
      this.d.a(this.c);
    }
  }
  
  public void a(zzq paramzzq)
    throws RemoteException
  {
    this.d.a = paramzzq;
    if (this.c != null) {
      this.d.a(this.c);
    }
  }
  
  public void a(zzw paramzzw)
    throws RemoteException
  {
    this.d.b = paramzzw;
    if (this.c != null) {
      this.d.a(this.c);
    }
  }
  
  public void a(zzx paramzzx)
    throws RemoteException
  {
    l();
    if (this.c != null) {
      this.c.a(paramzzx);
    }
  }
  
  public void a(com.google.android.gms.ads.internal.reward.client.zzd paramzzd)
  {
    this.d.f = paramzzd;
    if (this.c != null) {
      this.d.a(this.c);
    }
  }
  
  public void a(zzcf paramzzcf)
    throws RemoteException
  {
    this.d.d = paramzzcf;
    if (this.c != null) {
      this.d.a(this.c);
    }
  }
  
  public void a(zzgd paramzzgd)
    throws RemoteException
  {
    this.d.c = paramzzgd;
    if (this.c != null) {
      this.d.a(this.c);
    }
  }
  
  public void a(zzgh paramzzgh, String paramString)
    throws RemoteException
  {
    this.e = paramzzgh;
    this.f = paramString;
    m();
  }
  
  public void a(String paramString) {}
  
  public void a(boolean paramBoolean)
    throws RemoteException
  {
    l();
    if (this.c != null) {
      this.c.a(paramBoolean);
    }
  }
  
  public boolean a(AdRequestParcel paramAdRequestParcel)
    throws RemoteException
  {
    if (b(paramAdRequestParcel)) {
      l();
    }
    if (paramAdRequestParcel.j != null) {
      l();
    }
    if (this.c != null) {
      return this.c.a(paramAdRequestParcel);
    }
    zzea.zza localzza = zzr.p().a(paramAdRequestParcel, this.a);
    if (localzza != null)
    {
      if (!localzza.e) {
        localzza.a(paramAdRequestParcel);
      }
      this.c = localzza.a;
      localzza.a(this.b);
      localzza.c.a(this.d);
      this.d.a(this.c);
      m();
      return localzza.f;
    }
    this.c = this.b.a(this.a);
    this.d.a(this.c);
    m();
    return this.c.a(paramAdRequestParcel);
  }
  
  public void b()
    throws RemoteException
  {
    if (this.c != null) {
      this.c.b();
    }
  }
  
  boolean b(AdRequestParcel paramAdRequestParcel)
  {
    paramAdRequestParcel = paramAdRequestParcel.m;
    if (paramAdRequestParcel == null) {}
    do
    {
      return false;
      paramAdRequestParcel = paramAdRequestParcel.getBundle(AdMobAdapter.class.getCanonicalName());
    } while (paramAdRequestParcel == null);
    return paramAdRequestParcel.keySet().contains("gw");
  }
  
  public void b_()
    throws RemoteException
  {
    if (this.c != null) {
      this.c.b_();
    }
  }
  
  public boolean c()
    throws RemoteException
  {
    return (this.c != null) && (this.c.c());
  }
  
  public void d()
    throws RemoteException
  {
    if (this.c != null) {
      this.c.d();
    }
  }
  
  public void f()
    throws RemoteException
  {
    if (this.c != null)
    {
      this.c.f();
      return;
    }
    zzin.d("Interstitial ad must be loaded before showInterstitial().");
  }
  
  public void f_()
    throws RemoteException
  {
    if (this.c != null) {
      this.c.f_();
    }
  }
  
  public void h()
    throws RemoteException
  {
    if (this.c != null)
    {
      this.c.h();
      return;
    }
    zzin.d("Interstitial ad must be loaded before pingManualTrackingUrl().");
  }
  
  public AdSizeParcel i()
    throws RemoteException
  {
    if (this.c != null) {
      return this.c.i();
    }
    return null;
  }
  
  public String j()
    throws RemoteException
  {
    if (this.c != null) {
      return this.c.j();
    }
    return null;
  }
  
  public boolean k()
    throws RemoteException
  {
    return (this.c != null) && (this.c.k());
  }
  
  void l()
  {
    if (this.c != null) {
      return;
    }
    this.c = this.b.a(this.a);
    this.d.a(this.c);
    m();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzeb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */