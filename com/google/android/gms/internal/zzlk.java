package com.google.android.gms.internal;

import com.google.android.gms.cast.internal.zzl;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class zzlk
{
  private static final zzl n = new zzl("GameManagerMessage");
  protected final int a;
  protected final int b;
  protected final String c;
  protected final JSONObject d;
  protected final int e;
  protected final int f;
  protected final List<zzlo> g;
  protected final JSONObject h;
  protected final String i;
  protected final String j;
  protected final long k;
  protected final String l;
  protected final zzlj m;
  
  public zzlk(int paramInt1, int paramInt2, String paramString1, JSONObject paramJSONObject1, int paramInt3, int paramInt4, List<zzlo> paramList, JSONObject paramJSONObject2, String paramString2, String paramString3, long paramLong, String paramString4, zzlj paramzzlj)
  {
    this.a = paramInt1;
    this.b = paramInt2;
    this.c = paramString1;
    this.d = paramJSONObject1;
    this.e = paramInt3;
    this.f = paramInt4;
    this.g = paramList;
    this.h = paramJSONObject2;
    this.i = paramString2;
    this.j = paramString3;
    this.k = paramLong;
    this.l = paramString4;
    this.m = paramzzlj;
  }
  
  protected static zzlk a(JSONObject paramJSONObject)
  {
    int i1 = paramJSONObject.optInt("type", -1);
    switch (i1)
    {
    }
    try
    {
      n.d("Unrecognized Game Message type %d", new Object[] { Integer.valueOf(i1) });
    }
    catch (JSONException paramJSONObject)
    {
      n.b(paramJSONObject, "Exception while parsing GameManagerMessage from json", new Object[0]);
      break label247;
      zzlj localzzlj = null;
      JSONObject localJSONObject = paramJSONObject.optJSONObject("gameManagerConfig");
      if (localJSONObject == null) {
        break label166;
      }
      localzzlj = new zzlj(localJSONObject);
      label166:
      paramJSONObject = new zzlk(i1, paramJSONObject.optInt("statusCode"), paramJSONObject.optString("errorDescription"), paramJSONObject.optJSONObject("extraMessageData"), paramJSONObject.optInt("gameplayState"), paramJSONObject.optInt("lobbyState"), a(paramJSONObject.optJSONArray("players")), paramJSONObject.optJSONObject("gameData"), paramJSONObject.optString("gameStatusText"), paramJSONObject.optString("playerId"), paramJSONObject.optLong("requestId"), paramJSONObject.optString("playerToken"), localzzlj);
      return paramJSONObject;
    }
    paramJSONObject = new zzlk(i1, paramJSONObject.optInt("statusCode"), paramJSONObject.optString("errorDescription"), paramJSONObject.optJSONObject("extraMessageData"), paramJSONObject.optInt("gameplayState"), paramJSONObject.optInt("lobbyState"), a(paramJSONObject.optJSONArray("players")), paramJSONObject.optJSONObject("gameData"), paramJSONObject.optString("gameStatusText"), paramJSONObject.optString("playerId"), -1L, null, null);
    return paramJSONObject;
    label247:
    return null;
  }
  
  private static List<zzlo> a(JSONArray paramJSONArray)
  {
    ArrayList localArrayList = new ArrayList();
    if (paramJSONArray == null) {
      return localArrayList;
    }
    int i1 = 0;
    for (;;)
    {
      if (i1 < paramJSONArray.length())
      {
        Object localObject1 = paramJSONArray.optJSONObject(i1);
        if (localObject1 != null) {}
        try
        {
          localObject1 = new zzlo((JSONObject)localObject1);
          if (localObject1 != null) {
            localArrayList.add(localObject1);
          }
          i1 += 1;
        }
        catch (JSONException localJSONException)
        {
          for (;;)
          {
            n.b(localJSONException, "Exception when attempting to parse PlayerInfoMessageComponent at index %d", new Object[] { Integer.valueOf(i1) });
            Object localObject2 = null;
          }
        }
      }
    }
    return localArrayList;
  }
  
  public final int a()
  {
    return this.a;
  }
  
  public final int b()
  {
    return this.b;
  }
  
  public final String c()
  {
    return this.c;
  }
  
  public final JSONObject d()
  {
    return this.d;
  }
  
  public final int e()
  {
    return this.e;
  }
  
  public final int f()
  {
    return this.f;
  }
  
  public final List<zzlo> g()
  {
    return this.g;
  }
  
  public final JSONObject h()
  {
    return this.h;
  }
  
  public final String i()
  {
    return this.i;
  }
  
  public final String j()
  {
    return this.j;
  }
  
  public final long k()
  {
    return this.k;
  }
  
  public final String l()
  {
    return this.l;
  }
  
  public final zzlj m()
  {
    return this.m;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzlk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */