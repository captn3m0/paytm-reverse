package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@zzhb
public class zzev
  implements zzem
{
  private final AdRequestInfoParcel a;
  private final zzex b;
  private final Context c;
  private final Object d = new Object();
  private final zzeo e;
  private final boolean f;
  private final long g;
  private final long h;
  private final zzcb i;
  private final boolean j;
  private boolean k = false;
  private zzer l;
  
  public zzev(Context paramContext, AdRequestInfoParcel paramAdRequestInfoParcel, zzex paramzzex, zzeo paramzzeo, boolean paramBoolean1, boolean paramBoolean2, long paramLong1, long paramLong2, zzcb paramzzcb)
  {
    this.c = paramContext;
    this.a = paramAdRequestInfoParcel;
    this.b = paramzzex;
    this.e = paramzzeo;
    this.f = paramBoolean1;
    this.j = paramBoolean2;
    this.g = paramLong1;
    this.h = paramLong2;
    this.i = paramzzcb;
  }
  
  public zzes a(List<zzen> arg1)
  {
    zzin.a("Starting mediation.");
    Object localObject = new ArrayList();
    zzbz localzzbz1 = this.i.a();
    Iterator localIterator1 = ???.iterator();
    while (localIterator1.hasNext())
    {
      zzen localzzen = (zzen)localIterator1.next();
      zzin.c("Trying mediation network: " + localzzen.b);
      Iterator localIterator2 = localzzen.c.iterator();
      while (localIterator2.hasNext())
      {
        String str = (String)localIterator2.next();
        zzbz localzzbz2 = this.i.a();
        synchronized (this.d)
        {
          if (this.k)
          {
            localObject = new zzes(-1);
            return (zzes)localObject;
          }
          this.l = new zzer(this.c, str, this.b, this.e, localzzen, this.a.c, this.a.d, this.a.k, this.f, this.j, this.a.z, this.a.n);
          ??? = this.l.a(this.g, this.h);
          if (???.a == 0)
          {
            zzin.a("Adapter succeeded.");
            this.i.a("mediation_network_succeed", str);
            if (!((List)localObject).isEmpty()) {
              this.i.a("mediation_networks_fail", TextUtils.join(",", (Iterable)localObject));
            }
            this.i.a(localzzbz2, new String[] { "mls" });
            this.i.a(localzzbz1, new String[] { "ttm" });
            return (zzes)???;
          }
        }
        localIterable.add(str);
        this.i.a(localzzbz2, new String[] { "mlf" });
        if (???.c != null) {
          zzir.a.post(new Runnable()
          {
            public void run()
            {
              try
              {
                paramList.c.c();
                return;
              }
              catch (RemoteException localRemoteException)
              {
                zzin.d("Could not destroy mediation adapter.", localRemoteException);
              }
            }
          });
        }
      }
    }
    if (!localIterable.isEmpty()) {
      this.i.a("mediation_networks_fail", TextUtils.join(",", localIterable));
    }
    return new zzes(1);
  }
  
  public void cancel()
  {
    synchronized (this.d)
    {
      this.k = true;
      if (this.l != null) {
        this.l.cancel();
      }
      return;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzev.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */