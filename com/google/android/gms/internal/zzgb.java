package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@zzhb
public class zzgb
  implements zzfz
{
  final Set<WebView> a = Collections.synchronizedSet(new HashSet());
  private final Context b;
  
  public zzgb(Context paramContext)
  {
    this.b = paramContext;
  }
  
  public WebView a()
  {
    WebView localWebView = new WebView(this.b);
    localWebView.getSettings().setJavaScriptEnabled(true);
    return localWebView;
  }
  
  public void a(String paramString1, final String paramString2, final String paramString3)
  {
    zzin.a("Fetching assets for the given html");
    zzir.a.post(new Runnable()
    {
      public void run()
      {
        final WebView localWebView = zzgb.this.a();
        localWebView.setWebViewClient(new WebViewClient()
        {
          public void onPageFinished(WebView paramAnonymous2WebView, String paramAnonymous2String)
          {
            zzin.a("Loading assets have finished");
            zzgb.this.a.remove(localWebView);
          }
          
          public void onReceivedError(WebView paramAnonymous2WebView, int paramAnonymous2Int, String paramAnonymous2String1, String paramAnonymous2String2)
          {
            zzin.d("Loading assets have failed.");
            zzgb.this.a.remove(localWebView);
          }
        });
        zzgb.this.a.add(localWebView);
        localWebView.loadDataWithBaseURL(paramString2, paramString3, "text/html", "UTF-8", null);
        zzin.a("Fetching assets finished.");
      }
    });
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzgb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */