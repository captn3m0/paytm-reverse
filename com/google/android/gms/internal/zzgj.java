package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.zzl;
import com.google.android.gms.ads.internal.client.zzn;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.dynamic.zzg;
import com.google.android.gms.dynamic.zzg.zza;

@zzhb
public final class zzgj
  extends zzg<zzgf>
{
  private static final zzgj a = new zzgj();
  
  private zzgj()
  {
    super("com.google.android.gms.ads.InAppPurchaseManagerCreatorImpl");
  }
  
  @Nullable
  public static zzge a(Activity paramActivity)
  {
    try
    {
      zzge localzzge1;
      if (!b(paramActivity))
      {
        zzge localzzge2 = a.c(paramActivity);
        localzzge1 = localzzge2;
        if (localzzge2 != null) {}
      }
      else
      {
        zzin.a("Using AdOverlay from the client jar.");
        localzzge1 = zzn.c().a(paramActivity);
      }
      return localzzge1;
    }
    catch (zza paramActivity)
    {
      zzin.d(paramActivity.getMessage());
    }
    return null;
  }
  
  private static boolean b(Activity paramActivity)
    throws zzgj.zza
  {
    paramActivity = paramActivity.getIntent();
    if (!paramActivity.hasExtra("com.google.android.gms.ads.internal.purchase.useClientJar")) {
      throw new zza("InAppPurchaseManager requires the useClientJar flag in intent extras.");
    }
    return paramActivity.getBooleanExtra("com.google.android.gms.ads.internal.purchase.useClientJar", false);
  }
  
  private zzge c(Activity paramActivity)
  {
    try
    {
      zzd localzzd = zze.a(paramActivity);
      paramActivity = zzge.zza.a(((zzgf)a(paramActivity)).a(localzzd));
      return paramActivity;
    }
    catch (RemoteException paramActivity)
    {
      zzin.d("Could not create remote InAppPurchaseManager.", paramActivity);
      return null;
    }
    catch (zzg.zza paramActivity)
    {
      zzin.d("Could not create remote InAppPurchaseManager.", paramActivity);
    }
    return null;
  }
  
  protected zzgf a(IBinder paramIBinder)
  {
    return zzgf.zza.a(paramIBinder);
  }
  
  private static final class zza
    extends Exception
  {
    public zza(String paramString)
    {
      super();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzgj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */