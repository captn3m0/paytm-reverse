package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.provider.CalendarContract.Events;
import android.text.TextUtils;
import com.google.android.gms.R.string;
import com.google.android.gms.ads.internal.zzr;
import java.util.Map;

@zzhb
public class zzfm
  extends zzfs
{
  private final Map<String, String> a;
  private final Context b;
  private String c;
  private long d;
  private long e;
  private String f;
  private String g;
  
  public zzfm(zzjp paramzzjp, Map<String, String> paramMap)
  {
    super(paramzzjp, "createCalendarEvent");
    this.a = paramMap;
    this.b = paramzzjp.f();
    c();
  }
  
  private String a(String paramString)
  {
    if (TextUtils.isEmpty((CharSequence)this.a.get(paramString))) {
      return "";
    }
    return (String)this.a.get(paramString);
  }
  
  private void c()
  {
    this.c = a("description");
    this.f = a("summary");
    this.d = e("start_ticks");
    this.e = e("end_ticks");
    this.g = a("location");
  }
  
  private long e(String paramString)
  {
    paramString = (String)this.a.get(paramString);
    if (paramString == null) {
      return -1L;
    }
    try
    {
      long l = Long.parseLong(paramString);
      return l;
    }
    catch (NumberFormatException paramString) {}
    return -1L;
  }
  
  public void a()
  {
    if (this.b == null)
    {
      b("Activity context is not available.");
      return;
    }
    if (!zzr.e().e(this.b).f())
    {
      b("This feature is not available on the device.");
      return;
    }
    AlertDialog.Builder localBuilder = zzr.e().d(this.b);
    localBuilder.setTitle(zzr.h().a(R.string.create_calendar_title, "Create calendar event"));
    localBuilder.setMessage(zzr.h().a(R.string.create_calendar_message, "Allow Ad to create a calendar event?"));
    localBuilder.setPositiveButton(zzr.h().a(R.string.accept, "Accept"), new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        paramAnonymousDialogInterface = zzfm.this.b();
        zzr.e().a(zzfm.a(zzfm.this), paramAnonymousDialogInterface);
      }
    });
    localBuilder.setNegativeButton(zzr.h().a(R.string.decline, "Decline"), new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        zzfm.this.b("Operation denied by user.");
      }
    });
    localBuilder.create().show();
  }
  
  @TargetApi(14)
  Intent b()
  {
    Intent localIntent = new Intent("android.intent.action.EDIT").setData(CalendarContract.Events.CONTENT_URI);
    localIntent.putExtra("title", this.c);
    localIntent.putExtra("eventLocation", this.g);
    localIntent.putExtra("description", this.f);
    if (this.d > -1L) {
      localIntent.putExtra("beginTime", this.d);
    }
    if (this.e > -1L) {
      localIntent.putExtra("endTime", this.e);
    }
    localIntent.setFlags(268435456);
    return localIntent;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzfm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */