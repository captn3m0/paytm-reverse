package com.google.android.gms.internal;

import android.content.Context;
import android.os.Build.VERSION;
import com.google.android.gms.ads.internal.zzr;
import java.util.LinkedHashMap;
import java.util.Map;

@zzhb
public class zzbu
{
  private boolean a;
  private String b;
  private Map<String, String> c;
  private Context d = null;
  private String e = null;
  
  public zzbu(Context paramContext, String paramString)
  {
    this.d = paramContext;
    this.e = paramString;
    this.a = ((Boolean)zzbt.G.c()).booleanValue();
    this.b = ((String)zzbt.H.c());
    this.c = new LinkedHashMap();
    this.c.put("s", "gmob_sdk");
    this.c.put("v", "3");
    this.c.put("os", Build.VERSION.RELEASE);
    this.c.put("sdk", Build.VERSION.SDK);
    this.c.put("device", zzr.e().d());
    paramString = this.c;
    if (paramContext.getApplicationContext() != null) {}
    for (paramContext = paramContext.getApplicationContext().getPackageName();; paramContext = paramContext.getPackageName())
    {
      paramString.put("app", paramContext);
      paramContext = zzr.k().a(this.d);
      this.c.put("network_coarse", Integer.toString(paramContext.m));
      this.c.put("network_fine", Integer.toString(paramContext.n));
      return;
    }
  }
  
  boolean a()
  {
    return this.a;
  }
  
  String b()
  {
    return this.b;
  }
  
  Context c()
  {
    return this.d;
  }
  
  String d()
  {
    return this.e;
  }
  
  Map<String, String> e()
  {
    return this.c;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzbu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */