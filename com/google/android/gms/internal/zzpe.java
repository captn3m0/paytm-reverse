package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zzb;
import com.google.android.gms.fitness.RecordingApi;
import com.google.android.gms.fitness.request.ListSubscriptionsRequest;
import com.google.android.gms.fitness.request.SubscribeRequest;
import com.google.android.gms.fitness.request.UnsubscribeRequest;
import com.google.android.gms.fitness.result.ListSubscriptionsResult;

public class zzpe
  implements RecordingApi
{
  private static class zza
    extends zzor.zza
  {
    private final zza.zzb<ListSubscriptionsResult> a;
    
    private zza(zza.zzb<ListSubscriptionsResult> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void a(ListSubscriptionsResult paramListSubscriptionsResult)
    {
      this.a.a(paramListSubscriptionsResult);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzpe.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */