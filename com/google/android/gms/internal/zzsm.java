package com.google.android.gms.internal;

import java.io.IOException;

public final class zzsm
{
  private final byte[] a;
  private int b;
  private int c;
  private int d;
  private int e;
  private int f;
  private int g = Integer.MAX_VALUE;
  private int h;
  private int i = 64;
  private int j = 67108864;
  
  private zzsm(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    this.a = paramArrayOfByte;
    this.b = paramInt1;
    this.c = (paramInt1 + paramInt2);
    this.e = paramInt1;
  }
  
  public static long a(long paramLong)
  {
    return paramLong >>> 1 ^ -(1L & paramLong);
  }
  
  public static zzsm a(byte[] paramArrayOfByte)
  {
    return a(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public static zzsm a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    return new zzsm(paramArrayOfByte, paramInt1, paramInt2);
  }
  
  public static int c(int paramInt)
  {
    return paramInt >>> 1 ^ -(paramInt & 0x1);
  }
  
  private void u()
  {
    this.c += this.d;
    int k = this.c;
    if (k > this.g)
    {
      this.d = (k - this.g);
      this.c -= this.d;
      return;
    }
    this.d = 0;
  }
  
  public int a()
    throws IOException
  {
    if (r())
    {
      this.f = 0;
      return 0;
    }
    this.f = m();
    if (this.f == 0) {
      throw zzst.d();
    }
    return this.f;
  }
  
  public void a(int paramInt)
    throws zzst
  {
    if (this.f != paramInt) {
      throw zzst.e();
    }
  }
  
  public void a(zzsu paramzzsu)
    throws IOException
  {
    int k = m();
    if (this.h >= this.i) {
      throw zzst.g();
    }
    k = d(k);
    this.h += 1;
    paramzzsu.b(this);
    a(0);
    this.h -= 1;
    e(k);
  }
  
  public void a(zzsu paramzzsu, int paramInt)
    throws IOException
  {
    if (this.h >= this.i) {
      throw zzst.g();
    }
    this.h += 1;
    paramzzsu.b(this);
    a(zzsx.a(paramInt, 4));
    this.h -= 1;
  }
  
  public byte[] a(int paramInt1, int paramInt2)
  {
    if (paramInt2 == 0) {
      return zzsx.h;
    }
    byte[] arrayOfByte = new byte[paramInt2];
    int k = this.b;
    System.arraycopy(this.a, k + paramInt1, arrayOfByte, 0, paramInt2);
    return arrayOfByte;
  }
  
  public void b()
    throws IOException
  {
    int k;
    do
    {
      k = a();
    } while ((k != 0) && (b(k)));
  }
  
  public boolean b(int paramInt)
    throws IOException
  {
    switch (zzsx.a(paramInt))
    {
    default: 
      throw zzst.f();
    case 0: 
      g();
      return true;
    case 1: 
      p();
      return true;
    case 2: 
      h(m());
      return true;
    case 3: 
      b();
      a(zzsx.a(zzsx.b(paramInt), 4));
      return true;
    case 4: 
      return false;
    }
    o();
    return true;
  }
  
  public double c()
    throws IOException
  {
    return Double.longBitsToDouble(p());
  }
  
  public float d()
    throws IOException
  {
    return Float.intBitsToFloat(o());
  }
  
  public int d(int paramInt)
    throws zzst
  {
    if (paramInt < 0) {
      throw zzst.b();
    }
    paramInt = this.e + paramInt;
    int k = this.g;
    if (paramInt > k) {
      throw zzst.a();
    }
    this.g = paramInt;
    u();
    return k;
  }
  
  public long e()
    throws IOException
  {
    return n();
  }
  
  public void e(int paramInt)
  {
    this.g = paramInt;
    u();
  }
  
  public long f()
    throws IOException
  {
    return n();
  }
  
  public void f(int paramInt)
  {
    if (paramInt > this.e - this.b) {
      throw new IllegalArgumentException("Position " + paramInt + " is beyond current " + (this.e - this.b));
    }
    if (paramInt < 0) {
      throw new IllegalArgumentException("Bad position " + paramInt);
    }
    this.e = (this.b + paramInt);
  }
  
  public int g()
    throws IOException
  {
    return m();
  }
  
  public byte[] g(int paramInt)
    throws IOException
  {
    if (paramInt < 0) {
      throw zzst.b();
    }
    if (this.e + paramInt > this.g)
    {
      h(this.g - this.e);
      throw zzst.a();
    }
    if (paramInt <= this.c - this.e)
    {
      byte[] arrayOfByte = new byte[paramInt];
      System.arraycopy(this.a, this.e, arrayOfByte, 0, paramInt);
      this.e += paramInt;
      return arrayOfByte;
    }
    throw zzst.a();
  }
  
  public void h(int paramInt)
    throws IOException
  {
    if (paramInt < 0) {
      throw zzst.b();
    }
    if (this.e + paramInt > this.g)
    {
      h(this.g - this.e);
      throw zzst.a();
    }
    if (paramInt <= this.c - this.e)
    {
      this.e += paramInt;
      return;
    }
    throw zzst.a();
  }
  
  public boolean h()
    throws IOException
  {
    return m() != 0;
  }
  
  public String i()
    throws IOException
  {
    int k = m();
    if ((k <= this.c - this.e) && (k > 0))
    {
      String str = new String(this.a, this.e, k, "UTF-8");
      this.e = (k + this.e);
      return str;
    }
    return new String(g(k), "UTF-8");
  }
  
  public byte[] j()
    throws IOException
  {
    int k = m();
    if ((k <= this.c - this.e) && (k > 0))
    {
      byte[] arrayOfByte = new byte[k];
      System.arraycopy(this.a, this.e, arrayOfByte, 0, k);
      this.e = (k + this.e);
      return arrayOfByte;
    }
    if (k == 0) {
      return zzsx.h;
    }
    return g(k);
  }
  
  public int k()
    throws IOException
  {
    return c(m());
  }
  
  public long l()
    throws IOException
  {
    return a(n());
  }
  
  public int m()
    throws IOException
  {
    int k = t();
    if (k >= 0) {}
    int n;
    do
    {
      return k;
      k &= 0x7F;
      m = t();
      if (m >= 0) {
        return k | m << 7;
      }
      k |= (m & 0x7F) << 7;
      m = t();
      if (m >= 0) {
        return k | m << 14;
      }
      k |= (m & 0x7F) << 14;
      n = t();
      if (n >= 0) {
        return k | n << 21;
      }
      m = t();
      n = k | (n & 0x7F) << 21 | m << 28;
      k = n;
    } while (m >= 0);
    int m = 0;
    for (;;)
    {
      if (m >= 5) {
        break label133;
      }
      k = n;
      if (t() >= 0) {
        break;
      }
      m += 1;
    }
    label133:
    throw zzst.c();
  }
  
  public long n()
    throws IOException
  {
    int k = 0;
    long l = 0L;
    while (k < 64)
    {
      int m = t();
      l |= (m & 0x7F) << k;
      if ((m & 0x80) == 0) {
        return l;
      }
      k += 7;
    }
    throw zzst.c();
  }
  
  public int o()
    throws IOException
  {
    return t() & 0xFF | (t() & 0xFF) << 8 | (t() & 0xFF) << 16 | (t() & 0xFF) << 24;
  }
  
  public long p()
    throws IOException
  {
    int k = t();
    int m = t();
    int n = t();
    int i1 = t();
    int i2 = t();
    int i3 = t();
    int i4 = t();
    int i5 = t();
    long l = k;
    return (m & 0xFF) << 8 | l & 0xFF | (n & 0xFF) << 16 | (i1 & 0xFF) << 24 | (i2 & 0xFF) << 32 | (i3 & 0xFF) << 40 | (i4 & 0xFF) << 48 | (i5 & 0xFF) << 56;
  }
  
  public int q()
  {
    if (this.g == Integer.MAX_VALUE) {
      return -1;
    }
    int k = this.e;
    return this.g - k;
  }
  
  public boolean r()
  {
    return this.e == this.c;
  }
  
  public int s()
  {
    return this.e - this.b;
  }
  
  public byte t()
    throws IOException
  {
    if (this.e == this.c) {
      throw zzst.a();
    }
    byte[] arrayOfByte = this.a;
    int k = this.e;
    this.e = (k + 1);
    return arrayOfByte[k];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzsm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */