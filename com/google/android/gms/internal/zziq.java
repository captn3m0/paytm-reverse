package com.google.android.gms.internal;

import android.os.Process;
import com.google.android.gms.ads.internal.zzr;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

@zzhb
public final class zziq
{
  private static final ExecutorService a = Executors.newFixedThreadPool(10, a("Default"));
  private static final ExecutorService b = Executors.newFixedThreadPool(5, a("Loader"));
  
  public static zzjg<Void> a(int paramInt, Runnable paramRunnable)
  {
    if (paramInt == 1) {
      a(b, new Callable()
      {
        public Void a()
        {
          this.a.run();
          return null;
        }
      });
    }
    a(a, new Callable()
    {
      public Void a()
      {
        this.a.run();
        return null;
      }
    });
  }
  
  public static zzjg<Void> a(Runnable paramRunnable)
  {
    return a(0, paramRunnable);
  }
  
  public static <T> zzjg<T> a(Callable<T> paramCallable)
  {
    return a(a, paramCallable);
  }
  
  public static <T> zzjg<T> a(ExecutorService paramExecutorService, final Callable<T> paramCallable)
  {
    zzjd localzzjd = new zzjd();
    try
    {
      localzzjd.b(new Runnable()
      {
        public void run()
        {
          try
          {
            Process.setThreadPriority(10);
            this.a.b(paramCallable.call());
            return;
          }
          catch (Exception localException)
          {
            zzr.h().a(localException, true);
            this.a.cancel(true);
          }
        }
      }
      {
        public void run()
        {
          if (this.a.isCancelled()) {
            this.b.cancel(true);
          }
        }
      });
      return localzzjd;
    }
    catch (RejectedExecutionException paramExecutorService)
    {
      zzin.d("Thread execution is rejected.", paramExecutorService);
      localzzjd.cancel(true);
    }
    return localzzjd;
  }
  
  private static ThreadFactory a(String paramString)
  {
    new ThreadFactory()
    {
      private final AtomicInteger b = new AtomicInteger(1);
      
      public Thread newThread(Runnable paramAnonymousRunnable)
      {
        return new Thread(paramAnonymousRunnable, "AdWorker(" + this.a + ") #" + this.b.getAndIncrement());
      }
    };
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zziq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */