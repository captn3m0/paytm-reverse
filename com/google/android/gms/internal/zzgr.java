package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.zza;
import com.google.android.gms.ads.internal.zzp;

@zzhb
public class zzgr
{
  public zzit a(Context paramContext, zza paramzza, zzif.zza paramzza1, zzan paramzzan, zzjp paramzzjp, zzex paramzzex, zza paramzza2, zzcb paramzzcb)
  {
    AdResponseParcel localAdResponseParcel = paramzza1.b;
    if (localAdResponseParcel.h) {
      paramContext = new zzgu(paramContext, paramzza1, paramzzex, paramzza2, paramzzcb, paramzzjp);
    }
    for (;;)
    {
      zzin.a("AdRenderer: " + paramContext.getClass().getName());
      paramContext.d();
      return paramContext;
      if (localAdResponseParcel.t)
      {
        if ((paramzza instanceof zzp))
        {
          paramContext = new zzgv(paramContext, (zzp)paramzza, new zzee(), paramzza1, paramzzan, paramzza2);
        }
        else
        {
          paramzza1 = new StringBuilder().append("Invalid NativeAdManager type. Found: ");
          if (paramzza != null) {}
          for (paramContext = paramzza.getClass().getName();; paramContext = "null") {
            throw new IllegalArgumentException(paramContext + "; Required: NativeAdManager.");
          }
        }
      }
      else if (localAdResponseParcel.p) {
        paramContext = new zzgp(paramContext, paramzza1, paramzzjp, paramzza2);
      } else if ((((Boolean)zzbt.U.c()).booleanValue()) && (zzne.h()) && (!zzne.j()) && (paramzzjp.k().e)) {
        paramContext = new zzgt(paramContext, paramzza1, paramzzjp, paramzza2);
      } else {
        paramContext = new zzgs(paramContext, paramzza1, paramzzjp, paramzza2);
      }
    }
  }
  
  public zzit a(Context paramContext, String paramString, zzif.zza paramzza, zzht paramzzht)
  {
    paramContext = new zzhz(paramContext, paramString, paramzza, paramzzht);
    zzin.a("AdRenderer: " + paramContext.getClass().getName());
    paramContext.d();
    return paramContext;
  }
  
  public static abstract interface zza
  {
    public abstract void b(zzif paramzzif);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzgr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */