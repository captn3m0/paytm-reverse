package com.google.android.gms.internal;

import android.os.SystemClock;

public final class zzmt
  implements zzmq
{
  private static zzmt a;
  
  public static zzmq d()
  {
    try
    {
      if (a == null) {
        a = new zzmt();
      }
      zzmt localzzmt = a;
      return localzzmt;
    }
    finally {}
  }
  
  public long a()
  {
    return System.currentTimeMillis();
  }
  
  public long b()
  {
    return SystemClock.elapsedRealtime();
  }
  
  public long c()
  {
    return System.nanoTime();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzmt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */