package com.google.android.gms.internal;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.RemoteException;
import android.support.v4.util.SimpleArrayMap;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.formats.zza;
import com.google.android.gms.ads.internal.formats.zzc;
import com.google.android.gms.ads.internal.formats.zzf;
import com.google.android.gms.ads.internal.formats.zzh.zza;
import com.google.android.gms.ads.internal.formats.zzi;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.zzp;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.dynamic.zze;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzhb
public class zzgw
  implements Callable<zzif>
{
  private static final long a = TimeUnit.SECONDS.toMillis(60L);
  private final Context b;
  private final zziw c;
  private final zzp d;
  private final zzan e;
  private final zzee f;
  private final Object g = new Object();
  private final zzif.zza h;
  private boolean i;
  private int j;
  private List<String> k;
  private JSONObject l;
  
  public zzgw(Context paramContext, zzp paramzzp, zzee paramzzee, zziw paramzziw, zzan paramzzan, zzif.zza paramzza)
  {
    this.b = paramContext;
    this.d = paramzzp;
    this.c = paramzziw;
    this.f = paramzzee;
    this.h = paramzza;
    this.e = paramzzan;
    this.i = false;
    this.j = -2;
    this.k = null;
  }
  
  private zzh.zza a(zzed paramzzed, zza paramzza, JSONObject paramJSONObject)
    throws ExecutionException, InterruptedException, JSONException
  {
    if (b()) {
      return null;
    }
    JSONObject localJSONObject = paramJSONObject.getJSONObject("tracking_urls_and_actions");
    Object localObject = b(localJSONObject, "impression_tracking_urls");
    if (localObject == null) {}
    for (localObject = null;; localObject = Arrays.asList((Object[])localObject))
    {
      this.k = ((List)localObject);
      this.l = localJSONObject.optJSONObject("active_view");
      paramzza = paramzza.a(this, paramJSONObject);
      if (paramzza != null) {
        break;
      }
      zzin.b("Failed to retrieve ad assets.");
      return null;
    }
    paramzza.a(new zzi(this.b, this.d, paramzzed, this.e, paramJSONObject, paramzza, this.h.a.k));
    return paramzza;
  }
  
  private zzif a(zzh.zza paramzza)
  {
    for (;;)
    {
      synchronized (this.g)
      {
        int n = this.j;
        int m = n;
        if (paramzza == null)
        {
          m = n;
          if (this.j == -2) {
            m = 0;
          }
        }
        if (m != -2)
        {
          paramzza = null;
          return new zzif(this.h.a.c, null, this.h.b.d, m, this.h.b.f, this.k, this.h.b.l, this.h.b.k, this.h.a.i, false, null, null, null, null, null, 0L, this.h.d, this.h.b.g, this.h.f, this.h.g, this.h.b.o, this.l, paramzza, null, null, null, this.h.b.G);
        }
      }
    }
  }
  
  private zzjg<zzc> a(JSONObject paramJSONObject, final boolean paramBoolean1, boolean paramBoolean2)
    throws JSONException
  {
    if (paramBoolean1) {}
    final double d1;
    for (String str = paramJSONObject.getString("url");; str = paramJSONObject.optString("url"))
    {
      d1 = paramJSONObject.optDouble("scale", 1.0D);
      if (!TextUtils.isEmpty(str)) {
        break;
      }
      a(0, paramBoolean1);
      return new zzje(null);
    }
    if (paramBoolean2) {
      return new zzje(new zzc(null, Uri.parse(str), d1));
    }
    this.c.a(str, new zziw.zza()
    {
      public zzc a()
      {
        zzgw.this.a(2, paramBoolean1);
        return null;
      }
      
      public zzc a(InputStream paramAnonymousInputStream)
      {
        try
        {
          paramAnonymousInputStream = zzna.a(paramAnonymousInputStream);
          if (paramAnonymousInputStream == null)
          {
            zzgw.this.a(2, paramBoolean1);
            return null;
          }
        }
        catch (IOException paramAnonymousInputStream)
        {
          for (;;)
          {
            paramAnonymousInputStream = null;
          }
          paramAnonymousInputStream = BitmapFactory.decodeByteArray(paramAnonymousInputStream, 0, paramAnonymousInputStream.length);
          if (paramAnonymousInputStream == null)
          {
            zzgw.this.a(2, paramBoolean1);
            return null;
          }
          paramAnonymousInputStream.setDensity((int)(160.0D * d1));
        }
        return new zzc(new BitmapDrawable(Resources.getSystem(), paramAnonymousInputStream), Uri.parse(this.c), d1);
      }
    });
  }
  
  private Integer a(JSONObject paramJSONObject, String paramString)
  {
    try
    {
      paramJSONObject = paramJSONObject.getJSONObject(paramString);
      int m = Color.rgb(paramJSONObject.getInt("r"), paramJSONObject.getInt("g"), paramJSONObject.getInt("b"));
      return Integer.valueOf(m);
    }
    catch (JSONException paramJSONObject) {}
    return null;
  }
  
  private JSONObject a(final zzed paramzzed)
    throws TimeoutException, JSONException
  {
    if (b()) {
      return null;
    }
    final zzjd localzzjd = new zzjd();
    final zzb localzzb = new zzb();
    zzdf local1 = new zzdf()
    {
      public void a(zzjp paramAnonymouszzjp, Map<String, String> paramAnonymousMap)
      {
        paramzzed.b("/nativeAdPreProcess", localzzb.a);
        try
        {
          paramAnonymouszzjp = (String)paramAnonymousMap.get("success");
          if (!TextUtils.isEmpty(paramAnonymouszzjp))
          {
            localzzjd.b(new JSONObject(paramAnonymouszzjp).getJSONArray("ads").getJSONObject(0));
            return;
          }
        }
        catch (JSONException paramAnonymouszzjp)
        {
          zzin.b("Malformed native JSON response.", paramAnonymouszzjp);
          zzgw.this.a(0);
          zzx.a(zzgw.this.b(), "Unable to set the ad state error!");
          localzzjd.b(null);
        }
      }
    };
    localzzb.a = local1;
    paramzzed.a("/nativeAdPreProcess", local1);
    paramzzed.a("google.afma.nativeAds.preProcessJsonGmsg", new JSONObject(this.h.b.c));
    return (JSONObject)localzzjd.get(a, TimeUnit.MILLISECONDS);
  }
  
  private void a(zzh.zza paramzza, zzed paramzzed)
  {
    if (!(paramzza instanceof zzf)) {
      return;
    }
    final Object localObject = (zzf)paramzza;
    paramzza = new zzb();
    localObject = new zzdf()
    {
      public void a(zzjp paramAnonymouszzjp, Map<String, String> paramAnonymousMap)
      {
        paramAnonymouszzjp = (String)paramAnonymousMap.get("asset");
        zzgw.a(zzgw.this, localObject, paramAnonymouszzjp);
      }
    };
    paramzza.a = ((zzdf)localObject);
    paramzzed.a("/nativeAdCustomClick", (zzdf)localObject);
  }
  
  private void a(zzcp paramzzcp, String paramString)
  {
    try
    {
      zzct localzzct = this.d.c(paramzzcp.k());
      if (localzzct != null) {
        localzzct.a(paramzzcp, paramString);
      }
      return;
    }
    catch (RemoteException paramzzcp)
    {
      zzin.d("Failed to call onCustomClick for asset " + paramString + ".", paramzzcp);
    }
  }
  
  private static List<Drawable> b(List<zzc> paramList)
    throws RemoteException
  {
    ArrayList localArrayList = new ArrayList();
    paramList = paramList.iterator();
    while (paramList.hasNext()) {
      localArrayList.add((Drawable)zze.a(((zzc)paramList.next()).a()));
    }
    return localArrayList;
  }
  
  private String[] b(JSONObject paramJSONObject, String paramString)
    throws JSONException
  {
    paramJSONObject = paramJSONObject.optJSONArray(paramString);
    if (paramJSONObject == null) {
      return null;
    }
    paramString = new String[paramJSONObject.length()];
    int m = 0;
    while (m < paramJSONObject.length())
    {
      paramString[m] = paramJSONObject.getString(m);
      m += 1;
    }
    return paramString;
  }
  
  private zzed c()
    throws CancellationException, ExecutionException, InterruptedException, TimeoutException
  {
    if (b()) {
      return null;
    }
    String str = (String)zzbt.ac.c();
    if (this.h.b.b.indexOf("https") == 0) {}
    for (Object localObject = "https:";; localObject = "http:")
    {
      localObject = (String)localObject + str;
      localObject = (zzed)this.f.a(this.b, this.h.a.k, (String)localObject, this.e).get(a, TimeUnit.MILLISECONDS);
      ((zzed)localObject).a(this.d, this.d, this.d, this.d, false, null, null, null, null);
      return (zzed)localObject;
    }
  }
  
  protected zza a(JSONObject paramJSONObject)
    throws JSONException, TimeoutException
  {
    if (b()) {
      return null;
    }
    final String str = paramJSONObject.getString("template_id");
    boolean bool1;
    if (this.h.a.z != null)
    {
      bool1 = this.h.a.z.b;
      if (this.h.a.z == null) {
        break label98;
      }
    }
    label98:
    for (boolean bool2 = this.h.a.z.d;; bool2 = false)
    {
      if (!"2".equals(str)) {
        break label103;
      }
      return new zzgx(bool1, bool2);
      bool1 = false;
      break;
    }
    label103:
    if ("1".equals(str)) {
      return new zzgy(bool1, bool2);
    }
    if ("3".equals(str))
    {
      str = paramJSONObject.getString("custom_template_id");
      final zzjd localzzjd = new zzjd();
      zzir.a.post(new Runnable()
      {
        public void run()
        {
          localzzjd.b(zzgw.a(zzgw.this).z().get(str));
        }
      });
      if (localzzjd.get(a, TimeUnit.MILLISECONDS) != null) {
        return new zzgz(bool1);
      }
      zzin.b("No handler for custom template: " + paramJSONObject.getString("custom_template_id"));
    }
    for (;;)
    {
      return null;
      a(0);
    }
  }
  
  public zzif a()
  {
    try
    {
      Object localObject1 = c();
      Object localObject2 = a((zzed)localObject1);
      localObject2 = a((zzed)localObject1, a((JSONObject)localObject2), (JSONObject)localObject2);
      a((zzh.zza)localObject2, (zzed)localObject1);
      localObject1 = a((zzh.zza)localObject2);
      return (zzif)localObject1;
    }
    catch (JSONException localJSONException)
    {
      zzin.d("Malformed native JSON response.", localJSONException);
      if (!this.i) {
        a(0);
      }
      return a(null);
    }
    catch (TimeoutException localTimeoutException)
    {
      for (;;)
      {
        zzin.d("Timeout when loading native ad.", localTimeoutException);
      }
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;) {}
    }
    catch (ExecutionException localExecutionException)
    {
      for (;;) {}
    }
    catch (CancellationException localCancellationException)
    {
      for (;;) {}
    }
  }
  
  public zzjg<zzc> a(JSONObject paramJSONObject, String paramString, boolean paramBoolean1, boolean paramBoolean2)
    throws JSONException
  {
    if (paramBoolean1) {}
    for (paramJSONObject = paramJSONObject.getJSONObject(paramString);; paramJSONObject = paramJSONObject.optJSONObject(paramString))
    {
      paramString = paramJSONObject;
      if (paramJSONObject == null) {
        paramString = new JSONObject();
      }
      return a(paramString, paramBoolean1, paramBoolean2);
    }
  }
  
  public List<zzjg<zzc>> a(JSONObject paramJSONObject, String paramString, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
    throws JSONException
  {
    if (paramBoolean1) {}
    ArrayList localArrayList;
    for (paramJSONObject = paramJSONObject.getJSONArray(paramString);; paramJSONObject = paramJSONObject.optJSONArray(paramString))
    {
      localArrayList = new ArrayList();
      if ((paramJSONObject != null) && (paramJSONObject.length() != 0)) {
        break;
      }
      a(0, paramBoolean1);
      return localArrayList;
    }
    if (paramBoolean3) {}
    for (int m = paramJSONObject.length();; m = 1)
    {
      int n = 0;
      while (n < m)
      {
        JSONObject localJSONObject = paramJSONObject.getJSONObject(n);
        paramString = localJSONObject;
        if (localJSONObject == null) {
          paramString = new JSONObject();
        }
        localArrayList.add(a(paramString, paramBoolean1, paramBoolean2));
        n += 1;
      }
    }
    return localArrayList;
  }
  
  public Future<zzc> a(JSONObject paramJSONObject, String paramString, boolean paramBoolean)
    throws JSONException
  {
    paramString = paramJSONObject.getJSONObject(paramString);
    boolean bool = paramString.optBoolean("require", true);
    paramJSONObject = paramString;
    if (paramString == null) {
      paramJSONObject = new JSONObject();
    }
    return a(paramJSONObject, bool, paramBoolean);
  }
  
  public void a(int paramInt)
  {
    synchronized (this.g)
    {
      this.i = true;
      this.j = paramInt;
      return;
    }
  }
  
  public void a(int paramInt, boolean paramBoolean)
  {
    if (paramBoolean) {
      a(paramInt);
    }
  }
  
  public zzjg<zza> b(JSONObject paramJSONObject)
    throws JSONException
  {
    JSONObject localJSONObject = paramJSONObject.optJSONObject("attribution");
    if (localJSONObject == null) {
      return new zzje(null);
    }
    final String str = localJSONObject.optString("text");
    final int m = localJSONObject.optInt("text_size", -1);
    final Integer localInteger1 = a(localJSONObject, "text_color");
    final Integer localInteger2 = a(localJSONObject, "bg_color");
    final int n = localJSONObject.optInt("animation_ms", 1000);
    final int i1 = localJSONObject.optInt("presentation_ms", 4000);
    paramJSONObject = new ArrayList();
    if (localJSONObject.optJSONArray("images") != null) {
      paramJSONObject = a(localJSONObject, "images", false, false, true);
    }
    for (;;)
    {
      zzjf.a(zzjf.a(paramJSONObject), new zzjf.zza()
      {
        public zza a(List<zzc> paramAnonymousList)
        {
          if (paramAnonymousList != null) {
            for (;;)
            {
              try
              {
                if (paramAnonymousList.isEmpty()) {
                  break;
                }
                String str = str;
                List localList = zzgw.a(paramAnonymousList);
                Integer localInteger1 = localInteger2;
                Integer localInteger2 = localInteger1;
                if (m > 0)
                {
                  paramAnonymousList = Integer.valueOf(m);
                  paramAnonymousList = new zza(str, localList, localInteger1, localInteger2, paramAnonymousList, i1 + n);
                }
              }
              catch (RemoteException paramAnonymousList)
              {
                zzin.b("Could not get attribution icon", paramAnonymousList);
                return null;
              }
              paramAnonymousList = null;
            }
          }
          paramAnonymousList = null;
          return paramAnonymousList;
        }
      });
      paramJSONObject.add(a(localJSONObject, "image", false, false));
    }
  }
  
  public boolean b()
  {
    synchronized (this.g)
    {
      boolean bool = this.i;
      return bool;
    }
  }
  
  public static abstract interface zza<T extends zzh.zza>
  {
    public abstract T a(zzgw paramzzgw, JSONObject paramJSONObject)
      throws JSONException, InterruptedException, ExecutionException;
  }
  
  class zzb
  {
    public zzdf a;
    
    zzb() {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzgw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */