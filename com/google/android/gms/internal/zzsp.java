package com.google.android.gms.internal;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class zzsp<M extends zzso<M>, T>
{
  protected final int a;
  protected final Class<T> b;
  public final int c;
  protected final boolean d;
  
  private zzsp(int paramInt1, Class<T> paramClass, int paramInt2, boolean paramBoolean)
  {
    this.a = paramInt1;
    this.b = paramClass;
    this.c = paramInt2;
    this.d = paramBoolean;
  }
  
  public static <M extends zzso<M>, T extends zzsu> zzsp<M, T> a(int paramInt, Class<T> paramClass, long paramLong)
  {
    return new zzsp(paramInt, paramClass, (int)paramLong, false);
  }
  
  private T b(List<zzsw> paramList)
  {
    int j = 0;
    ArrayList localArrayList = new ArrayList();
    int i = 0;
    while (i < paramList.size())
    {
      localObject = (zzsw)paramList.get(i);
      if (((zzsw)localObject).b.length != 0) {
        a((zzsw)localObject, localArrayList);
      }
      i += 1;
    }
    int k = localArrayList.size();
    if (k == 0)
    {
      paramList = null;
      return paramList;
    }
    Object localObject = this.b.cast(Array.newInstance(this.b.getComponentType(), k));
    i = j;
    for (;;)
    {
      paramList = (List<zzsw>)localObject;
      if (i >= k) {
        break;
      }
      Array.set(localObject, i, localArrayList.get(i));
      i += 1;
    }
  }
  
  private T c(List<zzsw> paramList)
  {
    if (paramList.isEmpty()) {
      return null;
    }
    paramList = (zzsw)paramList.get(paramList.size() - 1);
    return (T)this.b.cast(a(zzsm.a(paramList.b)));
  }
  
  int a(Object paramObject)
  {
    if (this.d) {
      return b(paramObject);
    }
    return c(paramObject);
  }
  
  protected Object a(zzsm paramzzsm)
  {
    Class localClass;
    if (this.d) {
      localClass = this.b.getComponentType();
    }
    for (;;)
    {
      try
      {
        switch (this.a)
        {
        case 10: 
          throw new IllegalArgumentException("Unknown type " + this.a);
        }
      }
      catch (InstantiationException paramzzsm)
      {
        throw new IllegalArgumentException("Error creating instance of class " + localClass, paramzzsm);
        localClass = this.b;
        continue;
        zzsu localzzsu = (zzsu)localClass.newInstance();
        paramzzsm.a(localzzsu, zzsx.b(this.c));
        return localzzsu;
        localzzsu = (zzsu)localClass.newInstance();
        paramzzsm.a(localzzsu);
        return localzzsu;
      }
      catch (IllegalAccessException paramzzsm)
      {
        throw new IllegalArgumentException("Error creating instance of class " + localClass, paramzzsm);
      }
      catch (IOException paramzzsm)
      {
        throw new IllegalArgumentException("Error reading extension field", paramzzsm);
      }
    }
  }
  
  final T a(List<zzsw> paramList)
  {
    if (paramList == null) {
      return null;
    }
    if (this.d) {
      return (T)b(paramList);
    }
    return (T)c(paramList);
  }
  
  protected void a(zzsw paramzzsw, List<Object> paramList)
  {
    paramList.add(a(zzsm.a(paramzzsw.b)));
  }
  
  void a(Object paramObject, zzsn paramzzsn)
    throws IOException
  {
    if (this.d)
    {
      c(paramObject, paramzzsn);
      return;
    }
    b(paramObject, paramzzsn);
  }
  
  protected int b(Object paramObject)
  {
    int j = 0;
    int m = Array.getLength(paramObject);
    int i = 0;
    while (i < m)
    {
      int k = j;
      if (Array.get(paramObject, i) != null) {
        k = j + c(Array.get(paramObject, i));
      }
      i += 1;
      j = k;
    }
    return j;
  }
  
  protected void b(Object paramObject, zzsn paramzzsn)
  {
    for (;;)
    {
      try
      {
        paramzzsn.g(this.c);
        switch (this.a)
        {
        case 10: 
          throw new IllegalArgumentException("Unknown type " + this.a);
        }
      }
      catch (IOException paramObject)
      {
        throw new IllegalStateException((Throwable)paramObject);
      }
      paramObject = (zzsu)paramObject;
      int i = zzsx.b(this.c);
      paramzzsn.a((zzsu)paramObject);
      paramzzsn.e(i, 4);
      return;
      paramzzsn.b((zzsu)paramObject);
      return;
    }
  }
  
  protected int c(Object paramObject)
  {
    int i = zzsx.b(this.c);
    switch (this.a)
    {
    default: 
      throw new IllegalArgumentException("Unknown type " + this.a);
    case 10: 
      return zzsn.b(i, (zzsu)paramObject);
    }
    return zzsn.c(i, (zzsu)paramObject);
  }
  
  protected void c(Object paramObject, zzsn paramzzsn)
  {
    int j = Array.getLength(paramObject);
    int i = 0;
    while (i < j)
    {
      Object localObject = Array.get(paramObject, i);
      if (localObject != null) {
        b(localObject, paramzzsn);
      }
      i += 1;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzsp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */