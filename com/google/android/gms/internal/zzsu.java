package com.google.android.gms.internal;

import java.io.IOException;

public abstract class zzsu
{
  protected volatile int S = -1;
  
  public static final <T extends zzsu> T a(T paramT, byte[] paramArrayOfByte)
    throws zzst
  {
    return b(paramT, paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public static final void a(zzsu paramzzsu, byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    try
    {
      paramArrayOfByte = zzsn.a(paramArrayOfByte, paramInt1, paramInt2);
      paramzzsu.a(paramArrayOfByte);
      paramArrayOfByte.b();
      return;
    }
    catch (IOException paramzzsu)
    {
      throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", paramzzsu);
    }
  }
  
  public static final byte[] a(zzsu paramzzsu)
  {
    byte[] arrayOfByte = new byte[paramzzsu.g()];
    a(paramzzsu, arrayOfByte, 0, arrayOfByte.length);
    return arrayOfByte;
  }
  
  public static final <T extends zzsu> T b(T paramT, byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws zzst
  {
    try
    {
      paramArrayOfByte = zzsm.a(paramArrayOfByte, paramInt1, paramInt2);
      paramT.b(paramArrayOfByte);
      paramArrayOfByte.a(0);
      return paramT;
    }
    catch (zzst paramT)
    {
      throw paramT;
    }
    catch (IOException paramT)
    {
      throw new RuntimeException("Reading from a byte array threw an IOException (should never happen).");
    }
  }
  
  public void a(zzsn paramzzsn)
    throws IOException
  {}
  
  protected int b()
  {
    return 0;
  }
  
  public abstract zzsu b(zzsm paramzzsm)
    throws IOException;
  
  public zzsu e()
    throws CloneNotSupportedException
  {
    return (zzsu)super.clone();
  }
  
  public int f()
  {
    if (this.S < 0) {
      g();
    }
    return this.S;
  }
  
  public int g()
  {
    int i = b();
    this.S = i;
    return i;
  }
  
  public String toString()
  {
    return zzsv.a(this);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzsu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */