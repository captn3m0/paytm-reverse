package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.measurement.zze;
import java.util.HashMap;
import java.util.Map;

public final class zzpv
  extends zze<zzpv>
{
  public String a;
  public boolean b;
  
  public String a()
  {
    return this.a;
  }
  
  public void a(zzpv paramzzpv)
  {
    if (!TextUtils.isEmpty(this.a)) {
      paramzzpv.a(this.a);
    }
    if (this.b) {
      paramzzpv.a(this.b);
    }
  }
  
  public void a(String paramString)
  {
    this.a = paramString;
  }
  
  public void a(boolean paramBoolean)
  {
    this.b = paramBoolean;
  }
  
  public boolean b()
  {
    return this.b;
  }
  
  public String toString()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("description", this.a);
    localHashMap.put("fatal", Boolean.valueOf(this.b));
    return a(localHashMap);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzpv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */