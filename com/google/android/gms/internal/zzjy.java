package com.google.android.gms.internal;

import android.text.TextUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.common.internal.zzw;
import java.net.URI;
import java.net.URISyntaxException;

@zzhb
public class zzjy
  extends WebViewClient
{
  private final String a = b(paramString);
  private boolean b = false;
  private final zzjp c;
  private final zzgo d;
  
  public zzjy(zzgo paramzzgo, zzjp paramzzjp, String paramString)
  {
    this.c = paramzzjp;
    this.d = paramzzgo;
  }
  
  private String b(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {}
    for (;;)
    {
      return paramString;
      try
      {
        if (paramString.endsWith("/"))
        {
          String str = paramString.substring(0, paramString.length() - 1);
          return str;
        }
      }
      catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
      {
        zzin.b(localIndexOutOfBoundsException.getMessage());
      }
    }
    return paramString;
  }
  
  protected boolean a(String paramString)
  {
    paramString = b(paramString);
    if (TextUtils.isEmpty(paramString)) {}
    for (;;)
    {
      return false;
      try
      {
        Object localObject1 = new URI(paramString);
        if ("passback".equals(((URI)localObject1).getScheme()))
        {
          zzin.a("Passback received");
          this.d.b();
          return true;
        }
        if (!TextUtils.isEmpty(this.a))
        {
          Object localObject2 = new URI(this.a);
          paramString = ((URI)localObject2).getHost();
          String str = ((URI)localObject1).getHost();
          localObject2 = ((URI)localObject2).getPath();
          localObject1 = ((URI)localObject1).getPath();
          if ((zzw.a(paramString, str)) && (zzw.a(localObject2, localObject1)))
          {
            zzin.a("Passback received");
            this.d.b();
            return true;
          }
        }
      }
      catch (URISyntaxException paramString)
      {
        zzin.b(paramString.getMessage());
      }
    }
    return false;
  }
  
  public void onLoadResource(WebView paramWebView, String paramString)
  {
    zzin.a("JavascriptAdWebViewClient::onLoadResource: " + paramString);
    if (!a(paramString)) {
      this.c.l().onLoadResource(this.c.a(), paramString);
    }
  }
  
  public void onPageFinished(WebView paramWebView, String paramString)
  {
    zzin.a("JavascriptAdWebViewClient::onPageFinished: " + paramString);
    if (!this.b)
    {
      this.d.a();
      this.b = true;
    }
  }
  
  public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    zzin.a("JavascriptAdWebViewClient::shouldOverrideUrlLoading: " + paramString);
    if (a(paramString))
    {
      zzin.a("shouldOverrideUrlLoading: received passback url");
      return true;
    }
    return this.c.l().shouldOverrideUrlLoading(this.c.a(), paramString);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzjy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */