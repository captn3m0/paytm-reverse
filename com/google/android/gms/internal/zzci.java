package com.google.android.gms.internal;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.RemoteException;
import com.google.android.gms.ads.formats.NativeAd.Image;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zze;

@zzhb
public class zzci
  extends NativeAd.Image
{
  private final zzch a;
  private final Drawable b;
  private final Uri c;
  private final double d;
  
  public zzci(zzch paramzzch)
  {
    this.a = paramzzch;
    try
    {
      paramzzch = this.a.a();
      if (paramzzch == null) {
        break label83;
      }
      paramzzch = (Drawable)zze.a(paramzzch);
    }
    catch (RemoteException paramzzch)
    {
      try
      {
        paramzzch = this.a.b();
        this.c = paramzzch;
        double d1 = 1.0D;
        try
        {
          double d2 = this.a.c();
          d1 = d2;
        }
        catch (RemoteException paramzzch)
        {
          for (;;)
          {
            zzb.b("Failed to get scale.", paramzzch);
          }
        }
        this.d = d1;
        return;
        paramzzch = paramzzch;
        zzb.b("Failed to get drawable.", paramzzch);
        paramzzch = null;
      }
      catch (RemoteException paramzzch)
      {
        for (;;)
        {
          zzb.b("Failed to get uri.", paramzzch);
          paramzzch = (zzch)localObject;
        }
      }
    }
    this.b = paramzzch;
  }
  
  public Drawable a()
  {
    return this.b;
  }
  
  public Uri b()
  {
    return this.c;
  }
  
  public double c()
  {
    return this.d;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzci.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */