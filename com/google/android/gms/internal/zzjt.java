package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.MutableContextWrapper;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.webkit.DownloadListener;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzn;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.zzr;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

@zzhb
class zzjt
  extends WebView
  implements ViewTreeObserver.OnGlobalLayoutListener, DownloadListener, zzjp
{
  private int A = -1;
  private Map<String, zzdr> B;
  private final WindowManager C;
  private final zza a;
  private final Object b = new Object();
  private final zzan c;
  private final VersionInfoParcel d;
  private final com.google.android.gms.ads.internal.zzd e;
  private zzjq f;
  private com.google.android.gms.ads.internal.overlay.zzd g;
  private AdSizeParcel h;
  private boolean i;
  private boolean j;
  private boolean k;
  private boolean l;
  private Boolean m;
  private int n;
  private boolean o = true;
  private String p = "";
  private zzbz q;
  private zzbz r;
  private zzbz s;
  private zzca t;
  private WeakReference<View.OnClickListener> u;
  private com.google.android.gms.ads.internal.overlay.zzd v;
  private zzjc w;
  private int x = -1;
  private int y = -1;
  private int z = -1;
  
  protected zzjt(zza paramzza, AdSizeParcel paramAdSizeParcel, boolean paramBoolean1, boolean paramBoolean2, zzan paramzzan, VersionInfoParcel paramVersionInfoParcel, zzcb paramzzcb, com.google.android.gms.ads.internal.zzd paramzzd)
  {
    super(paramzza);
    this.a = paramzza;
    this.h = paramAdSizeParcel;
    this.k = paramBoolean1;
    this.n = -1;
    this.c = paramzzan;
    this.d = paramVersionInfoParcel;
    this.e = paramzzd;
    this.C = ((WindowManager)getContext().getSystemService("window"));
    setBackgroundColor(0);
    paramAdSizeParcel = getSettings();
    paramAdSizeParcel.setAllowFileAccess(false);
    paramAdSizeParcel.setJavaScriptEnabled(true);
    paramAdSizeParcel.setSavePassword(false);
    paramAdSizeParcel.setSupportMultipleWindows(true);
    paramAdSizeParcel.setJavaScriptCanOpenWindowsAutomatically(true);
    if (Build.VERSION.SDK_INT >= 21) {
      paramAdSizeParcel.setMixedContentMode(0);
    }
    zzr.e().a(paramzza, paramVersionInfoParcel.b, paramAdSizeParcel);
    zzr.g().a(getContext(), paramAdSizeParcel);
    setDownloadListener(this);
    F();
    if (zzne.f()) {
      addJavascriptInterface(new zzju(this), "googleAdsJsInterface");
    }
    this.w = new zzjc(this.a.a(), this, null);
    a(paramzzcb);
  }
  
  private void D()
  {
    synchronized (this.b)
    {
      this.m = zzr.h().j();
      Boolean localBoolean = this.m;
      if (localBoolean == null) {}
      try
      {
        evaluateJavascript("(function(){})()", null);
        a(Boolean.valueOf(true));
        return;
      }
      catch (IllegalStateException localIllegalStateException)
      {
        for (;;)
        {
          a(Boolean.valueOf(false));
        }
      }
    }
  }
  
  private void E()
  {
    zzbx.a(this.t.a(), this.q, new String[] { "aeh" });
  }
  
  private void F()
  {
    for (;;)
    {
      synchronized (this.b)
      {
        if ((this.k) || (this.h.e))
        {
          if (Build.VERSION.SDK_INT < 14)
          {
            zzin.a("Disabling hardware acceleration on an overlay.");
            G();
            return;
          }
          zzin.a("Enabling hardware acceleration on an overlay.");
          H();
        }
      }
      if (Build.VERSION.SDK_INT < 18)
      {
        zzin.a("Disabling hardware acceleration on an AdView.");
        G();
      }
      else
      {
        zzin.a("Enabling hardware acceleration on an AdView.");
        H();
      }
    }
  }
  
  private void G()
  {
    synchronized (this.b)
    {
      if (!this.l) {
        zzr.g().c(this);
      }
      this.l = true;
      return;
    }
  }
  
  private void H()
  {
    synchronized (this.b)
    {
      if (this.l) {
        zzr.g().b(this);
      }
      this.l = false;
      return;
    }
  }
  
  private void I()
  {
    synchronized (this.b)
    {
      if (this.B != null)
      {
        Iterator localIterator = this.B.values().iterator();
        if (localIterator.hasNext()) {
          ((zzdr)localIterator.next()).release();
        }
      }
    }
  }
  
  private void J()
  {
    if (this.t == null) {}
    zzcb localzzcb;
    do
    {
      return;
      localzzcb = this.t.a();
    } while ((localzzcb == null) || (zzr.h().e() == null));
    zzr.h().e().a(localzzcb);
  }
  
  static zzjt a(Context paramContext, AdSizeParcel paramAdSizeParcel, boolean paramBoolean1, boolean paramBoolean2, zzan paramzzan, VersionInfoParcel paramVersionInfoParcel, zzcb paramzzcb, com.google.android.gms.ads.internal.zzd paramzzd)
  {
    return new zzjt(new zza(paramContext), paramAdSizeParcel, paramBoolean1, paramBoolean2, paramzzan, paramVersionInfoParcel, paramzzcb, paramzzd);
  }
  
  private void a(zzcb paramzzcb)
  {
    J();
    this.t = new zzca(new zzcb(true, "make_wv", this.h.b));
    this.t.a().a(paramzzcb);
    this.r = zzbx.a(this.t.a());
    this.t.a("native:view_create", this.r);
    this.s = null;
    this.q = null;
  }
  
  public View.OnClickListener A()
  {
    return (View.OnClickListener)this.u.get();
  }
  
  public boolean B()
  {
    if (!l().b()) {
      return false;
    }
    DisplayMetrics localDisplayMetrics = zzr.e().a(this.C);
    int i3 = zzn.a().b(localDisplayMetrics, localDisplayMetrics.widthPixels);
    int i4 = zzn.a().b(localDisplayMetrics, localDisplayMetrics.heightPixels);
    Object localObject = f();
    int i2;
    int i1;
    if ((localObject == null) || (((Activity)localObject).getWindow() == null))
    {
      i2 = i4;
      i1 = i3;
      label77:
      if ((this.y == i3) && (this.x == i4) && (this.z == i1) && (this.A == i2)) {
        break label224;
      }
      if ((this.y == i3) && (this.x == i4)) {
        break label226;
      }
    }
    label224:
    label226:
    for (boolean bool = true;; bool = false)
    {
      this.y = i3;
      this.x = i4;
      this.z = i1;
      this.A = i2;
      new zzfs(this).a(i3, i4, i1, i2, localDisplayMetrics.density, this.C.getDefaultDisplay().getRotation());
      return bool;
      localObject = zzr.e().a((Activity)localObject);
      i1 = zzn.a().b(localDisplayMetrics, localObject[0]);
      i2 = zzn.a().b(localDisplayMetrics, localObject[1]);
      break label77;
      break;
    }
  }
  
  Boolean C()
  {
    synchronized (this.b)
    {
      Boolean localBoolean = this.m;
      return localBoolean;
    }
  }
  
  public WebView a()
  {
    return this;
  }
  
  public void a(int paramInt)
  {
    E();
    HashMap localHashMap = new HashMap(2);
    localHashMap.put("closetype", String.valueOf(paramInt));
    localHashMap.put("version", this.d.b);
    a("onhide", localHashMap);
  }
  
  public void a(Context paramContext)
  {
    this.a.setBaseContext(paramContext);
    this.w.a(this.a.a());
  }
  
  public void a(Context paramContext, AdSizeParcel paramAdSizeParcel, zzcb paramzzcb)
  {
    synchronized (this.b)
    {
      this.w.b();
      a(paramContext);
      this.g = null;
      this.h = paramAdSizeParcel;
      this.k = false;
      this.i = false;
      this.p = "";
      this.n = -1;
      zzr.g().b(this);
      loadUrl("about:blank");
      this.f.f();
      setOnTouchListener(null);
      setOnClickListener(null);
      this.o = true;
      a(paramzzcb);
      return;
    }
  }
  
  public void a(AdSizeParcel paramAdSizeParcel)
  {
    synchronized (this.b)
    {
      this.h = paramAdSizeParcel;
      requestLayout();
      return;
    }
  }
  
  public void a(com.google.android.gms.ads.internal.overlay.zzd paramzzd)
  {
    synchronized (this.b)
    {
      this.g = paramzzd;
      return;
    }
  }
  
  public void a(zzau paramzzau, boolean paramBoolean)
  {
    HashMap localHashMap = new HashMap();
    if (paramBoolean) {}
    for (paramzzau = "1";; paramzzau = "0")
    {
      localHashMap.put("isVisible", paramzzau);
      a("onAdVisibilityChanged", localHashMap);
      return;
    }
  }
  
  void a(Boolean paramBoolean)
  {
    this.m = paramBoolean;
    zzr.h().a(paramBoolean);
  }
  
  public void a(String paramString)
  {
    synchronized (this.b)
    {
      try
      {
        super.loadUrl(paramString);
        return;
      }
      catch (Throwable paramString)
      {
        for (;;)
        {
          zzin.d("Could not call loadUrl. " + paramString);
        }
      }
    }
  }
  
  @TargetApi(19)
  protected void a(String paramString, ValueCallback<String> paramValueCallback)
  {
    synchronized (this.b)
    {
      if (!r()) {
        evaluateJavascript(paramString, paramValueCallback);
      }
      do
      {
        return;
        zzin.d("The webview is destroyed. Ignoring action.");
      } while (paramValueCallback == null);
      paramValueCallback.onReceiveValue(null);
    }
  }
  
  public void a(String paramString, zzdf paramzzdf)
  {
    if (this.f != null) {
      this.f.a(paramString, paramzzdf);
    }
  }
  
  public void a(String paramString1, String paramString2)
  {
    d(paramString1 + "(" + paramString2 + ");");
  }
  
  public void a(String paramString, Map<String, ?> paramMap)
  {
    try
    {
      paramMap = zzr.e().a(paramMap);
      b(paramString, paramMap);
      return;
    }
    catch (JSONException paramString)
    {
      zzin.d("Could not convert parameters to JSON.");
    }
  }
  
  public void a(String paramString, JSONObject paramJSONObject)
  {
    JSONObject localJSONObject = paramJSONObject;
    if (paramJSONObject == null) {
      localJSONObject = new JSONObject();
    }
    a(paramString, localJSONObject.toString());
  }
  
  public void a(boolean paramBoolean)
  {
    synchronized (this.b)
    {
      this.k = paramBoolean;
      F();
      return;
    }
  }
  
  public View b()
  {
    return this;
  }
  
  public void b(int paramInt)
  {
    synchronized (this.b)
    {
      this.n = paramInt;
      if (this.g != null) {
        this.g.a(this.n);
      }
      return;
    }
  }
  
  public void b(com.google.android.gms.ads.internal.overlay.zzd paramzzd)
  {
    synchronized (this.b)
    {
      this.v = paramzzd;
      return;
    }
  }
  
  public void b(String paramString)
  {
    Object localObject = this.b;
    String str = paramString;
    if (paramString == null) {
      str = "";
    }
    try
    {
      this.p = str;
      return;
    }
    finally {}
  }
  
  public void b(String paramString, zzdf paramzzdf)
  {
    if (this.f != null) {
      this.f.b(paramString, paramzzdf);
    }
  }
  
  public void b(String paramString, JSONObject paramJSONObject)
  {
    Object localObject = paramJSONObject;
    if (paramJSONObject == null) {
      localObject = new JSONObject();
    }
    paramJSONObject = ((JSONObject)localObject).toString();
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append("AFMA_ReceiveMessage('");
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append("'");
    ((StringBuilder)localObject).append(",");
    ((StringBuilder)localObject).append(paramJSONObject);
    ((StringBuilder)localObject).append(");");
    zzin.e("Dispatching AFMA event: " + ((StringBuilder)localObject).toString());
    d(((StringBuilder)localObject).toString());
  }
  
  public void b(boolean paramBoolean)
  {
    synchronized (this.b)
    {
      if (this.g != null)
      {
        this.g.a(this.f.b(), paramBoolean);
        return;
      }
      this.i = paramBoolean;
    }
  }
  
  public void c()
  {
    E();
    HashMap localHashMap = new HashMap(1);
    localHashMap.put("version", this.d.b);
    a("onhide", localHashMap);
  }
  
  protected void c(String paramString)
  {
    synchronized (this.b)
    {
      if (!r())
      {
        loadUrl(paramString);
        return;
      }
      zzin.d("The webview is destroyed. Ignoring action.");
    }
  }
  
  public void c(boolean paramBoolean)
  {
    synchronized (this.b)
    {
      this.o = paramBoolean;
      return;
    }
  }
  
  public void d()
  {
    if (this.q == null)
    {
      zzbx.a(this.t.a(), this.s, new String[] { "aes" });
      this.q = zzbx.a(this.t.a());
      this.t.a("native:view_show", this.q);
    }
    HashMap localHashMap = new HashMap(1);
    localHashMap.put("version", this.d.b);
    a("onshow", localHashMap);
  }
  
  protected void d(String paramString)
  {
    if (zzne.h())
    {
      if (C() == null) {
        D();
      }
      if (C().booleanValue())
      {
        a(paramString, null);
        return;
      }
      c("javascript:" + paramString);
      return;
    }
    c("javascript:" + paramString);
  }
  
  public void destroy()
  {
    synchronized (this.b)
    {
      J();
      this.w.b();
      if (this.g != null)
      {
        this.g.a();
        this.g.k();
        this.g = null;
      }
      this.f.f();
      if (this.j) {
        return;
      }
      zzr.t().a(this);
      I();
      this.j = true;
      zzin.e("Initiating WebView self destruct sequence in 3...");
      this.f.d();
      return;
    }
  }
  
  public void e()
  {
    HashMap localHashMap = new HashMap(2);
    localHashMap.put("app_volume", String.valueOf(zzr.e().f()));
    localHashMap.put("device_volume", String.valueOf(zzr.e().i(getContext())));
    a("volume", localHashMap);
  }
  
  @TargetApi(19)
  public void evaluateJavascript(String paramString, ValueCallback<String> paramValueCallback)
  {
    synchronized (this.b)
    {
      if (r())
      {
        zzin.d("The webview is destroyed. Ignoring action.");
        if (paramValueCallback != null) {
          paramValueCallback.onReceiveValue(null);
        }
        return;
      }
      super.evaluateJavascript(paramString, paramValueCallback);
      return;
    }
  }
  
  public Activity f()
  {
    return this.a.a();
  }
  
  protected void finalize()
    throws Throwable
  {
    synchronized (this.b)
    {
      if (!this.j)
      {
        zzr.t().a(this);
        I();
      }
      super.finalize();
      return;
    }
  }
  
  public Context g()
  {
    return this.a.b();
  }
  
  public com.google.android.gms.ads.internal.zzd h()
  {
    return this.e;
  }
  
  public com.google.android.gms.ads.internal.overlay.zzd i()
  {
    synchronized (this.b)
    {
      com.google.android.gms.ads.internal.overlay.zzd localzzd = this.g;
      return localzzd;
    }
  }
  
  public com.google.android.gms.ads.internal.overlay.zzd j()
  {
    synchronized (this.b)
    {
      com.google.android.gms.ads.internal.overlay.zzd localzzd = this.v;
      return localzzd;
    }
  }
  
  public AdSizeParcel k()
  {
    synchronized (this.b)
    {
      AdSizeParcel localAdSizeParcel = this.h;
      return localAdSizeParcel;
    }
  }
  
  public zzjq l()
  {
    return this.f;
  }
  
  public void loadData(String paramString1, String paramString2, String paramString3)
  {
    synchronized (this.b)
    {
      if (!r())
      {
        super.loadData(paramString1, paramString2, paramString3);
        return;
      }
      zzin.d("The webview is destroyed. Ignoring action.");
    }
  }
  
  public void loadDataWithBaseURL(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    synchronized (this.b)
    {
      if (!r())
      {
        super.loadDataWithBaseURL(paramString1, paramString2, paramString3, paramString4, paramString5);
        return;
      }
      zzin.d("The webview is destroyed. Ignoring action.");
    }
  }
  
  public void loadUrl(String paramString)
  {
    for (;;)
    {
      synchronized (this.b)
      {
        boolean bool = r();
        if (!bool) {
          try
          {
            super.loadUrl(paramString);
            return;
          }
          catch (Throwable paramString)
          {
            zzin.d("Could not call loadUrl. " + paramString);
            continue;
          }
        }
      }
      zzin.d("The webview is destroyed. Ignoring action.");
    }
  }
  
  public boolean m()
  {
    return this.i;
  }
  
  public zzan n()
  {
    return this.c;
  }
  
  public VersionInfoParcel o()
  {
    return this.d;
  }
  
  protected void onAttachedToWindow()
  {
    synchronized (this.b)
    {
      super.onAttachedToWindow();
      if (!r()) {
        this.w.c();
      }
      return;
    }
  }
  
  protected void onDetachedFromWindow()
  {
    synchronized (this.b)
    {
      if (!r()) {
        this.w.d();
      }
      super.onDetachedFromWindow();
      return;
    }
  }
  
  public void onDownloadStart(String paramString1, String paramString2, String paramString3, String paramString4, long paramLong)
  {
    try
    {
      paramString2 = new Intent("android.intent.action.VIEW");
      paramString2.setDataAndType(Uri.parse(paramString1), paramString4);
      zzr.e().a(getContext(), paramString2);
      return;
    }
    catch (ActivityNotFoundException paramString2)
    {
      zzin.a("Couldn't find an Activity to view url/mimetype: " + paramString1 + " / " + paramString4);
    }
  }
  
  @TargetApi(21)
  protected void onDraw(Canvas paramCanvas)
  {
    if (r()) {}
    while ((Build.VERSION.SDK_INT == 21) && (paramCanvas.isHardwareAccelerated()) && (!isAttachedToWindow())) {
      return;
    }
    super.onDraw(paramCanvas);
  }
  
  public void onGlobalLayout()
  {
    boolean bool = B();
    com.google.android.gms.ads.internal.overlay.zzd localzzd = i();
    if ((localzzd != null) && (bool)) {
      localzzd.o();
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i2 = Integer.MAX_VALUE;
    synchronized (this.b)
    {
      if (r())
      {
        setMeasuredDimension(0, 0);
        return;
      }
      if ((isInEditMode()) || (this.k) || (this.h.i) || (this.h.j))
      {
        super.onMeasure(paramInt1, paramInt2);
        return;
      }
    }
    if (this.h.e)
    {
      DisplayMetrics localDisplayMetrics = new DisplayMetrics();
      this.C.getDefaultDisplay().getMetrics(localDisplayMetrics);
      setMeasuredDimension(localDisplayMetrics.widthPixels, localDisplayMetrics.heightPixels);
      return;
    }
    int i5 = View.MeasureSpec.getMode(paramInt1);
    int i1 = View.MeasureSpec.getSize(paramInt1);
    int i4 = View.MeasureSpec.getMode(paramInt2);
    int i3 = View.MeasureSpec.getSize(paramInt2);
    if (i5 != Integer.MIN_VALUE) {
      if (i5 == 1073741824) {
        break label368;
      }
    }
    for (;;)
    {
      if ((this.h.g > paramInt1) || (this.h.d > paramInt2))
      {
        float f1 = this.a.getResources().getDisplayMetrics().density;
        zzin.d("Not enough space to show ad. Needs " + (int)(this.h.g / f1) + "x" + (int)(this.h.d / f1) + " dp, but only has " + (int)(i1 / f1) + "x" + (int)(i3 / f1) + " dp.");
        if (getVisibility() != 8) {
          setVisibility(4);
        }
        setMeasuredDimension(0, 0);
      }
      for (;;)
      {
        return;
        if (getVisibility() != 8) {
          setVisibility(0);
        }
        setMeasuredDimension(this.h.g, this.h.d);
      }
      paramInt1 = Integer.MAX_VALUE;
      break label371;
      label368:
      paramInt1 = i1;
      label371:
      if (i4 != Integer.MIN_VALUE)
      {
        paramInt2 = i2;
        if (i4 != 1073741824) {}
      }
      else
      {
        paramInt2 = i3;
      }
    }
  }
  
  public void onPause()
  {
    if (r()) {}
    for (;;)
    {
      return;
      try
      {
        if (zzne.a())
        {
          super.onPause();
          return;
        }
      }
      catch (Exception localException)
      {
        zzin.b("Could not pause webview.", localException);
      }
    }
  }
  
  public void onResume()
  {
    if (r()) {}
    for (;;)
    {
      return;
      try
      {
        if (zzne.a())
        {
          super.onResume();
          return;
        }
      }
      catch (Exception localException)
      {
        zzin.b("Could not resume webview.", localException);
      }
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if (this.c != null) {
      this.c.a(paramMotionEvent);
    }
    if (r()) {
      return false;
    }
    return super.onTouchEvent(paramMotionEvent);
  }
  
  public boolean p()
  {
    synchronized (this.b)
    {
      boolean bool = this.k;
      return bool;
    }
  }
  
  public int q()
  {
    synchronized (this.b)
    {
      int i1 = this.n;
      return i1;
    }
  }
  
  public boolean r()
  {
    synchronized (this.b)
    {
      boolean bool = this.j;
      return bool;
    }
  }
  
  public void s()
  {
    synchronized (this.b)
    {
      zzin.e("Destroying WebView!");
      zzir.a.post(new Runnable()
      {
        public void run()
        {
          zzjt.a(zzjt.this);
        }
      });
      return;
    }
  }
  
  public void setOnClickListener(View.OnClickListener paramOnClickListener)
  {
    this.u = new WeakReference(paramOnClickListener);
    super.setOnClickListener(paramOnClickListener);
  }
  
  public void setWebViewClient(WebViewClient paramWebViewClient)
  {
    super.setWebViewClient(paramWebViewClient);
    if ((paramWebViewClient instanceof zzjq)) {
      this.f = ((zzjq)paramWebViewClient);
    }
  }
  
  public void stopLoading()
  {
    if (r()) {
      return;
    }
    try
    {
      super.stopLoading();
      return;
    }
    catch (Exception localException)
    {
      zzin.b("Could not stop loading webview.", localException);
    }
  }
  
  public boolean t()
  {
    synchronized (this.b)
    {
      zzbx.a(this.t.a(), this.q, new String[] { "aebb" });
      boolean bool = this.o;
      return bool;
    }
  }
  
  public String u()
  {
    synchronized (this.b)
    {
      String str = this.p;
      return str;
    }
  }
  
  public zzjo v()
  {
    return null;
  }
  
  public zzbz w()
  {
    return this.s;
  }
  
  public zzca x()
  {
    return this.t;
  }
  
  public void y()
  {
    this.w.a();
  }
  
  public void z()
  {
    if (this.s == null)
    {
      this.s = zzbx.a(this.t.a());
      this.t.a("native:view_load", this.s);
    }
  }
  
  @zzhb
  public static class zza
    extends MutableContextWrapper
  {
    private Activity a;
    private Context b;
    private Context c;
    
    public zza(Context paramContext)
    {
      super();
      setBaseContext(paramContext);
    }
    
    public Activity a()
    {
      return this.a;
    }
    
    public Context b()
    {
      return this.c;
    }
    
    public Object getSystemService(String paramString)
    {
      return this.c.getSystemService(paramString);
    }
    
    public void setBaseContext(Context paramContext)
    {
      this.b = paramContext.getApplicationContext();
      if ((paramContext instanceof Activity)) {}
      for (Activity localActivity = (Activity)paramContext;; localActivity = null)
      {
        this.a = localActivity;
        this.c = paramContext;
        super.setBaseContext(this.b);
        return;
      }
    }
    
    public void startActivity(Intent paramIntent)
    {
      if ((this.a != null) && (!zzne.j()))
      {
        this.a.startActivity(paramIntent);
        return;
      }
      paramIntent.setFlags(268435456);
      this.b.startActivity(paramIntent);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzjt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */