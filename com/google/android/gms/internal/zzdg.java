package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzr;
import java.io.BufferedOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzhb
public class zzdg
  implements zzdf
{
  private final Context a;
  private final VersionInfoParcel b;
  
  public zzdg(Context paramContext, VersionInfoParcel paramVersionInfoParcel)
  {
    this.a = paramContext;
    this.b = paramVersionInfoParcel;
  }
  
  protected zzb a(JSONObject paramJSONObject)
  {
    String str1 = paramJSONObject.optString("http_request_id");
    Object localObject1 = paramJSONObject.optString("url");
    String str2 = paramJSONObject.optString("post_body", null);
    try
    {
      localObject1 = new URL((String)localObject1);
      localArrayList = new ArrayList();
      localObject2 = paramJSONObject.optJSONArray("headers");
      paramJSONObject = (JSONObject)localObject2;
      if (localObject2 == null) {
        paramJSONObject = new JSONArray();
      }
      int i = 0;
      for (;;)
      {
        if (i >= paramJSONObject.length()) {
          break label137;
        }
        localObject2 = paramJSONObject.optJSONObject(i);
        if (localObject2 != null) {
          break;
        }
        i += 1;
      }
    }
    catch (MalformedURLException localMalformedURLException)
    {
      ArrayList localArrayList;
      URL localURL;
      for (;;)
      {
        Object localObject2;
        zzin.b("Error constructing http request.", localMalformedURLException);
        localURL = null;
        continue;
        localArrayList.add(new zza(((JSONObject)localObject2).optString("key"), ((JSONObject)localObject2).optString("value")));
      }
      label137:
      return new zzb(str1, localURL, localArrayList, str2);
    }
  }
  
  protected zzc a(zzb paramzzb)
  {
    HttpURLConnection localHttpURLConnection;
    try
    {
      localHttpURLConnection = (HttpURLConnection)paramzzb.b().openConnection();
      zzr.e().a(this.a, this.b.b, false, localHttpURLConnection);
      localObject1 = paramzzb.c().iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (zza)((Iterator)localObject1).next();
        localHttpURLConnection.addRequestProperty(((zza)localObject2).a(), ((zza)localObject2).b());
      }
      if (TextUtils.isEmpty(paramzzb.d())) {
        break label144;
      }
    }
    catch (Exception paramzzb)
    {
      return new zzc(false, null, paramzzb.toString());
    }
    localHttpURLConnection.setDoOutput(true);
    Object localObject1 = paramzzb.d().getBytes();
    localHttpURLConnection.setFixedLengthStreamingMode(localObject1.length);
    Object localObject2 = new BufferedOutputStream(localHttpURLConnection.getOutputStream());
    ((BufferedOutputStream)localObject2).write((byte[])localObject1);
    ((BufferedOutputStream)localObject2).close();
    label144:
    localObject1 = new ArrayList();
    if (localHttpURLConnection.getHeaderFields() != null)
    {
      localObject2 = localHttpURLConnection.getHeaderFields().entrySet().iterator();
      while (((Iterator)localObject2).hasNext())
      {
        Map.Entry localEntry = (Map.Entry)((Iterator)localObject2).next();
        Iterator localIterator = ((List)localEntry.getValue()).iterator();
        while (localIterator.hasNext())
        {
          String str = (String)localIterator.next();
          ((ArrayList)localObject1).add(new zza((String)localEntry.getKey(), str));
        }
      }
    }
    paramzzb = new zzc(true, new zzd(paramzzb.a(), localHttpURLConnection.getResponseCode(), (List)localObject1, zzr.e().a(new InputStreamReader(localHttpURLConnection.getInputStream()))), null);
    return paramzzb;
  }
  
  protected JSONObject a(zzd paramzzd)
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("http_request_id", paramzzd.a());
      if (paramzzd.d() != null) {
        localJSONObject.put("body", paramzzd.d());
      }
      JSONArray localJSONArray = new JSONArray();
      Iterator localIterator = paramzzd.c().iterator();
      while (localIterator.hasNext())
      {
        zza localzza = (zza)localIterator.next();
        localJSONArray.put(new JSONObject().put("key", localzza.a()).put("value", localzza.b()));
      }
      localJSONObject.put("headers", localJSONArray);
    }
    catch (JSONException paramzzd)
    {
      zzin.b("Error constructing JSON for http response.", paramzzd);
      return localJSONObject;
    }
    localJSONObject.put("response_code", paramzzd.b());
    return localJSONObject;
  }
  
  /* Error */
  public JSONObject a(String paramString)
  {
    // Byte code:
    //   0: new 44	org/json/JSONObject
    //   3: dup
    //   4: aload_1
    //   5: invokespecial 275	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   8: astore 4
    //   10: new 44	org/json/JSONObject
    //   13: dup
    //   14: invokespecial 247	org/json/JSONObject:<init>	()V
    //   17: astore_3
    //   18: ldc_w 277
    //   21: astore_1
    //   22: aload 4
    //   24: ldc 42
    //   26: invokevirtual 48	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   29: astore_2
    //   30: aload_2
    //   31: astore_1
    //   32: aload_0
    //   33: aload_0
    //   34: aload 4
    //   36: invokevirtual 279	com/google/android/gms/internal/zzdg:a	(Lorg/json/JSONObject;)Lcom/google/android/gms/internal/zzdg$zzb;
    //   39: invokevirtual 281	com/google/android/gms/internal/zzdg:a	(Lcom/google/android/gms/internal/zzdg$zzb;)Lcom/google/android/gms/internal/zzdg$zzc;
    //   42: astore 4
    //   44: aload_2
    //   45: astore_1
    //   46: aload 4
    //   48: invokevirtual 283	com/google/android/gms/internal/zzdg$zzc:c	()Z
    //   51: ifeq +68 -> 119
    //   54: aload_2
    //   55: astore_1
    //   56: aload_3
    //   57: ldc_w 285
    //   60: aload_0
    //   61: aload 4
    //   63: invokevirtual 288	com/google/android/gms/internal/zzdg$zzc:b	()Lcom/google/android/gms/internal/zzdg$zzd;
    //   66: invokevirtual 290	com/google/android/gms/internal/zzdg:a	(Lcom/google/android/gms/internal/zzdg$zzd;)Lorg/json/JSONObject;
    //   69: invokevirtual 252	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   72: pop
    //   73: aload_2
    //   74: astore_1
    //   75: aload_3
    //   76: ldc_w 292
    //   79: iconst_1
    //   80: invokevirtual 295	org/json/JSONObject:put	(Ljava/lang/String;Z)Lorg/json/JSONObject;
    //   83: pop
    //   84: aload_3
    //   85: areturn
    //   86: astore_1
    //   87: ldc_w 297
    //   90: invokestatic 299	com/google/android/gms/internal/zzin:b	(Ljava/lang/String;)V
    //   93: new 44	org/json/JSONObject
    //   96: dup
    //   97: invokespecial 247	org/json/JSONObject:<init>	()V
    //   100: ldc_w 292
    //   103: iconst_0
    //   104: invokevirtual 295	org/json/JSONObject:put	(Ljava/lang/String;Z)Lorg/json/JSONObject;
    //   107: astore_1
    //   108: aload_1
    //   109: areturn
    //   110: astore_1
    //   111: new 44	org/json/JSONObject
    //   114: dup
    //   115: invokespecial 247	org/json/JSONObject:<init>	()V
    //   118: areturn
    //   119: aload_2
    //   120: astore_1
    //   121: aload_3
    //   122: ldc_w 285
    //   125: new 44	org/json/JSONObject
    //   128: dup
    //   129: invokespecial 247	org/json/JSONObject:<init>	()V
    //   132: ldc 42
    //   134: aload_2
    //   135: invokevirtual 252	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   138: invokevirtual 252	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   141: pop
    //   142: aload_2
    //   143: astore_1
    //   144: aload_3
    //   145: ldc_w 292
    //   148: iconst_0
    //   149: invokevirtual 295	org/json/JSONObject:put	(Ljava/lang/String;Z)Lorg/json/JSONObject;
    //   152: pop
    //   153: aload_2
    //   154: astore_1
    //   155: aload_3
    //   156: ldc_w 301
    //   159: aload 4
    //   161: invokevirtual 302	com/google/android/gms/internal/zzdg$zzc:a	()Ljava/lang/String;
    //   164: invokevirtual 252	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   167: pop
    //   168: aload_3
    //   169: areturn
    //   170: astore_2
    //   171: aload_3
    //   172: ldc_w 285
    //   175: new 44	org/json/JSONObject
    //   178: dup
    //   179: invokespecial 247	org/json/JSONObject:<init>	()V
    //   182: ldc 42
    //   184: aload_1
    //   185: invokevirtual 252	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   188: invokevirtual 252	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   191: pop
    //   192: aload_3
    //   193: ldc_w 292
    //   196: iconst_0
    //   197: invokevirtual 295	org/json/JSONObject:put	(Ljava/lang/String;Z)Lorg/json/JSONObject;
    //   200: pop
    //   201: aload_3
    //   202: ldc_w 301
    //   205: aload_2
    //   206: invokevirtual 158	java/lang/Exception:toString	()Ljava/lang/String;
    //   209: invokevirtual 252	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   212: pop
    //   213: aload_3
    //   214: areturn
    //   215: astore_1
    //   216: aload_3
    //   217: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	218	0	this	zzdg
    //   0	218	1	paramString	String
    //   29	125	2	str	String
    //   170	36	2	localException	Exception
    //   17	200	3	localJSONObject	JSONObject
    //   8	152	4	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   0	10	86	org/json/JSONException
    //   93	108	110	org/json/JSONException
    //   22	30	170	java/lang/Exception
    //   32	44	170	java/lang/Exception
    //   46	54	170	java/lang/Exception
    //   56	73	170	java/lang/Exception
    //   75	84	170	java/lang/Exception
    //   121	142	170	java/lang/Exception
    //   144	153	170	java/lang/Exception
    //   155	168	170	java/lang/Exception
    //   171	213	215	org/json/JSONException
  }
  
  public void a(final zzjp paramzzjp, final Map<String, String> paramMap)
  {
    zziq.a(new Runnable()
    {
      public void run()
      {
        zzin.a("Received Http request.");
        final Object localObject = (String)paramMap.get("http_request");
        localObject = zzdg.this.a((String)localObject);
        if (localObject == null)
        {
          zzin.b("Response should not be null.");
          return;
        }
        zzir.a.post(new Runnable()
        {
          public void run()
          {
            zzdg.1.this.b.b("fetchHttpRequestCompleted", localObject);
            zzin.a("Dispatched http response.");
          }
        });
      }
    });
  }
  
  @zzhb
  static class zza
  {
    private final String a;
    private final String b;
    
    public zza(String paramString1, String paramString2)
    {
      this.a = paramString1;
      this.b = paramString2;
    }
    
    public String a()
    {
      return this.a;
    }
    
    public String b()
    {
      return this.b;
    }
  }
  
  @zzhb
  static class zzb
  {
    private final String a;
    private final URL b;
    private final ArrayList<zzdg.zza> c;
    private final String d;
    
    public zzb(String paramString1, URL paramURL, ArrayList<zzdg.zza> paramArrayList, String paramString2)
    {
      this.a = paramString1;
      this.b = paramURL;
      if (paramArrayList == null) {}
      for (this.c = new ArrayList();; this.c = paramArrayList)
      {
        this.d = paramString2;
        return;
      }
    }
    
    public String a()
    {
      return this.a;
    }
    
    public URL b()
    {
      return this.b;
    }
    
    public ArrayList<zzdg.zza> c()
    {
      return this.c;
    }
    
    public String d()
    {
      return this.d;
    }
  }
  
  @zzhb
  class zzc
  {
    private final zzdg.zzd b;
    private final boolean c;
    private final String d;
    
    public zzc(boolean paramBoolean, zzdg.zzd paramzzd, String paramString)
    {
      this.c = paramBoolean;
      this.b = paramzzd;
      this.d = paramString;
    }
    
    public String a()
    {
      return this.d;
    }
    
    public zzdg.zzd b()
    {
      return this.b;
    }
    
    public boolean c()
    {
      return this.c;
    }
  }
  
  @zzhb
  static class zzd
  {
    private final String a;
    private final int b;
    private final List<zzdg.zza> c;
    private final String d;
    
    public zzd(String paramString1, int paramInt, List<zzdg.zza> paramList, String paramString2)
    {
      this.a = paramString1;
      this.b = paramInt;
      if (paramList == null) {}
      for (this.c = new ArrayList();; this.c = paramList)
      {
        this.d = paramString2;
        return;
      }
    }
    
    public String a()
    {
      return this.a;
    }
    
    public int b()
    {
      return this.b;
    }
    
    public Iterable<zzdg.zza> c()
    {
      return this.c;
    }
    
    public String d()
    {
      return this.d;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzdg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */