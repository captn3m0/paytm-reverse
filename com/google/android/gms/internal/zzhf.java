package com.google.android.gms.internal;

import java.util.Map;
import java.util.concurrent.Future;

@zzhb
public final class zzhf
{
  zzjp a;
  zzeg.zzd b;
  public final zzdf c = new zzdf()
  {
    public void a(zzjp arg1, Map<String, String> paramAnonymousMap)
    {
      synchronized (zzhf.a(zzhf.this))
      {
        if (zzhf.b(zzhf.this).isDone()) {
          return;
        }
        if (!zzhf.c(zzhf.this).equals(paramAnonymousMap.get("request_id"))) {
          return;
        }
      }
      paramAnonymousMap = new zzhi(1, paramAnonymousMap);
      zzin.d("Invalid " + paramAnonymousMap.e() + " request error: " + paramAnonymousMap.b());
      zzhf.b(zzhf.this).b(paramAnonymousMap);
    }
  };
  public final zzdf d = new zzdf()
  {
    public void a(zzjp paramAnonymouszzjp, Map<String, String> paramAnonymousMap)
    {
      zzhi localzzhi;
      synchronized (zzhf.a(zzhf.this))
      {
        if (zzhf.b(zzhf.this).isDone()) {
          return;
        }
        localzzhi = new zzhi(-2, paramAnonymousMap);
        if (!zzhf.c(zzhf.this).equals(localzzhi.g()))
        {
          zzin.d(localzzhi.g() + " ==== " + zzhf.c(zzhf.this));
          return;
        }
      }
      String str = localzzhi.d();
      if (str == null)
      {
        zzin.d("URL missing in loadAdUrl GMSG.");
        return;
      }
      if (str.contains("%40mediation_adapters%40"))
      {
        paramAnonymouszzjp = str.replaceAll("%40mediation_adapters%40", zzil.a(paramAnonymouszzjp.getContext(), (String)paramAnonymousMap.get("check_adapters"), zzhf.d(zzhf.this)));
        localzzhi.a(paramAnonymouszzjp);
        zzin.e("Ad request URL modified to " + paramAnonymouszzjp);
      }
      zzhf.b(zzhf.this).b(localzzhi);
    }
  };
  private final Object e = new Object();
  private String f;
  private String g;
  private zzjd<zzhi> h = new zzjd();
  
  public zzhf(String paramString1, String paramString2)
  {
    this.g = paramString2;
    this.f = paramString1;
  }
  
  public zzeg.zzd a()
  {
    return this.b;
  }
  
  public void a(zzeg.zzd paramzzd)
  {
    this.b = paramzzd;
  }
  
  public void a(zzjp paramzzjp)
  {
    this.a = paramzzjp;
  }
  
  public Future<zzhi> b()
  {
    return this.h;
  }
  
  public void c()
  {
    if (this.a != null)
    {
      this.a.destroy();
      this.a = null;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzhf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */