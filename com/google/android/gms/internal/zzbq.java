package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@zzhb
public class zzbq
{
  private final Collection<zzbp> a = new ArrayList();
  private final Collection<zzbp<String>> b = new ArrayList();
  private final Collection<zzbp<String>> c = new ArrayList();
  
  public List<String> a()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.b.iterator();
    while (localIterator.hasNext())
    {
      String str = (String)((zzbp)localIterator.next()).c();
      if (str != null) {
        localArrayList.add(str);
      }
    }
    return localArrayList;
  }
  
  public void a(zzbp paramzzbp)
  {
    this.a.add(paramzzbp);
  }
  
  public List<String> b()
  {
    List localList = a();
    Iterator localIterator = this.c.iterator();
    while (localIterator.hasNext())
    {
      String str = (String)((zzbp)localIterator.next()).c();
      if (str != null) {
        localList.add(str);
      }
    }
    return localList;
  }
  
  public void b(zzbp<String> paramzzbp)
  {
    this.b.add(paramzzbp);
  }
  
  public void c(zzbp<String> paramzzbp)
  {
    this.c.add(paramzzbp);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzbq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */