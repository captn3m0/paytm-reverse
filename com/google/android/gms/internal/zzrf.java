package com.google.android.gms.internal;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;
import java.util.List;

public class zzrf
  extends zzj<zzrd>
{
  private final Context a;
  
  public zzrf(Context paramContext, Looper paramLooper, zzf paramzzf, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramContext, paramLooper, 45, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    this.a = paramContext;
  }
  
  private String h()
  {
    try
    {
      Object localObject = this.a.getPackageManager();
      if (localObject == null) {
        return null;
      }
      localObject = ((PackageManager)localObject).getApplicationInfo(this.a.getPackageName(), 128);
      if (localObject == null) {
        return null;
      }
      localObject = ((ApplicationInfo)localObject).metaData;
      if (localObject == null) {
        return null;
      }
      localObject = (String)((Bundle)localObject).get("com.google.android.safetynet.API_KEY");
      return (String)localObject;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
    return null;
  }
  
  protected zzrd a(IBinder paramIBinder)
  {
    return zzrd.zza.a(paramIBinder);
  }
  
  protected String a()
  {
    return "com.google.android.gms.safetynet.service.START";
  }
  
  public void a(zzrc paramzzrc, List<Integer> paramList, int paramInt, String paramString)
    throws RemoteException
  {
    int[] arrayOfInt = new int[paramList.size()];
    int i = 0;
    while (i < paramList.size())
    {
      arrayOfInt[i] = ((Integer)paramList.get(i)).intValue();
      i += 1;
    }
    ((zzrd)v()).a(paramzzrc, h(), arrayOfInt, paramInt, paramString);
  }
  
  public void a(zzrc paramzzrc, byte[] paramArrayOfByte)
    throws RemoteException
  {
    ((zzrd)v()).a(paramzzrc, paramArrayOfByte);
  }
  
  protected String b()
  {
    return "com.google.android.gms.safetynet.internal.ISafetyNetService";
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzrf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */