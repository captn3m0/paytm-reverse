package com.google.android.gms.internal;

import com.google.android.gms.measurement.zze;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class zzkb
  extends zze<zzkb>
{
  private Map<Integer, String> a = new HashMap(4);
  
  public Map<Integer, String> a()
  {
    return Collections.unmodifiableMap(this.a);
  }
  
  public void a(zzkb paramzzkb)
  {
    paramzzkb.a.putAll(this.a);
  }
  
  public String toString()
  {
    HashMap localHashMap = new HashMap();
    Iterator localIterator = this.a.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      localHashMap.put("dimension" + localEntry.getKey(), localEntry.getValue());
    }
    return a(localHashMap);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzkb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */