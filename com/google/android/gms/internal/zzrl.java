package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.HasOptions;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.signin.internal.zzg;
import com.google.android.gms.signin.internal.zzh;

public final class zzrl
{
  public static final Api.zzc<zzh> a = new Api.zzc();
  public static final Api.zzc<zzh> b = new Api.zzc();
  public static final Api.zza<zzh, zzro> c = new Api.zza()
  {
    public zzh a(Context paramAnonymousContext, Looper paramAnonymousLooper, zzf paramAnonymouszzf, zzro paramAnonymouszzro, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      if (paramAnonymouszzro == null) {
        paramAnonymouszzro = zzro.a;
      }
      for (;;)
      {
        return new zzh(paramAnonymousContext, paramAnonymousLooper, true, paramAnonymouszzf, paramAnonymouszzro, paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
      }
    }
  };
  static final Api.zza<zzh, zza> d = new Api.zza()
  {
    public zzh a(Context paramAnonymousContext, Looper paramAnonymousLooper, zzf paramAnonymouszzf, zzrl.zza paramAnonymouszza, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      return new zzh(paramAnonymousContext, paramAnonymousLooper, false, paramAnonymouszzf, paramAnonymouszza.a(), paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
    }
  };
  public static final Scope e = new Scope("profile");
  public static final Scope f = new Scope("email");
  public static final Api<zzro> g = new Api("SignIn.API", c, a);
  public static final Api<zza> h = new Api("SignIn.INTERNAL_API", d, b);
  public static final zzrm i = new zzg();
  
  public static class zza
    implements Api.ApiOptions.HasOptions
  {
    private final Bundle a;
    
    public Bundle a()
    {
      return this.a;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzrl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */