package com.google.android.gms.internal;

import java.io.IOException;

public abstract interface zzqb
{
  public static final class zza
    extends zzsu
  {
    private static volatile zza[] e;
    public Integer a;
    public zzqb.zzf b;
    public zzqb.zzf c;
    public Boolean d;
    
    public zza()
    {
      c();
    }
    
    public static zza[] a()
    {
      if (e == null) {}
      synchronized (zzss.a)
      {
        if (e == null) {
          e = new zza[0];
        }
        return e;
      }
    }
    
    public zza a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (zzsx.a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 8: 
          this.a = Integer.valueOf(paramzzsm.g());
          break;
        case 18: 
          if (this.b == null) {
            this.b = new zzqb.zzf();
          }
          paramzzsm.a(this.b);
          break;
        case 26: 
          if (this.c == null) {
            this.c = new zzqb.zzf();
          }
          paramzzsm.a(this.c);
          break;
        case 32: 
          this.d = Boolean.valueOf(paramzzsm.h());
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (this.a != null) {
        paramzzsn.a(1, this.a.intValue());
      }
      if (this.b != null) {
        paramzzsn.a(2, this.b);
      }
      if (this.c != null) {
        paramzzsn.a(3, this.c);
      }
      if (this.d != null) {
        paramzzsn.a(4, this.d.booleanValue());
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b();
      int i = j;
      if (this.a != null) {
        i = j + zzsn.c(1, this.a.intValue());
      }
      j = i;
      if (this.b != null) {
        j = i + zzsn.c(2, this.b);
      }
      i = j;
      if (this.c != null) {
        i = j + zzsn.c(3, this.c);
      }
      j = i;
      if (this.d != null) {
        j = i + zzsn.b(4, this.d.booleanValue());
      }
      return j;
    }
    
    public zza c()
    {
      this.a = null;
      this.b = null;
      this.c = null;
      this.d = null;
      this.S = -1;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      if (paramObject == this) {}
      do
      {
        do
        {
          return true;
          if (!(paramObject instanceof zza)) {
            return false;
          }
          paramObject = (zza)paramObject;
          if (this.a == null)
          {
            if (((zza)paramObject).a != null) {
              return false;
            }
          }
          else if (!this.a.equals(((zza)paramObject).a)) {
            return false;
          }
          if (this.b == null)
          {
            if (((zza)paramObject).b != null) {
              return false;
            }
          }
          else if (!this.b.equals(((zza)paramObject).b)) {
            return false;
          }
          if (this.c == null)
          {
            if (((zza)paramObject).c != null) {
              return false;
            }
          }
          else if (!this.c.equals(((zza)paramObject).c)) {
            return false;
          }
          if (this.d != null) {
            break;
          }
        } while (((zza)paramObject).d == null);
        return false;
      } while (this.d.equals(((zza)paramObject).d));
      return false;
    }
    
    public int hashCode()
    {
      int m = 0;
      int n = getClass().getName().hashCode();
      int i;
      int j;
      label33:
      int k;
      if (this.a == null)
      {
        i = 0;
        if (this.b != null) {
          break label88;
        }
        j = 0;
        if (this.c != null) {
          break label99;
        }
        k = 0;
        label42:
        if (this.d != null) {
          break label110;
        }
      }
      for (;;)
      {
        return (k + (j + (i + (n + 527) * 31) * 31) * 31) * 31 + m;
        i = this.a.hashCode();
        break;
        label88:
        j = this.b.hashCode();
        break label33;
        label99:
        k = this.c.hashCode();
        break label42;
        label110:
        m = this.d.hashCode();
      }
    }
  }
  
  public static final class zzb
    extends zzsu
  {
    private static volatile zzb[] f;
    public zzqb.zzc[] a;
    public String b;
    public Long c;
    public Long d;
    public Integer e;
    
    public zzb()
    {
      c();
    }
    
    public static zzb[] a()
    {
      if (f == null) {}
      synchronized (zzss.a)
      {
        if (f == null) {
          f = new zzb[0];
        }
        return f;
      }
    }
    
    public zzb a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (zzsx.a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 10: 
          int j = zzsx.b(paramzzsm, 10);
          if (this.a == null) {}
          zzqb.zzc[] arrayOfzzc;
          for (i = 0;; i = this.a.length)
          {
            arrayOfzzc = new zzqb.zzc[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.a, 0, arrayOfzzc, 0, i);
              j = i;
            }
            while (j < arrayOfzzc.length - 1)
            {
              arrayOfzzc[j] = new zzqb.zzc();
              paramzzsm.a(arrayOfzzc[j]);
              paramzzsm.a();
              j += 1;
            }
          }
          arrayOfzzc[j] = new zzqb.zzc();
          paramzzsm.a(arrayOfzzc[j]);
          this.a = arrayOfzzc;
          break;
        case 18: 
          this.b = paramzzsm.i();
          break;
        case 24: 
          this.c = Long.valueOf(paramzzsm.f());
          break;
        case 32: 
          this.d = Long.valueOf(paramzzsm.f());
          break;
        case 40: 
          this.e = Integer.valueOf(paramzzsm.g());
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if ((this.a != null) && (this.a.length > 0))
      {
        int i = 0;
        while (i < this.a.length)
        {
          zzqb.zzc localzzc = this.a[i];
          if (localzzc != null) {
            paramzzsn.a(1, localzzc);
          }
          i += 1;
        }
      }
      if (this.b != null) {
        paramzzsn.a(2, this.b);
      }
      if (this.c != null) {
        paramzzsn.b(3, this.c.longValue());
      }
      if (this.d != null) {
        paramzzsn.b(4, this.d.longValue());
      }
      if (this.e != null) {
        paramzzsn.a(5, this.e.intValue());
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int i = super.b();
      int j = i;
      if (this.a != null)
      {
        j = i;
        if (this.a.length > 0)
        {
          int k = 0;
          for (;;)
          {
            j = i;
            if (k >= this.a.length) {
              break;
            }
            zzqb.zzc localzzc = this.a[k];
            j = i;
            if (localzzc != null) {
              j = i + zzsn.c(1, localzzc);
            }
            k += 1;
            i = j;
          }
        }
      }
      i = j;
      if (this.b != null) {
        i = j + zzsn.b(2, this.b);
      }
      j = i;
      if (this.c != null) {
        j = i + zzsn.d(3, this.c.longValue());
      }
      i = j;
      if (this.d != null) {
        i = j + zzsn.d(4, this.d.longValue());
      }
      j = i;
      if (this.e != null) {
        j = i + zzsn.c(5, this.e.intValue());
      }
      return j;
    }
    
    public zzb c()
    {
      this.a = zzqb.zzc.a();
      this.b = null;
      this.c = null;
      this.d = null;
      this.e = null;
      this.S = -1;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      if (paramObject == this) {}
      do
      {
        do
        {
          return true;
          if (!(paramObject instanceof zzb)) {
            return false;
          }
          paramObject = (zzb)paramObject;
          if (!zzss.a(this.a, ((zzb)paramObject).a)) {
            return false;
          }
          if (this.b == null)
          {
            if (((zzb)paramObject).b != null) {
              return false;
            }
          }
          else if (!this.b.equals(((zzb)paramObject).b)) {
            return false;
          }
          if (this.c == null)
          {
            if (((zzb)paramObject).c != null) {
              return false;
            }
          }
          else if (!this.c.equals(((zzb)paramObject).c)) {
            return false;
          }
          if (this.d == null)
          {
            if (((zzb)paramObject).d != null) {
              return false;
            }
          }
          else if (!this.d.equals(((zzb)paramObject).d)) {
            return false;
          }
          if (this.e != null) {
            break;
          }
        } while (((zzb)paramObject).e == null);
        return false;
      } while (this.e.equals(((zzb)paramObject).e));
      return false;
    }
    
    public int hashCode()
    {
      int m = 0;
      int n = getClass().getName().hashCode();
      int i1 = zzss.a(this.a);
      int i;
      int j;
      label42:
      int k;
      if (this.b == null)
      {
        i = 0;
        if (this.c != null) {
          break label103;
        }
        j = 0;
        if (this.d != null) {
          break label114;
        }
        k = 0;
        label51:
        if (this.e != null) {
          break label125;
        }
      }
      for (;;)
      {
        return (k + (j + (i + ((n + 527) * 31 + i1) * 31) * 31) * 31) * 31 + m;
        i = this.b.hashCode();
        break;
        label103:
        j = this.c.hashCode();
        break label42;
        label114:
        k = this.d.hashCode();
        break label51;
        label125:
        m = this.e.hashCode();
      }
    }
  }
  
  public static final class zzc
    extends zzsu
  {
    private static volatile zzc[] e;
    public String a;
    public String b;
    public Long c;
    public Float d;
    
    public zzc()
    {
      c();
    }
    
    public static zzc[] a()
    {
      if (e == null) {}
      synchronized (zzss.a)
      {
        if (e == null) {
          e = new zzc[0];
        }
        return e;
      }
    }
    
    public zzc a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (zzsx.a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 10: 
          this.a = paramzzsm.i();
          break;
        case 18: 
          this.b = paramzzsm.i();
          break;
        case 24: 
          this.c = Long.valueOf(paramzzsm.f());
          break;
        case 37: 
          this.d = Float.valueOf(paramzzsm.d());
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (this.a != null) {
        paramzzsn.a(1, this.a);
      }
      if (this.b != null) {
        paramzzsn.a(2, this.b);
      }
      if (this.c != null) {
        paramzzsn.b(3, this.c.longValue());
      }
      if (this.d != null) {
        paramzzsn.a(4, this.d.floatValue());
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b();
      int i = j;
      if (this.a != null) {
        i = j + zzsn.b(1, this.a);
      }
      j = i;
      if (this.b != null) {
        j = i + zzsn.b(2, this.b);
      }
      i = j;
      if (this.c != null) {
        i = j + zzsn.d(3, this.c.longValue());
      }
      j = i;
      if (this.d != null) {
        j = i + zzsn.b(4, this.d.floatValue());
      }
      return j;
    }
    
    public zzc c()
    {
      this.a = null;
      this.b = null;
      this.c = null;
      this.d = null;
      this.S = -1;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      if (paramObject == this) {}
      do
      {
        do
        {
          return true;
          if (!(paramObject instanceof zzc)) {
            return false;
          }
          paramObject = (zzc)paramObject;
          if (this.a == null)
          {
            if (((zzc)paramObject).a != null) {
              return false;
            }
          }
          else if (!this.a.equals(((zzc)paramObject).a)) {
            return false;
          }
          if (this.b == null)
          {
            if (((zzc)paramObject).b != null) {
              return false;
            }
          }
          else if (!this.b.equals(((zzc)paramObject).b)) {
            return false;
          }
          if (this.c == null)
          {
            if (((zzc)paramObject).c != null) {
              return false;
            }
          }
          else if (!this.c.equals(((zzc)paramObject).c)) {
            return false;
          }
          if (this.d != null) {
            break;
          }
        } while (((zzc)paramObject).d == null);
        return false;
      } while (this.d.equals(((zzc)paramObject).d));
      return false;
    }
    
    public int hashCode()
    {
      int m = 0;
      int n = getClass().getName().hashCode();
      int i;
      int j;
      label33:
      int k;
      if (this.a == null)
      {
        i = 0;
        if (this.b != null) {
          break label88;
        }
        j = 0;
        if (this.c != null) {
          break label99;
        }
        k = 0;
        label42:
        if (this.d != null) {
          break label110;
        }
      }
      for (;;)
      {
        return (k + (j + (i + (n + 527) * 31) * 31) * 31) * 31 + m;
        i = this.a.hashCode();
        break;
        label88:
        j = this.b.hashCode();
        break label33;
        label99:
        k = this.c.hashCode();
        break label42;
        label110:
        m = this.d.hashCode();
      }
    }
  }
  
  public static final class zzd
    extends zzsu
  {
    public zzqb.zze[] a;
    
    public zzd()
    {
      a();
    }
    
    public zzd a()
    {
      this.a = zzqb.zze.a();
      this.S = -1;
      return this;
    }
    
    public zzd a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (zzsx.a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 10: 
          int j = zzsx.b(paramzzsm, 10);
          if (this.a == null) {}
          zzqb.zze[] arrayOfzze;
          for (i = 0;; i = this.a.length)
          {
            arrayOfzze = new zzqb.zze[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.a, 0, arrayOfzze, 0, i);
              j = i;
            }
            while (j < arrayOfzze.length - 1)
            {
              arrayOfzze[j] = new zzqb.zze();
              paramzzsm.a(arrayOfzze[j]);
              paramzzsm.a();
              j += 1;
            }
          }
          arrayOfzze[j] = new zzqb.zze();
          paramzzsm.a(arrayOfzze[j]);
          this.a = arrayOfzze;
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if ((this.a != null) && (this.a.length > 0))
      {
        int i = 0;
        while (i < this.a.length)
        {
          zzqb.zze localzze = this.a[i];
          if (localzze != null) {
            paramzzsn.a(1, localzze);
          }
          i += 1;
        }
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int i = super.b();
      int k = i;
      if (this.a != null)
      {
        k = i;
        if (this.a.length > 0)
        {
          int j = 0;
          for (;;)
          {
            k = i;
            if (j >= this.a.length) {
              break;
            }
            zzqb.zze localzze = this.a[j];
            k = i;
            if (localzze != null) {
              k = i + zzsn.c(1, localzze);
            }
            j += 1;
            i = k;
          }
        }
      }
      return k;
    }
    
    public boolean equals(Object paramObject)
    {
      if (paramObject == this) {}
      do
      {
        return true;
        if (!(paramObject instanceof zzd)) {
          return false;
        }
        paramObject = (zzd)paramObject;
      } while (zzss.a(this.a, ((zzd)paramObject).a));
      return false;
    }
    
    public int hashCode()
    {
      return (getClass().getName().hashCode() + 527) * 31 + zzss.a(this.a);
    }
  }
  
  public static final class zze
    extends zzsu
  {
    private static volatile zze[] B;
    public zzqb.zza[] A;
    public Integer a;
    public zzqb.zzb[] b;
    public zzqb.zzg[] c;
    public Long d;
    public Long e;
    public Long f;
    public Long g;
    public Long h;
    public String i;
    public String j;
    public String k;
    public String l;
    public Integer m;
    public String n;
    public String o;
    public String p;
    public Long q;
    public Long r;
    public String s;
    public Boolean t;
    public String u;
    public Long v;
    public Integer w;
    public String x;
    public String y;
    public Boolean z;
    
    public zze()
    {
      c();
    }
    
    public static zze[] a()
    {
      if (B == null) {}
      synchronized (zzss.a)
      {
        if (B == null) {
          B = new zze[0];
        }
        return B;
      }
    }
    
    public zze a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i1 = paramzzsm.a();
        int i2;
        Object localObject;
        switch (i1)
        {
        default: 
          if (zzsx.a(paramzzsm, i1)) {}
          break;
        case 0: 
          return this;
        case 8: 
          this.a = Integer.valueOf(paramzzsm.g());
          break;
        case 18: 
          i2 = zzsx.b(paramzzsm, 18);
          if (this.b == null) {}
          for (i1 = 0;; i1 = this.b.length)
          {
            localObject = new zzqb.zzb[i2 + i1];
            i2 = i1;
            if (i1 != 0)
            {
              System.arraycopy(this.b, 0, localObject, 0, i1);
              i2 = i1;
            }
            while (i2 < localObject.length - 1)
            {
              localObject[i2] = new zzqb.zzb();
              paramzzsm.a(localObject[i2]);
              paramzzsm.a();
              i2 += 1;
            }
          }
          localObject[i2] = new zzqb.zzb();
          paramzzsm.a(localObject[i2]);
          this.b = ((zzqb.zzb[])localObject);
          break;
        case 26: 
          i2 = zzsx.b(paramzzsm, 26);
          if (this.c == null) {}
          for (i1 = 0;; i1 = this.c.length)
          {
            localObject = new zzqb.zzg[i2 + i1];
            i2 = i1;
            if (i1 != 0)
            {
              System.arraycopy(this.c, 0, localObject, 0, i1);
              i2 = i1;
            }
            while (i2 < localObject.length - 1)
            {
              localObject[i2] = new zzqb.zzg();
              paramzzsm.a(localObject[i2]);
              paramzzsm.a();
              i2 += 1;
            }
          }
          localObject[i2] = new zzqb.zzg();
          paramzzsm.a(localObject[i2]);
          this.c = ((zzqb.zzg[])localObject);
          break;
        case 32: 
          this.d = Long.valueOf(paramzzsm.f());
          break;
        case 40: 
          this.e = Long.valueOf(paramzzsm.f());
          break;
        case 48: 
          this.f = Long.valueOf(paramzzsm.f());
          break;
        case 56: 
          this.h = Long.valueOf(paramzzsm.f());
          break;
        case 66: 
          this.i = paramzzsm.i();
          break;
        case 74: 
          this.j = paramzzsm.i();
          break;
        case 82: 
          this.k = paramzzsm.i();
          break;
        case 90: 
          this.l = paramzzsm.i();
          break;
        case 96: 
          this.m = Integer.valueOf(paramzzsm.g());
          break;
        case 106: 
          this.n = paramzzsm.i();
          break;
        case 114: 
          this.o = paramzzsm.i();
          break;
        case 130: 
          this.p = paramzzsm.i();
          break;
        case 136: 
          this.q = Long.valueOf(paramzzsm.f());
          break;
        case 144: 
          this.r = Long.valueOf(paramzzsm.f());
          break;
        case 154: 
          this.s = paramzzsm.i();
          break;
        case 160: 
          this.t = Boolean.valueOf(paramzzsm.h());
          break;
        case 170: 
          this.u = paramzzsm.i();
          break;
        case 176: 
          this.v = Long.valueOf(paramzzsm.f());
          break;
        case 184: 
          this.w = Integer.valueOf(paramzzsm.g());
          break;
        case 194: 
          this.x = paramzzsm.i();
          break;
        case 202: 
          this.y = paramzzsm.i();
          break;
        case 208: 
          this.g = Long.valueOf(paramzzsm.f());
          break;
        case 224: 
          this.z = Boolean.valueOf(paramzzsm.h());
          break;
        case 234: 
          i2 = zzsx.b(paramzzsm, 234);
          if (this.A == null) {}
          for (i1 = 0;; i1 = this.A.length)
          {
            localObject = new zzqb.zza[i2 + i1];
            i2 = i1;
            if (i1 != 0)
            {
              System.arraycopy(this.A, 0, localObject, 0, i1);
              i2 = i1;
            }
            while (i2 < localObject.length - 1)
            {
              localObject[i2] = new zzqb.zza();
              paramzzsm.a(localObject[i2]);
              paramzzsm.a();
              i2 += 1;
            }
          }
          localObject[i2] = new zzqb.zza();
          paramzzsm.a(localObject[i2]);
          this.A = ((zzqb.zza[])localObject);
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      int i2 = 0;
      if (this.a != null) {
        paramzzsn.a(1, this.a.intValue());
      }
      int i1;
      Object localObject;
      if ((this.b != null) && (this.b.length > 0))
      {
        i1 = 0;
        while (i1 < this.b.length)
        {
          localObject = this.b[i1];
          if (localObject != null) {
            paramzzsn.a(2, (zzsu)localObject);
          }
          i1 += 1;
        }
      }
      if ((this.c != null) && (this.c.length > 0))
      {
        i1 = 0;
        while (i1 < this.c.length)
        {
          localObject = this.c[i1];
          if (localObject != null) {
            paramzzsn.a(3, (zzsu)localObject);
          }
          i1 += 1;
        }
      }
      if (this.d != null) {
        paramzzsn.b(4, this.d.longValue());
      }
      if (this.e != null) {
        paramzzsn.b(5, this.e.longValue());
      }
      if (this.f != null) {
        paramzzsn.b(6, this.f.longValue());
      }
      if (this.h != null) {
        paramzzsn.b(7, this.h.longValue());
      }
      if (this.i != null) {
        paramzzsn.a(8, this.i);
      }
      if (this.j != null) {
        paramzzsn.a(9, this.j);
      }
      if (this.k != null) {
        paramzzsn.a(10, this.k);
      }
      if (this.l != null) {
        paramzzsn.a(11, this.l);
      }
      if (this.m != null) {
        paramzzsn.a(12, this.m.intValue());
      }
      if (this.n != null) {
        paramzzsn.a(13, this.n);
      }
      if (this.o != null) {
        paramzzsn.a(14, this.o);
      }
      if (this.p != null) {
        paramzzsn.a(16, this.p);
      }
      if (this.q != null) {
        paramzzsn.b(17, this.q.longValue());
      }
      if (this.r != null) {
        paramzzsn.b(18, this.r.longValue());
      }
      if (this.s != null) {
        paramzzsn.a(19, this.s);
      }
      if (this.t != null) {
        paramzzsn.a(20, this.t.booleanValue());
      }
      if (this.u != null) {
        paramzzsn.a(21, this.u);
      }
      if (this.v != null) {
        paramzzsn.b(22, this.v.longValue());
      }
      if (this.w != null) {
        paramzzsn.a(23, this.w.intValue());
      }
      if (this.x != null) {
        paramzzsn.a(24, this.x);
      }
      if (this.y != null) {
        paramzzsn.a(25, this.y);
      }
      if (this.g != null) {
        paramzzsn.b(26, this.g.longValue());
      }
      if (this.z != null) {
        paramzzsn.a(28, this.z.booleanValue());
      }
      if ((this.A != null) && (this.A.length > 0))
      {
        i1 = i2;
        while (i1 < this.A.length)
        {
          localObject = this.A[i1];
          if (localObject != null) {
            paramzzsn.a(29, (zzsu)localObject);
          }
          i1 += 1;
        }
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int i4 = 0;
      int i2 = super.b();
      int i1 = i2;
      if (this.a != null) {
        i1 = i2 + zzsn.c(1, this.a.intValue());
      }
      i2 = i1;
      Object localObject;
      if (this.b != null)
      {
        i2 = i1;
        if (this.b.length > 0)
        {
          i2 = 0;
          while (i2 < this.b.length)
          {
            localObject = this.b[i2];
            i3 = i1;
            if (localObject != null) {
              i3 = i1 + zzsn.c(2, (zzsu)localObject);
            }
            i2 += 1;
            i1 = i3;
          }
          i2 = i1;
        }
      }
      i1 = i2;
      if (this.c != null)
      {
        i1 = i2;
        if (this.c.length > 0)
        {
          i1 = i2;
          i2 = 0;
          while (i2 < this.c.length)
          {
            localObject = this.c[i2];
            i3 = i1;
            if (localObject != null) {
              i3 = i1 + zzsn.c(3, (zzsu)localObject);
            }
            i2 += 1;
            i1 = i3;
          }
        }
      }
      i2 = i1;
      if (this.d != null) {
        i2 = i1 + zzsn.d(4, this.d.longValue());
      }
      i1 = i2;
      if (this.e != null) {
        i1 = i2 + zzsn.d(5, this.e.longValue());
      }
      i2 = i1;
      if (this.f != null) {
        i2 = i1 + zzsn.d(6, this.f.longValue());
      }
      i1 = i2;
      if (this.h != null) {
        i1 = i2 + zzsn.d(7, this.h.longValue());
      }
      i2 = i1;
      if (this.i != null) {
        i2 = i1 + zzsn.b(8, this.i);
      }
      i1 = i2;
      if (this.j != null) {
        i1 = i2 + zzsn.b(9, this.j);
      }
      i2 = i1;
      if (this.k != null) {
        i2 = i1 + zzsn.b(10, this.k);
      }
      i1 = i2;
      if (this.l != null) {
        i1 = i2 + zzsn.b(11, this.l);
      }
      i2 = i1;
      if (this.m != null) {
        i2 = i1 + zzsn.c(12, this.m.intValue());
      }
      i1 = i2;
      if (this.n != null) {
        i1 = i2 + zzsn.b(13, this.n);
      }
      i2 = i1;
      if (this.o != null) {
        i2 = i1 + zzsn.b(14, this.o);
      }
      i1 = i2;
      if (this.p != null) {
        i1 = i2 + zzsn.b(16, this.p);
      }
      i2 = i1;
      if (this.q != null) {
        i2 = i1 + zzsn.d(17, this.q.longValue());
      }
      i1 = i2;
      if (this.r != null) {
        i1 = i2 + zzsn.d(18, this.r.longValue());
      }
      i2 = i1;
      if (this.s != null) {
        i2 = i1 + zzsn.b(19, this.s);
      }
      i1 = i2;
      if (this.t != null) {
        i1 = i2 + zzsn.b(20, this.t.booleanValue());
      }
      i2 = i1;
      if (this.u != null) {
        i2 = i1 + zzsn.b(21, this.u);
      }
      i1 = i2;
      if (this.v != null) {
        i1 = i2 + zzsn.d(22, this.v.longValue());
      }
      i2 = i1;
      if (this.w != null) {
        i2 = i1 + zzsn.c(23, this.w.intValue());
      }
      i1 = i2;
      if (this.x != null) {
        i1 = i2 + zzsn.b(24, this.x);
      }
      i2 = i1;
      if (this.y != null) {
        i2 = i1 + zzsn.b(25, this.y);
      }
      int i3 = i2;
      if (this.g != null) {
        i3 = i2 + zzsn.d(26, this.g.longValue());
      }
      i1 = i3;
      if (this.z != null) {
        i1 = i3 + zzsn.b(28, this.z.booleanValue());
      }
      i3 = i1;
      if (this.A != null)
      {
        i3 = i1;
        if (this.A.length > 0)
        {
          i2 = i4;
          for (;;)
          {
            i3 = i1;
            if (i2 >= this.A.length) {
              break;
            }
            localObject = this.A[i2];
            i3 = i1;
            if (localObject != null) {
              i3 = i1 + zzsn.c(29, (zzsu)localObject);
            }
            i2 += 1;
            i1 = i3;
          }
        }
      }
      return i3;
    }
    
    public zze c()
    {
      this.a = null;
      this.b = zzqb.zzb.a();
      this.c = zzqb.zzg.a();
      this.d = null;
      this.e = null;
      this.f = null;
      this.g = null;
      this.h = null;
      this.i = null;
      this.j = null;
      this.k = null;
      this.l = null;
      this.m = null;
      this.n = null;
      this.o = null;
      this.p = null;
      this.q = null;
      this.r = null;
      this.s = null;
      this.t = null;
      this.u = null;
      this.v = null;
      this.w = null;
      this.x = null;
      this.y = null;
      this.z = null;
      this.A = zzqb.zza.a();
      this.S = -1;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      if (paramObject == this) {}
      do
      {
        return true;
        if (!(paramObject instanceof zze)) {
          return false;
        }
        paramObject = (zze)paramObject;
        if (this.a == null)
        {
          if (((zze)paramObject).a != null) {
            return false;
          }
        }
        else if (!this.a.equals(((zze)paramObject).a)) {
          return false;
        }
        if (!zzss.a(this.b, ((zze)paramObject).b)) {
          return false;
        }
        if (!zzss.a(this.c, ((zze)paramObject).c)) {
          return false;
        }
        if (this.d == null)
        {
          if (((zze)paramObject).d != null) {
            return false;
          }
        }
        else if (!this.d.equals(((zze)paramObject).d)) {
          return false;
        }
        if (this.e == null)
        {
          if (((zze)paramObject).e != null) {
            return false;
          }
        }
        else if (!this.e.equals(((zze)paramObject).e)) {
          return false;
        }
        if (this.f == null)
        {
          if (((zze)paramObject).f != null) {
            return false;
          }
        }
        else if (!this.f.equals(((zze)paramObject).f)) {
          return false;
        }
        if (this.g == null)
        {
          if (((zze)paramObject).g != null) {
            return false;
          }
        }
        else if (!this.g.equals(((zze)paramObject).g)) {
          return false;
        }
        if (this.h == null)
        {
          if (((zze)paramObject).h != null) {
            return false;
          }
        }
        else if (!this.h.equals(((zze)paramObject).h)) {
          return false;
        }
        if (this.i == null)
        {
          if (((zze)paramObject).i != null) {
            return false;
          }
        }
        else if (!this.i.equals(((zze)paramObject).i)) {
          return false;
        }
        if (this.j == null)
        {
          if (((zze)paramObject).j != null) {
            return false;
          }
        }
        else if (!this.j.equals(((zze)paramObject).j)) {
          return false;
        }
        if (this.k == null)
        {
          if (((zze)paramObject).k != null) {
            return false;
          }
        }
        else if (!this.k.equals(((zze)paramObject).k)) {
          return false;
        }
        if (this.l == null)
        {
          if (((zze)paramObject).l != null) {
            return false;
          }
        }
        else if (!this.l.equals(((zze)paramObject).l)) {
          return false;
        }
        if (this.m == null)
        {
          if (((zze)paramObject).m != null) {
            return false;
          }
        }
        else if (!this.m.equals(((zze)paramObject).m)) {
          return false;
        }
        if (this.n == null)
        {
          if (((zze)paramObject).n != null) {
            return false;
          }
        }
        else if (!this.n.equals(((zze)paramObject).n)) {
          return false;
        }
        if (this.o == null)
        {
          if (((zze)paramObject).o != null) {
            return false;
          }
        }
        else if (!this.o.equals(((zze)paramObject).o)) {
          return false;
        }
        if (this.p == null)
        {
          if (((zze)paramObject).p != null) {
            return false;
          }
        }
        else if (!this.p.equals(((zze)paramObject).p)) {
          return false;
        }
        if (this.q == null)
        {
          if (((zze)paramObject).q != null) {
            return false;
          }
        }
        else if (!this.q.equals(((zze)paramObject).q)) {
          return false;
        }
        if (this.r == null)
        {
          if (((zze)paramObject).r != null) {
            return false;
          }
        }
        else if (!this.r.equals(((zze)paramObject).r)) {
          return false;
        }
        if (this.s == null)
        {
          if (((zze)paramObject).s != null) {
            return false;
          }
        }
        else if (!this.s.equals(((zze)paramObject).s)) {
          return false;
        }
        if (this.t == null)
        {
          if (((zze)paramObject).t != null) {
            return false;
          }
        }
        else if (!this.t.equals(((zze)paramObject).t)) {
          return false;
        }
        if (this.u == null)
        {
          if (((zze)paramObject).u != null) {
            return false;
          }
        }
        else if (!this.u.equals(((zze)paramObject).u)) {
          return false;
        }
        if (this.v == null)
        {
          if (((zze)paramObject).v != null) {
            return false;
          }
        }
        else if (!this.v.equals(((zze)paramObject).v)) {
          return false;
        }
        if (this.w == null)
        {
          if (((zze)paramObject).w != null) {
            return false;
          }
        }
        else if (!this.w.equals(((zze)paramObject).w)) {
          return false;
        }
        if (this.x == null)
        {
          if (((zze)paramObject).x != null) {
            return false;
          }
        }
        else if (!this.x.equals(((zze)paramObject).x)) {
          return false;
        }
        if (this.y == null)
        {
          if (((zze)paramObject).y != null) {
            return false;
          }
        }
        else if (!this.y.equals(((zze)paramObject).y)) {
          return false;
        }
        if (this.z == null)
        {
          if (((zze)paramObject).z != null) {
            return false;
          }
        }
        else if (!this.z.equals(((zze)paramObject).z)) {
          return false;
        }
      } while (zzss.a(this.A, ((zze)paramObject).A));
      return false;
    }
    
    public int hashCode()
    {
      int i24 = 0;
      int i25 = getClass().getName().hashCode();
      int i1;
      int i26;
      int i27;
      int i2;
      label51:
      int i3;
      label60:
      int i4;
      label70:
      int i5;
      label80:
      int i6;
      label90:
      int i7;
      label100:
      int i8;
      label110:
      int i9;
      label120:
      int i10;
      label130:
      int i11;
      label140:
      int i12;
      label150:
      int i13;
      label160:
      int i14;
      label170:
      int i15;
      label180:
      int i16;
      label190:
      int i17;
      label200:
      int i18;
      label210:
      int i19;
      label220:
      int i20;
      label230:
      int i21;
      label240:
      int i22;
      label250:
      int i23;
      if (this.a == null)
      {
        i1 = 0;
        i26 = zzss.a(this.b);
        i27 = zzss.a(this.c);
        if (this.d != null) {
          break label449;
        }
        i2 = 0;
        if (this.e != null) {
          break label460;
        }
        i3 = 0;
        if (this.f != null) {
          break label471;
        }
        i4 = 0;
        if (this.g != null) {
          break label483;
        }
        i5 = 0;
        if (this.h != null) {
          break label495;
        }
        i6 = 0;
        if (this.i != null) {
          break label507;
        }
        i7 = 0;
        if (this.j != null) {
          break label519;
        }
        i8 = 0;
        if (this.k != null) {
          break label531;
        }
        i9 = 0;
        if (this.l != null) {
          break label543;
        }
        i10 = 0;
        if (this.m != null) {
          break label555;
        }
        i11 = 0;
        if (this.n != null) {
          break label567;
        }
        i12 = 0;
        if (this.o != null) {
          break label579;
        }
        i13 = 0;
        if (this.p != null) {
          break label591;
        }
        i14 = 0;
        if (this.q != null) {
          break label603;
        }
        i15 = 0;
        if (this.r != null) {
          break label615;
        }
        i16 = 0;
        if (this.s != null) {
          break label627;
        }
        i17 = 0;
        if (this.t != null) {
          break label639;
        }
        i18 = 0;
        if (this.u != null) {
          break label651;
        }
        i19 = 0;
        if (this.v != null) {
          break label663;
        }
        i20 = 0;
        if (this.w != null) {
          break label675;
        }
        i21 = 0;
        if (this.x != null) {
          break label687;
        }
        i22 = 0;
        if (this.y != null) {
          break label699;
        }
        i23 = 0;
        label260:
        if (this.z != null) {
          break label711;
        }
      }
      for (;;)
      {
        return ((i23 + (i22 + (i21 + (i20 + (i19 + (i18 + (i17 + (i16 + (i15 + (i14 + (i13 + (i12 + (i11 + (i10 + (i9 + (i8 + (i7 + (i6 + (i5 + (i4 + (i3 + (i2 + (((i1 + (i25 + 527) * 31) * 31 + i26) * 31 + i27) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + i24) * 31 + zzss.a(this.A);
        i1 = this.a.hashCode();
        break;
        label449:
        i2 = this.d.hashCode();
        break label51;
        label460:
        i3 = this.e.hashCode();
        break label60;
        label471:
        i4 = this.f.hashCode();
        break label70;
        label483:
        i5 = this.g.hashCode();
        break label80;
        label495:
        i6 = this.h.hashCode();
        break label90;
        label507:
        i7 = this.i.hashCode();
        break label100;
        label519:
        i8 = this.j.hashCode();
        break label110;
        label531:
        i9 = this.k.hashCode();
        break label120;
        label543:
        i10 = this.l.hashCode();
        break label130;
        label555:
        i11 = this.m.hashCode();
        break label140;
        label567:
        i12 = this.n.hashCode();
        break label150;
        label579:
        i13 = this.o.hashCode();
        break label160;
        label591:
        i14 = this.p.hashCode();
        break label170;
        label603:
        i15 = this.q.hashCode();
        break label180;
        label615:
        i16 = this.r.hashCode();
        break label190;
        label627:
        i17 = this.s.hashCode();
        break label200;
        label639:
        i18 = this.t.hashCode();
        break label210;
        label651:
        i19 = this.u.hashCode();
        break label220;
        label663:
        i20 = this.v.hashCode();
        break label230;
        label675:
        i21 = this.w.hashCode();
        break label240;
        label687:
        i22 = this.x.hashCode();
        break label250;
        label699:
        i23 = this.y.hashCode();
        break label260;
        label711:
        i24 = this.z.hashCode();
      }
    }
  }
  
  public static final class zzf
    extends zzsu
  {
    public long[] a;
    public long[] b;
    
    public zzf()
    {
      a();
    }
    
    public zzf a()
    {
      this.a = zzsx.b;
      this.b = zzsx.b;
      this.S = -1;
      return this;
    }
    
    public zzf a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        int j;
        long[] arrayOfLong;
        int k;
        switch (i)
        {
        default: 
          if (zzsx.a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 8: 
          j = zzsx.b(paramzzsm, 8);
          if (this.a == null) {}
          for (i = 0;; i = this.a.length)
          {
            arrayOfLong = new long[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.a, 0, arrayOfLong, 0, i);
              j = i;
            }
            while (j < arrayOfLong.length - 1)
            {
              arrayOfLong[j] = paramzzsm.e();
              paramzzsm.a();
              j += 1;
            }
          }
          arrayOfLong[j] = paramzzsm.e();
          this.a = arrayOfLong;
          break;
        case 10: 
          k = paramzzsm.d(paramzzsm.m());
          i = paramzzsm.s();
          j = 0;
          while (paramzzsm.q() > 0)
          {
            paramzzsm.e();
            j += 1;
          }
          paramzzsm.f(i);
          if (this.a == null) {}
          for (i = 0;; i = this.a.length)
          {
            arrayOfLong = new long[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.a, 0, arrayOfLong, 0, i);
              j = i;
            }
            while (j < arrayOfLong.length)
            {
              arrayOfLong[j] = paramzzsm.e();
              j += 1;
            }
          }
          this.a = arrayOfLong;
          paramzzsm.e(k);
          break;
        case 16: 
          j = zzsx.b(paramzzsm, 16);
          if (this.b == null) {}
          for (i = 0;; i = this.b.length)
          {
            arrayOfLong = new long[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.b, 0, arrayOfLong, 0, i);
              j = i;
            }
            while (j < arrayOfLong.length - 1)
            {
              arrayOfLong[j] = paramzzsm.e();
              paramzzsm.a();
              j += 1;
            }
          }
          arrayOfLong[j] = paramzzsm.e();
          this.b = arrayOfLong;
          break;
        case 18: 
          k = paramzzsm.d(paramzzsm.m());
          i = paramzzsm.s();
          j = 0;
          while (paramzzsm.q() > 0)
          {
            paramzzsm.e();
            j += 1;
          }
          paramzzsm.f(i);
          if (this.b == null) {}
          for (i = 0;; i = this.b.length)
          {
            arrayOfLong = new long[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.b, 0, arrayOfLong, 0, i);
              j = i;
            }
            while (j < arrayOfLong.length)
            {
              arrayOfLong[j] = paramzzsm.e();
              j += 1;
            }
          }
          this.b = arrayOfLong;
          paramzzsm.e(k);
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      int j = 0;
      int i;
      if ((this.a != null) && (this.a.length > 0))
      {
        i = 0;
        while (i < this.a.length)
        {
          paramzzsn.a(1, this.a[i]);
          i += 1;
        }
      }
      if ((this.b != null) && (this.b.length > 0))
      {
        i = j;
        while (i < this.b.length)
        {
          paramzzsn.a(2, this.b[i]);
          i += 1;
        }
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int m = 0;
      int k = super.b();
      int j;
      if ((this.a != null) && (this.a.length > 0))
      {
        i = 0;
        j = 0;
        while (i < this.a.length)
        {
          j += zzsn.d(this.a[i]);
          i += 1;
        }
      }
      for (int i = k + j + this.a.length * 1;; i = k)
      {
        j = i;
        if (this.b != null)
        {
          j = i;
          if (this.b.length > 0)
          {
            k = 0;
            j = m;
            while (j < this.b.length)
            {
              k += zzsn.d(this.b[j]);
              j += 1;
            }
            j = i + k + this.b.length * 1;
          }
        }
        return j;
      }
    }
    
    public boolean equals(Object paramObject)
    {
      if (paramObject == this) {}
      do
      {
        return true;
        if (!(paramObject instanceof zzf)) {
          return false;
        }
        paramObject = (zzf)paramObject;
        if (!zzss.a(this.a, ((zzf)paramObject).a)) {
          return false;
        }
      } while (zzss.a(this.b, ((zzf)paramObject).b));
      return false;
    }
    
    public int hashCode()
    {
      return ((getClass().getName().hashCode() + 527) * 31 + zzss.a(this.a)) * 31 + zzss.a(this.b);
    }
  }
  
  public static final class zzg
    extends zzsu
  {
    private static volatile zzg[] f;
    public Long a;
    public String b;
    public String c;
    public Long d;
    public Float e;
    
    public zzg()
    {
      c();
    }
    
    public static zzg[] a()
    {
      if (f == null) {}
      synchronized (zzss.a)
      {
        if (f == null) {
          f = new zzg[0];
        }
        return f;
      }
    }
    
    public zzg a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (zzsx.a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 8: 
          this.a = Long.valueOf(paramzzsm.f());
          break;
        case 18: 
          this.b = paramzzsm.i();
          break;
        case 26: 
          this.c = paramzzsm.i();
          break;
        case 32: 
          this.d = Long.valueOf(paramzzsm.f());
          break;
        case 45: 
          this.e = Float.valueOf(paramzzsm.d());
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (this.a != null) {
        paramzzsn.b(1, this.a.longValue());
      }
      if (this.b != null) {
        paramzzsn.a(2, this.b);
      }
      if (this.c != null) {
        paramzzsn.a(3, this.c);
      }
      if (this.d != null) {
        paramzzsn.b(4, this.d.longValue());
      }
      if (this.e != null) {
        paramzzsn.a(5, this.e.floatValue());
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b();
      int i = j;
      if (this.a != null) {
        i = j + zzsn.d(1, this.a.longValue());
      }
      j = i;
      if (this.b != null) {
        j = i + zzsn.b(2, this.b);
      }
      i = j;
      if (this.c != null) {
        i = j + zzsn.b(3, this.c);
      }
      j = i;
      if (this.d != null) {
        j = i + zzsn.d(4, this.d.longValue());
      }
      i = j;
      if (this.e != null) {
        i = j + zzsn.b(5, this.e.floatValue());
      }
      return i;
    }
    
    public zzg c()
    {
      this.a = null;
      this.b = null;
      this.c = null;
      this.d = null;
      this.e = null;
      this.S = -1;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      if (paramObject == this) {}
      do
      {
        do
        {
          return true;
          if (!(paramObject instanceof zzg)) {
            return false;
          }
          paramObject = (zzg)paramObject;
          if (this.a == null)
          {
            if (((zzg)paramObject).a != null) {
              return false;
            }
          }
          else if (!this.a.equals(((zzg)paramObject).a)) {
            return false;
          }
          if (this.b == null)
          {
            if (((zzg)paramObject).b != null) {
              return false;
            }
          }
          else if (!this.b.equals(((zzg)paramObject).b)) {
            return false;
          }
          if (this.c == null)
          {
            if (((zzg)paramObject).c != null) {
              return false;
            }
          }
          else if (!this.c.equals(((zzg)paramObject).c)) {
            return false;
          }
          if (this.d == null)
          {
            if (((zzg)paramObject).d != null) {
              return false;
            }
          }
          else if (!this.d.equals(((zzg)paramObject).d)) {
            return false;
          }
          if (this.e != null) {
            break;
          }
        } while (((zzg)paramObject).e == null);
        return false;
      } while (this.e.equals(((zzg)paramObject).e));
      return false;
    }
    
    public int hashCode()
    {
      int n = 0;
      int i1 = getClass().getName().hashCode();
      int i;
      int j;
      label33:
      int k;
      label42:
      int m;
      if (this.a == null)
      {
        i = 0;
        if (this.b != null) {
          break label104;
        }
        j = 0;
        if (this.c != null) {
          break label115;
        }
        k = 0;
        if (this.d != null) {
          break label126;
        }
        m = 0;
        label52:
        if (this.e != null) {
          break label138;
        }
      }
      for (;;)
      {
        return (m + (k + (j + (i + (i1 + 527) * 31) * 31) * 31) * 31) * 31 + n;
        i = this.a.hashCode();
        break;
        label104:
        j = this.b.hashCode();
        break label33;
        label115:
        k = this.c.hashCode();
        break label42;
        label126:
        m = this.d.hashCode();
        break label52;
        label138:
        n = this.e.hashCode();
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzqb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */