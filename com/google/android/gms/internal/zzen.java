package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzr;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzhb
public final class zzen
{
  public final String a;
  public final String b;
  public final List<String> c;
  public final String d;
  public final String e;
  public final List<String> f;
  public final List<String> g;
  public final String h;
  public final List<String> i;
  public final List<String> j;
  public final String k;
  public final String l;
  public final String m;
  public final List<String> n;
  public final String o;
  
  public zzen(String paramString1, String paramString2, List<String> paramList1, String paramString3, String paramString4, List<String> paramList2, List<String> paramList3, String paramString5, String paramString6, List<String> paramList4, List<String> paramList5, String paramString7, String paramString8, String paramString9, List<String> paramList6, String paramString10)
  {
    this.a = paramString1;
    this.b = paramString2;
    this.c = paramList1;
    this.d = paramString3;
    this.e = paramString4;
    this.f = paramList2;
    this.g = paramList3;
    this.h = paramString5;
    this.i = paramList4;
    this.j = paramList5;
    this.k = paramString7;
    this.l = paramString8;
    this.m = paramString9;
    this.n = paramList6;
    this.o = paramString10;
  }
  
  public zzen(JSONObject paramJSONObject)
    throws JSONException
  {
    this.b = paramJSONObject.getString("id");
    Object localObject1 = paramJSONObject.getJSONArray("adapters");
    Object localObject3 = new ArrayList(((JSONArray)localObject1).length());
    int i1 = 0;
    while (i1 < ((JSONArray)localObject1).length())
    {
      ((List)localObject3).add(((JSONArray)localObject1).getString(i1));
      i1 += 1;
    }
    this.c = Collections.unmodifiableList((List)localObject3);
    this.d = paramJSONObject.optString("allocation_id", null);
    this.f = zzr.r().a(paramJSONObject, "clickurl");
    this.g = zzr.r().a(paramJSONObject, "imp_urls");
    this.i = zzr.r().a(paramJSONObject, "video_start_urls");
    this.j = zzr.r().a(paramJSONObject, "video_complete_urls");
    localObject1 = paramJSONObject.optJSONObject("ad");
    if (localObject1 != null)
    {
      localObject1 = ((JSONObject)localObject1).toString();
      this.a = ((String)localObject1);
      localObject3 = paramJSONObject.optJSONObject("data");
      if (localObject3 == null) {
        break label288;
      }
      localObject1 = ((JSONObject)localObject3).toString();
      label179:
      this.h = ((String)localObject1);
      if (localObject3 == null) {
        break label293;
      }
      localObject1 = ((JSONObject)localObject3).optString("class_name");
      label197:
      this.e = ((String)localObject1);
      this.k = paramJSONObject.optString("html_template", null);
      this.l = paramJSONObject.optString("ad_base_url", null);
      localObject1 = paramJSONObject.optJSONObject("assets");
      if (localObject1 == null) {
        break label298;
      }
    }
    label288:
    label293:
    label298:
    for (localObject1 = ((JSONObject)localObject1).toString();; localObject1 = null)
    {
      this.m = ((String)localObject1);
      this.n = zzr.r().a(paramJSONObject, "template_ids");
      localObject1 = paramJSONObject.optJSONObject("ad_loader_options");
      paramJSONObject = (JSONObject)localObject2;
      if (localObject1 != null) {
        paramJSONObject = ((JSONObject)localObject1).toString();
      }
      this.o = paramJSONObject;
      return;
      localObject1 = null;
      break;
      localObject1 = null;
      break label179;
      localObject1 = null;
      break label197;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzen.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */