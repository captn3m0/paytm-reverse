package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.zzr;
import com.google.android.gms.dynamic.zze;

@zzhb
public class zzhu
  extends zzim
  implements zzhv, zzhy
{
  private final zzif.zza a;
  private final Context b;
  private final zzia c;
  private final zzhy d;
  private final Object e;
  private final String f;
  private final String g;
  private final String h;
  private final String i;
  private int j = 0;
  private int k = 3;
  
  public zzhu(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4, zzif.zza paramzza, zzia paramzzia, zzhy paramzzhy)
  {
    this.b = paramContext;
    this.f = paramString1;
    this.i = paramString2;
    this.g = paramString3;
    this.h = paramString4;
    this.a = paramzza;
    this.c = paramzzia;
    this.e = new Object();
    this.d = paramzzhy;
  }
  
  private void a(AdRequestParcel paramAdRequestParcel, zzey paramzzey)
  {
    try
    {
      if ("com.google.ads.mediation.admob.AdMobAdapter".equals(this.f))
      {
        paramzzey.a(paramAdRequestParcel, this.g, this.h);
        return;
      }
      paramzzey.a(paramAdRequestParcel, this.g);
      return;
    }
    catch (RemoteException paramAdRequestParcel)
    {
      zzin.d("Fail to load ad from adapter.", paramAdRequestParcel);
      a(this.f, 0);
    }
  }
  
  private void b(long paramLong)
  {
    for (;;)
    {
      synchronized (this.e)
      {
        if (this.j != 0) {
          return;
        }
        if (!a(paramLong)) {
          return;
        }
      }
    }
  }
  
  public void a()
  {
    if ((this.c == null) || (this.c.b() == null) || (this.c.a() == null)) {
      return;
    }
    final zzhx localzzhx = this.c.b();
    localzzhx.a(this);
    localzzhx.a(this);
    final AdRequestParcel localAdRequestParcel = this.a.a.c;
    final zzey localzzey = this.c.a();
    try
    {
      if (localzzey.g()) {
        zza.a.post(new Runnable()
        {
          public void run()
          {
            zzhu.a(zzhu.this, localAdRequestParcel, localzzey);
          }
        });
      }
      for (;;)
      {
        b(zzr.i().b());
        localzzhx.a(null);
        localzzhx.a(null);
        if (this.j != 1) {
          break;
        }
        this.d.a(this.f);
        return;
        zza.a.post(new Runnable()
        {
          public void run()
          {
            try
            {
              localzzey.a(zze.a(zzhu.a(zzhu.this)), localAdRequestParcel, zzhu.b(zzhu.this), localzzhx, zzhu.c(zzhu.this));
              return;
            }
            catch (RemoteException localRemoteException)
            {
              zzin.d("Fail to initialize adapter " + zzhu.d(zzhu.this), localRemoteException);
              zzhu.this.a(zzhu.d(zzhu.this), 0);
            }
          }
        });
      }
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        zzin.d("Fail to check if adapter is initialized.", localRemoteException);
        a(this.f, 0);
      }
      this.d.a(this.f, this.k);
    }
  }
  
  public void a(int paramInt)
  {
    a(this.f, 0);
  }
  
  public void a(String arg1)
  {
    synchronized (this.e)
    {
      this.j = 1;
      this.e.notify();
      return;
    }
  }
  
  public void a(String arg1, int paramInt)
  {
    synchronized (this.e)
    {
      this.j = 2;
      this.k = paramInt;
      this.e.notify();
      return;
    }
  }
  
  protected boolean a(long paramLong)
  {
    paramLong = 20000L - (zzr.i().b() - paramLong);
    if (paramLong <= 0L) {
      return false;
    }
    try
    {
      this.e.wait(paramLong);
      return true;
    }
    catch (InterruptedException localInterruptedException) {}
    return false;
  }
  
  public void b() {}
  
  public void c()
  {
    a(this.a.a.c, this.c.a());
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzhu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */