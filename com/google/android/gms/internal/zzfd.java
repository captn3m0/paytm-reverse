package com.google.android.gms.internal;

import android.location.Location;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import java.util.Date;
import java.util.Set;

@zzhb
public final class zzfd
  implements MediationAdRequest
{
  private final Date a;
  private final int b;
  private final Set<String> c;
  private final boolean d;
  private final Location e;
  private final int f;
  private final boolean g;
  
  public zzfd(@Nullable Date paramDate, int paramInt1, @Nullable Set<String> paramSet, @Nullable Location paramLocation, boolean paramBoolean1, int paramInt2, boolean paramBoolean2)
  {
    this.a = paramDate;
    this.b = paramInt1;
    this.c = paramSet;
    this.e = paramLocation;
    this.d = paramBoolean1;
    this.f = paramInt2;
    this.g = paramBoolean2;
  }
  
  public Date a()
  {
    return this.a;
  }
  
  public int b()
  {
    return this.b;
  }
  
  public Set<String> c()
  {
    return this.c;
  }
  
  public Location d()
  {
    return this.e;
  }
  
  public int e()
  {
    return this.f;
  }
  
  public boolean f()
  {
    return this.d;
  }
  
  public boolean g()
  {
    return this.g;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzfd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */