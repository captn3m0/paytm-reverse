package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zzr;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzhb
public class zzet
{
  public List<String> a(JSONObject paramJSONObject, String paramString)
    throws JSONException
  {
    paramJSONObject = paramJSONObject.optJSONArray(paramString);
    if (paramJSONObject != null)
    {
      paramString = new ArrayList(paramJSONObject.length());
      int i = 0;
      while (i < paramJSONObject.length())
      {
        paramString.add(paramJSONObject.getString(i));
        i += 1;
      }
      return Collections.unmodifiableList(paramString);
    }
    return null;
  }
  
  public void a(Context paramContext, String paramString1, zzif paramzzif, String paramString2, boolean paramBoolean, List<String> paramList)
  {
    if ((paramList == null) || (paramList.isEmpty())) {
      return;
    }
    if (paramBoolean) {}
    for (String str1 = "1";; str1 = "0")
    {
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
      {
        String str2 = ((String)localIterator.next()).replaceAll("@gw_adlocid@", paramString2).replaceAll("@gw_adnetrefresh@", str1).replaceAll("@gw_qdata@", paramzzif.q.f).replaceAll("@gw_sdkver@", paramString1).replaceAll("@gw_sessid@", zzr.h().a()).replaceAll("@gw_seqnum@", paramzzif.i);
        paramList = str2;
        if (paramzzif.n != null) {
          paramList = str2.replaceAll("@gw_adnetid@", paramzzif.n.b).replaceAll("@gw_allocid@", paramzzif.n.d);
        }
        new zziy(paramContext, paramString1, paramList).f();
      }
      break;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzet.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */