package com.google.android.gms.internal;

import android.content.Context;
import com.google.ads.a.a.b.a;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class zzam
  extends zzal
{
  private static AdvertisingIdClient r = null;
  private static CountDownLatch s = new CountDownLatch(1);
  private boolean t;
  
  protected zzam(Context paramContext, zzap paramzzap, boolean paramBoolean)
  {
    super(paramContext, paramzzap);
    this.t = paramBoolean;
  }
  
  public static zzam a(String paramString, Context paramContext, boolean paramBoolean)
  {
    zzah localzzah = new zzah();
    a(paramString, paramContext, localzzah);
    if (paramBoolean) {}
    try
    {
      if (r == null) {
        new Thread(new zzb(paramContext)).start();
      }
      return new zzam(paramContext, localzzah, paramBoolean);
    }
    finally {}
  }
  
  private void a(Context paramContext, b.a parama)
  {
    if (!this.t) {}
    for (;;)
    {
      return;
      try
      {
        if (a())
        {
          paramContext = f();
          String str = paramContext.a();
          if (str != null)
          {
            parama.O = Boolean.valueOf(paramContext.b());
            parama.N = Integer.valueOf(5);
            parama.M = str;
            a(28, m);
          }
        }
        else
        {
          parama.M = d(paramContext);
          a(24, m);
          return;
        }
      }
      catch (zzal.zza paramContext) {}catch (IOException paramContext) {}
    }
  }
  
  protected b.a b(Context paramContext)
  {
    b.a locala = super.b(paramContext);
    a(paramContext, locala);
    return locala;
  }
  
  zza f()
    throws IOException
  {
    try
    {
      if (!s.await(2L, TimeUnit.SECONDS))
      {
        zza localzza1 = new zza(null, false);
        return localzza1;
      }
    }
    catch (InterruptedException localInterruptedException)
    {
      return new zza(null, false);
    }
    try
    {
      if (r == null)
      {
        zza localzza2 = new zza(null, false);
        return localzza2;
      }
    }
    finally {}
    AdvertisingIdClient.Info localInfo = r.b();
    return new zza(a(localInfo.a()), localInfo.b());
  }
  
  class zza
  {
    private String b;
    private boolean c;
    
    public zza(String paramString, boolean paramBoolean)
    {
      this.b = paramString;
      this.c = paramBoolean;
    }
    
    public String a()
    {
      return this.b;
    }
    
    public boolean b()
    {
      return this.c;
    }
  }
  
  private static final class zzb
    implements Runnable
  {
    private Context a;
    
    public zzb(Context paramContext)
    {
      this.a = paramContext.getApplicationContext();
      if (this.a == null) {
        this.a = paramContext;
      }
    }
    
    /* Error */
    public void run()
    {
      // Byte code:
      //   0: ldc 8
      //   2: monitorenter
      //   3: invokestatic 36	com/google/android/gms/internal/zzam:g	()Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;
      //   6: ifnonnull +28 -> 34
      //   9: iconst_1
      //   10: invokestatic 42	com/google/android/gms/ads/identifier/AdvertisingIdClient:b	(Z)V
      //   13: new 38	com/google/android/gms/ads/identifier/AdvertisingIdClient
      //   16: dup
      //   17: aload_0
      //   18: getfield 24	com/google/android/gms/internal/zzam$zzb:a	Landroid/content/Context;
      //   21: invokespecial 44	com/google/android/gms/ads/identifier/AdvertisingIdClient:<init>	(Landroid/content/Context;)V
      //   24: astore_1
      //   25: aload_1
      //   26: invokevirtual 46	com/google/android/gms/ads/identifier/AdvertisingIdClient:a	()V
      //   29: aload_1
      //   30: invokestatic 49	com/google/android/gms/internal/zzam:a	(Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;
      //   33: pop
      //   34: invokestatic 53	com/google/android/gms/internal/zzam:h	()Ljava/util/concurrent/CountDownLatch;
      //   37: invokevirtual 58	java/util/concurrent/CountDownLatch:countDown	()V
      //   40: ldc 8
      //   42: monitorexit
      //   43: return
      //   44: astore_1
      //   45: aconst_null
      //   46: invokestatic 49	com/google/android/gms/internal/zzam:a	(Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;
      //   49: pop
      //   50: invokestatic 53	com/google/android/gms/internal/zzam:h	()Ljava/util/concurrent/CountDownLatch;
      //   53: invokevirtual 58	java/util/concurrent/CountDownLatch:countDown	()V
      //   56: goto -16 -> 40
      //   59: astore_1
      //   60: ldc 8
      //   62: monitorexit
      //   63: aload_1
      //   64: athrow
      //   65: astore_1
      //   66: invokestatic 53	com/google/android/gms/internal/zzam:h	()Ljava/util/concurrent/CountDownLatch;
      //   69: invokevirtual 58	java/util/concurrent/CountDownLatch:countDown	()V
      //   72: aload_1
      //   73: athrow
      //   74: astore_1
      //   75: goto -30 -> 45
      //   78: astore_1
      //   79: goto -34 -> 45
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	82	0	this	zzb
      //   24	6	1	localAdvertisingIdClient	AdvertisingIdClient
      //   44	1	1	localGooglePlayServicesRepairableException	com.google.android.gms.common.GooglePlayServicesRepairableException
      //   59	5	1	localObject1	Object
      //   65	8	1	localObject2	Object
      //   74	1	1	localGooglePlayServicesNotAvailableException	com.google.android.gms.common.GooglePlayServicesNotAvailableException
      //   78	1	1	localIOException	IOException
      // Exception table:
      //   from	to	target	type
      //   3	34	44	com/google/android/gms/common/GooglePlayServicesRepairableException
      //   34	40	59	finally
      //   40	43	59	finally
      //   50	56	59	finally
      //   60	63	59	finally
      //   66	74	59	finally
      //   3	34	65	finally
      //   45	50	65	finally
      //   3	34	74	com/google/android/gms/common/GooglePlayServicesNotAvailableException
      //   3	34	78	java/io/IOException
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzam.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */