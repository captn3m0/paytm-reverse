package com.google.android.gms.internal;

import android.os.Bundle;
import android.support.v4.util.LongSparseArray;
import android.util.SparseArray;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.CustomPropertyKey;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties.zza;
import com.google.android.gms.drive.metadata.internal.CustomProperty;
import com.google.android.gms.drive.metadata.internal.zze.zza;
import com.google.android.gms.drive.metadata.internal.zzk;
import java.util.Arrays;

public class zznn
  extends zzk<AppVisibleCustomProperties>
{
  public static final zze.zza a = new zze.zza()
  {
    public String a()
    {
      return "customPropertiesExtraHolder";
    }
    
    public void a(DataHolder paramAnonymousDataHolder)
    {
      zznn.a(paramAnonymousDataHolder);
    }
  };
  
  public zznn(int paramInt)
  {
    super("customProperties", Arrays.asList(new String[] { "hasCustomProperties", "sqlId" }), Arrays.asList(new String[] { "customPropertiesExtra", "customPropertiesExtraHolder" }), paramInt);
  }
  
  private static LongSparseArray<AppVisibleCustomProperties.zza> b(DataHolder paramDataHolder)
  {
    Object localObject1 = paramDataHolder.f();
    String str1 = ((Bundle)localObject1).getString("entryIdColumn");
    String str2 = ((Bundle)localObject1).getString("keyColumn");
    String str3 = ((Bundle)localObject1).getString("visibilityColumn");
    String str4 = ((Bundle)localObject1).getString("valueColumn");
    LongSparseArray localLongSparseArray = new LongSparseArray();
    int i = 0;
    while (i < paramDataHolder.g())
    {
      int j = paramDataHolder.a(i);
      long l = paramDataHolder.a(str1, i, j);
      localObject1 = paramDataHolder.c(str2, i, j);
      int k = paramDataHolder.b(str3, i, j);
      Object localObject2 = paramDataHolder.c(str4, i, j);
      CustomProperty localCustomProperty = new CustomProperty(new CustomPropertyKey((String)localObject1, k), (String)localObject2);
      localObject2 = (AppVisibleCustomProperties.zza)localLongSparseArray.a(l);
      localObject1 = localObject2;
      if (localObject2 == null)
      {
        localObject1 = new AppVisibleCustomProperties.zza();
        localLongSparseArray.b(l, localObject1);
      }
      ((AppVisibleCustomProperties.zza)localObject1).a(localCustomProperty);
      i += 1;
    }
    return localLongSparseArray;
  }
  
  /* Error */
  private static void c(DataHolder paramDataHolder)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual 51	com/google/android/gms/common/data/DataHolder:f	()Landroid/os/Bundle;
    //   6: ldc 36
    //   8: invokevirtual 113	android/os/Bundle:getParcelable	(Ljava/lang/String;)Landroid/os/Parcelable;
    //   11: checkcast 47	com/google/android/gms/common/data/DataHolder
    //   14: astore_2
    //   15: aload_2
    //   16: ifnonnull +6 -> 22
    //   19: aload_0
    //   20: monitorexit
    //   21: return
    //   22: aload_2
    //   23: invokestatic 115	com/google/android/gms/internal/zznn:b	(Lcom/google/android/gms/common/data/DataHolder;)Landroid/support/v4/util/LongSparseArray;
    //   26: astore_3
    //   27: new 117	android/util/SparseArray
    //   30: dup
    //   31: invokespecial 118	android/util/SparseArray:<init>	()V
    //   34: astore 4
    //   36: iconst_0
    //   37: istore_1
    //   38: iload_1
    //   39: aload_0
    //   40: invokevirtual 72	com/google/android/gms/common/data/DataHolder:g	()I
    //   43: if_icmpge +43 -> 86
    //   46: aload_3
    //   47: aload_0
    //   48: ldc 26
    //   50: iload_1
    //   51: aload_0
    //   52: iload_1
    //   53: invokevirtual 75	com/google/android/gms/common/data/DataHolder:a	(I)I
    //   56: invokevirtual 78	com/google/android/gms/common/data/DataHolder:a	(Ljava/lang/String;II)J
    //   59: invokevirtual 98	android/support/v4/util/LongSparseArray:a	(J)Ljava/lang/Object;
    //   62: checkcast 100	com/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties$zza
    //   65: astore 5
    //   67: aload 5
    //   69: ifnull +65 -> 134
    //   72: aload 4
    //   74: iload_1
    //   75: aload 5
    //   77: invokevirtual 121	com/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties$zza:a	()Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;
    //   80: invokevirtual 125	android/util/SparseArray:append	(ILjava/lang/Object;)V
    //   83: goto +51 -> 134
    //   86: aload_0
    //   87: invokevirtual 51	com/google/android/gms/common/data/DataHolder:f	()Landroid/os/Bundle;
    //   90: ldc 34
    //   92: aload 4
    //   94: invokevirtual 129	android/os/Bundle:putSparseParcelableArray	(Ljava/lang/String;Landroid/util/SparseArray;)V
    //   97: aload_2
    //   98: invokevirtual 132	com/google/android/gms/common/data/DataHolder:i	()V
    //   101: aload_0
    //   102: invokevirtual 51	com/google/android/gms/common/data/DataHolder:f	()Landroid/os/Bundle;
    //   105: ldc 36
    //   107: invokevirtual 136	android/os/Bundle:remove	(Ljava/lang/String;)V
    //   110: aload_0
    //   111: monitorexit
    //   112: return
    //   113: astore_2
    //   114: aload_0
    //   115: monitorexit
    //   116: aload_2
    //   117: athrow
    //   118: astore_3
    //   119: aload_2
    //   120: invokevirtual 132	com/google/android/gms/common/data/DataHolder:i	()V
    //   123: aload_0
    //   124: invokevirtual 51	com/google/android/gms/common/data/DataHolder:f	()Landroid/os/Bundle;
    //   127: ldc 36
    //   129: invokevirtual 136	android/os/Bundle:remove	(Ljava/lang/String;)V
    //   132: aload_3
    //   133: athrow
    //   134: iload_1
    //   135: iconst_1
    //   136: iadd
    //   137: istore_1
    //   138: goto -100 -> 38
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	141	0	paramDataHolder	DataHolder
    //   37	101	1	i	int
    //   14	84	2	localDataHolder	DataHolder
    //   113	7	2	localObject1	Object
    //   26	21	3	localLongSparseArray	LongSparseArray
    //   118	15	3	localObject2	Object
    //   34	59	4	localSparseArray	SparseArray
    //   65	11	5	localzza	AppVisibleCustomProperties.zza
    // Exception table:
    //   from	to	target	type
    //   2	15	113	finally
    //   19	21	113	finally
    //   97	112	113	finally
    //   114	116	113	finally
    //   119	134	113	finally
    //   22	36	118	finally
    //   38	67	118	finally
    //   72	83	118	finally
    //   86	97	118	finally
  }
  
  private static void d(DataHolder paramDataHolder)
  {
    Bundle localBundle = paramDataHolder.f();
    if (localBundle == null) {
      return;
    }
    try
    {
      DataHolder localDataHolder = (DataHolder)localBundle.getParcelable("customPropertiesExtraHolder");
      if (localDataHolder != null)
      {
        localDataHolder.i();
        localBundle.remove("customPropertiesExtraHolder");
      }
      return;
    }
    finally {}
  }
  
  protected AppVisibleCustomProperties d(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    Bundle localBundle = paramDataHolder.f();
    SparseArray localSparseArray1 = localBundle.getSparseParcelableArray("customPropertiesExtra");
    SparseArray localSparseArray2 = localSparseArray1;
    if (localSparseArray1 == null)
    {
      if (localBundle.getParcelable("customPropertiesExtraHolder") != null)
      {
        c(paramDataHolder);
        localSparseArray1 = localBundle.getSparseParcelableArray("customPropertiesExtra");
      }
      localSparseArray2 = localSparseArray1;
      if (localSparseArray1 == null) {
        return AppVisibleCustomProperties.a;
      }
    }
    return (AppVisibleCustomProperties)localSparseArray2.get(paramInt1, AppVisibleCustomProperties.a);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zznn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */