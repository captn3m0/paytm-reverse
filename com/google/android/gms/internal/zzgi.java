package com.google.android.gms.internal;

import com.google.android.gms.ads.purchase.InAppPurchaseListener;

@zzhb
public final class zzgi
  extends zzgd.zza
{
  private final InAppPurchaseListener a;
  
  public zzgi(InAppPurchaseListener paramInAppPurchaseListener)
  {
    this.a = paramInAppPurchaseListener;
  }
  
  public void a(zzgc paramzzgc)
  {
    this.a.a(new zzgl(paramzzgc));
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzgi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */