package com.google.android.gms.internal;

public final class zzsq
  implements Cloneable
{
  private static final zzsr a = new zzsr();
  private boolean b = false;
  private int[] c;
  private zzsr[] d;
  private int e;
  
  zzsq()
  {
    this(10);
  }
  
  zzsq(int paramInt)
  {
    paramInt = c(paramInt);
    this.c = new int[paramInt];
    this.d = new zzsr[paramInt];
    this.e = 0;
  }
  
  private boolean a(int[] paramArrayOfInt1, int[] paramArrayOfInt2, int paramInt)
  {
    int i = 0;
    while (i < paramInt)
    {
      if (paramArrayOfInt1[i] != paramArrayOfInt2[i]) {
        return false;
      }
      i += 1;
    }
    return true;
  }
  
  private boolean a(zzsr[] paramArrayOfzzsr1, zzsr[] paramArrayOfzzsr2, int paramInt)
  {
    int i = 0;
    while (i < paramInt)
    {
      if (!paramArrayOfzzsr1[i].equals(paramArrayOfzzsr2[i])) {
        return false;
      }
      i += 1;
    }
    return true;
  }
  
  private int c(int paramInt)
  {
    return d(paramInt * 4) / 4;
  }
  
  private int d(int paramInt)
  {
    int i = 4;
    for (;;)
    {
      int j = paramInt;
      if (i < 32)
      {
        if (paramInt <= (1 << i) - 12) {
          j = (1 << i) - 12;
        }
      }
      else {
        return j;
      }
      i += 1;
    }
  }
  
  private void d()
  {
    int m = this.e;
    int[] arrayOfInt = this.c;
    zzsr[] arrayOfzzsr = this.d;
    int i = 0;
    int k;
    for (int j = 0; i < m; j = k)
    {
      zzsr localzzsr = arrayOfzzsr[i];
      k = j;
      if (localzzsr != a)
      {
        if (i != j)
        {
          arrayOfInt[j] = arrayOfInt[i];
          arrayOfzzsr[j] = localzzsr;
          arrayOfzzsr[i] = null;
        }
        k = j + 1;
      }
      i += 1;
    }
    this.b = false;
    this.e = j;
  }
  
  private int e(int paramInt)
  {
    int i = 0;
    int j = this.e - 1;
    while (i <= j)
    {
      int k = i + j >>> 1;
      int m = this.c[k];
      if (m < paramInt) {
        i = k + 1;
      } else if (m > paramInt) {
        j = k - 1;
      } else {
        return k;
      }
    }
    return i ^ 0xFFFFFFFF;
  }
  
  int a()
  {
    if (this.b) {
      d();
    }
    return this.e;
  }
  
  zzsr a(int paramInt)
  {
    paramInt = e(paramInt);
    if ((paramInt < 0) || (this.d[paramInt] == a)) {
      return null;
    }
    return this.d[paramInt];
  }
  
  void a(int paramInt, zzsr paramzzsr)
  {
    int i = e(paramInt);
    if (i >= 0)
    {
      this.d[i] = paramzzsr;
      return;
    }
    int j = i ^ 0xFFFFFFFF;
    if ((j < this.e) && (this.d[j] == a))
    {
      this.c[j] = paramInt;
      this.d[j] = paramzzsr;
      return;
    }
    i = j;
    if (this.b)
    {
      i = j;
      if (this.e >= this.c.length)
      {
        d();
        i = e(paramInt) ^ 0xFFFFFFFF;
      }
    }
    if (this.e >= this.c.length)
    {
      j = c(this.e + 1);
      int[] arrayOfInt = new int[j];
      zzsr[] arrayOfzzsr = new zzsr[j];
      System.arraycopy(this.c, 0, arrayOfInt, 0, this.c.length);
      System.arraycopy(this.d, 0, arrayOfzzsr, 0, this.d.length);
      this.c = arrayOfInt;
      this.d = arrayOfzzsr;
    }
    if (this.e - i != 0)
    {
      System.arraycopy(this.c, i, this.c, i + 1, this.e - i);
      System.arraycopy(this.d, i, this.d, i + 1, this.e - i);
    }
    this.c[i] = paramInt;
    this.d[i] = paramzzsr;
    this.e += 1;
  }
  
  zzsr b(int paramInt)
  {
    if (this.b) {
      d();
    }
    return this.d[paramInt];
  }
  
  public boolean b()
  {
    return a() == 0;
  }
  
  public final zzsq c()
  {
    int i = 0;
    int j = a();
    zzsq localzzsq = new zzsq(j);
    System.arraycopy(this.c, 0, localzzsq.c, 0, j);
    while (i < j)
    {
      if (this.d[i] != null) {
        localzzsq.d[i] = this.d[i].b();
      }
      i += 1;
    }
    localzzsq.e = j;
    return localzzsq;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzsq)) {
        return false;
      }
      paramObject = (zzsq)paramObject;
      if (a() != ((zzsq)paramObject).a()) {
        return false;
      }
    } while ((a(this.c, ((zzsq)paramObject).c, this.e)) && (a(this.d, ((zzsq)paramObject).d, this.e)));
    return false;
  }
  
  public int hashCode()
  {
    if (this.b) {
      d();
    }
    int j = 17;
    int i = 0;
    while (i < this.e)
    {
      j = (j * 31 + this.c[i]) * 31 + this.d[i].hashCode();
      i += 1;
    }
    return j;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzsq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */