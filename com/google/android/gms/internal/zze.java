package com.google.android.gms.internal;

import android.os.Handler;
import java.util.concurrent.Executor;

public class zze
  implements zzn
{
  private final Executor a;
  
  public zze(final Handler paramHandler)
  {
    this.a = new Executor()
    {
      public void execute(Runnable paramAnonymousRunnable)
      {
        paramHandler.post(paramAnonymousRunnable);
      }
    };
  }
  
  public void a(zzk<?> paramzzk, zzm<?> paramzzm)
  {
    a(paramzzk, paramzzm, null);
  }
  
  public void a(zzk<?> paramzzk, zzm<?> paramzzm, Runnable paramRunnable)
  {
    paramzzk.t();
    paramzzk.b("post-response");
    this.a.execute(new zza(paramzzk, paramzzm, paramRunnable));
  }
  
  public void a(zzk<?> paramzzk, zzr paramzzr)
  {
    paramzzk.b("post-error");
    paramzzr = zzm.a(paramzzr);
    this.a.execute(new zza(paramzzk, paramzzr, null));
  }
  
  private class zza
    implements Runnable
  {
    private final zzk b;
    private final zzm c;
    private final Runnable d;
    
    public zza(zzk paramzzk, zzm paramzzm, Runnable paramRunnable)
    {
      this.b = paramzzk;
      this.c = paramzzm;
      this.d = paramRunnable;
    }
    
    public void run()
    {
      if (this.b.g()) {
        this.b.c("canceled-at-delivery");
      }
      label97:
      label107:
      for (;;)
      {
        return;
        if (this.c.a())
        {
          this.b.a(this.c.a);
          if (!this.c.d) {
            break label97;
          }
          this.b.b("intermediate-response");
        }
        for (;;)
        {
          if (this.d == null) {
            break label107;
          }
          this.d.run();
          return;
          this.b.b(this.c.c);
          break;
          this.b.c("done");
        }
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */