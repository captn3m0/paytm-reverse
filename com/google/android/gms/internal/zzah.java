package com.google.android.gms.internal;

import android.util.Base64;

class zzah
  implements zzap
{
  public String a(byte[] paramArrayOfByte, boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (int i = 11;; i = 2) {
      return Base64.encodeToString(paramArrayOfByte, i);
    }
  }
  
  public byte[] a(String paramString, boolean paramBoolean)
    throws IllegalArgumentException
  {
    if (paramBoolean) {}
    for (int i = 11;; i = 2) {
      return Base64.decode(paramString, i);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzah.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */