package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.nearby.connection.AppMetadata;

public abstract class zzqj
  extends zzqm.zza
{
  public void a(int paramInt)
    throws RemoteException
  {}
  
  public void a(int paramInt, String paramString)
    throws RemoteException
  {}
  
  public void a(String paramString)
    throws RemoteException
  {}
  
  public void a(String paramString, int paramInt, byte[] paramArrayOfByte)
    throws RemoteException
  {}
  
  public void a(String paramString1, String paramString2, String paramString3, String paramString4)
    throws RemoteException
  {}
  
  public void a(String paramString1, String paramString2, String paramString3, String paramString4, AppMetadata paramAppMetadata)
    throws RemoteException
  {}
  
  public void a(String paramString1, String paramString2, String paramString3, byte[] paramArrayOfByte)
    throws RemoteException
  {}
  
  public void a(String paramString, byte[] paramArrayOfByte, boolean paramBoolean)
    throws RemoteException
  {}
  
  public void b(int paramInt)
    throws RemoteException
  {}
  
  public void b(String paramString)
    throws RemoteException
  {}
  
  public void c(int paramInt)
    throws RemoteException
  {}
  
  public void c(String paramString)
    throws RemoteException
  {}
  
  public void d(int paramInt)
    throws RemoteException
  {}
  
  public void e(int paramInt)
    throws RemoteException
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzqj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */