package com.google.android.gms.internal;

import java.io.IOException;

public abstract interface zzaf
{
  public static final class zza
    extends zzso<zza>
  {
    public int a;
    public int b;
    public int c;
    
    public zza()
    {
      a();
    }
    
    public zza a()
    {
      this.a = 1;
      this.b = 0;
      this.c = 0;
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public zza a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 8: 
          i = paramzzsm.g();
          switch (i)
          {
          default: 
            break;
          case 1: 
          case 2: 
          case 3: 
            this.a = i;
          }
          break;
        case 16: 
          this.b = paramzzsm.g();
          break;
        case 24: 
          this.c = paramzzsm.g();
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (this.a != 1) {
        paramzzsn.a(1, this.a);
      }
      if (this.b != 0) {
        paramzzsn.a(2, this.b);
      }
      if (this.c != 0) {
        paramzzsn.a(3, this.c);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b();
      int i = j;
      if (this.a != 1) {
        i = j + zzsn.c(1, this.a);
      }
      j = i;
      if (this.b != 0) {
        j = i + zzsn.c(2, this.b);
      }
      i = j;
      if (this.c != 0) {
        i = j + zzsn.c(3, this.c);
      }
      return i;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                return bool1;
                bool1 = bool2;
              } while (!(paramObject instanceof zza));
              paramObject = (zza)paramObject;
              bool1 = bool2;
            } while (this.a != ((zza)paramObject).a);
            bool1 = bool2;
          } while (this.b != ((zza)paramObject).b);
          bool1 = bool2;
        } while (this.c != ((zza)paramObject).c);
        if ((this.r != null) && (!this.r.b())) {
          break label102;
        }
        if (((zza)paramObject).r == null) {
          break;
        }
        bool1 = bool2;
      } while (!((zza)paramObject).r.b());
      return true;
      label102:
      return this.r.equals(((zza)paramObject).r);
    }
    
    public int hashCode()
    {
      int j = getClass().getName().hashCode();
      int k = this.a;
      int m = this.b;
      int n = this.c;
      if ((this.r == null) || (this.r.b())) {}
      for (int i = 0;; i = this.r.hashCode()) {
        return i + ((((j + 527) * 31 + k) * 31 + m) * 31 + n) * 31;
      }
    }
  }
  
  public static final class zzb
    extends zzso<zzb>
  {
    private static volatile zzb[] f;
    public int[] a;
    public int b;
    public int c;
    public boolean d;
    public boolean e;
    
    public zzb()
    {
      c();
    }
    
    public static zzb[] a()
    {
      if (f == null) {}
      synchronized (zzss.a)
      {
        if (f == null) {
          f = new zzb[0];
        }
        return f;
      }
    }
    
    public zzb a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        int j;
        int[] arrayOfInt;
        switch (i)
        {
        default: 
          if (a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 8: 
          this.e = paramzzsm.h();
          break;
        case 16: 
          this.b = paramzzsm.g();
          break;
        case 24: 
          j = zzsx.b(paramzzsm, 24);
          if (this.a == null) {}
          for (i = 0;; i = this.a.length)
          {
            arrayOfInt = new int[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.a, 0, arrayOfInt, 0, i);
              j = i;
            }
            while (j < arrayOfInt.length - 1)
            {
              arrayOfInt[j] = paramzzsm.g();
              paramzzsm.a();
              j += 1;
            }
          }
          arrayOfInt[j] = paramzzsm.g();
          this.a = arrayOfInt;
          break;
        case 26: 
          int k = paramzzsm.d(paramzzsm.m());
          i = paramzzsm.s();
          j = 0;
          while (paramzzsm.q() > 0)
          {
            paramzzsm.g();
            j += 1;
          }
          paramzzsm.f(i);
          if (this.a == null) {}
          for (i = 0;; i = this.a.length)
          {
            arrayOfInt = new int[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.a, 0, arrayOfInt, 0, i);
              j = i;
            }
            while (j < arrayOfInt.length)
            {
              arrayOfInt[j] = paramzzsm.g();
              j += 1;
            }
          }
          this.a = arrayOfInt;
          paramzzsm.e(k);
          break;
        case 32: 
          this.c = paramzzsm.g();
          break;
        case 48: 
          this.d = paramzzsm.h();
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (this.e) {
        paramzzsn.a(1, this.e);
      }
      paramzzsn.a(2, this.b);
      if ((this.a != null) && (this.a.length > 0))
      {
        int i = 0;
        while (i < this.a.length)
        {
          paramzzsn.a(3, this.a[i]);
          i += 1;
        }
      }
      if (this.c != 0) {
        paramzzsn.a(4, this.c);
      }
      if (this.d) {
        paramzzsn.a(6, this.d);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = 0;
      int k = super.b();
      int i = k;
      if (this.e) {
        i = k + zzsn.b(1, this.e);
      }
      k = zzsn.c(2, this.b) + i;
      if ((this.a != null) && (this.a.length > 0))
      {
        i = 0;
        while (i < this.a.length)
        {
          j += zzsn.c(this.a[i]);
          i += 1;
        }
      }
      for (j = k + j + this.a.length * 1;; j = k)
      {
        i = j;
        if (this.c != 0) {
          i = j + zzsn.c(4, this.c);
        }
        j = i;
        if (this.d) {
          j = i + zzsn.b(6, this.d);
        }
        return j;
      }
    }
    
    public zzb c()
    {
      this.a = zzsx.a;
      this.b = 0;
      this.c = 0;
      this.d = false;
      this.e = false;
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  do
                  {
                    return bool1;
                    bool1 = bool2;
                  } while (!(paramObject instanceof zzb));
                  paramObject = (zzb)paramObject;
                  bool1 = bool2;
                } while (!zzss.a(this.a, ((zzb)paramObject).a));
                bool1 = bool2;
              } while (this.b != ((zzb)paramObject).b);
              bool1 = bool2;
            } while (this.c != ((zzb)paramObject).c);
            bool1 = bool2;
          } while (this.d != ((zzb)paramObject).d);
          bool1 = bool2;
        } while (this.e != ((zzb)paramObject).e);
        if ((this.r != null) && (!this.r.b())) {
          break label131;
        }
        if (((zzb)paramObject).r == null) {
          break;
        }
        bool1 = bool2;
      } while (!((zzb)paramObject).r.b());
      return true;
      label131:
      return this.r.equals(((zzb)paramObject).r);
    }
    
    public int hashCode()
    {
      int j = 1231;
      int m = getClass().getName().hashCode();
      int n = zzss.a(this.a);
      int i1 = this.b;
      int i2 = this.c;
      int i;
      if (this.d)
      {
        i = 1231;
        if (!this.e) {
          break label121;
        }
        label55:
        if ((this.r != null) && (!this.r.b())) {
          break label128;
        }
      }
      label121:
      label128:
      for (int k = 0;; k = this.r.hashCode())
      {
        return k + ((i + ((((m + 527) * 31 + n) * 31 + i1) * 31 + i2) * 31) * 31 + j) * 31;
        i = 1237;
        break;
        j = 1237;
        break label55;
      }
    }
  }
  
  public static final class zzc
    extends zzso<zzc>
  {
    private static volatile zzc[] f;
    public String a;
    public long b;
    public long c;
    public boolean d;
    public long e;
    
    public zzc()
    {
      c();
    }
    
    public static zzc[] a()
    {
      if (f == null) {}
      synchronized (zzss.a)
      {
        if (f == null) {
          f = new zzc[0];
        }
        return f;
      }
    }
    
    public zzc a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 10: 
          this.a = paramzzsm.i();
          break;
        case 16: 
          this.b = paramzzsm.f();
          break;
        case 24: 
          this.c = paramzzsm.f();
          break;
        case 32: 
          this.d = paramzzsm.h();
          break;
        case 40: 
          this.e = paramzzsm.f();
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (!this.a.equals("")) {
        paramzzsn.a(1, this.a);
      }
      if (this.b != 0L) {
        paramzzsn.b(2, this.b);
      }
      if (this.c != 2147483647L) {
        paramzzsn.b(3, this.c);
      }
      if (this.d) {
        paramzzsn.a(4, this.d);
      }
      if (this.e != 0L) {
        paramzzsn.b(5, this.e);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b();
      int i = j;
      if (!this.a.equals("")) {
        i = j + zzsn.b(1, this.a);
      }
      j = i;
      if (this.b != 0L) {
        j = i + zzsn.d(2, this.b);
      }
      i = j;
      if (this.c != 2147483647L) {
        i = j + zzsn.d(3, this.c);
      }
      j = i;
      if (this.d) {
        j = i + zzsn.b(4, this.d);
      }
      i = j;
      if (this.e != 0L) {
        i = j + zzsn.d(5, this.e);
      }
      return i;
    }
    
    public zzc c()
    {
      this.a = "";
      this.b = 0L;
      this.c = 2147483647L;
      this.d = false;
      this.e = 0L;
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      do
      {
        do
        {
          return bool1;
          bool1 = bool2;
        } while (!(paramObject instanceof zzc));
        paramObject = (zzc)paramObject;
        if (this.a != null) {
          break;
        }
        bool1 = bool2;
      } while (((zzc)paramObject).a != null);
      while (this.a.equals(((zzc)paramObject).a))
      {
        bool1 = bool2;
        if (this.b != ((zzc)paramObject).b) {
          break;
        }
        bool1 = bool2;
        if (this.c != ((zzc)paramObject).c) {
          break;
        }
        bool1 = bool2;
        if (this.d != ((zzc)paramObject).d) {
          break;
        }
        bool1 = bool2;
        if (this.e != ((zzc)paramObject).e) {
          break;
        }
        if ((this.r != null) && (!this.r.b())) {
          break label150;
        }
        if (((zzc)paramObject).r != null)
        {
          bool1 = bool2;
          if (!((zzc)paramObject).r.b()) {
            break;
          }
        }
        return true;
      }
      return false;
      label150:
      return this.r.equals(((zzc)paramObject).r);
    }
    
    public int hashCode()
    {
      int m = 0;
      int n = getClass().getName().hashCode();
      int i;
      int i1;
      int i2;
      int j;
      label65:
      int i3;
      if (this.a == null)
      {
        i = 0;
        i1 = (int)(this.b ^ this.b >>> 32);
        i2 = (int)(this.c ^ this.c >>> 32);
        if (!this.d) {
          break label154;
        }
        j = 1231;
        i3 = (int)(this.e ^ this.e >>> 32);
        k = m;
        if (this.r != null) {
          if (!this.r.b()) {
            break label161;
          }
        }
      }
      label154:
      label161:
      for (int k = m;; k = this.r.hashCode())
      {
        return ((j + (((i + (n + 527) * 31) * 31 + i1) * 31 + i2) * 31) * 31 + i3) * 31 + k;
        i = this.a.hashCode();
        break;
        j = 1237;
        break label65;
      }
    }
  }
  
  public static final class zzd
    extends zzso<zzd>
  {
    public zzag.zza[] a;
    public zzag.zza[] b;
    public zzaf.zzc[] c;
    
    public zzd()
    {
      a();
    }
    
    public zzd a()
    {
      this.a = zzag.zza.a();
      this.b = zzag.zza.a();
      this.c = zzaf.zzc.a();
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public zzd a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        int j;
        Object localObject;
        switch (i)
        {
        default: 
          if (a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 10: 
          j = zzsx.b(paramzzsm, 10);
          if (this.a == null) {}
          for (i = 0;; i = this.a.length)
          {
            localObject = new zzag.zza[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.a, 0, localObject, 0, i);
              j = i;
            }
            while (j < localObject.length - 1)
            {
              localObject[j] = new zzag.zza();
              paramzzsm.a(localObject[j]);
              paramzzsm.a();
              j += 1;
            }
          }
          localObject[j] = new zzag.zza();
          paramzzsm.a(localObject[j]);
          this.a = ((zzag.zza[])localObject);
          break;
        case 18: 
          j = zzsx.b(paramzzsm, 18);
          if (this.b == null) {}
          for (i = 0;; i = this.b.length)
          {
            localObject = new zzag.zza[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.b, 0, localObject, 0, i);
              j = i;
            }
            while (j < localObject.length - 1)
            {
              localObject[j] = new zzag.zza();
              paramzzsm.a(localObject[j]);
              paramzzsm.a();
              j += 1;
            }
          }
          localObject[j] = new zzag.zza();
          paramzzsm.a(localObject[j]);
          this.b = ((zzag.zza[])localObject);
          break;
        case 26: 
          j = zzsx.b(paramzzsm, 26);
          if (this.c == null) {}
          for (i = 0;; i = this.c.length)
          {
            localObject = new zzaf.zzc[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.c, 0, localObject, 0, i);
              j = i;
            }
            while (j < localObject.length - 1)
            {
              localObject[j] = new zzaf.zzc();
              paramzzsm.a(localObject[j]);
              paramzzsm.a();
              j += 1;
            }
          }
          localObject[j] = new zzaf.zzc();
          paramzzsm.a(localObject[j]);
          this.c = ((zzaf.zzc[])localObject);
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      int j = 0;
      int i;
      Object localObject;
      if ((this.a != null) && (this.a.length > 0))
      {
        i = 0;
        while (i < this.a.length)
        {
          localObject = this.a[i];
          if (localObject != null) {
            paramzzsn.a(1, (zzsu)localObject);
          }
          i += 1;
        }
      }
      if ((this.b != null) && (this.b.length > 0))
      {
        i = 0;
        while (i < this.b.length)
        {
          localObject = this.b[i];
          if (localObject != null) {
            paramzzsn.a(2, (zzsu)localObject);
          }
          i += 1;
        }
      }
      if ((this.c != null) && (this.c.length > 0))
      {
        i = j;
        while (i < this.c.length)
        {
          localObject = this.c[i];
          if (localObject != null) {
            paramzzsn.a(3, (zzsu)localObject);
          }
          i += 1;
        }
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int m = 0;
      int i = super.b();
      int j = i;
      Object localObject;
      if (this.a != null)
      {
        j = i;
        if (this.a.length > 0)
        {
          j = 0;
          while (j < this.a.length)
          {
            localObject = this.a[j];
            k = i;
            if (localObject != null) {
              k = i + zzsn.c(1, (zzsu)localObject);
            }
            j += 1;
            i = k;
          }
          j = i;
        }
      }
      i = j;
      if (this.b != null)
      {
        i = j;
        if (this.b.length > 0)
        {
          i = j;
          j = 0;
          while (j < this.b.length)
          {
            localObject = this.b[j];
            k = i;
            if (localObject != null) {
              k = i + zzsn.c(2, (zzsu)localObject);
            }
            j += 1;
            i = k;
          }
        }
      }
      int k = i;
      if (this.c != null)
      {
        k = i;
        if (this.c.length > 0)
        {
          j = m;
          for (;;)
          {
            k = i;
            if (j >= this.c.length) {
              break;
            }
            localObject = this.c[j];
            k = i;
            if (localObject != null) {
              k = i + zzsn.c(3, (zzsu)localObject);
            }
            j += 1;
            i = k;
          }
        }
      }
      return k;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                return bool1;
                bool1 = bool2;
              } while (!(paramObject instanceof zzd));
              paramObject = (zzd)paramObject;
              bool1 = bool2;
            } while (!zzss.a(this.a, ((zzd)paramObject).a));
            bool1 = bool2;
          } while (!zzss.a(this.b, ((zzd)paramObject).b));
          bool1 = bool2;
        } while (!zzss.a(this.c, ((zzd)paramObject).c));
        if ((this.r != null) && (!this.r.b())) {
          break label111;
        }
        if (((zzd)paramObject).r == null) {
          break;
        }
        bool1 = bool2;
      } while (!((zzd)paramObject).r.b());
      return true;
      label111:
      return this.r.equals(((zzd)paramObject).r);
    }
    
    public int hashCode()
    {
      int j = getClass().getName().hashCode();
      int k = zzss.a(this.a);
      int m = zzss.a(this.b);
      int n = zzss.a(this.c);
      if ((this.r == null) || (this.r.b())) {}
      for (int i = 0;; i = this.r.hashCode()) {
        return i + ((((j + 527) * 31 + k) * 31 + m) * 31 + n) * 31;
      }
    }
  }
  
  public static final class zze
    extends zzso<zze>
  {
    private static volatile zze[] c;
    public int a;
    public int b;
    
    public zze()
    {
      c();
    }
    
    public static zze[] a()
    {
      if (c == null) {}
      synchronized (zzss.a)
      {
        if (c == null) {
          c = new zze[0];
        }
        return c;
      }
    }
    
    public zze a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 8: 
          this.a = paramzzsm.g();
          break;
        case 16: 
          this.b = paramzzsm.g();
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      paramzzsn.a(1, this.a);
      paramzzsn.a(2, this.b);
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      return super.b() + zzsn.c(1, this.a) + zzsn.c(2, this.b);
    }
    
    public zze c()
    {
      this.a = 0;
      this.b = 0;
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      do
      {
        do
        {
          do
          {
            do
            {
              return bool1;
              bool1 = bool2;
            } while (!(paramObject instanceof zze));
            paramObject = (zze)paramObject;
            bool1 = bool2;
          } while (this.a != ((zze)paramObject).a);
          bool1 = bool2;
        } while (this.b != ((zze)paramObject).b);
        if ((this.r != null) && (!this.r.b())) {
          break label89;
        }
        if (((zze)paramObject).r == null) {
          break;
        }
        bool1 = bool2;
      } while (!((zze)paramObject).r.b());
      return true;
      label89:
      return this.r.equals(((zze)paramObject).r);
    }
    
    public int hashCode()
    {
      int j = getClass().getName().hashCode();
      int k = this.a;
      int m = this.b;
      if ((this.r == null) || (this.r.b())) {}
      for (int i = 0;; i = this.r.hashCode()) {
        return i + (((j + 527) * 31 + k) * 31 + m) * 31;
      }
    }
  }
  
  public static final class zzf
    extends zzso<zzf>
  {
    public String[] a;
    public String[] b;
    public zzag.zza[] c;
    public zzaf.zze[] d;
    public zzaf.zzb[] e;
    public zzaf.zzb[] f;
    public zzaf.zzb[] g;
    public zzaf.zzg[] h;
    public String i;
    public String j;
    public String k;
    public String l;
    public zzaf.zza m;
    public float n;
    public boolean o;
    public String[] p;
    public int q;
    
    public zzf()
    {
      a();
    }
    
    public static zzf a(byte[] paramArrayOfByte)
      throws zzst
    {
      return (zzf)zzsu.a(new zzf(), paramArrayOfByte);
    }
    
    public zzf a()
    {
      this.a = zzsx.f;
      this.b = zzsx.f;
      this.c = zzag.zza.a();
      this.d = zzaf.zze.a();
      this.e = zzaf.zzb.a();
      this.f = zzaf.zzb.a();
      this.g = zzaf.zzb.a();
      this.h = zzaf.zzg.a();
      this.i = "";
      this.j = "";
      this.k = "0";
      this.l = "";
      this.m = null;
      this.n = 0.0F;
      this.o = false;
      this.p = zzsx.f;
      this.q = 0;
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public zzf a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i1 = paramzzsm.a();
        int i2;
        Object localObject;
        switch (i1)
        {
        default: 
          if (a(paramzzsm, i1)) {}
          break;
        case 0: 
          return this;
        case 10: 
          i2 = zzsx.b(paramzzsm, 10);
          if (this.b == null) {}
          for (i1 = 0;; i1 = this.b.length)
          {
            localObject = new String[i2 + i1];
            i2 = i1;
            if (i1 != 0)
            {
              System.arraycopy(this.b, 0, localObject, 0, i1);
              i2 = i1;
            }
            while (i2 < localObject.length - 1)
            {
              localObject[i2] = paramzzsm.i();
              paramzzsm.a();
              i2 += 1;
            }
          }
          localObject[i2] = paramzzsm.i();
          this.b = ((String[])localObject);
          break;
        case 18: 
          i2 = zzsx.b(paramzzsm, 18);
          if (this.c == null) {}
          for (i1 = 0;; i1 = this.c.length)
          {
            localObject = new zzag.zza[i2 + i1];
            i2 = i1;
            if (i1 != 0)
            {
              System.arraycopy(this.c, 0, localObject, 0, i1);
              i2 = i1;
            }
            while (i2 < localObject.length - 1)
            {
              localObject[i2] = new zzag.zza();
              paramzzsm.a(localObject[i2]);
              paramzzsm.a();
              i2 += 1;
            }
          }
          localObject[i2] = new zzag.zza();
          paramzzsm.a(localObject[i2]);
          this.c = ((zzag.zza[])localObject);
          break;
        case 26: 
          i2 = zzsx.b(paramzzsm, 26);
          if (this.d == null) {}
          for (i1 = 0;; i1 = this.d.length)
          {
            localObject = new zzaf.zze[i2 + i1];
            i2 = i1;
            if (i1 != 0)
            {
              System.arraycopy(this.d, 0, localObject, 0, i1);
              i2 = i1;
            }
            while (i2 < localObject.length - 1)
            {
              localObject[i2] = new zzaf.zze();
              paramzzsm.a(localObject[i2]);
              paramzzsm.a();
              i2 += 1;
            }
          }
          localObject[i2] = new zzaf.zze();
          paramzzsm.a(localObject[i2]);
          this.d = ((zzaf.zze[])localObject);
          break;
        case 34: 
          i2 = zzsx.b(paramzzsm, 34);
          if (this.e == null) {}
          for (i1 = 0;; i1 = this.e.length)
          {
            localObject = new zzaf.zzb[i2 + i1];
            i2 = i1;
            if (i1 != 0)
            {
              System.arraycopy(this.e, 0, localObject, 0, i1);
              i2 = i1;
            }
            while (i2 < localObject.length - 1)
            {
              localObject[i2] = new zzaf.zzb();
              paramzzsm.a(localObject[i2]);
              paramzzsm.a();
              i2 += 1;
            }
          }
          localObject[i2] = new zzaf.zzb();
          paramzzsm.a(localObject[i2]);
          this.e = ((zzaf.zzb[])localObject);
          break;
        case 42: 
          i2 = zzsx.b(paramzzsm, 42);
          if (this.f == null) {}
          for (i1 = 0;; i1 = this.f.length)
          {
            localObject = new zzaf.zzb[i2 + i1];
            i2 = i1;
            if (i1 != 0)
            {
              System.arraycopy(this.f, 0, localObject, 0, i1);
              i2 = i1;
            }
            while (i2 < localObject.length - 1)
            {
              localObject[i2] = new zzaf.zzb();
              paramzzsm.a(localObject[i2]);
              paramzzsm.a();
              i2 += 1;
            }
          }
          localObject[i2] = new zzaf.zzb();
          paramzzsm.a(localObject[i2]);
          this.f = ((zzaf.zzb[])localObject);
          break;
        case 50: 
          i2 = zzsx.b(paramzzsm, 50);
          if (this.g == null) {}
          for (i1 = 0;; i1 = this.g.length)
          {
            localObject = new zzaf.zzb[i2 + i1];
            i2 = i1;
            if (i1 != 0)
            {
              System.arraycopy(this.g, 0, localObject, 0, i1);
              i2 = i1;
            }
            while (i2 < localObject.length - 1)
            {
              localObject[i2] = new zzaf.zzb();
              paramzzsm.a(localObject[i2]);
              paramzzsm.a();
              i2 += 1;
            }
          }
          localObject[i2] = new zzaf.zzb();
          paramzzsm.a(localObject[i2]);
          this.g = ((zzaf.zzb[])localObject);
          break;
        case 58: 
          i2 = zzsx.b(paramzzsm, 58);
          if (this.h == null) {}
          for (i1 = 0;; i1 = this.h.length)
          {
            localObject = new zzaf.zzg[i2 + i1];
            i2 = i1;
            if (i1 != 0)
            {
              System.arraycopy(this.h, 0, localObject, 0, i1);
              i2 = i1;
            }
            while (i2 < localObject.length - 1)
            {
              localObject[i2] = new zzaf.zzg();
              paramzzsm.a(localObject[i2]);
              paramzzsm.a();
              i2 += 1;
            }
          }
          localObject[i2] = new zzaf.zzg();
          paramzzsm.a(localObject[i2]);
          this.h = ((zzaf.zzg[])localObject);
          break;
        case 74: 
          this.i = paramzzsm.i();
          break;
        case 82: 
          this.j = paramzzsm.i();
          break;
        case 98: 
          this.k = paramzzsm.i();
          break;
        case 106: 
          this.l = paramzzsm.i();
          break;
        case 114: 
          if (this.m == null) {
            this.m = new zzaf.zza();
          }
          paramzzsm.a(this.m);
          break;
        case 125: 
          this.n = paramzzsm.d();
          break;
        case 130: 
          i2 = zzsx.b(paramzzsm, 130);
          if (this.p == null) {}
          for (i1 = 0;; i1 = this.p.length)
          {
            localObject = new String[i2 + i1];
            i2 = i1;
            if (i1 != 0)
            {
              System.arraycopy(this.p, 0, localObject, 0, i1);
              i2 = i1;
            }
            while (i2 < localObject.length - 1)
            {
              localObject[i2] = paramzzsm.i();
              paramzzsm.a();
              i2 += 1;
            }
          }
          localObject[i2] = paramzzsm.i();
          this.p = ((String[])localObject);
          break;
        case 136: 
          this.q = paramzzsm.g();
          break;
        case 144: 
          this.o = paramzzsm.h();
          break;
        case 154: 
          i2 = zzsx.b(paramzzsm, 154);
          if (this.a == null) {}
          for (i1 = 0;; i1 = this.a.length)
          {
            localObject = new String[i2 + i1];
            i2 = i1;
            if (i1 != 0)
            {
              System.arraycopy(this.a, 0, localObject, 0, i1);
              i2 = i1;
            }
            while (i2 < localObject.length - 1)
            {
              localObject[i2] = paramzzsm.i();
              paramzzsm.a();
              i2 += 1;
            }
          }
          localObject[i2] = paramzzsm.i();
          this.a = ((String[])localObject);
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      int i2 = 0;
      int i1;
      Object localObject;
      if ((this.b != null) && (this.b.length > 0))
      {
        i1 = 0;
        while (i1 < this.b.length)
        {
          localObject = this.b[i1];
          if (localObject != null) {
            paramzzsn.a(1, (String)localObject);
          }
          i1 += 1;
        }
      }
      if ((this.c != null) && (this.c.length > 0))
      {
        i1 = 0;
        while (i1 < this.c.length)
        {
          localObject = this.c[i1];
          if (localObject != null) {
            paramzzsn.a(2, (zzsu)localObject);
          }
          i1 += 1;
        }
      }
      if ((this.d != null) && (this.d.length > 0))
      {
        i1 = 0;
        while (i1 < this.d.length)
        {
          localObject = this.d[i1];
          if (localObject != null) {
            paramzzsn.a(3, (zzsu)localObject);
          }
          i1 += 1;
        }
      }
      if ((this.e != null) && (this.e.length > 0))
      {
        i1 = 0;
        while (i1 < this.e.length)
        {
          localObject = this.e[i1];
          if (localObject != null) {
            paramzzsn.a(4, (zzsu)localObject);
          }
          i1 += 1;
        }
      }
      if ((this.f != null) && (this.f.length > 0))
      {
        i1 = 0;
        while (i1 < this.f.length)
        {
          localObject = this.f[i1];
          if (localObject != null) {
            paramzzsn.a(5, (zzsu)localObject);
          }
          i1 += 1;
        }
      }
      if ((this.g != null) && (this.g.length > 0))
      {
        i1 = 0;
        while (i1 < this.g.length)
        {
          localObject = this.g[i1];
          if (localObject != null) {
            paramzzsn.a(6, (zzsu)localObject);
          }
          i1 += 1;
        }
      }
      if ((this.h != null) && (this.h.length > 0))
      {
        i1 = 0;
        while (i1 < this.h.length)
        {
          localObject = this.h[i1];
          if (localObject != null) {
            paramzzsn.a(7, (zzsu)localObject);
          }
          i1 += 1;
        }
      }
      if (!this.i.equals("")) {
        paramzzsn.a(9, this.i);
      }
      if (!this.j.equals("")) {
        paramzzsn.a(10, this.j);
      }
      if (!this.k.equals("0")) {
        paramzzsn.a(12, this.k);
      }
      if (!this.l.equals("")) {
        paramzzsn.a(13, this.l);
      }
      if (this.m != null) {
        paramzzsn.a(14, this.m);
      }
      if (Float.floatToIntBits(this.n) != Float.floatToIntBits(0.0F)) {
        paramzzsn.a(15, this.n);
      }
      if ((this.p != null) && (this.p.length > 0))
      {
        i1 = 0;
        while (i1 < this.p.length)
        {
          localObject = this.p[i1];
          if (localObject != null) {
            paramzzsn.a(16, (String)localObject);
          }
          i1 += 1;
        }
      }
      if (this.q != 0) {
        paramzzsn.a(17, this.q);
      }
      if (this.o) {
        paramzzsn.a(18, this.o);
      }
      if ((this.a != null) && (this.a.length > 0))
      {
        i1 = i2;
        while (i1 < this.a.length)
        {
          localObject = this.a[i1];
          if (localObject != null) {
            paramzzsn.a(19, (String)localObject);
          }
          i1 += 1;
        }
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int i7 = 0;
      int i6 = super.b();
      int i1;
      int i3;
      Object localObject;
      int i5;
      int i4;
      if ((this.b != null) && (this.b.length > 0))
      {
        i1 = 0;
        i2 = 0;
        for (i3 = 0; i1 < this.b.length; i3 = i4)
        {
          localObject = this.b[i1];
          i5 = i2;
          i4 = i3;
          if (localObject != null)
          {
            i4 = i3 + 1;
            i5 = i2 + zzsn.b((String)localObject);
          }
          i1 += 1;
          i2 = i5;
        }
      }
      for (int i2 = i6 + i2 + i3 * 1;; i2 = i6)
      {
        i1 = i2;
        if (this.c != null)
        {
          i1 = i2;
          if (this.c.length > 0)
          {
            i1 = i2;
            i2 = 0;
            while (i2 < this.c.length)
            {
              localObject = this.c[i2];
              i3 = i1;
              if (localObject != null) {
                i3 = i1 + zzsn.c(2, (zzsu)localObject);
              }
              i2 += 1;
              i1 = i3;
            }
          }
        }
        i2 = i1;
        if (this.d != null)
        {
          i2 = i1;
          if (this.d.length > 0)
          {
            i2 = 0;
            while (i2 < this.d.length)
            {
              localObject = this.d[i2];
              i3 = i1;
              if (localObject != null) {
                i3 = i1 + zzsn.c(3, (zzsu)localObject);
              }
              i2 += 1;
              i1 = i3;
            }
            i2 = i1;
          }
        }
        i1 = i2;
        if (this.e != null)
        {
          i1 = i2;
          if (this.e.length > 0)
          {
            i1 = i2;
            i2 = 0;
            while (i2 < this.e.length)
            {
              localObject = this.e[i2];
              i3 = i1;
              if (localObject != null) {
                i3 = i1 + zzsn.c(4, (zzsu)localObject);
              }
              i2 += 1;
              i1 = i3;
            }
          }
        }
        i2 = i1;
        if (this.f != null)
        {
          i2 = i1;
          if (this.f.length > 0)
          {
            i2 = 0;
            while (i2 < this.f.length)
            {
              localObject = this.f[i2];
              i3 = i1;
              if (localObject != null) {
                i3 = i1 + zzsn.c(5, (zzsu)localObject);
              }
              i2 += 1;
              i1 = i3;
            }
            i2 = i1;
          }
        }
        i1 = i2;
        if (this.g != null)
        {
          i1 = i2;
          if (this.g.length > 0)
          {
            i1 = i2;
            i2 = 0;
            while (i2 < this.g.length)
            {
              localObject = this.g[i2];
              i3 = i1;
              if (localObject != null) {
                i3 = i1 + zzsn.c(6, (zzsu)localObject);
              }
              i2 += 1;
              i1 = i3;
            }
          }
        }
        i2 = i1;
        if (this.h != null)
        {
          i2 = i1;
          if (this.h.length > 0)
          {
            i2 = 0;
            while (i2 < this.h.length)
            {
              localObject = this.h[i2];
              i3 = i1;
              if (localObject != null) {
                i3 = i1 + zzsn.c(7, (zzsu)localObject);
              }
              i2 += 1;
              i1 = i3;
            }
            i2 = i1;
          }
        }
        i1 = i2;
        if (!this.i.equals("")) {
          i1 = i2 + zzsn.b(9, this.i);
        }
        i2 = i1;
        if (!this.j.equals("")) {
          i2 = i1 + zzsn.b(10, this.j);
        }
        i1 = i2;
        if (!this.k.equals("0")) {
          i1 = i2 + zzsn.b(12, this.k);
        }
        i2 = i1;
        if (!this.l.equals("")) {
          i2 = i1 + zzsn.b(13, this.l);
        }
        i3 = i2;
        if (this.m != null) {
          i3 = i2 + zzsn.c(14, this.m);
        }
        i1 = i3;
        if (Float.floatToIntBits(this.n) != Float.floatToIntBits(0.0F)) {
          i1 = i3 + zzsn.b(15, this.n);
        }
        i2 = i1;
        if (this.p != null)
        {
          i2 = i1;
          if (this.p.length > 0)
          {
            i2 = 0;
            i3 = 0;
            for (i4 = 0; i2 < this.p.length; i4 = i5)
            {
              localObject = this.p[i2];
              i6 = i3;
              i5 = i4;
              if (localObject != null)
              {
                i5 = i4 + 1;
                i6 = i3 + zzsn.b((String)localObject);
              }
              i2 += 1;
              i3 = i6;
            }
            i2 = i1 + i3 + i4 * 2;
          }
        }
        i3 = i2;
        if (this.q != 0) {
          i3 = i2 + zzsn.c(17, this.q);
        }
        i1 = i3;
        if (this.o) {
          i1 = i3 + zzsn.b(18, this.o);
        }
        i2 = i1;
        if (this.a != null)
        {
          i2 = i1;
          if (this.a.length > 0)
          {
            i3 = 0;
            i4 = 0;
            i2 = i7;
            while (i2 < this.a.length)
            {
              localObject = this.a[i2];
              i6 = i3;
              i5 = i4;
              if (localObject != null)
              {
                i5 = i4 + 1;
                i6 = i3 + zzsn.b((String)localObject);
              }
              i2 += 1;
              i3 = i6;
              i4 = i5;
            }
            i2 = i1 + i3 + i4 * 2;
          }
        }
        return i2;
      }
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      label169:
      label185:
      label201:
      label217:
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  do
                  {
                    do
                    {
                      do
                      {
                        do
                        {
                          do
                          {
                            do
                            {
                              do
                              {
                                do
                                {
                                  return bool1;
                                  bool1 = bool2;
                                } while (!(paramObject instanceof zzf));
                                paramObject = (zzf)paramObject;
                                bool1 = bool2;
                              } while (!zzss.a(this.a, ((zzf)paramObject).a));
                              bool1 = bool2;
                            } while (!zzss.a(this.b, ((zzf)paramObject).b));
                            bool1 = bool2;
                          } while (!zzss.a(this.c, ((zzf)paramObject).c));
                          bool1 = bool2;
                        } while (!zzss.a(this.d, ((zzf)paramObject).d));
                        bool1 = bool2;
                      } while (!zzss.a(this.e, ((zzf)paramObject).e));
                      bool1 = bool2;
                    } while (!zzss.a(this.f, ((zzf)paramObject).f));
                    bool1 = bool2;
                  } while (!zzss.a(this.g, ((zzf)paramObject).g));
                  bool1 = bool2;
                } while (!zzss.a(this.h, ((zzf)paramObject).h));
                if (this.i != null) {
                  break;
                }
                bool1 = bool2;
              } while (((zzf)paramObject).i != null);
              if (this.j != null) {
                break label348;
              }
              bool1 = bool2;
            } while (((zzf)paramObject).j != null);
            if (this.k != null) {
              break label364;
            }
            bool1 = bool2;
          } while (((zzf)paramObject).k != null);
          if (this.l != null) {
            break label380;
          }
          bool1 = bool2;
        } while (((zzf)paramObject).l != null);
        if (this.m != null) {
          break label396;
        }
        bool1 = bool2;
      } while (((zzf)paramObject).m != null);
      label348:
      label364:
      label380:
      label396:
      while (this.m.equals(((zzf)paramObject).m))
      {
        bool1 = bool2;
        if (Float.floatToIntBits(this.n) != Float.floatToIntBits(((zzf)paramObject).n)) {
          break;
        }
        bool1 = bool2;
        if (this.o != ((zzf)paramObject).o) {
          break;
        }
        bool1 = bool2;
        if (!zzss.a(this.p, ((zzf)paramObject).p)) {
          break;
        }
        bool1 = bool2;
        if (this.q != ((zzf)paramObject).q) {
          break;
        }
        if ((this.r != null) && (!this.r.b())) {
          break label412;
        }
        if (((zzf)paramObject).r != null)
        {
          bool1 = bool2;
          if (!((zzf)paramObject).r.b()) {
            break;
          }
        }
        return true;
        if (this.i.equals(((zzf)paramObject).i)) {
          break label169;
        }
        return false;
        if (this.j.equals(((zzf)paramObject).j)) {
          break label185;
        }
        return false;
        if (this.k.equals(((zzf)paramObject).k)) {
          break label201;
        }
        return false;
        if (this.l.equals(((zzf)paramObject).l)) {
          break label217;
        }
        return false;
      }
      return false;
      label412:
      return this.r.equals(((zzf)paramObject).r);
    }
    
    public int hashCode()
    {
      int i8 = 0;
      int i9 = getClass().getName().hashCode();
      int i10 = zzss.a(this.a);
      int i11 = zzss.a(this.b);
      int i12 = zzss.a(this.c);
      int i13 = zzss.a(this.d);
      int i14 = zzss.a(this.e);
      int i15 = zzss.a(this.f);
      int i16 = zzss.a(this.g);
      int i17 = zzss.a(this.h);
      int i1;
      int i2;
      label105:
      int i3;
      label114:
      int i4;
      label124:
      int i5;
      label134:
      int i18;
      int i6;
      label155:
      int i19;
      int i20;
      if (this.i == null)
      {
        i1 = 0;
        if (this.j != null) {
          break label318;
        }
        i2 = 0;
        if (this.k != null) {
          break label329;
        }
        i3 = 0;
        if (this.l != null) {
          break label340;
        }
        i4 = 0;
        if (this.m != null) {
          break label352;
        }
        i5 = 0;
        i18 = Float.floatToIntBits(this.n);
        if (!this.o) {
          break label364;
        }
        i6 = 1231;
        i19 = zzss.a(this.p);
        i20 = this.q;
        i7 = i8;
        if (this.r != null) {
          if (!this.r.b()) {
            break label372;
          }
        }
      }
      label318:
      label329:
      label340:
      label352:
      label364:
      label372:
      for (int i7 = i8;; i7 = this.r.hashCode())
      {
        return (((i6 + ((i5 + (i4 + (i3 + (i2 + (i1 + (((((((((i9 + 527) * 31 + i10) * 31 + i11) * 31 + i12) * 31 + i13) * 31 + i14) * 31 + i15) * 31 + i16) * 31 + i17) * 31) * 31) * 31) * 31) * 31) * 31 + i18) * 31) * 31 + i19) * 31 + i20) * 31 + i7;
        i1 = this.i.hashCode();
        break;
        i2 = this.j.hashCode();
        break label105;
        i3 = this.k.hashCode();
        break label114;
        i4 = this.l.hashCode();
        break label124;
        i5 = this.m.hashCode();
        break label134;
        i6 = 1237;
        break label155;
      }
    }
  }
  
  public static final class zzg
    extends zzso<zzg>
  {
    private static volatile zzg[] k;
    public int[] a;
    public int[] b;
    public int[] c;
    public int[] d;
    public int[] e;
    public int[] f;
    public int[] g;
    public int[] h;
    public int[] i;
    public int[] j;
    
    public zzg()
    {
      c();
    }
    
    public static zzg[] a()
    {
      if (k == null) {}
      synchronized (zzss.a)
      {
        if (k == null) {
          k = new zzg[0];
        }
        return k;
      }
    }
    
    public zzg a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int m = paramzzsm.a();
        int n;
        int[] arrayOfInt;
        int i1;
        switch (m)
        {
        default: 
          if (a(paramzzsm, m)) {}
          break;
        case 0: 
          return this;
        case 8: 
          n = zzsx.b(paramzzsm, 8);
          if (this.a == null) {}
          for (m = 0;; m = this.a.length)
          {
            arrayOfInt = new int[n + m];
            n = m;
            if (m != 0)
            {
              System.arraycopy(this.a, 0, arrayOfInt, 0, m);
              n = m;
            }
            while (n < arrayOfInt.length - 1)
            {
              arrayOfInt[n] = paramzzsm.g();
              paramzzsm.a();
              n += 1;
            }
          }
          arrayOfInt[n] = paramzzsm.g();
          this.a = arrayOfInt;
          break;
        case 10: 
          i1 = paramzzsm.d(paramzzsm.m());
          m = paramzzsm.s();
          n = 0;
          while (paramzzsm.q() > 0)
          {
            paramzzsm.g();
            n += 1;
          }
          paramzzsm.f(m);
          if (this.a == null) {}
          for (m = 0;; m = this.a.length)
          {
            arrayOfInt = new int[n + m];
            n = m;
            if (m != 0)
            {
              System.arraycopy(this.a, 0, arrayOfInt, 0, m);
              n = m;
            }
            while (n < arrayOfInt.length)
            {
              arrayOfInt[n] = paramzzsm.g();
              n += 1;
            }
          }
          this.a = arrayOfInt;
          paramzzsm.e(i1);
          break;
        case 16: 
          n = zzsx.b(paramzzsm, 16);
          if (this.b == null) {}
          for (m = 0;; m = this.b.length)
          {
            arrayOfInt = new int[n + m];
            n = m;
            if (m != 0)
            {
              System.arraycopy(this.b, 0, arrayOfInt, 0, m);
              n = m;
            }
            while (n < arrayOfInt.length - 1)
            {
              arrayOfInt[n] = paramzzsm.g();
              paramzzsm.a();
              n += 1;
            }
          }
          arrayOfInt[n] = paramzzsm.g();
          this.b = arrayOfInt;
          break;
        case 18: 
          i1 = paramzzsm.d(paramzzsm.m());
          m = paramzzsm.s();
          n = 0;
          while (paramzzsm.q() > 0)
          {
            paramzzsm.g();
            n += 1;
          }
          paramzzsm.f(m);
          if (this.b == null) {}
          for (m = 0;; m = this.b.length)
          {
            arrayOfInt = new int[n + m];
            n = m;
            if (m != 0)
            {
              System.arraycopy(this.b, 0, arrayOfInt, 0, m);
              n = m;
            }
            while (n < arrayOfInt.length)
            {
              arrayOfInt[n] = paramzzsm.g();
              n += 1;
            }
          }
          this.b = arrayOfInt;
          paramzzsm.e(i1);
          break;
        case 24: 
          n = zzsx.b(paramzzsm, 24);
          if (this.c == null) {}
          for (m = 0;; m = this.c.length)
          {
            arrayOfInt = new int[n + m];
            n = m;
            if (m != 0)
            {
              System.arraycopy(this.c, 0, arrayOfInt, 0, m);
              n = m;
            }
            while (n < arrayOfInt.length - 1)
            {
              arrayOfInt[n] = paramzzsm.g();
              paramzzsm.a();
              n += 1;
            }
          }
          arrayOfInt[n] = paramzzsm.g();
          this.c = arrayOfInt;
          break;
        case 26: 
          i1 = paramzzsm.d(paramzzsm.m());
          m = paramzzsm.s();
          n = 0;
          while (paramzzsm.q() > 0)
          {
            paramzzsm.g();
            n += 1;
          }
          paramzzsm.f(m);
          if (this.c == null) {}
          for (m = 0;; m = this.c.length)
          {
            arrayOfInt = new int[n + m];
            n = m;
            if (m != 0)
            {
              System.arraycopy(this.c, 0, arrayOfInt, 0, m);
              n = m;
            }
            while (n < arrayOfInt.length)
            {
              arrayOfInt[n] = paramzzsm.g();
              n += 1;
            }
          }
          this.c = arrayOfInt;
          paramzzsm.e(i1);
          break;
        case 32: 
          n = zzsx.b(paramzzsm, 32);
          if (this.d == null) {}
          for (m = 0;; m = this.d.length)
          {
            arrayOfInt = new int[n + m];
            n = m;
            if (m != 0)
            {
              System.arraycopy(this.d, 0, arrayOfInt, 0, m);
              n = m;
            }
            while (n < arrayOfInt.length - 1)
            {
              arrayOfInt[n] = paramzzsm.g();
              paramzzsm.a();
              n += 1;
            }
          }
          arrayOfInt[n] = paramzzsm.g();
          this.d = arrayOfInt;
          break;
        case 34: 
          i1 = paramzzsm.d(paramzzsm.m());
          m = paramzzsm.s();
          n = 0;
          while (paramzzsm.q() > 0)
          {
            paramzzsm.g();
            n += 1;
          }
          paramzzsm.f(m);
          if (this.d == null) {}
          for (m = 0;; m = this.d.length)
          {
            arrayOfInt = new int[n + m];
            n = m;
            if (m != 0)
            {
              System.arraycopy(this.d, 0, arrayOfInt, 0, m);
              n = m;
            }
            while (n < arrayOfInt.length)
            {
              arrayOfInt[n] = paramzzsm.g();
              n += 1;
            }
          }
          this.d = arrayOfInt;
          paramzzsm.e(i1);
          break;
        case 40: 
          n = zzsx.b(paramzzsm, 40);
          if (this.e == null) {}
          for (m = 0;; m = this.e.length)
          {
            arrayOfInt = new int[n + m];
            n = m;
            if (m != 0)
            {
              System.arraycopy(this.e, 0, arrayOfInt, 0, m);
              n = m;
            }
            while (n < arrayOfInt.length - 1)
            {
              arrayOfInt[n] = paramzzsm.g();
              paramzzsm.a();
              n += 1;
            }
          }
          arrayOfInt[n] = paramzzsm.g();
          this.e = arrayOfInt;
          break;
        case 42: 
          i1 = paramzzsm.d(paramzzsm.m());
          m = paramzzsm.s();
          n = 0;
          while (paramzzsm.q() > 0)
          {
            paramzzsm.g();
            n += 1;
          }
          paramzzsm.f(m);
          if (this.e == null) {}
          for (m = 0;; m = this.e.length)
          {
            arrayOfInt = new int[n + m];
            n = m;
            if (m != 0)
            {
              System.arraycopy(this.e, 0, arrayOfInt, 0, m);
              n = m;
            }
            while (n < arrayOfInt.length)
            {
              arrayOfInt[n] = paramzzsm.g();
              n += 1;
            }
          }
          this.e = arrayOfInt;
          paramzzsm.e(i1);
          break;
        case 48: 
          n = zzsx.b(paramzzsm, 48);
          if (this.f == null) {}
          for (m = 0;; m = this.f.length)
          {
            arrayOfInt = new int[n + m];
            n = m;
            if (m != 0)
            {
              System.arraycopy(this.f, 0, arrayOfInt, 0, m);
              n = m;
            }
            while (n < arrayOfInt.length - 1)
            {
              arrayOfInt[n] = paramzzsm.g();
              paramzzsm.a();
              n += 1;
            }
          }
          arrayOfInt[n] = paramzzsm.g();
          this.f = arrayOfInt;
          break;
        case 50: 
          i1 = paramzzsm.d(paramzzsm.m());
          m = paramzzsm.s();
          n = 0;
          while (paramzzsm.q() > 0)
          {
            paramzzsm.g();
            n += 1;
          }
          paramzzsm.f(m);
          if (this.f == null) {}
          for (m = 0;; m = this.f.length)
          {
            arrayOfInt = new int[n + m];
            n = m;
            if (m != 0)
            {
              System.arraycopy(this.f, 0, arrayOfInt, 0, m);
              n = m;
            }
            while (n < arrayOfInt.length)
            {
              arrayOfInt[n] = paramzzsm.g();
              n += 1;
            }
          }
          this.f = arrayOfInt;
          paramzzsm.e(i1);
          break;
        case 56: 
          n = zzsx.b(paramzzsm, 56);
          if (this.g == null) {}
          for (m = 0;; m = this.g.length)
          {
            arrayOfInt = new int[n + m];
            n = m;
            if (m != 0)
            {
              System.arraycopy(this.g, 0, arrayOfInt, 0, m);
              n = m;
            }
            while (n < arrayOfInt.length - 1)
            {
              arrayOfInt[n] = paramzzsm.g();
              paramzzsm.a();
              n += 1;
            }
          }
          arrayOfInt[n] = paramzzsm.g();
          this.g = arrayOfInt;
          break;
        case 58: 
          i1 = paramzzsm.d(paramzzsm.m());
          m = paramzzsm.s();
          n = 0;
          while (paramzzsm.q() > 0)
          {
            paramzzsm.g();
            n += 1;
          }
          paramzzsm.f(m);
          if (this.g == null) {}
          for (m = 0;; m = this.g.length)
          {
            arrayOfInt = new int[n + m];
            n = m;
            if (m != 0)
            {
              System.arraycopy(this.g, 0, arrayOfInt, 0, m);
              n = m;
            }
            while (n < arrayOfInt.length)
            {
              arrayOfInt[n] = paramzzsm.g();
              n += 1;
            }
          }
          this.g = arrayOfInt;
          paramzzsm.e(i1);
          break;
        case 64: 
          n = zzsx.b(paramzzsm, 64);
          if (this.h == null) {}
          for (m = 0;; m = this.h.length)
          {
            arrayOfInt = new int[n + m];
            n = m;
            if (m != 0)
            {
              System.arraycopy(this.h, 0, arrayOfInt, 0, m);
              n = m;
            }
            while (n < arrayOfInt.length - 1)
            {
              arrayOfInt[n] = paramzzsm.g();
              paramzzsm.a();
              n += 1;
            }
          }
          arrayOfInt[n] = paramzzsm.g();
          this.h = arrayOfInt;
          break;
        case 66: 
          i1 = paramzzsm.d(paramzzsm.m());
          m = paramzzsm.s();
          n = 0;
          while (paramzzsm.q() > 0)
          {
            paramzzsm.g();
            n += 1;
          }
          paramzzsm.f(m);
          if (this.h == null) {}
          for (m = 0;; m = this.h.length)
          {
            arrayOfInt = new int[n + m];
            n = m;
            if (m != 0)
            {
              System.arraycopy(this.h, 0, arrayOfInt, 0, m);
              n = m;
            }
            while (n < arrayOfInt.length)
            {
              arrayOfInt[n] = paramzzsm.g();
              n += 1;
            }
          }
          this.h = arrayOfInt;
          paramzzsm.e(i1);
          break;
        case 72: 
          n = zzsx.b(paramzzsm, 72);
          if (this.i == null) {}
          for (m = 0;; m = this.i.length)
          {
            arrayOfInt = new int[n + m];
            n = m;
            if (m != 0)
            {
              System.arraycopy(this.i, 0, arrayOfInt, 0, m);
              n = m;
            }
            while (n < arrayOfInt.length - 1)
            {
              arrayOfInt[n] = paramzzsm.g();
              paramzzsm.a();
              n += 1;
            }
          }
          arrayOfInt[n] = paramzzsm.g();
          this.i = arrayOfInt;
          break;
        case 74: 
          i1 = paramzzsm.d(paramzzsm.m());
          m = paramzzsm.s();
          n = 0;
          while (paramzzsm.q() > 0)
          {
            paramzzsm.g();
            n += 1;
          }
          paramzzsm.f(m);
          if (this.i == null) {}
          for (m = 0;; m = this.i.length)
          {
            arrayOfInt = new int[n + m];
            n = m;
            if (m != 0)
            {
              System.arraycopy(this.i, 0, arrayOfInt, 0, m);
              n = m;
            }
            while (n < arrayOfInt.length)
            {
              arrayOfInt[n] = paramzzsm.g();
              n += 1;
            }
          }
          this.i = arrayOfInt;
          paramzzsm.e(i1);
          break;
        case 80: 
          n = zzsx.b(paramzzsm, 80);
          if (this.j == null) {}
          for (m = 0;; m = this.j.length)
          {
            arrayOfInt = new int[n + m];
            n = m;
            if (m != 0)
            {
              System.arraycopy(this.j, 0, arrayOfInt, 0, m);
              n = m;
            }
            while (n < arrayOfInt.length - 1)
            {
              arrayOfInt[n] = paramzzsm.g();
              paramzzsm.a();
              n += 1;
            }
          }
          arrayOfInt[n] = paramzzsm.g();
          this.j = arrayOfInt;
          break;
        case 82: 
          i1 = paramzzsm.d(paramzzsm.m());
          m = paramzzsm.s();
          n = 0;
          while (paramzzsm.q() > 0)
          {
            paramzzsm.g();
            n += 1;
          }
          paramzzsm.f(m);
          if (this.j == null) {}
          for (m = 0;; m = this.j.length)
          {
            arrayOfInt = new int[n + m];
            n = m;
            if (m != 0)
            {
              System.arraycopy(this.j, 0, arrayOfInt, 0, m);
              n = m;
            }
            while (n < arrayOfInt.length)
            {
              arrayOfInt[n] = paramzzsm.g();
              n += 1;
            }
          }
          this.j = arrayOfInt;
          paramzzsm.e(i1);
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      int n = 0;
      int m;
      if ((this.a != null) && (this.a.length > 0))
      {
        m = 0;
        while (m < this.a.length)
        {
          paramzzsn.a(1, this.a[m]);
          m += 1;
        }
      }
      if ((this.b != null) && (this.b.length > 0))
      {
        m = 0;
        while (m < this.b.length)
        {
          paramzzsn.a(2, this.b[m]);
          m += 1;
        }
      }
      if ((this.c != null) && (this.c.length > 0))
      {
        m = 0;
        while (m < this.c.length)
        {
          paramzzsn.a(3, this.c[m]);
          m += 1;
        }
      }
      if ((this.d != null) && (this.d.length > 0))
      {
        m = 0;
        while (m < this.d.length)
        {
          paramzzsn.a(4, this.d[m]);
          m += 1;
        }
      }
      if ((this.e != null) && (this.e.length > 0))
      {
        m = 0;
        while (m < this.e.length)
        {
          paramzzsn.a(5, this.e[m]);
          m += 1;
        }
      }
      if ((this.f != null) && (this.f.length > 0))
      {
        m = 0;
        while (m < this.f.length)
        {
          paramzzsn.a(6, this.f[m]);
          m += 1;
        }
      }
      if ((this.g != null) && (this.g.length > 0))
      {
        m = 0;
        while (m < this.g.length)
        {
          paramzzsn.a(7, this.g[m]);
          m += 1;
        }
      }
      if ((this.h != null) && (this.h.length > 0))
      {
        m = 0;
        while (m < this.h.length)
        {
          paramzzsn.a(8, this.h[m]);
          m += 1;
        }
      }
      if ((this.i != null) && (this.i.length > 0))
      {
        m = 0;
        while (m < this.i.length)
        {
          paramzzsn.a(9, this.i[m]);
          m += 1;
        }
      }
      if ((this.j != null) && (this.j.length > 0))
      {
        m = n;
        while (m < this.j.length)
        {
          paramzzsn.a(10, this.j[m]);
          m += 1;
        }
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int i2 = 0;
      int i1 = super.b();
      int m;
      if ((this.a != null) && (this.a.length > 0))
      {
        m = 0;
        n = 0;
        while (m < this.a.length)
        {
          n += zzsn.c(this.a[m]);
          m += 1;
        }
      }
      for (int n = i1 + n + this.a.length * 1;; n = i1)
      {
        m = n;
        if (this.b != null)
        {
          m = n;
          if (this.b.length > 0)
          {
            m = 0;
            i1 = 0;
            while (m < this.b.length)
            {
              i1 += zzsn.c(this.b[m]);
              m += 1;
            }
            m = n + i1 + this.b.length * 1;
          }
        }
        n = m;
        if (this.c != null)
        {
          n = m;
          if (this.c.length > 0)
          {
            n = 0;
            i1 = 0;
            while (n < this.c.length)
            {
              i1 += zzsn.c(this.c[n]);
              n += 1;
            }
            n = m + i1 + this.c.length * 1;
          }
        }
        m = n;
        if (this.d != null)
        {
          m = n;
          if (this.d.length > 0)
          {
            m = 0;
            i1 = 0;
            while (m < this.d.length)
            {
              i1 += zzsn.c(this.d[m]);
              m += 1;
            }
            m = n + i1 + this.d.length * 1;
          }
        }
        n = m;
        if (this.e != null)
        {
          n = m;
          if (this.e.length > 0)
          {
            n = 0;
            i1 = 0;
            while (n < this.e.length)
            {
              i1 += zzsn.c(this.e[n]);
              n += 1;
            }
            n = m + i1 + this.e.length * 1;
          }
        }
        m = n;
        if (this.f != null)
        {
          m = n;
          if (this.f.length > 0)
          {
            m = 0;
            i1 = 0;
            while (m < this.f.length)
            {
              i1 += zzsn.c(this.f[m]);
              m += 1;
            }
            m = n + i1 + this.f.length * 1;
          }
        }
        n = m;
        if (this.g != null)
        {
          n = m;
          if (this.g.length > 0)
          {
            n = 0;
            i1 = 0;
            while (n < this.g.length)
            {
              i1 += zzsn.c(this.g[n]);
              n += 1;
            }
            n = m + i1 + this.g.length * 1;
          }
        }
        m = n;
        if (this.h != null)
        {
          m = n;
          if (this.h.length > 0)
          {
            m = 0;
            i1 = 0;
            while (m < this.h.length)
            {
              i1 += zzsn.c(this.h[m]);
              m += 1;
            }
            m = n + i1 + this.h.length * 1;
          }
        }
        n = m;
        if (this.i != null)
        {
          n = m;
          if (this.i.length > 0)
          {
            n = 0;
            i1 = 0;
            while (n < this.i.length)
            {
              i1 += zzsn.c(this.i[n]);
              n += 1;
            }
            n = m + i1 + this.i.length * 1;
          }
        }
        m = n;
        if (this.j != null)
        {
          m = n;
          if (this.j.length > 0)
          {
            i1 = 0;
            m = i2;
            while (m < this.j.length)
            {
              i1 += zzsn.c(this.j[m]);
              m += 1;
            }
            m = n + i1 + this.j.length * 1;
          }
        }
        return m;
      }
    }
    
    public zzg c()
    {
      this.a = zzsx.a;
      this.b = zzsx.a;
      this.c = zzsx.a;
      this.d = zzsx.a;
      this.e = zzsx.a;
      this.f = zzsx.a;
      this.g = zzsx.a;
      this.h = zzsx.a;
      this.i = zzsx.a;
      this.j = zzsx.a;
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  do
                  {
                    do
                    {
                      do
                      {
                        do
                        {
                          do
                          {
                            do
                            {
                              return bool1;
                              bool1 = bool2;
                            } while (!(paramObject instanceof zzg));
                            paramObject = (zzg)paramObject;
                            bool1 = bool2;
                          } while (!zzss.a(this.a, ((zzg)paramObject).a));
                          bool1 = bool2;
                        } while (!zzss.a(this.b, ((zzg)paramObject).b));
                        bool1 = bool2;
                      } while (!zzss.a(this.c, ((zzg)paramObject).c));
                      bool1 = bool2;
                    } while (!zzss.a(this.d, ((zzg)paramObject).d));
                    bool1 = bool2;
                  } while (!zzss.a(this.e, ((zzg)paramObject).e));
                  bool1 = bool2;
                } while (!zzss.a(this.f, ((zzg)paramObject).f));
                bool1 = bool2;
              } while (!zzss.a(this.g, ((zzg)paramObject).g));
              bool1 = bool2;
            } while (!zzss.a(this.h, ((zzg)paramObject).h));
            bool1 = bool2;
          } while (!zzss.a(this.i, ((zzg)paramObject).i));
          bool1 = bool2;
        } while (!zzss.a(this.j, ((zzg)paramObject).j));
        if ((this.r != null) && (!this.r.b())) {
          break label223;
        }
        if (((zzg)paramObject).r == null) {
          break;
        }
        bool1 = bool2;
      } while (!((zzg)paramObject).r.b());
      return true;
      label223:
      return this.r.equals(((zzg)paramObject).r);
    }
    
    public int hashCode()
    {
      int n = getClass().getName().hashCode();
      int i1 = zzss.a(this.a);
      int i2 = zzss.a(this.b);
      int i3 = zzss.a(this.c);
      int i4 = zzss.a(this.d);
      int i5 = zzss.a(this.e);
      int i6 = zzss.a(this.f);
      int i7 = zzss.a(this.g);
      int i8 = zzss.a(this.h);
      int i9 = zzss.a(this.i);
      int i10 = zzss.a(this.j);
      if ((this.r == null) || (this.r.b())) {}
      for (int m = 0;; m = this.r.hashCode()) {
        return m + (((((((((((n + 527) * 31 + i1) * 31 + i2) * 31 + i3) * 31 + i4) * 31 + i5) * 31 + i6) * 31 + i7) * 31 + i8) * 31 + i9) * 31 + i10) * 31;
      }
    }
  }
  
  public static final class zzh
    extends zzso<zzh>
  {
    public static final zzsp<zzag.zza, zzh> a = zzsp.a(11, zzh.class, 810L);
    private static final zzh[] i = new zzh[0];
    public int[] b;
    public int[] c;
    public int[] d;
    public int e;
    public int[] f;
    public int g;
    public int h;
    
    public zzh()
    {
      a();
    }
    
    public zzh a()
    {
      this.b = zzsx.a;
      this.c = zzsx.a;
      this.d = zzsx.a;
      this.e = 0;
      this.f = zzsx.a;
      this.g = 0;
      this.h = 0;
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public zzh a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int j = paramzzsm.a();
        int k;
        int[] arrayOfInt;
        int m;
        switch (j)
        {
        default: 
          if (a(paramzzsm, j)) {}
          break;
        case 0: 
          return this;
        case 8: 
          k = zzsx.b(paramzzsm, 8);
          if (this.b == null) {}
          for (j = 0;; j = this.b.length)
          {
            arrayOfInt = new int[k + j];
            k = j;
            if (j != 0)
            {
              System.arraycopy(this.b, 0, arrayOfInt, 0, j);
              k = j;
            }
            while (k < arrayOfInt.length - 1)
            {
              arrayOfInt[k] = paramzzsm.g();
              paramzzsm.a();
              k += 1;
            }
          }
          arrayOfInt[k] = paramzzsm.g();
          this.b = arrayOfInt;
          break;
        case 10: 
          m = paramzzsm.d(paramzzsm.m());
          j = paramzzsm.s();
          k = 0;
          while (paramzzsm.q() > 0)
          {
            paramzzsm.g();
            k += 1;
          }
          paramzzsm.f(j);
          if (this.b == null) {}
          for (j = 0;; j = this.b.length)
          {
            arrayOfInt = new int[k + j];
            k = j;
            if (j != 0)
            {
              System.arraycopy(this.b, 0, arrayOfInt, 0, j);
              k = j;
            }
            while (k < arrayOfInt.length)
            {
              arrayOfInt[k] = paramzzsm.g();
              k += 1;
            }
          }
          this.b = arrayOfInt;
          paramzzsm.e(m);
          break;
        case 16: 
          k = zzsx.b(paramzzsm, 16);
          if (this.c == null) {}
          for (j = 0;; j = this.c.length)
          {
            arrayOfInt = new int[k + j];
            k = j;
            if (j != 0)
            {
              System.arraycopy(this.c, 0, arrayOfInt, 0, j);
              k = j;
            }
            while (k < arrayOfInt.length - 1)
            {
              arrayOfInt[k] = paramzzsm.g();
              paramzzsm.a();
              k += 1;
            }
          }
          arrayOfInt[k] = paramzzsm.g();
          this.c = arrayOfInt;
          break;
        case 18: 
          m = paramzzsm.d(paramzzsm.m());
          j = paramzzsm.s();
          k = 0;
          while (paramzzsm.q() > 0)
          {
            paramzzsm.g();
            k += 1;
          }
          paramzzsm.f(j);
          if (this.c == null) {}
          for (j = 0;; j = this.c.length)
          {
            arrayOfInt = new int[k + j];
            k = j;
            if (j != 0)
            {
              System.arraycopy(this.c, 0, arrayOfInt, 0, j);
              k = j;
            }
            while (k < arrayOfInt.length)
            {
              arrayOfInt[k] = paramzzsm.g();
              k += 1;
            }
          }
          this.c = arrayOfInt;
          paramzzsm.e(m);
          break;
        case 24: 
          k = zzsx.b(paramzzsm, 24);
          if (this.d == null) {}
          for (j = 0;; j = this.d.length)
          {
            arrayOfInt = new int[k + j];
            k = j;
            if (j != 0)
            {
              System.arraycopy(this.d, 0, arrayOfInt, 0, j);
              k = j;
            }
            while (k < arrayOfInt.length - 1)
            {
              arrayOfInt[k] = paramzzsm.g();
              paramzzsm.a();
              k += 1;
            }
          }
          arrayOfInt[k] = paramzzsm.g();
          this.d = arrayOfInt;
          break;
        case 26: 
          m = paramzzsm.d(paramzzsm.m());
          j = paramzzsm.s();
          k = 0;
          while (paramzzsm.q() > 0)
          {
            paramzzsm.g();
            k += 1;
          }
          paramzzsm.f(j);
          if (this.d == null) {}
          for (j = 0;; j = this.d.length)
          {
            arrayOfInt = new int[k + j];
            k = j;
            if (j != 0)
            {
              System.arraycopy(this.d, 0, arrayOfInt, 0, j);
              k = j;
            }
            while (k < arrayOfInt.length)
            {
              arrayOfInt[k] = paramzzsm.g();
              k += 1;
            }
          }
          this.d = arrayOfInt;
          paramzzsm.e(m);
          break;
        case 32: 
          this.e = paramzzsm.g();
          break;
        case 40: 
          k = zzsx.b(paramzzsm, 40);
          if (this.f == null) {}
          for (j = 0;; j = this.f.length)
          {
            arrayOfInt = new int[k + j];
            k = j;
            if (j != 0)
            {
              System.arraycopy(this.f, 0, arrayOfInt, 0, j);
              k = j;
            }
            while (k < arrayOfInt.length - 1)
            {
              arrayOfInt[k] = paramzzsm.g();
              paramzzsm.a();
              k += 1;
            }
          }
          arrayOfInt[k] = paramzzsm.g();
          this.f = arrayOfInt;
          break;
        case 42: 
          m = paramzzsm.d(paramzzsm.m());
          j = paramzzsm.s();
          k = 0;
          while (paramzzsm.q() > 0)
          {
            paramzzsm.g();
            k += 1;
          }
          paramzzsm.f(j);
          if (this.f == null) {}
          for (j = 0;; j = this.f.length)
          {
            arrayOfInt = new int[k + j];
            k = j;
            if (j != 0)
            {
              System.arraycopy(this.f, 0, arrayOfInt, 0, j);
              k = j;
            }
            while (k < arrayOfInt.length)
            {
              arrayOfInt[k] = paramzzsm.g();
              k += 1;
            }
          }
          this.f = arrayOfInt;
          paramzzsm.e(m);
          break;
        case 48: 
          this.g = paramzzsm.g();
          break;
        case 56: 
          this.h = paramzzsm.g();
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      int k = 0;
      int j;
      if ((this.b != null) && (this.b.length > 0))
      {
        j = 0;
        while (j < this.b.length)
        {
          paramzzsn.a(1, this.b[j]);
          j += 1;
        }
      }
      if ((this.c != null) && (this.c.length > 0))
      {
        j = 0;
        while (j < this.c.length)
        {
          paramzzsn.a(2, this.c[j]);
          j += 1;
        }
      }
      if ((this.d != null) && (this.d.length > 0))
      {
        j = 0;
        while (j < this.d.length)
        {
          paramzzsn.a(3, this.d[j]);
          j += 1;
        }
      }
      if (this.e != 0) {
        paramzzsn.a(4, this.e);
      }
      if ((this.f != null) && (this.f.length > 0))
      {
        j = k;
        while (j < this.f.length)
        {
          paramzzsn.a(5, this.f[j]);
          j += 1;
        }
      }
      if (this.g != 0) {
        paramzzsn.a(6, this.g);
      }
      if (this.h != 0) {
        paramzzsn.a(7, this.h);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int n = 0;
      int m = super.b();
      int j;
      if ((this.b != null) && (this.b.length > 0))
      {
        j = 0;
        k = 0;
        while (j < this.b.length)
        {
          k += zzsn.c(this.b[j]);
          j += 1;
        }
      }
      for (int k = m + k + this.b.length * 1;; k = m)
      {
        j = k;
        if (this.c != null)
        {
          j = k;
          if (this.c.length > 0)
          {
            j = 0;
            m = 0;
            while (j < this.c.length)
            {
              m += zzsn.c(this.c[j]);
              j += 1;
            }
            j = k + m + this.c.length * 1;
          }
        }
        k = j;
        if (this.d != null)
        {
          k = j;
          if (this.d.length > 0)
          {
            k = 0;
            m = 0;
            while (k < this.d.length)
            {
              m += zzsn.c(this.d[k]);
              k += 1;
            }
            k = j + m + this.d.length * 1;
          }
        }
        j = k;
        if (this.e != 0) {
          j = k + zzsn.c(4, this.e);
        }
        k = j;
        if (this.f != null)
        {
          k = j;
          if (this.f.length > 0)
          {
            m = 0;
            k = n;
            while (k < this.f.length)
            {
              m += zzsn.c(this.f[k]);
              k += 1;
            }
            k = j + m + this.f.length * 1;
          }
        }
        j = k;
        if (this.g != 0) {
          j = k + zzsn.c(6, this.g);
        }
        k = j;
        if (this.h != 0) {
          k = j + zzsn.c(7, this.h);
        }
        return k;
      }
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  do
                  {
                    do
                    {
                      do
                      {
                        return bool1;
                        bool1 = bool2;
                      } while (!(paramObject instanceof zzh));
                      paramObject = (zzh)paramObject;
                      bool1 = bool2;
                    } while (!zzss.a(this.b, ((zzh)paramObject).b));
                    bool1 = bool2;
                  } while (!zzss.a(this.c, ((zzh)paramObject).c));
                  bool1 = bool2;
                } while (!zzss.a(this.d, ((zzh)paramObject).d));
                bool1 = bool2;
              } while (this.e != ((zzh)paramObject).e);
              bool1 = bool2;
            } while (!zzss.a(this.f, ((zzh)paramObject).f));
            bool1 = bool2;
          } while (this.g != ((zzh)paramObject).g);
          bool1 = bool2;
        } while (this.h != ((zzh)paramObject).h);
        if ((this.r != null) && (!this.r.b())) {
          break label166;
        }
        if (((zzh)paramObject).r == null) {
          break;
        }
        bool1 = bool2;
      } while (!((zzh)paramObject).r.b());
      return true;
      label166:
      return this.r.equals(((zzh)paramObject).r);
    }
    
    public int hashCode()
    {
      int k = getClass().getName().hashCode();
      int m = zzss.a(this.b);
      int n = zzss.a(this.c);
      int i1 = zzss.a(this.d);
      int i2 = this.e;
      int i3 = zzss.a(this.f);
      int i4 = this.g;
      int i5 = this.h;
      if ((this.r == null) || (this.r.b())) {}
      for (int j = 0;; j = this.r.hashCode()) {
        return j + ((((((((k + 527) * 31 + m) * 31 + n) * 31 + i1) * 31 + i2) * 31 + i3) * 31 + i4) * 31 + i5) * 31;
      }
    }
  }
  
  public static final class zzi
    extends zzso<zzi>
  {
    private static volatile zzi[] d;
    public String a;
    public zzag.zza b;
    public zzaf.zzd c;
    
    public zzi()
    {
      c();
    }
    
    public static zzi[] a()
    {
      if (d == null) {}
      synchronized (zzss.a)
      {
        if (d == null) {
          d = new zzi[0];
        }
        return d;
      }
    }
    
    public zzi a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 10: 
          this.a = paramzzsm.i();
          break;
        case 18: 
          if (this.b == null) {
            this.b = new zzag.zza();
          }
          paramzzsm.a(this.b);
          break;
        case 26: 
          if (this.c == null) {
            this.c = new zzaf.zzd();
          }
          paramzzsm.a(this.c);
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (!this.a.equals("")) {
        paramzzsn.a(1, this.a);
      }
      if (this.b != null) {
        paramzzsn.a(2, this.b);
      }
      if (this.c != null) {
        paramzzsn.a(3, this.c);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b();
      int i = j;
      if (!this.a.equals("")) {
        i = j + zzsn.b(1, this.a);
      }
      j = i;
      if (this.b != null) {
        j = i + zzsn.c(2, this.b);
      }
      i = j;
      if (this.c != null) {
        i = j + zzsn.c(3, this.c);
      }
      return i;
    }
    
    public zzi c()
    {
      this.a = "";
      this.b = null;
      this.c = null;
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      label41:
      label57:
      do
      {
        do
        {
          do
          {
            do
            {
              return bool1;
              bool1 = bool2;
            } while (!(paramObject instanceof zzi));
            paramObject = (zzi)paramObject;
            if (this.a != null) {
              break;
            }
            bool1 = bool2;
          } while (((zzi)paramObject).a != null);
          if (this.b != null) {
            break label127;
          }
          bool1 = bool2;
        } while (((zzi)paramObject).b != null);
        if (this.c != null) {
          break label143;
        }
        bool1 = bool2;
      } while (((zzi)paramObject).c != null);
      for (;;)
      {
        if ((this.r == null) || (this.r.b()))
        {
          if (((zzi)paramObject).r != null)
          {
            bool1 = bool2;
            if (!((zzi)paramObject).r.b()) {
              break;
            }
          }
          return true;
          if (this.a.equals(((zzi)paramObject).a)) {
            break label41;
          }
          return false;
          label127:
          if (this.b.equals(((zzi)paramObject).b)) {
            break label57;
          }
          return false;
          label143:
          if (!this.c.equals(((zzi)paramObject).c)) {
            return false;
          }
        }
      }
      return this.r.equals(((zzi)paramObject).r);
    }
    
    public int hashCode()
    {
      int n = 0;
      int i1 = getClass().getName().hashCode();
      int i;
      int j;
      label33:
      int k;
      if (this.a == null)
      {
        i = 0;
        if (this.b != null) {
          break label106;
        }
        j = 0;
        if (this.c != null) {
          break label117;
        }
        k = 0;
        label42:
        m = n;
        if (this.r != null) {
          if (!this.r.b()) {
            break label128;
          }
        }
      }
      label106:
      label117:
      label128:
      for (int m = n;; m = this.r.hashCode())
      {
        return (k + (j + (i + (i1 + 527) * 31) * 31) * 31) * 31 + m;
        i = this.a.hashCode();
        break;
        j = this.b.hashCode();
        break label33;
        k = this.c.hashCode();
        break label42;
      }
    }
  }
  
  public static final class zzj
    extends zzso<zzj>
  {
    public zzaf.zzi[] a;
    public zzaf.zzf b;
    public String c;
    
    public zzj()
    {
      a();
    }
    
    public static zzj a(byte[] paramArrayOfByte)
      throws zzst
    {
      return (zzj)zzsu.a(new zzj(), paramArrayOfByte);
    }
    
    public zzj a()
    {
      this.a = zzaf.zzi.a();
      this.b = null;
      this.c = "";
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public zzj a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 10: 
          int j = zzsx.b(paramzzsm, 10);
          if (this.a == null) {}
          zzaf.zzi[] arrayOfzzi;
          for (i = 0;; i = this.a.length)
          {
            arrayOfzzi = new zzaf.zzi[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.a, 0, arrayOfzzi, 0, i);
              j = i;
            }
            while (j < arrayOfzzi.length - 1)
            {
              arrayOfzzi[j] = new zzaf.zzi();
              paramzzsm.a(arrayOfzzi[j]);
              paramzzsm.a();
              j += 1;
            }
          }
          arrayOfzzi[j] = new zzaf.zzi();
          paramzzsm.a(arrayOfzzi[j]);
          this.a = arrayOfzzi;
          break;
        case 18: 
          if (this.b == null) {
            this.b = new zzaf.zzf();
          }
          paramzzsm.a(this.b);
          break;
        case 26: 
          this.c = paramzzsm.i();
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if ((this.a != null) && (this.a.length > 0))
      {
        int i = 0;
        while (i < this.a.length)
        {
          zzaf.zzi localzzi = this.a[i];
          if (localzzi != null) {
            paramzzsn.a(1, localzzi);
          }
          i += 1;
        }
      }
      if (this.b != null) {
        paramzzsn.a(2, this.b);
      }
      if (!this.c.equals("")) {
        paramzzsn.a(3, this.c);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int i = super.b();
      int j = i;
      if (this.a != null)
      {
        j = i;
        if (this.a.length > 0)
        {
          int k = 0;
          for (;;)
          {
            j = i;
            if (k >= this.a.length) {
              break;
            }
            zzaf.zzi localzzi = this.a[k];
            j = i;
            if (localzzi != null) {
              j = i + zzsn.c(1, localzzi);
            }
            k += 1;
            i = j;
          }
        }
      }
      i = j;
      if (this.b != null) {
        i = j + zzsn.c(2, this.b);
      }
      j = i;
      if (!this.c.equals("")) {
        j = i + zzsn.b(3, this.c);
      }
      return j;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      label57:
      do
      {
        do
        {
          do
          {
            do
            {
              return bool1;
              bool1 = bool2;
            } while (!(paramObject instanceof zzj));
            paramObject = (zzj)paramObject;
            bool1 = bool2;
          } while (!zzss.a(this.a, ((zzj)paramObject).a));
          if (this.b != null) {
            break;
          }
          bool1 = bool2;
        } while (((zzj)paramObject).b != null);
        if (this.c != null) {
          break label127;
        }
        bool1 = bool2;
      } while (((zzj)paramObject).c != null);
      for (;;)
      {
        if ((this.r == null) || (this.r.b()))
        {
          if (((zzj)paramObject).r != null)
          {
            bool1 = bool2;
            if (!((zzj)paramObject).r.b()) {
              break;
            }
          }
          return true;
          if (this.b.equals(((zzj)paramObject).b)) {
            break label57;
          }
          return false;
          label127:
          if (!this.c.equals(((zzj)paramObject).c)) {
            return false;
          }
        }
      }
      return this.r.equals(((zzj)paramObject).r);
    }
    
    public int hashCode()
    {
      int m = 0;
      int n = getClass().getName().hashCode();
      int i1 = zzss.a(this.a);
      int i;
      int j;
      if (this.b == null)
      {
        i = 0;
        if (this.c != null) {
          break label104;
        }
        j = 0;
        label42:
        k = m;
        if (this.r != null) {
          if (!this.r.b()) {
            break label115;
          }
        }
      }
      label104:
      label115:
      for (int k = m;; k = this.r.hashCode())
      {
        return (j + (i + ((n + 527) * 31 + i1) * 31) * 31) * 31 + k;
        i = this.b.hashCode();
        break;
        j = this.c.hashCode();
        break label42;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzaf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */