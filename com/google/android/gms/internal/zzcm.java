package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.ads.formats.NativeAd.Image;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zzd;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@zzhb
public class zzcm
  extends NativeAppInstallAd
{
  private final zzcl a;
  private final List<NativeAd.Image> b = new ArrayList();
  private final zzci c;
  
  public zzcm(zzcl paramzzcl)
  {
    this.a = paramzzcl;
    try
    {
      paramzzcl = this.a.b();
      if (paramzzcl != null)
      {
        paramzzcl = paramzzcl.iterator();
        while (paramzzcl.hasNext())
        {
          zzch localzzch = a(paramzzcl.next());
          if (localzzch != null) {
            this.b.add(new zzci(localzzch));
          }
        }
      }
      try
      {
        paramzzcl = this.a.d();
        if (paramzzcl == null) {
          break label129;
        }
        paramzzcl = new zzci(paramzzcl);
      }
      catch (RemoteException paramzzcl)
      {
        for (;;)
        {
          zzb.b("Failed to get icon.", paramzzcl);
          paramzzcl = null;
        }
      }
    }
    catch (RemoteException paramzzcl)
    {
      zzb.b("Failed to get image.", paramzzcl);
    }
    this.c = paramzzcl;
  }
  
  zzch a(Object paramObject)
  {
    if ((paramObject instanceof IBinder)) {
      return zzch.zza.a((IBinder)paramObject);
    }
    return null;
  }
  
  public CharSequence b()
  {
    try
    {
      String str = this.a.a();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.b("Failed to get headline.", localRemoteException);
    }
    return null;
  }
  
  public List<NativeAd.Image> c()
  {
    return this.b;
  }
  
  public CharSequence d()
  {
    try
    {
      String str = this.a.c();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.b("Failed to get body.", localRemoteException);
    }
    return null;
  }
  
  public NativeAd.Image e()
  {
    return this.c;
  }
  
  public CharSequence f()
  {
    try
    {
      String str = this.a.e();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.b("Failed to get call to action.", localRemoteException);
    }
    return null;
  }
  
  public Double g()
  {
    try
    {
      double d = this.a.f();
      if (d == -1.0D) {
        return null;
      }
      return Double.valueOf(d);
    }
    catch (RemoteException localRemoteException)
    {
      zzb.b("Failed to get star rating.", localRemoteException);
    }
    return null;
  }
  
  public CharSequence h()
  {
    try
    {
      String str = this.a.g();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.b("Failed to get store", localRemoteException);
    }
    return null;
  }
  
  public CharSequence i()
  {
    try
    {
      String str = this.a.h();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.b("Failed to get price.", localRemoteException);
    }
    return null;
  }
  
  protected zzd j()
  {
    try
    {
      zzd localzzd = this.a.i();
      return localzzd;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.b("Failed to retrieve native ad engine.", localRemoteException);
    }
    return null;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzcm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */