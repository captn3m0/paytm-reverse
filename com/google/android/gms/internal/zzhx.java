package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.zza.zza;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;

@zzhb
public class zzhx
  extends zza.zza
{
  private zzhv a;
  private zzhy b;
  private zzhw c;
  
  public zzhx(zzhw paramzzhw)
  {
    this.c = paramzzhw;
  }
  
  public void a(zzd paramzzd)
  {
    if (this.a != null) {
      this.a.c();
    }
  }
  
  public void a(zzd paramzzd, int paramInt)
  {
    if (this.a != null) {
      this.a.a(paramInt);
    }
  }
  
  public void a(zzd paramzzd, RewardItemParcel paramRewardItemParcel)
  {
    if (this.c != null) {
      this.c.b(paramRewardItemParcel);
    }
  }
  
  public void a(zzhv paramzzhv)
  {
    this.a = paramzzhv;
  }
  
  public void a(zzhy paramzzhy)
  {
    this.b = paramzzhy;
  }
  
  public void b(zzd paramzzd)
  {
    if (this.b != null) {
      this.b.a(zze.a(paramzzd).getClass().getName());
    }
  }
  
  public void b(zzd paramzzd, int paramInt)
  {
    if (this.b != null) {
      this.b.a(zze.a(paramzzd).getClass().getName(), paramInt);
    }
  }
  
  public void c(zzd paramzzd)
  {
    if (this.c != null) {
      this.c.B();
    }
  }
  
  public void d(zzd paramzzd)
  {
    if (this.c != null) {
      this.c.C();
    }
  }
  
  public void e(zzd paramzzd)
  {
    if (this.c != null) {
      this.c.D();
    }
  }
  
  public void f(zzd paramzzd)
  {
    if (this.c != null) {
      this.c.E();
    }
  }
  
  public void g(zzd paramzzd)
  {
    if (this.c != null) {
      this.c.F();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzhx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */