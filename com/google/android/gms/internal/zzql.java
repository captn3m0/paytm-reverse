package com.google.android.gms.internal;

import android.content.Context;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zza;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.nearby.connection.Connections;
import com.google.android.gms.nearby.connection.Connections.StartAdvertisingResult;

public final class zzql
  implements Connections
{
  public static final Api.zzc<zzqk> a = new Api.zzc();
  public static final Api.zza<zzqk, Api.ApiOptions.NoOptions> b = new Api.zza()
  {
    public zzqk a(Context paramAnonymousContext, Looper paramAnonymousLooper, zzf paramAnonymouszzf, Api.ApiOptions.NoOptions paramAnonymousNoOptions, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      return new zzqk(paramAnonymousContext, paramAnonymousLooper, paramAnonymouszzf, paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
    }
  };
  
  private static abstract class zza<R extends Result>
    extends zza.zza<R, zzqk>
  {}
  
  private static abstract class zzb
    extends zzql.zza<Connections.StartAdvertisingResult>
  {
    public Connections.StartAdvertisingResult a(final Status paramStatus)
    {
      new Connections.StartAdvertisingResult()
      {
        public Status getStatus()
        {
          return paramStatus;
        }
      };
    }
  }
  
  private static abstract class zzc
    extends zzql.zza<Status>
  {
    public Status a(Status paramStatus)
    {
      return paramStatus;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzql.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */