package com.google.android.gms.internal;

public enum zzad
{
  private final String bn;
  
  static
  {
    A = new zzad("RESOLUTION", 26, "_rs");
    B = new zzad("RUNTIME_VERSION", 27, "_rv");
    C = new zzad("SDK_VERSION", 28, "_sv");
    D = new zzad("SIMPLE_MAP", 29, "_smm");
    E = new zzad("TIME", 30, "_t");
    F = new zzad("URL", 31, "_u");
    G = new zzad("ADWORDS_CLICK_REFERRER", 32, "_awcr");
    H = new zzad("DEVICE_ID", 33, "_did");
    I = new zzad("ENCODE", 34, "_enc");
    J = new zzad("GTM_VERSION", 35, "_gtmv");
    K = new zzad("HASH", 36, "_hsh");
    L = new zzad("INSTALL_REFERRER", 37, "_ir");
    M = new zzad("JOINER", 38, "_jn");
    N = new zzad("MOBILE_ADWORDS_UNIQUE_ID", 39, "_awid");
    O = new zzad("REGEX_GROUP", 40, "_reg");
    P = new zzad("DATA_LAYER_WRITE", 41, "_dlw");
    Q = new zzad("LOWERCASE_STRING", 42, "_ls");
    R = new zzad("UPPERCASE_STRING", 43, "_us");
    S = new zzad("EXPERIMENT_ENABLED", 44, "_ee");
    T = new zzad("IN_EXPERIMENT", 45, "_ie");
    U = new zzad("EXPERIMENT_VARIATION_INDEX", 46, "_evi");
    V = new zzad("EXPERIMENT_UID", 47, "_euid");
    W = new zzad("AUDIENCE_LISTS", 48, "_aud");
    X = new zzad("CSS_SELECTOR", 49, "_sel");
    Y = new zzad("GA_CLIENT_ID", 50, "_gacid");
    Z = new zzad("GEO_IP", 51, "_geo");
    aa = new zzad("USER_AGENT", 52, "_uagt");
    ab = new zzad("GA_FIRST_PAGE", 53, "_gafp");
    ac = new zzad("EXPERIMENT_EXPIRATION_DATES", 54, "_xxd");
    ad = new zzad("UNDEFINED_VALUE", 55, "_uv");
    ae = new zzad("PRODUCT_SETTING_PROPERTY", 56, "_prodset");
    af = new zzad("GA_OPT_OUT_CLIENT", 57, "_gaoo_c");
    ag = new zzad("GA_OPT_OUT_SERVER", 58, "_gaoo_s");
    ah = new zzad("REGEX", 59, "_re");
    ai = new zzad("STARTS_WITH", 60, "_sw");
    aj = new zzad("ENDS_WITH", 61, "_ew");
    ak = new zzad("CONTAINS", 62, "_cn");
    al = new zzad("EQUALS", 63, "_eq");
    am = new zzad("LESS_THAN", 64, "_lt");
    an = new zzad("LESS_EQUALS", 65, "_le");
    ao = new zzad("GREATER_THAN", 66, "_gt");
    ap = new zzad("GREATER_EQUALS", 67, "_ge");
    aq = new zzad("CSS_SELECTOR_PREDICATE", 68, "_css");
    ar = new zzad("URL_MATCHES", 69, "_um");
    as = new zzad("ARBITRARY_PIXEL", 70, "_img");
    at = new zzad("ARBITRARY_HTML", 71, "_html");
    au = new zzad("GOOGLE_TAG_MANAGER", 72, "_gtm");
    av = new zzad("GOOGLE_ANALYTICS", 73, "_ga");
    aw = new zzad("ADWORDS_CONVERSION", 74, "_awct");
    ax = new zzad("SMART_PIXEL", 75, "_sp");
    ay = new zzad("FLOODLIGHT_COUNTER", 76, "_flc");
    az = new zzad("FLOODLIGHT_SALES", 77, "_fls");
  }
  
  private zzad(String paramString)
  {
    this.bn = paramString;
  }
  
  public String toString()
  {
    return this.bn;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzad.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */