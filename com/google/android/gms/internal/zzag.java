package com.google.android.gms.internal;

import java.io.IOException;

public abstract interface zzag
{
  public static final class zza
    extends zzso<zza>
  {
    private static volatile zza[] m;
    public int a;
    public String b;
    public zza[] c;
    public zza[] d;
    public zza[] e;
    public String f;
    public String g;
    public long h;
    public boolean i;
    public zza[] j;
    public int[] k;
    public boolean l;
    
    public zza()
    {
      c();
    }
    
    public static zza[] a()
    {
      if (m == null) {}
      synchronized (zzss.a)
      {
        if (m == null) {
          m = new zza[0];
        }
        return m;
      }
    }
    
    public zza a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int n = paramzzsm.a();
        int i1;
        Object localObject;
        int i2;
        switch (n)
        {
        default: 
          if (a(paramzzsm, n)) {}
          break;
        case 0: 
          return this;
        case 8: 
          n = paramzzsm.g();
          switch (n)
          {
          default: 
            break;
          case 1: 
          case 2: 
          case 3: 
          case 4: 
          case 5: 
          case 6: 
          case 7: 
          case 8: 
            this.a = n;
          }
          break;
        case 18: 
          this.b = paramzzsm.i();
          break;
        case 26: 
          i1 = zzsx.b(paramzzsm, 26);
          if (this.c == null) {}
          for (n = 0;; n = this.c.length)
          {
            localObject = new zza[i1 + n];
            i1 = n;
            if (n != 0)
            {
              System.arraycopy(this.c, 0, localObject, 0, n);
              i1 = n;
            }
            while (i1 < localObject.length - 1)
            {
              localObject[i1] = new zza();
              paramzzsm.a(localObject[i1]);
              paramzzsm.a();
              i1 += 1;
            }
          }
          localObject[i1] = new zza();
          paramzzsm.a(localObject[i1]);
          this.c = ((zza[])localObject);
          break;
        case 34: 
          i1 = zzsx.b(paramzzsm, 34);
          if (this.d == null) {}
          for (n = 0;; n = this.d.length)
          {
            localObject = new zza[i1 + n];
            i1 = n;
            if (n != 0)
            {
              System.arraycopy(this.d, 0, localObject, 0, n);
              i1 = n;
            }
            while (i1 < localObject.length - 1)
            {
              localObject[i1] = new zza();
              paramzzsm.a(localObject[i1]);
              paramzzsm.a();
              i1 += 1;
            }
          }
          localObject[i1] = new zza();
          paramzzsm.a(localObject[i1]);
          this.d = ((zza[])localObject);
          break;
        case 42: 
          i1 = zzsx.b(paramzzsm, 42);
          if (this.e == null) {}
          for (n = 0;; n = this.e.length)
          {
            localObject = new zza[i1 + n];
            i1 = n;
            if (n != 0)
            {
              System.arraycopy(this.e, 0, localObject, 0, n);
              i1 = n;
            }
            while (i1 < localObject.length - 1)
            {
              localObject[i1] = new zza();
              paramzzsm.a(localObject[i1]);
              paramzzsm.a();
              i1 += 1;
            }
          }
          localObject[i1] = new zza();
          paramzzsm.a(localObject[i1]);
          this.e = ((zza[])localObject);
          break;
        case 50: 
          this.f = paramzzsm.i();
          break;
        case 58: 
          this.g = paramzzsm.i();
          break;
        case 64: 
          this.h = paramzzsm.f();
          break;
        case 72: 
          this.l = paramzzsm.h();
          break;
        case 80: 
          int i3 = zzsx.b(paramzzsm, 80);
          localObject = new int[i3];
          i1 = 0;
          n = 0;
          if (i1 < i3)
          {
            if (i1 != 0) {
              paramzzsm.a();
            }
            int i4 = paramzzsm.g();
            switch (i4)
            {
            }
            for (;;)
            {
              i1 += 1;
              break;
              i2 = n + 1;
              localObject[n] = i4;
              n = i2;
            }
          }
          if (n != 0)
          {
            if (this.k == null) {}
            for (i1 = 0;; i1 = this.k.length)
            {
              if ((i1 != 0) || (n != localObject.length)) {
                break label810;
              }
              this.k = ((int[])localObject);
              break;
            }
            int[] arrayOfInt = new int[i1 + n];
            if (i1 != 0) {
              System.arraycopy(this.k, 0, arrayOfInt, 0, i1);
            }
            System.arraycopy(localObject, 0, arrayOfInt, i1, n);
            this.k = arrayOfInt;
          }
          break;
        case 82: 
          i2 = paramzzsm.d(paramzzsm.m());
          n = paramzzsm.s();
          i1 = 0;
          while (paramzzsm.q() > 0) {
            switch (paramzzsm.g())
            {
            default: 
              break;
            case 1: 
            case 2: 
            case 3: 
            case 4: 
            case 5: 
            case 6: 
            case 7: 
            case 8: 
            case 9: 
            case 10: 
            case 11: 
            case 12: 
            case 13: 
            case 14: 
            case 15: 
            case 16: 
            case 17: 
              i1 += 1;
            }
          }
          if (i1 != 0)
          {
            paramzzsm.f(n);
            if (this.k == null) {}
            for (n = 0;; n = this.k.length)
            {
              localObject = new int[i1 + n];
              i1 = n;
              if (n != 0)
              {
                System.arraycopy(this.k, 0, localObject, 0, n);
                i1 = n;
              }
              while (paramzzsm.q() > 0)
              {
                n = paramzzsm.g();
                switch (n)
                {
                default: 
                  break;
                case 1: 
                case 2: 
                case 3: 
                case 4: 
                case 5: 
                case 6: 
                case 7: 
                case 8: 
                case 9: 
                case 10: 
                case 11: 
                case 12: 
                case 13: 
                case 14: 
                case 15: 
                case 16: 
                case 17: 
                  localObject[i1] = n;
                  i1 += 1;
                }
              }
            }
            this.k = ((int[])localObject);
          }
          paramzzsm.e(i2);
          break;
        case 90: 
          i1 = zzsx.b(paramzzsm, 90);
          if (this.j == null) {}
          for (n = 0;; n = this.j.length)
          {
            localObject = new zza[i1 + n];
            i1 = n;
            if (n != 0)
            {
              System.arraycopy(this.j, 0, localObject, 0, n);
              i1 = n;
            }
            while (i1 < localObject.length - 1)
            {
              localObject[i1] = new zza();
              paramzzsm.a(localObject[i1]);
              paramzzsm.a();
              i1 += 1;
            }
          }
          localObject[i1] = new zza();
          paramzzsm.a(localObject[i1]);
          this.j = ((zza[])localObject);
          break;
        case 96: 
          label810:
          this.i = paramzzsm.h();
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      int i1 = 0;
      paramzzsn.a(1, this.a);
      if (!this.b.equals("")) {
        paramzzsn.a(2, this.b);
      }
      int n;
      zza localzza;
      if ((this.c != null) && (this.c.length > 0))
      {
        n = 0;
        while (n < this.c.length)
        {
          localzza = this.c[n];
          if (localzza != null) {
            paramzzsn.a(3, localzza);
          }
          n += 1;
        }
      }
      if ((this.d != null) && (this.d.length > 0))
      {
        n = 0;
        while (n < this.d.length)
        {
          localzza = this.d[n];
          if (localzza != null) {
            paramzzsn.a(4, localzza);
          }
          n += 1;
        }
      }
      if ((this.e != null) && (this.e.length > 0))
      {
        n = 0;
        while (n < this.e.length)
        {
          localzza = this.e[n];
          if (localzza != null) {
            paramzzsn.a(5, localzza);
          }
          n += 1;
        }
      }
      if (!this.f.equals("")) {
        paramzzsn.a(6, this.f);
      }
      if (!this.g.equals("")) {
        paramzzsn.a(7, this.g);
      }
      if (this.h != 0L) {
        paramzzsn.b(8, this.h);
      }
      if (this.l) {
        paramzzsn.a(9, this.l);
      }
      if ((this.k != null) && (this.k.length > 0))
      {
        n = 0;
        while (n < this.k.length)
        {
          paramzzsn.a(10, this.k[n]);
          n += 1;
        }
      }
      if ((this.j != null) && (this.j.length > 0))
      {
        n = i1;
        while (n < this.j.length)
        {
          localzza = this.j[n];
          if (localzza != null) {
            paramzzsn.a(11, localzza);
          }
          n += 1;
        }
      }
      if (this.i) {
        paramzzsn.a(12, this.i);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int i3 = 0;
      int i1 = super.b() + zzsn.c(1, this.a);
      int n = i1;
      if (!this.b.equals("")) {
        n = i1 + zzsn.b(2, this.b);
      }
      i1 = n;
      zza localzza;
      int i2;
      if (this.c != null)
      {
        i1 = n;
        if (this.c.length > 0)
        {
          i1 = 0;
          while (i1 < this.c.length)
          {
            localzza = this.c[i1];
            i2 = n;
            if (localzza != null) {
              i2 = n + zzsn.c(3, localzza);
            }
            i1 += 1;
            n = i2;
          }
          i1 = n;
        }
      }
      n = i1;
      if (this.d != null)
      {
        n = i1;
        if (this.d.length > 0)
        {
          n = i1;
          i1 = 0;
          while (i1 < this.d.length)
          {
            localzza = this.d[i1];
            i2 = n;
            if (localzza != null) {
              i2 = n + zzsn.c(4, localzza);
            }
            i1 += 1;
            n = i2;
          }
        }
      }
      i1 = n;
      if (this.e != null)
      {
        i1 = n;
        if (this.e.length > 0)
        {
          i1 = 0;
          while (i1 < this.e.length)
          {
            localzza = this.e[i1];
            i2 = n;
            if (localzza != null) {
              i2 = n + zzsn.c(5, localzza);
            }
            i1 += 1;
            n = i2;
          }
          i1 = n;
        }
      }
      n = i1;
      if (!this.f.equals("")) {
        n = i1 + zzsn.b(6, this.f);
      }
      i1 = n;
      if (!this.g.equals("")) {
        i1 = n + zzsn.b(7, this.g);
      }
      n = i1;
      if (this.h != 0L) {
        n = i1 + zzsn.d(8, this.h);
      }
      i1 = n;
      if (this.l) {
        i1 = n + zzsn.b(9, this.l);
      }
      n = i1;
      if (this.k != null)
      {
        n = i1;
        if (this.k.length > 0)
        {
          n = 0;
          i2 = 0;
          while (n < this.k.length)
          {
            i2 += zzsn.c(this.k[n]);
            n += 1;
          }
          n = i1 + i2 + this.k.length * 1;
        }
      }
      i1 = n;
      if (this.j != null)
      {
        i1 = n;
        if (this.j.length > 0)
        {
          i2 = i3;
          for (;;)
          {
            i1 = n;
            if (i2 >= this.j.length) {
              break;
            }
            localzza = this.j[i2];
            i1 = n;
            if (localzza != null) {
              i1 = n + zzsn.c(11, localzza);
            }
            i2 += 1;
            n = i1;
          }
        }
      }
      n = i1;
      if (this.i) {
        n = i1 + zzsn.b(12, this.i);
      }
      return n;
    }
    
    public zza c()
    {
      this.a = 1;
      this.b = "";
      this.c = a();
      this.d = a();
      this.e = a();
      this.f = "";
      this.g = "";
      this.h = 0L;
      this.i = false;
      this.j = a();
      this.k = zzsx.a;
      this.l = false;
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      label54:
      label118:
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  do
                  {
                    do
                    {
                      return bool1;
                      bool1 = bool2;
                    } while (!(paramObject instanceof zza));
                    paramObject = (zza)paramObject;
                    bool1 = bool2;
                  } while (this.a != ((zza)paramObject).a);
                  if (this.b != null) {
                    break;
                  }
                  bool1 = bool2;
                } while (((zza)paramObject).b != null);
                bool1 = bool2;
              } while (!zzss.a(this.c, ((zza)paramObject).c));
              bool1 = bool2;
            } while (!zzss.a(this.d, ((zza)paramObject).d));
            bool1 = bool2;
          } while (!zzss.a(this.e, ((zza)paramObject).e));
          if (this.f != null) {
            break label260;
          }
          bool1 = bool2;
        } while (((zza)paramObject).f != null);
        if (this.g != null) {
          break label276;
        }
        bool1 = bool2;
      } while (((zza)paramObject).g != null);
      label260:
      label276:
      while (this.g.equals(((zza)paramObject).g))
      {
        bool1 = bool2;
        if (this.h != ((zza)paramObject).h) {
          break;
        }
        bool1 = bool2;
        if (this.i != ((zza)paramObject).i) {
          break;
        }
        bool1 = bool2;
        if (!zzss.a(this.j, ((zza)paramObject).j)) {
          break;
        }
        bool1 = bool2;
        if (!zzss.a(this.k, ((zza)paramObject).k)) {
          break;
        }
        bool1 = bool2;
        if (this.l != ((zza)paramObject).l) {
          break;
        }
        if ((this.r != null) && (!this.r.b())) {
          break label292;
        }
        if (((zza)paramObject).r != null)
        {
          bool1 = bool2;
          if (!((zza)paramObject).r.b()) {
            break;
          }
        }
        return true;
        if (this.b.equals(((zza)paramObject).b)) {
          break label54;
        }
        return false;
        if (this.f.equals(((zza)paramObject).f)) {
          break label118;
        }
        return false;
      }
      return false;
      label292:
      return this.r.equals(((zza)paramObject).r);
    }
    
    public int hashCode()
    {
      int i4 = 1231;
      int i6 = 0;
      int i7 = getClass().getName().hashCode();
      int i8 = this.a;
      int n;
      int i9;
      int i10;
      int i11;
      int i1;
      label71:
      int i2;
      label80:
      int i12;
      int i3;
      label107:
      int i13;
      int i14;
      if (this.b == null)
      {
        n = 0;
        i9 = zzss.a(this.c);
        i10 = zzss.a(this.d);
        i11 = zzss.a(this.e);
        if (this.f != null) {
          break label250;
        }
        i1 = 0;
        if (this.g != null) {
          break label261;
        }
        i2 = 0;
        i12 = (int)(this.h ^ this.h >>> 32);
        if (!this.i) {
          break label272;
        }
        i3 = 1231;
        i13 = zzss.a(this.j);
        i14 = zzss.a(this.k);
        if (!this.l) {
          break label280;
        }
        label132:
        i5 = i6;
        if (this.r != null) {
          if (!this.r.b()) {
            break label288;
          }
        }
      }
      label250:
      label261:
      label272:
      label280:
      label288:
      for (int i5 = i6;; i5 = this.r.hashCode())
      {
        return ((((i3 + ((i2 + (i1 + ((((n + ((i7 + 527) * 31 + i8) * 31) * 31 + i9) * 31 + i10) * 31 + i11) * 31) * 31) * 31 + i12) * 31) * 31 + i13) * 31 + i14) * 31 + i4) * 31 + i5;
        n = this.b.hashCode();
        break;
        i1 = this.f.hashCode();
        break label71;
        i2 = this.g.hashCode();
        break label80;
        i3 = 1237;
        break label107;
        i4 = 1237;
        break label132;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzag.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */