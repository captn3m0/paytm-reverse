package com.google.android.gms.internal;

import android.content.Intent;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.panorama.PanoramaApi.PanoramaResult;

class zzqs
  implements PanoramaApi.PanoramaResult
{
  private final Status a;
  private final Intent b;
  
  public zzqs(Status paramStatus, Intent paramIntent)
  {
    this.a = ((Status)zzx.a(paramStatus));
    this.b = paramIntent;
  }
  
  public Status getStatus()
  {
    return this.a;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzqs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */