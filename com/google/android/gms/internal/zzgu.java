package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.zzm;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@zzhb
public class zzgu
  extends zzgq
{
  protected zzes g;
  private zzex h;
  private zzem i;
  private zzeo j;
  private final zzcb k;
  private final zzjp l;
  private boolean m;
  
  zzgu(Context paramContext, zzif.zza paramzza, zzex paramzzex, zzgr.zza paramzza1, zzcb paramzzcb, zzjp paramzzjp)
  {
    super(paramContext, paramzza, paramzza1);
    this.h = paramzzex;
    this.j = paramzza.c;
    this.k = paramzzcb;
    this.l = paramzzjp;
  }
  
  private void c()
    throws zzgq.zza
  {
    ??? = new CountDownLatch(1);
    zzir.a.post(new Runnable()
    {
      public void run()
      {
        synchronized (zzgu.this.d)
        {
          zzgu.a(zzgu.this, zzm.a(zzgu.a(zzgu.this), zzgu.this.g, localObject1));
          return;
        }
      }
    });
    try
    {
      ((CountDownLatch)???).await(10L, TimeUnit.SECONDS);
      synchronized (this.d)
      {
        if (!this.m) {
          throw new zzgq.zza("View could not be prepared", 0);
        }
      }
      if (!this.l.r()) {
        break label118;
      }
    }
    catch (InterruptedException localInterruptedException)
    {
      throw new zzgq.zza("Interrupted while waiting for latch : " + localInterruptedException, 0);
    }
    throw new zzgq.zza("Assets not loaded, web view is destroyed", 0);
    label118:
  }
  
  protected zzif a(int paramInt)
  {
    Object localObject = this.e.a;
    AdRequestParcel localAdRequestParcel = ((AdRequestInfoParcel)localObject).c;
    zzjp localzzjp = this.l;
    List localList1 = this.f.d;
    List localList2 = this.f.f;
    List localList3 = this.f.j;
    int n = this.f.l;
    long l1 = this.f.k;
    String str2 = ((AdRequestInfoParcel)localObject).i;
    boolean bool = this.f.h;
    zzey localzzey;
    label113:
    String str1;
    label129:
    zzeo localzzeo;
    if (this.g != null)
    {
      localObject = this.g.b;
      if (this.g == null) {
        break label270;
      }
      localzzey = this.g.c;
      if (this.g == null) {
        break label276;
      }
      str1 = this.g.d;
      localzzeo = this.j;
      if (this.g == null) {
        break label286;
      }
    }
    label270:
    label276:
    label286:
    for (zzeq localzzeq = this.g.e;; localzzeq = null)
    {
      return new zzif(localAdRequestParcel, localzzjp, localList1, paramInt, localList2, localList3, n, l1, str2, bool, (zzen)localObject, localzzey, str1, localzzeo, localzzeq, this.f.i, this.e.d, this.f.g, this.e.f, this.f.n, this.f.o, this.e.h, null, this.f.D, this.f.E, this.f.F, this.f.G);
      localObject = null;
      break;
      localzzey = null;
      break label113;
      str1 = AdMobAdapter.class.getName();
      break label129;
    }
  }
  
  protected void a(long paramLong)
    throws zzgq.zza
  {
    synchronized (this.d)
    {
      this.i = b(paramLong);
      this.g = this.i.a(this.j.a);
      switch (this.g.a)
      {
      default: 
        throw new zzgq.zza("Unexpected mediation result: " + this.g.a, 0);
      }
    }
    throw new zzgq.zza("No fill from any mediation ad networks.", 3);
    if ((this.g.b != null) && (this.g.b.k != null)) {
      c();
    }
  }
  
  zzem b(long paramLong)
  {
    if (this.j.j != -1) {
      return new zzeu(this.b, this.e.a, this.h, this.j, this.f.t, this.f.C, paramLong, ((Long)zzbt.ay.c()).longValue(), 2);
    }
    return new zzev(this.b, this.e.a, this.h, this.j, this.f.t, this.f.C, paramLong, ((Long)zzbt.ay.c()).longValue(), this.k);
  }
  
  public void b()
  {
    synchronized (this.d)
    {
      super.b();
      if (this.i != null) {
        this.i.cancel();
      }
      return;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzgu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */