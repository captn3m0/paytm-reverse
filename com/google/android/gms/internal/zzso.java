package com.google.android.gms.internal;

import java.io.IOException;

public abstract class zzso<M extends zzso<M>>
  extends zzsu
{
  protected zzsq r;
  
  public final <T> T a(zzsp<M, T> paramzzsp)
  {
    if (this.r == null) {}
    zzsr localzzsr;
    do
    {
      return null;
      localzzsr = this.r.a(zzsx.b(paramzzsp.c));
    } while (localzzsr == null);
    return (T)localzzsr.a(paramzzsp);
  }
  
  public void a(zzsn paramzzsn)
    throws IOException
  {
    if (this.r == null) {}
    for (;;)
    {
      return;
      int i = 0;
      while (i < this.r.a())
      {
        this.r.b(i).a(paramzzsn);
        i += 1;
      }
    }
  }
  
  protected final boolean a(zzsm paramzzsm, int paramInt)
    throws IOException
  {
    int i = paramzzsm.s();
    if (!paramzzsm.b(paramInt)) {
      return false;
    }
    int j = zzsx.b(paramInt);
    zzsw localzzsw = new zzsw(paramInt, paramzzsm.a(i, paramzzsm.s() - i));
    paramzzsm = null;
    if (this.r == null) {
      this.r = new zzsq();
    }
    for (;;)
    {
      Object localObject = paramzzsm;
      if (paramzzsm == null)
      {
        localObject = new zzsr();
        this.r.a(j, (zzsr)localObject);
      }
      ((zzsr)localObject).a(localzzsw);
      return true;
      paramzzsm = this.r.a(j);
    }
  }
  
  protected int b()
  {
    int j = 0;
    if (this.r != null)
    {
      int i = 0;
      for (;;)
      {
        k = i;
        if (j >= this.r.a()) {
          break;
        }
        i += this.r.b(j).a();
        j += 1;
      }
    }
    int k = 0;
    return k;
  }
  
  public M d()
    throws CloneNotSupportedException
  {
    zzso localzzso = (zzso)super.e();
    zzss.a(this, localzzso);
    return localzzso;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzso.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */