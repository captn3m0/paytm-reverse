package com.google.android.gms.internal;

import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zza;
import com.google.android.gms.search.GoogleNowAuthState;
import com.google.android.gms.search.SearchAuthApi;
import com.google.android.gms.search.SearchAuthApi.GoogleNowAuthResult;

public class zzrk
  implements SearchAuthApi
{
  static abstract class zza
    extends zzrh.zza
  {
    public void a(Status paramStatus)
    {
      throw new UnsupportedOperationException();
    }
    
    public void a(Status paramStatus, GoogleNowAuthState paramGoogleNowAuthState)
    {
      throw new UnsupportedOperationException();
    }
  }
  
  static class zzb
    extends zza.zza<Status, zzrj>
  {
    private final String a;
    private final String b;
    private final boolean c;
    
    protected Status a(Status paramStatus)
    {
      if (this.c) {
        Log.d("SearchAuth", "ClearTokenImpl received failure: " + paramStatus.b());
      }
      return paramStatus;
    }
    
    protected void a(zzrj paramzzrj)
      throws RemoteException
    {
      if (this.c) {
        Log.d("SearchAuth", "ClearTokenImpl started");
      }
      zzrk.zza local1 = new zzrk.zza()
      {
        public void a(Status paramAnonymousStatus)
        {
          if (zzrk.zzb.a(zzrk.zzb.this)) {
            Log.d("SearchAuth", "ClearTokenImpl success");
          }
          zzrk.zzb.this.zza(paramAnonymousStatus);
        }
      };
      ((zzri)paramzzrj.v()).b(local1, this.a, this.b);
    }
  }
  
  static class zzc
    extends zza.zza<SearchAuthApi.GoogleNowAuthResult, zzrj>
  {
    private final String a;
    private final String b;
    private final boolean c;
    
    protected SearchAuthApi.GoogleNowAuthResult a(Status paramStatus)
    {
      if (this.c) {
        Log.d("SearchAuth", "GetGoogleNowAuthImpl received failure: " + paramStatus.b());
      }
      return new zzrk.zzd(paramStatus, null);
    }
    
    protected void a(zzrj paramzzrj)
      throws RemoteException
    {
      if (this.c) {
        Log.d("SearchAuth", "GetGoogleNowAuthImpl started");
      }
      zzrk.zza local1 = new zzrk.zza()
      {
        public void a(Status paramAnonymousStatus, GoogleNowAuthState paramAnonymousGoogleNowAuthState)
        {
          if (zzrk.zzc.a(zzrk.zzc.this)) {
            Log.d("SearchAuth", "GetGoogleNowAuthImpl success");
          }
          zzrk.zzc.this.zza(new zzrk.zzd(paramAnonymousStatus, paramAnonymousGoogleNowAuthState));
        }
      };
      ((zzri)paramzzrj.v()).a(local1, this.b, this.a);
    }
  }
  
  static class zzd
    implements SearchAuthApi.GoogleNowAuthResult
  {
    private final Status a;
    private final GoogleNowAuthState b;
    
    zzd(Status paramStatus, GoogleNowAuthState paramGoogleNowAuthState)
    {
      this.a = paramStatus;
      this.b = paramGoogleNowAuthState;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzrk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */