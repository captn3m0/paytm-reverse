package com.google.android.gms.internal;

import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;

@zzhb
public class zzjk
{
  public static void a(View paramView, ViewTreeObserver.OnGlobalLayoutListener paramOnGlobalLayoutListener)
  {
    new zzjl(paramView, paramOnGlobalLayoutListener).a();
  }
  
  public static void a(View paramView, ViewTreeObserver.OnScrollChangedListener paramOnScrollChangedListener)
  {
    new zzjm(paramView, paramOnScrollChangedListener).a();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzjk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */