package com.google.android.gms.internal;

import android.support.v4.util.ArrayMap;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class zzmm<E>
  extends AbstractSet<E>
{
  private final ArrayMap<E, E> a;
  
  public zzmm()
  {
    this.a = new ArrayMap();
  }
  
  public zzmm(int paramInt)
  {
    this.a = new ArrayMap(paramInt);
  }
  
  public zzmm(Collection<E> paramCollection)
  {
    this(paramCollection.size());
    addAll(paramCollection);
  }
  
  public boolean a(zzmm<? extends E> paramzzmm)
  {
    int i = size();
    this.a.a(paramzzmm.a);
    return size() > i;
  }
  
  public boolean add(E paramE)
  {
    if (this.a.containsKey(paramE)) {
      return false;
    }
    this.a.put(paramE, paramE);
    return true;
  }
  
  public boolean addAll(Collection<? extends E> paramCollection)
  {
    if ((paramCollection instanceof zzmm)) {
      return a((zzmm)paramCollection);
    }
    return super.addAll(paramCollection);
  }
  
  public void clear()
  {
    this.a.clear();
  }
  
  public boolean contains(Object paramObject)
  {
    return this.a.containsKey(paramObject);
  }
  
  public Iterator<E> iterator()
  {
    return this.a.keySet().iterator();
  }
  
  public boolean remove(Object paramObject)
  {
    if (!this.a.containsKey(paramObject)) {
      return false;
    }
    this.a.remove(paramObject);
    return true;
  }
  
  public int size()
  {
    return this.a.size();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzmm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */