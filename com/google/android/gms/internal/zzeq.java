package com.google.android.gms.internal;

@zzhb
public final class zzeq
  extends zzez.zza
{
  private final Object a = new Object();
  private zzes.zza b;
  private zzep c;
  
  public void a()
  {
    synchronized (this.a)
    {
      if (this.c != null) {
        this.c.t();
      }
      return;
    }
  }
  
  public void a(int paramInt)
  {
    for (;;)
    {
      synchronized (this.a)
      {
        if (this.b != null)
        {
          if (paramInt == 3)
          {
            paramInt = 1;
            this.b.a(paramInt);
            this.b = null;
          }
        }
        else {
          return;
        }
      }
      paramInt = 2;
    }
  }
  
  public void a(zzep paramzzep)
  {
    synchronized (this.a)
    {
      this.c = paramzzep;
      return;
    }
  }
  
  public void a(zzes.zza paramzza)
  {
    synchronized (this.a)
    {
      this.b = paramzza;
      return;
    }
  }
  
  public void a(zzfa paramzzfa)
  {
    synchronized (this.a)
    {
      if (this.b != null)
      {
        this.b.a(0, paramzzfa);
        this.b = null;
        return;
      }
      if (this.c != null) {
        this.c.x();
      }
      return;
    }
  }
  
  public void b()
  {
    synchronized (this.a)
    {
      if (this.c != null) {
        this.c.u();
      }
      return;
    }
  }
  
  public void c()
  {
    synchronized (this.a)
    {
      if (this.c != null) {
        this.c.v();
      }
      return;
    }
  }
  
  public void d()
  {
    synchronized (this.a)
    {
      if (this.c != null) {
        this.c.w();
      }
      return;
    }
  }
  
  public void e()
  {
    synchronized (this.a)
    {
      if (this.b != null)
      {
        this.b.a(0);
        this.b = null;
        return;
      }
      if (this.c != null) {
        this.c.x();
      }
      return;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzeq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */