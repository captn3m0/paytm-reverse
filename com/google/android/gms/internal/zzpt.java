package com.google.android.gms.internal;

import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.google.android.gms.analytics.ecommerce.Promotion;
import com.google.android.gms.measurement.zze;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class zzpt
  extends zze<zzpt>
{
  private final List<Product> a = new ArrayList();
  private final List<Promotion> b = new ArrayList();
  private final Map<String, List<Product>> c = new HashMap();
  private ProductAction d;
  
  public ProductAction a()
  {
    return this.d;
  }
  
  public void a(Product paramProduct, String paramString)
  {
    if (paramProduct == null) {
      return;
    }
    String str = paramString;
    if (paramString == null) {
      str = "";
    }
    if (!this.c.containsKey(str)) {
      this.c.put(str, new ArrayList());
    }
    ((List)this.c.get(str)).add(paramProduct);
  }
  
  public void a(zzpt paramzzpt)
  {
    paramzzpt.a.addAll(this.a);
    paramzzpt.b.addAll(this.b);
    Iterator localIterator = this.c.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Object localObject = (Map.Entry)localIterator.next();
      String str = (String)((Map.Entry)localObject).getKey();
      localObject = ((List)((Map.Entry)localObject).getValue()).iterator();
      while (((Iterator)localObject).hasNext()) {
        paramzzpt.a((Product)((Iterator)localObject).next(), str);
      }
    }
    if (this.d != null) {
      paramzzpt.d = this.d;
    }
  }
  
  public List<Product> b()
  {
    return Collections.unmodifiableList(this.a);
  }
  
  public Map<String, List<Product>> c()
  {
    return this.c;
  }
  
  public List<Promotion> d()
  {
    return Collections.unmodifiableList(this.b);
  }
  
  public String toString()
  {
    HashMap localHashMap = new HashMap();
    if (!this.a.isEmpty()) {
      localHashMap.put("products", this.a);
    }
    if (!this.b.isEmpty()) {
      localHashMap.put("promotions", this.b);
    }
    if (!this.c.isEmpty()) {
      localHashMap.put("impressions", this.c);
    }
    localHashMap.put("productAction", this.d);
    return a(localHashMap);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzpt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */