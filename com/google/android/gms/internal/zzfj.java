package com.google.android.gms.internal;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.ads.mediation.c;
import com.google.ads.mediation.d;
import com.google.ads.mediation.f;
import com.google.ads.mediation.h;
import com.google.ads.mediation.i;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

@zzhb
public final class zzfj<NETWORK_EXTRAS extends i, SERVER_PARAMETERS extends h>
  extends zzey.zza
{
  private final c<NETWORK_EXTRAS, SERVER_PARAMETERS> a;
  private final NETWORK_EXTRAS b;
  
  public zzfj(c<NETWORK_EXTRAS, SERVER_PARAMETERS> paramc, NETWORK_EXTRAS paramNETWORK_EXTRAS)
  {
    this.a = paramc;
    this.b = paramNETWORK_EXTRAS;
  }
  
  private SERVER_PARAMETERS a(String paramString1, int paramInt, String paramString2)
    throws RemoteException
  {
    if (paramString1 != null) {
      try
      {
        localObject = new JSONObject(paramString1);
        paramString2 = new HashMap(((JSONObject)localObject).length());
        Iterator localIterator = ((JSONObject)localObject).keys();
        for (;;)
        {
          paramString1 = paramString2;
          if (!localIterator.hasNext()) {
            break;
          }
          paramString1 = (String)localIterator.next();
          paramString2.put(paramString1, ((JSONObject)localObject).getString(paramString1));
        }
        paramString1 = new HashMap(0);
      }
      catch (Throwable paramString1)
      {
        zzb.d("Could not get MediationServerParameters.", paramString1);
        throw new RemoteException();
      }
    }
    Object localObject = this.a.c();
    paramString2 = null;
    if (localObject != null)
    {
      paramString2 = (h)((Class)localObject).newInstance();
      paramString2.a(paramString1);
    }
    return paramString2;
  }
  
  public zzd a()
    throws RemoteException
  {
    if (!(this.a instanceof d))
    {
      zzb.d("MediationAdapter is not a MediationBannerAdapter: " + this.a.getClass().getCanonicalName());
      throw new RemoteException();
    }
    try
    {
      zzd localzzd = zze.a(((d)this.a).d());
      return localzzd;
    }
    catch (Throwable localThrowable)
    {
      zzb.d("Could not get banner view from adapter.", localThrowable);
      throw new RemoteException();
    }
  }
  
  public void a(AdRequestParcel paramAdRequestParcel, String paramString) {}
  
  public void a(AdRequestParcel paramAdRequestParcel, String paramString1, String paramString2) {}
  
  public void a(zzd paramzzd, AdRequestParcel paramAdRequestParcel, String paramString1, zza paramzza, String paramString2)
    throws RemoteException
  {}
  
  public void a(zzd paramzzd, AdRequestParcel paramAdRequestParcel, String paramString, zzez paramzzez)
    throws RemoteException
  {
    a(paramzzd, paramAdRequestParcel, paramString, null, paramzzez);
  }
  
  public void a(zzd paramzzd, AdRequestParcel paramAdRequestParcel, String paramString1, String paramString2, zzez paramzzez)
    throws RemoteException
  {
    if (!(this.a instanceof f))
    {
      zzb.d("MediationAdapter is not a MediationInterstitialAdapter: " + this.a.getClass().getCanonicalName());
      throw new RemoteException();
    }
    zzb.a("Requesting interstitial ad from adapter.");
    try
    {
      ((f)this.a).a(new zzfk(paramzzez), (Activity)zze.a(paramzzd), a(paramString1, paramAdRequestParcel.g, paramString2), zzfl.a(paramAdRequestParcel), this.b);
      return;
    }
    catch (Throwable paramzzd)
    {
      zzb.d("Could not request interstitial ad from adapter.", paramzzd);
      throw new RemoteException();
    }
  }
  
  public void a(zzd paramzzd, AdRequestParcel paramAdRequestParcel, String paramString1, String paramString2, zzez paramzzez, NativeAdOptionsParcel paramNativeAdOptionsParcel, List<String> paramList) {}
  
  public void a(zzd paramzzd, AdSizeParcel paramAdSizeParcel, AdRequestParcel paramAdRequestParcel, String paramString, zzez paramzzez)
    throws RemoteException
  {
    a(paramzzd, paramAdSizeParcel, paramAdRequestParcel, paramString, null, paramzzez);
  }
  
  public void a(zzd paramzzd, AdSizeParcel paramAdSizeParcel, AdRequestParcel paramAdRequestParcel, String paramString1, String paramString2, zzez paramzzez)
    throws RemoteException
  {
    if (!(this.a instanceof d))
    {
      zzb.d("MediationAdapter is not a MediationBannerAdapter: " + this.a.getClass().getCanonicalName());
      throw new RemoteException();
    }
    zzb.a("Requesting banner ad from adapter.");
    try
    {
      ((d)this.a).a(new zzfk(paramzzez), (Activity)zze.a(paramzzd), a(paramString1, paramAdRequestParcel.g, paramString2), zzfl.a(paramAdSizeParcel), zzfl.a(paramAdRequestParcel), this.b);
      return;
    }
    catch (Throwable paramzzd)
    {
      zzb.d("Could not request banner ad from adapter.", paramzzd);
      throw new RemoteException();
    }
  }
  
  public void b()
    throws RemoteException
  {
    if (!(this.a instanceof f))
    {
      zzb.d("MediationAdapter is not a MediationInterstitialAdapter: " + this.a.getClass().getCanonicalName());
      throw new RemoteException();
    }
    zzb.a("Showing interstitial from adapter.");
    try
    {
      ((f)this.a).e();
      return;
    }
    catch (Throwable localThrowable)
    {
      zzb.d("Could not show interstitial from adapter.", localThrowable);
      throw new RemoteException();
    }
  }
  
  public void c()
    throws RemoteException
  {
    try
    {
      this.a.a();
      return;
    }
    catch (Throwable localThrowable)
    {
      zzb.d("Could not destroy adapter.", localThrowable);
      throw new RemoteException();
    }
  }
  
  public void d()
    throws RemoteException
  {
    throw new RemoteException();
  }
  
  public void e()
    throws RemoteException
  {
    throw new RemoteException();
  }
  
  public void f() {}
  
  public boolean g()
  {
    return true;
  }
  
  public zzfb h()
  {
    return null;
  }
  
  public zzfc i()
  {
    return null;
  }
  
  public Bundle j()
  {
    return new Bundle();
  }
  
  public Bundle k()
  {
    return new Bundle();
  }
  
  public Bundle l()
  {
    return new Bundle();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzfj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */