package com.google.android.gms.internal;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.RemoteException;
import com.google.android.gms.appdatasearch.UsageInfo;
import com.google.android.gms.appdatasearch.zza;
import com.google.android.gms.appdatasearch.zzk;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndexApi;
import com.google.android.gms.appindexing.AppIndexApi.ActionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zza;
import com.google.android.gms.common.api.internal.zza.zzb;
import java.util.List;

public final class zzkk
  implements zzk, AppIndexApi
{
  public static Intent a(String paramString, Uri paramUri)
  {
    b(paramString, paramUri);
    if (b(paramUri)) {
      return new Intent("android.intent.action.VIEW", paramUri);
    }
    if (c(paramUri)) {
      return new Intent("android.intent.action.VIEW", a(paramUri));
    }
    throw new RuntimeException("appIndexingUri is neither an HTTP(S) URL nor an \"android-app://\" URL: " + paramUri);
  }
  
  private static Uri a(Uri paramUri)
  {
    List localList = paramUri.getPathSegments();
    String str = (String)localList.get(0);
    Uri.Builder localBuilder = new Uri.Builder();
    localBuilder.scheme(str);
    if (localList.size() > 1)
    {
      localBuilder.authority((String)localList.get(1));
      int i = 2;
      while (i < localList.size())
      {
        localBuilder.appendPath((String)localList.get(i));
        i += 1;
      }
    }
    localBuilder.encodedQuery(paramUri.getEncodedQuery());
    localBuilder.encodedFragment(paramUri.getEncodedFragment());
    return localBuilder.build();
  }
  
  private PendingResult<Status> a(GoogleApiClient paramGoogleApiClient, Action paramAction, int paramInt)
  {
    String str = paramGoogleApiClient.b().getPackageName();
    return a(paramGoogleApiClient, new UsageInfo[] { zzkj.a(paramAction, System.currentTimeMillis(), str, paramInt) });
  }
  
  private static void b(String paramString, Uri paramUri)
  {
    if (b(paramUri))
    {
      if (paramUri.getHost().isEmpty()) {
        throw new IllegalArgumentException("AppIndex: The web URL must have a host (follow the format http(s)://<host>/[path]). Provided URI: " + paramUri);
      }
    }
    else if (c(paramUri))
    {
      if ((paramString != null) && (!paramString.equals(paramUri.getHost()))) {
        throw new IllegalArgumentException("AppIndex: The android-app URI host must match the package name and follow the format android-app://<package_name>/<scheme>/[host_path]. Provided URI: " + paramUri);
      }
      paramString = paramUri.getPathSegments();
      if ((paramString.isEmpty()) || (((String)paramString.get(0)).isEmpty())) {
        throw new IllegalArgumentException("AppIndex: The app URI scheme must exist and follow the format android-app://<package_name>/<scheme>/[host_path]). Provided URI: " + paramUri);
      }
    }
    else
    {
      throw new IllegalArgumentException("AppIndex: The URI scheme must either be 'http(s)' or 'android-app'. If the latter, it must follow the format 'android-app://<package_name>/<scheme>/[host_path]'. Provided URI: " + paramUri);
    }
  }
  
  private static boolean b(Uri paramUri)
  {
    paramUri = paramUri.getScheme();
    return ("http".equals(paramUri)) || ("https".equals(paramUri));
  }
  
  private static boolean c(Uri paramUri)
  {
    return "android-app".equals(paramUri.getScheme());
  }
  
  public PendingResult<Status> a(GoogleApiClient paramGoogleApiClient, Action paramAction)
  {
    return a(paramGoogleApiClient, paramAction, 1);
  }
  
  public PendingResult<Status> a(GoogleApiClient paramGoogleApiClient, final UsageInfo... paramVarArgs)
  {
    paramGoogleApiClient.a(new zzc(paramGoogleApiClient)
    {
      protected void a(zzkf paramAnonymouszzkf)
        throws RemoteException
      {
        paramAnonymouszzkf.a(new zzkk.zzd(this), this.a, paramVarArgs);
      }
    });
  }
  
  public PendingResult<Status> b(GoogleApiClient paramGoogleApiClient, Action paramAction)
  {
    return a(paramGoogleApiClient, paramAction, 2);
  }
  
  @Deprecated
  private static final class zza
    implements AppIndexApi.ActionResult
  {}
  
  private static abstract class zzb<T extends Result>
    extends zza.zza<T, zzki>
  {
    public zzb(GoogleApiClient paramGoogleApiClient)
    {
      super(paramGoogleApiClient);
    }
    
    protected abstract void a(zzkf paramzzkf)
      throws RemoteException;
    
    protected final void a(zzki paramzzki)
      throws RemoteException
    {
      a(paramzzki.h());
    }
  }
  
  private static abstract class zzc<T extends Result>
    extends zzkk.zzb<Status>
  {
    zzc(GoogleApiClient paramGoogleApiClient)
    {
      super();
    }
    
    protected Status a(Status paramStatus)
    {
      return paramStatus;
    }
  }
  
  private static final class zzd
    extends zzkh<Status>
  {
    public zzd(zza.zzb<Status> paramzzb)
    {
      super();
    }
    
    public void a(Status paramStatus)
    {
      this.a.a(paramStatus);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzkk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */