package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.reward.client.RewardedVideoAdRequestParcel;
import com.google.android.gms.ads.internal.reward.client.zzb.zza;
import com.google.android.gms.ads.internal.reward.client.zzd;

@zzhb
public class zzhs
  extends zzb.zza
{
  private final zzht a;
  private final Object b;
  
  public void a()
  {
    synchronized (this.b)
    {
      this.a.z();
      return;
    }
  }
  
  public void a(RewardedVideoAdRequestParcel paramRewardedVideoAdRequestParcel)
  {
    synchronized (this.b)
    {
      this.a.a(paramRewardedVideoAdRequestParcel);
      return;
    }
  }
  
  public void a(zzd paramzzd)
  {
    synchronized (this.b)
    {
      this.a.a(paramzzd);
      return;
    }
  }
  
  public void a(String paramString)
  {
    synchronized (this.b)
    {
      this.a.a(paramString);
      return;
    }
  }
  
  public boolean b()
  {
    synchronized (this.b)
    {
      boolean bool = this.a.A();
      return bool;
    }
  }
  
  public void c()
  {
    synchronized (this.b)
    {
      this.a.d();
      return;
    }
  }
  
  public void d()
  {
    synchronized (this.b)
    {
      this.a.b_();
      return;
    }
  }
  
  public void e()
  {
    synchronized (this.b)
    {
      this.a.b();
      return;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzhs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */