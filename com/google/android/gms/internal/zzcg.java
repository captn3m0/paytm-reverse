package com.google.android.gms.internal;

import com.google.android.gms.ads.doubleclick.OnCustomRenderedAdLoadedListener;

@zzhb
public final class zzcg
  extends zzcf.zza
{
  private final OnCustomRenderedAdLoadedListener a;
  
  public zzcg(OnCustomRenderedAdLoadedListener paramOnCustomRenderedAdLoadedListener)
  {
    this.a = paramOnCustomRenderedAdLoadedListener;
  }
  
  public void a(zzce paramzzce)
  {
    this.a.a(new zzcd(paramzzce));
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzcg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */