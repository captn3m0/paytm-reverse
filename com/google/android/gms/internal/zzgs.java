package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.zzr;

@zzhb
public class zzgs
  extends zzgn
  implements zzjq.zza
{
  zzgs(Context paramContext, zzif.zza paramzza, zzjp paramzzjp, zzgr.zza paramzza1)
  {
    super(paramContext, paramzza, paramzzjp, paramzza1);
  }
  
  protected void b()
  {
    if (this.e.e != -2) {
      return;
    }
    this.c.l().a(this);
    e();
    zzin.a("Loading HTML in WebView.");
    this.c.loadDataWithBaseURL(zzr.e().a(this.e.b), this.e.c, "text/html", "UTF-8", null);
  }
  
  protected void e() {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzgs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */