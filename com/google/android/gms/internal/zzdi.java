package com.google.android.gms.internal;

import java.util.Map;

@zzhb
public class zzdi
  implements zzdf
{
  private final zzdj a;
  
  public zzdi(zzdj paramzzdj)
  {
    this.a = paramzzdj;
  }
  
  public void a(zzjp paramzzjp, Map<String, String> paramMap)
  {
    boolean bool1 = "1".equals(paramMap.get("transparentBackground"));
    boolean bool2 = "1".equals(paramMap.get("blur"));
    try
    {
      if (paramMap.get("blurRadius") != null)
      {
        f = Float.parseFloat((String)paramMap.get("blurRadius"));
        this.a.b(bool1);
        this.a.a(bool2, f);
        return;
      }
    }
    catch (NumberFormatException paramzzjp)
    {
      for (;;)
      {
        zzin.b("Fail to parse float", paramzzjp);
        float f = 0.0F;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzdi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */