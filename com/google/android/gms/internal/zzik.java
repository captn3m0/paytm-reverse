package com.google.android.gms.internal;

import android.os.Bundle;
import com.google.android.gms.ads.internal.zzr;

@zzhb
public class zzik
{
  private final Object a = new Object();
  private int b;
  private int c;
  private final zzih d;
  private final String e;
  
  zzik(zzih paramzzih, String paramString)
  {
    this.d = paramzzih;
    this.e = paramString;
  }
  
  public zzik(String paramString)
  {
    this(zzr.h(), paramString);
  }
  
  public Bundle a()
  {
    synchronized (this.a)
    {
      Bundle localBundle = new Bundle();
      localBundle.putInt("pmnli", this.b);
      localBundle.putInt("pmnll", this.c);
      return localBundle;
    }
  }
  
  public void a(int paramInt1, int paramInt2)
  {
    synchronized (this.a)
    {
      this.b = paramInt1;
      this.c = paramInt2;
      this.d.a(this.e, this);
      return;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzik.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */