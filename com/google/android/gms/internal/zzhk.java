package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zzr;
import java.util.WeakHashMap;

@zzhb
public final class zzhk
{
  private WeakHashMap<Context, zza> a = new WeakHashMap();
  
  public zzhj a(Context paramContext)
  {
    Object localObject = (zza)this.a.get(paramContext);
    if ((localObject != null) && (!((zza)localObject).a()) && (((Boolean)zzbt.al.c()).booleanValue())) {}
    for (localObject = new zzhj.zza(paramContext, ((zza)localObject).b).a();; localObject = new zzhj.zza(paramContext).a())
    {
      this.a.put(paramContext, new zza((zzhj)localObject));
      return (zzhj)localObject;
    }
  }
  
  private class zza
  {
    public final long a = zzr.i().a();
    public final zzhj b;
    
    public zza(zzhj paramzzhj)
    {
      this.b = paramzzhj;
    }
    
    public boolean a()
    {
      long l = this.a;
      return ((Long)zzbt.am.c()).longValue() + l < zzr.i().a();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzhk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */