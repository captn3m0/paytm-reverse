package com.google.android.gms.internal;

import java.io.IOException;

public abstract interface zzqa
{
  public static final class zza
    extends zzsu
  {
    private static volatile zza[] c;
    public String a;
    public Boolean b;
    
    public zza()
    {
      c();
    }
    
    public static zza[] a()
    {
      if (c == null) {}
      synchronized (zzss.a)
      {
        if (c == null) {
          c = new zza[0];
        }
        return c;
      }
    }
    
    public zza a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (zzsx.a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 10: 
          this.a = paramzzsm.i();
          break;
        case 16: 
          this.b = Boolean.valueOf(paramzzsm.h());
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (this.a != null) {
        paramzzsn.a(1, this.a);
      }
      if (this.b != null) {
        paramzzsn.a(2, this.b.booleanValue());
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b();
      int i = j;
      if (this.a != null) {
        i = j + zzsn.b(1, this.a);
      }
      j = i;
      if (this.b != null) {
        j = i + zzsn.b(2, this.b.booleanValue());
      }
      return j;
    }
    
    public zza c()
    {
      this.a = null;
      this.b = null;
      this.S = -1;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      if (paramObject == this) {}
      do
      {
        do
        {
          return true;
          if (!(paramObject instanceof zza)) {
            return false;
          }
          paramObject = (zza)paramObject;
          if (this.a == null)
          {
            if (((zza)paramObject).a != null) {
              return false;
            }
          }
          else if (!this.a.equals(((zza)paramObject).a)) {
            return false;
          }
          if (this.b != null) {
            break;
          }
        } while (((zza)paramObject).b == null);
        return false;
      } while (this.b.equals(((zza)paramObject).b));
      return false;
    }
    
    public int hashCode()
    {
      int j = 0;
      int k = getClass().getName().hashCode();
      int i;
      if (this.a == null)
      {
        i = 0;
        if (this.b != null) {
          break label56;
        }
      }
      for (;;)
      {
        return (i + (k + 527) * 31) * 31 + j;
        i = this.a.hashCode();
        break;
        label56:
        j = this.b.hashCode();
      }
    }
  }
  
  public static final class zzb
    extends zzsu
  {
    public Long a;
    public String b;
    public Integer c;
    public zzqa.zzc[] d;
    public zzqa.zza[] e;
    public zzpz.zza[] f;
    
    public zzb()
    {
      a();
    }
    
    public zzb a()
    {
      this.a = null;
      this.b = null;
      this.c = null;
      this.d = zzqa.zzc.a();
      this.e = zzqa.zza.a();
      this.f = zzpz.zza.a();
      this.S = -1;
      return this;
    }
    
    public zzb a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        int j;
        Object localObject;
        switch (i)
        {
        default: 
          if (zzsx.a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 8: 
          this.a = Long.valueOf(paramzzsm.f());
          break;
        case 18: 
          this.b = paramzzsm.i();
          break;
        case 24: 
          this.c = Integer.valueOf(paramzzsm.g());
          break;
        case 34: 
          j = zzsx.b(paramzzsm, 34);
          if (this.d == null) {}
          for (i = 0;; i = this.d.length)
          {
            localObject = new zzqa.zzc[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.d, 0, localObject, 0, i);
              j = i;
            }
            while (j < localObject.length - 1)
            {
              localObject[j] = new zzqa.zzc();
              paramzzsm.a(localObject[j]);
              paramzzsm.a();
              j += 1;
            }
          }
          localObject[j] = new zzqa.zzc();
          paramzzsm.a(localObject[j]);
          this.d = ((zzqa.zzc[])localObject);
          break;
        case 42: 
          j = zzsx.b(paramzzsm, 42);
          if (this.e == null) {}
          for (i = 0;; i = this.e.length)
          {
            localObject = new zzqa.zza[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.e, 0, localObject, 0, i);
              j = i;
            }
            while (j < localObject.length - 1)
            {
              localObject[j] = new zzqa.zza();
              paramzzsm.a(localObject[j]);
              paramzzsm.a();
              j += 1;
            }
          }
          localObject[j] = new zzqa.zza();
          paramzzsm.a(localObject[j]);
          this.e = ((zzqa.zza[])localObject);
          break;
        case 50: 
          j = zzsx.b(paramzzsm, 50);
          if (this.f == null) {}
          for (i = 0;; i = this.f.length)
          {
            localObject = new zzpz.zza[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.f, 0, localObject, 0, i);
              j = i;
            }
            while (j < localObject.length - 1)
            {
              localObject[j] = new zzpz.zza();
              paramzzsm.a(localObject[j]);
              paramzzsm.a();
              j += 1;
            }
          }
          localObject[j] = new zzpz.zza();
          paramzzsm.a(localObject[j]);
          this.f = ((zzpz.zza[])localObject);
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      int j = 0;
      if (this.a != null) {
        paramzzsn.b(1, this.a.longValue());
      }
      if (this.b != null) {
        paramzzsn.a(2, this.b);
      }
      if (this.c != null) {
        paramzzsn.a(3, this.c.intValue());
      }
      int i;
      Object localObject;
      if ((this.d != null) && (this.d.length > 0))
      {
        i = 0;
        while (i < this.d.length)
        {
          localObject = this.d[i];
          if (localObject != null) {
            paramzzsn.a(4, (zzsu)localObject);
          }
          i += 1;
        }
      }
      if ((this.e != null) && (this.e.length > 0))
      {
        i = 0;
        while (i < this.e.length)
        {
          localObject = this.e[i];
          if (localObject != null) {
            paramzzsn.a(5, (zzsu)localObject);
          }
          i += 1;
        }
      }
      if ((this.f != null) && (this.f.length > 0))
      {
        i = j;
        while (i < this.f.length)
        {
          localObject = this.f[i];
          if (localObject != null) {
            paramzzsn.a(6, (zzsu)localObject);
          }
          i += 1;
        }
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int m = 0;
      int j = super.b();
      int i = j;
      if (this.a != null) {
        i = j + zzsn.d(1, this.a.longValue());
      }
      j = i;
      if (this.b != null) {
        j = i + zzsn.b(2, this.b);
      }
      i = j;
      if (this.c != null) {
        i = j + zzsn.c(3, this.c.intValue());
      }
      j = i;
      Object localObject;
      if (this.d != null)
      {
        j = i;
        if (this.d.length > 0)
        {
          j = 0;
          while (j < this.d.length)
          {
            localObject = this.d[j];
            k = i;
            if (localObject != null) {
              k = i + zzsn.c(4, (zzsu)localObject);
            }
            j += 1;
            i = k;
          }
          j = i;
        }
      }
      i = j;
      if (this.e != null)
      {
        i = j;
        if (this.e.length > 0)
        {
          i = j;
          j = 0;
          while (j < this.e.length)
          {
            localObject = this.e[j];
            k = i;
            if (localObject != null) {
              k = i + zzsn.c(5, (zzsu)localObject);
            }
            j += 1;
            i = k;
          }
        }
      }
      int k = i;
      if (this.f != null)
      {
        k = i;
        if (this.f.length > 0)
        {
          j = m;
          for (;;)
          {
            k = i;
            if (j >= this.f.length) {
              break;
            }
            localObject = this.f[j];
            k = i;
            if (localObject != null) {
              k = i + zzsn.c(6, (zzsu)localObject);
            }
            j += 1;
            i = k;
          }
        }
      }
      return k;
    }
    
    public boolean equals(Object paramObject)
    {
      if (paramObject == this) {}
      do
      {
        return true;
        if (!(paramObject instanceof zzb)) {
          return false;
        }
        paramObject = (zzb)paramObject;
        if (this.a == null)
        {
          if (((zzb)paramObject).a != null) {
            return false;
          }
        }
        else if (!this.a.equals(((zzb)paramObject).a)) {
          return false;
        }
        if (this.b == null)
        {
          if (((zzb)paramObject).b != null) {
            return false;
          }
        }
        else if (!this.b.equals(((zzb)paramObject).b)) {
          return false;
        }
        if (this.c == null)
        {
          if (((zzb)paramObject).c != null) {
            return false;
          }
        }
        else if (!this.c.equals(((zzb)paramObject).c)) {
          return false;
        }
        if (!zzss.a(this.d, ((zzb)paramObject).d)) {
          return false;
        }
        if (!zzss.a(this.e, ((zzb)paramObject).e)) {
          return false;
        }
      } while (zzss.a(this.f, ((zzb)paramObject).f));
      return false;
    }
    
    public int hashCode()
    {
      int k = 0;
      int m = getClass().getName().hashCode();
      int i;
      int j;
      if (this.a == null)
      {
        i = 0;
        if (this.b != null) {
          break label105;
        }
        j = 0;
        label32:
        if (this.c != null) {
          break label116;
        }
      }
      for (;;)
      {
        return ((((j + (i + (m + 527) * 31) * 31) * 31 + k) * 31 + zzss.a(this.d)) * 31 + zzss.a(this.e)) * 31 + zzss.a(this.f);
        i = this.a.hashCode();
        break;
        label105:
        j = this.b.hashCode();
        break label32;
        label116:
        k = this.c.hashCode();
      }
    }
  }
  
  public static final class zzc
    extends zzsu
  {
    private static volatile zzc[] c;
    public String a;
    public String b;
    
    public zzc()
    {
      c();
    }
    
    public static zzc[] a()
    {
      if (c == null) {}
      synchronized (zzss.a)
      {
        if (c == null) {
          c = new zzc[0];
        }
        return c;
      }
    }
    
    public zzc a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (zzsx.a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 10: 
          this.a = paramzzsm.i();
          break;
        case 18: 
          this.b = paramzzsm.i();
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (this.a != null) {
        paramzzsn.a(1, this.a);
      }
      if (this.b != null) {
        paramzzsn.a(2, this.b);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b();
      int i = j;
      if (this.a != null) {
        i = j + zzsn.b(1, this.a);
      }
      j = i;
      if (this.b != null) {
        j = i + zzsn.b(2, this.b);
      }
      return j;
    }
    
    public zzc c()
    {
      this.a = null;
      this.b = null;
      this.S = -1;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      if (paramObject == this) {}
      do
      {
        do
        {
          return true;
          if (!(paramObject instanceof zzc)) {
            return false;
          }
          paramObject = (zzc)paramObject;
          if (this.a == null)
          {
            if (((zzc)paramObject).a != null) {
              return false;
            }
          }
          else if (!this.a.equals(((zzc)paramObject).a)) {
            return false;
          }
          if (this.b != null) {
            break;
          }
        } while (((zzc)paramObject).b == null);
        return false;
      } while (this.b.equals(((zzc)paramObject).b));
      return false;
    }
    
    public int hashCode()
    {
      int j = 0;
      int k = getClass().getName().hashCode();
      int i;
      if (this.a == null)
      {
        i = 0;
        if (this.b != null) {
          break label56;
        }
      }
      for (;;)
      {
        return (i + (k + 527) * 31) * 31 + j;
        i = this.a.hashCode();
        break;
        label56:
        j = this.b.hashCode();
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzqa.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */