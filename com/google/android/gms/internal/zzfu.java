package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.client.zzl;
import com.google.android.gms.ads.internal.client.zzn;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.dynamic.zzg;
import com.google.android.gms.dynamic.zzg.zza;

@zzhb
public final class zzfu
  extends zzg<zzfw>
{
  private static final zzfu a = new zzfu();
  
  private zzfu()
  {
    super("com.google.android.gms.ads.AdOverlayCreatorImpl");
  }
  
  @Nullable
  public static zzfv a(Activity paramActivity)
  {
    try
    {
      zzfv localzzfv1;
      if (!b(paramActivity))
      {
        zzfv localzzfv2 = a.c(paramActivity);
        localzzfv1 = localzzfv2;
        if (localzzfv2 != null) {}
      }
      else
      {
        zzb.a("Using AdOverlay from the client jar.");
        localzzfv1 = zzn.c().b(paramActivity);
      }
      return localzzfv1;
    }
    catch (zza paramActivity)
    {
      zzb.d(paramActivity.getMessage());
    }
    return null;
  }
  
  private static boolean b(Activity paramActivity)
    throws zzfu.zza
  {
    paramActivity = paramActivity.getIntent();
    if (!paramActivity.hasExtra("com.google.android.gms.ads.internal.overlay.useClientJar")) {
      throw new zza("Ad overlay requires the useClientJar flag in intent extras.");
    }
    return paramActivity.getBooleanExtra("com.google.android.gms.ads.internal.overlay.useClientJar", false);
  }
  
  private zzfv c(Activity paramActivity)
  {
    try
    {
      zzd localzzd = zze.a(paramActivity);
      paramActivity = zzfv.zza.a(((zzfw)a(paramActivity)).a(localzzd));
      return paramActivity;
    }
    catch (RemoteException paramActivity)
    {
      zzb.d("Could not create remote AdOverlay.", paramActivity);
      return null;
    }
    catch (zzg.zza paramActivity)
    {
      zzb.d("Could not create remote AdOverlay.", paramActivity);
    }
    return null;
  }
  
  protected zzfw a(IBinder paramIBinder)
  {
    return zzfw.zza.a(paramIBinder);
  }
  
  private static final class zza
    extends Exception
  {
    public zza(String paramString)
    {
      super();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzfu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */