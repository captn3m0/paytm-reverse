package com.google.android.gms.internal;

import android.content.Context;
import java.util.HashMap;
import java.util.Map;

public class zzrr
{
  Map<String, Object<zzrs.zzc>> a = new HashMap();
  private final Context b;
  private final zzrt c;
  private final zzmq d;
  private String e = null;
  private final Map<String, Object> f;
  
  public zzrr(Context paramContext)
  {
    this(paramContext, new HashMap(), new zzrt(paramContext), zzmt.d());
  }
  
  zzrr(Context paramContext, Map<String, Object> paramMap, zzrt paramzzrt, zzmq paramzzmq)
  {
    this.b = paramContext;
    this.d = paramzzmq;
    this.c = paramzzrt;
    this.f = paramMap;
  }
  
  public void a(String paramString)
  {
    this.e = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzrr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */