package com.google.android.gms.internal;

import com.google.ads.a.a;
import com.google.ads.a.b;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.zza;
import java.util.Date;
import java.util.HashSet;

@zzhb
public final class zzfl
{
  public static int a(a.a parama)
  {
    switch (1.b[parama.ordinal()])
    {
    default: 
      return 0;
    case 2: 
      return 1;
    case 3: 
      return 2;
    }
    return 3;
  }
  
  public static a.b a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return a.b.a;
    case 2: 
      return a.b.c;
    }
    return a.b.b;
  }
  
  public static com.google.ads.b a(AdSizeParcel paramAdSizeParcel)
  {
    int i = 0;
    com.google.ads.b[] arrayOfb = new com.google.ads.b[6];
    arrayOfb[0] = com.google.ads.b.a;
    arrayOfb[1] = com.google.ads.b.b;
    arrayOfb[2] = com.google.ads.b.c;
    arrayOfb[3] = com.google.ads.b.d;
    arrayOfb[4] = com.google.ads.b.e;
    arrayOfb[5] = com.google.ads.b.f;
    while (i < arrayOfb.length)
    {
      if ((arrayOfb[i].a() == paramAdSizeParcel.f) && (arrayOfb[i].b() == paramAdSizeParcel.c)) {
        return arrayOfb[i];
      }
      i += 1;
    }
    return new com.google.ads.b(zza.a(paramAdSizeParcel.f, paramAdSizeParcel.c, paramAdSizeParcel.b));
  }
  
  public static com.google.ads.mediation.b a(AdRequestParcel paramAdRequestParcel)
  {
    if (paramAdRequestParcel.e != null) {}
    for (HashSet localHashSet = new HashSet(paramAdRequestParcel.e);; localHashSet = null) {
      return new com.google.ads.mediation.b(new Date(paramAdRequestParcel.b), a(paramAdRequestParcel.d), localHashSet, paramAdRequestParcel.f, paramAdRequestParcel.k);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzfl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */