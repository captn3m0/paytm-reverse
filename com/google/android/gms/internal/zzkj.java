package com.google.android.gms.internal;

import android.accounts.Account;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.appdatasearch.DocumentContents.zza;
import com.google.android.gms.appdatasearch.DocumentSection;
import com.google.android.gms.appdatasearch.RegisterSectionInfo.zza;
import com.google.android.gms.appdatasearch.UsageInfo;
import com.google.android.gms.appdatasearch.UsageInfo.zza;
import com.google.android.gms.appindexing.Action;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class zzkj
{
  private static DocumentSection a(String paramString, zzpm.zzc paramzzc)
  {
    paramString = new RegisterSectionInfo.zza(paramString).a(true).b(paramString).a("blob").a();
    return new DocumentSection(zzsu.a(paramzzc), paramString);
  }
  
  public static UsageInfo a(Action paramAction, long paramLong, String paramString, int paramInt)
  {
    boolean bool = false;
    Bundle localBundle = new Bundle();
    localBundle.putAll(paramAction.a());
    Object localObject = localBundle.getBundle("object");
    int i;
    if (((Bundle)localObject).containsKey("id"))
    {
      paramAction = Uri.parse(((Bundle)localObject).getString("id"));
      String str1 = ((Bundle)localObject).getString("name");
      String str2 = ((Bundle)localObject).getString("type");
      localObject = zzkk.a(paramString, Uri.parse(((Bundle)localObject).getString("url")));
      paramAction = UsageInfo.a((Intent)localObject, str1, paramAction, str2, null);
      if (localBundle.containsKey(".private:ssbContext"))
      {
        paramAction.a(DocumentSection.a(localBundle.getByteArray(".private:ssbContext")));
        localBundle.remove(".private:ssbContext");
      }
      if (localBundle.containsKey(".private:accountName"))
      {
        paramAction.a(new Account(localBundle.getString(".private:accountName"), "com.google"));
        localBundle.remove(".private:accountName");
      }
      if ((!localBundle.containsKey(".private:isContextOnly")) || (!localBundle.getBoolean(".private:isContextOnly"))) {
        break label290;
      }
      i = 4;
      localBundle.remove(".private:isContextOnly");
    }
    for (;;)
    {
      if (localBundle.containsKey(".private:isDeviceOnly"))
      {
        bool = localBundle.getBoolean(".private:isDeviceOnly", false);
        localBundle.remove(".private:isDeviceOnly");
      }
      paramAction.a(a(".private:action", a(localBundle)));
      return new UsageInfo.zza().a(UsageInfo.a(paramString, (Intent)localObject)).a(paramLong).a(i).a(paramAction.a()).a(bool).b(paramInt).a();
      paramAction = null;
      break;
      label290:
      i = 0;
    }
  }
  
  private static zzpm.zzb a(String paramString, Bundle paramBundle)
  {
    zzpm.zzb localzzb = new zzpm.zzb();
    localzzb.a = paramString;
    localzzb.b = new zzpm.zzd();
    localzzb.b.e = a(paramBundle);
    return localzzb;
  }
  
  private static zzpm.zzb a(String paramString1, String paramString2)
  {
    zzpm.zzb localzzb = new zzpm.zzb();
    localzzb.a = paramString1;
    localzzb.b = new zzpm.zzd();
    localzzb.b.b = paramString2;
    return localzzb;
  }
  
  private static zzpm.zzb a(String paramString, boolean paramBoolean)
  {
    zzpm.zzb localzzb = new zzpm.zzb();
    localzzb.a = paramString;
    localzzb.b = new zzpm.zzd();
    localzzb.b.a = paramBoolean;
    return localzzb;
  }
  
  static zzpm.zzc a(Bundle paramBundle)
  {
    ArrayList localArrayList = new ArrayList();
    Object localObject1 = paramBundle.keySet().iterator();
    while (((Iterator)localObject1).hasNext())
    {
      String str1 = (String)((Iterator)localObject1).next();
      Object localObject2 = paramBundle.get(str1);
      if ((localObject2 instanceof String))
      {
        localArrayList.add(a(str1, (String)localObject2));
      }
      else if ((localObject2 instanceof Bundle))
      {
        localArrayList.add(a(str1, (Bundle)localObject2));
      }
      else
      {
        int j;
        int i;
        label129:
        String str2;
        if ((localObject2 instanceof String[]))
        {
          localObject2 = (String[])localObject2;
          j = localObject2.length;
          i = 0;
          if (i < j)
          {
            str2 = localObject2[i];
            if (str2 != null) {
              break label152;
            }
          }
          for (;;)
          {
            i += 1;
            break label129;
            break;
            label152:
            localArrayList.add(a(str1, str2));
          }
        }
        if ((localObject2 instanceof Bundle[]))
        {
          localObject2 = (Bundle[])localObject2;
          j = localObject2.length;
          i = 0;
          label193:
          if (i < j)
          {
            str2 = localObject2[i];
            if (str2 != null) {
              break label216;
            }
          }
          for (;;)
          {
            i += 1;
            break label193;
            break;
            label216:
            localArrayList.add(a(str1, str2));
          }
        }
        if ((localObject2 instanceof Boolean)) {
          localArrayList.add(a(str1, ((Boolean)localObject2).booleanValue()));
        } else {
          Log.e("SearchIndex", "Unsupported value: " + localObject2);
        }
      }
    }
    localObject1 = new zzpm.zzc();
    if (paramBundle.containsKey("type")) {
      ((zzpm.zzc)localObject1).a = paramBundle.getString("type");
    }
    ((zzpm.zzc)localObject1).b = ((zzpm.zzb[])localArrayList.toArray(new zzpm.zzb[localArrayList.size()]));
    return (zzpm.zzc)localObject1;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzkj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */