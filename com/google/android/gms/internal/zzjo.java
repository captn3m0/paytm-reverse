package com.google.android.gms.internal;

import android.content.Context;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import com.google.android.gms.ads.internal.overlay.zzk;
import com.google.android.gms.common.internal.zzx;

@zzhb
public class zzjo
{
  private final zzjp a;
  private final Context b;
  private final ViewGroup c;
  private zzk d;
  
  public zzjo(Context paramContext, ViewGroup paramViewGroup, zzjp paramzzjp)
  {
    this(paramContext, paramViewGroup, paramzzjp, null);
  }
  
  zzjo(Context paramContext, ViewGroup paramViewGroup, zzjp paramzzjp, zzk paramzzk)
  {
    this.b = paramContext;
    this.c = paramViewGroup;
    this.a = paramzzjp;
    this.d = paramzzk;
  }
  
  public zzk a()
  {
    zzx.b("getAdVideoUnderlay must be called from the UI thread.");
    return this.d;
  }
  
  public void a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    zzx.b("The underlay may only be modified from the UI thread.");
    if (this.d != null) {
      this.d.a(paramInt1, paramInt2, paramInt3, paramInt4);
    }
  }
  
  public void a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    if (this.d != null) {
      return;
    }
    zzbx.a(this.a.x().a(), this.a.w(), new String[] { "vpr" });
    zzbz localzzbz = zzbx.a(this.a.x().a());
    this.d = new zzk(this.b, this.a, paramInt5, this.a.x().a(), localzzbz);
    this.c.addView(this.d, 0, new ViewGroup.LayoutParams(-1, -1));
    this.d.a(paramInt1, paramInt2, paramInt3, paramInt4);
    this.a.l().a(false);
  }
  
  public void b()
  {
    zzx.b("onPause must be called from the UI thread.");
    if (this.d != null) {
      this.d.h();
    }
  }
  
  public void c()
  {
    zzx.b("onDestroy must be called from the UI thread.");
    if (this.d != null) {
      this.d.m();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzjo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */