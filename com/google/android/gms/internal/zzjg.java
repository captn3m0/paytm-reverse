package com.google.android.gms.internal;

import java.util.concurrent.Future;

public abstract interface zzjg<A>
  extends Future<A>
{
  public abstract void a(Runnable paramRunnable);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzjg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */