package com.google.android.gms.internal;

import android.view.View;
import com.google.android.gms.ads.internal.zzg;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;

@zzhb
public final class zzcc
  extends zzce.zza
{
  private final zzg a;
  private final String b;
  private final String c;
  
  public zzcc(zzg paramzzg, String paramString1, String paramString2)
  {
    this.a = paramzzg;
    this.b = paramString1;
    this.c = paramString2;
  }
  
  public String a()
  {
    return this.b;
  }
  
  public void a(zzd paramzzd)
  {
    if (paramzzd == null) {
      return;
    }
    this.a.b((View)zze.a(paramzzd));
  }
  
  public String b()
  {
    return this.c;
  }
  
  public void c()
  {
    this.a.z();
  }
  
  public void d()
  {
    this.a.A();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzcc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */