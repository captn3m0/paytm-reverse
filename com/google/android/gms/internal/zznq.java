package com.google.android.gms.internal;

import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.internal.zzb;
import com.google.android.gms.drive.metadata.internal.zzf;

public class zznq
{
  public static final MetadataField<Integer> a = new zzf("contentAvailability", 4300000);
  public static final MetadataField<Boolean> b = new zzb("isPinnable", 4300000);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zznq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */