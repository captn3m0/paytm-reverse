package com.google.android.gms.internal;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import org.json.JSONException;
import org.json.JSONObject;

@zzhb
public class zzdk
  implements zzdf
{
  final HashMap<String, zzjd<JSONObject>> a = new HashMap();
  
  public Future<JSONObject> a(String paramString)
  {
    zzjd localzzjd = new zzjd();
    this.a.put(paramString, localzzjd);
    return localzzjd;
  }
  
  public void a(zzjp paramzzjp, Map<String, String> paramMap)
  {
    a((String)paramMap.get("request_id"), (String)paramMap.get("fetched_ad"));
  }
  
  public void a(String paramString1, String paramString2)
  {
    zzin.a("Received ad from the cache.");
    zzjd localzzjd = (zzjd)this.a.get(paramString1);
    if (localzzjd == null)
    {
      zzin.b("Could not find the ad request for the corresponding ad response.");
      return;
    }
    try
    {
      localzzjd.b(new JSONObject(paramString2));
      return;
    }
    catch (JSONException paramString2)
    {
      zzin.b("Failed constructing JSON object from value passed from javascript", paramString2);
      localzzjd.b(null);
      return;
    }
    finally
    {
      this.a.remove(paramString1);
    }
  }
  
  public void b(String paramString)
  {
    zzjd localzzjd = (zzjd)this.a.get(paramString);
    if (localzzjd == null)
    {
      zzin.b("Could not find the ad request for the corresponding ad response.");
      return;
    }
    if (!localzzjd.isDone()) {
      localzzjd.cancel(true);
    }
    this.a.remove(paramString);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzdk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */