package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.zzx;
import java.util.Map;

@zzhb
public class zzeg
{
  private final Object a = new Object();
  private final Context b;
  private final String c;
  private final VersionInfoParcel d;
  private zzb<zzed> e;
  private zzb<zzed> f;
  private zze g;
  private int h = 1;
  
  public zzeg(Context paramContext, VersionInfoParcel paramVersionInfoParcel, String paramString)
  {
    this.c = paramString;
    this.b = paramContext.getApplicationContext();
    this.d = paramVersionInfoParcel;
    this.e = new zzc();
    this.f = new zzc();
  }
  
  public zzeg(Context paramContext, VersionInfoParcel paramVersionInfoParcel, String paramString, zzb<zzed> paramzzb1, zzb<zzed> paramzzb2)
  {
    this(paramContext, paramVersionInfoParcel, paramString);
    this.e = paramzzb1;
    this.f = paramzzb2;
  }
  
  private zze c()
  {
    final zze localzze = new zze(this.f);
    zzir.a(new Runnable()
    {
      public void run()
      {
        final zzed localzzed = zzeg.this.a(zzeg.a(zzeg.this), zzeg.b(zzeg.this));
        localzzed.a(new zzed.zza()
        {
          public void a()
          {
            zzir.a.postDelayed(new Runnable()
            {
              public void run()
              {
                synchronized (zzeg.c(zzeg.this))
                {
                  if ((zzeg.1.this.a.f() == -1) || (zzeg.1.this.a.f() == 1)) {
                    return;
                  }
                  zzeg.1.this.a.e();
                  zzir.a(new Runnable()
                  {
                    public void run()
                    {
                      zzeg.1.1.this.a.a();
                    }
                  });
                  zzin.e("Could not receive loaded message in a timely manner. Rejecting.");
                  return;
                }
              }
            }, zzeg.zza.b);
          }
        });
        localzzed.a("/jsLoaded", new zzdf()
        {
          public void a(zzjp arg1, Map<String, String> paramAnonymous2Map)
          {
            synchronized (zzeg.c(zzeg.this))
            {
              if ((zzeg.1.this.a.f() == -1) || (zzeg.1.this.a.f() == 1)) {
                return;
              }
              zzeg.a(zzeg.this, 0);
              zzeg.d(zzeg.this).a(localzzed);
              zzeg.1.this.a.a(localzzed);
              zzeg.a(zzeg.this, zzeg.1.this.a);
              zzin.e("Successfully loaded JS Engine.");
              return;
            }
          }
        });
        final zzja localzzja = new zzja();
        zzdf local3 = new zzdf()
        {
          public void a(zzjp arg1, Map<String, String> paramAnonymous2Map)
          {
            synchronized (zzeg.c(zzeg.this))
            {
              zzin.c("JS Engine is requesting an update");
              if (zzeg.e(zzeg.this) == 0)
              {
                zzin.c("Starting reload.");
                zzeg.a(zzeg.this, 2);
                zzeg.this.a();
              }
              localzzed.b("/requestReload", (zzdf)localzzja.a());
              return;
            }
          }
        };
        localzzja.a(local3);
        localzzed.a("/requestReload", local3);
        if (zzeg.f(zzeg.this).endsWith(".js")) {
          localzzed.a(zzeg.f(zzeg.this));
        }
        for (;;)
        {
          zzir.a.postDelayed(new Runnable()
          {
            public void run()
            {
              synchronized (zzeg.c(zzeg.this))
              {
                if ((zzeg.1.this.a.f() == -1) || (zzeg.1.this.a.f() == 1)) {
                  return;
                }
                zzeg.1.this.a.e();
                zzir.a(new Runnable()
                {
                  public void run()
                  {
                    zzeg.1.4.this.a.a();
                  }
                });
                zzin.e("Could not receive loaded message in a timely manner. Rejecting.");
                return;
              }
            }
          }, zzeg.zza.a);
          return;
          if (zzeg.f(zzeg.this).startsWith("<html>")) {
            localzzed.c(zzeg.f(zzeg.this));
          } else {
            localzzed.b(zzeg.f(zzeg.this));
          }
        }
      }
    });
    return localzze;
  }
  
  protected zzed a(Context paramContext, VersionInfoParcel paramVersionInfoParcel)
  {
    return new zzef(paramContext, paramVersionInfoParcel, null);
  }
  
  protected zze a()
  {
    final zze localzze = c();
    localzze.a(new zzji.zzc()new zzji.zza
    {
      public void a(zzed arg1)
      {
        synchronized (zzeg.c(zzeg.this))
        {
          zzeg.a(zzeg.this, 0);
          if ((zzeg.g(zzeg.this) != null) && (localzze != zzeg.g(zzeg.this)))
          {
            zzin.e("New JS engine is loaded, marking previous one as destroyable.");
            zzeg.g(zzeg.this).c();
          }
          zzeg.a(zzeg.this, localzze);
          return;
        }
      }
    }, new zzji.zza()
    {
      public void a()
      {
        synchronized (zzeg.c(zzeg.this))
        {
          zzeg.a(zzeg.this, 1);
          zzin.e("Failed loading new engine. Marking new engine destroyable.");
          localzze.c();
          return;
        }
      }
    });
    return localzze;
  }
  
  public zzd b()
  {
    synchronized (this.a)
    {
      zzd localzzd1;
      if ((this.g == null) || (this.g.f() == -1))
      {
        this.h = 2;
        this.g = a();
        localzzd1 = this.g.a();
        return localzzd1;
      }
      if (this.h == 0)
      {
        localzzd1 = this.g.a();
        return localzzd1;
      }
    }
    if (this.h == 1)
    {
      this.h = 2;
      a();
      localzzd2 = this.g.a();
      return localzzd2;
    }
    if (this.h == 2)
    {
      localzzd2 = this.g.a();
      return localzzd2;
    }
    zzd localzzd2 = this.g.a();
    return localzzd2;
  }
  
  static class zza
  {
    static int a = 60000;
    static int b = 10000;
  }
  
  public static abstract interface zzb<T>
  {
    public abstract void a(T paramT);
  }
  
  public static class zzc<T>
    implements zzeg.zzb<T>
  {
    public void a(T paramT) {}
  }
  
  public static class zzd
    extends zzjj<zzeh>
  {
    private final Object d = new Object();
    private final zzeg.zze e;
    private boolean f;
    
    public zzd(zzeg.zze paramzze)
    {
      this.e = paramzze;
    }
    
    public void a()
    {
      synchronized (this.d)
      {
        if (this.f) {
          return;
        }
        this.f = true;
        a(new zzji.zzc()new zzji.zzb
        {
          public void a(zzeh paramAnonymouszzeh)
          {
            zzin.e("Ending javascript session.");
            ((zzei)paramAnonymouszzeh).a();
          }
        }, new zzji.zzb());
        a(new zzji.zzc()new zzji.zza
        {
          public void a(zzeh paramAnonymouszzeh)
          {
            zzin.e("Releasing engine reference.");
            zzeg.zzd.a(zzeg.zzd.this).b();
          }
        }, new zzji.zza()
        {
          public void a()
          {
            zzeg.zzd.a(zzeg.zzd.this).b();
          }
        });
        return;
      }
    }
  }
  
  public static class zze
    extends zzjj<zzed>
  {
    private final Object d = new Object();
    private zzeg.zzb<zzed> e;
    private boolean f;
    private int g;
    
    public zze(zzeg.zzb<zzed> paramzzb)
    {
      this.e = paramzzb;
      this.f = false;
      this.g = 0;
    }
    
    public zzeg.zzd a()
    {
      final zzeg.zzd localzzd = new zzeg.zzd(this);
      for (;;)
      {
        synchronized (this.d)
        {
          a(new zzji.zzc()new zzji.zza
          {
            public void a(zzed paramAnonymouszzed)
            {
              zzin.e("Getting a new session for JS Engine.");
              localzzd.a(paramAnonymouszzed.b());
            }
          }, new zzji.zza()
          {
            public void a()
            {
              zzin.e("Rejecting reference for JS Engine.");
              localzzd.e();
            }
          });
          if (this.g >= 0)
          {
            bool = true;
            zzx.a(bool);
            this.g += 1;
            return localzzd;
          }
        }
        boolean bool = false;
      }
    }
    
    protected void b()
    {
      for (boolean bool = true;; bool = false) {
        synchronized (this.d)
        {
          if (this.g >= 1)
          {
            zzx.a(bool);
            zzin.e("Releasing 1 reference for JS Engine");
            this.g -= 1;
            d();
            return;
          }
        }
      }
    }
    
    public void c()
    {
      for (boolean bool = true;; bool = false) {
        synchronized (this.d)
        {
          if (this.g >= 0)
          {
            zzx.a(bool);
            zzin.e("Releasing root reference. JS Engine will be destroyed once other references are released.");
            this.f = true;
            d();
            return;
          }
        }
      }
    }
    
    protected void d()
    {
      for (;;)
      {
        synchronized (this.d)
        {
          if (this.g >= 0)
          {
            bool = true;
            zzx.a(bool);
            if ((this.f) && (this.g == 0))
            {
              zzin.e("No reference is left (including root). Cleaning up engine.");
              a(new zzji.zzc()new zzji.zzb
              {
                public void a(final zzed paramAnonymouszzed)
                {
                  zzir.a(new Runnable()
                  {
                    public void run()
                    {
                      zzeg.zze.a(zzeg.zze.this).a(paramAnonymouszzed);
                      paramAnonymouszzed.a();
                    }
                  });
                }
              }, new zzji.zzb());
              return;
            }
            zzin.e("There are still references to the engine. Not destroying.");
          }
        }
        boolean bool = false;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzeg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */