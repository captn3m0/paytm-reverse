package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.zzr;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzhb
public final class zzeo
{
  public final List<zzen> a;
  public final long b;
  public final List<String> c;
  public final List<String> d;
  public final List<String> e;
  public final String f;
  public final long g;
  public final String h;
  public final int i;
  public final int j;
  public final long k;
  public int l;
  public int m;
  
  public zzeo(String paramString)
    throws JSONException
  {
    paramString = new JSONObject(paramString);
    if (zzin.a(2)) {
      zzin.e("Mediation Response JSON: " + paramString.toString(2));
    }
    JSONArray localJSONArray = paramString.getJSONArray("ad_networks");
    ArrayList localArrayList = new ArrayList(localJSONArray.length());
    int n = 0;
    int i2;
    for (int i1 = -1; n < localJSONArray.length(); i1 = i2)
    {
      zzen localzzen = new zzen(localJSONArray.getJSONObject(n));
      localArrayList.add(localzzen);
      i2 = i1;
      if (i1 < 0)
      {
        i2 = i1;
        if (a(localzzen)) {
          i2 = n;
        }
      }
      n += 1;
    }
    this.l = i1;
    this.m = localJSONArray.length();
    this.a = Collections.unmodifiableList(localArrayList);
    this.f = paramString.getString("qdata");
    this.j = paramString.optInt("fs_model_type", -1);
    this.k = paramString.optLong("timeout_ms", -1L);
    paramString = paramString.optJSONObject("settings");
    if (paramString != null)
    {
      this.b = paramString.optLong("ad_network_timeout_millis", -1L);
      this.c = zzr.r().a(paramString, "click_urls");
      this.d = zzr.r().a(paramString, "imp_urls");
      this.e = zzr.r().a(paramString, "nofill_urls");
      long l1 = paramString.optLong("refresh", -1L);
      if (l1 > 0L) {}
      for (l1 *= 1000L;; l1 = -1L)
      {
        this.g = l1;
        paramString = RewardItemParcel.a(paramString.optJSONArray("rewards"));
        if (paramString != null) {
          break;
        }
        this.h = null;
        this.i = 0;
        return;
      }
      this.h = paramString.b;
      this.i = paramString.c;
      return;
    }
    this.b = -1L;
    this.c = null;
    this.d = null;
    this.e = null;
    this.g = -1L;
    this.h = null;
    this.i = 0;
  }
  
  public zzeo(List<zzen> paramList, long paramLong1, List<String> paramList1, List<String> paramList2, List<String> paramList3, String paramString1, long paramLong2, int paramInt1, int paramInt2, String paramString2, int paramInt3, int paramInt4, long paramLong3)
  {
    this.a = paramList;
    this.b = paramLong1;
    this.c = paramList1;
    this.d = paramList2;
    this.e = paramList3;
    this.f = paramString1;
    this.g = paramLong2;
    this.l = paramInt1;
    this.m = paramInt2;
    this.h = paramString2;
    this.i = paramInt3;
    this.j = paramInt4;
    this.k = paramLong3;
  }
  
  private boolean a(zzen paramzzen)
  {
    paramzzen = paramzzen.c.iterator();
    while (paramzzen.hasNext()) {
      if (((String)paramzzen.next()).equals("com.google.ads.mediation.admob.AdMobAdapter")) {
        return true;
      }
    }
    return false;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzeo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */