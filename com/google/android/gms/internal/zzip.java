package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import java.util.concurrent.Future;

@zzhb
public final class zzip
{
  public static SharedPreferences a(Context paramContext)
  {
    return paramContext.getSharedPreferences("admob", 0);
  }
  
  public static Future a(Context paramContext, final int paramInt)
  {
    new zza(paramContext)
    {
      public void a()
      {
        SharedPreferences.Editor localEditor = zzip.a(this.a).edit();
        localEditor.putInt("webview_cache_version", paramInt);
        localEditor.apply();
      }
    }.f();
  }
  
  public static Future a(Context paramContext, final zzb paramzzb)
  {
    new zza(paramContext)
    {
      public void a()
      {
        SharedPreferences localSharedPreferences = zzip.a(this.a);
        Bundle localBundle = new Bundle();
        localBundle.putBoolean("use_https", localSharedPreferences.getBoolean("use_https", true));
        if (paramzzb != null) {
          paramzzb.a(localBundle);
        }
      }
    }.f();
  }
  
  public static Future a(Context paramContext, final String paramString)
  {
    new zza(paramContext)
    {
      public void a()
      {
        SharedPreferences.Editor localEditor = zzip.a(this.a).edit();
        localEditor.putString("content_url_hashes", paramString);
        localEditor.apply();
      }
    }.f();
  }
  
  public static Future a(Context paramContext, final boolean paramBoolean)
  {
    new zza(paramContext)
    {
      public void a()
      {
        SharedPreferences.Editor localEditor = zzip.a(this.a).edit();
        localEditor.putBoolean("use_https", paramBoolean);
        localEditor.apply();
      }
    }.f();
  }
  
  public static Future b(Context paramContext, final zzb paramzzb)
  {
    new zza(paramContext)
    {
      public void a()
      {
        SharedPreferences localSharedPreferences = zzip.a(this.a);
        Bundle localBundle = new Bundle();
        localBundle.putInt("webview_cache_version", localSharedPreferences.getInt("webview_cache_version", 0));
        if (paramzzb != null) {
          paramzzb.a(localBundle);
        }
      }
    }.f();
  }
  
  public static Future b(Context paramContext, final boolean paramBoolean)
  {
    new zza(paramContext)
    {
      public void a()
      {
        SharedPreferences.Editor localEditor = zzip.a(this.a).edit();
        localEditor.putBoolean("content_url_opted_out", paramBoolean);
        localEditor.apply();
      }
    }.f();
  }
  
  public static Future c(Context paramContext, final zzb paramzzb)
  {
    new zza(paramContext)
    {
      public void a()
      {
        SharedPreferences localSharedPreferences = zzip.a(this.a);
        Bundle localBundle = new Bundle();
        localBundle.putBoolean("content_url_opted_out", localSharedPreferences.getBoolean("content_url_opted_out", true));
        if (paramzzb != null) {
          paramzzb.a(localBundle);
        }
      }
    }.f();
  }
  
  public static Future d(Context paramContext, final zzb paramzzb)
  {
    new zza(paramContext)
    {
      public void a()
      {
        SharedPreferences localSharedPreferences = zzip.a(this.a);
        Bundle localBundle = new Bundle();
        localBundle.putString("content_url_hashes", localSharedPreferences.getString("content_url_hashes", ""));
        if (paramzzb != null) {
          paramzzb.a(localBundle);
        }
      }
    }.f();
  }
  
  private static abstract class zza
    extends zzim
  {
    public void b() {}
  }
  
  public static abstract interface zzb
  {
    public abstract void a(Bundle paramBundle);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzip.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */