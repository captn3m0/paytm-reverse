package com.google.android.gms.internal;

import android.content.Intent;

public class zza
  extends zzr
{
  private Intent b;
  
  public zza() {}
  
  public zza(zzi paramzzi)
  {
    super(paramzzi);
  }
  
  public String getMessage()
  {
    if (this.b != null) {
      return "User needs to (re)enter credentials.";
    }
    return super.getMessage();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */