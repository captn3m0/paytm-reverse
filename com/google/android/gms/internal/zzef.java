package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import android.webkit.WebView;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzn;
import com.google.android.gms.ads.internal.overlay.zzg;
import com.google.android.gms.ads.internal.overlay.zzp;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zze;
import com.google.android.gms.ads.internal.zzr;
import org.json.JSONObject;

@zzhb
public class zzef
  implements zzed
{
  private final zzjp a;
  
  public zzef(Context paramContext, VersionInfoParcel paramVersionInfoParcel, zzan paramzzan)
  {
    this.a = zzr.f().a(paramContext, new AdSizeParcel(), false, false, paramzzan, paramVersionInfoParcel);
    this.a.a().setWillNotDraw(true);
  }
  
  private void a(Runnable paramRunnable)
  {
    if (zzn.a().b())
    {
      paramRunnable.run();
      return;
    }
    zzir.a.post(paramRunnable);
  }
  
  public void a()
  {
    this.a.destroy();
  }
  
  public void a(com.google.android.gms.ads.internal.client.zza paramzza, zzg paramzzg, zzdb paramzzdb, zzp paramzzp, boolean paramBoolean, zzdh paramzzdh, zzdj paramzzdj, zze paramzze, zzft paramzzft)
  {
    this.a.l().a(paramzza, paramzzg, paramzzdb, paramzzp, paramBoolean, paramzzdh, paramzzdj, new zze(false), paramzzft);
  }
  
  public void a(final zzed.zza paramzza)
  {
    this.a.l().a(new zzjq.zza()
    {
      public void a(zzjp paramAnonymouszzjp, boolean paramAnonymousBoolean)
      {
        paramzza.a();
      }
    });
  }
  
  public void a(String paramString)
  {
    a(new Runnable()
    {
      public void run()
      {
        zzef.a(zzef.this).loadData(this.a, "text/html", "UTF-8");
      }
    });
  }
  
  public void a(String paramString, zzdf paramzzdf)
  {
    this.a.l().a(paramString, paramzzdf);
  }
  
  public void a(final String paramString1, final String paramString2)
  {
    a(new Runnable()
    {
      public void run()
      {
        zzef.a(zzef.this).a(paramString1, paramString2);
      }
    });
  }
  
  public void a(final String paramString, final JSONObject paramJSONObject)
  {
    a(new Runnable()
    {
      public void run()
      {
        zzef.a(zzef.this).a(paramString, paramJSONObject);
      }
    });
  }
  
  public zzei b()
  {
    return new zzej(this);
  }
  
  public void b(final String paramString)
  {
    a(new Runnable()
    {
      public void run()
      {
        zzef.a(zzef.this).loadUrl(paramString);
      }
    });
  }
  
  public void b(String paramString, zzdf paramzzdf)
  {
    this.a.l().b(paramString, paramzzdf);
  }
  
  public void b(String paramString, JSONObject paramJSONObject)
  {
    this.a.b(paramString, paramJSONObject);
  }
  
  public void c(final String paramString)
  {
    a(new Runnable()
    {
      public void run()
      {
        zzef.a(zzef.this).loadData(paramString, "text/html", "UTF-8");
      }
    });
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzef.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */