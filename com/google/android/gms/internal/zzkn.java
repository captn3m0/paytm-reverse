package com.google.android.gms.internal;

import android.content.Intent;
import com.google.android.gms.appinvite.AppInviteInvitationResult;
import com.google.android.gms.common.api.Status;

public class zzkn
  implements AppInviteInvitationResult
{
  private final Status a;
  private final Intent b;
  
  public zzkn(Status paramStatus, Intent paramIntent)
  {
    this.a = paramStatus;
    this.b = paramIntent;
  }
  
  public Status getStatus()
  {
    return this.a;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzkn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */