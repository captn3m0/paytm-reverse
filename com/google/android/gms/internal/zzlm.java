package com.google.android.gms.internal;

import com.google.android.gms.cast.games.GameManagerState;
import com.google.android.gms.cast.games.PlayerInfo;
import com.google.android.gms.cast.internal.zzf;
import com.google.android.gms.common.internal.zzw;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;

public final class zzlm
  implements GameManagerState
{
  private final int a;
  private final int b;
  private final String c;
  private final JSONObject d;
  private final String e;
  private final int f;
  private final Map<String, PlayerInfo> g;
  
  public zzlm(int paramInt1, int paramInt2, String paramString1, JSONObject paramJSONObject, Collection<PlayerInfo> paramCollection, String paramString2, int paramInt3)
  {
    this.a = paramInt1;
    this.b = paramInt2;
    this.c = paramString1;
    this.d = paramJSONObject;
    this.e = paramString2;
    this.f = paramInt3;
    this.g = new HashMap(paramCollection.size());
    paramString1 = paramCollection.iterator();
    while (paramString1.hasNext())
    {
      paramJSONObject = (PlayerInfo)paramString1.next();
      this.g.put(paramJSONObject.c(), paramJSONObject);
    }
  }
  
  public int a()
  {
    return this.a;
  }
  
  public PlayerInfo a(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return (PlayerInfo)this.g.get(paramString);
  }
  
  public int b()
  {
    return this.b;
  }
  
  public JSONObject c()
  {
    return this.d;
  }
  
  public CharSequence d()
  {
    return this.c;
  }
  
  public CharSequence e()
  {
    return this.e;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = true;
    if ((paramObject == null) || (!(paramObject instanceof GameManagerState))) {}
    do
    {
      return false;
      paramObject = (GameManagerState)paramObject;
    } while (g().size() != ((GameManagerState)paramObject).g().size());
    Iterator localIterator1 = g().iterator();
    label248:
    label249:
    for (;;)
    {
      int i;
      if (localIterator1.hasNext())
      {
        PlayerInfo localPlayerInfo1 = (PlayerInfo)localIterator1.next();
        Iterator localIterator2 = ((GameManagerState)paramObject).g().iterator();
        i = 0;
        if (localIterator2.hasNext())
        {
          PlayerInfo localPlayerInfo2 = (PlayerInfo)localIterator2.next();
          if (!zzf.a(localPlayerInfo1.c(), localPlayerInfo2.c())) {
            break label248;
          }
          if (!zzf.a(localPlayerInfo1, localPlayerInfo2)) {
            break;
          }
          i = 1;
        }
      }
      for (;;)
      {
        break;
        if (i != 0) {
          break label249;
        }
        return false;
        if ((this.a == ((GameManagerState)paramObject).a()) && (this.b == ((GameManagerState)paramObject).b()) && (this.f == ((GameManagerState)paramObject).f()) && (zzf.a(this.e, ((GameManagerState)paramObject).e())) && (zzf.a(this.c, ((GameManagerState)paramObject).d())) && (zznb.a(this.d, ((GameManagerState)paramObject).c()))) {}
        for (;;)
        {
          return bool;
          bool = false;
        }
      }
    }
  }
  
  public int f()
  {
    return this.f;
  }
  
  public Collection<PlayerInfo> g()
  {
    return Collections.unmodifiableCollection(this.g.values());
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Integer.valueOf(this.a), Integer.valueOf(this.b), this.g, this.c, this.d, this.e, Integer.valueOf(this.f) });
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzlm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */