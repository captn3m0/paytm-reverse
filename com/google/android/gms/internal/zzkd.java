package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.measurement.zze;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class zzkd
  extends zze<zzkd>
{
  private final Map<String, Object> a = new HashMap();
  
  private String a(String paramString)
  {
    zzx.a(paramString);
    String str = paramString;
    if (paramString != null)
    {
      str = paramString;
      if (paramString.startsWith("&")) {
        str = paramString.substring(1);
      }
    }
    zzx.a(str, "Name can not be empty or \"&\"");
    return str;
  }
  
  public Map<String, Object> a()
  {
    return Collections.unmodifiableMap(this.a);
  }
  
  public void a(zzkd paramzzkd)
  {
    zzx.a(paramzzkd);
    paramzzkd.a.putAll(this.a);
  }
  
  public void a(String paramString1, String paramString2)
  {
    paramString1 = a(paramString1);
    this.a.put(paramString1, paramString2);
  }
  
  public String toString()
  {
    return a(this.a);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzkd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */