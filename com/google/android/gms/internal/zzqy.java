package com.google.android.gms.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Moments;
import com.google.android.gms.plus.Moments.LoadMomentsResult;
import com.google.android.gms.plus.Plus.zza;
import com.google.android.gms.plus.internal.zze;

public final class zzqy
  implements Moments
{
  private static abstract class zza
    extends Plus.zza<Moments.LoadMomentsResult>
  {
    public Moments.LoadMomentsResult a(final Status paramStatus)
    {
      new Moments.LoadMomentsResult()
      {
        public Status getStatus()
        {
          return paramStatus;
        }
        
        public void release() {}
      };
    }
  }
  
  private static abstract class zzb
    extends Plus.zza<Status>
  {
    public Status a(Status paramStatus)
    {
      return paramStatus;
    }
  }
  
  private static abstract class zzc
    extends Plus.zza<Status>
  {
    public Status a(Status paramStatus)
    {
      return paramStatus;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzqy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */