package com.google.android.gms.internal;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;

public abstract interface zzmg
{
  public abstract PendingResult<Status> a(GoogleApiClient paramGoogleApiClient);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzmg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */