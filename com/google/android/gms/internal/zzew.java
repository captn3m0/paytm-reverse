package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.ads.mediation.AdUrlAdapter;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.ads.mediation.c;
import com.google.ads.mediation.h;
import com.google.ads.mediation.i;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.mediation.MediationAdapter;
import com.google.android.gms.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.mediation.customevent.CustomEvent;
import com.google.android.gms.ads.mediation.customevent.CustomEventExtras;
import java.util.Map;

@zzhb
public final class zzew
  extends zzex.zza
{
  private Map<Class<? extends NetworkExtras>, NetworkExtras> a;
  
  private <NETWORK_EXTRAS extends i, SERVER_PARAMETERS extends h> zzey c(String paramString)
    throws RemoteException
  {
    try
    {
      Object localObject = Class.forName(paramString, false, zzew.class.getClassLoader());
      if (c.class.isAssignableFrom((Class)localObject))
      {
        localObject = (c)((Class)localObject).newInstance();
        return new zzfj((c)localObject, (i)this.a.get(((c)localObject).b()));
      }
      if (MediationAdapter.class.isAssignableFrom((Class)localObject)) {
        return new zzfe((MediationAdapter)((Class)localObject).newInstance());
      }
      zzb.d("Could not instantiate mediation adapter: " + paramString + " (not a valid adapter).");
      throw new RemoteException();
    }
    catch (Throwable localThrowable) {}
    return d(paramString);
  }
  
  private zzey d(String paramString)
    throws RemoteException
  {
    do
    {
      try
      {
        zzb.a("Reflection failed, retrying using direct instantiation");
        if ("com.google.ads.mediation.admob.AdMobAdapter".equals(paramString)) {
          return new zzfe(new AdMobAdapter());
        }
        if ("com.google.ads.mediation.AdUrlAdapter".equals(paramString))
        {
          zzfe localzzfe = new zzfe(new AdUrlAdapter());
          return localzzfe;
        }
      }
      catch (Throwable localThrowable)
      {
        zzb.d("Could not instantiate mediation adapter: " + paramString + ". ", localThrowable);
        throw new RemoteException();
      }
      if ("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter".equals(paramString)) {
        return new zzfe(new com.google.android.gms.ads.mediation.customevent.CustomEventAdapter());
      }
    } while (!"com.google.ads.mediation.customevent.CustomEventAdapter".equals(paramString));
    Object localObject = new com.google.ads.mediation.customevent.CustomEventAdapter();
    localObject = new zzfj((c)localObject, (CustomEventExtras)this.a.get(((com.google.ads.mediation.customevent.CustomEventAdapter)localObject).b()));
    return (zzey)localObject;
  }
  
  public zzey a(String paramString)
    throws RemoteException
  {
    return c(paramString);
  }
  
  public void a(Map<Class<? extends NetworkExtras>, NetworkExtras> paramMap)
  {
    this.a = paramMap;
  }
  
  public boolean b(String paramString)
    throws RemoteException
  {
    try
    {
      boolean bool = CustomEvent.class.isAssignableFrom(Class.forName(paramString, false, zzew.class.getClassLoader()));
      return bool;
    }
    catch (Throwable localThrowable)
    {
      zzb.d("Could not load custom event implementation class: " + paramString + ", assuming old implementation.");
    }
    return false;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzew.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */