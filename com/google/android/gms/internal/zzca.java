package com.google.android.gms.internal;

import android.support.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

@zzhb
public class zzca
{
  private final Map<String, zzbz> a;
  @Nullable
  private final zzcb b;
  
  public zzca(@Nullable zzcb paramzzcb)
  {
    this.b = paramzzcb;
    this.a = new HashMap();
  }
  
  @Nullable
  public zzcb a()
  {
    return this.b;
  }
  
  public void a(String paramString, zzbz paramzzbz)
  {
    this.a.put(paramString, paramzzbz);
  }
  
  public void a(String paramString1, String paramString2, long paramLong)
  {
    zzbx.a(this.b, (zzbz)this.a.get(paramString2), paramLong, new String[] { paramString1 });
    this.a.put(paramString1, zzbx.a(this.b, paramLong));
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzca.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */