package com.google.android.gms.internal;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import com.google.android.gms.cast.Cast.CastApi;
import com.google.android.gms.cast.Cast.MessageReceivedCallback;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.games.GameManagerClient;
import com.google.android.gms.cast.games.GameManagerClient.GameManagerInstanceResult;
import com.google.android.gms.cast.games.GameManagerClient.GameManagerResult;
import com.google.android.gms.cast.games.GameManagerClient.Listener;
import com.google.android.gms.cast.games.GameManagerState;
import com.google.android.gms.cast.games.PlayerInfo;
import com.google.android.gms.cast.internal.zzb;
import com.google.android.gms.cast.internal.zzc;
import com.google.android.gms.cast.internal.zze;
import com.google.android.gms.cast.internal.zzf;
import com.google.android.gms.cast.internal.zzl;
import com.google.android.gms.cast.internal.zzo;
import com.google.android.gms.cast.internal.zzp;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class zzli
  extends zzc
{
  static final String a = zzf.b("com.google.cast.games");
  private static final zzl g = new zzl("GameManagerChannel");
  private final Map<String, String> h;
  private final List<zzp> i;
  private final SharedPreferences j;
  private final String k;
  private final Cast.CastApi l;
  private final GoogleApiClient m;
  private zzlj n;
  private boolean o;
  private GameManagerState p;
  private GameManagerState q;
  private String r;
  private JSONObject s;
  private long t;
  private GameManagerClient.Listener u;
  private String v;
  
  private int a(int paramInt)
  {
    int i1 = 0;
    switch (paramInt)
    {
    default: 
      g.d("Unknown GameManager protocol status code: " + paramInt, new Object[0]);
      i1 = 13;
    case 0: 
      return i1;
    case 4: 
      return 2151;
    case 3: 
      return 2150;
    case 2: 
      return 2003;
    }
    return 2001;
  }
  
  private JSONObject a(long paramLong, String paramString, int paramInt, JSONObject paramJSONObject)
  {
    try
    {
      JSONObject localJSONObject = new JSONObject();
      localJSONObject.put("requestId", paramLong);
      localJSONObject.put("type", paramInt);
      localJSONObject.put("extraMessageData", paramJSONObject);
      localJSONObject.put("playerId", paramString);
      localJSONObject.put("playerToken", c(paramString));
      return localJSONObject;
    }
    catch (JSONException paramString)
    {
      g.d("JSONException when trying to create a message: %s", new Object[] { paramString.getMessage() });
    }
    return null;
  }
  
  private void a(long paramLong, int paramInt, Object paramObject)
  {
    Iterator localIterator = this.i.iterator();
    while (localIterator.hasNext()) {
      if (((zzp)localIterator.next()).a(paramLong, paramInt, paramObject)) {
        localIterator.remove();
      }
    }
  }
  
  private void a(zzlk paramzzlk)
  {
    int i1 = 1;
    for (;;)
    {
      try
      {
        if (paramzzlk.a() == 1)
        {
          this.q = this.p;
          if ((i1 != 0) && (paramzzlk.m() != null)) {
            this.n = paramzzlk.m();
          }
          boolean bool = b();
          if (bool) {}
        }
        else
        {
          i1 = 0;
          continue;
        }
        localObject = new ArrayList();
        Iterator localIterator = paramzzlk.g().iterator();
        if (localIterator.hasNext())
        {
          zzlo localzzlo = (zzlo)localIterator.next();
          String str = localzzlo.c();
          ((ArrayList)localObject).add(new zzln(str, localzzlo.a(), localzzlo.b(), this.h.containsKey(str)));
          continue;
        }
        this.p = new zzlm(paramzzlk.f(), paramzzlk.e(), paramzzlk.i(), paramzzlk.h(), (Collection)localObject, this.n.a(), this.n.b());
      }
      finally {}
      Object localObject = this.p.a(paramzzlk.j());
      if ((localObject != null) && (((PlayerInfo)localObject).d()) && (paramzzlk.a() == 2))
      {
        this.r = paramzzlk.j();
        this.s = paramzzlk.d();
      }
    }
  }
  
  private void a(String paramString, int paramInt, JSONObject paramJSONObject, zzo paramzzo)
  {
    final long l1 = 1L + this.t;
    this.t = l1;
    paramString = a(l1, paramString, paramInt, paramJSONObject);
    if (paramString == null)
    {
      paramzzo.a(-1L, 2001, null);
      g.d("Not sending request because it was invalid.", new Object[0]);
      return;
    }
    paramJSONObject = new zzp(30000L);
    paramJSONObject.a(l1, paramzzo);
    this.i.add(paramJSONObject);
    a(true);
    this.l.a(this.m, f(), paramString.toString()).setResultCallback(new ResultCallback()
    {
      public void a(Status paramAnonymousStatus)
      {
        if (!paramAnonymousStatus.d()) {
          zzli.this.a(l1, paramAnonymousStatus.f());
        }
      }
    });
  }
  
  private void d()
  {
    if (this.u != null)
    {
      if ((this.q != null) && (!this.p.equals(this.q))) {
        this.u.a(this.p, this.q);
      }
      if ((this.s != null) && (this.r != null)) {
        this.u.a(this.r, this.s);
      }
    }
    this.q = null;
    this.r = null;
    this.s = null;
  }
  
  private void h()
  {
    try
    {
      JSONObject localJSONObject = new JSONObject();
      localJSONObject.put("castSessionId", this.k);
      localJSONObject.put("playerTokenMap", new JSONObject(this.h));
      this.j.edit().putString("save_data", localJSONObject.toString()).commit();
      return;
    }
    catch (JSONException localJSONException)
    {
      for (;;)
      {
        g.d("Error while saving data: %s", new Object[] { localJSONException.getMessage() });
      }
    }
    finally {}
  }
  
  private void i()
  {
    for (;;)
    {
      try
      {
        Object localObject1 = this.j.getString("save_data", null);
        if (localObject1 == null) {
          return;
        }
        try
        {
          localObject1 = new JSONObject((String)localObject1);
          Object localObject3 = ((JSONObject)localObject1).getString("castSessionId");
          if (!this.k.equals(localObject3)) {
            continue;
          }
          localObject1 = ((JSONObject)localObject1).getJSONObject("playerTokenMap");
          localObject3 = ((JSONObject)localObject1).keys();
          if (((Iterator)localObject3).hasNext())
          {
            String str = (String)((Iterator)localObject3).next();
            this.h.put(str, ((JSONObject)localObject1).getString(str));
            continue;
          }
        }
        catch (JSONException localJSONException)
        {
          g.d("Error while loading data: %s", new Object[] { localJSONException.getMessage() });
        }
        this.t = 0L;
      }
      finally {}
    }
  }
  
  public void a(long paramLong, int paramInt)
  {
    a(paramLong, paramInt, null);
  }
  
  public boolean a()
  {
    try
    {
      boolean bool = this.o;
      return bool;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  protected boolean a(long paramLong)
  {
    ??? = this.i.iterator();
    while (((Iterator)???).hasNext()) {
      if (((zzp)((Iterator)???).next()).b(paramLong, 15)) {
        ((Iterator)???).remove();
      }
    }
    for (;;)
    {
      synchronized (zzp.a)
      {
        Iterator localIterator = this.i.iterator();
        if (localIterator.hasNext())
        {
          if (!((zzp)localIterator.next()).b()) {
            continue;
          }
          bool = true;
          return bool;
        }
      }
      boolean bool = false;
    }
  }
  
  public final void b(String paramString)
  {
    g.b("message received: %s", new Object[] { paramString });
    do
    {
      try
      {
        zzlk localzzlk = zzlk.a(new JSONObject(paramString));
        if (localzzlk == null)
        {
          g.d("Could not parse game manager message from string: %s", new Object[] { paramString });
          return;
        }
      }
      catch (JSONException localJSONException)
      {
        g.d("Message is malformed (%s); ignoring: %s", new Object[] { localJSONException.getMessage(), paramString });
        return;
      }
    } while (((!b()) && (localJSONException.m() == null)) || (a()));
    int i1;
    if (localJSONException.a() == 1)
    {
      i1 = 1;
      label114:
      if ((i1 != 0) && (!TextUtils.isEmpty(localJSONException.l())))
      {
        this.h.put(localJSONException.j(), localJSONException.l());
        h();
      }
      if (localJSONException.b() != 0) {
        break label214;
      }
      a(localJSONException);
    }
    for (;;)
    {
      int i2 = a(localJSONException.b());
      if (i1 != 0) {
        a(localJSONException.k(), i2, localJSONException);
      }
      if ((!b()) || (i2 != 0)) {
        break;
      }
      d();
      return;
      i1 = 0;
      break label114;
      label214:
      g.d("Not updating from game message because the message contains error code: %d", new Object[] { Integer.valueOf(localJSONException.b()) });
    }
  }
  
  /* Error */
  public boolean b()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 118	com/google/android/gms/internal/zzli:n	Lcom/google/android/gms/internal/zzlj;
    //   6: astore_2
    //   7: aload_2
    //   8: ifnull +9 -> 17
    //   11: iconst_1
    //   12: istore_1
    //   13: aload_0
    //   14: monitorexit
    //   15: iload_1
    //   16: ireturn
    //   17: iconst_0
    //   18: istore_1
    //   19: goto -6 -> 13
    //   22: astore_2
    //   23: aload_0
    //   24: monitorexit
    //   25: aload_2
    //   26: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	27	0	this	zzli
    //   12	7	1	bool	boolean
    //   6	2	2	localzzlj	zzlj
    //   22	4	2	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   2	7	22	finally
  }
  
  public String c(String paramString)
    throws IllegalStateException
  {
    if (paramString == null) {
      paramString = null;
    }
    for (;;)
    {
      return paramString;
      try
      {
        paramString = (String)this.h.get(paramString);
      }
      finally {}
    }
  }
  
  public abstract class zza
    extends zzli.zzb<GameManagerClient.GameManagerResult>
  {
    public GameManagerClient.GameManagerResult a(Status paramStatus)
    {
      return new zzli.zze(paramStatus, null, -1L, null);
    }
  }
  
  public abstract class zzb<R extends Result>
    extends zzb<R>
  {
    protected zzo f;
    
    protected void a(zze paramzze)
    {
      c();
    }
    
    public abstract void c();
    
    public zzo d()
    {
      return this.f;
    }
  }
  
  public abstract class zzc
    extends zzli.zzb<GameManagerClient.GameManagerInstanceResult>
  {
    private GameManagerClient a;
    
    public GameManagerClient.GameManagerInstanceResult a(Status paramStatus)
    {
      return new zzli.zzd(paramStatus, null);
    }
  }
  
  private static final class zzd
    implements GameManagerClient.GameManagerInstanceResult
  {
    private final Status a;
    private final GameManagerClient b;
    
    zzd(Status paramStatus, GameManagerClient paramGameManagerClient)
    {
      this.a = paramStatus;
      this.b = paramGameManagerClient;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
  
  private static final class zze
    implements GameManagerClient.GameManagerResult
  {
    private final Status a;
    private final String b;
    private final long c;
    private final JSONObject d;
    
    zze(Status paramStatus, String paramString, long paramLong, JSONObject paramJSONObject)
    {
      this.a = paramStatus;
      this.b = paramString;
      this.c = paramLong;
      this.d = paramJSONObject;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzli.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */