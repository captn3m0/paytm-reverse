package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.mediation.MediationAdapter;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.NativeAdMapper;
import com.google.android.gms.ads.mediation.NativeAppInstallAdMapper;
import com.google.android.gms.ads.mediation.NativeContentAdMapper;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdAdapter;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.json.JSONObject;

@zzhb
public final class zzfe
  extends zzey.zza
{
  private final MediationAdapter a;
  private zzff b;
  
  public zzfe(MediationAdapter paramMediationAdapter)
  {
    this.a = paramMediationAdapter;
  }
  
  private Bundle a(String paramString1, int paramInt, String paramString2)
    throws RemoteException
  {
    com.google.android.gms.ads.internal.util.client.zzb.d("Server parameters: " + paramString1);
    Bundle localBundle;
    try
    {
      localBundle = new Bundle();
      if (paramString1 != null)
      {
        paramString1 = new JSONObject(paramString1);
        localBundle = new Bundle();
        Iterator localIterator = paramString1.keys();
        while (localIterator.hasNext())
        {
          String str = (String)localIterator.next();
          localBundle.putString(str, paramString1.getString(str));
        }
      }
      if (!(this.a instanceof AdMobAdapter)) {
        break label138;
      }
    }
    catch (Throwable paramString1)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Could not get Server Parameters Bundle.", paramString1);
      throw new RemoteException();
    }
    localBundle.putString("adJson", paramString2);
    localBundle.putInt("tagForChildDirectedTreatment", paramInt);
    label138:
    return localBundle;
  }
  
  public zzd a()
    throws RemoteException
  {
    if (!(this.a instanceof MediationBannerAdapter))
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("MediationAdapter is not a MediationBannerAdapter: " + this.a.getClass().getCanonicalName());
      throw new RemoteException();
    }
    try
    {
      zzd localzzd = zze.a(((MediationBannerAdapter)this.a).d());
      return localzzd;
    }
    catch (Throwable localThrowable)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Could not get banner view from adapter.", localThrowable);
      throw new RemoteException();
    }
  }
  
  public void a(AdRequestParcel paramAdRequestParcel, String paramString)
    throws RemoteException
  {
    a(paramAdRequestParcel, paramString, null);
  }
  
  public void a(AdRequestParcel paramAdRequestParcel, String paramString1, String paramString2)
    throws RemoteException
  {
    if (!(this.a instanceof MediationRewardedVideoAdAdapter))
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("MediationAdapter is not a MediationRewardedVideoAdAdapter: " + this.a.getClass().getCanonicalName());
      throw new RemoteException();
    }
    com.google.android.gms.ads.internal.util.client.zzb.a("Requesting rewarded video ad from adapter.");
    for (;;)
    {
      try
      {
        MediationRewardedVideoAdAdapter localMediationRewardedVideoAdAdapter = (MediationRewardedVideoAdAdapter)this.a;
        if (paramAdRequestParcel.e == null) {
          break label213;
        }
        localObject1 = new HashSet(paramAdRequestParcel.e);
        Object localObject2;
        if (paramAdRequestParcel.b == -1L)
        {
          localObject2 = null;
          localObject2 = new zzfd((Date)localObject2, paramAdRequestParcel.d, (Set)localObject1, paramAdRequestParcel.k, paramAdRequestParcel.f, paramAdRequestParcel.g, paramAdRequestParcel.r);
          if (paramAdRequestParcel.m != null)
          {
            localObject1 = paramAdRequestParcel.m.getBundle(localMediationRewardedVideoAdAdapter.getClass().getName());
            localMediationRewardedVideoAdAdapter.a((MediationAdRequest)localObject2, a(paramString1, paramAdRequestParcel.g, paramString2), (Bundle)localObject1);
          }
        }
        else
        {
          localObject2 = new Date(paramAdRequestParcel.b);
          continue;
        }
        localObject1 = null;
      }
      catch (Throwable paramAdRequestParcel)
      {
        com.google.android.gms.ads.internal.util.client.zzb.d("Could not load rewarded video ad from adapter.", paramAdRequestParcel);
        throw new RemoteException();
      }
      continue;
      label213:
      Object localObject1 = null;
    }
  }
  
  public void a(zzd paramzzd, AdRequestParcel paramAdRequestParcel, String paramString1, com.google.android.gms.ads.internal.reward.mediation.client.zza paramzza, String paramString2)
    throws RemoteException
  {
    if (!(this.a instanceof MediationRewardedVideoAdAdapter))
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("MediationAdapter is not a MediationRewardedVideoAdAdapter: " + this.a.getClass().getCanonicalName());
      throw new RemoteException();
    }
    com.google.android.gms.ads.internal.util.client.zzb.a("Initialize rewarded video adapter.");
    for (;;)
    {
      try
      {
        MediationRewardedVideoAdAdapter localMediationRewardedVideoAdAdapter = (MediationRewardedVideoAdAdapter)this.a;
        if (paramAdRequestParcel.e == null) {
          break label231;
        }
        localObject1 = new HashSet(paramAdRequestParcel.e);
        Object localObject2;
        if (paramAdRequestParcel.b == -1L)
        {
          localObject2 = null;
          localObject2 = new zzfd((Date)localObject2, paramAdRequestParcel.d, (Set)localObject1, paramAdRequestParcel.k, paramAdRequestParcel.f, paramAdRequestParcel.g, paramAdRequestParcel.r);
          if (paramAdRequestParcel.m != null)
          {
            localObject1 = paramAdRequestParcel.m.getBundle(localMediationRewardedVideoAdAdapter.getClass().getName());
            localMediationRewardedVideoAdAdapter.a((Context)zze.a(paramzzd), (MediationAdRequest)localObject2, paramString1, new com.google.android.gms.ads.internal.reward.mediation.client.zzb(paramzza), a(paramString2, paramAdRequestParcel.g, null), (Bundle)localObject1);
          }
        }
        else
        {
          localObject2 = new Date(paramAdRequestParcel.b);
          continue;
        }
        localObject1 = null;
      }
      catch (Throwable paramzzd)
      {
        com.google.android.gms.ads.internal.util.client.zzb.d("Could not initialize rewarded video adapter.", paramzzd);
        throw new RemoteException();
      }
      continue;
      label231:
      Object localObject1 = null;
    }
  }
  
  public void a(zzd paramzzd, AdRequestParcel paramAdRequestParcel, String paramString, zzez paramzzez)
    throws RemoteException
  {
    a(paramzzd, paramAdRequestParcel, paramString, null, paramzzez);
  }
  
  public void a(zzd paramzzd, AdRequestParcel paramAdRequestParcel, String paramString1, String paramString2, zzez paramzzez)
    throws RemoteException
  {
    if (!(this.a instanceof MediationInterstitialAdapter))
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("MediationAdapter is not a MediationInterstitialAdapter: " + this.a.getClass().getCanonicalName());
      throw new RemoteException();
    }
    com.google.android.gms.ads.internal.util.client.zzb.a("Requesting interstitial ad from adapter.");
    for (;;)
    {
      try
      {
        MediationInterstitialAdapter localMediationInterstitialAdapter = (MediationInterstitialAdapter)this.a;
        if (paramAdRequestParcel.e == null) {
          break label230;
        }
        localObject1 = new HashSet(paramAdRequestParcel.e);
        Object localObject2;
        if (paramAdRequestParcel.b == -1L)
        {
          localObject2 = null;
          localObject2 = new zzfd((Date)localObject2, paramAdRequestParcel.d, (Set)localObject1, paramAdRequestParcel.k, paramAdRequestParcel.f, paramAdRequestParcel.g, paramAdRequestParcel.r);
          if (paramAdRequestParcel.m != null)
          {
            localObject1 = paramAdRequestParcel.m.getBundle(localMediationInterstitialAdapter.getClass().getName());
            localMediationInterstitialAdapter.a((Context)zze.a(paramzzd), new zzff(paramzzez), a(paramString1, paramAdRequestParcel.g, paramString2), (MediationAdRequest)localObject2, (Bundle)localObject1);
          }
        }
        else
        {
          localObject2 = new Date(paramAdRequestParcel.b);
          continue;
        }
        localObject1 = null;
      }
      catch (Throwable paramzzd)
      {
        com.google.android.gms.ads.internal.util.client.zzb.d("Could not request interstitial ad from adapter.", paramzzd);
        throw new RemoteException();
      }
      continue;
      label230:
      Object localObject1 = null;
    }
  }
  
  public void a(zzd paramzzd, AdRequestParcel paramAdRequestParcel, String paramString1, String paramString2, zzez paramzzez, NativeAdOptionsParcel paramNativeAdOptionsParcel, List<String> paramList)
    throws RemoteException
  {
    if (!(this.a instanceof MediationNativeAdapter))
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("MediationAdapter is not a MediationNativeAdapter: " + this.a.getClass().getCanonicalName());
      throw new RemoteException();
    }
    for (;;)
    {
      try
      {
        MediationNativeAdapter localMediationNativeAdapter = (MediationNativeAdapter)this.a;
        if (paramAdRequestParcel.e == null) {
          break label237;
        }
        localHashSet = new HashSet(paramAdRequestParcel.e);
        Date localDate;
        if (paramAdRequestParcel.b == -1L)
        {
          localDate = null;
          paramList = new zzfi(localDate, paramAdRequestParcel.d, localHashSet, paramAdRequestParcel.k, paramAdRequestParcel.f, paramAdRequestParcel.g, paramNativeAdOptionsParcel, paramList, paramAdRequestParcel.r);
          if (paramAdRequestParcel.m != null)
          {
            paramNativeAdOptionsParcel = paramAdRequestParcel.m.getBundle(localMediationNativeAdapter.getClass().getName());
            this.b = new zzff(paramzzez);
            localMediationNativeAdapter.a((Context)zze.a(paramzzd), this.b, a(paramString1, paramAdRequestParcel.g, paramString2), paramList, paramNativeAdOptionsParcel);
          }
        }
        else
        {
          localDate = new Date(paramAdRequestParcel.b);
          continue;
        }
        paramNativeAdOptionsParcel = null;
      }
      catch (Throwable paramzzd)
      {
        com.google.android.gms.ads.internal.util.client.zzb.d("Could not request native ad from adapter.", paramzzd);
        throw new RemoteException();
      }
      continue;
      label237:
      HashSet localHashSet = null;
    }
  }
  
  public void a(zzd paramzzd, AdSizeParcel paramAdSizeParcel, AdRequestParcel paramAdRequestParcel, String paramString, zzez paramzzez)
    throws RemoteException
  {
    a(paramzzd, paramAdSizeParcel, paramAdRequestParcel, paramString, null, paramzzez);
  }
  
  public void a(zzd paramzzd, AdSizeParcel paramAdSizeParcel, AdRequestParcel paramAdRequestParcel, String paramString1, String paramString2, zzez paramzzez)
    throws RemoteException
  {
    if (!(this.a instanceof MediationBannerAdapter))
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("MediationAdapter is not a MediationBannerAdapter: " + this.a.getClass().getCanonicalName());
      throw new RemoteException();
    }
    com.google.android.gms.ads.internal.util.client.zzb.a("Requesting banner ad from adapter.");
    for (;;)
    {
      try
      {
        MediationBannerAdapter localMediationBannerAdapter = (MediationBannerAdapter)this.a;
        if (paramAdRequestParcel.e == null) {
          break label247;
        }
        localObject1 = new HashSet(paramAdRequestParcel.e);
        Object localObject2;
        if (paramAdRequestParcel.b == -1L)
        {
          localObject2 = null;
          localObject2 = new zzfd((Date)localObject2, paramAdRequestParcel.d, (Set)localObject1, paramAdRequestParcel.k, paramAdRequestParcel.f, paramAdRequestParcel.g, paramAdRequestParcel.r);
          if (paramAdRequestParcel.m != null)
          {
            localObject1 = paramAdRequestParcel.m.getBundle(localMediationBannerAdapter.getClass().getName());
            localMediationBannerAdapter.a((Context)zze.a(paramzzd), new zzff(paramzzez), a(paramString1, paramAdRequestParcel.g, paramString2), com.google.android.gms.ads.zza.a(paramAdSizeParcel.f, paramAdSizeParcel.c, paramAdSizeParcel.b), (MediationAdRequest)localObject2, (Bundle)localObject1);
          }
        }
        else
        {
          localObject2 = new Date(paramAdRequestParcel.b);
          continue;
        }
        localObject1 = null;
      }
      catch (Throwable paramzzd)
      {
        com.google.android.gms.ads.internal.util.client.zzb.d("Could not request banner ad from adapter.", paramzzd);
        throw new RemoteException();
      }
      continue;
      label247:
      Object localObject1 = null;
    }
  }
  
  public void b()
    throws RemoteException
  {
    if (!(this.a instanceof MediationInterstitialAdapter))
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("MediationAdapter is not a MediationInterstitialAdapter: " + this.a.getClass().getCanonicalName());
      throw new RemoteException();
    }
    com.google.android.gms.ads.internal.util.client.zzb.a("Showing interstitial from adapter.");
    try
    {
      ((MediationInterstitialAdapter)this.a).e();
      return;
    }
    catch (Throwable localThrowable)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Could not show interstitial from adapter.", localThrowable);
      throw new RemoteException();
    }
  }
  
  public void c()
    throws RemoteException
  {
    try
    {
      this.a.a();
      return;
    }
    catch (Throwable localThrowable)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Could not destroy adapter.", localThrowable);
      throw new RemoteException();
    }
  }
  
  public void d()
    throws RemoteException
  {
    try
    {
      this.a.b();
      return;
    }
    catch (Throwable localThrowable)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Could not pause adapter.", localThrowable);
      throw new RemoteException();
    }
  }
  
  public void e()
    throws RemoteException
  {
    try
    {
      this.a.c();
      return;
    }
    catch (Throwable localThrowable)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Could not resume adapter.", localThrowable);
      throw new RemoteException();
    }
  }
  
  public void f()
    throws RemoteException
  {
    if (!(this.a instanceof MediationRewardedVideoAdAdapter))
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("MediationAdapter is not a MediationRewardedVideoAdAdapter: " + this.a.getClass().getCanonicalName());
      throw new RemoteException();
    }
    com.google.android.gms.ads.internal.util.client.zzb.a("Show rewarded video ad from adapter.");
    try
    {
      ((MediationRewardedVideoAdAdapter)this.a).g();
      return;
    }
    catch (Throwable localThrowable)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Could not show rewarded video ad from adapter.", localThrowable);
      throw new RemoteException();
    }
  }
  
  public boolean g()
    throws RemoteException
  {
    if (!(this.a instanceof MediationRewardedVideoAdAdapter))
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("MediationAdapter is not a MediationRewardedVideoAdAdapter: " + this.a.getClass().getCanonicalName());
      throw new RemoteException();
    }
    com.google.android.gms.ads.internal.util.client.zzb.a("Check if adapter is initialized.");
    try
    {
      boolean bool = ((MediationRewardedVideoAdAdapter)this.a).h();
      return bool;
    }
    catch (Throwable localThrowable)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Could not check if adapter is initialized.", localThrowable);
      throw new RemoteException();
    }
  }
  
  public zzfb h()
  {
    NativeAdMapper localNativeAdMapper = this.b.a();
    if ((localNativeAdMapper instanceof NativeAppInstallAdMapper)) {
      return new zzfg((NativeAppInstallAdMapper)localNativeAdMapper);
    }
    return null;
  }
  
  public zzfc i()
  {
    NativeAdMapper localNativeAdMapper = this.b.a();
    if ((localNativeAdMapper instanceof NativeContentAdMapper)) {
      return new zzfh((NativeContentAdMapper)localNativeAdMapper);
    }
    return null;
  }
  
  public Bundle j()
  {
    if (!(this.a instanceof zzjz))
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("MediationAdapter is not a v2 MediationBannerAdapter: " + this.a.getClass().getCanonicalName());
      return new Bundle();
    }
    return ((zzjz)this.a).e();
  }
  
  public Bundle k()
  {
    if (!(this.a instanceof zzka))
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("MediationAdapter is not a v2 MediationInterstitialAdapter: " + this.a.getClass().getCanonicalName());
      return new Bundle();
    }
    return ((zzka)this.a).f();
  }
  
  public Bundle l()
  {
    return new Bundle();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzfe.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */