package com.google.android.gms.internal;

import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveSpace;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.SearchableMetadataField;
import com.google.android.gms.drive.metadata.SortableMetadataField;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties;
import com.google.android.gms.drive.metadata.internal.zzg;
import com.google.android.gms.drive.metadata.internal.zzj;
import com.google.android.gms.drive.metadata.internal.zzk;
import com.google.android.gms.drive.metadata.internal.zzm;
import com.google.android.gms.drive.metadata.internal.zzo;
import com.google.android.gms.drive.metadata.internal.zzp;
import com.google.android.gms.drive.metadata.internal.zzq;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class zznm
{
  public static final zzq A = new zzq("lastModifyingUser", 6000000);
  public static final zzq B = new zzq("sharingUser", 6000000);
  public static final zzm C = new zzm(4100000);
  public static final zzd D = new zzd("quotaBytesUsed", 4300000);
  public static final zzf E = new zzf("starred", 4100000);
  public static final MetadataField<BitmapTeleporter> F = new zzk("thumbnail", Collections.emptySet(), Collections.emptySet(), 4400000)
  {
    protected BitmapTeleporter d(DataHolder paramAnonymousDataHolder, int paramAnonymousInt1, int paramAnonymousInt2)
    {
      throw new IllegalStateException("Thumbnail field is write only");
    }
  };
  public static final zzg G = new zzg("title", 4100000);
  public static final zzh H = new zzh("trashed", 4100000);
  public static final MetadataField<String> I = new zzp("webContentLink", 4300000);
  public static final MetadataField<String> J = new zzp("webViewLink", 4300000);
  public static final MetadataField<String> K = new zzp("uniqueIdentifier", 5000000);
  public static final com.google.android.gms.drive.metadata.internal.zzb L = new com.google.android.gms.drive.metadata.internal.zzb("writersCanShare", 6000000);
  public static final MetadataField<String> M = new zzp("role", 6000000);
  public static final MetadataField<String> N = new zzp("md5Checksum", 7000000);
  public static final zze O = new zze(7000000);
  public static final MetadataField<String> P = new zzp("recencyReason", 8000000);
  public static final MetadataField<Boolean> Q = new com.google.android.gms.drive.metadata.internal.zzb("subscribed", 8000000);
  public static final MetadataField<DriveId> a = zznp.a;
  public static final MetadataField<String> b = new zzp("alternateLink", 4300000);
  public static final zza c = new zza(5000000);
  public static final MetadataField<String> d = new zzp("description", 4300000);
  public static final MetadataField<String> e = new zzp("embedLink", 4300000);
  public static final MetadataField<String> f = new zzp("fileExtension", 4300000);
  public static final MetadataField<Long> g = new zzg("fileSize", 4300000);
  public static final MetadataField<String> h = new zzp("folderColorRgb", 7500000);
  public static final MetadataField<Boolean> i = new com.google.android.gms.drive.metadata.internal.zzb("hasThumbnail", 4300000);
  public static final MetadataField<String> j = new zzp("indexableText", 4300000);
  public static final MetadataField<Boolean> k = new com.google.android.gms.drive.metadata.internal.zzb("isAppData", 4300000);
  public static final MetadataField<Boolean> l = new com.google.android.gms.drive.metadata.internal.zzb("isCopyable", 4300000);
  public static final MetadataField<Boolean> m = new com.google.android.gms.drive.metadata.internal.zzb("isEditable", 4100000);
  public static final MetadataField<Boolean> n = new com.google.android.gms.drive.metadata.internal.zzb("isExplicitlyTrashed", Collections.singleton("trashed"), Collections.emptySet(), 7000000)
  {
    protected Boolean a_(DataHolder paramAnonymousDataHolder, int paramAnonymousInt1, int paramAnonymousInt2)
    {
      if (paramAnonymousDataHolder.b("trashed", paramAnonymousInt1, paramAnonymousInt2) == 2) {}
      for (boolean bool = true;; bool = false) {
        return Boolean.valueOf(bool);
      }
    }
  };
  public static final MetadataField<Boolean> o = new com.google.android.gms.drive.metadata.internal.zzb("isLocalContentUpToDate", 7800000);
  public static final zzb p = new zzb("isPinned", 4100000);
  public static final MetadataField<Boolean> q = new com.google.android.gms.drive.metadata.internal.zzb("isOpenable", 7200000);
  public static final MetadataField<Boolean> r = new com.google.android.gms.drive.metadata.internal.zzb("isRestricted", 4300000);
  public static final MetadataField<Boolean> s = new com.google.android.gms.drive.metadata.internal.zzb("isShared", 4300000);
  public static final MetadataField<Boolean> t = new com.google.android.gms.drive.metadata.internal.zzb("isGooglePhotosFolder", 7000000);
  public static final MetadataField<Boolean> u = new com.google.android.gms.drive.metadata.internal.zzb("isGooglePhotosRootFolder", 7000000);
  public static final MetadataField<Boolean> v = new com.google.android.gms.drive.metadata.internal.zzb("isTrashable", 4400000);
  public static final MetadataField<Boolean> w = new com.google.android.gms.drive.metadata.internal.zzb("isViewed", 4300000);
  public static final zzc x = new zzc(4100000);
  public static final MetadataField<String> y = new zzp("originalFilename", 4300000);
  public static final com.google.android.gms.drive.metadata.zzb<String> z = new zzo("ownerNames", 4300000);
  
  public static class zza
    extends zznn
    implements SearchableMetadataField<AppVisibleCustomProperties>
  {
    public zza(int paramInt)
    {
      super();
    }
  }
  
  public static class zzb
    extends com.google.android.gms.drive.metadata.internal.zzb
    implements SearchableMetadataField<Boolean>
  {
    public zzb(String paramString, int paramInt)
    {
      super(paramInt);
    }
  }
  
  public static class zzc
    extends zzp
    implements SearchableMetadataField<String>
  {
    public zzc(int paramInt)
    {
      super(paramInt);
    }
  }
  
  public static class zzd
    extends zzg
    implements SortableMetadataField<Long>
  {
    public zzd(String paramString, int paramInt)
    {
      super(paramInt);
    }
  }
  
  public static class zze
    extends zzj<DriveSpace>
  {
    public zze(int paramInt)
    {
      super(Arrays.asList(new String[] { "inDriveSpace", "isAppData", "inGooglePhotosSpace" }), Collections.emptySet(), paramInt);
    }
    
    protected Collection<DriveSpace> e_(DataHolder paramDataHolder, int paramInt1, int paramInt2)
    {
      ArrayList localArrayList = new ArrayList();
      if (paramDataHolder.d("inDriveSpace", paramInt1, paramInt2)) {
        localArrayList.add(DriveSpace.a);
      }
      if (paramDataHolder.d("isAppData", paramInt1, paramInt2)) {
        localArrayList.add(DriveSpace.b);
      }
      if (paramDataHolder.d("inGooglePhotosSpace", paramInt1, paramInt2)) {
        localArrayList.add(DriveSpace.c);
      }
      return localArrayList;
    }
  }
  
  public static class zzf
    extends com.google.android.gms.drive.metadata.internal.zzb
    implements SearchableMetadataField<Boolean>
  {
    public zzf(String paramString, int paramInt)
    {
      super(paramInt);
    }
  }
  
  public static class zzg
    extends zzp
    implements SearchableMetadataField<String>, SortableMetadataField<String>
  {
    public zzg(String paramString, int paramInt)
    {
      super(paramInt);
    }
  }
  
  public static class zzh
    extends com.google.android.gms.drive.metadata.internal.zzb
    implements SearchableMetadataField<Boolean>
  {
    public zzh(String paramString, int paramInt)
    {
      super(paramInt);
    }
    
    protected Boolean a_(DataHolder paramDataHolder, int paramInt1, int paramInt2)
    {
      if (paramDataHolder.b(a(), paramInt1, paramInt2) != 0) {}
      for (boolean bool = true;; bool = false) {
        return Boolean.valueOf(bool);
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zznm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */