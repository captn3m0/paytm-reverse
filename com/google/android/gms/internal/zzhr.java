package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.reward.client.zza.zza;

@zzhb
public class zzhr
  extends zza.zza
{
  private final String a;
  private final int b;
  
  public zzhr(String paramString, int paramInt)
  {
    this.a = paramString;
    this.b = paramInt;
  }
  
  public String a()
  {
    return this.a;
  }
  
  public int b()
  {
    return this.b;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzhr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */