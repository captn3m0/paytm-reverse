package com.google.android.gms.internal;

public class zzr
  extends Exception
{
  public final zzi a;
  private long b;
  
  public zzr()
  {
    this.a = null;
  }
  
  public zzr(zzi paramzzi)
  {
    this.a = paramzzi;
  }
  
  public zzr(Throwable paramThrowable)
  {
    super(paramThrowable);
    this.a = null;
  }
  
  void a(long paramLong)
  {
    this.b = paramLong;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */