package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.text.TextUtils;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.Auth.zza;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;
import java.util.Set;

public final class zzkz
  extends zzj<zzlb>
{
  private final Bundle a;
  
  public zzkz(Context paramContext, Looper paramLooper, zzf paramzzf, Auth.zza paramzza, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramContext, paramLooper, 16, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    if (paramzza == null) {}
    for (paramContext = new Bundle();; paramContext = paramzza.a())
    {
      this.a = paramContext;
      return;
    }
  }
  
  protected zzlb a(IBinder paramIBinder)
  {
    return zzlb.zza.a(paramIBinder);
  }
  
  protected String a()
  {
    return "com.google.android.gms.auth.service.START";
  }
  
  protected Bundle a_()
  {
    return this.a;
  }
  
  protected String b()
  {
    return "com.google.android.gms.auth.api.internal.IAuthService";
  }
  
  public boolean l()
  {
    zzf localzzf = t();
    return (!TextUtils.isEmpty(localzzf.a())) && (!localzzf.a(Auth.g).isEmpty());
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzkz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */