package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.util.Log;

@Deprecated
public class zzqt
  implements zzqu.zza
{
  private final zzqu a;
  private boolean b;
  
  public zzqt(Context paramContext, int paramInt)
  {
    this(paramContext, paramInt, null);
  }
  
  public zzqt(Context paramContext, int paramInt, String paramString)
  {
    this(paramContext, paramInt, paramString, null, true);
  }
  
  public zzqt(Context paramContext, int paramInt, String paramString1, String paramString2, boolean paramBoolean)
  {
    if (paramContext != paramContext.getApplicationContext()) {}
    for (String str = paramContext.getClass().getName();; str = "OneTimePlayLogger")
    {
      this.a = new zzqu(paramContext, paramInt, paramString1, paramString2, this, paramBoolean, str);
      this.b = true;
      return;
    }
  }
  
  private void d()
  {
    if (!this.b) {
      throw new IllegalStateException("Cannot reuse one-time logger after sending.");
    }
  }
  
  public void a()
  {
    d();
    this.a.a();
    this.b = false;
  }
  
  public void a(PendingIntent paramPendingIntent)
  {
    Log.w("OneTimePlayLogger", "logger connection failed: " + paramPendingIntent);
  }
  
  public void a(String paramString, byte[] paramArrayOfByte, String... paramVarArgs)
  {
    d();
    this.a.a(paramString, paramArrayOfByte, paramVarArgs);
  }
  
  public void b()
  {
    this.a.b();
  }
  
  public void c()
  {
    Log.w("OneTimePlayLogger", "logger connection failed");
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzqt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */