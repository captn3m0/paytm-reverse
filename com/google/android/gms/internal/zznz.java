package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.Looper;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zza;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzx;

public class zznz
  extends zzny<zzok>
{
  public zznz(Context paramContext, Looper paramLooper, zzf paramzzf, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramContext, paramLooper, 59, paramConnectionCallbacks, paramOnConnectionFailedListener, paramzzf);
  }
  
  protected zzok a(IBinder paramIBinder)
  {
    return zzok.zza.a(paramIBinder);
  }
  
  protected String a()
  {
    return "com.google.android.gms.fitness.BleApi";
  }
  
  protected String b()
  {
    return "com.google.android.gms.fitness.internal.IGoogleFitBleApi";
  }
  
  static abstract class zza<R extends Result>
    extends zza.zza<R, zznz>
  {}
  
  public static class zzb
    extends Api.zza<zznz, Api.ApiOptions.NoOptions>
  {
    public zznz a(Context paramContext, Looper paramLooper, zzf paramzzf, Api.ApiOptions.NoOptions paramNoOptions, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
    {
      return new zznz(paramContext, paramLooper, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    }
  }
  
  static abstract class zzc
    extends zznz.zza<Status>
  {
    protected Status a(Status paramStatus)
    {
      if (!paramStatus.d()) {}
      for (boolean bool = true;; bool = false)
      {
        zzx.b(bool);
        return paramStatus;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zznz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */