package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Future;

@zzhb
public class zzhz
  extends zzim
  implements zzhy
{
  private final zzif.zza a;
  private final Context b;
  private final ArrayList<Future> c = new ArrayList();
  private final ArrayList<String> d = new ArrayList();
  private final HashSet<String> e = new HashSet();
  private final Object f = new Object();
  private final zzht g;
  private final String h;
  
  public zzhz(Context paramContext, String paramString, zzif.zza paramzza, zzht paramzzht)
  {
    this.b = paramContext;
    this.h = paramString;
    this.a = paramzza;
    this.g = paramzzht;
  }
  
  private zzif a(int paramInt, @Nullable String paramString, @Nullable zzen paramzzen)
  {
    return new zzif(this.a.a.c, null, this.a.b.d, paramInt, this.a.b.f, this.a.b.j, this.a.b.l, this.a.b.k, this.a.a.i, this.a.b.h, paramzzen, null, paramString, this.a.c, null, this.a.b.i, this.a.d, this.a.b.g, this.a.f, this.a.b.n, this.a.b.o, this.a.h, null, this.a.b.D, this.a.b.E, this.a.b.F, this.a.b.G);
  }
  
  private zzif a(String paramString, zzen paramzzen)
  {
    return a(-2, paramString, paramzzen);
  }
  
  private void a(String paramString1, String paramString2, String paramString3)
  {
    synchronized (this.f)
    {
      zzia localzzia = this.g.c(paramString1);
      if ((localzzia == null) || (localzzia.b() == null) || (localzzia.a() == null)) {
        return;
      }
      paramString2 = new zzhu(this.b, paramString1, this.h, paramString2, paramString3, this.a, localzzia, this);
      this.c.add(paramString2.f());
      this.d.add(paramString1);
      return;
    }
  }
  
  private zzif c()
  {
    return a(3, null, null);
  }
  
  public void a()
  {
    ??? = this.a.c.a.iterator();
    final Object localObject2;
    while (((Iterator)???).hasNext())
    {
      localObject2 = (zzen)((Iterator)???).next();
      String str = ((zzen)localObject2).h;
      Iterator localIterator = ((zzen)localObject2).c.iterator();
      while (localIterator.hasNext()) {
        a((String)localIterator.next(), str, ((zzen)localObject2).a);
      }
    }
    int i = 0;
    for (;;)
    {
      if (i < this.c.size()) {}
      try
      {
        ((Future)this.c.get(i)).get();
        synchronized (this.f)
        {
          if (this.e.contains(this.d.get(i)))
          {
            localObject2 = a((String)this.d.get(i), (zzen)this.a.c.a.get(i));
            zza.a.post(new Runnable()
            {
              public void run()
              {
                zzhz.a(zzhz.this).b(localObject2);
              }
            });
            return;
          }
        }
      }
      catch (InterruptedException localInterruptedException)
      {
        final zzif localzzif = c();
        zza.a.post(new Runnable()
        {
          public void run()
          {
            zzhz.a(zzhz.this).b(localzzif);
          }
        });
        return;
      }
      catch (Exception localException)
      {
        i += 1;
      }
    }
  }
  
  public void a(String paramString)
  {
    synchronized (this.f)
    {
      this.e.add(paramString);
      return;
    }
  }
  
  public void a(String paramString, int paramInt) {}
  
  public void b() {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzhz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */