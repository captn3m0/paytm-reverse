package com.google.android.gms.internal;

import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.clearcut.LogEventParcelable;
import com.google.android.gms.clearcut.zzb;
import com.google.android.gms.clearcut.zzb.zzb;
import com.google.android.gms.clearcut.zzc;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.PendingResult.zza;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zza;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class zzlv
  implements zzc
{
  private static final Object a = new Object();
  private static final zze b = new zze(null);
  private static final long c = TimeUnit.MILLISECONDS.convert(2L, TimeUnit.MINUTES);
  private final zzmq d;
  private final zza e;
  private final Object f = new Object();
  private long g = 0L;
  private final long h;
  private ScheduledFuture<?> i = null;
  private GoogleApiClient j = null;
  private final Runnable k = new Runnable()
  {
    public void run()
    {
      synchronized (zzlv.a(zzlv.this))
      {
        if ((zzlv.b(zzlv.this) <= zzlv.c(zzlv.this).b()) && (zzlv.d(zzlv.this) != null))
        {
          Log.i("ClearcutLoggerApiImpl", "disconnect managed GoogleApiClient");
          zzlv.d(zzlv.this).g();
          zzlv.a(zzlv.this, null);
        }
        return;
      }
    }
  };
  
  public zzlv()
  {
    this(new zzmt(), c, new zzb());
  }
  
  public zzlv(zzmq paramzzmq, long paramLong, zza paramzza)
  {
    this.d = paramzzmq;
    this.h = paramLong;
    this.e = paramzza;
  }
  
  private zzd b(GoogleApiClient paramGoogleApiClient, LogEventParcelable paramLogEventParcelable)
  {
    b.a();
    paramGoogleApiClient = new zzd(paramLogEventParcelable, paramGoogleApiClient);
    paramGoogleApiClient.zza(new PendingResult.zza()
    {
      public void a(Status paramAnonymousStatus)
      {
        zzlv.a().b();
      }
    });
    return paramGoogleApiClient;
  }
  
  private static void b(LogEventParcelable paramLogEventParcelable)
  {
    if ((paramLogEventParcelable.f != null) && (paramLogEventParcelable.e.j.length == 0)) {
      paramLogEventParcelable.e.j = paramLogEventParcelable.f.a();
    }
    if ((paramLogEventParcelable.g != null) && (paramLogEventParcelable.e.q.length == 0)) {
      paramLogEventParcelable.e.q = paramLogEventParcelable.g.a();
    }
    paramLogEventParcelable.c = zzsu.a(paramLogEventParcelable.e);
  }
  
  public PendingResult<Status> a(GoogleApiClient paramGoogleApiClient, LogEventParcelable paramLogEventParcelable)
  {
    b(paramLogEventParcelable);
    return paramGoogleApiClient.a(b(paramGoogleApiClient, paramLogEventParcelable));
  }
  
  public boolean a(GoogleApiClient paramGoogleApiClient, long paramLong, TimeUnit paramTimeUnit)
  {
    try
    {
      boolean bool = b.a(paramLong, paramTimeUnit);
      return bool;
    }
    catch (InterruptedException paramGoogleApiClient)
    {
      Log.e("ClearcutLoggerApiImpl", "flush interrupted");
      Thread.currentThread().interrupt();
    }
    return false;
  }
  
  public static abstract interface zza {}
  
  public static class zzb
    implements zzlv.zza
  {}
  
  static abstract class zzc<R extends Result>
    extends zza.zza<R, zzlw>
  {
    public zzc(GoogleApiClient paramGoogleApiClient)
    {
      super(paramGoogleApiClient);
    }
  }
  
  final class zzd
    extends zzlv.zzc<Status>
  {
    private final LogEventParcelable b;
    
    zzd(LogEventParcelable paramLogEventParcelable, GoogleApiClient paramGoogleApiClient)
    {
      super();
      this.b = paramLogEventParcelable;
    }
    
    protected Status a(Status paramStatus)
    {
      return paramStatus;
    }
    
    protected void a(zzlw paramzzlw)
      throws RemoteException
    {
      zzlx.zza local1 = new zzlx.zza()
      {
        public void a(Status paramAnonymousStatus)
        {
          zzlv.zzd.this.zza(paramAnonymousStatus);
        }
      };
      try
      {
        zzlv.a(this.b);
        paramzzlw.a(local1, this.b);
        return;
      }
      catch (Throwable paramzzlw)
      {
        Log.e("ClearcutLoggerApiImpl", "MessageNanoProducer " + this.b.f.toString() + " threw: " + paramzzlw.toString());
      }
    }
    
    public boolean equals(Object paramObject)
    {
      if (!(paramObject instanceof zzd)) {
        return false;
      }
      paramObject = (zzd)paramObject;
      return this.b.equals(((zzd)paramObject).b);
    }
    
    public String toString()
    {
      return "MethodImpl(" + this.b + ")";
    }
  }
  
  private static final class zze
  {
    private int a = 0;
    
    public void a()
    {
      try
      {
        this.a += 1;
        return;
      }
      finally
      {
        localObject = finally;
        throw ((Throwable)localObject);
      }
    }
    
    public boolean a(long paramLong, TimeUnit paramTimeUnit)
      throws InterruptedException
    {
      long l1 = System.currentTimeMillis();
      paramLong = TimeUnit.MILLISECONDS.convert(paramLong, paramTimeUnit);
      for (;;)
      {
        try
        {
          if (this.a == 0) {
            return true;
          }
          if (paramLong <= 0L) {
            return false;
          }
        }
        finally {}
        wait(paramLong);
        long l2 = System.currentTimeMillis();
        paramLong -= l2 - l1;
      }
    }
    
    public void b()
    {
      try
      {
        if (this.a == 0) {
          throw new RuntimeException("too many decrements");
        }
      }
      finally {}
      this.a -= 1;
      if (this.a == 0) {
        notifyAll();
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzlv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */