package com.google.android.gms.internal;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.google.android.gms.common.internal.zzx;

@zzhb
public class zzix
{
  private HandlerThread a = null;
  private Handler b = null;
  private int c = 0;
  private final Object d = new Object();
  
  public Looper a()
  {
    for (;;)
    {
      synchronized (this.d)
      {
        if (this.c == 0)
        {
          if (this.a == null)
          {
            zzin.e("Starting the looper thread.");
            this.a = new HandlerThread("LooperProvider");
            this.a.start();
            this.b = new Handler(this.a.getLooper());
            zzin.e("Looper thread started.");
            this.c += 1;
            Looper localLooper = this.a.getLooper();
            return localLooper;
          }
          zzin.e("Resuming the looper thread");
          this.d.notifyAll();
        }
      }
      zzx.a(this.a, "Invalid state: mHandlerThread should already been initialized.");
    }
  }
  
  public void b()
  {
    for (;;)
    {
      synchronized (this.d)
      {
        if (this.c > 0)
        {
          bool = true;
          zzx.b(bool, "Invalid state: release() called more times than expected.");
          int i = this.c - 1;
          this.c = i;
          if (i == 0) {
            this.b.post(new Runnable()
            {
              public void run()
              {
                synchronized (zzix.a(zzix.this))
                {
                  zzin.e("Suspending the looper thread");
                  for (;;)
                  {
                    int i = zzix.b(zzix.this);
                    if (i == 0) {
                      try
                      {
                        zzix.a(zzix.this).wait();
                        zzin.e("Looper thread resumed");
                      }
                      catch (InterruptedException localInterruptedException)
                      {
                        zzin.e("Looper thread interrupted.");
                      }
                    }
                  }
                }
              }
            });
          }
          return;
        }
      }
      boolean bool = false;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzix.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */