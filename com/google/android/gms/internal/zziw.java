package com.google.android.gms.internal;

import android.content.Context;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;

@zzhb
public class zziw
{
  public static final zza<Void> a = new zza()
  {
    public Void a()
    {
      return null;
    }
    
    public Void a(InputStream paramAnonymousInputStream)
    {
      return null;
    }
  };
  private static zzl b;
  private static final Object c = new Object();
  
  public zziw(Context paramContext)
  {
    b = a(paramContext);
  }
  
  private static zzl a(Context paramContext)
  {
    synchronized (c)
    {
      if (b == null) {
        b = zzac.a(paramContext.getApplicationContext());
      }
      paramContext = b;
      return paramContext;
    }
  }
  
  public <T> zzjg<T> a(String paramString, zza<T> paramzza)
  {
    zzc localzzc = new zzc(null);
    b.a(new zzb(paramString, paramzza, localzzc));
    return localzzc;
  }
  
  public zzjg<String> a(final String paramString, final Map<String, String> paramMap)
  {
    final zzc localzzc = new zzc(null);
    paramString = new zzab(paramString, localzzc, new zzm.zza()
    {
      public void a(zzr paramAnonymouszzr)
      {
        zzin.d("Failed to load URL: " + paramString + "\n" + paramAnonymouszzr.toString());
        localzzc.a(null);
      }
    })
    {
      public Map<String, String> a()
        throws zza
      {
        if (paramMap == null) {
          return super.a();
        }
        return paramMap;
      }
    };
    b.a(paramString);
    return localzzc;
  }
  
  public static abstract interface zza<T>
  {
    public abstract T b();
    
    public abstract T b(InputStream paramInputStream);
  }
  
  private static class zzb<T>
    extends zzk<InputStream>
  {
    private final zziw.zza<T> a;
    private final zzm.zzb<T> b;
    
    public zzb(String paramString, final zziw.zza<T> paramzza, zzm.zzb<T> paramzzb)
    {
      super(paramString, new zzm.zza()
      {
        public void a(zzr paramAnonymouszzr)
        {
          zziw.zzb.this.a(paramzza.b());
        }
      });
      this.a = paramzza;
      this.b = paramzzb;
    }
    
    protected zzm<InputStream> a(zzi paramzzi)
    {
      return zzm.a(new ByteArrayInputStream(paramzzi.b), zzx.a(paramzzi));
    }
    
    protected void a(InputStream paramInputStream)
    {
      this.b.a(this.a.b(paramInputStream));
    }
  }
  
  private class zzc<T>
    extends zzjd<T>
    implements zzm.zzb<T>
  {
    private zzc() {}
    
    public void a(T paramT)
    {
      super.b(paramT);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zziw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */