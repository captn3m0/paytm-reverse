package com.google.android.gms.internal;

import android.content.MutableContextWrapper;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.zzk;
import com.google.android.gms.ads.internal.zzr;
import com.google.android.gms.common.internal.zzx;
import java.util.Iterator;
import java.util.LinkedList;

@zzhb
class zzea
{
  private final LinkedList<zza> a;
  private AdRequestParcel b;
  private final String c;
  private final int d;
  
  zzea(AdRequestParcel paramAdRequestParcel, String paramString, int paramInt)
  {
    zzx.a(paramAdRequestParcel);
    zzx.a(paramString);
    this.a = new LinkedList();
    this.b = paramAdRequestParcel;
    this.c = paramString;
    this.d = paramInt;
  }
  
  AdRequestParcel a()
  {
    return this.b;
  }
  
  void a(zzdv paramzzdv)
  {
    paramzzdv = new zza(paramzzdv);
    this.a.add(paramzzdv);
    paramzzdv.a(this.b);
  }
  
  int b()
  {
    return this.d;
  }
  
  String c()
  {
    return this.c;
  }
  
  zza d()
  {
    return (zza)this.a.remove();
  }
  
  int e()
  {
    return this.a.size();
  }
  
  class zza
  {
    zzk a;
    MutableContextWrapper b;
    zzdw c;
    long d;
    boolean e;
    boolean f;
    
    zza(zzdv paramzzdv)
    {
      zzdv localzzdv = paramzzdv.a();
      this.b = paramzzdv.b();
      this.a = localzzdv.a(zzea.a(zzea.this));
      this.c = new zzdw();
      this.c.a(this.a);
    }
    
    private void a()
    {
      if ((!this.e) && (zzea.c(zzea.this) != null))
      {
        this.f = this.a.a(zzea.c(zzea.this));
        this.e = true;
        this.d = zzr.i().a();
      }
    }
    
    void a(AdRequestParcel paramAdRequestParcel)
    {
      if (paramAdRequestParcel != null) {
        zzea.a(zzea.this, paramAdRequestParcel);
      }
      a();
      paramAdRequestParcel = zzea.b(zzea.this).iterator();
      while (paramAdRequestParcel.hasNext()) {
        ((zza)paramAdRequestParcel.next()).a();
      }
    }
    
    void a(zzdv paramzzdv)
    {
      paramzzdv = paramzzdv.b().getBaseContext();
      this.b.setBaseContext(paramzzdv);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzea.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */