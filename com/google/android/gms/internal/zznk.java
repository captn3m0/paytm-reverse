package com.google.android.gms.internal;

import com.google.android.gms.common.internal.zzx;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class zznk
  implements ThreadFactory
{
  private final String a;
  private final int b;
  private final AtomicInteger c = new AtomicInteger();
  private final ThreadFactory d = Executors.defaultThreadFactory();
  
  public zznk(String paramString)
  {
    this(paramString, 0);
  }
  
  public zznk(String paramString, int paramInt)
  {
    this.a = ((String)zzx.a(paramString, "Name must not be null"));
    this.b = paramInt;
  }
  
  public Thread newThread(Runnable paramRunnable)
  {
    paramRunnable = this.d.newThread(new zznl(paramRunnable, this.b));
    paramRunnable.setName(this.a + "[" + this.c.getAndIncrement() + "]");
    return paramRunnable;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zznk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */