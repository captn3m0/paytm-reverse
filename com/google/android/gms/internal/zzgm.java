package com.google.android.gms.internal;

import com.google.android.gms.ads.purchase.PlayStorePurchaseListener;

@zzhb
public final class zzgm
  extends zzgh.zza
{
  private final PlayStorePurchaseListener a;
  
  public zzgm(PlayStorePurchaseListener paramPlayStorePurchaseListener)
  {
    this.a = paramPlayStorePurchaseListener;
  }
  
  public void a(zzgg paramzzgg)
  {
    this.a.a(new zzgk(paramzzgg));
  }
  
  public boolean a(String paramString)
  {
    return this.a.a(paramString);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzgm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */