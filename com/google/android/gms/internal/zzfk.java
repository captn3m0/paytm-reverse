package com.google.android.gms.internal;

import android.os.Handler;
import android.os.RemoteException;
import com.google.ads.a.a;
import com.google.ads.mediation.d;
import com.google.ads.mediation.e;
import com.google.ads.mediation.f;
import com.google.ads.mediation.g;
import com.google.ads.mediation.h;
import com.google.ads.mediation.i;
import com.google.android.gms.ads.internal.client.zzn;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;

@zzhb
public final class zzfk<NETWORK_EXTRAS extends i, SERVER_PARAMETERS extends h>
  implements e, g
{
  private final zzez a;
  
  public zzfk(zzez paramzzez)
  {
    this.a = paramzzez;
  }
  
  public void a(d<?, ?> paramd, final a.a parama)
  {
    zzb.a("Adapter called onFailedToReceiveAd with error. " + parama);
    if (!zzn.a().b())
    {
      zzb.d("onFailedToReceiveAd must be called on the main UI thread.");
      zza.a.post(new Runnable()
      {
        public void run()
        {
          try
          {
            zzfk.a(zzfk.this).a(zzfl.a(parama));
            return;
          }
          catch (RemoteException localRemoteException)
          {
            zzb.d("Could not call onAdFailedToLoad.", localRemoteException);
          }
        }
      });
      return;
    }
    try
    {
      this.a.a(zzfl.a(parama));
      return;
    }
    catch (RemoteException paramd)
    {
      zzb.d("Could not call onAdFailedToLoad.", paramd);
    }
  }
  
  public void a(f<?, ?> paramf, final a.a parama)
  {
    zzb.a("Adapter called onFailedToReceiveAd with error " + parama + ".");
    if (!zzn.a().b())
    {
      zzb.d("onFailedToReceiveAd must be called on the main UI thread.");
      zza.a.post(new Runnable()
      {
        public void run()
        {
          try
          {
            zzfk.a(zzfk.this).a(zzfl.a(parama));
            return;
          }
          catch (RemoteException localRemoteException)
          {
            zzb.d("Could not call onAdFailedToLoad.", localRemoteException);
          }
        }
      });
      return;
    }
    try
    {
      this.a.a(zzfl.a(parama));
      return;
    }
    catch (RemoteException paramf)
    {
      zzb.d("Could not call onAdFailedToLoad.", paramf);
    }
  }
  
  public void onClick(d<?, ?> paramd)
  {
    zzb.a("Adapter called onClick.");
    if (!zzn.a().b())
    {
      zzb.d("onClick must be called on the main UI thread.");
      zza.a.post(new Runnable()
      {
        public void run()
        {
          try
          {
            zzfk.a(zzfk.this).a();
            return;
          }
          catch (RemoteException localRemoteException)
          {
            zzb.d("Could not call onAdClicked.", localRemoteException);
          }
        }
      });
      return;
    }
    try
    {
      this.a.a();
      return;
    }
    catch (RemoteException paramd)
    {
      zzb.d("Could not call onAdClicked.", paramd);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzfk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */