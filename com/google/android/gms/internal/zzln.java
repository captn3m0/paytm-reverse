package com.google.android.gms.internal;

import com.google.android.gms.cast.games.PlayerInfo;
import com.google.android.gms.cast.internal.zzf;
import com.google.android.gms.common.internal.zzw;
import org.json.JSONObject;

public final class zzln
  implements PlayerInfo
{
  private final String a;
  private final int b;
  private final JSONObject c;
  private final boolean d;
  
  public zzln(String paramString, int paramInt, JSONObject paramJSONObject, boolean paramBoolean)
  {
    this.a = paramString;
    this.b = paramInt;
    this.c = paramJSONObject;
    this.d = paramBoolean;
  }
  
  public int a()
  {
    return this.b;
  }
  
  public JSONObject b()
  {
    return this.c;
  }
  
  public String c()
  {
    return this.a;
  }
  
  public boolean d()
  {
    return this.d;
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject == null) || (!(paramObject instanceof PlayerInfo))) {}
    do
    {
      return false;
      paramObject = (PlayerInfo)paramObject;
    } while ((this.d != ((PlayerInfo)paramObject).d()) || (this.b != ((PlayerInfo)paramObject).a()) || (!zzf.a(this.a, ((PlayerInfo)paramObject).c())) || (!zznb.a(this.c, ((PlayerInfo)paramObject).b())));
    return true;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.a, Integer.valueOf(this.b), this.c, Boolean.valueOf(this.d) });
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzln.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */