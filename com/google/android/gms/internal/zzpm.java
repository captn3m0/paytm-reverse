package com.google.android.gms.internal;

import java.io.IOException;

public abstract interface zzpm
{
  public static final class zza
    extends zzso<zza>
  {
    public zza[] a;
    
    public zza()
    {
      a();
    }
    
    public zza a()
    {
      this.a = zza.a();
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public zza a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 10: 
          int j = zzsx.b(paramzzsm, 10);
          if (this.a == null) {}
          zza[] arrayOfzza;
          for (i = 0;; i = this.a.length)
          {
            arrayOfzza = new zza[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.a, 0, arrayOfzza, 0, i);
              j = i;
            }
            while (j < arrayOfzza.length - 1)
            {
              arrayOfzza[j] = new zza();
              paramzzsm.a(arrayOfzza[j]);
              paramzzsm.a();
              j += 1;
            }
          }
          arrayOfzza[j] = new zza();
          paramzzsm.a(arrayOfzza[j]);
          this.a = arrayOfzza;
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if ((this.a != null) && (this.a.length > 0))
      {
        int i = 0;
        while (i < this.a.length)
        {
          zza localzza = this.a[i];
          if (localzza != null) {
            paramzzsn.a(1, localzza);
          }
          i += 1;
        }
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int i = super.b();
      int k = i;
      if (this.a != null)
      {
        k = i;
        if (this.a.length > 0)
        {
          int j = 0;
          for (;;)
          {
            k = i;
            if (j >= this.a.length) {
              break;
            }
            zza localzza = this.a[j];
            k = i;
            if (localzza != null) {
              k = i + zzsn.c(1, localzza);
            }
            j += 1;
            i = k;
          }
        }
      }
      return k;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      do
      {
        do
        {
          do
          {
            return bool1;
            bool1 = bool2;
          } while (!(paramObject instanceof zza));
          paramObject = (zza)paramObject;
          bool1 = bool2;
        } while (!zzss.a(this.a, ((zza)paramObject).a));
        if ((this.r != null) && (!this.r.b())) {
          break label79;
        }
        if (((zza)paramObject).r == null) {
          break;
        }
        bool1 = bool2;
      } while (!((zza)paramObject).r.b());
      return true;
      label79:
      return this.r.equals(((zza)paramObject).r);
    }
    
    public int hashCode()
    {
      int j = getClass().getName().hashCode();
      int k = zzss.a(this.a);
      if ((this.r == null) || (this.r.b())) {}
      for (int i = 0;; i = this.r.hashCode()) {
        return i + ((j + 527) * 31 + k) * 31;
      }
    }
    
    public static final class zza
      extends zzso<zza>
    {
      private static volatile zza[] d;
      public String a;
      public String b;
      public int c;
      
      public zza()
      {
        c();
      }
      
      public static zza[] a()
      {
        if (d == null) {}
        synchronized (zzss.a)
        {
          if (d == null) {
            d = new zza[0];
          }
          return d;
        }
      }
      
      public zza a(zzsm paramzzsm)
        throws IOException
      {
        for (;;)
        {
          int i = paramzzsm.a();
          switch (i)
          {
          default: 
            if (a(paramzzsm, i)) {}
            break;
          case 0: 
            return this;
          case 10: 
            this.a = paramzzsm.i();
            break;
          case 18: 
            this.b = paramzzsm.i();
            break;
          case 24: 
            this.c = paramzzsm.g();
          }
        }
      }
      
      public void a(zzsn paramzzsn)
        throws IOException
      {
        if (!this.a.equals("")) {
          paramzzsn.a(1, this.a);
        }
        if (!this.b.equals("")) {
          paramzzsn.a(2, this.b);
        }
        if (this.c != 0) {
          paramzzsn.a(3, this.c);
        }
        super.a(paramzzsn);
      }
      
      protected int b()
      {
        int j = super.b();
        int i = j;
        if (!this.a.equals("")) {
          i = j + zzsn.b(1, this.a);
        }
        j = i;
        if (!this.b.equals("")) {
          j = i + zzsn.b(2, this.b);
        }
        i = j;
        if (this.c != 0) {
          i = j + zzsn.c(3, this.c);
        }
        return i;
      }
      
      public zza c()
      {
        this.a = "";
        this.b = "";
        this.c = 0;
        this.r = null;
        this.S = -1;
        return this;
      }
      
      public boolean equals(Object paramObject)
      {
        boolean bool2 = false;
        boolean bool1;
        if (paramObject == this) {
          bool1 = true;
        }
        label41:
        do
        {
          do
          {
            do
            {
              return bool1;
              bool1 = bool2;
            } while (!(paramObject instanceof zza));
            paramObject = (zza)paramObject;
            if (this.a != null) {
              break;
            }
            bool1 = bool2;
          } while (((zza)paramObject).a != null);
          if (this.b != null) {
            break label124;
          }
          bool1 = bool2;
        } while (((zza)paramObject).b != null);
        label124:
        while (this.b.equals(((zza)paramObject).b))
        {
          bool1 = bool2;
          if (this.c != ((zza)paramObject).c) {
            break;
          }
          if ((this.r != null) && (!this.r.b())) {
            break label140;
          }
          if (((zza)paramObject).r != null)
          {
            bool1 = bool2;
            if (!((zza)paramObject).r.b()) {
              break;
            }
          }
          return true;
          if (this.a.equals(((zza)paramObject).a)) {
            break label41;
          }
          return false;
        }
        return false;
        label140:
        return this.r.equals(((zza)paramObject).r);
      }
      
      public int hashCode()
      {
        int m = 0;
        int n = getClass().getName().hashCode();
        int i;
        int j;
        label33:
        int i1;
        if (this.a == null)
        {
          i = 0;
          if (this.b != null) {
            break label101;
          }
          j = 0;
          i1 = this.c;
          k = m;
          if (this.r != null) {
            if (!this.r.b()) {
              break label112;
            }
          }
        }
        label101:
        label112:
        for (int k = m;; k = this.r.hashCode())
        {
          return ((j + (i + (n + 527) * 31) * 31) * 31 + i1) * 31 + k;
          i = this.a.hashCode();
          break;
          j = this.b.hashCode();
          break label33;
        }
      }
    }
  }
  
  public static final class zzb
    extends zzso<zzb>
  {
    private static volatile zzb[] c;
    public String a;
    public zzpm.zzd b;
    
    public zzb()
    {
      c();
    }
    
    public static zzb[] a()
    {
      if (c == null) {}
      synchronized (zzss.a)
      {
        if (c == null) {
          c = new zzb[0];
        }
        return c;
      }
    }
    
    public zzb a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 10: 
          this.a = paramzzsm.i();
          break;
        case 18: 
          if (this.b == null) {
            this.b = new zzpm.zzd();
          }
          paramzzsm.a(this.b);
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (!this.a.equals("")) {
        paramzzsn.a(1, this.a);
      }
      if (this.b != null) {
        paramzzsn.a(2, this.b);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b();
      int i = j;
      if (!this.a.equals("")) {
        i = j + zzsn.b(1, this.a);
      }
      j = i;
      if (this.b != null) {
        j = i + zzsn.c(2, this.b);
      }
      return j;
    }
    
    public zzb c()
    {
      this.a = "";
      this.b = null;
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      label41:
      do
      {
        do
        {
          do
          {
            return bool1;
            bool1 = bool2;
          } while (!(paramObject instanceof zzb));
          paramObject = (zzb)paramObject;
          if (this.a != null) {
            break;
          }
          bool1 = bool2;
        } while (((zzb)paramObject).a != null);
        if (this.b != null) {
          break label111;
        }
        bool1 = bool2;
      } while (((zzb)paramObject).b != null);
      for (;;)
      {
        if ((this.r == null) || (this.r.b()))
        {
          if (((zzb)paramObject).r != null)
          {
            bool1 = bool2;
            if (!((zzb)paramObject).r.b()) {
              break;
            }
          }
          return true;
          if (this.a.equals(((zzb)paramObject).a)) {
            break label41;
          }
          return false;
          label111:
          if (!this.b.equals(((zzb)paramObject).b)) {
            return false;
          }
        }
      }
      return this.r.equals(((zzb)paramObject).r);
    }
    
    public int hashCode()
    {
      int m = 0;
      int n = getClass().getName().hashCode();
      int i;
      int j;
      if (this.a == null)
      {
        i = 0;
        if (this.b != null) {
          break label89;
        }
        j = 0;
        label33:
        k = m;
        if (this.r != null) {
          if (!this.r.b()) {
            break label100;
          }
        }
      }
      label89:
      label100:
      for (int k = m;; k = this.r.hashCode())
      {
        return (j + (i + (n + 527) * 31) * 31) * 31 + k;
        i = this.a.hashCode();
        break;
        j = this.b.hashCode();
        break label33;
      }
    }
  }
  
  public static final class zzc
    extends zzso<zzc>
  {
    public String a;
    public zzpm.zzb[] b;
    
    public zzc()
    {
      a();
    }
    
    public zzc a()
    {
      this.a = "";
      this.b = zzpm.zzb.a();
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public zzc a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 10: 
          this.a = paramzzsm.i();
          break;
        case 18: 
          int j = zzsx.b(paramzzsm, 18);
          if (this.b == null) {}
          zzpm.zzb[] arrayOfzzb;
          for (i = 0;; i = this.b.length)
          {
            arrayOfzzb = new zzpm.zzb[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.b, 0, arrayOfzzb, 0, i);
              j = i;
            }
            while (j < arrayOfzzb.length - 1)
            {
              arrayOfzzb[j] = new zzpm.zzb();
              paramzzsm.a(arrayOfzzb[j]);
              paramzzsm.a();
              j += 1;
            }
          }
          arrayOfzzb[j] = new zzpm.zzb();
          paramzzsm.a(arrayOfzzb[j]);
          this.b = arrayOfzzb;
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (!this.a.equals("")) {
        paramzzsn.a(1, this.a);
      }
      if ((this.b != null) && (this.b.length > 0))
      {
        int i = 0;
        while (i < this.b.length)
        {
          zzpm.zzb localzzb = this.b[i];
          if (localzzb != null) {
            paramzzsn.a(2, localzzb);
          }
          i += 1;
        }
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b();
      int i = j;
      if (!this.a.equals("")) {
        i = j + zzsn.b(1, this.a);
      }
      j = i;
      if (this.b != null)
      {
        j = i;
        if (this.b.length > 0)
        {
          j = 0;
          while (j < this.b.length)
          {
            zzpm.zzb localzzb = this.b[j];
            int k = i;
            if (localzzb != null) {
              k = i + zzsn.c(2, localzzb);
            }
            j += 1;
            i = k;
          }
          j = i;
        }
      }
      return j;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      do
      {
        do
        {
          return bool1;
          bool1 = bool2;
        } while (!(paramObject instanceof zzc));
        paramObject = (zzc)paramObject;
        if (this.a != null) {
          break;
        }
        bool1 = bool2;
      } while (((zzc)paramObject).a != null);
      while (this.a.equals(((zzc)paramObject).a))
      {
        bool1 = bool2;
        if (!zzss.a(this.b, ((zzc)paramObject).b)) {
          break;
        }
        if ((this.r != null) && (!this.r.b())) {
          break label111;
        }
        if (((zzc)paramObject).r != null)
        {
          bool1 = bool2;
          if (!((zzc)paramObject).r.b()) {
            break;
          }
        }
        return true;
      }
      return false;
      label111:
      return this.r.equals(((zzc)paramObject).r);
    }
    
    public int hashCode()
    {
      int k = 0;
      int m = getClass().getName().hashCode();
      int i;
      int n;
      if (this.a == null)
      {
        i = 0;
        n = zzss.a(this.b);
        j = k;
        if (this.r != null) {
          if (!this.r.b()) {
            break label87;
          }
        }
      }
      label87:
      for (int j = k;; j = this.r.hashCode())
      {
        return ((i + (m + 527) * 31) * 31 + n) * 31 + j;
        i = this.a.hashCode();
        break;
      }
    }
  }
  
  public static final class zzd
    extends zzso<zzd>
  {
    public boolean a;
    public String b;
    public long c;
    public double d;
    public zzpm.zzc e;
    
    public zzd()
    {
      a();
    }
    
    public zzd a()
    {
      this.a = false;
      this.b = "";
      this.c = 0L;
      this.d = 0.0D;
      this.e = null;
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public zzd a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 8: 
          this.a = paramzzsm.h();
          break;
        case 18: 
          this.b = paramzzsm.i();
          break;
        case 24: 
          this.c = paramzzsm.f();
          break;
        case 33: 
          this.d = paramzzsm.c();
          break;
        case 42: 
          if (this.e == null) {
            this.e = new zzpm.zzc();
          }
          paramzzsm.a(this.e);
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (this.a) {
        paramzzsn.a(1, this.a);
      }
      if (!this.b.equals("")) {
        paramzzsn.a(2, this.b);
      }
      if (this.c != 0L) {
        paramzzsn.b(3, this.c);
      }
      if (Double.doubleToLongBits(this.d) != Double.doubleToLongBits(0.0D)) {
        paramzzsn.a(4, this.d);
      }
      if (this.e != null) {
        paramzzsn.a(5, this.e);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b();
      int i = j;
      if (this.a) {
        i = j + zzsn.b(1, this.a);
      }
      j = i;
      if (!this.b.equals("")) {
        j = i + zzsn.b(2, this.b);
      }
      i = j;
      if (this.c != 0L) {
        i = j + zzsn.d(3, this.c);
      }
      j = i;
      if (Double.doubleToLongBits(this.d) != Double.doubleToLongBits(0.0D)) {
        j = i + zzsn.b(4, this.d);
      }
      i = j;
      if (this.e != null) {
        i = j + zzsn.c(5, this.e);
      }
      return i;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      label54:
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  return bool1;
                  bool1 = bool2;
                } while (!(paramObject instanceof zzd));
                paramObject = (zzd)paramObject;
                bool1 = bool2;
              } while (this.a != ((zzd)paramObject).a);
              if (this.b != null) {
                break;
              }
              bool1 = bool2;
            } while (((zzd)paramObject).b != null);
            bool1 = bool2;
          } while (this.c != ((zzd)paramObject).c);
          bool1 = bool2;
        } while (Double.doubleToLongBits(this.d) != Double.doubleToLongBits(((zzd)paramObject).d));
        if (this.e != null) {
          break label158;
        }
        bool1 = bool2;
      } while (((zzd)paramObject).e != null);
      for (;;)
      {
        if ((this.r == null) || (this.r.b()))
        {
          if (((zzd)paramObject).r != null)
          {
            bool1 = bool2;
            if (!((zzd)paramObject).r.b()) {
              break;
            }
          }
          return true;
          if (this.b.equals(((zzd)paramObject).b)) {
            break label54;
          }
          return false;
          label158:
          if (!this.e.equals(((zzd)paramObject).e)) {
            return false;
          }
        }
      }
      return this.r.equals(((zzd)paramObject).r);
    }
    
    public int hashCode()
    {
      int n = 0;
      int i1 = getClass().getName().hashCode();
      int i;
      int j;
      label35:
      int i2;
      int i3;
      int k;
      if (this.a)
      {
        i = 1231;
        if (this.b != null) {
          break label151;
        }
        j = 0;
        i2 = (int)(this.c ^ this.c >>> 32);
        long l = Double.doubleToLongBits(this.d);
        i3 = (int)(l ^ l >>> 32);
        if (this.e != null) {
          break label162;
        }
        k = 0;
        label79:
        m = n;
        if (this.r != null) {
          if (!this.r.b()) {
            break label173;
          }
        }
      }
      label151:
      label162:
      label173:
      for (int m = n;; m = this.r.hashCode())
      {
        return (k + (((j + (i + (i1 + 527) * 31) * 31) * 31 + i2) * 31 + i3) * 31) * 31 + m;
        i = 1237;
        break;
        j = this.b.hashCode();
        break label35;
        k = this.e.hashCode();
        break label79;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzpm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */