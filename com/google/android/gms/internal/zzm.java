package com.google.android.gms.internal;

public class zzm<T>
{
  public final T a;
  public final zzb.zza b;
  public final zzr c;
  public boolean d = false;
  
  private zzm(zzr paramzzr)
  {
    this.a = null;
    this.b = null;
    this.c = paramzzr;
  }
  
  private zzm(T paramT, zzb.zza paramzza)
  {
    this.a = paramT;
    this.b = paramzza;
    this.c = null;
  }
  
  public static <T> zzm<T> a(zzr paramzzr)
  {
    return new zzm(paramzzr);
  }
  
  public static <T> zzm<T> a(T paramT, zzb.zza paramzza)
  {
    return new zzm(paramT, paramzza);
  }
  
  public boolean a()
  {
    return this.c == null;
  }
  
  public static abstract interface zza
  {
    public abstract void a(zzr paramzzr);
  }
  
  public static abstract interface zzb<T>
  {
    public abstract void a(T paramT);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */