package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import java.util.Map;

public class zzdn
  implements zzdf
{
  private final zza a;
  
  public zzdn(zza paramzza)
  {
    this.a = paramzza;
  }
  
  public static void a(zzjp paramzzjp, zza paramzza)
  {
    paramzzjp.l().a("/reward", new zzdn(paramzza));
  }
  
  private void a(Map<String, String> paramMap)
  {
    try
    {
      int i = Integer.parseInt((String)paramMap.get("amount"));
      paramMap = (String)paramMap.get("type");
      if (!TextUtils.isEmpty(paramMap))
      {
        paramMap = new RewardItemParcel(paramMap, i);
        this.a.b(paramMap);
        return;
      }
    }
    catch (NumberFormatException paramMap)
    {
      for (;;)
      {
        zzin.d("Unable to parse reward amount.", paramMap);
        paramMap = null;
      }
    }
  }
  
  private void b(Map<String, String> paramMap)
  {
    this.a.E();
  }
  
  public void a(zzjp paramzzjp, Map<String, String> paramMap)
  {
    paramzzjp = (String)paramMap.get("action");
    if ("grant".equals(paramzzjp)) {
      a(paramMap);
    }
    while (!"video_start".equals(paramzzjp)) {
      return;
    }
    b(paramMap);
  }
  
  public static abstract interface zza
  {
    public abstract void E();
    
    public abstract void b(RewardItemParcel paramRewardItemParcel);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzdn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */