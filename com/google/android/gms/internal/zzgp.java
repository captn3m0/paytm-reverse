package com.google.android.gms.internal;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.internal.client.AdSizeParcel;

@zzhb
public class zzgp
  extends zzgn
{
  private zzgo g;
  
  zzgp(Context paramContext, zzif.zza paramzza, zzjp paramzzjp, zzgr.zza paramzza1)
  {
    super(paramContext, paramzza, paramzzjp, paramzza1);
  }
  
  protected void b()
  {
    Object localObject = this.c.k();
    int j;
    if (((AdSizeParcel)localObject).e)
    {
      localObject = this.b.getResources().getDisplayMetrics();
      j = ((DisplayMetrics)localObject).widthPixels;
    }
    for (int i = ((DisplayMetrics)localObject).heightPixels;; i = ((AdSizeParcel)localObject).d)
    {
      this.g = new zzgo(this, this.c, j, i);
      this.c.l().a(this);
      this.g.a(this.e);
      return;
      j = ((AdSizeParcel)localObject).g;
    }
  }
  
  protected int c()
  {
    if (this.g.c())
    {
      zzin.a("Ad-Network indicated no fill with passback URL.");
      return 3;
    }
    if (!this.g.d()) {
      return 2;
    }
    return -2;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzgp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */