package com.google.android.gms.internal;

import java.io.IOException;

public abstract interface zzpz
{
  public static final class zza
    extends zzsu
  {
    private static volatile zza[] d;
    public Integer a;
    public zzpz.zze[] b;
    public zzpz.zzb[] c;
    
    public zza()
    {
      c();
    }
    
    public static zza[] a()
    {
      if (d == null) {}
      synchronized (zzss.a)
      {
        if (d == null) {
          d = new zza[0];
        }
        return d;
      }
    }
    
    public zza a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        int j;
        Object localObject;
        switch (i)
        {
        default: 
          if (zzsx.a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 8: 
          this.a = Integer.valueOf(paramzzsm.g());
          break;
        case 18: 
          j = zzsx.b(paramzzsm, 18);
          if (this.b == null) {}
          for (i = 0;; i = this.b.length)
          {
            localObject = new zzpz.zze[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.b, 0, localObject, 0, i);
              j = i;
            }
            while (j < localObject.length - 1)
            {
              localObject[j] = new zzpz.zze();
              paramzzsm.a(localObject[j]);
              paramzzsm.a();
              j += 1;
            }
          }
          localObject[j] = new zzpz.zze();
          paramzzsm.a(localObject[j]);
          this.b = ((zzpz.zze[])localObject);
          break;
        case 26: 
          j = zzsx.b(paramzzsm, 26);
          if (this.c == null) {}
          for (i = 0;; i = this.c.length)
          {
            localObject = new zzpz.zzb[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.c, 0, localObject, 0, i);
              j = i;
            }
            while (j < localObject.length - 1)
            {
              localObject[j] = new zzpz.zzb();
              paramzzsm.a(localObject[j]);
              paramzzsm.a();
              j += 1;
            }
          }
          localObject[j] = new zzpz.zzb();
          paramzzsm.a(localObject[j]);
          this.c = ((zzpz.zzb[])localObject);
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      int j = 0;
      if (this.a != null) {
        paramzzsn.a(1, this.a.intValue());
      }
      int i;
      Object localObject;
      if ((this.b != null) && (this.b.length > 0))
      {
        i = 0;
        while (i < this.b.length)
        {
          localObject = this.b[i];
          if (localObject != null) {
            paramzzsn.a(2, (zzsu)localObject);
          }
          i += 1;
        }
      }
      if ((this.c != null) && (this.c.length > 0))
      {
        i = j;
        while (i < this.c.length)
        {
          localObject = this.c[i];
          if (localObject != null) {
            paramzzsn.a(3, (zzsu)localObject);
          }
          i += 1;
        }
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int m = 0;
      int i = super.b();
      int j = i;
      if (this.a != null) {
        j = i + zzsn.c(1, this.a.intValue());
      }
      i = j;
      Object localObject;
      if (this.b != null)
      {
        i = j;
        if (this.b.length > 0)
        {
          i = j;
          j = 0;
          while (j < this.b.length)
          {
            localObject = this.b[j];
            k = i;
            if (localObject != null) {
              k = i + zzsn.c(2, (zzsu)localObject);
            }
            j += 1;
            i = k;
          }
        }
      }
      int k = i;
      if (this.c != null)
      {
        k = i;
        if (this.c.length > 0)
        {
          j = m;
          for (;;)
          {
            k = i;
            if (j >= this.c.length) {
              break;
            }
            localObject = this.c[j];
            k = i;
            if (localObject != null) {
              k = i + zzsn.c(3, (zzsu)localObject);
            }
            j += 1;
            i = k;
          }
        }
      }
      return k;
    }
    
    public zza c()
    {
      this.a = null;
      this.b = zzpz.zze.a();
      this.c = zzpz.zzb.a();
      this.S = -1;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      if (paramObject == this) {}
      do
      {
        return true;
        if (!(paramObject instanceof zza)) {
          return false;
        }
        paramObject = (zza)paramObject;
        if (this.a == null)
        {
          if (((zza)paramObject).a != null) {
            return false;
          }
        }
        else if (!this.a.equals(((zza)paramObject).a)) {
          return false;
        }
        if (!zzss.a(this.b, ((zza)paramObject).b)) {
          return false;
        }
      } while (zzss.a(this.c, ((zza)paramObject).c));
      return false;
    }
    
    public int hashCode()
    {
      int j = getClass().getName().hashCode();
      if (this.a == null) {}
      for (int i = 0;; i = this.a.hashCode()) {
        return ((i + (j + 527) * 31) * 31 + zzss.a(this.b)) * 31 + zzss.a(this.c);
      }
    }
  }
  
  public static final class zzb
    extends zzsu
  {
    private static volatile zzb[] f;
    public Integer a;
    public String b;
    public zzpz.zzc[] c;
    public Boolean d;
    public zzpz.zzd e;
    
    public zzb()
    {
      c();
    }
    
    public static zzb[] a()
    {
      if (f == null) {}
      synchronized (zzss.a)
      {
        if (f == null) {
          f = new zzb[0];
        }
        return f;
      }
    }
    
    public zzb a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (zzsx.a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 8: 
          this.a = Integer.valueOf(paramzzsm.g());
          break;
        case 18: 
          this.b = paramzzsm.i();
          break;
        case 26: 
          int j = zzsx.b(paramzzsm, 26);
          if (this.c == null) {}
          zzpz.zzc[] arrayOfzzc;
          for (i = 0;; i = this.c.length)
          {
            arrayOfzzc = new zzpz.zzc[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.c, 0, arrayOfzzc, 0, i);
              j = i;
            }
            while (j < arrayOfzzc.length - 1)
            {
              arrayOfzzc[j] = new zzpz.zzc();
              paramzzsm.a(arrayOfzzc[j]);
              paramzzsm.a();
              j += 1;
            }
          }
          arrayOfzzc[j] = new zzpz.zzc();
          paramzzsm.a(arrayOfzzc[j]);
          this.c = arrayOfzzc;
          break;
        case 32: 
          this.d = Boolean.valueOf(paramzzsm.h());
          break;
        case 42: 
          if (this.e == null) {
            this.e = new zzpz.zzd();
          }
          paramzzsm.a(this.e);
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (this.a != null) {
        paramzzsn.a(1, this.a.intValue());
      }
      if (this.b != null) {
        paramzzsn.a(2, this.b);
      }
      if ((this.c != null) && (this.c.length > 0))
      {
        int i = 0;
        while (i < this.c.length)
        {
          zzpz.zzc localzzc = this.c[i];
          if (localzzc != null) {
            paramzzsn.a(3, localzzc);
          }
          i += 1;
        }
      }
      if (this.d != null) {
        paramzzsn.a(4, this.d.booleanValue());
      }
      if (this.e != null) {
        paramzzsn.a(5, this.e);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int i = super.b();
      int j = i;
      if (this.a != null) {
        j = i + zzsn.c(1, this.a.intValue());
      }
      i = j;
      if (this.b != null) {
        i = j + zzsn.b(2, this.b);
      }
      j = i;
      if (this.c != null)
      {
        j = i;
        if (this.c.length > 0)
        {
          j = 0;
          while (j < this.c.length)
          {
            zzpz.zzc localzzc = this.c[j];
            int k = i;
            if (localzzc != null) {
              k = i + zzsn.c(3, localzzc);
            }
            j += 1;
            i = k;
          }
          j = i;
        }
      }
      i = j;
      if (this.d != null) {
        i = j + zzsn.b(4, this.d.booleanValue());
      }
      j = i;
      if (this.e != null) {
        j = i + zzsn.c(5, this.e);
      }
      return j;
    }
    
    public zzb c()
    {
      this.a = null;
      this.b = null;
      this.c = zzpz.zzc.a();
      this.d = null;
      this.e = null;
      this.S = -1;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      if (paramObject == this) {}
      do
      {
        do
        {
          return true;
          if (!(paramObject instanceof zzb)) {
            return false;
          }
          paramObject = (zzb)paramObject;
          if (this.a == null)
          {
            if (((zzb)paramObject).a != null) {
              return false;
            }
          }
          else if (!this.a.equals(((zzb)paramObject).a)) {
            return false;
          }
          if (this.b == null)
          {
            if (((zzb)paramObject).b != null) {
              return false;
            }
          }
          else if (!this.b.equals(((zzb)paramObject).b)) {
            return false;
          }
          if (!zzss.a(this.c, ((zzb)paramObject).c)) {
            return false;
          }
          if (this.d == null)
          {
            if (((zzb)paramObject).d != null) {
              return false;
            }
          }
          else if (!this.d.equals(((zzb)paramObject).d)) {
            return false;
          }
          if (this.e != null) {
            break;
          }
        } while (((zzb)paramObject).e == null);
        return false;
      } while (this.e.equals(((zzb)paramObject).e));
      return false;
    }
    
    public int hashCode()
    {
      int m = 0;
      int n = getClass().getName().hashCode();
      int i;
      int j;
      label33:
      int i1;
      int k;
      if (this.a == null)
      {
        i = 0;
        if (this.b != null) {
          break label103;
        }
        j = 0;
        i1 = zzss.a(this.c);
        if (this.d != null) {
          break label114;
        }
        k = 0;
        label51:
        if (this.e != null) {
          break label125;
        }
      }
      for (;;)
      {
        return (k + ((j + (i + (n + 527) * 31) * 31) * 31 + i1) * 31) * 31 + m;
        i = this.a.hashCode();
        break;
        label103:
        j = this.b.hashCode();
        break label33;
        label114:
        k = this.d.hashCode();
        break label51;
        label125:
        m = this.e.hashCode();
      }
    }
  }
  
  public static final class zzc
    extends zzsu
  {
    private static volatile zzc[] e;
    public zzpz.zzf a;
    public zzpz.zzd b;
    public Boolean c;
    public String d;
    
    public zzc()
    {
      c();
    }
    
    public static zzc[] a()
    {
      if (e == null) {}
      synchronized (zzss.a)
      {
        if (e == null) {
          e = new zzc[0];
        }
        return e;
      }
    }
    
    public zzc a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (zzsx.a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 10: 
          if (this.a == null) {
            this.a = new zzpz.zzf();
          }
          paramzzsm.a(this.a);
          break;
        case 18: 
          if (this.b == null) {
            this.b = new zzpz.zzd();
          }
          paramzzsm.a(this.b);
          break;
        case 24: 
          this.c = Boolean.valueOf(paramzzsm.h());
          break;
        case 34: 
          this.d = paramzzsm.i();
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (this.a != null) {
        paramzzsn.a(1, this.a);
      }
      if (this.b != null) {
        paramzzsn.a(2, this.b);
      }
      if (this.c != null) {
        paramzzsn.a(3, this.c.booleanValue());
      }
      if (this.d != null) {
        paramzzsn.a(4, this.d);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b();
      int i = j;
      if (this.a != null) {
        i = j + zzsn.c(1, this.a);
      }
      j = i;
      if (this.b != null) {
        j = i + zzsn.c(2, this.b);
      }
      i = j;
      if (this.c != null) {
        i = j + zzsn.b(3, this.c.booleanValue());
      }
      j = i;
      if (this.d != null) {
        j = i + zzsn.b(4, this.d);
      }
      return j;
    }
    
    public zzc c()
    {
      this.a = null;
      this.b = null;
      this.c = null;
      this.d = null;
      this.S = -1;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      if (paramObject == this) {}
      do
      {
        do
        {
          return true;
          if (!(paramObject instanceof zzc)) {
            return false;
          }
          paramObject = (zzc)paramObject;
          if (this.a == null)
          {
            if (((zzc)paramObject).a != null) {
              return false;
            }
          }
          else if (!this.a.equals(((zzc)paramObject).a)) {
            return false;
          }
          if (this.b == null)
          {
            if (((zzc)paramObject).b != null) {
              return false;
            }
          }
          else if (!this.b.equals(((zzc)paramObject).b)) {
            return false;
          }
          if (this.c == null)
          {
            if (((zzc)paramObject).c != null) {
              return false;
            }
          }
          else if (!this.c.equals(((zzc)paramObject).c)) {
            return false;
          }
          if (this.d != null) {
            break;
          }
        } while (((zzc)paramObject).d == null);
        return false;
      } while (this.d.equals(((zzc)paramObject).d));
      return false;
    }
    
    public int hashCode()
    {
      int m = 0;
      int n = getClass().getName().hashCode();
      int i;
      int j;
      label33:
      int k;
      if (this.a == null)
      {
        i = 0;
        if (this.b != null) {
          break label88;
        }
        j = 0;
        if (this.c != null) {
          break label99;
        }
        k = 0;
        label42:
        if (this.d != null) {
          break label110;
        }
      }
      for (;;)
      {
        return (k + (j + (i + (n + 527) * 31) * 31) * 31) * 31 + m;
        i = this.a.hashCode();
        break;
        label88:
        j = this.b.hashCode();
        break label33;
        label99:
        k = this.c.hashCode();
        break label42;
        label110:
        m = this.d.hashCode();
      }
    }
  }
  
  public static final class zzd
    extends zzsu
  {
    public Integer a;
    public Boolean b;
    public String c;
    public String d;
    public String e;
    
    public zzd()
    {
      a();
    }
    
    public zzd a()
    {
      this.a = null;
      this.b = null;
      this.c = null;
      this.d = null;
      this.e = null;
      this.S = -1;
      return this;
    }
    
    public zzd a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (zzsx.a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 8: 
          i = paramzzsm.g();
          switch (i)
          {
          default: 
            break;
          case 0: 
          case 1: 
          case 2: 
          case 3: 
          case 4: 
            this.a = Integer.valueOf(i);
          }
          break;
        case 16: 
          this.b = Boolean.valueOf(paramzzsm.h());
          break;
        case 26: 
          this.c = paramzzsm.i();
          break;
        case 34: 
          this.d = paramzzsm.i();
          break;
        case 42: 
          this.e = paramzzsm.i();
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (this.a != null) {
        paramzzsn.a(1, this.a.intValue());
      }
      if (this.b != null) {
        paramzzsn.a(2, this.b.booleanValue());
      }
      if (this.c != null) {
        paramzzsn.a(3, this.c);
      }
      if (this.d != null) {
        paramzzsn.a(4, this.d);
      }
      if (this.e != null) {
        paramzzsn.a(5, this.e);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b();
      int i = j;
      if (this.a != null) {
        i = j + zzsn.c(1, this.a.intValue());
      }
      j = i;
      if (this.b != null) {
        j = i + zzsn.b(2, this.b.booleanValue());
      }
      i = j;
      if (this.c != null) {
        i = j + zzsn.b(3, this.c);
      }
      j = i;
      if (this.d != null) {
        j = i + zzsn.b(4, this.d);
      }
      i = j;
      if (this.e != null) {
        i = j + zzsn.b(5, this.e);
      }
      return i;
    }
    
    public boolean equals(Object paramObject)
    {
      if (paramObject == this) {}
      do
      {
        do
        {
          return true;
          if (!(paramObject instanceof zzd)) {
            return false;
          }
          paramObject = (zzd)paramObject;
          if (this.a == null)
          {
            if (((zzd)paramObject).a != null) {
              return false;
            }
          }
          else if (!this.a.equals(((zzd)paramObject).a)) {
            return false;
          }
          if (this.b == null)
          {
            if (((zzd)paramObject).b != null) {
              return false;
            }
          }
          else if (!this.b.equals(((zzd)paramObject).b)) {
            return false;
          }
          if (this.c == null)
          {
            if (((zzd)paramObject).c != null) {
              return false;
            }
          }
          else if (!this.c.equals(((zzd)paramObject).c)) {
            return false;
          }
          if (this.d == null)
          {
            if (((zzd)paramObject).d != null) {
              return false;
            }
          }
          else if (!this.d.equals(((zzd)paramObject).d)) {
            return false;
          }
          if (this.e != null) {
            break;
          }
        } while (((zzd)paramObject).e == null);
        return false;
      } while (this.e.equals(((zzd)paramObject).e));
      return false;
    }
    
    public int hashCode()
    {
      int n = 0;
      int i1 = getClass().getName().hashCode();
      int i;
      int j;
      label33:
      int k;
      label42:
      int m;
      if (this.a == null)
      {
        i = 0;
        if (this.b != null) {
          break label104;
        }
        j = 0;
        if (this.c != null) {
          break label115;
        }
        k = 0;
        if (this.d != null) {
          break label126;
        }
        m = 0;
        label52:
        if (this.e != null) {
          break label138;
        }
      }
      for (;;)
      {
        return (m + (k + (j + (i + (i1 + 527) * 31) * 31) * 31) * 31) * 31 + n;
        i = this.a.intValue();
        break;
        label104:
        j = this.b.hashCode();
        break label33;
        label115:
        k = this.c.hashCode();
        break label42;
        label126:
        m = this.d.hashCode();
        break label52;
        label138:
        n = this.e.hashCode();
      }
    }
  }
  
  public static final class zze
    extends zzsu
  {
    private static volatile zze[] d;
    public Integer a;
    public String b;
    public zzpz.zzc c;
    
    public zze()
    {
      c();
    }
    
    public static zze[] a()
    {
      if (d == null) {}
      synchronized (zzss.a)
      {
        if (d == null) {
          d = new zze[0];
        }
        return d;
      }
    }
    
    public zze a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (zzsx.a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 8: 
          this.a = Integer.valueOf(paramzzsm.g());
          break;
        case 18: 
          this.b = paramzzsm.i();
          break;
        case 26: 
          if (this.c == null) {
            this.c = new zzpz.zzc();
          }
          paramzzsm.a(this.c);
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (this.a != null) {
        paramzzsn.a(1, this.a.intValue());
      }
      if (this.b != null) {
        paramzzsn.a(2, this.b);
      }
      if (this.c != null) {
        paramzzsn.a(3, this.c);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b();
      int i = j;
      if (this.a != null) {
        i = j + zzsn.c(1, this.a.intValue());
      }
      j = i;
      if (this.b != null) {
        j = i + zzsn.b(2, this.b);
      }
      i = j;
      if (this.c != null) {
        i = j + zzsn.c(3, this.c);
      }
      return i;
    }
    
    public zze c()
    {
      this.a = null;
      this.b = null;
      this.c = null;
      this.S = -1;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      if (paramObject == this) {}
      do
      {
        do
        {
          return true;
          if (!(paramObject instanceof zze)) {
            return false;
          }
          paramObject = (zze)paramObject;
          if (this.a == null)
          {
            if (((zze)paramObject).a != null) {
              return false;
            }
          }
          else if (!this.a.equals(((zze)paramObject).a)) {
            return false;
          }
          if (this.b == null)
          {
            if (((zze)paramObject).b != null) {
              return false;
            }
          }
          else if (!this.b.equals(((zze)paramObject).b)) {
            return false;
          }
          if (this.c != null) {
            break;
          }
        } while (((zze)paramObject).c == null);
        return false;
      } while (this.c.equals(((zze)paramObject).c));
      return false;
    }
    
    public int hashCode()
    {
      int k = 0;
      int m = getClass().getName().hashCode();
      int i;
      int j;
      if (this.a == null)
      {
        i = 0;
        if (this.b != null) {
          break label72;
        }
        j = 0;
        label32:
        if (this.c != null) {
          break label83;
        }
      }
      for (;;)
      {
        return (j + (i + (m + 527) * 31) * 31) * 31 + k;
        i = this.a.hashCode();
        break;
        label72:
        j = this.b.hashCode();
        break label32;
        label83:
        k = this.c.hashCode();
      }
    }
  }
  
  public static final class zzf
    extends zzsu
  {
    public Integer a;
    public String b;
    public Boolean c;
    public String[] d;
    
    public zzf()
    {
      a();
    }
    
    public zzf a()
    {
      this.a = null;
      this.b = null;
      this.c = null;
      this.d = zzsx.f;
      this.S = -1;
      return this;
    }
    
    public zzf a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (zzsx.a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 8: 
          i = paramzzsm.g();
          switch (i)
          {
          default: 
            break;
          case 0: 
          case 1: 
          case 2: 
          case 3: 
          case 4: 
          case 5: 
          case 6: 
            this.a = Integer.valueOf(i);
          }
          break;
        case 18: 
          this.b = paramzzsm.i();
          break;
        case 24: 
          this.c = Boolean.valueOf(paramzzsm.h());
          break;
        case 34: 
          int j = zzsx.b(paramzzsm, 34);
          if (this.d == null) {}
          String[] arrayOfString;
          for (i = 0;; i = this.d.length)
          {
            arrayOfString = new String[j + i];
            j = i;
            if (i != 0)
            {
              System.arraycopy(this.d, 0, arrayOfString, 0, i);
              j = i;
            }
            while (j < arrayOfString.length - 1)
            {
              arrayOfString[j] = paramzzsm.i();
              paramzzsm.a();
              j += 1;
            }
          }
          arrayOfString[j] = paramzzsm.i();
          this.d = arrayOfString;
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (this.a != null) {
        paramzzsn.a(1, this.a.intValue());
      }
      if (this.b != null) {
        paramzzsn.a(2, this.b);
      }
      if (this.c != null) {
        paramzzsn.a(3, this.c.booleanValue());
      }
      if ((this.d != null) && (this.d.length > 0))
      {
        int i = 0;
        while (i < this.d.length)
        {
          String str = this.d[i];
          if (str != null) {
            paramzzsn.a(4, str);
          }
          i += 1;
        }
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int n = 0;
      int j = super.b();
      int i = j;
      if (this.a != null) {
        i = j + zzsn.c(1, this.a.intValue());
      }
      j = i;
      if (this.b != null) {
        j = i + zzsn.b(2, this.b);
      }
      i = j;
      if (this.c != null) {
        i = j + zzsn.b(3, this.c.booleanValue());
      }
      j = i;
      if (this.d != null)
      {
        j = i;
        if (this.d.length > 0)
        {
          int k = 0;
          int m = 0;
          j = n;
          while (j < this.d.length)
          {
            String str = this.d[j];
            int i1 = k;
            n = m;
            if (str != null)
            {
              n = m + 1;
              i1 = k + zzsn.b(str);
            }
            j += 1;
            k = i1;
            m = n;
          }
          j = i + k + m * 1;
        }
      }
      return j;
    }
    
    public boolean equals(Object paramObject)
    {
      if (paramObject == this) {}
      do
      {
        return true;
        if (!(paramObject instanceof zzf)) {
          return false;
        }
        paramObject = (zzf)paramObject;
        if (this.a == null)
        {
          if (((zzf)paramObject).a != null) {
            return false;
          }
        }
        else if (!this.a.equals(((zzf)paramObject).a)) {
          return false;
        }
        if (this.b == null)
        {
          if (((zzf)paramObject).b != null) {
            return false;
          }
        }
        else if (!this.b.equals(((zzf)paramObject).b)) {
          return false;
        }
        if (this.c == null)
        {
          if (((zzf)paramObject).c != null) {
            return false;
          }
        }
        else if (!this.c.equals(((zzf)paramObject).c)) {
          return false;
        }
      } while (zzss.a(this.d, ((zzf)paramObject).d));
      return false;
    }
    
    public int hashCode()
    {
      int k = 0;
      int m = getClass().getName().hashCode();
      int i;
      int j;
      if (this.a == null)
      {
        i = 0;
        if (this.b != null) {
          break label83;
        }
        j = 0;
        label32:
        if (this.c != null) {
          break label94;
        }
      }
      for (;;)
      {
        return ((j + (i + (m + 527) * 31) * 31) * 31 + k) * 31 + zzss.a(this.d);
        i = this.a.intValue();
        break;
        label83:
        j = this.b.hashCode();
        break label32;
        label94:
        k = this.c.hashCode();
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzpz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */