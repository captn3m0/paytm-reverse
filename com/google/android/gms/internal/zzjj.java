package com.google.android.gms.internal;

import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@zzhb
public class zzjj<T>
  implements zzji<T>
{
  protected int a = 0;
  protected final BlockingQueue<zzjj<T>.zza> b = new LinkedBlockingQueue();
  protected T c;
  private final Object d = new Object();
  
  public void a(zzji.zzc<T> paramzzc, zzji.zza paramzza)
  {
    for (;;)
    {
      synchronized (this.d)
      {
        if (this.a == 1)
        {
          paramzzc.a(this.c);
          return;
        }
        if (this.a == -1) {
          paramzza.a();
        }
      }
      if (this.a == 0) {
        this.b.add(new zza(paramzzc, paramzza));
      }
    }
  }
  
  public void a(T paramT)
  {
    synchronized (this.d)
    {
      if (this.a != 0) {
        throw new UnsupportedOperationException();
      }
    }
    this.c = paramT;
    this.a = 1;
    Iterator localIterator = this.b.iterator();
    while (localIterator.hasNext()) {
      ((zza)localIterator.next()).a.a(paramT);
    }
    this.b.clear();
  }
  
  public void e()
  {
    synchronized (this.d)
    {
      if (this.a != 0) {
        throw new UnsupportedOperationException();
      }
    }
    this.a = -1;
    Iterator localIterator = this.b.iterator();
    while (localIterator.hasNext()) {
      ((zza)localIterator.next()).b.a();
    }
    this.b.clear();
  }
  
  public int f()
  {
    return this.a;
  }
  
  class zza
  {
    public final zzji.zzc<T> a;
    public final zzji.zza b;
    
    public zza(zzji.zza paramzza)
    {
      this.a = paramzza;
      zzji.zza localzza;
      this.b = localzza;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzjj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */