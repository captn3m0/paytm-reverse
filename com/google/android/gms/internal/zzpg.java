package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zzb;
import com.google.android.gms.fitness.SessionsApi;
import com.google.android.gms.fitness.request.SessionInsertRequest;
import com.google.android.gms.fitness.request.SessionReadRequest;
import com.google.android.gms.fitness.request.SessionRegistrationRequest;
import com.google.android.gms.fitness.request.SessionStartRequest;
import com.google.android.gms.fitness.request.SessionStopRequest;
import com.google.android.gms.fitness.request.SessionUnregistrationRequest;
import com.google.android.gms.fitness.result.SessionReadResult;
import com.google.android.gms.fitness.result.SessionStopResult;

public class zzpg
  implements SessionsApi
{
  private static class zza
    extends zzou.zza
  {
    private final zza.zzb<SessionReadResult> a;
    
    private zza(zza.zzb<SessionReadResult> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void a(SessionReadResult paramSessionReadResult)
      throws RemoteException
    {
      this.a.a(paramSessionReadResult);
    }
  }
  
  private static class zzb
    extends zzov.zza
  {
    private final zza.zzb<SessionStopResult> a;
    
    private zzb(zza.zzb<SessionStopResult> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void a(SessionStopResult paramSessionStopResult)
    {
      this.a.a(paramSessionStopResult);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzpg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */