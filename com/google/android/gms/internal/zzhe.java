package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.SearchAdRequestParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzr;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzhb
public final class zzhe
{
  private static final SimpleDateFormat a = new SimpleDateFormat("yyyyMMdd", Locale.US);
  
  public static AdResponseParcel a(Context paramContext, AdRequestInfoParcel paramAdRequestInfoParcel, String paramString)
  {
    Object localObject1;
    String str1;
    boolean bool1;
    long l1;
    label107:
    Object localObject2;
    int i;
    label180:
    label200:
    label249:
    label265:
    label297:
    label329:
    int j;
    try
    {
      JSONObject localJSONObject = new JSONObject(paramString);
      localObject1 = localJSONObject.optString("ad_base_url", null);
      str1 = localJSONObject.optString("ad_url", null);
      String str2 = localJSONObject.optString("ad_size", null);
      if ((paramAdRequestInfoParcel == null) || (paramAdRequestInfoParcel.m == 0)) {
        break label783;
      }
      bool1 = true;
      long l2;
      String str3;
      if (bool1)
      {
        paramString = localJSONObject.optString("ad_json", null);
        l2 = -1L;
        str3 = localJSONObject.optString("debug_dialog", null);
        if (!localJSONObject.has("interstitial_timeout")) {
          break label789;
        }
        l1 = (localJSONObject.getDouble("interstitial_timeout") * 1000.0D);
        localObject2 = localJSONObject.optString("orientation", null);
        i = -1;
        if (!"portrait".equals(localObject2)) {
          break label180;
        }
        i = zzr.g().b();
      }
      for (;;)
      {
        localObject2 = null;
        if (TextUtils.isEmpty(paramString)) {
          break label200;
        }
        if (!TextUtils.isEmpty((CharSequence)localObject1)) {
          break label774;
        }
        zzin.d("Could not parse the mediation config: Missing required ad_base_url field");
        return new AdResponseParcel(0);
        paramString = localJSONObject.optString("ad_html", null);
        break;
        if ("landscape".equals(localObject2)) {
          i = zzr.g().a();
        }
      }
      if (!TextUtils.isEmpty(str1))
      {
        localObject2 = zzhd.a(paramAdRequestInfoParcel, paramContext, paramAdRequestInfoParcel.k.b, str1, null, null, null, null, null);
        paramString = ((AdResponseParcel)localObject2).b;
        str1 = ((AdResponseParcel)localObject2).c;
        l2 = ((AdResponseParcel)localObject2).n;
        Object localObject3 = localJSONObject.optJSONArray("click_urls");
        if (localObject2 != null) {
          break label741;
        }
        paramContext = null;
        localObject1 = paramContext;
        if (localObject3 != null) {
          localObject1 = a((JSONArray)localObject3, paramContext);
        }
        Object localObject4 = localJSONObject.optJSONArray("impression_urls");
        if (localObject2 != null) {
          break label750;
        }
        paramContext = null;
        localObject3 = paramContext;
        if (localObject4 != null) {
          localObject3 = a((JSONArray)localObject4, paramContext);
        }
        Object localObject5 = localJSONObject.optJSONArray("manual_impression_urls");
        if (localObject2 != null) {
          break label759;
        }
        paramContext = null;
        localObject4 = paramContext;
        if (localObject5 != null) {
          localObject4 = a((JSONArray)localObject5, paramContext);
        }
        j = i;
        if (localObject2 == null) {
          break label768;
        }
        if (((AdResponseParcel)localObject2).l != -1) {
          i = ((AdResponseParcel)localObject2).l;
        }
        j = i;
        if (((AdResponseParcel)localObject2).g <= 0L) {
          break label768;
        }
        l1 = ((AdResponseParcel)localObject2).g;
        label388:
        localObject2 = localJSONObject.optString("active_view");
        paramContext = null;
        boolean bool2 = localJSONObject.optBoolean("ad_is_javascript", false);
        if (bool2) {
          paramContext = localJSONObject.optString("ad_passback_url", null);
        }
        boolean bool3 = localJSONObject.optBoolean("mediation", false);
        boolean bool4 = localJSONObject.optBoolean("custom_render_allowed", false);
        boolean bool5 = localJSONObject.optBoolean("content_url_opted_out", true);
        boolean bool6 = localJSONObject.optBoolean("prefetch", false);
        j = localJSONObject.optInt("oauth2_token_status", 0);
        long l3 = localJSONObject.optLong("refresh_interval_milliseconds", -1L);
        long l4 = localJSONObject.optLong("mediation_config_cache_time_milliseconds", -1L);
        localObject5 = localJSONObject.optString("gws_query_id", "");
        boolean bool7 = "height".equals(localJSONObject.optString("fluid", ""));
        boolean bool8 = localJSONObject.optBoolean("native_express", false);
        List localList1 = a(localJSONObject.optJSONArray("video_start_urls"), null);
        List localList2 = a(localJSONObject.optJSONArray("video_complete_urls"), null);
        RewardItemParcel localRewardItemParcel = RewardItemParcel.a(localJSONObject.optJSONArray("rewards"));
        boolean bool9 = localJSONObject.optBoolean("use_displayed_impression", false);
        paramContext = new AdResponseParcel(paramAdRequestInfoParcel, paramString, str1, (List)localObject1, (List)localObject3, l1, bool3, l4, (List)localObject4, l3, i, str2, l2, str3, bool2, paramContext, (String)localObject2, bool4, bool1, paramAdRequestInfoParcel.p, bool5, bool6, j, (String)localObject5, bool7, bool8, localRewardItemParcel, localList1, localList2, bool9);
        return paramContext;
      }
    }
    catch (JSONException paramContext)
    {
      zzin.d("Could not parse the mediation config: " + paramContext.getMessage());
      return new AdResponseParcel(0);
    }
    paramAdRequestInfoParcel = new StringBuilder().append("Could not parse the mediation config: Missing required ");
    if (bool1) {}
    for (paramContext = "ad_json";; paramContext = "ad_html")
    {
      zzin.d(paramContext + " or " + "ad_url" + " field.");
      return new AdResponseParcel(0);
      label741:
      paramContext = ((AdResponseParcel)localObject2).d;
      break label265;
      label750:
      paramContext = ((AdResponseParcel)localObject2).f;
      break label297;
      label759:
      paramContext = ((AdResponseParcel)localObject2).j;
      break label329;
      label768:
      i = j;
      break label388;
      label774:
      str1 = paramString;
      paramString = (String)localObject1;
      break label249;
      label783:
      bool1 = false;
      break;
      label789:
      l1 = -1L;
      break label107;
    }
  }
  
  private static Integer a(boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (int i = 1;; i = 0) {
      return Integer.valueOf(i);
    }
  }
  
  private static String a(int paramInt)
  {
    return String.format(Locale.US, "#%06x", new Object[] { Integer.valueOf(0xFFFFFF & paramInt) });
  }
  
  private static String a(NativeAdOptionsParcel paramNativeAdOptionsParcel)
  {
    if (paramNativeAdOptionsParcel != null) {}
    for (int i = paramNativeAdOptionsParcel.c;; i = 0) {
      switch (i)
      {
      default: 
        return "any";
      }
    }
    return "portrait";
    return "landscape";
  }
  
  @Nullable
  private static List<String> a(@Nullable JSONArray paramJSONArray, @Nullable List<String> paramList)
    throws JSONException
  {
    if (paramJSONArray == null)
    {
      paramList = null;
      return paramList;
    }
    Object localObject = paramList;
    if (paramList == null) {
      localObject = new LinkedList();
    }
    int i = 0;
    for (;;)
    {
      paramList = (List<String>)localObject;
      if (i >= paramJSONArray.length()) {
        break;
      }
      ((List)localObject).add(paramJSONArray.getString(i));
      i += 1;
    }
  }
  
  @Nullable
  static JSONArray a(List<String> paramList)
    throws JSONException
  {
    JSONArray localJSONArray = new JSONArray();
    paramList = paramList.iterator();
    while (paramList.hasNext()) {
      localJSONArray.put((String)paramList.next());
    }
    return localJSONArray;
  }
  
  /* Error */
  public static JSONObject a(Context paramContext, AdRequestInfoParcel paramAdRequestInfoParcel, zzhj paramzzhj, zzhn.zza paramzza, Location paramLocation, zzbm paramzzbm, String paramString1, String paramString2, List<String> paramList, Bundle paramBundle)
  {
    // Byte code:
    //   0: new 319	java/util/HashMap
    //   3: dup
    //   4: invokespecial 320	java/util/HashMap:<init>	()V
    //   7: astore_0
    //   8: aload 8
    //   10: invokeinterface 323 1 0
    //   15: ifle +19 -> 34
    //   18: aload_0
    //   19: ldc_w 325
    //   22: ldc_w 327
    //   25: aload 8
    //   27: invokestatic 331	android/text/TextUtils:join	(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;
    //   30: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   33: pop
    //   34: aload_1
    //   35: getfield 337	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:b	Landroid/os/Bundle;
    //   38: ifnull +15 -> 53
    //   41: aload_0
    //   42: ldc_w 339
    //   45: aload_1
    //   46: getfield 337	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:b	Landroid/os/Bundle;
    //   49: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   52: pop
    //   53: aload_0
    //   54: aload_1
    //   55: getfield 342	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:c	Lcom/google/android/gms/ads/internal/client/AdRequestParcel;
    //   58: invokestatic 345	com/google/android/gms/internal/zzhe:a	(Ljava/util/HashMap;Lcom/google/android/gms/ads/internal/client/AdRequestParcel;)V
    //   61: aload_0
    //   62: ldc_w 346
    //   65: aload_1
    //   66: getfield 349	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:d	Lcom/google/android/gms/ads/internal/client/AdSizeParcel;
    //   69: getfield 352	com/google/android/gms/ads/internal/client/AdSizeParcel:b	Ljava/lang/String;
    //   72: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   75: pop
    //   76: aload_1
    //   77: getfield 349	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:d	Lcom/google/android/gms/ads/internal/client/AdSizeParcel;
    //   80: getfield 354	com/google/android/gms/ads/internal/client/AdSizeParcel:f	I
    //   83: iconst_m1
    //   84: if_icmpne +14 -> 98
    //   87: aload_0
    //   88: ldc_w 356
    //   91: ldc_w 358
    //   94: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   97: pop
    //   98: aload_1
    //   99: getfield 349	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:d	Lcom/google/android/gms/ads/internal/client/AdSizeParcel;
    //   102: getfield 359	com/google/android/gms/ads/internal/client/AdSizeParcel:c	I
    //   105: bipush -2
    //   107: if_icmpne +14 -> 121
    //   110: aload_0
    //   111: ldc_w 361
    //   114: ldc_w 363
    //   117: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   120: pop
    //   121: aload_1
    //   122: getfield 349	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:d	Lcom/google/android/gms/ads/internal/client/AdSizeParcel;
    //   125: getfield 365	com/google/android/gms/ads/internal/client/AdSizeParcel:j	Z
    //   128: ifeq +12 -> 140
    //   131: aload_0
    //   132: ldc -59
    //   134: ldc -61
    //   136: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   139: pop
    //   140: aload_1
    //   141: getfield 349	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:d	Lcom/google/android/gms/ads/internal/client/AdSizeParcel;
    //   144: getfield 369	com/google/android/gms/ads/internal/client/AdSizeParcel:h	[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;
    //   147: ifnull +171 -> 318
    //   150: new 221	java/lang/StringBuilder
    //   153: dup
    //   154: invokespecial 223	java/lang/StringBuilder:<init>	()V
    //   157: astore 4
    //   159: aload_1
    //   160: getfield 349	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:d	Lcom/google/android/gms/ads/internal/client/AdSizeParcel;
    //   163: getfield 369	com/google/android/gms/ads/internal/client/AdSizeParcel:h	[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;
    //   166: astore 5
    //   168: aload 5
    //   170: arraylength
    //   171: istore 12
    //   173: iconst_0
    //   174: istore 10
    //   176: iload 10
    //   178: iload 12
    //   180: if_icmpge +128 -> 308
    //   183: aload 5
    //   185: iload 10
    //   187: aaload
    //   188: astore 8
    //   190: aload 4
    //   192: invokevirtual 370	java/lang/StringBuilder:length	()I
    //   195: ifeq +12 -> 207
    //   198: aload 4
    //   200: ldc_w 372
    //   203: invokevirtual 229	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   206: pop
    //   207: aload 8
    //   209: getfield 354	com/google/android/gms/ads/internal/client/AdSizeParcel:f	I
    //   212: iconst_m1
    //   213: if_icmpne +75 -> 288
    //   216: aload 8
    //   218: getfield 374	com/google/android/gms/ads/internal/client/AdSizeParcel:g	I
    //   221: i2f
    //   222: aload_2
    //   223: getfield 380	com/google/android/gms/internal/zzhj:r	F
    //   226: fdiv
    //   227: f2i
    //   228: istore 11
    //   230: aload 4
    //   232: iload 11
    //   234: invokevirtual 383	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   237: pop
    //   238: aload 4
    //   240: ldc_w 385
    //   243: invokevirtual 229	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   246: pop
    //   247: aload 8
    //   249: getfield 359	com/google/android/gms/ads/internal/client/AdSizeParcel:c	I
    //   252: bipush -2
    //   254: if_icmpne +44 -> 298
    //   257: aload 8
    //   259: getfield 387	com/google/android/gms/ads/internal/client/AdSizeParcel:d	I
    //   262: i2f
    //   263: aload_2
    //   264: getfield 380	com/google/android/gms/internal/zzhj:r	F
    //   267: fdiv
    //   268: f2i
    //   269: istore 11
    //   271: aload 4
    //   273: iload 11
    //   275: invokevirtual 383	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   278: pop
    //   279: iload 10
    //   281: iconst_1
    //   282: iadd
    //   283: istore 10
    //   285: goto -109 -> 176
    //   288: aload 8
    //   290: getfield 354	com/google/android/gms/ads/internal/client/AdSizeParcel:f	I
    //   293: istore 11
    //   295: goto -65 -> 230
    //   298: aload 8
    //   300: getfield 359	com/google/android/gms/ads/internal/client/AdSizeParcel:c	I
    //   303: istore 11
    //   305: goto -34 -> 271
    //   308: aload_0
    //   309: ldc_w 389
    //   312: aload 4
    //   314: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   317: pop
    //   318: aload_1
    //   319: getfield 50	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:m	I
    //   322: ifeq +79 -> 401
    //   325: aload_0
    //   326: ldc_w 391
    //   329: aload_1
    //   330: getfield 50	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:m	I
    //   333: invokestatic 258	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   336: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   339: pop
    //   340: aload_1
    //   341: getfield 349	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:d	Lcom/google/android/gms/ads/internal/client/AdSizeParcel;
    //   344: getfield 393	com/google/android/gms/ads/internal/client/AdSizeParcel:k	Z
    //   347: ifne +54 -> 401
    //   350: aload_0
    //   351: ldc_w 395
    //   354: aload_1
    //   355: getfield 397	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:n	Ljava/util/List;
    //   358: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   361: pop
    //   362: aload_0
    //   363: ldc_w 399
    //   366: aload_1
    //   367: getfield 403	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:z	Lcom/google/android/gms/ads/internal/formats/NativeAdOptionsParcel;
    //   370: invokestatic 405	com/google/android/gms/internal/zzhe:a	(Lcom/google/android/gms/ads/internal/formats/NativeAdOptionsParcel;)Ljava/lang/String;
    //   373: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   376: pop
    //   377: aload_1
    //   378: getfield 408	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:A	Ljava/util/List;
    //   381: invokeinterface 410 1 0
    //   386: ifne +15 -> 401
    //   389: aload_0
    //   390: ldc_w 412
    //   393: aload_1
    //   394: getfield 408	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:A	Ljava/util/List;
    //   397: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   400: pop
    //   401: aload_0
    //   402: ldc_w 414
    //   405: aload_1
    //   406: getfield 417	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:e	Ljava/lang/String;
    //   409: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   412: pop
    //   413: aload_0
    //   414: ldc_w 419
    //   417: aload_1
    //   418: getfield 422	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:f	Landroid/content/pm/ApplicationInfo;
    //   421: getfield 427	android/content/pm/ApplicationInfo:packageName	Ljava/lang/String;
    //   424: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   427: pop
    //   428: aload_1
    //   429: getfield 430	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:g	Landroid/content/pm/PackageInfo;
    //   432: ifnull +21 -> 453
    //   435: aload_0
    //   436: ldc_w 432
    //   439: aload_1
    //   440: getfield 430	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:g	Landroid/content/pm/PackageInfo;
    //   443: getfield 437	android/content/pm/PackageInfo:versionCode	I
    //   446: invokestatic 258	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   449: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   452: pop
    //   453: aload_0
    //   454: ldc_w 439
    //   457: aload 7
    //   459: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   462: pop
    //   463: aload_0
    //   464: ldc_w 441
    //   467: aload_1
    //   468: getfield 444	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:i	Ljava/lang/String;
    //   471: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   474: pop
    //   475: aload_0
    //   476: ldc_w 446
    //   479: aload_1
    //   480: getfield 448	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:j	Ljava/lang/String;
    //   483: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   486: pop
    //   487: aload_0
    //   488: ldc_w 450
    //   491: aload_1
    //   492: getfield 118	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:k	Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;
    //   495: getfield 123	com/google/android/gms/ads/internal/util/client/VersionInfoParcel:b	Ljava/lang/String;
    //   498: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   501: pop
    //   502: aload_0
    //   503: aload_2
    //   504: aload_3
    //   505: invokestatic 453	com/google/android/gms/internal/zzhe:a	(Ljava/util/HashMap;Lcom/google/android/gms/internal/zzhj;Lcom/google/android/gms/internal/zzhn$zza;)V
    //   508: aload_0
    //   509: ldc_w 455
    //   512: getstatic 460	android/os/Build:MANUFACTURER	Ljava/lang/String;
    //   515: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   518: pop
    //   519: aload_0
    //   520: ldc_w 462
    //   523: getstatic 465	android/os/Build:MODEL	Ljava/lang/String;
    //   526: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   529: pop
    //   530: aload_1
    //   531: getfield 342	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:c	Lcom/google/android/gms/ads/internal/client/AdRequestParcel;
    //   534: getfield 469	com/google/android/gms/ads/internal/client/AdRequestParcel:a	I
    //   537: iconst_2
    //   538: if_icmplt +24 -> 562
    //   541: aload_1
    //   542: getfield 342	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:c	Lcom/google/android/gms/ads/internal/client/AdRequestParcel;
    //   545: getfield 472	com/google/android/gms/ads/internal/client/AdRequestParcel:k	Landroid/location/Location;
    //   548: ifnull +14 -> 562
    //   551: aload_0
    //   552: aload_1
    //   553: getfield 342	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:c	Lcom/google/android/gms/ads/internal/client/AdRequestParcel;
    //   556: getfield 472	com/google/android/gms/ads/internal/client/AdRequestParcel:k	Landroid/location/Location;
    //   559: invokestatic 475	com/google/android/gms/internal/zzhe:a	(Ljava/util/HashMap;Landroid/location/Location;)V
    //   562: aload_1
    //   563: getfield 476	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:a	I
    //   566: iconst_2
    //   567: if_icmplt +15 -> 582
    //   570: aload_0
    //   571: ldc_w 478
    //   574: aload_1
    //   575: getfield 480	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:l	Landroid/os/Bundle;
    //   578: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   581: pop
    //   582: aload_1
    //   583: getfield 476	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:a	I
    //   586: iconst_4
    //   587: if_icmplt +25 -> 612
    //   590: aload_1
    //   591: getfield 216	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:p	Z
    //   594: ifeq +18 -> 612
    //   597: aload_0
    //   598: ldc_w 482
    //   601: aload_1
    //   602: getfield 216	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:p	Z
    //   605: invokestatic 487	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   608: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   611: pop
    //   612: aload 9
    //   614: ifnull +13 -> 627
    //   617: aload_0
    //   618: ldc_w 489
    //   621: aload 9
    //   623: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   626: pop
    //   627: aload_1
    //   628: getfield 476	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:a	I
    //   631: iconst_5
    //   632: if_icmplt +332 -> 964
    //   635: aload_0
    //   636: ldc_w 491
    //   639: aload_1
    //   640: getfield 494	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:t	F
    //   643: invokestatic 499	java/lang/Float:valueOf	(F)Ljava/lang/Float;
    //   646: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   649: pop
    //   650: aload_0
    //   651: ldc_w 501
    //   654: aload_1
    //   655: getfield 504	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:s	I
    //   658: invokestatic 258	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   661: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   664: pop
    //   665: aload_0
    //   666: ldc_w 506
    //   669: aload_1
    //   670: getfield 508	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:r	I
    //   673: invokestatic 258	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   676: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   679: pop
    //   680: aload_1
    //   681: getfield 476	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:a	I
    //   684: bipush 6
    //   686: if_icmplt +51 -> 737
    //   689: aload_1
    //   690: getfield 511	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:u	Ljava/lang/String;
    //   693: invokestatic 96	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   696: istore 13
    //   698: iload 13
    //   700: ifne +22 -> 722
    //   703: aload_0
    //   704: ldc_w 513
    //   707: new 31	org/json/JSONObject
    //   710: dup
    //   711: aload_1
    //   712: getfield 511	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:u	Ljava/lang/String;
    //   715: invokespecial 34	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   718: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   721: pop
    //   722: aload_0
    //   723: ldc_w 515
    //   726: aload_1
    //   727: getfield 518	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:v	J
    //   730: invokestatic 523	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   733: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   736: pop
    //   737: aload_1
    //   738: getfield 476	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:a	I
    //   741: bipush 7
    //   743: if_icmplt +15 -> 758
    //   746: aload_0
    //   747: ldc_w 525
    //   750: aload_1
    //   751: getfield 528	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:w	Ljava/lang/String;
    //   754: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   757: pop
    //   758: aload_1
    //   759: getfield 476	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:a	I
    //   762: bipush 11
    //   764: if_icmplt +25 -> 789
    //   767: aload_1
    //   768: getfield 532	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:C	Lcom/google/android/gms/ads/internal/request/CapabilityParcel;
    //   771: ifnull +18 -> 789
    //   774: aload_0
    //   775: ldc_w 534
    //   778: aload_1
    //   779: getfield 532	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:C	Lcom/google/android/gms/ads/internal/request/CapabilityParcel;
    //   782: invokevirtual 539	com/google/android/gms/ads/internal/request/CapabilityParcel:a	()Landroid/os/Bundle;
    //   785: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   788: pop
    //   789: aload_0
    //   790: aload 6
    //   792: invokestatic 542	com/google/android/gms/internal/zzhe:a	(Ljava/util/HashMap;Ljava/lang/String;)V
    //   795: aload_1
    //   796: getfield 476	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:a	I
    //   799: bipush 12
    //   801: if_icmplt +25 -> 826
    //   804: aload_1
    //   805: getfield 545	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:D	Ljava/lang/String;
    //   808: invokestatic 96	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   811: ifne +15 -> 826
    //   814: aload_0
    //   815: ldc_w 547
    //   818: aload_1
    //   819: getfield 545	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:D	Ljava/lang/String;
    //   822: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   825: pop
    //   826: aload_1
    //   827: getfield 476	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:a	I
    //   830: bipush 13
    //   832: if_icmplt +18 -> 850
    //   835: aload_0
    //   836: ldc_w 549
    //   839: aload_1
    //   840: getfield 552	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:E	F
    //   843: invokestatic 499	java/lang/Float:valueOf	(F)Ljava/lang/Float;
    //   846: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   849: pop
    //   850: aload_1
    //   851: getfield 476	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:a	I
    //   854: bipush 14
    //   856: if_icmplt +25 -> 881
    //   859: aload_1
    //   860: getfield 554	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:F	I
    //   863: ifle +18 -> 881
    //   866: aload_0
    //   867: ldc_w 556
    //   870: aload_1
    //   871: getfield 554	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:F	I
    //   874: invokestatic 258	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   877: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   880: pop
    //   881: aload_1
    //   882: getfield 476	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:a	I
    //   885: bipush 15
    //   887: if_icmplt +27 -> 914
    //   890: aload_1
    //   891: getfield 559	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:G	I
    //   894: iconst_m1
    //   895: if_icmpne +157 -> 1052
    //   898: iconst_m1
    //   899: istore 10
    //   901: aload_0
    //   902: ldc_w 561
    //   905: iload 10
    //   907: invokestatic 258	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   910: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   913: pop
    //   914: iconst_2
    //   915: invokestatic 564	com/google/android/gms/internal/zzin:a	(I)Z
    //   918: ifeq +38 -> 956
    //   921: invokestatic 567	com/google/android/gms/ads/internal/zzr:e	()Lcom/google/android/gms/internal/zzir;
    //   924: aload_0
    //   925: invokevirtual 572	com/google/android/gms/internal/zzir:a	(Ljava/util/Map;)Lorg/json/JSONObject;
    //   928: iconst_2
    //   929: invokevirtual 574	org/json/JSONObject:toString	(I)Ljava/lang/String;
    //   932: astore_1
    //   933: new 221	java/lang/StringBuilder
    //   936: dup
    //   937: invokespecial 223	java/lang/StringBuilder:<init>	()V
    //   940: ldc_w 576
    //   943: invokevirtual 229	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   946: aload_1
    //   947: invokevirtual 229	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   950: invokevirtual 236	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   953: invokestatic 578	com/google/android/gms/internal/zzin:e	(Ljava/lang/String;)V
    //   956: invokestatic 567	com/google/android/gms/ads/internal/zzr:e	()Lcom/google/android/gms/internal/zzir;
    //   959: aload_0
    //   960: invokevirtual 572	com/google/android/gms/internal/zzir:a	(Ljava/util/Map;)Lorg/json/JSONObject;
    //   963: areturn
    //   964: aload_0
    //   965: ldc_w 491
    //   968: aload_2
    //   969: getfield 380	com/google/android/gms/internal/zzhj:r	F
    //   972: invokestatic 499	java/lang/Float:valueOf	(F)Ljava/lang/Float;
    //   975: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   978: pop
    //   979: aload_0
    //   980: ldc_w 501
    //   983: aload_2
    //   984: getfield 580	com/google/android/gms/internal/zzhj:t	I
    //   987: invokestatic 258	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   990: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   993: pop
    //   994: aload_0
    //   995: ldc_w 506
    //   998: aload_2
    //   999: getfield 581	com/google/android/gms/internal/zzhj:s	I
    //   1002: invokestatic 258	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   1005: invokevirtual 334	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   1008: pop
    //   1009: goto -329 -> 680
    //   1012: astore_0
    //   1013: new 221	java/lang/StringBuilder
    //   1016: dup
    //   1017: invokespecial 223	java/lang/StringBuilder:<init>	()V
    //   1020: ldc_w 583
    //   1023: invokevirtual 229	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1026: aload_0
    //   1027: invokevirtual 233	org/json/JSONException:getMessage	()Ljava/lang/String;
    //   1030: invokevirtual 229	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1033: invokevirtual 236	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1036: invokestatic 103	com/google/android/gms/internal/zzin:d	(Ljava/lang/String;)V
    //   1039: aconst_null
    //   1040: areturn
    //   1041: astore_2
    //   1042: ldc_w 585
    //   1045: aload_2
    //   1046: invokestatic 588	com/google/android/gms/internal/zzin:d	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   1049: goto -327 -> 722
    //   1052: aload_1
    //   1053: getfield 559	com/google/android/gms/ads/internal/request/AdRequestInfoParcel:G	I
    //   1056: istore 10
    //   1058: goto -157 -> 901
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1061	0	paramContext	Context
    //   0	1061	1	paramAdRequestInfoParcel	AdRequestInfoParcel
    //   0	1061	2	paramzzhj	zzhj
    //   0	1061	3	paramzza	zzhn.zza
    //   0	1061	4	paramLocation	Location
    //   0	1061	5	paramzzbm	zzbm
    //   0	1061	6	paramString1	String
    //   0	1061	7	paramString2	String
    //   0	1061	8	paramList	List<String>
    //   0	1061	9	paramBundle	Bundle
    //   174	883	10	i	int
    //   228	76	11	j	int
    //   171	10	12	k	int
    //   696	3	13	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   0	34	1012	org/json/JSONException
    //   34	53	1012	org/json/JSONException
    //   53	98	1012	org/json/JSONException
    //   98	121	1012	org/json/JSONException
    //   121	140	1012	org/json/JSONException
    //   140	173	1012	org/json/JSONException
    //   190	207	1012	org/json/JSONException
    //   207	230	1012	org/json/JSONException
    //   230	271	1012	org/json/JSONException
    //   271	279	1012	org/json/JSONException
    //   288	295	1012	org/json/JSONException
    //   298	305	1012	org/json/JSONException
    //   308	318	1012	org/json/JSONException
    //   318	401	1012	org/json/JSONException
    //   401	453	1012	org/json/JSONException
    //   453	562	1012	org/json/JSONException
    //   562	582	1012	org/json/JSONException
    //   582	612	1012	org/json/JSONException
    //   617	627	1012	org/json/JSONException
    //   627	680	1012	org/json/JSONException
    //   680	698	1012	org/json/JSONException
    //   722	737	1012	org/json/JSONException
    //   737	758	1012	org/json/JSONException
    //   758	789	1012	org/json/JSONException
    //   789	826	1012	org/json/JSONException
    //   826	850	1012	org/json/JSONException
    //   850	881	1012	org/json/JSONException
    //   881	898	1012	org/json/JSONException
    //   901	914	1012	org/json/JSONException
    //   914	956	1012	org/json/JSONException
    //   956	964	1012	org/json/JSONException
    //   964	1009	1012	org/json/JSONException
    //   1042	1049	1012	org/json/JSONException
    //   1052	1058	1012	org/json/JSONException
    //   703	722	1041	org/json/JSONException
  }
  
  public static JSONObject a(AdResponseParcel paramAdResponseParcel)
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject();
    if (paramAdResponseParcel.b != null) {
      localJSONObject.put("ad_base_url", paramAdResponseParcel.b);
    }
    if (paramAdResponseParcel.m != null) {
      localJSONObject.put("ad_size", paramAdResponseParcel.m);
    }
    localJSONObject.put("native", paramAdResponseParcel.t);
    if (paramAdResponseParcel.t)
    {
      localJSONObject.put("ad_json", paramAdResponseParcel.c);
      if (paramAdResponseParcel.o != null) {
        localJSONObject.put("debug_dialog", paramAdResponseParcel.o);
      }
      if (paramAdResponseParcel.g != -1L) {
        localJSONObject.put("interstitial_timeout", paramAdResponseParcel.g / 1000.0D);
      }
      if (paramAdResponseParcel.l != zzr.g().b()) {
        break label490;
      }
      localJSONObject.put("orientation", "portrait");
      label141:
      if (paramAdResponseParcel.d != null) {
        localJSONObject.put("click_urls", a(paramAdResponseParcel.d));
      }
      if (paramAdResponseParcel.f != null) {
        localJSONObject.put("impression_urls", a(paramAdResponseParcel.f));
      }
      if (paramAdResponseParcel.j != null) {
        localJSONObject.put("manual_impression_urls", a(paramAdResponseParcel.j));
      }
      if (paramAdResponseParcel.r != null) {
        localJSONObject.put("active_view", paramAdResponseParcel.r);
      }
      localJSONObject.put("ad_is_javascript", paramAdResponseParcel.p);
      if (paramAdResponseParcel.q != null) {
        localJSONObject.put("ad_passback_url", paramAdResponseParcel.q);
      }
      localJSONObject.put("mediation", paramAdResponseParcel.h);
      localJSONObject.put("custom_render_allowed", paramAdResponseParcel.s);
      localJSONObject.put("content_url_opted_out", paramAdResponseParcel.v);
      localJSONObject.put("prefetch", paramAdResponseParcel.w);
      localJSONObject.put("oauth2_token_status", paramAdResponseParcel.x);
      if (paramAdResponseParcel.k != -1L) {
        localJSONObject.put("refresh_interval_milliseconds", paramAdResponseParcel.k);
      }
      if (paramAdResponseParcel.i != -1L) {
        localJSONObject.put("mediation_config_cache_time_milliseconds", paramAdResponseParcel.i);
      }
      if (!TextUtils.isEmpty(paramAdResponseParcel.A)) {
        localJSONObject.put("gws_query_id", paramAdResponseParcel.A);
      }
      if (!paramAdResponseParcel.B) {
        break label515;
      }
    }
    label490:
    label515:
    for (String str = "height";; str = "")
    {
      localJSONObject.put("fluid", str);
      localJSONObject.put("native_express", paramAdResponseParcel.C);
      if (paramAdResponseParcel.E != null) {
        localJSONObject.put("video_start_urls", a(paramAdResponseParcel.E));
      }
      if (paramAdResponseParcel.F != null) {
        localJSONObject.put("video_complete_urls", a(paramAdResponseParcel.F));
      }
      if (paramAdResponseParcel.D != null) {
        localJSONObject.put("rewards", paramAdResponseParcel.D.a());
      }
      localJSONObject.put("use_displayed_impression", paramAdResponseParcel.G);
      return localJSONObject;
      localJSONObject.put("ad_html", paramAdResponseParcel.c);
      break;
      if (paramAdResponseParcel.l != zzr.g().a()) {
        break label141;
      }
      localJSONObject.put("orientation", "landscape");
      break label141;
    }
  }
  
  private static void a(HashMap<String, Object> paramHashMap, Location paramLocation)
  {
    HashMap localHashMap = new HashMap();
    float f = paramLocation.getAccuracy();
    long l1 = paramLocation.getTime();
    long l2 = (paramLocation.getLatitude() * 1.0E7D);
    long l3 = (paramLocation.getLongitude() * 1.0E7D);
    localHashMap.put("radius", Float.valueOf(f * 1000.0F));
    localHashMap.put("lat", Long.valueOf(l2));
    localHashMap.put("long", Long.valueOf(l3));
    localHashMap.put("time", Long.valueOf(l1 * 1000L));
    paramHashMap.put("uule", localHashMap);
  }
  
  private static void a(HashMap<String, Object> paramHashMap, AdRequestParcel paramAdRequestParcel)
  {
    String str = zzil.a();
    if (str != null) {
      paramHashMap.put("abf", str);
    }
    if (paramAdRequestParcel.b != -1L) {
      paramHashMap.put("cust_age", a.format(new Date(paramAdRequestParcel.b)));
    }
    if (paramAdRequestParcel.c != null) {
      paramHashMap.put("extras", paramAdRequestParcel.c);
    }
    if (paramAdRequestParcel.d != -1) {
      paramHashMap.put("cust_gender", Integer.valueOf(paramAdRequestParcel.d));
    }
    if (paramAdRequestParcel.e != null) {
      paramHashMap.put("kw", paramAdRequestParcel.e);
    }
    if (paramAdRequestParcel.g != -1) {
      paramHashMap.put("tag_for_child_directed_treatment", Integer.valueOf(paramAdRequestParcel.g));
    }
    if (paramAdRequestParcel.f) {
      paramHashMap.put("adtest", "on");
    }
    if (paramAdRequestParcel.a >= 2)
    {
      if (paramAdRequestParcel.h) {
        paramHashMap.put("d_imp_hdr", Integer.valueOf(1));
      }
      if (!TextUtils.isEmpty(paramAdRequestParcel.i)) {
        paramHashMap.put("ppid", paramAdRequestParcel.i);
      }
      if (paramAdRequestParcel.j != null) {
        a(paramHashMap, paramAdRequestParcel.j);
      }
    }
    if ((paramAdRequestParcel.a >= 3) && (paramAdRequestParcel.l != null)) {
      paramHashMap.put("url", paramAdRequestParcel.l);
    }
    if (paramAdRequestParcel.a >= 5)
    {
      if (paramAdRequestParcel.n != null) {
        paramHashMap.put("custom_targeting", paramAdRequestParcel.n);
      }
      if (paramAdRequestParcel.o != null) {
        paramHashMap.put("category_exclusions", paramAdRequestParcel.o);
      }
      if (paramAdRequestParcel.p != null) {
        paramHashMap.put("request_agent", paramAdRequestParcel.p);
      }
    }
    if ((paramAdRequestParcel.a >= 6) && (paramAdRequestParcel.q != null)) {
      paramHashMap.put("request_pkg", paramAdRequestParcel.q);
    }
    if (paramAdRequestParcel.a >= 7) {
      paramHashMap.put("is_designed_for_families", Boolean.valueOf(paramAdRequestParcel.r));
    }
  }
  
  private static void a(HashMap<String, Object> paramHashMap, SearchAdRequestParcel paramSearchAdRequestParcel)
  {
    Object localObject2 = null;
    if (Color.alpha(paramSearchAdRequestParcel.b) != 0) {
      paramHashMap.put("acolor", a(paramSearchAdRequestParcel.b));
    }
    if (Color.alpha(paramSearchAdRequestParcel.c) != 0) {
      paramHashMap.put("bgcolor", a(paramSearchAdRequestParcel.c));
    }
    if ((Color.alpha(paramSearchAdRequestParcel.d) != 0) && (Color.alpha(paramSearchAdRequestParcel.e) != 0))
    {
      paramHashMap.put("gradientto", a(paramSearchAdRequestParcel.d));
      paramHashMap.put("gradientfrom", a(paramSearchAdRequestParcel.e));
    }
    if (Color.alpha(paramSearchAdRequestParcel.f) != 0) {
      paramHashMap.put("bcolor", a(paramSearchAdRequestParcel.f));
    }
    paramHashMap.put("bthick", Integer.toString(paramSearchAdRequestParcel.g));
    Object localObject1;
    switch (paramSearchAdRequestParcel.h)
    {
    default: 
      localObject1 = null;
      if (localObject1 != null) {
        paramHashMap.put("btype", localObject1);
      }
      switch (paramSearchAdRequestParcel.i)
      {
      default: 
        localObject1 = localObject2;
      }
      break;
    }
    for (;;)
    {
      if (localObject1 != null) {
        paramHashMap.put("callbuttoncolor", localObject1);
      }
      if (paramSearchAdRequestParcel.j != null) {
        paramHashMap.put("channel", paramSearchAdRequestParcel.j);
      }
      if (Color.alpha(paramSearchAdRequestParcel.k) != 0) {
        paramHashMap.put("dcolor", a(paramSearchAdRequestParcel.k));
      }
      if (paramSearchAdRequestParcel.l != null) {
        paramHashMap.put("font", paramSearchAdRequestParcel.l);
      }
      if (Color.alpha(paramSearchAdRequestParcel.m) != 0) {
        paramHashMap.put("hcolor", a(paramSearchAdRequestParcel.m));
      }
      paramHashMap.put("headersize", Integer.toString(paramSearchAdRequestParcel.n));
      if (paramSearchAdRequestParcel.o != null) {
        paramHashMap.put("q", paramSearchAdRequestParcel.o);
      }
      return;
      localObject1 = "none";
      break;
      localObject1 = "dashed";
      break;
      localObject1 = "dotted";
      break;
      localObject1 = "solid";
      break;
      localObject1 = "dark";
      continue;
      localObject1 = "light";
      continue;
      localObject1 = "medium";
    }
  }
  
  private static void a(HashMap<String, Object> paramHashMap, zzhj paramzzhj, zzhn.zza paramzza)
  {
    paramHashMap.put("am", Integer.valueOf(paramzzhj.a));
    paramHashMap.put("cog", a(paramzzhj.b));
    paramHashMap.put("coh", a(paramzzhj.c));
    if (!TextUtils.isEmpty(paramzzhj.d)) {
      paramHashMap.put("carrier", paramzzhj.d);
    }
    paramHashMap.put("gl", paramzzhj.e);
    if (paramzzhj.f) {
      paramHashMap.put("simulator", Integer.valueOf(1));
    }
    if (paramzzhj.g) {
      paramHashMap.put("is_sidewinder", Integer.valueOf(1));
    }
    paramHashMap.put("ma", a(paramzzhj.h));
    paramHashMap.put("sp", a(paramzzhj.i));
    paramHashMap.put("hl", paramzzhj.j);
    if (!TextUtils.isEmpty(paramzzhj.k)) {
      paramHashMap.put("mv", paramzzhj.k);
    }
    paramHashMap.put("muv", Integer.valueOf(paramzzhj.l));
    if (paramzzhj.m != -2) {
      paramHashMap.put("cnt", Integer.valueOf(paramzzhj.m));
    }
    paramHashMap.put("gnt", Integer.valueOf(paramzzhj.n));
    paramHashMap.put("pt", Integer.valueOf(paramzzhj.o));
    paramHashMap.put("rm", Integer.valueOf(paramzzhj.p));
    paramHashMap.put("riv", Integer.valueOf(paramzzhj.q));
    Bundle localBundle1 = new Bundle();
    localBundle1.putString("build", paramzzhj.y);
    Bundle localBundle2 = new Bundle();
    localBundle2.putBoolean("is_charging", paramzzhj.v);
    localBundle2.putDouble("battery_level", paramzzhj.u);
    localBundle1.putBundle("battery", localBundle2);
    localBundle2 = new Bundle();
    localBundle2.putInt("active_network_state", paramzzhj.x);
    localBundle2.putBoolean("active_network_metered", paramzzhj.w);
    if (paramzza != null)
    {
      paramzzhj = new Bundle();
      paramzzhj.putInt("predicted_latency_micros", paramzza.a);
      paramzzhj.putLong("predicted_down_throughput_bps", paramzza.b);
      paramzzhj.putLong("predicted_up_throughput_bps", paramzza.c);
      localBundle2.putBundle("predictions", paramzzhj);
    }
    localBundle1.putBundle("network", localBundle2);
    paramHashMap.put("device", localBundle1);
  }
  
  private static void a(HashMap<String, Object> paramHashMap, String paramString)
  {
    if (paramString != null)
    {
      HashMap localHashMap = new HashMap();
      localHashMap.put("token", paramString);
      paramHashMap.put("pan", localHashMap);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzhe.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */