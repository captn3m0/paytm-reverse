package com.google.android.gms.internal;

import com.google.android.gms.measurement.zze;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class zzkc
  extends zze<zzkc>
{
  private Map<Integer, Double> a = new HashMap(4);
  
  public Map<Integer, Double> a()
  {
    return Collections.unmodifiableMap(this.a);
  }
  
  public void a(zzkc paramzzkc)
  {
    paramzzkc.a.putAll(this.a);
  }
  
  public String toString()
  {
    HashMap localHashMap = new HashMap();
    Iterator localIterator = this.a.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      localHashMap.put("metric" + localEntry.getKey(), localEntry.getValue());
    }
    return a(localHashMap);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzkc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */