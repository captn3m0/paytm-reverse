package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.ads.formats.NativeAd.Image;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zzd;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@zzhb
public class zzco
  extends NativeContentAd
{
  private final zzcn a;
  private final List<NativeAd.Image> b = new ArrayList();
  private final zzci c;
  
  public zzco(zzcn paramzzcn)
  {
    this.a = paramzzcn;
    try
    {
      paramzzcn = this.a.b();
      if (paramzzcn != null)
      {
        paramzzcn = paramzzcn.iterator();
        while (paramzzcn.hasNext())
        {
          zzch localzzch = a(paramzzcn.next());
          if (localzzch != null) {
            this.b.add(new zzci(localzzch));
          }
        }
      }
      try
      {
        paramzzcn = this.a.d();
        if (paramzzcn == null) {
          break label129;
        }
        paramzzcn = new zzci(paramzzcn);
      }
      catch (RemoteException paramzzcn)
      {
        for (;;)
        {
          zzb.b("Failed to get icon.", paramzzcn);
          paramzzcn = null;
        }
      }
    }
    catch (RemoteException paramzzcn)
    {
      zzb.b("Failed to get image.", paramzzcn);
    }
    this.c = paramzzcn;
  }
  
  zzch a(Object paramObject)
  {
    if ((paramObject instanceof IBinder)) {
      return zzch.zza.a((IBinder)paramObject);
    }
    return null;
  }
  
  public CharSequence b()
  {
    try
    {
      String str = this.a.a();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.b("Failed to get headline.", localRemoteException);
    }
    return null;
  }
  
  public List<NativeAd.Image> c()
  {
    return this.b;
  }
  
  public CharSequence d()
  {
    try
    {
      String str = this.a.c();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.b("Failed to get body.", localRemoteException);
    }
    return null;
  }
  
  public NativeAd.Image e()
  {
    return this.c;
  }
  
  public CharSequence f()
  {
    try
    {
      String str = this.a.e();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.b("Failed to get call to action.", localRemoteException);
    }
    return null;
  }
  
  public CharSequence g()
  {
    try
    {
      String str = this.a.f();
      return str;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.b("Failed to get attribution.", localRemoteException);
    }
    return null;
  }
  
  protected zzd h()
  {
    try
    {
      zzd localzzd = this.a.g();
      return localzzd;
    }
    catch (RemoteException localRemoteException)
    {
      zzb.b("Failed to retrieve native ad engine.", localRemoteException);
    }
    return null;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzco.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */