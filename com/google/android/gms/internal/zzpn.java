package com.google.android.gms.internal;

import android.accounts.Account;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.identity.intents.UserAddressRequest;

public class zzpn
  extends zzj<zzpp>
{
  private Activity a;
  private zza e;
  private final String f;
  private final int g;
  
  public zzpn(Activity paramActivity, Looper paramLooper, zzf paramzzf, int paramInt, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramActivity, paramLooper, 12, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    this.f = paramzzf.a();
    this.a = paramActivity;
    this.g = paramInt;
  }
  
  protected zzpp a(IBinder paramIBinder)
  {
    return zzpp.zza.a(paramIBinder);
  }
  
  protected String a()
  {
    return "com.google.android.gms.identity.service.BIND";
  }
  
  public void a(UserAddressRequest paramUserAddressRequest, int paramInt)
  {
    i();
    this.e = new zza(paramInt, this.a);
    try
    {
      Bundle localBundle = new Bundle();
      localBundle.putString("com.google.android.gms.identity.intents.EXTRA_CALLING_PACKAGE_NAME", q().getPackageName());
      if (!TextUtils.isEmpty(this.f)) {
        localBundle.putParcelable("com.google.android.gms.identity.intents.EXTRA_ACCOUNT", new Account(this.f, "com.google"));
      }
      localBundle.putInt("com.google.android.gms.identity.intents.EXTRA_THEME", this.g);
      h().a(this.e, paramUserAddressRequest, localBundle);
      return;
    }
    catch (RemoteException paramUserAddressRequest)
    {
      Log.e("AddressClientImpl", "Exception requesting user address", paramUserAddressRequest);
      paramUserAddressRequest = new Bundle();
      paramUserAddressRequest.putInt("com.google.android.gms.identity.intents.EXTRA_ERROR_CODE", 555);
      this.e.a(1, paramUserAddressRequest);
    }
  }
  
  protected String b()
  {
    return "com.google.android.gms.identity.intents.internal.IAddressService";
  }
  
  public void f()
  {
    super.f();
    if (this.e != null)
    {
      zza.a(this.e, null);
      this.e = null;
    }
  }
  
  protected zzpp h()
    throws DeadObjectException
  {
    return (zzpp)super.v();
  }
  
  protected void i()
  {
    super.u();
  }
  
  public boolean w()
  {
    return true;
  }
  
  public static final class zza
    extends zzpo.zza
  {
    private final int a;
    private Activity b;
    
    public zza(int paramInt, Activity paramActivity)
    {
      this.a = paramInt;
      this.b = paramActivity;
    }
    
    private void a(Activity paramActivity)
    {
      this.b = paramActivity;
    }
    
    public void a(int paramInt, Bundle paramBundle)
    {
      Object localObject;
      if (paramInt == 1)
      {
        localObject = new Intent();
        ((Intent)localObject).putExtras(paramBundle);
        paramBundle = this.b.createPendingResult(this.a, (Intent)localObject, 1073741824);
        if (paramBundle != null) {}
      }
      for (;;)
      {
        return;
        try
        {
          paramBundle.send(1);
          return;
        }
        catch (PendingIntent.CanceledException paramBundle)
        {
          Log.w("AddressClientImpl", "Exception settng pending result", paramBundle);
          return;
        }
        localObject = null;
        if (paramBundle != null) {
          localObject = (PendingIntent)paramBundle.getParcelable("com.google.android.gms.identity.intents.EXTRA_PENDING_INTENT");
        }
        paramBundle = new ConnectionResult(paramInt, (PendingIntent)localObject);
        if (paramBundle.a()) {
          try
          {
            paramBundle.a(this.b, this.a);
            return;
          }
          catch (IntentSender.SendIntentException paramBundle)
          {
            Log.w("AddressClientImpl", "Exception starting pending intent", paramBundle);
            return;
          }
        }
        try
        {
          paramBundle = this.b.createPendingResult(this.a, new Intent(), 1073741824);
          if (paramBundle != null)
          {
            paramBundle.send(1);
            return;
          }
        }
        catch (PendingIntent.CanceledException paramBundle)
        {
          Log.w("AddressClientImpl", "Exception setting pending result", paramBundle);
        }
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzpn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */