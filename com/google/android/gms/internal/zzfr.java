package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzn;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.zzr;
import java.util.Map;

@zzhb
public class zzfr
  extends zzfs
  implements zzdf
{
  DisplayMetrics a;
  int b = -1;
  int c = -1;
  int d = -1;
  int e = -1;
  int f = -1;
  int g = -1;
  private final zzjp h;
  private final Context i;
  private final WindowManager j;
  private final zzbl k;
  private float l;
  private int m;
  
  public zzfr(zzjp paramzzjp, Context paramContext, zzbl paramzzbl)
  {
    super(paramzzjp);
    this.h = paramzzjp;
    this.i = paramContext;
    this.k = paramzzbl;
    this.j = ((WindowManager)paramContext.getSystemService("window"));
  }
  
  private void g()
  {
    this.a = new DisplayMetrics();
    Display localDisplay = this.j.getDefaultDisplay();
    localDisplay.getMetrics(this.a);
    this.l = this.a.density;
    this.m = localDisplay.getRotation();
  }
  
  private void h()
  {
    int[] arrayOfInt = new int[2];
    this.h.getLocationOnScreen(arrayOfInt);
    a(zzn.a().b(this.i, arrayOfInt[0]), zzn.a().b(this.i, arrayOfInt[1]));
  }
  
  private zzfq i()
  {
    return new zzfq.zza().b(this.k.a()).a(this.k.b()).c(this.k.f()).d(this.k.c()).e(this.k.d()).a();
  }
  
  void a()
  {
    this.b = zzn.a().b(this.a, this.a.widthPixels);
    this.c = zzn.a().b(this.a, this.a.heightPixels);
    Object localObject = this.h.f();
    if ((localObject == null) || (((Activity)localObject).getWindow() == null))
    {
      this.d = this.b;
      this.e = this.c;
      return;
    }
    localObject = zzr.e().a((Activity)localObject);
    this.d = zzn.a().b(this.a, localObject[0]);
    this.e = zzn.a().b(this.a, localObject[1]);
  }
  
  public void a(int paramInt1, int paramInt2)
  {
    if ((this.i instanceof Activity)) {}
    for (int n = zzr.e().d((Activity)this.i)[0];; n = 0)
    {
      b(paramInt1, paramInt2 - n, this.f, this.g);
      this.h.l().a(paramInt1, paramInt2);
      return;
    }
  }
  
  public void a(zzjp paramzzjp, Map<String, String> paramMap)
  {
    c();
  }
  
  void b()
  {
    if (this.h.k().e)
    {
      this.f = this.b;
      this.g = this.c;
      return;
    }
    this.h.measure(0, 0);
    this.f = zzn.a().b(this.i, this.h.getMeasuredWidth());
    this.g = zzn.a().b(this.i, this.h.getMeasuredHeight());
  }
  
  public void c()
  {
    g();
    a();
    b();
    e();
    f();
    h();
    d();
  }
  
  void d()
  {
    if (zzin.a(2)) {
      zzin.c("Dispatching Ready Event.");
    }
    c(this.h.o().b);
  }
  
  void e()
  {
    a(this.b, this.c, this.d, this.e, this.l, this.m);
  }
  
  void f()
  {
    zzfq localzzfq = i();
    this.h.b("onDeviceFeaturesReceived", localzzfq.a());
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzfr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */