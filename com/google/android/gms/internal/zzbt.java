package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.zzr;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

@zzhb
public final class zzbt
{
  public static final zzbp<Boolean> A;
  public static final zzbp<Boolean> B;
  public static final zzbp<Boolean> C;
  public static final zzbp<String> D;
  public static final zzbp<String> E;
  public static final zzbp<String> F;
  public static final zzbp<Boolean> G;
  public static final zzbp<String> H;
  public static final zzbp<Boolean> I;
  public static final zzbp<Boolean> J;
  public static final zzbp<Integer> K;
  public static final zzbp<Integer> L;
  public static final zzbp<Integer> M;
  public static final zzbp<Integer> N;
  public static final zzbp<Integer> O;
  public static final zzbp<Boolean> P;
  public static final zzbp<Boolean> Q;
  public static final zzbp<Long> R;
  public static final zzbp<String> S;
  public static final zzbp<String> T;
  public static final zzbp<Boolean> U;
  public static final zzbp<Boolean> V;
  public static final zzbp<Boolean> W;
  public static final zzbp<String> X;
  public static final zzbp<Boolean> Y;
  public static final zzbp<Boolean> Z;
  public static final zzbp<String> a = zzbp.a(0, "gads:sdk_core_experiment_id");
  public static final zzbp<Boolean> aA = zzbp.a(0, "gass:enabled", Boolean.valueOf(false));
  public static final zzbp<Boolean> aB = zzbp.a(0, "gass:enable_int_signal", Boolean.valueOf(true));
  public static final zzbp<Boolean> aC = zzbp.a(0, "gads:adid_notification:first_party_check:enabled", Boolean.valueOf(true));
  public static final zzbp<Boolean> aD = zzbp.a(0, "gads:edu_device_helper:enabled", Boolean.valueOf(true));
  public static final zzbp<Boolean> aE = zzbp.a(0, "gads:support_screen_shot", Boolean.valueOf(true));
  public static final zzbp<Long> aF = zzbp.a(0, "gads:js_flags:update_interval", TimeUnit.HOURS.toMillis(12L));
  public static final zzbp<Boolean> aG = zzbp.a(0, "gads:custom_render:ping_on_ad_rendered", Boolean.valueOf(false));
  public static final zzbp<Integer> aa;
  public static final zzbp<String> ab;
  public static final zzbp<String> ac;
  public static final zzbp<Boolean> ad;
  public static final zzbp<Boolean> ae;
  public static final zzbp<String> af;
  public static final zzbp<Integer> ag;
  public static final zzbp<Integer> ah;
  public static final zzbp<Integer> ai;
  public static final zzbp<String> aj;
  public static final zzbp<Boolean> ak;
  public static final zzbp<Boolean> al;
  public static final zzbp<Long> am;
  public static final zzbp<Boolean> an;
  public static final zzbp<Boolean> ao;
  public static final zzbp<Boolean> ap;
  public static final zzbp<Boolean> aq;
  public static final zzbp<Boolean> ar;
  public static final zzbp<Boolean> as;
  public static final zzbp<Boolean> at;
  public static final zzbp<Long> au;
  public static final zzbp<Boolean> av;
  public static final zzbp<Boolean> aw;
  public static final zzbp<Long> ax;
  public static final zzbp<Long> ay;
  public static final zzbp<Boolean> az;
  public static final zzbp<String> b = zzbp.a(0, "gads:sdk_core_location", "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/sdk-core-v40.html");
  public static final zzbp<Boolean> c = zzbp.a(0, "gads:request_builder:singleton_webview", Boolean.valueOf(false));
  public static final zzbp<String> d = zzbp.a(0, "gads:request_builder:singleton_webview_experiment_id");
  public static final zzbp<Boolean> e = zzbp.a(0, "gads:sdk_use_dynamic_module", Boolean.valueOf(false));
  public static final zzbp<String> f = zzbp.a(0, "gads:sdk_use_dynamic_module_experiment_id");
  public static final zzbp<Boolean> g = zzbp.a(0, "gads:sdk_crash_report_enabled", Boolean.valueOf(false));
  public static final zzbp<Boolean> h = zzbp.a(0, "gads:sdk_crash_report_full_stacktrace", Boolean.valueOf(false));
  public static final zzbp<Boolean> i = zzbp.a(0, "gads:block_autoclicks", Boolean.valueOf(false));
  public static final zzbp<String> j = zzbp.a(0, "gads:block_autoclicks_experiment_id");
  public static final zzbp<String> k = zzbp.b(0, "gads:prefetch:experiment_id");
  public static final zzbp<String> l = zzbp.a(0, "gads:spam_app_context:experiment_id");
  public static final zzbp<Boolean> m = zzbp.a(0, "gads:spam_app_context:enabled", Boolean.valueOf(false));
  public static final zzbp<String> n = zzbp.a(0, "gads:video_stream_cache:experiment_id");
  public static final zzbp<Integer> o = zzbp.a(0, "gads:video_stream_cache:limit_count", 5);
  public static final zzbp<Integer> p = zzbp.a(0, "gads:video_stream_cache:limit_space", 8388608);
  public static final zzbp<Integer> q = zzbp.a(0, "gads:video_stream_exo_cache:buffer_size", 8388608);
  public static final zzbp<Long> r = zzbp.a(0, "gads:video_stream_cache:limit_time_sec", 300L);
  public static final zzbp<Long> s = zzbp.a(0, "gads:video_stream_cache:notify_interval_millis", 1000L);
  public static final zzbp<Integer> t = zzbp.a(0, "gads:video_stream_cache:connect_timeout_millis", 10000);
  public static final zzbp<Boolean> u = zzbp.a(0, "gads:video:metric_reporting_enabled", Boolean.valueOf(false));
  public static final zzbp<String> v = zzbp.a(0, "gads:video:metric_frame_hash_times", "");
  public static final zzbp<Long> w = zzbp.a(0, "gads:video:metric_frame_hash_time_leniency", 500L);
  public static final zzbp<String> x = zzbp.b(0, "gads:spam_ad_id_decorator:experiment_id");
  public static final zzbp<Boolean> y = zzbp.a(0, "gads:spam_ad_id_decorator:enabled", Boolean.valueOf(false));
  public static final zzbp<String> z = zzbp.b(0, "gads:looper_for_gms_client:experiment_id");
  
  static
  {
    A = zzbp.a(0, "gads:looper_for_gms_client:enabled", Boolean.valueOf(true));
    B = zzbp.a(0, "gads:sw_ad_request_service:enabled", Boolean.valueOf(true));
    C = zzbp.a(0, "gads:sw_dynamite:enabled", Boolean.valueOf(true));
    D = zzbp.a(0, "gad:mraid:url_banner", "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/mraid/v2/mraid_app_banner.js");
    E = zzbp.a(0, "gad:mraid:url_expanded_banner", "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/mraid/v2/mraid_app_expanded_banner.js");
    F = zzbp.a(0, "gad:mraid:url_interstitial", "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/mraid/v2/mraid_app_interstitial.js");
    G = zzbp.a(0, "gads:enabled_sdk_csi", Boolean.valueOf(false));
    H = zzbp.a(0, "gads:sdk_csi_server", "https://csi.gstatic.com/csi");
    I = zzbp.a(0, "gads:sdk_csi_write_to_file", Boolean.valueOf(false));
    J = zzbp.a(0, "gads:enable_content_fetching", Boolean.valueOf(true));
    K = zzbp.a(0, "gads:content_length_weight", 1);
    L = zzbp.a(0, "gads:content_age_weight", 1);
    M = zzbp.a(0, "gads:min_content_len", 11);
    N = zzbp.a(0, "gads:fingerprint_number", 10);
    O = zzbp.a(0, "gads:sleep_sec", 10);
    P = zzbp.a(0, "gad:app_index_enabled", Boolean.valueOf(true));
    Q = zzbp.a(0, "gads:app_index:without_content_info_present:enabled", Boolean.valueOf(true));
    R = zzbp.a(0, "gads:app_index:timeout_ms", 1000L);
    S = zzbp.a(0, "gads:app_index:experiment_id");
    T = zzbp.a(0, "gads:kitkat_interstitial_workaround:experiment_id");
    U = zzbp.a(0, "gads:kitkat_interstitial_workaround:enabled", Boolean.valueOf(true));
    V = zzbp.a(0, "gads:interstitial_follow_url", Boolean.valueOf(true));
    W = zzbp.a(0, "gads:interstitial_follow_url:register_click", Boolean.valueOf(true));
    X = zzbp.a(0, "gads:interstitial_follow_url:experiment_id");
    Y = zzbp.a(0, "gads:analytics_enabled", Boolean.valueOf(true));
    Z = zzbp.a(0, "gads:ad_key_enabled", Boolean.valueOf(false));
    aa = zzbp.a(0, "gads:webview_cache_version", 0);
    ab = zzbp.b(0, "gads:pan:experiment_id");
    ac = zzbp.a(0, "gads:native:engine_url", "//googleads.g.doubleclick.net/mads/static/mad/sdk/native/native_ads.html");
    ad = zzbp.a(0, "gads:ad_manager_creator:enabled", Boolean.valueOf(true));
    ae = zzbp.a(1, "gads:interstitial_ad_pool:enabled", Boolean.valueOf(false));
    af = zzbp.a(1, "gads:interstitial_ad_pool:schema", "customTargeting");
    ag = zzbp.a(1, "gads:interstitial_ad_pool:max_pools", 3);
    ah = zzbp.a(1, "gads:interstitial_ad_pool:max_pool_depth", 2);
    ai = zzbp.a(1, "gads:interstitial_ad_pool:time_limit_sec", 1200);
    aj = zzbp.a(1, "gads:interstitial_ad_pool:experiment_id");
    ak = zzbp.a(0, "gads:log:verbose_enabled", Boolean.valueOf(false));
    al = zzbp.a(0, "gads:device_info_caching:enabled", Boolean.valueOf(true));
    am = zzbp.a(0, "gads:device_info_caching_expiry_ms:expiry", 300000L);
    an = zzbp.a(0, "gads:gen204_signals:enabled", Boolean.valueOf(false));
    ao = zzbp.a(0, "gads:webview:error_reporting_enabled", Boolean.valueOf(false));
    ap = zzbp.a(0, "gads:adid_reporting:enabled", Boolean.valueOf(false));
    aq = zzbp.a(0, "gads:ad_settings_page_reporting:enabled", Boolean.valueOf(false));
    ar = zzbp.a(0, "gads:adid_info_gmscore_upgrade_reporting:enabled", Boolean.valueOf(false));
    as = zzbp.a(0, "gads:request_pkg:enabled", Boolean.valueOf(true));
    at = zzbp.a(0, "gads:gmsg:disable_back_button:enabled", Boolean.valueOf(false));
    au = zzbp.a(0, "gads:network:cache_prediction_duration_s", 300L);
    av = zzbp.a(0, "gads:mediation:dynamite_first:admobadapter", Boolean.valueOf(true));
    aw = zzbp.a(0, "gads:mediation:dynamite_first:adurladapter", Boolean.valueOf(true));
    ax = zzbp.a(0, "gads:ad_loader:timeout_ms", 60000L);
    ay = zzbp.a(0, "gads:rendering:timeout_ms", 60000L);
    az = zzbp.a(0, "gads:adshield:enable_adshield_instrumentation", Boolean.valueOf(false));
  }
  
  public static List<String> a()
  {
    return zzr.m().a();
  }
  
  public static void a(Context paramContext)
  {
    zzjb.a(new Callable()
    {
      public Void a()
      {
        zzr.n().a(this.a);
        return null;
      }
    });
  }
  
  public static List<String> b()
  {
    return zzr.m().b();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzbt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */