package com.google.android.gms.internal;

import android.content.Intent;
import com.google.android.gms.auth.api.signin.zzd;

public abstract interface zzlf
{
  public abstract zzd a();
  
  public abstract void a(zza paramzza);
  
  public abstract void a(String paramString, zza paramzza);
  
  public abstract void a(String paramString1, String paramString2, zza paramzza);
  
  public abstract boolean a(int paramInt1, int paramInt2, Intent paramIntent, zza paramzza);
  
  public static abstract interface zza
  {
    public abstract void a(Intent paramIntent);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzlf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */