package com.google.android.gms.internal;

import android.os.Process;

class zznl
  implements Runnable
{
  private final Runnable a;
  private final int b;
  
  public zznl(Runnable paramRunnable, int paramInt)
  {
    this.a = paramRunnable;
    this.b = paramInt;
  }
  
  public void run()
  {
    Process.setThreadPriority(this.b);
    this.a.run();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zznl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */