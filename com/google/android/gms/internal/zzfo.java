package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzr;
import java.util.Map;

@zzhb
public class zzfo
{
  private final zzjp a;
  private final boolean b;
  private final String c;
  
  public zzfo(zzjp paramzzjp, Map<String, String> paramMap)
  {
    this.a = paramzzjp;
    this.c = ((String)paramMap.get("forceOrientation"));
    if (paramMap.containsKey("allowOrientationChange"))
    {
      this.b = Boolean.parseBoolean((String)paramMap.get("allowOrientationChange"));
      return;
    }
    this.b = true;
  }
  
  public void a()
  {
    if (this.a == null)
    {
      zzin.d("AdWebView is null");
      return;
    }
    int i;
    if ("portrait".equalsIgnoreCase(this.c)) {
      i = zzr.g().b();
    }
    for (;;)
    {
      this.a.b(i);
      return;
      if ("landscape".equalsIgnoreCase(this.c)) {
        i = zzr.g().a();
      } else if (this.b) {
        i = -1;
      } else {
        i = zzr.g().c();
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */