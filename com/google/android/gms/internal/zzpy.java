package com.google.android.gms.internal;

import android.text.TextUtils;
import com.google.android.gms.measurement.zze;
import java.util.HashMap;
import java.util.Map;

public final class zzpy
  extends zze<zzpy>
{
  public String a;
  public long b;
  public String c;
  public String d;
  
  public String a()
  {
    return this.a;
  }
  
  public void a(long paramLong)
  {
    this.b = paramLong;
  }
  
  public void a(zzpy paramzzpy)
  {
    if (!TextUtils.isEmpty(this.a)) {
      paramzzpy.a(this.a);
    }
    if (this.b != 0L) {
      paramzzpy.a(this.b);
    }
    if (!TextUtils.isEmpty(this.c)) {
      paramzzpy.b(this.c);
    }
    if (!TextUtils.isEmpty(this.d)) {
      paramzzpy.c(this.d);
    }
  }
  
  public void a(String paramString)
  {
    this.a = paramString;
  }
  
  public long b()
  {
    return this.b;
  }
  
  public void b(String paramString)
  {
    this.c = paramString;
  }
  
  public String c()
  {
    return this.c;
  }
  
  public void c(String paramString)
  {
    this.d = paramString;
  }
  
  public String d()
  {
    return this.d;
  }
  
  public String toString()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("variableName", this.a);
    localHashMap.put("timeInMillis", Long.valueOf(this.b));
    localHashMap.put("category", this.c);
    localHashMap.put("label", this.d);
    return a(localHashMap);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzpy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */