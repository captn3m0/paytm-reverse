package com.google.android.gms.internal;

import android.support.annotation.Nullable;

@zzhb
public class zzbx
{
  @Nullable
  public static zzbz a(@Nullable zzcb paramzzcb)
  {
    if (paramzzcb == null) {
      return null;
    }
    return paramzzcb.a();
  }
  
  @Nullable
  public static zzbz a(@Nullable zzcb paramzzcb, long paramLong)
  {
    if (paramzzcb == null) {
      return null;
    }
    return paramzzcb.a(paramLong);
  }
  
  public static boolean a(@Nullable zzcb paramzzcb, @Nullable zzbz paramzzbz, long paramLong, String... paramVarArgs)
  {
    if ((paramzzcb == null) || (paramzzbz == null)) {
      return false;
    }
    return paramzzcb.a(paramzzbz, paramLong, paramVarArgs);
  }
  
  public static boolean a(@Nullable zzcb paramzzcb, @Nullable zzbz paramzzbz, String... paramVarArgs)
  {
    if ((paramzzcb == null) || (paramzzbz == null)) {
      return false;
    }
    return paramzzcb.a(paramzzbz, paramVarArgs);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzbx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */