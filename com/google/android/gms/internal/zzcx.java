package com.google.android.gms.internal;

import com.google.android.gms.ads.formats.NativeContentAd.OnContentAdLoadedListener;

@zzhb
public class zzcx
  extends zzcs.zza
{
  private final NativeContentAd.OnContentAdLoadedListener a;
  
  public zzcx(NativeContentAd.OnContentAdLoadedListener paramOnContentAdLoadedListener)
  {
    this.a = paramOnContentAdLoadedListener;
  }
  
  public void a(zzcn paramzzcn)
  {
    this.a.a(b(paramzzcn));
  }
  
  zzco b(zzcn paramzzcn)
  {
    return new zzco(paramzzcn);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzcx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */