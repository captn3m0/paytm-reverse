package com.google.android.gms.internal;

public class zznt
{
  public static final zzsy.zzb A = e("food_item");
  public static final zzsy.zzb B = g("nutrients");
  public static final zzsy.zzb C = c("elevation.change");
  public static final zzsy.zzb D = g("elevation.gain");
  public static final zzsy.zzb E = g("elevation.loss");
  public static final zzsy.zzb F = c("floors");
  public static final zzsy.zzb G = g("floor.gain");
  public static final zzsy.zzb H = g("floor.loss");
  public static final zzsy.zzb I = e("exercise");
  public static final zzsy.zzb J = a("repetitions");
  public static final zzsy.zzb K = c("resistance");
  public static final zzsy.zzb L = a("resistance_type");
  public static final zzsy.zzb M = a("num_segments");
  public static final zzsy.zzb N = c("average");
  public static final zzsy.zzb O = c("max");
  public static final zzsy.zzb P = c("min");
  public static final zzsy.zzb Q = c("low_latitude");
  public static final zzsy.zzb R = c("low_longitude");
  public static final zzsy.zzb S = c("high_latitude");
  public static final zzsy.zzb T = c("high_longitude");
  public static final zzsy.zzb U = c("x");
  public static final zzsy.zzb V = c("y");
  public static final zzsy.zzb W = c("z");
  public static final zzsy.zzb X = h("timestamps");
  public static final zzsy.zzb Y = i("sensor_values");
  public static final zzsy.zzb Z = a("sensor_type");
  public static final zzsy.zzb a = a("activity");
  public static final zzsy.zzb aa = e("identifier");
  public static final zzsy.zzb ab = f("name");
  public static final zzsy.zzb ac = f("description");
  public static final zzsy.zzb ad = b("active_time");
  public static final zzsy.zzb b = c("confidence");
  public static final zzsy.zzb c = g("activity_confidence");
  public static final zzsy.zzb d = a("steps");
  public static final zzsy.zzb e = a("duration");
  public static final zzsy.zzb f = g("activity_duration");
  public static final zzsy.zzb g = g("activity_duration.ascending");
  public static final zzsy.zzb h = g("activity_duration.descending");
  public static final zzsy.zzb i = c("bpm");
  public static final zzsy.zzb j = c("latitude");
  public static final zzsy.zzb k = c("longitude");
  public static final zzsy.zzb l = c("accuracy");
  public static final zzsy.zzb m = d("altitude");
  public static final zzsy.zzb n = c("distance");
  public static final zzsy.zzb o = j("google.android.fitness.GoalV2");
  public static final zzsy.zzb p = c("progress");
  public static final zzsy.zzb q = c("height");
  public static final zzsy.zzb r = c("weight");
  public static final zzsy.zzb s = c("circumference");
  public static final zzsy.zzb t = c("percentage");
  public static final zzsy.zzb u = c("speed");
  public static final zzsy.zzb v = c("rpm");
  public static final zzsy.zzb w = a("revolutions");
  public static final zzsy.zzb x = c("calories");
  public static final zzsy.zzb y = c("watts");
  public static final zzsy.zzb z = a("meal_type");
  
  private static zzsy.zzb a(String paramString)
  {
    return a(paramString, 1);
  }
  
  public static zzsy.zzb a(String paramString, int paramInt)
  {
    return a(paramString, paramInt, null);
  }
  
  private static zzsy.zzb a(String paramString, int paramInt, Boolean paramBoolean)
  {
    zzsy.zzb localzzb = new zzsy.zzb();
    localzzb.a = paramString;
    localzzb.b = Integer.valueOf(paramInt);
    if (paramBoolean != null) {
      localzzb.c = paramBoolean;
    }
    return localzzb;
  }
  
  private static zzsy.zzb b(String paramString)
  {
    return a(paramString, 1, Boolean.valueOf(true));
  }
  
  private static zzsy.zzb c(String paramString)
  {
    return a(paramString, 2);
  }
  
  private static zzsy.zzb d(String paramString)
  {
    return a(paramString, 2, Boolean.valueOf(true));
  }
  
  private static zzsy.zzb e(String paramString)
  {
    return a(paramString, 3);
  }
  
  private static zzsy.zzb f(String paramString)
  {
    return a(paramString, 3, Boolean.valueOf(true));
  }
  
  private static zzsy.zzb g(String paramString)
  {
    return a(paramString, 4);
  }
  
  private static zzsy.zzb h(String paramString)
  {
    return a(paramString, 5);
  }
  
  private static zzsy.zzb i(String paramString)
  {
    return a(paramString, 6);
  }
  
  private static zzsy.zzb j(String paramString)
  {
    return a(paramString, 7);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zznt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */