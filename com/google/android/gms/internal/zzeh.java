package com.google.android.gms.internal;

import org.json.JSONObject;

public abstract interface zzeh
{
  public abstract void a(String paramString, zzdf paramzzdf);
  
  public abstract void a(String paramString1, String paramString2);
  
  public abstract void a(String paramString, JSONObject paramJSONObject);
  
  public abstract void b(String paramString, zzdf paramzzdf);
  
  public abstract void b(String paramString, JSONObject paramJSONObject);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzeh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */