package com.google.android.gms.internal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.WorkSource;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.internal.zzd;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.common.stats.zzg;
import com.google.android.gms.common.stats.zzi;

public class zzrp
{
  private static String a = "WakeLock";
  private static String b = "*gcore*:";
  private static boolean c = false;
  private final PowerManager.WakeLock d;
  private WorkSource e;
  private final int f;
  private final String g;
  private final String h;
  private final Context i;
  private boolean j = true;
  private int k;
  private int l;
  
  public zzrp(Context paramContext, int paramInt, String paramString) {}
  
  @SuppressLint({"UnwrappedWakeLock"})
  public zzrp(Context paramContext, int paramInt, String paramString1, String paramString2, String paramString3)
  {
    zzx.a(paramString1, "Wake lock name can NOT be empty");
    this.f = paramInt;
    this.h = paramString2;
    this.i = paramContext.getApplicationContext();
    if ((!zzni.a(paramString3)) && ("com.google.android.gms" != paramString3))
    {
      this.g = (b + paramString1);
      this.d = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(paramInt, paramString1);
      if (zznj.a(this.i))
      {
        paramString1 = paramString3;
        if (zzni.a(paramString3))
        {
          if ((!zzd.a) || (!zzlz.b())) {
            break label195;
          }
          Log.e(a, "callingPackage is not supposed to be empty for wakelock " + this.g + "!", new IllegalArgumentException());
        }
      }
    }
    label195:
    for (paramString1 = "com.google.android.gms";; paramString1 = paramContext.getPackageName())
    {
      this.e = zznj.a(paramContext, paramString1);
      a(this.e);
      return;
      this.g = paramString1;
      break;
    }
  }
  
  private String a(String paramString, boolean paramBoolean)
  {
    if (this.j)
    {
      if (paramBoolean) {
        return paramString;
      }
      return this.h;
    }
    return this.h;
  }
  
  private void a(String paramString)
  {
    boolean bool = b(paramString);
    String str = a(paramString, bool);
    if (c) {
      Log.d(a, "Release:\n mWakeLockName: " + this.g + "\n mSecondaryName: " + this.h + "\nmReferenceCounted: " + this.j + "\nreason: " + paramString + "\n mOpenEventCount" + this.l + "\nuseWithReason: " + bool + "\ntrackingName: " + str);
    }
    try
    {
      if (this.j)
      {
        int m = this.k - 1;
        this.k = m;
        if ((m == 0) || (bool)) {}
      }
      else
      {
        if ((this.j) || (this.l != 1)) {
          break label205;
        }
      }
      zzi.a().a(this.i, zzg.a(this.d, str), 8, this.g, str, this.f, zznj.b(this.e));
      this.l -= 1;
      label205:
      return;
    }
    finally {}
  }
  
  private void a(String paramString, long paramLong)
  {
    boolean bool = b(paramString);
    String str = a(paramString, bool);
    if (c) {
      Log.d(a, "Acquire:\n mWakeLockName: " + this.g + "\n mSecondaryName: " + this.h + "\nmReferenceCounted: " + this.j + "\nreason: " + paramString + "\nmOpenEventCount" + this.l + "\nuseWithReason: " + bool + "\ntrackingName: " + str + "\ntimeout: " + paramLong);
    }
    try
    {
      if (this.j)
      {
        int m = this.k;
        this.k = (m + 1);
        if ((m == 0) || (bool)) {}
      }
      else
      {
        if ((this.j) || (this.l != 0)) {
          break label221;
        }
      }
      zzi.a().a(this.i, zzg.a(this.d, str), 7, this.g, str, this.f, zznj.b(this.e), paramLong);
      this.l += 1;
      label221:
      return;
    }
    finally {}
  }
  
  private boolean b(String paramString)
  {
    return (!TextUtils.isEmpty(paramString)) && (!paramString.equals(this.h));
  }
  
  public void a()
  {
    a(null);
    this.d.release();
  }
  
  public void a(long paramLong)
  {
    if ((!zzne.d()) && (this.j)) {
      Log.wtf(a, "Do not acquire with timeout on reference counted WakeLocks before ICS. wakelock: " + this.g);
    }
    a(null, paramLong);
    this.d.acquire(paramLong);
  }
  
  public void a(WorkSource paramWorkSource)
  {
    if ((zznj.a(this.i)) && (paramWorkSource != null))
    {
      if (this.e == null) {
        break label42;
      }
      this.e.add(paramWorkSource);
    }
    for (;;)
    {
      this.d.setWorkSource(this.e);
      return;
      label42:
      this.e = paramWorkSource;
    }
  }
  
  public void a(boolean paramBoolean)
  {
    this.d.setReferenceCounted(paramBoolean);
    this.j = paramBoolean;
  }
  
  public boolean b()
  {
    return this.d.isHeld();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzrp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */