package com.google.android.gms.internal;

@zzhb
public class zzbz
{
  private final long a;
  private final String b;
  private final zzbz c;
  
  public zzbz(long paramLong, String paramString, zzbz paramzzbz)
  {
    this.a = paramLong;
    this.b = paramString;
    this.c = paramzzbz;
  }
  
  long a()
  {
    return this.a;
  }
  
  String b()
  {
    return this.b;
  }
  
  zzbz c()
  {
    return this.c;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzbz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */