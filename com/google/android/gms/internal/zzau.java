package com.google.android.gms.internal;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.zzh;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzr;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzhb
public abstract class zzau
  implements ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener
{
  protected final Object a = new Object();
  protected final zzaw b;
  BroadcastReceiver c;
  private final WeakReference<zzif> d;
  private WeakReference<ViewTreeObserver> e;
  private final zzbb f;
  private final Context g;
  private final WindowManager h;
  private final PowerManager i;
  private final KeyguardManager j;
  private zzay k;
  private boolean l;
  private boolean m = false;
  private boolean n = false;
  private boolean o;
  private boolean p;
  private boolean q;
  private final HashSet<zzav> r = new HashSet();
  private zziz s;
  private final zzdf t = new zzdf()
  {
    public void a(zzjp paramAnonymouszzjp, Map<String, String> paramAnonymousMap)
    {
      if (!zzau.this.a(paramAnonymousMap)) {
        return;
      }
      zzau.this.a(paramAnonymouszzjp.b(), paramAnonymousMap);
    }
  };
  private final zzdf u = new zzdf()
  {
    public void a(zzjp paramAnonymouszzjp, Map<String, String> paramAnonymousMap)
    {
      if (!zzau.this.a(paramAnonymousMap)) {
        return;
      }
      zzin.a("Received request to untrack: " + zzau.this.b.d());
      zzau.this.c();
    }
  };
  private final zzdf v = new zzdf()
  {
    public void a(zzjp paramAnonymouszzjp, Map<String, String> paramAnonymousMap)
    {
      if (!zzau.this.a(paramAnonymousMap)) {}
      while (!paramAnonymousMap.containsKey("isVisible")) {
        return;
      }
      if (("1".equals(paramAnonymousMap.get("isVisible"))) || ("true".equals(paramAnonymousMap.get("isVisible")))) {}
      for (boolean bool = true;; bool = false)
      {
        zzau.this.a(Boolean.valueOf(bool).booleanValue());
        return;
      }
    }
  };
  
  public zzau(Context paramContext, AdSizeParcel paramAdSizeParcel, zzif paramzzif, VersionInfoParcel paramVersionInfoParcel, zzbb paramzzbb)
  {
    this.d = new WeakReference(paramzzif);
    this.f = paramzzbb;
    this.e = new WeakReference(null);
    this.o = true;
    this.q = false;
    this.s = new zziz(200L);
    this.b = new zzaw(UUID.randomUUID().toString(), paramVersionInfoParcel, paramAdSizeParcel.b, paramzzif.j, paramzzif.a(), paramAdSizeParcel.i);
    this.h = ((WindowManager)paramContext.getSystemService("window"));
    this.i = ((PowerManager)paramContext.getApplicationContext().getSystemService("power"));
    this.j = ((KeyguardManager)paramContext.getSystemService("keyguard"));
    this.g = paramContext;
  }
  
  protected int a(int paramInt, DisplayMetrics paramDisplayMetrics)
  {
    float f1 = paramDisplayMetrics.density;
    return (int)(paramInt / f1);
  }
  
  protected JSONObject a(View paramView)
    throws JSONException
  {
    if (paramView == null) {
      return k();
    }
    boolean bool1 = zzr.g().a(paramView);
    Object localObject2 = new int[2];
    Object localObject1 = new int[2];
    try
    {
      paramView.getLocationOnScreen((int[])localObject2);
      paramView.getLocationInWindow((int[])localObject1);
      localObject1 = paramView.getContext().getResources().getDisplayMetrics();
      Rect localRect1 = new Rect();
      localRect1.left = localObject2[0];
      localRect1.top = localObject2[1];
      localRect1.right = (localRect1.left + paramView.getWidth());
      localRect1.bottom = (localRect1.top + paramView.getHeight());
      localObject2 = new Rect();
      ((Rect)localObject2).right = this.h.getDefaultDisplay().getWidth();
      ((Rect)localObject2).bottom = this.h.getDefaultDisplay().getHeight();
      Rect localRect2 = new Rect();
      boolean bool2 = paramView.getGlobalVisibleRect(localRect2, null);
      Rect localRect3 = new Rect();
      boolean bool3 = paramView.getLocalVisibleRect(localRect3);
      Rect localRect4 = new Rect();
      paramView.getHitRect(localRect4);
      JSONObject localJSONObject = i();
      localJSONObject.put("windowVisibility", paramView.getWindowVisibility()).put("isAttachedToWindow", bool1).put("viewBox", new JSONObject().put("top", a(((Rect)localObject2).top, (DisplayMetrics)localObject1)).put("bottom", a(((Rect)localObject2).bottom, (DisplayMetrics)localObject1)).put("left", a(((Rect)localObject2).left, (DisplayMetrics)localObject1)).put("right", a(((Rect)localObject2).right, (DisplayMetrics)localObject1))).put("adBox", new JSONObject().put("top", a(localRect1.top, (DisplayMetrics)localObject1)).put("bottom", a(localRect1.bottom, (DisplayMetrics)localObject1)).put("left", a(localRect1.left, (DisplayMetrics)localObject1)).put("right", a(localRect1.right, (DisplayMetrics)localObject1))).put("globalVisibleBox", new JSONObject().put("top", a(localRect2.top, (DisplayMetrics)localObject1)).put("bottom", a(localRect2.bottom, (DisplayMetrics)localObject1)).put("left", a(localRect2.left, (DisplayMetrics)localObject1)).put("right", a(localRect2.right, (DisplayMetrics)localObject1))).put("globalVisibleBoxVisible", bool2).put("localVisibleBox", new JSONObject().put("top", a(localRect3.top, (DisplayMetrics)localObject1)).put("bottom", a(localRect3.bottom, (DisplayMetrics)localObject1)).put("left", a(localRect3.left, (DisplayMetrics)localObject1)).put("right", a(localRect3.right, (DisplayMetrics)localObject1))).put("localVisibleBoxVisible", bool3).put("hitBox", new JSONObject().put("top", a(localRect4.top, (DisplayMetrics)localObject1)).put("bottom", a(localRect4.bottom, (DisplayMetrics)localObject1)).put("left", a(localRect4.left, (DisplayMetrics)localObject1)).put("right", a(localRect4.right, (DisplayMetrics)localObject1))).put("screenDensity", ((DisplayMetrics)localObject1).density).put("isVisible", zzr.e().a(paramView, this.i, this.j));
      return localJSONObject;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        zzin.b("Failure getting view location.", localException);
      }
    }
  }
  
  protected void a()
  {
    synchronized (this.a)
    {
      if (this.c != null) {
        return;
      }
      IntentFilter localIntentFilter = new IntentFilter();
      localIntentFilter.addAction("android.intent.action.SCREEN_ON");
      localIntentFilter.addAction("android.intent.action.SCREEN_OFF");
      this.c = new BroadcastReceiver()
      {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
          zzau.this.b(false);
        }
      };
      this.g.registerReceiver(this.c, localIntentFilter);
      return;
    }
  }
  
  protected void a(View paramView, Map<String, String> paramMap)
  {
    b(false);
  }
  
  public void a(zzav paramzzav)
  {
    this.r.add(paramzzav);
  }
  
  public void a(zzay paramzzay)
  {
    synchronized (this.a)
    {
      this.k = paramzzay;
      return;
    }
  }
  
  protected void a(zzeh paramzzeh)
  {
    paramzzeh.a("/updateActiveView", this.t);
    paramzzeh.a("/untrackActiveViewUnit", this.u);
    paramzzeh.a("/visibilityChanged", this.v);
  }
  
  protected void a(JSONObject paramJSONObject)
  {
    try
    {
      JSONArray localJSONArray = new JSONArray();
      JSONObject localJSONObject = new JSONObject();
      localJSONArray.put(paramJSONObject);
      localJSONObject.put("units", localJSONArray);
      b(localJSONObject);
      return;
    }
    catch (Throwable paramJSONObject)
    {
      zzin.b("Skipping active view message.", paramJSONObject);
    }
  }
  
  protected void a(boolean paramBoolean)
  {
    Iterator localIterator = this.r.iterator();
    while (localIterator.hasNext()) {
      ((zzav)localIterator.next()).a(this, paramBoolean);
    }
  }
  
  protected boolean a(Map<String, String> paramMap)
  {
    if (paramMap == null) {
      return false;
    }
    paramMap = (String)paramMap.get("hashCode");
    if ((!TextUtils.isEmpty(paramMap)) && (paramMap.equals(this.b.d()))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  protected void b()
  {
    synchronized (this.a)
    {
      BroadcastReceiver localBroadcastReceiver = this.c;
      if (localBroadcastReceiver == null) {}
    }
    try
    {
      this.g.unregisterReceiver(this.c);
      this.c = null;
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      for (;;)
      {
        zzin.b("Failed trying to unregister the receiver", localIllegalStateException);
      }
      localObject2 = finally;
      throw ((Throwable)localObject2);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        zzr.h().a(localException, true);
      }
    }
  }
  
  protected void b(zzeh paramzzeh)
  {
    paramzzeh.b("/visibilityChanged", this.v);
    paramzzeh.b("/untrackActiveViewUnit", this.u);
    paramzzeh.b("/updateActiveView", this.t);
  }
  
  protected abstract void b(JSONObject paramJSONObject);
  
  protected void b(boolean paramBoolean)
  {
    boolean bool;
    for (;;)
    {
      synchronized (this.a)
      {
        if ((!j()) || (!this.o)) {
          return;
        }
        View localView1 = this.f.a();
        if ((localView1 != null) && (zzr.e().a(localView1, this.i, this.j)) && (localView1.getGlobalVisibleRect(new Rect(), null)))
        {
          bool = true;
          if ((!paramBoolean) || (this.s.a()) || (bool != this.q)) {
            break;
          }
          return;
        }
      }
      bool = false;
    }
    this.q = bool;
    if (this.f.b())
    {
      d();
      return;
    }
    for (;;)
    {
      try
      {
        a(a(localView2));
        g();
        e();
        return;
      }
      catch (JSONException localJSONException)
      {
        continue;
      }
      catch (RuntimeException localRuntimeException)
      {
        continue;
      }
      zzin.a("Active view update failed.", localView2);
    }
  }
  
  protected void c()
  {
    synchronized (this.a)
    {
      h();
      b();
      this.o = false;
      e();
      return;
    }
  }
  
  public void d()
  {
    synchronized (this.a)
    {
      if (this.o) {
        this.p = true;
      }
    }
    try
    {
      a(m());
      zzin.a("Untracking ad unit: " + this.b.d());
      return;
    }
    catch (JSONException localJSONException)
    {
      for (;;)
      {
        zzin.b("JSON failure while processing active view data.", localJSONException);
      }
      localObject2 = finally;
      throw ((Throwable)localObject2);
    }
    catch (RuntimeException localRuntimeException)
    {
      for (;;)
      {
        zzin.b("Failure while processing active view data.", localRuntimeException);
      }
    }
  }
  
  protected void e()
  {
    if (this.k != null) {
      this.k.a(this);
    }
  }
  
  public boolean f()
  {
    synchronized (this.a)
    {
      boolean bool = this.o;
      return bool;
    }
  }
  
  protected void g()
  {
    Object localObject = this.f.c().a();
    if (localObject == null) {}
    ViewTreeObserver localViewTreeObserver;
    do
    {
      return;
      localViewTreeObserver = (ViewTreeObserver)this.e.get();
      localObject = ((View)localObject).getViewTreeObserver();
    } while (localObject == localViewTreeObserver);
    h();
    if ((!this.l) || ((localViewTreeObserver != null) && (localViewTreeObserver.isAlive())))
    {
      this.l = true;
      ((ViewTreeObserver)localObject).addOnScrollChangedListener(this);
      ((ViewTreeObserver)localObject).addOnGlobalLayoutListener(this);
    }
    this.e = new WeakReference(localObject);
  }
  
  protected void h()
  {
    ViewTreeObserver localViewTreeObserver = (ViewTreeObserver)this.e.get();
    if ((localViewTreeObserver == null) || (!localViewTreeObserver.isAlive())) {
      return;
    }
    localViewTreeObserver.removeOnScrollChangedListener(this);
    localViewTreeObserver.removeGlobalOnLayoutListener(this);
  }
  
  protected JSONObject i()
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject();
    localJSONObject.put("afmaVersion", this.b.b()).put("activeViewJSON", this.b.c()).put("timestamp", zzr.i().b()).put("adFormat", this.b.a()).put("hashCode", this.b.d()).put("isMraid", this.b.e()).put("isStopped", this.n).put("isPaused", this.m).put("isScreenOn", l()).put("isNative", this.b.f());
    return localJSONObject;
  }
  
  protected abstract boolean j();
  
  protected JSONObject k()
    throws JSONException
  {
    return i().put("isAttachedToWindow", false).put("isScreenOn", l()).put("isVisible", false);
  }
  
  boolean l()
  {
    return this.i.isScreenOn();
  }
  
  protected JSONObject m()
    throws JSONException
  {
    JSONObject localJSONObject = i();
    localJSONObject.put("doneReasonCode", "u");
    return localJSONObject;
  }
  
  public void n()
  {
    synchronized (this.a)
    {
      this.n = true;
      b(false);
      return;
    }
  }
  
  public void o()
  {
    synchronized (this.a)
    {
      this.m = true;
      b(false);
      return;
    }
  }
  
  public void onGlobalLayout()
  {
    b(false);
  }
  
  public void onScrollChanged()
  {
    b(true);
  }
  
  public void p()
  {
    synchronized (this.a)
    {
      this.m = false;
      b(false);
      return;
    }
  }
  
  public static class zza
    implements zzbb
  {
    private WeakReference<zzh> a;
    
    public zza(zzh paramzzh)
    {
      this.a = new WeakReference(paramzzh);
    }
    
    public View a()
    {
      zzh localzzh = (zzh)this.a.get();
      if (localzzh != null) {
        return localzzh.e();
      }
      return null;
    }
    
    public boolean b()
    {
      return this.a.get() == null;
    }
    
    public zzbb c()
    {
      return new zzau.zzb((zzh)this.a.get());
    }
  }
  
  public static class zzb
    implements zzbb
  {
    private zzh a;
    
    public zzb(zzh paramzzh)
    {
      this.a = paramzzh;
    }
    
    public View a()
    {
      return this.a.e();
    }
    
    public boolean b()
    {
      return this.a == null;
    }
    
    public zzbb c()
    {
      return this;
    }
  }
  
  public static class zzc
    implements zzbb
  {
    private final View a;
    private final zzif b;
    
    public zzc(View paramView, zzif paramzzif)
    {
      this.a = paramView;
      this.b = paramzzif;
    }
    
    public View a()
    {
      return this.a;
    }
    
    public boolean b()
    {
      return (this.b == null) || (this.a == null);
    }
    
    public zzbb c()
    {
      return this;
    }
  }
  
  public static class zzd
    implements zzbb
  {
    private final WeakReference<View> a;
    private final WeakReference<zzif> b;
    
    public zzd(View paramView, zzif paramzzif)
    {
      this.a = new WeakReference(paramView);
      this.b = new WeakReference(paramzzif);
    }
    
    public View a()
    {
      return (View)this.a.get();
    }
    
    public boolean b()
    {
      return (this.a.get() == null) || (this.b.get() == null);
    }
    
    public zzbb c()
    {
      return new zzau.zzc((View)this.a.get(), (zzif)this.b.get());
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzau.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */