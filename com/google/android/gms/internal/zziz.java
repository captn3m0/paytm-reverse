package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zzr;

@zzhb
public class zziz
{
  private long a;
  private long b = Long.MIN_VALUE;
  private Object c = new Object();
  
  public zziz(long paramLong)
  {
    this.a = paramLong;
  }
  
  public boolean a()
  {
    synchronized (this.c)
    {
      long l = zzr.i().b();
      if (this.b + this.a > l) {
        return false;
      }
      this.b = l;
      return true;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zziz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */