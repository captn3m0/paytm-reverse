package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zzb;
import com.google.android.gms.fitness.ConfigApi;
import com.google.android.gms.fitness.request.DataTypeCreateRequest;
import com.google.android.gms.fitness.request.DataTypeReadRequest;
import com.google.android.gms.fitness.request.DisableFitRequest;
import com.google.android.gms.fitness.result.DataTypeResult;

public class zzpb
  implements ConfigApi
{
  private static class zza
    extends zzoj.zza
  {
    private final zza.zzb<DataTypeResult> a;
    
    private zza(zza.zzb<DataTypeResult> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void a(DataTypeResult paramDataTypeResult)
    {
      this.a.a(paramDataTypeResult);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzpb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */