package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import java.util.Map;
import org.json.JSONObject;

@zzhb
class zzjs
  extends FrameLayout
  implements zzjp
{
  private final zzjp a;
  private final zzjo b;
  
  public zzjs(zzjp paramzzjp)
  {
    super(paramzzjp.getContext());
    this.a = paramzzjp;
    this.b = new zzjo(paramzzjp.g(), this, this);
    paramzzjp = this.a.l();
    if (paramzzjp != null) {
      paramzzjp.a(this);
    }
    addView(this.a.b());
  }
  
  public View.OnClickListener A()
  {
    return this.a.A();
  }
  
  public WebView a()
  {
    return this.a.a();
  }
  
  public void a(int paramInt)
  {
    this.a.a(paramInt);
  }
  
  public void a(Context paramContext)
  {
    this.a.a(paramContext);
  }
  
  public void a(Context paramContext, AdSizeParcel paramAdSizeParcel, zzcb paramzzcb)
  {
    this.a.a(paramContext, paramAdSizeParcel, paramzzcb);
  }
  
  public void a(AdSizeParcel paramAdSizeParcel)
  {
    this.a.a(paramAdSizeParcel);
  }
  
  public void a(com.google.android.gms.ads.internal.overlay.zzd paramzzd)
  {
    this.a.a(paramzzd);
  }
  
  public void a(zzau paramzzau, boolean paramBoolean)
  {
    this.a.a(paramzzau, paramBoolean);
  }
  
  public void a(String paramString)
  {
    this.a.a(paramString);
  }
  
  public void a(String paramString, zzdf paramzzdf)
  {
    this.a.a(paramString, paramzzdf);
  }
  
  public void a(String paramString1, String paramString2)
  {
    this.a.a(paramString1, paramString2);
  }
  
  public void a(String paramString, Map<String, ?> paramMap)
  {
    this.a.a(paramString, paramMap);
  }
  
  public void a(String paramString, JSONObject paramJSONObject)
  {
    this.a.a(paramString, paramJSONObject);
  }
  
  public void a(boolean paramBoolean)
  {
    this.a.a(paramBoolean);
  }
  
  public View b()
  {
    return this;
  }
  
  public void b(int paramInt)
  {
    this.a.b(paramInt);
  }
  
  public void b(com.google.android.gms.ads.internal.overlay.zzd paramzzd)
  {
    this.a.b(paramzzd);
  }
  
  public void b(String paramString)
  {
    this.a.b(paramString);
  }
  
  public void b(String paramString, zzdf paramzzdf)
  {
    this.a.b(paramString, paramzzdf);
  }
  
  public void b(String paramString, JSONObject paramJSONObject)
  {
    this.a.b(paramString, paramJSONObject);
  }
  
  public void b(boolean paramBoolean)
  {
    this.a.b(paramBoolean);
  }
  
  public void c()
  {
    this.a.c();
  }
  
  public void c(boolean paramBoolean)
  {
    this.a.c(paramBoolean);
  }
  
  public void clearCache(boolean paramBoolean)
  {
    this.a.clearCache(paramBoolean);
  }
  
  public void d()
  {
    this.a.d();
  }
  
  public void destroy()
  {
    this.a.destroy();
  }
  
  public void e()
  {
    this.a.e();
  }
  
  public Activity f()
  {
    return this.a.f();
  }
  
  public Context g()
  {
    return this.a.g();
  }
  
  public com.google.android.gms.ads.internal.zzd h()
  {
    return this.a.h();
  }
  
  public com.google.android.gms.ads.internal.overlay.zzd i()
  {
    return this.a.i();
  }
  
  public com.google.android.gms.ads.internal.overlay.zzd j()
  {
    return this.a.j();
  }
  
  public AdSizeParcel k()
  {
    return this.a.k();
  }
  
  public zzjq l()
  {
    return this.a.l();
  }
  
  public void loadData(String paramString1, String paramString2, String paramString3)
  {
    this.a.loadData(paramString1, paramString2, paramString3);
  }
  
  public void loadDataWithBaseURL(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    this.a.loadDataWithBaseURL(paramString1, paramString2, paramString3, paramString4, paramString5);
  }
  
  public void loadUrl(String paramString)
  {
    this.a.loadUrl(paramString);
  }
  
  public boolean m()
  {
    return this.a.m();
  }
  
  public zzan n()
  {
    return this.a.n();
  }
  
  public VersionInfoParcel o()
  {
    return this.a.o();
  }
  
  public void onPause()
  {
    this.b.b();
    this.a.onPause();
  }
  
  public void onResume()
  {
    this.a.onResume();
  }
  
  public boolean p()
  {
    return this.a.p();
  }
  
  public int q()
  {
    return this.a.q();
  }
  
  public boolean r()
  {
    return this.a.r();
  }
  
  public void s()
  {
    this.b.c();
    this.a.s();
  }
  
  public void setBackgroundColor(int paramInt)
  {
    this.a.setBackgroundColor(paramInt);
  }
  
  public void setOnClickListener(View.OnClickListener paramOnClickListener)
  {
    this.a.setOnClickListener(paramOnClickListener);
  }
  
  public void setOnTouchListener(View.OnTouchListener paramOnTouchListener)
  {
    this.a.setOnTouchListener(paramOnTouchListener);
  }
  
  public void setWebChromeClient(WebChromeClient paramWebChromeClient)
  {
    this.a.setWebChromeClient(paramWebChromeClient);
  }
  
  public void setWebViewClient(WebViewClient paramWebViewClient)
  {
    this.a.setWebViewClient(paramWebViewClient);
  }
  
  public void stopLoading()
  {
    this.a.stopLoading();
  }
  
  public boolean t()
  {
    return this.a.t();
  }
  
  public String u()
  {
    return this.a.u();
  }
  
  public zzjo v()
  {
    return this.b;
  }
  
  public zzbz w()
  {
    return this.a.w();
  }
  
  public zzca x()
  {
    return this.a.x();
  }
  
  public void y()
  {
    this.a.y();
  }
  
  public void z()
  {
    this.a.z();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzjs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */