package com.google.android.gms.internal;

import android.os.Process;
import java.util.concurrent.BlockingQueue;

public class zzc
  extends Thread
{
  private static final boolean a = zzs.b;
  private final BlockingQueue<zzk<?>> b;
  private final BlockingQueue<zzk<?>> c;
  private final zzb d;
  private final zzn e;
  private volatile boolean f = false;
  
  public zzc(BlockingQueue<zzk<?>> paramBlockingQueue1, BlockingQueue<zzk<?>> paramBlockingQueue2, zzb paramzzb, zzn paramzzn)
  {
    this.b = paramBlockingQueue1;
    this.c = paramBlockingQueue2;
    this.d = paramzzb;
    this.e = paramzzn;
  }
  
  public void a()
  {
    this.f = true;
    interrupt();
  }
  
  public void run()
  {
    if (a) {
      zzs.a("start new dispatcher", new Object[0]);
    }
    Process.setThreadPriority(10);
    this.d.a();
    for (;;)
    {
      try
      {
        zzk localzzk = (zzk)this.b.take();
        localzzk.b("cache-queue-take");
        if (!localzzk.g()) {
          break label73;
        }
        localzzk.c("cache-discard-canceled");
        continue;
        if (!this.f) {
          continue;
        }
      }
      catch (InterruptedException localInterruptedException) {}
      return;
      label73:
      zzb.zza localzza = this.d.a(localInterruptedException.e());
      if (localzza == null)
      {
        localInterruptedException.b("cache-miss");
        this.c.put(localInterruptedException);
      }
      else if (localzza.a())
      {
        localInterruptedException.b("cache-hit-expired");
        localInterruptedException.a(localzza);
        this.c.put(localInterruptedException);
      }
      else
      {
        localInterruptedException.b("cache-hit");
        zzm localzzm = localInterruptedException.a(new zzi(localzza.a, localzza.g));
        localInterruptedException.b("cache-hit-parsed");
        if (!localzza.b())
        {
          this.e.a(localInterruptedException, localzzm);
        }
        else
        {
          localInterruptedException.b("cache-hit-refresh-needed");
          localInterruptedException.a(localzza);
          localzzm.d = true;
          this.e.a(localInterruptedException, localzzm, new Runnable()
          {
            public void run()
            {
              try
              {
                zzc.a(zzc.this).put(localInterruptedException);
                return;
              }
              catch (InterruptedException localInterruptedException) {}
            }
          });
        }
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */