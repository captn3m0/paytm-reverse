package com.google.android.gms.internal;

import java.io.IOException;
import java.util.Arrays;

public final class zzsj
  extends zzso<zzsj>
{
  public zza[] a;
  
  public zzsj()
  {
    a();
  }
  
  public static zzsj a(byte[] paramArrayOfByte)
    throws zzst
  {
    return (zzsj)zzsu.a(new zzsj(), paramArrayOfByte);
  }
  
  public zzsj a()
  {
    this.a = zza.a();
    this.r = null;
    this.S = -1;
    return this;
  }
  
  public zzsj a(zzsm paramzzsm)
    throws IOException
  {
    for (;;)
    {
      int i = paramzzsm.a();
      switch (i)
      {
      default: 
        if (a(paramzzsm, i)) {}
        break;
      case 0: 
        return this;
      case 10: 
        int j = zzsx.b(paramzzsm, 10);
        if (this.a == null) {}
        zza[] arrayOfzza;
        for (i = 0;; i = this.a.length)
        {
          arrayOfzza = new zza[j + i];
          j = i;
          if (i != 0)
          {
            System.arraycopy(this.a, 0, arrayOfzza, 0, i);
            j = i;
          }
          while (j < arrayOfzza.length - 1)
          {
            arrayOfzza[j] = new zza();
            paramzzsm.a(arrayOfzza[j]);
            paramzzsm.a();
            j += 1;
          }
        }
        arrayOfzza[j] = new zza();
        paramzzsm.a(arrayOfzza[j]);
        this.a = arrayOfzza;
      }
    }
  }
  
  public void a(zzsn paramzzsn)
    throws IOException
  {
    if ((this.a != null) && (this.a.length > 0))
    {
      int i = 0;
      while (i < this.a.length)
      {
        zza localzza = this.a[i];
        if (localzza != null) {
          paramzzsn.a(1, localzza);
        }
        i += 1;
      }
    }
    super.a(paramzzsn);
  }
  
  protected int b()
  {
    int i = super.b();
    int k = i;
    if (this.a != null)
    {
      k = i;
      if (this.a.length > 0)
      {
        int j = 0;
        for (;;)
        {
          k = i;
          if (j >= this.a.length) {
            break;
          }
          zza localzza = this.a[j];
          k = i;
          if (localzza != null) {
            k = i + zzsn.c(1, localzza);
          }
          j += 1;
          i = k;
        }
      }
    }
    return k;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1;
    if (paramObject == this) {
      bool1 = true;
    }
    do
    {
      do
      {
        do
        {
          return bool1;
          bool1 = bool2;
        } while (!(paramObject instanceof zzsj));
        paramObject = (zzsj)paramObject;
        bool1 = bool2;
      } while (!zzss.a(this.a, ((zzsj)paramObject).a));
      if ((this.r != null) && (!this.r.b())) {
        break label79;
      }
      if (((zzsj)paramObject).r == null) {
        break;
      }
      bool1 = bool2;
    } while (!((zzsj)paramObject).r.b());
    return true;
    label79:
    return this.r.equals(((zzsj)paramObject).r);
  }
  
  public int hashCode()
  {
    int j = getClass().getName().hashCode();
    int k = zzss.a(this.a);
    if ((this.r == null) || (this.r.b())) {}
    for (int i = 0;; i = this.r.hashCode()) {
      return i + ((j + 527) * 31 + k) * 31;
    }
  }
  
  public static final class zza
    extends zzso<zza>
  {
    private static volatile zza[] c;
    public String a;
    public zza b;
    
    public zza()
    {
      c();
    }
    
    public static zza[] a()
    {
      if (c == null) {}
      synchronized (zzss.a)
      {
        if (c == null) {
          c = new zza[0];
        }
        return c;
      }
    }
    
    public zza a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 10: 
          this.a = paramzzsm.i();
          break;
        case 18: 
          if (this.b == null) {
            this.b = new zza();
          }
          paramzzsm.a(this.b);
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      paramzzsn.a(1, this.a);
      if (this.b != null) {
        paramzzsn.a(2, this.b);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b() + zzsn.b(1, this.a);
      int i = j;
      if (this.b != null) {
        i = j + zzsn.c(2, this.b);
      }
      return i;
    }
    
    public zza c()
    {
      this.a = "";
      this.b = null;
      this.r = null;
      this.S = -1;
      return this;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramObject == this) {
        bool1 = true;
      }
      label41:
      do
      {
        do
        {
          do
          {
            return bool1;
            bool1 = bool2;
          } while (!(paramObject instanceof zza));
          paramObject = (zza)paramObject;
          if (this.a != null) {
            break;
          }
          bool1 = bool2;
        } while (((zza)paramObject).a != null);
        if (this.b != null) {
          break label111;
        }
        bool1 = bool2;
      } while (((zza)paramObject).b != null);
      for (;;)
      {
        if ((this.r == null) || (this.r.b()))
        {
          if (((zza)paramObject).r != null)
          {
            bool1 = bool2;
            if (!((zza)paramObject).r.b()) {
              break;
            }
          }
          return true;
          if (this.a.equals(((zza)paramObject).a)) {
            break label41;
          }
          return false;
          label111:
          if (!this.b.equals(((zza)paramObject).b)) {
            return false;
          }
        }
      }
      return this.r.equals(((zza)paramObject).r);
    }
    
    public int hashCode()
    {
      int m = 0;
      int n = getClass().getName().hashCode();
      int i;
      int j;
      if (this.a == null)
      {
        i = 0;
        if (this.b != null) {
          break label89;
        }
        j = 0;
        label33:
        k = m;
        if (this.r != null) {
          if (!this.r.b()) {
            break label100;
          }
        }
      }
      label89:
      label100:
      for (int k = m;; k = this.r.hashCode())
      {
        return (j + (i + (n + 527) * 31) * 31) * 31 + k;
        i = this.a.hashCode();
        break;
        j = this.b.hashCode();
        break label33;
      }
    }
    
    public static final class zza
      extends zzso<zza>
    {
      private static volatile zza[] c;
      public int a;
      public zza b;
      
      public zza()
      {
        c();
      }
      
      public static zza[] a()
      {
        if (c == null) {}
        synchronized (zzss.a)
        {
          if (c == null) {
            c = new zza[0];
          }
          return c;
        }
      }
      
      public zza a(zzsm paramzzsm)
        throws IOException
      {
        for (;;)
        {
          int i = paramzzsm.a();
          switch (i)
          {
          default: 
            if (a(paramzzsm, i)) {}
            break;
          case 0: 
            return this;
          case 8: 
            i = paramzzsm.g();
            switch (i)
            {
            default: 
              break;
            case 1: 
            case 2: 
            case 3: 
            case 4: 
            case 5: 
            case 6: 
            case 7: 
            case 8: 
            case 9: 
            case 10: 
            case 11: 
            case 12: 
            case 13: 
            case 14: 
            case 15: 
              this.a = i;
            }
            break;
          case 18: 
            if (this.b == null) {
              this.b = new zza();
            }
            paramzzsm.a(this.b);
          }
        }
      }
      
      public void a(zzsn paramzzsn)
        throws IOException
      {
        paramzzsn.a(1, this.a);
        if (this.b != null) {
          paramzzsn.a(2, this.b);
        }
        super.a(paramzzsn);
      }
      
      protected int b()
      {
        int j = super.b() + zzsn.c(1, this.a);
        int i = j;
        if (this.b != null) {
          i = j + zzsn.c(2, this.b);
        }
        return i;
      }
      
      public zza c()
      {
        this.a = 1;
        this.b = null;
        this.r = null;
        this.S = -1;
        return this;
      }
      
      public boolean equals(Object paramObject)
      {
        boolean bool2 = false;
        boolean bool1;
        if (paramObject == this) {
          bool1 = true;
        }
        do
        {
          do
          {
            do
            {
              return bool1;
              bool1 = bool2;
            } while (!(paramObject instanceof zza));
            paramObject = (zza)paramObject;
            bool1 = bool2;
          } while (this.a != ((zza)paramObject).a);
          if (this.b != null) {
            break;
          }
          bool1 = bool2;
        } while (((zza)paramObject).b != null);
        for (;;)
        {
          if ((this.r == null) || (this.r.b()))
          {
            if (((zza)paramObject).r != null)
            {
              bool1 = bool2;
              if (!((zza)paramObject).r.b()) {
                break;
              }
            }
            return true;
            if (!this.b.equals(((zza)paramObject).b)) {
              return false;
            }
          }
        }
        return this.r.equals(((zza)paramObject).r);
      }
      
      public int hashCode()
      {
        int k = 0;
        int m = getClass().getName().hashCode();
        int n = this.a;
        int i;
        if (this.b == null)
        {
          i = 0;
          j = k;
          if (this.r != null) {
            if (!this.r.b()) {
              break label84;
            }
          }
        }
        label84:
        for (int j = k;; j = this.r.hashCode())
        {
          return (i + ((m + 527) * 31 + n) * 31) * 31 + j;
          i = this.b.hashCode();
          break;
        }
      }
      
      public static final class zza
        extends zzso<zza>
      {
        public byte[] a;
        public String b;
        public double c;
        public float d;
        public long e;
        public int f;
        public int g;
        public boolean h;
        public zzsj.zza[] i;
        public zzsj.zza.zza[] j;
        public String[] k;
        public long[] l;
        public float[] m;
        public long n;
        
        public zza()
        {
          a();
        }
        
        public zza a()
        {
          this.a = zzsx.h;
          this.b = "";
          this.c = 0.0D;
          this.d = 0.0F;
          this.e = 0L;
          this.f = 0;
          this.g = 0;
          this.h = false;
          this.i = zzsj.zza.a();
          this.j = zzsj.zza.zza.a();
          this.k = zzsx.f;
          this.l = zzsx.b;
          this.m = zzsx.c;
          this.n = 0L;
          this.r = null;
          this.S = -1;
          return this;
        }
        
        public zza a(zzsm paramzzsm)
          throws IOException
        {
          for (;;)
          {
            int i1 = paramzzsm.a();
            int i2;
            Object localObject;
            int i3;
            switch (i1)
            {
            default: 
              if (a(paramzzsm, i1)) {}
              break;
            case 0: 
              return this;
            case 10: 
              this.a = paramzzsm.j();
              break;
            case 18: 
              this.b = paramzzsm.i();
              break;
            case 25: 
              this.c = paramzzsm.c();
              break;
            case 37: 
              this.d = paramzzsm.d();
              break;
            case 40: 
              this.e = paramzzsm.f();
              break;
            case 48: 
              this.f = paramzzsm.g();
              break;
            case 56: 
              this.g = paramzzsm.k();
              break;
            case 64: 
              this.h = paramzzsm.h();
              break;
            case 74: 
              i2 = zzsx.b(paramzzsm, 74);
              if (this.i == null) {}
              for (i1 = 0;; i1 = this.i.length)
              {
                localObject = new zzsj.zza[i2 + i1];
                i2 = i1;
                if (i1 != 0)
                {
                  System.arraycopy(this.i, 0, localObject, 0, i1);
                  i2 = i1;
                }
                while (i2 < localObject.length - 1)
                {
                  localObject[i2] = new zzsj.zza();
                  paramzzsm.a(localObject[i2]);
                  paramzzsm.a();
                  i2 += 1;
                }
              }
              localObject[i2] = new zzsj.zza();
              paramzzsm.a(localObject[i2]);
              this.i = ((zzsj.zza[])localObject);
              break;
            case 82: 
              i2 = zzsx.b(paramzzsm, 82);
              if (this.j == null) {}
              for (i1 = 0;; i1 = this.j.length)
              {
                localObject = new zzsj.zza.zza[i2 + i1];
                i2 = i1;
                if (i1 != 0)
                {
                  System.arraycopy(this.j, 0, localObject, 0, i1);
                  i2 = i1;
                }
                while (i2 < localObject.length - 1)
                {
                  localObject[i2] = new zzsj.zza.zza();
                  paramzzsm.a(localObject[i2]);
                  paramzzsm.a();
                  i2 += 1;
                }
              }
              localObject[i2] = new zzsj.zza.zza();
              paramzzsm.a(localObject[i2]);
              this.j = ((zzsj.zza.zza[])localObject);
              break;
            case 90: 
              i2 = zzsx.b(paramzzsm, 90);
              if (this.k == null) {}
              for (i1 = 0;; i1 = this.k.length)
              {
                localObject = new String[i2 + i1];
                i2 = i1;
                if (i1 != 0)
                {
                  System.arraycopy(this.k, 0, localObject, 0, i1);
                  i2 = i1;
                }
                while (i2 < localObject.length - 1)
                {
                  localObject[i2] = paramzzsm.i();
                  paramzzsm.a();
                  i2 += 1;
                }
              }
              localObject[i2] = paramzzsm.i();
              this.k = ((String[])localObject);
              break;
            case 96: 
              i2 = zzsx.b(paramzzsm, 96);
              if (this.l == null) {}
              for (i1 = 0;; i1 = this.l.length)
              {
                localObject = new long[i2 + i1];
                i2 = i1;
                if (i1 != 0)
                {
                  System.arraycopy(this.l, 0, localObject, 0, i1);
                  i2 = i1;
                }
                while (i2 < localObject.length - 1)
                {
                  localObject[i2] = paramzzsm.f();
                  paramzzsm.a();
                  i2 += 1;
                }
              }
              localObject[i2] = paramzzsm.f();
              this.l = ((long[])localObject);
              break;
            case 98: 
              i3 = paramzzsm.d(paramzzsm.m());
              i1 = paramzzsm.s();
              i2 = 0;
              while (paramzzsm.q() > 0)
              {
                paramzzsm.f();
                i2 += 1;
              }
              paramzzsm.f(i1);
              if (this.l == null) {}
              for (i1 = 0;; i1 = this.l.length)
              {
                localObject = new long[i2 + i1];
                i2 = i1;
                if (i1 != 0)
                {
                  System.arraycopy(this.l, 0, localObject, 0, i1);
                  i2 = i1;
                }
                while (i2 < localObject.length)
                {
                  localObject[i2] = paramzzsm.f();
                  i2 += 1;
                }
              }
              this.l = ((long[])localObject);
              paramzzsm.e(i3);
              break;
            case 104: 
              this.n = paramzzsm.f();
              break;
            case 117: 
              i2 = zzsx.b(paramzzsm, 117);
              if (this.m == null) {}
              for (i1 = 0;; i1 = this.m.length)
              {
                localObject = new float[i2 + i1];
                i2 = i1;
                if (i1 != 0)
                {
                  System.arraycopy(this.m, 0, localObject, 0, i1);
                  i2 = i1;
                }
                while (i2 < localObject.length - 1)
                {
                  localObject[i2] = paramzzsm.d();
                  paramzzsm.a();
                  i2 += 1;
                }
              }
              localObject[i2] = paramzzsm.d();
              this.m = ((float[])localObject);
              break;
            case 114: 
              i1 = paramzzsm.m();
              i3 = paramzzsm.d(i1);
              i2 = i1 / 4;
              if (this.m == null) {}
              for (i1 = 0;; i1 = this.m.length)
              {
                localObject = new float[i2 + i1];
                i2 = i1;
                if (i1 != 0)
                {
                  System.arraycopy(this.m, 0, localObject, 0, i1);
                  i2 = i1;
                }
                while (i2 < localObject.length)
                {
                  localObject[i2] = paramzzsm.d();
                  i2 += 1;
                }
              }
              this.m = ((float[])localObject);
              paramzzsm.e(i3);
            }
          }
        }
        
        public void a(zzsn paramzzsn)
          throws IOException
        {
          int i2 = 0;
          if (!Arrays.equals(this.a, zzsx.h)) {
            paramzzsn.a(1, this.a);
          }
          if (!this.b.equals("")) {
            paramzzsn.a(2, this.b);
          }
          if (Double.doubleToLongBits(this.c) != Double.doubleToLongBits(0.0D)) {
            paramzzsn.a(3, this.c);
          }
          if (Float.floatToIntBits(this.d) != Float.floatToIntBits(0.0F)) {
            paramzzsn.a(4, this.d);
          }
          if (this.e != 0L) {
            paramzzsn.b(5, this.e);
          }
          if (this.f != 0) {
            paramzzsn.a(6, this.f);
          }
          if (this.g != 0) {
            paramzzsn.b(7, this.g);
          }
          if (this.h) {
            paramzzsn.a(8, this.h);
          }
          int i1;
          Object localObject;
          if ((this.i != null) && (this.i.length > 0))
          {
            i1 = 0;
            while (i1 < this.i.length)
            {
              localObject = this.i[i1];
              if (localObject != null) {
                paramzzsn.a(9, (zzsu)localObject);
              }
              i1 += 1;
            }
          }
          if ((this.j != null) && (this.j.length > 0))
          {
            i1 = 0;
            while (i1 < this.j.length)
            {
              localObject = this.j[i1];
              if (localObject != null) {
                paramzzsn.a(10, (zzsu)localObject);
              }
              i1 += 1;
            }
          }
          if ((this.k != null) && (this.k.length > 0))
          {
            i1 = 0;
            while (i1 < this.k.length)
            {
              localObject = this.k[i1];
              if (localObject != null) {
                paramzzsn.a(11, (String)localObject);
              }
              i1 += 1;
            }
          }
          if ((this.l != null) && (this.l.length > 0))
          {
            i1 = 0;
            while (i1 < this.l.length)
            {
              paramzzsn.b(12, this.l[i1]);
              i1 += 1;
            }
          }
          if (this.n != 0L) {
            paramzzsn.b(13, this.n);
          }
          if ((this.m != null) && (this.m.length > 0))
          {
            i1 = i2;
            while (i1 < this.m.length)
            {
              paramzzsn.a(14, this.m[i1]);
              i1 += 1;
            }
          }
          super.a(paramzzsn);
        }
        
        protected int b()
        {
          int i7 = 0;
          int i2 = super.b();
          int i1 = i2;
          if (!Arrays.equals(this.a, zzsx.h)) {
            i1 = i2 + zzsn.b(1, this.a);
          }
          i2 = i1;
          if (!this.b.equals("")) {
            i2 = i1 + zzsn.b(2, this.b);
          }
          i1 = i2;
          if (Double.doubleToLongBits(this.c) != Double.doubleToLongBits(0.0D)) {
            i1 = i2 + zzsn.b(3, this.c);
          }
          i2 = i1;
          if (Float.floatToIntBits(this.d) != Float.floatToIntBits(0.0F)) {
            i2 = i1 + zzsn.b(4, this.d);
          }
          i1 = i2;
          if (this.e != 0L) {
            i1 = i2 + zzsn.d(5, this.e);
          }
          i2 = i1;
          if (this.f != 0) {
            i2 = i1 + zzsn.c(6, this.f);
          }
          int i3 = i2;
          if (this.g != 0) {
            i3 = i2 + zzsn.d(7, this.g);
          }
          i1 = i3;
          if (this.h) {
            i1 = i3 + zzsn.b(8, this.h);
          }
          i2 = i1;
          Object localObject;
          if (this.i != null)
          {
            i2 = i1;
            if (this.i.length > 0)
            {
              i2 = 0;
              while (i2 < this.i.length)
              {
                localObject = this.i[i2];
                i3 = i1;
                if (localObject != null) {
                  i3 = i1 + zzsn.c(9, (zzsu)localObject);
                }
                i2 += 1;
                i1 = i3;
              }
              i2 = i1;
            }
          }
          i1 = i2;
          if (this.j != null)
          {
            i1 = i2;
            if (this.j.length > 0)
            {
              i1 = i2;
              i2 = 0;
              while (i2 < this.j.length)
              {
                localObject = this.j[i2];
                i3 = i1;
                if (localObject != null) {
                  i3 = i1 + zzsn.c(10, (zzsu)localObject);
                }
                i2 += 1;
                i1 = i3;
              }
            }
          }
          i2 = i1;
          if (this.k != null)
          {
            i2 = i1;
            if (this.k.length > 0)
            {
              i2 = 0;
              i3 = 0;
              int i5;
              for (int i4 = 0; i2 < this.k.length; i4 = i5)
              {
                localObject = this.k[i2];
                int i6 = i3;
                i5 = i4;
                if (localObject != null)
                {
                  i5 = i4 + 1;
                  i6 = i3 + zzsn.b((String)localObject);
                }
                i2 += 1;
                i3 = i6;
              }
              i2 = i1 + i3 + i4 * 1;
            }
          }
          i1 = i2;
          if (this.l != null)
          {
            i1 = i2;
            if (this.l.length > 0)
            {
              i3 = 0;
              i1 = i7;
              while (i1 < this.l.length)
              {
                i3 += zzsn.e(this.l[i1]);
                i1 += 1;
              }
              i1 = i2 + i3 + this.l.length * 1;
            }
          }
          i2 = i1;
          if (this.n != 0L) {
            i2 = i1 + zzsn.d(13, this.n);
          }
          i1 = i2;
          if (this.m != null)
          {
            i1 = i2;
            if (this.m.length > 0) {
              i1 = i2 + this.m.length * 4 + this.m.length * 1;
            }
          }
          return i1;
        }
        
        public boolean equals(Object paramObject)
        {
          boolean bool2 = false;
          boolean bool1;
          if (paramObject == this) {
            bool1 = true;
          }
          do
          {
            do
            {
              do
              {
                return bool1;
                bool1 = bool2;
              } while (!(paramObject instanceof zza));
              paramObject = (zza)paramObject;
              bool1 = bool2;
            } while (!Arrays.equals(this.a, ((zza)paramObject).a));
            if (this.b != null) {
              break;
            }
            bool1 = bool2;
          } while (((zza)paramObject).b != null);
          while (this.b.equals(((zza)paramObject).b))
          {
            bool1 = bool2;
            if (Double.doubleToLongBits(this.c) != Double.doubleToLongBits(((zza)paramObject).c)) {
              break;
            }
            bool1 = bool2;
            if (Float.floatToIntBits(this.d) != Float.floatToIntBits(((zza)paramObject).d)) {
              break;
            }
            bool1 = bool2;
            if (this.e != ((zza)paramObject).e) {
              break;
            }
            bool1 = bool2;
            if (this.f != ((zza)paramObject).f) {
              break;
            }
            bool1 = bool2;
            if (this.g != ((zza)paramObject).g) {
              break;
            }
            bool1 = bool2;
            if (this.h != ((zza)paramObject).h) {
              break;
            }
            bool1 = bool2;
            if (!zzss.a(this.i, ((zza)paramObject).i)) {
              break;
            }
            bool1 = bool2;
            if (!zzss.a(this.j, ((zza)paramObject).j)) {
              break;
            }
            bool1 = bool2;
            if (!zzss.a(this.k, ((zza)paramObject).k)) {
              break;
            }
            bool1 = bool2;
            if (!zzss.a(this.l, ((zza)paramObject).l)) {
              break;
            }
            bool1 = bool2;
            if (!zzss.a(this.m, ((zza)paramObject).m)) {
              break;
            }
            bool1 = bool2;
            if (this.n != ((zza)paramObject).n) {
              break;
            }
            if ((this.r != null) && (!this.r.b())) {
              break label297;
            }
            if (((zza)paramObject).r != null)
            {
              bool1 = bool2;
              if (!((zza)paramObject).r.b()) {
                break;
              }
            }
            return true;
          }
          return false;
          label297:
          return this.r.equals(((zza)paramObject).r);
        }
        
        public int hashCode()
        {
          int i4 = 0;
          int i5 = getClass().getName().hashCode();
          int i6 = Arrays.hashCode(this.a);
          int i1;
          int i7;
          int i8;
          int i9;
          int i10;
          int i11;
          int i2;
          label100:
          int i12;
          int i13;
          int i14;
          int i15;
          int i16;
          int i17;
          if (this.b == null)
          {
            i1 = 0;
            long l1 = Double.doubleToLongBits(this.c);
            i7 = (int)(l1 ^ l1 >>> 32);
            i8 = Float.floatToIntBits(this.d);
            i9 = (int)(this.e ^ this.e >>> 32);
            i10 = this.f;
            i11 = this.g;
            if (!this.h) {
              break label288;
            }
            i2 = 1231;
            i12 = zzss.a(this.i);
            i13 = zzss.a(this.j);
            i14 = zzss.a(this.k);
            i15 = zzss.a(this.l);
            i16 = zzss.a(this.m);
            i17 = (int)(this.n ^ this.n >>> 32);
            i3 = i4;
            if (this.r != null) {
              if (!this.r.b()) {
                break label295;
              }
            }
          }
          label288:
          label295:
          for (int i3 = i4;; i3 = this.r.hashCode())
          {
            return (((((((i2 + ((((((i1 + ((i5 + 527) * 31 + i6) * 31) * 31 + i7) * 31 + i8) * 31 + i9) * 31 + i10) * 31 + i11) * 31) * 31 + i12) * 31 + i13) * 31 + i14) * 31 + i15) * 31 + i16) * 31 + i17) * 31 + i3;
            i1 = this.b.hashCode();
            break;
            i2 = 1237;
            break label100;
          }
        }
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzsj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */