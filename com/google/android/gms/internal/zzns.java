package com.google.android.gms.internal;

import android.annotation.TargetApi;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

@TargetApi(9)
public class zzns
{
  private static final List<TimeUnit> a = Arrays.asList(new TimeUnit[] { TimeUnit.NANOSECONDS, TimeUnit.MICROSECONDS, TimeUnit.MILLISECONDS, TimeUnit.SECONDS, TimeUnit.MINUTES, TimeUnit.HOURS, TimeUnit.DAYS });
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzns.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */