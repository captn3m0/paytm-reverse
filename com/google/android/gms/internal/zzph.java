package com.google.android.gms.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zzb;

public class zzph
  extends zzow.zza
{
  private final zza.zzb<Status> a;
  
  public zzph(zza.zzb<Status> paramzzb)
  {
    this.a = paramzzb;
  }
  
  public void a(Status paramStatus)
  {
    this.a.a(paramStatus);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzph.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */