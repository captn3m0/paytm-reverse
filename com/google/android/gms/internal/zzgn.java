package com.google.android.gms.internal;

import android.content.Context;
import android.os.Handler;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.zzr;
import com.google.android.gms.common.internal.zzx;
import java.util.concurrent.atomic.AtomicBoolean;

@zzhb
public abstract class zzgn
  implements zzit<Void>, zzjq.zza
{
  protected final zzgr.zza a;
  protected final Context b;
  protected final zzjp c;
  protected final zzif.zza d;
  protected AdResponseParcel e;
  protected final Object f = new Object();
  private Runnable g;
  private AtomicBoolean h = new AtomicBoolean(true);
  
  protected zzgn(Context paramContext, zzif.zza paramzza, zzjp paramzzjp, zzgr.zza paramzza1)
  {
    this.b = paramContext;
    this.d = paramzza;
    this.e = this.d.b;
    this.c = paramzzjp;
    this.a = paramzza1;
  }
  
  private zzif b(int paramInt)
  {
    AdRequestInfoParcel localAdRequestInfoParcel = this.d.a;
    return new zzif(localAdRequestInfoParcel.c, this.c, this.e.d, paramInt, this.e.f, this.e.j, this.e.l, this.e.k, localAdRequestInfoParcel.i, this.e.h, null, null, null, null, null, this.e.i, this.d.d, this.e.g, this.d.f, this.e.n, this.e.o, this.d.h, null, this.e.D, this.e.E, this.e.F, this.e.G);
  }
  
  public final Void a()
  {
    zzx.b("Webview render task needs to be called on UI thread.");
    this.g = new Runnable()
    {
      public void run()
      {
        if (!zzgn.a(zzgn.this).get()) {
          return;
        }
        zzin.b("Timed out waiting for WebView to finish loading.");
        zzgn.this.cancel();
      }
    };
    zzir.a.postDelayed(this.g, ((Long)zzbt.ay.c()).longValue());
    b();
    return null;
  }
  
  protected void a(int paramInt)
  {
    if (paramInt != -2) {
      this.e = new AdResponseParcel(paramInt, this.e.k);
    }
    this.c.e();
    this.a.b(b(paramInt));
  }
  
  public void a(zzjp paramzzjp, boolean paramBoolean)
  {
    zzin.a("WebView finished loading.");
    if (!this.h.getAndSet(false)) {
      return;
    }
    if (paramBoolean) {}
    for (int i = c();; i = -1)
    {
      a(i);
      zzir.a.removeCallbacks(this.g);
      return;
    }
  }
  
  protected abstract void b();
  
  protected int c()
  {
    return -2;
  }
  
  public void cancel()
  {
    if (!this.h.getAndSet(false)) {
      return;
    }
    this.c.stopLoading();
    zzr.g().a(this.c);
    a(-1);
    zzir.a.removeCallbacks(this.g);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzgn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */