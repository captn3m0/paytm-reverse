package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zza;
import com.google.android.gms.common.api.internal.zza.zzb;
import com.google.android.gms.panorama.PanoramaApi;
import com.google.android.gms.panorama.PanoramaApi.PanoramaResult;

public class zzqq
  implements PanoramaApi
{
  private static void b(Context paramContext, Uri paramUri)
  {
    paramContext.revokeUriPermission(paramUri, 1);
  }
  
  private static void b(Context paramContext, zzqp paramzzqp, final zzqo paramzzqo, final Uri paramUri, Bundle paramBundle)
    throws RemoteException
  {
    paramContext.grantUriPermission("com.google.android.gms", paramUri, 1);
    paramzzqo = new zzqo.zza()
    {
      public void a(int paramAnonymousInt1, Bundle paramAnonymousBundle, int paramAnonymousInt2, Intent paramAnonymousIntent)
        throws RemoteException
      {
        zzqq.a(this.a, paramUri);
        paramzzqo.a(paramAnonymousInt1, paramAnonymousBundle, paramAnonymousInt2, paramAnonymousIntent);
      }
    };
    try
    {
      paramzzqp.a(paramzzqo, paramUri, paramBundle, true);
      return;
    }
    catch (RemoteException paramzzqp)
    {
      b(paramContext, paramUri);
      throw paramzzqp;
    }
    catch (RuntimeException paramzzqp)
    {
      b(paramContext, paramUri);
      throw paramzzqp;
    }
  }
  
  private static abstract class zza
    extends zzqq.zzc<PanoramaApi.PanoramaResult>
  {
    protected PanoramaApi.PanoramaResult a(Status paramStatus)
    {
      return new zzqs(paramStatus, null);
    }
  }
  
  private static final class zzb
    extends zzqo.zza
  {
    private final zza.zzb<PanoramaApi.PanoramaResult> a;
    
    public zzb(zza.zzb<PanoramaApi.PanoramaResult> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void a(int paramInt1, Bundle paramBundle, int paramInt2, Intent paramIntent)
    {
      if (paramBundle != null) {}
      for (paramBundle = (PendingIntent)paramBundle.getParcelable("pendingIntent");; paramBundle = null)
      {
        paramBundle = new Status(paramInt1, null, paramBundle);
        this.a.a(new zzqs(paramBundle, paramIntent));
        return;
      }
    }
  }
  
  private static abstract class zzc<R extends Result>
    extends zza.zza<R, zzqr>
  {
    protected abstract void a(Context paramContext, zzqp paramzzqp)
      throws RemoteException;
    
    protected final void a(zzqr paramzzqr)
      throws RemoteException
    {
      a(paramzzqr.q(), (zzqp)paramzzqr.v());
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzqq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */