package com.google.android.gms.internal;

import android.text.TextUtils;

@zzhb
public final class zzbm
{
  private String a;
  
  public zzbm()
  {
    this((String)zzbt.b.b());
  }
  
  public zzbm(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      paramString = (String)zzbt.b.b();
    }
    for (;;)
    {
      this.a = paramString;
      return;
    }
  }
  
  public String a()
  {
    return this.a;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzbm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */