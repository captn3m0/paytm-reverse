package com.google.android.gms.internal;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.android.gms.ads.internal.zzr;
import com.google.android.gms.common.zze;
import java.util.concurrent.Callable;

@zzhb
public class zzbs
{
  private final Object a = new Object();
  private boolean b = false;
  private SharedPreferences c = null;
  
  public <T> T a(final zzbp<T> paramzzbp)
  {
    synchronized (this.a)
    {
      if (!this.b)
      {
        paramzzbp = paramzzbp.b();
        return paramzzbp;
      }
      (T)zzjb.a(new Callable()
      {
        public T call()
        {
          return (T)paramzzbp.a(zzbs.a(zzbs.this));
        }
      });
    }
  }
  
  public void a(Context paramContext)
  {
    synchronized (this.a)
    {
      if (this.b) {
        return;
      }
      paramContext = zze.g(paramContext);
      if (paramContext == null) {
        return;
      }
    }
    this.c = zzr.l().a(paramContext);
    this.b = true;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzbs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */