package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.zze;
import java.util.Map;

@zzhb
public class zzdl
  implements zzdf
{
  static final Map<String, Integer> a = zzmr.a("resize", Integer.valueOf(1), "playVideo", Integer.valueOf(2), "storePicture", Integer.valueOf(3), "createCalendarEvent", Integer.valueOf(4), "setOrientationProperties", Integer.valueOf(5), "closeResizedAd", Integer.valueOf(6));
  private final zze b;
  private final zzfn c;
  
  public zzdl(zze paramzze, zzfn paramzzfn)
  {
    this.b = paramzze;
    this.c = paramzzfn;
  }
  
  public void a(zzjp paramzzjp, Map<String, String> paramMap)
  {
    String str = (String)paramMap.get("a");
    int i = ((Integer)a.get(str)).intValue();
    if ((i != 5) && (this.b != null) && (!this.b.b()))
    {
      this.b.a(null);
      return;
    }
    switch (i)
    {
    case 2: 
    default: 
      zzin.c("Unknown MRAID command called.");
      return;
    case 1: 
      this.c.a(paramMap);
      return;
    case 4: 
      new zzfm(paramzzjp, paramMap).a();
      return;
    case 3: 
      new zzfp(paramzzjp, paramMap).a();
      return;
    case 5: 
      new zzfo(paramzzjp, paramMap).a();
      return;
    }
    this.c.a(true);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzdl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */