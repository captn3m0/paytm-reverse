package com.google.android.gms.internal;

import java.io.IOException;

public final class zzsk
  extends zzso<zzsk>
{
  public String[] a;
  public int[] b;
  public byte[][] c;
  
  public zzsk()
  {
    a();
  }
  
  public static zzsk a(byte[] paramArrayOfByte)
    throws zzst
  {
    return (zzsk)zzsu.a(new zzsk(), paramArrayOfByte);
  }
  
  public zzsk a()
  {
    this.a = zzsx.f;
    this.b = zzsx.a;
    this.c = zzsx.g;
    this.r = null;
    this.S = -1;
    return this;
  }
  
  public zzsk a(zzsm paramzzsm)
    throws IOException
  {
    for (;;)
    {
      int i = paramzzsm.a();
      int j;
      Object localObject;
      switch (i)
      {
      default: 
        if (a(paramzzsm, i)) {}
        break;
      case 0: 
        return this;
      case 10: 
        j = zzsx.b(paramzzsm, 10);
        if (this.a == null) {}
        for (i = 0;; i = this.a.length)
        {
          localObject = new String[j + i];
          j = i;
          if (i != 0)
          {
            System.arraycopy(this.a, 0, localObject, 0, i);
            j = i;
          }
          while (j < localObject.length - 1)
          {
            localObject[j] = paramzzsm.i();
            paramzzsm.a();
            j += 1;
          }
        }
        localObject[j] = paramzzsm.i();
        this.a = ((String[])localObject);
        break;
      case 16: 
        j = zzsx.b(paramzzsm, 16);
        if (this.b == null) {}
        for (i = 0;; i = this.b.length)
        {
          localObject = new int[j + i];
          j = i;
          if (i != 0)
          {
            System.arraycopy(this.b, 0, localObject, 0, i);
            j = i;
          }
          while (j < localObject.length - 1)
          {
            localObject[j] = paramzzsm.g();
            paramzzsm.a();
            j += 1;
          }
        }
        localObject[j] = paramzzsm.g();
        this.b = ((int[])localObject);
        break;
      case 18: 
        int k = paramzzsm.d(paramzzsm.m());
        i = paramzzsm.s();
        j = 0;
        while (paramzzsm.q() > 0)
        {
          paramzzsm.g();
          j += 1;
        }
        paramzzsm.f(i);
        if (this.b == null) {}
        for (i = 0;; i = this.b.length)
        {
          localObject = new int[j + i];
          j = i;
          if (i != 0)
          {
            System.arraycopy(this.b, 0, localObject, 0, i);
            j = i;
          }
          while (j < localObject.length)
          {
            localObject[j] = paramzzsm.g();
            j += 1;
          }
        }
        this.b = ((int[])localObject);
        paramzzsm.e(k);
        break;
      case 26: 
        j = zzsx.b(paramzzsm, 26);
        if (this.c == null) {}
        for (i = 0;; i = this.c.length)
        {
          localObject = new byte[j + i][];
          j = i;
          if (i != 0)
          {
            System.arraycopy(this.c, 0, localObject, 0, i);
            j = i;
          }
          while (j < localObject.length - 1)
          {
            localObject[j] = paramzzsm.j();
            paramzzsm.a();
            j += 1;
          }
        }
        localObject[j] = paramzzsm.j();
        this.c = ((byte[][])localObject);
      }
    }
  }
  
  public void a(zzsn paramzzsn)
    throws IOException
  {
    int j = 0;
    int i;
    Object localObject;
    if ((this.a != null) && (this.a.length > 0))
    {
      i = 0;
      while (i < this.a.length)
      {
        localObject = this.a[i];
        if (localObject != null) {
          paramzzsn.a(1, (String)localObject);
        }
        i += 1;
      }
    }
    if ((this.b != null) && (this.b.length > 0))
    {
      i = 0;
      while (i < this.b.length)
      {
        paramzzsn.a(2, this.b[i]);
        i += 1;
      }
    }
    if ((this.c != null) && (this.c.length > 0))
    {
      i = j;
      while (i < this.c.length)
      {
        localObject = this.c[i];
        if (localObject != null) {
          paramzzsn.a(3, (byte[])localObject);
        }
        i += 1;
      }
    }
    super.a(paramzzsn);
  }
  
  protected int b()
  {
    int i1 = 0;
    int i2 = super.b();
    int i;
    int k;
    Object localObject;
    int n;
    int m;
    if ((this.a != null) && (this.a.length > 0))
    {
      i = 0;
      j = 0;
      for (k = 0; i < this.a.length; k = m)
      {
        localObject = this.a[i];
        n = j;
        m = k;
        if (localObject != null)
        {
          m = k + 1;
          n = j + zzsn.b((String)localObject);
        }
        i += 1;
        j = n;
      }
    }
    for (int j = i2 + j + k * 1;; j = i2)
    {
      i = j;
      if (this.b != null)
      {
        i = j;
        if (this.b.length > 0)
        {
          i = 0;
          k = 0;
          while (i < this.b.length)
          {
            k += zzsn.c(this.b[i]);
            i += 1;
          }
          i = j + k + this.b.length * 1;
        }
      }
      j = i;
      if (this.c != null)
      {
        j = i;
        if (this.c.length > 0)
        {
          k = 0;
          m = 0;
          j = i1;
          while (j < this.c.length)
          {
            localObject = this.c[j];
            i1 = k;
            n = m;
            if (localObject != null)
            {
              n = m + 1;
              i1 = k + zzsn.c((byte[])localObject);
            }
            j += 1;
            k = i1;
            m = n;
          }
          j = i + k + m * 1;
        }
      }
      return j;
    }
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1;
    if (paramObject == this) {
      bool1 = true;
    }
    do
    {
      do
      {
        do
        {
          do
          {
            do
            {
              return bool1;
              bool1 = bool2;
            } while (!(paramObject instanceof zzsk));
            paramObject = (zzsk)paramObject;
            bool1 = bool2;
          } while (!zzss.a(this.a, ((zzsk)paramObject).a));
          bool1 = bool2;
        } while (!zzss.a(this.b, ((zzsk)paramObject).b));
        bool1 = bool2;
      } while (!zzss.a(this.c, ((zzsk)paramObject).c));
      if ((this.r != null) && (!this.r.b())) {
        break label111;
      }
      if (((zzsk)paramObject).r == null) {
        break;
      }
      bool1 = bool2;
    } while (!((zzsk)paramObject).r.b());
    return true;
    label111:
    return this.r.equals(((zzsk)paramObject).r);
  }
  
  public int hashCode()
  {
    int j = getClass().getName().hashCode();
    int k = zzss.a(this.a);
    int m = zzss.a(this.b);
    int n = zzss.a(this.c);
    if ((this.r == null) || (this.r.b())) {}
    for (int i = 0;; i = this.r.hashCode()) {
      return i + ((((j + 527) * 31 + k) * 31 + m) * 31 + n) * 31;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/internal/zzsk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */