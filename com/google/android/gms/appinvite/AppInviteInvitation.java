package com.google.android.gms.appinvite;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class AppInviteInvitation
{
  private static final String[] a = { "jpg", "jpeg", "png" };
  
  public static final class IntentBuilder
  {
    @Retention(RetentionPolicy.SOURCE)
    public static @interface PlatformMode {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/appinvite/AppInviteInvitation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */