package com.google.android.gms.appinvite;

import android.content.Intent;

public class AppInviteReferral
{
  public static boolean a(Intent paramIntent)
  {
    return (paramIntent != null) && (paramIntent.getBundleExtra("com.google.android.gms.appinvite.REFERRAL_BUNDLE") != null);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/appinvite/AppInviteReferral.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */