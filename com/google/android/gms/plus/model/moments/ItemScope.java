package com.google.android.gms.plus.model.moments;

import com.google.android.gms.common.data.Freezable;
import java.util.HashSet;
import java.util.Set;

public abstract interface ItemScope
  extends Freezable<ItemScope>
{
  public static class Builder
  {
    private final Set<Integer> a = new HashSet();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/plus/model/moments/ItemScope.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */