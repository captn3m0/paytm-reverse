package com.google.android.gms.plus.model.people;

import android.os.Bundle;
import com.google.android.gms.common.data.AbstractDataBuffer;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzd;
import com.google.android.gms.plus.internal.model.people.PersonEntity;
import com.google.android.gms.plus.internal.model.people.zzk;

public final class PersonBuffer
  extends AbstractDataBuffer<Person>
{
  private final zzd<PersonEntity> b;
  
  public PersonBuffer(DataHolder paramDataHolder)
  {
    super(paramDataHolder);
    if ((paramDataHolder.f() != null) && (paramDataHolder.f().getBoolean("com.google.android.gms.plus.IsSafeParcelable", false)))
    {
      this.b = new zzd(paramDataHolder, PersonEntity.CREATOR);
      return;
    }
    this.b = null;
  }
  
  public Person b(int paramInt)
  {
    if (this.b != null) {
      return (Person)this.b.b(paramInt);
    }
    return new zzk(this.a, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/plus/model/people/PersonBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */