package com.google.android.gms.plus.model.people;

import com.google.android.gms.common.data.Freezable;

public abstract interface Person
  extends Freezable<Person>
{
  public static abstract interface AgeRange
    extends Freezable<AgeRange>
  {}
  
  public static abstract interface Cover
    extends Freezable<Cover>
  {
    public static abstract interface CoverInfo
      extends Freezable<CoverInfo>
    {}
    
    public static abstract interface CoverPhoto
      extends Freezable<CoverPhoto>
    {}
    
    public static final class Layout {}
  }
  
  public static final class Gender {}
  
  public static abstract interface Image
    extends Freezable<Image>
  {}
  
  public static abstract interface Name
    extends Freezable<Name>
  {}
  
  public static final class ObjectType {}
  
  public static abstract interface Organizations
    extends Freezable<Organizations>
  {
    public static final class Type {}
  }
  
  public static abstract interface PlacesLived
    extends Freezable<PlacesLived>
  {}
  
  public static final class RelationshipStatus {}
  
  public static abstract interface Urls
    extends Freezable<Urls>
  {
    public static final class Type {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/plus/model/people/Person.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */