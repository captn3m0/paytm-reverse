package com.google.android.gms.plus;

import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;

public abstract interface People
{
  public static abstract interface LoadPeopleResult
    extends Releasable, Result
  {}
  
  public static abstract interface OrderBy {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/plus/People.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */