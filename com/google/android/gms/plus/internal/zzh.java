package com.google.android.gms.plus.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzh
  implements Parcelable.Creator<PlusSession>
{
  static void a(PlusSession paramPlusSession, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramPlusSession.b(), false);
    zzb.a(paramParcel, 1000, paramPlusSession.a());
    zzb.a(paramParcel, 2, paramPlusSession.c(), false);
    zzb.a(paramParcel, 3, paramPlusSession.d(), false);
    zzb.a(paramParcel, 4, paramPlusSession.e(), false);
    zzb.a(paramParcel, 5, paramPlusSession.f(), false);
    zzb.a(paramParcel, 6, paramPlusSession.g(), false);
    zzb.a(paramParcel, 7, paramPlusSession.h(), false);
    zzb.a(paramParcel, 8, paramPlusSession.i(), false);
    zzb.a(paramParcel, 9, paramPlusSession.j(), paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public PlusSession a(Parcel paramParcel)
  {
    PlusCommonExtras localPlusCommonExtras = null;
    int j = zza.b(paramParcel);
    int i = 0;
    String str1 = null;
    String str2 = null;
    String str3 = null;
    String str4 = null;
    String[] arrayOfString1 = null;
    String[] arrayOfString2 = null;
    String[] arrayOfString3 = null;
    String str5 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        str5 = zza.p(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        arrayOfString3 = zza.B(paramParcel, k);
        break;
      case 3: 
        arrayOfString2 = zza.B(paramParcel, k);
        break;
      case 4: 
        arrayOfString1 = zza.B(paramParcel, k);
        break;
      case 5: 
        str4 = zza.p(paramParcel, k);
        break;
      case 6: 
        str3 = zza.p(paramParcel, k);
        break;
      case 7: 
        str2 = zza.p(paramParcel, k);
        break;
      case 8: 
        str1 = zza.p(paramParcel, k);
        break;
      case 9: 
        localPlusCommonExtras = (PlusCommonExtras)zza.a(paramParcel, k, PlusCommonExtras.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new PlusSession(i, str5, arrayOfString3, arrayOfString2, arrayOfString1, str4, str3, str2, str1, localPlusCommonExtras);
  }
  
  public PlusSession[] a(int paramInt)
  {
    return new PlusSession[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/plus/internal/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */