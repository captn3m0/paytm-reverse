package com.google.android.gms.plus.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zzb;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.common.internal.zzq;
import com.google.android.gms.common.server.response.SafeParcelResponse;
import com.google.android.gms.plus.Moments.LoadMomentsResult;
import com.google.android.gms.plus.People.LoadPeopleResult;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.internal.model.moments.MomentEntity;
import com.google.android.gms.plus.internal.model.people.PersonEntity;
import com.google.android.gms.plus.model.moments.Moment;
import com.google.android.gms.plus.model.moments.MomentBuffer;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

public class zze
  extends zzj<zzd>
{
  private Person a;
  private final PlusSession e;
  
  public zze(Context paramContext, Looper paramLooper, zzf paramzzf, PlusSession paramPlusSession, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramContext, paramLooper, 2, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    this.e = paramPlusSession;
  }
  
  public static boolean b(Set<Scope> paramSet)
  {
    if ((paramSet == null) || (paramSet.isEmpty())) {}
    while ((paramSet.size() == 1) && (paramSet.contains(new Scope("plus_one_placeholder_scope")))) {
      return false;
    }
    return true;
  }
  
  public zzq a(zza.zzb<People.LoadPeopleResult> paramzzb, int paramInt, String paramString)
  {
    u();
    paramzzb = new zze(paramzzb);
    try
    {
      paramString = ((zzd)v()).a(paramzzb, 1, paramInt, -1, paramString);
      return paramString;
    }
    catch (RemoteException paramString)
    {
      paramzzb.a(DataHolder.b(8), null);
    }
    return null;
  }
  
  public zzq a(zza.zzb<People.LoadPeopleResult> paramzzb, String paramString)
  {
    return a(paramzzb, 0, paramString);
  }
  
  protected zzd a(IBinder paramIBinder)
  {
    return zzd.zza.a(paramIBinder);
  }
  
  protected String a()
  {
    return "com.google.android.gms.plus.service.START";
  }
  
  protected void a(int paramInt1, IBinder paramIBinder, Bundle paramBundle, int paramInt2)
  {
    if ((paramInt1 == 0) && (paramBundle != null) && (paramBundle.containsKey("loaded_person"))) {
      this.a = PersonEntity.a(paramBundle.getByteArray("loaded_person"));
    }
    super.a(paramInt1, paramIBinder, paramBundle, paramInt2);
  }
  
  public void a(zza.zzb<Moments.LoadMomentsResult> paramzzb)
  {
    a(paramzzb, 20, null, null, null, "me");
  }
  
  public void a(zza.zzb<Moments.LoadMomentsResult> paramzzb, int paramInt, String paramString1, Uri paramUri, String paramString2, String paramString3)
  {
    u();
    if (paramzzb != null) {}
    for (paramzzb = new zzd(paramzzb);; paramzzb = null) {
      try
      {
        ((zzd)v()).a(paramzzb, paramInt, paramString1, paramUri, paramString2, paramString3);
        return;
      }
      catch (RemoteException paramString1)
      {
        paramzzb.a(DataHolder.b(8), null, null);
      }
    }
  }
  
  public void a(zza.zzb<Status> paramzzb, Moment paramMoment)
  {
    u();
    if (paramzzb != null) {}
    for (paramzzb = new zzc(paramzzb);; paramzzb = null) {
      try
      {
        paramMoment = SafeParcelResponse.a((MomentEntity)paramMoment);
        ((zzd)v()).a(paramzzb, paramMoment);
        return;
      }
      catch (RemoteException paramMoment)
      {
        if (paramzzb != null) {
          break;
        }
        throw new IllegalStateException(paramMoment);
        paramzzb.a(new Status(8, null, null));
      }
    }
  }
  
  public void a(zza.zzb<People.LoadPeopleResult> paramzzb, Collection<String> paramCollection)
  {
    u();
    paramzzb = new zze(paramzzb);
    try
    {
      ((zzd)v()).a(paramzzb, new ArrayList(paramCollection));
      return;
    }
    catch (RemoteException paramCollection)
    {
      paramzzb.a(DataHolder.b(8), null);
    }
  }
  
  public void a(zza.zzb<People.LoadPeopleResult> paramzzb, String[] paramArrayOfString)
  {
    a(paramzzb, Arrays.asList(paramArrayOfString));
  }
  
  public void a(String paramString)
  {
    u();
    try
    {
      ((zzd)v()).a(paramString);
      return;
    }
    catch (RemoteException paramString)
    {
      throw new IllegalStateException(paramString);
    }
  }
  
  protected Bundle a_()
  {
    Bundle localBundle = this.e.k();
    localBundle.putStringArray("request_visible_actions", this.e.d());
    localBundle.putString("auth_package", this.e.f());
    return localBundle;
  }
  
  protected String b()
  {
    return "com.google.android.gms.plus.internal.IPlusService";
  }
  
  public void b(zza.zzb<People.LoadPeopleResult> paramzzb)
  {
    u();
    paramzzb = new zze(paramzzb);
    try
    {
      ((zzd)v()).a(paramzzb, 2, 1, -1, null);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      paramzzb.a(DataHolder.b(8), null);
    }
  }
  
  public void c(zza.zzb<Status> paramzzb)
  {
    u();
    h();
    paramzzb = new zzf(paramzzb);
    try
    {
      ((zzd)v()).b(paramzzb);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      paramzzb.a(8, null);
    }
  }
  
  public void h()
  {
    u();
    try
    {
      this.a = null;
      ((zzd)v()).b();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      throw new IllegalStateException(localRemoteException);
    }
  }
  
  public boolean l()
  {
    return b(t().a(Plus.c));
  }
  
  static final class zza
    implements Moments.LoadMomentsResult
  {
    private final Status a;
    private final String b;
    private final String c;
    private final MomentBuffer d;
    
    public zza(Status paramStatus, DataHolder paramDataHolder, String paramString1, String paramString2)
    {
      this.a = paramStatus;
      this.b = paramString1;
      this.c = paramString2;
      if (paramDataHolder != null) {}
      for (paramStatus = new MomentBuffer(paramDataHolder);; paramStatus = null)
      {
        this.d = paramStatus;
        return;
      }
    }
    
    public Status getStatus()
    {
      return this.a;
    }
    
    public void release()
    {
      if (this.d != null) {
        this.d.release();
      }
    }
  }
  
  static final class zzb
    implements People.LoadPeopleResult
  {
    private final Status a;
    private final String b;
    private final PersonBuffer c;
    
    public zzb(Status paramStatus, DataHolder paramDataHolder, String paramString)
    {
      this.a = paramStatus;
      this.b = paramString;
      if (paramDataHolder != null) {}
      for (paramStatus = new PersonBuffer(paramDataHolder);; paramStatus = null)
      {
        this.c = paramStatus;
        return;
      }
    }
    
    public Status getStatus()
    {
      return this.a;
    }
    
    public void release()
    {
      if (this.c != null) {
        this.c.release();
      }
    }
  }
  
  static final class zzc
    extends zza
  {
    private final zza.zzb<Status> a;
    
    public zzc(zza.zzb<Status> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void a(Status paramStatus)
    {
      this.a.a(paramStatus);
    }
  }
  
  static final class zzd
    extends zza
  {
    private final zza.zzb<Moments.LoadMomentsResult> a;
    
    public zzd(zza.zzb<Moments.LoadMomentsResult> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void a(DataHolder paramDataHolder, String paramString1, String paramString2)
    {
      if (paramDataHolder.f() != null) {}
      for (Object localObject = (PendingIntent)paramDataHolder.f().getParcelable("pendingIntent");; localObject = null)
      {
        Status localStatus = new Status(paramDataHolder.e(), null, (PendingIntent)localObject);
        localObject = paramDataHolder;
        if (!localStatus.d())
        {
          localObject = paramDataHolder;
          if (paramDataHolder != null)
          {
            if (!paramDataHolder.h()) {
              paramDataHolder.i();
            }
            localObject = null;
          }
        }
        this.a.a(new zze.zza(localStatus, (DataHolder)localObject, paramString1, paramString2));
        return;
      }
    }
  }
  
  static final class zze
    extends zza
  {
    private final zza.zzb<People.LoadPeopleResult> a;
    
    public zze(zza.zzb<People.LoadPeopleResult> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void a(DataHolder paramDataHolder, String paramString)
    {
      if (paramDataHolder.f() != null) {}
      for (Object localObject = (PendingIntent)paramDataHolder.f().getParcelable("pendingIntent");; localObject = null)
      {
        Status localStatus = new Status(paramDataHolder.e(), null, (PendingIntent)localObject);
        localObject = paramDataHolder;
        if (!localStatus.d())
        {
          localObject = paramDataHolder;
          if (paramDataHolder != null)
          {
            if (!paramDataHolder.h()) {
              paramDataHolder.i();
            }
            localObject = null;
          }
        }
        this.a.a(new zze.zzb(localStatus, (DataHolder)localObject, paramString));
        return;
      }
    }
  }
  
  static final class zzf
    extends zza
  {
    private final zza.zzb<Status> a;
    
    public zzf(zza.zzb<Status> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void a(int paramInt, Bundle paramBundle)
    {
      if (paramBundle != null) {}
      for (paramBundle = (PendingIntent)paramBundle.getParcelable("pendingIntent");; paramBundle = null)
      {
        paramBundle = new Status(paramInt, null, paramBundle);
        this.a.a(paramBundle);
        return;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/plus/internal/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */