package com.google.android.gms.plus.internal.model.moments;

import android.os.Parcel;
import com.google.android.gms.common.server.response.FastJsonResponse.Field;
import com.google.android.gms.common.server.response.FastSafeParcelableJsonResponse;
import com.google.android.gms.plus.model.moments.Moment;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public final class MomentEntity
  extends FastSafeParcelableJsonResponse
  implements Moment
{
  public static final zzb CREATOR = new zzb();
  private static final HashMap<String, FastJsonResponse.Field<?, ?>> h = new HashMap();
  final Set<Integer> a;
  final int b;
  String c;
  ItemScopeEntity d;
  String e;
  ItemScopeEntity f;
  String g;
  
  static
  {
    h.put("id", FastJsonResponse.Field.d("id", 2));
    h.put("result", FastJsonResponse.Field.a("result", 4, ItemScopeEntity.class));
    h.put("startDate", FastJsonResponse.Field.d("startDate", 5));
    h.put("target", FastJsonResponse.Field.a("target", 6, ItemScopeEntity.class));
    h.put("type", FastJsonResponse.Field.d("type", 7));
  }
  
  public MomentEntity()
  {
    this.b = 1;
    this.a = new HashSet();
  }
  
  MomentEntity(Set<Integer> paramSet, int paramInt, String paramString1, ItemScopeEntity paramItemScopeEntity1, String paramString2, ItemScopeEntity paramItemScopeEntity2, String paramString3)
  {
    this.a = paramSet;
    this.b = paramInt;
    this.c = paramString1;
    this.d = paramItemScopeEntity1;
    this.e = paramString2;
    this.f = paramItemScopeEntity2;
    this.g = paramString3;
  }
  
  protected boolean a(FastJsonResponse.Field paramField)
  {
    return this.a.contains(Integer.valueOf(paramField.h()));
  }
  
  protected Object b(FastJsonResponse.Field paramField)
  {
    switch (paramField.h())
    {
    case 3: 
    default: 
      throw new IllegalStateException("Unknown safe parcelable id=" + paramField.h());
    case 2: 
      return this.c;
    case 4: 
      return this.d;
    case 5: 
      return this.e;
    case 6: 
      return this.f;
    }
    return this.g;
  }
  
  public int describeContents()
  {
    zzb localzzb = CREATOR;
    return 0;
  }
  
  public HashMap<String, FastJsonResponse.Field<?, ?>> e()
  {
    return h;
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof MomentEntity)) {
      return false;
    }
    if (this == paramObject) {
      return true;
    }
    paramObject = (MomentEntity)paramObject;
    Iterator localIterator = h.values().iterator();
    while (localIterator.hasNext())
    {
      FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
      if (a(localField))
      {
        if (((MomentEntity)paramObject).a(localField))
        {
          if (!b(localField).equals(((MomentEntity)paramObject).b(localField))) {
            return false;
          }
        }
        else {
          return false;
        }
      }
      else if (((MomentEntity)paramObject).a(localField)) {
        return false;
      }
    }
    return true;
  }
  
  public MomentEntity f()
  {
    return this;
  }
  
  public int hashCode()
  {
    Iterator localIterator = h.values().iterator();
    int i = 0;
    if (localIterator.hasNext())
    {
      FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
      if (!a(localField)) {
        break label68;
      }
      int j = localField.h();
      i = b(localField).hashCode() + (i + j);
    }
    label68:
    for (;;)
    {
      break;
      return i;
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb localzzb = CREATOR;
    zzb.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/plus/internal/model/moments/MomentEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */