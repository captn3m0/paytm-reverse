package com.google.android.gms.plus.internal.model.moments;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import java.util.HashSet;
import java.util.Set;

public class zzb
  implements Parcelable.Creator<MomentEntity>
{
  static void a(MomentEntity paramMomentEntity, Parcel paramParcel, int paramInt)
  {
    int i = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    Set localSet = paramMomentEntity.a;
    if (localSet.contains(Integer.valueOf(1))) {
      com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramMomentEntity.b);
    }
    if (localSet.contains(Integer.valueOf(2))) {
      com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramMomentEntity.c, true);
    }
    if (localSet.contains(Integer.valueOf(4))) {
      com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 4, paramMomentEntity.d, paramInt, true);
    }
    if (localSet.contains(Integer.valueOf(5))) {
      com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 5, paramMomentEntity.e, true);
    }
    if (localSet.contains(Integer.valueOf(6))) {
      com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 6, paramMomentEntity.f, paramInt, true);
    }
    if (localSet.contains(Integer.valueOf(7))) {
      com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 7, paramMomentEntity.g, true);
    }
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, i);
  }
  
  public MomentEntity a(Parcel paramParcel)
  {
    String str1 = null;
    int j = zza.b(paramParcel);
    HashSet localHashSet = new HashSet();
    int i = 0;
    ItemScopeEntity localItemScopeEntity1 = null;
    String str2 = null;
    ItemScopeEntity localItemScopeEntity2 = null;
    String str3 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      case 3: 
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        localHashSet.add(Integer.valueOf(1));
        break;
      case 2: 
        str3 = zza.p(paramParcel, k);
        localHashSet.add(Integer.valueOf(2));
        break;
      case 4: 
        localItemScopeEntity2 = (ItemScopeEntity)zza.a(paramParcel, k, ItemScopeEntity.CREATOR);
        localHashSet.add(Integer.valueOf(4));
        break;
      case 5: 
        str2 = zza.p(paramParcel, k);
        localHashSet.add(Integer.valueOf(5));
        break;
      case 6: 
        localItemScopeEntity1 = (ItemScopeEntity)zza.a(paramParcel, k, ItemScopeEntity.CREATOR);
        localHashSet.add(Integer.valueOf(6));
        break;
      case 7: 
        str1 = zza.p(paramParcel, k);
        localHashSet.add(Integer.valueOf(7));
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new MomentEntity(localHashSet, i, str3, localItemScopeEntity2, str2, localItemScopeEntity1, str1);
  }
  
  public MomentEntity[] a(int paramInt)
  {
    return new MomentEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/plus/internal/model/moments/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */