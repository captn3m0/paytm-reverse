package com.google.android.gms.plus.internal.model.moments;

import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.plus.model.moments.Moment;

public final class zzc
  extends com.google.android.gms.common.data.zzc
  implements Moment
{
  private MomentEntity c;
  
  public zzc(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
  }
  
  private MomentEntity d()
  {
    try
    {
      if (this.c == null)
      {
        byte[] arrayOfByte = g("momentImpl");
        Parcel localParcel = Parcel.obtain();
        localParcel.unmarshall(arrayOfByte, 0, arrayOfByte.length);
        localParcel.setDataPosition(0);
        this.c = MomentEntity.CREATOR.a(localParcel);
        localParcel.recycle();
      }
      return this.c;
    }
    finally {}
  }
  
  public MomentEntity c()
  {
    return d();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/plus/internal/model/moments/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */