package com.google.android.gms.plus.internal.model.moments;

import android.os.Parcel;
import com.google.android.gms.common.server.response.FastJsonResponse.Field;
import com.google.android.gms.common.server.response.FastSafeParcelableJsonResponse;
import com.google.android.gms.plus.model.moments.ItemScope;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class ItemScopeEntity
  extends FastSafeParcelableJsonResponse
  implements ItemScope
{
  public static final zza CREATOR = new zza();
  private static final HashMap<String, FastJsonResponse.Field<?, ?>> ae = new HashMap();
  String A;
  String B;
  String C;
  ItemScopeEntity D;
  String E;
  String F;
  String G;
  String H;
  ItemScopeEntity I;
  double J;
  ItemScopeEntity K;
  double L;
  String M;
  ItemScopeEntity N;
  List<ItemScopeEntity> O;
  String P;
  String Q;
  String R;
  String S;
  ItemScopeEntity T;
  String U;
  String V;
  String W;
  ItemScopeEntity X;
  String Y;
  String Z;
  final Set<Integer> a;
  String aa;
  String ab;
  String ac;
  String ad;
  final int b;
  ItemScopeEntity c;
  List<String> d;
  ItemScopeEntity e;
  String f;
  String g;
  String h;
  List<ItemScopeEntity> i;
  int j;
  List<ItemScopeEntity> k;
  ItemScopeEntity l;
  List<ItemScopeEntity> m;
  String n;
  String o;
  ItemScopeEntity p;
  String q;
  String r;
  String s;
  List<ItemScopeEntity> t;
  String u;
  String v;
  String w;
  String x;
  String y;
  String z;
  
  static
  {
    ae.put("about", FastJsonResponse.Field.a("about", 2, ItemScopeEntity.class));
    ae.put("additionalName", FastJsonResponse.Field.e("additionalName", 3));
    ae.put("address", FastJsonResponse.Field.a("address", 4, ItemScopeEntity.class));
    ae.put("addressCountry", FastJsonResponse.Field.d("addressCountry", 5));
    ae.put("addressLocality", FastJsonResponse.Field.d("addressLocality", 6));
    ae.put("addressRegion", FastJsonResponse.Field.d("addressRegion", 7));
    ae.put("associated_media", FastJsonResponse.Field.b("associated_media", 8, ItemScopeEntity.class));
    ae.put("attendeeCount", FastJsonResponse.Field.a("attendeeCount", 9));
    ae.put("attendees", FastJsonResponse.Field.b("attendees", 10, ItemScopeEntity.class));
    ae.put("audio", FastJsonResponse.Field.a("audio", 11, ItemScopeEntity.class));
    ae.put("author", FastJsonResponse.Field.b("author", 12, ItemScopeEntity.class));
    ae.put("bestRating", FastJsonResponse.Field.d("bestRating", 13));
    ae.put("birthDate", FastJsonResponse.Field.d("birthDate", 14));
    ae.put("byArtist", FastJsonResponse.Field.a("byArtist", 15, ItemScopeEntity.class));
    ae.put("caption", FastJsonResponse.Field.d("caption", 16));
    ae.put("contentSize", FastJsonResponse.Field.d("contentSize", 17));
    ae.put("contentUrl", FastJsonResponse.Field.d("contentUrl", 18));
    ae.put("contributor", FastJsonResponse.Field.b("contributor", 19, ItemScopeEntity.class));
    ae.put("dateCreated", FastJsonResponse.Field.d("dateCreated", 20));
    ae.put("dateModified", FastJsonResponse.Field.d("dateModified", 21));
    ae.put("datePublished", FastJsonResponse.Field.d("datePublished", 22));
    ae.put("description", FastJsonResponse.Field.d("description", 23));
    ae.put("duration", FastJsonResponse.Field.d("duration", 24));
    ae.put("embedUrl", FastJsonResponse.Field.d("embedUrl", 25));
    ae.put("endDate", FastJsonResponse.Field.d("endDate", 26));
    ae.put("familyName", FastJsonResponse.Field.d("familyName", 27));
    ae.put("gender", FastJsonResponse.Field.d("gender", 28));
    ae.put("geo", FastJsonResponse.Field.a("geo", 29, ItemScopeEntity.class));
    ae.put("givenName", FastJsonResponse.Field.d("givenName", 30));
    ae.put("height", FastJsonResponse.Field.d("height", 31));
    ae.put("id", FastJsonResponse.Field.d("id", 32));
    ae.put("image", FastJsonResponse.Field.d("image", 33));
    ae.put("inAlbum", FastJsonResponse.Field.a("inAlbum", 34, ItemScopeEntity.class));
    ae.put("latitude", FastJsonResponse.Field.b("latitude", 36));
    ae.put("location", FastJsonResponse.Field.a("location", 37, ItemScopeEntity.class));
    ae.put("longitude", FastJsonResponse.Field.b("longitude", 38));
    ae.put("name", FastJsonResponse.Field.d("name", 39));
    ae.put("partOfTVSeries", FastJsonResponse.Field.a("partOfTVSeries", 40, ItemScopeEntity.class));
    ae.put("performers", FastJsonResponse.Field.b("performers", 41, ItemScopeEntity.class));
    ae.put("playerType", FastJsonResponse.Field.d("playerType", 42));
    ae.put("postOfficeBoxNumber", FastJsonResponse.Field.d("postOfficeBoxNumber", 43));
    ae.put("postalCode", FastJsonResponse.Field.d("postalCode", 44));
    ae.put("ratingValue", FastJsonResponse.Field.d("ratingValue", 45));
    ae.put("reviewRating", FastJsonResponse.Field.a("reviewRating", 46, ItemScopeEntity.class));
    ae.put("startDate", FastJsonResponse.Field.d("startDate", 47));
    ae.put("streetAddress", FastJsonResponse.Field.d("streetAddress", 48));
    ae.put("text", FastJsonResponse.Field.d("text", 49));
    ae.put("thumbnail", FastJsonResponse.Field.a("thumbnail", 50, ItemScopeEntity.class));
    ae.put("thumbnailUrl", FastJsonResponse.Field.d("thumbnailUrl", 51));
    ae.put("tickerSymbol", FastJsonResponse.Field.d("tickerSymbol", 52));
    ae.put("type", FastJsonResponse.Field.d("type", 53));
    ae.put("url", FastJsonResponse.Field.d("url", 54));
    ae.put("width", FastJsonResponse.Field.d("width", 55));
    ae.put("worstRating", FastJsonResponse.Field.d("worstRating", 56));
  }
  
  public ItemScopeEntity()
  {
    this.b = 1;
    this.a = new HashSet();
  }
  
  ItemScopeEntity(Set<Integer> paramSet, int paramInt1, ItemScopeEntity paramItemScopeEntity1, List<String> paramList, ItemScopeEntity paramItemScopeEntity2, String paramString1, String paramString2, String paramString3, List<ItemScopeEntity> paramList1, int paramInt2, List<ItemScopeEntity> paramList2, ItemScopeEntity paramItemScopeEntity3, List<ItemScopeEntity> paramList3, String paramString4, String paramString5, ItemScopeEntity paramItemScopeEntity4, String paramString6, String paramString7, String paramString8, List<ItemScopeEntity> paramList4, String paramString9, String paramString10, String paramString11, String paramString12, String paramString13, String paramString14, String paramString15, String paramString16, String paramString17, ItemScopeEntity paramItemScopeEntity5, String paramString18, String paramString19, String paramString20, String paramString21, ItemScopeEntity paramItemScopeEntity6, double paramDouble1, ItemScopeEntity paramItemScopeEntity7, double paramDouble2, String paramString22, ItemScopeEntity paramItemScopeEntity8, List<ItemScopeEntity> paramList5, String paramString23, String paramString24, String paramString25, String paramString26, ItemScopeEntity paramItemScopeEntity9, String paramString27, String paramString28, String paramString29, ItemScopeEntity paramItemScopeEntity10, String paramString30, String paramString31, String paramString32, String paramString33, String paramString34, String paramString35)
  {
    this.a = paramSet;
    this.b = paramInt1;
    this.c = paramItemScopeEntity1;
    this.d = paramList;
    this.e = paramItemScopeEntity2;
    this.f = paramString1;
    this.g = paramString2;
    this.h = paramString3;
    this.i = paramList1;
    this.j = paramInt2;
    this.k = paramList2;
    this.l = paramItemScopeEntity3;
    this.m = paramList3;
    this.n = paramString4;
    this.o = paramString5;
    this.p = paramItemScopeEntity4;
    this.q = paramString6;
    this.r = paramString7;
    this.s = paramString8;
    this.t = paramList4;
    this.u = paramString9;
    this.v = paramString10;
    this.w = paramString11;
    this.x = paramString12;
    this.y = paramString13;
    this.z = paramString14;
    this.A = paramString15;
    this.B = paramString16;
    this.C = paramString17;
    this.D = paramItemScopeEntity5;
    this.E = paramString18;
    this.F = paramString19;
    this.G = paramString20;
    this.H = paramString21;
    this.I = paramItemScopeEntity6;
    this.J = paramDouble1;
    this.K = paramItemScopeEntity7;
    this.L = paramDouble2;
    this.M = paramString22;
    this.N = paramItemScopeEntity8;
    this.O = paramList5;
    this.P = paramString23;
    this.Q = paramString24;
    this.R = paramString25;
    this.S = paramString26;
    this.T = paramItemScopeEntity9;
    this.U = paramString27;
    this.V = paramString28;
    this.W = paramString29;
    this.X = paramItemScopeEntity10;
    this.Y = paramString30;
    this.Z = paramString31;
    this.aa = paramString32;
    this.ab = paramString33;
    this.ac = paramString34;
    this.ad = paramString35;
  }
  
  protected boolean a(FastJsonResponse.Field paramField)
  {
    return this.a.contains(Integer.valueOf(paramField.h()));
  }
  
  protected Object b(FastJsonResponse.Field paramField)
  {
    switch (paramField.h())
    {
    case 35: 
    default: 
      throw new IllegalStateException("Unknown safe parcelable id=" + paramField.h());
    case 2: 
      return this.c;
    case 3: 
      return this.d;
    case 4: 
      return this.e;
    case 5: 
      return this.f;
    case 6: 
      return this.g;
    case 7: 
      return this.h;
    case 8: 
      return this.i;
    case 9: 
      return Integer.valueOf(this.j);
    case 10: 
      return this.k;
    case 11: 
      return this.l;
    case 12: 
      return this.m;
    case 13: 
      return this.n;
    case 14: 
      return this.o;
    case 15: 
      return this.p;
    case 16: 
      return this.q;
    case 17: 
      return this.r;
    case 18: 
      return this.s;
    case 19: 
      return this.t;
    case 20: 
      return this.u;
    case 21: 
      return this.v;
    case 22: 
      return this.w;
    case 23: 
      return this.x;
    case 24: 
      return this.y;
    case 25: 
      return this.z;
    case 26: 
      return this.A;
    case 27: 
      return this.B;
    case 28: 
      return this.C;
    case 29: 
      return this.D;
    case 30: 
      return this.E;
    case 31: 
      return this.F;
    case 32: 
      return this.G;
    case 33: 
      return this.H;
    case 34: 
      return this.I;
    case 36: 
      return Double.valueOf(this.J);
    case 37: 
      return this.K;
    case 38: 
      return Double.valueOf(this.L);
    case 39: 
      return this.M;
    case 40: 
      return this.N;
    case 41: 
      return this.O;
    case 42: 
      return this.P;
    case 43: 
      return this.Q;
    case 44: 
      return this.R;
    case 45: 
      return this.S;
    case 46: 
      return this.T;
    case 47: 
      return this.U;
    case 48: 
      return this.V;
    case 49: 
      return this.W;
    case 50: 
      return this.X;
    case 51: 
      return this.Y;
    case 52: 
      return this.Z;
    case 53: 
      return this.aa;
    case 54: 
      return this.ab;
    case 55: 
      return this.ac;
    }
    return this.ad;
  }
  
  public int describeContents()
  {
    zza localzza = CREATOR;
    return 0;
  }
  
  public HashMap<String, FastJsonResponse.Field<?, ?>> e()
  {
    return ae;
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof ItemScopeEntity)) {
      return false;
    }
    if (this == paramObject) {
      return true;
    }
    paramObject = (ItemScopeEntity)paramObject;
    Iterator localIterator = ae.values().iterator();
    while (localIterator.hasNext())
    {
      FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
      if (a(localField))
      {
        if (((ItemScopeEntity)paramObject).a(localField))
        {
          if (!b(localField).equals(((ItemScopeEntity)paramObject).b(localField))) {
            return false;
          }
        }
        else {
          return false;
        }
      }
      else if (((ItemScopeEntity)paramObject).a(localField)) {
        return false;
      }
    }
    return true;
  }
  
  public ItemScopeEntity f()
  {
    return this;
  }
  
  public int hashCode()
  {
    Iterator localIterator = ae.values().iterator();
    int i1 = 0;
    if (localIterator.hasNext())
    {
      FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
      if (!a(localField)) {
        break label68;
      }
      int i2 = localField.h();
      i1 = b(localField).hashCode() + (i1 + i2);
    }
    label68:
    for (;;)
    {
      break;
      return i1;
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza localzza = CREATOR;
    zza.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/plus/internal/model/moments/ItemScopeEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */