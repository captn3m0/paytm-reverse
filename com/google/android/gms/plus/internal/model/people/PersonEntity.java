package com.google.android.gms.plus.internal.model.people;

import android.os.Parcel;
import com.google.android.gms.common.server.converter.StringToIntConverter;
import com.google.android.gms.common.server.response.FastJsonResponse.Field;
import com.google.android.gms.common.server.response.FastSafeParcelableJsonResponse;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.Person.AgeRange;
import com.google.android.gms.plus.model.people.Person.Cover;
import com.google.android.gms.plus.model.people.Person.Cover.CoverInfo;
import com.google.android.gms.plus.model.people.Person.Cover.CoverPhoto;
import com.google.android.gms.plus.model.people.Person.Image;
import com.google.android.gms.plus.model.people.Person.Name;
import com.google.android.gms.plus.model.people.Person.Organizations;
import com.google.android.gms.plus.model.people.Person.PlacesLived;
import com.google.android.gms.plus.model.people.Person.Urls;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class PersonEntity
  extends FastSafeParcelableJsonResponse
  implements Person
{
  private static final HashMap<String, FastJsonResponse.Field<?, ?>> A;
  public static final zza CREATOR = new zza();
  final Set<Integer> a;
  final int b;
  String c;
  AgeRangeEntity d;
  String e;
  String f;
  int g;
  CoverEntity h;
  String i;
  String j;
  int k;
  String l;
  ImageEntity m;
  boolean n;
  String o;
  NameEntity p;
  String q;
  int r;
  List<OrganizationsEntity> s;
  List<PlacesLivedEntity> t;
  int u;
  int v;
  String w;
  String x;
  List<UrlsEntity> y;
  boolean z;
  
  static
  {
    A = new HashMap();
    A.put("aboutMe", FastJsonResponse.Field.d("aboutMe", 2));
    A.put("ageRange", FastJsonResponse.Field.a("ageRange", 3, AgeRangeEntity.class));
    A.put("birthday", FastJsonResponse.Field.d("birthday", 4));
    A.put("braggingRights", FastJsonResponse.Field.d("braggingRights", 5));
    A.put("circledByCount", FastJsonResponse.Field.a("circledByCount", 6));
    A.put("cover", FastJsonResponse.Field.a("cover", 7, CoverEntity.class));
    A.put("currentLocation", FastJsonResponse.Field.d("currentLocation", 8));
    A.put("displayName", FastJsonResponse.Field.d("displayName", 9));
    A.put("gender", FastJsonResponse.Field.a("gender", 12, new StringToIntConverter().a("male", 0).a("female", 1).a("other", 2), false));
    A.put("id", FastJsonResponse.Field.d("id", 14));
    A.put("image", FastJsonResponse.Field.a("image", 15, ImageEntity.class));
    A.put("isPlusUser", FastJsonResponse.Field.c("isPlusUser", 16));
    A.put("language", FastJsonResponse.Field.d("language", 18));
    A.put("name", FastJsonResponse.Field.a("name", 19, NameEntity.class));
    A.put("nickname", FastJsonResponse.Field.d("nickname", 20));
    A.put("objectType", FastJsonResponse.Field.a("objectType", 21, new StringToIntConverter().a("person", 0).a("page", 1), false));
    A.put("organizations", FastJsonResponse.Field.b("organizations", 22, OrganizationsEntity.class));
    A.put("placesLived", FastJsonResponse.Field.b("placesLived", 23, PlacesLivedEntity.class));
    A.put("plusOneCount", FastJsonResponse.Field.a("plusOneCount", 24));
    A.put("relationshipStatus", FastJsonResponse.Field.a("relationshipStatus", 25, new StringToIntConverter().a("single", 0).a("in_a_relationship", 1).a("engaged", 2).a("married", 3).a("its_complicated", 4).a("open_relationship", 5).a("widowed", 6).a("in_domestic_partnership", 7).a("in_civil_union", 8), false));
    A.put("tagline", FastJsonResponse.Field.d("tagline", 26));
    A.put("url", FastJsonResponse.Field.d("url", 27));
    A.put("urls", FastJsonResponse.Field.b("urls", 28, UrlsEntity.class));
    A.put("verified", FastJsonResponse.Field.c("verified", 29));
  }
  
  public PersonEntity()
  {
    this.b = 1;
    this.a = new HashSet();
  }
  
  public PersonEntity(String paramString1, String paramString2, ImageEntity paramImageEntity, int paramInt, String paramString3)
  {
    this.b = 1;
    this.a = new HashSet();
    this.j = paramString1;
    this.a.add(Integer.valueOf(9));
    this.l = paramString2;
    this.a.add(Integer.valueOf(14));
    this.m = paramImageEntity;
    this.a.add(Integer.valueOf(15));
    this.r = paramInt;
    this.a.add(Integer.valueOf(21));
    this.x = paramString3;
    this.a.add(Integer.valueOf(27));
  }
  
  PersonEntity(Set<Integer> paramSet, int paramInt1, String paramString1, AgeRangeEntity paramAgeRangeEntity, String paramString2, String paramString3, int paramInt2, CoverEntity paramCoverEntity, String paramString4, String paramString5, int paramInt3, String paramString6, ImageEntity paramImageEntity, boolean paramBoolean1, String paramString7, NameEntity paramNameEntity, String paramString8, int paramInt4, List<OrganizationsEntity> paramList, List<PlacesLivedEntity> paramList1, int paramInt5, int paramInt6, String paramString9, String paramString10, List<UrlsEntity> paramList2, boolean paramBoolean2)
  {
    this.a = paramSet;
    this.b = paramInt1;
    this.c = paramString1;
    this.d = paramAgeRangeEntity;
    this.e = paramString2;
    this.f = paramString3;
    this.g = paramInt2;
    this.h = paramCoverEntity;
    this.i = paramString4;
    this.j = paramString5;
    this.k = paramInt3;
    this.l = paramString6;
    this.m = paramImageEntity;
    this.n = paramBoolean1;
    this.o = paramString7;
    this.p = paramNameEntity;
    this.q = paramString8;
    this.r = paramInt4;
    this.s = paramList;
    this.t = paramList1;
    this.u = paramInt5;
    this.v = paramInt6;
    this.w = paramString9;
    this.x = paramString10;
    this.y = paramList2;
    this.z = paramBoolean2;
  }
  
  public static PersonEntity a(byte[] paramArrayOfByte)
  {
    Parcel localParcel = Parcel.obtain();
    localParcel.unmarshall(paramArrayOfByte, 0, paramArrayOfByte.length);
    localParcel.setDataPosition(0);
    paramArrayOfByte = CREATOR.a(localParcel);
    localParcel.recycle();
    return paramArrayOfByte;
  }
  
  protected boolean a(FastJsonResponse.Field paramField)
  {
    return this.a.contains(Integer.valueOf(paramField.h()));
  }
  
  protected Object b(FastJsonResponse.Field paramField)
  {
    switch (paramField.h())
    {
    case 10: 
    case 11: 
    case 13: 
    case 17: 
    default: 
      throw new IllegalStateException("Unknown safe parcelable id=" + paramField.h());
    case 2: 
      return this.c;
    case 3: 
      return this.d;
    case 4: 
      return this.e;
    case 5: 
      return this.f;
    case 6: 
      return Integer.valueOf(this.g);
    case 7: 
      return this.h;
    case 8: 
      return this.i;
    case 9: 
      return this.j;
    case 12: 
      return Integer.valueOf(this.k);
    case 14: 
      return this.l;
    case 15: 
      return this.m;
    case 16: 
      return Boolean.valueOf(this.n);
    case 18: 
      return this.o;
    case 19: 
      return this.p;
    case 20: 
      return this.q;
    case 21: 
      return Integer.valueOf(this.r);
    case 22: 
      return this.s;
    case 23: 
      return this.t;
    case 24: 
      return Integer.valueOf(this.u);
    case 25: 
      return Integer.valueOf(this.v);
    case 26: 
      return this.w;
    case 27: 
      return this.x;
    case 28: 
      return this.y;
    }
    return Boolean.valueOf(this.z);
  }
  
  public int describeContents()
  {
    zza localzza = CREATOR;
    return 0;
  }
  
  public HashMap<String, FastJsonResponse.Field<?, ?>> e()
  {
    return A;
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof PersonEntity)) {
      return false;
    }
    if (this == paramObject) {
      return true;
    }
    paramObject = (PersonEntity)paramObject;
    Iterator localIterator = A.values().iterator();
    while (localIterator.hasNext())
    {
      FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
      if (a(localField))
      {
        if (((PersonEntity)paramObject).a(localField))
        {
          if (!b(localField).equals(((PersonEntity)paramObject).b(localField))) {
            return false;
          }
        }
        else {
          return false;
        }
      }
      else if (((PersonEntity)paramObject).a(localField)) {
        return false;
      }
    }
    return true;
  }
  
  public PersonEntity f()
  {
    return this;
  }
  
  public int hashCode()
  {
    Iterator localIterator = A.values().iterator();
    int i1 = 0;
    if (localIterator.hasNext())
    {
      FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
      if (!a(localField)) {
        break label68;
      }
      int i2 = localField.h();
      i1 = b(localField).hashCode() + (i1 + i2);
    }
    label68:
    for (;;)
    {
      break;
      return i1;
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza localzza = CREATOR;
    zza.a(this, paramParcel, paramInt);
  }
  
  public static final class AgeRangeEntity
    extends FastSafeParcelableJsonResponse
    implements Person.AgeRange
  {
    public static final zzb CREATOR = new zzb();
    private static final HashMap<String, FastJsonResponse.Field<?, ?>> e = new HashMap();
    final Set<Integer> a;
    final int b;
    int c;
    int d;
    
    static
    {
      e.put("max", FastJsonResponse.Field.a("max", 2));
      e.put("min", FastJsonResponse.Field.a("min", 3));
    }
    
    public AgeRangeEntity()
    {
      this.b = 1;
      this.a = new HashSet();
    }
    
    AgeRangeEntity(Set<Integer> paramSet, int paramInt1, int paramInt2, int paramInt3)
    {
      this.a = paramSet;
      this.b = paramInt1;
      this.c = paramInt2;
      this.d = paramInt3;
    }
    
    protected boolean a(FastJsonResponse.Field paramField)
    {
      return this.a.contains(Integer.valueOf(paramField.h()));
    }
    
    protected Object b(FastJsonResponse.Field paramField)
    {
      switch (paramField.h())
      {
      default: 
        throw new IllegalStateException("Unknown safe parcelable id=" + paramField.h());
      case 2: 
        return Integer.valueOf(this.c);
      }
      return Integer.valueOf(this.d);
    }
    
    public int describeContents()
    {
      zzb localzzb = CREATOR;
      return 0;
    }
    
    public HashMap<String, FastJsonResponse.Field<?, ?>> e()
    {
      return e;
    }
    
    public boolean equals(Object paramObject)
    {
      if (!(paramObject instanceof AgeRangeEntity)) {
        return false;
      }
      if (this == paramObject) {
        return true;
      }
      paramObject = (AgeRangeEntity)paramObject;
      Iterator localIterator = e.values().iterator();
      while (localIterator.hasNext())
      {
        FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
        if (a(localField))
        {
          if (((AgeRangeEntity)paramObject).a(localField))
          {
            if (!b(localField).equals(((AgeRangeEntity)paramObject).b(localField))) {
              return false;
            }
          }
          else {
            return false;
          }
        }
        else if (((AgeRangeEntity)paramObject).a(localField)) {
          return false;
        }
      }
      return true;
    }
    
    public AgeRangeEntity f()
    {
      return this;
    }
    
    public int hashCode()
    {
      Iterator localIterator = e.values().iterator();
      int i = 0;
      if (localIterator.hasNext())
      {
        FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
        if (!a(localField)) {
          break label68;
        }
        int j = localField.h();
        i = b(localField).hashCode() + (i + j);
      }
      label68:
      for (;;)
      {
        break;
        return i;
      }
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      zzb localzzb = CREATOR;
      zzb.a(this, paramParcel, paramInt);
    }
  }
  
  public static final class CoverEntity
    extends FastSafeParcelableJsonResponse
    implements Person.Cover
  {
    public static final zzc CREATOR = new zzc();
    private static final HashMap<String, FastJsonResponse.Field<?, ?>> f = new HashMap();
    final Set<Integer> a;
    final int b;
    CoverInfoEntity c;
    CoverPhotoEntity d;
    int e;
    
    static
    {
      f.put("coverInfo", FastJsonResponse.Field.a("coverInfo", 2, CoverInfoEntity.class));
      f.put("coverPhoto", FastJsonResponse.Field.a("coverPhoto", 3, CoverPhotoEntity.class));
      f.put("layout", FastJsonResponse.Field.a("layout", 4, new StringToIntConverter().a("banner", 0), false));
    }
    
    public CoverEntity()
    {
      this.b = 1;
      this.a = new HashSet();
    }
    
    CoverEntity(Set<Integer> paramSet, int paramInt1, CoverInfoEntity paramCoverInfoEntity, CoverPhotoEntity paramCoverPhotoEntity, int paramInt2)
    {
      this.a = paramSet;
      this.b = paramInt1;
      this.c = paramCoverInfoEntity;
      this.d = paramCoverPhotoEntity;
      this.e = paramInt2;
    }
    
    protected boolean a(FastJsonResponse.Field paramField)
    {
      return this.a.contains(Integer.valueOf(paramField.h()));
    }
    
    protected Object b(FastJsonResponse.Field paramField)
    {
      switch (paramField.h())
      {
      default: 
        throw new IllegalStateException("Unknown safe parcelable id=" + paramField.h());
      case 2: 
        return this.c;
      case 3: 
        return this.d;
      }
      return Integer.valueOf(this.e);
    }
    
    public int describeContents()
    {
      zzc localzzc = CREATOR;
      return 0;
    }
    
    public HashMap<String, FastJsonResponse.Field<?, ?>> e()
    {
      return f;
    }
    
    public boolean equals(Object paramObject)
    {
      if (!(paramObject instanceof CoverEntity)) {
        return false;
      }
      if (this == paramObject) {
        return true;
      }
      paramObject = (CoverEntity)paramObject;
      Iterator localIterator = f.values().iterator();
      while (localIterator.hasNext())
      {
        FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
        if (a(localField))
        {
          if (((CoverEntity)paramObject).a(localField))
          {
            if (!b(localField).equals(((CoverEntity)paramObject).b(localField))) {
              return false;
            }
          }
          else {
            return false;
          }
        }
        else if (((CoverEntity)paramObject).a(localField)) {
          return false;
        }
      }
      return true;
    }
    
    public CoverEntity f()
    {
      return this;
    }
    
    public int hashCode()
    {
      Iterator localIterator = f.values().iterator();
      int i = 0;
      if (localIterator.hasNext())
      {
        FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
        if (!a(localField)) {
          break label68;
        }
        int j = localField.h();
        i = b(localField).hashCode() + (i + j);
      }
      label68:
      for (;;)
      {
        break;
        return i;
      }
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      zzc localzzc = CREATOR;
      zzc.a(this, paramParcel, paramInt);
    }
    
    public static final class CoverInfoEntity
      extends FastSafeParcelableJsonResponse
      implements Person.Cover.CoverInfo
    {
      public static final zzd CREATOR = new zzd();
      private static final HashMap<String, FastJsonResponse.Field<?, ?>> e = new HashMap();
      final Set<Integer> a;
      final int b;
      int c;
      int d;
      
      static
      {
        e.put("leftImageOffset", FastJsonResponse.Field.a("leftImageOffset", 2));
        e.put("topImageOffset", FastJsonResponse.Field.a("topImageOffset", 3));
      }
      
      public CoverInfoEntity()
      {
        this.b = 1;
        this.a = new HashSet();
      }
      
      CoverInfoEntity(Set<Integer> paramSet, int paramInt1, int paramInt2, int paramInt3)
      {
        this.a = paramSet;
        this.b = paramInt1;
        this.c = paramInt2;
        this.d = paramInt3;
      }
      
      protected boolean a(FastJsonResponse.Field paramField)
      {
        return this.a.contains(Integer.valueOf(paramField.h()));
      }
      
      protected Object b(FastJsonResponse.Field paramField)
      {
        switch (paramField.h())
        {
        default: 
          throw new IllegalStateException("Unknown safe parcelable id=" + paramField.h());
        case 2: 
          return Integer.valueOf(this.c);
        }
        return Integer.valueOf(this.d);
      }
      
      public int describeContents()
      {
        zzd localzzd = CREATOR;
        return 0;
      }
      
      public HashMap<String, FastJsonResponse.Field<?, ?>> e()
      {
        return e;
      }
      
      public boolean equals(Object paramObject)
      {
        if (!(paramObject instanceof CoverInfoEntity)) {
          return false;
        }
        if (this == paramObject) {
          return true;
        }
        paramObject = (CoverInfoEntity)paramObject;
        Iterator localIterator = e.values().iterator();
        while (localIterator.hasNext())
        {
          FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
          if (a(localField))
          {
            if (((CoverInfoEntity)paramObject).a(localField))
            {
              if (!b(localField).equals(((CoverInfoEntity)paramObject).b(localField))) {
                return false;
              }
            }
            else {
              return false;
            }
          }
          else if (((CoverInfoEntity)paramObject).a(localField)) {
            return false;
          }
        }
        return true;
      }
      
      public CoverInfoEntity f()
      {
        return this;
      }
      
      public int hashCode()
      {
        Iterator localIterator = e.values().iterator();
        int i = 0;
        if (localIterator.hasNext())
        {
          FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
          if (!a(localField)) {
            break label68;
          }
          int j = localField.h();
          i = b(localField).hashCode() + (i + j);
        }
        label68:
        for (;;)
        {
          break;
          return i;
        }
      }
      
      public void writeToParcel(Parcel paramParcel, int paramInt)
      {
        zzd localzzd = CREATOR;
        zzd.a(this, paramParcel, paramInt);
      }
    }
    
    public static final class CoverPhotoEntity
      extends FastSafeParcelableJsonResponse
      implements Person.Cover.CoverPhoto
    {
      public static final zze CREATOR = new zze();
      private static final HashMap<String, FastJsonResponse.Field<?, ?>> f = new HashMap();
      final Set<Integer> a;
      final int b;
      int c;
      String d;
      int e;
      
      static
      {
        f.put("height", FastJsonResponse.Field.a("height", 2));
        f.put("url", FastJsonResponse.Field.d("url", 3));
        f.put("width", FastJsonResponse.Field.a("width", 4));
      }
      
      public CoverPhotoEntity()
      {
        this.b = 1;
        this.a = new HashSet();
      }
      
      CoverPhotoEntity(Set<Integer> paramSet, int paramInt1, int paramInt2, String paramString, int paramInt3)
      {
        this.a = paramSet;
        this.b = paramInt1;
        this.c = paramInt2;
        this.d = paramString;
        this.e = paramInt3;
      }
      
      protected boolean a(FastJsonResponse.Field paramField)
      {
        return this.a.contains(Integer.valueOf(paramField.h()));
      }
      
      protected Object b(FastJsonResponse.Field paramField)
      {
        switch (paramField.h())
        {
        default: 
          throw new IllegalStateException("Unknown safe parcelable id=" + paramField.h());
        case 2: 
          return Integer.valueOf(this.c);
        case 3: 
          return this.d;
        }
        return Integer.valueOf(this.e);
      }
      
      public int describeContents()
      {
        zze localzze = CREATOR;
        return 0;
      }
      
      public HashMap<String, FastJsonResponse.Field<?, ?>> e()
      {
        return f;
      }
      
      public boolean equals(Object paramObject)
      {
        if (!(paramObject instanceof CoverPhotoEntity)) {
          return false;
        }
        if (this == paramObject) {
          return true;
        }
        paramObject = (CoverPhotoEntity)paramObject;
        Iterator localIterator = f.values().iterator();
        while (localIterator.hasNext())
        {
          FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
          if (a(localField))
          {
            if (((CoverPhotoEntity)paramObject).a(localField))
            {
              if (!b(localField).equals(((CoverPhotoEntity)paramObject).b(localField))) {
                return false;
              }
            }
            else {
              return false;
            }
          }
          else if (((CoverPhotoEntity)paramObject).a(localField)) {
            return false;
          }
        }
        return true;
      }
      
      public CoverPhotoEntity f()
      {
        return this;
      }
      
      public int hashCode()
      {
        Iterator localIterator = f.values().iterator();
        int i = 0;
        if (localIterator.hasNext())
        {
          FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
          if (!a(localField)) {
            break label68;
          }
          int j = localField.h();
          i = b(localField).hashCode() + (i + j);
        }
        label68:
        for (;;)
        {
          break;
          return i;
        }
      }
      
      public void writeToParcel(Parcel paramParcel, int paramInt)
      {
        zze localzze = CREATOR;
        zze.a(this, paramParcel, paramInt);
      }
    }
  }
  
  public static final class ImageEntity
    extends FastSafeParcelableJsonResponse
    implements Person.Image
  {
    public static final zzf CREATOR = new zzf();
    private static final HashMap<String, FastJsonResponse.Field<?, ?>> d = new HashMap();
    final Set<Integer> a;
    final int b;
    String c;
    
    static
    {
      d.put("url", FastJsonResponse.Field.d("url", 2));
    }
    
    public ImageEntity()
    {
      this.b = 1;
      this.a = new HashSet();
    }
    
    public ImageEntity(String paramString)
    {
      this.a = new HashSet();
      this.b = 1;
      this.c = paramString;
      this.a.add(Integer.valueOf(2));
    }
    
    ImageEntity(Set<Integer> paramSet, int paramInt, String paramString)
    {
      this.a = paramSet;
      this.b = paramInt;
      this.c = paramString;
    }
    
    protected boolean a(FastJsonResponse.Field paramField)
    {
      return this.a.contains(Integer.valueOf(paramField.h()));
    }
    
    protected Object b(FastJsonResponse.Field paramField)
    {
      switch (paramField.h())
      {
      default: 
        throw new IllegalStateException("Unknown safe parcelable id=" + paramField.h());
      }
      return this.c;
    }
    
    public int describeContents()
    {
      zzf localzzf = CREATOR;
      return 0;
    }
    
    public HashMap<String, FastJsonResponse.Field<?, ?>> e()
    {
      return d;
    }
    
    public boolean equals(Object paramObject)
    {
      if (!(paramObject instanceof ImageEntity)) {
        return false;
      }
      if (this == paramObject) {
        return true;
      }
      paramObject = (ImageEntity)paramObject;
      Iterator localIterator = d.values().iterator();
      while (localIterator.hasNext())
      {
        FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
        if (a(localField))
        {
          if (((ImageEntity)paramObject).a(localField))
          {
            if (!b(localField).equals(((ImageEntity)paramObject).b(localField))) {
              return false;
            }
          }
          else {
            return false;
          }
        }
        else if (((ImageEntity)paramObject).a(localField)) {
          return false;
        }
      }
      return true;
    }
    
    public ImageEntity f()
    {
      return this;
    }
    
    public int hashCode()
    {
      Iterator localIterator = d.values().iterator();
      int i = 0;
      if (localIterator.hasNext())
      {
        FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
        if (!a(localField)) {
          break label68;
        }
        int j = localField.h();
        i = b(localField).hashCode() + (i + j);
      }
      label68:
      for (;;)
      {
        break;
        return i;
      }
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      zzf localzzf = CREATOR;
      zzf.a(this, paramParcel, paramInt);
    }
  }
  
  public static final class NameEntity
    extends FastSafeParcelableJsonResponse
    implements Person.Name
  {
    public static final zzg CREATOR = new zzg();
    private static final HashMap<String, FastJsonResponse.Field<?, ?>> i = new HashMap();
    final Set<Integer> a;
    final int b;
    String c;
    String d;
    String e;
    String f;
    String g;
    String h;
    
    static
    {
      i.put("familyName", FastJsonResponse.Field.d("familyName", 2));
      i.put("formatted", FastJsonResponse.Field.d("formatted", 3));
      i.put("givenName", FastJsonResponse.Field.d("givenName", 4));
      i.put("honorificPrefix", FastJsonResponse.Field.d("honorificPrefix", 5));
      i.put("honorificSuffix", FastJsonResponse.Field.d("honorificSuffix", 6));
      i.put("middleName", FastJsonResponse.Field.d("middleName", 7));
    }
    
    public NameEntity()
    {
      this.b = 1;
      this.a = new HashSet();
    }
    
    NameEntity(Set<Integer> paramSet, int paramInt, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
    {
      this.a = paramSet;
      this.b = paramInt;
      this.c = paramString1;
      this.d = paramString2;
      this.e = paramString3;
      this.f = paramString4;
      this.g = paramString5;
      this.h = paramString6;
    }
    
    protected boolean a(FastJsonResponse.Field paramField)
    {
      return this.a.contains(Integer.valueOf(paramField.h()));
    }
    
    protected Object b(FastJsonResponse.Field paramField)
    {
      switch (paramField.h())
      {
      default: 
        throw new IllegalStateException("Unknown safe parcelable id=" + paramField.h());
      case 2: 
        return this.c;
      case 3: 
        return this.d;
      case 4: 
        return this.e;
      case 5: 
        return this.f;
      case 6: 
        return this.g;
      }
      return this.h;
    }
    
    public int describeContents()
    {
      zzg localzzg = CREATOR;
      return 0;
    }
    
    public HashMap<String, FastJsonResponse.Field<?, ?>> e()
    {
      return i;
    }
    
    public boolean equals(Object paramObject)
    {
      if (!(paramObject instanceof NameEntity)) {
        return false;
      }
      if (this == paramObject) {
        return true;
      }
      paramObject = (NameEntity)paramObject;
      Iterator localIterator = i.values().iterator();
      while (localIterator.hasNext())
      {
        FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
        if (a(localField))
        {
          if (((NameEntity)paramObject).a(localField))
          {
            if (!b(localField).equals(((NameEntity)paramObject).b(localField))) {
              return false;
            }
          }
          else {
            return false;
          }
        }
        else if (((NameEntity)paramObject).a(localField)) {
          return false;
        }
      }
      return true;
    }
    
    public NameEntity f()
    {
      return this;
    }
    
    public int hashCode()
    {
      Iterator localIterator = i.values().iterator();
      int j = 0;
      if (localIterator.hasNext())
      {
        FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
        if (!a(localField)) {
          break label68;
        }
        int k = localField.h();
        j = b(localField).hashCode() + (j + k);
      }
      label68:
      for (;;)
      {
        break;
        return j;
      }
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      zzg localzzg = CREATOR;
      zzg.a(this, paramParcel, paramInt);
    }
  }
  
  public static final class OrganizationsEntity
    extends FastSafeParcelableJsonResponse
    implements Person.Organizations
  {
    public static final zzh CREATOR = new zzh();
    private static final HashMap<String, FastJsonResponse.Field<?, ?>> l = new HashMap();
    final Set<Integer> a;
    final int b;
    String c;
    String d;
    String e;
    String f;
    String g;
    boolean h;
    String i;
    String j;
    int k;
    
    static
    {
      l.put("department", FastJsonResponse.Field.d("department", 2));
      l.put("description", FastJsonResponse.Field.d("description", 3));
      l.put("endDate", FastJsonResponse.Field.d("endDate", 4));
      l.put("location", FastJsonResponse.Field.d("location", 5));
      l.put("name", FastJsonResponse.Field.d("name", 6));
      l.put("primary", FastJsonResponse.Field.c("primary", 7));
      l.put("startDate", FastJsonResponse.Field.d("startDate", 8));
      l.put("title", FastJsonResponse.Field.d("title", 9));
      l.put("type", FastJsonResponse.Field.a("type", 10, new StringToIntConverter().a("work", 0).a("school", 1), false));
    }
    
    public OrganizationsEntity()
    {
      this.b = 1;
      this.a = new HashSet();
    }
    
    OrganizationsEntity(Set<Integer> paramSet, int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, boolean paramBoolean, String paramString6, String paramString7, int paramInt2)
    {
      this.a = paramSet;
      this.b = paramInt1;
      this.c = paramString1;
      this.d = paramString2;
      this.e = paramString3;
      this.f = paramString4;
      this.g = paramString5;
      this.h = paramBoolean;
      this.i = paramString6;
      this.j = paramString7;
      this.k = paramInt2;
    }
    
    protected boolean a(FastJsonResponse.Field paramField)
    {
      return this.a.contains(Integer.valueOf(paramField.h()));
    }
    
    protected Object b(FastJsonResponse.Field paramField)
    {
      switch (paramField.h())
      {
      default: 
        throw new IllegalStateException("Unknown safe parcelable id=" + paramField.h());
      case 2: 
        return this.c;
      case 3: 
        return this.d;
      case 4: 
        return this.e;
      case 5: 
        return this.f;
      case 6: 
        return this.g;
      case 7: 
        return Boolean.valueOf(this.h);
      case 8: 
        return this.i;
      case 9: 
        return this.j;
      }
      return Integer.valueOf(this.k);
    }
    
    public int describeContents()
    {
      zzh localzzh = CREATOR;
      return 0;
    }
    
    public HashMap<String, FastJsonResponse.Field<?, ?>> e()
    {
      return l;
    }
    
    public boolean equals(Object paramObject)
    {
      if (!(paramObject instanceof OrganizationsEntity)) {
        return false;
      }
      if (this == paramObject) {
        return true;
      }
      paramObject = (OrganizationsEntity)paramObject;
      Iterator localIterator = l.values().iterator();
      while (localIterator.hasNext())
      {
        FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
        if (a(localField))
        {
          if (((OrganizationsEntity)paramObject).a(localField))
          {
            if (!b(localField).equals(((OrganizationsEntity)paramObject).b(localField))) {
              return false;
            }
          }
          else {
            return false;
          }
        }
        else if (((OrganizationsEntity)paramObject).a(localField)) {
          return false;
        }
      }
      return true;
    }
    
    public OrganizationsEntity f()
    {
      return this;
    }
    
    public int hashCode()
    {
      Iterator localIterator = l.values().iterator();
      int m = 0;
      if (localIterator.hasNext())
      {
        FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
        if (!a(localField)) {
          break label68;
        }
        int n = localField.h();
        m = b(localField).hashCode() + (m + n);
      }
      label68:
      for (;;)
      {
        break;
        return m;
      }
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      zzh localzzh = CREATOR;
      zzh.a(this, paramParcel, paramInt);
    }
  }
  
  public static final class PlacesLivedEntity
    extends FastSafeParcelableJsonResponse
    implements Person.PlacesLived
  {
    public static final zzi CREATOR = new zzi();
    private static final HashMap<String, FastJsonResponse.Field<?, ?>> e = new HashMap();
    final Set<Integer> a;
    final int b;
    boolean c;
    String d;
    
    static
    {
      e.put("primary", FastJsonResponse.Field.c("primary", 2));
      e.put("value", FastJsonResponse.Field.d("value", 3));
    }
    
    public PlacesLivedEntity()
    {
      this.b = 1;
      this.a = new HashSet();
    }
    
    PlacesLivedEntity(Set<Integer> paramSet, int paramInt, boolean paramBoolean, String paramString)
    {
      this.a = paramSet;
      this.b = paramInt;
      this.c = paramBoolean;
      this.d = paramString;
    }
    
    protected boolean a(FastJsonResponse.Field paramField)
    {
      return this.a.contains(Integer.valueOf(paramField.h()));
    }
    
    protected Object b(FastJsonResponse.Field paramField)
    {
      switch (paramField.h())
      {
      default: 
        throw new IllegalStateException("Unknown safe parcelable id=" + paramField.h());
      case 2: 
        return Boolean.valueOf(this.c);
      }
      return this.d;
    }
    
    public int describeContents()
    {
      zzi localzzi = CREATOR;
      return 0;
    }
    
    public HashMap<String, FastJsonResponse.Field<?, ?>> e()
    {
      return e;
    }
    
    public boolean equals(Object paramObject)
    {
      if (!(paramObject instanceof PlacesLivedEntity)) {
        return false;
      }
      if (this == paramObject) {
        return true;
      }
      paramObject = (PlacesLivedEntity)paramObject;
      Iterator localIterator = e.values().iterator();
      while (localIterator.hasNext())
      {
        FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
        if (a(localField))
        {
          if (((PlacesLivedEntity)paramObject).a(localField))
          {
            if (!b(localField).equals(((PlacesLivedEntity)paramObject).b(localField))) {
              return false;
            }
          }
          else {
            return false;
          }
        }
        else if (((PlacesLivedEntity)paramObject).a(localField)) {
          return false;
        }
      }
      return true;
    }
    
    public PlacesLivedEntity f()
    {
      return this;
    }
    
    public int hashCode()
    {
      Iterator localIterator = e.values().iterator();
      int i = 0;
      if (localIterator.hasNext())
      {
        FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
        if (!a(localField)) {
          break label68;
        }
        int j = localField.h();
        i = b(localField).hashCode() + (i + j);
      }
      label68:
      for (;;)
      {
        break;
        return i;
      }
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      zzi localzzi = CREATOR;
      zzi.a(this, paramParcel, paramInt);
    }
  }
  
  public static final class UrlsEntity
    extends FastSafeParcelableJsonResponse
    implements Person.Urls
  {
    public static final zzj CREATOR = new zzj();
    private static final HashMap<String, FastJsonResponse.Field<?, ?>> f = new HashMap();
    final Set<Integer> a;
    final int b;
    String c;
    int d;
    String e;
    private final int g = 4;
    
    static
    {
      f.put("label", FastJsonResponse.Field.d("label", 5));
      f.put("type", FastJsonResponse.Field.a("type", 6, new StringToIntConverter().a("home", 0).a("work", 1).a("blog", 2).a("profile", 3).a("other", 4).a("otherProfile", 5).a("contributor", 6).a("website", 7), false));
      f.put("value", FastJsonResponse.Field.d("value", 4));
    }
    
    public UrlsEntity()
    {
      this.b = 1;
      this.a = new HashSet();
    }
    
    UrlsEntity(Set<Integer> paramSet, int paramInt1, String paramString1, int paramInt2, String paramString2, int paramInt3)
    {
      this.a = paramSet;
      this.b = paramInt1;
      this.c = paramString1;
      this.d = paramInt2;
      this.e = paramString2;
    }
    
    protected boolean a(FastJsonResponse.Field paramField)
    {
      return this.a.contains(Integer.valueOf(paramField.h()));
    }
    
    protected Object b(FastJsonResponse.Field paramField)
    {
      switch (paramField.h())
      {
      default: 
        throw new IllegalStateException("Unknown safe parcelable id=" + paramField.h());
      case 5: 
        return this.c;
      case 6: 
        return Integer.valueOf(this.d);
      }
      return this.e;
    }
    
    public int describeContents()
    {
      zzj localzzj = CREATOR;
      return 0;
    }
    
    public HashMap<String, FastJsonResponse.Field<?, ?>> e()
    {
      return f;
    }
    
    public boolean equals(Object paramObject)
    {
      if (!(paramObject instanceof UrlsEntity)) {
        return false;
      }
      if (this == paramObject) {
        return true;
      }
      paramObject = (UrlsEntity)paramObject;
      Iterator localIterator = f.values().iterator();
      while (localIterator.hasNext())
      {
        FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
        if (a(localField))
        {
          if (((UrlsEntity)paramObject).a(localField))
          {
            if (!b(localField).equals(((UrlsEntity)paramObject).b(localField))) {
              return false;
            }
          }
          else {
            return false;
          }
        }
        else if (((UrlsEntity)paramObject).a(localField)) {
          return false;
        }
      }
      return true;
    }
    
    @Deprecated
    public int f()
    {
      return 4;
    }
    
    public UrlsEntity g()
    {
      return this;
    }
    
    public int hashCode()
    {
      Iterator localIterator = f.values().iterator();
      int i = 0;
      if (localIterator.hasNext())
      {
        FastJsonResponse.Field localField = (FastJsonResponse.Field)localIterator.next();
        if (!a(localField)) {
          break label68;
        }
        int j = localField.h();
        i = b(localField).hashCode() + (i + j);
      }
      label68:
      for (;;)
      {
        break;
        return i;
      }
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      zzj localzzj = CREATOR;
      zzj.a(this, paramParcel, paramInt);
    }
  }
  
  public static class zza
  {
    public static int a(String paramString)
    {
      if (paramString.equals("person")) {
        return 0;
      }
      if (paramString.equals("page")) {
        return 1;
      }
      throw new IllegalArgumentException("Unknown objectType string: " + paramString);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/plus/internal/model/people/PersonEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */