package com.google.android.gms.plus.internal.model.people;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.HashSet;
import java.util.Set;

public class zzj
  implements Parcelable.Creator<PersonEntity.UrlsEntity>
{
  static void a(PersonEntity.UrlsEntity paramUrlsEntity, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    Set localSet = paramUrlsEntity.a;
    if (localSet.contains(Integer.valueOf(1))) {
      zzb.a(paramParcel, 1, paramUrlsEntity.b);
    }
    if (localSet.contains(Integer.valueOf(3))) {
      zzb.a(paramParcel, 3, paramUrlsEntity.f());
    }
    if (localSet.contains(Integer.valueOf(4))) {
      zzb.a(paramParcel, 4, paramUrlsEntity.e, true);
    }
    if (localSet.contains(Integer.valueOf(5))) {
      zzb.a(paramParcel, 5, paramUrlsEntity.c, true);
    }
    if (localSet.contains(Integer.valueOf(6))) {
      zzb.a(paramParcel, 6, paramUrlsEntity.d);
    }
    zzb.a(paramParcel, paramInt);
  }
  
  public PersonEntity.UrlsEntity a(Parcel paramParcel)
  {
    String str1 = null;
    int i = 0;
    int m = zza.b(paramParcel);
    HashSet localHashSet = new HashSet();
    int j = 0;
    String str2 = null;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      case 2: 
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        k = zza.g(paramParcel, n);
        localHashSet.add(Integer.valueOf(1));
        break;
      case 3: 
        i = zza.g(paramParcel, n);
        localHashSet.add(Integer.valueOf(3));
        break;
      case 4: 
        str1 = zza.p(paramParcel, n);
        localHashSet.add(Integer.valueOf(4));
        break;
      case 5: 
        str2 = zza.p(paramParcel, n);
        localHashSet.add(Integer.valueOf(5));
        break;
      case 6: 
        j = zza.g(paramParcel, n);
        localHashSet.add(Integer.valueOf(6));
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new PersonEntity.UrlsEntity(localHashSet, k, str2, j, str1, i);
  }
  
  public PersonEntity.UrlsEntity[] a(int paramInt)
  {
    return new PersonEntity.UrlsEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/plus/internal/model/people/zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */