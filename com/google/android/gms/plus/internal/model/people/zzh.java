package com.google.android.gms.plus.internal.model.people;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.HashSet;
import java.util.Set;

public class zzh
  implements Parcelable.Creator<PersonEntity.OrganizationsEntity>
{
  static void a(PersonEntity.OrganizationsEntity paramOrganizationsEntity, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    Set localSet = paramOrganizationsEntity.a;
    if (localSet.contains(Integer.valueOf(1))) {
      zzb.a(paramParcel, 1, paramOrganizationsEntity.b);
    }
    if (localSet.contains(Integer.valueOf(2))) {
      zzb.a(paramParcel, 2, paramOrganizationsEntity.c, true);
    }
    if (localSet.contains(Integer.valueOf(3))) {
      zzb.a(paramParcel, 3, paramOrganizationsEntity.d, true);
    }
    if (localSet.contains(Integer.valueOf(4))) {
      zzb.a(paramParcel, 4, paramOrganizationsEntity.e, true);
    }
    if (localSet.contains(Integer.valueOf(5))) {
      zzb.a(paramParcel, 5, paramOrganizationsEntity.f, true);
    }
    if (localSet.contains(Integer.valueOf(6))) {
      zzb.a(paramParcel, 6, paramOrganizationsEntity.g, true);
    }
    if (localSet.contains(Integer.valueOf(7))) {
      zzb.a(paramParcel, 7, paramOrganizationsEntity.h);
    }
    if (localSet.contains(Integer.valueOf(8))) {
      zzb.a(paramParcel, 8, paramOrganizationsEntity.i, true);
    }
    if (localSet.contains(Integer.valueOf(9))) {
      zzb.a(paramParcel, 9, paramOrganizationsEntity.j, true);
    }
    if (localSet.contains(Integer.valueOf(10))) {
      zzb.a(paramParcel, 10, paramOrganizationsEntity.k);
    }
    zzb.a(paramParcel, paramInt);
  }
  
  public PersonEntity.OrganizationsEntity a(Parcel paramParcel)
  {
    int i = 0;
    String str1 = null;
    int k = zza.b(paramParcel);
    HashSet localHashSet = new HashSet();
    String str2 = null;
    boolean bool = false;
    String str3 = null;
    String str4 = null;
    String str5 = null;
    String str6 = null;
    String str7 = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        j = zza.g(paramParcel, m);
        localHashSet.add(Integer.valueOf(1));
        break;
      case 2: 
        str7 = zza.p(paramParcel, m);
        localHashSet.add(Integer.valueOf(2));
        break;
      case 3: 
        str6 = zza.p(paramParcel, m);
        localHashSet.add(Integer.valueOf(3));
        break;
      case 4: 
        str5 = zza.p(paramParcel, m);
        localHashSet.add(Integer.valueOf(4));
        break;
      case 5: 
        str4 = zza.p(paramParcel, m);
        localHashSet.add(Integer.valueOf(5));
        break;
      case 6: 
        str3 = zza.p(paramParcel, m);
        localHashSet.add(Integer.valueOf(6));
        break;
      case 7: 
        bool = zza.c(paramParcel, m);
        localHashSet.add(Integer.valueOf(7));
        break;
      case 8: 
        str2 = zza.p(paramParcel, m);
        localHashSet.add(Integer.valueOf(8));
        break;
      case 9: 
        str1 = zza.p(paramParcel, m);
        localHashSet.add(Integer.valueOf(9));
        break;
      case 10: 
        i = zza.g(paramParcel, m);
        localHashSet.add(Integer.valueOf(10));
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new PersonEntity.OrganizationsEntity(localHashSet, j, str7, str6, str5, str4, str3, bool, str2, str1, i);
  }
  
  public PersonEntity.OrganizationsEntity[] a(int paramInt)
  {
    return new PersonEntity.OrganizationsEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/plus/internal/model/people/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */