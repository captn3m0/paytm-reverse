package com.google.android.gms.plus.internal.model.people;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.zzc;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.Person.Image;

public final class zzk
  extends zzc
  implements Person
{
  public zzk(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
  }
  
  public String c()
  {
    return e("displayName");
  }
  
  public String d()
  {
    return e("personId");
  }
  
  public Person.Image e()
  {
    return new PersonEntity.ImageEntity(e("image"));
  }
  
  public int f()
  {
    return PersonEntity.zza.a(e("objectType"));
  }
  
  public String g()
  {
    return e("url");
  }
  
  public Person h()
  {
    return new PersonEntity(c(), d(), (PersonEntity.ImageEntity)e(), f(), g());
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/plus/internal/model/people/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */