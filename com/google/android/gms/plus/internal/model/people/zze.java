package com.google.android.gms.plus.internal.model.people;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.HashSet;
import java.util.Set;

public class zze
  implements Parcelable.Creator<PersonEntity.CoverEntity.CoverPhotoEntity>
{
  static void a(PersonEntity.CoverEntity.CoverPhotoEntity paramCoverPhotoEntity, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    Set localSet = paramCoverPhotoEntity.a;
    if (localSet.contains(Integer.valueOf(1))) {
      zzb.a(paramParcel, 1, paramCoverPhotoEntity.b);
    }
    if (localSet.contains(Integer.valueOf(2))) {
      zzb.a(paramParcel, 2, paramCoverPhotoEntity.c);
    }
    if (localSet.contains(Integer.valueOf(3))) {
      zzb.a(paramParcel, 3, paramCoverPhotoEntity.d, true);
    }
    if (localSet.contains(Integer.valueOf(4))) {
      zzb.a(paramParcel, 4, paramCoverPhotoEntity.e);
    }
    zzb.a(paramParcel, paramInt);
  }
  
  public PersonEntity.CoverEntity.CoverPhotoEntity a(Parcel paramParcel)
  {
    int i = 0;
    int m = zza.b(paramParcel);
    HashSet localHashSet = new HashSet();
    String str = null;
    int j = 0;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        k = zza.g(paramParcel, n);
        localHashSet.add(Integer.valueOf(1));
        break;
      case 2: 
        j = zza.g(paramParcel, n);
        localHashSet.add(Integer.valueOf(2));
        break;
      case 3: 
        str = zza.p(paramParcel, n);
        localHashSet.add(Integer.valueOf(3));
        break;
      case 4: 
        i = zza.g(paramParcel, n);
        localHashSet.add(Integer.valueOf(4));
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new PersonEntity.CoverEntity.CoverPhotoEntity(localHashSet, k, j, str, i);
  }
  
  public PersonEntity.CoverEntity.CoverPhotoEntity[] a(int paramInt)
  {
    return new PersonEntity.CoverEntity.CoverPhotoEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/plus/internal/model/people/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */