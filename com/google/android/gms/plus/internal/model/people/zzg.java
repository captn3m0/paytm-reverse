package com.google.android.gms.plus.internal.model.people;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.HashSet;
import java.util.Set;

public class zzg
  implements Parcelable.Creator<PersonEntity.NameEntity>
{
  static void a(PersonEntity.NameEntity paramNameEntity, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    Set localSet = paramNameEntity.a;
    if (localSet.contains(Integer.valueOf(1))) {
      zzb.a(paramParcel, 1, paramNameEntity.b);
    }
    if (localSet.contains(Integer.valueOf(2))) {
      zzb.a(paramParcel, 2, paramNameEntity.c, true);
    }
    if (localSet.contains(Integer.valueOf(3))) {
      zzb.a(paramParcel, 3, paramNameEntity.d, true);
    }
    if (localSet.contains(Integer.valueOf(4))) {
      zzb.a(paramParcel, 4, paramNameEntity.e, true);
    }
    if (localSet.contains(Integer.valueOf(5))) {
      zzb.a(paramParcel, 5, paramNameEntity.f, true);
    }
    if (localSet.contains(Integer.valueOf(6))) {
      zzb.a(paramParcel, 6, paramNameEntity.g, true);
    }
    if (localSet.contains(Integer.valueOf(7))) {
      zzb.a(paramParcel, 7, paramNameEntity.h, true);
    }
    zzb.a(paramParcel, paramInt);
  }
  
  public PersonEntity.NameEntity a(Parcel paramParcel)
  {
    String str1 = null;
    int j = zza.b(paramParcel);
    HashSet localHashSet = new HashSet();
    int i = 0;
    String str2 = null;
    String str3 = null;
    String str4 = null;
    String str5 = null;
    String str6 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        localHashSet.add(Integer.valueOf(1));
        break;
      case 2: 
        str6 = zza.p(paramParcel, k);
        localHashSet.add(Integer.valueOf(2));
        break;
      case 3: 
        str5 = zza.p(paramParcel, k);
        localHashSet.add(Integer.valueOf(3));
        break;
      case 4: 
        str4 = zza.p(paramParcel, k);
        localHashSet.add(Integer.valueOf(4));
        break;
      case 5: 
        str3 = zza.p(paramParcel, k);
        localHashSet.add(Integer.valueOf(5));
        break;
      case 6: 
        str2 = zza.p(paramParcel, k);
        localHashSet.add(Integer.valueOf(6));
        break;
      case 7: 
        str1 = zza.p(paramParcel, k);
        localHashSet.add(Integer.valueOf(7));
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new PersonEntity.NameEntity(localHashSet, i, str6, str5, str4, str3, str2, str1);
  }
  
  public PersonEntity.NameEntity[] a(int paramInt)
  {
    return new PersonEntity.NameEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/plus/internal/model/people/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */