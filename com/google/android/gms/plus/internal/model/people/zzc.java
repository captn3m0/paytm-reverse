package com.google.android.gms.plus.internal.model.people;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.HashSet;
import java.util.Set;

public class zzc
  implements Parcelable.Creator<PersonEntity.CoverEntity>
{
  static void a(PersonEntity.CoverEntity paramCoverEntity, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    Set localSet = paramCoverEntity.a;
    if (localSet.contains(Integer.valueOf(1))) {
      zzb.a(paramParcel, 1, paramCoverEntity.b);
    }
    if (localSet.contains(Integer.valueOf(2))) {
      zzb.a(paramParcel, 2, paramCoverEntity.c, paramInt, true);
    }
    if (localSet.contains(Integer.valueOf(3))) {
      zzb.a(paramParcel, 3, paramCoverEntity.d, paramInt, true);
    }
    if (localSet.contains(Integer.valueOf(4))) {
      zzb.a(paramParcel, 4, paramCoverEntity.e);
    }
    zzb.a(paramParcel, i);
  }
  
  public PersonEntity.CoverEntity a(Parcel paramParcel)
  {
    PersonEntity.CoverEntity.CoverPhotoEntity localCoverPhotoEntity = null;
    int i = 0;
    int k = zza.b(paramParcel);
    HashSet localHashSet = new HashSet();
    PersonEntity.CoverEntity.CoverInfoEntity localCoverInfoEntity = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        j = zza.g(paramParcel, m);
        localHashSet.add(Integer.valueOf(1));
        break;
      case 2: 
        localCoverInfoEntity = (PersonEntity.CoverEntity.CoverInfoEntity)zza.a(paramParcel, m, PersonEntity.CoverEntity.CoverInfoEntity.CREATOR);
        localHashSet.add(Integer.valueOf(2));
        break;
      case 3: 
        localCoverPhotoEntity = (PersonEntity.CoverEntity.CoverPhotoEntity)zza.a(paramParcel, m, PersonEntity.CoverEntity.CoverPhotoEntity.CREATOR);
        localHashSet.add(Integer.valueOf(3));
        break;
      case 4: 
        i = zza.g(paramParcel, m);
        localHashSet.add(Integer.valueOf(4));
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new PersonEntity.CoverEntity(localHashSet, j, localCoverInfoEntity, localCoverPhotoEntity, i);
  }
  
  public PersonEntity.CoverEntity[] a(int paramInt)
  {
    return new PersonEntity.CoverEntity[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/plus/internal/model/people/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */