package com.google.android.gms.plus.internal;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.zzc;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;

@KeepName
public class PlusCommonExtras
  implements SafeParcelable
{
  public static final zzf CREATOR = new zzf();
  private final int a;
  private String b;
  private String c;
  
  public PlusCommonExtras()
  {
    this.a = 1;
    this.b = "";
    this.c = "";
  }
  
  PlusCommonExtras(int paramInt, String paramString1, String paramString2)
  {
    this.a = paramInt;
    this.b = paramString1;
    this.c = paramString2;
  }
  
  public int a()
  {
    return this.a;
  }
  
  public void a(Bundle paramBundle)
  {
    paramBundle.putByteArray("android.gms.plus.internal.PlusCommonExtras.extraPlusCommon", zzc.a(this));
  }
  
  public String b()
  {
    return this.b;
  }
  
  public String c()
  {
    return this.c;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof PlusCommonExtras)) {}
    do
    {
      return false;
      paramObject = (PlusCommonExtras)paramObject;
    } while ((this.a != ((PlusCommonExtras)paramObject).a) || (!zzw.a(this.b, ((PlusCommonExtras)paramObject).b)) || (!zzw.a(this.c, ((PlusCommonExtras)paramObject).c)));
    return true;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Integer.valueOf(this.a), this.b, this.c });
  }
  
  public String toString()
  {
    return zzw.a(this).a("versionCode", Integer.valueOf(this.a)).a("Gpsrc", this.b).a("ClientCallingPackage", this.c).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/plus/internal/PlusCommonExtras.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */