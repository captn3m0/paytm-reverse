package com.google.android.gms.plus.internal;

import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.server.response.SafeParcelResponse;
import com.google.android.gms.plus.internal.model.people.PersonEntity;

public abstract class zza
  extends zzb.zza
{
  public void a(int paramInt, Bundle paramBundle) {}
  
  public void a(int paramInt, Bundle paramBundle1, Bundle paramBundle2)
    throws RemoteException
  {}
  
  public void a(int paramInt, Bundle paramBundle, ParcelFileDescriptor paramParcelFileDescriptor)
    throws RemoteException
  {}
  
  public final void a(int paramInt, Bundle paramBundle, SafeParcelResponse paramSafeParcelResponse) {}
  
  public void a(int paramInt, PersonEntity paramPersonEntity) {}
  
  public void a(Status paramStatus) {}
  
  public void a(DataHolder paramDataHolder, String paramString) {}
  
  public void a(DataHolder paramDataHolder, String paramString1, String paramString2) {}
  
  public void a(String paramString)
    throws RemoteException
  {}
  
  public void b(String paramString) {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/plus/internal/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */