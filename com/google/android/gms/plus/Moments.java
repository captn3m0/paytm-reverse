package com.google.android.gms.plus;

import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;

public abstract interface Moments
{
  public static abstract interface LoadMomentsResult
    extends Releasable, Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/plus/Moments.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */