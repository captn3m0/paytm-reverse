package com.google.android.gms.plus;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.Optional;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.internal.zza.zza;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.internal.zznh;
import com.google.android.gms.internal.zzqv;
import com.google.android.gms.internal.zzqw;
import com.google.android.gms.internal.zzqx;
import com.google.android.gms.internal.zzqy;
import com.google.android.gms.internal.zzqz;
import com.google.android.gms.plus.internal.PlusCommonExtras;
import com.google.android.gms.plus.internal.PlusSession;
import com.google.android.gms.plus.internal.zze;
import java.util.HashSet;
import java.util.Set;

public final class Plus
{
  public static final Api.zzc<zze> a = new Api.zzc();
  static final Api.zza<zze, PlusOptions> b = new Api.zza()
  {
    public int a()
    {
      return 2;
    }
    
    public zze a(Context paramAnonymousContext, Looper paramAnonymousLooper, zzf paramAnonymouszzf, Plus.PlusOptions paramAnonymousPlusOptions, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      Object localObject = paramAnonymousPlusOptions;
      if (paramAnonymousPlusOptions == null) {
        localObject = new Plus.PlusOptions(null);
      }
      paramAnonymousPlusOptions = paramAnonymouszzf.c().name;
      String[] arrayOfString = zznh.a(paramAnonymouszzf.f());
      localObject = (String[])((Plus.PlusOptions)localObject).b.toArray(new String[0]);
      String str1 = paramAnonymousContext.getPackageName();
      String str2 = paramAnonymousContext.getPackageName();
      PlusCommonExtras localPlusCommonExtras = new PlusCommonExtras();
      return new zze(paramAnonymousContext, paramAnonymousLooper, paramAnonymouszzf, new PlusSession(paramAnonymousPlusOptions, arrayOfString, (String[])localObject, new String[0], str1, str2, null, localPlusCommonExtras), paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
    }
  };
  public static final Api<PlusOptions> c = new Api("Plus.API", b, a);
  public static final Scope d = new Scope("https://www.googleapis.com/auth/plus.login");
  public static final Scope e = new Scope("https://www.googleapis.com/auth/plus.me");
  public static final Moments f = new zzqy();
  public static final People g = new zzqz();
  @Deprecated
  public static final Account h = new zzqv();
  public static final zzb i = new zzqx();
  public static final zza j = new zzqw();
  
  public static final class PlusOptions
    implements Api.ApiOptions.Optional
  {
    final String a = null;
    final Set<String> b = new HashSet();
    
    public static final class Builder
    {
      final Set<String> a = new HashSet();
    }
  }
  
  public static abstract class zza<R extends Result>
    extends zza.zza<R, zze>
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/plus/Plus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */