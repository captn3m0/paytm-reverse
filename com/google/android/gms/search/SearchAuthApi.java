package com.google.android.gms.search;

import com.google.android.gms.common.api.Result;

public abstract interface SearchAuthApi
{
  public static abstract interface GoogleNowAuthResult
    extends Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/search/SearchAuthApi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */