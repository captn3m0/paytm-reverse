package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzk
  implements Parcelable.Creator<RealtimeDocumentSyncRequest>
{
  static void a(RealtimeDocumentSyncRequest paramRealtimeDocumentSyncRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramRealtimeDocumentSyncRequest.a);
    zzb.b(paramParcel, 2, paramRealtimeDocumentSyncRequest.b, false);
    zzb.b(paramParcel, 3, paramRealtimeDocumentSyncRequest.c, false);
    zzb.a(paramParcel, paramInt);
  }
  
  public RealtimeDocumentSyncRequest a(Parcel paramParcel)
  {
    ArrayList localArrayList2 = null;
    int j = zza.b(paramParcel);
    int i = 0;
    ArrayList localArrayList1 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localArrayList1 = zza.D(paramParcel, k);
        break;
      case 3: 
        localArrayList2 = zza.D(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new RealtimeDocumentSyncRequest(i, localArrayList1, localArrayList2);
  }
  
  public RealtimeDocumentSyncRequest[] a(int paramInt)
  {
    return new RealtimeDocumentSyncRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */