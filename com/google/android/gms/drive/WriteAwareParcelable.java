package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.zzx;

public abstract class WriteAwareParcelable
  implements Parcelable
{
  private volatile transient boolean a = false;
  
  protected abstract void a(Parcel paramParcel, int paramInt);
  
  public final boolean h_()
  {
    return this.a;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    if (!h_()) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.a(bool);
      this.a = true;
      a(paramParcel, paramInt);
      return;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/WriteAwareParcelable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */