package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.DriveId;

public class zzbq
  implements Parcelable.Creator<RemovePermissionRequest>
{
  static void a(RemovePermissionRequest paramRemovePermissionRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramRemovePermissionRequest.a);
    zzb.a(paramParcel, 2, paramRemovePermissionRequest.b, paramInt, false);
    zzb.a(paramParcel, 3, paramRemovePermissionRequest.c, false);
    zzb.a(paramParcel, 4, paramRemovePermissionRequest.d);
    zzb.a(paramParcel, 5, paramRemovePermissionRequest.e, false);
    zzb.a(paramParcel, i);
  }
  
  public RemovePermissionRequest a(Parcel paramParcel)
  {
    boolean bool = false;
    String str1 = null;
    int j = zza.b(paramParcel);
    String str2 = null;
    DriveId localDriveId = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localDriveId = (DriveId)zza.a(paramParcel, k, DriveId.CREATOR);
        break;
      case 3: 
        str2 = zza.p(paramParcel, k);
        break;
      case 4: 
        bool = zza.c(paramParcel, k);
        break;
      case 5: 
        str1 = zza.p(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new RemovePermissionRequest(i, localDriveId, str2, bool, str1);
  }
  
  public RemovePermissionRequest[] a(int paramInt)
  {
    return new RemovePermissionRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzbq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */