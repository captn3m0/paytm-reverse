package com.google.android.gms.drive.internal;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.Looper;
import android.os.Process;
import android.os.RemoteException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.events.ChangeListener;
import com.google.android.gms.drive.events.zzc;
import com.google.android.gms.drive.events.zzi;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class zzu
  extends zzj<zzam>
{
  final GoogleApiClient.ConnectionCallbacks a;
  final Map<DriveId, Map<ChangeListener, zzae>> e = new HashMap();
  final Map<zzc, zzae> f = new HashMap();
  final Map<DriveId, Map<zzi, zzae>> g = new HashMap();
  final Map<DriveId, Map<zzi, zzae>> h = new HashMap();
  private final String i;
  private final Bundle j;
  private final boolean k;
  private volatile DriveId l;
  private volatile DriveId m;
  private volatile boolean n = false;
  
  public zzu(Context paramContext, Looper paramLooper, zzf paramzzf, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener, Bundle paramBundle)
  {
    super(paramContext, paramLooper, 11, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    this.i = paramzzf.h();
    this.a = paramConnectionCallbacks;
    this.j = paramBundle;
    paramLooper = new Intent("com.google.android.gms.drive.events.HANDLE_EVENT");
    paramLooper.setPackage(paramContext.getPackageName());
    paramContext = paramContext.getPackageManager().queryIntentServices(paramLooper, 0);
    switch (paramContext.size())
    {
    default: 
      throw new IllegalStateException("AndroidManifest.xml can only define one service that handles the " + paramLooper.getAction() + " action");
    case 0: 
      this.k = false;
      return;
    }
    paramContext = ((ResolveInfo)paramContext.get(0)).serviceInfo;
    if (!paramContext.exported) {
      throw new IllegalStateException("Drive event service " + paramContext.name + " must be exported in AndroidManifest.xml");
    }
    this.k = true;
  }
  
  protected zzam a(IBinder paramIBinder)
  {
    return zzam.zza.a(paramIBinder);
  }
  
  protected String a()
  {
    return "com.google.android.gms.drive.ApiService.START";
  }
  
  protected void a(int paramInt1, IBinder paramIBinder, Bundle paramBundle, int paramInt2)
  {
    if (paramBundle != null)
    {
      paramBundle.setClassLoader(getClass().getClassLoader());
      this.l = ((DriveId)paramBundle.getParcelable("com.google.android.gms.drive.root_id"));
      this.m = ((DriveId)paramBundle.getParcelable("com.google.android.gms.drive.appdata_id"));
      this.n = true;
    }
    super.a(paramInt1, paramIBinder, paramBundle, paramInt2);
  }
  
  protected Bundle a_()
  {
    String str = q().getPackageName();
    zzx.a(str);
    if (!t().f().isEmpty()) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.a(bool);
      Bundle localBundle = new Bundle();
      if (!str.equals(this.i)) {
        localBundle.putString("proxy_package_name", this.i);
      }
      localBundle.putAll(this.j);
      return localBundle;
    }
  }
  
  protected String b()
  {
    return "com.google.android.gms.drive.internal.IDriveService";
  }
  
  public void f()
  {
    if (k()) {}
    try
    {
      ((zzam)v()).a(new DisconnectRequest());
      super.f();
      synchronized (this.e)
      {
        this.e.clear();
        synchronized (this.f)
        {
          this.f.clear();
          synchronized (this.g)
          {
            this.g.clear();
          }
        }
      }
      synchronized (this.h)
      {
        this.h.clear();
        return;
        localObject1 = finally;
        throw ((Throwable)localObject1);
        localObject2 = finally;
        throw ((Throwable)localObject2);
        localObject3 = finally;
        throw ((Throwable)localObject3);
      }
    }
    catch (RemoteException localRemoteException)
    {
      for (;;) {}
    }
  }
  
  boolean h()
  {
    return GooglePlayServicesUtil.b(q(), Process.myUid());
  }
  
  public zzam i()
    throws DeadObjectException
  {
    return (zzam)v();
  }
  
  public boolean l()
  {
    return (!q().getPackageName().equals(this.i)) || (!h());
  }
  
  public boolean w()
  {
    return true;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */