package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.DriveId;

public class zzj
  implements Parcelable.Creator<ControlProgressRequest>
{
  static void a(ControlProgressRequest paramControlProgressRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramControlProgressRequest.a);
    zzb.a(paramParcel, 2, paramControlProgressRequest.b);
    zzb.a(paramParcel, 3, paramControlProgressRequest.c);
    zzb.a(paramParcel, 4, paramControlProgressRequest.d, paramInt, false);
    zzb.a(paramParcel, 5, paramControlProgressRequest.e, paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public ControlProgressRequest a(Parcel paramParcel)
  {
    ParcelableTransferPreferences localParcelableTransferPreferences = null;
    int i = 0;
    int m = zza.b(paramParcel);
    DriveId localDriveId = null;
    int j = 0;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        j = zza.g(paramParcel, n);
        break;
      case 3: 
        i = zza.g(paramParcel, n);
        break;
      case 4: 
        localDriveId = (DriveId)zza.a(paramParcel, n, DriveId.CREATOR);
        break;
      case 5: 
        localParcelableTransferPreferences = (ParcelableTransferPreferences)zza.a(paramParcel, n, ParcelableTransferPreferences.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new ControlProgressRequest(k, j, i, localDriveId, localParcelableTransferPreferences);
  }
  
  public ControlProgressRequest[] a(int paramInt)
  {
    return new ControlProgressRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */