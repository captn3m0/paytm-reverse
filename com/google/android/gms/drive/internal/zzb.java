package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.Permission;

public class zzb
  implements Parcelable.Creator<AddPermissionRequest>
{
  static void a(AddPermissionRequest paramAddPermissionRequest, Parcel paramParcel, int paramInt)
  {
    int i = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramAddPermissionRequest.a);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramAddPermissionRequest.b, paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 3, paramAddPermissionRequest.c, paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 4, paramAddPermissionRequest.d);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 5, paramAddPermissionRequest.e, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 6, paramAddPermissionRequest.f);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 7, paramAddPermissionRequest.g, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, i);
  }
  
  public AddPermissionRequest a(Parcel paramParcel)
  {
    boolean bool1 = false;
    String str1 = null;
    int j = zza.b(paramParcel);
    String str2 = null;
    boolean bool2 = false;
    Permission localPermission = null;
    DriveId localDriveId = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localDriveId = (DriveId)zza.a(paramParcel, k, DriveId.CREATOR);
        break;
      case 3: 
        localPermission = (Permission)zza.a(paramParcel, k, Permission.CREATOR);
        break;
      case 4: 
        bool2 = zza.c(paramParcel, k);
        break;
      case 5: 
        str2 = zza.p(paramParcel, k);
        break;
      case 6: 
        bool1 = zza.c(paramParcel, k);
        break;
      case 7: 
        str1 = zza.p(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new AddPermissionRequest(i, localDriveId, localPermission, bool2, str2, bool1, str1);
  }
  
  public AddPermissionRequest[] a(int paramInt)
  {
    return new AddPermissionRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */