package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzbs
  implements Parcelable.Creator<SetPinnedDownloadPreferencesRequest>
{
  static void a(SetPinnedDownloadPreferencesRequest paramSetPinnedDownloadPreferencesRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramSetPinnedDownloadPreferencesRequest.a);
    zzb.a(paramParcel, 2, paramSetPinnedDownloadPreferencesRequest.b, paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public SetPinnedDownloadPreferencesRequest a(Parcel paramParcel)
  {
    int j = zza.b(paramParcel);
    int i = 0;
    ParcelableTransferPreferences localParcelableTransferPreferences = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localParcelableTransferPreferences = (ParcelableTransferPreferences)zza.a(paramParcel, k, ParcelableTransferPreferences.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new SetPinnedDownloadPreferencesRequest(i, localParcelableTransferPreferences);
  }
  
  public SetPinnedDownloadPreferencesRequest[] a(int paramInt)
  {
    return new SetPinnedDownloadPreferencesRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzbs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */