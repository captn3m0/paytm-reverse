package com.google.android.gms.drive.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.internal.zzna;

public class zzv
  implements DriveContents
{
  private final Contents a;
  private boolean b = false;
  private boolean c = false;
  private boolean d = false;
  
  public zzv(Contents paramContents)
  {
    this.a = ((Contents)zzx.a(paramContents));
  }
  
  public void a()
  {
    zzna.a(this.a.a());
    this.b = true;
  }
  
  public DriveId b()
  {
    return this.a.b();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */