package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.DriveId;
import java.util.ArrayList;
import java.util.List;

public class zzf
  implements Parcelable.Creator<ChangeResourceParentsRequest>
{
  static void a(ChangeResourceParentsRequest paramChangeResourceParentsRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramChangeResourceParentsRequest.a);
    zzb.a(paramParcel, 2, paramChangeResourceParentsRequest.b, paramInt, false);
    zzb.c(paramParcel, 3, paramChangeResourceParentsRequest.c, false);
    zzb.c(paramParcel, 4, paramChangeResourceParentsRequest.d, false);
    zzb.a(paramParcel, i);
  }
  
  public ChangeResourceParentsRequest a(Parcel paramParcel)
  {
    ArrayList localArrayList = null;
    int j = zza.b(paramParcel);
    int i = 0;
    Object localObject2 = null;
    Object localObject1 = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      Object localObject3;
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject3;
      }
      for (;;)
      {
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject3;
        break;
        i = zza.g(paramParcel, k);
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject3;
        continue;
        localObject3 = (DriveId)zza.a(paramParcel, k, DriveId.CREATOR);
        localObject1 = localObject2;
        localObject2 = localObject3;
        continue;
        localObject3 = zza.c(paramParcel, k, DriveId.CREATOR);
        localObject2 = localObject1;
        localObject1 = localObject3;
        continue;
        localArrayList = zza.c(paramParcel, k, DriveId.CREATOR);
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject3;
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new ChangeResourceParentsRequest(i, (DriveId)localObject1, (List)localObject2, localArrayList);
  }
  
  public ChangeResourceParentsRequest[] a(int paramInt)
  {
    return new ChangeResourceParentsRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */