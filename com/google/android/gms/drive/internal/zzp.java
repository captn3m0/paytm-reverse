package com.google.android.gms.drive.internal;

import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public final class zzp
  extends Metadata
{
  private final MetadataBundle a;
  
  public zzp(MetadataBundle paramMetadataBundle)
  {
    this.a = paramMetadataBundle;
  }
  
  public <T> T a(MetadataField<T> paramMetadataField)
  {
    return (T)this.a.a(paramMetadataField);
  }
  
  public Metadata c()
  {
    return new zzp(this.a.b());
  }
  
  public String toString()
  {
    return "Metadata [mImpl=" + this.a + "]";
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */