package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.WriteAwareParcelable;

public class OnListEntriesResponse
  extends WriteAwareParcelable
  implements SafeParcelable
{
  public static final Parcelable.Creator<OnListEntriesResponse> CREATOR = new zzbc();
  final int a;
  final DataHolder b;
  final boolean c;
  
  OnListEntriesResponse(int paramInt, DataHolder paramDataHolder, boolean paramBoolean)
  {
    this.a = paramInt;
    this.b = paramDataHolder;
    this.c = paramBoolean;
  }
  
  protected void a(Parcel paramParcel, int paramInt)
  {
    zzbc.a(this, paramParcel, paramInt);
  }
  
  public DataHolder b()
  {
    return this.b;
  }
  
  public boolean c()
  {
    return this.c;
  }
  
  public int describeContents()
  {
    return 0;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/OnListEntriesResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */