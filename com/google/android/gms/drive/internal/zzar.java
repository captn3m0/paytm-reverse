package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.DriveId;
import java.util.ArrayList;

public class zzar
  implements Parcelable.Creator<LoadRealtimeRequest>
{
  static void a(LoadRealtimeRequest paramLoadRealtimeRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramLoadRealtimeRequest.a);
    zzb.a(paramParcel, 2, paramLoadRealtimeRequest.b, paramInt, false);
    zzb.a(paramParcel, 3, paramLoadRealtimeRequest.c);
    zzb.b(paramParcel, 4, paramLoadRealtimeRequest.d, false);
    zzb.a(paramParcel, 5, paramLoadRealtimeRequest.e);
    zzb.a(paramParcel, 6, paramLoadRealtimeRequest.f, paramInt, false);
    zzb.a(paramParcel, 7, paramLoadRealtimeRequest.g, false);
    zzb.a(paramParcel, i);
  }
  
  public LoadRealtimeRequest a(Parcel paramParcel)
  {
    boolean bool1 = false;
    String str = null;
    int j = zza.b(paramParcel);
    DataHolder localDataHolder = null;
    ArrayList localArrayList = null;
    boolean bool2 = false;
    DriveId localDriveId = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localDriveId = (DriveId)zza.a(paramParcel, k, DriveId.CREATOR);
        break;
      case 3: 
        bool2 = zza.c(paramParcel, k);
        break;
      case 4: 
        localArrayList = zza.D(paramParcel, k);
        break;
      case 5: 
        bool1 = zza.c(paramParcel, k);
        break;
      case 6: 
        localDataHolder = (DataHolder)zza.a(paramParcel, k, DataHolder.CREATOR);
        break;
      case 7: 
        str = zza.p(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new LoadRealtimeRequest(i, localDriveId, bool2, localArrayList, bool1, localDataHolder, str);
  }
  
  public LoadRealtimeRequest[] a(int paramInt)
  {
    return new LoadRealtimeRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzar.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */