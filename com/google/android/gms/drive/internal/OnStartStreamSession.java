package com.google.android.gms.drive.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class OnStartStreamSession
  implements SafeParcelable
{
  public static final Parcelable.Creator<OnStartStreamSession> CREATOR = new zzbi();
  final int a;
  final ParcelFileDescriptor b;
  final IBinder c;
  final String d;
  
  OnStartStreamSession(int paramInt, ParcelFileDescriptor paramParcelFileDescriptor, IBinder paramIBinder, String paramString)
  {
    this.a = paramInt;
    this.b = paramParcelFileDescriptor;
    this.c = paramIBinder;
    this.d = paramString;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzbi.a(this, paramParcel, paramInt | 0x1);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/OnStartStreamSession.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */