package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.drive.zzi;

public class CloseContentsAndUpdateMetadataRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<CloseContentsAndUpdateMetadataRequest> CREATOR = new zzh();
  final int a;
  final DriveId b;
  final MetadataBundle c;
  final Contents d;
  final boolean e;
  final String f;
  final int g;
  final int h;
  final boolean i;
  final boolean j;
  
  CloseContentsAndUpdateMetadataRequest(int paramInt1, DriveId paramDriveId, MetadataBundle paramMetadataBundle, Contents paramContents, boolean paramBoolean1, String paramString, int paramInt2, int paramInt3, boolean paramBoolean2, boolean paramBoolean3)
  {
    this.a = paramInt1;
    this.b = paramDriveId;
    this.c = paramMetadataBundle;
    this.d = paramContents;
    this.e = paramBoolean1;
    this.f = paramString;
    this.g = paramInt2;
    this.h = paramInt3;
    this.i = paramBoolean2;
    this.j = paramBoolean3;
  }
  
  public CloseContentsAndUpdateMetadataRequest(DriveId paramDriveId, MetadataBundle paramMetadataBundle, int paramInt, boolean paramBoolean, zzi paramzzi)
  {
    this(1, paramDriveId, paramMetadataBundle, null, paramzzi.b(), paramzzi.a(), paramzzi.c(), paramInt, paramBoolean, paramzzi.d());
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzh.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */