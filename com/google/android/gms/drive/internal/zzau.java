package com.google.android.gms.drive.internal;

import com.google.android.gms.internal.zzsm;
import com.google.android.gms.internal.zzsn;
import com.google.android.gms.internal.zzso;
import com.google.android.gms.internal.zzsq;
import java.io.IOException;

public final class zzau
  extends zzso<zzau>
{
  public long a;
  public long b;
  
  public zzau()
  {
    a();
  }
  
  public zzau a()
  {
    this.a = -1L;
    this.b = -1L;
    this.r = null;
    this.S = -1;
    return this;
  }
  
  public zzau a(zzsm paramzzsm)
    throws IOException
  {
    for (;;)
    {
      int i = paramzzsm.a();
      switch (i)
      {
      default: 
        if (a(paramzzsm, i)) {}
        break;
      case 0: 
        return this;
      case 8: 
        this.a = paramzzsm.l();
        break;
      case 16: 
        this.b = paramzzsm.l();
      }
    }
  }
  
  public void a(zzsn paramzzsn)
    throws IOException
  {
    paramzzsn.c(1, this.a);
    paramzzsn.c(2, this.b);
    super.a(paramzzsn);
  }
  
  protected int b()
  {
    return super.b() + zzsn.e(1, this.a) + zzsn.e(2, this.b);
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1;
    if (paramObject == this) {
      bool1 = true;
    }
    do
    {
      do
      {
        do
        {
          do
          {
            return bool1;
            bool1 = bool2;
          } while (!(paramObject instanceof zzau));
          paramObject = (zzau)paramObject;
          bool1 = bool2;
        } while (this.a != ((zzau)paramObject).a);
        bool1 = bool2;
      } while (this.b != ((zzau)paramObject).b);
      if ((this.r != null) && (!this.r.b())) {
        break label91;
      }
      if (((zzau)paramObject).r == null) {
        break;
      }
      bool1 = bool2;
    } while (!((zzau)paramObject).r.b());
    return true;
    label91:
    return this.r.equals(((zzau)paramObject).r);
  }
  
  public int hashCode()
  {
    int j = getClass().getName().hashCode();
    int k = (int)(this.a ^ this.a >>> 32);
    int m = (int)(this.b ^ this.b >>> 32);
    if ((this.r == null) || (this.r.b())) {}
    for (int i = 0;; i = this.r.hashCode()) {
      return i + (((j + 527) * 31 + k) * 31 + m) * 31;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzau.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */