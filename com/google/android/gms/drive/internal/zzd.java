package com.google.android.gms.drive.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.ChangeSequenceNumber;
import com.google.android.gms.drive.realtime.internal.zzm;

public class zzd
  extends zzan.zza
{
  public void a()
    throws RemoteException
  {}
  
  public void a(Status paramStatus)
    throws RemoteException
  {}
  
  public void a(ChangeSequenceNumber paramChangeSequenceNumber)
    throws RemoteException
  {}
  
  public void a(GetPermissionsResponse paramGetPermissionsResponse)
    throws RemoteException
  {}
  
  public void a(OnChangesResponse paramOnChangesResponse)
    throws RemoteException
  {}
  
  public void a(OnContentsResponse paramOnContentsResponse)
    throws RemoteException
  {}
  
  public void a(OnDeviceUsagePreferenceResponse paramOnDeviceUsagePreferenceResponse)
    throws RemoteException
  {}
  
  public void a(OnDownloadProgressResponse paramOnDownloadProgressResponse)
    throws RemoteException
  {}
  
  public void a(OnDriveIdResponse paramOnDriveIdResponse)
    throws RemoteException
  {}
  
  public void a(OnFetchThumbnailResponse paramOnFetchThumbnailResponse)
    throws RemoteException
  {}
  
  public void a(OnListEntriesResponse paramOnListEntriesResponse)
    throws RemoteException
  {}
  
  public void a(OnListParentsResponse paramOnListParentsResponse)
    throws RemoteException
  {}
  
  public void a(OnLoadRealtimeResponse paramOnLoadRealtimeResponse, zzm paramzzm)
    throws RemoteException
  {}
  
  public void a(OnMetadataResponse paramOnMetadataResponse)
    throws RemoteException
  {}
  
  public void a(OnPinnedDownloadPreferencesResponse paramOnPinnedDownloadPreferencesResponse)
    throws RemoteException
  {}
  
  public void a(OnResourceIdSetResponse paramOnResourceIdSetResponse)
    throws RemoteException
  {}
  
  public void a(OnStartStreamSession paramOnStartStreamSession)
    throws RemoteException
  {}
  
  public void a(OnSyncMoreResponse paramOnSyncMoreResponse)
    throws RemoteException
  {}
  
  public void a(StringListResponse paramStringListResponse)
    throws RemoteException
  {}
  
  public void a(boolean paramBoolean)
    throws RemoteException
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */