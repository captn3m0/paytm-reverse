package com.google.android.gms.drive.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zzb;

public class zzbu
  extends zzd
{
  private final zza.zzb<Status> a;
  
  public zzbu(zza.zzb<Status> paramzzb)
  {
    this.a = paramzzb;
  }
  
  public void a()
    throws RemoteException
  {
    this.a.a(Status.a);
  }
  
  public void a(Status paramStatus)
    throws RemoteException
  {
    this.a.a(paramStatus);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzbu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */