package com.google.android.gms.drive.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zzb;
import com.google.android.gms.drive.DriveApi.MetadataBufferResult;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.drive.DriveResource.MetadataResult;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public class zzab
  implements DriveResource
{
  protected final DriveId a;
  
  public zzab(DriveId paramDriveId)
  {
    this.a = paramDriveId;
  }
  
  public DriveId a()
  {
    return this.a;
  }
  
  private static class zza
    extends zzd
  {
    private final zza.zzb<DriveApi.MetadataBufferResult> a;
    
    public zza(zza.zzb<DriveApi.MetadataBufferResult> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void a(Status paramStatus)
      throws RemoteException
    {
      this.a.a(new zzs.zzg(paramStatus, null, false));
    }
    
    public void a(OnListParentsResponse paramOnListParentsResponse)
      throws RemoteException
    {
      paramOnListParentsResponse = new MetadataBuffer(paramOnListParentsResponse.b());
      this.a.a(new zzs.zzg(Status.a, paramOnListParentsResponse, false));
    }
  }
  
  private static class zzb
    extends zzd
  {
    private final zza.zzb<DriveResource.MetadataResult> a;
    
    public zzb(zza.zzb<DriveResource.MetadataResult> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void a(Status paramStatus)
      throws RemoteException
    {
      this.a.a(new zzab.zzc(paramStatus, null));
    }
    
    public void a(OnMetadataResponse paramOnMetadataResponse)
      throws RemoteException
    {
      this.a.a(new zzab.zzc(Status.a, new zzp(paramOnMetadataResponse.a())));
    }
  }
  
  private static class zzc
    implements DriveResource.MetadataResult
  {
    private final Status a;
    private final Metadata b;
    
    public zzc(Status paramStatus, Metadata paramMetadata)
    {
      this.a = paramStatus;
      this.b = paramMetadata;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
  
  private abstract class zzd
    extends zzt<DriveResource.MetadataResult>
  {
    public DriveResource.MetadataResult a(Status paramStatus)
    {
      return new zzab.zzc(paramStatus, null);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzab.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */