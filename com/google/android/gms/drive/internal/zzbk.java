package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.DriveId;

public class zzbk
  implements Parcelable.Creator<OpenContentsRequest>
{
  static void a(OpenContentsRequest paramOpenContentsRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramOpenContentsRequest.a);
    zzb.a(paramParcel, 2, paramOpenContentsRequest.b, paramInt, false);
    zzb.a(paramParcel, 3, paramOpenContentsRequest.c);
    zzb.a(paramParcel, 4, paramOpenContentsRequest.d);
    zzb.a(paramParcel, i);
  }
  
  public OpenContentsRequest a(Parcel paramParcel)
  {
    int i = 0;
    int m = zza.b(paramParcel);
    DriveId localDriveId = null;
    int j = 0;
    int k = 0;
    if (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
      }
      for (;;)
      {
        break;
        k = zza.g(paramParcel, n);
        continue;
        localDriveId = (DriveId)zza.a(paramParcel, n, DriveId.CREATOR);
        continue;
        j = zza.g(paramParcel, n);
        continue;
        i = zza.g(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new OpenContentsRequest(k, localDriveId, j, i);
  }
  
  public OpenContentsRequest[] a(int paramInt)
  {
    return new OpenContentsRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzbk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */