package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.DriveId;

public class zzcb
  implements Parcelable.Creator<UpdatePermissionRequest>
{
  static void a(UpdatePermissionRequest paramUpdatePermissionRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramUpdatePermissionRequest.a);
    zzb.a(paramParcel, 2, paramUpdatePermissionRequest.b, paramInt, false);
    zzb.a(paramParcel, 3, paramUpdatePermissionRequest.c, false);
    zzb.a(paramParcel, 4, paramUpdatePermissionRequest.d);
    zzb.a(paramParcel, 5, paramUpdatePermissionRequest.e);
    zzb.a(paramParcel, 6, paramUpdatePermissionRequest.f, false);
    zzb.a(paramParcel, i);
  }
  
  public UpdatePermissionRequest a(Parcel paramParcel)
  {
    String str1 = null;
    boolean bool = false;
    int k = zza.b(paramParcel);
    int i = 0;
    String str2 = null;
    DriveId localDriveId = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        localDriveId = (DriveId)zza.a(paramParcel, m, DriveId.CREATOR);
        break;
      case 3: 
        str2 = zza.p(paramParcel, m);
        break;
      case 4: 
        i = zza.g(paramParcel, m);
        break;
      case 5: 
        bool = zza.c(paramParcel, m);
        break;
      case 6: 
        str1 = zza.p(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new UpdatePermissionRequest(j, localDriveId, str2, i, bool, str1);
  }
  
  public UpdatePermissionRequest[] a(int paramInt)
  {
    return new UpdatePermissionRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzcb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */