package com.google.android.gms.drive.internal;

import android.annotation.SuppressLint;
import android.os.RemoteException;
import com.google.android.gms.common.api.BooleanResult;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zzb;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveApi.DriveContentsResult;
import com.google.android.gms.drive.DriveApi.DriveIdResult;
import com.google.android.gms.drive.DriveApi.MetadataBufferResult;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.MetadataBuffer;

public class zzs
  implements DriveApi
{
  private static class zza
    extends zzd
  {
    private final zza.zzb<DriveApi.DriveContentsResult> a;
    
    public zza(zza.zzb<DriveApi.DriveContentsResult> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void a(Status paramStatus)
      throws RemoteException
    {
      this.a.a(new zzs.zzb(paramStatus, null));
    }
    
    public void a(OnContentsResponse paramOnContentsResponse)
      throws RemoteException
    {
      this.a.a(new zzs.zzb(Status.a, new zzv(paramOnContentsResponse.a())));
    }
  }
  
  static class zzb
    implements Releasable, DriveApi.DriveContentsResult
  {
    private final Status a;
    private final DriveContents b;
    
    public zzb(Status paramStatus, DriveContents paramDriveContents)
    {
      this.a = paramStatus;
      this.b = paramDriveContents;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
    
    public void release()
    {
      if (this.b != null) {
        this.b.a();
      }
    }
  }
  
  static abstract class zzc
    extends zzt<DriveApi.DriveContentsResult>
  {
    public DriveApi.DriveContentsResult a(Status paramStatus)
    {
      return new zzs.zzb(paramStatus, null);
    }
  }
  
  static class zzd
    extends zzd
  {
    private final zza.zzb<DriveApi.DriveIdResult> a;
    
    public zzd(zza.zzb<DriveApi.DriveIdResult> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void a(Status paramStatus)
      throws RemoteException
    {
      this.a.a(new zzs.zze(paramStatus, null));
    }
    
    public void a(OnDriveIdResponse paramOnDriveIdResponse)
      throws RemoteException
    {
      this.a.a(new zzs.zze(Status.a, paramOnDriveIdResponse.a()));
    }
    
    public void a(OnMetadataResponse paramOnMetadataResponse)
      throws RemoteException
    {
      this.a.a(new zzs.zze(Status.a, new zzp(paramOnMetadataResponse.a()).b()));
    }
  }
  
  private static class zze
    implements DriveApi.DriveIdResult
  {
    private final Status a;
    private final DriveId b;
    
    public zze(Status paramStatus, DriveId paramDriveId)
    {
      this.a = paramStatus;
      this.b = paramDriveId;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
  
  static abstract class zzf
    extends zzt<DriveApi.DriveIdResult>
  {
    public DriveApi.DriveIdResult a(Status paramStatus)
    {
      return new zzs.zze(paramStatus, null);
    }
  }
  
  static class zzg
    implements DriveApi.MetadataBufferResult
  {
    private final Status a;
    private final MetadataBuffer b;
    private final boolean c;
    
    public zzg(Status paramStatus, MetadataBuffer paramMetadataBuffer, boolean paramBoolean)
    {
      this.a = paramStatus;
      this.b = paramMetadataBuffer;
      this.c = paramBoolean;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
    
    public void release()
    {
      if (this.b != null) {
        this.b.release();
      }
    }
  }
  
  static abstract class zzh
    extends zzt<DriveApi.MetadataBufferResult>
  {
    public DriveApi.MetadataBufferResult a(Status paramStatus)
    {
      return new zzs.zzg(paramStatus, null, false);
    }
  }
  
  private static class zzi
    extends zzd
  {
    private final zza.zzb<DriveApi.MetadataBufferResult> a;
    
    public zzi(zza.zzb<DriveApi.MetadataBufferResult> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void a(Status paramStatus)
      throws RemoteException
    {
      this.a.a(new zzs.zzg(paramStatus, null, false));
    }
    
    public void a(OnListEntriesResponse paramOnListEntriesResponse)
      throws RemoteException
    {
      MetadataBuffer localMetadataBuffer = new MetadataBuffer(paramOnListEntriesResponse.b());
      this.a.a(new zzs.zzg(Status.a, localMetadataBuffer, paramOnListEntriesResponse.c()));
    }
  }
  
  @SuppressLint({"MissingRemoteException"})
  static class zzj
    extends zzt.zza
  {
    protected void a(zzu paramzzu) {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */