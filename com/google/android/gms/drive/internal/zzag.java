package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzag
  implements Parcelable.Creator<FileUploadPreferencesImpl>
{
  static void a(FileUploadPreferencesImpl paramFileUploadPreferencesImpl, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramFileUploadPreferencesImpl.a);
    zzb.a(paramParcel, 2, paramFileUploadPreferencesImpl.b);
    zzb.a(paramParcel, 3, paramFileUploadPreferencesImpl.c);
    zzb.a(paramParcel, 4, paramFileUploadPreferencesImpl.d);
    zzb.a(paramParcel, paramInt);
  }
  
  public FileUploadPreferencesImpl a(Parcel paramParcel)
  {
    boolean bool = false;
    int m = zza.b(paramParcel);
    int k = 0;
    int j = 0;
    int i = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        i = zza.g(paramParcel, n);
        break;
      case 2: 
        j = zza.g(paramParcel, n);
        break;
      case 3: 
        k = zza.g(paramParcel, n);
        break;
      case 4: 
        bool = zza.c(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new FileUploadPreferencesImpl(i, j, k, bool);
  }
  
  public FileUploadPreferencesImpl[] a(int paramInt)
  {
    return new FileUploadPreferencesImpl[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzag.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */