package com.google.android.gms.drive.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.zzq;
import com.google.android.gms.common.api.internal.zzq.zzb;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFile.DownloadProgressListener;
import com.google.android.gms.drive.DriveId;

public class zzw
  extends zzab
  implements DriveFile
{
  public zzw(DriveId paramDriveId)
  {
    super(paramDriveId);
  }
  
  private static class zza
    implements DriveFile.DownloadProgressListener
  {
    private final zzq<DriveFile.DownloadProgressListener> a;
    
    public void a(final long paramLong1, long paramLong2)
    {
      this.a.a(new zzq.zzb()
      {
        public void a() {}
        
        public void a(DriveFile.DownloadProgressListener paramAnonymousDownloadProgressListener)
        {
          paramAnonymousDownloadProgressListener.a(paramLong1, this.b);
        }
      });
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */