package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.List;

public class StringListResponse
  implements SafeParcelable
{
  public static final Parcelable.Creator<StringListResponse> CREATOR = new zzbw();
  private final int a;
  private final List<String> b;
  
  StringListResponse(int paramInt, List<String> paramList)
  {
    this.a = paramInt;
    this.b = paramList;
  }
  
  public int a()
  {
    return this.a;
  }
  
  public List<String> b()
  {
    return this.b;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzbw.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/StringListResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */