package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.Permission;
import java.util.ArrayList;

public class zzal
  implements Parcelable.Creator<GetPermissionsResponse>
{
  static void a(GetPermissionsResponse paramGetPermissionsResponse, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramGetPermissionsResponse.a);
    zzb.c(paramParcel, 2, paramGetPermissionsResponse.b, false);
    zzb.a(paramParcel, 3, paramGetPermissionsResponse.c);
    zzb.a(paramParcel, paramInt);
  }
  
  public GetPermissionsResponse a(Parcel paramParcel)
  {
    int j = 0;
    int k = zza.b(paramParcel);
    ArrayList localArrayList = null;
    int i = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        i = zza.g(paramParcel, m);
        break;
      case 2: 
        localArrayList = zza.c(paramParcel, m, Permission.CREATOR);
        break;
      case 3: 
        j = zza.g(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new GetPermissionsResponse(i, localArrayList, j);
  }
  
  public GetPermissionsResponse[] a(int paramInt)
  {
    return new GetPermissionsResponse[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */