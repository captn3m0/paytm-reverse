package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class SetPinnedDownloadPreferencesRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<SetPinnedDownloadPreferencesRequest> CREATOR = new zzbs();
  final int a;
  final ParcelableTransferPreferences b;
  
  SetPinnedDownloadPreferencesRequest(int paramInt, ParcelableTransferPreferences paramParcelableTransferPreferences)
  {
    this.a = paramInt;
    this.b = paramParcelableTransferPreferences;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzbs.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/SetPinnedDownloadPreferencesRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */