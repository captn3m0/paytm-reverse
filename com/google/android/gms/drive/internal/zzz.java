package com.google.android.gms.drive.internal;

import android.content.Context;
import com.google.android.gms.common.internal.zzo;

public final class zzz
{
  private static final zzo a = new zzo("GmsDrive");
  
  public static void a(Context paramContext, String paramString1, String paramString2)
  {
    a(paramContext, paramString1, paramString2, new Throwable());
  }
  
  public static void a(Context paramContext, String paramString1, String paramString2, Throwable paramThrowable)
  {
    a.a(paramContext, paramString1, paramString2, paramThrowable);
  }
  
  public static void a(String paramString1, String paramString2)
  {
    a.a(paramString1, paramString2);
  }
  
  public static void a(String paramString1, Throwable paramThrowable, String paramString2)
  {
    a.b(paramString1, paramString2, paramThrowable);
  }
  
  public static void b(String paramString1, String paramString2)
  {
    a.b(paramString1, paramString2);
  }
  
  public static void c(String paramString1, String paramString2)
  {
    a.c(paramString1, paramString2);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */