package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.DriveId;

public class zzby
  implements Parcelable.Creator<UnsubscribeResourceRequest>
{
  static void a(UnsubscribeResourceRequest paramUnsubscribeResourceRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramUnsubscribeResourceRequest.a);
    zzb.a(paramParcel, 2, paramUnsubscribeResourceRequest.b, paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public UnsubscribeResourceRequest a(Parcel paramParcel)
  {
    int j = zza.b(paramParcel);
    int i = 0;
    DriveId localDriveId = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localDriveId = (DriveId)zza.a(paramParcel, k, DriveId.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new UnsubscribeResourceRequest(i, localDriveId);
  }
  
  public UnsubscribeResourceRequest[] a(int paramInt)
  {
    return new UnsubscribeResourceRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzby.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */