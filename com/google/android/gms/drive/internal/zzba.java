package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.events.ChangeEvent;
import com.google.android.gms.drive.events.ChangesAvailableEvent;
import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.drive.events.QueryResultEventParcelable;
import com.google.android.gms.drive.events.TransferProgressEvent;
import com.google.android.gms.drive.events.TransferStateEvent;

public class zzba
  implements Parcelable.Creator<OnEventResponse>
{
  static void a(OnEventResponse paramOnEventResponse, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramOnEventResponse.a);
    zzb.a(paramParcel, 2, paramOnEventResponse.b);
    zzb.a(paramParcel, 3, paramOnEventResponse.c, paramInt, false);
    zzb.a(paramParcel, 5, paramOnEventResponse.d, paramInt, false);
    zzb.a(paramParcel, 6, paramOnEventResponse.e, paramInt, false);
    zzb.a(paramParcel, 7, paramOnEventResponse.f, paramInt, false);
    zzb.a(paramParcel, 9, paramOnEventResponse.g, paramInt, false);
    zzb.a(paramParcel, 10, paramOnEventResponse.h, paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public OnEventResponse a(Parcel paramParcel)
  {
    int i = 0;
    TransferProgressEvent localTransferProgressEvent = null;
    int k = zza.b(paramParcel);
    TransferStateEvent localTransferStateEvent = null;
    ChangesAvailableEvent localChangesAvailableEvent = null;
    QueryResultEventParcelable localQueryResultEventParcelable = null;
    CompletionEvent localCompletionEvent = null;
    ChangeEvent localChangeEvent = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      case 4: 
      case 8: 
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        i = zza.g(paramParcel, m);
        break;
      case 3: 
        localChangeEvent = (ChangeEvent)zza.a(paramParcel, m, ChangeEvent.CREATOR);
        break;
      case 5: 
        localCompletionEvent = (CompletionEvent)zza.a(paramParcel, m, CompletionEvent.CREATOR);
        break;
      case 6: 
        localQueryResultEventParcelable = (QueryResultEventParcelable)zza.a(paramParcel, m, QueryResultEventParcelable.CREATOR);
        break;
      case 7: 
        localChangesAvailableEvent = (ChangesAvailableEvent)zza.a(paramParcel, m, ChangesAvailableEvent.CREATOR);
        break;
      case 9: 
        localTransferStateEvent = (TransferStateEvent)zza.a(paramParcel, m, TransferStateEvent.CREATOR);
        break;
      case 10: 
        localTransferProgressEvent = (TransferProgressEvent)zza.a(paramParcel, m, TransferProgressEvent.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new OnEventResponse(j, i, localChangeEvent, localCompletionEvent, localQueryResultEventParcelable, localChangesAvailableEvent, localTransferStateEvent, localTransferProgressEvent);
  }
  
  public OnEventResponse[] a(int paramInt)
  {
    return new OnEventResponse[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzba.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */