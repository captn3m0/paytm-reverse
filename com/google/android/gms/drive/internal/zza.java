package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.events.ChangesAvailableOptions;
import com.google.android.gms.drive.events.TransferProgressOptions;
import com.google.android.gms.drive.events.TransferStateOptions;

public class zza
  implements Parcelable.Creator<AddEventListenerRequest>
{
  static void a(AddEventListenerRequest paramAddEventListenerRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramAddEventListenerRequest.a);
    zzb.a(paramParcel, 2, paramAddEventListenerRequest.b, paramInt, false);
    zzb.a(paramParcel, 3, paramAddEventListenerRequest.c);
    zzb.a(paramParcel, 4, paramAddEventListenerRequest.d, paramInt, false);
    zzb.a(paramParcel, 5, paramAddEventListenerRequest.e, paramInt, false);
    zzb.a(paramParcel, 6, paramAddEventListenerRequest.f, paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public AddEventListenerRequest a(Parcel paramParcel)
  {
    int i = 0;
    TransferProgressOptions localTransferProgressOptions = null;
    int k = com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel);
    TransferStateOptions localTransferStateOptions = null;
    ChangesAvailableOptions localChangesAvailableOptions = null;
    DriveId localDriveId = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = com.google.android.gms.common.internal.safeparcel.zza.a(paramParcel);
      switch (com.google.android.gms.common.internal.safeparcel.zza.a(m))
      {
      default: 
        com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel, m);
        break;
      case 1: 
        j = com.google.android.gms.common.internal.safeparcel.zza.g(paramParcel, m);
        break;
      case 2: 
        localDriveId = (DriveId)com.google.android.gms.common.internal.safeparcel.zza.a(paramParcel, m, DriveId.CREATOR);
        break;
      case 3: 
        i = com.google.android.gms.common.internal.safeparcel.zza.g(paramParcel, m);
        break;
      case 4: 
        localChangesAvailableOptions = (ChangesAvailableOptions)com.google.android.gms.common.internal.safeparcel.zza.a(paramParcel, m, ChangesAvailableOptions.CREATOR);
        break;
      case 5: 
        localTransferStateOptions = (TransferStateOptions)com.google.android.gms.common.internal.safeparcel.zza.a(paramParcel, m, TransferStateOptions.CREATOR);
        break;
      case 6: 
        localTransferProgressOptions = (TransferProgressOptions)com.google.android.gms.common.internal.safeparcel.zza.a(paramParcel, m, TransferProgressOptions.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new AddEventListenerRequest(j, localDriveId, i, localChangesAvailableOptions, localTransferStateOptions, localTransferProgressOptions);
  }
  
  public AddEventListenerRequest[] a(int paramInt)
  {
    return new AddEventListenerRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */