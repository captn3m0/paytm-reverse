package com.google.android.gms.drive.internal;

import com.google.android.gms.internal.zzsm;
import com.google.android.gms.internal.zzsn;
import com.google.android.gms.internal.zzso;
import com.google.android.gms.internal.zzsq;
import java.io.IOException;

public final class zzas
  extends zzso<zzas>
{
  public int a;
  public long b;
  public long c;
  public long d;
  
  public zzas()
  {
    a();
  }
  
  public zzas a()
  {
    this.a = 1;
    this.b = -1L;
    this.c = -1L;
    this.d = -1L;
    this.r = null;
    this.S = -1;
    return this;
  }
  
  public zzas a(zzsm paramzzsm)
    throws IOException
  {
    for (;;)
    {
      int i = paramzzsm.a();
      switch (i)
      {
      default: 
        if (a(paramzzsm, i)) {}
        break;
      case 0: 
        return this;
      case 8: 
        this.a = paramzzsm.g();
        break;
      case 16: 
        this.b = paramzzsm.l();
        break;
      case 24: 
        this.c = paramzzsm.l();
        break;
      case 32: 
        this.d = paramzzsm.l();
      }
    }
  }
  
  public void a(zzsn paramzzsn)
    throws IOException
  {
    paramzzsn.a(1, this.a);
    paramzzsn.c(2, this.b);
    paramzzsn.c(3, this.c);
    paramzzsn.c(4, this.d);
    super.a(paramzzsn);
  }
  
  protected int b()
  {
    return super.b() + zzsn.c(1, this.a) + zzsn.e(2, this.b) + zzsn.e(3, this.c) + zzsn.e(4, this.d);
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1;
    if (paramObject == this) {
      bool1 = true;
    }
    do
    {
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                return bool1;
                bool1 = bool2;
              } while (!(paramObject instanceof zzas));
              paramObject = (zzas)paramObject;
              bool1 = bool2;
            } while (this.a != ((zzas)paramObject).a);
            bool1 = bool2;
          } while (this.b != ((zzas)paramObject).b);
          bool1 = bool2;
        } while (this.c != ((zzas)paramObject).c);
        bool1 = bool2;
      } while (this.d != ((zzas)paramObject).d);
      if ((this.r != null) && (!this.r.b())) {
        break label118;
      }
      if (((zzas)paramObject).r == null) {
        break;
      }
      bool1 = bool2;
    } while (!((zzas)paramObject).r.b());
    return true;
    label118:
    return this.r.equals(((zzas)paramObject).r);
  }
  
  public int hashCode()
  {
    int j = getClass().getName().hashCode();
    int k = this.a;
    int m = (int)(this.b ^ this.b >>> 32);
    int n = (int)(this.c ^ this.c >>> 32);
    int i1 = (int)(this.d ^ this.d >>> 32);
    if ((this.r == null) || (this.r.b())) {}
    for (int i = 0;; i = this.r.hashCode()) {
      return i + (((((j + 527) * 31 + k) * 31 + m) * 31 + n) * 31 + i1) * 31;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzas.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */