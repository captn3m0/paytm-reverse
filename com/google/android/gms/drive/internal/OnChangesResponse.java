package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.ChangeSequenceNumber;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.WriteAwareParcelable;
import java.util.List;

public class OnChangesResponse
  extends WriteAwareParcelable
  implements SafeParcelable
{
  public static final Parcelable.Creator<OnChangesResponse> CREATOR = new zzav();
  final int a;
  final DataHolder b;
  final List<DriveId> c;
  final ChangeSequenceNumber d;
  final boolean e;
  
  OnChangesResponse(int paramInt, DataHolder paramDataHolder, List<DriveId> paramList, ChangeSequenceNumber paramChangeSequenceNumber, boolean paramBoolean)
  {
    this.a = paramInt;
    this.b = paramDataHolder;
    this.c = paramList;
    this.d = paramChangeSequenceNumber;
    this.e = paramBoolean;
  }
  
  protected void a(Parcel paramParcel, int paramInt)
  {
    zzav.a(this, paramParcel, paramInt | 0x1);
  }
  
  public int describeContents()
  {
    return 0;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/OnChangesResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */