package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.WriteAwareParcelable;

public class OnListParentsResponse
  extends WriteAwareParcelable
  implements SafeParcelable
{
  public static final Parcelable.Creator<OnListParentsResponse> CREATOR = new zzbd();
  final int a;
  final DataHolder b;
  
  OnListParentsResponse(int paramInt, DataHolder paramDataHolder)
  {
    this.a = paramInt;
    this.b = paramDataHolder;
  }
  
  protected void a(Parcel paramParcel, int paramInt)
  {
    zzbd.a(this, paramParcel, paramInt);
  }
  
  public DataHolder b()
  {
    return this.b;
  }
  
  public int describeContents()
  {
    return 0;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/OnListParentsResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */