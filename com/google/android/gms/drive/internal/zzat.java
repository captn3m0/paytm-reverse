package com.google.android.gms.drive.internal;

import com.google.android.gms.internal.zzsm;
import com.google.android.gms.internal.zzsn;
import com.google.android.gms.internal.zzso;
import com.google.android.gms.internal.zzsq;
import java.io.IOException;

public final class zzat
  extends zzso<zzat>
{
  public int a;
  public String b;
  public long c;
  public long d;
  public int e;
  
  public zzat()
  {
    a();
  }
  
  public zzat a()
  {
    this.a = 1;
    this.b = "";
    this.c = -1L;
    this.d = -1L;
    this.e = -1;
    this.r = null;
    this.S = -1;
    return this;
  }
  
  public zzat a(zzsm paramzzsm)
    throws IOException
  {
    for (;;)
    {
      int i = paramzzsm.a();
      switch (i)
      {
      default: 
        if (a(paramzzsm, i)) {}
        break;
      case 0: 
        return this;
      case 8: 
        this.a = paramzzsm.g();
        break;
      case 18: 
        this.b = paramzzsm.i();
        break;
      case 24: 
        this.c = paramzzsm.l();
        break;
      case 32: 
        this.d = paramzzsm.l();
        break;
      case 40: 
        this.e = paramzzsm.g();
      }
    }
  }
  
  public void a(zzsn paramzzsn)
    throws IOException
  {
    paramzzsn.a(1, this.a);
    paramzzsn.a(2, this.b);
    paramzzsn.c(3, this.c);
    paramzzsn.c(4, this.d);
    if (this.e != -1) {
      paramzzsn.a(5, this.e);
    }
    super.a(paramzzsn);
  }
  
  protected int b()
  {
    int j = super.b() + zzsn.c(1, this.a) + zzsn.b(2, this.b) + zzsn.e(3, this.c) + zzsn.e(4, this.d);
    int i = j;
    if (this.e != -1) {
      i = j + zzsn.c(5, this.e);
    }
    return i;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1;
    if (paramObject == this) {
      bool1 = true;
    }
    do
    {
      do
      {
        do
        {
          return bool1;
          bool1 = bool2;
        } while (!(paramObject instanceof zzat));
        paramObject = (zzat)paramObject;
        bool1 = bool2;
      } while (this.a != ((zzat)paramObject).a);
      if (this.b != null) {
        break;
      }
      bool1 = bool2;
    } while (((zzat)paramObject).b != null);
    while (this.b.equals(((zzat)paramObject).b))
    {
      bool1 = bool2;
      if (this.c != ((zzat)paramObject).c) {
        break;
      }
      bool1 = bool2;
      if (this.d != ((zzat)paramObject).d) {
        break;
      }
      bool1 = bool2;
      if (this.e != ((zzat)paramObject).e) {
        break;
      }
      if ((this.r != null) && (!this.r.b())) {
        break label149;
      }
      if (((zzat)paramObject).r != null)
      {
        bool1 = bool2;
        if (!((zzat)paramObject).r.b()) {
          break;
        }
      }
      return true;
    }
    return false;
    label149:
    return this.r.equals(((zzat)paramObject).r);
  }
  
  public int hashCode()
  {
    int k = 0;
    int m = getClass().getName().hashCode();
    int n = this.a;
    int i;
    int i1;
    int i2;
    int i3;
    if (this.b == null)
    {
      i = 0;
      i1 = (int)(this.c ^ this.c >>> 32);
      i2 = (int)(this.d ^ this.d >>> 32);
      i3 = this.e;
      j = k;
      if (this.r != null) {
        if (!this.r.b()) {
          break label138;
        }
      }
    }
    label138:
    for (int j = k;; j = this.r.hashCode())
    {
      return ((((i + ((m + 527) * 31 + n) * 31) * 31 + i1) * 31 + i2) * 31 + i3) * 31 + j;
      i = this.b.hashCode();
      break;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */