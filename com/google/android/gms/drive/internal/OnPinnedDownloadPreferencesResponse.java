package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class OnPinnedDownloadPreferencesResponse
  implements SafeParcelable
{
  public static final Parcelable.Creator<OnPinnedDownloadPreferencesResponse> CREATOR = new zzbf();
  final int a;
  final ParcelableTransferPreferences b;
  
  OnPinnedDownloadPreferencesResponse(int paramInt, ParcelableTransferPreferences paramParcelableTransferPreferences)
  {
    this.a = paramInt;
    this.b = paramParcelableTransferPreferences;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzbf.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/OnPinnedDownloadPreferencesResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */