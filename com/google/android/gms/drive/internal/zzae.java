package com.google.android.gms.drive.internal;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.util.Pair;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.events.ChangeEvent;
import com.google.android.gms.drive.events.ChangeListener;
import com.google.android.gms.drive.events.ChangesAvailableEvent;
import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.drive.events.CompletionListener;
import com.google.android.gms.drive.events.DriveEvent;
import com.google.android.gms.drive.events.QueryResultEventParcelable;
import com.google.android.gms.drive.events.TransferProgressEvent;
import com.google.android.gms.drive.events.internal.zza;
import com.google.android.gms.drive.events.zzc;
import com.google.android.gms.drive.events.zzf;
import com.google.android.gms.drive.events.zzh;
import com.google.android.gms.drive.events.zzi;
import com.google.android.gms.drive.events.zzk;
import com.google.android.gms.drive.events.zzm;
import java.util.List;

public class zzae
  extends zzao.zza
{
  private final int a;
  private final zzf b;
  private final zza c;
  private final List<Integer> d;
  
  public void a(OnEventResponse paramOnEventResponse)
    throws RemoteException
  {
    paramOnEventResponse = paramOnEventResponse.a();
    if (this.a == paramOnEventResponse.a()) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.a(bool);
      zzx.a(this.d.contains(Integer.valueOf(paramOnEventResponse.a())));
      this.c.a(this.b, paramOnEventResponse);
      return;
    }
  }
  
  private static class zza
    extends Handler
  {
    private final Context a;
    
    private static void a(zzm paramzzm, QueryResultEventParcelable paramQueryResultEventParcelable)
    {
      DataHolder localDataHolder = paramQueryResultEventParcelable.b();
      if (localDataHolder != null) {
        paramzzm.a(new zzk() {});
      }
      if (paramQueryResultEventParcelable.c()) {
        paramzzm.a(paramQueryResultEventParcelable.d());
      }
    }
    
    public void a(zzf paramzzf, DriveEvent paramDriveEvent)
    {
      sendMessage(obtainMessage(1, new Pair(paramzzf, paramDriveEvent)));
    }
    
    public void handleMessage(Message paramMessage)
    {
      switch (paramMessage.what)
      {
      default: 
        zzz.a(this.a, "EventCallback", "Don't know how to handle this event");
        return;
      }
      Object localObject = (Pair)paramMessage.obj;
      paramMessage = (zzf)((Pair)localObject).first;
      localObject = (DriveEvent)((Pair)localObject).second;
      switch (((DriveEvent)localObject).a())
      {
      case 5: 
      case 6: 
      case 7: 
      default: 
        zzz.b("EventCallback", "Unexpected event: " + localObject);
        return;
      case 1: 
        ((ChangeListener)paramMessage).a((ChangeEvent)localObject);
        return;
      case 2: 
        ((CompletionListener)paramMessage).a((CompletionEvent)localObject);
        return;
      case 3: 
        a((zzm)paramMessage, (QueryResultEventParcelable)localObject);
        return;
      case 4: 
        ((zzc)paramMessage).a((ChangesAvailableEvent)localObject);
        return;
      }
      localObject = new zza(((TransferProgressEvent)localObject).b());
      ((zzi)paramMessage).a((zzh)localObject);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzae.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */