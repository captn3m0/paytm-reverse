package com.google.android.gms.drive.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zzb;
import com.google.android.gms.drive.DrivePreferencesApi;
import com.google.android.gms.drive.DrivePreferencesApi.FileUploadPreferencesResult;
import com.google.android.gms.drive.FileUploadPreferences;

public class zzaa
  implements DrivePreferencesApi
{
  private class zza
    extends zzd
  {
    private final zza.zzb<DrivePreferencesApi.FileUploadPreferencesResult> b;
    
    private zza()
    {
      zza.zzb localzzb;
      this.b = localzzb;
    }
    
    public void a(Status paramStatus)
      throws RemoteException
    {
      this.b.a(new zzaa.zzb(zzaa.this, paramStatus, null, null));
    }
    
    public void a(OnDeviceUsagePreferenceResponse paramOnDeviceUsagePreferenceResponse)
      throws RemoteException
    {
      this.b.a(new zzaa.zzb(zzaa.this, Status.a, paramOnDeviceUsagePreferenceResponse.a(), null));
    }
  }
  
  private class zzb
    implements DrivePreferencesApi.FileUploadPreferencesResult
  {
    private final Status b;
    private final FileUploadPreferences c;
    
    private zzb(Status paramStatus, FileUploadPreferences paramFileUploadPreferences)
    {
      this.b = paramStatus;
      this.c = paramFileUploadPreferences;
    }
    
    public Status getStatus()
    {
      return this.b;
    }
  }
  
  private abstract class zzc
    extends zzt<DrivePreferencesApi.FileUploadPreferencesResult>
  {
    protected DrivePreferencesApi.FileUploadPreferencesResult a(Status paramStatus)
    {
      return new zzaa.zzb(this.b, paramStatus, null, null);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzaa.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */