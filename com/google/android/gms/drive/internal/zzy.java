package com.google.android.gms.drive.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zzb;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveFolder.DriveFileResult;
import com.google.android.gms.drive.DriveFolder.DriveFolderResult;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public class zzy
  extends zzab
  implements DriveFolder
{
  public zzy(DriveId paramDriveId)
  {
    super(paramDriveId);
  }
  
  private static class zza
    extends zzd
  {
    private final zza.zzb<DriveFolder.DriveFileResult> a;
    
    public zza(zza.zzb<DriveFolder.DriveFileResult> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void a(Status paramStatus)
      throws RemoteException
    {
      this.a.a(new zzy.zzc(paramStatus, null));
    }
    
    public void a(OnDriveIdResponse paramOnDriveIdResponse)
      throws RemoteException
    {
      this.a.a(new zzy.zzc(Status.a, new zzw(paramOnDriveIdResponse.a())));
    }
  }
  
  private static class zzb
    extends zzd
  {
    private final zza.zzb<DriveFolder.DriveFolderResult> a;
    
    public zzb(zza.zzb<DriveFolder.DriveFolderResult> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void a(Status paramStatus)
      throws RemoteException
    {
      this.a.a(new zzy.zze(paramStatus, null));
    }
    
    public void a(OnDriveIdResponse paramOnDriveIdResponse)
      throws RemoteException
    {
      this.a.a(new zzy.zze(Status.a, new zzy(paramOnDriveIdResponse.a())));
    }
  }
  
  private static class zzc
    implements DriveFolder.DriveFileResult
  {
    private final Status a;
    private final DriveFile b;
    
    public zzc(Status paramStatus, DriveFile paramDriveFile)
    {
      this.a = paramStatus;
      this.b = paramDriveFile;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
  
  static abstract class zzd
    extends zzt<DriveFolder.DriveFileResult>
  {
    public DriveFolder.DriveFileResult a(Status paramStatus)
    {
      return new zzy.zzc(paramStatus, null);
    }
  }
  
  private static class zze
    implements DriveFolder.DriveFolderResult
  {
    private final Status a;
    private final DriveFolder b;
    
    public zze(Status paramStatus, DriveFolder paramDriveFolder)
    {
      this.a = paramStatus;
      this.b = paramDriveFolder;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
  
  static abstract class zzf
    extends zzt<DriveFolder.DriveFolderResult>
  {
    public DriveFolder.DriveFolderResult a(Status paramStatus)
    {
      return new zzy.zze(paramStatus, null);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */