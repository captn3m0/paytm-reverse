package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.DriveFileRange;
import java.util.ArrayList;

public class zzay
  implements Parcelable.Creator<OnDownloadProgressResponse>
{
  static void a(OnDownloadProgressResponse paramOnDownloadProgressResponse, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramOnDownloadProgressResponse.a);
    zzb.a(paramParcel, 2, paramOnDownloadProgressResponse.b);
    zzb.a(paramParcel, 3, paramOnDownloadProgressResponse.c);
    zzb.a(paramParcel, 4, paramOnDownloadProgressResponse.d);
    zzb.c(paramParcel, 5, paramOnDownloadProgressResponse.e, false);
    zzb.a(paramParcel, paramInt);
  }
  
  public OnDownloadProgressResponse a(Parcel paramParcel)
  {
    long l1 = 0L;
    int i = 0;
    int k = zza.b(paramParcel);
    ArrayList localArrayList = null;
    long l2 = 0L;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        l2 = zza.i(paramParcel, m);
        break;
      case 3: 
        l1 = zza.i(paramParcel, m);
        break;
      case 4: 
        i = zza.g(paramParcel, m);
        break;
      case 5: 
        localArrayList = zza.c(paramParcel, m, DriveFileRange.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new OnDownloadProgressResponse(j, l2, l1, i, localArrayList);
  }
  
  public OnDownloadProgressResponse[] a(int paramInt)
  {
    return new OnDownloadProgressResponse[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzay.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */