package com.google.android.gms.drive.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zzb;
import com.google.android.gms.drive.DriveApi.DriveContentsResult;
import com.google.android.gms.drive.DriveFile.DownloadProgressListener;

class zzbl
  extends zzd
{
  private final zza.zzb<DriveApi.DriveContentsResult> a;
  private final DriveFile.DownloadProgressListener b;
  
  zzbl(zza.zzb<DriveApi.DriveContentsResult> paramzzb, DriveFile.DownloadProgressListener paramDownloadProgressListener)
  {
    this.a = paramzzb;
    this.b = paramDownloadProgressListener;
  }
  
  public void a(Status paramStatus)
    throws RemoteException
  {
    this.a.a(new zzs.zzb(paramStatus, null));
  }
  
  public void a(OnContentsResponse paramOnContentsResponse)
    throws RemoteException
  {
    if (paramOnContentsResponse.b()) {}
    for (Status localStatus = new Status(-1);; localStatus = Status.a)
    {
      this.a.a(new zzs.zzb(localStatus, new zzv(paramOnContentsResponse.a())));
      return;
    }
  }
  
  public void a(OnDownloadProgressResponse paramOnDownloadProgressResponse)
    throws RemoteException
  {
    if (this.b != null) {
      this.b.a(paramOnDownloadProgressResponse.a(), paramOnDownloadProgressResponse.b());
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzbl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */