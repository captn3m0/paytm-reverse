package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.events.TransferProgressOptions;

public class zzbp
  implements Parcelable.Creator<RemoveEventListenerRequest>
{
  static void a(RemoveEventListenerRequest paramRemoveEventListenerRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramRemoveEventListenerRequest.a);
    zzb.a(paramParcel, 2, paramRemoveEventListenerRequest.b, paramInt, false);
    zzb.a(paramParcel, 3, paramRemoveEventListenerRequest.c);
    zzb.a(paramParcel, 4, paramRemoveEventListenerRequest.d, paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public RemoveEventListenerRequest a(Parcel paramParcel)
  {
    TransferProgressOptions localTransferProgressOptions = null;
    int j = 0;
    int m = zza.b(paramParcel);
    DriveId localDriveId = null;
    int i = 0;
    if (paramParcel.dataPosition() < m)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        k = j;
        j = i;
        i = k;
      }
      for (;;)
      {
        k = j;
        j = i;
        i = k;
        break;
        k = zza.g(paramParcel, k);
        i = j;
        j = k;
        continue;
        localDriveId = (DriveId)zza.a(paramParcel, k, DriveId.CREATOR);
        k = i;
        i = j;
        j = k;
        continue;
        k = zza.g(paramParcel, k);
        j = i;
        i = k;
        continue;
        localTransferProgressOptions = (TransferProgressOptions)zza.a(paramParcel, k, TransferProgressOptions.CREATOR);
        k = i;
        i = j;
        j = k;
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new RemoveEventListenerRequest(i, localDriveId, j, localTransferProgressOptions);
  }
  
  public RemoveEventListenerRequest[] a(int paramInt)
  {
    return new RemoveEventListenerRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzbp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */