package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public class zzm
  implements Parcelable.Creator<CreateFileIntentSenderRequest>
{
  static void a(CreateFileIntentSenderRequest paramCreateFileIntentSenderRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramCreateFileIntentSenderRequest.a);
    zzb.a(paramParcel, 2, paramCreateFileIntentSenderRequest.b, paramInt, false);
    zzb.a(paramParcel, 3, paramCreateFileIntentSenderRequest.c);
    zzb.a(paramParcel, 4, paramCreateFileIntentSenderRequest.d, false);
    zzb.a(paramParcel, 5, paramCreateFileIntentSenderRequest.e, paramInt, false);
    zzb.a(paramParcel, 6, paramCreateFileIntentSenderRequest.f, false);
    zzb.a(paramParcel, i);
  }
  
  public CreateFileIntentSenderRequest a(Parcel paramParcel)
  {
    int i = 0;
    Integer localInteger = null;
    int k = zza.b(paramParcel);
    DriveId localDriveId = null;
    String str = null;
    MetadataBundle localMetadataBundle = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        localMetadataBundle = (MetadataBundle)zza.a(paramParcel, m, MetadataBundle.CREATOR);
        break;
      case 3: 
        i = zza.g(paramParcel, m);
        break;
      case 4: 
        str = zza.p(paramParcel, m);
        break;
      case 5: 
        localDriveId = (DriveId)zza.a(paramParcel, m, DriveId.CREATOR);
        break;
      case 6: 
        localInteger = zza.h(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new CreateFileIntentSenderRequest(j, localMetadataBundle, i, str, localDriveId, localInteger);
  }
  
  public CreateFileIntentSenderRequest[] a(int paramInt)
  {
    return new CreateFileIntentSenderRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */