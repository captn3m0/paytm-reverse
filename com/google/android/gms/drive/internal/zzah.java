package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.ChangeSequenceNumber;
import com.google.android.gms.drive.DriveSpace;
import java.util.ArrayList;

public class zzah
  implements Parcelable.Creator<GetChangesRequest>
{
  static void a(GetChangesRequest paramGetChangesRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramGetChangesRequest.a);
    zzb.a(paramParcel, 2, paramGetChangesRequest.b, paramInt, false);
    zzb.a(paramParcel, 3, paramGetChangesRequest.c);
    zzb.c(paramParcel, 4, paramGetChangesRequest.d, false);
    zzb.a(paramParcel, 5, paramGetChangesRequest.e);
    zzb.a(paramParcel, i);
  }
  
  public GetChangesRequest a(Parcel paramParcel)
  {
    ArrayList localArrayList = null;
    boolean bool = false;
    int k = zza.b(paramParcel);
    int i = 0;
    ChangeSequenceNumber localChangeSequenceNumber = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        localChangeSequenceNumber = (ChangeSequenceNumber)zza.a(paramParcel, m, ChangeSequenceNumber.CREATOR);
        break;
      case 3: 
        i = zza.g(paramParcel, m);
        break;
      case 4: 
        localArrayList = zza.c(paramParcel, m, DriveSpace.CREATOR);
        break;
      case 5: 
        bool = zza.c(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new GetChangesRequest(j, localChangeSequenceNumber, i, localArrayList, bool);
  }
  
  public GetChangesRequest[] a(int paramInt)
  {
    return new GetChangesRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzah.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */