package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public class zzn
  implements Parcelable.Creator<CreateFileRequest>
{
  static void a(CreateFileRequest paramCreateFileRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramCreateFileRequest.a);
    zzb.a(paramParcel, 2, paramCreateFileRequest.b, paramInt, false);
    zzb.a(paramParcel, 3, paramCreateFileRequest.c, paramInt, false);
    zzb.a(paramParcel, 4, paramCreateFileRequest.d, paramInt, false);
    zzb.a(paramParcel, 5, paramCreateFileRequest.e, false);
    zzb.a(paramParcel, 6, paramCreateFileRequest.f);
    zzb.a(paramParcel, 7, paramCreateFileRequest.g, false);
    zzb.a(paramParcel, 8, paramCreateFileRequest.h);
    zzb.a(paramParcel, 9, paramCreateFileRequest.i);
    zzb.a(paramParcel, 10, paramCreateFileRequest.j, false);
    zzb.a(paramParcel, i);
  }
  
  public CreateFileRequest a(Parcel paramParcel)
  {
    int i = 0;
    String str1 = null;
    int m = zza.b(paramParcel);
    int j = 0;
    String str2 = null;
    boolean bool = false;
    Integer localInteger = null;
    Contents localContents = null;
    MetadataBundle localMetadataBundle = null;
    DriveId localDriveId = null;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        localDriveId = (DriveId)zza.a(paramParcel, n, DriveId.CREATOR);
        break;
      case 3: 
        localMetadataBundle = (MetadataBundle)zza.a(paramParcel, n, MetadataBundle.CREATOR);
        break;
      case 4: 
        localContents = (Contents)zza.a(paramParcel, n, Contents.CREATOR);
        break;
      case 5: 
        localInteger = zza.h(paramParcel, n);
        break;
      case 6: 
        bool = zza.c(paramParcel, n);
        break;
      case 7: 
        str2 = zza.p(paramParcel, n);
        break;
      case 8: 
        j = zza.g(paramParcel, n);
        break;
      case 9: 
        i = zza.g(paramParcel, n);
        break;
      case 10: 
        str1 = zza.p(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new CreateFileRequest(k, localDriveId, localMetadataBundle, localContents, localInteger, bool, str2, j, i, str1);
  }
  
  public CreateFileRequest[] a(int paramInt)
  {
    return new CreateFileRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */