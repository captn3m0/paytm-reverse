package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public class zzh
  implements Parcelable.Creator<CloseContentsAndUpdateMetadataRequest>
{
  static void a(CloseContentsAndUpdateMetadataRequest paramCloseContentsAndUpdateMetadataRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramCloseContentsAndUpdateMetadataRequest.a);
    zzb.a(paramParcel, 2, paramCloseContentsAndUpdateMetadataRequest.b, paramInt, false);
    zzb.a(paramParcel, 3, paramCloseContentsAndUpdateMetadataRequest.c, paramInt, false);
    zzb.a(paramParcel, 4, paramCloseContentsAndUpdateMetadataRequest.d, paramInt, false);
    zzb.a(paramParcel, 5, paramCloseContentsAndUpdateMetadataRequest.e);
    zzb.a(paramParcel, 6, paramCloseContentsAndUpdateMetadataRequest.f, false);
    zzb.a(paramParcel, 7, paramCloseContentsAndUpdateMetadataRequest.g);
    zzb.a(paramParcel, 8, paramCloseContentsAndUpdateMetadataRequest.h);
    zzb.a(paramParcel, 9, paramCloseContentsAndUpdateMetadataRequest.i);
    zzb.a(paramParcel, 10, paramCloseContentsAndUpdateMetadataRequest.j);
    zzb.a(paramParcel, i);
  }
  
  public CloseContentsAndUpdateMetadataRequest a(Parcel paramParcel)
  {
    String str = null;
    boolean bool2 = false;
    int m = zza.b(paramParcel);
    boolean bool1 = true;
    int i = 0;
    int j = 0;
    boolean bool3 = false;
    Contents localContents = null;
    MetadataBundle localMetadataBundle = null;
    DriveId localDriveId = null;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        localDriveId = (DriveId)zza.a(paramParcel, n, DriveId.CREATOR);
        break;
      case 3: 
        localMetadataBundle = (MetadataBundle)zza.a(paramParcel, n, MetadataBundle.CREATOR);
        break;
      case 4: 
        localContents = (Contents)zza.a(paramParcel, n, Contents.CREATOR);
        break;
      case 5: 
        bool3 = zza.c(paramParcel, n);
        break;
      case 6: 
        str = zza.p(paramParcel, n);
        break;
      case 7: 
        j = zza.g(paramParcel, n);
        break;
      case 8: 
        i = zza.g(paramParcel, n);
        break;
      case 9: 
        bool2 = zza.c(paramParcel, n);
        break;
      case 10: 
        bool1 = zza.c(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new CloseContentsAndUpdateMetadataRequest(k, localDriveId, localMetadataBundle, localContents, bool3, str, j, i, bool2, bool1);
  }
  
  public CloseContentsAndUpdateMetadataRequest[] a(int paramInt)
  {
    return new CloseContentsAndUpdateMetadataRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */