package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public class zzca
  implements Parcelable.Creator<UpdateMetadataRequest>
{
  static void a(UpdateMetadataRequest paramUpdateMetadataRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramUpdateMetadataRequest.a);
    zzb.a(paramParcel, 2, paramUpdateMetadataRequest.b, paramInt, false);
    zzb.a(paramParcel, 3, paramUpdateMetadataRequest.c, paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public UpdateMetadataRequest a(Parcel paramParcel)
  {
    MetadataBundle localMetadataBundle = null;
    int j = zza.b(paramParcel);
    int i = 0;
    DriveId localDriveId = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
      }
      for (;;)
      {
        break;
        i = zza.g(paramParcel, k);
        continue;
        localDriveId = (DriveId)zza.a(paramParcel, k, DriveId.CREATOR);
        continue;
        localMetadataBundle = (MetadataBundle)zza.a(paramParcel, k, MetadataBundle.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new UpdateMetadataRequest(i, localDriveId, localMetadataBundle);
  }
  
  public UpdateMetadataRequest[] a(int paramInt)
  {
    return new UpdateMetadataRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzca.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */