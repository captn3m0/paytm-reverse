package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzbn
  implements Parcelable.Creator<ParcelableTransferPreferences>
{
  static void a(ParcelableTransferPreferences paramParcelableTransferPreferences, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramParcelableTransferPreferences.a);
    zzb.a(paramParcel, 2, paramParcelableTransferPreferences.b);
    zzb.a(paramParcel, 3, paramParcelableTransferPreferences.c);
    zzb.a(paramParcel, 4, paramParcelableTransferPreferences.d);
    zzb.a(paramParcel, paramInt);
  }
  
  public ParcelableTransferPreferences a(Parcel paramParcel)
  {
    boolean bool = false;
    int m = zza.b(paramParcel);
    int k = 0;
    int j = 0;
    int i = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        i = zza.g(paramParcel, n);
        break;
      case 2: 
        j = zza.g(paramParcel, n);
        break;
      case 3: 
        k = zza.g(paramParcel, n);
        break;
      case 4: 
        bool = zza.c(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new ParcelableTransferPreferences(i, j, k, bool);
  }
  
  public ParcelableTransferPreferences[] a(int paramInt)
  {
    return new ParcelableTransferPreferences[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/zzbn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */