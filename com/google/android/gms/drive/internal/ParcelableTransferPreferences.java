package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class ParcelableTransferPreferences
  implements SafeParcelable
{
  public static final Parcelable.Creator<ParcelableTransferPreferences> CREATOR = new zzbn();
  final int a;
  final int b;
  final int c;
  final boolean d;
  
  ParcelableTransferPreferences(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
  {
    this.a = paramInt1;
    this.b = paramInt2;
    this.c = paramInt3;
    this.d = paramBoolean;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzbn.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/ParcelableTransferPreferences.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */