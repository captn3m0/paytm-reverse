package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.DriveId;
import java.util.List;

public class ChangeResourceParentsRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<ChangeResourceParentsRequest> CREATOR = new zzf();
  final int a;
  final DriveId b;
  final List<DriveId> c;
  final List<DriveId> d;
  
  ChangeResourceParentsRequest(int paramInt, DriveId paramDriveId, List<DriveId> paramList1, List<DriveId> paramList2)
  {
    this.a = paramInt;
    this.b = paramDriveId;
    this.c = paramList1;
    this.d = paramList2;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/ChangeResourceParentsRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */