package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.events.ChangeEvent;
import com.google.android.gms.drive.events.ChangesAvailableEvent;
import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.drive.events.DriveEvent;
import com.google.android.gms.drive.events.QueryResultEventParcelable;
import com.google.android.gms.drive.events.TransferProgressEvent;
import com.google.android.gms.drive.events.TransferStateEvent;

public class OnEventResponse
  implements SafeParcelable
{
  public static final Parcelable.Creator<OnEventResponse> CREATOR = new zzba();
  final int a;
  final int b;
  final ChangeEvent c;
  final CompletionEvent d;
  final QueryResultEventParcelable e;
  final ChangesAvailableEvent f;
  final TransferStateEvent g;
  final TransferProgressEvent h;
  
  OnEventResponse(int paramInt1, int paramInt2, ChangeEvent paramChangeEvent, CompletionEvent paramCompletionEvent, QueryResultEventParcelable paramQueryResultEventParcelable, ChangesAvailableEvent paramChangesAvailableEvent, TransferStateEvent paramTransferStateEvent, TransferProgressEvent paramTransferProgressEvent)
  {
    this.a = paramInt1;
    this.b = paramInt2;
    this.c = paramChangeEvent;
    this.d = paramCompletionEvent;
    this.e = paramQueryResultEventParcelable;
    this.f = paramChangesAvailableEvent;
    this.g = paramTransferStateEvent;
    this.h = paramTransferProgressEvent;
  }
  
  public DriveEvent a()
  {
    switch (this.b)
    {
    case 5: 
    case 6: 
    default: 
      throw new IllegalStateException("Unexpected event type " + this.b);
    case 1: 
      return this.c;
    case 2: 
      return this.d;
    case 3: 
      return this.e;
    case 4: 
      return this.f;
    case 7: 
      return this.g;
    }
    return this.h;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzba.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/internal/OnEventResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */