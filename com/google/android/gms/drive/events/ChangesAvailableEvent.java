package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import java.util.Locale;

public final class ChangesAvailableEvent
  implements SafeParcelable, DriveEvent
{
  public static final Parcelable.Creator<ChangesAvailableEvent> CREATOR = new zzb();
  final int a;
  final String b;
  final ChangesAvailableOptions c;
  
  ChangesAvailableEvent(int paramInt, String paramString, ChangesAvailableOptions paramChangesAvailableOptions)
  {
    this.a = paramInt;
    this.b = paramString;
    this.c = paramChangesAvailableOptions;
  }
  
  public int a()
  {
    return 4;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (ChangesAvailableEvent)paramObject;
      if (!zzw.a(this.c, ((ChangesAvailableEvent)paramObject).c)) {
        break;
      }
      bool1 = bool2;
    } while (zzw.a(this.b, ((ChangesAvailableEvent)paramObject).b));
    return false;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.c, this.b });
  }
  
  public String toString()
  {
    return String.format(Locale.US, "ChangesAvailableEvent [changesAvailableOptions=%s]", new Object[] { this.c });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/events/ChangesAvailableEvent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */