package com.google.android.gms.drive.events.internal;

import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.drive.events.zzh;
import com.google.android.gms.drive.events.zzj;

public class zza
  implements zzh
{
  private final zzj a;
  private final long b;
  private final long c;
  
  public zza(TransferProgressData paramTransferProgressData)
  {
    this.a = new zzb(paramTransferProgressData);
    this.b = paramTransferProgressData.d();
    this.c = paramTransferProgressData.e();
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (zza)paramObject;
      if ((!zzw.a(this.a, ((zza)paramObject).a)) || (this.b != ((zza)paramObject).b)) {
        break;
      }
      bool1 = bool2;
    } while (this.c == ((zza)paramObject).c);
    return false;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Long.valueOf(this.c), Long.valueOf(this.b), Long.valueOf(this.c) });
  }
  
  public String toString()
  {
    return String.format("FileTransferProgress[FileTransferState: %s, BytesTransferred: %d, TotalBytes: %d]", new Object[] { this.a.toString(), Long.valueOf(this.b), Long.valueOf(this.c) });
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/events/internal/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */