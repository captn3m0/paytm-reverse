package com.google.android.gms.drive.events.internal;

import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.events.zzj;

public class zzb
  implements zzj
{
  private final DriveId a;
  private final int b;
  private final int c;
  
  public zzb(TransferProgressData paramTransferProgressData)
  {
    this.a = paramTransferProgressData.b();
    this.b = paramTransferProgressData.a();
    this.c = paramTransferProgressData.c();
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (zzb)paramObject;
      if ((!zzw.a(this.a, ((zzb)paramObject).a)) || (this.b != ((zzb)paramObject).b)) {
        break;
      }
      bool1 = bool2;
    } while (this.c == ((zzb)paramObject).c);
    return false;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.a, Integer.valueOf(this.b), Integer.valueOf(this.c) });
  }
  
  public String toString()
  {
    return String.format("FileTransferState[TransferType: %d, DriveId: %s, status: %d]", new Object[] { Integer.valueOf(this.b), this.a, Integer.valueOf(this.c) });
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/events/internal/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */