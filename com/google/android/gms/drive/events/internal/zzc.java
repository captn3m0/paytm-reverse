package com.google.android.gms.drive.events.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.DriveId;

public class zzc
  implements Parcelable.Creator<TransferProgressData>
{
  static void a(TransferProgressData paramTransferProgressData, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramTransferProgressData.a);
    zzb.a(paramParcel, 2, paramTransferProgressData.b);
    zzb.a(paramParcel, 3, paramTransferProgressData.c, paramInt, false);
    zzb.a(paramParcel, 4, paramTransferProgressData.d);
    zzb.a(paramParcel, 5, paramTransferProgressData.e);
    zzb.a(paramParcel, 6, paramTransferProgressData.f);
    zzb.a(paramParcel, i);
  }
  
  public TransferProgressData a(Parcel paramParcel)
  {
    long l1 = 0L;
    int i = 0;
    int m = zza.b(paramParcel);
    DriveId localDriveId = null;
    long l2 = 0L;
    int j = 0;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        j = zza.g(paramParcel, n);
        break;
      case 3: 
        localDriveId = (DriveId)zza.a(paramParcel, n, DriveId.CREATOR);
        break;
      case 4: 
        i = zza.g(paramParcel, n);
        break;
      case 5: 
        l2 = zza.i(paramParcel, n);
        break;
      case 6: 
        l1 = zza.i(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new TransferProgressData(k, j, localDriveId, i, l2, l1);
  }
  
  public TransferProgressData[] a(int paramInt)
  {
    return new TransferProgressData[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/events/internal/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */