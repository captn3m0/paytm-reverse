package com.google.android.gms.drive.events.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.drive.DriveId;

public class TransferProgressData
  implements SafeParcelable
{
  public static final Parcelable.Creator<TransferProgressData> CREATOR = new zzc();
  final int a;
  final int b;
  final DriveId c;
  final int d;
  final long e;
  final long f;
  
  TransferProgressData(int paramInt1, int paramInt2, DriveId paramDriveId, int paramInt3, long paramLong1, long paramLong2)
  {
    this.a = paramInt1;
    this.b = paramInt2;
    this.c = paramDriveId;
    this.d = paramInt3;
    this.e = paramLong1;
    this.f = paramLong2;
  }
  
  public int a()
  {
    return this.b;
  }
  
  public DriveId b()
  {
    return this.c;
  }
  
  public int c()
  {
    return this.d;
  }
  
  public long d()
  {
    return this.e;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public long e()
  {
    return this.f;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (TransferProgressData)paramObject;
      if ((this.b != ((TransferProgressData)paramObject).b) || (!zzw.a(this.c, ((TransferProgressData)paramObject).c)) || (this.d != ((TransferProgressData)paramObject).d) || (this.e != ((TransferProgressData)paramObject).e)) {
        break;
      }
      bool1 = bool2;
    } while (this.f == ((TransferProgressData)paramObject).f);
    return false;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Integer.valueOf(this.b), this.c, Integer.valueOf(this.d), Long.valueOf(this.e), Long.valueOf(this.f) });
  }
  
  public String toString()
  {
    return String.format("TransferProgressData[TransferType: %d, DriveId: %s, status: %d, bytes transferred: %d, total bytes: %d]", new Object[] { Integer.valueOf(this.b), this.c, Integer.valueOf(this.d), Long.valueOf(this.e), Long.valueOf(this.f) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/events/internal/TransferProgressData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */