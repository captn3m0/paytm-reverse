package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.drive.DriveSpace;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public final class ChangesAvailableOptions
  implements SafeParcelable
{
  public static final Parcelable.Creator<ChangesAvailableOptions> CREATOR = new zzd();
  final int a;
  final int b;
  final boolean c;
  final List<DriveSpace> d;
  private final Set<DriveSpace> e;
  
  ChangesAvailableOptions(int paramInt1, int paramInt2, boolean paramBoolean, List<DriveSpace> paramList) {}
  
  private ChangesAvailableOptions(int paramInt1, int paramInt2, boolean paramBoolean, List<DriveSpace> paramList, Set<DriveSpace> paramSet)
  {
    this.a = paramInt1;
    this.b = paramInt2;
    this.c = paramBoolean;
    this.d = paramList;
    this.e = paramSet;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (ChangesAvailableOptions)paramObject;
      if ((!zzw.a(this.e, ((ChangesAvailableOptions)paramObject).e)) || (this.b != ((ChangesAvailableOptions)paramObject).b)) {
        break;
      }
      bool1 = bool2;
    } while (this.c == ((ChangesAvailableOptions)paramObject).c);
    return false;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.e, Integer.valueOf(this.b), Boolean.valueOf(this.c) });
  }
  
  public String toString()
  {
    return String.format(Locale.US, "ChangesAvailableOptions[ChangesSizeLimit=%d, Repeats=%s, Spaces=%s]", new Object[] { Integer.valueOf(this.b), Boolean.valueOf(this.c), this.d });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/events/ChangesAvailableOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */