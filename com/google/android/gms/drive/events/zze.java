package com.google.android.gms.drive.events;

import android.os.IBinder;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import java.util.ArrayList;

public class zze
  implements Parcelable.Creator<CompletionEvent>
{
  static void a(CompletionEvent paramCompletionEvent, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramCompletionEvent.a);
    zzb.a(paramParcel, 2, paramCompletionEvent.b, paramInt, false);
    zzb.a(paramParcel, 3, paramCompletionEvent.c, false);
    zzb.a(paramParcel, 4, paramCompletionEvent.d, paramInt, false);
    zzb.a(paramParcel, 5, paramCompletionEvent.e, paramInt, false);
    zzb.a(paramParcel, 6, paramCompletionEvent.f, paramInt, false);
    zzb.b(paramParcel, 7, paramCompletionEvent.g, false);
    zzb.a(paramParcel, 8, paramCompletionEvent.h);
    zzb.a(paramParcel, 9, paramCompletionEvent.i, false);
    zzb.a(paramParcel, i);
  }
  
  public CompletionEvent a(Parcel paramParcel)
  {
    int i = 0;
    IBinder localIBinder = null;
    int k = zza.b(paramParcel);
    ArrayList localArrayList = null;
    MetadataBundle localMetadataBundle = null;
    ParcelFileDescriptor localParcelFileDescriptor1 = null;
    ParcelFileDescriptor localParcelFileDescriptor2 = null;
    String str = null;
    DriveId localDriveId = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        localDriveId = (DriveId)zza.a(paramParcel, m, DriveId.CREATOR);
        break;
      case 3: 
        str = zza.p(paramParcel, m);
        break;
      case 4: 
        localParcelFileDescriptor2 = (ParcelFileDescriptor)zza.a(paramParcel, m, ParcelFileDescriptor.CREATOR);
        break;
      case 5: 
        localParcelFileDescriptor1 = (ParcelFileDescriptor)zza.a(paramParcel, m, ParcelFileDescriptor.CREATOR);
        break;
      case 6: 
        localMetadataBundle = (MetadataBundle)zza.a(paramParcel, m, MetadataBundle.CREATOR);
        break;
      case 7: 
        localArrayList = zza.D(paramParcel, m);
        break;
      case 8: 
        i = zza.g(paramParcel, m);
        break;
      case 9: 
        localIBinder = zza.q(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new CompletionEvent(j, localDriveId, str, localParcelFileDescriptor2, localParcelFileDescriptor1, localMetadataBundle, localArrayList, i, localIBinder);
  }
  
  public CompletionEvent[] a(int paramInt)
  {
    return new CompletionEvent[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/events/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */