package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.events.internal.TransferProgressData;

public class zzn
  implements Parcelable.Creator<TransferProgressEvent>
{
  static void a(TransferProgressEvent paramTransferProgressEvent, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramTransferProgressEvent.a);
    zzb.a(paramParcel, 2, paramTransferProgressEvent.b, paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public TransferProgressEvent a(Parcel paramParcel)
  {
    int j = zza.b(paramParcel);
    int i = 0;
    TransferProgressData localTransferProgressData = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localTransferProgressData = (TransferProgressData)zza.a(paramParcel, k, TransferProgressData.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new TransferProgressEvent(i, localTransferProgressData);
  }
  
  public TransferProgressEvent[] a(int paramInt)
  {
    return new TransferProgressEvent[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/events/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */