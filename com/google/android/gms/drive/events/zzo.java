package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzo
  implements Parcelable.Creator<TransferProgressOptions>
{
  static void a(TransferProgressOptions paramTransferProgressOptions, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramTransferProgressOptions.a);
    zzb.a(paramParcel, 2, paramTransferProgressOptions.b);
    zzb.a(paramParcel, paramInt);
  }
  
  public TransferProgressOptions a(Parcel paramParcel)
  {
    int j = 0;
    int k = zza.b(paramParcel);
    int i = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        i = zza.g(paramParcel, m);
        break;
      case 2: 
        j = zza.g(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new TransferProgressOptions(i, j);
  }
  
  public TransferProgressOptions[] a(int paramInt)
  {
    return new TransferProgressOptions[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/events/zzo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */