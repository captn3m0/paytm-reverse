package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.DriveSpace;
import java.util.ArrayList;

public class zzd
  implements Parcelable.Creator<ChangesAvailableOptions>
{
  static void a(ChangesAvailableOptions paramChangesAvailableOptions, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramChangesAvailableOptions.a);
    zzb.a(paramParcel, 2, paramChangesAvailableOptions.b);
    zzb.a(paramParcel, 3, paramChangesAvailableOptions.c);
    zzb.c(paramParcel, 4, paramChangesAvailableOptions.d, false);
    zzb.a(paramParcel, paramInt);
  }
  
  public ChangesAvailableOptions a(Parcel paramParcel)
  {
    boolean bool = false;
    int k = zza.b(paramParcel);
    ArrayList localArrayList = null;
    int j = 0;
    int i = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        i = zza.g(paramParcel, m);
        break;
      case 2: 
        j = zza.g(paramParcel, m);
        break;
      case 3: 
        bool = zza.c(paramParcel, m);
        break;
      case 4: 
        localArrayList = zza.c(paramParcel, m, DriveSpace.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new ChangesAvailableOptions(i, j, bool, localArrayList);
  }
  
  public ChangesAvailableOptions[] a(int paramInt)
  {
    return new ChangesAvailableOptions[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/events/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */