package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.events.internal.TransferProgressData;
import java.util.ArrayList;

public class zzp
  implements Parcelable.Creator<TransferStateEvent>
{
  static void a(TransferStateEvent paramTransferStateEvent, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramTransferStateEvent.a);
    zzb.a(paramParcel, 2, paramTransferStateEvent.b, false);
    zzb.c(paramParcel, 3, paramTransferStateEvent.c, false);
    zzb.a(paramParcel, paramInt);
  }
  
  public TransferStateEvent a(Parcel paramParcel)
  {
    ArrayList localArrayList = null;
    int j = zza.b(paramParcel);
    int i = 0;
    String str = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str = zza.p(paramParcel, k);
        break;
      case 3: 
        localArrayList = zza.c(paramParcel, k, TransferProgressData.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new TransferStateEvent(i, str, localArrayList);
  }
  
  public TransferStateEvent[] a(int paramInt)
  {
    return new TransferStateEvent[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/events/zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */