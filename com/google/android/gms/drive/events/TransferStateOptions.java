package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.drive.DriveSpace;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public final class TransferStateOptions
  implements SafeParcelable
{
  public static final Parcelable.Creator<TransferStateOptions> CREATOR = new zzr();
  final int a;
  final List<DriveSpace> b;
  private final Set<DriveSpace> c;
  
  TransferStateOptions(int paramInt, List<DriveSpace> paramList) {}
  
  private TransferStateOptions(int paramInt, List<DriveSpace> paramList, Set<DriveSpace> paramSet)
  {
    this.a = paramInt;
    this.b = paramList;
    this.c = paramSet;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    paramObject = (TransferStateOptions)paramObject;
    return zzw.a(this.c, ((TransferStateOptions)paramObject).c);
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.c });
  }
  
  public String toString()
  {
    return String.format(Locale.US, "TransferStateOptions[Spaces=%s]", new Object[] { this.b });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzr.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/events/TransferStateOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */