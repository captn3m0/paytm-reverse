package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzl
  implements Parcelable.Creator<QueryResultEventParcelable>
{
  static void a(QueryResultEventParcelable paramQueryResultEventParcelable, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramQueryResultEventParcelable.a);
    zzb.a(paramParcel, 2, paramQueryResultEventParcelable.b, paramInt, false);
    zzb.a(paramParcel, 3, paramQueryResultEventParcelable.c);
    zzb.a(paramParcel, 4, paramQueryResultEventParcelable.d);
    zzb.a(paramParcel, i);
  }
  
  public QueryResultEventParcelable a(Parcel paramParcel)
  {
    int i = 0;
    int k = zza.b(paramParcel);
    DataHolder localDataHolder = null;
    boolean bool = false;
    int j = 0;
    if (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
      }
      for (;;)
      {
        break;
        j = zza.g(paramParcel, m);
        continue;
        localDataHolder = (DataHolder)zza.a(paramParcel, m, DataHolder.CREATOR);
        continue;
        bool = zza.c(paramParcel, m);
        continue;
        i = zza.g(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new QueryResultEventParcelable(j, localDataHolder, bool, i);
  }
  
  public QueryResultEventParcelable[] a(int paramInt)
  {
    return new QueryResultEventParcelable[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/events/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */