package com.google.android.gms.drive.events;

import android.app.Service;
import android.os.Binder;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.drive.internal.OnEventResponse;
import com.google.android.gms.drive.internal.zzao.zza;
import com.google.android.gms.drive.internal.zzz;
import java.util.concurrent.CountDownLatch;

public abstract class DriveEventService
  extends Service
  implements ChangeListener, CompletionListener, zzc, zzq
{
  zza a;
  boolean b = false;
  int c = -1;
  private final String d;
  private CountDownLatch e;
  
  protected DriveEventService()
  {
    this("DriveEventService");
  }
  
  protected DriveEventService(String paramString)
  {
    this.d = paramString;
  }
  
  private void a(OnEventResponse paramOnEventResponse)
  {
    paramOnEventResponse = paramOnEventResponse.a();
    zzz.a("DriveEventService", "handleEventMessage: " + paramOnEventResponse);
    for (;;)
    {
      try
      {
        switch (paramOnEventResponse.a())
        {
        case 3: 
        case 5: 
        case 6: 
          zzz.b(this.d, "Unhandled event: " + paramOnEventResponse);
          return;
        }
      }
      catch (Exception localException)
      {
        zzz.a(this.d, localException, "Error handling event: " + paramOnEventResponse);
        return;
      }
      a((ChangeEvent)paramOnEventResponse);
      return;
      a((CompletionEvent)paramOnEventResponse);
      return;
      a((ChangesAvailableEvent)paramOnEventResponse);
      return;
      a((TransferStateEvent)paramOnEventResponse);
      return;
    }
  }
  
  private void b()
    throws SecurityException
  {
    int i = a();
    if (i == this.c) {
      return;
    }
    if (GooglePlayServicesUtil.b(this, i))
    {
      this.c = i;
      return;
    }
    throw new SecurityException("Caller is not GooglePlayServices");
  }
  
  protected int a()
  {
    return Binder.getCallingUid();
  }
  
  public void a(ChangeEvent paramChangeEvent)
  {
    zzz.b(this.d, "Unhandled change event: " + paramChangeEvent);
  }
  
  public void a(ChangesAvailableEvent paramChangesAvailableEvent)
  {
    zzz.b(this.d, "Unhandled changes available event: " + paramChangesAvailableEvent);
  }
  
  public void a(CompletionEvent paramCompletionEvent)
  {
    zzz.b(this.d, "Unhandled completion event: " + paramCompletionEvent);
  }
  
  public void a(TransferStateEvent paramTransferStateEvent)
  {
    zzz.b(this.d, "Unhandled transfer state event: " + paramTransferStateEvent);
  }
  
  final class zza
    extends Handler
  {
    zza() {}
    
    private Message a(OnEventResponse paramOnEventResponse)
    {
      return obtainMessage(1, paramOnEventResponse);
    }
    
    public void handleMessage(Message paramMessage)
    {
      zzz.a("DriveEventService", "handleMessage message type:" + paramMessage.what);
      switch (paramMessage.what)
      {
      default: 
        zzz.b("DriveEventService", "Unexpected message type:" + paramMessage.what);
        return;
      case 1: 
        DriveEventService.a(DriveEventService.this, (OnEventResponse)paramMessage.obj);
        return;
      }
      getLooper().quit();
    }
  }
  
  final class zzb
    extends zzao.zza
  {
    public void a(OnEventResponse paramOnEventResponse)
      throws RemoteException
    {
      synchronized (this.a)
      {
        zzz.a("DriveEventService", "onEvent: " + paramOnEventResponse);
        DriveEventService.a(this.a);
        if (this.a.a != null)
        {
          paramOnEventResponse = DriveEventService.zza.a(this.a.a, paramOnEventResponse);
          this.a.a.sendMessage(paramOnEventResponse);
          return;
        }
        zzz.c("DriveEventService", "Receiving event before initialize is completed.");
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/events/DriveEventService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */