package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.drive.events.internal.TransferProgressData;
import java.util.List;

public final class TransferStateEvent
  implements DriveEvent
{
  public static final Parcelable.Creator<TransferStateEvent> CREATOR = new zzp();
  final int a;
  final String b;
  final List<TransferProgressData> c;
  
  TransferStateEvent(int paramInt, String paramString, List<TransferProgressData> paramList)
  {
    this.a = paramInt;
    this.b = paramString;
    this.c = paramList;
  }
  
  public int a()
  {
    return 7;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (TransferStateEvent)paramObject;
      if (!zzw.a(this.b, ((TransferStateEvent)paramObject).b)) {
        break;
      }
      bool1 = bool2;
    } while (zzw.a(this.c, ((TransferStateEvent)paramObject).c));
    return false;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b, this.c });
  }
  
  public String toString()
  {
    return String.format("TransferStateEvent[%s]", new Object[] { TextUtils.join("','", this.c) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzp.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/events/TransferStateEvent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */