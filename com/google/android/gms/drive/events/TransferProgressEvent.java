package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.drive.events.internal.TransferProgressData;

public final class TransferProgressEvent
  implements DriveEvent
{
  public static final Parcelable.Creator<TransferProgressEvent> CREATOR = new zzn();
  final int a;
  final TransferProgressData b;
  
  TransferProgressEvent(int paramInt, TransferProgressData paramTransferProgressData)
  {
    this.a = paramInt;
    this.b = paramTransferProgressData;
  }
  
  public int a()
  {
    return 8;
  }
  
  public TransferProgressData b()
  {
    return this.b;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    paramObject = (TransferProgressEvent)paramObject;
    return zzw.a(this.b, ((TransferProgressEvent)paramObject).b);
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b });
  }
  
  public String toString()
  {
    return String.format("TransferProgressEvent[%s]", new Object[] { this.b.toString() });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzn.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/events/TransferProgressEvent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */