package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import java.util.Locale;

public final class TransferProgressOptions
  implements SafeParcelable
{
  public static final Parcelable.Creator<TransferProgressOptions> CREATOR = new zzo();
  final int a;
  final int b;
  
  TransferProgressOptions(int paramInt1, int paramInt2)
  {
    this.a = paramInt1;
    this.b = paramInt2;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    paramObject = (TransferProgressOptions)paramObject;
    return zzw.a(Integer.valueOf(this.b), Integer.valueOf(((TransferProgressOptions)paramObject).b));
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Integer.valueOf(this.b) });
  }
  
  public String toString()
  {
    return String.format(Locale.US, "TransferProgressOptions[type=%d]", new Object[] { Integer.valueOf(this.b) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzo.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/events/TransferProgressOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */