package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzc
  implements Parcelable.Creator<DriveFileRange>
{
  static void a(DriveFileRange paramDriveFileRange, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramDriveFileRange.a);
    zzb.a(paramParcel, 2, paramDriveFileRange.b);
    zzb.a(paramParcel, 3, paramDriveFileRange.c);
    zzb.a(paramParcel, paramInt);
  }
  
  public DriveFileRange a(Parcel paramParcel)
  {
    long l1 = 0L;
    int j = zza.b(paramParcel);
    int i = 0;
    long l2 = 0L;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        l2 = zza.i(paramParcel, k);
        break;
      case 3: 
        l1 = zza.i(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new DriveFileRange(i, l2, l1);
  }
  
  public DriveFileRange[] a(int paramInt)
  {
    return new DriveFileRange[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */