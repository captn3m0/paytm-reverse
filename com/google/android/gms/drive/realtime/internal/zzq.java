package com.google.android.gms.drive.realtime.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzq
  implements Parcelable.Creator<ParcelableCollaborator>
{
  static void a(ParcelableCollaborator paramParcelableCollaborator, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramParcelableCollaborator.a);
    zzb.a(paramParcel, 2, paramParcelableCollaborator.b);
    zzb.a(paramParcel, 3, paramParcelableCollaborator.c);
    zzb.a(paramParcel, 4, paramParcelableCollaborator.d, false);
    zzb.a(paramParcel, 5, paramParcelableCollaborator.e, false);
    zzb.a(paramParcel, 6, paramParcelableCollaborator.f, false);
    zzb.a(paramParcel, 7, paramParcelableCollaborator.g, false);
    zzb.a(paramParcel, 8, paramParcelableCollaborator.h, false);
    zzb.a(paramParcel, paramInt);
  }
  
  public ParcelableCollaborator a(Parcel paramParcel)
  {
    boolean bool1 = false;
    String str1 = null;
    int j = zza.b(paramParcel);
    String str2 = null;
    String str3 = null;
    String str4 = null;
    String str5 = null;
    boolean bool2 = false;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        bool2 = zza.c(paramParcel, k);
        break;
      case 3: 
        bool1 = zza.c(paramParcel, k);
        break;
      case 4: 
        str5 = zza.p(paramParcel, k);
        break;
      case 5: 
        str4 = zza.p(paramParcel, k);
        break;
      case 6: 
        str3 = zza.p(paramParcel, k);
        break;
      case 7: 
        str2 = zza.p(paramParcel, k);
        break;
      case 8: 
        str1 = zza.p(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new ParcelableCollaborator(i, bool2, bool1, str5, str4, str3, str2, str1);
  }
  
  public ParcelableCollaborator[] a(int paramInt)
  {
    return new ParcelableCollaborator[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/realtime/internal/zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */