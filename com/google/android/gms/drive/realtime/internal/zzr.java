package com.google.android.gms.drive.realtime.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzr
  implements Parcelable.Creator<ParcelableIndexReference>
{
  static void a(ParcelableIndexReference paramParcelableIndexReference, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramParcelableIndexReference.a);
    zzb.a(paramParcel, 2, paramParcelableIndexReference.b, false);
    zzb.a(paramParcel, 3, paramParcelableIndexReference.c);
    zzb.a(paramParcel, 4, paramParcelableIndexReference.d);
    zzb.a(paramParcel, 5, paramParcelableIndexReference.e);
    zzb.a(paramParcel, paramInt);
  }
  
  public ParcelableIndexReference a(Parcel paramParcel)
  {
    boolean bool = false;
    int m = zza.b(paramParcel);
    String str = null;
    int i = -1;
    int j = 0;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        str = zza.p(paramParcel, n);
        break;
      case 3: 
        j = zza.g(paramParcel, n);
        break;
      case 4: 
        bool = zza.c(paramParcel, n);
        break;
      case 5: 
        i = zza.g(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new ParcelableIndexReference(k, str, j, bool, i);
  }
  
  public ParcelableIndexReference[] a(int paramInt)
  {
    return new ParcelableIndexReference[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/realtime/internal/zzr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */