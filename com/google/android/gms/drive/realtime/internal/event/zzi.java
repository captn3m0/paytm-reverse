package com.google.android.gms.drive.realtime.internal.event;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzi
  implements Parcelable.Creator<ValuesAddedDetails>
{
  static void a(ValuesAddedDetails paramValuesAddedDetails, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramValuesAddedDetails.a);
    zzb.a(paramParcel, 2, paramValuesAddedDetails.b);
    zzb.a(paramParcel, 3, paramValuesAddedDetails.c);
    zzb.a(paramParcel, 4, paramValuesAddedDetails.d);
    zzb.a(paramParcel, 5, paramValuesAddedDetails.e, false);
    zzb.a(paramParcel, 6, paramValuesAddedDetails.f);
    zzb.a(paramParcel, paramInt);
  }
  
  public ValuesAddedDetails a(Parcel paramParcel)
  {
    int i = 0;
    int i1 = zza.b(paramParcel);
    String str = null;
    int j = 0;
    int k = 0;
    int m = 0;
    int n = 0;
    while (paramParcel.dataPosition() < i1)
    {
      int i2 = zza.a(paramParcel);
      switch (zza.a(i2))
      {
      default: 
        zza.b(paramParcel, i2);
        break;
      case 1: 
        n = zza.g(paramParcel, i2);
        break;
      case 2: 
        m = zza.g(paramParcel, i2);
        break;
      case 3: 
        k = zza.g(paramParcel, i2);
        break;
      case 4: 
        j = zza.g(paramParcel, i2);
        break;
      case 5: 
        str = zza.p(paramParcel, i2);
        break;
      case 6: 
        i = zza.g(paramParcel, i2);
      }
    }
    if (paramParcel.dataPosition() != i1) {
      throw new zza.zza("Overread allowed size end=" + i1, paramParcel);
    }
    return new ValuesAddedDetails(n, m, k, j, str, i);
  }
  
  public ValuesAddedDetails[] a(int paramInt)
  {
    return new ValuesAddedDetails[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/realtime/internal/event/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */