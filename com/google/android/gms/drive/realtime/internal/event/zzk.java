package com.google.android.gms.drive.realtime.internal.event;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzk
  implements Parcelable.Creator<ValuesSetDetails>
{
  static void a(ValuesSetDetails paramValuesSetDetails, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramValuesSetDetails.a);
    zzb.a(paramParcel, 2, paramValuesSetDetails.b);
    zzb.a(paramParcel, 3, paramValuesSetDetails.c);
    zzb.a(paramParcel, 4, paramValuesSetDetails.d);
    zzb.a(paramParcel, paramInt);
  }
  
  public ValuesSetDetails a(Parcel paramParcel)
  {
    int m = 0;
    int n = zza.b(paramParcel);
    int k = 0;
    int j = 0;
    int i = 0;
    while (paramParcel.dataPosition() < n)
    {
      int i1 = zza.a(paramParcel);
      switch (zza.a(i1))
      {
      default: 
        zza.b(paramParcel, i1);
        break;
      case 1: 
        i = zza.g(paramParcel, i1);
        break;
      case 2: 
        j = zza.g(paramParcel, i1);
        break;
      case 3: 
        k = zza.g(paramParcel, i1);
        break;
      case 4: 
        m = zza.g(paramParcel, i1);
      }
    }
    if (paramParcel.dataPosition() != n) {
      throw new zza.zza("Overread allowed size end=" + n, paramParcel);
    }
    return new ValuesSetDetails(i, j, k, m);
  }
  
  public ValuesSetDetails[] a(int paramInt)
  {
    return new ValuesSetDetails[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/realtime/internal/event/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */