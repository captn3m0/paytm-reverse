package com.google.android.gms.drive.realtime.internal.event;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zze
  implements Parcelable.Creator<ReferenceShiftedDetails>
{
  static void a(ReferenceShiftedDetails paramReferenceShiftedDetails, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramReferenceShiftedDetails.a);
    zzb.a(paramParcel, 2, paramReferenceShiftedDetails.b, false);
    zzb.a(paramParcel, 3, paramReferenceShiftedDetails.c, false);
    zzb.a(paramParcel, 4, paramReferenceShiftedDetails.d);
    zzb.a(paramParcel, 5, paramReferenceShiftedDetails.e);
    zzb.a(paramParcel, paramInt);
  }
  
  public ReferenceShiftedDetails a(Parcel paramParcel)
  {
    String str1 = null;
    int i = 0;
    int m = zza.b(paramParcel);
    int j = 0;
    String str2 = null;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        str2 = zza.p(paramParcel, n);
        break;
      case 3: 
        str1 = zza.p(paramParcel, n);
        break;
      case 4: 
        j = zza.g(paramParcel, n);
        break;
      case 5: 
        i = zza.g(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new ReferenceShiftedDetails(k, str2, str1, j, i);
  }
  
  public ReferenceShiftedDetails[] a(int paramInt)
  {
    return new ReferenceShiftedDetails[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/realtime/internal/event/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */