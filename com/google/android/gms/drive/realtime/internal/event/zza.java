package com.google.android.gms.drive.realtime.internal.event;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zza
  implements Parcelable.Creator<FieldChangedDetails>
{
  static void a(FieldChangedDetails paramFieldChangedDetails, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramFieldChangedDetails.a);
    zzb.a(paramParcel, 2, paramFieldChangedDetails.b);
    zzb.a(paramParcel, paramInt);
  }
  
  public FieldChangedDetails a(Parcel paramParcel)
  {
    int j = 0;
    int k = com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel);
    int i = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = com.google.android.gms.common.internal.safeparcel.zza.a(paramParcel);
      switch (com.google.android.gms.common.internal.safeparcel.zza.a(m))
      {
      default: 
        com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel, m);
        break;
      case 1: 
        i = com.google.android.gms.common.internal.safeparcel.zza.g(paramParcel, m);
        break;
      case 2: 
        j = com.google.android.gms.common.internal.safeparcel.zza.g(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new FieldChangedDetails(i, j);
  }
  
  public FieldChangedDetails[] a(int paramInt)
  {
    return new FieldChangedDetails[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/realtime/internal/event/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */