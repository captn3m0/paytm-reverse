package com.google.android.gms.drive.realtime.internal.event;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzc
  implements Parcelable.Creator<ParcelableEvent>
{
  static void a(ParcelableEvent paramParcelableEvent, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramParcelableEvent.a);
    zzb.a(paramParcel, 2, paramParcelableEvent.b, false);
    zzb.a(paramParcel, 3, paramParcelableEvent.c, false);
    zzb.b(paramParcel, 4, paramParcelableEvent.d, false);
    zzb.a(paramParcel, 5, paramParcelableEvent.e);
    zzb.a(paramParcel, 6, paramParcelableEvent.h, false);
    zzb.a(paramParcel, 7, paramParcelableEvent.i, false);
    zzb.a(paramParcel, 8, paramParcelableEvent.j, paramInt, false);
    zzb.a(paramParcel, 9, paramParcelableEvent.k, paramInt, false);
    zzb.a(paramParcel, 10, paramParcelableEvent.l, paramInt, false);
    zzb.a(paramParcel, 11, paramParcelableEvent.m, paramInt, false);
    zzb.a(paramParcel, 12, paramParcelableEvent.n, paramInt, false);
    zzb.a(paramParcel, 13, paramParcelableEvent.o, paramInt, false);
    zzb.a(paramParcel, 14, paramParcelableEvent.p, paramInt, false);
    zzb.a(paramParcel, 15, paramParcelableEvent.q, paramInt, false);
    zzb.a(paramParcel, 17, paramParcelableEvent.g);
    zzb.a(paramParcel, 16, paramParcelableEvent.f);
    zzb.a(paramParcel, 18, paramParcelableEvent.r, paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public ParcelableEvent a(Parcel paramParcel)
  {
    int j = zza.b(paramParcel);
    int i = 0;
    String str4 = null;
    String str3 = null;
    ArrayList localArrayList = null;
    boolean bool3 = false;
    boolean bool2 = false;
    boolean bool1 = false;
    String str2 = null;
    String str1 = null;
    TextInsertedDetails localTextInsertedDetails = null;
    TextDeletedDetails localTextDeletedDetails = null;
    ValuesAddedDetails localValuesAddedDetails = null;
    ValuesRemovedDetails localValuesRemovedDetails = null;
    ValuesSetDetails localValuesSetDetails = null;
    ValueChangedDetails localValueChangedDetails = null;
    ReferenceShiftedDetails localReferenceShiftedDetails = null;
    ObjectChangedDetails localObjectChangedDetails = null;
    FieldChangedDetails localFieldChangedDetails = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str4 = zza.p(paramParcel, k);
        break;
      case 3: 
        str3 = zza.p(paramParcel, k);
        break;
      case 4: 
        localArrayList = zza.D(paramParcel, k);
        break;
      case 5: 
        bool3 = zza.c(paramParcel, k);
        break;
      case 6: 
        str2 = zza.p(paramParcel, k);
        break;
      case 7: 
        str1 = zza.p(paramParcel, k);
        break;
      case 8: 
        localTextInsertedDetails = (TextInsertedDetails)zza.a(paramParcel, k, TextInsertedDetails.CREATOR);
        break;
      case 9: 
        localTextDeletedDetails = (TextDeletedDetails)zza.a(paramParcel, k, TextDeletedDetails.CREATOR);
        break;
      case 10: 
        localValuesAddedDetails = (ValuesAddedDetails)zza.a(paramParcel, k, ValuesAddedDetails.CREATOR);
        break;
      case 11: 
        localValuesRemovedDetails = (ValuesRemovedDetails)zza.a(paramParcel, k, ValuesRemovedDetails.CREATOR);
        break;
      case 12: 
        localValuesSetDetails = (ValuesSetDetails)zza.a(paramParcel, k, ValuesSetDetails.CREATOR);
        break;
      case 13: 
        localValueChangedDetails = (ValueChangedDetails)zza.a(paramParcel, k, ValueChangedDetails.CREATOR);
        break;
      case 14: 
        localReferenceShiftedDetails = (ReferenceShiftedDetails)zza.a(paramParcel, k, ReferenceShiftedDetails.CREATOR);
        break;
      case 15: 
        localObjectChangedDetails = (ObjectChangedDetails)zza.a(paramParcel, k, ObjectChangedDetails.CREATOR);
        break;
      case 17: 
        bool1 = zza.c(paramParcel, k);
        break;
      case 16: 
        bool2 = zza.c(paramParcel, k);
        break;
      case 18: 
        localFieldChangedDetails = (FieldChangedDetails)zza.a(paramParcel, k, FieldChangedDetails.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new ParcelableEvent(i, str4, str3, localArrayList, bool3, bool2, bool1, str2, str1, localTextInsertedDetails, localTextDeletedDetails, localValuesAddedDetails, localValuesRemovedDetails, localValuesSetDetails, localValueChangedDetails, localReferenceShiftedDetails, localObjectChangedDetails, localFieldChangedDetails);
  }
  
  public ParcelableEvent[] a(int paramInt)
  {
    return new ParcelableEvent[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/realtime/internal/event/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */