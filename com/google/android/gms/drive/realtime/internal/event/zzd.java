package com.google.android.gms.drive.realtime.internal.event;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.realtime.internal.ParcelableChangeInfo;
import java.util.ArrayList;

public class zzd
  implements Parcelable.Creator<ParcelableEventList>
{
  static void a(ParcelableEventList paramParcelableEventList, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramParcelableEventList.a);
    zzb.c(paramParcel, 2, paramParcelableEventList.b, false);
    zzb.a(paramParcel, 3, paramParcelableEventList.c, paramInt, false);
    zzb.a(paramParcel, 4, paramParcelableEventList.d);
    zzb.b(paramParcel, 5, paramParcelableEventList.e, false);
    zzb.a(paramParcel, 6, paramParcelableEventList.f, paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public ParcelableEventList a(Parcel paramParcel)
  {
    boolean bool = false;
    ParcelableChangeInfo localParcelableChangeInfo = null;
    int j = zza.b(paramParcel);
    ArrayList localArrayList1 = null;
    DataHolder localDataHolder = null;
    ArrayList localArrayList2 = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localArrayList2 = zza.c(paramParcel, k, ParcelableEvent.CREATOR);
        break;
      case 3: 
        localDataHolder = (DataHolder)zza.a(paramParcel, k, DataHolder.CREATOR);
        break;
      case 4: 
        bool = zza.c(paramParcel, k);
        break;
      case 5: 
        localArrayList1 = zza.D(paramParcel, k);
        break;
      case 6: 
        localParcelableChangeInfo = (ParcelableChangeInfo)zza.a(paramParcel, k, ParcelableChangeInfo.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new ParcelableEventList(i, localArrayList2, localDataHolder, bool, localArrayList1, localParcelableChangeInfo);
  }
  
  public ParcelableEventList[] a(int paramInt)
  {
    return new ParcelableEventList[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/realtime/internal/event/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */