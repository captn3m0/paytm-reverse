package com.google.android.gms.drive.realtime.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.drive.realtime.internal.event.ParcelableEvent;
import java.util.ArrayList;

public class zzp
  implements Parcelable.Creator<ParcelableChangeInfo>
{
  static void a(ParcelableChangeInfo paramParcelableChangeInfo, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramParcelableChangeInfo.a);
    zzb.a(paramParcel, 2, paramParcelableChangeInfo.b);
    zzb.c(paramParcel, 3, paramParcelableChangeInfo.c, false);
    zzb.a(paramParcel, paramInt);
  }
  
  public ParcelableChangeInfo a(Parcel paramParcel)
  {
    int j = zza.b(paramParcel);
    int i = 0;
    long l = 0L;
    ArrayList localArrayList = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        l = zza.i(paramParcel, k);
        break;
      case 3: 
        localArrayList = zza.c(paramParcel, k, ParcelableEvent.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new ParcelableChangeInfo(i, l, localArrayList);
  }
  
  public ParcelableChangeInfo[] a(int paramInt)
  {
    return new ParcelableChangeInfo[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/realtime/internal/zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */