package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class zzb
  implements Parcelable.Creator<Contents>
{
  static void a(Contents paramContents, Parcel paramParcel, int paramInt)
  {
    int i = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramContents.a);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramContents.b, paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 3, paramContents.c);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 4, paramContents.d);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 5, paramContents.e, paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 7, paramContents.f);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 8, paramContents.g, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, i);
  }
  
  public Contents a(Parcel paramParcel)
  {
    String str = null;
    boolean bool = false;
    int m = zza.b(paramParcel);
    DriveId localDriveId = null;
    int i = 0;
    int j = 0;
    ParcelFileDescriptor localParcelFileDescriptor = null;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      case 6: 
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        localParcelFileDescriptor = (ParcelFileDescriptor)zza.a(paramParcel, n, ParcelFileDescriptor.CREATOR);
        break;
      case 3: 
        j = zza.g(paramParcel, n);
        break;
      case 4: 
        i = zza.g(paramParcel, n);
        break;
      case 5: 
        localDriveId = (DriveId)zza.a(paramParcel, n, DriveId.CREATOR);
        break;
      case 7: 
        bool = zza.c(paramParcel, n);
        break;
      case 8: 
        str = zza.p(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new Contents(k, localParcelFileDescriptor, j, i, localDriveId, bool, str);
  }
  
  public Contents[] a(int paramInt)
  {
    return new Contents[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */