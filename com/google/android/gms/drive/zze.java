package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zze
  implements Parcelable.Creator<DriveId>
{
  static void a(DriveId paramDriveId, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramDriveId.a);
    zzb.a(paramParcel, 2, paramDriveId.b, false);
    zzb.a(paramParcel, 3, paramDriveId.c);
    zzb.a(paramParcel, 4, paramDriveId.d);
    zzb.a(paramParcel, 5, paramDriveId.e);
    zzb.a(paramParcel, paramInt);
  }
  
  public DriveId a(Parcel paramParcel)
  {
    long l1 = 0L;
    int k = zza.b(paramParcel);
    int j = 0;
    String str = null;
    int i = -1;
    long l2 = 0L;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        str = zza.p(paramParcel, m);
        break;
      case 3: 
        l2 = zza.i(paramParcel, m);
        break;
      case 4: 
        l1 = zza.i(paramParcel, m);
        break;
      case 5: 
        i = zza.g(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new DriveId(j, str, l2, l1, i);
  }
  
  public DriveId[] a(int paramInt)
  {
    return new DriveId[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */