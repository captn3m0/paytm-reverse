package com.google.android.gms.drive;

import android.os.Bundle;
import com.google.android.gms.common.data.AbstractDataBuffer;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.internal.zzp;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.drive.metadata.internal.zze;
import com.google.android.gms.internal.zznm;
import java.util.Collection;
import java.util.Iterator;

public final class MetadataBuffer
  extends AbstractDataBuffer<Metadata>
{
  private zza b;
  
  public MetadataBuffer(DataHolder paramDataHolder)
  {
    super(paramDataHolder);
    paramDataHolder.f().setClassLoader(MetadataBuffer.class.getClassLoader());
  }
  
  public Metadata b(int paramInt)
  {
    zza localzza2 = this.b;
    zza localzza1;
    if (localzza2 != null)
    {
      localzza1 = localzza2;
      if (zza.a(localzza2) == paramInt) {}
    }
    else
    {
      localzza1 = new zza(this.a, paramInt);
      this.b = localzza1;
    }
    return localzza1;
  }
  
  public void release()
  {
    if (this.a != null) {
      zze.a(this.a);
    }
    super.release();
  }
  
  private static class zza
    extends Metadata
  {
    private final DataHolder a;
    private final int b;
    private final int c;
    
    public zza(DataHolder paramDataHolder, int paramInt)
    {
      this.a = paramDataHolder;
      this.b = paramInt;
      this.c = paramDataHolder.a(paramInt);
    }
    
    public <T> T a(MetadataField<T> paramMetadataField)
    {
      return (T)paramMetadataField.a(this.a, this.b, this.c);
    }
    
    public Metadata c()
    {
      MetadataBundle localMetadataBundle = MetadataBundle.a();
      Iterator localIterator = zze.a().iterator();
      while (localIterator.hasNext())
      {
        MetadataField localMetadataField = (MetadataField)localIterator.next();
        if (localMetadataField != zznm.F) {
          localMetadataField.a(this.a, localMetadataBundle, this.b, this.c);
        }
      }
      return new zzp(localMetadataBundle);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/MetadataBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */