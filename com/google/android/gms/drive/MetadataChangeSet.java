package com.google.android.gms.drive;

import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public final class MetadataChangeSet
{
  public static final MetadataChangeSet a = new MetadataChangeSet(MetadataBundle.a());
  private final MetadataBundle b;
  
  public MetadataChangeSet(MetadataBundle paramMetadataBundle)
  {
    this.b = paramMetadataBundle.b();
  }
  
  public MetadataBundle a()
  {
    return this.b;
  }
  
  public static class Builder
  {
    private final MetadataBundle a = MetadataBundle.a();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/MetadataChangeSet.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */