package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;

public class Permission
  implements SafeParcelable
{
  public static final Parcelable.Creator<Permission> CREATOR = new zzj();
  final int a;
  private String b;
  private int c;
  private String d;
  private String e;
  private int f;
  private boolean g;
  
  Permission(int paramInt1, String paramString1, int paramInt2, String paramString2, String paramString3, int paramInt3, boolean paramBoolean)
  {
    this.a = paramInt1;
    this.b = paramString1;
    this.c = paramInt2;
    this.d = paramString2;
    this.e = paramString3;
    this.f = paramInt3;
    this.g = paramBoolean;
  }
  
  public static boolean a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return false;
    }
    return true;
  }
  
  public static boolean b(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return false;
    }
    return true;
  }
  
  public String a()
  {
    if (!a(this.c)) {
      return null;
    }
    return this.b;
  }
  
  public int b()
  {
    if (!a(this.c)) {
      return -1;
    }
    return this.c;
  }
  
  public String c()
  {
    return this.d;
  }
  
  public String d()
  {
    return this.e;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public int e()
  {
    if (!b(this.f)) {
      return -1;
    }
    return this.f;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (Permission)paramObject;
      if ((!zzw.a(this.b, ((Permission)paramObject).b)) || (this.c != ((Permission)paramObject).c) || (this.f != ((Permission)paramObject).f)) {
        break;
      }
      bool1 = bool2;
    } while (this.g == ((Permission)paramObject).g);
    return false;
  }
  
  public boolean f()
  {
    return this.g;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b, Integer.valueOf(this.c), Integer.valueOf(this.f), Boolean.valueOf(this.g) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzj.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/Permission.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */