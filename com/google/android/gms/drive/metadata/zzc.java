package com.google.android.gms.drive.metadata;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzc
  implements Parcelable.Creator<CustomPropertyKey>
{
  static void a(CustomPropertyKey paramCustomPropertyKey, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramCustomPropertyKey.a);
    zzb.a(paramParcel, 2, paramCustomPropertyKey.b, false);
    zzb.a(paramParcel, 3, paramCustomPropertyKey.c);
    zzb.a(paramParcel, paramInt);
  }
  
  public CustomPropertyKey a(Parcel paramParcel)
  {
    int j = 0;
    int k = zza.b(paramParcel);
    String str = null;
    int i = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        i = zza.g(paramParcel, m);
        break;
      case 2: 
        str = zza.p(paramParcel, m);
        break;
      case 3: 
        j = zza.g(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new CustomPropertyKey(i, str, j);
  }
  
  public CustomPropertyKey[] a(int paramInt)
  {
    return new CustomPropertyKey[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/metadata/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */