package com.google.android.gms.drive.metadata;

import android.os.Bundle;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public abstract class zza<T>
  implements MetadataField<T>
{
  private final String a;
  private final Set<String> b;
  private final Set<String> c;
  private final int d;
  
  protected zza(String paramString, int paramInt)
  {
    this.a = ((String)zzx.a(paramString, "fieldName"));
    this.b = Collections.singleton(paramString);
    this.c = Collections.emptySet();
    this.d = paramInt;
  }
  
  protected zza(String paramString, Collection<String> paramCollection1, Collection<String> paramCollection2, int paramInt)
  {
    this.a = ((String)zzx.a(paramString, "fieldName"));
    this.b = Collections.unmodifiableSet(new HashSet(paramCollection1));
    this.c = Collections.unmodifiableSet(new HashSet(paramCollection2));
    this.d = paramInt;
  }
  
  public final T a(Bundle paramBundle)
  {
    zzx.a(paramBundle, "bundle");
    if (paramBundle.get(a()) != null) {
      return (T)c(paramBundle);
    }
    return null;
  }
  
  public final T a(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    if (b(paramDataHolder, paramInt1, paramInt2)) {
      return (T)c(paramDataHolder, paramInt1, paramInt2);
    }
    return null;
  }
  
  public final String a()
  {
    return this.a;
  }
  
  protected abstract void a(Bundle paramBundle, T paramT);
  
  public final void a(DataHolder paramDataHolder, MetadataBundle paramMetadataBundle, int paramInt1, int paramInt2)
  {
    zzx.a(paramDataHolder, "dataHolder");
    zzx.a(paramMetadataBundle, "bundle");
    if (b(paramDataHolder, paramInt1, paramInt2)) {
      paramMetadataBundle.a(this, c(paramDataHolder, paramInt1, paramInt2));
    }
  }
  
  public final void a(T paramT, Bundle paramBundle)
  {
    zzx.a(paramBundle, "bundle");
    if (paramT == null)
    {
      paramBundle.putString(a(), null);
      return;
    }
    a(paramBundle, paramT);
  }
  
  public final Collection<String> b()
  {
    return this.b;
  }
  
  protected boolean b(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    Iterator localIterator = this.b.iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      if ((!paramDataHolder.a(str)) || (paramDataHolder.h(str, paramInt1, paramInt2))) {
        return false;
      }
    }
    return true;
  }
  
  protected abstract T c(Bundle paramBundle);
  
  protected abstract T c(DataHolder paramDataHolder, int paramInt1, int paramInt2);
  
  public String toString()
  {
    return this.a;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/metadata/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */