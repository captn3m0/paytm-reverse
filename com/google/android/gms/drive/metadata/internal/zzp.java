package com.google.android.gms.drive.metadata.internal;

import android.os.Bundle;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.zza;

public class zzp
  extends zza<String>
{
  public zzp(String paramString, int paramInt)
  {
    super(paramString, paramInt);
  }
  
  protected void a(Bundle paramBundle, String paramString)
  {
    paramBundle.putString(a(), paramString);
  }
  
  protected String b(Bundle paramBundle)
  {
    return paramBundle.getString(a());
  }
  
  protected String f_(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    return paramDataHolder.c(a(), paramInt1, paramInt2);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/metadata/internal/zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */