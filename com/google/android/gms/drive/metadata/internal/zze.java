package com.google.android.gms.drive.metadata.internal;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.internal.zznm;
import com.google.android.gms.internal.zznn;
import com.google.android.gms.internal.zzno;
import com.google.android.gms.internal.zznq;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public final class zze
{
  private static final Map<String, MetadataField<?>> a = new HashMap();
  private static final Map<String, zza> b = new HashMap();
  
  static
  {
    a(zznm.a);
    a(zznm.G);
    a(zznm.x);
    a(zznm.E);
    a(zznm.H);
    a(zznm.n);
    a(zznm.m);
    a(zznm.o);
    a(zznm.p);
    a(zznm.q);
    a(zznm.k);
    a(zznm.s);
    a(zznm.t);
    a(zznm.u);
    a(zznm.C);
    a(zznm.b);
    a(zznm.z);
    a(zznm.d);
    a(zznm.l);
    a(zznm.e);
    a(zznm.f);
    a(zznm.g);
    a(zznm.h);
    a(zznm.w);
    a(zznm.r);
    a(zznm.y);
    a(zznm.A);
    a(zznm.B);
    a(zznm.D);
    a(zznm.I);
    a(zznm.J);
    a(zznm.j);
    a(zznm.i);
    a(zznm.F);
    a(zznm.v);
    a(zznm.c);
    a(zznm.K);
    a(zznm.L);
    a(zznm.M);
    a(zznm.N);
    a(zznm.O);
    a(zznm.P);
    a(zznm.Q);
    a(zzno.a);
    a(zzno.c);
    a(zzno.d);
    a(zzno.e);
    a(zzno.b);
    a(zzno.f);
    a(zznq.a);
    a(zznq.b);
    zzm localzzm = zznm.C;
    a(zzm.a);
    a(zznn.a);
  }
  
  public static MetadataField<?> a(String paramString)
  {
    return (MetadataField)a.get(paramString);
  }
  
  public static Collection<MetadataField<?>> a()
  {
    return Collections.unmodifiableCollection(a.values());
  }
  
  public static void a(DataHolder paramDataHolder)
  {
    Iterator localIterator = b.values().iterator();
    while (localIterator.hasNext()) {
      ((zza)localIterator.next()).a(paramDataHolder);
    }
  }
  
  private static void a(MetadataField<?> paramMetadataField)
  {
    if (a.containsKey(paramMetadataField.a())) {
      throw new IllegalArgumentException("Duplicate field name registered: " + paramMetadataField.a());
    }
    a.put(paramMetadataField.a(), paramMetadataField);
  }
  
  private static void a(zza paramzza)
  {
    if (b.put(paramzza.a(), paramzza) != null) {
      throw new IllegalStateException("A cleaner for key " + paramzza.a() + " has already been registered");
    }
  }
  
  public static abstract interface zza
  {
    public abstract String a();
    
    public abstract void a(DataHolder paramDataHolder);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/metadata/internal/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */