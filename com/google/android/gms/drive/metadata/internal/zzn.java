package com.google.android.gms.drive.metadata.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzn
  implements Parcelable.Creator<PartialDriveId>
{
  static void a(PartialDriveId paramPartialDriveId, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramPartialDriveId.a);
    zzb.a(paramParcel, 2, paramPartialDriveId.b, false);
    zzb.a(paramParcel, 3, paramPartialDriveId.c);
    zzb.a(paramParcel, 4, paramPartialDriveId.d);
    zzb.a(paramParcel, paramInt);
  }
  
  public PartialDriveId a(Parcel paramParcel)
  {
    int k = zza.b(paramParcel);
    int j = 0;
    String str = null;
    long l = 0L;
    int i = -1;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        str = zza.p(paramParcel, m);
        break;
      case 3: 
        l = zza.i(paramParcel, m);
        break;
      case 4: 
        i = zza.g(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new PartialDriveId(j, str, l, i);
  }
  
  public PartialDriveId[] a(int paramInt)
  {
    return new PartialDriveId[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/metadata/internal/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */