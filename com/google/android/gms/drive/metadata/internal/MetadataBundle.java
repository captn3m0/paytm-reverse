package com.google.android.gms.drive.metadata.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.drive.internal.zzz;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.internal.zznm;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class MetadataBundle
  implements SafeParcelable
{
  public static final Parcelable.Creator<MetadataBundle> CREATOR = new zzh();
  final int a;
  final Bundle b;
  
  MetadataBundle(int paramInt, Bundle paramBundle)
  {
    this.a = paramInt;
    this.b = ((Bundle)zzx.a(paramBundle));
    this.b.setClassLoader(getClass().getClassLoader());
    paramBundle = new ArrayList();
    Object localObject = this.b.keySet().iterator();
    while (((Iterator)localObject).hasNext())
    {
      String str = (String)((Iterator)localObject).next();
      if (zze.a(str) == null)
      {
        paramBundle.add(str);
        zzz.b("MetadataBundle", "Ignored unknown metadata field in bundle: " + str);
      }
    }
    paramBundle = paramBundle.iterator();
    while (paramBundle.hasNext())
    {
      localObject = (String)paramBundle.next();
      this.b.remove((String)localObject);
    }
  }
  
  private MetadataBundle(Bundle paramBundle)
  {
    this(1, paramBundle);
  }
  
  public static MetadataBundle a()
  {
    return new MetadataBundle(new Bundle());
  }
  
  public <T> T a(MetadataField<T> paramMetadataField)
  {
    return (T)paramMetadataField.a(this.b);
  }
  
  public void a(Context paramContext)
  {
    BitmapTeleporter localBitmapTeleporter = (BitmapTeleporter)a(zznm.F);
    if (localBitmapTeleporter != null) {
      localBitmapTeleporter.a(paramContext.getCacheDir());
    }
  }
  
  public <T> void a(MetadataField<T> paramMetadataField, T paramT)
  {
    if (zze.a(paramMetadataField.a()) == null) {
      throw new IllegalArgumentException("Unregistered field: " + paramMetadataField.a());
    }
    paramMetadataField.a(paramT, this.b);
  }
  
  public MetadataBundle b()
  {
    return new MetadataBundle(new Bundle(this.b));
  }
  
  public Set<MetadataField<?>> c()
  {
    HashSet localHashSet = new HashSet();
    Iterator localIterator = this.b.keySet().iterator();
    while (localIterator.hasNext()) {
      localHashSet.add(zze.a((String)localIterator.next()));
    }
    return localHashSet;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof MetadataBundle)) {
      return false;
    }
    paramObject = (MetadataBundle)paramObject;
    Object localObject = this.b.keySet();
    if (!((Set)localObject).equals(((MetadataBundle)paramObject).b.keySet())) {
      return false;
    }
    localObject = ((Set)localObject).iterator();
    while (((Iterator)localObject).hasNext())
    {
      String str = (String)((Iterator)localObject).next();
      if (!zzw.a(this.b.get(str), ((MetadataBundle)paramObject).b.get(str))) {
        return false;
      }
    }
    return true;
  }
  
  public int hashCode()
  {
    Iterator localIterator = this.b.keySet().iterator();
    String str;
    for (int i = 1; localIterator.hasNext(); i = this.b.get(str).hashCode() + i * 31) {
      str = (String)localIterator.next();
    }
    return i;
  }
  
  public String toString()
  {
    return "MetadataBundle [values=" + this.b + "]";
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzh.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/metadata/internal/MetadataBundle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */