package com.google.android.gms.drive.metadata.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.drive.metadata.CustomPropertyKey;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class AppVisibleCustomProperties
  implements SafeParcelable, Iterable<CustomProperty>
{
  public static final Parcelable.Creator<AppVisibleCustomProperties> CREATOR = new zza();
  public static final AppVisibleCustomProperties a = new zza().a();
  final int b;
  final List<CustomProperty> c;
  
  AppVisibleCustomProperties(int paramInt, Collection<CustomProperty> paramCollection)
  {
    this.b = paramInt;
    zzx.a(paramCollection);
    this.c = new ArrayList(paramCollection);
  }
  
  private AppVisibleCustomProperties(Collection<CustomProperty> paramCollection)
  {
    this(1, paramCollection);
  }
  
  public Map<CustomPropertyKey, String> a()
  {
    HashMap localHashMap = new HashMap(this.c.size());
    Iterator localIterator = this.c.iterator();
    while (localIterator.hasNext())
    {
      CustomProperty localCustomProperty = (CustomProperty)localIterator.next();
      localHashMap.put(localCustomProperty.a(), localCustomProperty.b());
    }
    return Collections.unmodifiableMap(localHashMap);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      return false;
    }
    return a().equals(((AppVisibleCustomProperties)paramObject).a());
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.c });
  }
  
  public Iterator<CustomProperty> iterator()
  {
    return this.c.iterator();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.a(this, paramParcel, paramInt);
  }
  
  public static class zza
  {
    private final Map<CustomPropertyKey, CustomProperty> a = new HashMap();
    
    public zza a(CustomProperty paramCustomProperty)
    {
      zzx.a(paramCustomProperty, "property");
      this.a.put(paramCustomProperty.a(), paramCustomProperty);
      return this;
    }
    
    public AppVisibleCustomProperties a()
    {
      return new AppVisibleCustomProperties(this.a.values(), null);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */