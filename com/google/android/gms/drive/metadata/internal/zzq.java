package com.google.android.gms.drive.metadata.internal;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.UserMetadata;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class zzq
  extends zzk<UserMetadata>
{
  public zzq(String paramString, int paramInt)
  {
    super(paramString, b(paramString), Collections.emptyList(), paramInt);
  }
  
  private String a(String paramString)
  {
    return a(a(), paramString);
  }
  
  private static String a(String paramString1, String paramString2)
  {
    return paramString1 + "." + paramString2;
  }
  
  private static Collection<String> b(String paramString)
  {
    return Arrays.asList(new String[] { a(paramString, "permissionId"), a(paramString, "displayName"), a(paramString, "picture"), a(paramString, "isAuthenticatedUser"), a(paramString, "emailAddress") });
  }
  
  protected boolean b(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    return (paramDataHolder.a(a("permissionId"))) && (!paramDataHolder.h(a("permissionId"), paramInt1, paramInt2));
  }
  
  protected UserMetadata d(DataHolder paramDataHolder, int paramInt1, int paramInt2)
  {
    String str1 = paramDataHolder.c(a("permissionId"), paramInt1, paramInt2);
    if (str1 != null)
    {
      String str2 = paramDataHolder.c(a("displayName"), paramInt1, paramInt2);
      String str3 = paramDataHolder.c(a("picture"), paramInt1, paramInt2);
      boolean bool = paramDataHolder.d(a("isAuthenticatedUser"), paramInt1, paramInt2);
      paramDataHolder = paramDataHolder.c(a("emailAddress"), paramInt1, paramInt2);
      return new UserMetadata(str1, str2, str3, Boolean.valueOf(bool).booleanValue(), paramDataHolder);
    }
    return null;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/metadata/internal/zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */