package com.google.android.gms.drive.metadata.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.DriveId;

public class PartialDriveId
  implements SafeParcelable
{
  public static final Parcelable.Creator<PartialDriveId> CREATOR = new zzn();
  final int a;
  final String b;
  final long c;
  final int d;
  
  PartialDriveId(int paramInt1, String paramString, long paramLong, int paramInt2)
  {
    this.a = paramInt1;
    this.b = paramString;
    this.c = paramLong;
    this.d = paramInt2;
  }
  
  public PartialDriveId(String paramString, long paramLong, int paramInt)
  {
    this(1, paramString, paramLong, paramInt);
  }
  
  public DriveId a(long paramLong)
  {
    return new DriveId(this.b, this.c, paramLong, this.d);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzn.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/metadata/internal/PartialDriveId.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */