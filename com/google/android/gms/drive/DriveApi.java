package com.google.android.gms.drive;

import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;

public abstract interface DriveApi
{
  public static abstract interface DriveContentsResult
    extends Result
  {}
  
  public static abstract interface DriveIdResult
    extends Result
  {}
  
  public static abstract interface MetadataBufferResult
    extends Releasable, Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/DriveApi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */