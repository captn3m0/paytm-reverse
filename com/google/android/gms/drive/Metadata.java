package com.google.android.gms.drive;

import com.google.android.gms.common.data.Freezable;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.internal.zznm;

public abstract class Metadata
  implements Freezable<Metadata>
{
  public abstract <T> T a(MetadataField<T> paramMetadataField);
  
  public DriveId b()
  {
    return (DriveId)a(zznm.a);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/Metadata.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */