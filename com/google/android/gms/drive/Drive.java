package com.google.android.gms.drive;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.common.api.Api.ApiOptions.Optional;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.drive.internal.zzaa;
import com.google.android.gms.drive.internal.zzac;
import com.google.android.gms.drive.internal.zzs;
import com.google.android.gms.drive.internal.zzu;
import com.google.android.gms.drive.internal.zzx;

public final class Drive
{
  public static final Api.zzc<zzu> a = new Api.zzc();
  public static final Scope b = new Scope("https://www.googleapis.com/auth/drive.file");
  public static final Scope c = new Scope("https://www.googleapis.com/auth/drive.appdata");
  public static final Scope d = new Scope("https://www.googleapis.com/auth/drive");
  public static final Scope e = new Scope("https://www.googleapis.com/auth/drive.apps");
  public static final Api<Api.ApiOptions.NoOptions> f = new Api("Drive.API", new zza()
  {
    protected Bundle a(Api.ApiOptions.NoOptions paramAnonymousNoOptions)
    {
      return new Bundle();
    }
  }, a);
  public static final Api<zzb> g = new Api("Drive.INTERNAL_API", new zza()
  {
    protected Bundle a(Drive.zzb paramAnonymouszzb)
    {
      if (paramAnonymouszzb == null) {
        return new Bundle();
      }
      return paramAnonymouszzb.a();
    }
  }, a);
  public static final DriveApi h = new zzs();
  public static final zzd i = new zzx();
  public static final zzf j = new zzac();
  public static final DrivePreferencesApi k = new zzaa();
  
  public static abstract class zza<O extends Api.ApiOptions>
    extends Api.zza<zzu, O>
  {
    protected abstract Bundle a(O paramO);
    
    public zzu a(Context paramContext, Looper paramLooper, com.google.android.gms.common.internal.zzf paramzzf, O paramO, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
    {
      return new zzu(paramContext, paramLooper, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener, a(paramO));
    }
  }
  
  public static class zzb
    implements Api.ApiOptions.Optional
  {
    private final Bundle a;
    
    public Bundle a()
    {
      return this.a;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/Drive.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */