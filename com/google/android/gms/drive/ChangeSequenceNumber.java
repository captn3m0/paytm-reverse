package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.Base64;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.drive.internal.zzas;
import com.google.android.gms.internal.zzsu;

public class ChangeSequenceNumber
  implements SafeParcelable
{
  public static final Parcelable.Creator<ChangeSequenceNumber> CREATOR = new zza();
  final int a;
  final long b;
  final long c;
  final long d;
  private volatile String e = null;
  
  ChangeSequenceNumber(int paramInt, long paramLong1, long paramLong2, long paramLong3)
  {
    if (paramLong1 != -1L)
    {
      bool1 = true;
      zzx.b(bool1);
      if (paramLong2 == -1L) {
        break label92;
      }
      bool1 = true;
      label40:
      zzx.b(bool1);
      if (paramLong3 == -1L) {
        break label98;
      }
    }
    label92:
    label98:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzx.b(bool1);
      this.a = paramInt;
      this.b = paramLong1;
      this.c = paramLong2;
      this.d = paramLong3;
      return;
      bool1 = false;
      break;
      bool1 = false;
      break label40;
    }
  }
  
  public final String a()
  {
    if (this.e == null)
    {
      String str = Base64.encodeToString(b(), 10);
      this.e = ("ChangeSequenceNumber:" + str);
    }
    return this.e;
  }
  
  final byte[] b()
  {
    zzas localzzas = new zzas();
    localzzas.a = this.a;
    localzzas.b = this.b;
    localzzas.c = this.c;
    localzzas.d = this.d;
    return zzsu.a(localzzas);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof ChangeSequenceNumber)) {}
    do
    {
      return false;
      paramObject = (ChangeSequenceNumber)paramObject;
    } while ((((ChangeSequenceNumber)paramObject).c != this.c) || (((ChangeSequenceNumber)paramObject).d != this.d) || (((ChangeSequenceNumber)paramObject).b != this.b));
    return true;
  }
  
  public int hashCode()
  {
    return (String.valueOf(this.b) + String.valueOf(this.c) + String.valueOf(this.d)).hashCode();
  }
  
  public String toString()
  {
    return a();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/ChangeSequenceNumber.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */