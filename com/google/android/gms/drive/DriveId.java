package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.Base64;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.drive.internal.zzat;
import com.google.android.gms.drive.internal.zzz;
import com.google.android.gms.internal.zzsu;

public class DriveId
  implements SafeParcelable
{
  public static final Parcelable.Creator<DriveId> CREATOR = new zze();
  final int a;
  final String b;
  final long c;
  final long d;
  final int e;
  private volatile String f = null;
  private volatile String g = null;
  
  DriveId(int paramInt1, String paramString, long paramLong1, long paramLong2, int paramInt2)
  {
    this.a = paramInt1;
    this.b = paramString;
    if (!"".equals(paramString)) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      zzx.b(bool1);
      if (paramString == null)
      {
        bool1 = bool2;
        if (paramLong1 == -1L) {}
      }
      else
      {
        bool1 = true;
      }
      zzx.b(bool1);
      this.c = paramLong1;
      this.d = paramLong2;
      this.e = paramInt2;
      return;
    }
  }
  
  public DriveId(String paramString, long paramLong1, long paramLong2, int paramInt)
  {
    this(1, paramString, paramLong1, paramLong2, paramInt);
  }
  
  public static DriveId a(String paramString)
  {
    zzx.a(paramString);
    return new DriveId(paramString, -1L, -1L, -1);
  }
  
  public final String a()
  {
    if (this.f == null)
    {
      String str = Base64.encodeToString(b(), 10);
      this.f = ("DriveId:" + str);
    }
    return this.f;
  }
  
  final byte[] b()
  {
    zzat localzzat = new zzat();
    localzzat.a = this.a;
    if (this.b == null) {}
    for (String str = "";; str = this.b)
    {
      localzzat.b = str;
      localzzat.c = this.c;
      localzzat.d = this.d;
      localzzat.e = this.e;
      return zzsu.a(localzzat);
    }
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (!(paramObject instanceof DriveId)) {}
    do
    {
      do
      {
        return false;
        paramObject = (DriveId)paramObject;
      } while (((DriveId)paramObject).d != this.d);
      if ((((DriveId)paramObject).c == -1L) && (this.c == -1L)) {
        return ((DriveId)paramObject).b.equals(this.b);
      }
      if ((this.b == null) || (((DriveId)paramObject).b == null))
      {
        if (((DriveId)paramObject).c == this.c) {}
        for (;;)
        {
          return bool;
          bool = false;
        }
      }
    } while (((DriveId)paramObject).c != this.c);
    if (((DriveId)paramObject).b.equals(this.b)) {
      return true;
    }
    zzz.b("DriveId", "Unexpected unequal resourceId for same DriveId object.");
    return false;
  }
  
  public int hashCode()
  {
    if (this.c == -1L) {
      return this.b.hashCode();
    }
    return (String.valueOf(this.d) + String.valueOf(this.c)).hashCode();
  }
  
  public String toString()
  {
    return a();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zze.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/DriveId.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */