package com.google.android.gms.drive.query.internal;

import android.os.Parcel;

public class FullTextSearchFilter
  extends AbstractFilter
{
  public static final zzh CREATOR = new zzh();
  final String a;
  final int b;
  
  FullTextSearchFilter(int paramInt, String paramString)
  {
    this.b = paramInt;
    this.a = paramString;
  }
  
  public <F> F a(zzf<F> paramzzf)
  {
    return (F)paramzzf.a(this.a);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzh.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/query/internal/FullTextSearchFilter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */