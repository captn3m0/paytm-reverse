package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.drive.metadata.zzb;
import java.util.Collection;
import java.util.Iterator;

public class InFilter<T>
  extends AbstractFilter
{
  public static final zzj CREATOR = new zzj();
  final MetadataBundle a;
  final int b;
  private final zzb<T> c;
  
  InFilter(int paramInt, MetadataBundle paramMetadataBundle)
  {
    this.b = paramInt;
    this.a = paramMetadataBundle;
    this.c = ((zzb)zze.a(paramMetadataBundle));
  }
  
  public T a()
  {
    return (T)((Collection)this.a.a(this.c)).iterator().next();
  }
  
  public <F> F a(zzf<F> paramzzf)
  {
    return (F)paramzzf.a(this.c, a());
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzj.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/query/internal/InFilter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */