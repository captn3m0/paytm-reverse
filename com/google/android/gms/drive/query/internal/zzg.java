package com.google.android.gms.drive.query.internal;

import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.zzb;
import java.util.List;

public class zzg
  implements zzf<Boolean>
{
  private Boolean a = Boolean.valueOf(false);
  
  public Boolean a(Boolean paramBoolean)
  {
    return this.a;
  }
  
  public Boolean b(MetadataField<?> paramMetadataField)
  {
    return this.a;
  }
  
  public <T> Boolean b(MetadataField<T> paramMetadataField, T paramT)
  {
    return this.a;
  }
  
  public <T> Boolean b(zzb<T> paramzzb, T paramT)
  {
    return this.a;
  }
  
  public <T> Boolean b(Operator paramOperator, MetadataField<T> paramMetadataField, T paramT)
  {
    return this.a;
  }
  
  public Boolean b(Operator paramOperator, List<Boolean> paramList)
  {
    return this.a;
  }
  
  public Boolean b(String paramString)
  {
    if (!paramString.isEmpty()) {
      this.a = Boolean.valueOf(true);
    }
    return this.a;
  }
  
  public Boolean c()
  {
    return this.a;
  }
  
  public Boolean d()
  {
    return this.a;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/query/internal/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */