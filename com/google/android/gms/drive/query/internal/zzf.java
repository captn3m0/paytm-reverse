package com.google.android.gms.drive.query.internal;

import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.zzb;
import java.util.List;

public abstract interface zzf<F>
{
  public abstract F a();
  
  public abstract F a(MetadataField<?> paramMetadataField);
  
  public abstract <T> F a(MetadataField<T> paramMetadataField, T paramT);
  
  public abstract <T> F a(zzb<T> paramzzb, T paramT);
  
  public abstract <T> F a(Operator paramOperator, MetadataField<T> paramMetadataField, T paramT);
  
  public abstract F a(Operator paramOperator, List<F> paramList);
  
  public abstract F a(F paramF);
  
  public abstract F a(String paramString);
  
  public abstract F b();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/query/internal/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */