package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public class ComparisonFilter<T>
  extends AbstractFilter
{
  public static final zza CREATOR = new zza();
  final Operator a;
  final MetadataBundle b;
  final int c;
  final MetadataField<T> d;
  
  ComparisonFilter(int paramInt, Operator paramOperator, MetadataBundle paramMetadataBundle)
  {
    this.c = paramInt;
    this.a = paramOperator;
    this.b = paramMetadataBundle;
    this.d = zze.a(paramMetadataBundle);
  }
  
  public T a()
  {
    return (T)this.b.a(this.d);
  }
  
  public <F> F a(zzf<F> paramzzf)
  {
    return (F)paramzzf.a(this.a, this.d, a());
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/query/internal/ComparisonFilter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */