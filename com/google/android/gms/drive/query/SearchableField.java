package com.google.android.gms.drive.query;

import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.SearchableCollectionMetadataField;
import com.google.android.gms.drive.metadata.SearchableMetadataField;
import com.google.android.gms.drive.metadata.SearchableOrderedMetadataField;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties;
import com.google.android.gms.internal.zznm;
import com.google.android.gms.internal.zzno;
import java.util.Date;

public class SearchableField
{
  public static final SearchableMetadataField<String> a = zznm.G;
  public static final SearchableMetadataField<String> b = zznm.x;
  public static final SearchableMetadataField<Boolean> c = zznm.H;
  public static final SearchableCollectionMetadataField<DriveId> d = zznm.C;
  public static final SearchableOrderedMetadataField<Date> e = zzno.e;
  public static final SearchableMetadataField<Boolean> f = zznm.E;
  public static final SearchableOrderedMetadataField<Date> g = zzno.c;
  public static final SearchableOrderedMetadataField<Date> h = zzno.b;
  public static final SearchableMetadataField<Boolean> i = zznm.p;
  public static final SearchableMetadataField<AppVisibleCustomProperties> j = zznm.c;
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/query/SearchableField.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */