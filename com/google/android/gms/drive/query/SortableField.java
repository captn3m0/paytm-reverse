package com.google.android.gms.drive.query;

import com.google.android.gms.drive.metadata.SortableMetadataField;
import com.google.android.gms.internal.zznm;
import com.google.android.gms.internal.zzno;
import java.util.Date;

public class SortableField
{
  public static final SortableMetadataField<String> a = zznm.G;
  public static final SortableMetadataField<Date> b = zzno.a;
  public static final SortableMetadataField<Date> c = zzno.c;
  public static final SortableMetadataField<Date> d = zzno.d;
  public static final SortableMetadataField<Date> e = zzno.b;
  public static final SortableMetadataField<Date> f = zzno.e;
  public static final SortableMetadataField<Long> g = zznm.D;
  public static final SortableMetadataField<Date> h = zzno.f;
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/query/SortableField.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */