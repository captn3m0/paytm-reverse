package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzl
  implements Parcelable.Creator<UserMetadata>
{
  static void a(UserMetadata paramUserMetadata, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramUserMetadata.a);
    zzb.a(paramParcel, 2, paramUserMetadata.b, false);
    zzb.a(paramParcel, 3, paramUserMetadata.c, false);
    zzb.a(paramParcel, 4, paramUserMetadata.d, false);
    zzb.a(paramParcel, 5, paramUserMetadata.e);
    zzb.a(paramParcel, 6, paramUserMetadata.f, false);
    zzb.a(paramParcel, paramInt);
  }
  
  public UserMetadata a(Parcel paramParcel)
  {
    boolean bool = false;
    String str1 = null;
    int j = zza.b(paramParcel);
    String str2 = null;
    String str3 = null;
    String str4 = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str4 = zza.p(paramParcel, k);
        break;
      case 3: 
        str3 = zza.p(paramParcel, k);
        break;
      case 4: 
        str2 = zza.p(paramParcel, k);
        break;
      case 5: 
        bool = zza.c(paramParcel, k);
        break;
      case 6: 
        str1 = zza.p(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new UserMetadata(i, str4, str3, str2, bool, str1);
  }
  
  public UserMetadata[] a(int paramInt)
  {
    return new UserMetadata[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */