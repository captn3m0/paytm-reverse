package com.google.android.gms.drive;

import com.google.android.gms.common.api.Result;

public abstract interface DriveFolder
  extends DriveResource
{
  public static abstract interface DriveFileResult
    extends Result
  {}
  
  public static abstract interface DriveFolderResult
    extends Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/DriveFolder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */