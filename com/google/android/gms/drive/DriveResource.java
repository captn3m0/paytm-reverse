package com.google.android.gms.drive;

import com.google.android.gms.common.api.Result;

public abstract interface DriveResource
{
  public static abstract interface MetadataResult
    extends Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/DriveResource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */