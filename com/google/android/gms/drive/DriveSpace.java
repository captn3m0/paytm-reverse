package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzmr;
import java.util.Set;
import java.util.regex.Pattern;

public class DriveSpace
  implements SafeParcelable
{
  public static final Parcelable.Creator<DriveSpace> CREATOR = new zzg();
  public static final DriveSpace a = new DriveSpace("DRIVE");
  public static final DriveSpace b = new DriveSpace("APP_DATA_FOLDER");
  public static final DriveSpace c = new DriveSpace("PHOTOS");
  public static final Set<DriveSpace> d = zzmr.a(a, b, c);
  public static final String e = TextUtils.join(",", d.toArray());
  private static final Pattern g = Pattern.compile("[A-Z0-9_]*");
  final int f;
  private final String h;
  
  DriveSpace(int paramInt, String paramString)
  {
    this.f = paramInt;
    this.h = ((String)zzx.a(paramString));
  }
  
  private DriveSpace(String paramString)
  {
    this(1, paramString);
  }
  
  public String a()
  {
    return this.h;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject == null) || (paramObject.getClass() != DriveSpace.class)) {
      return false;
    }
    return this.h.equals(((DriveSpace)paramObject).h);
  }
  
  public int hashCode()
  {
    return 0x4A54C0DE ^ this.h.hashCode();
  }
  
  public String toString()
  {
    return this.h;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzg.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/DriveSpace.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */