package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzj
  implements Parcelable.Creator<Permission>
{
  static void a(Permission paramPermission, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramPermission.a);
    zzb.a(paramParcel, 2, paramPermission.a(), false);
    zzb.a(paramParcel, 3, paramPermission.b());
    zzb.a(paramParcel, 4, paramPermission.c(), false);
    zzb.a(paramParcel, 5, paramPermission.d(), false);
    zzb.a(paramParcel, 6, paramPermission.e());
    zzb.a(paramParcel, 7, paramPermission.f());
    zzb.a(paramParcel, paramInt);
  }
  
  public Permission a(Parcel paramParcel)
  {
    String str1 = null;
    boolean bool = false;
    int m = zza.b(paramParcel);
    int i = 0;
    String str2 = null;
    int j = 0;
    String str3 = null;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        str3 = zza.p(paramParcel, n);
        break;
      case 3: 
        j = zza.g(paramParcel, n);
        break;
      case 4: 
        str2 = zza.p(paramParcel, n);
        break;
      case 5: 
        str1 = zza.p(paramParcel, n);
        break;
      case 6: 
        i = zza.g(paramParcel, n);
        break;
      case 7: 
        bool = zza.c(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new Permission(k, str3, j, str2, str1, i, bool);
  }
  
  public Permission[] a(int paramInt)
  {
    return new Permission[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */