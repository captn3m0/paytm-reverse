package com.google.android.gms.drive;

import com.google.android.gms.common.internal.zzw;

public class ExecutionOptions
{
  private final String a;
  private final boolean b;
  private final int c;
  
  public String a()
  {
    return this.a;
  }
  
  public boolean b()
  {
    return this.b;
  }
  
  public int c()
  {
    return this.c;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramObject == null) || (paramObject.getClass() != getClass())) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
        bool1 = bool2;
      } while (paramObject == this);
      paramObject = (ExecutionOptions)paramObject;
      if ((!zzw.a(this.a, ((ExecutionOptions)paramObject).a)) || (this.c != ((ExecutionOptions)paramObject).c)) {
        break;
      }
      bool1 = bool2;
    } while (this.b == ((ExecutionOptions)paramObject).b);
    return false;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.a, Integer.valueOf(this.c), Boolean.valueOf(this.b) });
  }
  
  public static class Builder
  {
    protected int a = 0;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/ExecutionOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */