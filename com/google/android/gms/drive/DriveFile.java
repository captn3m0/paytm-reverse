package com.google.android.gms.drive;

public abstract interface DriveFile
  extends DriveResource
{
  public static abstract interface DownloadProgressListener
  {
    public abstract void a(long paramLong1, long paramLong2);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/drive/DriveFile.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */