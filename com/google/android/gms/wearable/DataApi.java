package com.google.android.gms.wearable;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract interface DataApi
{
  public abstract PendingResult<Status> a(GoogleApiClient paramGoogleApiClient, DataListener paramDataListener);
  
  public abstract PendingResult<DataItemResult> a(GoogleApiClient paramGoogleApiClient, PutDataRequest paramPutDataRequest);
  
  public abstract PendingResult<Status> b(GoogleApiClient paramGoogleApiClient, DataListener paramDataListener);
  
  public static abstract interface DataItemResult
    extends Result
  {}
  
  public static abstract interface DataListener
  {
    public abstract void a(DataEventBuffer paramDataEventBuffer);
  }
  
  public static abstract interface DeleteDataItemsResult
    extends Result
  {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface FilterType {}
  
  public static abstract interface GetFdForAssetResult
    extends Releasable, Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/wearable/DataApi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */