package com.google.android.gms.wearable;

import com.google.android.gms.common.api.Result;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract interface ChannelApi
{
  public static abstract interface ChannelListener
  {
    public abstract void a(Channel paramChannel);
    
    public abstract void a(Channel paramChannel, int paramInt1, int paramInt2);
    
    public abstract void b(Channel paramChannel, int paramInt1, int paramInt2);
    
    public abstract void c(Channel paramChannel, int paramInt1, int paramInt2);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface CloseReason {}
  
  public static abstract interface OpenChannelResult
    extends Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/wearable/ChannelApi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */