package com.google.android.gms.wearable;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract interface MessageApi
{
  public abstract PendingResult<Status> a(GoogleApiClient paramGoogleApiClient, MessageListener paramMessageListener);
  
  public abstract PendingResult<SendMessageResult> a(GoogleApiClient paramGoogleApiClient, String paramString1, String paramString2, byte[] paramArrayOfByte);
  
  public abstract PendingResult<Status> b(GoogleApiClient paramGoogleApiClient, MessageListener paramMessageListener);
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface FilterType {}
  
  public static abstract interface MessageListener
  {
    public abstract void a(MessageEvent paramMessageEvent);
  }
  
  public static abstract interface SendMessageResult
    extends Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/wearable/MessageApi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */