package com.google.android.gms.wearable;

import com.google.android.gms.common.api.Result;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract interface CapabilityApi
{
  public static abstract interface AddLocalCapabilityResult
    extends Result
  {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface CapabilityFilterType {}
  
  public static abstract interface CapabilityListener
  {
    public abstract void a(CapabilityInfo paramCapabilityInfo);
  }
  
  public static abstract interface GetAllCapabilitiesResult
    extends Result
  {}
  
  public static abstract interface GetCapabilityResult
    extends Result
  {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface NodeFilterType {}
  
  public static abstract interface RemoveLocalCapabilityResult
    extends Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/wearable/CapabilityApi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */