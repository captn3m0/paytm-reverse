package com.google.android.gms.wearable.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;

public abstract class zza
  extends zzav.zza
{
  public void a(Status paramStatus)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(DataHolder paramDataHolder)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(AddLocalCapabilityResponse paramAddLocalCapabilityResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(ChannelReceiveFileResponse paramChannelReceiveFileResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(ChannelSendFileResponse paramChannelSendFileResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(CloseChannelResponse paramCloseChannelResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(DeleteDataItemsResponse paramDeleteDataItemsResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(GetAllCapabilitiesResponse paramGetAllCapabilitiesResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(GetCapabilityResponse paramGetCapabilityResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(GetChannelInputStreamResponse paramGetChannelInputStreamResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(GetChannelOutputStreamResponse paramGetChannelOutputStreamResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  @Deprecated
  public void a(GetCloudSyncOptInOutDoneResponse paramGetCloudSyncOptInOutDoneResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(GetCloudSyncOptInStatusResponse paramGetCloudSyncOptInStatusResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(GetCloudSyncSettingResponse paramGetCloudSyncSettingResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(GetConfigResponse paramGetConfigResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(GetConfigsResponse paramGetConfigsResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(GetConnectedNodesResponse paramGetConnectedNodesResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(GetDataItemResponse paramGetDataItemResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(GetFdForAssetResponse paramGetFdForAssetResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(GetLocalNodeResponse paramGetLocalNodeResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(OpenChannelResponse paramOpenChannelResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(PutDataResponse paramPutDataResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(RemoveLocalCapabilityResponse paramRemoveLocalCapabilityResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(SendMessageResponse paramSendMessageResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(StorageInfoResponse paramStorageInfoResponse)
  {
    throw new UnsupportedOperationException();
  }
  
  public void b(CloseChannelResponse paramCloseChannelResponse)
  {
    throw new UnsupportedOperationException();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/wearable/internal/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */