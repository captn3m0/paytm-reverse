package com.google.android.gms.wearable;

import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.wearable.internal.DataItemAssetParcelable;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class PutDataRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<PutDataRequest> CREATOR = new zzh();
  private static final long b = TimeUnit.MINUTES.toMillis(30L);
  private static final Random c = new SecureRandom();
  final int a;
  private final Uri d;
  private final Bundle e;
  private byte[] f;
  private long g;
  
  private PutDataRequest(int paramInt, Uri paramUri)
  {
    this(paramInt, paramUri, new Bundle(), null, b);
  }
  
  PutDataRequest(int paramInt, Uri paramUri, Bundle paramBundle, byte[] paramArrayOfByte, long paramLong)
  {
    this.a = paramInt;
    this.d = paramUri;
    this.e = paramBundle;
    this.e.setClassLoader(DataItemAssetParcelable.class.getClassLoader());
    this.f = paramArrayOfByte;
    this.g = paramLong;
  }
  
  public static PutDataRequest a(Uri paramUri)
  {
    return new PutDataRequest(2, paramUri);
  }
  
  public static PutDataRequest a(String paramString)
  {
    return a(b(paramString));
  }
  
  private static Uri b(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      throw new IllegalArgumentException("An empty path was supplied.");
    }
    if (!paramString.startsWith("/")) {
      throw new IllegalArgumentException("A path must start with a single / .");
    }
    if (paramString.startsWith("//")) {
      throw new IllegalArgumentException("A path must start with a single / .");
    }
    return new Uri.Builder().scheme("wear").path(paramString).build();
  }
  
  public Uri a()
  {
    return this.d;
  }
  
  public PutDataRequest a(String paramString, Asset paramAsset)
  {
    zzx.a(paramString);
    zzx.a(paramAsset);
    this.e.putParcelable(paramString, paramAsset);
    return this;
  }
  
  public PutDataRequest a(byte[] paramArrayOfByte)
  {
    this.f = paramArrayOfByte;
    return this;
  }
  
  public String a(boolean paramBoolean)
  {
    StringBuilder localStringBuilder = new StringBuilder("PutDataRequest[");
    Object localObject2 = new StringBuilder().append("dataSz=");
    if (this.f == null) {}
    for (Object localObject1 = "null";; localObject1 = Integer.valueOf(this.f.length))
    {
      localStringBuilder.append(localObject1);
      localStringBuilder.append(", numAssets=" + this.e.size());
      localStringBuilder.append(", uri=" + this.d);
      localStringBuilder.append(", syncDeadline=" + this.g);
      if (paramBoolean) {
        break;
      }
      localStringBuilder.append("]");
      return localStringBuilder.toString();
    }
    localStringBuilder.append("]\n  assets: ");
    localObject1 = this.e.keySet().iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (String)((Iterator)localObject1).next();
      localStringBuilder.append("\n    " + (String)localObject2 + ": " + this.e.getParcelable((String)localObject2));
    }
    localStringBuilder.append("\n  ]");
    return localStringBuilder.toString();
  }
  
  public byte[] b()
  {
    return this.f;
  }
  
  public Map<String, Asset> c()
  {
    HashMap localHashMap = new HashMap();
    Iterator localIterator = this.e.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      localHashMap.put(str, (Asset)this.e.getParcelable(str));
    }
    return Collections.unmodifiableMap(localHashMap);
  }
  
  public Bundle d()
  {
    return this.e;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public long e()
  {
    return this.g;
  }
  
  public boolean f()
  {
    return this.g == 0L;
  }
  
  public PutDataRequest g()
  {
    this.g = 0L;
    return this;
  }
  
  public String toString()
  {
    return a(Log.isLoggable("DataMap", 3));
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzh.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/wearable/PutDataRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */