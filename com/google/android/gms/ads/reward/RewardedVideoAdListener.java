package com.google.android.gms.ads.reward;

public abstract interface RewardedVideoAdListener
{
  public abstract void a();
  
  public abstract void a(int paramInt);
  
  public abstract void a(RewardItem paramRewardItem);
  
  public abstract void b();
  
  public abstract void c();
  
  public abstract void d();
  
  public abstract void e();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/reward/RewardedVideoAdListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */