package com.google.android.gms.ads.formats;

import android.content.Context;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.google.android.gms.ads.internal.client.zzn;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzcj;
import com.google.android.gms.internal.zzcv;

public abstract class NativeAdView
  extends FrameLayout
{
  private final FrameLayout a = b(paramContext);
  private final zzcj b = a();
  
  public NativeAdView(Context paramContext)
  {
    super(paramContext);
  }
  
  public NativeAdView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public NativeAdView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  private zzcj a()
  {
    zzx.a(this.a, "createDelegate must be called after mOverlayFrame has been created");
    return zzn.d().a(this.a.getContext(), this, this.a);
  }
  
  private FrameLayout b(Context paramContext)
  {
    paramContext = a(paramContext);
    paramContext.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
    addView(paramContext);
    return paramContext;
  }
  
  protected View a(String paramString)
  {
    try
    {
      paramString = this.b.a(paramString);
      if (paramString != null)
      {
        paramString = (View)zze.a(paramString);
        return paramString;
      }
    }
    catch (RemoteException paramString)
    {
      zzb.b("Unable to call getAssetView on delegate", paramString);
    }
    return null;
  }
  
  FrameLayout a(Context paramContext)
  {
    return new FrameLayout(paramContext);
  }
  
  protected void a(String paramString, View paramView)
  {
    try
    {
      this.b.a(paramString, zze.a(paramView));
      return;
    }
    catch (RemoteException paramString)
    {
      zzb.b("Unable to call setAssetView on delegate", paramString);
    }
  }
  
  public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams)
  {
    super.addView(paramView, paramInt, paramLayoutParams);
    super.bringChildToFront(this.a);
  }
  
  public void bringChildToFront(View paramView)
  {
    super.bringChildToFront(paramView);
    if (this.a != paramView) {
      super.bringChildToFront(this.a);
    }
  }
  
  public void removeAllViews()
  {
    super.removeAllViews();
    super.addView(this.a);
  }
  
  public void removeView(View paramView)
  {
    if (this.a == paramView) {
      return;
    }
    super.removeView(paramView);
  }
  
  public void setNativeAd(NativeAd paramNativeAd)
  {
    try
    {
      this.b.a((zzd)paramNativeAd.a());
      return;
    }
    catch (RemoteException paramNativeAd)
    {
      zzb.b("Unable to call setNativeAd on delegate", paramNativeAd);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/formats/NativeAdView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */