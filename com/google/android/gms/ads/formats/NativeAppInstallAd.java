package com.google.android.gms.ads.formats;

import java.util.List;

public abstract class NativeAppInstallAd
  extends NativeAd
{
  public abstract CharSequence b();
  
  public abstract List<NativeAd.Image> c();
  
  public abstract CharSequence d();
  
  public abstract NativeAd.Image e();
  
  public abstract CharSequence f();
  
  public abstract Double g();
  
  public abstract CharSequence h();
  
  public abstract CharSequence i();
  
  public static abstract interface OnAppInstallAdLoadedListener
  {
    public abstract void a(NativeAppInstallAd paramNativeAppInstallAd);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/formats/NativeAppInstallAd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */