package com.google.android.gms.ads.formats;

public abstract interface NativeCustomTemplateAd
{
  public static abstract interface OnCustomClickListener
  {
    public abstract void a(NativeCustomTemplateAd paramNativeCustomTemplateAd, String paramString);
  }
  
  public static abstract interface OnCustomTemplateAdLoadedListener
  {
    public abstract void a(NativeCustomTemplateAd paramNativeCustomTemplateAd);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/formats/NativeCustomTemplateAd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */