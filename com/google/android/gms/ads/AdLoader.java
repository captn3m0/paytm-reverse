package com.google.android.gms.ads;

import android.content.Context;
import android.os.RemoteException;
import android.support.annotation.RequiresPermission;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAppInstallAd.OnAppInstallAdLoadedListener;
import com.google.android.gms.ads.formats.NativeContentAd.OnContentAdLoadedListener;
import com.google.android.gms.ads.internal.client.zzaa;
import com.google.android.gms.ads.internal.client.zzc;
import com.google.android.gms.ads.internal.client.zzd;
import com.google.android.gms.ads.internal.client.zzh;
import com.google.android.gms.ads.internal.client.zzr;
import com.google.android.gms.ads.internal.client.zzs;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzcw;
import com.google.android.gms.internal.zzcx;
import com.google.android.gms.internal.zzew;

public class AdLoader
{
  private final zzh a;
  private final Context b;
  private final zzr c;
  
  AdLoader(Context paramContext, zzr paramzzr)
  {
    this(paramContext, paramzzr, zzh.a());
  }
  
  AdLoader(Context paramContext, zzr paramzzr, zzh paramzzh)
  {
    this.b = paramContext;
    this.c = paramzzr;
    this.a = paramzzh;
  }
  
  private void a(zzaa paramzzaa)
  {
    try
    {
      this.c.a(this.a.a(this.b, paramzzaa));
      return;
    }
    catch (RemoteException paramzzaa)
    {
      zzb.b("Failed to load ad.", paramzzaa);
    }
  }
  
  @RequiresPermission
  public void a(AdRequest paramAdRequest)
  {
    a(paramAdRequest.a());
  }
  
  public static class Builder
  {
    private final Context a;
    private final zzs b;
    
    Builder(Context paramContext, zzs paramzzs)
    {
      this.a = paramContext;
      this.b = paramzzs;
    }
    
    public Builder(Context paramContext, String paramString)
    {
      this((Context)zzx.a(paramContext, "context cannot be null"), zzd.a(paramContext, paramString, new zzew()));
    }
    
    public Builder a(AdListener paramAdListener)
    {
      try
      {
        this.b.a(new zzc(paramAdListener));
        return this;
      }
      catch (RemoteException paramAdListener)
      {
        zzb.d("Failed to set AdListener.", paramAdListener);
      }
      return this;
    }
    
    public Builder a(NativeAdOptions paramNativeAdOptions)
    {
      try
      {
        this.b.a(new NativeAdOptionsParcel(paramNativeAdOptions));
        return this;
      }
      catch (RemoteException paramNativeAdOptions)
      {
        zzb.d("Failed to specify native ad options", paramNativeAdOptions);
      }
      return this;
    }
    
    public Builder a(NativeAppInstallAd.OnAppInstallAdLoadedListener paramOnAppInstallAdLoadedListener)
    {
      try
      {
        this.b.a(new zzcw(paramOnAppInstallAdLoadedListener));
        return this;
      }
      catch (RemoteException paramOnAppInstallAdLoadedListener)
      {
        zzb.d("Failed to add app install ad listener", paramOnAppInstallAdLoadedListener);
      }
      return this;
    }
    
    public Builder a(NativeContentAd.OnContentAdLoadedListener paramOnContentAdLoadedListener)
    {
      try
      {
        this.b.a(new zzcx(paramOnContentAdLoadedListener));
        return this;
      }
      catch (RemoteException paramOnContentAdLoadedListener)
      {
        zzb.d("Failed to add content ad listener", paramOnContentAdLoadedListener);
      }
      return this;
    }
    
    public AdLoader a()
    {
      try
      {
        AdLoader localAdLoader = new AdLoader(this.a, this.b.a());
        return localAdLoader;
      }
      catch (RemoteException localRemoteException)
      {
        zzb.b("Failed to build AdLoader.", localRemoteException);
      }
      return null;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/AdLoader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */