package com.google.android.gms.ads;

import android.content.Context;
import android.support.annotation.RequiresPermission;
import com.google.android.gms.ads.internal.client.zza;
import com.google.android.gms.ads.internal.client.zzac;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

public final class InterstitialAd
{
  private final zzac a;
  
  public InterstitialAd(Context paramContext)
  {
    this.a = new zzac(paramContext);
  }
  
  public void a()
  {
    this.a.a();
  }
  
  public void a(AdListener paramAdListener)
  {
    this.a.a(paramAdListener);
    if ((paramAdListener != null) && ((paramAdListener instanceof zza))) {
      this.a.a((zza)paramAdListener);
    }
    while (paramAdListener != null) {
      return;
    }
    this.a.a(null);
  }
  
  @RequiresPermission
  public void a(AdRequest paramAdRequest)
  {
    this.a.a(paramAdRequest.a());
  }
  
  public void a(RewardedVideoAdListener paramRewardedVideoAdListener)
  {
    this.a.a(paramRewardedVideoAdListener);
  }
  
  public void a(String paramString)
  {
    this.a.a(paramString);
  }
  
  public void a(boolean paramBoolean)
  {
    this.a.a(paramBoolean);
  }
  
  public void b(String paramString)
  {
    this.a.b(paramString);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/InterstitialAd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */