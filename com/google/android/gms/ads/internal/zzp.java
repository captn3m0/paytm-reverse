package com.google.android.gms.ads.internal;

import android.content.Context;
import android.os.Handler;
import android.os.RemoteException;
import android.support.v4.util.SimpleArrayMap;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.formats.zze;
import com.google.android.gms.ads.internal.formats.zzf;
import com.google.android.gms.ads.internal.formats.zzg;
import com.google.android.gms.ads.internal.formats.zzh;
import com.google.android.gms.ads.internal.formats.zzh.zza;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzax;
import com.google.android.gms.internal.zzcb;
import com.google.android.gms.internal.zzcf;
import com.google.android.gms.internal.zzch;
import com.google.android.gms.internal.zzcr;
import com.google.android.gms.internal.zzcs;
import com.google.android.gms.internal.zzct;
import com.google.android.gms.internal.zzcu;
import com.google.android.gms.internal.zzex;
import com.google.android.gms.internal.zzey;
import com.google.android.gms.internal.zzfb;
import com.google.android.gms.internal.zzfc;
import com.google.android.gms.internal.zzgd;
import com.google.android.gms.internal.zzgr;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzif;
import com.google.android.gms.internal.zzif.zza;
import com.google.android.gms.internal.zzih;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzir;
import java.util.List;

@zzhb
public class zzp
  extends zzb
{
  public zzp(Context paramContext, zzd paramzzd, AdSizeParcel paramAdSizeParcel, String paramString, zzex paramzzex, VersionInfoParcel paramVersionInfoParcel)
  {
    super(paramContext, paramAdSizeParcel, paramString, paramzzex, paramVersionInfoParcel, paramzzd);
  }
  
  private static com.google.android.gms.ads.internal.formats.zzd a(zzfb paramzzfb)
    throws RemoteException
  {
    String str1 = paramzzfb.a();
    List localList = paramzzfb.b();
    String str2 = paramzzfb.c();
    if (paramzzfb.d() != null) {}
    for (zzch localzzch = paramzzfb.d();; localzzch = null) {
      return new com.google.android.gms.ads.internal.formats.zzd(str1, localList, str2, localzzch, paramzzfb.e(), paramzzfb.f(), paramzzfb.g(), paramzzfb.h(), null, paramzzfb.l());
    }
  }
  
  private static zze a(zzfc paramzzfc)
    throws RemoteException
  {
    String str1 = paramzzfc.a();
    List localList = paramzzfc.b();
    String str2 = paramzzfc.c();
    if (paramzzfc.d() != null) {}
    for (zzch localzzch = paramzzfc.d();; localzzch = null) {
      return new zze(str1, localList, str2, localzzch, paramzzfc.e(), paramzzfc.f(), null, paramzzfc.j());
    }
  }
  
  private void a(final com.google.android.gms.ads.internal.formats.zzd paramzzd)
  {
    zzir.a.post(new Runnable()
    {
      public void run()
      {
        try
        {
          zzp.this.f.s.a(paramzzd);
          return;
        }
        catch (RemoteException localRemoteException)
        {
          zzin.d("Could not call OnAppInstallAdLoadedListener.onAppInstallAdLoaded().", localRemoteException);
        }
      }
    });
  }
  
  private void a(final zze paramzze)
  {
    zzir.a.post(new Runnable()
    {
      public void run()
      {
        try
        {
          zzp.this.f.t.a(paramzze);
          return;
        }
        catch (RemoteException localRemoteException)
        {
          zzin.d("Could not call OnContentAdLoadedListener.onContentAdLoaded().", localRemoteException);
        }
      }
    });
  }
  
  private void a(final zzif paramzzif, final String paramString)
  {
    zzir.a.post(new Runnable()
    {
      public void run()
      {
        try
        {
          ((zzcu)zzp.this.f.v.get(paramString)).a((zzf)paramzzif.B);
          return;
        }
        catch (RemoteException localRemoteException)
        {
          zzin.d("Could not call onCustomTemplateAdLoadedListener.onCustomTemplateAdLoaded().", localRemoteException);
        }
      }
    });
  }
  
  public void a(SimpleArrayMap<String, zzcu> paramSimpleArrayMap)
  {
    zzx.b("setOnCustomTemplateAdLoadedListeners must be called on the main UI thread.");
    this.f.v = paramSimpleArrayMap;
  }
  
  public void a(NativeAdOptionsParcel paramNativeAdOptionsParcel)
  {
    zzx.b("setNativeAdOptions must be called on the main UI thread.");
    this.f.w = paramNativeAdOptionsParcel;
  }
  
  public void a(zzh paramzzh)
  {
    if (this.f.j.j != null) {
      zzr.h().k().a(this.f.i, this.f.j, paramzzh);
    }
  }
  
  public void a(zzcf paramzzcf)
  {
    throw new IllegalStateException("CustomRendering is NOT supported by NativeAdManager.");
  }
  
  public void a(zzcr paramzzcr)
  {
    zzx.b("setOnAppInstallAdLoadedListener must be called on the main UI thread.");
    this.f.s = paramzzcr;
  }
  
  public void a(zzcs paramzzcs)
  {
    zzx.b("setOnContentAdLoadedListener must be called on the main UI thread.");
    this.f.t = paramzzcs;
  }
  
  public void a(zzgd paramzzgd)
  {
    throw new IllegalStateException("In App Purchase is NOT supported by NativeAdManager.");
  }
  
  public void a(final zzif.zza paramzza, zzcb paramzzcb)
  {
    if (paramzza.d != null) {
      this.f.i = paramzza.d;
    }
    if (paramzza.e != -2)
    {
      zzir.a.post(new Runnable()
      {
        public void run()
        {
          zzp.this.b(new zzif(paramzza, null, null, null, null, null, null));
        }
      });
      return;
    }
    this.f.D = 0;
    this.f.h = zzr.d().a(this.f.c, this, paramzza, this.f.d, null, this.j, this, paramzzcb);
    zzin.a("AdRenderer: " + this.f.h.getClass().getName());
  }
  
  public void a(List<String> paramList)
  {
    zzx.b("setNativeTemplates must be called on the main UI thread.");
    this.f.z = paramList;
  }
  
  protected boolean a(AdRequestParcel paramAdRequestParcel, zzif paramzzif, boolean paramBoolean)
  {
    return this.e.c();
  }
  
  protected boolean a(zzif paramzzif1, zzif paramzzif2)
  {
    a(null);
    if (!this.f.e()) {
      throw new IllegalStateException("Native ad DOES NOT have custom rendering mode.");
    }
    if (paramzzif2.m) {}
    for (;;)
    {
      try
      {
        localObject1 = paramzzif2.o.h();
        localObject2 = paramzzif2.o.i();
        if (localObject1 == null) {
          continue;
        }
        localObject2 = a((zzfb)localObject1);
        ((com.google.android.gms.ads.internal.formats.zzd)localObject2).a(new zzg(this.f.c, this, this.f.d, (zzfb)localObject1));
        a((com.google.android.gms.ads.internal.formats.zzd)localObject2);
      }
      catch (RemoteException localRemoteException)
      {
        Object localObject1;
        Object localObject2;
        zzin.d("Failed to get native ad mapper", localRemoteException);
        continue;
        zzin.d("No matching mapper for retrieved native ad template.");
        a(0);
        return false;
      }
      return super.a(paramzzif1, paramzzif2);
      if (localObject2 != null)
      {
        localObject1 = a((zzfc)localObject2);
        ((zze)localObject1).a(new zzg(this.f.c, this, this.f.d, (zzfc)localObject2));
        a((zze)localObject1);
      }
      else
      {
        zzh.zza localzza = paramzzif2.B;
        if (((localzza instanceof zze)) && (this.f.t != null))
        {
          a((zze)paramzzif2.B);
        }
        else if (((localzza instanceof com.google.android.gms.ads.internal.formats.zzd)) && (this.f.s != null))
        {
          a((com.google.android.gms.ads.internal.formats.zzd)paramzzif2.B);
        }
        else
        {
          if ((!(localzza instanceof zzf)) || (this.f.v == null) || (this.f.v.get(((zzf)localzza).k()) == null)) {
            break;
          }
          a(paramzzif2, ((zzf)localzza).k());
        }
      }
    }
    zzin.d("No matching listener for retrieved native ad template.");
    a(0);
    return false;
  }
  
  public void b(SimpleArrayMap<String, zzct> paramSimpleArrayMap)
  {
    zzx.b("setOnCustomClickListener must be called on the main UI thread.");
    this.f.u = paramSimpleArrayMap;
  }
  
  public void b_()
  {
    throw new IllegalStateException("Native Ad DOES NOT support resume().");
  }
  
  public zzct c(String paramString)
  {
    zzx.b("getOnCustomClickListener must be called on the main UI thread.");
    return (zzct)this.f.u.get(paramString);
  }
  
  public void d()
  {
    throw new IllegalStateException("Native Ad DOES NOT support pause().");
  }
  
  public void f()
  {
    throw new IllegalStateException("Interstitial is NOT supported by NativeAdManager.");
  }
  
  public SimpleArrayMap<String, zzcu> z()
  {
    zzx.b("getOnCustomTemplateAdLoadedListeners must be called on the main UI thread.");
    return this.f.v;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */