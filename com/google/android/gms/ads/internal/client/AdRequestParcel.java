package com.google.android.gms.ads.internal.client;

import android.location.Location;
import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.internal.zzhb;
import java.util.List;

@zzhb
public final class AdRequestParcel
  implements SafeParcelable
{
  public static final zzg CREATOR = new zzg();
  public final int a;
  public final long b;
  public final Bundle c;
  public final int d;
  public final List<String> e;
  public final boolean f;
  public final int g;
  public final boolean h;
  public final String i;
  public final SearchAdRequestParcel j;
  public final Location k;
  public final String l;
  public final Bundle m;
  public final Bundle n;
  public final List<String> o;
  public final String p;
  public final String q;
  public final boolean r;
  
  public AdRequestParcel(int paramInt1, long paramLong, Bundle paramBundle1, int paramInt2, List<String> paramList1, boolean paramBoolean1, int paramInt3, boolean paramBoolean2, String paramString1, SearchAdRequestParcel paramSearchAdRequestParcel, Location paramLocation, String paramString2, Bundle paramBundle2, Bundle paramBundle3, List<String> paramList2, String paramString3, String paramString4, boolean paramBoolean3)
  {
    this.a = paramInt1;
    this.b = paramLong;
    Bundle localBundle = paramBundle1;
    if (paramBundle1 == null) {
      localBundle = new Bundle();
    }
    this.c = localBundle;
    this.d = paramInt2;
    this.e = paramList1;
    this.f = paramBoolean1;
    this.g = paramInt3;
    this.h = paramBoolean2;
    this.i = paramString1;
    this.j = paramSearchAdRequestParcel;
    this.k = paramLocation;
    this.l = paramString2;
    this.m = paramBundle2;
    this.n = paramBundle3;
    this.o = paramList2;
    this.p = paramString3;
    this.q = paramString4;
    this.r = paramBoolean3;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof AdRequestParcel)) {}
    do
    {
      return false;
      paramObject = (AdRequestParcel)paramObject;
    } while ((this.a != ((AdRequestParcel)paramObject).a) || (this.b != ((AdRequestParcel)paramObject).b) || (!zzw.a(this.c, ((AdRequestParcel)paramObject).c)) || (this.d != ((AdRequestParcel)paramObject).d) || (!zzw.a(this.e, ((AdRequestParcel)paramObject).e)) || (this.f != ((AdRequestParcel)paramObject).f) || (this.g != ((AdRequestParcel)paramObject).g) || (this.h != ((AdRequestParcel)paramObject).h) || (!zzw.a(this.i, ((AdRequestParcel)paramObject).i)) || (!zzw.a(this.j, ((AdRequestParcel)paramObject).j)) || (!zzw.a(this.k, ((AdRequestParcel)paramObject).k)) || (!zzw.a(this.l, ((AdRequestParcel)paramObject).l)) || (!zzw.a(this.m, ((AdRequestParcel)paramObject).m)) || (!zzw.a(this.n, ((AdRequestParcel)paramObject).n)) || (!zzw.a(this.o, ((AdRequestParcel)paramObject).o)) || (!zzw.a(this.p, ((AdRequestParcel)paramObject).p)) || (!zzw.a(this.q, ((AdRequestParcel)paramObject).q)) || (this.r != ((AdRequestParcel)paramObject).r));
    return true;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Integer.valueOf(this.a), Long.valueOf(this.b), this.c, Integer.valueOf(this.d), this.e, Boolean.valueOf(this.f), Integer.valueOf(this.g), Boolean.valueOf(this.h), this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.p, this.q, Boolean.valueOf(this.r) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzg.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/client/AdRequestParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */