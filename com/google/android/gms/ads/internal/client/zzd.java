package com.google.android.gms.ads.internal.client;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.dynamic.zzg;
import com.google.android.gms.dynamic.zzg.zza;
import com.google.android.gms.internal.zzew;
import com.google.android.gms.internal.zzhb;

@zzhb
public final class zzd
  extends zzg<zzt>
{
  private static final zzd a = new zzd();
  
  private zzd()
  {
    super("com.google.android.gms.ads.AdLoaderBuilderCreatorImpl");
  }
  
  public static zzs a(Context paramContext, String paramString, zzew paramzzew)
  {
    Object localObject;
    if (zzn.a().b(paramContext))
    {
      zzs localzzs = a.b(paramContext, paramString, paramzzew);
      localObject = localzzs;
      if (localzzs != null) {}
    }
    else
    {
      zzb.a("Using AdLoader from the client jar.");
      localObject = new VersionInfoParcel(8487000, 8487000, true);
      localObject = zzn.c().a(paramContext, paramString, paramzzew, (VersionInfoParcel)localObject);
    }
    return (zzs)localObject;
  }
  
  private zzs b(Context paramContext, String paramString, zzew paramzzew)
  {
    try
    {
      com.google.android.gms.dynamic.zzd localzzd = zze.a(paramContext);
      paramContext = zzs.zza.a(((zzt)a(paramContext)).a(localzzd, paramString, paramzzew, 8487000));
      return paramContext;
    }
    catch (RemoteException paramContext)
    {
      zzb.d("Could not create remote builder for AdLoader.", paramContext);
      return null;
    }
    catch (zzg.zza paramContext)
    {
      for (;;)
      {
        zzb.d("Could not create remote builder for AdLoader.", paramContext);
      }
    }
  }
  
  protected zzt a(IBinder paramIBinder)
  {
    return zzt.zza.a(paramIBinder);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */