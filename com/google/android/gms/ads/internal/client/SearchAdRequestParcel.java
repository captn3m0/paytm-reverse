package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import com.google.android.gms.ads.search.SearchAdRequest;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.zzhb;

@zzhb
public final class SearchAdRequestParcel
  implements SafeParcelable
{
  public static final zzam CREATOR = new zzam();
  public final int a;
  public final int b;
  public final int c;
  public final int d;
  public final int e;
  public final int f;
  public final int g;
  public final int h;
  public final int i;
  public final String j;
  public final int k;
  public final String l;
  public final int m;
  public final int n;
  public final String o;
  
  SearchAdRequestParcel(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, String paramString1, int paramInt10, String paramString2, int paramInt11, int paramInt12, String paramString3)
  {
    this.a = paramInt1;
    this.b = paramInt2;
    this.c = paramInt3;
    this.d = paramInt4;
    this.e = paramInt5;
    this.f = paramInt6;
    this.g = paramInt7;
    this.h = paramInt8;
    this.i = paramInt9;
    this.j = paramString1;
    this.k = paramInt10;
    this.l = paramString2;
    this.m = paramInt11;
    this.n = paramInt12;
    this.o = paramString3;
  }
  
  public SearchAdRequestParcel(SearchAdRequest paramSearchAdRequest)
  {
    this.a = 1;
    this.b = paramSearchAdRequest.a();
    this.c = paramSearchAdRequest.b();
    this.d = paramSearchAdRequest.c();
    this.e = paramSearchAdRequest.d();
    this.f = paramSearchAdRequest.e();
    this.g = paramSearchAdRequest.f();
    this.h = paramSearchAdRequest.g();
    this.i = paramSearchAdRequest.h();
    this.j = paramSearchAdRequest.i();
    this.k = paramSearchAdRequest.j();
    this.l = paramSearchAdRequest.k();
    this.m = paramSearchAdRequest.l();
    this.n = paramSearchAdRequest.m();
    this.o = paramSearchAdRequest.n();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzam.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/client/SearchAdRequestParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */