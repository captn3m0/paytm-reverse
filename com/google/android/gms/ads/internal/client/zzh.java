package com.google.android.gms.ads.internal.client;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.search.SearchAdRequest;
import com.google.android.gms.internal.zzhb;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

@zzhb
public class zzh
{
  public static final zzh a = new zzh();
  
  public static zzh a()
  {
    return a;
  }
  
  public AdRequestParcel a(Context paramContext, zzaa paramzzaa)
  {
    Object localObject1 = paramzzaa.a();
    long l;
    String str1;
    int i;
    label59:
    boolean bool1;
    int j;
    Location localLocation;
    Bundle localBundle;
    boolean bool2;
    String str2;
    if (localObject1 != null)
    {
      l = ((Date)localObject1).getTime();
      str1 = paramzzaa.b();
      i = paramzzaa.c();
      localObject1 = paramzzaa.d();
      if (((Set)localObject1).isEmpty()) {
        break label231;
      }
      localObject1 = Collections.unmodifiableList(new ArrayList((Collection)localObject1));
      bool1 = paramzzaa.a(paramContext);
      j = paramzzaa.l();
      localLocation = paramzzaa.e();
      localBundle = paramzzaa.a(AdMobAdapter.class);
      bool2 = paramzzaa.f();
      str2 = paramzzaa.g();
      localObject2 = paramzzaa.i();
      if (localObject2 == null) {
        break label237;
      }
    }
    label231:
    label237:
    for (Object localObject2 = new SearchAdRequestParcel((SearchAdRequest)localObject2);; localObject2 = null)
    {
      Object localObject3 = null;
      Context localContext = paramContext.getApplicationContext();
      paramContext = (Context)localObject3;
      if (localContext != null)
      {
        paramContext = localContext.getPackageName();
        paramContext = zzn.a().a(Thread.currentThread().getStackTrace(), paramContext);
      }
      boolean bool3 = paramzzaa.o();
      return new AdRequestParcel(7, l, localBundle, i, (List)localObject1, bool1, j, bool2, str2, (SearchAdRequestParcel)localObject2, localLocation, str1, paramzzaa.k(), paramzzaa.m(), Collections.unmodifiableList(new ArrayList(paramzzaa.n())), paramzzaa.h(), paramContext, bool3);
      l = -1L;
      break;
      localObject1 = null;
      break label59;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */