package com.google.android.gms.ads.internal.client;

import android.os.Handler;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.internal.zzcr;
import com.google.android.gms.internal.zzcs;
import com.google.android.gms.internal.zzct;
import com.google.android.gms.internal.zzcu;

public class zzag
  extends zzs.zza
{
  private zzq a;
  
  public zzr a()
    throws RemoteException
  {
    return new zza(null);
  }
  
  public void a(zzq paramzzq)
    throws RemoteException
  {
    this.a = paramzzq;
  }
  
  public void a(zzx paramzzx)
    throws RemoteException
  {}
  
  public void a(NativeAdOptionsParcel paramNativeAdOptionsParcel)
    throws RemoteException
  {}
  
  public void a(zzcr paramzzcr)
    throws RemoteException
  {}
  
  public void a(zzcs paramzzcs)
    throws RemoteException
  {}
  
  public void a(String paramString, zzcu paramzzcu, zzct paramzzct)
    throws RemoteException
  {}
  
  private class zza
    extends zzr.zza
  {
    private zza() {}
    
    public void a(AdRequestParcel paramAdRequestParcel)
      throws RemoteException
    {
      zzb.b("This app is using a lightweight version of the Google Mobile Ads SDK that requires the latest Google Play services to be installed, but Google Play services is either missing or out of date.");
      zza.a.post(new Runnable()
      {
        public void run()
        {
          if (zzag.a(zzag.this) != null) {}
          try
          {
            zzag.a(zzag.this).a(1);
            return;
          }
          catch (RemoteException localRemoteException)
          {
            zzb.d("Could not notify onAdFailedToLoad event.", localRemoteException);
          }
        }
      });
    }
    
    public boolean a()
      throws RemoteException
    {
      return false;
    }
    
    public String b()
      throws RemoteException
    {
      return null;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzag.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */