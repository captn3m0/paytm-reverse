package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.internal.zzhb;

@zzhb
public final class zzj
  extends zzw.zza
{
  private final AppEventListener a;
  
  public zzj(AppEventListener paramAppEventListener)
  {
    this.a = paramAppEventListener;
  }
  
  public void a(String paramString1, String paramString2)
  {
    this.a.a(paramString1, paramString2);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */