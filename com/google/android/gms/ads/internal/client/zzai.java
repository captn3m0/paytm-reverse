package com.google.android.gms.ads.internal.client;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.FrameLayout;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.internal.zzcj;
import com.google.android.gms.internal.zzew;
import com.google.android.gms.internal.zzfv;
import com.google.android.gms.internal.zzge;

public class zzai
  implements zzm
{
  public zzs a(Context paramContext, String paramString, zzew paramzzew, VersionInfoParcel paramVersionInfoParcel)
  {
    return new zzag();
  }
  
  public zzu a(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, zzew paramzzew, VersionInfoParcel paramVersionInfoParcel)
  {
    return new zzah();
  }
  
  public zzcj a(FrameLayout paramFrameLayout1, FrameLayout paramFrameLayout2)
  {
    return new zzak();
  }
  
  @Nullable
  public zzge a(Activity paramActivity)
  {
    return null;
  }
  
  public zzu b(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, zzew paramzzew, VersionInfoParcel paramVersionInfoParcel)
  {
    return new zzah();
  }
  
  @Nullable
  public zzfv b(Activity paramActivity)
  {
    return null;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzai.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */