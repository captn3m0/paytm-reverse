package com.google.android.gms.ads.internal.client;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.FrameLayout;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.internal.zzcj;
import com.google.android.gms.internal.zzew;
import com.google.android.gms.internal.zzfv;
import com.google.android.gms.internal.zzge;

public abstract interface zzm
{
  public abstract zzs a(Context paramContext, String paramString, zzew paramzzew, VersionInfoParcel paramVersionInfoParcel);
  
  public abstract zzu a(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, zzew paramzzew, VersionInfoParcel paramVersionInfoParcel);
  
  public abstract zzcj a(FrameLayout paramFrameLayout1, FrameLayout paramFrameLayout2);
  
  @Nullable
  public abstract zzge a(Activity paramActivity);
  
  public abstract zzu b(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, zzew paramzzew, VersionInfoParcel paramVersionInfoParcel);
  
  @Nullable
  public abstract zzfv b(Activity paramActivity);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */