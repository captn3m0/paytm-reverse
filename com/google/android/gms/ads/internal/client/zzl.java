package com.google.android.gms.ads.internal.client;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.FrameLayout;
import com.google.android.gms.ads.internal.ClientApi;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.internal.zzcj;
import com.google.android.gms.internal.zzew;
import com.google.android.gms.internal.zzfv;
import com.google.android.gms.internal.zzge;
import com.google.android.gms.internal.zzhb;

@zzhb
public class zzl
{
  public static String a = null;
  private zzm b;
  
  public zzl()
  {
    ClientApi.a();
    if (a != null) {
      try
      {
        this.b = ((zzm)zzl.class.getClassLoader().loadClass(a).newInstance());
        return;
      }
      catch (Exception localException)
      {
        zzb.d("Failed to instantiate ClientApi class.", localException);
        this.b = new zzai();
        return;
      }
    }
    zzb.d("No client jar implementation found.");
    this.b = new zzai();
  }
  
  public zzs a(Context paramContext, String paramString, zzew paramzzew, VersionInfoParcel paramVersionInfoParcel)
  {
    return this.b.a(paramContext, paramString, paramzzew, paramVersionInfoParcel);
  }
  
  public zzu a(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, zzew paramzzew, VersionInfoParcel paramVersionInfoParcel)
  {
    return this.b.a(paramContext, paramAdSizeParcel, paramString, paramzzew, paramVersionInfoParcel);
  }
  
  public zzcj a(FrameLayout paramFrameLayout1, FrameLayout paramFrameLayout2)
  {
    return this.b.a(paramFrameLayout1, paramFrameLayout2);
  }
  
  @Nullable
  public zzge a(Activity paramActivity)
  {
    return this.b.a(paramActivity);
  }
  
  public zzu b(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, zzew paramzzew, VersionInfoParcel paramVersionInfoParcel)
  {
    return this.b.b(paramContext, paramAdSizeParcel, paramString, paramzzew, paramVersionInfoParcel);
  }
  
  @Nullable
  public zzfv b(Activity paramActivity)
  {
    return this.b.b(paramActivity);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */