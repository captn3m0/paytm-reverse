package com.google.android.gms.ads.internal.client;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zzg;
import com.google.android.gms.dynamic.zzg.zza;
import com.google.android.gms.internal.zzew;
import com.google.android.gms.internal.zzhb;

@zzhb
public class zze
  extends zzg<zzv>
{
  public zze()
  {
    super("com.google.android.gms.ads.AdManagerCreatorImpl");
  }
  
  private zzu a(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, zzew paramzzew, int paramInt)
  {
    try
    {
      zzd localzzd = com.google.android.gms.dynamic.zze.a(paramContext);
      paramContext = zzu.zza.a(((zzv)a(paramContext)).a(localzzd, paramAdSizeParcel, paramString, paramzzew, 8487000, paramInt));
      return paramContext;
    }
    catch (RemoteException paramContext)
    {
      zzb.a("Could not create remote AdManager.", paramContext);
      return null;
    }
    catch (zzg.zza paramContext)
    {
      for (;;) {}
    }
  }
  
  public zzu a(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, zzew paramzzew)
  {
    Object localObject;
    if (zzn.a().b(paramContext))
    {
      zzu localzzu = a(paramContext, paramAdSizeParcel, paramString, paramzzew, 1);
      localObject = localzzu;
      if (localzzu != null) {}
    }
    else
    {
      zzb.a("Using BannerAdManager from the client jar.");
      localObject = new VersionInfoParcel(8487000, 8487000, true);
      localObject = zzn.c().a(paramContext, paramAdSizeParcel, paramString, paramzzew, (VersionInfoParcel)localObject);
    }
    return (zzu)localObject;
  }
  
  protected zzv a(IBinder paramIBinder)
  {
    return zzv.zza.a(paramIBinder);
  }
  
  public zzu b(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, zzew paramzzew)
  {
    Object localObject;
    if (zzn.a().b(paramContext))
    {
      zzu localzzu = a(paramContext, paramAdSizeParcel, paramString, paramzzew, 2);
      localObject = localzzu;
      if (localzzu != null) {}
    }
    else
    {
      zzb.d("Using InterstitialAdManager from the client jar.");
      localObject = new VersionInfoParcel(8487000, 8487000, true);
      localObject = zzn.c().b(paramContext, paramAdSizeParcel, paramString, paramzzew, (VersionInfoParcel)localObject);
    }
    return (zzu)localObject;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/client/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */