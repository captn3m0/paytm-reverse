package com.google.android.gms.ads.internal.client;

import android.os.Handler;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.reward.client.RewardedVideoAdRequestParcel;
import com.google.android.gms.ads.internal.reward.client.zzb.zza;
import com.google.android.gms.ads.internal.reward.client.zzd;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;

public class zzal
  extends zzb.zza
{
  private zzd a;
  
  public void a()
    throws RemoteException
  {}
  
  public void a(RewardedVideoAdRequestParcel paramRewardedVideoAdRequestParcel)
    throws RemoteException
  {
    zzb.b("This app is using a lightweight version of the Google Mobile Ads SDK that requires the latest Google Play services to be installed, but Google Play services is either missing or out of date.");
    zza.a.post(new Runnable()
    {
      public void run()
      {
        if (zzal.a(zzal.this) != null) {}
        try
        {
          zzal.a(zzal.this).a(1);
          return;
        }
        catch (RemoteException localRemoteException)
        {
          zzb.d("Could not notify onRewardedVideoAdFailedToLoad event.", localRemoteException);
        }
      }
    });
  }
  
  public void a(zzd paramzzd)
    throws RemoteException
  {
    this.a = paramzzd;
  }
  
  public void a(String paramString)
    throws RemoteException
  {}
  
  public boolean b()
    throws RemoteException
  {
    return false;
  }
  
  public void c()
    throws RemoteException
  {}
  
  public void d()
    throws RemoteException
  {}
  
  public void e()
    throws RemoteException
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */