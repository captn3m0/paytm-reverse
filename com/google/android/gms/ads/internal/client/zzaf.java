package com.google.android.gms.ads.internal.client;

import android.os.IBinder;
import com.google.android.gms.dynamic.zzg;
import com.google.android.gms.internal.zzhb;

@zzhb
public class zzaf
  extends zzg<zzz>
{
  public zzaf()
  {
    super("com.google.android.gms.ads.MobileAdsSettingManagerCreatorImpl");
  }
  
  protected zzz a(IBinder paramIBinder)
  {
    return zzz.zza.a(paramIBinder);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzaf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */