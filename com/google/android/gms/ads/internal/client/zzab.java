package com.google.android.gms.ads.internal.client;

import android.content.Context;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.Correlator;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.ads.doubleclick.OnCustomRenderedAdLoadedListener;
import com.google.android.gms.ads.purchase.InAppPurchaseListener;
import com.google.android.gms.ads.purchase.PlayStorePurchaseListener;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.internal.zzcg;
import com.google.android.gms.internal.zzew;
import com.google.android.gms.internal.zzgi;
import com.google.android.gms.internal.zzgm;
import com.google.android.gms.internal.zzhb;
import java.util.concurrent.atomic.AtomicBoolean;

@zzhb
public class zzab
{
  private final zzew a = new zzew();
  private final zzh b;
  private final AtomicBoolean c;
  private AdListener d;
  private zza e;
  private zzu f;
  private AdSize[] g;
  private String h;
  private String i;
  private ViewGroup j;
  private AppEventListener k;
  private InAppPurchaseListener l;
  private PlayStorePurchaseListener m;
  private OnCustomRenderedAdLoadedListener n;
  private boolean o;
  private Correlator p;
  private boolean q;
  
  public zzab(ViewGroup paramViewGroup)
  {
    this(paramViewGroup, null, false, zzh.a(), false);
  }
  
  public zzab(ViewGroup paramViewGroup, AttributeSet paramAttributeSet, boolean paramBoolean)
  {
    this(paramViewGroup, paramAttributeSet, paramBoolean, zzh.a(), false);
  }
  
  zzab(ViewGroup paramViewGroup, AttributeSet paramAttributeSet, boolean paramBoolean1, zzh paramzzh, zzu paramzzu, boolean paramBoolean2)
  {
    this.j = paramViewGroup;
    this.b = paramzzh;
    this.f = paramzzu;
    this.c = new AtomicBoolean(false);
    this.q = paramBoolean2;
    if (paramAttributeSet != null) {
      paramzzh = paramViewGroup.getContext();
    }
    try
    {
      paramAttributeSet = new zzk(paramzzh, paramAttributeSet);
      this.g = paramAttributeSet.a(paramBoolean1);
      this.h = paramAttributeSet.a();
      if (paramViewGroup.isInEditMode()) {
        zzn.a().a(paramViewGroup, a(paramzzh, this.g[0], this.q), "Ads by Google");
      }
      return;
    }
    catch (IllegalArgumentException paramAttributeSet)
    {
      zzn.a().a(paramViewGroup, new AdSizeParcel(paramzzh, AdSize.a), paramAttributeSet.getMessage(), paramAttributeSet.getMessage());
    }
  }
  
  zzab(ViewGroup paramViewGroup, AttributeSet paramAttributeSet, boolean paramBoolean1, zzh paramzzh, boolean paramBoolean2)
  {
    this(paramViewGroup, paramAttributeSet, paramBoolean1, paramzzh, null, paramBoolean2);
  }
  
  public zzab(ViewGroup paramViewGroup, AttributeSet paramAttributeSet, boolean paramBoolean1, boolean paramBoolean2)
  {
    this(paramViewGroup, paramAttributeSet, paramBoolean1, zzh.a(), paramBoolean2);
  }
  
  public zzab(ViewGroup paramViewGroup, boolean paramBoolean)
  {
    this(paramViewGroup, null, false, zzh.a(), paramBoolean);
  }
  
  private static AdSizeParcel a(Context paramContext, AdSize paramAdSize, boolean paramBoolean)
  {
    paramContext = new AdSizeParcel(paramContext, paramAdSize);
    paramContext.a(paramBoolean);
    return paramContext;
  }
  
  private static AdSizeParcel a(Context paramContext, AdSize[] paramArrayOfAdSize, boolean paramBoolean)
  {
    paramContext = new AdSizeParcel(paramContext, paramArrayOfAdSize);
    paramContext.a(paramBoolean);
    return paramContext;
  }
  
  private void n()
  {
    try
    {
      zzd localzzd = this.f.a();
      if (localzzd == null) {
        return;
      }
      this.j.addView((View)com.google.android.gms.dynamic.zze.a(localzzd));
      return;
    }
    catch (RemoteException localRemoteException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Failed to get an ad frame.", localRemoteException);
    }
  }
  
  public void a()
  {
    try
    {
      if (this.f != null) {
        this.f.b();
      }
      return;
    }
    catch (RemoteException localRemoteException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Failed to destroy AdView.", localRemoteException);
    }
  }
  
  public void a(AdListener paramAdListener)
  {
    try
    {
      this.d = paramAdListener;
      zzu localzzu;
      if (this.f != null)
      {
        localzzu = this.f;
        if (paramAdListener == null) {
          break label38;
        }
      }
      label38:
      for (paramAdListener = new zzc(paramAdListener);; paramAdListener = null)
      {
        localzzu.a(paramAdListener);
        return;
      }
      return;
    }
    catch (RemoteException paramAdListener)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Failed to set the AdListener.", paramAdListener);
    }
  }
  
  public void a(Correlator paramCorrelator)
  {
    this.p = paramCorrelator;
    try
    {
      if (this.f != null)
      {
        zzu localzzu = this.f;
        if (this.p == null) {}
        for (paramCorrelator = null;; paramCorrelator = this.p.a())
        {
          localzzu.a(paramCorrelator);
          return;
        }
      }
      return;
    }
    catch (RemoteException paramCorrelator)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Failed to set correlator.", paramCorrelator);
    }
  }
  
  public void a(AppEventListener paramAppEventListener)
  {
    try
    {
      this.k = paramAppEventListener;
      zzu localzzu;
      if (this.f != null)
      {
        localzzu = this.f;
        if (paramAppEventListener == null) {
          break label38;
        }
      }
      label38:
      for (paramAppEventListener = new zzj(paramAppEventListener);; paramAppEventListener = null)
      {
        localzzu.a(paramAppEventListener);
        return;
      }
      return;
    }
    catch (RemoteException paramAppEventListener)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Failed to set the AppEventListener.", paramAppEventListener);
    }
  }
  
  public void a(OnCustomRenderedAdLoadedListener paramOnCustomRenderedAdLoadedListener)
  {
    this.n = paramOnCustomRenderedAdLoadedListener;
    try
    {
      zzu localzzu;
      if (this.f != null)
      {
        localzzu = this.f;
        if (paramOnCustomRenderedAdLoadedListener == null) {
          break label38;
        }
      }
      label38:
      for (paramOnCustomRenderedAdLoadedListener = new zzcg(paramOnCustomRenderedAdLoadedListener);; paramOnCustomRenderedAdLoadedListener = null)
      {
        localzzu.a(paramOnCustomRenderedAdLoadedListener);
        return;
      }
      return;
    }
    catch (RemoteException paramOnCustomRenderedAdLoadedListener)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Failed to set the onCustomRenderedAdLoadedListener.", paramOnCustomRenderedAdLoadedListener);
    }
  }
  
  public void a(zza paramzza)
  {
    try
    {
      this.e = paramzza;
      zzu localzzu;
      if (this.f != null)
      {
        localzzu = this.f;
        if (paramzza == null) {
          break label38;
        }
      }
      label38:
      for (paramzza = new zzb(paramzza);; paramzza = null)
      {
        localzzu.a(paramzza);
        return;
      }
      return;
    }
    catch (RemoteException paramzza)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Failed to set the AdClickListener.", paramzza);
    }
  }
  
  public void a(zzaa paramzzaa)
  {
    try
    {
      if (this.f == null) {
        l();
      }
      if (this.f.a(this.b.a(this.j.getContext(), paramzzaa))) {
        this.a.a(paramzzaa.j());
      }
      return;
    }
    catch (RemoteException paramzzaa)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Failed to load ad.", paramzzaa);
    }
  }
  
  public void a(InAppPurchaseListener paramInAppPurchaseListener)
  {
    if (this.m != null) {
      throw new IllegalStateException("Play store purchase parameter has already been set.");
    }
    try
    {
      this.l = paramInAppPurchaseListener;
      zzu localzzu;
      if (this.f != null)
      {
        localzzu = this.f;
        if (paramInAppPurchaseListener == null) {
          break label56;
        }
      }
      label56:
      for (paramInAppPurchaseListener = new zzgi(paramInAppPurchaseListener);; paramInAppPurchaseListener = null)
      {
        localzzu.a(paramInAppPurchaseListener);
        return;
      }
      return;
    }
    catch (RemoteException paramInAppPurchaseListener)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Failed to set the InAppPurchaseListener.", paramInAppPurchaseListener);
    }
  }
  
  public void a(PlayStorePurchaseListener paramPlayStorePurchaseListener, String paramString)
  {
    if (this.l != null) {
      throw new IllegalStateException("InAppPurchaseListener has already been set.");
    }
    try
    {
      this.m = paramPlayStorePurchaseListener;
      this.i = paramString;
      zzu localzzu;
      if (this.f != null)
      {
        localzzu = this.f;
        if (paramPlayStorePurchaseListener == null) {
          break label62;
        }
      }
      label62:
      for (paramPlayStorePurchaseListener = new zzgm(paramPlayStorePurchaseListener);; paramPlayStorePurchaseListener = null)
      {
        localzzu.a(paramPlayStorePurchaseListener, paramString);
        return;
      }
      return;
    }
    catch (RemoteException paramPlayStorePurchaseListener)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Failed to set the play store purchase parameter.", paramPlayStorePurchaseListener);
    }
  }
  
  public void a(String paramString)
  {
    if (this.h != null) {
      throw new IllegalStateException("The ad unit ID can only be set once on AdView.");
    }
    this.h = paramString;
  }
  
  public void a(boolean paramBoolean)
  {
    this.o = paramBoolean;
    try
    {
      if (this.f != null) {
        this.f.a(this.o);
      }
      return;
    }
    catch (RemoteException localRemoteException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Failed to set manual impressions.", localRemoteException);
    }
  }
  
  public void a(AdSize... paramVarArgs)
  {
    if (this.g != null) {
      throw new IllegalStateException("The ad size can only be set once on AdView.");
    }
    b(paramVarArgs);
  }
  
  public AdListener b()
  {
    return this.d;
  }
  
  public void b(AdSize... paramVarArgs)
  {
    this.g = paramVarArgs;
    try
    {
      if (this.f != null) {
        this.f.a(a(this.j.getContext(), this.g, this.q));
      }
      this.j.requestLayout();
      return;
    }
    catch (RemoteException paramVarArgs)
    {
      for (;;)
      {
        com.google.android.gms.ads.internal.util.client.zzb.d("Failed to set the ad size.", paramVarArgs);
      }
    }
  }
  
  public AdSize c()
  {
    try
    {
      if (this.f != null)
      {
        Object localObject = this.f.i();
        if (localObject != null)
        {
          localObject = ((AdSizeParcel)localObject).b();
          return (AdSize)localObject;
        }
      }
    }
    catch (RemoteException localRemoteException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Failed to get the current AdSize.", localRemoteException);
      if (this.g != null) {
        return this.g[0];
      }
    }
    return null;
  }
  
  public AdSize[] d()
  {
    return this.g;
  }
  
  public String e()
  {
    return this.h;
  }
  
  public AppEventListener f()
  {
    return this.k;
  }
  
  public InAppPurchaseListener g()
  {
    return this.l;
  }
  
  public OnCustomRenderedAdLoadedListener h()
  {
    return this.n;
  }
  
  public void i()
  {
    try
    {
      if (this.f != null) {
        this.f.d();
      }
      return;
    }
    catch (RemoteException localRemoteException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Failed to call pause.", localRemoteException);
    }
  }
  
  public void j()
  {
    try
    {
      if (this.f != null) {
        this.f.b_();
      }
      return;
    }
    catch (RemoteException localRemoteException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Failed to call resume.", localRemoteException);
    }
  }
  
  public String k()
  {
    try
    {
      if (this.f != null)
      {
        String str = this.f.j();
        return str;
      }
    }
    catch (RemoteException localRemoteException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Failed to get the mediation adapter class name.", localRemoteException);
    }
    return null;
  }
  
  void l()
    throws RemoteException
  {
    if (((this.g == null) || (this.h == null)) && (this.f == null)) {
      throw new IllegalStateException("The ad size and ad unit ID must be set before loadAd is called.");
    }
    this.f = m();
    if (this.d != null) {
      this.f.a(new zzc(this.d));
    }
    if (this.e != null) {
      this.f.a(new zzb(this.e));
    }
    if (this.k != null) {
      this.f.a(new zzj(this.k));
    }
    if (this.l != null) {
      this.f.a(new zzgi(this.l));
    }
    if (this.m != null) {
      this.f.a(new zzgm(this.m), this.i);
    }
    if (this.n != null) {
      this.f.a(new zzcg(this.n));
    }
    if (this.p != null) {
      this.f.a(this.p.a());
    }
    this.f.a(this.o);
    n();
  }
  
  protected zzu m()
    throws RemoteException
  {
    Context localContext = this.j.getContext();
    return zzn.b().a(localContext, a(localContext, this.g, this.q), this.h, this.a);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzab.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */