package com.google.android.gms.ads.internal.client;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.Correlator;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.ads.doubleclick.OnCustomRenderedAdLoadedListener;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;
import com.google.android.gms.ads.internal.reward.client.zzg;
import com.google.android.gms.ads.purchase.InAppPurchaseListener;
import com.google.android.gms.ads.purchase.PlayStorePurchaseListener;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.internal.zzcg;
import com.google.android.gms.internal.zzew;
import com.google.android.gms.internal.zzgi;
import com.google.android.gms.internal.zzgm;
import com.google.android.gms.internal.zzhb;

@zzhb
public class zzac
{
  private final zzew a = new zzew();
  private final Context b;
  private final zzh c;
  private AdListener d;
  private zza e;
  private zzu f;
  private String g;
  private String h;
  private AppEventListener i;
  private PlayStorePurchaseListener j;
  private InAppPurchaseListener k;
  private PublisherInterstitialAd l;
  private OnCustomRenderedAdLoadedListener m;
  private Correlator n;
  private RewardedVideoAdListener o;
  private String p;
  private boolean q;
  
  public zzac(Context paramContext)
  {
    this(paramContext, zzh.a(), null);
  }
  
  public zzac(Context paramContext, zzh paramzzh, PublisherInterstitialAd paramPublisherInterstitialAd)
  {
    this.b = paramContext;
    this.c = paramzzh;
    this.l = paramPublisherInterstitialAd;
  }
  
  private void c(String paramString)
    throws RemoteException
  {
    if (this.g == null) {
      d(paramString);
    }
    if (this.q) {}
    for (paramString = AdSizeParcel.a();; paramString = new AdSizeParcel())
    {
      this.f = zzn.b().b(this.b, paramString, this.g, this.a);
      if (this.d != null) {
        this.f.a(new zzc(this.d));
      }
      if (this.e != null) {
        this.f.a(new zzb(this.e));
      }
      if (this.i != null) {
        this.f.a(new zzj(this.i));
      }
      if (this.k != null) {
        this.f.a(new zzgi(this.k));
      }
      if (this.j != null) {
        this.f.a(new zzgm(this.j), this.h);
      }
      if (this.m != null) {
        this.f.a(new zzcg(this.m));
      }
      if (this.n != null) {
        this.f.a(this.n.a());
      }
      if (this.o != null) {
        this.f.a(new zzg(this.o));
      }
      if (this.p != null) {
        this.f.a(this.p);
      }
      return;
    }
  }
  
  private void d(String paramString)
  {
    if (this.f == null) {
      throw new IllegalStateException("The ad unit ID must be set on InterstitialAd before " + paramString + " is called.");
    }
  }
  
  public void a()
  {
    try
    {
      d("show");
      this.f.f();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Failed to show interstitial.", localRemoteException);
    }
  }
  
  public void a(AdListener paramAdListener)
  {
    try
    {
      this.d = paramAdListener;
      zzu localzzu;
      if (this.f != null)
      {
        localzzu = this.f;
        if (paramAdListener == null) {
          break label38;
        }
      }
      label38:
      for (paramAdListener = new zzc(paramAdListener);; paramAdListener = null)
      {
        localzzu.a(paramAdListener);
        return;
      }
      return;
    }
    catch (RemoteException paramAdListener)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Failed to set the AdListener.", paramAdListener);
    }
  }
  
  public void a(zza paramzza)
  {
    try
    {
      this.e = paramzza;
      zzu localzzu;
      if (this.f != null)
      {
        localzzu = this.f;
        if (paramzza == null) {
          break label38;
        }
      }
      label38:
      for (paramzza = new zzb(paramzza);; paramzza = null)
      {
        localzzu.a(paramzza);
        return;
      }
      return;
    }
    catch (RemoteException paramzza)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Failed to set the AdClickListener.", paramzza);
    }
  }
  
  public void a(zzaa paramzzaa)
  {
    try
    {
      if (this.f == null) {
        c("loadAd");
      }
      if (this.f.a(this.c.a(this.b, paramzzaa))) {
        this.a.a(paramzzaa.j());
      }
      return;
    }
    catch (RemoteException paramzzaa)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Failed to load ad.", paramzzaa);
    }
  }
  
  public void a(RewardedVideoAdListener paramRewardedVideoAdListener)
  {
    try
    {
      this.o = paramRewardedVideoAdListener;
      zzu localzzu;
      if (this.f != null)
      {
        localzzu = this.f;
        if (paramRewardedVideoAdListener == null) {
          break label38;
        }
      }
      label38:
      for (paramRewardedVideoAdListener = new zzg(paramRewardedVideoAdListener);; paramRewardedVideoAdListener = null)
      {
        localzzu.a(paramRewardedVideoAdListener);
        return;
      }
      return;
    }
    catch (RemoteException paramRewardedVideoAdListener)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Failed to set the AdListener.", paramRewardedVideoAdListener);
    }
  }
  
  public void a(String paramString)
  {
    if (this.g != null) {
      throw new IllegalStateException("The ad unit ID can only be set once on InterstitialAd.");
    }
    this.g = paramString;
  }
  
  public void a(boolean paramBoolean)
  {
    this.q = paramBoolean;
  }
  
  public void b(String paramString)
  {
    try
    {
      this.p = paramString;
      if (this.f != null) {
        this.f.a(paramString);
      }
      return;
    }
    catch (RemoteException paramString)
    {
      com.google.android.gms.ads.internal.util.client.zzb.d("Failed to set the AdListener.", paramString);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzac.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */