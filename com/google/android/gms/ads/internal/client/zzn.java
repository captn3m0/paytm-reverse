package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.internal.reward.client.zzf;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.internal.zzcv;
import com.google.android.gms.internal.zzhb;

@zzhb
public class zzn
{
  private static final Object a = new Object();
  private static zzn b;
  private final zza c = new zza();
  private final zze d = new zze();
  private final zzl e = new zzl();
  private final zzaf f = new zzaf();
  private final zzcv g = new zzcv();
  private final zzf h = new zzf();
  
  static
  {
    a(new zzn());
  }
  
  public static zza a()
  {
    return e().c;
  }
  
  protected static void a(zzn paramzzn)
  {
    synchronized (a)
    {
      b = paramzzn;
      return;
    }
  }
  
  public static zze b()
  {
    return e().d;
  }
  
  public static zzl c()
  {
    return e().e;
  }
  
  public static zzcv d()
  {
    return e().g;
  }
  
  private static zzn e()
  {
    synchronized (a)
    {
      zzn localzzn = b;
      return localzzn;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */