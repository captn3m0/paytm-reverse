package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.internal.zzhb;

@zzhb
public final class zzc
  extends zzq.zza
{
  private final AdListener a;
  
  public zzc(AdListener paramAdListener)
  {
    this.a = paramAdListener;
  }
  
  public void a()
  {
    this.a.c();
  }
  
  public void a(int paramInt)
  {
    this.a.a(paramInt);
  }
  
  public void b()
  {
    this.a.d();
  }
  
  public void c()
  {
    this.a.a();
  }
  
  public void d()
  {
    this.a.b();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */