package com.google.android.gms.ads.internal.client;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcel;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.zzhb;

@zzhb
public class AdSizeParcel
  implements SafeParcelable
{
  public static final zzi CREATOR = new zzi();
  public final int a;
  public final String b;
  public final int c;
  public final int d;
  public final boolean e;
  public final int f;
  public final int g;
  public final AdSizeParcel[] h;
  public final boolean i;
  public final boolean j;
  public boolean k;
  
  public AdSizeParcel()
  {
    this(5, "interstitial_mb", 0, 0, true, 0, 0, null, false, false, false);
  }
  
  AdSizeParcel(int paramInt1, String paramString, int paramInt2, int paramInt3, boolean paramBoolean1, int paramInt4, int paramInt5, AdSizeParcel[] paramArrayOfAdSizeParcel, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4)
  {
    this.a = paramInt1;
    this.b = paramString;
    this.c = paramInt2;
    this.d = paramInt3;
    this.e = paramBoolean1;
    this.f = paramInt4;
    this.g = paramInt5;
    this.h = paramArrayOfAdSizeParcel;
    this.i = paramBoolean2;
    this.j = paramBoolean3;
    this.k = paramBoolean4;
  }
  
  public AdSizeParcel(Context paramContext, AdSize paramAdSize)
  {
    this(paramContext, new AdSize[] { paramAdSize });
  }
  
  public AdSizeParcel(Context paramContext, AdSize[] paramArrayOfAdSize)
  {
    AdSize localAdSize = paramArrayOfAdSize[0];
    this.a = 5;
    this.e = false;
    this.j = localAdSize.c();
    int n;
    label66:
    int i1;
    label78:
    DisplayMetrics localDisplayMetrics;
    label129:
    int i2;
    int m;
    if (this.j)
    {
      this.f = AdSize.a.b();
      this.c = AdSize.a.a();
      if (this.f != -1) {
        break label312;
      }
      n = 1;
      if (this.c != -2) {
        break label318;
      }
      i1 = 1;
      localDisplayMetrics = paramContext.getResources().getDisplayMetrics();
      if (n == 0) {
        break label336;
      }
      if ((!zzn.a().c(paramContext)) || (!zzn.a().d(paramContext))) {
        break label324;
      }
      this.g = (a(localDisplayMetrics) - zzn.a().e(paramContext));
      double d1 = this.g / localDisplayMetrics.density;
      i2 = (int)d1;
      m = i2;
      if (d1 - (int)d1 >= 0.01D) {
        m = i2 + 1;
      }
      label168:
      if (i1 == 0) {
        break label361;
      }
      i2 = c(localDisplayMetrics);
      label180:
      this.d = zzn.a().a(localDisplayMetrics, i2);
      if ((n == 0) && (i1 == 0)) {
        break label370;
      }
      this.b = (m + "x" + i2 + "_as");
    }
    for (;;)
    {
      if (paramArrayOfAdSize.length <= 1) {
        break label398;
      }
      this.h = new AdSizeParcel[paramArrayOfAdSize.length];
      m = 0;
      while (m < paramArrayOfAdSize.length)
      {
        this.h[m] = new AdSizeParcel(paramContext, paramArrayOfAdSize[m]);
        m += 1;
      }
      this.f = localAdSize.b();
      this.c = localAdSize.a();
      break;
      label312:
      n = 0;
      break label66;
      label318:
      i1 = 0;
      break label78;
      label324:
      this.g = a(localDisplayMetrics);
      break label129;
      label336:
      m = this.f;
      this.g = zzn.a().a(localDisplayMetrics, this.f);
      break label168;
      label361:
      i2 = this.c;
      break label180;
      label370:
      if (this.j) {
        this.b = "320x50_mb";
      } else {
        this.b = localAdSize.toString();
      }
    }
    label398:
    this.h = null;
    this.i = false;
    this.k = false;
  }
  
  public AdSizeParcel(AdSizeParcel paramAdSizeParcel, AdSizeParcel[] paramArrayOfAdSizeParcel)
  {
    this(5, paramAdSizeParcel.b, paramAdSizeParcel.c, paramAdSizeParcel.d, paramAdSizeParcel.e, paramAdSizeParcel.f, paramAdSizeParcel.g, paramArrayOfAdSizeParcel, paramAdSizeParcel.i, paramAdSizeParcel.j, paramAdSizeParcel.k);
  }
  
  public static int a(DisplayMetrics paramDisplayMetrics)
  {
    return paramDisplayMetrics.widthPixels;
  }
  
  public static AdSizeParcel a()
  {
    return new AdSizeParcel(5, "reward_mb", 0, 0, true, 0, 0, null, false, false, false);
  }
  
  public static AdSizeParcel a(Context paramContext)
  {
    return new AdSizeParcel(5, "320x50_mb", 0, 0, false, 0, 0, null, true, false, false);
  }
  
  public static int b(DisplayMetrics paramDisplayMetrics)
  {
    return (int)(c(paramDisplayMetrics) * paramDisplayMetrics.density);
  }
  
  private static int c(DisplayMetrics paramDisplayMetrics)
  {
    int m = (int)(paramDisplayMetrics.heightPixels / paramDisplayMetrics.density);
    if (m <= 400) {
      return 32;
    }
    if (m <= 720) {
      return 50;
    }
    return 90;
  }
  
  public void a(boolean paramBoolean)
  {
    this.k = paramBoolean;
  }
  
  public AdSize b()
  {
    return com.google.android.gms.ads.zza.a(this.f, this.c, this.b);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzi.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/client/AdSizeParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */