package com.google.android.gms.ads.internal.client;

import android.os.Handler;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.internal.zzcf;
import com.google.android.gms.internal.zzgd;
import com.google.android.gms.internal.zzgh;

public class zzah
  extends zzu.zza
{
  private zzq a;
  
  public com.google.android.gms.dynamic.zzd a()
  {
    return null;
  }
  
  public void a(AdSizeParcel paramAdSizeParcel) {}
  
  public void a(zzp paramzzp) {}
  
  public void a(zzq paramzzq)
  {
    this.a = paramzzq;
  }
  
  public void a(zzw paramzzw) {}
  
  public void a(zzx paramzzx) {}
  
  public void a(com.google.android.gms.ads.internal.reward.client.zzd paramzzd) {}
  
  public void a(zzcf paramzzcf) {}
  
  public void a(zzgd paramzzgd) {}
  
  public void a(zzgh paramzzgh, String paramString) {}
  
  public void a(String paramString) {}
  
  public void a(boolean paramBoolean) {}
  
  public boolean a(AdRequestParcel paramAdRequestParcel)
  {
    zzb.b("This app is using a lightweight version of the Google Mobile Ads SDK that requires the latest Google Play services to be installed, but Google Play services is either missing or out of date.");
    zza.a.post(new Runnable()
    {
      public void run()
      {
        if (zzah.a(zzah.this) != null) {}
        try
        {
          zzah.a(zzah.this).a(1);
          return;
        }
        catch (RemoteException localRemoteException)
        {
          zzb.d("Could not notify onAdFailedToLoad event.", localRemoteException);
        }
      }
    });
    return false;
  }
  
  public void b() {}
  
  public void b_() {}
  
  public boolean c()
  {
    return false;
  }
  
  public void d() {}
  
  public void f() {}
  
  public void f_() {}
  
  public void h() {}
  
  public AdSizeParcel i()
  {
    return null;
  }
  
  public String j()
  {
    return null;
  }
  
  public boolean k()
  {
    return false;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/client/zzah.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */