package com.google.android.gms.ads.internal;

import android.content.Context;
import android.support.v4.util.SimpleArrayMap;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.client.zzq;
import com.google.android.gms.ads.internal.client.zzr;
import com.google.android.gms.ads.internal.client.zzs.zza;
import com.google.android.gms.ads.internal.client.zzx;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.internal.zzcr;
import com.google.android.gms.internal.zzcs;
import com.google.android.gms.internal.zzct;
import com.google.android.gms.internal.zzcu;
import com.google.android.gms.internal.zzex;
import com.google.android.gms.internal.zzhb;

@zzhb
public class zzj
  extends zzs.zza
{
  private zzq a;
  private zzcr b;
  private zzcs c;
  private SimpleArrayMap<String, zzct> d;
  private SimpleArrayMap<String, zzcu> e;
  private NativeAdOptionsParcel f;
  private zzx g;
  private final Context h;
  private final zzex i;
  private final String j;
  private final VersionInfoParcel k;
  private final zzd l;
  
  public zzj(Context paramContext, String paramString, zzex paramzzex, VersionInfoParcel paramVersionInfoParcel, zzd paramzzd)
  {
    this.h = paramContext;
    this.j = paramString;
    this.i = paramzzex;
    this.k = paramVersionInfoParcel;
    this.e = new SimpleArrayMap();
    this.d = new SimpleArrayMap();
    this.l = paramzzd;
  }
  
  public zzr a()
  {
    return new zzi(this.h, this.j, this.i, this.k, this.a, this.b, this.c, this.e, this.d, this.f, this.g, this.l);
  }
  
  public void a(zzq paramzzq)
  {
    this.a = paramzzq;
  }
  
  public void a(zzx paramzzx)
  {
    this.g = paramzzx;
  }
  
  public void a(NativeAdOptionsParcel paramNativeAdOptionsParcel)
  {
    this.f = paramNativeAdOptionsParcel;
  }
  
  public void a(zzcr paramzzcr)
  {
    this.b = paramzzcr;
  }
  
  public void a(zzcs paramzzcs)
  {
    this.c = paramzzcs;
  }
  
  public void a(String paramString, zzcu paramzzcu, zzct paramzzct)
  {
    if (TextUtils.isEmpty(paramString)) {
      throw new IllegalArgumentException("Custom template ID for native custom template ad is empty. Please provide a valid template id.");
    }
    this.e.put(paramString, paramzzcu);
    this.d.put(paramString, paramzzct);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */