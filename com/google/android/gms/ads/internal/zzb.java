package com.google.android.gms.ads.internal;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.util.SimpleArrayMap;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzn;
import com.google.android.gms.ads.internal.overlay.zzm;
import com.google.android.gms.ads.internal.purchase.GInAppPurchaseManagerInfoParcel;
import com.google.android.gms.ads.internal.purchase.zzc;
import com.google.android.gms.ads.internal.purchase.zzf;
import com.google.android.gms.ads.internal.purchase.zzi;
import com.google.android.gms.ads.internal.purchase.zzj;
import com.google.android.gms.ads.internal.purchase.zzk;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel.zza;
import com.google.android.gms.ads.internal.request.CapabilityParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.internal.zzax;
import com.google.android.gms.internal.zzbt;
import com.google.android.gms.internal.zzcb;
import com.google.android.gms.internal.zzdh;
import com.google.android.gms.internal.zzen;
import com.google.android.gms.internal.zzeo;
import com.google.android.gms.internal.zzep;
import com.google.android.gms.internal.zzeq;
import com.google.android.gms.internal.zzet;
import com.google.android.gms.internal.zzex;
import com.google.android.gms.internal.zzey;
import com.google.android.gms.internal.zzga;
import com.google.android.gms.internal.zzgd;
import com.google.android.gms.internal.zzgh;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzif;
import com.google.android.gms.internal.zzig;
import com.google.android.gms.internal.zzih;
import com.google.android.gms.internal.zzik;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzir;
import com.google.android.gms.internal.zzis;
import com.google.android.gms.internal.zzjp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@zzhb
public abstract class zzb
  extends zza
  implements com.google.android.gms.ads.internal.overlay.zzg, zzj, zzdh, zzep
{
  protected final zzex j;
  protected transient boolean k;
  private final Messenger l;
  
  public zzb(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, zzex paramzzex, VersionInfoParcel paramVersionInfoParcel, zzd paramzzd)
  {
    this(new zzs(paramContext, paramAdSizeParcel, paramString, paramVersionInfoParcel), paramzzex, null, paramzzd);
  }
  
  zzb(zzs paramzzs, zzex paramzzex, zzq paramzzq, zzd paramzzd)
  {
    super(paramzzs, paramzzq, paramzzd);
    this.j = paramzzex;
    this.l = new Messenger(new zzga(this.f.c));
    this.k = false;
  }
  
  private AdRequestInfoParcel.zza a(AdRequestParcel paramAdRequestParcel, Bundle paramBundle)
  {
    ApplicationInfo localApplicationInfo = this.f.c.getApplicationInfo();
    DisplayMetrics localDisplayMetrics;
    String str1;
    Object localObject;
    String str2;
    long l1;
    Bundle localBundle;
    ArrayList localArrayList;
    PackageInfo localPackageInfo2;
    try
    {
      PackageInfo localPackageInfo1 = this.f.c.getPackageManager().getPackageInfo(localApplicationInfo.packageName, 0);
      localDisplayMetrics = this.f.c.getResources().getDisplayMetrics();
      str1 = null;
      localObject = str1;
      if (this.f.f != null)
      {
        localObject = str1;
        if (this.f.f.getParent() != null)
        {
          localObject = new int[2];
          this.f.f.getLocationOnScreen((int[])localObject);
          int n = localObject[0];
          int i1 = localObject[1];
          int i2 = this.f.f.getWidth();
          int i3 = this.f.f.getHeight();
          int m = 0;
          i = m;
          if (this.f.f.isShown())
          {
            i = m;
            if (n + i2 > 0)
            {
              i = m;
              if (i1 + i3 > 0)
              {
                i = m;
                if (n <= localDisplayMetrics.widthPixels)
                {
                  i = m;
                  if (i1 <= localDisplayMetrics.heightPixels) {
                    i = 1;
                  }
                }
              }
            }
          }
          localObject = new Bundle(5);
          ((Bundle)localObject).putInt("x", n);
          ((Bundle)localObject).putInt("y", i1);
          ((Bundle)localObject).putInt("width", i2);
          ((Bundle)localObject).putInt("height", i3);
          ((Bundle)localObject).putInt("visible", i);
        }
      }
      str1 = zzr.h().c();
      this.f.l = new zzig(str1, this.f.b);
      this.f.l.a(paramAdRequestParcel);
      str2 = zzr.e().a(this.f.c, this.f.f, this.f.i);
      l2 = 0L;
      l1 = l2;
      if (this.f.p == null) {}
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      try
      {
        l1 = this.f.p.b();
        String str3 = UUID.randomUUID().toString();
        localBundle = zzr.h().a(this.f.c, this, str1);
        localArrayList = new ArrayList();
        int i = 0;
        while (i < this.f.v.size())
        {
          localArrayList.add(this.f.v.b(i));
          i += 1;
          continue;
          localNameNotFoundException = localNameNotFoundException;
          localPackageInfo2 = null;
        }
      }
      catch (RemoteException localRemoteException)
      {
        for (;;)
        {
          long l2;
          zzin.d("Cannot get correlation id, default to 0.");
          l1 = l2;
        }
      }
    }
    boolean bool1;
    if (this.f.q != null)
    {
      bool1 = true;
      if ((this.f.r == null) || (!zzr.h().m())) {
        break label670;
      }
    }
    label670:
    for (boolean bool2 = true;; bool2 = false)
    {
      boolean bool3 = this.i.c.a();
      return new AdRequestInfoParcel.zza((Bundle)localObject, paramAdRequestParcel, this.f.i, this.f.b, localApplicationInfo, localPackageInfo2, str1, zzr.h().a(), this.f.e, localBundle, this.f.z, localArrayList, paramBundle, zzr.h().g(), this.l, localDisplayMetrics.widthPixels, localDisplayMetrics.heightPixels, localDisplayMetrics.density, str2, l1, localRemoteException, zzbt.a(), this.f.a, this.f.w, new CapabilityParcel(bool1, bool2, bool3), this.f.i(), zzr.e().f(), zzr.e().j(this.f.c), zzr.e().b(this.f.f));
      bool1 = false;
      break;
    }
  }
  
  public void a(zzgd paramzzgd)
  {
    com.google.android.gms.common.internal.zzx.b("setInAppPurchaseListener must be called on the main UI thread.");
    this.f.q = paramzzgd;
  }
  
  public void a(zzgh paramzzgh, String paramString)
  {
    com.google.android.gms.common.internal.zzx.b("setPlayStorePurchaseParams must be called on the main UI thread.");
    this.f.A = new zzk(paramString);
    this.f.r = paramzzgh;
    if ((!zzr.h().f()) && (paramzzgh != null)) {
      new zzc(this.f.c, this.f.r, this.f.A).f();
    }
  }
  
  protected void a(zzif paramzzif, boolean paramBoolean)
  {
    if (paramzzif == null) {
      zzin.d("Ad state was null when trying to ping impression URLs.");
    }
    do
    {
      return;
      super.c(paramzzif);
      if ((paramzzif.q != null) && (paramzzif.q.d != null)) {
        zzr.r().a(this.f.c, this.f.e.b, paramzzif, this.f.b, paramBoolean, paramzzif.q.d);
      }
    } while ((paramzzif.n == null) || (paramzzif.n.g == null));
    zzr.r().a(this.f.c, this.f.e.b, paramzzif, this.f.b, paramBoolean, paramzzif.n.g);
  }
  
  public void a(String paramString, ArrayList<String> paramArrayList)
  {
    paramArrayList = new com.google.android.gms.ads.internal.purchase.zzd(paramString, paramArrayList, this.f.c, this.f.e.b);
    if (this.f.q == null)
    {
      zzin.d("InAppPurchaseListener is not set. Try to launch default purchase flow.");
      if (!zzn.a().b(this.f.c))
      {
        zzin.d("Google Play Service unavailable, cannot launch default purchase flow.");
        return;
      }
      if (this.f.r == null)
      {
        zzin.d("PlayStorePurchaseListener is not set.");
        return;
      }
      if (this.f.A == null)
      {
        zzin.d("PlayStorePurchaseVerifier is not initialized.");
        return;
      }
      if (this.f.E)
      {
        zzin.d("An in-app purchase request is already in progress, abort");
        return;
      }
      this.f.E = true;
      try
      {
        if (!this.f.r.a(paramString))
        {
          this.f.E = false;
          return;
        }
      }
      catch (RemoteException paramString)
      {
        zzin.d("Could not start In-App purchase.");
        this.f.E = false;
        return;
      }
      zzr.o().a(this.f.c, this.f.e.e, new GInAppPurchaseManagerInfoParcel(this.f.c, this.f.A, paramArrayList, this));
      return;
    }
    try
    {
      this.f.q.a(paramArrayList);
      return;
    }
    catch (RemoteException paramString)
    {
      zzin.d("Could not start In-App purchase.");
    }
  }
  
  public void a(String paramString, boolean paramBoolean, int paramInt, final Intent paramIntent, zzf paramzzf)
  {
    try
    {
      if (this.f.r != null) {
        this.f.r.a(new com.google.android.gms.ads.internal.purchase.zzg(this.f.c, paramString, paramBoolean, paramInt, paramIntent, paramzzf));
      }
      zzir.a.postDelayed(new Runnable()
      {
        public void run()
        {
          int i = zzr.o().a(paramIntent);
          zzr.o();
          if ((i == 0) && (zzb.this.f.j != null) && (zzb.this.f.j.b != null) && (zzb.this.f.j.b.i() != null)) {
            zzb.this.f.j.b.i().a();
          }
          zzb.this.f.E = false;
        }
      }, 500L);
      return;
    }
    catch (RemoteException paramString)
    {
      for (;;)
      {
        zzin.d("Fail to invoke PlayStorePurchaseListener.");
      }
    }
  }
  
  public boolean a(AdRequestParcel paramAdRequestParcel, zzcb paramzzcb)
  {
    if (!s()) {
      return false;
    }
    Bundle localBundle = a(zzr.h().a(this.f.c));
    this.e.cancel();
    this.f.D = 0;
    paramAdRequestParcel = a(paramAdRequestParcel, localBundle);
    paramzzcb.a("seq_num", paramAdRequestParcel.g);
    paramzzcb.a("request_id", paramAdRequestParcel.v);
    paramzzcb.a("session_id", paramAdRequestParcel.h);
    if (paramAdRequestParcel.f != null) {
      paramzzcb.a("app_version", String.valueOf(paramAdRequestParcel.f.versionCode));
    }
    this.f.g = zzr.a().a(this.f.c, paramAdRequestParcel, this.f.d, this);
    return true;
  }
  
  protected boolean a(AdRequestParcel paramAdRequestParcel, zzif paramzzif, boolean paramBoolean)
  {
    if ((!paramBoolean) && (this.f.e()))
    {
      if (paramzzif.h <= 0L) {
        break label43;
      }
      this.e.a(paramAdRequestParcel, paramzzif.h);
    }
    for (;;)
    {
      return this.e.c();
      label43:
      if ((paramzzif.q != null) && (paramzzif.q.g > 0L)) {
        this.e.a(paramAdRequestParcel, paramzzif.q.g);
      } else if ((!paramzzif.m) && (paramzzif.d == 2)) {
        this.e.a(paramAdRequestParcel);
      }
    }
  }
  
  boolean a(zzif paramzzif)
  {
    boolean bool = false;
    Object localObject;
    if (this.g != null)
    {
      localObject = this.g;
      this.g = null;
    }
    for (;;)
    {
      return a((AdRequestParcel)localObject, paramzzif, bool);
      AdRequestParcel localAdRequestParcel = paramzzif.a;
      localObject = localAdRequestParcel;
      if (localAdRequestParcel.c != null)
      {
        bool = localAdRequestParcel.c.getBoolean("_noRefresh", false);
        localObject = localAdRequestParcel;
      }
    }
  }
  
  protected boolean a(zzif paramzzif1, zzif paramzzif2)
  {
    int i = 0;
    if ((paramzzif1 != null) && (paramzzif1.r != null)) {
      paramzzif1.r.a(null);
    }
    if (paramzzif2.r != null) {
      paramzzif2.r.a(this);
    }
    int m;
    if (paramzzif2.q != null)
    {
      m = paramzzif2.q.l;
      i = paramzzif2.q.m;
    }
    for (;;)
    {
      this.f.B.a(m, i);
      return true;
      m = 0;
    }
  }
  
  public void b(zzif paramzzif)
  {
    super.b(paramzzif);
    if ((paramzzif.d == 3) && (paramzzif.q != null) && (paramzzif.q.e != null))
    {
      zzin.a("Pinging no fill URLs.");
      zzr.r().a(this.f.c, this.f.e.b, paramzzif, this.f.b, false, paramzzif.q.e);
    }
  }
  
  protected boolean b(AdRequestParcel paramAdRequestParcel)
  {
    return (super.b(paramAdRequestParcel)) && (!this.k);
  }
  
  public void b_()
  {
    com.google.android.gms.common.internal.zzx.b("resume must be called on the main UI thread.");
    if ((this.f.j != null) && (this.f.j.b != null) && (this.f.e())) {
      zzr.g().b(this.f.j.b);
    }
    if ((this.f.j != null) && (this.f.j.o != null)) {}
    try
    {
      this.f.j.o.e();
      this.e.b();
      this.h.e(this.f.j);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        zzin.d("Could not resume mediation adapter.");
      }
    }
  }
  
  public void c_()
  {
    this.h.b(this.f.j);
    this.k = false;
    n();
    this.f.l.c();
  }
  
  public void d()
  {
    com.google.android.gms.common.internal.zzx.b("pause must be called on the main UI thread.");
    if ((this.f.j != null) && (this.f.j.b != null) && (this.f.e())) {
      zzr.g().a(this.f.j.b);
    }
    if ((this.f.j != null) && (this.f.j.o != null)) {}
    try
    {
      this.f.j.o.d();
      this.h.d(this.f.j);
      this.e.a();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        zzin.d("Could not pause mediation adapter.");
      }
    }
  }
  
  public void d_()
  {
    this.k = true;
    p();
  }
  
  public void e()
  {
    if (this.f.j == null)
    {
      zzin.d("Ad state was null when trying to ping click URLs.");
      return;
    }
    if ((this.f.j.q != null) && (this.f.j.q.c != null)) {
      zzr.r().a(this.f.c, this.f.e.b, this.f.j, this.f.b, false, this.f.j.q.c);
    }
    if ((this.f.j.n != null) && (this.f.j.n.f != null)) {
      zzr.r().a(this.f.c, this.f.e.b, this.f.j, this.f.b, false, this.f.j.n.f);
    }
    super.e();
  }
  
  public void e_()
  {
    this.h.d(this.f.j);
  }
  
  public void f()
  {
    throw new IllegalStateException("showInterstitial is not supported for current ad type");
  }
  
  public void g()
  {
    this.h.e(this.f.j);
  }
  
  public String j()
  {
    if (this.f.j == null) {
      return null;
    }
    return this.f.j.p;
  }
  
  protected boolean s()
  {
    boolean bool = true;
    if ((!zzr.e().a(this.f.c.getPackageManager(), this.f.c.getPackageName(), "android.permission.INTERNET")) || (!zzr.e().a(this.f.c))) {
      bool = false;
    }
    return bool;
  }
  
  public void t()
  {
    e();
  }
  
  public void u()
  {
    c_();
  }
  
  public void v()
  {
    l();
  }
  
  public void w()
  {
    d_();
  }
  
  public void x()
  {
    if (this.f.j != null) {
      zzin.d("Mediation adapter " + this.f.j.p + " refreshed, but mediation adapters should never refresh.");
    }
    a(this.f.j, true);
    q();
  }
  
  public void y()
  {
    a(this.f.j, false);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */