package com.google.android.gms.ads.internal;

import android.content.Context;
import android.os.Handler;
import android.os.RemoteException;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzax;
import com.google.android.gms.internal.zzcb;
import com.google.android.gms.internal.zzcc;
import com.google.android.gms.internal.zzce;
import com.google.android.gms.internal.zzcf;
import com.google.android.gms.internal.zzdf;
import com.google.android.gms.internal.zzeh;
import com.google.android.gms.internal.zzex;
import com.google.android.gms.internal.zzft;
import com.google.android.gms.internal.zzgr;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzif;
import com.google.android.gms.internal.zzif.zza;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzir;
import com.google.android.gms.internal.zziu;
import com.google.android.gms.internal.zzjp;
import com.google.android.gms.internal.zzjq;
import com.google.android.gms.internal.zzjr;
import java.util.Map;

@zzhb
public abstract class zzc
  extends zzb
  implements zzg, zzft
{
  public zzc(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, zzex paramzzex, VersionInfoParcel paramVersionInfoParcel, zzd paramzzd)
  {
    super(paramContext, paramAdSizeParcel, paramString, paramzzex, paramVersionInfoParcel, paramzzd);
  }
  
  public void A()
  {
    y();
    h();
  }
  
  public void B()
  {
    n();
  }
  
  protected zzjp a(zzif.zza paramzza, zze paramzze)
  {
    Object localObject = this.f.f.getNextView();
    if ((localObject instanceof zzjp))
    {
      zzin.a("Reusing webview...");
      localObject = (zzjp)localObject;
      ((zzjp)localObject).a(this.f.c, this.f.i, this.a);
    }
    for (;;)
    {
      ((zzjp)localObject).l().a(this, this, this, this, false, this, null, paramzze, this);
      a((zzeh)localObject);
      ((zzjp)localObject).b(paramzza.a.w);
      return (zzjp)localObject;
      if (localObject != null) {
        this.f.f.removeView((View)localObject);
      }
      localObject = zzr.f().a(this.f.c, this.f.i, false, false, this.f.d, this.f.e, this.a, this.i);
      if (this.f.i.h == null) {
        a(((zzjp)localObject).b());
      }
    }
  }
  
  public void a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    p();
  }
  
  public void a(zzcf paramzzcf)
  {
    zzx.b("setOnCustomRenderedAdLoadedListener must be called on the main UI thread.");
    this.f.x = paramzzcf;
  }
  
  protected void a(zzeh paramzzeh)
  {
    paramzzeh.a("/trackActiveViewUnit", new zzdf()
    {
      public void a(zzjp paramAnonymouszzjp, Map<String, String> paramAnonymousMap)
      {
        if (zzc.this.f.j != null)
        {
          zzc.this.h.a(zzc.this.f.i, zzc.this.f.j, paramAnonymouszzjp.b(), paramAnonymouszzjp);
          return;
        }
        zzin.d("Request to enable ActiveView before adState is available.");
      }
    });
  }
  
  protected void a(final zzif.zza paramzza, final zzcb paramzzcb)
  {
    if (paramzza.e != -2)
    {
      zzir.a.post(new Runnable()
      {
        public void run()
        {
          zzc.this.b(new zzif(paramzza, null, null, null, null, null, null));
        }
      });
      return;
    }
    if (paramzza.d != null) {
      this.f.i = paramzza.d;
    }
    if ((paramzza.b.h) && (!paramzza.b.C))
    {
      this.f.D = 0;
      this.f.h = zzr.d().a(this.f.c, this, paramzza, this.f.d, null, this.j, this, paramzzcb);
      return;
    }
    zzir.a.post(new Runnable()
    {
      public void run()
      {
        if ((paramzza.b.s) && (zzc.this.f.x != null))
        {
          Object localObject = null;
          if (paramzza.b.b != null) {
            localObject = zzr.e().a(paramzza.b.b);
          }
          localObject = new zzcc(zzc.this, (String)localObject, paramzza.b.c);
          zzc.this.f.D = 1;
          try
          {
            zzc.this.d = false;
            zzc.this.f.x.a((zzce)localObject);
            return;
          }
          catch (RemoteException localRemoteException)
          {
            zzin.d("Could not call the onCustomRenderedAdLoadedListener.", localRemoteException);
            zzc.this.d = true;
          }
        }
        final zze localzze = new zze();
        zzjp localzzjp = zzc.this.a(paramzza, localzze);
        localzze.a(new zze.zzb(paramzza, localzzjp));
        localzzjp.setOnTouchListener(new View.OnTouchListener()
        {
          public boolean onTouch(View paramAnonymous2View, MotionEvent paramAnonymous2MotionEvent)
          {
            localzze.a();
            return false;
          }
        });
        localzzjp.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymous2View)
          {
            localzze.a();
          }
        });
        zzc.this.f.D = 0;
        zzc.this.f.h = zzr.d().a(zzc.this.f.c, zzc.this, paramzza, zzc.this.f.d, localzzjp, zzc.this.j, zzc.this, paramzzcb);
      }
    });
  }
  
  protected boolean a(zzif paramzzif1, zzif paramzzif2)
  {
    if ((this.f.e()) && (this.f.f != null)) {
      this.f.f.a().a(paramzzif2.A);
    }
    return super.a(paramzzif1, paramzzif2);
  }
  
  public void b(View paramView)
  {
    this.f.C = paramView;
    b(new zzif(this.f.k, null, null, null, null, null, null));
  }
  
  public void z()
  {
    e();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */