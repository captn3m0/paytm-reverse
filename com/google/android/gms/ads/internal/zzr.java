package com.google.android.gms.ads.internal;

import android.os.Build.VERSION;
import com.google.android.gms.ads.internal.overlay.zze;
import com.google.android.gms.ads.internal.purchase.zzi;
import com.google.android.gms.internal.zzbq;
import com.google.android.gms.internal.zzbr;
import com.google.android.gms.internal.zzbs;
import com.google.android.gms.internal.zzbw;
import com.google.android.gms.internal.zzdq;
import com.google.android.gms.internal.zzdy;
import com.google.android.gms.internal.zzet;
import com.google.android.gms.internal.zzgr;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzhk;
import com.google.android.gms.internal.zzih;
import com.google.android.gms.internal.zzir;
import com.google.android.gms.internal.zzis;
import com.google.android.gms.internal.zzix;
import com.google.android.gms.internal.zzjr;
import com.google.android.gms.internal.zzmq;
import com.google.android.gms.internal.zzmt;

@zzhb
public class zzr
{
  private static final Object a = new Object();
  private static zzr b;
  private final com.google.android.gms.ads.internal.request.zza c = new com.google.android.gms.ads.internal.request.zza();
  private final com.google.android.gms.ads.internal.overlay.zza d = new com.google.android.gms.ads.internal.overlay.zza();
  private final zze e = new zze();
  private final zzgr f = new zzgr();
  private final zzir g = new zzir();
  private final zzjr h = new zzjr();
  private final zzis i = zzis.a(Build.VERSION.SDK_INT);
  private final zzih j = new zzih(this.g);
  private final zzmq k = new zzmt();
  private final zzbw l = new zzbw();
  private final zzhk m = new zzhk();
  private final zzbr n = new zzbr();
  private final zzbq o = new zzbq();
  private final zzbs p = new zzbs();
  private final zzi q = new zzi();
  private final zzdy r = new zzdy();
  private final zzix s = new zzix();
  private final zzet t = new zzet();
  private final zzo u = new zzo();
  private final zzdq v = new zzdq();
  
  static
  {
    a(new zzr());
  }
  
  public static com.google.android.gms.ads.internal.request.zza a()
  {
    return u().c;
  }
  
  protected static void a(zzr paramzzr)
  {
    synchronized (a)
    {
      b = paramzzr;
      return;
    }
  }
  
  public static com.google.android.gms.ads.internal.overlay.zza b()
  {
    return u().d;
  }
  
  public static zze c()
  {
    return u().e;
  }
  
  public static zzgr d()
  {
    return u().f;
  }
  
  public static zzir e()
  {
    return u().g;
  }
  
  public static zzjr f()
  {
    return u().h;
  }
  
  public static zzis g()
  {
    return u().i;
  }
  
  public static zzih h()
  {
    return u().j;
  }
  
  public static zzmq i()
  {
    return u().k;
  }
  
  public static zzbw j()
  {
    return u().l;
  }
  
  public static zzhk k()
  {
    return u().m;
  }
  
  public static zzbr l()
  {
    return u().n;
  }
  
  public static zzbq m()
  {
    return u().o;
  }
  
  public static zzbs n()
  {
    return u().p;
  }
  
  public static zzi o()
  {
    return u().q;
  }
  
  public static zzdy p()
  {
    return u().r;
  }
  
  public static zzix q()
  {
    return u().s;
  }
  
  public static zzet r()
  {
    return u().t;
  }
  
  public static zzo s()
  {
    return u().u;
  }
  
  public static zzdq t()
  {
    return u().v;
  }
  
  private static zzr u()
  {
    synchronized (a)
    {
      zzr localzzr = b;
      return localzzr;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/zzr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */