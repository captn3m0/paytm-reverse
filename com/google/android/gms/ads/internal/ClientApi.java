package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.widget.FrameLayout;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.ads.internal.client.zzs;
import com.google.android.gms.ads.internal.client.zzu;
import com.google.android.gms.ads.internal.purchase.zze;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.internal.zzbp;
import com.google.android.gms.internal.zzbt;
import com.google.android.gms.internal.zzcj;
import com.google.android.gms.internal.zzeb;
import com.google.android.gms.internal.zzew;
import com.google.android.gms.internal.zzfv;
import com.google.android.gms.internal.zzge;

public class ClientApi
  implements zzm
{
  public static void a()
  {
    com.google.android.gms.ads.internal.client.zzl.a = ClientApi.class.getName();
  }
  
  public zzs a(Context paramContext, String paramString, zzew paramzzew, VersionInfoParcel paramVersionInfoParcel)
  {
    return new zzj(paramContext, paramString, paramzzew, paramVersionInfoParcel, zzd.a());
  }
  
  public zzu a(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, zzew paramzzew, VersionInfoParcel paramVersionInfoParcel)
  {
    return new zzf(paramContext, paramAdSizeParcel, paramString, paramzzew, paramVersionInfoParcel, zzd.a());
  }
  
  public zzcj a(FrameLayout paramFrameLayout1, FrameLayout paramFrameLayout2)
  {
    return new com.google.android.gms.ads.internal.formats.zzk(paramFrameLayout1, paramFrameLayout2);
  }
  
  public zzge a(Activity paramActivity)
  {
    return new zze(paramActivity);
  }
  
  public zzu b(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, zzew paramzzew, VersionInfoParcel paramVersionInfoParcel)
  {
    if (((Boolean)zzbt.ae.c()).booleanValue()) {
      return new zzeb(paramContext, paramString, paramzzew, paramVersionInfoParcel, zzd.a());
    }
    return new zzk(paramContext, paramAdSizeParcel, paramString, paramzzew, paramVersionInfoParcel, zzd.a());
  }
  
  public zzfv b(Activity paramActivity)
  {
    return new com.google.android.gms.ads.internal.overlay.zzd(paramActivity);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/ClientApi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */