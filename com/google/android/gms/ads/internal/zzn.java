package com.google.android.gms.ads.internal;

import com.google.android.gms.ads.internal.client.zzy.zza;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzin;

@zzhb
public class zzn
  extends zzy.zza
{
  private static final Object a = new Object();
  private static zzn b;
  private final Object c;
  private boolean d;
  private float e;
  
  public static zzn b()
  {
    synchronized (a)
    {
      zzn localzzn = b;
      return localzzn;
    }
  }
  
  public void a()
  {
    synchronized (a)
    {
      if (this.d)
      {
        zzin.d("Mobile ads is initialized already.");
        return;
      }
      this.d = true;
      return;
    }
  }
  
  public void a(float paramFloat)
  {
    synchronized (this.c)
    {
      this.e = paramFloat;
      return;
    }
  }
  
  public float c()
  {
    synchronized (this.c)
    {
      float f = this.e;
      return f;
    }
  }
  
  public boolean d()
  {
    for (;;)
    {
      synchronized (this.c)
      {
        if (this.e >= 0.0F)
        {
          bool = true;
          return bool;
        }
      }
      boolean bool = false;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */