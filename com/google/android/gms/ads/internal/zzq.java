package com.google.android.gms.ads.internal;

import android.os.Handler;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzir;
import java.lang.ref.WeakReference;

@zzhb
public class zzq
{
  private final zza a;
  private final Runnable b;
  private AdRequestParcel c;
  private boolean d = false;
  private boolean e = false;
  private long f = 0L;
  
  public zzq(zza paramzza)
  {
    this(paramzza, new zza(zzir.a));
  }
  
  zzq(zza paramzza, zza paramzza1)
  {
    this.a = paramzza1;
    this.b = new Runnable()
    {
      public void run()
      {
        zzq.a(zzq.this, false);
        zza localzza = (zza)this.a.get();
        if (localzza != null) {
          localzza.c(zzq.a(zzq.this));
        }
      }
    };
  }
  
  public void a()
  {
    this.e = true;
    if (this.d) {
      this.a.a(this.b);
    }
  }
  
  public void a(AdRequestParcel paramAdRequestParcel)
  {
    a(paramAdRequestParcel, 60000L);
  }
  
  public void a(AdRequestParcel paramAdRequestParcel, long paramLong)
  {
    if (this.d) {
      zzin.d("An ad refresh is already scheduled.");
    }
    do
    {
      return;
      this.c = paramAdRequestParcel;
      this.d = true;
      this.f = paramLong;
    } while (this.e);
    zzin.c("Scheduling ad refresh " + paramLong + " milliseconds from now.");
    this.a.a(this.b, paramLong);
  }
  
  public void b()
  {
    this.e = false;
    if (this.d)
    {
      this.d = false;
      a(this.c, this.f);
    }
  }
  
  public boolean c()
  {
    return this.d;
  }
  
  public void cancel()
  {
    this.d = false;
    this.a.a(this.b);
  }
  
  public static class zza
  {
    private final Handler a;
    
    public zza(Handler paramHandler)
    {
      this.a = paramHandler;
    }
    
    public void a(Runnable paramRunnable)
    {
      this.a.removeCallbacks(paramRunnable);
    }
    
    public boolean a(Runnable paramRunnable, long paramLong)
    {
      return this.a.postDelayed(paramRunnable, paramLong);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */