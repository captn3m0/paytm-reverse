package com.google.android.gms.ads.internal.formats;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.RemoteException;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzch.zza;
import com.google.android.gms.internal.zzhb;

@zzhb
public class zzc
  extends zzch.zza
{
  private final Drawable a;
  private final Uri b;
  private final double c;
  
  public zzc(Drawable paramDrawable, Uri paramUri, double paramDouble)
  {
    this.a = paramDrawable;
    this.b = paramUri;
    this.c = paramDouble;
  }
  
  public zzd a()
    throws RemoteException
  {
    return zze.a(this.a);
  }
  
  public Uri b()
    throws RemoteException
  {
    return this.b;
  }
  
  public double c()
  {
    return this.c;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/formats/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */