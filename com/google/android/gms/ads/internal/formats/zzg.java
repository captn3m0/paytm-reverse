package com.google.android.gms.ads.internal.formats;

import android.content.Context;
import android.os.RemoteException;
import android.view.View;
import android.view.View.OnClickListener;
import com.google.android.gms.ads.internal.zzp;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzan;
import com.google.android.gms.internal.zzfb;
import com.google.android.gms.internal.zzfc;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzjp;
import java.lang.ref.WeakReference;
import java.util.Map;
import org.json.JSONObject;

@zzhb
public class zzg
  extends zzi
{
  private zzfb a;
  private zzfc b;
  private final zzp c;
  private zzh d;
  private boolean e = false;
  private Object f = new Object();
  
  private zzg(Context paramContext, zzp paramzzp, zzan paramzzan)
  {
    super(paramContext, paramzzp, null, paramzzan, null, null, null);
    this.c = paramzzp;
  }
  
  public zzg(Context paramContext, zzp paramzzp, zzan paramzzan, zzfb paramzzfb)
  {
    this(paramContext, paramzzp, paramzzan);
    this.a = paramzzfb;
  }
  
  public zzg(Context paramContext, zzp paramzzp, zzan paramzzan, zzfc paramzzfc)
  {
    this(paramContext, paramzzp, paramzzan);
    this.b = paramzzfc;
  }
  
  public zzb a(View.OnClickListener paramOnClickListener)
  {
    return null;
  }
  
  public void a()
  {
    zzx.b("recordImpression must be called on the main UI thread.");
    for (;;)
    {
      synchronized (this.f)
      {
        a(true);
        if (this.d != null)
        {
          this.d.a();
          this.c.y();
          return;
        }
        try
        {
          if ((this.a != null) && (!this.a.j())) {
            this.a.i();
          }
        }
        catch (RemoteException localRemoteException)
        {
          zzin.d("Failed to call recordImpression", localRemoteException);
        }
      }
      if ((this.b != null) && (!this.b.h())) {
        this.b.g();
      }
    }
  }
  
  public void a(View paramView)
  {
    synchronized (this.f)
    {
      this.e = true;
      try
      {
        if (this.a != null) {
          this.a.b(zze.a(paramView));
        }
        for (;;)
        {
          this.e = false;
          return;
          if (this.b != null) {
            this.b.b(zze.a(paramView));
          }
        }
      }
      catch (RemoteException paramView)
      {
        for (;;)
        {
          zzin.d("Failed to call prepareAd", paramView);
        }
      }
    }
  }
  
  public void a(View paramView, Map<String, WeakReference<View>> paramMap, JSONObject paramJSONObject1, JSONObject paramJSONObject2, JSONObject paramJSONObject3)
  {
    zzx.b("performClick must be called on the main UI thread.");
    synchronized (this.f)
    {
      if (this.d != null)
      {
        this.d.a(paramView, paramMap, paramJSONObject1, paramJSONObject2, paramJSONObject3);
        this.c.e();
      }
      for (;;)
      {
        return;
        try
        {
          if ((this.a != null) && (!this.a.k()))
          {
            this.a.a(zze.a(paramView));
            this.c.e();
          }
          if ((this.b == null) || (this.b.i())) {
            continue;
          }
          this.b.a(zze.a(paramView));
          this.c.e();
        }
        catch (RemoteException paramView)
        {
          zzin.d("Failed to call performClick", paramView);
        }
      }
    }
  }
  
  public void a(zzh paramzzh)
  {
    synchronized (this.f)
    {
      this.d = paramzzh;
      return;
    }
  }
  
  public boolean b()
  {
    synchronized (this.f)
    {
      boolean bool = this.e;
      return bool;
    }
  }
  
  public zzh c()
  {
    synchronized (this.f)
    {
      zzh localzzh = this.d;
      return localzzh;
    }
  }
  
  public zzjp d()
  {
    return null;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/formats/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */