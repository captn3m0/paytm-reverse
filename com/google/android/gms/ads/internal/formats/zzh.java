package com.google.android.gms.ads.internal.formats;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.Map;
import org.json.JSONObject;

public abstract interface zzh
{
  public abstract void a();
  
  public abstract void a(MotionEvent paramMotionEvent);
  
  public abstract void a(View paramView, Map<String, WeakReference<View>> paramMap, JSONObject paramJSONObject1, JSONObject paramJSONObject2, JSONObject paramJSONObject3);
  
  public abstract void a(String paramString, JSONObject paramJSONObject1, JSONObject paramJSONObject2, JSONObject paramJSONObject3);
  
  public abstract void b(View paramView);
  
  public abstract void c(View paramView);
  
  public abstract View e();
  
  public abstract Context f();
  
  public static abstract interface zza
  {
    public abstract void a(zzh paramzzh);
    
    public abstract String j();
    
    public abstract String k();
    
    public abstract zza l();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/formats/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */