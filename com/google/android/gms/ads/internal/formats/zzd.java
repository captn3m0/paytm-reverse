package com.google.android.gms.ads.internal.formats;

import android.os.Bundle;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzch;
import com.google.android.gms.internal.zzcl.zza;
import com.google.android.gms.internal.zzhb;
import java.util.List;

@zzhb
public class zzd
  extends zzcl.zza
  implements zzh.zza
{
  private String a;
  private List<zzc> b;
  private String c;
  private zzch d;
  private String e;
  private double f;
  private String g;
  private String h;
  private zza i;
  private Bundle j;
  private Object k = new Object();
  private zzh l;
  
  public zzd(String paramString1, List paramList, String paramString2, zzch paramzzch, String paramString3, double paramDouble, String paramString4, String paramString5, zza paramzza, Bundle paramBundle)
  {
    this.a = paramString1;
    this.b = paramList;
    this.c = paramString2;
    this.d = paramzzch;
    this.e = paramString3;
    this.f = paramDouble;
    this.g = paramString4;
    this.h = paramString5;
    this.i = paramzza;
    this.j = paramBundle;
  }
  
  public String a()
  {
    return this.a;
  }
  
  public void a(zzh paramzzh)
  {
    synchronized (this.k)
    {
      this.l = paramzzh;
      return;
    }
  }
  
  public List b()
  {
    return this.b;
  }
  
  public String c()
  {
    return this.c;
  }
  
  public zzch d()
  {
    return this.d;
  }
  
  public String e()
  {
    return this.e;
  }
  
  public double f()
  {
    return this.f;
  }
  
  public String g()
  {
    return this.g;
  }
  
  public String h()
  {
    return this.h;
  }
  
  public com.google.android.gms.dynamic.zzd i()
  {
    return zze.a(this.l);
  }
  
  public String j()
  {
    return "2";
  }
  
  public String k()
  {
    return "";
  }
  
  public zza l()
  {
    return this.i;
  }
  
  public Bundle m()
  {
    return this.j;
  }
  
  public void n()
  {
    this.a = null;
    this.b = null;
    this.c = null;
    this.d = null;
    this.e = null;
    this.f = 0.0D;
    this.g = null;
    this.h = null;
    this.i = null;
    this.j = null;
    this.k = null;
    this.l = null;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/formats/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */