package com.google.android.gms.ads.internal.formats;

import android.os.Bundle;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.internal.zzch;
import com.google.android.gms.internal.zzcn.zza;
import com.google.android.gms.internal.zzhb;
import java.util.List;

@zzhb
public class zze
  extends zzcn.zza
  implements zzh.zza
{
  private String a;
  private List<zzc> b;
  private String c;
  private zzch d;
  private String e;
  private String f;
  private zza g;
  private Bundle h;
  private Object i = new Object();
  private zzh j;
  
  public zze(String paramString1, List paramList, String paramString2, zzch paramzzch, String paramString3, String paramString4, zza paramzza, Bundle paramBundle)
  {
    this.a = paramString1;
    this.b = paramList;
    this.c = paramString2;
    this.d = paramzzch;
    this.e = paramString3;
    this.f = paramString4;
    this.g = paramzza;
    this.h = paramBundle;
  }
  
  public String a()
  {
    return this.a;
  }
  
  public void a(zzh paramzzh)
  {
    synchronized (this.i)
    {
      this.j = paramzzh;
      return;
    }
  }
  
  public List b()
  {
    return this.b;
  }
  
  public String c()
  {
    return this.c;
  }
  
  public zzch d()
  {
    return this.d;
  }
  
  public String e()
  {
    return this.e;
  }
  
  public String f()
  {
    return this.f;
  }
  
  public zzd g()
  {
    return com.google.android.gms.dynamic.zze.a(this.j);
  }
  
  public Bundle h()
  {
    return this.h;
  }
  
  public void i()
  {
    this.a = null;
    this.b = null;
    this.c = null;
    this.d = null;
    this.e = null;
    this.f = null;
    this.g = null;
    this.h = null;
    this.i = null;
    this.j = null;
  }
  
  public String j()
  {
    return "1";
  }
  
  public String k()
  {
    return "";
  }
  
  public zza l()
  {
    return this.g;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/formats/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */