package com.google.android.gms.ads.internal.formats;

import android.support.v4.util.SimpleArrayMap;
import com.google.android.gms.internal.zzch;
import com.google.android.gms.internal.zzcp.zza;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzin;
import java.util.Arrays;
import java.util.List;

@zzhb
public class zzf
  extends zzcp.zza
  implements zzh.zza
{
  private final zza a;
  private final String b;
  private final SimpleArrayMap<String, zzc> c;
  private final SimpleArrayMap<String, String> d;
  private final Object e = new Object();
  private zzh f;
  
  public zzf(String paramString, SimpleArrayMap<String, zzc> paramSimpleArrayMap, SimpleArrayMap<String, String> paramSimpleArrayMap1, zza paramzza)
  {
    this.b = paramString;
    this.c = paramSimpleArrayMap;
    this.d = paramSimpleArrayMap1;
    this.a = paramzza;
  }
  
  public String a(String paramString)
  {
    return (String)this.d.get(paramString);
  }
  
  public List<String> a()
  {
    int n = 0;
    String[] arrayOfString = new String[this.c.size() + this.d.size()];
    int j = 0;
    int i = 0;
    int k;
    int m;
    for (;;)
    {
      k = n;
      m = i;
      if (j >= this.c.size()) {
        break;
      }
      arrayOfString[i] = ((String)this.c.b(j));
      i += 1;
      j += 1;
    }
    while (k < this.d.size())
    {
      arrayOfString[m] = ((String)this.d.b(k));
      k += 1;
      m += 1;
    }
    return Arrays.asList(arrayOfString);
  }
  
  public void a(zzh paramzzh)
  {
    synchronized (this.e)
    {
      this.f = paramzzh;
      return;
    }
  }
  
  public zzch b(String paramString)
  {
    return (zzch)this.c.get(paramString);
  }
  
  public void b()
  {
    synchronized (this.e)
    {
      if (this.f == null)
      {
        zzin.b("Attempt to perform recordImpression before ad initialized.");
        return;
      }
      this.f.a();
      return;
    }
  }
  
  public void c(String paramString)
  {
    synchronized (this.e)
    {
      if (this.f == null)
      {
        zzin.b("Attempt to call performClick before ad initialized.");
        return;
      }
      this.f.a(paramString, null, null, null);
      return;
    }
  }
  
  public String j()
  {
    return "3";
  }
  
  public String k()
  {
    return this.b;
  }
  
  public zza l()
  {
    return this.a;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/formats/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */