package com.google.android.gms.ads.internal.formats;

import android.content.Context;
import android.graphics.Rect;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzp;
import com.google.android.gms.ads.internal.zzr;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzan;
import com.google.android.gms.internal.zzdf;
import com.google.android.gms.internal.zzed;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzjp;
import com.google.android.gms.internal.zzjq;
import com.google.android.gms.internal.zzjq.zza;
import com.google.android.gms.internal.zzjr;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

@zzhb
public class zzi
  implements zzh
{
  private final Object a = new Object();
  private final zzp b;
  private final Context c;
  private final JSONObject d;
  private final zzed e;
  private final zzh.zza f;
  private final zzan g;
  private final VersionInfoParcel h;
  private boolean i;
  private zzjp j;
  private String k;
  private WeakReference<View> l = null;
  
  public zzi(Context paramContext, zzp paramzzp, zzed paramzzed, zzan paramzzan, JSONObject paramJSONObject, zzh.zza paramzza, VersionInfoParcel paramVersionInfoParcel)
  {
    this.c = paramContext;
    this.b = paramzzp;
    this.e = paramzzed;
    this.g = paramzzan;
    this.d = paramJSONObject;
    this.f = paramzza;
    this.h = paramVersionInfoParcel;
  }
  
  public zzb a(View.OnClickListener paramOnClickListener)
  {
    Object localObject = this.f.l();
    if (localObject == null) {
      return null;
    }
    localObject = new zzb(this.c, (zza)localObject);
    ((zzb)localObject).setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
    ((zzb)localObject).a().setOnClickListener(paramOnClickListener);
    ((zzb)localObject).a().setContentDescription("Ad attribution icon");
    return (zzb)localObject;
  }
  
  public void a()
  {
    zzx.b("recordImpression must be called on the main UI thread.");
    a(true);
    try
    {
      JSONObject localJSONObject = new JSONObject();
      localJSONObject.put("ad", this.d);
      this.e.a("google.afma.nativeAds.handleImpressionPing", localJSONObject);
      this.b.a(this);
      return;
    }
    catch (JSONException localJSONException)
    {
      for (;;)
      {
        zzin.b("Unable to create impression JSON.", localJSONException);
      }
    }
  }
  
  public void a(MotionEvent paramMotionEvent)
  {
    this.g.a(paramMotionEvent);
  }
  
  public void a(View paramView) {}
  
  public void a(View paramView, Map<String, WeakReference<View>> paramMap, JSONObject paramJSONObject1, JSONObject paramJSONObject2, JSONObject paramJSONObject3)
  {
    zzx.b("performClick must be called on the main UI thread.");
    paramMap = paramMap.entrySet().iterator();
    while (paramMap.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)paramMap.next();
      if (paramView.equals((View)((WeakReference)localEntry.getValue()).get())) {
        a((String)localEntry.getKey(), paramJSONObject1, paramJSONObject2, paramJSONObject3);
      }
    }
  }
  
  public void a(String paramString, JSONObject paramJSONObject1, JSONObject paramJSONObject2, JSONObject paramJSONObject3)
  {
    zzx.b("performClick must be called on the main UI thread.");
    try
    {
      JSONObject localJSONObject = new JSONObject();
      localJSONObject.put("asset", paramString);
      localJSONObject.put("template", this.f.j());
      paramString = new JSONObject();
      paramString.put("ad", this.d);
      paramString.put("click", localJSONObject);
      if (this.b.c(this.f.k()) != null) {}
      for (boolean bool = true;; bool = false)
      {
        paramString.put("has_custom_click_handler", bool);
        if (paramJSONObject1 != null) {
          paramString.put("view_rectangles", paramJSONObject1);
        }
        if (paramJSONObject2 != null) {
          paramString.put("click_point", paramJSONObject2);
        }
        if (paramJSONObject3 != null) {
          paramString.put("native_view_rectangle", paramJSONObject3);
        }
        this.e.a("google.afma.nativeAds.handleClickGmsg", paramString);
        return;
      }
      return;
    }
    catch (JSONException paramString)
    {
      zzin.b("Unable to create click JSON.", paramString);
    }
  }
  
  protected void a(boolean paramBoolean)
  {
    this.i = paramBoolean;
  }
  
  public void b(View paramView)
  {
    synchronized (this.a)
    {
      if (this.i) {
        return;
      }
      if (!paramView.isShown()) {
        return;
      }
    }
    if (!paramView.getGlobalVisibleRect(new Rect(), null)) {
      return;
    }
    a();
  }
  
  public void c(View paramView)
  {
    this.l = new WeakReference(paramView);
  }
  
  public zzjp d()
  {
    this.j = g();
    this.j.b().setVisibility(8);
    this.e.a("/loadHtml", new zzdf()
    {
      public void a(zzjp paramAnonymouszzjp, final Map<String, String> paramAnonymousMap)
      {
        zzi.c(zzi.this).l().a(new zzjq.zza()
        {
          public void a(zzjp paramAnonymous2zzjp, boolean paramAnonymous2Boolean)
          {
            zzi.a(zzi.this, (String)paramAnonymousMap.get("id"));
            paramAnonymous2zzjp = new JSONObject();
            try
            {
              paramAnonymous2zzjp.put("messageType", "htmlLoaded");
              paramAnonymous2zzjp.put("id", zzi.a(zzi.this));
              zzi.b(zzi.this).b("sendMessageToNativeJs", paramAnonymous2zzjp);
              return;
            }
            catch (JSONException paramAnonymous2zzjp)
            {
              zzin.b("Unable to dispatch sendMessageToNativeJsevent", paramAnonymous2zzjp);
            }
          }
        });
        paramAnonymouszzjp = (String)paramAnonymousMap.get("overlayHtml");
        paramAnonymousMap = (String)paramAnonymousMap.get("baseUrl");
        if (TextUtils.isEmpty(paramAnonymousMap))
        {
          zzi.c(zzi.this).loadData(paramAnonymouszzjp, "text/html", "UTF-8");
          return;
        }
        zzi.c(zzi.this).loadDataWithBaseURL(paramAnonymousMap, paramAnonymouszzjp, "text/html", "UTF-8", null);
      }
    });
    this.e.a("/showOverlay", new zzdf()
    {
      public void a(zzjp paramAnonymouszzjp, Map<String, String> paramAnonymousMap)
      {
        zzi.c(zzi.this).b().setVisibility(0);
      }
    });
    this.e.a("/hideOverlay", new zzdf()
    {
      public void a(zzjp paramAnonymouszzjp, Map<String, String> paramAnonymousMap)
      {
        zzi.c(zzi.this).b().setVisibility(8);
      }
    });
    this.j.l().a("/hideOverlay", new zzdf()
    {
      public void a(zzjp paramAnonymouszzjp, Map<String, String> paramAnonymousMap)
      {
        zzi.c(zzi.this).b().setVisibility(8);
      }
    });
    this.j.l().a("/sendMessageToSdk", new zzdf()
    {
      public void a(zzjp paramAnonymouszzjp, Map<String, String> paramAnonymousMap)
      {
        paramAnonymouszzjp = new JSONObject();
        try
        {
          Iterator localIterator = paramAnonymousMap.keySet().iterator();
          while (localIterator.hasNext())
          {
            String str = (String)localIterator.next();
            paramAnonymouszzjp.put(str, paramAnonymousMap.get(str));
          }
          paramAnonymouszzjp.put("id", zzi.a(zzi.this));
        }
        catch (JSONException paramAnonymouszzjp)
        {
          zzin.b("Unable to dispatch sendMessageToNativeJs event", paramAnonymouszzjp);
          return;
        }
        zzi.b(zzi.this).b("sendMessageToNativeJs", paramAnonymouszzjp);
      }
    });
    return this.j;
  }
  
  public View e()
  {
    if (this.l != null) {
      return (View)this.l.get();
    }
    return null;
  }
  
  public Context f()
  {
    return this.c;
  }
  
  zzjp g()
  {
    return zzr.f().a(this.c, AdSizeParcel.a(this.c), false, false, this.g, this.h);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/formats/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */