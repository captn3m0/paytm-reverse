package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.util.SimpleArrayMap;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.widget.ViewSwitcher;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzn;
import com.google.android.gms.ads.internal.client.zzp;
import com.google.android.gms.ads.internal.client.zzq;
import com.google.android.gms.ads.internal.client.zzw;
import com.google.android.gms.ads.internal.client.zzx;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.purchase.zzk;
import com.google.android.gms.ads.internal.reward.client.zzd;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.internal.zzan;
import com.google.android.gms.internal.zzbt;
import com.google.android.gms.internal.zzbv;
import com.google.android.gms.internal.zzcf;
import com.google.android.gms.internal.zzcr;
import com.google.android.gms.internal.zzcs;
import com.google.android.gms.internal.zzct;
import com.google.android.gms.internal.zzcu;
import com.google.android.gms.internal.zzey;
import com.google.android.gms.internal.zzgd;
import com.google.android.gms.internal.zzgh;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzif;
import com.google.android.gms.internal.zzif.zza;
import com.google.android.gms.internal.zzig;
import com.google.android.gms.internal.zzih;
import com.google.android.gms.internal.zzik;
import com.google.android.gms.internal.zzim;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzit;
import com.google.android.gms.internal.zziu;
import com.google.android.gms.internal.zziz;
import com.google.android.gms.internal.zzjc;
import com.google.android.gms.internal.zzjp;
import com.google.android.gms.internal.zzjq;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

@zzhb
public final class zzs
  implements ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener
{
  zzk A;
  public zzik B = null;
  View C = null;
  public int D = 0;
  boolean E = false;
  boolean F = false;
  @Nullable
  private String G;
  private HashSet<zzig> H = null;
  private int I = -1;
  private int J = -1;
  private zziz K;
  private boolean L = true;
  private boolean M = true;
  private boolean N = false;
  final String a;
  public String b;
  public final Context c;
  final zzan d;
  public final VersionInfoParcel e;
  zza f;
  public zzim g;
  public zzit h;
  public AdSizeParcel i;
  public zzif j;
  public zzif.zza k;
  public zzig l;
  zzp m;
  zzq n;
  zzw o;
  zzx p;
  zzgd q;
  zzgh r;
  zzcr s;
  zzcs t;
  SimpleArrayMap<String, zzct> u;
  SimpleArrayMap<String, zzcu> v;
  NativeAdOptionsParcel w;
  zzcf x;
  @Nullable
  zzd y;
  List<String> z;
  
  public zzs(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, VersionInfoParcel paramVersionInfoParcel)
  {
    this(paramContext, paramAdSizeParcel, paramString, paramVersionInfoParcel, null);
  }
  
  zzs(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, VersionInfoParcel paramVersionInfoParcel, zzan paramzzan)
  {
    zzbt.a(paramContext);
    if (zzr.h().e() != null)
    {
      List localList = zzbt.b();
      if (paramVersionInfoParcel.c != 0) {
        localList.add(Integer.toString(paramVersionInfoParcel.c));
      }
      zzr.h().e().a(localList);
    }
    this.a = UUID.randomUUID().toString();
    if ((paramAdSizeParcel.e) || (paramAdSizeParcel.i))
    {
      this.f = null;
      this.i = paramAdSizeParcel;
      this.b = paramString;
      this.c = paramContext;
      this.e = paramVersionInfoParcel;
      if (paramzzan == null) {
        break label246;
      }
    }
    for (;;)
    {
      this.d = paramzzan;
      this.K = new zziz(200L);
      this.v = new SimpleArrayMap();
      return;
      this.f = new zza(paramContext, this, this);
      this.f.setMinimumWidth(paramAdSizeParcel.g);
      this.f.setMinimumHeight(paramAdSizeParcel.d);
      this.f.setVisibility(4);
      break;
      label246:
      paramzzan = new zzan(new zzh(this));
    }
  }
  
  private void b(boolean paramBoolean)
  {
    boolean bool = true;
    if ((this.f == null) || (this.j == null) || (this.j.b == null)) {}
    while ((paramBoolean) && (!this.K.a())) {
      return;
    }
    Object localObject;
    int i1;
    int i2;
    if (this.j.b.l().b())
    {
      localObject = new int[2];
      this.f.getLocationOnScreen((int[])localObject);
      i1 = zzn.a().b(this.c, localObject[0]);
      i2 = zzn.a().b(this.c, localObject[1]);
      if ((i1 != this.I) || (i2 != this.J))
      {
        this.I = i1;
        this.J = i2;
        localObject = this.j.b.l();
        i1 = this.I;
        i2 = this.J;
        if (paramBoolean) {
          break label174;
        }
      }
    }
    label174:
    for (paramBoolean = bool;; paramBoolean = false)
    {
      ((zzjq)localObject).a(i1, i2, paramBoolean);
      l();
      return;
    }
  }
  
  private void l()
  {
    View localView = this.f.getRootView().findViewById(16908290);
    if (localView == null) {}
    Rect localRect1;
    Rect localRect2;
    do
    {
      return;
      localRect1 = new Rect();
      localRect2 = new Rect();
      this.f.getGlobalVisibleRect(localRect1);
      localView.getGlobalVisibleRect(localRect2);
      if (localRect1.top != localRect2.top) {
        this.L = false;
      }
    } while (localRect1.bottom == localRect2.bottom);
    this.M = false;
  }
  
  public HashSet<zzig> a()
  {
    return this.H;
  }
  
  void a(String paramString)
  {
    this.G = paramString;
  }
  
  public void a(HashSet<zzig> paramHashSet)
  {
    this.H = paramHashSet;
  }
  
  public void a(boolean paramBoolean)
  {
    if (this.D == 0) {
      c();
    }
    if (this.g != null) {
      this.g.cancel();
    }
    if (this.h != null) {
      this.h.cancel();
    }
    if (paramBoolean) {
      this.j = null;
    }
  }
  
  public void b()
  {
    if ((this.j != null) && (this.j.b != null)) {
      this.j.b.destroy();
    }
  }
  
  public void c()
  {
    if ((this.j != null) && (this.j.b != null)) {
      this.j.b.stopLoading();
    }
  }
  
  public void d()
  {
    if ((this.j != null) && (this.j.o != null)) {}
    try
    {
      this.j.o.c();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzin.d("Could not destroy mediation adapter.");
    }
  }
  
  public boolean e()
  {
    return this.D == 0;
  }
  
  public boolean f()
  {
    return this.D == 1;
  }
  
  public void g()
  {
    if (this.f != null) {
      this.f.b();
    }
  }
  
  public String h()
  {
    return this.G;
  }
  
  public String i()
  {
    if ((this.L) && (this.M)) {
      return "";
    }
    if (this.L)
    {
      if (this.N) {
        return "top-scrollable";
      }
      return "top-locked";
    }
    if (this.M)
    {
      if (this.N) {
        return "bottom-scrollable";
      }
      return "bottom-locked";
    }
    return "";
  }
  
  public void j()
  {
    this.l.a(this.j.y);
    this.l.b(this.j.z);
    this.l.a(this.i.e);
    this.l.b(this.j.m);
  }
  
  public void k()
  {
    g();
    this.n = null;
    this.o = null;
    this.r = null;
    this.q = null;
    this.x = null;
    this.p = null;
    a(false);
    if (this.f != null) {
      this.f.removeAllViews();
    }
    b();
    d();
    this.j = null;
  }
  
  public void onGlobalLayout()
  {
    b(false);
  }
  
  public void onScrollChanged()
  {
    b(true);
    this.N = true;
  }
  
  public static class zza
    extends ViewSwitcher
  {
    private final zziu a;
    private final zzjc b;
    
    public zza(Context paramContext, ViewTreeObserver.OnGlobalLayoutListener paramOnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener paramOnScrollChangedListener)
    {
      super();
      this.a = new zziu(paramContext);
      if ((paramContext instanceof Activity))
      {
        this.b = new zzjc((Activity)paramContext, paramOnGlobalLayoutListener, paramOnScrollChangedListener);
        this.b.a();
        return;
      }
      this.b = null;
    }
    
    public zziu a()
    {
      return this.a;
    }
    
    public void b()
    {
      zzin.e("Disable position monitoring on adFrame.");
      if (this.b != null) {
        this.b.b();
      }
    }
    
    protected void onAttachedToWindow()
    {
      super.onAttachedToWindow();
      if (this.b != null) {
        this.b.c();
      }
    }
    
    protected void onDetachedFromWindow()
    {
      super.onDetachedFromWindow();
      if (this.b != null) {
        this.b.d();
      }
    }
    
    public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
    {
      this.a.a(paramMotionEvent);
      return false;
    }
    
    public void removeAllViews()
    {
      Object localObject = new ArrayList();
      int i = 0;
      while (i < getChildCount())
      {
        View localView = getChildAt(i);
        if ((localView != null) && ((localView instanceof zzjp))) {
          ((List)localObject).add((zzjp)localView);
        }
        i += 1;
      }
      super.removeAllViews();
      localObject = ((List)localObject).iterator();
      while (((Iterator)localObject).hasNext()) {
        ((zzjp)((Iterator)localObject).next()).destroy();
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/zzs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */