package com.google.android.gms.ads.internal.reward.mediation.client;

import android.os.RemoteException;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdAdapter;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdListener;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzin;

@zzhb
public class zzb
  implements MediationRewardedVideoAdListener
{
  private final zza a;
  
  public zzb(zza paramzza)
  {
    this.a = paramzza;
  }
  
  public void a(MediationRewardedVideoAdAdapter paramMediationRewardedVideoAdAdapter)
  {
    zzx.b("onInitializationSucceeded must be called on the main UI thread.");
    zzin.a("Adapter called onInitializationSucceeded.");
    try
    {
      this.a.a(zze.a(paramMediationRewardedVideoAdAdapter));
      return;
    }
    catch (RemoteException paramMediationRewardedVideoAdAdapter)
    {
      zzin.d("Could not call onInitializationSucceeded.", paramMediationRewardedVideoAdAdapter);
    }
  }
  
  public void a(MediationRewardedVideoAdAdapter paramMediationRewardedVideoAdAdapter, int paramInt)
  {
    zzx.b("onAdFailedToLoad must be called on the main UI thread.");
    zzin.a("Adapter called onAdFailedToLoad.");
    try
    {
      this.a.b(zze.a(paramMediationRewardedVideoAdAdapter), paramInt);
      return;
    }
    catch (RemoteException paramMediationRewardedVideoAdAdapter)
    {
      zzin.d("Could not call onAdFailedToLoad.", paramMediationRewardedVideoAdAdapter);
    }
  }
  
  public void a(MediationRewardedVideoAdAdapter paramMediationRewardedVideoAdAdapter, RewardItem paramRewardItem)
  {
    zzx.b("onRewarded must be called on the main UI thread.");
    zzin.a("Adapter called onRewarded.");
    if (paramRewardItem != null) {}
    try
    {
      this.a.a(zze.a(paramMediationRewardedVideoAdAdapter), new RewardItemParcel(paramRewardItem));
      return;
    }
    catch (RemoteException paramMediationRewardedVideoAdAdapter)
    {
      zzin.d("Could not call onRewarded.", paramMediationRewardedVideoAdAdapter);
    }
    this.a.a(zze.a(paramMediationRewardedVideoAdAdapter), new RewardItemParcel(paramMediationRewardedVideoAdAdapter.getClass().getName(), 1));
    return;
  }
  
  public void b(MediationRewardedVideoAdAdapter paramMediationRewardedVideoAdAdapter)
  {
    zzx.b("onAdLoaded must be called on the main UI thread.");
    zzin.a("Adapter called onAdLoaded.");
    try
    {
      this.a.b(zze.a(paramMediationRewardedVideoAdAdapter));
      return;
    }
    catch (RemoteException paramMediationRewardedVideoAdAdapter)
    {
      zzin.d("Could not call onAdLoaded.", paramMediationRewardedVideoAdAdapter);
    }
  }
  
  public void c(MediationRewardedVideoAdAdapter paramMediationRewardedVideoAdAdapter)
  {
    zzx.b("onAdOpened must be called on the main UI thread.");
    zzin.a("Adapter called onAdOpened.");
    try
    {
      this.a.c(zze.a(paramMediationRewardedVideoAdAdapter));
      return;
    }
    catch (RemoteException paramMediationRewardedVideoAdAdapter)
    {
      zzin.d("Could not call onAdOpened.", paramMediationRewardedVideoAdAdapter);
    }
  }
  
  public void d(MediationRewardedVideoAdAdapter paramMediationRewardedVideoAdAdapter)
  {
    zzx.b("onVideoStarted must be called on the main UI thread.");
    zzin.a("Adapter called onVideoStarted.");
    try
    {
      this.a.d(zze.a(paramMediationRewardedVideoAdAdapter));
      return;
    }
    catch (RemoteException paramMediationRewardedVideoAdAdapter)
    {
      zzin.d("Could not call onVideoStarted.", paramMediationRewardedVideoAdAdapter);
    }
  }
  
  public void e(MediationRewardedVideoAdAdapter paramMediationRewardedVideoAdAdapter)
  {
    zzx.b("onAdClosed must be called on the main UI thread.");
    zzin.a("Adapter called onAdClosed.");
    try
    {
      this.a.e(zze.a(paramMediationRewardedVideoAdAdapter));
      return;
    }
    catch (RemoteException paramMediationRewardedVideoAdAdapter)
    {
      zzin.d("Could not call onAdClosed.", paramMediationRewardedVideoAdAdapter);
    }
  }
  
  public void f(MediationRewardedVideoAdAdapter paramMediationRewardedVideoAdAdapter)
  {
    zzx.b("onAdLeftApplication must be called on the main UI thread.");
    zzin.a("Adapter called onAdLeftApplication.");
    try
    {
      this.a.g(zze.a(paramMediationRewardedVideoAdAdapter));
      return;
    }
    catch (RemoteException paramMediationRewardedVideoAdAdapter)
    {
      zzin.d("Could not call onAdLeftApplication.", paramMediationRewardedVideoAdAdapter);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/reward/mediation/client/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */