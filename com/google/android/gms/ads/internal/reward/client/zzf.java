package com.google.android.gms.ads.internal.reward.client;

import android.os.IBinder;
import com.google.android.gms.dynamic.zzg;
import com.google.android.gms.internal.zzhb;

@zzhb
public class zzf
  extends zzg<zzc>
{
  public zzf()
  {
    super("com.google.android.gms.ads.reward.RewardedVideoAdCreatorImpl");
  }
  
  protected zzc a(IBinder paramIBinder)
  {
    return zzc.zza.a(paramIBinder);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/reward/client/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */