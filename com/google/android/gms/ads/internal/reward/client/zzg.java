package com.google.android.gms.ads.internal.reward.client;

import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.internal.zzhb;

@zzhb
public class zzg
  extends zzd.zza
{
  private final RewardedVideoAdListener a;
  
  public zzg(RewardedVideoAdListener paramRewardedVideoAdListener)
  {
    this.a = paramRewardedVideoAdListener;
  }
  
  public void a()
  {
    if (this.a != null) {
      this.a.a();
    }
  }
  
  public void a(int paramInt)
  {
    if (this.a != null) {
      this.a.a(paramInt);
    }
  }
  
  public void a(zza paramzza)
  {
    if (this.a != null) {
      this.a.a(new zze(paramzza));
    }
  }
  
  public void b()
  {
    if (this.a != null) {
      this.a.b();
    }
  }
  
  public void c()
  {
    if (this.a != null) {
      this.a.c();
    }
  }
  
  public void d()
  {
    if (this.a != null) {
      this.a.d();
    }
  }
  
  public void e()
  {
    if (this.a != null) {
      this.a.e();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/reward/client/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */