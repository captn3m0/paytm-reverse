package com.google.android.gms.ads.internal;

import com.google.android.gms.ads.internal.overlay.zzj;
import com.google.android.gms.ads.internal.overlay.zzm;
import com.google.android.gms.ads.internal.overlay.zzn;
import com.google.android.gms.ads.internal.overlay.zzr;
import com.google.android.gms.internal.zzdc;
import com.google.android.gms.internal.zzdt;
import com.google.android.gms.internal.zzhb;

@zzhb
public class zzd
{
  public final zzdt a;
  public final zzj b;
  public final zzm c;
  
  public zzd(zzdt paramzzdt, zzj paramzzj, zzm paramzzm)
  {
    this.a = paramzzdt;
    this.b = paramzzj;
    this.c = paramzzm;
  }
  
  public static zzd a()
  {
    return new zzd(new zzdc(), new zzn(), new zzr());
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */