package com.google.android.gms.ads.internal.request;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzh
  implements Parcelable.Creator<AdResponseParcel>
{
  static void a(AdResponseParcel paramAdResponseParcel, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramAdResponseParcel.a);
    zzb.a(paramParcel, 2, paramAdResponseParcel.b, false);
    zzb.a(paramParcel, 3, paramAdResponseParcel.c, false);
    zzb.b(paramParcel, 4, paramAdResponseParcel.d, false);
    zzb.a(paramParcel, 5, paramAdResponseParcel.e);
    zzb.b(paramParcel, 6, paramAdResponseParcel.f, false);
    zzb.a(paramParcel, 7, paramAdResponseParcel.g);
    zzb.a(paramParcel, 8, paramAdResponseParcel.h);
    zzb.a(paramParcel, 9, paramAdResponseParcel.i);
    zzb.b(paramParcel, 10, paramAdResponseParcel.j, false);
    zzb.a(paramParcel, 11, paramAdResponseParcel.k);
    zzb.a(paramParcel, 12, paramAdResponseParcel.l);
    zzb.a(paramParcel, 13, paramAdResponseParcel.m, false);
    zzb.a(paramParcel, 14, paramAdResponseParcel.n);
    zzb.a(paramParcel, 15, paramAdResponseParcel.o, false);
    zzb.a(paramParcel, 19, paramAdResponseParcel.q, false);
    zzb.a(paramParcel, 18, paramAdResponseParcel.p);
    zzb.a(paramParcel, 21, paramAdResponseParcel.r, false);
    zzb.a(paramParcel, 23, paramAdResponseParcel.t);
    zzb.a(paramParcel, 22, paramAdResponseParcel.s);
    zzb.a(paramParcel, 25, paramAdResponseParcel.v);
    zzb.a(paramParcel, 24, paramAdResponseParcel.u);
    zzb.a(paramParcel, 27, paramAdResponseParcel.x);
    zzb.a(paramParcel, 26, paramAdResponseParcel.w);
    zzb.a(paramParcel, 29, paramAdResponseParcel.z, false);
    zzb.a(paramParcel, 28, paramAdResponseParcel.y, paramInt, false);
    zzb.a(paramParcel, 31, paramAdResponseParcel.B);
    zzb.a(paramParcel, 30, paramAdResponseParcel.A, false);
    zzb.b(paramParcel, 34, paramAdResponseParcel.E, false);
    zzb.b(paramParcel, 35, paramAdResponseParcel.F, false);
    zzb.a(paramParcel, 32, paramAdResponseParcel.C);
    zzb.a(paramParcel, 33, paramAdResponseParcel.D, paramInt, false);
    zzb.a(paramParcel, 36, paramAdResponseParcel.G);
    zzb.a(paramParcel, i);
  }
  
  public AdResponseParcel a(Parcel paramParcel)
  {
    int n = zza.b(paramParcel);
    int m = 0;
    String str8 = null;
    String str7 = null;
    ArrayList localArrayList5 = null;
    int k = 0;
    ArrayList localArrayList4 = null;
    long l4 = 0L;
    boolean bool10 = false;
    long l3 = 0L;
    ArrayList localArrayList3 = null;
    long l2 = 0L;
    int j = 0;
    String str6 = null;
    long l1 = 0L;
    String str5 = null;
    boolean bool9 = false;
    String str4 = null;
    String str3 = null;
    boolean bool8 = false;
    boolean bool7 = false;
    boolean bool6 = false;
    boolean bool5 = false;
    boolean bool4 = false;
    int i = 0;
    LargeParcelTeleporter localLargeParcelTeleporter = null;
    String str2 = null;
    String str1 = null;
    boolean bool3 = false;
    boolean bool2 = false;
    RewardItemParcel localRewardItemParcel = null;
    ArrayList localArrayList2 = null;
    ArrayList localArrayList1 = null;
    boolean bool1 = false;
    while (paramParcel.dataPosition() < n)
    {
      int i1 = zza.a(paramParcel);
      switch (zza.a(i1))
      {
      case 16: 
      case 17: 
      case 20: 
      default: 
        zza.b(paramParcel, i1);
        break;
      case 1: 
        m = zza.g(paramParcel, i1);
        break;
      case 2: 
        str8 = zza.p(paramParcel, i1);
        break;
      case 3: 
        str7 = zza.p(paramParcel, i1);
        break;
      case 4: 
        localArrayList5 = zza.D(paramParcel, i1);
        break;
      case 5: 
        k = zza.g(paramParcel, i1);
        break;
      case 6: 
        localArrayList4 = zza.D(paramParcel, i1);
        break;
      case 7: 
        l4 = zza.i(paramParcel, i1);
        break;
      case 8: 
        bool10 = zza.c(paramParcel, i1);
        break;
      case 9: 
        l3 = zza.i(paramParcel, i1);
        break;
      case 10: 
        localArrayList3 = zza.D(paramParcel, i1);
        break;
      case 11: 
        l2 = zza.i(paramParcel, i1);
        break;
      case 12: 
        j = zza.g(paramParcel, i1);
        break;
      case 13: 
        str6 = zza.p(paramParcel, i1);
        break;
      case 14: 
        l1 = zza.i(paramParcel, i1);
        break;
      case 15: 
        str5 = zza.p(paramParcel, i1);
        break;
      case 19: 
        str4 = zza.p(paramParcel, i1);
        break;
      case 18: 
        bool9 = zza.c(paramParcel, i1);
        break;
      case 21: 
        str3 = zza.p(paramParcel, i1);
        break;
      case 23: 
        bool7 = zza.c(paramParcel, i1);
        break;
      case 22: 
        bool8 = zza.c(paramParcel, i1);
        break;
      case 25: 
        bool5 = zza.c(paramParcel, i1);
        break;
      case 24: 
        bool6 = zza.c(paramParcel, i1);
        break;
      case 27: 
        i = zza.g(paramParcel, i1);
        break;
      case 26: 
        bool4 = zza.c(paramParcel, i1);
        break;
      case 29: 
        str2 = zza.p(paramParcel, i1);
        break;
      case 28: 
        localLargeParcelTeleporter = (LargeParcelTeleporter)zza.a(paramParcel, i1, LargeParcelTeleporter.CREATOR);
        break;
      case 31: 
        bool3 = zza.c(paramParcel, i1);
        break;
      case 30: 
        str1 = zza.p(paramParcel, i1);
        break;
      case 34: 
        localArrayList2 = zza.D(paramParcel, i1);
        break;
      case 35: 
        localArrayList1 = zza.D(paramParcel, i1);
        break;
      case 32: 
        bool2 = zza.c(paramParcel, i1);
        break;
      case 33: 
        localRewardItemParcel = (RewardItemParcel)zza.a(paramParcel, i1, RewardItemParcel.CREATOR);
        break;
      case 36: 
        bool1 = zza.c(paramParcel, i1);
      }
    }
    if (paramParcel.dataPosition() != n) {
      throw new zza.zza("Overread allowed size end=" + n, paramParcel);
    }
    return new AdResponseParcel(m, str8, str7, localArrayList5, k, localArrayList4, l4, bool10, l3, localArrayList3, l2, j, str6, l1, str5, bool9, str4, str3, bool8, bool7, bool6, bool5, bool4, i, localLargeParcelTeleporter, str2, str1, bool3, bool2, localRewardItemParcel, localArrayList2, localArrayList1, bool1);
  }
  
  public AdResponseParcel[] a(int paramInt)
  {
    return new AdResponseParcel[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/request/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */