package com.google.android.gms.ads.internal.request;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Messenger;
import android.os.Parcel;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.zzhb;
import java.util.Collections;
import java.util.List;

@zzhb
public final class AdRequestInfoParcel
  implements SafeParcelable
{
  public static final zzf CREATOR = new zzf();
  public final List<String> A;
  public final long B;
  public final CapabilityParcel C;
  public final String D;
  public final float E;
  public final int F;
  public final int G;
  public final int a;
  public final Bundle b;
  public final AdRequestParcel c;
  public final AdSizeParcel d;
  public final String e;
  public final ApplicationInfo f;
  public final PackageInfo g;
  public final String h;
  public final String i;
  public final String j;
  public final VersionInfoParcel k;
  public final Bundle l;
  public final int m;
  public final List<String> n;
  public final Bundle o;
  public final boolean p;
  public final Messenger q;
  public final int r;
  public final int s;
  public final float t;
  public final String u;
  public final long v;
  public final String w;
  public final List<String> x;
  public final String y;
  public final NativeAdOptionsParcel z;
  
  AdRequestInfoParcel(int paramInt1, Bundle paramBundle1, AdRequestParcel paramAdRequestParcel, AdSizeParcel paramAdSizeParcel, String paramString1, ApplicationInfo paramApplicationInfo, PackageInfo paramPackageInfo, String paramString2, String paramString3, String paramString4, VersionInfoParcel paramVersionInfoParcel, Bundle paramBundle2, int paramInt2, List<String> paramList1, Bundle paramBundle3, boolean paramBoolean, Messenger paramMessenger, int paramInt3, int paramInt4, float paramFloat1, String paramString5, long paramLong1, String paramString6, List<String> paramList2, String paramString7, NativeAdOptionsParcel paramNativeAdOptionsParcel, List<String> paramList3, long paramLong2, CapabilityParcel paramCapabilityParcel, String paramString8, float paramFloat2, int paramInt5, int paramInt6)
  {
    this.a = paramInt1;
    this.b = paramBundle1;
    this.c = paramAdRequestParcel;
    this.d = paramAdSizeParcel;
    this.e = paramString1;
    this.f = paramApplicationInfo;
    this.g = paramPackageInfo;
    this.h = paramString2;
    this.i = paramString3;
    this.j = paramString4;
    this.k = paramVersionInfoParcel;
    this.l = paramBundle2;
    this.m = paramInt2;
    this.n = paramList1;
    if (paramList3 == null)
    {
      paramBundle1 = Collections.emptyList();
      this.A = paramBundle1;
      this.o = paramBundle3;
      this.p = paramBoolean;
      this.q = paramMessenger;
      this.r = paramInt3;
      this.s = paramInt4;
      this.t = paramFloat1;
      this.u = paramString5;
      this.v = paramLong1;
      this.w = paramString6;
      if (paramList2 != null) {
        break label225;
      }
    }
    label225:
    for (paramBundle1 = Collections.emptyList();; paramBundle1 = Collections.unmodifiableList(paramList2))
    {
      this.x = paramBundle1;
      this.y = paramString7;
      this.z = paramNativeAdOptionsParcel;
      this.B = paramLong2;
      this.C = paramCapabilityParcel;
      this.D = paramString8;
      this.E = paramFloat2;
      this.F = paramInt5;
      this.G = paramInt6;
      return;
      paramBundle1 = Collections.unmodifiableList(paramList3);
      break;
    }
  }
  
  public AdRequestInfoParcel(Bundle paramBundle1, AdRequestParcel paramAdRequestParcel, AdSizeParcel paramAdSizeParcel, String paramString1, ApplicationInfo paramApplicationInfo, PackageInfo paramPackageInfo, String paramString2, String paramString3, String paramString4, VersionInfoParcel paramVersionInfoParcel, Bundle paramBundle2, int paramInt1, List<String> paramList1, List<String> paramList2, Bundle paramBundle3, boolean paramBoolean, Messenger paramMessenger, int paramInt2, int paramInt3, float paramFloat1, String paramString5, long paramLong1, String paramString6, List<String> paramList3, String paramString7, NativeAdOptionsParcel paramNativeAdOptionsParcel, long paramLong2, CapabilityParcel paramCapabilityParcel, String paramString8, float paramFloat2, int paramInt4, int paramInt5)
  {
    this(15, paramBundle1, paramAdRequestParcel, paramAdSizeParcel, paramString1, paramApplicationInfo, paramPackageInfo, paramString2, paramString3, paramString4, paramVersionInfoParcel, paramBundle2, paramInt1, paramList1, paramBundle3, paramBoolean, paramMessenger, paramInt2, paramInt3, paramFloat1, paramString5, paramLong1, paramString6, paramList3, paramString7, paramNativeAdOptionsParcel, paramList2, paramLong2, paramCapabilityParcel, paramString8, paramFloat2, paramInt4, paramInt5);
  }
  
  public AdRequestInfoParcel(zza paramzza, String paramString, long paramLong)
  {
    this(paramzza.a, paramzza.b, paramzza.c, paramzza.d, paramzza.e, paramzza.f, paramString, paramzza.g, paramzza.h, paramzza.j, paramzza.i, paramzza.k, paramzza.l, paramzza.m, paramzza.n, paramzza.o, paramzza.p, paramzza.q, paramzza.r, paramzza.s, paramzza.t, paramzza.u, paramzza.v, paramzza.w, paramzza.x, paramzza.y, paramLong, paramzza.z, paramzza.A, paramzza.B, paramzza.C, paramzza.D);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf.a(this, paramParcel, paramInt);
  }
  
  @zzhb
  public static final class zza
  {
    public final String A;
    public final float B;
    public final int C;
    public final int D;
    public final Bundle a;
    public final AdRequestParcel b;
    public final AdSizeParcel c;
    public final String d;
    public final ApplicationInfo e;
    public final PackageInfo f;
    public final String g;
    public final String h;
    public final Bundle i;
    public final VersionInfoParcel j;
    public final int k;
    public final List<String> l;
    public final List<String> m;
    public final Bundle n;
    public final boolean o;
    public final Messenger p;
    public final int q;
    public final int r;
    public final float s;
    public final String t;
    public final long u;
    public final String v;
    public final List<String> w;
    public final String x;
    public final NativeAdOptionsParcel y;
    public final CapabilityParcel z;
    
    public zza(Bundle paramBundle1, AdRequestParcel paramAdRequestParcel, AdSizeParcel paramAdSizeParcel, String paramString1, ApplicationInfo paramApplicationInfo, PackageInfo paramPackageInfo, String paramString2, String paramString3, VersionInfoParcel paramVersionInfoParcel, Bundle paramBundle2, List<String> paramList1, List<String> paramList2, Bundle paramBundle3, boolean paramBoolean, Messenger paramMessenger, int paramInt1, int paramInt2, float paramFloat1, String paramString4, long paramLong, String paramString5, List<String> paramList3, String paramString6, NativeAdOptionsParcel paramNativeAdOptionsParcel, CapabilityParcel paramCapabilityParcel, String paramString7, float paramFloat2, int paramInt3, int paramInt4)
    {
      this.a = paramBundle1;
      this.b = paramAdRequestParcel;
      this.c = paramAdSizeParcel;
      this.d = paramString1;
      this.e = paramApplicationInfo;
      this.f = paramPackageInfo;
      this.g = paramString2;
      this.h = paramString3;
      this.j = paramVersionInfoParcel;
      this.i = paramBundle2;
      this.o = paramBoolean;
      this.p = paramMessenger;
      this.q = paramInt1;
      this.r = paramInt2;
      this.s = paramFloat1;
      if ((paramList1 != null) && (paramList1.size() > 0))
      {
        this.k = 3;
        this.l = paramList1;
        this.m = paramList2;
        this.n = paramBundle3;
        this.t = paramString4;
        this.u = paramLong;
        this.v = paramString5;
        this.w = paramList3;
        this.x = paramString6;
        this.y = paramNativeAdOptionsParcel;
        this.z = paramCapabilityParcel;
        this.A = paramString7;
        this.B = paramFloat2;
        this.C = paramInt3;
        this.D = paramInt4;
        return;
      }
      if (paramAdSizeParcel.k) {}
      for (this.k = 4;; this.k = 0)
      {
        this.l = null;
        this.m = null;
        break;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/request/AdRequestInfoParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */