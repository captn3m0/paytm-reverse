package com.google.android.gms.ads.internal.request;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.zzhb;

@zzhb
public class StringParcel
  implements SafeParcelable
{
  public static final Parcelable.Creator<StringParcel> CREATOR = new zzn();
  final int a;
  String b;
  
  StringParcel(int paramInt, String paramString)
  {
    this.a = paramInt;
    this.b = paramString;
  }
  
  public StringParcel(String paramString)
  {
    this.a = 1;
    this.b = paramString;
  }
  
  public String a()
  {
    return this.b;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzn.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/request/StringParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */