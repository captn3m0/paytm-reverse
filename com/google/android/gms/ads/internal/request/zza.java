package com.google.android.gms.ads.internal.request;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.internal.zzan;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzif.zza;
import com.google.android.gms.internal.zzim;

@zzhb
public class zza
{
  public zzim a(Context paramContext, AdRequestInfoParcel.zza paramzza, zzan paramzzan, zza paramzza1)
  {
    if (paramzza.b.c.getBundle("sdk_less_server_data") != null) {}
    for (paramContext = new zzm(paramContext, paramzza, paramzza1);; paramContext = new zzb(paramContext, paramzza, paramzzan, paramzza1))
    {
      paramContext.f();
      return paramContext;
    }
  }
  
  public static abstract interface zza
  {
    public abstract void a(zzif.zza paramzza);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/request/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */