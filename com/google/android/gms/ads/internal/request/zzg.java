package com.google.android.gms.ads.internal.request;

import com.google.android.gms.internal.zzhb;
import java.lang.ref.WeakReference;

@zzhb
public final class zzg
  extends zzk.zza
{
  private final WeakReference<zzc.zza> a;
  
  public zzg(zzc.zza paramzza)
  {
    this.a = new WeakReference(paramzza);
  }
  
  public void a(AdResponseParcel paramAdResponseParcel)
  {
    zzc.zza localzza = (zzc.zza)this.a.get();
    if (localzza != null) {
      localzza.a(paramAdResponseParcel);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/request/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */