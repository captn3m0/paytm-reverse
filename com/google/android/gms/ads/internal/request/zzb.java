package com.google.android.gms.ads.internal.request;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzr;
import com.google.android.gms.internal.zzaj;
import com.google.android.gms.internal.zzan;
import com.google.android.gms.internal.zzbp;
import com.google.android.gms.internal.zzbt;
import com.google.android.gms.internal.zzeo;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzif.zza;
import com.google.android.gms.internal.zzih;
import com.google.android.gms.internal.zzim;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zziq;
import com.google.android.gms.internal.zzir;
import com.google.android.gms.internal.zzit;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzjj;
import com.google.android.gms.internal.zzmq;
import org.json.JSONException;
import org.json.JSONObject;

@zzhb
public class zzb
  extends zzim
  implements zzc.zza
{
  zzit a;
  AdResponseParcel b;
  zzeo c;
  private final zza.zza d;
  private final AdRequestInfoParcel.zza e;
  private final Object f = new Object();
  private final Context g;
  private final zzan h;
  private AdRequestInfoParcel i;
  private Runnable j;
  
  public zzb(Context paramContext, AdRequestInfoParcel.zza paramzza, zzan paramzzan, zza.zza paramzza1)
  {
    this.d = paramzza1;
    this.g = paramContext;
    this.e = paramzza;
    this.h = paramzzan;
  }
  
  private void a(int paramInt, String paramString)
  {
    if ((paramInt == 3) || (paramInt == -1))
    {
      zzin.c(paramString);
      if (this.b != null) {
        break label93;
      }
      this.b = new AdResponseParcel(paramInt);
      label33:
      if (this.i == null) {
        break label115;
      }
    }
    label93:
    label115:
    for (paramString = this.i;; paramString = new AdRequestInfoParcel(this.e, null, -1L))
    {
      paramString = new zzif.zza(paramString, this.b, this.c, null, paramInt, -1L, this.b.n, null);
      this.d.a(paramString);
      return;
      zzin.d(paramString);
      break;
      this.b = new AdResponseParcel(paramInt, this.b.k);
      break label33;
    }
  }
  
  protected AdSizeParcel a(AdRequestInfoParcel paramAdRequestInfoParcel)
    throws zzb.zza
  {
    if (this.b.m == null) {
      throw new zza("The ad response must specify one of the supported ad sizes.", 0);
    }
    Object localObject = this.b.m.split("x");
    if (localObject.length != 2) {
      throw new zza("Invalid ad size format from the ad response: " + this.b.m, 0);
    }
    for (;;)
    {
      int k;
      AdSizeParcel localAdSizeParcel;
      try
      {
        int i1 = Integer.parseInt(localObject[0]);
        int i2 = Integer.parseInt(localObject[1]);
        localObject = paramAdRequestInfoParcel.d.h;
        int i3 = localObject.length;
        k = 0;
        if (k >= i3) {
          break;
        }
        localAdSizeParcel = localObject[k];
        float f1 = this.g.getResources().getDisplayMetrics().density;
        if (localAdSizeParcel.f == -1)
        {
          m = (int)(localAdSizeParcel.g / f1);
          if (localAdSizeParcel.c != -2) {
            break label253;
          }
          n = (int)(localAdSizeParcel.d / f1);
          if ((i1 != m) || (i2 != n)) {
            break label263;
          }
          return new AdSizeParcel(localAdSizeParcel, paramAdRequestInfoParcel.d.h);
        }
      }
      catch (NumberFormatException paramAdRequestInfoParcel)
      {
        throw new zza("Invalid ad size number from the ad response: " + this.b.m, 0);
      }
      int m = localAdSizeParcel.f;
      continue;
      label253:
      int n = localAdSizeParcel.c;
      continue;
      label263:
      k += 1;
    }
    throw new zza("The ad size from the ad response was not one of the requested sizes: " + this.b.m, 0);
  }
  
  zzit a(VersionInfoParcel paramVersionInfoParcel, zzji<AdRequestInfoParcel> paramzzji)
  {
    return zzc.a(this.g, paramVersionInfoParcel, paramzzji, this);
  }
  
  public void a()
  {
    zzin.a("AdLoaderBackgroundTask started.");
    this.j = new Runnable()
    {
      public void run()
      {
        synchronized (zzb.a(zzb.this))
        {
          if (zzb.this.a == null) {
            return;
          }
          zzb.this.b();
          zzb.a(zzb.this, 2, "Timed out waiting for ad response.");
          return;
        }
      }
    };
    zzir.a.postDelayed(this.j, ((Long)zzbt.ax.c()).longValue());
    final zzjj localzzjj = new zzjj();
    long l = zzr.i().b();
    zziq.a(new Runnable()
    {
      public void run()
      {
        synchronized (zzb.a(zzb.this))
        {
          zzb.this.a = zzb.this.a(zzb.b(zzb.this).j, localzzjj);
          if (zzb.this.a == null)
          {
            zzb.a(zzb.this, 0, "Could not start the ad request service.");
            zzir.a.removeCallbacks(zzb.c(zzb.this));
          }
          return;
        }
      }
    });
    String str = this.h.a().a(this.g);
    this.i = new AdRequestInfoParcel(this.e, str, l);
    localzzjj.a(this.i);
  }
  
  public void a(@NonNull AdResponseParcel arg1)
  {
    zzin.a("Received ad response.");
    this.b = ???;
    long l = zzr.i().b();
    synchronized (this.f)
    {
      this.a = null;
      try
      {
        if ((this.b.e != -2) && (this.b.e != -3)) {
          throw new zza("There was a problem getting an ad response. ErrorCode: " + this.b.e, this.b.e);
        }
      }
      catch (zza ???)
      {
        a(???.a(), ???.getMessage());
        zzir.a.removeCallbacks(this.j);
        return;
      }
    }
    c();
    if (this.i.d.h != null) {}
    for (??? = a(this.i);; ??? = null)
    {
      zzr.h().a(this.b.v);
      if (!TextUtils.isEmpty(this.b.r)) {}
      for (;;)
      {
        try
        {
          JSONObject localJSONObject = new JSONObject(this.b.r);
          ??? = new zzif.zza(this.i, this.b, this.c, ???, -2, l, this.b.n, localJSONObject);
          this.d.a(???);
          zzir.a.removeCallbacks(this.j);
          return;
        }
        catch (Exception localException)
        {
          zzin.b("Error parsing the JSON for Active View.", localException);
        }
        Object localObject2 = null;
      }
    }
  }
  
  public void b()
  {
    synchronized (this.f)
    {
      if (this.a != null) {
        this.a.cancel();
      }
      return;
    }
  }
  
  protected void c()
    throws zzb.zza
  {
    if (this.b.e == -3) {}
    do
    {
      return;
      if (TextUtils.isEmpty(this.b.c)) {
        throw new zza("No fill from ad server.", 3);
      }
      zzr.h().a(this.g, this.b.u);
    } while (!this.b.h);
    try
    {
      this.c = new zzeo(this.b.c);
      return;
    }
    catch (JSONException localJSONException)
    {
      throw new zza("Could not parse mediation config: " + this.b.c, 0);
    }
  }
  
  @zzhb
  static final class zza
    extends Exception
  {
    private final int a;
    
    public zza(String paramString, int paramInt)
    {
      super();
      this.a = paramInt;
    }
    
    public int a()
    {
      return this.a;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/request/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */