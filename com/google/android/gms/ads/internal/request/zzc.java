package com.google.android.gms.ads.internal.request;

import android.content.Context;
import com.google.android.gms.ads.internal.client.zzn;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.common.zze;
import com.google.android.gms.internal.zzbp;
import com.google.android.gms.internal.zzbt;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzit;
import com.google.android.gms.internal.zzji;

@zzhb
public final class zzc
{
  public static zzit a(Context paramContext, VersionInfoParcel paramVersionInfoParcel, zzji<AdRequestInfoParcel> paramzzji, zza paramzza)
  {
    a(paramContext, paramVersionInfoParcel, paramzzji, paramzza, new zzb()
    {
      public boolean a(VersionInfoParcel paramAnonymousVersionInfoParcel)
      {
        return (paramAnonymousVersionInfoParcel.e) || ((zze.j(this.a)) && (!((Boolean)zzbt.B.c()).booleanValue()));
      }
    });
  }
  
  static zzit a(Context paramContext, VersionInfoParcel paramVersionInfoParcel, zzji<AdRequestInfoParcel> paramzzji, zza paramzza, zzb paramzzb)
  {
    if (paramzzb.a(paramVersionInfoParcel)) {
      return a(paramContext, paramzzji, paramzza);
    }
    return b(paramContext, paramVersionInfoParcel, paramzzji, paramzza);
  }
  
  private static zzit a(Context paramContext, zzji<AdRequestInfoParcel> paramzzji, zza paramzza)
  {
    zzin.a("Fetching ad response from local ad request service.");
    paramContext = new zzd.zza(paramContext, paramzzji, paramzza);
    paramContext.c();
    return paramContext;
  }
  
  private static zzit b(Context paramContext, VersionInfoParcel paramVersionInfoParcel, zzji<AdRequestInfoParcel> paramzzji, zza paramzza)
  {
    zzin.a("Fetching ad response from remote ad request service.");
    if (!zzn.a().b(paramContext))
    {
      zzin.d("Failed to connect to remote ad request service.");
      return null;
    }
    return new zzd.zzb(paramContext, paramVersionInfoParcel, paramzzji, paramzza);
  }
  
  public static abstract interface zza
  {
    public abstract void a(AdResponseParcel paramAdResponseParcel);
  }
  
  static abstract interface zzb
  {
    public abstract boolean a(VersionInfoParcel paramVersionInfoParcel);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/request/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */