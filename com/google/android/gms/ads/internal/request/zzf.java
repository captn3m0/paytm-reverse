package com.google.android.gms.ads.internal.request;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzf
  implements Parcelable.Creator<AdRequestInfoParcel>
{
  static void a(AdRequestInfoParcel paramAdRequestInfoParcel, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramAdRequestInfoParcel.a);
    zzb.a(paramParcel, 2, paramAdRequestInfoParcel.b, false);
    zzb.a(paramParcel, 3, paramAdRequestInfoParcel.c, paramInt, false);
    zzb.a(paramParcel, 4, paramAdRequestInfoParcel.d, paramInt, false);
    zzb.a(paramParcel, 5, paramAdRequestInfoParcel.e, false);
    zzb.a(paramParcel, 6, paramAdRequestInfoParcel.f, paramInt, false);
    zzb.a(paramParcel, 7, paramAdRequestInfoParcel.g, paramInt, false);
    zzb.a(paramParcel, 8, paramAdRequestInfoParcel.h, false);
    zzb.a(paramParcel, 9, paramAdRequestInfoParcel.i, false);
    zzb.a(paramParcel, 10, paramAdRequestInfoParcel.j, false);
    zzb.a(paramParcel, 11, paramAdRequestInfoParcel.k, paramInt, false);
    zzb.a(paramParcel, 12, paramAdRequestInfoParcel.l, false);
    zzb.a(paramParcel, 13, paramAdRequestInfoParcel.m);
    zzb.b(paramParcel, 14, paramAdRequestInfoParcel.n, false);
    zzb.a(paramParcel, 15, paramAdRequestInfoParcel.o, false);
    zzb.a(paramParcel, 17, paramAdRequestInfoParcel.q, paramInt, false);
    zzb.a(paramParcel, 16, paramAdRequestInfoParcel.p);
    zzb.a(paramParcel, 19, paramAdRequestInfoParcel.s);
    zzb.a(paramParcel, 18, paramAdRequestInfoParcel.r);
    zzb.a(paramParcel, 21, paramAdRequestInfoParcel.u, false);
    zzb.a(paramParcel, 20, paramAdRequestInfoParcel.t);
    zzb.a(paramParcel, 25, paramAdRequestInfoParcel.v);
    zzb.b(paramParcel, 27, paramAdRequestInfoParcel.x, false);
    zzb.a(paramParcel, 26, paramAdRequestInfoParcel.w, false);
    zzb.a(paramParcel, 29, paramAdRequestInfoParcel.z, paramInt, false);
    zzb.a(paramParcel, 28, paramAdRequestInfoParcel.y, false);
    zzb.a(paramParcel, 31, paramAdRequestInfoParcel.B);
    zzb.b(paramParcel, 30, paramAdRequestInfoParcel.A, false);
    zzb.a(paramParcel, 34, paramAdRequestInfoParcel.E);
    zzb.a(paramParcel, 35, paramAdRequestInfoParcel.F);
    zzb.a(paramParcel, 32, paramAdRequestInfoParcel.C, paramInt, false);
    zzb.a(paramParcel, 33, paramAdRequestInfoParcel.D, false);
    zzb.a(paramParcel, 36, paramAdRequestInfoParcel.G);
    zzb.a(paramParcel, i);
  }
  
  public AdRequestInfoParcel a(Parcel paramParcel)
  {
    int i2 = zza.b(paramParcel);
    int i1 = 0;
    Bundle localBundle3 = null;
    AdRequestParcel localAdRequestParcel = null;
    AdSizeParcel localAdSizeParcel = null;
    String str8 = null;
    ApplicationInfo localApplicationInfo = null;
    PackageInfo localPackageInfo = null;
    String str7 = null;
    String str6 = null;
    String str5 = null;
    VersionInfoParcel localVersionInfoParcel = null;
    Bundle localBundle2 = null;
    int n = 0;
    ArrayList localArrayList3 = null;
    Bundle localBundle1 = null;
    boolean bool = false;
    Messenger localMessenger = null;
    int m = 0;
    int k = 0;
    float f2 = 0.0F;
    String str4 = null;
    long l2 = 0L;
    String str3 = null;
    ArrayList localArrayList2 = null;
    String str2 = null;
    NativeAdOptionsParcel localNativeAdOptionsParcel = null;
    ArrayList localArrayList1 = null;
    long l1 = 0L;
    CapabilityParcel localCapabilityParcel = null;
    String str1 = null;
    float f1 = 0.0F;
    int j = 0;
    int i = 0;
    while (paramParcel.dataPosition() < i2)
    {
      int i3 = zza.a(paramParcel);
      switch (zza.a(i3))
      {
      case 22: 
      case 23: 
      case 24: 
      default: 
        zza.b(paramParcel, i3);
        break;
      case 1: 
        i1 = zza.g(paramParcel, i3);
        break;
      case 2: 
        localBundle3 = zza.r(paramParcel, i3);
        break;
      case 3: 
        localAdRequestParcel = (AdRequestParcel)zza.a(paramParcel, i3, AdRequestParcel.CREATOR);
        break;
      case 4: 
        localAdSizeParcel = (AdSizeParcel)zza.a(paramParcel, i3, AdSizeParcel.CREATOR);
        break;
      case 5: 
        str8 = zza.p(paramParcel, i3);
        break;
      case 6: 
        localApplicationInfo = (ApplicationInfo)zza.a(paramParcel, i3, ApplicationInfo.CREATOR);
        break;
      case 7: 
        localPackageInfo = (PackageInfo)zza.a(paramParcel, i3, PackageInfo.CREATOR);
        break;
      case 8: 
        str7 = zza.p(paramParcel, i3);
        break;
      case 9: 
        str6 = zza.p(paramParcel, i3);
        break;
      case 10: 
        str5 = zza.p(paramParcel, i3);
        break;
      case 11: 
        localVersionInfoParcel = (VersionInfoParcel)zza.a(paramParcel, i3, VersionInfoParcel.CREATOR);
        break;
      case 12: 
        localBundle2 = zza.r(paramParcel, i3);
        break;
      case 13: 
        n = zza.g(paramParcel, i3);
        break;
      case 14: 
        localArrayList3 = zza.D(paramParcel, i3);
        break;
      case 15: 
        localBundle1 = zza.r(paramParcel, i3);
        break;
      case 17: 
        localMessenger = (Messenger)zza.a(paramParcel, i3, Messenger.CREATOR);
        break;
      case 16: 
        bool = zza.c(paramParcel, i3);
        break;
      case 19: 
        k = zza.g(paramParcel, i3);
        break;
      case 18: 
        m = zza.g(paramParcel, i3);
        break;
      case 21: 
        str4 = zza.p(paramParcel, i3);
        break;
      case 20: 
        f2 = zza.l(paramParcel, i3);
        break;
      case 25: 
        l2 = zza.i(paramParcel, i3);
        break;
      case 27: 
        localArrayList2 = zza.D(paramParcel, i3);
        break;
      case 26: 
        str3 = zza.p(paramParcel, i3);
        break;
      case 29: 
        localNativeAdOptionsParcel = (NativeAdOptionsParcel)zza.a(paramParcel, i3, NativeAdOptionsParcel.CREATOR);
        break;
      case 28: 
        str2 = zza.p(paramParcel, i3);
        break;
      case 31: 
        l1 = zza.i(paramParcel, i3);
        break;
      case 30: 
        localArrayList1 = zza.D(paramParcel, i3);
        break;
      case 34: 
        f1 = zza.l(paramParcel, i3);
        break;
      case 35: 
        j = zza.g(paramParcel, i3);
        break;
      case 32: 
        localCapabilityParcel = (CapabilityParcel)zza.a(paramParcel, i3, CapabilityParcel.CREATOR);
        break;
      case 33: 
        str1 = zza.p(paramParcel, i3);
        break;
      case 36: 
        i = zza.g(paramParcel, i3);
      }
    }
    if (paramParcel.dataPosition() != i2) {
      throw new zza.zza("Overread allowed size end=" + i2, paramParcel);
    }
    return new AdRequestInfoParcel(i1, localBundle3, localAdRequestParcel, localAdSizeParcel, str8, localApplicationInfo, localPackageInfo, str7, str6, str5, localVersionInfoParcel, localBundle2, n, localArrayList3, localBundle1, bool, localMessenger, m, k, f2, str4, l2, str3, localArrayList2, str2, localNativeAdOptionsParcel, localArrayList1, l1, localCapabilityParcel, str1, f1, j, i);
  }
  
  public AdRequestInfoParcel[] a(int paramInt)
  {
    return new AdRequestInfoParcel[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/request/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */