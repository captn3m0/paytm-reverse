package com.google.android.gms.ads.internal.request;

import android.content.Context;
import android.os.Binder;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzr;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.internal.zzbm;
import com.google.android.gms.internal.zzbp;
import com.google.android.gms.internal.zzbt;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzhc;
import com.google.android.gms.internal.zzhd;
import com.google.android.gms.internal.zzih;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzir;
import com.google.android.gms.internal.zzit;
import com.google.android.gms.internal.zzix;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzji.zza;
import com.google.android.gms.internal.zzji.zzc;

@zzhb
public abstract class zzd
  implements zzc.zza, zzit<Void>
{
  private final zzji<AdRequestInfoParcel> a;
  private final zzc.zza b;
  private final Object c = new Object();
  
  public zzd(zzji<AdRequestInfoParcel> paramzzji, zzc.zza paramzza)
  {
    this.a = paramzzji;
    this.b = paramzza;
  }
  
  public abstract void a();
  
  public void a(AdResponseParcel paramAdResponseParcel)
  {
    synchronized (this.c)
    {
      this.b.a(paramAdResponseParcel);
      a();
      return;
    }
  }
  
  boolean a(zzj paramzzj, AdRequestInfoParcel paramAdRequestInfoParcel)
  {
    try
    {
      paramzzj.a(paramAdRequestInfoParcel, new zzg(this));
      return true;
    }
    catch (RemoteException paramzzj)
    {
      zzin.d("Could not fetch ad response from ad request service.", paramzzj);
      zzr.h().a(paramzzj, true);
      this.b.a(new AdResponseParcel(0));
      return false;
    }
    catch (NullPointerException paramzzj)
    {
      for (;;)
      {
        zzin.d("Could not fetch ad response from ad request service due to an Exception.", paramzzj);
        zzr.h().a(paramzzj, true);
      }
    }
    catch (SecurityException paramzzj)
    {
      for (;;)
      {
        zzin.d("Could not fetch ad response from ad request service due to an Exception.", paramzzj);
        zzr.h().a(paramzzj, true);
      }
    }
    catch (Throwable paramzzj)
    {
      for (;;)
      {
        zzin.d("Could not fetch ad response from ad request service due to an Exception.", paramzzj);
        zzr.h().a(paramzzj, true);
      }
    }
  }
  
  public abstract zzj b();
  
  public Void c()
  {
    final zzj localzzj = b();
    if (localzzj == null)
    {
      this.b.a(new AdResponseParcel(0));
      a();
      return null;
    }
    this.a.a(new zzji.zzc()new zzji.zza
    {
      public void a(AdRequestInfoParcel paramAnonymousAdRequestInfoParcel)
      {
        if (!zzd.this.a(localzzj, paramAnonymousAdRequestInfoParcel)) {
          zzd.this.a();
        }
      }
    }, new zzji.zza()
    {
      public void a()
      {
        zzd.this.a();
      }
    });
    return null;
  }
  
  public void cancel()
  {
    a();
  }
  
  @zzhb
  public static final class zza
    extends zzd
  {
    private final Context a;
    
    public zza(Context paramContext, zzji<AdRequestInfoParcel> paramzzji, zzc.zza paramzza)
    {
      super(paramzza);
      this.a = paramContext;
    }
    
    public void a() {}
    
    public zzj b()
    {
      zzbm localzzbm = new zzbm((String)zzbt.b.c());
      return zzhd.a(this.a, localzzbm, zzhc.a());
    }
  }
  
  @zzhb
  public static class zzb
    extends zzd
    implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
  {
    protected zze a;
    private Context b;
    private VersionInfoParcel c;
    private zzji<AdRequestInfoParcel> d;
    private final zzc.zza e;
    private final Object f = new Object();
    private boolean g;
    
    public zzb(Context paramContext, VersionInfoParcel paramVersionInfoParcel, zzji<AdRequestInfoParcel> paramzzji, zzc.zza paramzza)
    {
      super(paramzza);
      this.b = paramContext;
      this.c = paramVersionInfoParcel;
      this.d = paramzzji;
      this.e = paramzza;
      if (((Boolean)zzbt.A.c()).booleanValue()) {
        this.g = true;
      }
      for (paramVersionInfoParcel = zzr.q().a();; paramVersionInfoParcel = paramContext.getMainLooper())
      {
        this.a = new zze(paramContext, paramVersionInfoParcel, this, this, this.c.d);
        e();
        return;
      }
    }
    
    public void a()
    {
      synchronized (this.f)
      {
        if ((this.a.k()) || (this.a.p())) {
          this.a.f();
        }
        Binder.flushPendingCommands();
        if (this.g)
        {
          zzr.q().b();
          this.g = false;
        }
        return;
      }
    }
    
    /* Error */
    public zzj b()
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 37	com/google/android/gms/ads/internal/request/zzd$zzb:f	Ljava/lang/Object;
      //   4: astore_1
      //   5: aload_1
      //   6: monitorenter
      //   7: aload_0
      //   8: getfield 87	com/google/android/gms/ads/internal/request/zzd$zzb:a	Lcom/google/android/gms/ads/internal/request/zze;
      //   11: invokevirtual 119	com/google/android/gms/ads/internal/request/zze:c	()Lcom/google/android/gms/ads/internal/request/zzj;
      //   14: astore_2
      //   15: aload_1
      //   16: monitorexit
      //   17: aload_2
      //   18: areturn
      //   19: aload_1
      //   20: monitorexit
      //   21: aconst_null
      //   22: areturn
      //   23: astore_2
      //   24: aload_1
      //   25: monitorexit
      //   26: aload_2
      //   27: athrow
      //   28: astore_2
      //   29: goto -10 -> 19
      //   32: astore_2
      //   33: goto -14 -> 19
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	36	0	this	zzb
      //   14	4	2	localzzj	zzj
      //   23	4	2	localObject2	Object
      //   28	1	2	localDeadObjectException	android.os.DeadObjectException
      //   32	1	2	localIllegalStateException	IllegalStateException
      // Exception table:
      //   from	to	target	type
      //   7	15	23	finally
      //   15	17	23	finally
      //   19	21	23	finally
      //   24	26	23	finally
      //   7	15	28	android/os/DeadObjectException
      //   7	15	32	java/lang/IllegalStateException
    }
    
    protected void e()
    {
      this.a.o();
    }
    
    zzit f()
    {
      return new zzd.zza(this.b, this.d, this.e);
    }
    
    public void onConnected(Bundle paramBundle)
    {
      c();
    }
    
    public void onConnectionFailed(@NonNull ConnectionResult paramConnectionResult)
    {
      zzin.a("Cannot connect to remote service, fallback to local instance.");
      f().d();
      paramConnectionResult = new Bundle();
      paramConnectionResult.putString("action", "gms_connection_failed_fallback_to_local");
      zzr.e().b(this.b, this.c.b, "gmob-apps", paramConnectionResult, true);
    }
    
    public void onConnectionSuspended(int paramInt)
    {
      zzin.a("Disconnected from remote ad request service.");
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/request/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */