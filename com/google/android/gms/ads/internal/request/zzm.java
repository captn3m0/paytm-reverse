package com.google.android.gms.ads.internal.request;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.zzr;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.internal.zzbm;
import com.google.android.gms.internal.zzbp;
import com.google.android.gms.internal.zzbt;
import com.google.android.gms.internal.zzdf;
import com.google.android.gms.internal.zzdg;
import com.google.android.gms.internal.zzdk;
import com.google.android.gms.internal.zzed;
import com.google.android.gms.internal.zzeg;
import com.google.android.gms.internal.zzeg.zzb;
import com.google.android.gms.internal.zzeg.zzd;
import com.google.android.gms.internal.zzeh;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzhe;
import com.google.android.gms.internal.zzhk;
import com.google.android.gms.internal.zzif.zza;
import com.google.android.gms.internal.zzim;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzir;
import com.google.android.gms.internal.zzji.zza;
import com.google.android.gms.internal.zzji.zzc;
import com.google.android.gms.internal.zzjp;
import com.google.android.gms.internal.zzmq;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

@zzhb
public class zzm
  extends zzim
{
  static final long a = TimeUnit.SECONDS.toMillis(10L);
  private static final Object b = new Object();
  private static boolean c = false;
  private static zzeg d = null;
  private static zzdg e = null;
  private static zzdk f = null;
  private static zzdf g = null;
  private final zza.zza h;
  private final AdRequestInfoParcel.zza i;
  private final Object j = new Object();
  private final Context k;
  private zzeg.zzd l;
  
  public zzm(Context paramContext, AdRequestInfoParcel.zza paramzza, zza.zza arg3)
  {
    super(true);
    this.h = ???;
    this.k = paramContext;
    this.i = paramzza;
    synchronized (b)
    {
      if (!c)
      {
        f = new zzdk();
        e = new zzdg(paramContext.getApplicationContext(), paramzza.j);
        g = new zzc();
        d = new zzeg(this.k.getApplicationContext(), this.i.j, (String)zzbt.b.c(), new zzb(), new zza());
        c = true;
      }
      return;
    }
  }
  
  private AdResponseParcel a(AdRequestInfoParcel paramAdRequestInfoParcel)
  {
    final Object localObject = UUID.randomUUID().toString();
    final JSONObject localJSONObject = a(paramAdRequestInfoParcel, (String)localObject);
    if (localJSONObject == null) {
      paramAdRequestInfoParcel = new AdResponseParcel(0);
    }
    for (;;)
    {
      return paramAdRequestInfoParcel;
      long l1 = zzr.i().b();
      Future localFuture = f.a((String)localObject);
      zza.a.post(new Runnable()
      {
        public void run()
        {
          zzm.a(zzm.this, zzm.e().b());
          zzm.b(zzm.this).a(new zzji.zzc()new zzji.zza
          {
            public void a(zzeh paramAnonymous2zzeh)
            {
              try
              {
                paramAnonymous2zzeh.a("AFMA_getAdapterLessMediationAd", zzm.2.this.a);
                return;
              }
              catch (Exception paramAnonymous2zzeh)
              {
                zzin.b("Error requesting an ad url", paramAnonymous2zzeh);
                zzm.c().b(zzm.2.this.b);
              }
            }
          }, new zzji.zza()
          {
            public void a()
            {
              zzm.c().b(zzm.2.this.b);
            }
          });
        }
      });
      long l2 = a;
      long l3 = zzr.i().b();
      try
      {
        localObject = (JSONObject)localFuture.get(l2 - (l3 - l1), TimeUnit.MILLISECONDS);
        if (localObject == null) {
          return new AdResponseParcel(-1);
        }
      }
      catch (InterruptedException paramAdRequestInfoParcel)
      {
        return new AdResponseParcel(-1);
      }
      catch (TimeoutException paramAdRequestInfoParcel)
      {
        return new AdResponseParcel(2);
      }
      catch (ExecutionException paramAdRequestInfoParcel)
      {
        return new AdResponseParcel(0);
        localObject = zzhe.a(this.k, paramAdRequestInfoParcel, ((JSONObject)localObject).toString());
        paramAdRequestInfoParcel = (AdRequestInfoParcel)localObject;
        if (((AdResponseParcel)localObject).e == -3) {
          continue;
        }
        paramAdRequestInfoParcel = (AdRequestInfoParcel)localObject;
        if (!TextUtils.isEmpty(((AdResponseParcel)localObject).c)) {
          continue;
        }
        return new AdResponseParcel(3);
      }
      catch (CancellationException paramAdRequestInfoParcel)
      {
        for (;;) {}
      }
    }
  }
  
  private JSONObject a(AdRequestInfoParcel paramAdRequestInfoParcel, String paramString)
  {
    Bundle localBundle = paramAdRequestInfoParcel.c.c.getBundle("sdk_less_server_data");
    String str = paramAdRequestInfoParcel.c.c.getString("sdk_less_network_id");
    if (localBundle == null) {}
    JSONObject localJSONObject;
    do
    {
      return null;
      localJSONObject = zzhe.a(this.k, paramAdRequestInfoParcel, zzr.k().a(this.k), null, null, new zzbm((String)zzbt.b.c()), null, null, new ArrayList(), null);
    } while (localJSONObject == null);
    try
    {
      paramAdRequestInfoParcel = AdvertisingIdClient.b(this.k);
      localHashMap = new HashMap();
      localHashMap.put("request_id", paramString);
      localHashMap.put("network_id", str);
      localHashMap.put("request_param", localJSONObject);
      localHashMap.put("data", localBundle);
      if (paramAdRequestInfoParcel != null)
      {
        localHashMap.put("adid", paramAdRequestInfoParcel.a());
        if (!paramAdRequestInfoParcel.b()) {
          break label211;
        }
        m = 1;
        localHashMap.put("lat", Integer.valueOf(m));
      }
    }
    catch (GooglePlayServicesRepairableException paramAdRequestInfoParcel)
    {
      for (;;)
      {
        try
        {
          HashMap localHashMap;
          paramAdRequestInfoParcel = zzr.e().a(localHashMap);
          return paramAdRequestInfoParcel;
        }
        catch (JSONException paramAdRequestInfoParcel)
        {
          int m;
          return null;
        }
        paramAdRequestInfoParcel = paramAdRequestInfoParcel;
        zzin.d("Cannot get advertising id info", paramAdRequestInfoParcel);
        paramAdRequestInfoParcel = null;
        continue;
        m = 0;
      }
    }
    catch (IllegalStateException paramAdRequestInfoParcel)
    {
      for (;;) {}
    }
    catch (GooglePlayServicesNotAvailableException paramAdRequestInfoParcel)
    {
      for (;;) {}
    }
    catch (IOException paramAdRequestInfoParcel)
    {
      label211:
      for (;;) {}
    }
  }
  
  protected static void a(zzed paramzzed)
  {
    paramzzed.a("/loadAd", f);
    paramzzed.a("/fetchHttpRequest", e);
    paramzzed.a("/invalidRequest", g);
  }
  
  protected static void b(zzed paramzzed)
  {
    paramzzed.b("/loadAd", f);
    paramzzed.b("/fetchHttpRequest", e);
    paramzzed.b("/invalidRequest", g);
  }
  
  public void a()
  {
    zzin.a("SdkLessAdLoaderBackgroundTask started.");
    final Object localObject = new AdRequestInfoParcel(this.i, null, -1L);
    AdResponseParcel localAdResponseParcel = a((AdRequestInfoParcel)localObject);
    long l1 = zzr.i().b();
    localObject = new zzif.zza((AdRequestInfoParcel)localObject, localAdResponseParcel, null, null, localAdResponseParcel.e, l1, localAdResponseParcel.n, null);
    zza.a.post(new Runnable()
    {
      public void run()
      {
        zzm.a(zzm.this).a(localObject);
        if (zzm.b(zzm.this) != null)
        {
          zzm.b(zzm.this).a();
          zzm.a(zzm.this, null);
        }
      }
    });
  }
  
  public void b()
  {
    synchronized (this.j)
    {
      zza.a.post(new Runnable()
      {
        public void run()
        {
          if (zzm.b(zzm.this) != null)
          {
            zzm.b(zzm.this).a();
            zzm.a(zzm.this, null);
          }
        }
      });
      return;
    }
  }
  
  public static class zza
    implements zzeg.zzb<zzed>
  {
    public void a(zzed paramzzed)
    {
      zzm.b(paramzzed);
    }
  }
  
  public static class zzb
    implements zzeg.zzb<zzed>
  {
    public void a(zzed paramzzed)
    {
      zzm.a(paramzzed);
    }
  }
  
  public static class zzc
    implements zzdf
  {
    public void a(zzjp paramzzjp, Map<String, String> paramMap)
    {
      paramzzjp = (String)paramMap.get("request_id");
      paramMap = (String)paramMap.get("errors");
      zzin.d("Invalid request: " + paramMap);
      zzm.c().b(paramzzjp);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/request/zzm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */