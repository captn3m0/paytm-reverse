package com.google.android.gms.ads.internal.overlay;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzr;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzir;

@zzhb
public class zza
{
  public boolean a(Context paramContext, Intent paramIntent, zzp paramzzp)
  {
    try
    {
      zzin.e("Launching an intent: " + paramIntent.toURI());
      zzr.e().a(paramContext, paramIntent);
      if (paramzzp != null) {
        paramzzp.l();
      }
      return true;
    }
    catch (ActivityNotFoundException paramContext)
    {
      zzin.d(paramContext.getMessage());
    }
    return false;
  }
  
  public boolean a(Context paramContext, AdLauncherIntentInfoParcel paramAdLauncherIntentInfoParcel, zzp paramzzp)
  {
    int i = 0;
    if (paramAdLauncherIntentInfoParcel == null)
    {
      zzin.d("No intent data for launcher overlay.");
      return false;
    }
    if (paramAdLauncherIntentInfoParcel.i != null) {
      return a(paramContext, paramAdLauncherIntentInfoParcel.i, paramzzp);
    }
    Intent localIntent = new Intent();
    if (TextUtils.isEmpty(paramAdLauncherIntentInfoParcel.c))
    {
      zzin.d("Open GMSG did not contain a URL.");
      return false;
    }
    if (!TextUtils.isEmpty(paramAdLauncherIntentInfoParcel.d)) {
      localIntent.setDataAndType(Uri.parse(paramAdLauncherIntentInfoParcel.c), paramAdLauncherIntentInfoParcel.d);
    }
    String[] arrayOfString;
    for (;;)
    {
      localIntent.setAction("android.intent.action.VIEW");
      if (!TextUtils.isEmpty(paramAdLauncherIntentInfoParcel.e)) {
        localIntent.setPackage(paramAdLauncherIntentInfoParcel.e);
      }
      if (TextUtils.isEmpty(paramAdLauncherIntentInfoParcel.f)) {
        break label199;
      }
      arrayOfString = paramAdLauncherIntentInfoParcel.f.split("/", 2);
      if (arrayOfString.length >= 2) {
        break;
      }
      zzin.d("Could not parse component name from open GMSG: " + paramAdLauncherIntentInfoParcel.f);
      return false;
      localIntent.setData(Uri.parse(paramAdLauncherIntentInfoParcel.c));
    }
    localIntent.setClassName(arrayOfString[0], arrayOfString[1]);
    label199:
    paramAdLauncherIntentInfoParcel = paramAdLauncherIntentInfoParcel.g;
    if (!TextUtils.isEmpty(paramAdLauncherIntentInfoParcel)) {}
    try
    {
      int j = Integer.parseInt(paramAdLauncherIntentInfoParcel);
      i = j;
    }
    catch (NumberFormatException paramAdLauncherIntentInfoParcel)
    {
      for (;;)
      {
        zzin.d("Could not parse intent flags.");
      }
    }
    localIntent.addFlags(i);
    return a(paramContext, localIntent, paramzzp);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */