package com.google.android.gms.ads.internal.overlay;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class zzb
  implements Parcelable.Creator<AdLauncherIntentInfoParcel>
{
  static void a(AdLauncherIntentInfoParcel paramAdLauncherIntentInfoParcel, Parcel paramParcel, int paramInt)
  {
    int i = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramAdLauncherIntentInfoParcel.a);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramAdLauncherIntentInfoParcel.b, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 3, paramAdLauncherIntentInfoParcel.c, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 4, paramAdLauncherIntentInfoParcel.d, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 5, paramAdLauncherIntentInfoParcel.e, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 6, paramAdLauncherIntentInfoParcel.f, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 7, paramAdLauncherIntentInfoParcel.g, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 8, paramAdLauncherIntentInfoParcel.h, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 9, paramAdLauncherIntentInfoParcel.i, paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, i);
  }
  
  public AdLauncherIntentInfoParcel a(Parcel paramParcel)
  {
    Intent localIntent = null;
    int j = zza.b(paramParcel);
    int i = 0;
    String str1 = null;
    String str2 = null;
    String str3 = null;
    String str4 = null;
    String str5 = null;
    String str6 = null;
    String str7 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str7 = zza.p(paramParcel, k);
        break;
      case 3: 
        str6 = zza.p(paramParcel, k);
        break;
      case 4: 
        str5 = zza.p(paramParcel, k);
        break;
      case 5: 
        str4 = zza.p(paramParcel, k);
        break;
      case 6: 
        str3 = zza.p(paramParcel, k);
        break;
      case 7: 
        str2 = zza.p(paramParcel, k);
        break;
      case 8: 
        str1 = zza.p(paramParcel, k);
        break;
      case 9: 
        localIntent = (Intent)zza.a(paramParcel, k, Intent.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new AdLauncherIntentInfoParcel(i, str7, str6, str5, str4, str3, str2, str1, localIntent);
  }
  
  public AdLauncherIntentInfoParcel[] a(int paramInt)
  {
    return new AdLauncherIntentInfoParcel[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */