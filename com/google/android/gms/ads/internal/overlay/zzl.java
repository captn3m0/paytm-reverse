package com.google.android.gms.ads.internal.overlay;

import com.google.android.gms.internal.zzjp;
import java.util.Map;

public abstract interface zzl
{
  public abstract void a();
  
  public abstract void a(zzjp paramzzjp, Map<String, String> paramMap);
  
  public abstract void b();
  
  public abstract void c();
  
  public abstract boolean d();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */