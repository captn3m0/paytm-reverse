package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.support.annotation.Nullable;
import com.google.android.gms.internal.zzbz;
import com.google.android.gms.internal.zzcb;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzjp;

@zzhb
public class zzn
  extends zzj
{
  @Nullable
  public zzi a(Context paramContext, zzjp paramzzjp, int paramInt, zzcb paramzzcb, zzbz paramzzbz)
  {
    if (!a(paramContext)) {
      return null;
    }
    return new zzc(paramContext, new zzt(paramContext, paramzzjp.o(), paramzzjp.u(), paramzzcb, paramzzbz));
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */