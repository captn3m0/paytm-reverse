package com.google.android.gms.ads.internal.overlay;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.ads.internal.InterstitialAdParameterParcel;
import com.google.android.gms.ads.internal.client.zza;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zzd.zza;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzdb;
import com.google.android.gms.internal.zzdh;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzjp;

@zzhb
public final class AdOverlayInfoParcel
  implements SafeParcelable
{
  public static final zzf CREATOR = new zzf();
  public final int a;
  public final AdLauncherIntentInfoParcel b;
  public final zza c;
  public final zzg d;
  public final zzjp e;
  public final zzdb f;
  public final String g;
  public final boolean h;
  public final String i;
  public final zzp j;
  public final int k;
  public final int l;
  public final String m;
  public final VersionInfoParcel n;
  public final zzdh o;
  public final String p;
  public final InterstitialAdParameterParcel q;
  
  AdOverlayInfoParcel(int paramInt1, AdLauncherIntentInfoParcel paramAdLauncherIntentInfoParcel, IBinder paramIBinder1, IBinder paramIBinder2, IBinder paramIBinder3, IBinder paramIBinder4, String paramString1, boolean paramBoolean, String paramString2, IBinder paramIBinder5, int paramInt2, int paramInt3, String paramString3, VersionInfoParcel paramVersionInfoParcel, IBinder paramIBinder6, String paramString4, InterstitialAdParameterParcel paramInterstitialAdParameterParcel)
  {
    this.a = paramInt1;
    this.b = paramAdLauncherIntentInfoParcel;
    this.c = ((zza)zze.a(zzd.zza.a(paramIBinder1)));
    this.d = ((zzg)zze.a(zzd.zza.a(paramIBinder2)));
    this.e = ((zzjp)zze.a(zzd.zza.a(paramIBinder3)));
    this.f = ((zzdb)zze.a(zzd.zza.a(paramIBinder4)));
    this.g = paramString1;
    this.h = paramBoolean;
    this.i = paramString2;
    this.j = ((zzp)zze.a(zzd.zza.a(paramIBinder5)));
    this.k = paramInt2;
    this.l = paramInt3;
    this.m = paramString3;
    this.n = paramVersionInfoParcel;
    this.o = ((zzdh)zze.a(zzd.zza.a(paramIBinder6)));
    this.p = paramString4;
    this.q = paramInterstitialAdParameterParcel;
  }
  
  public AdOverlayInfoParcel(zza paramzza, zzg paramzzg, zzp paramzzp, zzjp paramzzjp, int paramInt, VersionInfoParcel paramVersionInfoParcel, String paramString, InterstitialAdParameterParcel paramInterstitialAdParameterParcel)
  {
    this.a = 4;
    this.b = null;
    this.c = paramzza;
    this.d = paramzzg;
    this.e = paramzzjp;
    this.f = null;
    this.g = null;
    this.h = false;
    this.i = null;
    this.j = paramzzp;
    this.k = paramInt;
    this.l = 1;
    this.m = null;
    this.n = paramVersionInfoParcel;
    this.o = null;
    this.p = paramString;
    this.q = paramInterstitialAdParameterParcel;
  }
  
  public AdOverlayInfoParcel(zza paramzza, zzg paramzzg, zzp paramzzp, zzjp paramzzjp, boolean paramBoolean, int paramInt, VersionInfoParcel paramVersionInfoParcel)
  {
    this.a = 4;
    this.b = null;
    this.c = paramzza;
    this.d = paramzzg;
    this.e = paramzzjp;
    this.f = null;
    this.g = null;
    this.h = paramBoolean;
    this.i = null;
    this.j = paramzzp;
    this.k = paramInt;
    this.l = 2;
    this.m = null;
    this.n = paramVersionInfoParcel;
    this.o = null;
    this.p = null;
    this.q = null;
  }
  
  public AdOverlayInfoParcel(zza paramzza, zzg paramzzg, zzdb paramzzdb, zzp paramzzp, zzjp paramzzjp, boolean paramBoolean, int paramInt, String paramString, VersionInfoParcel paramVersionInfoParcel, zzdh paramzzdh)
  {
    this.a = 4;
    this.b = null;
    this.c = paramzza;
    this.d = paramzzg;
    this.e = paramzzjp;
    this.f = paramzzdb;
    this.g = null;
    this.h = paramBoolean;
    this.i = null;
    this.j = paramzzp;
    this.k = paramInt;
    this.l = 3;
    this.m = paramString;
    this.n = paramVersionInfoParcel;
    this.o = paramzzdh;
    this.p = null;
    this.q = null;
  }
  
  public AdOverlayInfoParcel(zza paramzza, zzg paramzzg, zzdb paramzzdb, zzp paramzzp, zzjp paramzzjp, boolean paramBoolean, int paramInt, String paramString1, String paramString2, VersionInfoParcel paramVersionInfoParcel, zzdh paramzzdh)
  {
    this.a = 4;
    this.b = null;
    this.c = paramzza;
    this.d = paramzzg;
    this.e = paramzzjp;
    this.f = paramzzdb;
    this.g = paramString2;
    this.h = paramBoolean;
    this.i = paramString1;
    this.j = paramzzp;
    this.k = paramInt;
    this.l = 3;
    this.m = null;
    this.n = paramVersionInfoParcel;
    this.o = paramzzdh;
    this.p = null;
    this.q = null;
  }
  
  public AdOverlayInfoParcel(AdLauncherIntentInfoParcel paramAdLauncherIntentInfoParcel, zza paramzza, zzg paramzzg, zzp paramzzp, VersionInfoParcel paramVersionInfoParcel)
  {
    this.a = 4;
    this.b = paramAdLauncherIntentInfoParcel;
    this.c = paramzza;
    this.d = paramzzg;
    this.e = null;
    this.f = null;
    this.g = null;
    this.h = false;
    this.i = null;
    this.j = paramzzp;
    this.k = -1;
    this.l = 4;
    this.m = null;
    this.n = paramVersionInfoParcel;
    this.o = null;
    this.p = null;
    this.q = null;
  }
  
  public static AdOverlayInfoParcel a(Intent paramIntent)
  {
    try
    {
      paramIntent = paramIntent.getBundleExtra("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo");
      paramIntent.setClassLoader(AdOverlayInfoParcel.class.getClassLoader());
      paramIntent = (AdOverlayInfoParcel)paramIntent.getParcelable("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo");
      return paramIntent;
    }
    catch (Exception paramIntent) {}
    return null;
  }
  
  public static void a(Intent paramIntent, AdOverlayInfoParcel paramAdOverlayInfoParcel)
  {
    Bundle localBundle = new Bundle(1);
    localBundle.putParcelable("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo", paramAdOverlayInfoParcel);
    paramIntent.putExtra("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo", localBundle);
  }
  
  IBinder a()
  {
    return zze.a(this.c).asBinder();
  }
  
  IBinder b()
  {
    return zze.a(this.d).asBinder();
  }
  
  IBinder c()
  {
    return zze.a(this.e).asBinder();
  }
  
  IBinder d()
  {
    return zze.a(this.f).asBinder();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  IBinder e()
  {
    return zze.a(this.o).asBinder();
  }
  
  IBinder f()
  {
    return zze.a(this.j).asBinder();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */