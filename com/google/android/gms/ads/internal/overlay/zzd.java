package com.google.android.gms.ads.internal.overlay;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.Window;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.google.android.gms.ads.internal.InterstitialAdParameterParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzr;
import com.google.android.gms.internal.zzbp;
import com.google.android.gms.internal.zzbt;
import com.google.android.gms.internal.zzfv.zza;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzim;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzir;
import com.google.android.gms.internal.zzis;
import com.google.android.gms.internal.zziu;
import com.google.android.gms.internal.zzjp;
import com.google.android.gms.internal.zzjq;
import com.google.android.gms.internal.zzjq.zza;
import com.google.android.gms.internal.zzjr;
import java.util.Collections;
import java.util.Map;

@zzhb
public class zzd
  extends zzfv.zza
  implements zzs
{
  static final int a = Color.argb(0, 0, 0, 0);
  AdOverlayInfoParcel b;
  zzjp c;
  zzc d;
  zzo e;
  boolean f = false;
  FrameLayout g;
  WebChromeClient.CustomViewCallback h;
  boolean i = false;
  boolean j = false;
  RelativeLayout k;
  boolean l = false;
  int m = 0;
  zzl n;
  private final Activity o;
  private boolean p;
  private boolean q = false;
  private boolean r = true;
  
  public zzd(Activity paramActivity)
  {
    this.o = paramActivity;
    this.n = new zzq();
  }
  
  public void a()
  {
    this.m = 2;
    this.o.finish();
  }
  
  public void a(int paramInt)
  {
    this.o.setRequestedOrientation(paramInt);
  }
  
  public void a(Bundle paramBundle)
  {
    boolean bool = false;
    if (paramBundle != null) {
      bool = paramBundle.getBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", false);
    }
    this.i = bool;
    try
    {
      this.b = AdOverlayInfoParcel.a(this.o.getIntent());
      if (this.b != null) {
        break label71;
      }
      throw new zza("Could not get info for ad overlay.");
    }
    catch (zza paramBundle)
    {
      zzin.d(paramBundle.getMessage());
      this.m = 3;
      this.o.finish();
    }
    return;
    label71:
    if (this.b.n.d > 7500000) {
      this.m = 3;
    }
    if (this.o.getIntent() != null) {
      this.r = this.o.getIntent().getBooleanExtra("shouldCallOnOverlayOpened", true);
    }
    if (this.b.q != null)
    {
      this.j = this.b.q.b;
      label142:
      if ((((Boolean)zzbt.aE.c()).booleanValue()) && (this.j) && (this.b.q.d != null)) {
        new zzd(null).f();
      }
      if (paramBundle == null)
      {
        if ((this.b.d != null) && (this.r)) {
          this.b.d.d_();
        }
        if ((this.b.l != 1) && (this.b.c != null)) {
          this.b.c.e();
        }
      }
      this.k = new zzb(this.o, this.b.p);
      this.k.setId(1000);
      switch (this.b.l)
      {
      }
    }
    for (;;)
    {
      throw new zza("Could not determine ad overlay type.");
      this.j = false;
      break label142;
      b(false);
      return;
      this.d = new zzc(this.b.e);
      b(false);
      return;
      b(true);
      return;
      if (this.i)
      {
        this.m = 3;
        this.o.finish();
        return;
      }
      if (zzr.b().a(this.o, this.b.b, this.b.j)) {
        break;
      }
      this.m = 3;
      this.o.finish();
      return;
    }
  }
  
  public void a(View paramView, WebChromeClient.CustomViewCallback paramCustomViewCallback)
  {
    this.g = new FrameLayout(this.o);
    this.g.setBackgroundColor(-16777216);
    this.g.addView(paramView, -1, -1);
    this.o.setContentView(this.g);
    l();
    this.h = paramCustomViewCallback;
    this.f = true;
  }
  
  public void a(zzjp paramzzjp, Map<String, String> paramMap)
  {
    this.n.a(paramzzjp, paramMap);
  }
  
  public void a(boolean paramBoolean)
  {
    RelativeLayout.LayoutParams localLayoutParams;
    if (paramBoolean)
    {
      i1 = 50;
      this.e = new zzo(this.o, i1, this);
      localLayoutParams = new RelativeLayout.LayoutParams(-2, -2);
      localLayoutParams.addRule(10);
      if (!paramBoolean) {
        break label88;
      }
    }
    label88:
    for (int i1 = 11;; i1 = 9)
    {
      localLayoutParams.addRule(i1);
      this.e.a(paramBoolean, this.b.h);
      this.k.addView(this.e, localLayoutParams);
      return;
      i1 = 32;
      break;
    }
  }
  
  public void a(boolean paramBoolean1, boolean paramBoolean2)
  {
    if (this.e != null) {
      this.e.a(paramBoolean1, paramBoolean2);
    }
  }
  
  public void b()
  {
    if ((this.b != null) && (this.f)) {
      a(this.b.k);
    }
    if (this.g != null)
    {
      this.o.setContentView(this.k);
      l();
      this.g.removeAllViews();
      this.g = null;
    }
    if (this.h != null)
    {
      this.h.onCustomViewHidden();
      this.h = null;
    }
    this.f = false;
  }
  
  protected void b(int paramInt)
  {
    this.c.a(paramInt);
  }
  
  public void b(Bundle paramBundle)
  {
    paramBundle.putBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", this.i);
  }
  
  protected void b(boolean paramBoolean)
    throws zzd.zza
  {
    if (!this.p) {
      this.o.requestWindowFeature(1);
    }
    Object localObject = this.o.getWindow();
    if (localObject == null) {
      throw new zza("Invalid activity, no window available.");
    }
    if ((!this.j) || ((this.b.q != null) && (this.b.q.c))) {
      ((Window)localObject).setFlags(1024, 1024);
    }
    boolean bool2 = this.b.e.l().b();
    this.l = false;
    boolean bool1;
    if (bool2)
    {
      if (this.b.k != zzr.g().a()) {
        break label590;
      }
      if (this.o.getResources().getConfiguration().orientation == 1)
      {
        bool1 = true;
        this.l = bool1;
      }
    }
    else
    {
      label147:
      zzin.a("Delay onShow to next orientation change: " + this.l);
      a(this.b.k);
      if (zzr.g().a((Window)localObject)) {
        zzin.a("Hardware acceleration on the AdActivity window enabled.");
      }
      if (this.j) {
        break label638;
      }
      this.k.setBackgroundColor(-16777216);
      label217:
      this.o.setContentView(this.k);
      l();
      if (!paramBoolean) {
        break label705;
      }
      this.c = zzr.f().a(this.o, this.b.e.k(), true, bool2, null, this.b.n, null, this.b.e.h());
      this.c.l().a(null, null, this.b.f, this.b.j, true, this.b.o, null, this.b.e.l().a(), null);
      this.c.l().a(new zzjq.zza()
      {
        public void a(zzjp paramAnonymouszzjp, boolean paramAnonymousBoolean)
        {
          paramAnonymouszzjp.d();
        }
      });
      if (this.b.m == null) {
        break label651;
      }
      this.c.loadUrl(this.b.m);
      label384:
      if (this.b.e != null) {
        this.b.e.b(this);
      }
      label407:
      this.c.a(this);
      localObject = this.c.getParent();
      if ((localObject != null) && ((localObject instanceof ViewGroup))) {
        ((ViewGroup)localObject).removeView(this.c.b());
      }
      if (this.j) {
        this.c.setBackgroundColor(a);
      }
      this.k.addView(this.c.b(), -1, -1);
      if ((!paramBoolean) && (!this.l)) {
        p();
      }
      a(bool2);
      if (this.c.m()) {
        a(bool2, true);
      }
      localObject = this.c.h();
      if (localObject == null) {
        break label732;
      }
    }
    label590:
    label638:
    label651:
    label705:
    label732:
    for (localObject = ((com.google.android.gms.ads.internal.zzd)localObject).c;; localObject = null)
    {
      if (localObject == null) {
        break label738;
      }
      this.n = ((zzm)localObject).a(this.o, this.c, this.k);
      return;
      bool1 = false;
      break;
      if (this.b.k != zzr.g().b()) {
        break label147;
      }
      if (this.o.getResources().getConfiguration().orientation == 2) {}
      for (bool1 = true;; bool1 = false)
      {
        this.l = bool1;
        break;
      }
      this.k.setBackgroundColor(a);
      break label217;
      if (this.b.i != null)
      {
        this.c.loadDataWithBaseURL(this.b.g, this.b.i, "text/html", "UTF-8", null);
        break label384;
      }
      throw new zza("No URL or HTML to display in ad overlay.");
      this.c = this.b.e;
      this.c.a(this.o);
      break label407;
    }
    label738:
    zzin.d("Appstreaming controller is null.");
  }
  
  public void c()
  {
    this.m = 1;
    this.o.finish();
  }
  
  public void d()
  {
    this.m = 0;
  }
  
  public boolean e()
  {
    boolean bool1 = true;
    boolean bool2 = true;
    this.m = 0;
    if (this.c == null) {
      return bool2;
    }
    if ((this.c.t()) && (this.n.d())) {}
    for (;;)
    {
      bool2 = bool1;
      if (bool1) {
        break;
      }
      this.c.a("onbackblocked", Collections.emptyMap());
      return bool1;
      bool1 = false;
    }
  }
  
  public void f() {}
  
  public void g() {}
  
  public void h()
  {
    if ((this.b != null) && (this.b.l == 4))
    {
      if (this.i)
      {
        this.m = 3;
        this.o.finish();
      }
    }
    else
    {
      if (this.b.d != null) {
        this.b.d.g();
      }
      if ((this.c == null) || (this.c.r())) {
        break label107;
      }
      zzr.g().b(this.c);
    }
    for (;;)
    {
      this.n.b();
      return;
      this.i = true;
      break;
      label107:
      zzin.d("The webview does not exit. Ignoring action.");
    }
  }
  
  public void i()
  {
    this.n.a();
    b();
    if (this.b.d != null) {
      this.b.d.e_();
    }
    if ((this.c != null) && ((!this.o.isFinishing()) || (this.d == null))) {
      zzr.g().a(this.c);
    }
    n();
  }
  
  public void j()
  {
    n();
  }
  
  public void k()
  {
    if (this.c != null) {
      this.k.removeView(this.c.b());
    }
    n();
  }
  
  public void l()
  {
    this.p = true;
  }
  
  public void m()
  {
    this.k.removeView(this.e);
    a(true);
  }
  
  protected void n()
  {
    if ((!this.o.isFinishing()) || (this.q)) {
      return;
    }
    this.q = true;
    if (this.c != null)
    {
      b(this.m);
      this.k.removeView(this.c.b());
      if (this.d == null) {
        break label169;
      }
      this.c.a(this.d.d);
      this.c.a(false);
      this.d.c.addView(this.c.b(), this.d.a, this.d.b);
      this.d = null;
    }
    for (;;)
    {
      this.c = null;
      if ((this.b != null) && (this.b.d != null)) {
        this.b.d.c_();
      }
      this.n.c();
      return;
      label169:
      if (this.o.getApplicationContext() != null) {
        this.c.a(this.o.getApplicationContext());
      }
    }
  }
  
  public void o()
  {
    if (this.l)
    {
      this.l = false;
      p();
    }
  }
  
  protected void p()
  {
    this.c.d();
  }
  
  @zzhb
  private static final class zza
    extends Exception
  {
    public zza(String paramString)
    {
      super();
    }
  }
  
  @zzhb
  static final class zzb
    extends RelativeLayout
  {
    zziu a;
    
    public zzb(Context paramContext, String paramString)
    {
      super();
      this.a = new zziu(paramContext, paramString);
    }
    
    public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
    {
      this.a.a(paramMotionEvent);
      return false;
    }
  }
  
  @zzhb
  public static class zzc
  {
    public final int a;
    public final ViewGroup.LayoutParams b;
    public final ViewGroup c;
    public final Context d;
    
    public zzc(zzjp paramzzjp)
      throws zzd.zza
    {
      this.b = paramzzjp.getLayoutParams();
      ViewParent localViewParent = paramzzjp.getParent();
      this.d = paramzzjp.g();
      if ((localViewParent != null) && ((localViewParent instanceof ViewGroup)))
      {
        this.c = ((ViewGroup)localViewParent);
        this.a = this.c.indexOfChild(paramzzjp.b());
        this.c.removeView(paramzzjp.b());
        paramzzjp.a(true);
        return;
      }
      throw new zzd.zza("Could not get the parent of the WebView for an overlay.");
    }
  }
  
  @zzhb
  private class zzd
    extends zzim
  {
    private zzd() {}
    
    public void a()
    {
      final Object localObject = zzr.e().b(zzd.a(zzd.this), zzd.this.b.q.d);
      if (localObject != null)
      {
        localObject = zzr.g().a(zzd.a(zzd.this), (Bitmap)localObject, zzd.this.b.q.e, zzd.this.b.q.f);
        zzir.a.post(new Runnable()
        {
          public void run()
          {
            zzd.a(zzd.this).getWindow().setBackgroundDrawable(localObject);
          }
        });
      }
    }
    
    public void b() {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */