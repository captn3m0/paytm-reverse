package com.google.android.gms.ads.internal.overlay;

import android.os.Handler;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzir;

@zzhb
class zzu
  implements Runnable
{
  private zzk a;
  private boolean b = false;
  
  zzu(zzk paramzzk)
  {
    this.a = paramzzk;
  }
  
  public void a()
  {
    zzir.a.postDelayed(this, 250L);
  }
  
  public void cancel()
  {
    this.b = true;
    zzir.a.removeCallbacks(this);
  }
  
  public void run()
  {
    if (!this.b)
    {
      this.a.n();
      a();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */