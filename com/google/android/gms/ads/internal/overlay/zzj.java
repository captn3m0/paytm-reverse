package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.support.annotation.Nullable;
import com.google.android.gms.internal.zzbz;
import com.google.android.gms.internal.zzcb;
import com.google.android.gms.internal.zzjp;
import com.google.android.gms.internal.zzne;

public abstract class zzj
{
  @Nullable
  public abstract zzi a(Context paramContext, zzjp paramzzjp, int paramInt, zzcb paramzzcb, zzbz paramzzbz);
  
  protected boolean a(Context paramContext)
  {
    paramContext = paramContext.getApplicationInfo();
    return (zzne.d()) && ((paramContext == null) || (paramContext.targetSdkVersion >= 11));
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */