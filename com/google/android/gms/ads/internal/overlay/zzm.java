package com.google.android.gms.ads.internal.overlay;

import android.app.Activity;
import android.widget.RelativeLayout;
import com.google.android.gms.internal.zzjp;

public abstract interface zzm
{
  public abstract zzl a(Activity paramActivity, zzjp paramzzjp, RelativeLayout paramRelativeLayout);
  
  public abstract boolean a();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/overlay/zzm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */