package com.google.android.gms.ads.internal;

import android.net.Uri.Builder;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.internal.zzbp;
import com.google.android.gms.internal.zzbt;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzif.zza;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzir;
import com.google.android.gms.internal.zzjp;

@zzhb
public class zze
{
  private zza a;
  private boolean b;
  private boolean c;
  
  public zze()
  {
    this.c = ((Boolean)zzbt.i.c()).booleanValue();
  }
  
  public zze(boolean paramBoolean)
  {
    this.c = paramBoolean;
  }
  
  public void a()
  {
    this.b = true;
  }
  
  public void a(zza paramzza)
  {
    this.a = paramzza;
  }
  
  public void a(String paramString)
  {
    zzin.a("Action was blocked because no click was detected.");
    if (this.a != null) {
      this.a.a(paramString);
    }
  }
  
  public boolean b()
  {
    return (!this.c) || (this.b);
  }
  
  public static abstract interface zza
  {
    public abstract void a(String paramString);
  }
  
  @zzhb
  public static class zzb
    implements zze.zza
  {
    private final zzif.zza a;
    private final zzjp b;
    
    public zzb(zzif.zza paramzza, zzjp paramzzjp)
    {
      this.a = paramzza;
      this.b = paramzzjp;
    }
    
    public void a(String paramString)
    {
      zzin.a("An auto-clicking creative is blocked");
      Uri.Builder localBuilder = new Uri.Builder();
      localBuilder.scheme("https");
      localBuilder.path("//pagead2.googlesyndication.com/pagead/gen_204");
      localBuilder.appendQueryParameter("id", "gmob-apps-blocked-navigation");
      if (!TextUtils.isEmpty(paramString)) {
        localBuilder.appendQueryParameter("navigationURL", paramString);
      }
      if ((this.a != null) && (this.a.b != null) && (!TextUtils.isEmpty(this.a.b.o))) {
        localBuilder.appendQueryParameter("debugDialog", this.a.b.o);
      }
      zzr.e().a(this.b.getContext(), this.b.o().b, localBuilder.toString());
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */