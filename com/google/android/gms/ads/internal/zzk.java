package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.view.View;
import android.view.Window;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzax;
import com.google.android.gms.internal.zzbp;
import com.google.android.gms.internal.zzbt;
import com.google.android.gms.internal.zzcb;
import com.google.android.gms.internal.zzdj;
import com.google.android.gms.internal.zzdn;
import com.google.android.gms.internal.zzdn.zza;
import com.google.android.gms.internal.zzex;
import com.google.android.gms.internal.zzey;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzif;
import com.google.android.gms.internal.zzif.zza;
import com.google.android.gms.internal.zzim;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzir;
import com.google.android.gms.internal.zzis;
import com.google.android.gms.internal.zzjp;
import com.google.android.gms.internal.zzjq;
import com.google.android.gms.internal.zzjr;

@zzhb
public class zzk
  extends zzc
  implements zzdj, zzdn.zza
{
  protected transient boolean l = false;
  private boolean m;
  private float n;
  private String o = "background" + hashCode() + "." + "png";
  
  public zzk(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, zzex paramzzex, VersionInfoParcel paramVersionInfoParcel, zzd paramzzd)
  {
    super(paramContext, paramAdSizeParcel, paramString, paramzzex, paramVersionInfoParcel, paramzzd);
  }
  
  private void a(Bundle paramBundle)
  {
    zzr.e().b(this.f.c, this.f.e.b, "gmob-apps", paramBundle, false);
  }
  
  protected boolean C()
  {
    if (!(this.f.c instanceof Activity)) {}
    Window localWindow;
    do
    {
      return false;
      localWindow = ((Activity)this.f.c).getWindow();
    } while ((localWindow == null) || (localWindow.getDecorView() == null));
    Rect localRect1 = new Rect();
    Rect localRect2 = new Rect();
    localWindow.getDecorView().getGlobalVisibleRect(localRect1, null);
    localWindow.getDecorView().getWindowVisibleDisplayFrame(localRect2);
    if ((localRect1.bottom != 0) && (localRect2.bottom != 0) && (localRect1.top == localRect2.top)) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public void D()
  {
    new zza(this.o).f();
    if (this.f.e())
    {
      this.f.b();
      this.f.j = null;
      this.f.F = false;
      this.l = false;
    }
  }
  
  public void E()
  {
    if ((this.f.j != null) && (this.f.j.w != null)) {
      zzr.e().a(this.f.c, this.f.e.b, this.f.j.w);
    }
    r();
  }
  
  protected zzjp a(zzif.zza paramzza, zze paramzze)
  {
    zzjp localzzjp = zzr.f().a(this.f.c, this.f.i, false, false, this.f.d, this.f.e, this.a, this.i);
    localzzjp.l().a(this, null, this, this, ((Boolean)zzbt.V.c()).booleanValue(), this, this, paramzze, null);
    a(localzzjp);
    localzzjp.b(paramzza.a.w);
    zzdn.a(localzzjp, this);
    return localzzjp;
  }
  
  public void a(boolean paramBoolean, float paramFloat)
  {
    this.m = paramBoolean;
    this.n = paramFloat;
  }
  
  public boolean a(AdRequestParcel paramAdRequestParcel, zzcb paramzzcb)
  {
    if (this.f.j != null)
    {
      zzin.d("An interstitial is already loading. Aborting.");
      return false;
    }
    return super.a(paramAdRequestParcel, paramzzcb);
  }
  
  protected boolean a(AdRequestParcel paramAdRequestParcel, zzif paramzzif, boolean paramBoolean)
  {
    if ((this.f.e()) && (paramzzif.b != null)) {
      zzr.g().a(paramzzif.b);
    }
    return this.e.c();
  }
  
  public boolean a(zzif paramzzif1, zzif paramzzif2)
  {
    if (!super.a(paramzzif1, paramzzif2)) {
      return false;
    }
    if ((!this.f.e()) && (this.f.C != null) && (paramzzif2.j != null)) {
      this.h.a(this.f.i, paramzzif2, this.f.C);
    }
    return true;
  }
  
  public void b(RewardItemParcel paramRewardItemParcel)
  {
    RewardItemParcel localRewardItemParcel = paramRewardItemParcel;
    if (this.f.j != null)
    {
      if (this.f.j.x != null) {
        zzr.e().a(this.f.c, this.f.e.b, this.f.j.x);
      }
      localRewardItemParcel = paramRewardItemParcel;
      if (this.f.j.v != null) {
        localRewardItemParcel = this.f.j.v;
      }
    }
    a(localRewardItemParcel);
  }
  
  public void b(boolean paramBoolean)
  {
    this.f.F = paramBoolean;
  }
  
  public void d_()
  {
    y();
    super.d_();
  }
  
  public void f()
  {
    zzx.b("showInterstitial must be called on the main UI thread.");
    if (this.f.j == null)
    {
      zzin.d("The interstitial has not loaded.");
      return;
    }
    if (((Boolean)zzbt.an.c()).booleanValue()) {
      if (this.f.c.getApplicationContext() == null) {
        break label222;
      }
    }
    label222:
    for (String str = this.f.c.getApplicationContext().getPackageName();; localObject = this.f.c.getPackageName())
    {
      Bundle localBundle;
      if (!this.l)
      {
        zzin.d("It is not recommended to show an interstitial before onAdLoaded completes.");
        localBundle = new Bundle();
        localBundle.putString("appid", str);
        localBundle.putString("action", "show_interstitial_before_load_finish");
        a(localBundle);
      }
      if (!zzr.e().g(this.f.c))
      {
        zzin.d("It is not recommended to show an interstitial when app is not in foreground.");
        localBundle = new Bundle();
        localBundle.putString("appid", str);
        localBundle.putString("action", "show_interstitial_app_not_in_foreground");
        a(localBundle);
      }
      if (this.f.f()) {
        break;
      }
      if (!this.f.j.m) {
        break label236;
      }
      try
      {
        this.f.j.o.b();
        return;
      }
      catch (RemoteException localRemoteException)
      {
        zzin.d("Could not show interstitial.", localRemoteException);
        D();
        return;
      }
    }
    label236:
    if (this.f.j.b == null)
    {
      zzin.d("The interstitial failed to load.");
      return;
    }
    if (this.f.j.b.p())
    {
      zzin.d("The interstitial is already showing.");
      return;
    }
    this.f.j.b.a(true);
    if (this.f.j.j != null) {
      this.h.a(this.f.i, this.f.j);
    }
    if (this.f.F) {}
    for (Object localObject = zzr.e().h(this.f.c); (((Boolean)zzbt.aE.c()).booleanValue()) && (localObject != null); localObject = null)
    {
      new zzb((Bitmap)localObject, this.o).f();
      return;
    }
    localObject = new InterstitialAdParameterParcel(this.f.F, C(), null, false, 0.0F);
    int j = this.f.j.b.q();
    int i = j;
    if (j == -1) {
      i = this.f.j.g;
    }
    localObject = new AdOverlayInfoParcel(this, this, this, this.f.j.b, i, this.f.e, this.f.j.A, (InterstitialAdParameterParcel)localObject);
    zzr.c().a(this.f.c, (AdOverlayInfoParcel)localObject);
  }
  
  protected void n()
  {
    D();
    super.n();
  }
  
  protected void q()
  {
    super.q();
    this.l = true;
  }
  
  @zzhb
  private class zza
    extends zzim
  {
    private final String b;
    
    public zza(String paramString)
    {
      this.b = paramString;
    }
    
    public void a()
    {
      zzr.e().c(zzk.this.f.c, this.b);
    }
    
    public void b() {}
  }
  
  @zzhb
  private class zzb
    extends zzim
  {
    private final Bitmap b;
    private final String c;
    
    public zzb(Bitmap paramBitmap, String paramString)
    {
      this.b = paramBitmap;
      this.c = paramString;
    }
    
    public void a()
    {
      boolean bool1;
      boolean bool2;
      boolean bool3;
      if (zzk.this.f.F)
      {
        bool1 = zzr.e().a(zzk.this.f.c, this.b, this.c);
        bool2 = zzk.this.f.F;
        bool3 = zzk.this.C();
        if (!bool1) {
          break label221;
        }
      }
      label221:
      for (final Object localObject = this.c;; localObject = null)
      {
        localObject = new InterstitialAdParameterParcel(bool2, bool3, (String)localObject, zzk.a(zzk.this), zzk.b(zzk.this));
        int j = zzk.this.f.j.b.q();
        int i = j;
        if (j == -1) {
          i = zzk.this.f.j.g;
        }
        localObject = new AdOverlayInfoParcel(zzk.this, zzk.this, zzk.this, zzk.this.f.j.b, i, zzk.this.f.e, zzk.this.f.j.A, (InterstitialAdParameterParcel)localObject);
        zzir.a.post(new Runnable()
        {
          public void run()
          {
            zzr.c().a(zzk.this.f.c, localObject);
          }
        });
        return;
        bool1 = false;
        break;
      }
    }
    
    public void b() {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */