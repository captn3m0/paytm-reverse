package com.google.android.gms.ads.internal.purchase;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.google.android.gms.ads.internal.zzr;
import com.google.android.gms.internal.zzgg.zza;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzin;

@zzhb
public final class zzg
  extends zzgg.zza
  implements ServiceConnection
{
  zzb a;
  private boolean b = false;
  private Context c;
  private int d;
  private Intent e;
  private zzf f;
  private String g;
  
  public zzg(Context paramContext, String paramString, boolean paramBoolean, int paramInt, Intent paramIntent, zzf paramzzf)
  {
    this.g = paramString;
    this.d = paramInt;
    this.e = paramIntent;
    this.b = paramBoolean;
    this.c = paramContext;
    this.f = paramzzf;
  }
  
  public boolean a()
  {
    return this.b;
  }
  
  public String b()
  {
    return this.g;
  }
  
  public Intent c()
  {
    return this.e;
  }
  
  public int d()
  {
    return this.d;
  }
  
  public void e()
  {
    int i = zzr.o().a(this.e);
    if ((this.d != -1) || (i != 0)) {
      return;
    }
    this.a = new zzb(this.c);
    Intent localIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
    localIntent.setPackage("com.android.vending");
    com.google.android.gms.common.stats.zzb.a().a(this.c, localIntent, this, 1);
  }
  
  public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    zzin.c("In-app billing service connected.");
    this.a.a(paramIBinder);
    paramComponentName = zzr.o().b(this.e);
    paramComponentName = zzr.o().b(paramComponentName);
    if (paramComponentName == null) {
      return;
    }
    if (this.a.a(this.c.getPackageName(), paramComponentName) == 0) {
      zzh.a(this.c).a(this.f);
    }
    com.google.android.gms.common.stats.zzb.a().a(this.c, this);
    this.a.a();
  }
  
  public void onServiceDisconnected(ComponentName paramComponentName)
  {
    zzin.c("In-app billing service disconnected.");
    this.a.a();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/purchase/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */