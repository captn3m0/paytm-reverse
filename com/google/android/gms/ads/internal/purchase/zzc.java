package com.google.android.gms.ads.internal.purchase;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.SystemClock;
import com.google.android.gms.ads.internal.zzr;
import com.google.android.gms.internal.zzgh;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzim;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzir;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@zzhb
public class zzc
  extends zzim
  implements ServiceConnection
{
  private final Object a = new Object();
  private boolean b = false;
  private Context c;
  private zzgh d;
  private zzb e;
  private zzh f;
  private List<zzf> g = null;
  private zzk h;
  
  public zzc(Context paramContext, zzgh paramzzgh, zzk paramzzk)
  {
    this(paramContext, paramzzgh, paramzzk, new zzb(paramContext), zzh.a(paramContext.getApplicationContext()));
  }
  
  zzc(Context paramContext, zzgh paramzzgh, zzk paramzzk, zzb paramzzb, zzh paramzzh)
  {
    this.c = paramContext;
    this.d = paramzzgh;
    this.h = paramzzk;
    this.e = paramzzb;
    this.f = paramzzh;
    this.g = this.f.a(10L);
  }
  
  private void a(long paramLong)
  {
    do
    {
      if (!b(paramLong)) {
        zzin.e("Timeout waiting for pending transaction to be processed.");
      }
    } while (!this.b);
  }
  
  private boolean b(long paramLong)
  {
    paramLong = 60000L - (SystemClock.elapsedRealtime() - paramLong);
    if (paramLong <= 0L) {
      return false;
    }
    try
    {
      this.a.wait(paramLong);
      return true;
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;)
      {
        zzin.d("waitWithTimeout_lock interrupted");
      }
    }
  }
  
  public void a()
  {
    synchronized (this.a)
    {
      Intent localIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
      localIntent.setPackage("com.android.vending");
      com.google.android.gms.common.stats.zzb.a().a(this.c, localIntent, this, 1);
      a(SystemClock.elapsedRealtime());
      com.google.android.gms.common.stats.zzb.a().a(this.c, this);
      this.e.a();
      return;
    }
  }
  
  protected void a(final zzf paramzzf, String paramString1, String paramString2)
  {
    final Intent localIntent = new Intent();
    zzr.o();
    localIntent.putExtra("RESPONSE_CODE", 0);
    zzr.o();
    localIntent.putExtra("INAPP_PURCHASE_DATA", paramString1);
    zzr.o();
    localIntent.putExtra("INAPP_DATA_SIGNATURE", paramString2);
    zzir.a.post(new Runnable()
    {
      public void run()
      {
        try
        {
          if (zzc.a(zzc.this).a(paramzzf.b, -1, localIntent))
          {
            zzc.c(zzc.this).a(new zzg(zzc.b(zzc.this), paramzzf.c, true, -1, localIntent, paramzzf));
            return;
          }
          zzc.c(zzc.this).a(new zzg(zzc.b(zzc.this), paramzzf.c, false, -1, localIntent, paramzzf));
          return;
        }
        catch (RemoteException localRemoteException)
        {
          zzin.d("Fail to verify and dispatch pending transaction");
        }
      }
    });
  }
  
  public void b()
  {
    synchronized (this.a)
    {
      com.google.android.gms.common.stats.zzb.a().a(this.c, this);
      this.e.a();
      return;
    }
  }
  
  protected void c()
  {
    if (this.g.isEmpty()) {
      return;
    }
    HashMap localHashMap = new HashMap();
    Object localObject1 = this.g.iterator();
    Object localObject2;
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (zzf)((Iterator)localObject1).next();
      localHashMap.put(((zzf)localObject2).c, localObject2);
    }
    localObject1 = null;
    for (;;)
    {
      localObject1 = this.e.b(this.c.getPackageName(), (String)localObject1);
      if (localObject1 == null) {}
      do
      {
        do
        {
          localObject1 = localHashMap.keySet().iterator();
          while (((Iterator)localObject1).hasNext())
          {
            localObject2 = (String)((Iterator)localObject1).next();
            this.f.a((zzf)localHashMap.get(localObject2));
          }
          break;
        } while (zzr.o().a((Bundle)localObject1) != 0);
        localObject2 = ((Bundle)localObject1).getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
        ArrayList localArrayList1 = ((Bundle)localObject1).getStringArrayList("INAPP_PURCHASE_DATA_LIST");
        ArrayList localArrayList2 = ((Bundle)localObject1).getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
        localObject1 = ((Bundle)localObject1).getString("INAPP_CONTINUATION_TOKEN");
        int i = 0;
        while (i < ((ArrayList)localObject2).size())
        {
          if (localHashMap.containsKey(((ArrayList)localObject2).get(i)))
          {
            String str1 = (String)((ArrayList)localObject2).get(i);
            String str2 = (String)localArrayList1.get(i);
            String str3 = (String)localArrayList2.get(i);
            zzf localzzf = (zzf)localHashMap.get(str1);
            String str4 = zzr.o().a(str2);
            if (localzzf.b.equals(str4))
            {
              a(localzzf, str2, str3);
              localHashMap.remove(str1);
            }
          }
          i += 1;
        }
      } while ((localObject1 == null) || (localHashMap.isEmpty()));
    }
  }
  
  public void onServiceConnected(ComponentName arg1, IBinder paramIBinder)
  {
    synchronized (this.a)
    {
      this.e.a(paramIBinder);
      c();
      this.b = true;
      this.a.notify();
      return;
    }
  }
  
  public void onServiceDisconnected(ComponentName paramComponentName)
  {
    zzin.c("In-app billing service disconnected.");
    this.e.a();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/purchase/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */