package com.google.android.gms.ads.internal.purchase;

import android.content.Intent;
import com.google.android.gms.ads.internal.zzr;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzir;

@zzhb
public class zzk
{
  private final String a;
  
  public zzk(String paramString)
  {
    this.a = paramString;
  }
  
  public String a()
  {
    return zzr.e().c();
  }
  
  public boolean a(String paramString, int paramInt, Intent paramIntent)
  {
    if ((paramString == null) || (paramIntent == null)) {}
    String str;
    do
    {
      return false;
      str = zzr.o().b(paramIntent);
      paramIntent = zzr.o().c(paramIntent);
    } while ((str == null) || (paramIntent == null));
    if (!paramString.equals(zzr.o().a(str)))
    {
      zzin.d("Developer payload not match.");
      return false;
    }
    if ((this.a != null) && (!zzl.a(this.a, str, paramIntent)))
    {
      zzin.d("Fail to verify signature.");
      return false;
    }
    return true;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/purchase/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */