package com.google.android.gms.ads.internal.purchase;

import com.google.android.gms.internal.zzhb;

@zzhb
public final class zzf
{
  public long a;
  public final String b;
  public final String c;
  
  public zzf(long paramLong, String paramString1, String paramString2)
  {
    this.a = paramLong;
    this.c = paramString1;
    this.b = paramString2;
  }
  
  public zzf(String paramString1, String paramString2)
  {
    this(-1L, paramString1, paramString2);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/purchase/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */