package com.google.android.gms.ads.internal;

import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.ThinAdSizeParcel;
import com.google.android.gms.ads.internal.client.zzf;
import com.google.android.gms.ads.internal.client.zzn;
import com.google.android.gms.ads.internal.client.zzu.zza;
import com.google.android.gms.ads.internal.client.zzw;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.request.zza.zza;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.internal.zzax;
import com.google.android.gms.internal.zzbc;
import com.google.android.gms.internal.zzbf;
import com.google.android.gms.internal.zzbp;
import com.google.android.gms.internal.zzbt;
import com.google.android.gms.internal.zzbv;
import com.google.android.gms.internal.zzbz;
import com.google.android.gms.internal.zzcb;
import com.google.android.gms.internal.zzcf;
import com.google.android.gms.internal.zzdb;
import com.google.android.gms.internal.zzgd;
import com.google.android.gms.internal.zzgh;
import com.google.android.gms.internal.zzgr.zza;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzhr;
import com.google.android.gms.internal.zzif;
import com.google.android.gms.internal.zzif.zza;
import com.google.android.gms.internal.zzig;
import com.google.android.gms.internal.zzih;
import com.google.android.gms.internal.zzij;
import com.google.android.gms.internal.zzik;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzir;
import com.google.android.gms.internal.zzis;
import com.google.android.gms.internal.zzjp;
import com.google.android.gms.internal.zzjq;
import java.util.HashSet;

@zzhb
public abstract class zza
  extends zzu.zza
  implements com.google.android.gms.ads.internal.client.zza, com.google.android.gms.ads.internal.overlay.zzp, zza.zza, zzdb, zzgr.zza, zzij
{
  protected zzcb a;
  protected zzbz b;
  protected zzbz c;
  protected boolean d = false;
  protected final zzq e;
  protected final zzs f;
  protected transient AdRequestParcel g;
  protected final zzax h;
  protected final zzd i;
  
  zza(zzs paramzzs, zzq paramzzq, zzd paramzzd)
  {
    this.f = paramzzs;
    if (paramzzq != null) {}
    for (;;)
    {
      this.e = paramzzq;
      this.i = paramzzd;
      zzr.e().b(this.f.c);
      zzr.h().a(this.f.c, this.f.e);
      this.h = zzr.h().k();
      return;
      paramzzq = new zzq(this);
    }
  }
  
  private AdRequestParcel d(AdRequestParcel paramAdRequestParcel)
  {
    AdRequestParcel localAdRequestParcel = paramAdRequestParcel;
    if (com.google.android.gms.common.zze.j(this.f.c))
    {
      localAdRequestParcel = paramAdRequestParcel;
      if (paramAdRequestParcel.k != null) {
        localAdRequestParcel = new zzf(paramAdRequestParcel).a(null).a();
      }
    }
    return localAdRequestParcel;
  }
  
  Bundle a(zzbf paramzzbf)
  {
    Object localObject2 = null;
    if (paramzzbf == null) {}
    label145:
    for (;;)
    {
      return (Bundle)localObject2;
      if (paramzzbf.f()) {
        paramzzbf.d();
      }
      paramzzbf = paramzzbf.c();
      Object localObject3;
      String str;
      Object localObject1;
      if (paramzzbf != null)
      {
        localObject3 = paramzzbf.b();
        str = paramzzbf.c();
        zzin.a("In AdManager: loadAd, " + paramzzbf.toString());
        paramzzbf = str;
        localObject1 = localObject3;
        if (localObject3 != null)
        {
          zzr.h().a((String)localObject3);
          localObject1 = localObject3;
        }
      }
      for (paramzzbf = str;; paramzzbf = null)
      {
        if (localObject1 == null) {
          break label145;
        }
        localObject3 = new Bundle(1);
        ((Bundle)localObject3).putString("fingerprint", (String)localObject1);
        localObject2 = localObject3;
        if (((String)localObject1).equals(paramzzbf)) {
          break;
        }
        ((Bundle)localObject3).putString("v_fp", paramzzbf);
        return (Bundle)localObject3;
        localObject1 = zzr.h().i();
      }
    }
  }
  
  public com.google.android.gms.dynamic.zzd a()
  {
    com.google.android.gms.common.internal.zzx.b("getAdFrame must be called on the main UI thread.");
    return com.google.android.gms.dynamic.zze.a(this.f.f);
  }
  
  protected void a(int paramInt)
  {
    zzin.d("Failed to load ad: " + paramInt);
    this.d = false;
    if (this.f.n != null) {}
    try
    {
      this.f.n.a(paramInt);
      if (this.f.y == null) {}
    }
    catch (RemoteException localRemoteException1)
    {
      for (;;)
      {
        try
        {
          this.f.y.a(paramInt);
          return;
        }
        catch (RemoteException localRemoteException2)
        {
          zzin.d("Could not call RewardedVideoAdListener.onRewardedVideoAdFailedToLoad().", localRemoteException2);
        }
        localRemoteException1 = localRemoteException1;
        zzin.d("Could not call AdListener.onAdFailedToLoad().", localRemoteException1);
      }
    }
  }
  
  protected void a(View paramView)
  {
    this.f.f.addView(paramView, zzr.g().d());
  }
  
  public void a(AdSizeParcel paramAdSizeParcel)
  {
    com.google.android.gms.common.internal.zzx.b("setAdSize must be called on the main UI thread.");
    this.f.i = paramAdSizeParcel;
    if ((this.f.j != null) && (this.f.j.b != null) && (this.f.D == 0)) {
      this.f.j.b.a(paramAdSizeParcel);
    }
    if (this.f.f == null) {
      return;
    }
    if (this.f.f.getChildCount() > 1) {
      this.f.f.removeView(this.f.f.getNextView());
    }
    this.f.f.setMinimumWidth(paramAdSizeParcel.g);
    this.f.f.setMinimumHeight(paramAdSizeParcel.d);
    this.f.f.requestLayout();
  }
  
  public void a(com.google.android.gms.ads.internal.client.zzp paramzzp)
  {
    com.google.android.gms.common.internal.zzx.b("setAdListener must be called on the main UI thread.");
    this.f.m = paramzzp;
  }
  
  public void a(com.google.android.gms.ads.internal.client.zzq paramzzq)
  {
    com.google.android.gms.common.internal.zzx.b("setAdListener must be called on the main UI thread.");
    this.f.n = paramzzq;
  }
  
  public void a(zzw paramzzw)
  {
    com.google.android.gms.common.internal.zzx.b("setAppEventListener must be called on the main UI thread.");
    this.f.o = paramzzw;
  }
  
  public void a(com.google.android.gms.ads.internal.client.zzx paramzzx)
  {
    com.google.android.gms.common.internal.zzx.b("setCorrelationIdProvider must be called on the main UI thread");
    this.f.p = paramzzx;
  }
  
  public void a(com.google.android.gms.ads.internal.reward.client.zzd paramzzd)
  {
    com.google.android.gms.common.internal.zzx.b("setRewardedVideoAdListener can only be called from the UI thread.");
    this.f.y = paramzzd;
  }
  
  protected void a(RewardItemParcel paramRewardItemParcel)
  {
    if (this.f.y == null) {
      return;
    }
    String str = "";
    int j = 0;
    if (paramRewardItemParcel != null) {}
    try
    {
      str = paramRewardItemParcel.b;
      j = paramRewardItemParcel.c;
      this.f.y.a(new zzhr(str, j));
      return;
    }
    catch (RemoteException paramRewardItemParcel)
    {
      zzin.d("Could not call RewardedVideoAdListener.onRewarded().", paramRewardItemParcel);
    }
  }
  
  public void a(zzcf paramzzcf)
  {
    throw new IllegalStateException("setOnCustomRenderedAdLoadedListener is not supported for current ad type");
  }
  
  public void a(zzgd paramzzgd)
  {
    throw new IllegalStateException("setInAppPurchaseListener is not supported for current ad type");
  }
  
  public void a(zzgh paramzzgh, String paramString)
  {
    throw new IllegalStateException("setPlayStorePurchaseParams is not supported for current ad type");
  }
  
  public void a(zzif.zza paramzza)
  {
    if ((paramzza.b.n != -1L) && (!TextUtils.isEmpty(paramzza.b.z)))
    {
      long l = b(paramzza.b.z);
      if (l != -1L)
      {
        zzbz localzzbz = this.a.a(l + paramzza.b.n);
        this.a.a(localzzbz, new String[] { "stc" });
      }
    }
    this.a.a(paramzza.b.z);
    this.a.a(this.b, new String[] { "arf" });
    this.c = this.a.a();
    this.a.a("gqi", paramzza.b.A);
    this.f.g = null;
    this.f.k = paramzza;
    a(paramzza, this.a);
  }
  
  protected abstract void a(zzif.zza paramzza, zzcb paramzzcb);
  
  public void a(String paramString)
  {
    com.google.android.gms.common.internal.zzx.b("setUserId must be called on the main UI thread.");
    this.f.a(paramString);
  }
  
  public void a(String paramString1, String paramString2)
  {
    if (this.f.o != null) {}
    try
    {
      this.f.o.a(paramString1, paramString2);
      return;
    }
    catch (RemoteException paramString1)
    {
      zzin.d("Could not call the AppEventListener.", paramString1);
    }
  }
  
  public void a(HashSet<zzig> paramHashSet)
  {
    this.f.a(paramHashSet);
  }
  
  public void a(boolean paramBoolean)
  {
    throw new UnsupportedOperationException("Attempt to call setManualImpressionsEnabled for an unsupported ad type.");
  }
  
  public boolean a(AdRequestParcel paramAdRequestParcel)
  {
    com.google.android.gms.common.internal.zzx.b("loadAd must be called on the main UI thread.");
    paramAdRequestParcel = d(paramAdRequestParcel);
    if ((this.f.g != null) || (this.f.h != null))
    {
      if (this.g != null) {
        zzin.d("Aborting last ad request since another ad request is already in progress. The current request object will still be cached for future refreshes.");
      }
      for (;;)
      {
        this.g = paramAdRequestParcel;
        return false;
        zzin.d("Loading already in progress, saving this object for future refreshes.");
      }
    }
    zzin.c("Starting ad request.");
    m();
    this.b = this.a.a();
    if (!paramAdRequestParcel.f) {
      zzin.c("Use AdRequest.Builder.addTestDevice(\"" + zzn.a().a(this.f.c) + "\") to get test ads on this device.");
    }
    this.d = a(paramAdRequestParcel, this.a);
    return this.d;
  }
  
  protected abstract boolean a(AdRequestParcel paramAdRequestParcel, zzcb paramzzcb);
  
  boolean a(zzif paramzzif)
  {
    return false;
  }
  
  protected abstract boolean a(zzif paramzzif1, zzif paramzzif2);
  
  long b(@NonNull String paramString)
  {
    int m = paramString.indexOf("ufe");
    int k = paramString.indexOf(',', m);
    int j = k;
    if (k == -1) {
      j = paramString.length();
    }
    try
    {
      long l = Long.parseLong(paramString.substring(m + 4, j));
      return l;
    }
    catch (IndexOutOfBoundsException paramString)
    {
      zzin.d("Invalid index for Url fetch time in CSI latency info.");
      return -1L;
    }
    catch (NumberFormatException paramString)
    {
      for (;;)
      {
        zzin.d("Cannot find valid format of Url fetch time in CSI latency info.");
      }
    }
  }
  
  public void b()
  {
    com.google.android.gms.common.internal.zzx.b("destroy must be called on the main UI thread.");
    this.e.cancel();
    this.h.c(this.f.j);
    this.f.k();
  }
  
  public void b(zzif paramzzif)
  {
    this.a.a(this.c, new String[] { "awr" });
    this.f.h = null;
    if ((paramzzif.d != -2) && (paramzzif.d != 3)) {
      zzr.h().a(this.f.a());
    }
    if (paramzzif.d == -1) {
      this.d = false;
    }
    do
    {
      return;
      if (a(paramzzif)) {
        zzin.a("Ad refresh scheduled.");
      }
      if (paramzzif.d != -2)
      {
        a(paramzzif.d);
        return;
      }
      if (this.f.B == null) {
        this.f.B = new zzik(this.f.b);
      }
      this.h.b(this.f.j);
    } while (!a(this.f.j, paramzzif));
    this.f.j = paramzzif;
    this.f.j();
    zzcb localzzcb = this.a;
    if (this.f.j.a())
    {
      paramzzif = "1";
      label203:
      localzzcb.a("is_mraid", paramzzif);
      localzzcb = this.a;
      if (!this.f.j.m) {
        break label377;
      }
      paramzzif = "1";
      label233:
      localzzcb.a("is_mediation", paramzzif);
      if ((this.f.j.b != null) && (this.f.j.b.l() != null))
      {
        localzzcb = this.a;
        if (!this.f.j.b.l().c()) {
          break label384;
        }
      }
    }
    label377:
    label384:
    for (paramzzif = "1";; paramzzif = "0")
    {
      localzzcb.a("is_video", paramzzif);
      this.a.a(this.b, new String[] { "ttc" });
      if (zzr.h().e() != null) {
        zzr.h().e().a(this.a);
      }
      if (!this.f.e()) {
        break;
      }
      q();
      return;
      paramzzif = "0";
      break label203;
      paramzzif = "0";
      break label233;
    }
  }
  
  protected boolean b(AdRequestParcel paramAdRequestParcel)
  {
    paramAdRequestParcel = this.f.f.getParent();
    return ((paramAdRequestParcel instanceof View)) && (((View)paramAdRequestParcel).isShown()) && (zzr.e().a());
  }
  
  public void b_()
  {
    com.google.android.gms.common.internal.zzx.b("resume must be called on the main UI thread.");
  }
  
  public void c(AdRequestParcel paramAdRequestParcel)
  {
    if (b(paramAdRequestParcel))
    {
      a(paramAdRequestParcel);
      return;
    }
    zzin.c("Ad is not visible. Not refreshing ad.");
    this.e.a(paramAdRequestParcel);
  }
  
  protected void c(zzif paramzzif)
  {
    if (paramzzif == null) {
      zzin.d("Ad state was null when trying to ping impression URLs.");
    }
    do
    {
      return;
      zzin.a("Pinging Impression URLs.");
      this.f.l.a();
    } while ((paramzzif.e == null) || (paramzzif.C));
    zzr.e().a(this.f.c, this.f.e.b, paramzzif.e);
    paramzzif.C = true;
  }
  
  public boolean c()
  {
    com.google.android.gms.common.internal.zzx.b("isLoaded must be called on the main UI thread.");
    return (this.f.g == null) && (this.f.h == null) && (this.f.j != null);
  }
  
  public void d()
  {
    com.google.android.gms.common.internal.zzx.b("pause must be called on the main UI thread.");
  }
  
  public void e()
  {
    if (this.f.j == null) {
      zzin.d("Ad state was null when trying to ping click URLs.");
    }
    do
    {
      return;
      zzin.a("Pinging click URLs.");
      this.f.l.b();
      if (this.f.j.c != null) {
        zzr.e().a(this.f.c, this.f.e.b, this.f.j.c);
      }
    } while (this.f.m == null);
    try
    {
      this.f.m.a();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzin.d("Could not notify onAdClicked event.", localRemoteException);
    }
  }
  
  public void f_()
  {
    com.google.android.gms.common.internal.zzx.b("stopLoading must be called on the main UI thread.");
    this.d = false;
    this.f.a(true);
  }
  
  public void h()
  {
    com.google.android.gms.common.internal.zzx.b("recordManualImpression must be called on the main UI thread.");
    if (this.f.j == null) {
      zzin.d("Ad state was null when trying to ping manual tracking URLs.");
    }
    do
    {
      return;
      zzin.a("Pinging manual tracking URLs.");
    } while ((this.f.j.f == null) || (this.f.j.D));
    zzr.e().a(this.f.c, this.f.e.b, this.f.j.f);
    this.f.j.D = true;
  }
  
  public AdSizeParcel i()
  {
    com.google.android.gms.common.internal.zzx.b("getAdSize must be called on the main UI thread.");
    if (this.f.i == null) {
      return null;
    }
    return new ThinAdSizeParcel(this.f.i);
  }
  
  public boolean k()
  {
    return this.d;
  }
  
  public void l()
  {
    o();
  }
  
  void m()
  {
    this.a = new zzcb(((Boolean)zzbt.G.c()).booleanValue(), "load_ad", this.f.i.b);
    this.b = new zzbz(-1L, null, null);
    this.c = new zzbz(-1L, null, null);
  }
  
  protected void n()
  {
    zzin.c("Ad closing.");
    if (this.f.n != null) {}
    try
    {
      this.f.n.a();
      if (this.f.y == null) {}
    }
    catch (RemoteException localRemoteException1)
    {
      for (;;)
      {
        try
        {
          this.f.y.d();
          return;
        }
        catch (RemoteException localRemoteException2)
        {
          zzin.d("Could not call RewardedVideoAdListener.onRewardedVideoAdClosed().", localRemoteException2);
        }
        localRemoteException1 = localRemoteException1;
        zzin.d("Could not call AdListener.onAdClosed().", localRemoteException1);
      }
    }
  }
  
  protected void o()
  {
    zzin.c("Ad leaving application.");
    if (this.f.n != null) {}
    try
    {
      this.f.n.b();
      if (this.f.y == null) {}
    }
    catch (RemoteException localRemoteException1)
    {
      for (;;)
      {
        try
        {
          this.f.y.e();
          return;
        }
        catch (RemoteException localRemoteException2)
        {
          zzin.d("Could not call  RewardedVideoAdListener.onRewardedVideoAdLeftApplication().", localRemoteException2);
        }
        localRemoteException1 = localRemoteException1;
        zzin.d("Could not call AdListener.onAdLeftApplication().", localRemoteException1);
      }
    }
  }
  
  protected void p()
  {
    zzin.c("Ad opening.");
    if (this.f.n != null) {}
    try
    {
      this.f.n.d();
      if (this.f.y == null) {}
    }
    catch (RemoteException localRemoteException1)
    {
      for (;;)
      {
        try
        {
          this.f.y.b();
          return;
        }
        catch (RemoteException localRemoteException2)
        {
          zzin.d("Could not call RewardedVideoAdListener.onRewardedVideoAdOpened().", localRemoteException2);
        }
        localRemoteException1 = localRemoteException1;
        zzin.d("Could not call AdListener.onAdOpened().", localRemoteException1);
      }
    }
  }
  
  protected void q()
  {
    zzin.c("Ad finished loading.");
    this.d = false;
    if (this.f.n != null) {}
    try
    {
      this.f.n.c();
      if (this.f.y == null) {}
    }
    catch (RemoteException localRemoteException1)
    {
      for (;;)
      {
        try
        {
          this.f.y.a();
          return;
        }
        catch (RemoteException localRemoteException2)
        {
          zzin.d("Could not call RewardedVideoAdListener.onRewardedVideoAdLoaded().", localRemoteException2);
        }
        localRemoteException1 = localRemoteException1;
        zzin.d("Could not call AdListener.onAdLoaded().", localRemoteException1);
      }
    }
  }
  
  protected void r()
  {
    if (this.f.y == null) {
      return;
    }
    try
    {
      this.f.y.c();
      return;
    }
    catch (RemoteException localRemoteException)
    {
      zzin.d("Could not call RewardedVideoAdListener.onVideoStarted().", localRemoteException);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */