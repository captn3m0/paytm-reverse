package com.google.android.gms.ads.internal;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import com.google.android.gms.ads.internal.formats.zzd;
import com.google.android.gms.internal.zzch;
import com.google.android.gms.internal.zzch.zza;
import com.google.android.gms.internal.zzdf;
import com.google.android.gms.internal.zzen;
import com.google.android.gms.internal.zzes;
import com.google.android.gms.internal.zzey;
import com.google.android.gms.internal.zzfb;
import com.google.android.gms.internal.zzfc;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzif;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzjp;
import com.google.android.gms.internal.zzjq;
import com.google.android.gms.internal.zzjq.zza;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzhb
public class zzm
{
  public static View a(zzif paramzzif)
  {
    if (paramzzif == null)
    {
      zzin.b("AdState is null");
      return null;
    }
    if (b(paramzzif)) {
      return paramzzif.b.b();
    }
    try
    {
      paramzzif = paramzzif.o.a();
      if (paramzzif == null)
      {
        zzin.d("View in mediation adapter is null.");
        return null;
      }
      paramzzif = (View)com.google.android.gms.dynamic.zze.a(paramzzif);
      return paramzzif;
    }
    catch (RemoteException paramzzif)
    {
      zzin.d("Could not get View from mediation adapter.", paramzzif);
    }
    return null;
  }
  
  private static zzd a(zzfb paramzzfb)
    throws RemoteException
  {
    return new zzd(paramzzfb.a(), paramzzfb.b(), paramzzfb.c(), paramzzfb.d(), paramzzfb.e(), paramzzfb.f(), paramzzfb.g(), paramzzfb.h(), null, paramzzfb.l());
  }
  
  private static com.google.android.gms.ads.internal.formats.zze a(zzfc paramzzfc)
    throws RemoteException
  {
    return new com.google.android.gms.ads.internal.formats.zze(paramzzfc.a(), paramzzfc.b(), paramzzfc.c(), paramzzfc.d(), paramzzfc.e(), paramzzfc.f(), null, paramzzfc.j());
  }
  
  static zzdf a(zzfb paramzzfb, final zzfc paramzzfc, final zzf.zza paramzza)
  {
    new zzdf()
    {
      public void a(zzjp paramAnonymouszzjp, Map<String, String> paramAnonymousMap)
      {
        paramAnonymousMap = paramAnonymouszzjp.b();
        if (paramAnonymousMap == null) {}
        do
        {
          return;
          try
          {
            if (this.a == null) {
              continue;
            }
            if (!this.a.k())
            {
              this.a.a(com.google.android.gms.dynamic.zze.a(paramAnonymousMap));
              paramzza.onClick();
              return;
            }
          }
          catch (RemoteException paramAnonymouszzjp)
          {
            zzin.d("Unable to call handleClick on mapper", paramAnonymouszzjp);
            return;
          }
          zzm.a(paramAnonymouszzjp);
          return;
        } while (paramzzfc == null);
        if (!paramzzfc.i())
        {
          paramzzfc.a(com.google.android.gms.dynamic.zze.a(paramAnonymousMap));
          paramzza.onClick();
          return;
        }
        zzm.a(paramAnonymouszzjp);
      }
    };
  }
  
  static zzdf a(CountDownLatch paramCountDownLatch)
  {
    new zzdf()
    {
      public void a(zzjp paramAnonymouszzjp, Map<String, String> paramAnonymousMap)
      {
        this.a.countDown();
        paramAnonymouszzjp = paramAnonymouszzjp.b();
        if (paramAnonymouszzjp == null) {
          return;
        }
        paramAnonymouszzjp.setVisibility(0);
      }
    };
  }
  
  private static String a(Bitmap paramBitmap)
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    if (paramBitmap == null)
    {
      zzin.d("Bitmap is null. Returning empty string");
      return "";
    }
    paramBitmap.compress(Bitmap.CompressFormat.PNG, 100, localByteArrayOutputStream);
    paramBitmap = Base64.encodeToString(localByteArrayOutputStream.toByteArray(), 0);
    return "data:image/png;base64," + paramBitmap;
  }
  
  static String a(zzch paramzzch)
  {
    if (paramzzch == null)
    {
      zzin.d("Image is null. Returning empty string");
      return "";
    }
    try
    {
      Object localObject = paramzzch.b();
      if (localObject != null)
      {
        localObject = ((Uri)localObject).toString();
        return (String)localObject;
      }
    }
    catch (RemoteException localRemoteException)
    {
      zzin.d("Unable to get image uri. Trying data uri next");
    }
    return b(paramzzch);
  }
  
  public static void a(zzif paramzzif, zzf.zza paramzza)
  {
    zzjp localzzjp;
    View localView;
    if (b(paramzzif))
    {
      localzzjp = paramzzif.b;
      localView = localzzjp.b();
      if (localView == null) {
        zzin.d("AdWebView is null");
      }
    }
    else
    {
      return;
    }
    List localList;
    try
    {
      localList = paramzzif.n.n;
      if ((localList == null) || (localList.isEmpty()))
      {
        zzin.d("No template ids present in mediation response");
        return;
      }
    }
    catch (RemoteException paramzzif)
    {
      zzin.d("Error occurred while recording impression and registering for clicks", paramzzif);
      return;
    }
    zzfb localzzfb = paramzzif.o.h();
    paramzzif = paramzzif.o.i();
    if ((localList.contains("2")) && (localzzfb != null))
    {
      localzzfb.b(com.google.android.gms.dynamic.zze.a(localView));
      if (!localzzfb.j()) {
        localzzfb.i();
      }
      localzzjp.l().a("/nativeExpressViewClicked", a(localzzfb, null, paramzza));
      return;
    }
    if ((localList.contains("1")) && (paramzzif != null))
    {
      paramzzif.b(com.google.android.gms.dynamic.zze.a(localView));
      if (!paramzzif.h()) {
        paramzzif.g();
      }
      localzzjp.l().a("/nativeExpressViewClicked", a(null, paramzzif, paramzza));
      return;
    }
    zzin.d("No matching template id and mapper");
  }
  
  private static void a(final zzjp paramzzjp, zzd paramzzd, final String paramString)
  {
    paramzzjp.l().a(new zzjq.zza()
    {
      public void a(zzjp paramAnonymouszzjp, boolean paramAnonymousBoolean)
      {
        try
        {
          paramAnonymouszzjp = new JSONObject();
          paramAnonymouszzjp.put("headline", this.a.a());
          paramAnonymouszzjp.put("body", this.a.c());
          paramAnonymouszzjp.put("call_to_action", this.a.e());
          paramAnonymouszzjp.put("price", this.a.h());
          paramAnonymouszzjp.put("star_rating", String.valueOf(this.a.f()));
          paramAnonymouszzjp.put("store", this.a.g());
          paramAnonymouszzjp.put("icon", zzm.a(this.a.d()));
          localObject1 = new JSONArray();
          Object localObject2 = this.a.b();
          if (localObject2 != null)
          {
            localObject2 = ((List)localObject2).iterator();
            while (((Iterator)localObject2).hasNext()) {
              ((JSONArray)localObject1).put(zzm.a(zzm.a(((Iterator)localObject2).next())));
            }
          }
          paramAnonymouszzjp.put("images", localObject1);
        }
        catch (JSONException paramAnonymouszzjp)
        {
          zzin.d("Exception occurred when loading assets", paramAnonymouszzjp);
          return;
        }
        paramAnonymouszzjp.put("extras", zzm.a(this.a.m(), paramString));
        Object localObject1 = new JSONObject();
        ((JSONObject)localObject1).put("assets", paramAnonymouszzjp);
        ((JSONObject)localObject1).put("template_id", "2");
        paramzzjp.a("google.afma.nativeExpressAds.loadAssets", (JSONObject)localObject1);
      }
    });
  }
  
  private static void a(final zzjp paramzzjp, com.google.android.gms.ads.internal.formats.zze paramzze, final String paramString)
  {
    paramzzjp.l().a(new zzjq.zza()
    {
      public void a(zzjp paramAnonymouszzjp, boolean paramAnonymousBoolean)
      {
        try
        {
          paramAnonymouszzjp = new JSONObject();
          paramAnonymouszzjp.put("headline", this.a.a());
          paramAnonymouszzjp.put("body", this.a.c());
          paramAnonymouszzjp.put("call_to_action", this.a.e());
          paramAnonymouszzjp.put("advertiser", this.a.f());
          paramAnonymouszzjp.put("logo", zzm.a(this.a.d()));
          localObject1 = new JSONArray();
          Object localObject2 = this.a.b();
          if (localObject2 != null)
          {
            localObject2 = ((List)localObject2).iterator();
            while (((Iterator)localObject2).hasNext()) {
              ((JSONArray)localObject1).put(zzm.a(zzm.a(((Iterator)localObject2).next())));
            }
          }
          paramAnonymouszzjp.put("images", localObject1);
        }
        catch (JSONException paramAnonymouszzjp)
        {
          zzin.d("Exception occurred when loading assets", paramAnonymouszzjp);
          return;
        }
        paramAnonymouszzjp.put("extras", zzm.a(this.a.h(), paramString));
        Object localObject1 = new JSONObject();
        ((JSONObject)localObject1).put("assets", paramAnonymouszzjp);
        ((JSONObject)localObject1).put("template_id", "1");
        paramzzjp.a("google.afma.nativeExpressAds.loadAssets", (JSONObject)localObject1);
      }
    });
  }
  
  private static void a(zzjp paramzzjp, CountDownLatch paramCountDownLatch)
  {
    paramzzjp.l().a("/nativeExpressAssetsLoaded", a(paramCountDownLatch));
    paramzzjp.l().a("/nativeExpressAssetsLoadingFailed", b(paramCountDownLatch));
  }
  
  public static boolean a(zzjp paramzzjp, zzes paramzzes, CountDownLatch paramCountDownLatch)
  {
    boolean bool1 = false;
    try
    {
      boolean bool2 = b(paramzzjp, paramzzes, paramCountDownLatch);
      bool1 = bool2;
    }
    catch (RemoteException paramzzjp)
    {
      for (;;)
      {
        zzin.d("Unable to invoke load assets", paramzzjp);
      }
    }
    catch (RuntimeException paramzzjp)
    {
      paramCountDownLatch.countDown();
      throw paramzzjp;
    }
    if (!bool1) {
      paramCountDownLatch.countDown();
    }
    return bool1;
  }
  
  private static zzch b(Object paramObject)
  {
    if ((paramObject instanceof IBinder)) {
      return zzch.zza.a((IBinder)paramObject);
    }
    return null;
  }
  
  static zzdf b(CountDownLatch paramCountDownLatch)
  {
    new zzdf()
    {
      public void a(zzjp paramAnonymouszzjp, Map<String, String> paramAnonymousMap)
      {
        zzin.d("Adapter returned an ad, but assets substitution failed");
        this.a.countDown();
        paramAnonymouszzjp.destroy();
      }
    };
  }
  
  private static String b(zzch paramzzch)
  {
    try
    {
      paramzzch = paramzzch.a();
      if (paramzzch == null)
      {
        zzin.d("Drawable is null. Returning empty string");
        return "";
      }
      paramzzch = (Drawable)com.google.android.gms.dynamic.zze.a(paramzzch);
      if (!(paramzzch instanceof BitmapDrawable))
      {
        zzin.d("Drawable is not an instance of BitmapDrawable. Returning empty string");
        return "";
      }
    }
    catch (RemoteException paramzzch)
    {
      zzin.d("Unable to get drawable. Returning empty string");
      return "";
    }
    return a(((BitmapDrawable)paramzzch).getBitmap());
  }
  
  private static JSONObject b(Bundle paramBundle, String paramString)
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject();
    if ((paramBundle == null) || (TextUtils.isEmpty(paramString))) {
      return localJSONObject;
    }
    paramString = new JSONObject(paramString);
    Iterator localIterator = paramString.keys();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      if (paramBundle.containsKey(str)) {
        if ("image".equals(paramString.getString(str)))
        {
          Object localObject = paramBundle.get(str);
          if ((localObject instanceof Bitmap)) {
            localJSONObject.put(str, a((Bitmap)localObject));
          } else {
            zzin.d("Invalid type. An image type extra should return a bitmap");
          }
        }
        else if ((paramBundle.get(str) instanceof Bitmap))
        {
          zzin.d("Invalid asset type. Bitmap should be returned only for image type");
        }
        else
        {
          localJSONObject.put(str, String.valueOf(paramBundle.get(str)));
        }
      }
    }
    return localJSONObject;
  }
  
  private static void b(zzjp paramzzjp)
  {
    View.OnClickListener localOnClickListener = paramzzjp.A();
    if (localOnClickListener != null) {
      localOnClickListener.onClick(paramzzjp.b());
    }
  }
  
  public static boolean b(zzif paramzzif)
  {
    return (paramzzif != null) && (paramzzif.m) && (paramzzif.n != null) && (paramzzif.n.k != null);
  }
  
  private static boolean b(zzjp paramzzjp, zzes paramzzes, CountDownLatch paramCountDownLatch)
    throws RemoteException
  {
    Object localObject = paramzzjp.b();
    if (localObject == null)
    {
      zzin.d("AdWebView is null");
      return false;
    }
    ((View)localObject).setVisibility(4);
    localObject = paramzzes.b.n;
    if ((localObject == null) || (((List)localObject).isEmpty()))
    {
      zzin.d("No template ids present in mediation response");
      return false;
    }
    a(paramzzjp, paramCountDownLatch);
    paramCountDownLatch = paramzzes.c.h();
    zzfc localzzfc = paramzzes.c.i();
    if ((((List)localObject).contains("2")) && (paramCountDownLatch != null))
    {
      a(paramzzjp, a(paramCountDownLatch), paramzzes.b.m);
      paramCountDownLatch = paramzzes.b.k;
      paramzzes = paramzzes.b.l;
      if (paramzzes == null) {
        break label188;
      }
      paramzzjp.loadDataWithBaseURL(paramzzes, paramCountDownLatch, "text/html", "UTF-8", null);
    }
    for (;;)
    {
      return true;
      if ((((List)localObject).contains("1")) && (localzzfc != null))
      {
        a(paramzzjp, a(localzzfc), paramzzes.b.m);
        break;
      }
      zzin.d("No matching template id and mapper");
      return false;
      label188:
      paramzzjp.loadData(paramCountDownLatch, "text/html", "UTF-8");
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/zzm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */