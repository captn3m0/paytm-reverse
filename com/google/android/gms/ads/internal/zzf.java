package com.google.android.gms.ads.internal;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzn;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzau;
import com.google.android.gms.internal.zzax;
import com.google.android.gms.internal.zzbp;
import com.google.android.gms.internal.zzbt;
import com.google.android.gms.internal.zzex;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzif;
import com.google.android.gms.internal.zzif.zza;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zzir;
import com.google.android.gms.internal.zzjk;
import com.google.android.gms.internal.zzjp;
import com.google.android.gms.internal.zzjq;
import com.google.android.gms.internal.zzjq.zzb;
import java.util.List;

@zzhb
public class zzf
  extends zzc
  implements ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener
{
  private boolean l;
  
  public zzf(Context paramContext, AdSizeParcel paramAdSizeParcel, String paramString, zzex paramzzex, VersionInfoParcel paramVersionInfoParcel, zzd paramzzd)
  {
    super(paramContext, paramAdSizeParcel, paramString, paramzzex, paramVersionInfoParcel, paramzzd);
  }
  
  private AdSizeParcel b(zzif.zza paramzza)
  {
    if (paramzza.b.B) {
      return this.f.i;
    }
    paramzza = paramzza.b.m;
    if (paramzza != null)
    {
      paramzza = paramzza.split("[xX]");
      paramzza[0] = paramzza[0].trim();
      paramzza[1] = paramzza[1].trim();
    }
    for (paramzza = new AdSize(Integer.parseInt(paramzza[0]), Integer.parseInt(paramzza[1]));; paramzza = this.f.i.b()) {
      return new AdSizeParcel(this.f.c, paramzza);
    }
  }
  
  private boolean b(zzif paramzzif1, zzif paramzzif2)
  {
    View localView1;
    if (paramzzif2.m)
    {
      localView1 = zzm.a(paramzzif2);
      if (localView1 == null)
      {
        zzin.d("Could not get mediation view");
        return false;
      }
      View localView2 = this.f.f.getNextView();
      if (localView2 != null)
      {
        if ((localView2 instanceof zzjp)) {
          ((zzjp)localView2).destroy();
        }
        this.f.f.removeView(localView2);
      }
      if (zzm.b(paramzzif2)) {}
    }
    for (;;)
    {
      try
      {
        a(localView1);
        if (this.f.f.getChildCount() > 1) {
          this.f.f.showNext();
        }
        if (paramzzif1 != null)
        {
          paramzzif1 = this.f.f.getNextView();
          if (!(paramzzif1 instanceof zzjp)) {
            break label271;
          }
          ((zzjp)paramzzif1).a(this.f.c, this.f.i, this.a);
          this.f.d();
        }
        this.f.f.setVisibility(0);
        return true;
      }
      catch (Throwable paramzzif1)
      {
        zzin.d("Could not add mediation view to view hierarchy.", paramzzif1);
        return false;
      }
      if ((paramzzif2.t != null) && (paramzzif2.b != null))
      {
        paramzzif2.b.a(paramzzif2.t);
        this.f.f.removeAllViews();
        this.f.f.setMinimumWidth(paramzzif2.t.g);
        this.f.f.setMinimumHeight(paramzzif2.t.d);
        a(paramzzif2.b.b());
        continue;
        label271:
        if (paramzzif1 != null) {
          this.f.f.removeView(paramzzif1);
        }
      }
    }
  }
  
  private void e(final zzif paramzzif)
  {
    if (this.f.e()) {
      if (paramzzif.b != null)
      {
        if (paramzzif.j != null) {
          this.h.a(this.f.i, paramzzif);
        }
        if (!paramzzif.a()) {
          break label70;
        }
        this.h.a(this.f.i, paramzzif).a(paramzzif.b);
      }
    }
    label70:
    while ((this.f.C == null) || (paramzzif.j == null))
    {
      return;
      paramzzif.b.l().a(new zzjq.zzb()
      {
        public void a()
        {
          zzf.this.h.a(zzf.this.f.i, paramzzif).a(paramzzif.b);
        }
      });
      return;
    }
    this.h.a(this.f.i, paramzzif, this.f.C);
  }
  
  protected zzjp a(zzif.zza paramzza, zze paramzze)
  {
    if (this.f.i.j) {
      this.f.i = b(paramzza);
    }
    return super.a(paramzza, paramzze);
  }
  
  protected void a(zzif paramzzif, boolean paramBoolean)
  {
    super.a(paramzzif, paramBoolean);
    if (zzm.b(paramzzif)) {
      zzm.a(paramzzif, new zza());
    }
  }
  
  public void a(boolean paramBoolean)
  {
    zzx.b("setManualImpressionsEnabled must be called from the main thread.");
    this.l = paramBoolean;
  }
  
  public boolean a(AdRequestParcel paramAdRequestParcel)
  {
    return super.a(d(paramAdRequestParcel));
  }
  
  public boolean a(zzif paramzzif1, zzif paramzzif2)
  {
    if (!super.a(paramzzif1, paramzzif2)) {
      return false;
    }
    if ((this.f.e()) && (!b(paramzzif1, paramzzif2)))
    {
      a(0);
      return false;
    }
    if (paramzzif2.k)
    {
      d(paramzzif2);
      zzjk.a(this.f.f, this);
      zzjk.a(this.f.f, this);
    }
    for (;;)
    {
      e(paramzzif2);
      return true;
      if ((!this.f.f()) || (((Boolean)zzbt.aG.c()).booleanValue())) {
        a(paramzzif2, false);
      }
    }
  }
  
  AdRequestParcel d(AdRequestParcel paramAdRequestParcel)
  {
    if (paramAdRequestParcel.h == this.l) {
      return paramAdRequestParcel;
    }
    int i = paramAdRequestParcel.a;
    long l1 = paramAdRequestParcel.b;
    Bundle localBundle = paramAdRequestParcel.c;
    int j = paramAdRequestParcel.d;
    List localList = paramAdRequestParcel.e;
    boolean bool2 = paramAdRequestParcel.f;
    int k = paramAdRequestParcel.g;
    if ((paramAdRequestParcel.h) || (this.l)) {}
    for (boolean bool1 = true;; bool1 = false) {
      return new AdRequestParcel(i, l1, localBundle, j, localList, bool2, k, bool1, paramAdRequestParcel.i, paramAdRequestParcel.j, paramAdRequestParcel.k, paramAdRequestParcel.l, paramAdRequestParcel.m, paramAdRequestParcel.n, paramAdRequestParcel.o, paramAdRequestParcel.p, paramAdRequestParcel.q, paramAdRequestParcel.r);
    }
  }
  
  void d(zzif paramzzif)
  {
    if (paramzzif == null) {}
    while ((paramzzif.l) || (this.f.f == null) || (!zzr.e().a(this.f.f, this.f.c)) || (!this.f.f.getGlobalVisibleRect(new Rect(), null))) {
      return;
    }
    a(paramzzif, false);
    paramzzif.l = true;
  }
  
  public void f()
  {
    throw new IllegalStateException("Interstitial is NOT supported by BannerAdManager.");
  }
  
  public void onGlobalLayout()
  {
    d(this.f.j);
  }
  
  public void onScrollChanged()
  {
    d(this.f.j);
  }
  
  protected boolean s()
  {
    boolean bool = true;
    if (!zzr.e().a(this.f.c.getPackageManager(), this.f.c.getPackageName(), "android.permission.INTERNET"))
    {
      zzn.a().a(this.f.f, this.f.i, "Missing internet permission in AndroidManifest.xml.", "Missing internet permission in AndroidManifest.xml. You must have the following declaration: <uses-permission android:name=\"android.permission.INTERNET\" />");
      bool = false;
    }
    if (!zzr.e().a(this.f.c))
    {
      zzn.a().a(this.f.f, this.f.i, "Missing AdActivity with android:configChanges in AndroidManifest.xml.", "Missing AdActivity with android:configChanges in AndroidManifest.xml. You must have the following declaration within the <application> element: <activity android:name=\"com.google.android.gms.ads.AdActivity\" android:configChanges=\"keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize\" />");
      bool = false;
    }
    if ((!bool) && (this.f.f != null)) {
      this.f.f.setVisibility(0);
    }
    return bool;
  }
  
  public class zza
  {
    public zza() {}
    
    public void onClick()
    {
      zzf.this.e();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */