package com.google.android.gms.ads.internal;

import android.content.Context;
import android.os.Handler;
import android.support.v4.util.SimpleArrayMap;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzq;
import com.google.android.gms.ads.internal.client.zzr.zza;
import com.google.android.gms.ads.internal.client.zzx;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.internal.zzcr;
import com.google.android.gms.internal.zzcs;
import com.google.android.gms.internal.zzct;
import com.google.android.gms.internal.zzcu;
import com.google.android.gms.internal.zzex;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzir;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

@zzhb
public class zzi
  extends zzr.zza
{
  private final Context a;
  private final zzq b;
  private final zzex c;
  private final zzcr d;
  private final zzcs e;
  private final SimpleArrayMap<String, zzcu> f;
  private final SimpleArrayMap<String, zzct> g;
  private final NativeAdOptionsParcel h;
  private final List<String> i;
  private final zzx j;
  private final String k;
  private final VersionInfoParcel l;
  private WeakReference<zzp> m;
  private final zzd n;
  private final Object o = new Object();
  
  zzi(Context paramContext, String paramString, zzex paramzzex, VersionInfoParcel paramVersionInfoParcel, zzq paramzzq, zzcr paramzzcr, zzcs paramzzcs, SimpleArrayMap<String, zzcu> paramSimpleArrayMap, SimpleArrayMap<String, zzct> paramSimpleArrayMap1, NativeAdOptionsParcel paramNativeAdOptionsParcel, zzx paramzzx, zzd paramzzd)
  {
    this.a = paramContext;
    this.k = paramString;
    this.c = paramzzex;
    this.l = paramVersionInfoParcel;
    this.b = paramzzq;
    this.e = paramzzcs;
    this.d = paramzzcr;
    this.f = paramSimpleArrayMap;
    this.g = paramSimpleArrayMap1;
    this.h = paramNativeAdOptionsParcel;
    this.i = d();
    this.j = paramzzx;
    this.n = paramzzd;
  }
  
  private List<String> d()
  {
    ArrayList localArrayList = new ArrayList();
    if (this.e != null) {
      localArrayList.add("1");
    }
    if (this.d != null) {
      localArrayList.add("2");
    }
    if (this.f.size() > 0) {
      localArrayList.add("3");
    }
    return localArrayList;
  }
  
  public void a(final AdRequestParcel paramAdRequestParcel)
  {
    a(new Runnable()
    {
      public void run()
      {
        synchronized (zzi.a(zzi.this))
        {
          zzp localzzp = zzi.this.c();
          zzi.a(zzi.this, new WeakReference(localzzp));
          localzzp.a(zzi.b(zzi.this));
          localzzp.a(zzi.c(zzi.this));
          localzzp.a(zzi.d(zzi.this));
          localzzp.a(zzi.e(zzi.this));
          localzzp.b(zzi.f(zzi.this));
          localzzp.a(zzi.g(zzi.this));
          localzzp.a(zzi.h(zzi.this));
          localzzp.a(zzi.i(zzi.this));
          localzzp.a(paramAdRequestParcel);
          return;
        }
      }
    });
  }
  
  protected void a(Runnable paramRunnable)
  {
    zzir.a.post(paramRunnable);
  }
  
  public boolean a()
  {
    for (;;)
    {
      synchronized (this.o)
      {
        if (this.m != null)
        {
          zzp localzzp = (zzp)this.m.get();
          if (localzzp != null)
          {
            bool = localzzp.k();
            return bool;
          }
        }
        else
        {
          return false;
        }
      }
      boolean bool = false;
    }
  }
  
  public String b()
  {
    for (;;)
    {
      synchronized (this.o)
      {
        if (this.m != null)
        {
          Object localObject1 = (zzp)this.m.get();
          if (localObject1 != null)
          {
            localObject1 = ((zzp)localObject1).j();
            return (String)localObject1;
          }
        }
        else
        {
          return null;
        }
      }
      Object localObject3 = null;
    }
  }
  
  protected zzp c()
  {
    return new zzp(this.a, this.n, AdSizeParcel.a(this.a), this.k, this.c, this.l);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */