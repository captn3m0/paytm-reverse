package com.google.android.gms.ads.internal.util.client;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzc
  implements Parcelable.Creator<VersionInfoParcel>
{
  static void a(VersionInfoParcel paramVersionInfoParcel, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramVersionInfoParcel.a);
    zzb.a(paramParcel, 2, paramVersionInfoParcel.b, false);
    zzb.a(paramParcel, 3, paramVersionInfoParcel.c);
    zzb.a(paramParcel, 4, paramVersionInfoParcel.d);
    zzb.a(paramParcel, 5, paramVersionInfoParcel.e);
    zzb.a(paramParcel, paramInt);
  }
  
  public VersionInfoParcel a(Parcel paramParcel)
  {
    boolean bool = false;
    int m = zza.b(paramParcel);
    String str = null;
    int i = 0;
    int j = 0;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        str = zza.p(paramParcel, n);
        break;
      case 3: 
        j = zza.g(paramParcel, n);
        break;
      case 4: 
        i = zza.g(paramParcel, n);
        break;
      case 5: 
        bool = zza.c(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new VersionInfoParcel(k, str, j, i, bool);
  }
  
  public VersionInfoParcel[] a(int paramInt)
  {
    return new VersionInfoParcel[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/internal/util/client/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */