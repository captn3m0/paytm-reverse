package com.google.android.gms.ads.identifier;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.common.stats.zzb;
import com.google.android.gms.common.zza;
import com.google.android.gms.common.zzc;
import com.google.android.gms.internal.zzat;
import com.google.android.gms.internal.zzat.zza;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class AdvertisingIdClient
{
  zza a;
  zzat b;
  boolean c;
  Object d = new Object();
  zza e;
  final long f;
  private final Context g;
  
  public AdvertisingIdClient(Context paramContext)
  {
    this(paramContext, 30000L);
  }
  
  public AdvertisingIdClient(Context paramContext, long paramLong)
  {
    zzx.a(paramContext);
    this.g = paramContext;
    this.c = false;
    this.f = paramLong;
  }
  
  static zza a(Context paramContext)
    throws IOException, GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException
  {
    try
    {
      paramContext.getPackageManager().getPackageInfo("com.android.vending", 0);
      switch (zzc.b().a(paramContext))
      {
      case 1: 
      default: 
        throw new IOException("Google Play services not available");
      }
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      throw new GooglePlayServicesNotAvailableException(9);
    }
    zza localzza = new zza();
    Intent localIntent = new Intent("com.google.android.gms.ads.identifier.service.START");
    localIntent.setPackage("com.google.android.gms");
    try
    {
      boolean bool = zzb.a().a(paramContext, localIntent, localzza, 1);
      if (bool) {
        return localzza;
      }
    }
    catch (Throwable paramContext)
    {
      throw new IOException(paramContext);
    }
    throw new IOException("Connection failure");
  }
  
  static zzat a(Context paramContext, zza paramzza)
    throws IOException
  {
    try
    {
      paramContext = zzat.zza.a(paramzza.a());
      return paramContext;
    }
    catch (InterruptedException paramContext)
    {
      throw new IOException("Interrupted exception");
    }
    catch (Throwable paramContext)
    {
      throw new IOException(paramContext);
    }
  }
  
  public static Info b(Context paramContext)
    throws IOException, IllegalStateException, GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException
  {
    paramContext = new AdvertisingIdClient(paramContext, -1L);
    try
    {
      paramContext.a(false);
      Info localInfo = paramContext.b();
      return localInfo;
    }
    finally
    {
      paramContext.c();
    }
  }
  
  public static void b(boolean paramBoolean) {}
  
  private void d()
  {
    synchronized (this.d)
    {
      if (this.e != null) {
        this.e.cancel();
      }
    }
    try
    {
      this.e.join();
      if (this.f > 0L) {
        this.e = new zza(this, this.f);
      }
      return;
      localObject2 = finally;
      throw ((Throwable)localObject2);
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;) {}
    }
  }
  
  public void a()
    throws IOException, IllegalStateException, GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException
  {
    a(true);
  }
  
  protected void a(boolean paramBoolean)
    throws IOException, IllegalStateException, GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException
  {
    zzx.c("Calling this from your main thread can lead to deadlock");
    try
    {
      if (this.c) {
        c();
      }
      this.a = a(this.g);
      this.b = a(this.g, this.a);
      this.c = true;
      if (paramBoolean) {
        d();
      }
      return;
    }
    finally {}
  }
  
  /* Error */
  public Info b()
    throws IOException
  {
    // Byte code:
    //   0: ldc -100
    //   2: invokestatic 158	com/google/android/gms/common/internal/zzx:c	(Ljava/lang/String;)V
    //   5: aload_0
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield 46	com/google/android/gms/ads/identifier/AdvertisingIdClient:c	Z
    //   11: ifne +83 -> 94
    //   14: aload_0
    //   15: getfield 37	com/google/android/gms/ads/identifier/AdvertisingIdClient:d	Ljava/lang/Object;
    //   18: astore_1
    //   19: aload_1
    //   20: monitorenter
    //   21: aload_0
    //   22: getfield 145	com/google/android/gms/ads/identifier/AdvertisingIdClient:e	Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$zza;
    //   25: ifnull +13 -> 38
    //   28: aload_0
    //   29: getfield 145	com/google/android/gms/ads/identifier/AdvertisingIdClient:e	Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$zza;
    //   32: invokevirtual 175	com/google/android/gms/ads/identifier/AdvertisingIdClient$zza:a	()Z
    //   35: ifne +23 -> 58
    //   38: new 51	java/io/IOException
    //   41: dup
    //   42: ldc -79
    //   44: invokespecial 86	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   47: athrow
    //   48: astore_2
    //   49: aload_1
    //   50: monitorexit
    //   51: aload_2
    //   52: athrow
    //   53: astore_1
    //   54: aload_0
    //   55: monitorexit
    //   56: aload_1
    //   57: athrow
    //   58: aload_1
    //   59: monitorexit
    //   60: aload_0
    //   61: iconst_0
    //   62: invokevirtual 138	com/google/android/gms/ads/identifier/AdvertisingIdClient:a	(Z)V
    //   65: aload_0
    //   66: getfield 46	com/google/android/gms/ads/identifier/AdvertisingIdClient:c	Z
    //   69: ifne +25 -> 94
    //   72: new 51	java/io/IOException
    //   75: dup
    //   76: ldc -77
    //   78: invokespecial 86	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   81: athrow
    //   82: astore_1
    //   83: new 51	java/io/IOException
    //   86: dup
    //   87: ldc -77
    //   89: aload_1
    //   90: invokespecial 182	java/io/IOException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   93: athrow
    //   94: aload_0
    //   95: getfield 162	com/google/android/gms/ads/identifier/AdvertisingIdClient:a	Lcom/google/android/gms/common/zza;
    //   98: invokestatic 42	com/google/android/gms/common/internal/zzx:a	(Ljava/lang/Object;)Ljava/lang/Object;
    //   101: pop
    //   102: aload_0
    //   103: getfield 166	com/google/android/gms/ads/identifier/AdvertisingIdClient:b	Lcom/google/android/gms/internal/zzat;
    //   106: invokestatic 42	com/google/android/gms/common/internal/zzx:a	(Ljava/lang/Object;)Ljava/lang/Object;
    //   109: pop
    //   110: new 6	com/google/android/gms/ads/identifier/AdvertisingIdClient$Info
    //   113: dup
    //   114: aload_0
    //   115: getfield 166	com/google/android/gms/ads/identifier/AdvertisingIdClient:b	Lcom/google/android/gms/internal/zzat;
    //   118: invokeinterface 187 1 0
    //   123: aload_0
    //   124: getfield 166	com/google/android/gms/ads/identifier/AdvertisingIdClient:b	Lcom/google/android/gms/internal/zzat;
    //   127: iconst_1
    //   128: invokeinterface 190 2 0
    //   133: invokespecial 193	com/google/android/gms/ads/identifier/AdvertisingIdClient$Info:<init>	(Ljava/lang/String;Z)V
    //   136: astore_1
    //   137: aload_0
    //   138: monitorexit
    //   139: aload_0
    //   140: invokespecial 168	com/google/android/gms/ads/identifier/AdvertisingIdClient:d	()V
    //   143: aload_1
    //   144: areturn
    //   145: astore_1
    //   146: ldc -61
    //   148: ldc -59
    //   150: aload_1
    //   151: invokestatic 203	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   154: pop
    //   155: new 51	java/io/IOException
    //   158: dup
    //   159: ldc -51
    //   161: invokespecial 86	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   164: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	165	0	this	AdvertisingIdClient
    //   53	6	1	localObject2	Object
    //   82	8	1	localException	Exception
    //   136	8	1	localInfo	Info
    //   145	6	1	localRemoteException	android.os.RemoteException
    //   48	4	2	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   21	38	48	finally
    //   38	48	48	finally
    //   49	51	48	finally
    //   58	60	48	finally
    //   7	21	53	finally
    //   51	53	53	finally
    //   54	56	53	finally
    //   60	65	53	finally
    //   65	82	53	finally
    //   83	94	53	finally
    //   94	110	53	finally
    //   110	137	53	finally
    //   137	139	53	finally
    //   146	165	53	finally
    //   60	65	82	java/lang/Exception
    //   110	137	145	android/os/RemoteException
  }
  
  /* Error */
  public void c()
  {
    // Byte code:
    //   0: ldc -100
    //   2: invokestatic 158	com/google/android/gms/common/internal/zzx:c	(Ljava/lang/String;)V
    //   5: aload_0
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield 44	com/google/android/gms/ads/identifier/AdvertisingIdClient:g	Landroid/content/Context;
    //   11: ifnull +10 -> 21
    //   14: aload_0
    //   15: getfield 162	com/google/android/gms/ads/identifier/AdvertisingIdClient:a	Lcom/google/android/gms/common/zza;
    //   18: ifnonnull +6 -> 24
    //   21: aload_0
    //   22: monitorexit
    //   23: return
    //   24: aload_0
    //   25: getfield 46	com/google/android/gms/ads/identifier/AdvertisingIdClient:c	Z
    //   28: ifeq +17 -> 45
    //   31: invokestatic 108	com/google/android/gms/common/stats/zzb:a	()Lcom/google/android/gms/common/stats/zzb;
    //   34: aload_0
    //   35: getfield 44	com/google/android/gms/ads/identifier/AdvertisingIdClient:g	Landroid/content/Context;
    //   38: aload_0
    //   39: getfield 162	com/google/android/gms/ads/identifier/AdvertisingIdClient:a	Lcom/google/android/gms/common/zza;
    //   42: invokevirtual 210	com/google/android/gms/common/stats/zzb:a	(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    //   45: aload_0
    //   46: iconst_0
    //   47: putfield 46	com/google/android/gms/ads/identifier/AdvertisingIdClient:c	Z
    //   50: aload_0
    //   51: aconst_null
    //   52: putfield 166	com/google/android/gms/ads/identifier/AdvertisingIdClient:b	Lcom/google/android/gms/internal/zzat;
    //   55: aload_0
    //   56: aconst_null
    //   57: putfield 162	com/google/android/gms/ads/identifier/AdvertisingIdClient:a	Lcom/google/android/gms/common/zza;
    //   60: aload_0
    //   61: monitorexit
    //   62: return
    //   63: astore_1
    //   64: aload_0
    //   65: monitorexit
    //   66: aload_1
    //   67: athrow
    //   68: astore_1
    //   69: ldc -61
    //   71: ldc -44
    //   73: aload_1
    //   74: invokestatic 203	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   77: pop
    //   78: goto -33 -> 45
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	81	0	this	AdvertisingIdClient
    //   63	4	1	localObject	Object
    //   68	6	1	localIllegalArgumentException	IllegalArgumentException
    // Exception table:
    //   from	to	target	type
    //   7	21	63	finally
    //   21	23	63	finally
    //   24	45	63	finally
    //   45	62	63	finally
    //   64	66	63	finally
    //   69	78	63	finally
    //   24	45	68	java/lang/IllegalArgumentException
  }
  
  protected void finalize()
    throws Throwable
  {
    c();
    super.finalize();
  }
  
  public static final class Info
  {
    private final String a;
    private final boolean b;
    
    public Info(String paramString, boolean paramBoolean)
    {
      this.a = paramString;
      this.b = paramBoolean;
    }
    
    public String a()
    {
      return this.a;
    }
    
    public boolean b()
    {
      return this.b;
    }
    
    public String toString()
    {
      return "{" + this.a + "}" + this.b;
    }
  }
  
  static class zza
    extends Thread
  {
    CountDownLatch a;
    boolean b;
    private WeakReference<AdvertisingIdClient> c;
    private long d;
    
    public zza(AdvertisingIdClient paramAdvertisingIdClient, long paramLong)
    {
      this.c = new WeakReference(paramAdvertisingIdClient);
      this.d = paramLong;
      this.a = new CountDownLatch(1);
      this.b = false;
      start();
    }
    
    private void b()
    {
      AdvertisingIdClient localAdvertisingIdClient = (AdvertisingIdClient)this.c.get();
      if (localAdvertisingIdClient != null)
      {
        localAdvertisingIdClient.c();
        this.b = true;
      }
    }
    
    public boolean a()
    {
      return this.b;
    }
    
    public void cancel()
    {
      this.a.countDown();
    }
    
    public void run()
    {
      try
      {
        if (!this.a.await(this.d, TimeUnit.MILLISECONDS)) {
          b();
        }
        return;
      }
      catch (InterruptedException localInterruptedException)
      {
        b();
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/identifier/AdvertisingIdClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */