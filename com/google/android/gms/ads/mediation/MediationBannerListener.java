package com.google.android.gms.ads.mediation;

public abstract interface MediationBannerListener
{
  public abstract void a(MediationBannerAdapter paramMediationBannerAdapter);
  
  public abstract void a(MediationBannerAdapter paramMediationBannerAdapter, int paramInt);
  
  public abstract void b(MediationBannerAdapter paramMediationBannerAdapter);
  
  public abstract void c(MediationBannerAdapter paramMediationBannerAdapter);
  
  public abstract void d(MediationBannerAdapter paramMediationBannerAdapter);
  
  public abstract void e(MediationBannerAdapter paramMediationBannerAdapter);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/mediation/MediationBannerListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */