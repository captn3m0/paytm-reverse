package com.google.android.gms.ads.mediation;

import android.os.Bundle;

public abstract interface MediationAdapter
{
  public abstract void a();
  
  public abstract void b();
  
  public abstract void c();
  
  public static class zza
  {
    private int a;
    
    public Bundle a()
    {
      Bundle localBundle = new Bundle();
      localBundle.putInt("capabilities", this.a);
      return localBundle;
    }
    
    public zza a(int paramInt)
    {
      this.a = paramInt;
      return this;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/mediation/MediationAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */