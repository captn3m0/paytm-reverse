package com.google.android.gms.ads.mediation;

public abstract interface MediationNativeListener
{
  public abstract void a(MediationNativeAdapter paramMediationNativeAdapter);
  
  public abstract void a(MediationNativeAdapter paramMediationNativeAdapter, int paramInt);
  
  public abstract void a(MediationNativeAdapter paramMediationNativeAdapter, NativeAdMapper paramNativeAdMapper);
  
  public abstract void b(MediationNativeAdapter paramMediationNativeAdapter);
  
  public abstract void c(MediationNativeAdapter paramMediationNativeAdapter);
  
  public abstract void d(MediationNativeAdapter paramMediationNativeAdapter);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/mediation/MediationNativeListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */