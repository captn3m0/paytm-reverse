package com.google.android.gms.ads.mediation.customevent;

import com.google.ads.mediation.i;
import java.util.HashMap;

@Deprecated
public final class CustomEventExtras
  implements i
{
  private final HashMap<String, Object> a = new HashMap();
  
  public Object a(String paramString)
  {
    return this.a.get(paramString);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/mediation/customevent/CustomEventExtras.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */