package com.google.android.gms.ads.mediation;

import android.location.Location;
import java.util.Date;
import java.util.Set;

public abstract interface MediationAdRequest
{
  public abstract Date a();
  
  public abstract int b();
  
  public abstract Set<String> c();
  
  public abstract Location d();
  
  public abstract int e();
  
  public abstract boolean f();
  
  public abstract boolean g();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/ads/mediation/MediationAdRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */