package com.google.android.gms.appindexing;

import android.net.Uri;
import android.os.Bundle;
import com.google.android.gms.common.internal.zzx;

public class Thing
{
  final Bundle a;
  
  Thing(Bundle paramBundle)
  {
    this.a = paramBundle;
  }
  
  public Bundle a()
  {
    return this.a;
  }
  
  public static class Builder
  {
    final Bundle a = new Bundle();
    
    public Builder b(Uri paramUri)
    {
      zzx.a(paramUri);
      b("url", paramUri.toString());
      return this;
    }
    
    public Builder b(String paramString)
    {
      zzx.a(paramString);
      b("name", paramString);
      return this;
    }
    
    public Builder b(String paramString, Thing paramThing)
    {
      zzx.a(paramString);
      if (paramThing != null) {
        this.a.putParcelable(paramString, paramThing.a);
      }
      return this;
    }
    
    public Builder b(String paramString1, String paramString2)
    {
      zzx.a(paramString1);
      if (paramString2 != null) {
        this.a.putString(paramString1, paramString2);
      }
      return this;
    }
    
    public Thing b()
    {
      return new Thing(this.a);
    }
    
    public Builder c(String paramString)
    {
      if (paramString != null) {
        b("id", paramString);
      }
      return this;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/appindexing/Thing.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */