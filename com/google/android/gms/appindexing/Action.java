package com.google.android.gms.appindexing;

import android.net.Uri;
import android.os.Bundle;
import com.google.android.gms.common.internal.zzx;

public final class Action
  extends Thing
{
  private Action(Bundle paramBundle)
  {
    super(paramBundle);
  }
  
  public static Action a(String paramString1, String paramString2, Uri paramUri1, Uri paramUri2)
  {
    Builder localBuilder = new Builder(paramString1);
    paramString2 = new Thing.Builder().b(paramString2);
    if (paramUri1 == null) {}
    for (paramString1 = null;; paramString1 = paramUri1.toString()) {
      return localBuilder.a(paramString2.c(paramString1).b(paramUri2).b()).a();
    }
  }
  
  public static final class Builder
    extends Thing.Builder
  {
    public Builder(String paramString)
    {
      zzx.a(paramString);
      super.b("type", paramString);
    }
    
    public Builder a(Uri paramUri)
    {
      if (paramUri != null) {
        super.b("url", paramUri.toString());
      }
      return this;
    }
    
    public Builder a(Thing paramThing)
    {
      zzx.a(paramThing);
      return (Builder)super.b("object", paramThing);
    }
    
    public Builder a(String paramString)
    {
      return (Builder)super.b("name", paramString);
    }
    
    public Builder a(String paramString, Thing paramThing)
    {
      return (Builder)super.b(paramString, paramThing);
    }
    
    public Builder a(String paramString1, String paramString2)
    {
      return (Builder)super.b(paramString1, paramString2);
    }
    
    public Action a()
    {
      zzx.a(this.a.get("object"), "setObject is required before calling build().");
      zzx.a(this.a.get("type"), "setType is required before calling build().");
      Bundle localBundle = (Bundle)this.a.getParcelable("object");
      zzx.a(localBundle.get("name"), "Must call setObject() with a valid name. Example: setObject(new Thing.Builder().setName(name).setUrl(url))");
      zzx.a(localBundle.get("url"), "Must call setObject() with a valid app URI. Example: setObject(new Thing.Builder().setName(name).setUrl(url))");
      return new Action(this.a, null);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/appindexing/Action.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */