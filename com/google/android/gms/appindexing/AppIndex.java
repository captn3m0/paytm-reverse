package com.google.android.gms.appindexing;

import com.google.android.gms.appdatasearch.zza;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.internal.zzkk;

public final class AppIndex
{
  public static final Api<Api.ApiOptions.NoOptions> a = zza.b;
  public static final Api<Api.ApiOptions.NoOptions> b = zza.b;
  public static final AppIndexApi c = new zzkk();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/appindexing/AppIndex.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */