package com.google.android.gms.appindexing;

import android.net.Uri;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;

public abstract interface AppIndexApi
{
  public abstract PendingResult<Status> a(GoogleApiClient paramGoogleApiClient, Action paramAction);
  
  public abstract PendingResult<Status> b(GoogleApiClient paramGoogleApiClient, Action paramAction);
  
  @Deprecated
  public static abstract interface ActionResult {}
  
  @Deprecated
  public static final class AppIndexingLink
  {
    public final Uri a;
    public final Uri b;
    public final int c;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/appindexing/AppIndexApi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */