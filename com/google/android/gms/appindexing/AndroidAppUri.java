package com.google.android.gms.appindexing;

import android.net.Uri;
import com.google.android.gms.common.internal.zzw;

public final class AndroidAppUri
{
  private final Uri a;
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject instanceof AndroidAppUri)) {
      return this.a.equals(((AndroidAppUri)paramObject).a);
    }
    return false;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.a });
  }
  
  public String toString()
  {
    return this.a.toString();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/appindexing/AndroidAppUri.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */