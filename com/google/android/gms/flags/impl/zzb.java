package com.google.android.gms.flags.impl;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.android.gms.internal.zzpl;
import java.util.concurrent.Callable;

public class zzb
{
  private static SharedPreferences a = null;
  
  public static SharedPreferences a(Context paramContext)
  {
    try
    {
      if (a == null) {
        a = (SharedPreferences)zzpl.a(new Callable()
        {
          public SharedPreferences a()
          {
            return this.a.getSharedPreferences("google_sdk_flags", 1);
          }
        });
      }
      paramContext = a;
      return paramContext;
    }
    finally {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/flags/impl/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */