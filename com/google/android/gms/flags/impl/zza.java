package com.google.android.gms.flags.impl;

import android.content.SharedPreferences;
import com.google.android.gms.internal.zzpl;
import java.util.concurrent.Callable;

public abstract class zza<T>
{
  public static class zza
    extends zza<Boolean>
  {
    public static Boolean a(SharedPreferences paramSharedPreferences, final String paramString, final Boolean paramBoolean)
    {
      (Boolean)zzpl.a(new Callable()
      {
        public Boolean a()
        {
          return Boolean.valueOf(this.a.getBoolean(paramString, paramBoolean.booleanValue()));
        }
      });
    }
  }
  
  public static class zzb
    extends zza<Integer>
  {
    public static Integer a(SharedPreferences paramSharedPreferences, final String paramString, final Integer paramInteger)
    {
      (Integer)zzpl.a(new Callable()
      {
        public Integer a()
        {
          return Integer.valueOf(this.a.getInt(paramString, paramInteger.intValue()));
        }
      });
    }
  }
  
  public static class zzc
    extends zza<Long>
  {
    public static Long a(SharedPreferences paramSharedPreferences, final String paramString, final Long paramLong)
    {
      (Long)zzpl.a(new Callable()
      {
        public Long a()
        {
          return Long.valueOf(this.a.getLong(paramString, paramLong.longValue()));
        }
      });
    }
  }
  
  public static class zzd
    extends zza<String>
  {
    public static String a(SharedPreferences paramSharedPreferences, final String paramString1, final String paramString2)
    {
      (String)zzpl.a(new Callable()
      {
        public String a()
        {
          return this.a.getString(paramString1, paramString2);
        }
      });
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/flags/impl/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */