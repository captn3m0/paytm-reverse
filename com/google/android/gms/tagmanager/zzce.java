package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.zzad;
import com.google.android.gms.internal.zzae;
import com.google.android.gms.internal.zzag.zza;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

class zzce
  extends zzak
{
  private static final String ID = zzad.O.toString();
  private static final String zzbka = zzae.q.toString();
  private static final String zzbkb = zzae.r.toString();
  private static final String zzbkc = zzae.bj.toString();
  private static final String zzbkd = zzae.bd.toString();
  
  public zzce()
  {
    super(ID, new String[] { zzbka, zzbkb });
  }
  
  public boolean zzFW()
  {
    return true;
  }
  
  public zzag.zza zzP(Map<String, zzag.zza> paramMap)
  {
    Object localObject = (zzag.zza)paramMap.get(zzbka);
    zzag.zza localzza = (zzag.zza)paramMap.get(zzbkb);
    if ((localObject == null) || (localObject == zzdf.zzHF()) || (localzza == null) || (localzza == zzdf.zzHF())) {
      return zzdf.zzHF();
    }
    int i = 64;
    if (zzdf.zzk((zzag.zza)paramMap.get(zzbkc)).booleanValue()) {
      i = 66;
    }
    paramMap = (zzag.zza)paramMap.get(zzbkd);
    int j;
    if (paramMap != null)
    {
      paramMap = zzdf.zzi(paramMap);
      if (paramMap == zzdf.zzHA()) {
        return zzdf.zzHF();
      }
      int k = paramMap.intValue();
      j = k;
      if (k < 0) {
        return zzdf.zzHF();
      }
    }
    else
    {
      j = 1;
    }
    try
    {
      paramMap = zzdf.zzg((zzag.zza)localObject);
      localObject = zzdf.zzg(localzza);
      localzza = null;
      localObject = Pattern.compile((String)localObject, i).matcher(paramMap);
      paramMap = localzza;
      if (((Matcher)localObject).find())
      {
        paramMap = localzza;
        if (((Matcher)localObject).groupCount() >= j) {
          paramMap = ((Matcher)localObject).group(j);
        }
      }
      if (paramMap == null) {
        return zzdf.zzHF();
      }
      paramMap = zzdf.zzR(paramMap);
      return paramMap;
    }
    catch (PatternSyntaxException paramMap) {}
    return zzdf.zzHF();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/tagmanager/zzce.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */