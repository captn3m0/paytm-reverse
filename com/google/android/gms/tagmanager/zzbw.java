package com.google.android.gms.tagmanager;

class zzbw<T>
{
  private final T zzbjC;
  private final boolean zzbjD;
  
  zzbw(T paramT, boolean paramBoolean)
  {
    this.zzbjC = paramT;
    this.zzbjD = paramBoolean;
  }
  
  public T getObject()
  {
    return (T)this.zzbjC;
  }
  
  public boolean zzGP()
  {
    return this.zzbjD;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/tagmanager/zzbw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */