package com.google.android.gms.wallet;

import android.accounts.Account;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class WalletConstants
{
  public static final Account a = new Account("ACCOUNT_NO_WALLET", "com.google");
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface CardNetwork {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/wallet/WalletConstants.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */