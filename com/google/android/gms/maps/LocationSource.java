package com.google.android.gms.maps;

public abstract interface LocationSource
{
  public abstract void a();
  
  public abstract void a(OnLocationChangedListener paramOnLocationChangedListener);
  
  public static abstract interface OnLocationChangedListener {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/maps/LocationSource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */