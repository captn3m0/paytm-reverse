package com.google.android.gms.maps;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;

public class zzb
  implements Parcelable.Creator<StreetViewPanoramaOptions>
{
  static void a(StreetViewPanoramaOptions paramStreetViewPanoramaOptions, Parcel paramParcel, int paramInt)
  {
    int i = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramStreetViewPanoramaOptions.a());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramStreetViewPanoramaOptions.g(), paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 3, paramStreetViewPanoramaOptions.j(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 4, paramStreetViewPanoramaOptions.h(), paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 5, paramStreetViewPanoramaOptions.i(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 6, paramStreetViewPanoramaOptions.b());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 7, paramStreetViewPanoramaOptions.c());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 8, paramStreetViewPanoramaOptions.d());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 9, paramStreetViewPanoramaOptions.e());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 10, paramStreetViewPanoramaOptions.f());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, i);
  }
  
  public StreetViewPanoramaOptions a(Parcel paramParcel)
  {
    Integer localInteger = null;
    byte b1 = 0;
    int j = zza.b(paramParcel);
    byte b2 = 0;
    byte b3 = 0;
    byte b4 = 0;
    byte b5 = 0;
    LatLng localLatLng = null;
    String str = null;
    StreetViewPanoramaCamera localStreetViewPanoramaCamera = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localStreetViewPanoramaCamera = (StreetViewPanoramaCamera)zza.a(paramParcel, k, StreetViewPanoramaCamera.CREATOR);
        break;
      case 3: 
        str = zza.p(paramParcel, k);
        break;
      case 4: 
        localLatLng = (LatLng)zza.a(paramParcel, k, LatLng.CREATOR);
        break;
      case 5: 
        localInteger = zza.h(paramParcel, k);
        break;
      case 6: 
        b5 = zza.e(paramParcel, k);
        break;
      case 7: 
        b4 = zza.e(paramParcel, k);
        break;
      case 8: 
        b3 = zza.e(paramParcel, k);
        break;
      case 9: 
        b2 = zza.e(paramParcel, k);
        break;
      case 10: 
        b1 = zza.e(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new StreetViewPanoramaOptions(i, localStreetViewPanoramaCamera, str, localLatLng, localInteger, b5, b4, b3, b2, b1);
  }
  
  public StreetViewPanoramaOptions[] a(int paramInt)
  {
    return new StreetViewPanoramaOptions[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/maps/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */