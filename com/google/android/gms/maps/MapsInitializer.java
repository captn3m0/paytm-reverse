package com.google.android.gms.maps;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.maps.internal.zzad;
import com.google.android.gms.maps.internal.zzc;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.RuntimeRemoteException;

public final class MapsInitializer
{
  private static boolean a = false;
  
  public static int a(Context paramContext)
  {
    int i = 0;
    for (;;)
    {
      try
      {
        zzx.a(paramContext, "Context is null");
        boolean bool = a;
        if (!bool) {
          continue;
        }
      }
      finally
      {
        try
        {
          paramContext = zzad.a(paramContext);
          a(paramContext);
          a = true;
        }
        catch (GooglePlayServicesNotAvailableException paramContext)
        {
          i = paramContext.a;
        }
        paramContext = finally;
      }
      return i;
    }
  }
  
  public static void a(zzc paramzzc)
  {
    try
    {
      CameraUpdateFactory.a(paramzzc.a());
      BitmapDescriptorFactory.a(paramzzc.b());
      return;
    }
    catch (RemoteException paramzzc)
    {
      throw new RuntimeRemoteException(paramzzc);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/maps/MapsInitializer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */