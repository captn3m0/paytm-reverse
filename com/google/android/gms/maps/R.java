package com.google.android.gms.maps;

public final class R
{
  public static final class attr
  {
    public static final int adSize = 2130772038;
    public static final int adSizes = 2130772039;
    public static final int adUnitId = 2130772040;
    public static final int ambientEnabled = 2130772303;
    public static final int appTheme = 2130772429;
    public static final int buttonSize = 2130772349;
    public static final int buyButtonAppearance = 2130772436;
    public static final int buyButtonHeight = 2130772433;
    public static final int buyButtonText = 2130772435;
    public static final int buyButtonWidth = 2130772434;
    public static final int cameraBearing = 2130772288;
    public static final int cameraTargetLat = 2130772289;
    public static final int cameraTargetLng = 2130772290;
    public static final int cameraTilt = 2130772291;
    public static final int cameraZoom = 2130772292;
    public static final int circleCrop = 2130772286;
    public static final int colorScheme = 2130772350;
    public static final int environment = 2130772430;
    public static final int fragmentMode = 2130772432;
    public static final int fragmentStyle = 2130772431;
    public static final int imageAspectRatio = 2130772285;
    public static final int imageAspectRatioAdjust = 2130772284;
    public static final int liteMode = 2130772293;
    public static final int mapType = 2130772287;
    public static final int maskedWalletDetailsBackground = 2130772439;
    public static final int maskedWalletDetailsButtonBackground = 2130772441;
    public static final int maskedWalletDetailsButtonTextAppearance = 2130772440;
    public static final int maskedWalletDetailsHeaderTextAppearance = 2130772438;
    public static final int maskedWalletDetailsLogoImageType = 2130772443;
    public static final int maskedWalletDetailsLogoTextColor = 2130772442;
    public static final int maskedWalletDetailsTextAppearance = 2130772437;
    public static final int scopeUris = 2130772351;
    public static final int uiCompass = 2130772294;
    public static final int uiMapToolbar = 2130772302;
    public static final int uiRotateGestures = 2130772295;
    public static final int uiScrollGestures = 2130772296;
    public static final int uiTiltGestures = 2130772297;
    public static final int uiZoomControls = 2130772298;
    public static final int uiZoomGestures = 2130772299;
    public static final int useViewLifecycle = 2130772300;
    public static final int windowTransitionStyle = 2130772245;
    public static final int zOrderOnTop = 2130772301;
  }
  
  public static final class color
  {
    public static final int common_action_bar_splitter = 2131755172;
    public static final int common_google_signin_btn_text_dark = 2131755711;
    public static final int common_google_signin_btn_text_dark_default = 2131755173;
    public static final int common_google_signin_btn_text_dark_disabled = 2131755174;
    public static final int common_google_signin_btn_text_dark_focused = 2131755175;
    public static final int common_google_signin_btn_text_dark_pressed = 2131755176;
    public static final int common_google_signin_btn_text_light = 2131755712;
    public static final int common_google_signin_btn_text_light_default = 2131755177;
    public static final int common_google_signin_btn_text_light_disabled = 2131755178;
    public static final int common_google_signin_btn_text_light_focused = 2131755179;
    public static final int common_google_signin_btn_text_light_pressed = 2131755180;
    public static final int common_plus_signin_btn_text_dark = 2131755713;
    public static final int common_plus_signin_btn_text_dark_default = 2131755181;
    public static final int common_plus_signin_btn_text_dark_disabled = 2131755182;
    public static final int common_plus_signin_btn_text_dark_focused = 2131755183;
    public static final int common_plus_signin_btn_text_dark_pressed = 2131755184;
    public static final int common_plus_signin_btn_text_light = 2131755714;
    public static final int common_plus_signin_btn_text_light_default = 2131755185;
    public static final int common_plus_signin_btn_text_light_disabled = 2131755186;
    public static final int common_plus_signin_btn_text_light_focused = 2131755187;
    public static final int common_plus_signin_btn_text_light_pressed = 2131755188;
    public static final int place_autocomplete_prediction_primary_text = 2131755505;
    public static final int place_autocomplete_prediction_primary_text_highlight = 2131755506;
    public static final int place_autocomplete_prediction_secondary_text = 2131755507;
    public static final int place_autocomplete_search_hint = 2131755508;
    public static final int place_autocomplete_search_text = 2131755509;
    public static final int place_autocomplete_separator = 2131755510;
    public static final int wallet_bright_foreground_disabled_holo_light = 2131755665;
    public static final int wallet_bright_foreground_holo_dark = 2131755666;
    public static final int wallet_bright_foreground_holo_light = 2131755667;
    public static final int wallet_dim_foreground_disabled_holo_dark = 2131755668;
    public static final int wallet_dim_foreground_holo_dark = 2131755669;
    public static final int wallet_dim_foreground_inverse_disabled_holo_dark = 2131755670;
    public static final int wallet_dim_foreground_inverse_holo_dark = 2131755671;
    public static final int wallet_highlighted_text_holo_dark = 2131755676;
    public static final int wallet_highlighted_text_holo_light = 2131755677;
    public static final int wallet_hint_foreground_holo_dark = 2131755678;
    public static final int wallet_hint_foreground_holo_light = 2131755679;
    public static final int wallet_holo_blue_light = 2131755680;
    public static final int wallet_link_text_light = 2131755682;
    public static final int wallet_primary_text_holo_light = 2131755726;
    public static final int wallet_secondary_text_holo_dark = 2131755727;
  }
  
  public static final class dimen
  {
    public static final int place_autocomplete_button_padding = 2131493497;
    public static final int place_autocomplete_powered_by_google_height = 2131493498;
    public static final int place_autocomplete_powered_by_google_start = 2131493499;
    public static final int place_autocomplete_prediction_height = 2131493500;
    public static final int place_autocomplete_prediction_horizontal_margin = 2131493501;
    public static final int place_autocomplete_prediction_primary_text = 2131493502;
    public static final int place_autocomplete_prediction_secondary_text = 2131493503;
    public static final int place_autocomplete_progress_horizontal_margin = 2131493504;
    public static final int place_autocomplete_progress_size = 2131493505;
    public static final int place_autocomplete_separator_start = 2131493506;
  }
  
  public static final class drawable
  {
    public static final int cast_ic_notification_0 = 2130837796;
    public static final int cast_ic_notification_1 = 2130837797;
    public static final int cast_ic_notification_2 = 2130837798;
    public static final int cast_ic_notification_connecting = 2130837799;
    public static final int cast_ic_notification_on = 2130837800;
    public static final int common_full_open_on_phone = 2130837876;
    public static final int common_google_signin_btn_icon_dark = 2130837877;
    public static final int common_google_signin_btn_icon_dark_disabled = 2130837878;
    public static final int common_google_signin_btn_icon_dark_focused = 2130837879;
    public static final int common_google_signin_btn_icon_dark_normal = 2130837880;
    public static final int common_google_signin_btn_icon_dark_pressed = 2130837881;
    public static final int common_google_signin_btn_icon_light = 2130837882;
    public static final int common_google_signin_btn_icon_light_disabled = 2130837883;
    public static final int common_google_signin_btn_icon_light_focused = 2130837884;
    public static final int common_google_signin_btn_icon_light_normal = 2130837885;
    public static final int common_google_signin_btn_icon_light_pressed = 2130837886;
    public static final int common_google_signin_btn_text_dark = 2130837887;
    public static final int common_google_signin_btn_text_dark_disabled = 2130837888;
    public static final int common_google_signin_btn_text_dark_focused = 2130837889;
    public static final int common_google_signin_btn_text_dark_normal = 2130837890;
    public static final int common_google_signin_btn_text_dark_pressed = 2130837891;
    public static final int common_google_signin_btn_text_light = 2130837892;
    public static final int common_google_signin_btn_text_light_disabled = 2130837893;
    public static final int common_google_signin_btn_text_light_focused = 2130837894;
    public static final int common_google_signin_btn_text_light_normal = 2130837895;
    public static final int common_google_signin_btn_text_light_pressed = 2130837896;
    public static final int common_ic_googleplayservices = 2130837897;
    public static final int common_plus_signin_btn_icon_dark = 2130837898;
    public static final int common_plus_signin_btn_icon_dark_disabled = 2130837899;
    public static final int common_plus_signin_btn_icon_dark_focused = 2130837900;
    public static final int common_plus_signin_btn_icon_dark_normal = 2130837901;
    public static final int common_plus_signin_btn_icon_dark_pressed = 2130837902;
    public static final int common_plus_signin_btn_icon_light = 2130837903;
    public static final int common_plus_signin_btn_icon_light_disabled = 2130837904;
    public static final int common_plus_signin_btn_icon_light_focused = 2130837905;
    public static final int common_plus_signin_btn_icon_light_normal = 2130837906;
    public static final int common_plus_signin_btn_icon_light_pressed = 2130837907;
    public static final int common_plus_signin_btn_text_dark = 2130837908;
    public static final int common_plus_signin_btn_text_dark_disabled = 2130837909;
    public static final int common_plus_signin_btn_text_dark_focused = 2130837910;
    public static final int common_plus_signin_btn_text_dark_normal = 2130837911;
    public static final int common_plus_signin_btn_text_dark_pressed = 2130837912;
    public static final int common_plus_signin_btn_text_light = 2130837913;
    public static final int common_plus_signin_btn_text_light_disabled = 2130837914;
    public static final int common_plus_signin_btn_text_light_focused = 2130837915;
    public static final int common_plus_signin_btn_text_light_normal = 2130837916;
    public static final int common_plus_signin_btn_text_light_pressed = 2130837917;
    public static final int ic_plusone_medium_off_client = 2130838176;
    public static final int ic_plusone_small_off_client = 2130838177;
    public static final int ic_plusone_standard_off_client = 2130838178;
    public static final int ic_plusone_tall_off_client = 2130838179;
    public static final int places_ic_clear = 2130838535;
    public static final int places_ic_search = 2130838536;
    public static final int powered_by_google_dark = 2130838542;
    public static final int powered_by_google_light = 2130838543;
  }
  
  public static final class id
  {
    public static final int adjust_height = 2131689546;
    public static final int adjust_width = 2131689547;
    public static final int android_pay = 2131689592;
    public static final int android_pay_dark = 2131689583;
    public static final int android_pay_light = 2131689584;
    public static final int android_pay_light_with_border = 2131689585;
    public static final int auto = 2131689559;
    public static final int book_now = 2131689576;
    public static final int buyButton = 2131689573;
    public static final int buy_now = 2131689577;
    public static final int buy_with = 2131689578;
    public static final int buy_with_google = 2131689579;
    public static final int cast_notification_id = 2131689481;
    public static final int classic = 2131689586;
    public static final int dark = 2131689560;
    public static final int donate_with = 2131689580;
    public static final int donate_with_google = 2131689581;
    public static final int google_wallet_classic = 2131689587;
    public static final int google_wallet_grayscale = 2131689588;
    public static final int google_wallet_monochrome = 2131689589;
    public static final int grayscale = 2131689590;
    public static final int holo_dark = 2131689567;
    public static final int holo_light = 2131689568;
    public static final int hybrid = 2131689548;
    public static final int icon_only = 2131689556;
    public static final int light = 2131689561;
    public static final int logo_only = 2131689582;
    public static final int match_parent = 2131689575;
    public static final int monochrome = 2131689591;
    public static final int none = 2131689506;
    public static final int normal = 2131689502;
    public static final int place_autocomplete_clear_button = 2131694717;
    public static final int place_autocomplete_powered_by_google = 2131694719;
    public static final int place_autocomplete_prediction_primary_text = 2131694721;
    public static final int place_autocomplete_prediction_secondary_text = 2131694722;
    public static final int place_autocomplete_progress = 2131694720;
    public static final int place_autocomplete_search_button = 2131694715;
    public static final int place_autocomplete_search_input = 2131694716;
    public static final int place_autocomplete_separator = 2131694718;
    public static final int production = 2131689569;
    public static final int sandbox = 2131689570;
    public static final int satellite = 2131689549;
    public static final int selectionDetails = 2131689574;
    public static final int slide = 2131689542;
    public static final int standard = 2131689557;
    public static final int strict_sandbox = 2131689571;
    public static final int terrain = 2131689550;
    public static final int test = 2131689572;
    public static final int wide = 2131689558;
    public static final int wrap_content = 2131689516;
  }
  
  public static final class integer
  {
    public static final int google_play_services_version = 2131558415;
  }
  
  public static final class layout
  {
    public static final int place_autocomplete_fragment = 2130903991;
    public static final int place_autocomplete_item_powered_by_google = 2130903992;
    public static final int place_autocomplete_item_prediction = 2130903993;
    public static final int place_autocomplete_progress = 2130903994;
  }
  
  public static final class raw
  {
    public static final int gtm_analytics = 2131165188;
  }
  
  public static final class string
  {
    public static final int accept = 2131233928;
    public static final int auth_google_play_services_client_facebook_display_name = 2131233961;
    public static final int auth_google_play_services_client_google_display_name = 2131233962;
    public static final int cast_notification_connected_message = 2131233993;
    public static final int cast_notification_connecting_message = 2131233994;
    public static final int cast_notification_disconnect = 2131233995;
    public static final int common_google_play_services_api_unavailable_text = 2131230739;
    public static final int common_google_play_services_enable_button = 2131230740;
    public static final int common_google_play_services_enable_text = 2131230741;
    public static final int common_google_play_services_enable_title = 2131230742;
    public static final int common_google_play_services_install_button = 2131230743;
    public static final int common_google_play_services_install_text_phone = 2131230744;
    public static final int common_google_play_services_install_text_tablet = 2131230745;
    public static final int common_google_play_services_install_title = 2131230746;
    public static final int common_google_play_services_invalid_account_text = 2131230747;
    public static final int common_google_play_services_invalid_account_title = 2131230748;
    public static final int common_google_play_services_network_error_text = 2131230749;
    public static final int common_google_play_services_network_error_title = 2131230750;
    public static final int common_google_play_services_notification_ticker = 2131230751;
    public static final int common_google_play_services_restricted_profile_text = 2131230752;
    public static final int common_google_play_services_restricted_profile_title = 2131230753;
    public static final int common_google_play_services_sign_in_failed_text = 2131230754;
    public static final int common_google_play_services_sign_in_failed_title = 2131230755;
    public static final int common_google_play_services_unknown_issue = 2131230756;
    public static final int common_google_play_services_unsupported_text = 2131230757;
    public static final int common_google_play_services_unsupported_title = 2131230758;
    public static final int common_google_play_services_update_button = 2131230759;
    public static final int common_google_play_services_update_text = 2131230760;
    public static final int common_google_play_services_update_title = 2131230761;
    public static final int common_google_play_services_updating_text = 2131230762;
    public static final int common_google_play_services_updating_title = 2131230763;
    public static final int common_google_play_services_wear_update_text = 2131230764;
    public static final int common_open_on_phone = 2131230765;
    public static final int common_signin_button_text = 2131230766;
    public static final int common_signin_button_text_long = 2131230767;
    public static final int create_calendar_message = 2131234024;
    public static final int create_calendar_title = 2131234025;
    public static final int decline = 2131234041;
    public static final int place_autocomplete_clear_button = 2131230779;
    public static final int place_autocomplete_search_hint = 2131230780;
    public static final int store_picture_message = 2131234285;
    public static final int store_picture_title = 2131234286;
    public static final int wallet_buy_button_place_holder = 2131230782;
  }
  
  public static final class style
  {
    public static final int Theme_AppInvite_Preview = 2131624290;
    public static final int Theme_AppInvite_Preview_Base = 2131623972;
    public static final int Theme_IAPTheme = 2131624300;
    public static final int WalletFragmentDefaultButtonTextAppearance = 2131624317;
    public static final int WalletFragmentDefaultDetailsHeaderTextAppearance = 2131624318;
    public static final int WalletFragmentDefaultDetailsTextAppearance = 2131624319;
    public static final int WalletFragmentDefaultStyle = 2131624320;
  }
  
  public static final class styleable
  {
    public static final int[] AdsAttrs = { 2130772038, 2130772039, 2130772040 };
    public static final int AdsAttrs_adSize = 0;
    public static final int AdsAttrs_adSizes = 1;
    public static final int AdsAttrs_adUnitId = 2;
    public static final int[] CustomWalletTheme = { 2130772245 };
    public static final int CustomWalletTheme_windowTransitionStyle = 0;
    public static final int[] LoadingImageView = { 2130772284, 2130772285, 2130772286 };
    public static final int LoadingImageView_circleCrop = 2;
    public static final int LoadingImageView_imageAspectRatio = 1;
    public static final int LoadingImageView_imageAspectRatioAdjust = 0;
    public static final int[] MapAttrs = { 2130772287, 2130772288, 2130772289, 2130772290, 2130772291, 2130772292, 2130772293, 2130772294, 2130772295, 2130772296, 2130772297, 2130772298, 2130772299, 2130772300, 2130772301, 2130772302, 2130772303 };
    public static final int MapAttrs_ambientEnabled = 16;
    public static final int MapAttrs_cameraBearing = 1;
    public static final int MapAttrs_cameraTargetLat = 2;
    public static final int MapAttrs_cameraTargetLng = 3;
    public static final int MapAttrs_cameraTilt = 4;
    public static final int MapAttrs_cameraZoom = 5;
    public static final int MapAttrs_liteMode = 6;
    public static final int MapAttrs_mapType = 0;
    public static final int MapAttrs_uiCompass = 7;
    public static final int MapAttrs_uiMapToolbar = 15;
    public static final int MapAttrs_uiRotateGestures = 8;
    public static final int MapAttrs_uiScrollGestures = 9;
    public static final int MapAttrs_uiTiltGestures = 10;
    public static final int MapAttrs_uiZoomControls = 11;
    public static final int MapAttrs_uiZoomGestures = 12;
    public static final int MapAttrs_useViewLifecycle = 13;
    public static final int MapAttrs_zOrderOnTop = 14;
    public static final int[] SignInButton = { 2130772349, 2130772350, 2130772351 };
    public static final int SignInButton_buttonSize = 0;
    public static final int SignInButton_colorScheme = 1;
    public static final int SignInButton_scopeUris = 2;
    public static final int[] WalletFragmentOptions = { 2130772429, 2130772430, 2130772431, 2130772432 };
    public static final int WalletFragmentOptions_appTheme = 0;
    public static final int WalletFragmentOptions_environment = 1;
    public static final int WalletFragmentOptions_fragmentMode = 3;
    public static final int WalletFragmentOptions_fragmentStyle = 2;
    public static final int[] WalletFragmentStyle = { 2130772433, 2130772434, 2130772435, 2130772436, 2130772437, 2130772438, 2130772439, 2130772440, 2130772441, 2130772442, 2130772443 };
    public static final int WalletFragmentStyle_buyButtonAppearance = 3;
    public static final int WalletFragmentStyle_buyButtonHeight = 0;
    public static final int WalletFragmentStyle_buyButtonText = 2;
    public static final int WalletFragmentStyle_buyButtonWidth = 1;
    public static final int WalletFragmentStyle_maskedWalletDetailsBackground = 6;
    public static final int WalletFragmentStyle_maskedWalletDetailsButtonBackground = 8;
    public static final int WalletFragmentStyle_maskedWalletDetailsButtonTextAppearance = 7;
    public static final int WalletFragmentStyle_maskedWalletDetailsHeaderTextAppearance = 5;
    public static final int WalletFragmentStyle_maskedWalletDetailsLogoImageType = 10;
    public static final int WalletFragmentStyle_maskedWalletDetailsLogoTextColor = 9;
    public static final int WalletFragmentStyle_maskedWalletDetailsTextAppearance = 4;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/maps/R.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */