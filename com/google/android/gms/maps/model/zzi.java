package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzi
  implements Parcelable.Creator<PolylineOptions>
{
  static void a(PolylineOptions paramPolylineOptions, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramPolylineOptions.a());
    zzb.c(paramParcel, 2, paramPolylineOptions.b(), false);
    zzb.a(paramParcel, 3, paramPolylineOptions.c());
    zzb.a(paramParcel, 4, paramPolylineOptions.d());
    zzb.a(paramParcel, 5, paramPolylineOptions.e());
    zzb.a(paramParcel, 6, paramPolylineOptions.f());
    zzb.a(paramParcel, 7, paramPolylineOptions.g());
    zzb.a(paramParcel, 8, paramPolylineOptions.h());
    zzb.a(paramParcel, paramInt);
  }
  
  public PolylineOptions a(Parcel paramParcel)
  {
    float f1 = 0.0F;
    boolean bool1 = false;
    int k = zza.b(paramParcel);
    ArrayList localArrayList = null;
    boolean bool2 = false;
    boolean bool3 = false;
    int i = 0;
    float f2 = 0.0F;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        localArrayList = zza.c(paramParcel, m, LatLng.CREATOR);
        break;
      case 3: 
        f2 = zza.l(paramParcel, m);
        break;
      case 4: 
        i = zza.g(paramParcel, m);
        break;
      case 5: 
        f1 = zza.l(paramParcel, m);
        break;
      case 6: 
        bool3 = zza.c(paramParcel, m);
        break;
      case 7: 
        bool2 = zza.c(paramParcel, m);
        break;
      case 8: 
        bool1 = zza.c(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new PolylineOptions(j, localArrayList, f2, i, f1, bool3, bool2, bool1);
  }
  
  public PolylineOptions[] a(int paramInt)
  {
    return new PolylineOptions[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/maps/model/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */