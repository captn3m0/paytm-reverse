package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzc
  implements Parcelable.Creator<GroundOverlayOptions>
{
  static void a(GroundOverlayOptions paramGroundOverlayOptions, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramGroundOverlayOptions.b());
    zzb.a(paramParcel, 2, paramGroundOverlayOptions.a(), false);
    zzb.a(paramParcel, 3, paramGroundOverlayOptions.c(), paramInt, false);
    zzb.a(paramParcel, 4, paramGroundOverlayOptions.d());
    zzb.a(paramParcel, 5, paramGroundOverlayOptions.e());
    zzb.a(paramParcel, 6, paramGroundOverlayOptions.f(), paramInt, false);
    zzb.a(paramParcel, 7, paramGroundOverlayOptions.g());
    zzb.a(paramParcel, 8, paramGroundOverlayOptions.h());
    zzb.a(paramParcel, 9, paramGroundOverlayOptions.l());
    zzb.a(paramParcel, 10, paramGroundOverlayOptions.i());
    zzb.a(paramParcel, 11, paramGroundOverlayOptions.j());
    zzb.a(paramParcel, 12, paramGroundOverlayOptions.k());
    zzb.a(paramParcel, 13, paramGroundOverlayOptions.m());
    zzb.a(paramParcel, i);
  }
  
  public GroundOverlayOptions a(Parcel paramParcel)
  {
    int j = zza.b(paramParcel);
    int i = 0;
    IBinder localIBinder = null;
    LatLng localLatLng = null;
    float f7 = 0.0F;
    float f6 = 0.0F;
    LatLngBounds localLatLngBounds = null;
    float f5 = 0.0F;
    float f4 = 0.0F;
    boolean bool2 = false;
    float f3 = 0.0F;
    float f2 = 0.0F;
    float f1 = 0.0F;
    boolean bool1 = false;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localIBinder = zza.q(paramParcel, k);
        break;
      case 3: 
        localLatLng = (LatLng)zza.a(paramParcel, k, LatLng.CREATOR);
        break;
      case 4: 
        f7 = zza.l(paramParcel, k);
        break;
      case 5: 
        f6 = zza.l(paramParcel, k);
        break;
      case 6: 
        localLatLngBounds = (LatLngBounds)zza.a(paramParcel, k, LatLngBounds.CREATOR);
        break;
      case 7: 
        f5 = zza.l(paramParcel, k);
        break;
      case 8: 
        f4 = zza.l(paramParcel, k);
        break;
      case 9: 
        bool2 = zza.c(paramParcel, k);
        break;
      case 10: 
        f3 = zza.l(paramParcel, k);
        break;
      case 11: 
        f2 = zza.l(paramParcel, k);
        break;
      case 12: 
        f1 = zza.l(paramParcel, k);
        break;
      case 13: 
        bool1 = zza.c(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new GroundOverlayOptions(i, localIBinder, localLatLng, f7, f6, localLatLngBounds, f5, f4, bool2, f3, f2, f1, bool1);
  }
  
  public GroundOverlayOptions[] a(int paramInt)
  {
    return new GroundOverlayOptions[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/maps/model/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */