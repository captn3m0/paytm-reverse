package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzf
  implements Parcelable.Creator<MarkerOptions>
{
  static void a(MarkerOptions paramMarkerOptions, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramMarkerOptions.a());
    zzb.a(paramParcel, 2, paramMarkerOptions.c(), paramInt, false);
    zzb.a(paramParcel, 3, paramMarkerOptions.d(), false);
    zzb.a(paramParcel, 4, paramMarkerOptions.e(), false);
    zzb.a(paramParcel, 5, paramMarkerOptions.b(), false);
    zzb.a(paramParcel, 6, paramMarkerOptions.g());
    zzb.a(paramParcel, 7, paramMarkerOptions.h());
    zzb.a(paramParcel, 8, paramMarkerOptions.i());
    zzb.a(paramParcel, 9, paramMarkerOptions.j());
    zzb.a(paramParcel, 10, paramMarkerOptions.k());
    zzb.a(paramParcel, 11, paramMarkerOptions.l());
    zzb.a(paramParcel, 12, paramMarkerOptions.m());
    zzb.a(paramParcel, 13, paramMarkerOptions.n());
    zzb.a(paramParcel, 14, paramMarkerOptions.o());
    zzb.a(paramParcel, i);
  }
  
  public MarkerOptions a(Parcel paramParcel)
  {
    int j = zza.b(paramParcel);
    int i = 0;
    LatLng localLatLng = null;
    String str2 = null;
    String str1 = null;
    IBinder localIBinder = null;
    float f6 = 0.0F;
    float f5 = 0.0F;
    boolean bool3 = false;
    boolean bool2 = false;
    boolean bool1 = false;
    float f4 = 0.0F;
    float f3 = 0.5F;
    float f2 = 0.0F;
    float f1 = 1.0F;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localLatLng = (LatLng)zza.a(paramParcel, k, LatLng.CREATOR);
        break;
      case 3: 
        str2 = zza.p(paramParcel, k);
        break;
      case 4: 
        str1 = zza.p(paramParcel, k);
        break;
      case 5: 
        localIBinder = zza.q(paramParcel, k);
        break;
      case 6: 
        f6 = zza.l(paramParcel, k);
        break;
      case 7: 
        f5 = zza.l(paramParcel, k);
        break;
      case 8: 
        bool3 = zza.c(paramParcel, k);
        break;
      case 9: 
        bool2 = zza.c(paramParcel, k);
        break;
      case 10: 
        bool1 = zza.c(paramParcel, k);
        break;
      case 11: 
        f4 = zza.l(paramParcel, k);
        break;
      case 12: 
        f3 = zza.l(paramParcel, k);
        break;
      case 13: 
        f2 = zza.l(paramParcel, k);
        break;
      case 14: 
        f1 = zza.l(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new MarkerOptions(i, localLatLng, str2, str1, localIBinder, f6, f5, bool3, bool2, bool1, f4, f3, f2, f1);
  }
  
  public MarkerOptions[] a(int paramInt)
  {
    return new MarkerOptions[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/maps/model/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */