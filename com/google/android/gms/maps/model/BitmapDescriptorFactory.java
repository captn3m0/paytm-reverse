package com.google.android.gms.maps.model;

import android.graphics.Bitmap;
import android.os.RemoteException;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.maps.model.internal.zza;

public final class BitmapDescriptorFactory
{
  private static zza a;
  
  public static BitmapDescriptor a(Bitmap paramBitmap)
  {
    try
    {
      paramBitmap = new BitmapDescriptor(a().a(paramBitmap));
      return paramBitmap;
    }
    catch (RemoteException paramBitmap)
    {
      throw new RuntimeRemoteException(paramBitmap);
    }
  }
  
  private static zza a()
  {
    return (zza)zzx.a(a, "IBitmapDescriptorFactory is not initialized");
  }
  
  public static void a(zza paramzza)
  {
    if (a != null) {
      return;
    }
    a = (zza)zzx.a(paramzza);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/maps/model/BitmapDescriptorFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */