package com.google.android.gms.maps.model;

import android.os.RemoteException;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.maps.model.internal.IPolylineDelegate;

public final class Polyline
{
  private final IPolylineDelegate a;
  
  public Polyline(IPolylineDelegate paramIPolylineDelegate)
  {
    this.a = ((IPolylineDelegate)zzx.a(paramIPolylineDelegate));
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof Polyline)) {
      return false;
    }
    try
    {
      boolean bool = this.a.a(((Polyline)paramObject).a);
      return bool;
    }
    catch (RemoteException paramObject)
    {
      throw new RuntimeRemoteException((RemoteException)paramObject);
    }
  }
  
  public int hashCode()
  {
    try
    {
      int i = this.a.i();
      return i;
    }
    catch (RemoteException localRemoteException)
    {
      throw new RuntimeRemoteException(localRemoteException);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/maps/model/Polyline.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */