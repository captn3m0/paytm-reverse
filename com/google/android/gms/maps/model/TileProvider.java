package com.google.android.gms.maps.model;

public abstract interface TileProvider
{
  public static final Tile b = new Tile(-1, -1, null);
  
  public abstract Tile a(int paramInt1, int paramInt2, int paramInt3);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/maps/model/TileProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */