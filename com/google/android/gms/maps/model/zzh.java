package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzh
  implements Parcelable.Creator<PolygonOptions>
{
  static void a(PolygonOptions paramPolygonOptions, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramPolygonOptions.a());
    zzb.c(paramParcel, 2, paramPolygonOptions.c(), false);
    zzb.d(paramParcel, 3, paramPolygonOptions.b(), false);
    zzb.a(paramParcel, 4, paramPolygonOptions.d());
    zzb.a(paramParcel, 5, paramPolygonOptions.e());
    zzb.a(paramParcel, 6, paramPolygonOptions.f());
    zzb.a(paramParcel, 7, paramPolygonOptions.g());
    zzb.a(paramParcel, 8, paramPolygonOptions.h());
    zzb.a(paramParcel, 9, paramPolygonOptions.i());
    zzb.a(paramParcel, 10, paramPolygonOptions.j());
    zzb.a(paramParcel, paramInt);
  }
  
  public PolygonOptions a(Parcel paramParcel)
  {
    float f1 = 0.0F;
    boolean bool1 = false;
    int m = zza.b(paramParcel);
    ArrayList localArrayList1 = null;
    ArrayList localArrayList2 = new ArrayList();
    boolean bool2 = false;
    boolean bool3 = false;
    int i = 0;
    int j = 0;
    float f2 = 0.0F;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        localArrayList1 = zza.c(paramParcel, n, LatLng.CREATOR);
        break;
      case 3: 
        zza.a(paramParcel, n, localArrayList2, getClass().getClassLoader());
        break;
      case 4: 
        f2 = zza.l(paramParcel, n);
        break;
      case 5: 
        j = zza.g(paramParcel, n);
        break;
      case 6: 
        i = zza.g(paramParcel, n);
        break;
      case 7: 
        f1 = zza.l(paramParcel, n);
        break;
      case 8: 
        bool3 = zza.c(paramParcel, n);
        break;
      case 9: 
        bool2 = zza.c(paramParcel, n);
        break;
      case 10: 
        bool1 = zza.c(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new PolygonOptions(k, localArrayList1, localArrayList2, f2, j, i, f1, bool3, bool2, bool1);
  }
  
  public PolygonOptions[] a(int paramInt)
  {
    return new PolygonOptions[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/maps/model/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */