package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzd
  implements Parcelable.Creator<LatLngBounds>
{
  static void a(LatLngBounds paramLatLngBounds, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramLatLngBounds.a());
    zzb.a(paramParcel, 2, paramLatLngBounds.a, paramInt, false);
    zzb.a(paramParcel, 3, paramLatLngBounds.b, paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public LatLngBounds a(Parcel paramParcel)
  {
    LatLng localLatLng1 = null;
    int j = zza.b(paramParcel);
    int i = 0;
    LatLng localLatLng2 = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
      }
      for (;;)
      {
        break;
        i = zza.g(paramParcel, k);
        continue;
        localLatLng2 = (LatLng)zza.a(paramParcel, k, LatLng.CREATOR);
        continue;
        localLatLng1 = (LatLng)zza.a(paramParcel, k, LatLng.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new LatLngBounds(i, localLatLng2, localLatLng1);
  }
  
  public LatLngBounds[] a(int paramInt)
  {
    return new LatLngBounds[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/maps/model/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */