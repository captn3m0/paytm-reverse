package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class zzb
  implements Parcelable.Creator<CircleOptions>
{
  static void a(CircleOptions paramCircleOptions, Parcel paramParcel, int paramInt)
  {
    int i = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramCircleOptions.a());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramCircleOptions.b(), paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 3, paramCircleOptions.c());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 4, paramCircleOptions.d());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 5, paramCircleOptions.e());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 6, paramCircleOptions.f());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 7, paramCircleOptions.g());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 8, paramCircleOptions.h());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, i);
  }
  
  public CircleOptions a(Parcel paramParcel)
  {
    float f1 = 0.0F;
    boolean bool = false;
    int m = zza.b(paramParcel);
    LatLng localLatLng = null;
    double d = 0.0D;
    int i = 0;
    int j = 0;
    float f2 = 0.0F;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        localLatLng = (LatLng)zza.a(paramParcel, n, LatLng.CREATOR);
        break;
      case 3: 
        d = zza.n(paramParcel, n);
        break;
      case 4: 
        f2 = zza.l(paramParcel, n);
        break;
      case 5: 
        j = zza.g(paramParcel, n);
        break;
      case 6: 
        i = zza.g(paramParcel, n);
        break;
      case 7: 
        f1 = zza.l(paramParcel, n);
        break;
      case 8: 
        bool = zza.c(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new CircleOptions(k, localLatLng, d, f2, j, i, f1, bool);
  }
  
  public CircleOptions[] a(int paramInt)
  {
    return new CircleOptions[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/maps/model/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */