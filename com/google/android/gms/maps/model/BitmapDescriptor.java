package com.google.android.gms.maps.model;

import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.dynamic.zzd;

public final class BitmapDescriptor
{
  private final zzd a;
  
  public BitmapDescriptor(zzd paramzzd)
  {
    this.a = ((zzd)zzx.a(paramzzd));
  }
  
  public zzd a()
  {
    return this.a;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/maps/model/BitmapDescriptor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */