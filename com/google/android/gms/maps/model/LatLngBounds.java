package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.common.internal.zzx;

public final class LatLngBounds
  implements SafeParcelable
{
  public static final zzd CREATOR = new zzd();
  public final LatLng a;
  public final LatLng b;
  private final int c;
  
  LatLngBounds(int paramInt, LatLng paramLatLng1, LatLng paramLatLng2)
  {
    zzx.a(paramLatLng1, "null southwest");
    zzx.a(paramLatLng2, "null northeast");
    if (paramLatLng2.a >= paramLatLng1.a) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.b(bool, "southern latitude exceeds northern latitude (%s > %s)", new Object[] { Double.valueOf(paramLatLng1.a), Double.valueOf(paramLatLng2.a) });
      this.c = paramInt;
      this.a = paramLatLng1;
      this.b = paramLatLng2;
      return;
    }
  }
  
  public LatLngBounds(LatLng paramLatLng1, LatLng paramLatLng2)
  {
    this(1, paramLatLng1, paramLatLng2);
  }
  
  private boolean a(double paramDouble)
  {
    return (this.a.a <= paramDouble) && (paramDouble <= this.b.a);
  }
  
  public static Builder b()
  {
    return new Builder();
  }
  
  private boolean b(double paramDouble)
  {
    boolean bool = false;
    if (this.a.b <= this.b.b) {
      return (this.a.b <= paramDouble) && (paramDouble <= this.b.b);
    }
    if ((this.a.b <= paramDouble) || (paramDouble <= this.b.b)) {
      bool = true;
    }
    return bool;
  }
  
  private static double c(double paramDouble1, double paramDouble2)
  {
    return (paramDouble1 - paramDouble2 + 360.0D) % 360.0D;
  }
  
  private static double d(double paramDouble1, double paramDouble2)
  {
    return (paramDouble2 - paramDouble1 + 360.0D) % 360.0D;
  }
  
  int a()
  {
    return this.c;
  }
  
  public boolean a(LatLng paramLatLng)
  {
    return (a(paramLatLng.a)) && (b(paramLatLng.b));
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof LatLngBounds)) {
        return false;
      }
      paramObject = (LatLngBounds)paramObject;
    } while ((this.a.equals(((LatLngBounds)paramObject).a)) && (this.b.equals(((LatLngBounds)paramObject).b)));
    return false;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.a, this.b });
  }
  
  public String toString()
  {
    return zzw.a(this).a("southwest", this.a).a("northeast", this.b).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd.a(this, paramParcel, paramInt);
  }
  
  public static final class Builder
  {
    private double a = Double.POSITIVE_INFINITY;
    private double b = Double.NEGATIVE_INFINITY;
    private double c = NaN.0D;
    private double d = NaN.0D;
    
    private boolean a(double paramDouble)
    {
      boolean bool = false;
      if (this.c <= this.d) {
        return (this.c <= paramDouble) && (paramDouble <= this.d);
      }
      if ((this.c <= paramDouble) || (paramDouble <= this.d)) {
        bool = true;
      }
      return bool;
    }
    
    public Builder a(LatLng paramLatLng)
    {
      this.a = Math.min(this.a, paramLatLng.a);
      this.b = Math.max(this.b, paramLatLng.a);
      double d1 = paramLatLng.b;
      if (Double.isNaN(this.c))
      {
        this.c = d1;
        this.d = d1;
      }
      while (a(d1)) {
        return this;
      }
      if (LatLngBounds.a(this.c, d1) < LatLngBounds.b(this.d, d1))
      {
        this.c = d1;
        return this;
      }
      this.d = d1;
      return this;
    }
    
    public LatLngBounds a()
    {
      if (!Double.isNaN(this.c)) {}
      for (boolean bool = true;; bool = false)
      {
        zzx.a(bool, "no included points");
        return new LatLngBounds(new LatLng(this.a, this.c), new LatLng(this.b, this.d));
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/maps/model/LatLngBounds.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */