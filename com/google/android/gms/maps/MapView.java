package com.google.android.gms.maps;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.dynamic.zza;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.dynamic.zzf;
import com.google.android.gms.maps.internal.IGoogleMapDelegate;
import com.google.android.gms.maps.internal.IMapViewDelegate;
import com.google.android.gms.maps.internal.MapLifecycleDelegate;
import com.google.android.gms.maps.internal.zzad;
import com.google.android.gms.maps.internal.zzc;
import com.google.android.gms.maps.internal.zzo.zza;
import com.google.android.gms.maps.model.RuntimeRemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MapView
  extends FrameLayout
{
  private final zzb a;
  private GoogleMap b;
  
  public MapView(Context paramContext)
  {
    super(paramContext);
    this.a = new zzb(this, paramContext, null);
    a();
  }
  
  public MapView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.a = new zzb(this, paramContext, GoogleMapOptions.a(paramContext, paramAttributeSet));
    a();
  }
  
  public MapView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.a = new zzb(this, paramContext, GoogleMapOptions.a(paramContext, paramAttributeSet));
    a();
  }
  
  private void a()
  {
    setClickable(true);
  }
  
  @Deprecated
  public final GoogleMap getMap()
  {
    if (this.b != null) {
      return this.b;
    }
    this.a.i();
    if (this.a.a() == null) {
      return null;
    }
    try
    {
      this.b = new GoogleMap(((zza)this.a.a()).h().a());
      return this.b;
    }
    catch (RemoteException localRemoteException)
    {
      throw new RuntimeRemoteException(localRemoteException);
    }
  }
  
  static class zza
    implements MapLifecycleDelegate
  {
    private final ViewGroup a;
    private final IMapViewDelegate b;
    private View c;
    
    public zza(ViewGroup paramViewGroup, IMapViewDelegate paramIMapViewDelegate)
    {
      this.b = ((IMapViewDelegate)zzx.a(paramIMapViewDelegate));
      this.a = ((ViewGroup)zzx.a(paramViewGroup));
    }
    
    public View a(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
    {
      throw new UnsupportedOperationException("onCreateView not allowed on MapViewDelegate");
    }
    
    public void a() {}
    
    public void a(Activity paramActivity, Bundle paramBundle1, Bundle paramBundle2)
    {
      throw new UnsupportedOperationException("onInflate not allowed on MapViewDelegate");
    }
    
    public void a(Bundle paramBundle)
    {
      try
      {
        this.b.a(paramBundle);
        this.c = ((View)zze.a(this.b.f()));
        this.a.removeAllViews();
        this.a.addView(this.c);
        return;
      }
      catch (RemoteException paramBundle)
      {
        throw new RuntimeRemoteException(paramBundle);
      }
    }
    
    public void a(final OnMapReadyCallback paramOnMapReadyCallback)
    {
      try
      {
        this.b.a(new zzo.zza()
        {
          public void a(IGoogleMapDelegate paramAnonymousIGoogleMapDelegate)
            throws RemoteException
          {
            paramOnMapReadyCallback.a(new GoogleMap(paramAnonymousIGoogleMapDelegate));
          }
        });
        return;
      }
      catch (RemoteException paramOnMapReadyCallback)
      {
        throw new RuntimeRemoteException(paramOnMapReadyCallback);
      }
    }
    
    public void b()
    {
      try
      {
        this.b.b();
        return;
      }
      catch (RemoteException localRemoteException)
      {
        throw new RuntimeRemoteException(localRemoteException);
      }
    }
    
    public void b(Bundle paramBundle)
    {
      try
      {
        this.b.b(paramBundle);
        return;
      }
      catch (RemoteException paramBundle)
      {
        throw new RuntimeRemoteException(paramBundle);
      }
    }
    
    public void c()
    {
      try
      {
        this.b.c();
        return;
      }
      catch (RemoteException localRemoteException)
      {
        throw new RuntimeRemoteException(localRemoteException);
      }
    }
    
    public void d() {}
    
    public void e()
    {
      throw new UnsupportedOperationException("onDestroyView not allowed on MapViewDelegate");
    }
    
    public void f()
    {
      try
      {
        this.b.d();
        return;
      }
      catch (RemoteException localRemoteException)
      {
        throw new RuntimeRemoteException(localRemoteException);
      }
    }
    
    public void g()
    {
      try
      {
        this.b.e();
        return;
      }
      catch (RemoteException localRemoteException)
      {
        throw new RuntimeRemoteException(localRemoteException);
      }
    }
    
    public IMapViewDelegate h()
    {
      return this.b;
    }
  }
  
  static class zzb
    extends zza<MapView.zza>
  {
    protected zzf<MapView.zza> a;
    private final ViewGroup b;
    private final Context c;
    private final GoogleMapOptions d;
    private final List<OnMapReadyCallback> e = new ArrayList();
    
    zzb(ViewGroup paramViewGroup, Context paramContext, GoogleMapOptions paramGoogleMapOptions)
    {
      this.b = paramViewGroup;
      this.c = paramContext;
      this.d = paramGoogleMapOptions;
    }
    
    protected void a(zzf<MapView.zza> paramzzf)
    {
      this.a = paramzzf;
      i();
    }
    
    public void i()
    {
      if ((this.a != null) && (a() == null)) {
        try
        {
          MapsInitializer.a(this.c);
          Object localObject = zzad.a(this.c).a(zze.a(this.c), this.d);
          if (localObject == null) {
            return;
          }
          this.a.a(new MapView.zza(this.b, (IMapViewDelegate)localObject));
          localObject = this.e.iterator();
          while (((Iterator)localObject).hasNext())
          {
            OnMapReadyCallback localOnMapReadyCallback = (OnMapReadyCallback)((Iterator)localObject).next();
            ((MapView.zza)a()).a(localOnMapReadyCallback);
          }
          return;
        }
        catch (RemoteException localRemoteException)
        {
          throw new RuntimeRemoteException(localRemoteException);
          this.e.clear();
          return;
        }
        catch (GooglePlayServicesNotAvailableException localGooglePlayServicesNotAvailableException) {}
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/maps/MapView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */