package com.google.android.gms.common;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.internal.zzac;
import com.google.android.gms.dynamic.zzg.zza;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class SignInButton
  extends FrameLayout
  implements View.OnClickListener
{
  private int a;
  private int b;
  private Scope[] c;
  private View d;
  private View.OnClickListener e = null;
  
  public SignInButton(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public SignInButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public SignInButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    a(paramContext, paramAttributeSet);
    setStyle(this.a, this.b, this.c);
  }
  
  private static Button a(Context paramContext, int paramInt1, int paramInt2, Scope[] paramArrayOfScope)
  {
    zzac localzzac = new zzac(paramContext);
    localzzac.a(paramContext.getResources(), paramInt1, paramInt2, paramArrayOfScope);
    return localzzac;
  }
  
  private void a(Context paramContext)
  {
    if (this.d != null) {
      removeView(this.d);
    }
    try
    {
      this.d = zzab.a(paramContext, this.a, this.b, this.c);
      addView(this.d);
      this.d.setEnabled(isEnabled());
      this.d.setOnClickListener(this);
      return;
    }
    catch (zzg.zza localzza)
    {
      for (;;)
      {
        Log.w("SignInButton", "Sign in button not found, using placeholder instead");
        this.d = a(paramContext, this.a, this.b, this.c);
      }
    }
  }
  
  /* Error */
  private void a(Context paramContext, AttributeSet paramAttributeSet)
  {
    // Byte code:
    //   0: iconst_0
    //   1: istore_3
    //   2: aload_1
    //   3: invokevirtual 105	android/content/Context:getTheme	()Landroid/content/res/Resources$Theme;
    //   6: aload_2
    //   7: getstatic 110	com/google/android/gms/R$styleable:SignInButton	[I
    //   10: iconst_0
    //   11: iconst_0
    //   12: invokevirtual 116	android/content/res/Resources$Theme:obtainStyledAttributes	(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
    //   15: astore_1
    //   16: aload_0
    //   17: aload_1
    //   18: getstatic 119	com/google/android/gms/R$styleable:SignInButton_buttonSize	I
    //   21: iconst_0
    //   22: invokevirtual 125	android/content/res/TypedArray:getInt	(II)I
    //   25: putfield 37	com/google/android/gms/common/SignInButton:a	I
    //   28: aload_0
    //   29: aload_1
    //   30: getstatic 128	com/google/android/gms/R$styleable:SignInButton_colorScheme	I
    //   33: iconst_2
    //   34: invokevirtual 125	android/content/res/TypedArray:getInt	(II)I
    //   37: putfield 39	com/google/android/gms/common/SignInButton:b	I
    //   40: aload_1
    //   41: getstatic 131	com/google/android/gms/R$styleable:SignInButton_scopeUris	I
    //   44: invokevirtual 135	android/content/res/TypedArray:getString	(I)Ljava/lang/String;
    //   47: astore_2
    //   48: aload_2
    //   49: ifnonnull +13 -> 62
    //   52: aload_0
    //   53: aconst_null
    //   54: putfield 41	com/google/android/gms/common/SignInButton:c	[Lcom/google/android/gms/common/api/Scope;
    //   57: aload_1
    //   58: invokevirtual 139	android/content/res/TypedArray:recycle	()V
    //   61: return
    //   62: aload_2
    //   63: invokevirtual 145	java/lang/String:trim	()Ljava/lang/String;
    //   66: ldc -109
    //   68: invokevirtual 151	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   71: astore_2
    //   72: aload_0
    //   73: aload_2
    //   74: arraylength
    //   75: anewarray 153	com/google/android/gms/common/api/Scope
    //   78: putfield 41	com/google/android/gms/common/SignInButton:c	[Lcom/google/android/gms/common/api/Scope;
    //   81: iload_3
    //   82: aload_2
    //   83: arraylength
    //   84: if_icmpge -27 -> 57
    //   87: aload_0
    //   88: getfield 41	com/google/android/gms/common/SignInButton:c	[Lcom/google/android/gms/common/api/Scope;
    //   91: iload_3
    //   92: new 153	com/google/android/gms/common/api/Scope
    //   95: dup
    //   96: aload_2
    //   97: iload_3
    //   98: aaload
    //   99: invokevirtual 156	java/lang/String:toString	()Ljava/lang/String;
    //   102: invokespecial 159	com/google/android/gms/common/api/Scope:<init>	(Ljava/lang/String;)V
    //   105: aastore
    //   106: iload_3
    //   107: iconst_1
    //   108: iadd
    //   109: istore_3
    //   110: goto -29 -> 81
    //   113: astore_2
    //   114: aload_1
    //   115: invokevirtual 139	android/content/res/TypedArray:recycle	()V
    //   118: aload_2
    //   119: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	120	0	this	SignInButton
    //   0	120	1	paramContext	Context
    //   0	120	2	paramAttributeSet	AttributeSet
    //   1	109	3	i	int
    // Exception table:
    //   from	to	target	type
    //   16	48	113	finally
    //   52	57	113	finally
    //   62	81	113	finally
    //   81	106	113	finally
  }
  
  public void onClick(View paramView)
  {
    if ((this.e != null) && (paramView == this.d)) {
      this.e.onClick(this);
    }
  }
  
  public void setColorScheme(int paramInt)
  {
    setStyle(this.a, paramInt, this.c);
  }
  
  public void setEnabled(boolean paramBoolean)
  {
    super.setEnabled(paramBoolean);
    this.d.setEnabled(paramBoolean);
  }
  
  public void setOnClickListener(View.OnClickListener paramOnClickListener)
  {
    this.e = paramOnClickListener;
    if (this.d != null) {
      this.d.setOnClickListener(this);
    }
  }
  
  public void setScopes(Scope[] paramArrayOfScope)
  {
    setStyle(this.a, this.b, paramArrayOfScope);
  }
  
  public void setSize(int paramInt)
  {
    setStyle(paramInt, this.b, this.c);
  }
  
  public void setStyle(int paramInt1, int paramInt2)
  {
    setStyle(paramInt1, paramInt2, this.c);
  }
  
  public void setStyle(int paramInt1, int paramInt2, Scope[] paramArrayOfScope)
  {
    this.a = paramInt1;
    this.b = paramInt2;
    this.c = paramArrayOfScope;
    a(getContext());
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ButtonSize {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ColorScheme {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/SignInButton.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */