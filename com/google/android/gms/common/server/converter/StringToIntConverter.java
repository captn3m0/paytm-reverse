package com.google.android.gms.common.server.converter;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.server.response.FastJsonResponse.zza;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public final class StringToIntConverter
  implements SafeParcelable, FastJsonResponse.zza<String, Integer>
{
  public static final zzb CREATOR = new zzb();
  private final int a;
  private final HashMap<String, Integer> b;
  private final HashMap<Integer, String> c;
  private final ArrayList<Entry> d;
  
  public StringToIntConverter()
  {
    this.a = 1;
    this.b = new HashMap();
    this.c = new HashMap();
    this.d = null;
  }
  
  StringToIntConverter(int paramInt, ArrayList<Entry> paramArrayList)
  {
    this.a = paramInt;
    this.b = new HashMap();
    this.c = new HashMap();
    this.d = null;
    a(paramArrayList);
  }
  
  private void a(ArrayList<Entry> paramArrayList)
  {
    paramArrayList = paramArrayList.iterator();
    while (paramArrayList.hasNext())
    {
      Entry localEntry = (Entry)paramArrayList.next();
      a(localEntry.b, localEntry.c);
    }
  }
  
  int a()
  {
    return this.a;
  }
  
  public StringToIntConverter a(String paramString, int paramInt)
  {
    this.b.put(paramString, Integer.valueOf(paramInt));
    this.c.put(Integer.valueOf(paramInt), paramString);
    return this;
  }
  
  public String a(Integer paramInteger)
  {
    String str = (String)this.c.get(paramInteger);
    paramInteger = str;
    if (str == null)
    {
      paramInteger = str;
      if (this.b.containsKey("gms_unknown")) {
        paramInteger = "gms_unknown";
      }
    }
    return paramInteger;
  }
  
  ArrayList<Entry> b()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.b.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      localArrayList.add(new Entry(str, ((Integer)this.b.get(str)).intValue()));
    }
    return localArrayList;
  }
  
  public int c()
  {
    return 7;
  }
  
  public int d()
  {
    return 0;
  }
  
  public int describeContents()
  {
    zzb localzzb = CREATOR;
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb localzzb = CREATOR;
    zzb.a(this, paramParcel, paramInt);
  }
  
  public static final class Entry
    implements SafeParcelable
  {
    public static final zzc CREATOR = new zzc();
    final int a;
    final String b;
    final int c;
    
    Entry(int paramInt1, String paramString, int paramInt2)
    {
      this.a = paramInt1;
      this.b = paramString;
      this.c = paramInt2;
    }
    
    Entry(String paramString, int paramInt)
    {
      this.a = 1;
      this.b = paramString;
      this.c = paramInt;
    }
    
    public int describeContents()
    {
      zzc localzzc = CREATOR;
      return 0;
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      zzc localzzc = CREATOR;
      zzc.a(this, paramParcel, paramInt);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/server/converter/StringToIntConverter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */