package com.google.android.gms.common.server.response;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzmn;
import com.google.android.gms.internal.zzmo;
import com.google.android.gms.internal.zznb;
import com.google.android.gms.internal.zznc;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class SafeParcelResponse
  extends FastJsonResponse
  implements SafeParcelable
{
  public static final zze CREATOR = new zze();
  private final int a;
  private final Parcel b;
  private final int c;
  private final FieldMappingDictionary d;
  private final String e;
  private int f;
  private int g;
  
  SafeParcelResponse(int paramInt, Parcel paramParcel, FieldMappingDictionary paramFieldMappingDictionary)
  {
    this.a = paramInt;
    this.b = ((Parcel)zzx.a(paramParcel));
    this.c = 2;
    this.d = paramFieldMappingDictionary;
    if (this.d == null) {}
    for (this.e = null;; this.e = this.d.e())
    {
      this.f = 2;
      return;
    }
  }
  
  private SafeParcelResponse(SafeParcelable paramSafeParcelable, FieldMappingDictionary paramFieldMappingDictionary, String paramString)
  {
    this.a = 1;
    this.b = Parcel.obtain();
    paramSafeParcelable.writeToParcel(this.b, 0);
    this.c = 1;
    this.d = ((FieldMappingDictionary)zzx.a(paramFieldMappingDictionary));
    this.e = ((String)zzx.a(paramString));
    this.f = 2;
  }
  
  public static <T extends FastJsonResponse,  extends SafeParcelable> SafeParcelResponse a(T paramT)
  {
    String str = paramT.getClass().getCanonicalName();
    FieldMappingDictionary localFieldMappingDictionary = b(paramT);
    return new SafeParcelResponse((SafeParcelable)paramT, localFieldMappingDictionary, str);
  }
  
  public static HashMap<String, String> a(Bundle paramBundle)
  {
    HashMap localHashMap = new HashMap();
    Iterator localIterator = paramBundle.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      localHashMap.put(str, paramBundle.getString(str));
    }
    return localHashMap;
  }
  
  private static HashMap<Integer, Map.Entry<String, FastJsonResponse.Field<?, ?>>> a(Map<String, FastJsonResponse.Field<?, ?>> paramMap)
  {
    HashMap localHashMap = new HashMap();
    paramMap = paramMap.entrySet().iterator();
    while (paramMap.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)paramMap.next();
      localHashMap.put(Integer.valueOf(((FastJsonResponse.Field)localEntry.getValue()).h()), localEntry);
    }
    return localHashMap;
  }
  
  private static void a(FieldMappingDictionary paramFieldMappingDictionary, FastJsonResponse paramFastJsonResponse)
  {
    Object localObject = paramFastJsonResponse.getClass();
    if (!paramFieldMappingDictionary.a((Class)localObject))
    {
      Map localMap = paramFastJsonResponse.b();
      paramFieldMappingDictionary.a((Class)localObject, localMap);
      localObject = localMap.keySet().iterator();
      while (((Iterator)localObject).hasNext())
      {
        paramFastJsonResponse = (FastJsonResponse.Field)localMap.get((String)((Iterator)localObject).next());
        Class localClass = paramFastJsonResponse.i();
        if (localClass != null) {
          try
          {
            a(paramFieldMappingDictionary, (FastJsonResponse)localClass.newInstance());
          }
          catch (InstantiationException paramFieldMappingDictionary)
          {
            throw new IllegalStateException("Could not instantiate an object of type " + paramFastJsonResponse.i().getCanonicalName(), paramFieldMappingDictionary);
          }
          catch (IllegalAccessException paramFieldMappingDictionary)
          {
            throw new IllegalStateException("Could not access object of type " + paramFastJsonResponse.i().getCanonicalName(), paramFieldMappingDictionary);
          }
        }
      }
    }
  }
  
  private void a(StringBuilder paramStringBuilder, int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      throw new IllegalArgumentException("Unknown type = " + paramInt);
    case 0: 
    case 1: 
    case 2: 
    case 3: 
    case 4: 
    case 5: 
    case 6: 
      paramStringBuilder.append(paramObject);
      return;
    case 7: 
      paramStringBuilder.append("\"").append(zznb.a(paramObject.toString())).append("\"");
      return;
    case 8: 
      paramStringBuilder.append("\"").append(zzmo.a((byte[])paramObject)).append("\"");
      return;
    case 9: 
      paramStringBuilder.append("\"").append(zzmo.b((byte[])paramObject));
      paramStringBuilder.append("\"");
      return;
    case 10: 
      zznc.a(paramStringBuilder, (HashMap)paramObject);
      return;
    }
    throw new IllegalArgumentException("Method does not accept concrete type.");
  }
  
  private void a(StringBuilder paramStringBuilder, FastJsonResponse.Field<?, ?> paramField, Parcel paramParcel, int paramInt)
  {
    switch (paramField.e())
    {
    default: 
      throw new IllegalArgumentException("Unknown field out type = " + paramField.e());
    case 0: 
      a(paramStringBuilder, paramField, a(paramField, Integer.valueOf(zza.g(paramParcel, paramInt))));
      return;
    case 1: 
      a(paramStringBuilder, paramField, a(paramField, zza.k(paramParcel, paramInt)));
      return;
    case 2: 
      a(paramStringBuilder, paramField, a(paramField, Long.valueOf(zza.i(paramParcel, paramInt))));
      return;
    case 3: 
      a(paramStringBuilder, paramField, a(paramField, Float.valueOf(zza.l(paramParcel, paramInt))));
      return;
    case 4: 
      a(paramStringBuilder, paramField, a(paramField, Double.valueOf(zza.n(paramParcel, paramInt))));
      return;
    case 5: 
      a(paramStringBuilder, paramField, a(paramField, zza.o(paramParcel, paramInt)));
      return;
    case 6: 
      a(paramStringBuilder, paramField, a(paramField, Boolean.valueOf(zza.c(paramParcel, paramInt))));
      return;
    case 7: 
      a(paramStringBuilder, paramField, a(paramField, zza.p(paramParcel, paramInt)));
      return;
    case 8: 
    case 9: 
      a(paramStringBuilder, paramField, a(paramField, zza.s(paramParcel, paramInt)));
      return;
    case 10: 
      a(paramStringBuilder, paramField, a(paramField, a(zza.r(paramParcel, paramInt))));
      return;
    }
    throw new IllegalArgumentException("Method does not accept concrete type.");
  }
  
  private void a(StringBuilder paramStringBuilder, FastJsonResponse.Field<?, ?> paramField, Object paramObject)
  {
    if (paramField.d())
    {
      a(paramStringBuilder, paramField, (ArrayList)paramObject);
      return;
    }
    a(paramStringBuilder, paramField.c(), paramObject);
  }
  
  private void a(StringBuilder paramStringBuilder, FastJsonResponse.Field<?, ?> paramField, ArrayList<?> paramArrayList)
  {
    paramStringBuilder.append("[");
    int j = paramArrayList.size();
    int i = 0;
    while (i < j)
    {
      if (i != 0) {
        paramStringBuilder.append(",");
      }
      a(paramStringBuilder, paramField.c(), paramArrayList.get(i));
      i += 1;
    }
    paramStringBuilder.append("]");
  }
  
  private void a(StringBuilder paramStringBuilder, String paramString, FastJsonResponse.Field<?, ?> paramField, Parcel paramParcel, int paramInt)
  {
    paramStringBuilder.append("\"").append(paramString).append("\":");
    if (paramField.k())
    {
      a(paramStringBuilder, paramField, paramParcel, paramInt);
      return;
    }
    b(paramStringBuilder, paramField, paramParcel, paramInt);
  }
  
  private void a(StringBuilder paramStringBuilder, Map<String, FastJsonResponse.Field<?, ?>> paramMap, Parcel paramParcel)
  {
    paramMap = a(paramMap);
    paramStringBuilder.append('{');
    int j = zza.b(paramParcel);
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      Map.Entry localEntry = (Map.Entry)paramMap.get(Integer.valueOf(zza.a(k)));
      if (localEntry != null)
      {
        if (i != 0) {
          paramStringBuilder.append(",");
        }
        a(paramStringBuilder, (String)localEntry.getKey(), (FastJsonResponse.Field)localEntry.getValue(), paramParcel, k);
        i = 1;
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    paramStringBuilder.append('}');
  }
  
  private static FieldMappingDictionary b(FastJsonResponse paramFastJsonResponse)
  {
    FieldMappingDictionary localFieldMappingDictionary = new FieldMappingDictionary(paramFastJsonResponse.getClass());
    a(localFieldMappingDictionary, paramFastJsonResponse);
    localFieldMappingDictionary.b();
    localFieldMappingDictionary.a();
    return localFieldMappingDictionary;
  }
  
  private void b(StringBuilder paramStringBuilder, FastJsonResponse.Field<?, ?> paramField, Parcel paramParcel, int paramInt)
  {
    if (paramField.f())
    {
      paramStringBuilder.append("[");
      switch (paramField.e())
      {
      default: 
        throw new IllegalStateException("Unknown field type out.");
      case 0: 
        zzmn.a(paramStringBuilder, zza.v(paramParcel, paramInt));
      }
      for (;;)
      {
        paramStringBuilder.append("]");
        return;
        zzmn.a(paramStringBuilder, zza.x(paramParcel, paramInt));
        continue;
        zzmn.a(paramStringBuilder, zza.w(paramParcel, paramInt));
        continue;
        zzmn.a(paramStringBuilder, zza.y(paramParcel, paramInt));
        continue;
        zzmn.a(paramStringBuilder, zza.z(paramParcel, paramInt));
        continue;
        zzmn.a(paramStringBuilder, zza.A(paramParcel, paramInt));
        continue;
        zzmn.a(paramStringBuilder, zza.u(paramParcel, paramInt));
        continue;
        zzmn.a(paramStringBuilder, zza.B(paramParcel, paramInt));
        continue;
        throw new UnsupportedOperationException("List of type BASE64, BASE64_URL_SAFE, or STRING_MAP is not supported");
        paramParcel = zza.F(paramParcel, paramInt);
        int i = paramParcel.length;
        paramInt = 0;
        while (paramInt < i)
        {
          if (paramInt > 0) {
            paramStringBuilder.append(",");
          }
          paramParcel[paramInt].setDataPosition(0);
          a(paramStringBuilder, paramField.m(), paramParcel[paramInt]);
          paramInt += 1;
        }
      }
    }
    switch (paramField.e())
    {
    default: 
      throw new IllegalStateException("Unknown field type out");
    case 0: 
      paramStringBuilder.append(zza.g(paramParcel, paramInt));
      return;
    case 1: 
      paramStringBuilder.append(zza.k(paramParcel, paramInt));
      return;
    case 2: 
      paramStringBuilder.append(zza.i(paramParcel, paramInt));
      return;
    case 3: 
      paramStringBuilder.append(zza.l(paramParcel, paramInt));
      return;
    case 4: 
      paramStringBuilder.append(zza.n(paramParcel, paramInt));
      return;
    case 5: 
      paramStringBuilder.append(zza.o(paramParcel, paramInt));
      return;
    case 6: 
      paramStringBuilder.append(zza.c(paramParcel, paramInt));
      return;
    case 7: 
      paramField = zza.p(paramParcel, paramInt);
      paramStringBuilder.append("\"").append(zznb.a(paramField)).append("\"");
      return;
    case 8: 
      paramField = zza.s(paramParcel, paramInt);
      paramStringBuilder.append("\"").append(zzmo.a(paramField)).append("\"");
      return;
    case 9: 
      paramField = zza.s(paramParcel, paramInt);
      paramStringBuilder.append("\"").append(zzmo.b(paramField));
      paramStringBuilder.append("\"");
      return;
    case 10: 
      paramField = zza.r(paramParcel, paramInt);
      paramParcel = paramField.keySet();
      paramParcel.size();
      paramStringBuilder.append("{");
      paramParcel = paramParcel.iterator();
      for (paramInt = 1; paramParcel.hasNext(); paramInt = 0)
      {
        String str = (String)paramParcel.next();
        if (paramInt == 0) {
          paramStringBuilder.append(",");
        }
        paramStringBuilder.append("\"").append(str).append("\"");
        paramStringBuilder.append(":");
        paramStringBuilder.append("\"").append(zznb.a(paramField.getString(str))).append("\"");
      }
      paramStringBuilder.append("}");
      return;
    }
    paramParcel = zza.E(paramParcel, paramInt);
    paramParcel.setDataPosition(0);
    a(paramStringBuilder, paramField.m(), paramParcel);
  }
  
  public int a()
  {
    return this.a;
  }
  
  protected Object a(String paramString)
  {
    throw new UnsupportedOperationException("Converting to JSON does not require this method.");
  }
  
  public Map<String, FastJsonResponse.Field<?, ?>> b()
  {
    if (this.d == null) {
      return null;
    }
    return this.d.a(this.e);
  }
  
  protected boolean b(String paramString)
  {
    throw new UnsupportedOperationException("Converting to JSON does not require this method.");
  }
  
  public int describeContents()
  {
    zze localzze = CREATOR;
    return 0;
  }
  
  public Parcel e()
  {
    switch (this.f)
    {
    }
    for (;;)
    {
      return this.b;
      this.g = zzb.a(this.b);
      zzb.a(this.b, this.g);
      this.f = 2;
      continue;
      zzb.a(this.b, this.g);
      this.f = 2;
    }
  }
  
  FieldMappingDictionary f()
  {
    switch (this.c)
    {
    default: 
      throw new IllegalStateException("Invalid creation type: " + this.c);
    case 0: 
      return null;
    case 1: 
      return this.d;
    }
    return this.d;
  }
  
  public String toString()
  {
    zzx.a(this.d, "Cannot convert to JSON on client side.");
    Parcel localParcel = e();
    localParcel.setDataPosition(0);
    StringBuilder localStringBuilder = new StringBuilder(100);
    a(localStringBuilder, this.d.a(this.e), localParcel);
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zze localzze = CREATOR;
    zze.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/server/response/SafeParcelResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */