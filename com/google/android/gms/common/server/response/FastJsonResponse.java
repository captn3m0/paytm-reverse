package com.google.android.gms.common.server.response;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.common.server.converter.ConverterWrapper;
import com.google.android.gms.internal.zzmo;
import com.google.android.gms.internal.zznb;
import com.google.android.gms.internal.zznc;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public abstract class FastJsonResponse
{
  private void a(StringBuilder paramStringBuilder, Field paramField, Object paramObject)
  {
    if (paramField.c() == 11)
    {
      paramStringBuilder.append(((FastJsonResponse)paramField.i().cast(paramObject)).toString());
      return;
    }
    if (paramField.c() == 7)
    {
      paramStringBuilder.append("\"");
      paramStringBuilder.append(zznb.a((String)paramObject));
      paramStringBuilder.append("\"");
      return;
    }
    paramStringBuilder.append(paramObject);
  }
  
  private void a(StringBuilder paramStringBuilder, Field paramField, ArrayList<Object> paramArrayList)
  {
    paramStringBuilder.append("[");
    int i = 0;
    int j = paramArrayList.size();
    while (i < j)
    {
      if (i > 0) {
        paramStringBuilder.append(",");
      }
      Object localObject = paramArrayList.get(i);
      if (localObject != null) {
        a(paramStringBuilder, paramField, localObject);
      }
      i += 1;
    }
    paramStringBuilder.append("]");
  }
  
  protected <O, I> I a(Field<I, O> paramField, Object paramObject)
  {
    Object localObject = paramObject;
    if (Field.a(paramField) != null) {
      localObject = paramField.a(paramObject);
    }
    return (I)localObject;
  }
  
  protected abstract Object a(String paramString);
  
  protected boolean a(Field paramField)
  {
    if (paramField.e() == 11)
    {
      if (paramField.f()) {
        return d(paramField.g());
      }
      return c(paramField.g());
    }
    return b(paramField.g());
  }
  
  protected Object b(Field paramField)
  {
    String str = paramField.g();
    if (paramField.i() != null)
    {
      boolean bool;
      if (a(paramField.g()) == null)
      {
        bool = true;
        zzx.a(bool, "Concrete field shouldn't be value object: %s", new Object[] { paramField.g() });
        if (!paramField.f()) {
          break label71;
        }
      }
      label71:
      for (paramField = d();; paramField = c())
      {
        if (paramField == null) {
          break label79;
        }
        return paramField.get(str);
        bool = false;
        break;
      }
      try
      {
        label79:
        paramField = "get" + Character.toUpperCase(str.charAt(0)) + str.substring(1);
        paramField = getClass().getMethod(paramField, new Class[0]).invoke(this, new Object[0]);
        return paramField;
      }
      catch (Exception paramField)
      {
        throw new RuntimeException(paramField);
      }
    }
    return a(paramField.g());
  }
  
  public abstract Map<String, Field<?, ?>> b();
  
  protected abstract boolean b(String paramString);
  
  public HashMap<String, Object> c()
  {
    return null;
  }
  
  protected boolean c(String paramString)
  {
    throw new UnsupportedOperationException("Concrete types not supported");
  }
  
  public HashMap<String, Object> d()
  {
    return null;
  }
  
  protected boolean d(String paramString)
  {
    throw new UnsupportedOperationException("Concrete type arrays not supported");
  }
  
  public String toString()
  {
    Map localMap = b();
    StringBuilder localStringBuilder = new StringBuilder(100);
    Iterator localIterator = localMap.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      Field localField = (Field)localMap.get(str);
      if (a(localField))
      {
        Object localObject = a(localField, b(localField));
        if (localStringBuilder.length() == 0) {
          localStringBuilder.append("{");
        }
        for (;;)
        {
          localStringBuilder.append("\"").append(str).append("\":");
          if (localObject != null) {
            break label139;
          }
          localStringBuilder.append("null");
          break;
          localStringBuilder.append(",");
        }
        label139:
        switch (localField.e())
        {
        default: 
          if (localField.d()) {
            a(localStringBuilder, localField, (ArrayList)localObject);
          }
          break;
        case 8: 
          localStringBuilder.append("\"").append(zzmo.a((byte[])localObject)).append("\"");
          break;
        case 9: 
          localStringBuilder.append("\"").append(zzmo.b((byte[])localObject)).append("\"");
          break;
        case 10: 
          zznc.a(localStringBuilder, (HashMap)localObject);
          continue;
          a(localStringBuilder, localField, localObject);
        }
      }
    }
    if (localStringBuilder.length() > 0) {
      localStringBuilder.append("}");
    }
    for (;;)
    {
      return localStringBuilder.toString();
      localStringBuilder.append("{}");
    }
  }
  
  public static class Field<I, O>
    implements SafeParcelable
  {
    public static final zza CREATOR = new zza();
    protected final int a;
    protected final boolean b;
    protected final int c;
    protected final boolean d;
    protected final String e;
    protected final int f;
    protected final Class<? extends FastJsonResponse> g;
    protected final String h;
    private final int i;
    private FieldMappingDictionary j;
    private FastJsonResponse.zza<I, O> k;
    
    Field(int paramInt1, int paramInt2, boolean paramBoolean1, int paramInt3, boolean paramBoolean2, String paramString1, int paramInt4, String paramString2, ConverterWrapper paramConverterWrapper)
    {
      this.i = paramInt1;
      this.a = paramInt2;
      this.b = paramBoolean1;
      this.c = paramInt3;
      this.d = paramBoolean2;
      this.e = paramString1;
      this.f = paramInt4;
      if (paramString2 == null) {
        this.g = null;
      }
      for (this.h = null; paramConverterWrapper == null; this.h = paramString2)
      {
        this.k = null;
        return;
        this.g = SafeParcelResponse.class;
      }
      this.k = paramConverterWrapper.c();
    }
    
    protected Field(int paramInt1, boolean paramBoolean1, int paramInt2, boolean paramBoolean2, String paramString, int paramInt3, Class<? extends FastJsonResponse> paramClass, FastJsonResponse.zza<I, O> paramzza)
    {
      this.i = 1;
      this.a = paramInt1;
      this.b = paramBoolean1;
      this.c = paramInt2;
      this.d = paramBoolean2;
      this.e = paramString;
      this.f = paramInt3;
      this.g = paramClass;
      if (paramClass == null) {}
      for (this.h = null;; this.h = paramClass.getCanonicalName())
      {
        this.k = paramzza;
        return;
      }
    }
    
    public static Field<Integer, Integer> a(String paramString, int paramInt)
    {
      return new Field(0, false, 0, false, paramString, paramInt, null, null);
    }
    
    public static Field a(String paramString, int paramInt, FastJsonResponse.zza<?, ?> paramzza, boolean paramBoolean)
    {
      return new Field(paramzza.c(), paramBoolean, paramzza.d(), false, paramString, paramInt, null, paramzza);
    }
    
    public static <T extends FastJsonResponse> Field<T, T> a(String paramString, int paramInt, Class<T> paramClass)
    {
      return new Field(11, false, 11, false, paramString, paramInt, paramClass, null);
    }
    
    public static Field<Double, Double> b(String paramString, int paramInt)
    {
      return new Field(4, false, 4, false, paramString, paramInt, null, null);
    }
    
    public static <T extends FastJsonResponse> Field<ArrayList<T>, ArrayList<T>> b(String paramString, int paramInt, Class<T> paramClass)
    {
      return new Field(11, true, 11, true, paramString, paramInt, paramClass, null);
    }
    
    public static Field<Boolean, Boolean> c(String paramString, int paramInt)
    {
      return new Field(6, false, 6, false, paramString, paramInt, null, null);
    }
    
    public static Field<String, String> d(String paramString, int paramInt)
    {
      return new Field(7, false, 7, false, paramString, paramInt, null, null);
    }
    
    public static Field<ArrayList<String>, ArrayList<String>> e(String paramString, int paramInt)
    {
      return new Field(7, true, 7, true, paramString, paramInt, null, null);
    }
    
    public Field<I, O> a()
    {
      return new Field(this.i, this.a, this.b, this.c, this.d, this.e, this.f, this.h, l());
    }
    
    public I a(O paramO)
    {
      return (I)this.k.a(paramO);
    }
    
    public void a(FieldMappingDictionary paramFieldMappingDictionary)
    {
      this.j = paramFieldMappingDictionary;
    }
    
    public int b()
    {
      return this.i;
    }
    
    public int c()
    {
      return this.a;
    }
    
    public boolean d()
    {
      return this.b;
    }
    
    public int describeContents()
    {
      zza localzza = CREATOR;
      return 0;
    }
    
    public int e()
    {
      return this.c;
    }
    
    public boolean f()
    {
      return this.d;
    }
    
    public String g()
    {
      return this.e;
    }
    
    public int h()
    {
      return this.f;
    }
    
    public Class<? extends FastJsonResponse> i()
    {
      return this.g;
    }
    
    String j()
    {
      if (this.h == null) {
        return null;
      }
      return this.h;
    }
    
    public boolean k()
    {
      return this.k != null;
    }
    
    ConverterWrapper l()
    {
      if (this.k == null) {
        return null;
      }
      return ConverterWrapper.a(this.k);
    }
    
    public Map<String, Field<?, ?>> m()
    {
      zzx.a(this.h);
      zzx.a(this.j);
      return this.j.a(this.h);
    }
    
    public String toString()
    {
      StringBuilder localStringBuilder1 = new StringBuilder();
      localStringBuilder1.append("Field\n");
      localStringBuilder1.append("            versionCode=").append(this.i).append('\n');
      localStringBuilder1.append("                 typeIn=").append(this.a).append('\n');
      localStringBuilder1.append("            typeInArray=").append(this.b).append('\n');
      localStringBuilder1.append("                typeOut=").append(this.c).append('\n');
      localStringBuilder1.append("           typeOutArray=").append(this.d).append('\n');
      localStringBuilder1.append("        outputFieldName=").append(this.e).append('\n');
      localStringBuilder1.append("      safeParcelFieldId=").append(this.f).append('\n');
      localStringBuilder1.append("       concreteTypeName=").append(j()).append('\n');
      if (i() != null) {
        localStringBuilder1.append("     concreteType.class=").append(i().getCanonicalName()).append('\n');
      }
      StringBuilder localStringBuilder2 = localStringBuilder1.append("          converterName=");
      if (this.k == null) {}
      for (String str = "null";; str = this.k.getClass().getCanonicalName())
      {
        localStringBuilder2.append(str).append('\n');
        return localStringBuilder1.toString();
      }
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      zza localzza = CREATOR;
      zza.a(this, paramParcel, paramInt);
    }
  }
  
  public static abstract interface zza<I, O>
  {
    public abstract I a(O paramO);
    
    public abstract int c();
    
    public abstract int d();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/server/response/FastJsonResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */