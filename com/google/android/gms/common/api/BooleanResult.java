package com.google.android.gms.common.api;

import com.google.android.gms.common.internal.zzx;

public class BooleanResult
  implements Result
{
  private final Status a;
  private final boolean b;
  
  public BooleanResult(Status paramStatus, boolean paramBoolean)
  {
    this.a = ((Status)zzx.a(paramStatus, "Status must not be null"));
    this.b = paramBoolean;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof BooleanResult)) {
        return false;
      }
      paramObject = (BooleanResult)paramObject;
    } while ((this.a.equals(((BooleanResult)paramObject).a)) && (this.b == ((BooleanResult)paramObject).b));
    return false;
  }
  
  public Status getStatus()
  {
    return this.a;
  }
  
  public final int hashCode()
  {
    int j = this.a.hashCode();
    if (this.b) {}
    for (int i = 1;; i = 0) {
      return i + (j + 527) * 31;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/BooleanResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */