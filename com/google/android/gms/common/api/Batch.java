package com.google.android.gms.common.api;

import com.google.android.gms.common.api.internal.zzb;

public final class Batch
  extends zzb<BatchResult>
{
  private int a;
  private boolean b;
  private boolean c;
  private final PendingResult<?>[] d;
  private final Object e;
  
  public BatchResult a(Status paramStatus)
  {
    return new BatchResult(paramStatus, this.d);
  }
  
  public void cancel()
  {
    super.cancel();
    PendingResult[] arrayOfPendingResult = this.d;
    int j = arrayOfPendingResult.length;
    int i = 0;
    while (i < j)
    {
      arrayOfPendingResult[i].cancel();
      i += 1;
    }
  }
  
  public static final class Builder {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/Batch.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */