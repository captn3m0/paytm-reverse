package com.google.android.gms.common.api;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

public abstract class ResultTransform<R extends Result, S extends Result>
{
  @Nullable
  @WorkerThread
  public abstract PendingResult<S> a(@NonNull R paramR);
  
  @NonNull
  public Status a(@NonNull Status paramStatus)
  {
    return paramStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/ResultTransform.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */