package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import android.os.DeadObjectException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.zzb;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class zzg
  implements zzk
{
  private final zzl a;
  private boolean b = false;
  
  public zzg(zzl paramzzl)
  {
    this.a = paramzzl;
  }
  
  private <A extends Api.zzb> void a(zzj.zze<A> paramzze)
    throws DeadObjectException
  {
    this.a.g.a(paramzze);
    Api.zzb localzzb = this.a.g.a(paramzze.a());
    if ((!localzzb.k()) && (this.a.b.containsKey(paramzze.a())))
    {
      paramzze.b(new Status(17));
      return;
    }
    paramzze.b(localzzb);
  }
  
  public <A extends Api.zzb, R extends Result, T extends zza.zza<R, A>> T a(T paramT)
  {
    return b(paramT);
  }
  
  public void a() {}
  
  public void a(int paramInt)
  {
    this.a.a(null);
    this.a.h.a(paramInt, this.b);
  }
  
  public void a(Bundle paramBundle) {}
  
  public void a(ConnectionResult paramConnectionResult, Api<?> paramApi, int paramInt) {}
  
  public <A extends Api.zzb, T extends zza.zza<? extends Result, A>> T b(T paramT)
  {
    try
    {
      a(paramT);
      return paramT;
    }
    catch (DeadObjectException localDeadObjectException)
    {
      this.a.a(new zzl.zza(this)
      {
        public void a()
        {
          zzg.this.a(1);
        }
      });
    }
    return paramT;
  }
  
  public boolean b()
  {
    if (this.b) {
      return false;
    }
    if (this.a.g.o())
    {
      this.b = true;
      Iterator localIterator = this.a.g.i.iterator();
      while (localIterator.hasNext()) {
        ((zzx)localIterator.next()).a();
      }
      return false;
    }
    this.a.a(null);
    return true;
  }
  
  public void c()
  {
    if (this.b)
    {
      this.b = false;
      this.a.a(new zzl.zza(this)
      {
        public void a()
        {
          zzg.a(zzg.this).h.a(null);
        }
      });
    }
  }
  
  void d()
  {
    if (this.b)
    {
      this.b = false;
      this.a.g.a(false);
      b();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/internal/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */