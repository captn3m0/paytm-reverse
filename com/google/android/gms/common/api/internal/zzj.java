package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.Api.zzb;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.zza;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzk;
import com.google.android.gms.common.internal.zzk.zza;
import com.google.android.gms.internal.zzmf;
import com.google.android.gms.internal.zzmg;
import com.google.android.gms.internal.zzrn;
import com.google.android.gms.internal.zzro;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;

public final class zzj
  extends GoogleApiClient
  implements zzp.zza
{
  final Queue<zza.zza<?, ?>> a = new LinkedList();
  zzc b;
  final Map<Api.zzc<?>, Api.zzb> c;
  Set<Scope> d = new HashSet();
  final zzf e;
  final Map<Api<?>, Integer> f;
  final Api.zza<? extends zzrn, zzro> g;
  final Set<zze<?>> h = Collections.newSetFromMap(new ConcurrentHashMap(16, 0.75F, 2));
  Set<zzx> i = null;
  private final Lock j;
  private final zzk k;
  private zzp l = null;
  private final int m;
  private final Context n;
  private final Looper o;
  private volatile boolean p;
  private long q = 120000L;
  private long r = 5000L;
  private final zza s;
  private final com.google.android.gms.common.zzc t;
  private final Set<zzq<?>> u = Collections.newSetFromMap(new WeakHashMap());
  private zza v;
  private final ArrayList<zzc> w;
  private Integer x = null;
  private final zzd y = new zzd()
  {
    public void a(zzj.zze<?> paramAnonymouszze)
    {
      zzj.this.h.remove(paramAnonymouszze);
      if ((paramAnonymouszze.zzpa() != null) && (zzj.a(zzj.this) != null)) {
        zzj.a(zzj.this).a(paramAnonymouszze.zzpa().intValue());
      }
    }
  };
  private final zzk.zza z = new zzk.zza()
  {
    public boolean k()
    {
      return zzj.this.i();
    }
    
    public Bundle m_()
    {
      return null;
    }
  };
  
  public zzj(Context paramContext, Lock paramLock, Looper paramLooper, zzf paramzzf, com.google.android.gms.common.zzc paramzzc, Api.zza<? extends zzrn, zzro> paramzza, Map<Api<?>, Integer> paramMap, List<GoogleApiClient.ConnectionCallbacks> paramList, List<GoogleApiClient.OnConnectionFailedListener> paramList1, Map<Api.zzc<?>, Api.zzb> paramMap1, int paramInt1, int paramInt2, ArrayList<zzc> paramArrayList)
  {
    this.n = paramContext;
    this.j = paramLock;
    this.k = new zzk(paramLooper, this.z);
    this.o = paramLooper;
    this.s = new zza(paramLooper);
    this.t = paramzzc;
    this.m = paramInt1;
    if (this.m >= 0) {
      this.x = Integer.valueOf(paramInt2);
    }
    this.f = paramMap;
    this.c = paramMap1;
    this.w = paramArrayList;
    paramContext = paramList.iterator();
    while (paramContext.hasNext())
    {
      paramLock = (GoogleApiClient.ConnectionCallbacks)paramContext.next();
      this.k.a(paramLock);
    }
    paramContext = paramList1.iterator();
    while (paramContext.hasNext())
    {
      paramLock = (GoogleApiClient.OnConnectionFailedListener)paramContext.next();
      this.k.a(paramLock);
    }
    this.e = paramzzf;
    this.g = paramzza;
  }
  
  public static int a(Iterable<Api.zzb> paramIterable, boolean paramBoolean)
  {
    int i3 = 1;
    paramIterable = paramIterable.iterator();
    int i1 = 0;
    int i2 = 0;
    if (paramIterable.hasNext())
    {
      Api.zzb localzzb = (Api.zzb)paramIterable.next();
      if (localzzb.l()) {
        i2 = 1;
      }
      if (!localzzb.d()) {
        break label85;
      }
      i1 = 1;
    }
    label85:
    for (;;)
    {
      break;
      if (i2 != 0)
      {
        i2 = i3;
        if (i1 != 0)
        {
          i2 = i3;
          if (paramBoolean) {
            i2 = 2;
          }
        }
        return i2;
      }
      return 3;
    }
  }
  
  private void a(final GoogleApiClient paramGoogleApiClient, final zzv paramzzv, final boolean paramBoolean)
  {
    zzmf.c.a(paramGoogleApiClient).setResultCallback(new ResultCallback()
    {
      public void a(@NonNull Status paramAnonymousStatus)
      {
        com.google.android.gms.auth.api.signin.internal.zzq.a(zzj.d(zzj.this)).d();
        if ((paramAnonymousStatus.d()) && (zzj.this.i())) {
          zzj.this.k();
        }
        paramzzv.zza(paramAnonymousStatus);
        if (paramBoolean) {
          paramGoogleApiClient.g();
        }
      }
    });
  }
  
  private static void a(zze<?> paramzze, zza paramzza, IBinder paramIBinder)
  {
    if (paramzze.isReady())
    {
      paramzze.a(new zzb(paramzze, paramzza, paramIBinder, null));
      return;
    }
    if ((paramIBinder != null) && (paramIBinder.isBinderAlive()))
    {
      zzb localzzb = new zzb(paramzze, paramzza, paramIBinder, null);
      paramzze.a(localzzb);
      try
      {
        paramIBinder.linkToDeath(localzzb, 0);
        return;
      }
      catch (RemoteException paramIBinder)
      {
        paramzze.cancel();
        paramzza.a(paramzze.zzpa().intValue());
        return;
      }
    }
    paramzze.a(null);
    paramzze.cancel();
    paramzza.a(paramzze.zzpa().intValue());
  }
  
  static String b(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return "UNKNOWN";
    case 3: 
      return "SIGN_IN_MODE_NONE";
    case 1: 
      return "SIGN_IN_MODE_REQUIRED";
    }
    return "SIGN_IN_MODE_OPTIONAL";
  }
  
  private void c(int paramInt)
  {
    if (this.x == null) {
      this.x = Integer.valueOf(paramInt);
    }
    while (this.l != null)
    {
      return;
      if (this.x.intValue() != paramInt) {
        throw new IllegalStateException("Cannot use sign-in mode: " + b(paramInt) + ". Mode was already set to " + b(this.x.intValue()));
      }
    }
    Iterator localIterator = this.c.values().iterator();
    paramInt = 0;
    int i1 = 0;
    if (localIterator.hasNext())
    {
      Api.zzb localzzb = (Api.zzb)localIterator.next();
      if (localzzb.l()) {
        i1 = 1;
      }
      if (!localzzb.d()) {
        break label317;
      }
      paramInt = 1;
    }
    label317:
    for (;;)
    {
      break;
      switch (this.x.intValue())
      {
      }
      do
      {
        do
        {
          this.l = new zzl(this.n, this, this.j, this.o, this.t, this.c, this.e, this.f, this.g, this.w, this);
          return;
          if (i1 == 0) {
            throw new IllegalStateException("SIGN_IN_MODE_REQUIRED cannot be used on a GoogleApiClient that does not contain any authenticated APIs. Use connect() instead.");
          }
        } while (paramInt == 0);
        throw new IllegalStateException("Cannot use SIGN_IN_MODE_REQUIRED with GOOGLE_SIGN_IN_API. Use connect(SIGN_IN_MODE_OPTIONAL) instead.");
      } while (i1 == 0);
      this.l = new zzd(this.n, this, this.j, this.o, this.t, this.c, this.e, this.f, this.g, this.w);
      return;
    }
  }
  
  private void r()
  {
    this.k.b();
    this.l.a();
  }
  
  private void s()
  {
    this.j.lock();
    try
    {
      if (l()) {
        r();
      }
      return;
    }
    finally
    {
      this.j.unlock();
    }
  }
  
  private void t()
  {
    this.j.lock();
    try
    {
      if (n()) {
        r();
      }
      return;
    }
    finally
    {
      this.j.unlock();
    }
  }
  
  public ConnectionResult a(long paramLong, @NonNull TimeUnit paramTimeUnit)
  {
    boolean bool = false;
    if (Looper.myLooper() != Looper.getMainLooper()) {
      bool = true;
    }
    com.google.android.gms.common.internal.zzx.a(bool, "blockingConnect must not be called on the UI thread");
    com.google.android.gms.common.internal.zzx.a(paramTimeUnit, "TimeUnit must not be null");
    this.j.lock();
    try
    {
      if (this.x == null) {
        this.x = Integer.valueOf(a(this.c.values(), false));
      }
      while (this.x.intValue() != 2)
      {
        c(this.x.intValue());
        this.k.b();
        paramTimeUnit = this.l.a(paramLong, paramTimeUnit);
        return paramTimeUnit;
      }
      throw new IllegalStateException("Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
    }
    finally
    {
      this.j.unlock();
    }
  }
  
  @NonNull
  public <C extends Api.zzb> C a(@NonNull Api.zzc<C> paramzzc)
  {
    paramzzc = (Api.zzb)this.c.get(paramzzc);
    com.google.android.gms.common.internal.zzx.a(paramzzc, "Appropriate Api was not requested.");
    return paramzzc;
  }
  
  public <A extends Api.zzb, R extends Result, T extends zza.zza<R, A>> T a(@NonNull T paramT)
  {
    boolean bool;
    if (paramT.a() != null) {
      bool = true;
    }
    for (;;)
    {
      com.google.android.gms.common.internal.zzx.b(bool, "This task can not be enqueued (it's probably a Batch or malformed)");
      com.google.android.gms.common.internal.zzx.b(this.c.containsKey(paramT.a()), "GoogleApiClient is not configured to use the API required for this call.");
      this.j.lock();
      try
      {
        if (this.l == null)
        {
          this.a.add(paramT);
          return paramT;
          bool = false;
          continue;
        }
        paramT = this.l.a(paramT);
        return paramT;
      }
      finally
      {
        this.j.unlock();
      }
    }
  }
  
  public <L> zzq<L> a(@NonNull L paramL)
  {
    com.google.android.gms.common.internal.zzx.a(paramL, "Listener must not be null");
    this.j.lock();
    try
    {
      paramL = new zzq(this.o, paramL);
      this.u.add(paramL);
      return paramL;
    }
    finally
    {
      this.j.unlock();
    }
  }
  
  /* Error */
  public void a(int paramInt)
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_3
    //   2: aload_0
    //   3: getfield 149	com/google/android/gms/common/api/internal/zzj:j	Ljava/util/concurrent/locks/Lock;
    //   6: invokeinterface 358 1 0
    //   11: iload_3
    //   12: istore_2
    //   13: iload_1
    //   14: iconst_3
    //   15: if_icmpeq +17 -> 32
    //   18: iload_3
    //   19: istore_2
    //   20: iload_1
    //   21: iconst_1
    //   22: if_icmpeq +10 -> 32
    //   25: iload_1
    //   26: iconst_2
    //   27: if_icmpne +48 -> 75
    //   30: iload_3
    //   31: istore_2
    //   32: iload_2
    //   33: new 302	java/lang/StringBuilder
    //   36: dup
    //   37: invokespecial 303	java/lang/StringBuilder:<init>	()V
    //   40: ldc_w 445
    //   43: invokevirtual 309	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   46: iload_1
    //   47: invokevirtual 448	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   50: invokevirtual 317	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   53: invokestatic 417	com/google/android/gms/common/internal/zzx:b	(ZLjava/lang/Object;)V
    //   56: aload_0
    //   57: iload_1
    //   58: invokespecial 393	com/google/android/gms/common/api/internal/zzj:c	(I)V
    //   61: aload_0
    //   62: invokespecial 361	com/google/android/gms/common/api/internal/zzj:r	()V
    //   65: aload_0
    //   66: getfield 149	com/google/android/gms/common/api/internal/zzj:j	Ljava/util/concurrent/locks/Lock;
    //   69: invokeinterface 364 1 0
    //   74: return
    //   75: iconst_0
    //   76: istore_2
    //   77: goto -45 -> 32
    //   80: astore 4
    //   82: aload_0
    //   83: getfield 149	com/google/android/gms/common/api/internal/zzj:j	Ljava/util/concurrent/locks/Lock;
    //   86: invokeinterface 364 1 0
    //   91: aload 4
    //   93: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	94	0	this	zzj
    //   0	94	1	paramInt	int
    //   12	65	2	bool1	boolean
    //   1	30	3	bool2	boolean
    //   80	12	4	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   32	65	80	finally
  }
  
  public void a(int paramInt, boolean paramBoolean)
  {
    if ((paramInt == 1) && (!paramBoolean)) {
      m();
    }
    Iterator localIterator = this.h.iterator();
    while (localIterator.hasNext())
    {
      zze localzze = (zze)localIterator.next();
      if (paramBoolean) {
        localzze.b();
      }
      localzze.zzx(new Status(8, "The connection to Google Play services was lost"));
    }
    this.h.clear();
    this.k.a(paramInt);
    this.k.a();
    if (paramInt == 2) {
      r();
    }
  }
  
  public void a(Bundle paramBundle)
  {
    while (!this.a.isEmpty()) {
      b((zza.zza)this.a.remove());
    }
    this.k.a(paramBundle);
  }
  
  public void a(ConnectionResult paramConnectionResult)
  {
    if (!this.t.a(this.n, paramConnectionResult.c())) {
      n();
    }
    if (!l())
    {
      this.k.a(paramConnectionResult);
      this.k.a();
    }
  }
  
  public void a(@NonNull GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks)
  {
    this.k.a(paramConnectionCallbacks);
  }
  
  public void a(@NonNull GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    this.k.a(paramOnConnectionFailedListener);
  }
  
  <A extends Api.zzb> void a(zze<A> paramzze)
  {
    this.h.add(paramzze);
    paramzze.a(this.y);
  }
  
  public void a(zzx paramzzx)
  {
    this.j.lock();
    try
    {
      if (this.i == null) {
        this.i = new HashSet();
      }
      this.i.add(paramzzx);
      return;
    }
    finally
    {
      this.j.unlock();
    }
  }
  
  public void a(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
  {
    paramPrintWriter.append(paramString).append("mContext=").println(this.n);
    paramPrintWriter.append(paramString).append("mResuming=").print(this.p);
    paramPrintWriter.append(" mWorkQueue.size()=").print(this.a.size());
    paramPrintWriter.append(" mUnconsumedRunners.size()=").println(this.h.size());
    if (this.l != null) {
      this.l.a(paramString, paramFileDescriptor, paramPrintWriter, paramArrayOfString);
    }
  }
  
  void a(boolean paramBoolean)
  {
    Iterator localIterator = this.h.iterator();
    while (localIterator.hasNext())
    {
      zze localzze = (zze)localIterator.next();
      if (localzze.zzpa() == null)
      {
        if (paramBoolean)
        {
          localzze.zzpg();
        }
        else
        {
          localzze.cancel();
          this.h.remove(localzze);
        }
      }
      else
      {
        localzze.b();
        IBinder localIBinder = a(localzze.a()).m();
        a(localzze, this.v, localIBinder);
        this.h.remove(localzze);
      }
    }
  }
  
  public boolean a(zzu paramzzu)
  {
    return (this.l != null) && (this.l.a(paramzzu));
  }
  
  public Context b()
  {
    return this.n;
  }
  
  public <A extends Api.zzb, T extends zza.zza<? extends Result, A>> T b(@NonNull T paramT)
  {
    if (paramT.a() != null) {}
    for (boolean bool = true;; bool = false)
    {
      com.google.android.gms.common.internal.zzx.b(bool, "This task can not be executed (it's probably a Batch or malformed)");
      this.j.lock();
      try
      {
        if (this.l != null) {
          break;
        }
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
      }
      finally
      {
        this.j.unlock();
      }
    }
    if (l())
    {
      this.a.add(paramT);
      while (!this.a.isEmpty())
      {
        zze localzze = (zze)this.a.remove();
        a(localzze);
        localzze.b(Status.c);
      }
      this.j.unlock();
      return paramT;
    }
    paramT = this.l.b(paramT);
    this.j.unlock();
    return paramT;
  }
  
  public void b(@NonNull GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks)
  {
    this.k.b(paramConnectionCallbacks);
  }
  
  public void b(@NonNull GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    this.k.b(paramOnConnectionFailedListener);
  }
  
  public void b(zzx paramzzx)
  {
    this.j.lock();
    for (;;)
    {
      try
      {
        if (this.i == null)
        {
          Log.wtf("GoogleApiClientImpl", "Attempted to remove pending transform when no transforms are registered.", new Exception());
          return;
        }
        if (!this.i.remove(paramzzx))
        {
          Log.wtf("GoogleApiClientImpl", "Failed to remove pending transform - this may lead to memory leaks!", new Exception());
          continue;
        }
        if (o()) {
          continue;
        }
      }
      finally
      {
        this.j.unlock();
      }
      this.l.f();
    }
  }
  
  public Looper c()
  {
    return this.o;
  }
  
  public void d()
  {
    if (this.l != null) {
      this.l.g();
    }
  }
  
  public void e()
  {
    boolean bool = false;
    this.j.lock();
    try
    {
      if (this.m >= 0)
      {
        if (this.x != null) {
          bool = true;
        }
        com.google.android.gms.common.internal.zzx.a(bool, "Sign-in mode should have been set explicitly by auto-manage.");
      }
      do
      {
        for (;;)
        {
          a(this.x.intValue());
          return;
          if (this.x != null) {
            break;
          }
          this.x = Integer.valueOf(a(this.c.values(), false));
        }
      } while (this.x.intValue() != 2);
    }
    finally
    {
      this.j.unlock();
    }
    throw new IllegalStateException("Cannot call connect() when SignInMode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
  }
  
  public ConnectionResult f()
  {
    boolean bool2 = true;
    boolean bool1;
    if (Looper.myLooper() != Looper.getMainLooper()) {
      bool1 = true;
    }
    for (;;)
    {
      com.google.android.gms.common.internal.zzx.a(bool1, "blockingConnect must not be called on the UI thread");
      this.j.lock();
      try
      {
        if (this.m >= 0) {
          if (this.x != null)
          {
            bool1 = bool2;
            label45:
            com.google.android.gms.common.internal.zzx.a(bool1, "Sign-in mode should have been set explicitly by auto-manage.");
          }
        }
        do
        {
          for (;;)
          {
            c(this.x.intValue());
            this.k.b();
            ConnectionResult localConnectionResult = this.l.b();
            return localConnectionResult;
            bool1 = false;
            break;
            bool1 = false;
            break label45;
            if (this.x != null) {
              break label143;
            }
            this.x = Integer.valueOf(a(this.c.values(), false));
          }
        } while (this.x.intValue() != 2);
      }
      finally
      {
        this.j.unlock();
      }
    }
    label143:
    throw new IllegalStateException("Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
  }
  
  public void g()
  {
    this.j.lock();
    for (;;)
    {
      try
      {
        if ((this.l != null) && (!this.l.c()))
        {
          bool = true;
          a(bool);
          Iterator localIterator = this.u.iterator();
          if (!localIterator.hasNext()) {
            break;
          }
          ((zzq)localIterator.next()).a();
          continue;
        }
        boolean bool = false;
      }
      finally
      {
        this.j.unlock();
      }
    }
    this.u.clear();
    Object localObject2 = this.a.iterator();
    while (((Iterator)localObject2).hasNext())
    {
      zze localzze = (zze)((Iterator)localObject2).next();
      localzze.a(null);
      localzze.cancel();
    }
    this.a.clear();
    localObject2 = this.l;
    if (localObject2 == null)
    {
      this.j.unlock();
      return;
    }
    n();
    this.k.a();
    this.j.unlock();
  }
  
  public PendingResult<Status> h()
  {
    com.google.android.gms.common.internal.zzx.a(i(), "GoogleApiClient is not connected yet.");
    if (this.x.intValue() != 2) {}
    final zzv localzzv;
    for (boolean bool = true;; bool = false)
    {
      com.google.android.gms.common.internal.zzx.a(bool, "Cannot use clearDefaultAccountAndReconnect with GOOGLE_SIGN_IN_API");
      localzzv = new zzv(this);
      if (!this.c.containsKey(zzmf.a)) {
        break;
      }
      a(this, localzzv, false);
      return localzzv;
    }
    final AtomicReference localAtomicReference = new AtomicReference();
    Object localObject = new GoogleApiClient.ConnectionCallbacks()
    {
      public void onConnected(Bundle paramAnonymousBundle)
      {
        zzj.a(zzj.this, (GoogleApiClient)localAtomicReference.get(), localzzv, true);
      }
      
      public void onConnectionSuspended(int paramAnonymousInt) {}
    };
    GoogleApiClient.OnConnectionFailedListener local4 = new GoogleApiClient.OnConnectionFailedListener()
    {
      public void onConnectionFailed(@NonNull ConnectionResult paramAnonymousConnectionResult)
      {
        localzzv.zza(new Status(8));
      }
    };
    localObject = new GoogleApiClient.Builder(this.n).a(zzmf.b).a((GoogleApiClient.ConnectionCallbacks)localObject).a(local4).a(this.s).b();
    localAtomicReference.set(localObject);
    ((GoogleApiClient)localObject).e();
    return localzzv;
  }
  
  public boolean i()
  {
    return (this.l != null) && (this.l.d());
  }
  
  public void k()
  {
    g();
    e();
  }
  
  boolean l()
  {
    return this.p;
  }
  
  void m()
  {
    if (l()) {
      return;
    }
    this.p = true;
    if (this.b == null) {
      this.b = ((zzc)zzn.a(this.n.getApplicationContext(), new zzc(this), this.t));
    }
    this.s.sendMessageDelayed(this.s.obtainMessage(1), this.q);
    this.s.sendMessageDelayed(this.s.obtainMessage(2), this.r);
  }
  
  boolean n()
  {
    if (!l()) {
      return false;
    }
    this.p = false;
    this.s.removeMessages(2);
    this.s.removeMessages(1);
    if (this.b != null)
    {
      this.b.b();
      this.b = null;
    }
    return true;
  }
  
  boolean o()
  {
    boolean bool1 = false;
    this.j.lock();
    try
    {
      Set localSet = this.i;
      if (localSet == null) {
        return false;
      }
      boolean bool2 = this.i.isEmpty();
      if (!bool2) {
        bool1 = true;
      }
      return bool1;
    }
    finally
    {
      this.j.unlock();
    }
  }
  
  String p()
  {
    StringWriter localStringWriter = new StringWriter();
    a("", null, new PrintWriter(localStringWriter), null);
    return localStringWriter.toString();
  }
  
  public int q()
  {
    return System.identityHashCode(this);
  }
  
  final class zza
    extends Handler
  {
    zza(Looper paramLooper)
    {
      super();
    }
    
    public void handleMessage(Message paramMessage)
    {
      switch (paramMessage.what)
      {
      default: 
        Log.w("GoogleApiClientImpl", "Unknown message id: " + paramMessage.what);
        return;
      case 1: 
        zzj.c(zzj.this);
        return;
      }
      zzj.b(zzj.this);
    }
  }
  
  private static class zzb
    implements IBinder.DeathRecipient, zzj.zzd
  {
    private final WeakReference<zzj.zze<?>> a;
    private final WeakReference<zza> b;
    private final WeakReference<IBinder> c;
    
    private zzb(zzj.zze paramzze, zza paramzza, IBinder paramIBinder)
    {
      this.b = new WeakReference(paramzza);
      this.a = new WeakReference(paramzze);
      this.c = new WeakReference(paramIBinder);
    }
    
    private void a()
    {
      Object localObject = (zzj.zze)this.a.get();
      zza localzza = (zza)this.b.get();
      if ((localzza != null) && (localObject != null)) {
        localzza.a(((zzj.zze)localObject).zzpa().intValue());
      }
      localObject = (IBinder)this.c.get();
      if (this.c != null) {
        ((IBinder)localObject).unlinkToDeath(this, 0);
      }
    }
    
    public void a(zzj.zze<?> paramzze)
    {
      a();
    }
    
    public void binderDied()
    {
      a();
    }
  }
  
  static class zzc
    extends zzn
  {
    private WeakReference<zzj> a;
    
    zzc(zzj paramzzj)
    {
      this.a = new WeakReference(paramzzj);
    }
    
    public void a()
    {
      zzj localzzj = (zzj)this.a.get();
      if (localzzj == null) {
        return;
      }
      zzj.b(localzzj);
    }
  }
  
  static abstract interface zzd
  {
    public abstract void a(zzj.zze<?> paramzze);
  }
  
  static abstract interface zze<A extends Api.zzb>
  {
    public abstract Api.zzc<A> a();
    
    public abstract void a(zzj.zzd paramzzd);
    
    public abstract void b();
    
    public abstract void b(A paramA)
      throws DeadObjectException;
    
    public abstract void b(Status paramStatus);
    
    public abstract void cancel();
    
    public abstract boolean isReady();
    
    public abstract Integer zzpa();
    
    public abstract void zzpg();
    
    public abstract void zzx(Status paramStatus);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/internal/zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */