package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.data.DataHolder;

public abstract class zze<L>
  implements zzq.zzb<L>
{
  private final DataHolder a;
  
  protected zze(DataHolder paramDataHolder)
  {
    this.a = paramDataHolder;
  }
  
  public void a()
  {
    if (this.a != null) {
      this.a.i();
    }
  }
  
  public final void a(L paramL)
  {
    a(paramL, this.a);
  }
  
  protected abstract void a(L paramL, DataHolder paramDataHolder);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/internal/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */