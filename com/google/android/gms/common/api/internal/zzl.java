package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.Api.zzb;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.internal.zzrn;
import com.google.android.gms.internal.zzro;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class zzl
  implements zzp
{
  final Map<Api.zzc<?>, Api.zzb> a;
  final Map<Api.zzc<?>, ConnectionResult> b = new HashMap();
  final zzf c;
  final Map<Api<?>, Integer> d;
  final Api.zza<? extends zzrn, zzro> e;
  int f;
  final zzj g;
  final zzp.zza h;
  private final Lock i;
  private final Condition j;
  private final Context k;
  private final com.google.android.gms.common.zzc l;
  private final zzb m;
  private volatile zzk n;
  private ConnectionResult o = null;
  
  public zzl(Context paramContext, zzj paramzzj, Lock paramLock, Looper paramLooper, com.google.android.gms.common.zzc paramzzc, Map<Api.zzc<?>, Api.zzb> paramMap, zzf paramzzf, Map<Api<?>, Integer> paramMap1, Api.zza<? extends zzrn, zzro> paramzza, ArrayList<zzc> paramArrayList, zzp.zza paramzza1)
  {
    this.k = paramContext;
    this.i = paramLock;
    this.l = paramzzc;
    this.a = paramMap;
    this.c = paramzzf;
    this.d = paramMap1;
    this.e = paramzza;
    this.g = paramzzj;
    this.h = paramzza1;
    paramContext = paramArrayList.iterator();
    while (paramContext.hasNext()) {
      ((zzc)paramContext.next()).a(this);
    }
    this.m = new zzb(paramLooper);
    this.j = paramLock.newCondition();
    this.n = new zzi(this);
  }
  
  public ConnectionResult a(long paramLong, TimeUnit paramTimeUnit)
  {
    a();
    for (paramLong = paramTimeUnit.toNanos(paramLong); j(); paramLong = this.j.awaitNanos(paramLong))
    {
      if (paramLong <= 0L) {}
      try
      {
        c();
        return new ConnectionResult(14, null);
      }
      catch (InterruptedException paramTimeUnit)
      {
        Thread.currentThread().interrupt();
        return new ConnectionResult(15, null);
      }
    }
    if (d()) {
      return ConnectionResult.a;
    }
    if (this.o != null) {
      return this.o;
    }
    return new ConnectionResult(13, null);
  }
  
  public <A extends Api.zzb, R extends Result, T extends zza.zza<R, A>> T a(@NonNull T paramT)
  {
    return this.n.a(paramT);
  }
  
  public void a()
  {
    this.n.c();
  }
  
  public void a(int paramInt)
  {
    this.i.lock();
    try
    {
      this.n.a(paramInt);
      return;
    }
    finally
    {
      this.i.unlock();
    }
  }
  
  public void a(@Nullable Bundle paramBundle)
  {
    this.i.lock();
    try
    {
      this.n.a(paramBundle);
      return;
    }
    finally
    {
      this.i.unlock();
    }
  }
  
  void a(ConnectionResult paramConnectionResult)
  {
    this.i.lock();
    try
    {
      this.o = paramConnectionResult;
      this.n = new zzi(this);
      this.n.a();
      this.j.signalAll();
      return;
    }
    finally
    {
      this.i.unlock();
    }
  }
  
  public void a(@NonNull ConnectionResult paramConnectionResult, @NonNull Api<?> paramApi, int paramInt)
  {
    this.i.lock();
    try
    {
      this.n.a(paramConnectionResult, paramApi, paramInt);
      return;
    }
    finally
    {
      this.i.unlock();
    }
  }
  
  void a(zza paramzza)
  {
    paramzza = this.m.obtainMessage(1, paramzza);
    this.m.sendMessage(paramzza);
  }
  
  void a(RuntimeException paramRuntimeException)
  {
    paramRuntimeException = this.m.obtainMessage(2, paramRuntimeException);
    this.m.sendMessage(paramRuntimeException);
  }
  
  public void a(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
  {
    String str = paramString + "  ";
    Iterator localIterator = this.d.keySet().iterator();
    while (localIterator.hasNext())
    {
      Api localApi = (Api)localIterator.next();
      paramPrintWriter.append(paramString).append(localApi.e()).println(":");
      ((Api.zzb)this.a.get(localApi.c())).a(str, paramFileDescriptor, paramPrintWriter, paramArrayOfString);
    }
  }
  
  public boolean a(zzu paramzzu)
  {
    return false;
  }
  
  public ConnectionResult b()
  {
    a();
    while (j()) {
      try
      {
        this.j.await();
      }
      catch (InterruptedException localInterruptedException)
      {
        Thread.currentThread().interrupt();
        return new ConnectionResult(15, null);
      }
    }
    if (d()) {
      return ConnectionResult.a;
    }
    if (this.o != null) {
      return this.o;
    }
    return new ConnectionResult(13, null);
  }
  
  public <A extends Api.zzb, T extends zza.zza<? extends Result, A>> T b(@NonNull T paramT)
  {
    return this.n.b(paramT);
  }
  
  public boolean c()
  {
    boolean bool = this.n.b();
    if (bool) {
      this.b.clear();
    }
    return bool;
  }
  
  public boolean d()
  {
    return this.n instanceof zzg;
  }
  
  void e()
  {
    this.i.lock();
    try
    {
      this.n = new zzh(this, this.c, this.d, this.l, this.e, this.i, this.k);
      this.n.a();
      this.j.signalAll();
      return;
    }
    finally
    {
      this.i.unlock();
    }
  }
  
  public void f()
  {
    if (d()) {
      ((zzg)this.n).d();
    }
  }
  
  public void g() {}
  
  void h()
  {
    this.i.lock();
    try
    {
      this.g.n();
      this.n = new zzg(this);
      this.n.a();
      this.j.signalAll();
      return;
    }
    finally
    {
      this.i.unlock();
    }
  }
  
  void i()
  {
    Iterator localIterator = this.a.values().iterator();
    while (localIterator.hasNext()) {
      ((Api.zzb)localIterator.next()).f();
    }
  }
  
  public boolean j()
  {
    return this.n instanceof zzh;
  }
  
  static abstract class zza
  {
    private final zzk a;
    
    protected zza(zzk paramzzk)
    {
      this.a = paramzzk;
    }
    
    protected abstract void a();
    
    public final void a(zzl paramzzl)
    {
      zzl.a(paramzzl).lock();
      try
      {
        zzk localzzk1 = zzl.b(paramzzl);
        zzk localzzk2 = this.a;
        if (localzzk1 != localzzk2) {
          return;
        }
        a();
        return;
      }
      finally
      {
        zzl.a(paramzzl).unlock();
      }
    }
  }
  
  final class zzb
    extends Handler
  {
    zzb(Looper paramLooper)
    {
      super();
    }
    
    public void handleMessage(Message paramMessage)
    {
      switch (paramMessage.what)
      {
      default: 
        Log.w("GACStateManager", "Unknown message id: " + paramMessage.what);
        return;
      case 1: 
        ((zzl.zza)paramMessage.obj).a(zzl.this);
        return;
      }
      throw ((RuntimeException)paramMessage.obj);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/internal/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */