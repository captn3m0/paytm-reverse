package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import android.support.annotation.NonNull;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api.zzb;
import com.google.android.gms.common.api.Result;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

public abstract interface zzp
{
  public abstract ConnectionResult a(long paramLong, TimeUnit paramTimeUnit);
  
  public abstract <A extends Api.zzb, R extends Result, T extends zza.zza<R, A>> T a(@NonNull T paramT);
  
  public abstract void a();
  
  public abstract void a(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString);
  
  public abstract boolean a(zzu paramzzu);
  
  public abstract ConnectionResult b();
  
  public abstract <A extends Api.zzb, T extends zza.zza<? extends Result, A>> T b(@NonNull T paramT);
  
  public abstract boolean c();
  
  public abstract boolean d();
  
  public abstract void f();
  
  public abstract void g();
  
  public static abstract interface zza
  {
    public abstract void a(int paramInt, boolean paramBoolean);
    
    public abstract void a(Bundle paramBundle);
    
    public abstract void a(ConnectionResult paramConnectionResult);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/internal/zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */