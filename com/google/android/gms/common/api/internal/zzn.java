package com.google.android.gms.common.api.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.support.annotation.Nullable;
import com.google.android.gms.common.zzc;

abstract class zzn
  extends BroadcastReceiver
{
  protected Context c;
  
  @Nullable
  public static <T extends zzn> T a(Context paramContext, T paramT)
  {
    return a(paramContext, paramT, zzc.b());
  }
  
  @Nullable
  public static <T extends zzn> T a(Context paramContext, T paramT, zzc paramzzc)
  {
    Object localObject = new IntentFilter("android.intent.action.PACKAGE_ADDED");
    ((IntentFilter)localObject).addDataScheme("package");
    paramContext.registerReceiver(paramT, (IntentFilter)localObject);
    paramT.c = paramContext;
    localObject = paramT;
    if (!paramzzc.a(paramContext, "com.google.android.gms"))
    {
      paramT.a();
      paramT.b();
      localObject = null;
    }
    return (T)localObject;
  }
  
  protected abstract void a();
  
  public void b()
  {
    try
    {
      if (this.c != null) {
        this.c.unregisterReceiver(this);
      }
      this.c = null;
      return;
    }
    finally {}
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    paramIntent = paramIntent.getData();
    paramContext = null;
    if (paramIntent != null) {
      paramContext = paramIntent.getSchemeSpecificPart();
    }
    if ("com.google.android.gms".equals(paramContext))
    {
      a();
      b();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/internal/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */