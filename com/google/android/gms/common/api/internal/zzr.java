package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.PendingResult.zza;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import java.util.concurrent.TimeUnit;

public final class zzr<R extends Result>
  extends OptionalPendingResult<R>
{
  private final zzb<R> a;
  
  public R await()
  {
    return this.a.await();
  }
  
  public R await(long paramLong, TimeUnit paramTimeUnit)
  {
    return this.a.await(paramLong, paramTimeUnit);
  }
  
  public void cancel()
  {
    this.a.cancel();
  }
  
  public boolean isCanceled()
  {
    return this.a.isCanceled();
  }
  
  public void setResultCallback(ResultCallback<? super R> paramResultCallback)
  {
    this.a.setResultCallback(paramResultCallback);
  }
  
  public void setResultCallback(ResultCallback<? super R> paramResultCallback, long paramLong, TimeUnit paramTimeUnit)
  {
    this.a.setResultCallback(paramResultCallback, paramLong, paramTimeUnit);
  }
  
  public void zza(PendingResult.zza paramzza)
  {
    this.a.zza(paramzza);
  }
  
  public Integer zzpa()
  {
    return this.a.zzpa();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/internal/zzr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */