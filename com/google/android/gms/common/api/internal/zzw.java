package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.SparseArray;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.common.zzc;
import com.google.android.gms.common.zze;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.List;

public class zzw
  extends Fragment
  implements DialogInterface.OnCancelListener
{
  protected zzn a;
  private boolean b;
  private boolean c;
  private int d = -1;
  private ConnectionResult e;
  private final Handler f = new Handler(Looper.getMainLooper());
  private final SparseArray<zza> g = new SparseArray();
  
  /* Error */
  @Nullable
  private static zzw a()
  {
    // Byte code:
    //   0: ldc 73
    //   2: invokestatic 79	java/lang/Class:forName	(Ljava/lang/String;)Ljava/lang/Class;
    //   5: astore_0
    //   6: aload_0
    //   7: ifnull +56 -> 63
    //   10: aload_0
    //   11: invokevirtual 83	java/lang/Class:newInstance	()Ljava/lang/Object;
    //   14: checkcast 2	com/google/android/gms/common/api/internal/zzw
    //   17: astore_0
    //   18: aload_0
    //   19: areturn
    //   20: astore_0
    //   21: ldc 85
    //   23: iconst_3
    //   24: invokestatic 91	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
    //   27: ifeq +12 -> 39
    //   30: ldc 85
    //   32: ldc 93
    //   34: aload_0
    //   35: invokestatic 96	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   38: pop
    //   39: aconst_null
    //   40: astore_0
    //   41: goto -35 -> 6
    //   44: astore_0
    //   45: ldc 85
    //   47: iconst_3
    //   48: invokestatic 91	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
    //   51: ifeq +12 -> 63
    //   54: ldc 85
    //   56: ldc 98
    //   58: aload_0
    //   59: invokestatic 96	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   62: pop
    //   63: aconst_null
    //   64: areturn
    //   65: astore_0
    //   66: goto -21 -> 45
    //   69: astore_0
    //   70: goto -25 -> 45
    //   73: astore_0
    //   74: goto -29 -> 45
    //   77: astore_0
    //   78: goto -57 -> 21
    //   81: astore_0
    //   82: goto -61 -> 21
    // Local variable table:
    //   start	length	slot	name	signature
    //   5	14	0	localObject1	Object
    //   20	15	0	localClassNotFoundException	ClassNotFoundException
    //   40	1	0	localObject2	Object
    //   44	15	0	localInstantiationException	InstantiationException
    //   65	1	0	localRuntimeException	RuntimeException
    //   69	1	0	localIllegalAccessException	IllegalAccessException
    //   73	1	0	localExceptionInInitializerError	ExceptionInInitializerError
    //   77	1	0	localLinkageError	LinkageError
    //   81	1	0	localSecurityException	SecurityException
    // Exception table:
    //   from	to	target	type
    //   0	6	20	java/lang/ClassNotFoundException
    //   10	18	44	java/lang/InstantiationException
    //   10	18	65	java/lang/RuntimeException
    //   10	18	69	java/lang/IllegalAccessException
    //   10	18	73	java/lang/ExceptionInInitializerError
    //   0	6	77	java/lang/LinkageError
    //   0	6	81	java/lang/SecurityException
  }
  
  @Nullable
  public static zzw a(FragmentActivity paramFragmentActivity)
  {
    zzx.b("Must be called from main thread of process");
    paramFragmentActivity = paramFragmentActivity.getSupportFragmentManager();
    try
    {
      zzw localzzw = (zzw)paramFragmentActivity.findFragmentByTag("GmsSupportLifecycleFrag");
      if (localzzw != null)
      {
        paramFragmentActivity = localzzw;
        if (!localzzw.isRemoving()) {}
      }
      else
      {
        paramFragmentActivity = null;
      }
      return paramFragmentActivity;
    }
    catch (ClassCastException paramFragmentActivity)
    {
      throw new IllegalStateException("Fragment with tag GmsSupportLifecycleFrag is not a SupportLifecycleFragment", paramFragmentActivity);
    }
  }
  
  private static String a(ConnectionResult paramConnectionResult)
  {
    return paramConnectionResult.e() + " (" + paramConnectionResult.c() + ": " + zze.c(paramConnectionResult.c()) + ')';
  }
  
  public static zzw b(FragmentActivity paramFragmentActivity)
  {
    zzw localzzw = a(paramFragmentActivity);
    FragmentManager localFragmentManager = paramFragmentActivity.getSupportFragmentManager();
    paramFragmentActivity = localzzw;
    if (localzzw == null)
    {
      localzzw = a();
      paramFragmentActivity = localzzw;
      if (localzzw == null)
      {
        Log.w("GmsSupportLifecycleFrag", "Unable to find connection error message resources (Did you include play-services-base and the proper proguard rules?); error dialogs may be unavailable.");
        paramFragmentActivity = new zzw();
      }
      localFragmentManager.beginTransaction().add(paramFragmentActivity, "GmsSupportLifecycleFrag").commitAllowingStateLoss();
      localFragmentManager.executePendingTransactions();
    }
    return paramFragmentActivity;
  }
  
  private void c(int paramInt, ConnectionResult paramConnectionResult)
  {
    Log.w("GmsSupportLifecycleFrag", "Unresolved error while connecting client. Stopping auto-manage.");
    Object localObject = (zza)this.g.get(paramInt);
    if (localObject != null)
    {
      a(paramInt);
      localObject = ((zza)localObject).c;
      if (localObject != null) {
        ((GoogleApiClient.OnConnectionFailedListener)localObject).onConnectionFailed(paramConnectionResult);
      }
    }
    c();
  }
  
  public void a(int paramInt)
  {
    zza localzza = (zza)this.g.get(paramInt);
    this.g.remove(paramInt);
    if (localzza != null) {
      localzza.a();
    }
  }
  
  protected void a(int paramInt, ConnectionResult paramConnectionResult)
  {
    Log.w("GmsSupportLifecycleFrag", "Failed to connect due to user resolvable error " + a(paramConnectionResult));
    c(paramInt, paramConnectionResult);
  }
  
  public void a(int paramInt, GoogleApiClient paramGoogleApiClient, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    zzx.a(paramGoogleApiClient, "GoogleApiClient instance cannot be null");
    if (this.g.indexOfKey(paramInt) < 0) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.a(bool, "Already managing a GoogleApiClient with id " + paramInt);
      paramOnConnectionFailedListener = new zza(paramInt, paramGoogleApiClient, paramOnConnectionFailedListener);
      this.g.put(paramInt, paramOnConnectionFailedListener);
      if ((this.b) && (!this.c)) {
        paramGoogleApiClient.e();
      }
      return;
    }
  }
  
  protected zzc b()
  {
    return zzc.b();
  }
  
  protected void b(int paramInt, ConnectionResult paramConnectionResult)
  {
    Log.w("GmsSupportLifecycleFrag", "Unable to connect, GooglePlayServices is updating.");
    c(paramInt, paramConnectionResult);
  }
  
  protected void c()
  {
    this.c = false;
    this.d = -1;
    this.e = null;
    if (this.a != null)
    {
      this.a.b();
      this.a = null;
    }
    int i = 0;
    while (i < this.g.size())
    {
      ((zza)this.g.valueAt(i)).b.e();
      i += 1;
    }
  }
  
  public void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
  {
    super.dump(paramString, paramFileDescriptor, paramPrintWriter, paramArrayOfString);
    int i = 0;
    while (i < this.g.size())
    {
      ((zza)this.g.valueAt(i)).a(paramString, paramFileDescriptor, paramPrintWriter, paramArrayOfString);
      i += 1;
    }
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    int i = 1;
    switch (paramInt1)
    {
    }
    for (;;)
    {
      paramInt1 = 0;
      do
      {
        for (;;)
        {
          if (paramInt1 == 0) {
            break label88;
          }
          c();
          return;
          if (b().a(getActivity()) != 0) {
            break;
          }
          paramInt1 = i;
        }
        paramInt1 = i;
      } while (paramInt2 == -1);
      if (paramInt2 == 0) {
        this.e = new ConnectionResult(13, null);
      }
    }
    label88:
    c(this.d, this.e);
  }
  
  public void onCancel(DialogInterface paramDialogInterface)
  {
    c(this.d, new ConnectionResult(13, null));
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (paramBundle != null)
    {
      this.c = paramBundle.getBoolean("resolving_error", false);
      this.d = paramBundle.getInt("failed_client_id", -1);
      if (this.d >= 0) {
        this.e = new ConnectionResult(paramBundle.getInt("failed_status"), (PendingIntent)paramBundle.getParcelable("failed_resolution"));
      }
    }
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putBoolean("resolving_error", this.c);
    if (this.d >= 0)
    {
      paramBundle.putInt("failed_client_id", this.d);
      paramBundle.putInt("failed_status", this.e.c());
      paramBundle.putParcelable("failed_resolution", this.e.d());
    }
  }
  
  public void onStart()
  {
    super.onStart();
    this.b = true;
    if (!this.c)
    {
      int i = 0;
      while (i < this.g.size())
      {
        ((zza)this.g.valueAt(i)).b.e();
        i += 1;
      }
    }
  }
  
  public void onStop()
  {
    super.onStop();
    this.b = false;
    int i = 0;
    while (i < this.g.size())
    {
      ((zza)this.g.valueAt(i)).b.g();
      i += 1;
    }
  }
  
  private class zza
    implements GoogleApiClient.OnConnectionFailedListener
  {
    public final int a;
    public final GoogleApiClient b;
    public final GoogleApiClient.OnConnectionFailedListener c;
    
    public zza(int paramInt, GoogleApiClient paramGoogleApiClient, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
    {
      this.a = paramInt;
      this.b = paramGoogleApiClient;
      this.c = paramOnConnectionFailedListener;
      paramGoogleApiClient.a(this);
    }
    
    public void a()
    {
      this.b.b(this);
      this.b.g();
    }
    
    public void a(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
      paramPrintWriter.append(paramString).append("GoogleApiClient #").print(this.a);
      paramPrintWriter.println(":");
      this.b.a(paramString + "  ", paramFileDescriptor, paramPrintWriter, paramArrayOfString);
    }
    
    public void onConnectionFailed(@NonNull ConnectionResult paramConnectionResult)
    {
      zzw.c(zzw.this).post(new zzw.zzb(zzw.this, this.a, paramConnectionResult));
    }
  }
  
  private class zzb
    implements Runnable
  {
    private final int b;
    private final ConnectionResult c;
    
    public zzb(int paramInt, ConnectionResult paramConnectionResult)
    {
      this.b = paramInt;
      this.c = paramConnectionResult;
    }
    
    @MainThread
    public void run()
    {
      if ((!zzw.a(zzw.this)) || (zzw.b(zzw.this))) {
        return;
      }
      zzw.a(zzw.this, true);
      zzw.a(zzw.this, this.b);
      zzw.a(zzw.this, this.c);
      if (this.c.a()) {
        try
        {
          int i = zzw.this.getActivity().getSupportFragmentManager().getFragments().indexOf(zzw.this);
          this.c.a(zzw.this.getActivity(), (i + 1 << 16) + 1);
          return;
        }
        catch (IntentSender.SendIntentException localSendIntentException)
        {
          zzw.this.c();
          return;
        }
      }
      if (zzw.this.b().a(this.c.c()))
      {
        zzw.this.a(this.b, this.c);
        return;
      }
      if (this.c.c() == 18)
      {
        zzw.this.b(this.b, this.c);
        return;
      }
      zzw.a(zzw.this, this.b, this.c);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/internal/zzw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */