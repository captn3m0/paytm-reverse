package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.BinderThread;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.Api.zzb;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.GoogleApiClient.zza;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.ResolveAccountResponse;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzf.zza;
import com.google.android.gms.common.internal.zzp;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.common.zzc;
import com.google.android.gms.internal.zzrn;
import com.google.android.gms.internal.zzro;
import com.google.android.gms.signin.internal.SignInResponse;
import com.google.android.gms.signin.internal.zzb;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;

public class zzh
  implements zzk
{
  private final zzl a;
  private final Lock b;
  private final Context c;
  private final zzc d;
  private ConnectionResult e;
  private int f;
  private int g = 0;
  private int h;
  private final Bundle i = new Bundle();
  private final Set<Api.zzc> j = new HashSet();
  private zzrn k;
  private int l;
  private boolean m;
  private boolean n;
  private zzp o;
  private boolean p;
  private boolean q;
  private final zzf r;
  private final Map<Api<?>, Integer> s;
  private final Api.zza<? extends zzrn, zzro> t;
  private ArrayList<Future<?>> u = new ArrayList();
  
  public zzh(zzl paramzzl, zzf paramzzf, Map<Api<?>, Integer> paramMap, zzc paramzzc, Api.zza<? extends zzrn, zzro> paramzza, Lock paramLock, Context paramContext)
  {
    this.a = paramzzl;
    this.r = paramzzf;
    this.s = paramMap;
    this.d = paramzzc;
    this.t = paramzza;
    this.b = paramLock;
    this.c = paramContext;
  }
  
  private void a(SignInResponse paramSignInResponse)
  {
    if (!b(0)) {
      return;
    }
    ConnectionResult localConnectionResult = paramSignInResponse.a();
    if (localConnectionResult.b())
    {
      paramSignInResponse = paramSignInResponse.b();
      localConnectionResult = paramSignInResponse.b();
      if (!localConnectionResult.b())
      {
        Log.wtf("GoogleApiClientConnecting", "Sign-in succeeded with resolve account failure: " + localConnectionResult, new Exception());
        c(localConnectionResult);
        return;
      }
      this.n = true;
      this.o = paramSignInResponse.a();
      this.p = paramSignInResponse.c();
      this.q = paramSignInResponse.d();
      e();
      return;
    }
    if (b(localConnectionResult))
    {
      h();
      e();
      return;
    }
    c(localConnectionResult);
  }
  
  private void a(boolean paramBoolean)
  {
    if (this.k != null)
    {
      if ((this.k.k()) && (paramBoolean)) {
        this.k.g();
      }
      this.k.f();
      this.o = null;
    }
  }
  
  private boolean a(int paramInt1, int paramInt2, ConnectionResult paramConnectionResult)
  {
    if ((paramInt2 == 1) && (!a(paramConnectionResult))) {}
    while ((this.e != null) && (paramInt1 >= this.f)) {
      return false;
    }
    return true;
  }
  
  private boolean a(ConnectionResult paramConnectionResult)
  {
    if (paramConnectionResult.a()) {}
    while (this.d.b(paramConnectionResult.c()) != null) {
      return true;
    }
    return false;
  }
  
  private void b(ConnectionResult paramConnectionResult, Api<?> paramApi, int paramInt)
  {
    if (paramInt != 2)
    {
      int i1 = paramApi.a().a();
      if (a(i1, paramInt, paramConnectionResult))
      {
        this.e = paramConnectionResult;
        this.f = i1;
      }
    }
    this.a.b.put(paramApi.c(), paramConnectionResult);
  }
  
  private boolean b(int paramInt)
  {
    if (this.g != paramInt)
    {
      Log.i("GoogleApiClientConnecting", this.a.g.p());
      Log.wtf("GoogleApiClientConnecting", "GoogleApiClient connecting is in step " + c(this.g) + " but received callback for step " + c(paramInt), new Exception());
      c(new ConnectionResult(8, null));
      return false;
    }
    return true;
  }
  
  private boolean b(ConnectionResult paramConnectionResult)
  {
    return (this.l == 2) || ((this.l == 1) && (!paramConnectionResult.a()));
  }
  
  private String c(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return "UNKNOWN";
    case 0: 
      return "STEP_SERVICE_BINDINGS_AND_SIGN_IN";
    }
    return "STEP_GETTING_REMOTE_SERVICE";
  }
  
  private void c(ConnectionResult paramConnectionResult)
  {
    i();
    if (!paramConnectionResult.a()) {}
    for (boolean bool = true;; bool = false)
    {
      a(bool);
      this.a.a(paramConnectionResult);
      this.a.h.a(paramConnectionResult);
      return;
    }
  }
  
  private boolean d()
  {
    this.h -= 1;
    if (this.h > 0) {
      return false;
    }
    if (this.h < 0)
    {
      Log.i("GoogleApiClientConnecting", this.a.g.p());
      Log.wtf("GoogleApiClientConnecting", "GoogleApiClient received too many callbacks for the given step. Clients may be in an unexpected state; GoogleApiClient will now disconnect.", new Exception());
      c(new ConnectionResult(8, null));
      return false;
    }
    if (this.e != null)
    {
      this.a.f = this.f;
      c(this.e);
      return false;
    }
    return true;
  }
  
  private void e()
  {
    if (this.h != 0) {}
    while ((this.m) && (!this.n)) {
      return;
    }
    f();
  }
  
  private void f()
  {
    ArrayList localArrayList = new ArrayList();
    this.g = 1;
    this.h = this.a.a.size();
    Iterator localIterator = this.a.a.keySet().iterator();
    while (localIterator.hasNext())
    {
      Api.zzc localzzc = (Api.zzc)localIterator.next();
      if (this.a.b.containsKey(localzzc))
      {
        if (d()) {
          g();
        }
      }
      else {
        localArrayList.add(this.a.a.get(localzzc));
      }
    }
    if (!localArrayList.isEmpty()) {
      this.u.add(zzm.a().submit(new zzc(localArrayList)));
    }
  }
  
  private void g()
  {
    this.a.h();
    zzm.a().execute(new Runnable()
    {
      public void run()
      {
        zzh.b(zzh.this).d(zzh.a(zzh.this));
      }
    });
    if (this.k != null)
    {
      if (this.p) {
        this.k.a(this.o, this.q);
      }
      a(false);
    }
    Object localObject = this.a.b.keySet().iterator();
    while (((Iterator)localObject).hasNext())
    {
      Api.zzc localzzc = (Api.zzc)((Iterator)localObject).next();
      ((Api.zzb)this.a.a.get(localzzc)).f();
    }
    if (this.i.isEmpty()) {}
    for (localObject = null;; localObject = this.i)
    {
      this.a.h.a((Bundle)localObject);
      return;
    }
  }
  
  private void h()
  {
    this.m = false;
    this.a.g.d = Collections.emptySet();
    Iterator localIterator = this.j.iterator();
    while (localIterator.hasNext())
    {
      Api.zzc localzzc = (Api.zzc)localIterator.next();
      if (!this.a.b.containsKey(localzzc)) {
        this.a.b.put(localzzc, new ConnectionResult(17, null));
      }
    }
  }
  
  private void i()
  {
    Iterator localIterator = this.u.iterator();
    while (localIterator.hasNext()) {
      ((Future)localIterator.next()).cancel(true);
    }
    this.u.clear();
  }
  
  private Set<Scope> j()
  {
    if (this.r == null) {
      return Collections.emptySet();
    }
    HashSet localHashSet = new HashSet(this.r.e());
    Map localMap = this.r.g();
    Iterator localIterator = localMap.keySet().iterator();
    while (localIterator.hasNext())
    {
      Api localApi = (Api)localIterator.next();
      if (!this.a.b.containsKey(localApi.c())) {
        localHashSet.addAll(((zzf.zza)localMap.get(localApi)).a);
      }
    }
    return localHashSet;
  }
  
  public <A extends Api.zzb, R extends Result, T extends zza.zza<R, A>> T a(T paramT)
  {
    this.a.g.a.add(paramT);
    return paramT;
  }
  
  public void a()
  {
    this.a.b.clear();
    this.m = false;
    this.e = null;
    this.g = 0;
    this.l = 2;
    this.n = false;
    this.p = false;
    HashMap localHashMap = new HashMap();
    Object localObject = this.s.keySet().iterator();
    int i1 = 0;
    if (((Iterator)localObject).hasNext())
    {
      Api localApi = (Api)((Iterator)localObject).next();
      Api.zzb localzzb = (Api.zzb)this.a.a.get(localApi.c());
      int i3 = ((Integer)this.s.get(localApi)).intValue();
      if (localApi.a().a() == 1) {}
      for (int i2 = 1;; i2 = 0)
      {
        if (localzzb.l())
        {
          this.m = true;
          if (i3 < this.l) {
            this.l = i3;
          }
          if (i3 != 0) {
            this.j.add(localApi.c());
          }
        }
        localHashMap.put(localzzb, new zza(this, localApi, i3));
        i1 = i2 | i1;
        break;
      }
    }
    if (i1 != 0) {
      this.m = false;
    }
    if (this.m)
    {
      this.r.a(Integer.valueOf(this.a.g.q()));
      localObject = new zze(null);
      this.k = ((zzrn)this.t.a(this.c, this.a.g.c(), this.r, this.r.k(), (GoogleApiClient.ConnectionCallbacks)localObject, (GoogleApiClient.OnConnectionFailedListener)localObject));
    }
    this.h = this.a.a.size();
    this.u.add(zzm.a().submit(new zzb(localHashMap)));
  }
  
  public void a(int paramInt)
  {
    c(new ConnectionResult(8, null));
  }
  
  public void a(Bundle paramBundle)
  {
    if (!b(1)) {}
    do
    {
      return;
      if (paramBundle != null) {
        this.i.putAll(paramBundle);
      }
    } while (!d());
    g();
  }
  
  public void a(ConnectionResult paramConnectionResult, Api<?> paramApi, int paramInt)
  {
    if (!b(1)) {}
    do
    {
      return;
      b(paramConnectionResult, paramApi, paramInt);
    } while (!d());
    g();
  }
  
  public <A extends Api.zzb, T extends zza.zza<? extends Result, A>> T b(T paramT)
  {
    throw new IllegalStateException("GoogleApiClient is not connected yet.");
  }
  
  public boolean b()
  {
    i();
    a(true);
    this.a.a(null);
    return true;
  }
  
  public void c() {}
  
  private static class zza
    implements GoogleApiClient.zza
  {
    private final WeakReference<zzh> a;
    private final Api<?> b;
    private final int c;
    
    public zza(zzh paramzzh, Api<?> paramApi, int paramInt)
    {
      this.a = new WeakReference(paramzzh);
      this.b = paramApi;
      this.c = paramInt;
    }
    
    public void a(@NonNull ConnectionResult paramConnectionResult)
    {
      boolean bool = false;
      zzh localzzh = (zzh)this.a.get();
      if (localzzh == null) {
        return;
      }
      if (Looper.myLooper() == zzh.d(localzzh).g.c()) {
        bool = true;
      }
      zzx.a(bool, "onReportServiceBinding must be called on the GoogleApiClient handler thread");
      zzh.c(localzzh).lock();
      try
      {
        bool = zzh.a(localzzh, 0);
        if (!bool) {
          return;
        }
        if (!paramConnectionResult.b()) {
          zzh.a(localzzh, paramConnectionResult, this.b, this.c);
        }
        if (zzh.k(localzzh)) {
          zzh.j(localzzh);
        }
        return;
      }
      finally
      {
        zzh.c(localzzh).unlock();
      }
    }
  }
  
  private class zzb
    extends zzh.zzf
  {
    private final Map<Api.zzb, GoogleApiClient.zza> c;
    
    public zzb()
    {
      super(null);
      Map localMap;
      this.c = localMap;
    }
    
    @WorkerThread
    public void a()
    {
      int i = zzh.b(zzh.this).a(zzh.a(zzh.this));
      final Object localObject;
      if (i != 0)
      {
        localObject = new ConnectionResult(i, null);
        zzh.d(zzh.this).a(new zzl.zza(zzh.this)
        {
          public void a()
          {
            zzh.a(zzh.this, localObject);
          }
        });
      }
      for (;;)
      {
        return;
        if (zzh.e(zzh.this)) {
          zzh.f(zzh.this).h();
        }
        localObject = this.c.keySet().iterator();
        while (((Iterator)localObject).hasNext())
        {
          Api.zzb localzzb = (Api.zzb)((Iterator)localObject).next();
          localzzb.a((GoogleApiClient.zza)this.c.get(localzzb));
        }
      }
    }
  }
  
  private class zzc
    extends zzh.zzf
  {
    private final ArrayList<Api.zzb> c;
    
    public zzc()
    {
      super(null);
      ArrayList localArrayList;
      this.c = localArrayList;
    }
    
    @WorkerThread
    public void a()
    {
      zzh.d(zzh.this).g.d = zzh.g(zzh.this);
      Iterator localIterator = this.c.iterator();
      while (localIterator.hasNext()) {
        ((Api.zzb)localIterator.next()).a(zzh.h(zzh.this), zzh.d(zzh.this).g.d);
      }
    }
  }
  
  private static class zzd
    extends zzb
  {
    private final WeakReference<zzh> a;
    
    zzd(zzh paramzzh)
    {
      this.a = new WeakReference(paramzzh);
    }
    
    @BinderThread
    public void a(final SignInResponse paramSignInResponse)
    {
      final zzh localzzh = (zzh)this.a.get();
      if (localzzh == null) {
        return;
      }
      zzh.d(localzzh).a(new zzl.zza(localzzh)
      {
        public void a()
        {
          zzh.a(localzzh, paramSignInResponse);
        }
      });
    }
  }
  
  private class zze
    implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
  {
    private zze() {}
    
    public void onConnected(Bundle paramBundle)
    {
      zzh.f(zzh.this).a(new zzh.zzd(zzh.this));
    }
    
    /* Error */
    public void onConnectionFailed(@NonNull ConnectionResult paramConnectionResult)
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 17	com/google/android/gms/common/api/internal/zzh$zze:a	Lcom/google/android/gms/common/api/internal/zzh;
      //   4: invokestatic 45	com/google/android/gms/common/api/internal/zzh:c	(Lcom/google/android/gms/common/api/internal/zzh;)Ljava/util/concurrent/locks/Lock;
      //   7: invokeinterface 50 1 0
      //   12: aload_0
      //   13: getfield 17	com/google/android/gms/common/api/internal/zzh$zze:a	Lcom/google/android/gms/common/api/internal/zzh;
      //   16: aload_1
      //   17: invokestatic 54	com/google/android/gms/common/api/internal/zzh:b	(Lcom/google/android/gms/common/api/internal/zzh;Lcom/google/android/gms/common/ConnectionResult;)Z
      //   20: ifeq +30 -> 50
      //   23: aload_0
      //   24: getfield 17	com/google/android/gms/common/api/internal/zzh$zze:a	Lcom/google/android/gms/common/api/internal/zzh;
      //   27: invokestatic 57	com/google/android/gms/common/api/internal/zzh:i	(Lcom/google/android/gms/common/api/internal/zzh;)V
      //   30: aload_0
      //   31: getfield 17	com/google/android/gms/common/api/internal/zzh$zze:a	Lcom/google/android/gms/common/api/internal/zzh;
      //   34: invokestatic 60	com/google/android/gms/common/api/internal/zzh:j	(Lcom/google/android/gms/common/api/internal/zzh;)V
      //   37: aload_0
      //   38: getfield 17	com/google/android/gms/common/api/internal/zzh$zze:a	Lcom/google/android/gms/common/api/internal/zzh;
      //   41: invokestatic 45	com/google/android/gms/common/api/internal/zzh:c	(Lcom/google/android/gms/common/api/internal/zzh;)Ljava/util/concurrent/locks/Lock;
      //   44: invokeinterface 63 1 0
      //   49: return
      //   50: aload_0
      //   51: getfield 17	com/google/android/gms/common/api/internal/zzh$zze:a	Lcom/google/android/gms/common/api/internal/zzh;
      //   54: aload_1
      //   55: invokestatic 66	com/google/android/gms/common/api/internal/zzh:a	(Lcom/google/android/gms/common/api/internal/zzh;Lcom/google/android/gms/common/ConnectionResult;)V
      //   58: goto -21 -> 37
      //   61: astore_1
      //   62: aload_0
      //   63: getfield 17	com/google/android/gms/common/api/internal/zzh$zze:a	Lcom/google/android/gms/common/api/internal/zzh;
      //   66: invokestatic 45	com/google/android/gms/common/api/internal/zzh:c	(Lcom/google/android/gms/common/api/internal/zzh;)Ljava/util/concurrent/locks/Lock;
      //   69: invokeinterface 63 1 0
      //   74: aload_1
      //   75: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	76	0	this	zze
      //   0	76	1	paramConnectionResult	ConnectionResult
      // Exception table:
      //   from	to	target	type
      //   12	37	61	finally
      //   50	58	61	finally
    }
    
    public void onConnectionSuspended(int paramInt) {}
  }
  
  private abstract class zzf
    implements Runnable
  {
    private zzf() {}
    
    @WorkerThread
    protected abstract void a();
    
    @WorkerThread
    public void run()
    {
      zzh.c(zzh.this).lock();
      try
      {
        boolean bool = Thread.interrupted();
        if (bool) {
          return;
        }
        a();
        return;
      }
      catch (RuntimeException localRuntimeException)
      {
        zzh.d(zzh.this).a(localRuntimeException);
        return;
      }
      finally
      {
        zzh.c(zzh.this).unlock();
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/internal/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */