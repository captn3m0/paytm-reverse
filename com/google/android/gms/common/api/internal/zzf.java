package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;

public abstract class zzf
  implements Releasable, Result
{
  protected final Status a;
  protected final DataHolder b;
  
  protected zzf(DataHolder paramDataHolder, Status paramStatus)
  {
    this.a = paramStatus;
    this.b = paramDataHolder;
  }
  
  public Status getStatus()
  {
    return this.a;
  }
  
  public void release()
  {
    if (this.b != null) {
      this.b.i();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/internal/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */