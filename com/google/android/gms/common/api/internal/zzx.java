package com.google.android.gms.common.api.internal;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.ResultCallbacks;
import com.google.android.gms.common.api.ResultTransform;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.TransformedResult;
import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutorService;

public class zzx<R extends Result>
  extends TransformedResult<R>
  implements ResultCallback<R>
{
  private ResultTransform<? super R, ? extends Result> a = null;
  private zzx<? extends Result> b = null;
  private ResultCallbacks<? super R> c = null;
  private PendingResult<R> d = null;
  private final Object e = new Object();
  private Status f = null;
  private final WeakReference<GoogleApiClient> g;
  private final zzx<R>.zza h;
  
  public zzx(WeakReference<GoogleApiClient> paramWeakReference)
  {
    com.google.android.gms.common.internal.zzx.a(paramWeakReference, "GoogleApiClient reference must not be null");
    this.g = paramWeakReference;
    paramWeakReference = (GoogleApiClient)this.g.get();
    if (paramWeakReference != null) {}
    for (paramWeakReference = paramWeakReference.c();; paramWeakReference = Looper.getMainLooper())
    {
      this.h = new zza(paramWeakReference);
      return;
    }
  }
  
  private void a(Status paramStatus)
  {
    synchronized (this.e)
    {
      this.f = paramStatus;
      b(this.f);
      return;
    }
  }
  
  private void b()
  {
    if ((this.a == null) && (this.c == null)) {}
    do
    {
      return;
      GoogleApiClient localGoogleApiClient = (GoogleApiClient)this.g.get();
      if ((this.a != null) && (localGoogleApiClient != null)) {
        localGoogleApiClient.a(this);
      }
      if (this.f != null)
      {
        b(this.f);
        return;
      }
    } while (this.d == null);
    this.d.setResultCallback(this);
  }
  
  private void b(Result paramResult)
  {
    if ((paramResult instanceof Releasable)) {}
    try
    {
      ((Releasable)paramResult).release();
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      Log.w("TransformedResultImpl", "Unable to release " + paramResult, localRuntimeException);
    }
  }
  
  private void b(Status paramStatus)
  {
    synchronized (this.e)
    {
      if (this.a != null)
      {
        paramStatus = this.a.a(paramStatus);
        com.google.android.gms.common.internal.zzx.a(paramStatus, "onFailure must not return null");
        this.b.a(paramStatus);
      }
      while (!c()) {
        return;
      }
      this.c.a(paramStatus);
    }
  }
  
  private boolean c()
  {
    GoogleApiClient localGoogleApiClient = (GoogleApiClient)this.g.get();
    return (this.c != null) && (localGoogleApiClient != null);
  }
  
  @NonNull
  public <S extends Result> TransformedResult<S> a(@NonNull ResultTransform<? super R, ? extends S> paramResultTransform)
  {
    boolean bool2 = true;
    for (;;)
    {
      synchronized (this.e)
      {
        if (this.a == null)
        {
          bool1 = true;
          com.google.android.gms.common.internal.zzx.a(bool1, "Cannot call then() twice.");
          if (this.c != null) {
            break label83;
          }
          bool1 = bool2;
          com.google.android.gms.common.internal.zzx.a(bool1, "Cannot call then() and andFinally() on the same TransformedResult.");
          this.a = paramResultTransform;
          paramResultTransform = new zzx(this.g);
          this.b = paramResultTransform;
          b();
          return paramResultTransform;
        }
      }
      boolean bool1 = false;
      continue;
      label83:
      bool1 = false;
    }
  }
  
  void a()
  {
    synchronized (this.e)
    {
      this.c = null;
      return;
    }
  }
  
  public void a(PendingResult<?> paramPendingResult)
  {
    synchronized (this.e)
    {
      this.d = paramPendingResult;
      b();
      return;
    }
  }
  
  public void a(final R paramR)
  {
    for (;;)
    {
      synchronized (this.e)
      {
        if (paramR.getStatus().d())
        {
          if (this.a != null)
          {
            zzs.a().submit(new Runnable()
            {
              @WorkerThread
              public void run()
              {
                try
                {
                  Object localObject1 = zzx.a(zzx.this).a(paramR);
                  zzx.b(zzx.this).sendMessage(zzx.b(zzx.this).obtainMessage(0, localObject1));
                  zzx.a(zzx.this, paramR);
                  localObject1 = (GoogleApiClient)zzx.c(zzx.this).get();
                  if (localObject1 != null) {
                    ((GoogleApiClient)localObject1).b(zzx.this);
                  }
                  return;
                }
                catch (RuntimeException localRuntimeException)
                {
                  zzx.b(zzx.this).sendMessage(zzx.b(zzx.this).obtainMessage(1, localRuntimeException));
                  GoogleApiClient localGoogleApiClient1;
                  return;
                }
                finally
                {
                  zzx.a(zzx.this, paramR);
                  GoogleApiClient localGoogleApiClient2 = (GoogleApiClient)zzx.c(zzx.this).get();
                  if (localGoogleApiClient2 != null) {
                    localGoogleApiClient2.b(zzx.this);
                  }
                }
              }
            });
            return;
          }
          if (!c()) {
            continue;
          }
          this.c.b(paramR);
        }
      }
      a(paramR.getStatus());
      b(paramR);
    }
  }
  
  private final class zza
    extends Handler
  {
    public zza(Looper paramLooper)
    {
      super();
    }
    
    public void handleMessage(Message paramMessage)
    {
      switch (paramMessage.what)
      {
      default: 
        Log.e("TransformedResultImpl", "TransformationResultHandler received unknown message type: " + paramMessage.what);
        return;
      case 0: 
        PendingResult localPendingResult1 = (PendingResult)paramMessage.obj;
        paramMessage = zzx.d(zzx.this);
        if (localPendingResult1 == null) {}
        for (;;)
        {
          try
          {
            zzx.a(zzx.e(zzx.this), new Status(13, "Transform returned null"));
            return;
          }
          finally {}
          if ((localPendingResult2 instanceof zzt)) {
            zzx.a(zzx.e(zzx.this), ((zzt)localPendingResult2).a());
          } else {
            zzx.e(zzx.this).a(localPendingResult2);
          }
        }
      }
      paramMessage = (RuntimeException)paramMessage.obj;
      Log.e("TransformedResultImpl", "Runtime exception on the transformation worker thread: " + paramMessage.getMessage());
      throw paramMessage;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/internal/zzx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */