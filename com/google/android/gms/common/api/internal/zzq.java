package com.google.android.gms.common.api.internal;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.google.android.gms.common.internal.zzx;

public final class zzq<L>
{
  private final zzq<L>.zza a;
  private volatile L b;
  
  zzq(Looper paramLooper, L paramL)
  {
    this.a = new zza(paramLooper);
    this.b = zzx.a(paramL, "Listener must not be null");
  }
  
  public void a()
  {
    this.b = null;
  }
  
  public void a(zzb<? super L> paramzzb)
  {
    zzx.a(paramzzb, "Notifier must not be null");
    paramzzb = this.a.obtainMessage(1, paramzzb);
    this.a.sendMessage(paramzzb);
  }
  
  void b(zzb<? super L> paramzzb)
  {
    Object localObject = this.b;
    if (localObject == null)
    {
      paramzzb.a();
      return;
    }
    try
    {
      paramzzb.a(localObject);
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      paramzzb.a();
      throw localRuntimeException;
    }
  }
  
  private final class zza
    extends Handler
  {
    public zza(Looper paramLooper)
    {
      super();
    }
    
    public void handleMessage(Message paramMessage)
    {
      boolean bool = true;
      if (paramMessage.what == 1) {}
      for (;;)
      {
        zzx.b(bool);
        zzq.this.b((zzq.zzb)paramMessage.obj);
        return;
        bool = false;
      }
    }
  }
  
  public static abstract interface zzb<L>
  {
    public abstract void a();
    
    public abstract void a(L paramL);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/internal/zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */