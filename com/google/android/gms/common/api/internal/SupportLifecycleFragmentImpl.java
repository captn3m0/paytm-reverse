package com.google.android.gms.common.api.internal;

import android.app.Dialog;
import android.support.v4.app.FragmentActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.annotation.KeepName;

@KeepName
public class SupportLifecycleFragmentImpl
  extends zzw
{
  protected GoogleApiAvailability a()
  {
    return GoogleApiAvailability.a();
  }
  
  protected void a(int paramInt, ConnectionResult paramConnectionResult)
  {
    GooglePlayServicesUtil.a(paramConnectionResult.c(), getActivity(), this, 2, this);
  }
  
  protected void b(int paramInt, final ConnectionResult paramConnectionResult)
  {
    paramConnectionResult = a().a(getActivity(), this);
    this.a = zzn.a(getActivity().getApplicationContext(), new zzn()
    {
      protected void a()
      {
        SupportLifecycleFragmentImpl.this.c();
        paramConnectionResult.dismiss();
      }
    });
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/internal/SupportLifecycleFragmentImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */