package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.Api.zzb;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzrn;
import com.google.android.gms.internal.zzro;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

public class zzd
  implements zzp
{
  private final Context a;
  private final zzj b;
  private final Looper c;
  private final zzl d;
  private final zzl e;
  private final Map<Api.zzc<?>, zzl> f = new ArrayMap();
  private final Set<zzu> g = Collections.newSetFromMap(new WeakHashMap());
  private final Api.zzb h;
  private Bundle i;
  private ConnectionResult j = null;
  private ConnectionResult k = null;
  private boolean l = false;
  private final Lock m;
  private int n = 0;
  
  public zzd(Context paramContext, zzj paramzzj, Lock paramLock, Looper paramLooper, com.google.android.gms.common.zzc paramzzc, Map<Api.zzc<?>, Api.zzb> paramMap, zzf paramzzf, Map<Api<?>, Integer> paramMap1, Api.zza<? extends zzrn, zzro> paramzza, ArrayList<zzc> paramArrayList)
  {
    this.a = paramContext;
    this.b = paramzzj;
    this.m = paramLock;
    this.c = paramLooper;
    paramzzj = null;
    ArrayMap localArrayMap1 = new ArrayMap();
    ArrayMap localArrayMap2 = new ArrayMap();
    Object localObject2 = paramMap.keySet().iterator();
    Api.zzc localzzc;
    while (((Iterator)localObject2).hasNext())
    {
      localzzc = (Api.zzc)((Iterator)localObject2).next();
      localObject1 = (Api.zzb)paramMap.get(localzzc);
      if (((Api.zzb)localObject1).d()) {
        paramzzj = (zzj)localObject1;
      }
      if (((Api.zzb)localObject1).l()) {
        localArrayMap1.put(localzzc, localObject1);
      } else {
        localArrayMap2.put(localzzc, localObject1);
      }
    }
    this.h = paramzzj;
    if (localArrayMap1.isEmpty()) {
      throw new IllegalStateException("CompositeGoogleApiClient should not be used without any APIs that require sign-in.");
    }
    paramzzj = new ArrayMap();
    paramMap = new ArrayMap();
    Object localObject1 = paramMap1.keySet().iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (Api)((Iterator)localObject1).next();
      localzzc = ((Api)localObject2).c();
      if (localArrayMap1.containsKey(localzzc)) {
        paramzzj.put(localObject2, paramMap1.get(localObject2));
      } else if (localArrayMap2.containsKey(localzzc)) {
        paramMap.put(localObject2, paramMap1.get(localObject2));
      } else {
        throw new IllegalStateException("Each API in the apiTypeMap must have a corresponding client in the clients map.");
      }
    }
    paramMap1 = new ArrayList();
    localObject1 = new ArrayList();
    paramArrayList = paramArrayList.iterator();
    while (paramArrayList.hasNext())
    {
      localObject2 = (zzc)paramArrayList.next();
      if (paramzzj.containsKey(((zzc)localObject2).a)) {
        paramMap1.add(localObject2);
      } else if (paramMap.containsKey(((zzc)localObject2).a)) {
        ((ArrayList)localObject1).add(localObject2);
      } else {
        throw new IllegalStateException("Each ClientCallbacks must have a corresponding API in the apiTypeMap");
      }
    }
    paramArrayList = new zzp.zza()
    {
      public void a(int paramAnonymousInt, boolean paramAnonymousBoolean)
      {
        zzd.a(zzd.this).lock();
        try
        {
          if ((zzd.c(zzd.this)) || (zzd.d(zzd.this) == null) || (!zzd.d(zzd.this).b()))
          {
            zzd.a(zzd.this, false);
            zzd.a(zzd.this, paramAnonymousInt, paramAnonymousBoolean);
            return;
          }
          zzd.a(zzd.this, true);
          zzd.e(zzd.this).a(paramAnonymousInt);
          return;
        }
        finally
        {
          zzd.a(zzd.this).unlock();
        }
      }
      
      public void a(@Nullable Bundle paramAnonymousBundle)
      {
        zzd.a(zzd.this).lock();
        try
        {
          zzd.a(zzd.this, paramAnonymousBundle);
          zzd.a(zzd.this, ConnectionResult.a);
          zzd.b(zzd.this);
          return;
        }
        finally
        {
          zzd.a(zzd.this).unlock();
        }
      }
      
      public void a(@NonNull ConnectionResult paramAnonymousConnectionResult)
      {
        zzd.a(zzd.this).lock();
        try
        {
          zzd.a(zzd.this, paramAnonymousConnectionResult);
          zzd.b(zzd.this);
          return;
        }
        finally
        {
          zzd.a(zzd.this).unlock();
        }
      }
    };
    this.d = new zzl(paramContext, this.b, paramLock, paramLooper, paramzzc, localArrayMap2, null, paramMap, null, (ArrayList)localObject1, paramArrayList);
    paramMap = new zzp.zza()
    {
      public void a(int paramAnonymousInt, boolean paramAnonymousBoolean)
      {
        zzd.a(zzd.this).lock();
        try
        {
          if (zzd.c(zzd.this))
          {
            zzd.a(zzd.this, false);
            zzd.a(zzd.this, paramAnonymousInt, paramAnonymousBoolean);
            return;
          }
          zzd.a(zzd.this, true);
          zzd.f(zzd.this).a(paramAnonymousInt);
          return;
        }
        finally
        {
          zzd.a(zzd.this).unlock();
        }
      }
      
      public void a(@Nullable Bundle paramAnonymousBundle)
      {
        zzd.a(zzd.this).lock();
        try
        {
          zzd.b(zzd.this, ConnectionResult.a);
          zzd.b(zzd.this);
          return;
        }
        finally
        {
          zzd.a(zzd.this).unlock();
        }
      }
      
      public void a(@NonNull ConnectionResult paramAnonymousConnectionResult)
      {
        zzd.a(zzd.this).lock();
        try
        {
          zzd.b(zzd.this, paramAnonymousConnectionResult);
          zzd.b(zzd.this);
          return;
        }
        finally
        {
          zzd.a(zzd.this).unlock();
        }
      }
    };
    this.e = new zzl(paramContext, this.b, paramLock, paramLooper, paramzzc, localArrayMap1, paramzzf, paramzzj, paramzza, paramMap1, paramMap);
    paramContext = localArrayMap2.keySet().iterator();
    while (paramContext.hasNext())
    {
      paramzzj = (Api.zzc)paramContext.next();
      this.f.put(paramzzj, this.d);
    }
    paramContext = localArrayMap1.keySet().iterator();
    while (paramContext.hasNext())
    {
      paramzzj = (Api.zzc)paramContext.next();
      this.f.put(paramzzj, this.e);
    }
  }
  
  private void a(int paramInt, boolean paramBoolean)
  {
    this.b.a(paramInt, paramBoolean);
    this.k = null;
    this.j = null;
  }
  
  private void a(Bundle paramBundle)
  {
    if (this.i == null) {
      this.i = paramBundle;
    }
    while (paramBundle == null) {
      return;
    }
    this.i.putAll(paramBundle);
  }
  
  private void a(ConnectionResult paramConnectionResult)
  {
    switch (this.n)
    {
    default: 
      Log.wtf("CompositeGAC", "Attempted to call failure callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new Exception());
    }
    for (;;)
    {
      this.n = 0;
      return;
      this.b.a(paramConnectionResult);
      l();
    }
  }
  
  private static boolean b(ConnectionResult paramConnectionResult)
  {
    return (paramConnectionResult != null) && (paramConnectionResult.b());
  }
  
  private boolean c(zza.zza<? extends Result, ? extends Api.zzb> paramzza)
  {
    paramzza = paramzza.a();
    zzx.b(this.f.containsKey(paramzza), "GoogleApiClient is not configured to use the API required for this call.");
    return ((zzl)this.f.get(paramzza)).equals(this.e);
  }
  
  private void i()
  {
    this.k = null;
    this.j = null;
    this.d.a();
    this.e.a();
  }
  
  private void j()
  {
    if (b(this.j)) {
      if ((b(this.k)) || (m())) {
        k();
      }
    }
    do
    {
      do
      {
        return;
      } while (this.k == null);
      if (this.n == 1)
      {
        l();
        return;
      }
      a(this.k);
      this.d.c();
      return;
      if ((this.j != null) && (b(this.k)))
      {
        this.e.c();
        a(this.j);
        return;
      }
    } while ((this.j == null) || (this.k == null));
    ConnectionResult localConnectionResult = this.j;
    if (this.e.f < this.d.f) {
      localConnectionResult = this.k;
    }
    a(localConnectionResult);
  }
  
  private void k()
  {
    switch (this.n)
    {
    default: 
      Log.wtf("CompositeGAC", "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new Exception());
    }
    for (;;)
    {
      this.n = 0;
      return;
      this.b.a(this.i);
      l();
    }
  }
  
  private void l()
  {
    Iterator localIterator = this.g.iterator();
    while (localIterator.hasNext()) {
      ((zzu)localIterator.next()).g();
    }
    this.g.clear();
  }
  
  private boolean m()
  {
    return (this.k != null) && (this.k.c() == 4);
  }
  
  @Nullable
  private PendingIntent n()
  {
    if (this.h == null) {
      return null;
    }
    return PendingIntent.getActivity(this.a, this.b.q(), this.h.e(), 134217728);
  }
  
  public ConnectionResult a(long paramLong, @NonNull TimeUnit paramTimeUnit)
  {
    throw new UnsupportedOperationException();
  }
  
  public <A extends Api.zzb, R extends Result, T extends zza.zza<R, A>> T a(@NonNull T paramT)
  {
    if (c(paramT))
    {
      if (m())
      {
        paramT.b(new Status(4, null, n()));
        return paramT;
      }
      return this.e.a(paramT);
    }
    return this.d.a(paramT);
  }
  
  public void a()
  {
    this.n = 2;
    this.l = false;
    i();
  }
  
  public void a(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
  {
    paramPrintWriter.append(paramString).append("authClient").println(":");
    this.e.a(paramString + "  ", paramFileDescriptor, paramPrintWriter, paramArrayOfString);
    paramPrintWriter.append(paramString).append("anonClient").println(":");
    this.d.a(paramString + "  ", paramFileDescriptor, paramPrintWriter, paramArrayOfString);
  }
  
  public boolean a(zzu paramzzu)
  {
    this.m.lock();
    try
    {
      if (((e()) || (d())) && (!h()))
      {
        this.g.add(paramzzu);
        if (this.n == 0) {
          this.n = 1;
        }
        this.k = null;
        this.e.a();
        return true;
      }
      return false;
    }
    finally
    {
      this.m.unlock();
    }
  }
  
  public ConnectionResult b()
  {
    throw new UnsupportedOperationException();
  }
  
  public <A extends Api.zzb, T extends zza.zza<? extends Result, A>> T b(@NonNull T paramT)
  {
    if (c(paramT))
    {
      if (m())
      {
        paramT.b(new Status(4, null, n()));
        return paramT;
      }
      return this.e.b(paramT);
    }
    return this.d.b(paramT);
  }
  
  public boolean c()
  {
    boolean bool2 = false;
    this.k = null;
    this.j = null;
    this.n = 0;
    boolean bool3 = this.d.c();
    boolean bool4 = this.e.c();
    l();
    boolean bool1 = bool2;
    if (bool3)
    {
      bool1 = bool2;
      if (bool4) {
        bool1 = true;
      }
    }
    return bool1;
  }
  
  /* Error */
  public boolean d()
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_3
    //   2: aload_0
    //   3: getfield 75	com/google/android/gms/common/api/internal/zzd:m	Ljava/util/concurrent/locks/Lock;
    //   6: invokeinterface 335 1 0
    //   11: aload_0
    //   12: getfield 162	com/google/android/gms/common/api/internal/zzd:d	Lcom/google/android/gms/common/api/internal/zzl;
    //   15: invokevirtual 349	com/google/android/gms/common/api/internal/zzl:d	()Z
    //   18: ifeq +44 -> 62
    //   21: iload_3
    //   22: istore_2
    //   23: aload_0
    //   24: invokevirtual 340	com/google/android/gms/common/api/internal/zzd:h	()Z
    //   27: ifne +24 -> 51
    //   30: iload_3
    //   31: istore_2
    //   32: aload_0
    //   33: invokespecial 239	com/google/android/gms/common/api/internal/zzd:m	()Z
    //   36: ifne +15 -> 51
    //   39: aload_0
    //   40: getfield 69	com/google/android/gms/common/api/internal/zzd:n	I
    //   43: istore_1
    //   44: iload_1
    //   45: iconst_1
    //   46: if_icmpne +16 -> 62
    //   49: iload_3
    //   50: istore_2
    //   51: aload_0
    //   52: getfield 75	com/google/android/gms/common/api/internal/zzd:m	Ljava/util/concurrent/locks/Lock;
    //   55: invokeinterface 344 1 0
    //   60: iload_2
    //   61: ireturn
    //   62: iconst_0
    //   63: istore_2
    //   64: goto -13 -> 51
    //   67: astore 4
    //   69: aload_0
    //   70: getfield 75	com/google/android/gms/common/api/internal/zzd:m	Ljava/util/concurrent/locks/Lock;
    //   73: invokeinterface 344 1 0
    //   78: aload 4
    //   80: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	81	0	this	zzd
    //   43	4	1	i1	int
    //   22	42	2	bool1	boolean
    //   1	49	3	bool2	boolean
    //   67	12	4	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   11	21	67	finally
    //   23	30	67	finally
    //   32	44	67	finally
  }
  
  /* Error */
  public boolean e()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 75	com/google/android/gms/common/api/internal/zzd:m	Ljava/util/concurrent/locks/Lock;
    //   4: invokeinterface 335 1 0
    //   9: aload_0
    //   10: getfield 69	com/google/android/gms/common/api/internal/zzd:n	I
    //   13: istore_1
    //   14: iload_1
    //   15: iconst_2
    //   16: if_icmpne +16 -> 32
    //   19: iconst_1
    //   20: istore_2
    //   21: aload_0
    //   22: getfield 75	com/google/android/gms/common/api/internal/zzd:m	Ljava/util/concurrent/locks/Lock;
    //   25: invokeinterface 344 1 0
    //   30: iload_2
    //   31: ireturn
    //   32: iconst_0
    //   33: istore_2
    //   34: goto -13 -> 21
    //   37: astore_3
    //   38: aload_0
    //   39: getfield 75	com/google/android/gms/common/api/internal/zzd:m	Ljava/util/concurrent/locks/Lock;
    //   42: invokeinterface 344 1 0
    //   47: aload_3
    //   48: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	49	0	this	zzd
    //   13	4	1	i1	int
    //   20	14	2	bool	boolean
    //   37	11	3	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   9	14	37	finally
  }
  
  public void f()
  {
    this.d.f();
    this.e.f();
  }
  
  /* Error */
  public void g()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 75	com/google/android/gms/common/api/internal/zzd:m	Ljava/util/concurrent/locks/Lock;
    //   4: invokeinterface 335 1 0
    //   9: aload_0
    //   10: invokevirtual 337	com/google/android/gms/common/api/internal/zzd:e	()Z
    //   13: istore_1
    //   14: aload_0
    //   15: getfield 165	com/google/android/gms/common/api/internal/zzd:e	Lcom/google/android/gms/common/api/internal/zzl;
    //   18: invokevirtual 244	com/google/android/gms/common/api/internal/zzl:c	()Z
    //   21: pop
    //   22: aload_0
    //   23: new 212	com/google/android/gms/common/ConnectionResult
    //   26: dup
    //   27: iconst_4
    //   28: invokespecial 354	com/google/android/gms/common/ConnectionResult:<init>	(I)V
    //   31: putfield 65	com/google/android/gms/common/api/internal/zzd:k	Lcom/google/android/gms/common/ConnectionResult;
    //   34: iload_1
    //   35: ifeq +36 -> 71
    //   38: new 356	android/os/Handler
    //   41: dup
    //   42: aload_0
    //   43: getfield 77	com/google/android/gms/common/api/internal/zzd:c	Landroid/os/Looper;
    //   46: invokespecial 359	android/os/Handler:<init>	(Landroid/os/Looper;)V
    //   49: new 12	com/google/android/gms/common/api/internal/zzd$3
    //   52: dup
    //   53: aload_0
    //   54: invokespecial 360	com/google/android/gms/common/api/internal/zzd$3:<init>	(Lcom/google/android/gms/common/api/internal/zzd;)V
    //   57: invokevirtual 364	android/os/Handler:post	(Ljava/lang/Runnable;)Z
    //   60: pop
    //   61: aload_0
    //   62: getfield 75	com/google/android/gms/common/api/internal/zzd:m	Ljava/util/concurrent/locks/Lock;
    //   65: invokeinterface 344 1 0
    //   70: return
    //   71: aload_0
    //   72: invokespecial 201	com/google/android/gms/common/api/internal/zzd:l	()V
    //   75: goto -14 -> 61
    //   78: astore_2
    //   79: aload_0
    //   80: getfield 75	com/google/android/gms/common/api/internal/zzd:m	Ljava/util/concurrent/locks/Lock;
    //   83: invokeinterface 344 1 0
    //   88: aload_2
    //   89: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	90	0	this	zzd
    //   13	22	1	bool	boolean
    //   78	11	2	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   9	34	78	finally
    //   38	61	78	finally
    //   71	75	78	finally
  }
  
  public boolean h()
  {
    return this.e.d();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/internal/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */