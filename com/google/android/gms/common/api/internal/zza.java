package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api.zzb;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzx;
import java.util.concurrent.atomic.AtomicReference;

public class zza
{
  public static abstract class zza<R extends Result, A extends Api.zzb>
    extends zzb<R>
    implements zza.zzb<R>, zzj.zze<A>
  {
    private final Api.zzc<A> a;
    private AtomicReference<zzj.zzd> b = new AtomicReference();
    
    protected zza(Api.zzc<A> paramzzc, GoogleApiClient paramGoogleApiClient)
    {
      super();
      this.a = ((Api.zzc)zzx.a(paramzzc));
    }
    
    private void a(RemoteException paramRemoteException)
    {
      b(new Status(8, paramRemoteException.getLocalizedMessage(), null));
    }
    
    public final Api.zzc<A> a()
    {
      return this.a;
    }
    
    protected abstract void a(A paramA)
      throws RemoteException;
    
    public void a(zzj.zzd paramzzd)
    {
      this.b.set(paramzzd);
    }
    
    public void b()
    {
      setResultCallback(null);
    }
    
    public final void b(A paramA)
      throws DeadObjectException
    {
      try
      {
        a(paramA);
        return;
      }
      catch (DeadObjectException paramA)
      {
        a(paramA);
        throw paramA;
      }
      catch (RemoteException paramA)
      {
        a(paramA);
      }
    }
    
    public final void b(Status paramStatus)
    {
      if (!paramStatus.d()) {}
      for (boolean bool = true;; bool = false)
      {
        zzx.b(bool, "Failed result must not be success");
        zza(zzc(paramStatus));
        return;
      }
    }
    
    protected void zzpf()
    {
      zzj.zzd localzzd = (zzj.zzd)this.b.getAndSet(null);
      if (localzzd != null) {
        localzzd.a(this);
      }
    }
  }
  
  public static abstract interface zzb<R>
  {
    public abstract void a(R paramR);
    
    public abstract void b(Status paramStatus);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/internal/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */