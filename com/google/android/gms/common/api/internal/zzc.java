package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzx;

public class zzc
  implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
{
  public final Api<?> a;
  private final int b;
  private zzl c;
  
  public zzc(Api<?> paramApi, int paramInt)
  {
    this.a = paramApi;
    this.b = paramInt;
  }
  
  private void a()
  {
    zzx.a(this.c, "Callbacks must be attached to a GoogleApiClient instance before connecting the client.");
  }
  
  public void a(zzl paramzzl)
  {
    this.c = paramzzl;
  }
  
  public void onConnected(@Nullable Bundle paramBundle)
  {
    a();
    this.c.a(paramBundle);
  }
  
  public void onConnectionFailed(@NonNull ConnectionResult paramConnectionResult)
  {
    a();
    this.c.a(paramConnectionResult, this.a, this.b);
  }
  
  public void onConnectionSuspended(int paramInt)
  {
    a();
    this.c.a(paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/internal/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */