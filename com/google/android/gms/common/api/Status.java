package com.google.android.gms.common.api;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;

public final class Status
  implements Result, SafeParcelable
{
  public static final Parcelable.Creator<Status> CREATOR = new zzc();
  public static final Status a = new Status(0);
  public static final Status b = new Status(14);
  public static final Status c = new Status(8);
  public static final Status d = new Status(15);
  public static final Status e = new Status(16);
  private final int f;
  private final int g;
  private final String h;
  private final PendingIntent i;
  
  public Status(int paramInt)
  {
    this(paramInt, null);
  }
  
  Status(int paramInt1, int paramInt2, String paramString, PendingIntent paramPendingIntent)
  {
    this.f = paramInt1;
    this.g = paramInt2;
    this.h = paramString;
    this.i = paramPendingIntent;
  }
  
  public Status(int paramInt, String paramString)
  {
    this(1, paramInt, paramString, null);
  }
  
  public Status(int paramInt, String paramString, PendingIntent paramPendingIntent)
  {
    this(1, paramInt, paramString, paramPendingIntent);
  }
  
  private String g()
  {
    if (this.h != null) {
      return this.h;
    }
    return CommonStatusCodes.a(this.g);
  }
  
  PendingIntent a()
  {
    return this.i;
  }
  
  public String b()
  {
    return this.h;
  }
  
  int c()
  {
    return this.f;
  }
  
  public boolean d()
  {
    return this.g <= 0;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean e()
  {
    return this.g == 16;
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof Status)) {}
    do
    {
      return false;
      paramObject = (Status)paramObject;
    } while ((this.f != ((Status)paramObject).f) || (this.g != ((Status)paramObject).g) || (!zzw.a(this.h, ((Status)paramObject).h)) || (!zzw.a(this.i, ((Status)paramObject).i)));
    return true;
  }
  
  public int f()
  {
    return this.g;
  }
  
  public Status getStatus()
  {
    return this;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Integer.valueOf(this.f), Integer.valueOf(this.g), this.h, this.i });
  }
  
  public String toString()
  {
    return zzw.a(this).a("statusCode", g()).a("resolution", this.i).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/Status.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */