package com.google.android.gms.common.api;

import android.support.annotation.NonNull;

public abstract interface ResultCallback<R extends Result>
{
  public abstract void a(@NonNull R paramR);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/ResultCallback.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */