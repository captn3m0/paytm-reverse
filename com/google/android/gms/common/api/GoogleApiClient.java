package com.google.android.gms.common.api;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.util.ArrayMap;
import android.view.View;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.internal.zza.zza;
import com.google.android.gms.common.api.internal.zzj;
import com.google.android.gms.common.api.internal.zzq;
import com.google.android.gms.common.api.internal.zzu;
import com.google.android.gms.common.api.internal.zzw;
import com.google.android.gms.common.internal.zzad;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzf.zza;
import com.google.android.gms.internal.zzrl;
import com.google.android.gms.internal.zzrn;
import com.google.android.gms.internal.zzro;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public abstract class GoogleApiClient
{
  private static final Set<GoogleApiClient> a = Collections.newSetFromMap(new WeakHashMap());
  
  public static Set<GoogleApiClient> a()
  {
    return a;
  }
  
  public abstract ConnectionResult a(long paramLong, @NonNull TimeUnit paramTimeUnit);
  
  @NonNull
  public <C extends Api.zzb> C a(@NonNull Api.zzc<C> paramzzc)
  {
    throw new UnsupportedOperationException();
  }
  
  public <A extends Api.zzb, R extends Result, T extends zza.zza<R, A>> T a(@NonNull T paramT)
  {
    throw new UnsupportedOperationException();
  }
  
  public <L> zzq<L> a(@NonNull L paramL)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(int paramInt)
  {
    throw new UnsupportedOperationException();
  }
  
  public abstract void a(@NonNull ConnectionCallbacks paramConnectionCallbacks);
  
  public abstract void a(@NonNull OnConnectionFailedListener paramOnConnectionFailedListener);
  
  public void a(com.google.android.gms.common.api.internal.zzx paramzzx)
  {
    throw new UnsupportedOperationException();
  }
  
  public abstract void a(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString);
  
  public boolean a(zzu paramzzu)
  {
    throw new UnsupportedOperationException();
  }
  
  public Context b()
  {
    throw new UnsupportedOperationException();
  }
  
  public <A extends Api.zzb, T extends zza.zza<? extends Result, A>> T b(@NonNull T paramT)
  {
    throw new UnsupportedOperationException();
  }
  
  public abstract void b(@NonNull ConnectionCallbacks paramConnectionCallbacks);
  
  public abstract void b(@NonNull OnConnectionFailedListener paramOnConnectionFailedListener);
  
  public void b(com.google.android.gms.common.api.internal.zzx paramzzx)
  {
    throw new UnsupportedOperationException();
  }
  
  public Looper c()
  {
    throw new UnsupportedOperationException();
  }
  
  public void d()
  {
    throw new UnsupportedOperationException();
  }
  
  public abstract void e();
  
  public abstract ConnectionResult f();
  
  public abstract void g();
  
  public abstract PendingResult<Status> h();
  
  public abstract boolean i();
  
  public static final class Builder
  {
    private Account a;
    private final Set<Scope> b = new HashSet();
    private final Set<Scope> c = new HashSet();
    private int d;
    private View e;
    private String f;
    private String g;
    private final Map<Api<?>, zzf.zza> h = new ArrayMap();
    private final Context i;
    private final Map<Api<?>, Api.ApiOptions> j = new ArrayMap();
    private FragmentActivity k;
    private int l = -1;
    private GoogleApiClient.OnConnectionFailedListener m;
    private Looper n;
    private com.google.android.gms.common.zzc o = com.google.android.gms.common.zzc.b();
    private Api.zza<? extends zzrn, zzro> p = zzrl.c;
    private final ArrayList<GoogleApiClient.ConnectionCallbacks> q = new ArrayList();
    private final ArrayList<GoogleApiClient.OnConnectionFailedListener> r = new ArrayList();
    
    public Builder(@NonNull Context paramContext)
    {
      this.i = paramContext;
      this.n = paramContext.getMainLooper();
      this.f = paramContext.getPackageName();
      this.g = paramContext.getClass().getName();
    }
    
    public Builder(@NonNull Context paramContext, @NonNull GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, @NonNull GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
    {
      this(paramContext);
      com.google.android.gms.common.internal.zzx.a(paramConnectionCallbacks, "Must provide a connected listener");
      this.q.add(paramConnectionCallbacks);
      com.google.android.gms.common.internal.zzx.a(paramOnConnectionFailedListener, "Must provide a connection failed listener");
      this.r.add(paramOnConnectionFailedListener);
    }
    
    private static <C extends Api.zzb, O> C a(Api.zza<C, O> paramzza, Object paramObject, Context paramContext, Looper paramLooper, zzf paramzzf, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
    {
      return paramzza.a(paramContext, paramLooper, paramzzf, paramObject, paramConnectionCallbacks, paramOnConnectionFailedListener);
    }
    
    private static <C extends Api.zzd, O> zzad a(Api.zze<C, O> paramzze, Object paramObject, Context paramContext, Looper paramLooper, zzf paramzzf, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
    {
      return new zzad(paramContext, paramLooper, paramzze.b(), paramConnectionCallbacks, paramOnConnectionFailedListener, paramzzf, paramzze.a(paramObject));
    }
    
    private void a(final GoogleApiClient paramGoogleApiClient)
    {
      zzw localzzw = zzw.a(this.k);
      if (localzzw == null)
      {
        new Handler(this.i.getMainLooper()).post(new Runnable()
        {
          public void run()
          {
            if ((GoogleApiClient.Builder.a(GoogleApiClient.Builder.this).isFinishing()) || (GoogleApiClient.Builder.a(GoogleApiClient.Builder.this).getSupportFragmentManager().isDestroyed())) {
              return;
            }
            GoogleApiClient.Builder.a(GoogleApiClient.Builder.this, zzw.b(GoogleApiClient.Builder.a(GoogleApiClient.Builder.this)), paramGoogleApiClient);
          }
        });
        return;
      }
      a(localzzw, paramGoogleApiClient);
    }
    
    private void a(zzw paramzzw, GoogleApiClient paramGoogleApiClient)
    {
      paramzzw.a(this.l, paramGoogleApiClient, this.m);
    }
    
    private GoogleApiClient c()
    {
      zzf localzzf = a();
      Object localObject2 = null;
      Map localMap = localzzf.g();
      ArrayMap localArrayMap1 = new ArrayMap();
      ArrayMap localArrayMap2 = new ArrayMap();
      ArrayList localArrayList = new ArrayList();
      Iterator localIterator = this.j.keySet().iterator();
      Object localObject1 = null;
      Api localApi;
      Object localObject3;
      int i1;
      label130:
      com.google.android.gms.common.api.internal.zzc localzzc;
      Object localObject4;
      if (localIterator.hasNext())
      {
        localApi = (Api)localIterator.next();
        localObject3 = this.j.get(localApi);
        i1 = 0;
        if (localMap.get(localApi) != null)
        {
          if (((zzf.zza)localMap.get(localApi)).b) {
            i1 = 1;
          }
        }
        else
        {
          localArrayMap1.put(localApi, Integer.valueOf(i1));
          localzzc = new com.google.android.gms.common.api.internal.zzc(localApi, i1);
          localArrayList.add(localzzc);
          if (!localApi.d()) {
            break label295;
          }
          localObject4 = localApi.b();
          if (((Api.zze)localObject4).a() != 1) {
            break label530;
          }
          localObject1 = localApi;
        }
      }
      label216:
      label295:
      label344:
      label522:
      label527:
      label530:
      for (;;)
      {
        localObject3 = a((Api.zze)localObject4, localObject3, this.i, this.n, localzzf, localzzc, localzzc);
        localArrayMap2.put(localApi.c(), localObject3);
        if (((Api.zzb)localObject3).d())
        {
          localObject3 = localApi;
          if (localObject2 == null) {
            break label344;
          }
          throw new IllegalStateException(localApi.e() + " cannot be used with " + ((Api)localObject2).e());
          i1 = 2;
          break label130;
          localObject4 = localApi.a();
          if (((Api.zza)localObject4).a() != 1) {
            break label527;
          }
          localObject1 = localApi;
        }
        for (;;)
        {
          localObject3 = a((Api.zza)localObject4, localObject3, this.i, this.n, localzzf, localzzc, localzzc);
          break label216;
          localObject3 = localObject2;
          localObject2 = localObject3;
          break;
          if (localObject2 != null)
          {
            if (localObject1 != null) {
              throw new IllegalStateException(((Api)localObject2).e() + " cannot be used with " + ((Api)localObject1).e());
            }
            if (this.a != null) {
              break label522;
            }
          }
          for (boolean bool = true;; bool = false)
          {
            com.google.android.gms.common.internal.zzx.a(bool, "Must not set an account in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead", new Object[] { ((Api)localObject2).e() });
            com.google.android.gms.common.internal.zzx.a(this.b.equals(this.c), "Must not set scopes in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead.", new Object[] { ((Api)localObject2).e() });
            i1 = zzj.a(localArrayMap2.values(), true);
            return new zzj(this.i, new ReentrantLock(), this.n, localzzf, this.o, this.p, localArrayMap1, this.q, this.r, localArrayMap2, this.l, i1, localArrayList);
          }
        }
      }
    }
    
    public Builder a(@NonNull Handler paramHandler)
    {
      com.google.android.gms.common.internal.zzx.a(paramHandler, "Handler must not be null");
      this.n = paramHandler.getLooper();
      return this;
    }
    
    public Builder a(@NonNull Api<? extends Api.ApiOptions.NotRequiredOptions> paramApi)
    {
      com.google.android.gms.common.internal.zzx.a(paramApi, "Api must not be null");
      this.j.put(paramApi, null);
      paramApi = paramApi.a().a(null);
      this.c.addAll(paramApi);
      this.b.addAll(paramApi);
      return this;
    }
    
    public <O extends Api.ApiOptions.HasOptions> Builder a(@NonNull Api<O> paramApi, @NonNull O paramO)
    {
      com.google.android.gms.common.internal.zzx.a(paramApi, "Api must not be null");
      com.google.android.gms.common.internal.zzx.a(paramO, "Null options are not permitted for this Api");
      this.j.put(paramApi, paramO);
      paramApi = paramApi.a().a(paramO);
      this.c.addAll(paramApi);
      this.b.addAll(paramApi);
      return this;
    }
    
    public Builder a(@NonNull GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks)
    {
      com.google.android.gms.common.internal.zzx.a(paramConnectionCallbacks, "Listener must not be null");
      this.q.add(paramConnectionCallbacks);
      return this;
    }
    
    public Builder a(@NonNull GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
    {
      com.google.android.gms.common.internal.zzx.a(paramOnConnectionFailedListener, "Listener must not be null");
      this.r.add(paramOnConnectionFailedListener);
      return this;
    }
    
    public zzf a()
    {
      zzro localzzro = zzro.a;
      if (this.j.containsKey(zzrl.g)) {
        localzzro = (zzro)this.j.get(zzrl.g);
      }
      return new zzf(this.a, this.b, this.h, this.d, this.e, this.f, this.g, localzzro);
    }
    
    public GoogleApiClient b()
    {
      boolean bool;
      if (!this.j.isEmpty()) {
        bool = true;
      }
      for (;;)
      {
        com.google.android.gms.common.internal.zzx.b(bool, "must call addApi() to add at least one API");
        GoogleApiClient localGoogleApiClient = c();
        synchronized (GoogleApiClient.j())
        {
          GoogleApiClient.j().add(localGoogleApiClient);
          if (this.l >= 0) {
            a(localGoogleApiClient);
          }
          return localGoogleApiClient;
          bool = false;
        }
      }
    }
  }
  
  public static abstract interface ConnectionCallbacks
  {
    public abstract void onConnected(@Nullable Bundle paramBundle);
    
    public abstract void onConnectionSuspended(int paramInt);
  }
  
  public static abstract interface OnConnectionFailedListener
  {
    public abstract void onConnectionFailed(@NonNull ConnectionResult paramConnectionResult);
  }
  
  public static abstract interface zza
  {
    public abstract void a(@NonNull ConnectionResult paramConnectionResult);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/GoogleApiClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */