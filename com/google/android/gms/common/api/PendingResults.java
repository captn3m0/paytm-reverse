package com.google.android.gms.common.api;

import com.google.android.gms.common.api.internal.zzb;

public final class PendingResults
{
  private static final class zza<R extends Result>
    extends zzb<R>
  {
    private final R a;
    
    protected R zzc(Status paramStatus)
    {
      if (paramStatus.f() != this.a.getStatus().f()) {
        throw new UnsupportedOperationException("Creating failed results is not supported");
      }
      return this.a;
    }
  }
  
  private static final class zzb<R extends Result>
    extends zzb<R>
  {
    private final R a;
    
    protected R zzc(Status paramStatus)
    {
      return this.a;
    }
  }
  
  private static final class zzc<R extends Result>
    extends zzb<R>
  {
    protected R zzc(Status paramStatus)
    {
      throw new UnsupportedOperationException("Creating failed results is not supported");
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/PendingResults.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */