package com.google.android.gms.common.api;

public final class BatchResult
  implements Result
{
  private final Status a;
  private final PendingResult<?>[] b;
  
  BatchResult(Status paramStatus, PendingResult<?>[] paramArrayOfPendingResult)
  {
    this.a = paramStatus;
    this.b = paramArrayOfPendingResult;
  }
  
  public Status getStatus()
  {
    return this.a;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/BatchResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */