package com.google.android.gms.common.api;

import android.support.annotation.NonNull;

public abstract class ResultCallbacks<R extends Result>
  implements ResultCallback<R>
{
  public abstract void a(@NonNull Status paramStatus);
  
  public abstract void b(@NonNull R paramR);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/api/ResultCallbacks.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */