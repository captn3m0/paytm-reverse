package com.google.android.gms.common.data;

import java.util.Iterator;

public abstract class AbstractDataBuffer<T>
  implements DataBuffer<T>
{
  protected final DataHolder a;
  
  protected AbstractDataBuffer(DataHolder paramDataHolder)
  {
    this.a = paramDataHolder;
    if (this.a != null) {
      this.a.a(this);
    }
  }
  
  public int a()
  {
    if (this.a == null) {
      return 0;
    }
    return this.a.g();
  }
  
  @Deprecated
  public final void b()
  {
    release();
  }
  
  public Iterator<T> iterator()
  {
    return new zzb(this);
  }
  
  public void release()
  {
    if (this.a != null) {
      this.a.i();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/data/AbstractDataBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */