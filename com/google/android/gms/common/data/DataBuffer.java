package com.google.android.gms.common.data;

import com.google.android.gms.common.api.Releasable;

public abstract interface DataBuffer<T>
  extends Releasable, Iterable<T>
{
  public abstract int a();
  
  public abstract T a(int paramInt);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/data/DataBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */