package com.google.android.gms.common.data;

import android.net.Uri;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzx;

public abstract class zzc
{
  protected final DataHolder a;
  protected int b;
  private int c;
  
  public zzc(DataHolder paramDataHolder, int paramInt)
  {
    this.a = ((DataHolder)zzx.a(paramDataHolder));
    a(paramInt);
  }
  
  protected void a(int paramInt)
  {
    if ((paramInt >= 0) && (paramInt < this.a.g())) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.a(bool);
      this.b = paramInt;
      this.c = this.a.a(this.b);
      return;
    }
  }
  
  public boolean a_(String paramString)
  {
    return this.a.a(paramString);
  }
  
  protected long b(String paramString)
  {
    return this.a.a(paramString, this.b, this.c);
  }
  
  protected int c(String paramString)
  {
    return this.a.b(paramString, this.b, this.c);
  }
  
  protected boolean d(String paramString)
  {
    return this.a.d(paramString, this.b, this.c);
  }
  
  protected String e(String paramString)
  {
    return this.a.c(paramString, this.b, this.c);
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if ((paramObject instanceof zzc))
    {
      paramObject = (zzc)paramObject;
      bool1 = bool2;
      if (zzw.a(Integer.valueOf(((zzc)paramObject).b), Integer.valueOf(this.b)))
      {
        bool1 = bool2;
        if (zzw.a(Integer.valueOf(((zzc)paramObject).c), Integer.valueOf(this.c)))
        {
          bool1 = bool2;
          if (((zzc)paramObject).a == this.a) {
            bool1 = true;
          }
        }
      }
    }
    return bool1;
  }
  
  protected float f(String paramString)
  {
    return this.a.e(paramString, this.b, this.c);
  }
  
  protected byte[] g(String paramString)
  {
    return this.a.f(paramString, this.b, this.c);
  }
  
  protected Uri h(String paramString)
  {
    return this.a.g(paramString, this.b, this.c);
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Integer.valueOf(this.b), Integer.valueOf(this.c), this.a });
  }
  
  protected boolean i(String paramString)
  {
    return this.a.h(paramString, this.b, this.c);
  }
  
  protected int l_()
  {
    return this.b;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/data/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */