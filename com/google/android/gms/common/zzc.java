package com.google.android.gms.common;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzn;

public class zzc
{
  private static final zzc a = new zzc();
  public static final int b = zze.b;
  
  public static zzc b()
  {
    return a;
  }
  
  private String b(@Nullable Context paramContext, @Nullable String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("gcore_");
    localStringBuilder.append(b);
    localStringBuilder.append("-");
    if (!TextUtils.isEmpty(paramString)) {
      localStringBuilder.append(paramString);
    }
    localStringBuilder.append("-");
    if (paramContext != null) {
      localStringBuilder.append(paramContext.getPackageName());
    }
    localStringBuilder.append("-");
    if (paramContext != null) {}
    try
    {
      localStringBuilder.append(paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 0).versionCode);
      return localStringBuilder.toString();
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      for (;;) {}
    }
  }
  
  public int a(Context paramContext)
  {
    int j = zze.c(paramContext);
    int i = j;
    if (zze.c(paramContext, j)) {
      i = 18;
    }
    return i;
  }
  
  @Nullable
  public PendingIntent a(Context paramContext, int paramInt1, int paramInt2, @Nullable String paramString)
  {
    paramString = a(paramContext, paramInt1, paramString);
    if (paramString == null) {
      return null;
    }
    return PendingIntent.getActivity(paramContext, paramInt2, paramString, 268435456);
  }
  
  @Nullable
  public Intent a(Context paramContext, int paramInt, @Nullable String paramString)
  {
    switch (paramInt)
    {
    default: 
      return null;
    case 1: 
    case 2: 
      return zzn.a("com.google.android.gms", b(paramContext, paramString));
    case 42: 
      return zzn.a();
    }
    return zzn.a("com.google.android.gms");
  }
  
  public boolean a(int paramInt)
  {
    return zze.f(paramInt);
  }
  
  public boolean a(Context paramContext, int paramInt)
  {
    return zze.c(paramContext, paramInt);
  }
  
  public boolean a(Context paramContext, String paramString)
  {
    return zze.a(paramContext, paramString);
  }
  
  public int b(Context paramContext)
  {
    return zze.i(paramContext);
  }
  
  @Deprecated
  @Nullable
  public Intent b(int paramInt)
  {
    return a(null, paramInt, null);
  }
  
  public void c(Context paramContext)
    throws GooglePlayServicesRepairableException, GooglePlayServicesNotAvailableException
  {
    zze.d(paramContext);
  }
  
  public void d(Context paramContext)
  {
    zze.e(paramContext);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */