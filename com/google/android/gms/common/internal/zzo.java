package com.google.android.gms.common.internal;

import android.content.Context;
import android.util.Log;
import com.google.android.gms.internal.zzqt;

public final class zzo
{
  public static final int a = 23 - " PII_LOG".length();
  private static final String b = null;
  private final String c;
  private final String d;
  
  public zzo(String paramString)
  {
    this(paramString, b);
  }
  
  public zzo(String paramString1, String paramString2)
  {
    zzx.a(paramString1, "log tag cannot be null");
    if (paramString1.length() <= 23) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.b(bool, "tag \"%s\" is longer than the %d character maximum", new Object[] { paramString1, Integer.valueOf(23) });
      this.c = paramString1;
      if ((paramString2 != null) && (paramString2.length() > 0)) {
        break;
      }
      this.d = b;
      return;
    }
    this.d = paramString2;
  }
  
  private String a(String paramString)
  {
    if (this.d == null) {
      return paramString;
    }
    return this.d.concat(paramString);
  }
  
  public void a(Context paramContext, String paramString1, String paramString2, Throwable paramThrowable)
  {
    StackTraceElement[] arrayOfStackTraceElement = paramThrowable.getStackTrace();
    StringBuilder localStringBuilder = new StringBuilder();
    int i = 0;
    while ((i < arrayOfStackTraceElement.length) && (i < 2))
    {
      localStringBuilder.append(arrayOfStackTraceElement[i].toString());
      localStringBuilder.append("\n");
      i += 1;
    }
    paramContext = new zzqt(paramContext, 10);
    paramContext.a("GMS_WTF", null, new String[] { "GMS_WTF", localStringBuilder.toString() });
    paramContext.a();
    if (a(7))
    {
      Log.e(paramString1, a(paramString2), paramThrowable);
      Log.wtf(paramString1, a(paramString2), paramThrowable);
    }
  }
  
  public void a(String paramString1, String paramString2)
  {
    if (a(3)) {
      Log.d(paramString1, a(paramString2));
    }
  }
  
  public void a(String paramString1, String paramString2, Throwable paramThrowable)
  {
    if (a(5)) {
      Log.w(paramString1, a(paramString2), paramThrowable);
    }
  }
  
  public boolean a(int paramInt)
  {
    return Log.isLoggable(this.c, paramInt);
  }
  
  public void b(String paramString1, String paramString2)
  {
    if (a(5)) {
      Log.w(paramString1, a(paramString2));
    }
  }
  
  public void b(String paramString1, String paramString2, Throwable paramThrowable)
  {
    if (a(6)) {
      Log.e(paramString1, a(paramString2), paramThrowable);
    }
  }
  
  public void c(String paramString1, String paramString2)
  {
    if (a(6)) {
      Log.e(paramString1, a(paramString2));
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/internal/zzo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */