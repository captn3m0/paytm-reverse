package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzc
  implements Parcelable.Creator<AuthAccountRequest>
{
  static void a(AuthAccountRequest paramAuthAccountRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramAuthAccountRequest.a);
    zzb.a(paramParcel, 2, paramAuthAccountRequest.b, false);
    zzb.a(paramParcel, 3, paramAuthAccountRequest.c, paramInt, false);
    zzb.a(paramParcel, 4, paramAuthAccountRequest.d, false);
    zzb.a(paramParcel, 5, paramAuthAccountRequest.e, false);
    zzb.a(paramParcel, i);
  }
  
  public AuthAccountRequest a(Parcel paramParcel)
  {
    Integer localInteger1 = null;
    int j = zza.b(paramParcel);
    int i = 0;
    Integer localInteger2 = null;
    Scope[] arrayOfScope = null;
    IBinder localIBinder = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localIBinder = zza.q(paramParcel, k);
        break;
      case 3: 
        arrayOfScope = (Scope[])zza.b(paramParcel, k, Scope.CREATOR);
        break;
      case 4: 
        localInteger2 = zza.h(paramParcel, k);
        break;
      case 5: 
        localInteger1 = zza.h(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new AuthAccountRequest(i, localIBinder, arrayOfScope, localInteger2, localInteger1);
  }
  
  public AuthAccountRequest[] a(int paramInt)
  {
    return new AuthAccountRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/internal/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */