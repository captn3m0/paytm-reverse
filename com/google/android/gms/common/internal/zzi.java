package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzi
  implements Parcelable.Creator<GetServiceRequest>
{
  static void a(GetServiceRequest paramGetServiceRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramGetServiceRequest.a);
    zzb.a(paramParcel, 2, paramGetServiceRequest.b);
    zzb.a(paramParcel, 3, paramGetServiceRequest.c);
    zzb.a(paramParcel, 4, paramGetServiceRequest.d, false);
    zzb.a(paramParcel, 5, paramGetServiceRequest.e, false);
    zzb.a(paramParcel, 6, paramGetServiceRequest.f, paramInt, false);
    zzb.a(paramParcel, 7, paramGetServiceRequest.g, false);
    zzb.a(paramParcel, 8, paramGetServiceRequest.h, paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public GetServiceRequest a(Parcel paramParcel)
  {
    int i = 0;
    Account localAccount = null;
    int m = zza.b(paramParcel);
    Bundle localBundle = null;
    Scope[] arrayOfScope = null;
    IBinder localIBinder = null;
    String str = null;
    int j = 0;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        j = zza.g(paramParcel, n);
        break;
      case 3: 
        i = zza.g(paramParcel, n);
        break;
      case 4: 
        str = zza.p(paramParcel, n);
        break;
      case 5: 
        localIBinder = zza.q(paramParcel, n);
        break;
      case 6: 
        arrayOfScope = (Scope[])zza.b(paramParcel, n, Scope.CREATOR);
        break;
      case 7: 
        localBundle = zza.r(paramParcel, n);
        break;
      case 8: 
        localAccount = (Account)zza.a(paramParcel, n, Account.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new GetServiceRequest(k, j, i, str, localIBinder, arrayOfScope, localBundle, localAccount);
  }
  
  public GetServiceRequest[] a(int paramInt)
  {
    return new GetServiceRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/internal/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */