package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.support.annotation.BinderThread;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api.zzb;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.GoogleApiClient.zza;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.zzc;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class zzj<T extends IInterface>
  implements Api.zzb, zzk.zza
{
  public static final String[] d = { "service_esmobile", "service_googleme" };
  private int a;
  final Handler b;
  protected AtomicInteger c = new AtomicInteger(0);
  private long e;
  private long f;
  private int g;
  private long h;
  private final Context i;
  private final zzf j;
  private final Looper k;
  private final zzl l;
  private final zzc m;
  private final Object n = new Object();
  private final Object o = new Object();
  private zzs p;
  private GoogleApiClient.zza q = new zzf();
  private T r;
  private final ArrayList<zzj<T>.zzc<?>> s = new ArrayList();
  private zzj<T>.zze t;
  private int u = 1;
  private final Set<Scope> v;
  private final Account w;
  private final GoogleApiClient.ConnectionCallbacks x;
  private final GoogleApiClient.OnConnectionFailedListener y;
  private final int z;
  
  protected zzj(Context paramContext, Looper paramLooper, int paramInt, zzf paramzzf, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    this(paramContext, paramLooper, zzl.a(paramContext), zzc.b(), paramInt, paramzzf, (GoogleApiClient.ConnectionCallbacks)zzx.a(paramConnectionCallbacks), (GoogleApiClient.OnConnectionFailedListener)zzx.a(paramOnConnectionFailedListener));
  }
  
  protected zzj(Context paramContext, Looper paramLooper, zzl paramzzl, zzc paramzzc, int paramInt, zzf paramzzf, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    this.i = ((Context)zzx.a(paramContext, "Context must not be null"));
    this.k = ((Looper)zzx.a(paramLooper, "Looper must not be null"));
    this.l = ((zzl)zzx.a(paramzzl, "Supervisor must not be null"));
    this.m = ((zzc)zzx.a(paramzzc, "API availability must not be null"));
    this.b = new zzb(paramLooper);
    this.z = paramInt;
    this.j = ((zzf)zzx.a(paramzzf));
    this.w = paramzzf.b();
    this.v = b(paramzzf.f());
    this.x = paramConnectionCallbacks;
    this.y = paramOnConnectionFailedListener;
  }
  
  private boolean a(int paramInt1, int paramInt2, T paramT)
  {
    synchronized (this.n)
    {
      if (this.u != paramInt1) {
        return false;
      }
      b(paramInt2, paramT);
      return true;
    }
  }
  
  private Set<Scope> b(Set<Scope> paramSet)
  {
    Set localSet = a(paramSet);
    if (localSet == null) {
      return localSet;
    }
    Iterator localIterator = localSet.iterator();
    while (localIterator.hasNext()) {
      if (!paramSet.contains((Scope)localIterator.next())) {
        throw new IllegalStateException("Expanding scopes is not permitted, use implied scopes instead");
      }
    }
    return localSet;
  }
  
  private void b(int paramInt, T paramT)
  {
    boolean bool = true;
    int i1;
    int i2;
    if (paramInt == 3)
    {
      i1 = 1;
      if (paramT == null) {
        break label120;
      }
      i2 = 1;
      label17:
      if (i1 != i2) {
        break label126;
      }
    }
    for (;;)
    {
      zzx.b(bool);
      for (;;)
      {
        synchronized (this.n)
        {
          this.u = paramInt;
          this.r = paramT;
          a(paramInt, paramT);
          switch (paramInt)
          {
          case 2: 
            return;
            h();
          }
        }
        a(paramT);
        continue;
        i();
      }
      i1 = 0;
      break;
      label120:
      i2 = 0;
      break label17;
      label126:
      bool = false;
    }
  }
  
  private void h()
  {
    if (this.t != null)
    {
      Log.e("GmsClient", "Calling connect() while still connected, missing disconnect() for " + a());
      this.l.b(a(), this.t, n());
      this.c.incrementAndGet();
    }
    this.t = new zze(this.c.get());
    if (!this.l.a(a(), this.t, n()))
    {
      Log.e("GmsClient", "unable to connect to service: " + a());
      a(8, this.c.get());
    }
  }
  
  private void i()
  {
    if (this.t != null)
    {
      this.l.b(a(), this.t, n());
      this.t = null;
    }
  }
  
  @NonNull
  protected abstract String a();
  
  @NonNull
  protected Set<Scope> a(@NonNull Set<Scope> paramSet)
  {
    return paramSet;
  }
  
  @CallSuper
  protected void a(int paramInt)
  {
    this.a = paramInt;
    this.e = System.currentTimeMillis();
  }
  
  protected void a(int paramInt1, int paramInt2)
  {
    this.b.sendMessage(this.b.obtainMessage(5, paramInt2, -1, new zzh(paramInt1)));
  }
  
  @BinderThread
  protected void a(int paramInt1, IBinder paramIBinder, Bundle paramBundle, int paramInt2)
  {
    this.b.sendMessage(this.b.obtainMessage(1, paramInt2, -1, new zzg(paramInt1, paramIBinder, paramBundle)));
  }
  
  void a(int paramInt, T paramT) {}
  
  @CallSuper
  protected void a(@NonNull T paramT)
  {
    this.f = System.currentTimeMillis();
  }
  
  @CallSuper
  protected void a(ConnectionResult paramConnectionResult)
  {
    this.g = paramConnectionResult.c();
    this.h = System.currentTimeMillis();
  }
  
  public void a(@NonNull GoogleApiClient.zza paramzza)
  {
    this.q = ((GoogleApiClient.zza)zzx.a(paramzza, "Connection progress callbacks cannot be null."));
    b(2, null);
  }
  
  @WorkerThread
  public void a(zzp arg1, Set<Scope> paramSet)
  {
    try
    {
      Object localObject = a_();
      localObject = new GetServiceRequest(this.z).a(this.i.getPackageName()).a((Bundle)localObject);
      if (paramSet != null) {
        ((GetServiceRequest)localObject).a(paramSet);
      }
      if (l()) {
        ((GetServiceRequest)localObject).a(s()).a(???);
      }
      return;
    }
    catch (DeadObjectException ???)
    {
      synchronized (this.o)
      {
        while (this.p != null)
        {
          this.p.a(new zzd(this, this.c.get()), (GetServiceRequest)localObject);
          return;
          if (w())
          {
            ((GetServiceRequest)localObject).a(this.w);
            continue;
            ??? = ???;
            Log.w("GmsClient", "service died");
            b(1);
            return;
          }
        }
        Log.w("GmsClient", "mServiceBroker is null, client disconnected");
      }
    }
    catch (RemoteException ???)
    {
      Log.w("GmsClient", "Remote exception occurred", ???);
    }
  }
  
  public void a(String paramString, FileDescriptor arg2, PrintWriter paramPrintWriter, String[] paramArrayOfString)
  {
    for (;;)
    {
      synchronized (this.n)
      {
        int i1 = this.u;
        paramArrayOfString = this.r;
        paramPrintWriter.append(paramString).append("mConnectState=");
        switch (i1)
        {
        default: 
          paramPrintWriter.print("UNKNOWN");
          paramPrintWriter.append(" mService=");
          if (paramArrayOfString != null) {
            break label407;
          }
          paramPrintWriter.println("null");
          ??? = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
          if (this.f > 0L) {
            paramPrintWriter.append(paramString).append("lastConnectedTime=").println(this.f + " " + ???.format(new Date(this.f)));
          }
          if (this.e > 0L) {
            paramPrintWriter.append(paramString).append("lastSuspendedCause=");
          }
          switch (this.a)
          {
          default: 
            paramPrintWriter.append(String.valueOf(this.a));
            paramPrintWriter.append(" lastSuspendedTime=").println(this.e + " " + ???.format(new Date(this.e)));
            if (this.h > 0L)
            {
              paramPrintWriter.append(paramString).append("lastFailedStatus=").append(CommonStatusCodes.a(this.g));
              paramPrintWriter.append(" lastFailedTime=").println(this.h + " " + ???.format(new Date(this.h)));
            }
            return;
          }
          break;
        }
      }
      paramPrintWriter.print("CONNECTING");
      continue;
      paramPrintWriter.print("CONNECTED");
      continue;
      paramPrintWriter.print("DISCONNECTING");
      continue;
      paramPrintWriter.print("DISCONNECTED");
      continue;
      label407:
      paramPrintWriter.append(b()).append("@").println(Integer.toHexString(System.identityHashCode(paramArrayOfString.asBinder())));
      continue;
      paramPrintWriter.append("CAUSE_SERVICE_DISCONNECTED");
      continue;
      paramPrintWriter.append("CAUSE_NETWORK_LOST");
    }
  }
  
  protected Bundle a_()
  {
    return new Bundle();
  }
  
  @Nullable
  protected abstract T b(IBinder paramIBinder);
  
  @NonNull
  protected abstract String b();
  
  public void b(int paramInt)
  {
    this.b.sendMessage(this.b.obtainMessage(4, this.c.get(), paramInt));
  }
  
  public boolean d()
  {
    return false;
  }
  
  public Intent e()
  {
    throw new UnsupportedOperationException("Not a sign in API");
  }
  
  public void f()
  {
    this.c.incrementAndGet();
    synchronized (this.s)
    {
      int i2 = this.s.size();
      int i1 = 0;
      while (i1 < i2)
      {
        ((zzc)this.s.get(i1)).e();
        i1 += 1;
      }
      this.s.clear();
    }
    synchronized (this.o)
    {
      this.p = null;
      b(1, null);
      return;
      localObject2 = finally;
      throw ((Throwable)localObject2);
    }
  }
  
  public boolean k()
  {
    for (;;)
    {
      synchronized (this.n)
      {
        if (this.u == 3)
        {
          bool = true;
          return bool;
        }
      }
      boolean bool = false;
    }
  }
  
  public boolean l()
  {
    return false;
  }
  
  @Nullable
  public IBinder m()
  {
    synchronized (this.o)
    {
      if (this.p == null) {
        return null;
      }
      IBinder localIBinder = this.p.asBinder();
      return localIBinder;
    }
  }
  
  public Bundle m_()
  {
    return null;
  }
  
  @Nullable
  protected final String n()
  {
    return this.j.i();
  }
  
  public void o()
  {
    int i1 = this.m.a(this.i);
    if (i1 != 0)
    {
      b(1, null);
      this.q = new zzf();
      this.b.sendMessage(this.b.obtainMessage(3, this.c.get(), i1));
      return;
    }
    a(new zzf());
  }
  
  public boolean p()
  {
    for (;;)
    {
      synchronized (this.n)
      {
        if (this.u == 2)
        {
          bool = true;
          return bool;
        }
      }
      boolean bool = false;
    }
  }
  
  public final Context q()
  {
    return this.i;
  }
  
  public final Looper r()
  {
    return this.k;
  }
  
  public final Account s()
  {
    if (this.w != null) {
      return this.w;
    }
    return new Account("<<default account>>", "com.google");
  }
  
  protected final zzf t()
  {
    return this.j;
  }
  
  protected final void u()
  {
    if (!k()) {
      throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
    }
  }
  
  public final T v()
    throws DeadObjectException
  {
    synchronized (this.n)
    {
      if (this.u == 4) {
        throw new DeadObjectException();
      }
    }
    u();
    if (this.r != null) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.a(bool, "Client is connected but service is null");
      IInterface localIInterface = this.r;
      return localIInterface;
    }
  }
  
  public boolean w()
  {
    return false;
  }
  
  private abstract class zza
    extends zzj<T>.zzc<Boolean>
  {
    public final int a;
    public final Bundle b;
    
    @BinderThread
    protected zza(int paramInt, Bundle paramBundle)
    {
      super(Boolean.valueOf(true));
      this.a = paramInt;
      this.b = paramBundle;
    }
    
    protected abstract void a(ConnectionResult paramConnectionResult);
    
    protected void a(Boolean paramBoolean)
    {
      Object localObject = null;
      if (paramBoolean == null) {
        zzj.a(zzj.this, 1, null);
      }
      do
      {
        return;
        switch (this.a)
        {
        default: 
          zzj.a(zzj.this, 1, null);
          paramBoolean = (Boolean)localObject;
          if (this.b != null) {
            paramBoolean = (PendingIntent)this.b.getParcelable("pendingIntent");
          }
          a(new ConnectionResult(this.a, paramBoolean));
          return;
        }
      } while (a());
      zzj.a(zzj.this, 1, null);
      a(new ConnectionResult(8, null));
      return;
      zzj.a(zzj.this, 1, null);
      throw new IllegalStateException("A fatal developer error has occurred. Check the logs for further information.");
    }
    
    protected abstract boolean a();
    
    protected void b() {}
  }
  
  final class zzb
    extends Handler
  {
    public zzb(Looper paramLooper)
    {
      super();
    }
    
    private void a(Message paramMessage)
    {
      paramMessage = (zzj.zzc)paramMessage.obj;
      paramMessage.b();
      paramMessage.d();
    }
    
    private boolean b(Message paramMessage)
    {
      return (paramMessage.what == 2) || (paramMessage.what == 1) || (paramMessage.what == 5);
    }
    
    public void handleMessage(Message paramMessage)
    {
      if (zzj.this.c.get() != paramMessage.arg1)
      {
        if (b(paramMessage)) {
          a(paramMessage);
        }
        return;
      }
      if (((paramMessage.what == 1) || (paramMessage.what == 5)) && (!zzj.this.p()))
      {
        a(paramMessage);
        return;
      }
      if (paramMessage.what == 3)
      {
        paramMessage = new ConnectionResult(paramMessage.arg2, null);
        zzj.b(zzj.this).a(paramMessage);
        zzj.this.a(paramMessage);
        return;
      }
      if (paramMessage.what == 4)
      {
        zzj.a(zzj.this, 4, null);
        if (zzj.c(zzj.this) != null) {
          zzj.c(zzj.this).onConnectionSuspended(paramMessage.arg2);
        }
        zzj.this.a(paramMessage.arg2);
        zzj.a(zzj.this, 4, 1, null);
        return;
      }
      if ((paramMessage.what == 2) && (!zzj.this.k()))
      {
        a(paramMessage);
        return;
      }
      if (b(paramMessage))
      {
        ((zzj.zzc)paramMessage.obj).c();
        return;
      }
      Log.wtf("GmsClient", "Don't know how to handle message: " + paramMessage.what, new Exception());
    }
  }
  
  protected abstract class zzc<TListener>
  {
    private TListener a;
    private boolean b;
    
    public zzc()
    {
      Object localObject;
      this.a = localObject;
      this.b = false;
    }
    
    protected abstract void a(TListener paramTListener);
    
    protected abstract void b();
    
    /* Error */
    public void c()
    {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield 24	com/google/android/gms/common/internal/zzj$zzc:a	Ljava/lang/Object;
      //   6: astore_1
      //   7: aload_0
      //   8: getfield 26	com/google/android/gms/common/internal/zzj$zzc:b	Z
      //   11: ifeq +33 -> 44
      //   14: ldc 35
      //   16: new 37	java/lang/StringBuilder
      //   19: dup
      //   20: invokespecial 38	java/lang/StringBuilder:<init>	()V
      //   23: ldc 40
      //   25: invokevirtual 44	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   28: aload_0
      //   29: invokevirtual 47	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   32: ldc 49
      //   34: invokevirtual 44	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   37: invokevirtual 53	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   40: invokestatic 59	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
      //   43: pop
      //   44: aload_0
      //   45: monitorexit
      //   46: aload_1
      //   47: ifnull +34 -> 81
      //   50: aload_0
      //   51: aload_1
      //   52: invokevirtual 61	com/google/android/gms/common/internal/zzj$zzc:a	(Ljava/lang/Object;)V
      //   55: aload_0
      //   56: monitorenter
      //   57: aload_0
      //   58: iconst_1
      //   59: putfield 26	com/google/android/gms/common/internal/zzj$zzc:b	Z
      //   62: aload_0
      //   63: monitorexit
      //   64: aload_0
      //   65: invokevirtual 63	com/google/android/gms/common/internal/zzj$zzc:d	()V
      //   68: return
      //   69: astore_1
      //   70: aload_0
      //   71: monitorexit
      //   72: aload_1
      //   73: athrow
      //   74: astore_1
      //   75: aload_0
      //   76: invokevirtual 65	com/google/android/gms/common/internal/zzj$zzc:b	()V
      //   79: aload_1
      //   80: athrow
      //   81: aload_0
      //   82: invokevirtual 65	com/google/android/gms/common/internal/zzj$zzc:b	()V
      //   85: goto -30 -> 55
      //   88: astore_1
      //   89: aload_0
      //   90: monitorexit
      //   91: aload_1
      //   92: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	93	0	this	zzc
      //   6	46	1	localObject1	Object
      //   69	4	1	localObject2	Object
      //   74	6	1	localRuntimeException	RuntimeException
      //   88	4	1	localObject3	Object
      // Exception table:
      //   from	to	target	type
      //   2	44	69	finally
      //   44	46	69	finally
      //   70	72	69	finally
      //   50	55	74	java/lang/RuntimeException
      //   57	64	88	finally
      //   89	91	88	finally
    }
    
    public void d()
    {
      e();
      synchronized (zzj.d(zzj.this))
      {
        zzj.d(zzj.this).remove(this);
        return;
      }
    }
    
    public void e()
    {
      try
      {
        this.a = null;
        return;
      }
      finally {}
    }
  }
  
  public static final class zzd
    extends zzr.zza
  {
    private zzj a;
    private final int b;
    
    public zzd(@NonNull zzj paramzzj, int paramInt)
    {
      this.a = paramzzj;
      this.b = paramInt;
    }
    
    private void a()
    {
      this.a = null;
    }
    
    @BinderThread
    public void a(int paramInt, @Nullable Bundle paramBundle)
    {
      Log.wtf("GmsClient", "received deprecated onAccountValidationComplete callback, ignoring", new Exception());
    }
    
    @BinderThread
    public void a(int paramInt, @NonNull IBinder paramIBinder, @Nullable Bundle paramBundle)
    {
      zzx.a(this.a, "onPostInitComplete can be called only once per call to getRemoteService");
      this.a.a(paramInt, paramIBinder, paramBundle, this.b);
      a();
    }
  }
  
  public final class zze
    implements ServiceConnection
  {
    private final int b;
    
    public zze(int paramInt)
    {
      this.b = paramInt;
    }
    
    public void onServiceConnected(ComponentName arg1, IBinder paramIBinder)
    {
      zzx.a(paramIBinder, "Expecting a valid IBinder");
      synchronized (zzj.a(zzj.this))
      {
        zzj.a(zzj.this, zzs.zza.a(paramIBinder));
        zzj.this.a(0, this.b);
        return;
      }
    }
    
    public void onServiceDisconnected(ComponentName arg1)
    {
      synchronized (zzj.a(zzj.this))
      {
        zzj.a(zzj.this, null);
        zzj.this.b.sendMessage(zzj.this.b.obtainMessage(4, this.b, 1));
        return;
      }
    }
  }
  
  protected class zzf
    implements GoogleApiClient.zza
  {
    public zzf() {}
    
    public void a(@NonNull ConnectionResult paramConnectionResult)
    {
      if (paramConnectionResult.b()) {
        zzj.this.a(null, zzj.e(zzj.this));
      }
      while (zzj.f(zzj.this) == null) {
        return;
      }
      zzj.f(zzj.this).onConnectionFailed(paramConnectionResult);
    }
  }
  
  protected final class zzg
    extends zzj<T>.zza
  {
    public final IBinder e;
    
    @BinderThread
    public zzg(int paramInt, IBinder paramIBinder, Bundle paramBundle)
    {
      super(paramInt, paramBundle);
      this.e = paramIBinder;
    }
    
    protected void a(ConnectionResult paramConnectionResult)
    {
      if (zzj.f(zzj.this) != null) {
        zzj.f(zzj.this).onConnectionFailed(paramConnectionResult);
      }
      zzj.this.a(paramConnectionResult);
    }
    
    protected boolean a()
    {
      do
      {
        try
        {
          String str = this.e.getInterfaceDescriptor();
          if (!zzj.this.b().equals(str))
          {
            Log.e("GmsClient", "service descriptor mismatch: " + zzj.this.b() + " vs. " + str);
            return false;
          }
        }
        catch (RemoteException localRemoteException)
        {
          Log.w("GmsClient", "service probably died");
          return false;
        }
        localObject = zzj.this.b(this.e);
      } while ((localObject == null) || (!zzj.a(zzj.this, 2, 3, (IInterface)localObject)));
      Object localObject = zzj.this.m_();
      if (zzj.c(zzj.this) != null) {
        zzj.c(zzj.this).onConnected((Bundle)localObject);
      }
      return true;
    }
  }
  
  protected final class zzh
    extends zzj<T>.zza
  {
    @BinderThread
    public zzh(int paramInt)
    {
      super(paramInt, null);
    }
    
    protected void a(ConnectionResult paramConnectionResult)
    {
      zzj.b(zzj.this).a(paramConnectionResult);
      zzj.this.a(paramConnectionResult);
    }
    
    protected boolean a()
    {
      zzj.b(zzj.this).a(ConnectionResult.a);
      return true;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/internal/zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */