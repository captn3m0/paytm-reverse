package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.content.Context;
import android.view.View;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.internal.zzro;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public final class zzf
{
  private final Account a;
  private final Set<Scope> b;
  private final Set<Scope> c;
  private final Map<Api<?>, zza> d;
  private final int e;
  private final View f;
  private final String g;
  private final String h;
  private final zzro i;
  private Integer j;
  
  public zzf(Account paramAccount, Set<Scope> paramSet, Map<Api<?>, zza> paramMap, int paramInt, View paramView, String paramString1, String paramString2, zzro paramzzro)
  {
    this.a = paramAccount;
    if (paramSet == null) {}
    for (paramAccount = Collections.EMPTY_SET;; paramAccount = Collections.unmodifiableSet(paramSet))
    {
      this.b = paramAccount;
      paramAccount = paramMap;
      if (paramMap == null) {
        paramAccount = Collections.EMPTY_MAP;
      }
      this.d = paramAccount;
      this.f = paramView;
      this.e = paramInt;
      this.g = paramString1;
      this.h = paramString2;
      this.i = paramzzro;
      paramAccount = new HashSet(this.b);
      paramSet = this.d.values().iterator();
      while (paramSet.hasNext()) {
        paramAccount.addAll(((zza)paramSet.next()).a);
      }
    }
    this.c = Collections.unmodifiableSet(paramAccount);
  }
  
  public static zzf a(Context paramContext)
  {
    return new GoogleApiClient.Builder(paramContext).a();
  }
  
  @Deprecated
  public String a()
  {
    if (this.a != null) {
      return this.a.name;
    }
    return null;
  }
  
  public Set<Scope> a(Api<?> paramApi)
  {
    paramApi = (zza)this.d.get(paramApi);
    if ((paramApi == null) || (paramApi.a.isEmpty())) {
      return this.b;
    }
    HashSet localHashSet = new HashSet(this.b);
    localHashSet.addAll(paramApi.a);
    return localHashSet;
  }
  
  public void a(Integer paramInteger)
  {
    this.j = paramInteger;
  }
  
  public Account b()
  {
    return this.a;
  }
  
  public Account c()
  {
    if (this.a != null) {
      return this.a;
    }
    return new Account("<<default account>>", "com.google");
  }
  
  public int d()
  {
    return this.e;
  }
  
  public Set<Scope> e()
  {
    return this.b;
  }
  
  public Set<Scope> f()
  {
    return this.c;
  }
  
  public Map<Api<?>, zza> g()
  {
    return this.d;
  }
  
  public String h()
  {
    return this.g;
  }
  
  public String i()
  {
    return this.h;
  }
  
  public View j()
  {
    return this.f;
  }
  
  public zzro k()
  {
    return this.i;
  }
  
  public Integer l()
  {
    return this.j;
  }
  
  public static final class zza
  {
    public final Set<Scope> a;
    public final boolean b;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/internal/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */