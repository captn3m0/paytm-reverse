package com.google.android.gms.common.stats;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.internal.zzlz;
import com.google.android.gms.internal.zzmp;
import com.google.android.gms.internal.zzmv;
import java.util.List;

public class zzi
{
  private static String a = "WakeLockTracker";
  private static zzi b = new zzi();
  private static Integer c;
  
  public static zzi a()
  {
    return b;
  }
  
  private static boolean a(Context paramContext)
  {
    if (c == null) {
      c = Integer.valueOf(b());
    }
    return c.intValue() != zzd.b;
  }
  
  private static int b()
  {
    try
    {
      if (zzmp.a()) {
        return ((Integer)zzc.zzb.a.c()).intValue();
      }
      int i = zzd.b;
      return i;
    }
    catch (SecurityException localSecurityException) {}
    return zzd.b;
  }
  
  public void a(Context paramContext, String paramString1, int paramInt1, String paramString2, String paramString3, int paramInt2, List<String> paramList)
  {
    a(paramContext, paramString1, paramInt1, paramString2, paramString3, paramInt2, paramList, 0L);
  }
  
  public void a(Context paramContext, String paramString1, int paramInt1, String paramString2, String paramString3, int paramInt2, List<String> paramList, long paramLong)
  {
    if (!a(paramContext)) {}
    long l;
    do
    {
      return;
      if (TextUtils.isEmpty(paramString1))
      {
        Log.e(a, "missing wakeLock key. " + paramString1);
        return;
      }
      l = System.currentTimeMillis();
    } while ((7 != paramInt1) && (8 != paramInt1) && (10 != paramInt1) && (11 != paramInt1));
    paramString1 = new WakeLockEvent(l, paramInt1, paramString2, paramInt2, paramList, paramString1, SystemClock.elapsedRealtime(), zzmv.a(paramContext), paramString3, paramContext.getPackageName(), zzmv.b(paramContext), paramLong);
    try
    {
      paramContext.startService(new Intent().setComponent(zzd.a).putExtra("com.google.android.gms.common.stats.EXTRA_LOG_EVENT", paramString1));
      return;
    }
    catch (Exception paramContext)
    {
      Log.wtf(a, paramContext);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/stats/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */