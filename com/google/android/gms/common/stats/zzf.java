package com.google.android.gms.common.stats;

public abstract class zzf
{
  public abstract long a();
  
  public abstract int b();
  
  public abstract long i();
  
  public abstract String l();
  
  public String toString()
  {
    return a() + "\t" + b() + "\t" + i() + l();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/stats/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */