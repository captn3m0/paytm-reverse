package com.google.android.gms.common.stats;

import com.google.android.gms.internal.zzlz;

public final class zzc
{
  public static zzlz<Integer> a = zzlz.a("gms:common:stats:max_num_of_events", Integer.valueOf(100));
  public static zzlz<Integer> b = zzlz.a("gms:common:stats:max_chunk_size", Integer.valueOf(100));
  
  public static final class zza
  {
    public static zzlz<Integer> a = zzlz.a("gms:common:stats:connections:level", Integer.valueOf(zzd.b));
    public static zzlz<String> b = zzlz.a("gms:common:stats:connections:ignored_calling_processes", "");
    public static zzlz<String> c = zzlz.a("gms:common:stats:connections:ignored_calling_services", "");
    public static zzlz<String> d = zzlz.a("gms:common:stats:connections:ignored_target_processes", "");
    public static zzlz<String> e = zzlz.a("gms:common:stats:connections:ignored_target_services", "com.google.android.gms.auth.GetToken");
    public static zzlz<Long> f = zzlz.a("gms:common:stats:connections:time_out_duration", Long.valueOf(600000L));
  }
  
  public static final class zzb
  {
    public static zzlz<Integer> a = zzlz.a("gms:common:stats:wakeLocks:level", Integer.valueOf(zzd.b));
    public static zzlz<Long> b = zzlz.a("gms:common:stats:wakelocks:time_out_duration", Long.valueOf(600000L));
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/stats/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */