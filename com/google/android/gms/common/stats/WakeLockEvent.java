package com.google.android.gms.common.stats;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.List;

public final class WakeLockEvent
  extends zzf
  implements SafeParcelable
{
  public static final Parcelable.Creator<WakeLockEvent> CREATOR = new zzh();
  final int a;
  private final long b;
  private int c;
  private final String d;
  private final int e;
  private final List<String> f;
  private final String g;
  private final long h;
  private int i;
  private final String j;
  private final String k;
  private final float l;
  private final long m;
  private long n;
  
  WakeLockEvent(int paramInt1, long paramLong1, int paramInt2, String paramString1, int paramInt3, List<String> paramList, String paramString2, long paramLong2, int paramInt4, String paramString3, String paramString4, float paramFloat, long paramLong3)
  {
    this.a = paramInt1;
    this.b = paramLong1;
    this.c = paramInt2;
    this.d = paramString1;
    this.j = paramString3;
    this.e = paramInt3;
    this.n = -1L;
    this.f = paramList;
    this.g = paramString2;
    this.h = paramLong2;
    this.i = paramInt4;
    this.k = paramString4;
    this.l = paramFloat;
    this.m = paramLong3;
  }
  
  public WakeLockEvent(long paramLong1, int paramInt1, String paramString1, int paramInt2, List<String> paramList, String paramString2, long paramLong2, int paramInt3, String paramString3, String paramString4, float paramFloat, long paramLong3)
  {
    this(1, paramLong1, paramInt1, paramString1, paramInt2, paramList, paramString2, paramLong2, paramInt3, paramString3, paramString4, paramFloat, paramLong3);
  }
  
  public long a()
  {
    return this.b;
  }
  
  public int b()
  {
    return this.c;
  }
  
  public String c()
  {
    return this.d;
  }
  
  public String d()
  {
    return this.j;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public int e()
  {
    return this.e;
  }
  
  public List<String> f()
  {
    return this.f;
  }
  
  public String g()
  {
    return this.g;
  }
  
  public long h()
  {
    return this.h;
  }
  
  public long i()
  {
    return this.n;
  }
  
  public int j()
  {
    return this.i;
  }
  
  public String k()
  {
    return this.k;
  }
  
  public String l()
  {
    StringBuilder localStringBuilder = new StringBuilder().append("\t").append(c()).append("\t").append(e()).append("\t");
    if (f() == null)
    {
      str = "";
      localStringBuilder = localStringBuilder.append(str).append("\t").append(j()).append("\t");
      if (d() != null) {
        break label135;
      }
      str = "";
      label80:
      localStringBuilder = localStringBuilder.append(str).append("\t");
      if (k() != null) {
        break label143;
      }
    }
    label135:
    label143:
    for (String str = "";; str = k())
    {
      return str + "\t" + m();
      str = TextUtils.join(",", f());
      break;
      str = d();
      break label80;
    }
  }
  
  public float m()
  {
    return this.l;
  }
  
  public long n()
  {
    return this.m;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzh.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/common/stats/WakeLockEvent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */