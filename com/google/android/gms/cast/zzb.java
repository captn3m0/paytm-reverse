package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import java.util.ArrayList;

public class zzb
  implements Parcelable.Creator<CastDevice>
{
  static void a(CastDevice paramCastDevice, Parcel paramParcel, int paramInt)
  {
    paramInt = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramCastDevice.a());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramCastDevice.c(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 3, paramCastDevice.a, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 4, paramCastDevice.d(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 5, paramCastDevice.e(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 6, paramCastDevice.f(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 7, paramCastDevice.h());
    com.google.android.gms.common.internal.safeparcel.zzb.c(paramParcel, 8, paramCastDevice.i(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 9, paramCastDevice.j());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 10, paramCastDevice.k());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 11, paramCastDevice.g(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, paramInt);
  }
  
  public CastDevice a(Parcel paramParcel)
  {
    int j = 0;
    String str1 = null;
    int n = zza.b(paramParcel);
    int i = -1;
    ArrayList localArrayList = null;
    int k = 0;
    String str2 = null;
    String str3 = null;
    String str4 = null;
    String str5 = null;
    String str6 = null;
    int m = 0;
    while (paramParcel.dataPosition() < n)
    {
      int i1 = zza.a(paramParcel);
      switch (zza.a(i1))
      {
      default: 
        zza.b(paramParcel, i1);
        break;
      case 1: 
        m = zza.g(paramParcel, i1);
        break;
      case 2: 
        str6 = zza.p(paramParcel, i1);
        break;
      case 3: 
        str5 = zza.p(paramParcel, i1);
        break;
      case 4: 
        str4 = zza.p(paramParcel, i1);
        break;
      case 5: 
        str3 = zza.p(paramParcel, i1);
        break;
      case 6: 
        str2 = zza.p(paramParcel, i1);
        break;
      case 7: 
        k = zza.g(paramParcel, i1);
        break;
      case 8: 
        localArrayList = zza.c(paramParcel, i1, WebImage.CREATOR);
        break;
      case 9: 
        j = zza.g(paramParcel, i1);
        break;
      case 10: 
        i = zza.g(paramParcel, i1);
        break;
      case 11: 
        str1 = zza.p(paramParcel, i1);
      }
    }
    if (paramParcel.dataPosition() != n) {
      throw new zza.zza("Overread allowed size end=" + n, paramParcel);
    }
    return new CastDevice(m, str6, str5, str4, str3, str2, k, localArrayList, j, i, str1);
  }
  
  public CastDevice[] a(int paramInt)
  {
    return new CastDevice[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */