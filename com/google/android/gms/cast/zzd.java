package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzd
  implements Parcelable.Creator<LaunchOptions>
{
  static void a(LaunchOptions paramLaunchOptions, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramLaunchOptions.a());
    zzb.a(paramParcel, 2, paramLaunchOptions.b());
    zzb.a(paramParcel, 3, paramLaunchOptions.c(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public LaunchOptions a(Parcel paramParcel)
  {
    boolean bool = false;
    int j = zza.b(paramParcel);
    String str = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        bool = zza.c(paramParcel, k);
        break;
      case 3: 
        str = zza.p(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new LaunchOptions(i, bool, str);
  }
  
  public LaunchOptions[] a(int paramInt)
  {
    return new LaunchOptions[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */