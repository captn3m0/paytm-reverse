package com.google.android.gms.cast;

import android.annotation.TargetApi;
import android.app.Presentation;

@TargetApi(19)
public abstract class CastPresentation
  extends Presentation
{}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/CastPresentation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */