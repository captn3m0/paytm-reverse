package com.google.android.gms.cast;

import android.annotation.SuppressLint;
import com.google.android.gms.cast.internal.zzb;
import com.google.android.gms.cast.internal.zze;
import com.google.android.gms.cast.internal.zzm;
import com.google.android.gms.cast.internal.zzn;
import com.google.android.gms.cast.internal.zzo;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import java.io.IOException;
import java.util.Locale;
import org.json.JSONObject;

@SuppressLint({"MissingRemoteException"})
public class RemoteMediaPlayer
  implements Cast.MessageReceivedCallback
{
  private final Object a = new Object();
  private final zzm b = new zzm(null)
  {
    protected void a()
    {
      RemoteMediaPlayer.a(RemoteMediaPlayer.this);
    }
    
    protected void b()
    {
      RemoteMediaPlayer.b(RemoteMediaPlayer.this);
    }
    
    protected void c()
    {
      RemoteMediaPlayer.c(RemoteMediaPlayer.this);
    }
    
    protected void d()
    {
      RemoteMediaPlayer.d(RemoteMediaPlayer.this);
    }
  };
  private final zza c = new zza();
  private OnPreloadStatusUpdatedListener d;
  private OnQueueStatusUpdatedListener e;
  private OnMetadataUpdatedListener f;
  private OnStatusUpdatedListener g;
  
  public RemoteMediaPlayer()
  {
    this.b.a(this.c);
  }
  
  private int a(int paramInt)
  {
    MediaStatus localMediaStatus = a();
    int i = 0;
    while (i < localMediaStatus.b())
    {
      if (localMediaStatus.a(i).a() == paramInt) {
        return i;
      }
      i += 1;
    }
    return -1;
  }
  
  private void b()
  {
    if (this.g != null) {
      this.g.a();
    }
  }
  
  private void c()
  {
    if (this.f != null) {
      this.f.a();
    }
  }
  
  private void d()
  {
    if (this.e != null) {
      this.e.a();
    }
  }
  
  private void e()
  {
    if (this.d != null) {
      this.d.a();
    }
  }
  
  public MediaStatus a()
  {
    synchronized (this.a)
    {
      MediaStatus localMediaStatus = this.b.h();
      return localMediaStatus;
    }
  }
  
  public void a(CastDevice paramCastDevice, String paramString1, String paramString2)
  {
    this.b.b(paramString2);
  }
  
  public static abstract interface MediaChannelResult
    extends Result
  {}
  
  public static abstract interface OnMetadataUpdatedListener
  {
    public abstract void a();
  }
  
  public static abstract interface OnPreloadStatusUpdatedListener
  {
    public abstract void a();
  }
  
  public static abstract interface OnQueueStatusUpdatedListener
  {
    public abstract void a();
  }
  
  public static abstract interface OnStatusUpdatedListener
  {
    public abstract void a();
  }
  
  private class zza
    implements zzn
  {
    private GoogleApiClient b;
    private long c = 0L;
    
    public zza() {}
    
    public long a()
    {
      long l = this.c + 1L;
      this.c = l;
      return l;
    }
    
    public void a(GoogleApiClient paramGoogleApiClient)
    {
      this.b = paramGoogleApiClient;
    }
    
    public void a(String paramString1, String paramString2, long paramLong, String paramString3)
      throws IOException
    {
      if (this.b == null) {
        throw new IOException("No GoogleApiClient available");
      }
      Cast.b.a(this.b, paramString1, paramString2).setResultCallback(new zza(paramLong));
    }
    
    private final class zza
      implements ResultCallback<Status>
    {
      private final long b;
      
      zza(long paramLong)
      {
        this.b = paramLong;
      }
      
      public void a(Status paramStatus)
      {
        if (!paramStatus.d()) {
          RemoteMediaPlayer.g(RemoteMediaPlayer.this).a(this.b, paramStatus.f());
        }
      }
    }
  }
  
  private static abstract class zzb
    extends zzb<RemoteMediaPlayer.MediaChannelResult>
  {
    zzo h;
    
    public RemoteMediaPlayer.MediaChannelResult a(final Status paramStatus)
    {
      new RemoteMediaPlayer.MediaChannelResult()
      {
        public Status getStatus()
        {
          return paramStatus;
        }
      };
    }
  }
  
  private static final class zzc
    implements RemoteMediaPlayer.MediaChannelResult
  {
    private final Status a;
    private final JSONObject b;
    
    zzc(Status paramStatus, JSONObject paramJSONObject)
    {
      this.a = paramStatus;
      this.b = paramJSONObject;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/RemoteMediaPlayer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */