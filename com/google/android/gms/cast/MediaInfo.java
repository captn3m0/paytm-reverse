package com.google.android.gms.cast;

import com.google.android.gms.cast.internal.zzf;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.internal.zznb;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class MediaInfo
{
  private final String a;
  private int b;
  private String c;
  private MediaMetadata d;
  private long e;
  private List<MediaTrack> f;
  private TextTrackStyle g;
  private JSONObject h;
  
  MediaInfo(JSONObject paramJSONObject)
    throws JSONException
  {
    this.a = paramJSONObject.getString("contentId");
    Object localObject1 = paramJSONObject.getString("streamType");
    if ("NONE".equals(localObject1)) {
      this.b = 0;
    }
    Object localObject2;
    for (;;)
    {
      this.c = paramJSONObject.getString("contentType");
      if (paramJSONObject.has("metadata"))
      {
        localObject1 = paramJSONObject.getJSONObject("metadata");
        this.d = new MediaMetadata(((JSONObject)localObject1).getInt("metadataType"));
        this.d.a((JSONObject)localObject1);
      }
      this.e = -1L;
      if ((paramJSONObject.has("duration")) && (!paramJSONObject.isNull("duration")))
      {
        double d1 = paramJSONObject.optDouble("duration", 0.0D);
        if ((!Double.isNaN(d1)) && (!Double.isInfinite(d1))) {
          this.e = zzf.a(d1);
        }
      }
      if (!paramJSONObject.has("tracks")) {
        break;
      }
      this.f = new ArrayList();
      localObject1 = paramJSONObject.getJSONArray("tracks");
      while (i < ((JSONArray)localObject1).length())
      {
        localObject2 = new MediaTrack(((JSONArray)localObject1).getJSONObject(i));
        this.f.add(localObject2);
        i += 1;
      }
      if ("BUFFERED".equals(localObject1)) {
        this.b = 1;
      } else if ("LIVE".equals(localObject1)) {
        this.b = 2;
      } else {
        this.b = -1;
      }
    }
    this.f = null;
    if (paramJSONObject.has("textTrackStyle"))
    {
      localObject1 = paramJSONObject.getJSONObject("textTrackStyle");
      localObject2 = new TextTrackStyle();
      ((TextTrackStyle)localObject2).a((JSONObject)localObject1);
    }
    for (this.g = ((TextTrackStyle)localObject2);; this.g = null)
    {
      this.h = paramJSONObject.optJSONObject("customData");
      return;
    }
  }
  
  public JSONObject a()
  {
    JSONObject localJSONObject = new JSONObject();
    for (;;)
    {
      try
      {
        localJSONObject.put("contentId", this.a);
        switch (this.b)
        {
        default: 
          localJSONObject.put("streamType", localObject);
          if (this.c != null) {
            localJSONObject.put("contentType", this.c);
          }
          if (this.d != null) {
            localJSONObject.put("metadata", this.d.b());
          }
          if (this.e <= -1L)
          {
            localJSONObject.put("duration", JSONObject.NULL);
            if (this.f == null) {
              continue;
            }
            localObject = new JSONArray();
            Iterator localIterator = this.f.iterator();
            if (localIterator.hasNext())
            {
              ((JSONArray)localObject).put(((MediaTrack)localIterator.next()).a());
              continue;
            }
          }
          else
          {
            localJSONObject.put("duration", zzf.a(this.e));
            continue;
          }
          localJSONObject.put("tracks", localObject);
          if (this.g != null) {
            localJSONObject.put("textTrackStyle", this.g.a());
          }
          if (this.h == null) {
            break label239;
          }
          localJSONObject.put("customData", this.h);
          return localJSONObject;
        }
      }
      catch (JSONException localJSONException) {}
      Object localObject = "NONE";
      continue;
      label239:
      return localJSONObject;
      String str = "BUFFERED";
      continue;
      str = "LIVE";
    }
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool3 = false;
    if (this == paramObject) {
      bool1 = true;
    }
    int i;
    int j;
    label51:
    do
    {
      do
      {
        do
        {
          return bool1;
          bool1 = bool3;
        } while (!(paramObject instanceof MediaInfo));
        paramObject = (MediaInfo)paramObject;
        if (this.h != null) {
          break;
        }
        i = 1;
        if (((MediaInfo)paramObject).h != null) {
          break label169;
        }
        j = 1;
        bool1 = bool3;
      } while (i != j);
      if ((this.h == null) || (((MediaInfo)paramObject).h == null)) {
        break;
      }
      bool1 = bool3;
    } while (!zznb.a(this.h, ((MediaInfo)paramObject).h));
    if ((zzf.a(this.a, ((MediaInfo)paramObject).a)) && (this.b == ((MediaInfo)paramObject).b) && (zzf.a(this.c, ((MediaInfo)paramObject).c)) && (zzf.a(this.d, ((MediaInfo)paramObject).d)) && (this.e == ((MediaInfo)paramObject).e)) {}
    for (boolean bool1 = bool2;; bool1 = false)
    {
      return bool1;
      i = 0;
      break;
      label169:
      j = 0;
      break label51;
    }
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.a, Integer.valueOf(this.b), this.c, this.d, Long.valueOf(this.e), String.valueOf(this.h) });
  }
  
  public static class Builder {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/MediaInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */