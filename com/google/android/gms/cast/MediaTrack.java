package com.google.android.gms.cast;

import android.text.TextUtils;
import com.google.android.gms.cast.internal.zzf;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.internal.zznb;
import org.json.JSONException;
import org.json.JSONObject;

public final class MediaTrack
{
  private long a;
  private int b;
  private String c;
  private String d;
  private String e;
  private String f;
  private int g;
  private JSONObject h;
  
  MediaTrack(JSONObject paramJSONObject)
    throws JSONException
  {
    a(paramJSONObject);
  }
  
  private void a(JSONObject paramJSONObject)
    throws JSONException
  {
    b();
    this.a = paramJSONObject.getLong("trackId");
    String str = paramJSONObject.getString("type");
    if ("TEXT".equals(str))
    {
      this.b = 1;
      this.c = paramJSONObject.optString("trackContentId", null);
      this.d = paramJSONObject.optString("trackContentType", null);
      this.e = paramJSONObject.optString("name", null);
      this.f = paramJSONObject.optString("language", null);
      if (!paramJSONObject.has("subtype")) {
        break label276;
      }
      str = paramJSONObject.getString("subtype");
      if (!"SUBTITLES".equals(str)) {
        break label181;
      }
      this.g = 1;
    }
    for (;;)
    {
      this.h = paramJSONObject.optJSONObject("customData");
      return;
      if ("AUDIO".equals(str))
      {
        this.b = 2;
        break;
      }
      if ("VIDEO".equals(str))
      {
        this.b = 3;
        break;
      }
      throw new JSONException("invalid type: " + str);
      label181:
      if ("CAPTIONS".equals(str))
      {
        this.g = 2;
      }
      else if ("DESCRIPTIONS".equals(str))
      {
        this.g = 3;
      }
      else if ("CHAPTERS".equals(str))
      {
        this.g = 4;
      }
      else if ("METADATA".equals(str))
      {
        this.g = 5;
      }
      else
      {
        throw new JSONException("invalid subtype: " + str);
        label276:
        this.g = 0;
      }
    }
  }
  
  private void b()
  {
    this.a = 0L;
    this.b = 0;
    this.c = null;
    this.e = null;
    this.f = null;
    this.g = -1;
    this.h = null;
  }
  
  public JSONObject a()
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("trackId", this.a);
      switch (this.b)
      {
      case 1: 
        if (this.c != null) {
          localJSONObject.put("trackContentId", this.c);
        }
        if (this.d != null) {
          localJSONObject.put("trackContentType", this.d);
        }
        if (this.e != null) {
          localJSONObject.put("name", this.e);
        }
        if (!TextUtils.isEmpty(this.f)) {
          localJSONObject.put("language", this.f);
        }
        switch (this.g)
        {
        }
        break;
      }
      for (;;)
      {
        if (this.h == null) {
          break label282;
        }
        localJSONObject.put("customData", this.h);
        return localJSONObject;
        localJSONObject.put("type", "TEXT");
        break;
        localJSONObject.put("type", "AUDIO");
        break;
        localJSONObject.put("type", "VIDEO");
        break;
        localJSONObject.put("subtype", "SUBTITLES");
        continue;
        localJSONObject.put("subtype", "CAPTIONS");
        continue;
        localJSONObject.put("subtype", "DESCRIPTIONS");
        continue;
        localJSONObject.put("subtype", "CHAPTERS");
        continue;
        localJSONObject.put("subtype", "METADATA");
        continue;
        break;
      }
      label282:
      return localJSONObject;
    }
    catch (JSONException localJSONException) {}
    return localJSONObject;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool3 = false;
    if (this == paramObject) {
      bool1 = true;
    }
    int i;
    int j;
    label51:
    do
    {
      do
      {
        do
        {
          return bool1;
          bool1 = bool3;
        } while (!(paramObject instanceof MediaTrack));
        paramObject = (MediaTrack)paramObject;
        if (this.h != null) {
          break;
        }
        i = 1;
        if (((MediaTrack)paramObject).h != null) {
          break label194;
        }
        j = 1;
        bool1 = bool3;
      } while (i != j);
      if ((this.h == null) || (((MediaTrack)paramObject).h == null)) {
        break;
      }
      bool1 = bool3;
    } while (!zznb.a(this.h, ((MediaTrack)paramObject).h));
    if ((this.a == ((MediaTrack)paramObject).a) && (this.b == ((MediaTrack)paramObject).b) && (zzf.a(this.c, ((MediaTrack)paramObject).c)) && (zzf.a(this.d, ((MediaTrack)paramObject).d)) && (zzf.a(this.e, ((MediaTrack)paramObject).e)) && (zzf.a(this.f, ((MediaTrack)paramObject).f)) && (this.g == ((MediaTrack)paramObject).g)) {}
    for (boolean bool1 = bool2;; bool1 = false)
    {
      return bool1;
      i = 0;
      break;
      label194:
      j = 0;
      break label51;
    }
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Long.valueOf(this.a), Integer.valueOf(this.b), this.c, this.d, this.e, this.f, Integer.valueOf(this.g), this.h });
  }
  
  public static class Builder {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/MediaTrack.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */