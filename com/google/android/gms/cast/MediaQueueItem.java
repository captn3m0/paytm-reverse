package com.google.android.gms.cast;

import com.google.android.gms.cast.internal.zzf;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.internal.zznb;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MediaQueueItem
{
  private MediaInfo a;
  private int b = 0;
  private boolean c = true;
  private double d;
  private double e = Double.POSITIVE_INFINITY;
  private double f;
  private long[] g;
  private JSONObject h;
  
  private MediaQueueItem(MediaInfo paramMediaInfo)
    throws IllegalArgumentException
  {
    if (paramMediaInfo == null) {
      throw new IllegalArgumentException("media cannot be null.");
    }
    this.a = paramMediaInfo;
  }
  
  MediaQueueItem(JSONObject paramJSONObject)
    throws JSONException
  {
    a(paramJSONObject);
  }
  
  public int a()
  {
    return this.b;
  }
  
  public boolean a(JSONObject paramJSONObject)
    throws JSONException
  {
    if (paramJSONObject.has("media")) {
      this.a = new MediaInfo(paramJSONObject.getJSONObject("media"));
    }
    for (boolean bool2 = true;; bool2 = false)
    {
      boolean bool1 = bool2;
      int i;
      if (paramJSONObject.has("itemId"))
      {
        i = paramJSONObject.getInt("itemId");
        bool1 = bool2;
        if (this.b != i)
        {
          this.b = i;
          bool1 = true;
        }
      }
      bool2 = bool1;
      if (paramJSONObject.has("autoplay"))
      {
        boolean bool3 = paramJSONObject.getBoolean("autoplay");
        bool2 = bool1;
        if (this.c != bool3)
        {
          this.c = bool3;
          bool2 = true;
        }
      }
      bool1 = bool2;
      double d1;
      if (paramJSONObject.has("startTime"))
      {
        d1 = paramJSONObject.getDouble("startTime");
        bool1 = bool2;
        if (Math.abs(d1 - this.d) > 1.0E-7D)
        {
          this.d = d1;
          bool1 = true;
        }
      }
      bool2 = bool1;
      if (paramJSONObject.has("playbackDuration"))
      {
        d1 = paramJSONObject.getDouble("playbackDuration");
        bool2 = bool1;
        if (Math.abs(d1 - this.e) > 1.0E-7D)
        {
          this.e = d1;
          bool2 = true;
        }
      }
      bool1 = bool2;
      if (paramJSONObject.has("preloadTime"))
      {
        d1 = paramJSONObject.getDouble("preloadTime");
        bool1 = bool2;
        if (Math.abs(d1 - this.f) > 1.0E-7D)
        {
          this.f = d1;
          bool1 = true;
        }
      }
      int j;
      long[] arrayOfLong;
      if (paramJSONObject.has("activeTrackIds"))
      {
        JSONArray localJSONArray = paramJSONObject.getJSONArray("activeTrackIds");
        j = localJSONArray.length();
        arrayOfLong = new long[j];
        i = 0;
        while (i < j)
        {
          arrayOfLong[i] = localJSONArray.getLong(i);
          i += 1;
        }
        if (this.g == null) {
          i = 1;
        }
      }
      for (;;)
      {
        if (i != 0)
        {
          this.g = arrayOfLong;
          bool1 = true;
        }
        if (paramJSONObject.has("customData"))
        {
          this.h = paramJSONObject.getJSONObject("customData");
          return true;
          if (this.g.length != j)
          {
            i = 1;
          }
          else
          {
            i = 0;
            for (;;)
            {
              if (i >= j) {
                break label425;
              }
              if (this.g[i] != arrayOfLong[i])
              {
                i = 1;
                break;
              }
              i += 1;
            }
          }
        }
        else
        {
          return bool1;
          label425:
          i = 0;
          continue;
          i = 0;
          arrayOfLong = null;
        }
      }
    }
  }
  
  void b()
    throws IllegalArgumentException
  {
    if (this.a == null) {
      throw new IllegalArgumentException("media cannot be null.");
    }
    if ((Double.isNaN(this.d)) || (this.d < 0.0D)) {
      throw new IllegalArgumentException("startTime cannot be negative or NaN.");
    }
    if (Double.isNaN(this.e)) {
      throw new IllegalArgumentException("playbackDuration cannot be NaN.");
    }
    if ((Double.isNaN(this.f)) || (this.f < 0.0D)) {
      throw new IllegalArgumentException("preloadTime cannot be negative or Nan.");
    }
  }
  
  public JSONObject c()
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("media", this.a.a());
      if (this.b != 0) {
        localJSONObject.put("itemId", this.b);
      }
      localJSONObject.put("autoplay", this.c);
      localJSONObject.put("startTime", this.d);
      if (this.e != Double.POSITIVE_INFINITY) {
        localJSONObject.put("playbackDuration", this.e);
      }
      localJSONObject.put("preloadTime", this.f);
      if (this.g != null)
      {
        JSONArray localJSONArray = new JSONArray();
        long[] arrayOfLong = this.g;
        int j = arrayOfLong.length;
        int i = 0;
        while (i < j)
        {
          localJSONArray.put(arrayOfLong[i]);
          i += 1;
        }
        localJSONObject.put("activeTrackIds", localJSONArray);
      }
      if (this.h != null) {
        localJSONObject.put("customData", this.h);
      }
      return localJSONObject;
    }
    catch (JSONException localJSONException) {}
    return localJSONObject;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool3 = false;
    if (this == paramObject) {
      bool1 = true;
    }
    int i;
    int j;
    label51:
    do
    {
      do
      {
        do
        {
          return bool1;
          bool1 = bool3;
        } while (!(paramObject instanceof MediaQueueItem));
        paramObject = (MediaQueueItem)paramObject;
        if (this.h != null) {
          break;
        }
        i = 1;
        if (((MediaQueueItem)paramObject).h != null) {
          break label190;
        }
        j = 1;
        bool1 = bool3;
      } while (i != j);
      if ((this.h == null) || (((MediaQueueItem)paramObject).h == null)) {
        break;
      }
      bool1 = bool3;
    } while (!zznb.a(this.h, ((MediaQueueItem)paramObject).h));
    if ((zzf.a(this.a, ((MediaQueueItem)paramObject).a)) && (this.b == ((MediaQueueItem)paramObject).b) && (this.c == ((MediaQueueItem)paramObject).c) && (this.d == ((MediaQueueItem)paramObject).d) && (this.e == ((MediaQueueItem)paramObject).e) && (this.f == ((MediaQueueItem)paramObject).f) && (zzf.a(this.g, ((MediaQueueItem)paramObject).g))) {}
    for (boolean bool1 = bool2;; bool1 = false)
    {
      return bool1;
      i = 0;
      break;
      label190:
      j = 0;
      break label51;
    }
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.a, Integer.valueOf(this.b), Boolean.valueOf(this.c), Double.valueOf(this.d), Double.valueOf(this.e), Double.valueOf(this.f), this.g, String.valueOf(this.h) });
  }
  
  public static class Builder
  {
    private final MediaQueueItem a;
    
    public Builder(MediaInfo paramMediaInfo)
      throws IllegalArgumentException
    {
      this.a = new MediaQueueItem(paramMediaInfo, null);
    }
    
    public MediaQueueItem a()
    {
      this.a.b();
      return this.a;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/MediaQueueItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */