package com.google.android.gms.cast;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v7.media.MediaRouteSelector.Builder;
import android.support.v7.media.MediaRouter;
import android.support.v7.media.MediaRouter.Callback;
import android.support.v7.media.MediaRouter.RouteInfo;
import android.text.TextUtils;
import android.view.Display;
import com.google.android.gms.R.drawable;
import com.google.android.gms.R.id;
import com.google.android.gms.R.string;
import com.google.android.gms.cast.internal.zzl;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzx;
import java.util.concurrent.atomic.AtomicBoolean;

@TargetApi(19)
public abstract class CastRemoteDisplayLocalService
  extends Service
{
  private static final zzl a = new zzl("CastRemoteDisplayLocalService");
  private static final int b = R.id.cast_notification_id;
  private static final Object c = new Object();
  private static AtomicBoolean d = new AtomicBoolean(false);
  private static CastRemoteDisplayLocalService v;
  private GoogleApiClient e;
  private CastRemoteDisplay.CastRemoteDisplaySessionCallbacks f;
  private String g;
  private Callbacks h;
  private zzb i;
  private NotificationSettings j;
  private Notification k;
  private boolean l;
  private PendingIntent m;
  private CastDevice n;
  private Display o;
  private Context p;
  private ServiceConnection q;
  private Handler r;
  private MediaRouter s;
  private boolean t = false;
  private final MediaRouter.Callback u = new MediaRouter.Callback()
  {
    public void e(MediaRouter paramAnonymousMediaRouter, MediaRouter.RouteInfo paramAnonymousRouteInfo)
    {
      CastRemoteDisplayLocalService.a(CastRemoteDisplayLocalService.this, "onRouteUnselected");
      if (CastRemoteDisplayLocalService.a(CastRemoteDisplayLocalService.this) == null)
      {
        CastRemoteDisplayLocalService.a(CastRemoteDisplayLocalService.this, "onRouteUnselected, no device was selected");
        return;
      }
      if (!CastDevice.b(paramAnonymousRouteInfo.m()).b().equals(CastRemoteDisplayLocalService.a(CastRemoteDisplayLocalService.this).b()))
      {
        CastRemoteDisplayLocalService.a(CastRemoteDisplayLocalService.this, "onRouteUnselected, device does not match");
        return;
      }
      CastRemoteDisplayLocalService.b();
    }
  };
  private final IBinder w = new zza(null);
  
  private GoogleApiClient a(CastDevice paramCastDevice)
  {
    paramCastDevice = new CastRemoteDisplay.CastRemoteDisplayOptions.Builder(paramCastDevice, this.f);
    new GoogleApiClient.Builder(this, new GoogleApiClient.ConnectionCallbacks()new GoogleApiClient.OnConnectionFailedListener
    {
      public void onConnected(Bundle paramAnonymousBundle)
      {
        CastRemoteDisplayLocalService.a(CastRemoteDisplayLocalService.this, "onConnected");
        CastRemoteDisplayLocalService.f(CastRemoteDisplayLocalService.this);
      }
      
      public void onConnectionSuspended(int paramAnonymousInt)
      {
        CastRemoteDisplayLocalService.c().d(String.format("[Instance: %s] ConnectionSuspended %d", new Object[] { this, Integer.valueOf(paramAnonymousInt) }), new Object[0]);
      }
    }, new GoogleApiClient.OnConnectionFailedListener()
    {
      public void onConnectionFailed(ConnectionResult paramAnonymousConnectionResult)
      {
        CastRemoteDisplayLocalService.b(CastRemoteDisplayLocalService.this, "Connection failed: " + paramAnonymousConnectionResult);
        CastRemoteDisplayLocalService.c(CastRemoteDisplayLocalService.this);
      }
    }).a(CastRemoteDisplay.a, paramCastDevice.a()).b();
  }
  
  private void a(NotificationSettings paramNotificationSettings)
  {
    zzx.b("updateNotificationSettingsInternal must be called on the main thread");
    if (this.j == null) {
      throw new IllegalStateException("No current notification settings to update");
    }
    if (this.l)
    {
      if (NotificationSettings.a(paramNotificationSettings) != null) {
        throw new IllegalStateException("Current mode is default notification, notification attribute must not be provided");
      }
      if (NotificationSettings.d(paramNotificationSettings) != null) {
        NotificationSettings.a(this.j, NotificationSettings.d(paramNotificationSettings));
      }
      if (!TextUtils.isEmpty(NotificationSettings.b(paramNotificationSettings))) {
        NotificationSettings.a(this.j, NotificationSettings.b(paramNotificationSettings));
      }
      if (!TextUtils.isEmpty(NotificationSettings.c(paramNotificationSettings))) {
        NotificationSettings.b(this.j, NotificationSettings.c(paramNotificationSettings));
      }
      this.k = e(true);
    }
    for (;;)
    {
      startForeground(b, this.k);
      return;
      zzx.a(NotificationSettings.a(paramNotificationSettings), "notification is required.");
      this.k = NotificationSettings.a(paramNotificationSettings);
      NotificationSettings.a(this.j, this.k);
    }
  }
  
  private void a(String paramString)
  {
    a.b("[Instance: %s] %s", new Object[] { this, paramString });
  }
  
  private boolean a(String paramString, CastDevice paramCastDevice, NotificationSettings paramNotificationSettings, Context paramContext, ServiceConnection paramServiceConnection, Callbacks paramCallbacks)
  {
    a("startRemoteDisplaySession");
    zzx.b("Starting the Cast Remote Display must be done on the main thread");
    for (;;)
    {
      synchronized (c)
      {
        if (v != null)
        {
          a.d("An existing service had not been stopped before starting one", new Object[0]);
          return false;
        }
        v = this;
        this.h = paramCallbacks;
        this.g = paramString;
        this.n = paramCastDevice;
        this.p = paramContext;
        this.q = paramServiceConnection;
        this.s = MediaRouter.a(getApplicationContext());
        paramString = new MediaRouteSelector.Builder().a(CastMediaControlIntent.a(this.g)).a();
        a("addMediaRouterCallback");
        this.s.a(paramString, this.u, 4);
        this.f = new CastRemoteDisplay.CastRemoteDisplaySessionCallbacks()
        {
          public void a(Status paramAnonymousStatus)
          {
            CastRemoteDisplayLocalService.c().b(String.format("Cast screen has ended: %d", new Object[] { Integer.valueOf(paramAnonymousStatus.f()) }), new Object[0]);
            CastRemoteDisplayLocalService.a(false);
          }
        };
        this.k = NotificationSettings.a(paramNotificationSettings);
        this.i = new zzb(null);
        registerReceiver(this.i, new IntentFilter("com.google.android.gms.cast.remote_display.ACTION_NOTIFICATION_DISCONNECT"));
        this.j = new NotificationSettings(paramNotificationSettings, null);
        if (NotificationSettings.a(this.j) == null)
        {
          this.l = true;
          this.k = e(false);
          startForeground(b, this.k);
          this.e = a(paramCastDevice);
          this.e.e();
          if (this.h != null) {
            this.h.a(this);
          }
          return true;
        }
      }
      this.l = false;
      this.k = NotificationSettings.a(this.j);
    }
  }
  
  public static void b()
  {
    d(false);
  }
  
  private void b(Display paramDisplay)
  {
    this.o = paramDisplay;
    if (this.l)
    {
      this.k = e(true);
      startForeground(b, this.k);
    }
    if (this.h != null)
    {
      this.h.b(this);
      this.h = null;
    }
    a(this.o);
  }
  
  private void b(String paramString)
  {
    a.e("[Instance: %s] %s", new Object[] { this, paramString });
  }
  
  private void b(final boolean paramBoolean)
  {
    if (this.r != null)
    {
      if (Looper.myLooper() != Looper.getMainLooper()) {
        this.r.post(new Runnable()
        {
          public void run()
          {
            CastRemoteDisplayLocalService.a(CastRemoteDisplayLocalService.this, paramBoolean);
          }
        });
      }
    }
    else {
      return;
    }
    c(paramBoolean);
  }
  
  private void c(boolean paramBoolean)
  {
    a("Stopping Service");
    zzx.b("stopServiceInstanceInternal must be called on the main thread");
    if ((!paramBoolean) && (this.s != null))
    {
      a("Setting default route");
      this.s.a(this.s.b());
    }
    if (this.i != null)
    {
      a("Unregistering notification receiver");
      unregisterReceiver(this.i);
    }
    k();
    l();
    g();
    if (this.e != null)
    {
      this.e.g();
      this.e = null;
    }
    if ((this.p != null) && (this.q != null)) {}
    try
    {
      this.p.unbindService(this.q);
      this.q = null;
      this.p = null;
      this.g = null;
      this.e = null;
      this.k = null;
      this.o = null;
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      for (;;)
      {
        a("No need to unbind service, already unbound");
      }
    }
  }
  
  private static void d(boolean paramBoolean)
  {
    a.b("Stopping Service", new Object[0]);
    d.set(false);
    synchronized (c)
    {
      if (v == null)
      {
        a.e("Service is already being stopped", new Object[0]);
        return;
      }
      CastRemoteDisplayLocalService localCastRemoteDisplayLocalService = v;
      v = null;
      localCastRemoteDisplayLocalService.b(paramBoolean);
      return;
    }
  }
  
  private Notification e(boolean paramBoolean)
  {
    a("createDefaultNotification");
    int i3 = getApplicationInfo().labelRes;
    String str3 = NotificationSettings.b(this.j);
    String str2 = NotificationSettings.c(this.j);
    int i2;
    int i1;
    String str1;
    if (paramBoolean)
    {
      i2 = R.string.cast_notification_connected_message;
      i1 = R.drawable.cast_ic_notification_on;
      str1 = str3;
      if (TextUtils.isEmpty(str3)) {
        str1 = getString(i3);
      }
      if (!TextUtils.isEmpty(str2)) {
        break label163;
      }
      str2 = getString(i2, new Object[] { this.n.d() });
    }
    label163:
    for (;;)
    {
      return new NotificationCompat.Builder(this).setContentTitle(str1).setContentText(str2).setContentIntent(NotificationSettings.d(this.j)).setSmallIcon(i1).setOngoing(true).addAction(17301560, getString(R.string.cast_notification_disconnect), m()).build();
      i2 = R.string.cast_notification_connecting_message;
      i1 = R.drawable.cast_ic_notification_connecting;
      break;
    }
  }
  
  private void g()
  {
    if (this.s != null)
    {
      zzx.b("CastRemoteDisplayLocalService calls must be done on the main thread");
      a("removeMediaRouterCallback");
      this.s.a(this.u);
    }
  }
  
  private void h()
  {
    a("startRemoteDisplay");
    if ((this.e == null) || (!this.e.i()))
    {
      a.e("Unable to start the remote display as the API client is not ready", new Object[0]);
      return;
    }
    CastRemoteDisplay.b.a(this.e, this.g).setResultCallback(new ResultCallback()
    {
      public void a(CastRemoteDisplay.CastRemoteDisplaySessionResult paramAnonymousCastRemoteDisplaySessionResult)
      {
        if (!paramAnonymousCastRemoteDisplaySessionResult.getStatus().d())
        {
          CastRemoteDisplayLocalService.c().e("Connection was not successful", new Object[0]);
          CastRemoteDisplayLocalService.c(CastRemoteDisplayLocalService.this);
          return;
        }
        CastRemoteDisplayLocalService.c().b("startRemoteDisplay successful", new Object[0]);
        synchronized (CastRemoteDisplayLocalService.e())
        {
          if (CastRemoteDisplayLocalService.f() == null)
          {
            CastRemoteDisplayLocalService.c().b("Remote Display started but session already cancelled", new Object[0]);
            CastRemoteDisplayLocalService.c(CastRemoteDisplayLocalService.this);
            return;
          }
        }
        paramAnonymousCastRemoteDisplaySessionResult = paramAnonymousCastRemoteDisplaySessionResult.a();
        if (paramAnonymousCastRemoteDisplaySessionResult != null) {
          CastRemoteDisplayLocalService.a(CastRemoteDisplayLocalService.this, paramAnonymousCastRemoteDisplaySessionResult);
        }
        for (;;)
        {
          CastRemoteDisplayLocalService.d().set(false);
          if ((CastRemoteDisplayLocalService.d(CastRemoteDisplayLocalService.this) == null) || (CastRemoteDisplayLocalService.e(CastRemoteDisplayLocalService.this) == null)) {
            break;
          }
          try
          {
            CastRemoteDisplayLocalService.d(CastRemoteDisplayLocalService.this).unbindService(CastRemoteDisplayLocalService.e(CastRemoteDisplayLocalService.this));
            CastRemoteDisplayLocalService.a(CastRemoteDisplayLocalService.this, null);
            CastRemoteDisplayLocalService.a(CastRemoteDisplayLocalService.this, null);
            return;
            CastRemoteDisplayLocalService.c().e("Cast Remote Display session created without display", new Object[0]);
          }
          catch (IllegalArgumentException paramAnonymousCastRemoteDisplaySessionResult)
          {
            for (;;)
            {
              CastRemoteDisplayLocalService.c().b("No need to unbind service, already unbound", new Object[0]);
            }
          }
        }
      }
    });
  }
  
  private void i()
  {
    a("stopRemoteDisplay");
    if ((this.e == null) || (!this.e.i()))
    {
      a.e("Unable to stop the remote display as the API client is not ready", new Object[0]);
      return;
    }
    CastRemoteDisplay.b.a(this.e).setResultCallback(new ResultCallback()
    {
      public void a(CastRemoteDisplay.CastRemoteDisplaySessionResult paramAnonymousCastRemoteDisplaySessionResult)
      {
        if (!paramAnonymousCastRemoteDisplaySessionResult.getStatus().d()) {
          CastRemoteDisplayLocalService.a(CastRemoteDisplayLocalService.this, "Unable to stop the remote display, result unsuccessful");
        }
        for (;;)
        {
          CastRemoteDisplayLocalService.b(CastRemoteDisplayLocalService.this, null);
          return;
          CastRemoteDisplayLocalService.a(CastRemoteDisplayLocalService.this, "remote display stopped");
        }
      }
    });
  }
  
  private void j()
  {
    if (this.h != null)
    {
      this.h.a(new Status(2200));
      this.h = null;
    }
    b();
  }
  
  private void k()
  {
    a("stopRemoteDisplaySession");
    i();
    a();
  }
  
  private void l()
  {
    a("Stopping the remote display Service");
    stopForeground(true);
    stopSelf();
  }
  
  private PendingIntent m()
  {
    if (this.m == null) {
      this.m = PendingIntent.getBroadcast(this, 0, new Intent("com.google.android.gms.cast.remote_display.ACTION_NOTIFICATION_DISCONNECT"), 268435456);
    }
    return this.m;
  }
  
  public abstract void a();
  
  public abstract void a(Display paramDisplay);
  
  public static abstract interface Callbacks
  {
    public abstract void a(CastRemoteDisplayLocalService paramCastRemoteDisplayLocalService);
    
    public abstract void a(Status paramStatus);
    
    public abstract void b(CastRemoteDisplayLocalService paramCastRemoteDisplayLocalService);
  }
  
  public static final class NotificationSettings
  {
    private Notification a;
    private PendingIntent b;
    private String c;
    private String d;
    
    private NotificationSettings() {}
    
    private NotificationSettings(NotificationSettings paramNotificationSettings)
    {
      this.a = paramNotificationSettings.a;
      this.b = paramNotificationSettings.b;
      this.c = paramNotificationSettings.c;
      this.d = paramNotificationSettings.d;
    }
    
    public static final class Builder
    {
      private CastRemoteDisplayLocalService.NotificationSettings a = new CastRemoteDisplayLocalService.NotificationSettings(null);
    }
  }
  
  private class zza
    extends Binder
  {
    private zza() {}
    
    CastRemoteDisplayLocalService a()
    {
      return CastRemoteDisplayLocalService.this;
    }
  }
  
  private static final class zzb
    extends BroadcastReceiver
  {
    public void onReceive(Context paramContext, Intent paramIntent)
    {
      if (paramIntent.getAction().equals("com.google.android.gms.cast.remote_display.ACTION_NOTIFICATION_DISCONNECT"))
      {
        CastRemoteDisplayLocalService.c().b("disconnecting", new Object[0]);
        CastRemoteDisplayLocalService.b();
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/CastRemoteDisplayLocalService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */