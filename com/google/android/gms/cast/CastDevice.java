package com.google.android.gms.cast;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.cast.internal.zzf;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CastDevice
  implements SafeParcelable
{
  public static final Parcelable.Creator<CastDevice> CREATOR = new zzb();
  String a;
  private final int b;
  private String c;
  private Inet4Address d;
  private String e;
  private String f;
  private String g;
  private int h;
  private List<WebImage> i;
  private int j;
  private int k;
  private String l;
  
  private CastDevice()
  {
    this(4, null, null, null, null, null, -1, new ArrayList(), 0, -1, null);
  }
  
  CastDevice(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt2, List<WebImage> paramList, int paramInt3, int paramInt4, String paramString6)
  {
    this.b = paramInt1;
    this.c = a(paramString1);
    this.a = a(paramString2);
    if (!TextUtils.isEmpty(this.a)) {}
    try
    {
      paramString1 = InetAddress.getByName(this.a);
      if ((paramString1 instanceof Inet4Address)) {
        this.d = ((Inet4Address)paramString1);
      }
      this.e = a(paramString3);
      this.f = a(paramString4);
      this.g = a(paramString5);
      this.h = paramInt2;
      if (paramList != null)
      {
        this.i = paramList;
        this.j = paramInt3;
        this.k = paramInt4;
        this.l = a(paramString6);
        return;
      }
    }
    catch (UnknownHostException paramString1)
    {
      for (;;)
      {
        Log.i("CastDevice", "Unable to convert host address (" + this.a + ") to ipaddress: " + paramString1.getMessage());
        continue;
        paramList = new ArrayList();
      }
    }
  }
  
  private static String a(String paramString)
  {
    String str = paramString;
    if (paramString == null) {
      str = "";
    }
    return str;
  }
  
  public static CastDevice b(Bundle paramBundle)
  {
    if (paramBundle == null) {
      return null;
    }
    paramBundle.setClassLoader(CastDevice.class.getClassLoader());
    return (CastDevice)paramBundle.getParcelable("com.google.android.gms.cast.EXTRA_CAST_DEVICE");
  }
  
  int a()
  {
    return this.b;
  }
  
  public void a(Bundle paramBundle)
  {
    if (paramBundle == null) {
      return;
    }
    paramBundle.putParcelable("com.google.android.gms.cast.EXTRA_CAST_DEVICE", this);
  }
  
  public String b()
  {
    if (this.c.startsWith("__cast_nearby__")) {
      return this.c.substring("__cast_nearby__".length() + 1);
    }
    return this.c;
  }
  
  public String c()
  {
    return this.c;
  }
  
  public String d()
  {
    return this.e;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String e()
  {
    return this.f;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      do
      {
        return true;
        if (!(paramObject instanceof CastDevice)) {
          return false;
        }
        paramObject = (CastDevice)paramObject;
        if (this.c != null) {
          break;
        }
      } while (((CastDevice)paramObject).c == null);
      return false;
    } while ((zzf.a(this.c, ((CastDevice)paramObject).c)) && (zzf.a(this.d, ((CastDevice)paramObject).d)) && (zzf.a(this.f, ((CastDevice)paramObject).f)) && (zzf.a(this.e, ((CastDevice)paramObject).e)) && (zzf.a(this.g, ((CastDevice)paramObject).g)) && (this.h == ((CastDevice)paramObject).h) && (zzf.a(this.i, ((CastDevice)paramObject).i)) && (this.j == ((CastDevice)paramObject).j) && (this.k == ((CastDevice)paramObject).k) && (zzf.a(this.l, ((CastDevice)paramObject).l)));
    return false;
  }
  
  public String f()
  {
    return this.g;
  }
  
  public String g()
  {
    return this.l;
  }
  
  public int h()
  {
    return this.h;
  }
  
  public int hashCode()
  {
    if (this.c == null) {
      return 0;
    }
    return this.c.hashCode();
  }
  
  public List<WebImage> i()
  {
    return Collections.unmodifiableList(this.i);
  }
  
  public int j()
  {
    return this.j;
  }
  
  public int k()
  {
    return this.k;
  }
  
  public String toString()
  {
    return String.format("\"%s\" (%s)", new Object[] { this.e, this.c });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/CastDevice.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */