package com.google.android.gms.cast.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.Cast.ApplicationConnectionResult;
import com.google.android.gms.cast.Cast.Listener;
import com.google.android.gms.cast.Cast.MessageReceivedCallback;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.JoinOptions;
import com.google.android.gms.cast.LaunchOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zzb;
import com.google.android.gms.common.internal.BinderWrapper;
import com.google.android.gms.common.internal.zzj;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public final class zze
  extends zzj<zzi>
{
  private static final Object A = new Object();
  private static final zzl a = new zzl("CastClientImpl");
  private static final Object z = new Object();
  private ApplicationMetadata e;
  private final CastDevice f;
  private final Cast.Listener g;
  private final Map<String, Cast.MessageReceivedCallback> h;
  private final long i;
  private zzb j;
  private String k;
  private boolean l;
  private boolean m;
  private boolean n;
  private boolean o;
  private double p;
  private int q;
  private int r;
  private final AtomicLong s;
  private String t;
  private String u;
  private Bundle v;
  private final Map<Long, zza.zzb<Status>> w;
  private zza.zzb<Cast.ApplicationConnectionResult> x;
  private zza.zzb<Status> y;
  
  public zze(Context paramContext, Looper paramLooper, com.google.android.gms.common.internal.zzf paramzzf, CastDevice paramCastDevice, long paramLong, Cast.Listener paramListener, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramContext, paramLooper, 10, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    this.f = paramCastDevice;
    this.g = paramListener;
    this.i = paramLong;
    this.h = new HashMap();
    this.s = new AtomicLong(0L);
    this.w = new HashMap();
    x();
  }
  
  private void a(ApplicationStatus paramApplicationStatus)
  {
    paramApplicationStatus = paramApplicationStatus.b();
    if (!zzf.a(paramApplicationStatus, this.k)) {
      this.k = paramApplicationStatus;
    }
    for (boolean bool = true;; bool = false)
    {
      a.b("hasChanged=%b, mFirstApplicationStatusUpdate=%b", new Object[] { Boolean.valueOf(bool), Boolean.valueOf(this.m) });
      if ((this.g != null) && ((bool) || (this.m))) {
        this.g.a();
      }
      this.m = false;
      return;
    }
  }
  
  private void a(DeviceStatus paramDeviceStatus)
  {
    ApplicationMetadata localApplicationMetadata = paramDeviceStatus.f();
    if (!zzf.a(localApplicationMetadata, this.e))
    {
      this.e = localApplicationMetadata;
      this.g.a(this.e);
    }
    double d = paramDeviceStatus.b();
    if ((d != NaN.0D) && (Math.abs(d - this.p) > 1.0E-7D)) {
      this.p = d;
    }
    for (boolean bool1 = true;; bool1 = false)
    {
      boolean bool2 = paramDeviceStatus.c();
      if (bool2 != this.l)
      {
        this.l = bool2;
        bool1 = true;
      }
      a.b("hasVolumeChanged=%b, mFirstDeviceStatusUpdate=%b", new Object[] { Boolean.valueOf(bool1), Boolean.valueOf(this.n) });
      if ((this.g != null) && ((bool1) || (this.n))) {
        this.g.b();
      }
      int i1 = paramDeviceStatus.d();
      if (i1 != this.q) {
        this.q = i1;
      }
      for (bool1 = true;; bool1 = false)
      {
        a.b("hasActiveInputChanged=%b, mFirstDeviceStatusUpdate=%b", new Object[] { Boolean.valueOf(bool1), Boolean.valueOf(this.n) });
        if ((this.g != null) && ((bool1) || (this.n))) {
          this.g.b(this.q);
        }
        i1 = paramDeviceStatus.e();
        if (i1 != this.r) {
          this.r = i1;
        }
        for (bool1 = true;; bool1 = false)
        {
          a.b("hasStandbyStateChanged=%b, mFirstDeviceStatusUpdate=%b", new Object[] { Boolean.valueOf(bool1), Boolean.valueOf(this.n) });
          if ((this.g != null) && ((bool1) || (this.n))) {
            this.g.c(this.r);
          }
          this.n = false;
          return;
        }
      }
    }
  }
  
  private void b(zza.zzb<Cast.ApplicationConnectionResult> paramzzb)
  {
    synchronized (z)
    {
      if (this.x != null) {
        this.x.a(new zza(new Status(2002)));
      }
      this.x = paramzzb;
      return;
    }
  }
  
  private void c(zza.zzb<Status> paramzzb)
  {
    synchronized (A)
    {
      if (this.y != null)
      {
        paramzzb.a(new Status(2001));
        return;
      }
      this.y = paramzzb;
      return;
    }
  }
  
  private void x()
  {
    this.o = false;
    this.q = -1;
    this.r = -1;
    this.e = null;
    this.k = null;
    this.p = 0.0D;
    this.l = false;
  }
  
  private void y()
  {
    a.b("removing all MessageReceivedCallbacks", new Object[0]);
    synchronized (this.h)
    {
      this.h.clear();
      return;
    }
  }
  
  private void z()
    throws IllegalStateException
  {
    if ((!this.o) || (this.j == null) || (this.j.b())) {
      throw new IllegalStateException("Not connected to a device");
    }
  }
  
  protected zzi a(IBinder paramIBinder)
  {
    return zzi.zza.a(paramIBinder);
  }
  
  protected String a()
  {
    return "com.google.android.gms.cast.service.BIND_CAST_DEVICE_CONTROLLER_SERVICE";
  }
  
  protected void a(int paramInt1, IBinder paramIBinder, Bundle paramBundle, int paramInt2)
  {
    a.b("in onPostInitHandler; statusCode=%d", new Object[] { Integer.valueOf(paramInt1) });
    if ((paramInt1 == 0) || (paramInt1 == 1001))
    {
      this.o = true;
      this.m = true;
      this.n = true;
    }
    for (;;)
    {
      int i1 = paramInt1;
      if (paramInt1 == 1001)
      {
        this.v = new Bundle();
        this.v.putBoolean("com.google.android.gms.cast.EXTRA_APP_NO_LONGER_RUNNING", true);
        i1 = 0;
      }
      super.a(i1, paramIBinder, paramBundle, paramInt2);
      return;
      this.o = false;
    }
  }
  
  public void a(ConnectionResult paramConnectionResult)
  {
    super.a(paramConnectionResult);
    y();
  }
  
  public void a(zza.zzb<Status> paramzzb)
    throws IllegalStateException, RemoteException
  {
    c(paramzzb);
    ((zzi)v()).b();
  }
  
  public void a(String paramString)
    throws IllegalArgumentException, RemoteException
  {
    if (TextUtils.isEmpty(paramString)) {
      throw new IllegalArgumentException("Channel namespace cannot be null or empty");
    }
    synchronized (this.h)
    {
      Cast.MessageReceivedCallback localMessageReceivedCallback = (Cast.MessageReceivedCallback)this.h.remove(paramString);
      if (localMessageReceivedCallback == null) {}
    }
  }
  
  public void a(String paramString, Cast.MessageReceivedCallback paramMessageReceivedCallback)
    throws IllegalArgumentException, IllegalStateException, RemoteException
  {
    zzf.a(paramString);
    a(paramString);
    if (paramMessageReceivedCallback != null) {}
    synchronized (this.h)
    {
      this.h.put(paramString, paramMessageReceivedCallback);
      ((zzi)v()).b(paramString);
      return;
    }
  }
  
  public void a(String paramString, LaunchOptions paramLaunchOptions, zza.zzb<Cast.ApplicationConnectionResult> paramzzb)
    throws IllegalStateException, RemoteException
  {
    b(paramzzb);
    ((zzi)v()).a(paramString, paramLaunchOptions);
  }
  
  public void a(String paramString, zza.zzb<Status> paramzzb)
    throws IllegalStateException, RemoteException
  {
    c(paramzzb);
    ((zzi)v()).a(paramString);
  }
  
  public void a(String paramString1, String paramString2, JoinOptions paramJoinOptions, zza.zzb<Cast.ApplicationConnectionResult> paramzzb)
    throws IllegalStateException, RemoteException
  {
    b(paramzzb);
    paramzzb = paramJoinOptions;
    if (paramJoinOptions == null) {
      paramzzb = new JoinOptions();
    }
    ((zzi)v()).a(paramString1, paramString2, paramzzb);
  }
  
  public void a(String paramString1, String paramString2, zza.zzb<Status> paramzzb)
    throws IllegalArgumentException, IllegalStateException, RemoteException
  {
    if (TextUtils.isEmpty(paramString2)) {
      throw new IllegalArgumentException("The message payload cannot be null or empty");
    }
    if (paramString2.length() > 65536) {
      throw new IllegalArgumentException("Message exceeds maximum size");
    }
    zzf.a(paramString1);
    z();
    long l1 = this.s.incrementAndGet();
    try
    {
      this.w.put(Long.valueOf(l1), paramzzb);
      ((zzi)v()).a(paramString1, paramString2, l1);
      return;
    }
    catch (Throwable paramString1)
    {
      this.w.remove(Long.valueOf(l1));
      throw paramString1;
    }
  }
  
  public void a(String paramString, boolean paramBoolean, zza.zzb<Cast.ApplicationConnectionResult> paramzzb)
    throws IllegalStateException, RemoteException
  {
    LaunchOptions localLaunchOptions = new LaunchOptions();
    localLaunchOptions.a(paramBoolean);
    a(paramString, localLaunchOptions, paramzzb);
  }
  
  protected Bundle a_()
  {
    Bundle localBundle = new Bundle();
    a.b("getRemoteService(): mLastApplicationId=%s, mLastSessionId=%s", new Object[] { this.t, this.u });
    this.f.a(localBundle);
    localBundle.putLong("com.google.android.gms.cast.EXTRA_CAST_FLAGS", this.i);
    this.j = new zzb(this);
    localBundle.putParcelable("listener", new BinderWrapper(this.j.asBinder()));
    if (this.t != null)
    {
      localBundle.putString("last_application_id", this.t);
      if (this.u != null) {
        localBundle.putString("last_session_id", this.u);
      }
    }
    return localBundle;
  }
  
  protected String b()
  {
    return "com.google.android.gms.cast.internal.ICastDeviceController";
  }
  
  /* Error */
  public void f()
  {
    // Byte code:
    //   0: getstatic 74	com/google/android/gms/cast/internal/zze:a	Lcom/google/android/gms/cast/internal/zzl;
    //   3: ldc_w 438
    //   6: iconst_2
    //   7: anewarray 76	java/lang/Object
    //   10: dup
    //   11: iconst_0
    //   12: aload_0
    //   13: getfield 250	com/google/android/gms/cast/internal/zze:j	Lcom/google/android/gms/cast/internal/zze$zzb;
    //   16: aastore
    //   17: dup
    //   18: iconst_1
    //   19: aload_0
    //   20: invokevirtual 440	com/google/android/gms/cast/internal/zze:k	()Z
    //   23: invokestatic 140	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   26: aastore
    //   27: invokevirtual 145	com/google/android/gms/cast/internal/zzl:b	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   30: aload_0
    //   31: getfield 250	com/google/android/gms/cast/internal/zze:j	Lcom/google/android/gms/cast/internal/zze$zzb;
    //   34: astore_1
    //   35: aload_0
    //   36: aconst_null
    //   37: putfield 250	com/google/android/gms/cast/internal/zze:j	Lcom/google/android/gms/cast/internal/zze$zzb;
    //   40: aload_1
    //   41: ifnull +10 -> 51
    //   44: aload_1
    //   45: invokevirtual 443	com/google/android/gms/cast/internal/zze$zzb:a	()Lcom/google/android/gms/cast/internal/zze;
    //   48: ifnonnull +17 -> 65
    //   51: getstatic 74	com/google/android/gms/cast/internal/zze:a	Lcom/google/android/gms/cast/internal/zzl;
    //   54: ldc_w 445
    //   57: iconst_0
    //   58: anewarray 76	java/lang/Object
    //   61: invokevirtual 145	com/google/android/gms/cast/internal/zzl:b	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   64: return
    //   65: aload_0
    //   66: invokespecial 289	com/google/android/gms/cast/internal/zze:y	()V
    //   69: aload_0
    //   70: invokevirtual 296	com/google/android/gms/cast/internal/zze:v	()Landroid/os/IInterface;
    //   73: checkcast 298	com/google/android/gms/cast/internal/zzi
    //   76: invokeinterface 446 1 0
    //   81: aload_0
    //   82: invokespecial 448	com/google/android/gms/common/internal/zzj:f	()V
    //   85: return
    //   86: astore_1
    //   87: getstatic 74	com/google/android/gms/cast/internal/zze:a	Lcom/google/android/gms/cast/internal/zzl;
    //   90: aload_1
    //   91: ldc_w 450
    //   94: iconst_1
    //   95: anewarray 76	java/lang/Object
    //   98: dup
    //   99: iconst_0
    //   100: aload_1
    //   101: invokevirtual 453	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   104: aastore
    //   105: invokevirtual 326	com/google/android/gms/cast/internal/zzl:a	(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    //   108: aload_0
    //   109: invokespecial 448	com/google/android/gms/common/internal/zzj:f	()V
    //   112: return
    //   113: astore_1
    //   114: aload_0
    //   115: invokespecial 448	com/google/android/gms/common/internal/zzj:f	()V
    //   118: aload_1
    //   119: athrow
    //   120: astore_1
    //   121: goto -34 -> 87
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	124	0	this	zze
    //   34	11	1	localzzb	zzb
    //   86	15	1	localRemoteException	RemoteException
    //   113	6	1	localObject	Object
    //   120	1	1	localIllegalStateException	IllegalStateException
    // Exception table:
    //   from	to	target	type
    //   69	81	86	android/os/RemoteException
    //   69	81	113	finally
    //   87	108	113	finally
    //   69	81	120	java/lang/IllegalStateException
  }
  
  public Bundle m_()
  {
    if (this.v != null)
    {
      Bundle localBundle = this.v;
      this.v = null;
      return localBundle;
    }
    return super.m_();
  }
  
  private static final class zza
    implements Cast.ApplicationConnectionResult
  {
    private final Status a;
    private final ApplicationMetadata b;
    private final String c;
    private final String d;
    private final boolean e;
    
    public zza(Status paramStatus)
    {
      this(paramStatus, null, null, null, false);
    }
    
    public zza(Status paramStatus, ApplicationMetadata paramApplicationMetadata, String paramString1, String paramString2, boolean paramBoolean)
    {
      this.a = paramStatus;
      this.b = paramApplicationMetadata;
      this.c = paramString1;
      this.d = paramString2;
      this.e = paramBoolean;
    }
    
    public Status getStatus()
    {
      return this.a;
    }
  }
  
  private static class zzb
    extends zzj.zza
  {
    private final AtomicReference<zze> a;
    private final Handler b;
    
    public zzb(zze paramzze)
    {
      this.a = new AtomicReference(paramzze);
      this.b = new Handler(paramzze.r());
    }
    
    private void a(zze paramzze, long paramLong, int paramInt)
    {
      synchronized (zze.f(paramzze))
      {
        paramzze = (zza.zzb)zze.f(paramzze).remove(Long.valueOf(paramLong));
        if (paramzze != null) {
          paramzze.a(new Status(paramInt));
        }
        return;
      }
    }
    
    private boolean a(zze paramzze, int paramInt)
    {
      synchronized ()
      {
        if (zze.g(paramzze) != null)
        {
          zze.g(paramzze).a(new Status(paramInt));
          zze.b(paramzze, null);
          return true;
        }
        return false;
      }
    }
    
    public zze a()
    {
      zze localzze = (zze)this.a.getAndSet(null);
      if (localzze == null) {
        return null;
      }
      zze.a(localzze);
      return localzze;
    }
    
    public void a(int paramInt)
    {
      zze localzze = a();
      if (localzze == null) {}
      do
      {
        return;
        zze.h().b("ICastDeviceControllerListener.onDisconnected: %d", new Object[] { Integer.valueOf(paramInt) });
      } while (paramInt == 0);
      localzze.b(2);
    }
    
    public void a(ApplicationMetadata paramApplicationMetadata, String paramString1, String paramString2, boolean paramBoolean)
    {
      zze localzze = (zze)this.a.get();
      if (localzze == null) {
        return;
      }
      zze.a(localzze, paramApplicationMetadata);
      zze.a(localzze, paramApplicationMetadata.b());
      zze.b(localzze, paramString2);
      synchronized (zze.i())
      {
        if (zze.b(localzze) != null)
        {
          zze.b(localzze).a(new zze.zza(new Status(0), paramApplicationMetadata, paramString1, paramString2, paramBoolean));
          zze.a(localzze, null);
        }
        return;
      }
    }
    
    public void a(final ApplicationStatus paramApplicationStatus)
    {
      final zze localzze = (zze)this.a.get();
      if (localzze == null) {
        return;
      }
      zze.h().b("onApplicationStatusChanged", new Object[0]);
      this.b.post(new Runnable()
      {
        public void run()
        {
          zze.a(localzze, paramApplicationStatus);
        }
      });
    }
    
    public void a(final DeviceStatus paramDeviceStatus)
    {
      final zze localzze = (zze)this.a.get();
      if (localzze == null) {
        return;
      }
      zze.h().b("onDeviceStatusChanged", new Object[0]);
      this.b.post(new Runnable()
      {
        public void run()
        {
          zze.a(localzze, paramDeviceStatus);
        }
      });
    }
    
    public void a(String paramString, double paramDouble, boolean paramBoolean)
    {
      zze.h().b("Deprecated callback: \"onStatusreceived\"", new Object[0]);
    }
    
    public void a(String paramString, long paramLong)
    {
      paramString = (zze)this.a.get();
      if (paramString == null) {
        return;
      }
      a(paramString, paramLong, 0);
    }
    
    public void a(String paramString, long paramLong, int paramInt)
    {
      paramString = (zze)this.a.get();
      if (paramString == null) {
        return;
      }
      a(paramString, paramLong, paramInt);
    }
    
    public void a(final String paramString1, final String paramString2)
    {
      final zze localzze = (zze)this.a.get();
      if (localzze == null) {
        return;
      }
      zze.h().b("Receive (type=text, ns=%s) %s", new Object[] { paramString1, paramString2 });
      this.b.post(new Runnable()
      {
        public void run()
        {
          synchronized (zze.d(localzze))
          {
            Cast.MessageReceivedCallback localMessageReceivedCallback = (Cast.MessageReceivedCallback)zze.d(localzze).get(paramString1);
            if (localMessageReceivedCallback != null)
            {
              localMessageReceivedCallback.a(zze.e(localzze), paramString1, paramString2);
              return;
            }
          }
          zze.h().b("Discarded message for unknown namespace '%s'", new Object[] { paramString1 });
        }
      });
    }
    
    public void a(String paramString, byte[] paramArrayOfByte)
    {
      if ((zze)this.a.get() == null) {
        return;
      }
      zze.h().b("IGNORING: Receive (type=binary, ns=%s) <%d bytes>", new Object[] { paramString, Integer.valueOf(paramArrayOfByte.length) });
    }
    
    public void b(int paramInt)
    {
      zze localzze = (zze)this.a.get();
      if (localzze == null) {
        return;
      }
      synchronized (zze.i())
      {
        if (zze.b(localzze) != null)
        {
          zze.b(localzze).a(new zze.zza(new Status(paramInt)));
          zze.a(localzze, null);
        }
        return;
      }
    }
    
    public boolean b()
    {
      return this.a.get() == null;
    }
    
    public void c(int paramInt)
    {
      zze localzze = (zze)this.a.get();
      if (localzze == null) {
        return;
      }
      a(localzze, paramInt);
    }
    
    public void d(int paramInt)
    {
      zze localzze = (zze)this.a.get();
      if (localzze == null) {
        return;
      }
      a(localzze, paramInt);
    }
    
    public void e(final int paramInt)
    {
      final zze localzze = (zze)this.a.get();
      if (localzze == null) {}
      do
      {
        return;
        zze.a(localzze, null);
        zze.b(localzze, null);
        a(localzze, paramInt);
      } while (zze.c(localzze) == null);
      this.b.post(new Runnable()
      {
        public void run()
        {
          zze.c(localzze).a(paramInt);
        }
      });
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/internal/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */