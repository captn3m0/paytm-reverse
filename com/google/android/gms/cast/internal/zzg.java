package com.google.android.gms.cast.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzg
  implements Parcelable.Creator<DeviceStatus>
{
  static void a(DeviceStatus paramDeviceStatus, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramDeviceStatus.a());
    zzb.a(paramParcel, 2, paramDeviceStatus.b());
    zzb.a(paramParcel, 3, paramDeviceStatus.c());
    zzb.a(paramParcel, 4, paramDeviceStatus.d());
    zzb.a(paramParcel, 5, paramDeviceStatus.f(), paramInt, false);
    zzb.a(paramParcel, 6, paramDeviceStatus.e());
    zzb.a(paramParcel, i);
  }
  
  public DeviceStatus a(Parcel paramParcel)
  {
    int i = 0;
    int m = zza.b(paramParcel);
    double d = 0.0D;
    ApplicationMetadata localApplicationMetadata = null;
    int j = 0;
    boolean bool = false;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        d = zza.n(paramParcel, n);
        break;
      case 3: 
        bool = zza.c(paramParcel, n);
        break;
      case 4: 
        j = zza.g(paramParcel, n);
        break;
      case 5: 
        localApplicationMetadata = (ApplicationMetadata)zza.a(paramParcel, n, ApplicationMetadata.CREATOR);
        break;
      case 6: 
        i = zza.g(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new DeviceStatus(k, d, bool, j, localApplicationMetadata, i);
  }
  
  public DeviceStatus[] a(int paramInt)
  {
    return new DeviceStatus[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/internal/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */