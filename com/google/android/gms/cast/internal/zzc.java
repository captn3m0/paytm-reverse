package com.google.android.gms.cast.internal;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;

public abstract class zzc
  extends zzd
{
  protected final Handler b = new Handler(Looper.getMainLooper());
  protected final long c;
  protected final Runnable d = new zza(null);
  protected boolean e;
  
  public zzc(String paramString1, String paramString2, String paramString3, long paramLong)
  {
    super(paramString1, paramString2, paramString3);
    this.c = paramLong;
    a(false);
  }
  
  protected final void a(boolean paramBoolean)
  {
    if (this.e != paramBoolean)
    {
      this.e = paramBoolean;
      if (paramBoolean) {
        this.b.postDelayed(this.d, this.c);
      }
    }
    else
    {
      return;
    }
    this.b.removeCallbacks(this.d);
  }
  
  protected abstract boolean a(long paramLong);
  
  public void e()
  {
    a(false);
  }
  
  private class zza
    implements Runnable
  {
    private zza() {}
    
    public void run()
    {
      zzc.this.e = false;
      long l = SystemClock.elapsedRealtime();
      boolean bool = zzc.this.a(l);
      zzc.this.a(bool);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/internal/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */