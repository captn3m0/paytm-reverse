package com.google.android.gms.cast.internal;

import android.os.SystemClock;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaQueueItem;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.cast.TextTrackStyle;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class zzm
  extends zzc
{
  private static final String a = zzf.b("com.google.cast.media");
  private long g;
  private MediaStatus h;
  private final List<zzp> i = new ArrayList();
  private final zzp j = new zzp(86400000L);
  private final zzp k = new zzp(86400000L);
  private final zzp l = new zzp(86400000L);
  private final zzp m = new zzp(86400000L);
  private final zzp n = new zzp(86400000L);
  private final zzp o = new zzp(86400000L);
  private final zzp p = new zzp(86400000L);
  private final zzp q = new zzp(86400000L);
  private final zzp r = new zzp(86400000L);
  private final zzp s = new zzp(86400000L);
  private final zzp t = new zzp(86400000L);
  private final zzp u = new zzp(86400000L);
  private final zzp v = new zzp(86400000L);
  private final zzp w = new zzp(86400000L);
  
  public zzm(String paramString)
  {
    super(a, "MediaControlChannel", paramString, 1000L);
    this.i.add(this.j);
    this.i.add(this.k);
    this.i.add(this.l);
    this.i.add(this.m);
    this.i.add(this.n);
    this.i.add(this.o);
    this.i.add(this.p);
    this.i.add(this.q);
    this.i.add(this.r);
    this.i.add(this.s);
    this.i.add(this.t);
    this.i.add(this.u);
    this.i.add(this.v);
    this.i.add(this.w);
    j();
  }
  
  private void a(long paramLong, JSONObject paramJSONObject)
    throws JSONException
  {
    int i3 = 1;
    boolean bool = this.j.a(paramLong);
    int i2;
    if ((this.n.b()) && (!this.n.a(paramLong)))
    {
      i1 = 1;
      if (this.o.b())
      {
        i2 = i3;
        if (!this.o.a(paramLong)) {}
      }
      else
      {
        if ((!this.p.b()) || (this.p.a(paramLong))) {
          break label259;
        }
        i2 = i3;
      }
      label87:
      if (i1 == 0) {
        break label281;
      }
    }
    label259:
    label281:
    for (int i1 = 2;; i1 = 0)
    {
      i3 = i1;
      if (i2 != 0) {
        i3 = i1 | 0x1;
      }
      if ((bool) || (this.h == null))
      {
        this.h = new MediaStatus(paramJSONObject);
        this.g = SystemClock.elapsedRealtime();
      }
      for (i1 = 31;; i1 = this.h.a(paramJSONObject, i3))
      {
        if ((i1 & 0x1) != 0)
        {
          this.g = SystemClock.elapsedRealtime();
          a();
        }
        if ((i1 & 0x2) != 0)
        {
          this.g = SystemClock.elapsedRealtime();
          a();
        }
        if ((i1 & 0x4) != 0) {
          b();
        }
        if ((i1 & 0x8) != 0) {
          c();
        }
        if ((i1 & 0x10) != 0) {
          d();
        }
        paramJSONObject = this.i.iterator();
        while (paramJSONObject.hasNext()) {
          ((zzp)paramJSONObject.next()).a(paramLong, 0);
        }
        i1 = 0;
        break;
        i2 = 0;
        break label87;
      }
      return;
    }
  }
  
  private void j()
  {
    this.g = 0L;
    this.h = null;
    Iterator localIterator = this.i.iterator();
    while (localIterator.hasNext()) {
      ((zzp)localIterator.next()).a();
    }
  }
  
  public long a(zzo paramzzo)
    throws IOException
  {
    JSONObject localJSONObject = new JSONObject();
    long l1 = g();
    this.q.a(l1, paramzzo);
    a(true);
    try
    {
      localJSONObject.put("requestId", l1);
      localJSONObject.put("type", "GET_STATUS");
      if (this.h != null) {
        localJSONObject.put("mediaSessionId", this.h.a());
      }
      a(localJSONObject.toString(), l1, null);
      return l1;
    }
    catch (JSONException paramzzo)
    {
      for (;;) {}
    }
  }
  
  public long a(zzo paramzzo, double paramDouble, JSONObject paramJSONObject)
    throws IOException, IllegalStateException, IllegalArgumentException
  {
    if ((Double.isInfinite(paramDouble)) || (Double.isNaN(paramDouble))) {
      throw new IllegalArgumentException("Volume cannot be " + paramDouble);
    }
    JSONObject localJSONObject = new JSONObject();
    long l1 = g();
    this.o.a(l1, paramzzo);
    a(true);
    try
    {
      localJSONObject.put("requestId", l1);
      localJSONObject.put("type", "SET_VOLUME");
      localJSONObject.put("mediaSessionId", i());
      paramzzo = new JSONObject();
      paramzzo.put("level", paramDouble);
      localJSONObject.put("volume", paramzzo);
      if (paramJSONObject != null) {
        localJSONObject.put("customData", paramJSONObject);
      }
    }
    catch (JSONException paramzzo)
    {
      for (;;) {}
    }
    a(localJSONObject.toString(), l1, null);
    return l1;
  }
  
  public long a(zzo paramzzo, int paramInt1, long paramLong, MediaQueueItem[] paramArrayOfMediaQueueItem, int paramInt2, Integer paramInteger, JSONObject paramJSONObject)
    throws IOException, IllegalStateException
  {
    if ((paramLong != -1L) && (paramLong < 0L)) {
      throw new IllegalArgumentException("playPosition cannot be negative: " + paramLong);
    }
    JSONObject localJSONObject = new JSONObject();
    long l1 = g();
    this.u.a(l1, paramzzo);
    a(true);
    for (;;)
    {
      try
      {
        localJSONObject.put("requestId", l1);
        localJSONObject.put("type", "QUEUE_UPDATE");
        localJSONObject.put("mediaSessionId", i());
        if (paramInt1 != 0) {
          localJSONObject.put("currentItemId", paramInt1);
        }
        if (paramInt2 != 0) {
          localJSONObject.put("jump", paramInt2);
        }
        if ((paramArrayOfMediaQueueItem != null) && (paramArrayOfMediaQueueItem.length > 0))
        {
          paramzzo = new JSONArray();
          paramInt1 = 0;
          if (paramInt1 < paramArrayOfMediaQueueItem.length)
          {
            paramzzo.put(paramInt1, paramArrayOfMediaQueueItem[paramInt1].c());
            paramInt1 += 1;
            continue;
          }
          localJSONObject.put("items", paramzzo);
        }
        if (paramInteger != null) {}
        switch (paramInteger.intValue())
        {
        case 0: 
          if (paramLong != -1L) {
            localJSONObject.put("currentTime", zzf.a(paramLong));
          }
          if (paramJSONObject != null) {
            localJSONObject.put("customData", paramJSONObject);
          }
          break;
        }
      }
      catch (JSONException paramzzo)
      {
        continue;
      }
      a(localJSONObject.toString(), l1, null);
      return l1;
      localJSONObject.put("repeatMode", "REPEAT_OFF");
      continue;
      localJSONObject.put("repeatMode", "REPEAT_ALL");
      continue;
      localJSONObject.put("repeatMode", "REPEAT_SINGLE");
      continue;
      localJSONObject.put("repeatMode", "REPEAT_ALL_AND_SHUFFLE");
    }
  }
  
  public long a(zzo paramzzo, long paramLong, int paramInt, JSONObject paramJSONObject)
    throws IOException, IllegalStateException
  {
    JSONObject localJSONObject = new JSONObject();
    long l1 = g();
    this.n.a(l1, paramzzo);
    a(true);
    for (;;)
    {
      try
      {
        localJSONObject.put("requestId", l1);
        localJSONObject.put("type", "SEEK");
        localJSONObject.put("mediaSessionId", i());
        localJSONObject.put("currentTime", zzf.a(paramLong));
        if (paramInt != 1) {
          continue;
        }
        localJSONObject.put("resumeState", "PLAYBACK_START");
        if (paramJSONObject != null) {
          localJSONObject.put("customData", paramJSONObject);
        }
      }
      catch (JSONException paramzzo)
      {
        continue;
      }
      a(localJSONObject.toString(), l1, null);
      return l1;
      if (paramInt == 2) {
        localJSONObject.put("resumeState", "PLAYBACK_PAUSE");
      }
    }
  }
  
  public long a(zzo paramzzo, MediaInfo paramMediaInfo, boolean paramBoolean, long paramLong, long[] paramArrayOfLong, JSONObject paramJSONObject)
    throws IOException
  {
    JSONObject localJSONObject = new JSONObject();
    long l1 = g();
    this.j.a(l1, paramzzo);
    a(true);
    try
    {
      localJSONObject.put("requestId", l1);
      localJSONObject.put("type", "LOAD");
      localJSONObject.put("media", paramMediaInfo.a());
      localJSONObject.put("autoplay", paramBoolean);
      localJSONObject.put("currentTime", zzf.a(paramLong));
      if (paramArrayOfLong != null)
      {
        paramzzo = new JSONArray();
        int i1 = 0;
        while (i1 < paramArrayOfLong.length)
        {
          paramzzo.put(i1, paramArrayOfLong[i1]);
          i1 += 1;
        }
        localJSONObject.put("activeTrackIds", paramzzo);
      }
      if (paramJSONObject != null) {
        localJSONObject.put("customData", paramJSONObject);
      }
    }
    catch (JSONException paramzzo)
    {
      for (;;) {}
    }
    a(localJSONObject.toString(), l1, null);
    return l1;
  }
  
  public long a(zzo paramzzo, TextTrackStyle paramTextTrackStyle)
    throws IOException
  {
    JSONObject localJSONObject = new JSONObject();
    long l1 = g();
    this.s.a(l1, paramzzo);
    a(true);
    try
    {
      localJSONObject.put("requestId", l1);
      localJSONObject.put("type", "EDIT_TRACKS_INFO");
      if (paramTextTrackStyle != null) {
        localJSONObject.put("textTrackStyle", paramTextTrackStyle.a());
      }
      localJSONObject.put("mediaSessionId", i());
    }
    catch (JSONException paramzzo)
    {
      for (;;) {}
    }
    a(localJSONObject.toString(), l1, null);
    return l1;
  }
  
  public long a(zzo paramzzo, JSONObject paramJSONObject)
    throws IOException
  {
    JSONObject localJSONObject = new JSONObject();
    long l1 = g();
    this.k.a(l1, paramzzo);
    a(true);
    try
    {
      localJSONObject.put("requestId", l1);
      localJSONObject.put("type", "PAUSE");
      localJSONObject.put("mediaSessionId", i());
      if (paramJSONObject != null) {
        localJSONObject.put("customData", paramJSONObject);
      }
    }
    catch (JSONException paramzzo)
    {
      for (;;) {}
    }
    a(localJSONObject.toString(), l1, null);
    return l1;
  }
  
  public long a(zzo paramzzo, boolean paramBoolean, JSONObject paramJSONObject)
    throws IOException, IllegalStateException
  {
    JSONObject localJSONObject = new JSONObject();
    long l1 = g();
    this.p.a(l1, paramzzo);
    a(true);
    try
    {
      localJSONObject.put("requestId", l1);
      localJSONObject.put("type", "SET_VOLUME");
      localJSONObject.put("mediaSessionId", i());
      paramzzo = new JSONObject();
      paramzzo.put("muted", paramBoolean);
      localJSONObject.put("volume", paramzzo);
      if (paramJSONObject != null) {
        localJSONObject.put("customData", paramJSONObject);
      }
    }
    catch (JSONException paramzzo)
    {
      for (;;) {}
    }
    a(localJSONObject.toString(), l1, null);
    return l1;
  }
  
  public long a(zzo paramzzo, int[] paramArrayOfInt, int paramInt, JSONObject paramJSONObject)
    throws IOException, IllegalStateException, IllegalArgumentException
  {
    if ((paramArrayOfInt == null) || (paramArrayOfInt.length == 0)) {
      throw new IllegalArgumentException("itemIdsToReorder must not be null or empty.");
    }
    JSONObject localJSONObject = new JSONObject();
    long l1 = g();
    this.w.a(l1, paramzzo);
    a(true);
    try
    {
      localJSONObject.put("requestId", l1);
      localJSONObject.put("type", "QUEUE_REORDER");
      localJSONObject.put("mediaSessionId", i());
      paramzzo = new JSONArray();
      int i1 = 0;
      while (i1 < paramArrayOfInt.length)
      {
        paramzzo.put(i1, paramArrayOfInt[i1]);
        i1 += 1;
      }
      localJSONObject.put("itemIds", paramzzo);
      if (paramInt != 0) {
        localJSONObject.put("insertBefore", paramInt);
      }
      if (paramJSONObject != null) {
        localJSONObject.put("customData", paramJSONObject);
      }
    }
    catch (JSONException paramzzo)
    {
      for (;;) {}
    }
    a(localJSONObject.toString(), l1, null);
    return l1;
  }
  
  public long a(zzo paramzzo, int[] paramArrayOfInt, JSONObject paramJSONObject)
    throws IOException, IllegalStateException, IllegalArgumentException
  {
    if ((paramArrayOfInt == null) || (paramArrayOfInt.length == 0)) {
      throw new IllegalArgumentException("itemIdsToRemove must not be null or empty.");
    }
    JSONObject localJSONObject = new JSONObject();
    long l1 = g();
    this.v.a(l1, paramzzo);
    a(true);
    try
    {
      localJSONObject.put("requestId", l1);
      localJSONObject.put("type", "QUEUE_REMOVE");
      localJSONObject.put("mediaSessionId", i());
      paramzzo = new JSONArray();
      int i1 = 0;
      while (i1 < paramArrayOfInt.length)
      {
        paramzzo.put(i1, paramArrayOfInt[i1]);
        i1 += 1;
      }
      localJSONObject.put("itemIds", paramzzo);
      if (paramJSONObject != null) {
        localJSONObject.put("customData", paramJSONObject);
      }
    }
    catch (JSONException paramzzo)
    {
      for (;;) {}
    }
    a(localJSONObject.toString(), l1, null);
    return l1;
  }
  
  public long a(zzo paramzzo, long[] paramArrayOfLong)
    throws IOException
  {
    JSONObject localJSONObject = new JSONObject();
    long l1 = g();
    this.r.a(l1, paramzzo);
    a(true);
    try
    {
      localJSONObject.put("requestId", l1);
      localJSONObject.put("type", "EDIT_TRACKS_INFO");
      localJSONObject.put("mediaSessionId", i());
      paramzzo = new JSONArray();
      int i1 = 0;
      while (i1 < paramArrayOfLong.length)
      {
        paramzzo.put(i1, paramArrayOfLong[i1]);
        i1 += 1;
      }
      localJSONObject.put("activeTrackIds", paramzzo);
    }
    catch (JSONException paramzzo)
    {
      for (;;) {}
    }
    a(localJSONObject.toString(), l1, null);
    return l1;
  }
  
  public long a(zzo paramzzo, MediaQueueItem[] paramArrayOfMediaQueueItem, int paramInt1, int paramInt2, int paramInt3, long paramLong, JSONObject paramJSONObject)
    throws IOException, IllegalStateException, IllegalStateException
  {
    if ((paramArrayOfMediaQueueItem == null) || (paramArrayOfMediaQueueItem.length == 0)) {
      throw new IllegalArgumentException("itemsToInsert must not be null or empty.");
    }
    if ((paramInt2 != 0) && (paramInt3 != -1)) {
      throw new IllegalArgumentException("can not set both currentItemId and currentItemIndexInItemsToInsert");
    }
    if ((paramInt3 != -1) && ((paramInt3 < 0) || (paramInt3 >= paramArrayOfMediaQueueItem.length))) {
      throw new IllegalArgumentException(String.format("currentItemIndexInItemsToInsert %d out of range [0, %d).", new Object[] { Integer.valueOf(paramInt3), Integer.valueOf(paramArrayOfMediaQueueItem.length) }));
    }
    if ((paramLong != -1L) && (paramLong < 0L)) {
      throw new IllegalArgumentException("playPosition can not be negative: " + paramLong);
    }
    JSONObject localJSONObject = new JSONObject();
    long l1 = g();
    this.t.a(l1, paramzzo);
    a(true);
    try
    {
      localJSONObject.put("requestId", l1);
      localJSONObject.put("type", "QUEUE_INSERT");
      localJSONObject.put("mediaSessionId", i());
      paramzzo = new JSONArray();
      int i1 = 0;
      while (i1 < paramArrayOfMediaQueueItem.length)
      {
        paramzzo.put(i1, paramArrayOfMediaQueueItem[i1].c());
        i1 += 1;
      }
      localJSONObject.put("items", paramzzo);
      if (paramInt1 != 0) {
        localJSONObject.put("insertBefore", paramInt1);
      }
      if (paramInt2 != 0) {
        localJSONObject.put("currentItemId", paramInt2);
      }
      if (paramInt3 != -1) {
        localJSONObject.put("currentItemIndex", paramInt3);
      }
      if (paramLong != -1L) {
        localJSONObject.put("currentTime", zzf.a(paramLong));
      }
      if (paramJSONObject != null) {
        localJSONObject.put("customData", paramJSONObject);
      }
    }
    catch (JSONException paramzzo)
    {
      for (;;) {}
    }
    a(localJSONObject.toString(), l1, null);
    return l1;
  }
  
  public long a(zzo paramzzo, MediaQueueItem[] paramArrayOfMediaQueueItem, int paramInt1, int paramInt2, long paramLong, JSONObject paramJSONObject)
    throws IOException, IllegalArgumentException
  {
    if ((paramArrayOfMediaQueueItem == null) || (paramArrayOfMediaQueueItem.length == 0)) {
      throw new IllegalArgumentException("items must not be null or empty.");
    }
    if ((paramInt1 < 0) || (paramInt1 >= paramArrayOfMediaQueueItem.length)) {
      throw new IllegalArgumentException("Invalid startIndex: " + paramInt1);
    }
    if ((paramLong != -1L) && (paramLong < 0L)) {
      throw new IllegalArgumentException("playPosition can not be negative: " + paramLong);
    }
    JSONObject localJSONObject = new JSONObject();
    long l1 = g();
    this.j.a(l1, paramzzo);
    a(true);
    for (;;)
    {
      try
      {
        localJSONObject.put("requestId", l1);
        localJSONObject.put("type", "QUEUE_LOAD");
        paramzzo = new JSONArray();
        int i1 = 0;
        if (i1 < paramArrayOfMediaQueueItem.length)
        {
          paramzzo.put(i1, paramArrayOfMediaQueueItem[i1].c());
          i1 += 1;
          continue;
        }
        localJSONObject.put("items", paramzzo);
        switch (paramInt2)
        {
        case 0: 
          throw new IllegalArgumentException("Invalid repeat mode: " + paramInt2);
        }
      }
      catch (JSONException paramzzo)
      {
        a(localJSONObject.toString(), l1, null);
        return l1;
      }
      localJSONObject.put("repeatMode", "REPEAT_OFF");
      for (;;)
      {
        localJSONObject.put("startIndex", paramInt1);
        if (paramLong != -1L) {
          localJSONObject.put("currentTime", zzf.a(paramLong));
        }
        if (paramJSONObject == null) {
          break;
        }
        localJSONObject.put("customData", paramJSONObject);
        break;
        localJSONObject.put("repeatMode", "REPEAT_ALL");
        continue;
        localJSONObject.put("repeatMode", "REPEAT_SINGLE");
        continue;
        localJSONObject.put("repeatMode", "REPEAT_ALL_AND_SHUFFLE");
      }
    }
  }
  
  protected void a() {}
  
  public void a(long paramLong, int paramInt)
  {
    Iterator localIterator = this.i.iterator();
    while (localIterator.hasNext()) {
      ((zzp)localIterator.next()).a(paramLong, paramInt);
    }
  }
  
  protected boolean a(long paramLong)
  {
    ??? = this.i.iterator();
    while (((Iterator)???).hasNext()) {
      ((zzp)((Iterator)???).next()).b(paramLong, 2102);
    }
    for (;;)
    {
      synchronized (zzp.a)
      {
        Iterator localIterator = this.i.iterator();
        if (localIterator.hasNext())
        {
          if (!((zzp)localIterator.next()).b()) {
            continue;
          }
          bool = true;
          return bool;
        }
      }
      boolean bool = false;
    }
  }
  
  public long b(zzo paramzzo, JSONObject paramJSONObject)
    throws IOException
  {
    JSONObject localJSONObject = new JSONObject();
    long l1 = g();
    this.m.a(l1, paramzzo);
    a(true);
    try
    {
      localJSONObject.put("requestId", l1);
      localJSONObject.put("type", "STOP");
      localJSONObject.put("mediaSessionId", i());
      if (paramJSONObject != null) {
        localJSONObject.put("customData", paramJSONObject);
      }
    }
    catch (JSONException paramzzo)
    {
      for (;;) {}
    }
    a(localJSONObject.toString(), l1, null);
    return l1;
  }
  
  protected void b() {}
  
  public final void b(String paramString)
  {
    this.f.b("message received: %s", new Object[] { paramString });
    Object localObject2;
    long l1;
    try
    {
      Object localObject1 = new JSONObject(paramString);
      localObject2 = ((JSONObject)localObject1).getString("type");
      l1 = ((JSONObject)localObject1).optLong("requestId", -1L);
      if (((String)localObject2).equals("MEDIA_STATUS"))
      {
        localObject1 = ((JSONObject)localObject1).getJSONArray("status");
        if (((JSONArray)localObject1).length() > 0)
        {
          a(l1, ((JSONArray)localObject1).getJSONObject(0));
          return;
        }
        this.h = null;
        a();
        b();
        c();
        d();
        this.q.a(l1, 0);
        return;
      }
    }
    catch (JSONException localJSONException)
    {
      this.f.d("Message is malformed (%s); ignoring: %s", new Object[] { localJSONException.getMessage(), paramString });
      return;
    }
    JSONObject localJSONObject;
    if (((String)localObject2).equals("INVALID_PLAYER_STATE"))
    {
      this.f.d("received unexpected error: Invalid Player State.", new Object[0]);
      localJSONObject = localJSONException.optJSONObject("customData");
      localObject2 = this.i.iterator();
      while (((Iterator)localObject2).hasNext()) {
        ((zzp)((Iterator)localObject2).next()).a(l1, 2100, localJSONObject);
      }
    }
    if (((String)localObject2).equals("LOAD_FAILED"))
    {
      localJSONObject = localJSONObject.optJSONObject("customData");
      this.j.a(l1, 2100, localJSONObject);
      return;
    }
    if (((String)localObject2).equals("LOAD_CANCELLED"))
    {
      localJSONObject = localJSONObject.optJSONObject("customData");
      this.j.a(l1, 2101, localJSONObject);
      return;
    }
    if (((String)localObject2).equals("INVALID_REQUEST"))
    {
      this.f.d("received unexpected error: Invalid Request.", new Object[0]);
      localJSONObject = localJSONObject.optJSONObject("customData");
      localObject2 = this.i.iterator();
      while (((Iterator)localObject2).hasNext()) {
        ((zzp)((Iterator)localObject2).next()).a(l1, 2100, localJSONObject);
      }
    }
  }
  
  public long c(zzo paramzzo, JSONObject paramJSONObject)
    throws IOException, IllegalStateException
  {
    JSONObject localJSONObject = new JSONObject();
    long l1 = g();
    this.l.a(l1, paramzzo);
    a(true);
    try
    {
      localJSONObject.put("requestId", l1);
      localJSONObject.put("type", "PLAY");
      localJSONObject.put("mediaSessionId", i());
      if (paramJSONObject != null) {
        localJSONObject.put("customData", paramJSONObject);
      }
    }
    catch (JSONException paramzzo)
    {
      for (;;) {}
    }
    a(localJSONObject.toString(), l1, null);
    return l1;
  }
  
  protected void c() {}
  
  protected void d() {}
  
  public void e()
  {
    super.e();
    j();
  }
  
  public MediaStatus h()
  {
    return this.h;
  }
  
  public long i()
    throws IllegalStateException
  {
    if (this.h == null) {
      throw new IllegalStateException("No current media session");
    }
    return this.h.a();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/internal/zzm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */