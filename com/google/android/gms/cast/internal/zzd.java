package com.google.android.gms.cast.internal;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import java.io.IOException;

public abstract class zzd
{
  private final String a;
  private zzn b;
  protected final zzl f;
  
  protected zzd(String paramString1, String paramString2, String paramString3)
  {
    zzf.a(paramString1);
    this.a = paramString1;
    this.f = new zzl(paramString2);
    a(paramString3);
  }
  
  public void a(long paramLong, int paramInt) {}
  
  public final void a(zzn paramzzn)
  {
    this.b = paramzzn;
    if (this.b == null) {
      e();
    }
  }
  
  public void a(String paramString)
  {
    if (!TextUtils.isEmpty(paramString)) {
      this.f.a(paramString);
    }
  }
  
  protected final void a(String paramString1, long paramLong, String paramString2)
    throws IOException
  {
    this.f.a("Sending text message: %s to: %s", new Object[] { paramString1, paramString2 });
    this.b.a(this.a, paramString1, paramLong, paramString2);
  }
  
  public void b(@NonNull String paramString) {}
  
  public void e() {}
  
  public final String f()
  {
    return this.a;
  }
  
  protected final long g()
  {
    return this.b.a();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/internal/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */