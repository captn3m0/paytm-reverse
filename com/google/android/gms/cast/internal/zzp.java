package com.google.android.gms.cast.internal;

import android.os.SystemClock;

public final class zzp
{
  public static final Object a = new Object();
  private static final zzl f = new zzl("RequestTracker");
  private long b;
  private long c;
  private long d;
  private zzo e;
  
  public zzp(long paramLong)
  {
    this.b = paramLong;
    this.c = -1L;
    this.d = 0L;
  }
  
  private void c()
  {
    this.c = -1L;
    this.e = null;
    this.d = 0L;
  }
  
  public void a()
  {
    synchronized (a)
    {
      if (this.c != -1L) {
        c();
      }
      return;
    }
  }
  
  public void a(long paramLong, zzo paramzzo)
  {
    synchronized (a)
    {
      zzo localzzo = this.e;
      long l = this.c;
      this.c = paramLong;
      this.e = paramzzo;
      this.d = SystemClock.elapsedRealtime();
      if (localzzo != null) {
        localzzo.a(l);
      }
      return;
    }
  }
  
  public boolean a(long paramLong)
  {
    for (;;)
    {
      synchronized (a)
      {
        if ((this.c != -1L) && (this.c == paramLong))
        {
          bool = true;
          return bool;
        }
      }
      boolean bool = false;
    }
  }
  
  public boolean a(long paramLong, int paramInt)
  {
    return a(paramLong, paramInt, null);
  }
  
  public boolean a(long paramLong, int paramInt, Object paramObject)
  {
    boolean bool = true;
    zzo localzzo = null;
    for (;;)
    {
      synchronized (a)
      {
        if ((this.c != -1L) && (this.c == paramLong))
        {
          f.b("request %d completed", new Object[] { Long.valueOf(this.c) });
          localzzo = this.e;
          c();
          if (localzzo != null) {
            localzzo.a(paramLong, paramInt, paramObject);
          }
          return bool;
        }
      }
      bool = false;
    }
  }
  
  public boolean b()
  {
    for (;;)
    {
      synchronized (a)
      {
        if (this.c != -1L)
        {
          bool = true;
          return bool;
        }
      }
      boolean bool = false;
    }
  }
  
  public boolean b(long paramLong, int paramInt)
  {
    boolean bool = true;
    long l = 0L;
    for (;;)
    {
      synchronized (a)
      {
        if ((this.c != -1L) && (paramLong - this.d >= this.b))
        {
          f.b("request %d timed out", new Object[] { Long.valueOf(this.c) });
          paramLong = this.c;
          zzo localzzo = this.e;
          c();
          if (localzzo != null) {
            localzzo.a(paramLong, paramInt, null);
          }
          return bool;
        }
      }
      bool = false;
      Object localObject2 = null;
      paramLong = l;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/internal/zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */