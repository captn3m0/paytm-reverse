package com.google.android.gms.cast.internal;

import java.io.IOException;

public abstract interface zzn
{
  public abstract long a();
  
  public abstract void a(String paramString1, String paramString2, long paramLong, String paramString3)
    throws IOException;
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/internal/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */