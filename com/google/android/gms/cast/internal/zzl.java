package com.google.android.gms.cast.internal;

import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.internal.zzx;
import java.util.Locale;

public class zzl
{
  private static boolean b = false;
  protected final String a;
  private final boolean c;
  private boolean d;
  private boolean e;
  private String f;
  
  public zzl(String paramString)
  {
    this(paramString, c());
  }
  
  public zzl(String paramString, boolean paramBoolean)
  {
    zzx.a(paramString, "The log tag cannot be null or empty.");
    this.a = paramString;
    if (paramString.length() <= 23) {}
    for (boolean bool = true;; bool = false)
    {
      this.c = bool;
      this.d = paramBoolean;
      this.e = false;
      return;
    }
  }
  
  public static boolean c()
  {
    return b;
  }
  
  public void a(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {}
    for (paramString = null;; paramString = String.format("[%s] ", new Object[] { paramString }))
    {
      this.f = paramString;
      return;
    }
  }
  
  public void a(String paramString, Object... paramVarArgs)
  {
    if (b()) {
      Log.v(this.a, f(paramString, paramVarArgs));
    }
  }
  
  public void a(Throwable paramThrowable, String paramString, Object... paramVarArgs)
  {
    if ((a()) || (b)) {
      Log.d(this.a, f(paramString, paramVarArgs), paramThrowable);
    }
  }
  
  public boolean a()
  {
    return (this.d) || ((this.c) && (Log.isLoggable(this.a, 3)));
  }
  
  public void b(String paramString, Object... paramVarArgs)
  {
    if ((a()) || (b)) {
      Log.d(this.a, f(paramString, paramVarArgs));
    }
  }
  
  public void b(Throwable paramThrowable, String paramString, Object... paramVarArgs)
  {
    Log.w(this.a, f(paramString, paramVarArgs), paramThrowable);
  }
  
  public boolean b()
  {
    return this.e;
  }
  
  public void c(String paramString, Object... paramVarArgs)
  {
    Log.i(this.a, f(paramString, paramVarArgs));
  }
  
  public void d(String paramString, Object... paramVarArgs)
  {
    Log.w(this.a, f(paramString, paramVarArgs));
  }
  
  public void e(String paramString, Object... paramVarArgs)
  {
    Log.e(this.a, f(paramString, paramVarArgs));
  }
  
  protected String f(String paramString, Object... paramVarArgs)
  {
    if (paramVarArgs.length == 0) {}
    for (;;)
    {
      paramVarArgs = paramString;
      if (!TextUtils.isEmpty(this.f)) {
        paramVarArgs = this.f + paramString;
      }
      return paramVarArgs;
      paramString = String.format(Locale.ROOT, paramString, paramVarArgs);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/internal/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */