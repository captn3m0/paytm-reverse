package com.google.android.gms.cast;

import android.content.Context;
import android.os.Looper;
import android.view.Display;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.HasOptions;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzlq;
import com.google.android.gms.internal.zzlr;

public final class CastRemoteDisplay
{
  public static final Api<CastRemoteDisplayOptions> a = new Api("CastRemoteDisplay.API", d, c);
  public static final CastRemoteDisplayApi b = new zzlq(c);
  private static final Api.zzc<zzlr> c = new Api.zzc();
  private static final Api.zza<zzlr, CastRemoteDisplayOptions> d = new Api.zza()
  {
    public zzlr a(Context paramAnonymousContext, Looper paramAnonymousLooper, zzf paramAnonymouszzf, CastRemoteDisplay.CastRemoteDisplayOptions paramAnonymousCastRemoteDisplayOptions, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      return new zzlr(paramAnonymousContext, paramAnonymousLooper, paramAnonymouszzf, paramAnonymousCastRemoteDisplayOptions.a, paramAnonymousCastRemoteDisplayOptions.b, paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
    }
  };
  
  public static final class CastRemoteDisplayOptions
    implements Api.ApiOptions.HasOptions
  {
    final CastDevice a;
    final CastRemoteDisplay.CastRemoteDisplaySessionCallbacks b;
    
    private CastRemoteDisplayOptions(Builder paramBuilder)
    {
      this.a = paramBuilder.a;
      this.b = paramBuilder.b;
    }
    
    public static final class Builder
    {
      CastDevice a;
      CastRemoteDisplay.CastRemoteDisplaySessionCallbacks b;
      
      public Builder(CastDevice paramCastDevice, CastRemoteDisplay.CastRemoteDisplaySessionCallbacks paramCastRemoteDisplaySessionCallbacks)
      {
        zzx.a(paramCastDevice, "CastDevice parameter cannot be null");
        this.a = paramCastDevice;
        this.b = paramCastRemoteDisplaySessionCallbacks;
      }
      
      public CastRemoteDisplay.CastRemoteDisplayOptions a()
      {
        return new CastRemoteDisplay.CastRemoteDisplayOptions(this, null);
      }
    }
  }
  
  public static abstract interface CastRemoteDisplaySessionCallbacks
  {
    public abstract void a(Status paramStatus);
  }
  
  public static abstract interface CastRemoteDisplaySessionResult
    extends Result
  {
    public abstract Display a();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/CastRemoteDisplay.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */