package com.google.android.gms.cast;

import android.util.SparseArray;
import com.google.android.gms.cast.internal.zzf;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class MediaStatus
{
  private int a = 0;
  private long b;
  private MediaInfo c;
  private double d;
  private int e;
  private int f;
  private long g;
  private long h;
  private double i;
  private boolean j;
  private long[] k;
  private JSONObject l;
  private int m = 0;
  private int n = 0;
  private final zza o = new zza();
  
  public MediaStatus(JSONObject paramJSONObject)
    throws JSONException
  {
    a(paramJSONObject, 0);
  }
  
  private boolean a(int paramInt1, int paramInt2)
  {
    return (paramInt1 == 1) && (paramInt2 == 0);
  }
  
  public int a(JSONObject paramJSONObject, int paramInt)
    throws JSONException
  {
    int i6 = 2;
    int i5 = 1;
    long l1 = paramJSONObject.getLong("mediaSessionId");
    if (l1 != this.b) {
      this.b = l1;
    }
    for (int i2 = 1;; i2 = 0)
    {
      int i3 = i2;
      Object localObject;
      int i1;
      if (paramJSONObject.has("playerState"))
      {
        localObject = paramJSONObject.getString("playerState");
        if (!((String)localObject).equals("IDLE")) {
          break label461;
        }
        i1 = 1;
      }
      for (;;)
      {
        int i4 = i2;
        if (i1 != this.e)
        {
          this.e = i1;
          i4 = i2 | 0x2;
        }
        i3 = i4;
        if (i1 == 1)
        {
          i3 = i4;
          if (paramJSONObject.has("idleReason"))
          {
            localObject = paramJSONObject.getString("idleReason");
            if (!((String)localObject).equals("CANCELLED")) {
              break label509;
            }
            i1 = i6;
          }
        }
        for (;;)
        {
          label137:
          i3 = i4;
          if (i1 != this.f)
          {
            this.f = i1;
            i3 = i4 | 0x2;
          }
          i1 = i3;
          double d1;
          if (paramJSONObject.has("playbackRate"))
          {
            d1 = paramJSONObject.getDouble("playbackRate");
            i1 = i3;
            if (this.d != d1)
            {
              this.d = d1;
              i1 = i3 | 0x2;
            }
          }
          i3 = i1;
          if (paramJSONObject.has("currentTime"))
          {
            i3 = i1;
            if ((paramInt & 0x2) == 0)
            {
              l1 = zzf.a(paramJSONObject.getDouble("currentTime"));
              i3 = i1;
              if (l1 != this.g)
              {
                this.g = l1;
                i3 = i1 | 0x2;
              }
            }
          }
          i2 = i3;
          if (paramJSONObject.has("supportedMediaCommands"))
          {
            l1 = paramJSONObject.getLong("supportedMediaCommands");
            i2 = i3;
            if (l1 != this.h)
            {
              this.h = l1;
              i2 = i3 | 0x2;
            }
          }
          i1 = i2;
          if (paramJSONObject.has("volume"))
          {
            i1 = i2;
            if ((paramInt & 0x1) == 0)
            {
              localObject = paramJSONObject.getJSONObject("volume");
              d1 = ((JSONObject)localObject).getDouble("level");
              paramInt = i2;
              if (d1 != this.i)
              {
                this.i = d1;
                paramInt = i2 | 0x2;
              }
              boolean bool = ((JSONObject)localObject).getBoolean("muted");
              i1 = paramInt;
              if (bool != this.j)
              {
                this.j = bool;
                i1 = paramInt | 0x2;
              }
            }
          }
          if (paramJSONObject.has("activeTrackIds"))
          {
            JSONArray localJSONArray = paramJSONObject.getJSONArray("activeTrackIds");
            i3 = localJSONArray.length();
            localObject = new long[i3];
            paramInt = 0;
            for (;;)
            {
              if (paramInt < i3)
              {
                localObject[paramInt] = localJSONArray.getLong(paramInt);
                paramInt += 1;
                continue;
                label461:
                if (((String)localObject).equals("PLAYING"))
                {
                  i1 = 2;
                  break;
                }
                if (((String)localObject).equals("PAUSED"))
                {
                  i1 = 3;
                  break;
                }
                if (!((String)localObject).equals("BUFFERING")) {
                  break label956;
                }
                i1 = 4;
                break;
                label509:
                if (((String)localObject).equals("INTERRUPTED"))
                {
                  i1 = 3;
                  break label137;
                }
                if (((String)localObject).equals("FINISHED"))
                {
                  i1 = 1;
                  break label137;
                }
                if (!((String)localObject).equals("ERROR")) {
                  break label950;
                }
                i1 = 4;
                break label137;
              }
            }
            if (this.k == null) {
              paramInt = i5;
            }
          }
          for (;;)
          {
            if (paramInt != 0) {
              this.k = ((long[])localObject);
            }
            i2 = paramInt;
            for (;;)
            {
              label580:
              paramInt = i1;
              if (i2 != 0)
              {
                this.k = ((long[])localObject);
                paramInt = i1 | 0x2;
              }
              i1 = paramInt;
              if (paramJSONObject.has("customData"))
              {
                this.l = paramJSONObject.getJSONObject("customData");
                i1 = paramInt | 0x2;
              }
              paramInt = i1;
              if (paramJSONObject.has("media"))
              {
                localObject = paramJSONObject.getJSONObject("media");
                this.c = new MediaInfo((JSONObject)localObject);
                i1 |= 0x2;
                paramInt = i1;
                if (((JSONObject)localObject).has("metadata")) {
                  paramInt = i1 | 0x4;
                }
              }
              i1 = paramInt;
              if (paramJSONObject.has("currentItemId"))
              {
                i2 = paramJSONObject.getInt("currentItemId");
                i1 = paramInt;
                if (this.a != i2)
                {
                  this.a = i2;
                  i1 = paramInt | 0x2;
                }
              }
              i2 = paramJSONObject.optInt("preloadedItemId", 0);
              paramInt = i1;
              if (this.n != i2)
              {
                this.n = i2;
                paramInt = i1 | 0x10;
              }
              i2 = paramJSONObject.optInt("loadingItemId", 0);
              i1 = paramInt;
              if (this.m != i2)
              {
                this.m = i2;
                i1 = paramInt | 0x2;
              }
              if (!a(this.e, this.m))
              {
                paramInt = i1;
                if (zza.a(this.o, paramJSONObject)) {
                  paramInt = i1 | 0x8;
                }
              }
              do
              {
                return paramInt;
                paramInt = i5;
                if (this.k.length != i3) {
                  break;
                }
                i2 = 0;
                for (;;)
                {
                  if (i2 >= i3) {
                    break label945;
                  }
                  paramInt = i5;
                  if (this.k[i2] != localObject[i2]) {
                    break;
                  }
                  i2 += 1;
                }
                if (this.k == null) {
                  break label936;
                }
                i2 = 1;
                localObject = null;
                break label580;
                this.a = 0;
                this.m = 0;
                this.n = 0;
                paramInt = i1;
              } while (this.o.a() <= 0);
              zza.a(this.o);
              return i1 | 0x8;
              label936:
              localObject = null;
              i2 = 0;
            }
            label945:
            paramInt = 0;
          }
          label950:
          i1 = 0;
        }
        label956:
        i1 = 0;
      }
    }
  }
  
  public long a()
  {
    return this.b;
  }
  
  public MediaQueueItem a(int paramInt)
  {
    return this.o.b(paramInt);
  }
  
  public int b()
  {
    return this.o.a();
  }
  
  private class zza
  {
    private int b = 0;
    private List<MediaQueueItem> c = new ArrayList();
    private SparseArray<Integer> d = new SparseArray();
    
    zza() {}
    
    private void a(MediaQueueItem[] paramArrayOfMediaQueueItem)
    {
      this.c.clear();
      this.d.clear();
      int i = 0;
      while (i < paramArrayOfMediaQueueItem.length)
      {
        MediaQueueItem localMediaQueueItem = paramArrayOfMediaQueueItem[i];
        this.c.add(localMediaQueueItem);
        this.d.put(localMediaQueueItem.a(), Integer.valueOf(i));
        i += 1;
      }
    }
    
    private boolean a(JSONObject paramJSONObject)
      throws JSONException
    {
      int j = 2;
      boolean bool2 = true;
      Object localObject;
      int i;
      if (paramJSONObject.has("repeatMode"))
      {
        int k = this.b;
        localObject = paramJSONObject.getString("repeatMode");
        i = -1;
        switch (((String)localObject).hashCode())
        {
        default: 
          switch (i)
          {
          default: 
            j = k;
          case 2: 
            label111:
            if (this.b != j) {
              this.b = j;
            }
            break;
          }
          break;
        }
      }
      for (boolean bool1 = true;; bool1 = false)
      {
        if (paramJSONObject.has("items"))
        {
          paramJSONObject = paramJSONObject.getJSONArray("items");
          j = paramJSONObject.length();
          localObject = new SparseArray();
          i = 0;
          for (;;)
          {
            if (i < j)
            {
              ((SparseArray)localObject).put(i, Integer.valueOf(paramJSONObject.getJSONObject(i).getInt("itemId")));
              i += 1;
              continue;
              if (!((String)localObject).equals("REPEAT_OFF")) {
                break;
              }
              i = 0;
              break;
              if (!((String)localObject).equals("REPEAT_ALL")) {
                break;
              }
              i = 1;
              break;
              if (!((String)localObject).equals("REPEAT_SINGLE")) {
                break;
              }
              i = 2;
              break;
              if (!((String)localObject).equals("REPEAT_ALL_AND_SHUFFLE")) {
                break;
              }
              i = 3;
              break;
              j = 0;
              break label111;
              j = 1;
              break label111;
              j = 3;
              break label111;
            }
          }
          MediaQueueItem[] arrayOfMediaQueueItem = new MediaQueueItem[j];
          i = 0;
          Integer localInteger;
          JSONObject localJSONObject;
          boolean bool3;
          if (i < j)
          {
            localInteger = (Integer)((SparseArray)localObject).get(i);
            localJSONObject = paramJSONObject.getJSONObject(i);
            MediaQueueItem localMediaQueueItem = a(localInteger.intValue());
            if (localMediaQueueItem != null)
            {
              bool3 = localMediaQueueItem.a(localJSONObject);
              arrayOfMediaQueueItem[i] = localMediaQueueItem;
              if (i == c(localInteger.intValue()).intValue()) {
                break label453;
              }
              bool1 = true;
            }
          }
          for (;;)
          {
            i += 1;
            break;
            if (localInteger.intValue() == MediaStatus.a(MediaStatus.this))
            {
              arrayOfMediaQueueItem[i] = new MediaQueueItem.Builder(MediaStatus.b(MediaStatus.this)).a();
              arrayOfMediaQueueItem[i].a(localJSONObject);
              bool1 = true;
            }
            else
            {
              arrayOfMediaQueueItem[i] = new MediaQueueItem(localJSONObject);
              bool1 = true;
              continue;
              if (this.c.size() != j) {
                bool1 = bool2;
              }
              for (;;)
              {
                a(arrayOfMediaQueueItem);
                return bool1;
              }
              label453:
              bool1 |= bool3;
            }
          }
        }
        return bool1;
      }
    }
    
    private void b()
    {
      this.b = 0;
      this.c.clear();
      this.d.clear();
    }
    
    private Integer c(int paramInt)
    {
      return (Integer)this.d.get(paramInt);
    }
    
    public int a()
    {
      return this.c.size();
    }
    
    public MediaQueueItem a(int paramInt)
    {
      Integer localInteger = (Integer)this.d.get(paramInt);
      if (localInteger == null) {
        return null;
      }
      return (MediaQueueItem)this.c.get(localInteger.intValue());
    }
    
    public MediaQueueItem b(int paramInt)
    {
      if ((paramInt < 0) || (paramInt >= this.c.size())) {
        return null;
      }
      return (MediaQueueItem)this.c.get(paramInt);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/MediaStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */