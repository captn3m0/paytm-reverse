package com.google.android.gms.cast;

import android.graphics.Color;
import com.google.android.gms.cast.internal.zzf;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.internal.zznb;
import org.json.JSONException;
import org.json.JSONObject;

public final class TextTrackStyle
{
  private float a;
  private int b;
  private int c;
  private int d;
  private int e;
  private int f;
  private int g;
  private int h;
  private String i;
  private int j;
  private int k;
  private JSONObject l;
  
  public TextTrackStyle()
  {
    b();
  }
  
  private int a(String paramString)
  {
    int n = 0;
    int m = n;
    if (paramString != null)
    {
      m = n;
      if (paramString.length() == 9)
      {
        m = n;
        if (paramString.charAt(0) != '#') {}
      }
    }
    try
    {
      m = Integer.parseInt(paramString.substring(1, 3), 16);
      n = Integer.parseInt(paramString.substring(3, 5), 16);
      int i1 = Integer.parseInt(paramString.substring(5, 7), 16);
      m = Color.argb(Integer.parseInt(paramString.substring(7, 9), 16), m, n, i1);
      return m;
    }
    catch (NumberFormatException paramString) {}
    return 0;
  }
  
  private String a(int paramInt)
  {
    return String.format("#%02X%02X%02X%02X", new Object[] { Integer.valueOf(Color.red(paramInt)), Integer.valueOf(Color.green(paramInt)), Integer.valueOf(Color.blue(paramInt)), Integer.valueOf(Color.alpha(paramInt)) });
  }
  
  private void b()
  {
    this.a = 1.0F;
    this.b = 0;
    this.c = 0;
    this.d = -1;
    this.e = 0;
    this.f = -1;
    this.g = 0;
    this.h = 0;
    this.i = null;
    this.j = -1;
    this.k = -1;
    this.l = null;
  }
  
  public JSONObject a()
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("fontScale", this.a);
      if (this.b != 0) {
        localJSONObject.put("foregroundColor", a(this.b));
      }
      if (this.c != 0) {
        localJSONObject.put("backgroundColor", a(this.c));
      }
      switch (this.d)
      {
      case 0: 
        if (this.e != 0) {
          localJSONObject.put("edgeColor", a(this.e));
        }
        switch (this.f)
        {
        case 0: 
          label156:
          if (this.g != 0) {
            localJSONObject.put("windowColor", a(this.g));
          }
          if (this.f == 2) {
            localJSONObject.put("windowRoundedCornerRadius", this.h);
          }
          if (this.i != null) {
            localJSONObject.put("fontFamily", this.i);
          }
          switch (this.j)
          {
          case 0: 
            label260:
            switch (this.k)
            {
            }
            break;
          }
          break;
        }
        break;
      }
      for (;;)
      {
        if (this.l == null) {
          break label556;
        }
        localJSONObject.put("customData", this.l);
        return localJSONObject;
        localJSONObject.put("edgeType", "NONE");
        break;
        localJSONObject.put("edgeType", "OUTLINE");
        break;
        localJSONObject.put("edgeType", "DROP_SHADOW");
        break;
        localJSONObject.put("edgeType", "RAISED");
        break;
        localJSONObject.put("edgeType", "DEPRESSED");
        break;
        localJSONObject.put("windowType", "NONE");
        break label156;
        localJSONObject.put("windowType", "NORMAL");
        break label156;
        localJSONObject.put("windowType", "ROUNDED_CORNERS");
        break label156;
        localJSONObject.put("fontGenericFamily", "SANS_SERIF");
        break label260;
        localJSONObject.put("fontGenericFamily", "MONOSPACED_SANS_SERIF");
        break label260;
        localJSONObject.put("fontGenericFamily", "SERIF");
        break label260;
        localJSONObject.put("fontGenericFamily", "MONOSPACED_SERIF");
        break label260;
        localJSONObject.put("fontGenericFamily", "CASUAL");
        break label260;
        localJSONObject.put("fontGenericFamily", "CURSIVE");
        break label260;
        localJSONObject.put("fontGenericFamily", "SMALL_CAPITALS");
        break label260;
        localJSONObject.put("fontStyle", "NORMAL");
        continue;
        localJSONObject.put("fontStyle", "BOLD");
        continue;
        localJSONObject.put("fontStyle", "ITALIC");
        continue;
        localJSONObject.put("fontStyle", "BOLD_ITALIC");
        continue;
        break;
        break label156;
        break label260;
      }
      label556:
      return localJSONObject;
    }
    catch (JSONException localJSONException) {}
    return localJSONObject;
  }
  
  public void a(JSONObject paramJSONObject)
    throws JSONException
  {
    b();
    this.a = ((float)paramJSONObject.optDouble("fontScale", 1.0D));
    this.b = a(paramJSONObject.optString("foregroundColor"));
    this.c = a(paramJSONObject.optString("backgroundColor"));
    String str;
    if (paramJSONObject.has("edgeType"))
    {
      str = paramJSONObject.getString("edgeType");
      if ("NONE".equals(str)) {
        this.d = 0;
      }
    }
    else
    {
      this.e = a(paramJSONObject.optString("edgeColor"));
      if (paramJSONObject.has("windowType"))
      {
        str = paramJSONObject.getString("windowType");
        if (!"NONE".equals(str)) {
          break label301;
        }
        this.f = 0;
      }
      label118:
      this.g = a(paramJSONObject.optString("windowColor"));
      if (this.f == 2) {
        this.h = paramJSONObject.optInt("windowRoundedCornerRadius", 0);
      }
      this.i = paramJSONObject.optString("fontFamily", null);
      if (paramJSONObject.has("fontGenericFamily"))
      {
        str = paramJSONObject.getString("fontGenericFamily");
        if (!"SANS_SERIF".equals(str)) {
          break label335;
        }
        this.j = 0;
      }
      label192:
      if (paramJSONObject.has("fontStyle"))
      {
        str = paramJSONObject.getString("fontStyle");
        if (!"NORMAL".equals(str)) {
          break label438;
        }
        this.k = 0;
      }
    }
    for (;;)
    {
      this.l = paramJSONObject.optJSONObject("customData");
      return;
      if ("OUTLINE".equals(str))
      {
        this.d = 1;
        break;
      }
      if ("DROP_SHADOW".equals(str))
      {
        this.d = 2;
        break;
      }
      if ("RAISED".equals(str))
      {
        this.d = 3;
        break;
      }
      if (!"DEPRESSED".equals(str)) {
        break;
      }
      this.d = 4;
      break;
      label301:
      if ("NORMAL".equals(str))
      {
        this.f = 1;
        break label118;
      }
      if (!"ROUNDED_CORNERS".equals(str)) {
        break label118;
      }
      this.f = 2;
      break label118;
      label335:
      if ("MONOSPACED_SANS_SERIF".equals(str))
      {
        this.j = 1;
        break label192;
      }
      if ("SERIF".equals(str))
      {
        this.j = 2;
        break label192;
      }
      if ("MONOSPACED_SERIF".equals(str))
      {
        this.j = 3;
        break label192;
      }
      if ("CASUAL".equals(str))
      {
        this.j = 4;
        break label192;
      }
      if ("CURSIVE".equals(str))
      {
        this.j = 5;
        break label192;
      }
      if (!"SMALL_CAPITALS".equals(str)) {
        break label192;
      }
      this.j = 6;
      break label192;
      label438:
      if ("BOLD".equals(str)) {
        this.k = 1;
      } else if ("ITALIC".equals(str)) {
        this.k = 2;
      } else if ("BOLD_ITALIC".equals(str)) {
        this.k = 3;
      }
    }
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool3 = false;
    if (this == paramObject) {
      bool1 = true;
    }
    int m;
    int n;
    label51:
    do
    {
      do
      {
        do
        {
          return bool1;
          bool1 = bool3;
        } while (!(paramObject instanceof TextTrackStyle));
        paramObject = (TextTrackStyle)paramObject;
        if (this.l != null) {
          break;
        }
        m = 1;
        if (((TextTrackStyle)paramObject).l != null) {
          break label218;
        }
        n = 1;
        bool1 = bool3;
      } while (m != n);
      if ((this.l == null) || (((TextTrackStyle)paramObject).l == null)) {
        break;
      }
      bool1 = bool3;
    } while (!zznb.a(this.l, ((TextTrackStyle)paramObject).l));
    if ((this.a == ((TextTrackStyle)paramObject).a) && (this.b == ((TextTrackStyle)paramObject).b) && (this.c == ((TextTrackStyle)paramObject).c) && (this.d == ((TextTrackStyle)paramObject).d) && (this.e == ((TextTrackStyle)paramObject).e) && (this.f == ((TextTrackStyle)paramObject).f) && (this.h == ((TextTrackStyle)paramObject).h) && (zzf.a(this.i, ((TextTrackStyle)paramObject).i)) && (this.j == ((TextTrackStyle)paramObject).j) && (this.k == ((TextTrackStyle)paramObject).k)) {}
    for (boolean bool1 = bool2;; bool1 = false)
    {
      return bool1;
      m = 0;
      break;
      label218:
      n = 0;
      break label51;
    }
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Float.valueOf(this.a), Integer.valueOf(this.b), Integer.valueOf(this.c), Integer.valueOf(this.d), Integer.valueOf(this.e), Integer.valueOf(this.f), Integer.valueOf(this.g), Integer.valueOf(this.h), this.i, Integer.valueOf(this.j), Integer.valueOf(this.k), this.l });
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/TextTrackStyle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */