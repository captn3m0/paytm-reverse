package com.google.android.gms.cast;

import com.google.android.gms.cast.internal.zzf;
import java.util.Collection;
import java.util.Iterator;

public final class CastMediaControlIntent
{
  public static String a(String paramString)
    throws IllegalArgumentException
  {
    if (paramString == null) {
      throw new IllegalArgumentException("applicationId cannot be null");
    }
    return a("com.google.android.gms.cast.CATEGORY_CAST", paramString, null);
  }
  
  private static String a(String paramString1, String paramString2, Collection<String> paramCollection)
    throws IllegalArgumentException
  {
    paramString1 = new StringBuilder(paramString1);
    if (paramString2 != null)
    {
      String str = paramString2.toUpperCase();
      if (!str.matches("[A-F0-9]+")) {
        throw new IllegalArgumentException("Invalid application ID: " + paramString2);
      }
      paramString1.append("/").append(str);
    }
    if (paramCollection != null)
    {
      if (paramCollection.isEmpty()) {
        throw new IllegalArgumentException("Must specify at least one namespace");
      }
      if (paramString2 == null) {
        paramString1.append("/");
      }
      paramString1.append("/");
      paramString2 = paramCollection.iterator();
      int i = 1;
      if (paramString2.hasNext())
      {
        paramCollection = (String)paramString2.next();
        zzf.a(paramCollection);
        if (i != 0) {
          i = 0;
        }
        for (;;)
        {
          paramString1.append(zzf.c(paramCollection));
          break;
          paramString1.append(",");
        }
      }
    }
    return paramString1.toString();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/CastMediaControlIntent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */