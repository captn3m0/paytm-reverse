package com.google.android.gms.cast;

import android.content.Context;
import android.os.Looper;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.cast.internal.zzb;
import com.google.android.gms.cast.internal.zze;
import com.google.android.gms.cast.internal.zzh;
import com.google.android.gms.cast.internal.zzk;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.HasOptions;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzx;
import java.io.IOException;

public final class Cast
{
  public static final Api<CastOptions> a = new Api("Cast.API", c, zzk.a);
  public static final CastApi b = new Cast.CastApi.zza();
  private static final Api.zza<zze, CastOptions> c = new Api.zza()
  {
    public zze a(Context paramAnonymousContext, Looper paramAnonymousLooper, zzf paramAnonymouszzf, Cast.CastOptions paramAnonymousCastOptions, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      zzx.a(paramAnonymousCastOptions, "Setting the API options is required.");
      return new zze(paramAnonymousContext, paramAnonymousLooper, paramAnonymouszzf, paramAnonymousCastOptions.a, Cast.CastOptions.a(paramAnonymousCastOptions), paramAnonymousCastOptions.b, paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
    }
  };
  
  public static abstract interface ApplicationConnectionResult
    extends Result
  {}
  
  public static abstract interface CastApi
  {
    public abstract PendingResult<Status> a(GoogleApiClient paramGoogleApiClient, String paramString1, String paramString2);
    
    public abstract void a(GoogleApiClient paramGoogleApiClient, String paramString, Cast.MessageReceivedCallback paramMessageReceivedCallback)
      throws IOException, IllegalStateException;
    
    public static final class zza
      implements Cast.CastApi
    {
      public PendingResult<Status> a(GoogleApiClient paramGoogleApiClient, final String paramString1, final String paramString2)
      {
        paramGoogleApiClient.b(new zzh(paramGoogleApiClient)
        {
          protected void a(zze paramAnonymouszze)
            throws RemoteException
          {
            try
            {
              paramAnonymouszze.a(paramString1, paramString2, this);
              return;
            }
            catch (IllegalStateException paramAnonymouszze)
            {
              a(2001);
              return;
            }
            catch (IllegalArgumentException paramAnonymouszze)
            {
              for (;;) {}
            }
          }
        });
      }
      
      public void a(GoogleApiClient paramGoogleApiClient, String paramString, Cast.MessageReceivedCallback paramMessageReceivedCallback)
        throws IOException, IllegalStateException
      {
        try
        {
          ((zze)paramGoogleApiClient.a(zzk.a)).a(paramString, paramMessageReceivedCallback);
          return;
        }
        catch (RemoteException paramGoogleApiClient)
        {
          throw new IOException("service error");
        }
      }
    }
  }
  
  public static final class CastOptions
    implements Api.ApiOptions.HasOptions
  {
    final CastDevice a;
    final Cast.Listener b;
    private final int c;
    
    public static final class Builder {}
  }
  
  public static class Listener
  {
    public void a() {}
    
    public void a(int paramInt) {}
    
    public void a(ApplicationMetadata paramApplicationMetadata) {}
    
    public void b() {}
    
    public void b(int paramInt) {}
    
    public void c(int paramInt) {}
  }
  
  public static abstract interface MessageReceivedCallback
  {
    public abstract void a(CastDevice paramCastDevice, String paramString1, String paramString2);
  }
  
  private static abstract class zza
    extends zzb<Cast.ApplicationConnectionResult>
  {
    public Cast.ApplicationConnectionResult a(final Status paramStatus)
    {
      new Cast.ApplicationConnectionResult()
      {
        public Status getStatus()
        {
          return paramStatus;
        }
      };
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/Cast.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */