package com.google.android.gms.cast.games;

import java.util.Collection;
import org.json.JSONObject;

public abstract interface GameManagerState
{
  public abstract int a();
  
  public abstract PlayerInfo a(String paramString);
  
  public abstract int b();
  
  public abstract JSONObject c();
  
  public abstract CharSequence d();
  
  public abstract CharSequence e();
  
  public abstract int f();
  
  public abstract Collection<PlayerInfo> g();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/games/GameManagerState.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */