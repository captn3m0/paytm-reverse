package com.google.android.gms.cast.games;

import com.google.android.gms.common.api.Result;
import org.json.JSONObject;

public final class GameManagerClient
{
  public static abstract interface GameManagerInstanceResult
    extends Result
  {}
  
  public static abstract interface GameManagerResult
    extends Result
  {}
  
  public static abstract interface Listener
  {
    public abstract void a(GameManagerState paramGameManagerState1, GameManagerState paramGameManagerState2);
    
    public abstract void a(String paramString, JSONObject paramJSONObject);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/games/GameManagerClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */