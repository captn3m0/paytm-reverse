package com.google.android.gms.cast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;

public abstract interface CastRemoteDisplayApi
{
  public abstract PendingResult<CastRemoteDisplay.CastRemoteDisplaySessionResult> a(GoogleApiClient paramGoogleApiClient);
  
  public abstract PendingResult<CastRemoteDisplay.CastRemoteDisplaySessionResult> a(GoogleApiClient paramGoogleApiClient, String paramString);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/cast/CastRemoteDisplayApi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */