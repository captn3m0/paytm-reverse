package com.google.android.gms.measurement;

import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzmq;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class zzf<T extends zzf>
{
  protected final zzc a;
  private final zzg b;
  private final List<zzd> c;
  
  protected zzf(zzg paramzzg, zzmq paramzzmq)
  {
    zzx.a(paramzzg);
    this.b = paramzzg;
    this.c = new ArrayList();
    paramzzg = new zzc(this, paramzzmq);
    paramzzg.k();
    this.a = paramzzg;
  }
  
  protected void a(zzc paramzzc) {}
  
  protected void b(zzc paramzzc)
  {
    Iterator localIterator = this.c.iterator();
    while (localIterator.hasNext()) {
      ((zzd)localIterator.next()).a(this, paramzzc);
    }
  }
  
  public zzc l()
  {
    zzc localzzc = this.a.a();
    b(localzzc);
    return localzzc;
  }
  
  public zzc m()
  {
    return this.a;
  }
  
  public List<zzi> n()
  {
    return this.a.c();
  }
  
  protected zzg o()
  {
    return this.b;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */