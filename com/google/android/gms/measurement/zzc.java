package com.google.android.gms.measurement;

import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzmq;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class zzc
{
  private final zzf a;
  private final zzmq b;
  private boolean c;
  private long d;
  private long e;
  private long f;
  private long g;
  private long h;
  private boolean i;
  private final Map<Class<? extends zze>, zze> j;
  private final List<zzi> k;
  
  zzc(zzc paramzzc)
  {
    this.a = paramzzc.a;
    this.b = paramzzc.b;
    this.d = paramzzc.d;
    this.e = paramzzc.e;
    this.f = paramzzc.f;
    this.g = paramzzc.g;
    this.h = paramzzc.h;
    this.k = new ArrayList(paramzzc.k);
    this.j = new HashMap(paramzzc.j.size());
    paramzzc = paramzzc.j.entrySet().iterator();
    while (paramzzc.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)paramzzc.next();
      zze localzze = c((Class)localEntry.getKey());
      ((zze)localEntry.getValue()).a(localzze);
      this.j.put(localEntry.getKey(), localzze);
    }
  }
  
  zzc(zzf paramzzf, zzmq paramzzmq)
  {
    zzx.a(paramzzf);
    zzx.a(paramzzmq);
    this.a = paramzzf;
    this.b = paramzzmq;
    this.g = 1800000L;
    this.h = 3024000000L;
    this.j = new HashMap();
    this.k = new ArrayList();
  }
  
  private static <T extends zze> T c(Class<T> paramClass)
  {
    try
    {
      paramClass = (zze)paramClass.newInstance();
      return paramClass;
    }
    catch (InstantiationException paramClass)
    {
      throw new IllegalArgumentException("dataType doesn't have default constructor", paramClass);
    }
    catch (IllegalAccessException paramClass)
    {
      throw new IllegalArgumentException("dataType default constructor is not accessible", paramClass);
    }
  }
  
  public zzc a()
  {
    return new zzc(this);
  }
  
  public <T extends zze> T a(Class<T> paramClass)
  {
    return (zze)this.j.get(paramClass);
  }
  
  public void a(long paramLong)
  {
    this.e = paramLong;
  }
  
  public void a(zze paramzze)
  {
    zzx.a(paramzze);
    Class localClass = paramzze.getClass();
    if (localClass.getSuperclass() != zze.class) {
      throw new IllegalArgumentException();
    }
    paramzze.a(b(localClass));
  }
  
  public <T extends zze> T b(Class<T> paramClass)
  {
    zze localzze2 = (zze)this.j.get(paramClass);
    zze localzze1 = localzze2;
    if (localzze2 == null)
    {
      localzze1 = c(paramClass);
      this.j.put(paramClass, localzze1);
    }
    return localzze1;
  }
  
  public Collection<zze> b()
  {
    return this.j.values();
  }
  
  public List<zzi> c()
  {
    return this.k;
  }
  
  public long d()
  {
    return this.d;
  }
  
  public void e()
  {
    i().a(this);
  }
  
  public boolean f()
  {
    return this.c;
  }
  
  void g()
  {
    this.f = this.b.b();
    if (this.e != 0L) {}
    for (this.d = this.e;; this.d = this.b.a())
    {
      this.c = true;
      return;
    }
  }
  
  zzf h()
  {
    return this.a;
  }
  
  zzg i()
  {
    return this.a.o();
  }
  
  boolean j()
  {
    return this.i;
  }
  
  void k()
  {
    this.i = true;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */