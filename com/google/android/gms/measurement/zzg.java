package com.google.android.gms.measurement;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Process;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import com.google.android.gms.analytics.internal.zzam;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzpq;
import com.google.android.gms.internal.zzps;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public final class zzg
{
  private static volatile zzg a;
  private final Context b;
  private final List<zzh> c;
  private final zzb d;
  private final zza e;
  private volatile zzpq f;
  private Thread.UncaughtExceptionHandler g;
  
  zzg(Context paramContext)
  {
    paramContext = paramContext.getApplicationContext();
    zzx.a(paramContext);
    this.b = paramContext;
    this.e = new zza();
    this.c = new CopyOnWriteArrayList();
    this.d = new zzb();
  }
  
  public static zzg a(Context paramContext)
  {
    zzx.a(paramContext);
    if (a == null) {}
    try
    {
      if (a == null) {
        a = new zzg(paramContext);
      }
      return a;
    }
    finally {}
  }
  
  private void b(zzc paramzzc)
  {
    zzx.c("deliver should be called from worker thread");
    zzx.b(paramzzc.f(), "Measurement must be submitted");
    Object localObject = paramzzc.c();
    if (((List)localObject).isEmpty()) {}
    for (;;)
    {
      return;
      HashSet localHashSet = new HashSet();
      localObject = ((List)localObject).iterator();
      while (((Iterator)localObject).hasNext())
      {
        zzi localzzi = (zzi)((Iterator)localObject).next();
        Uri localUri = localzzi.a();
        if (!localHashSet.contains(localUri))
        {
          localHashSet.add(localUri);
          localzzi.a(paramzzc);
        }
      }
    }
  }
  
  public static void d()
  {
    if (!(Thread.currentThread() instanceof zzc)) {
      throw new IllegalStateException("Call expected from worker thread");
    }
  }
  
  public zzpq a()
  {
    if (this.f == null) {}
    Object localObject5;
    Object localObject3;
    try
    {
      zzpq localzzpq;
      PackageManager localPackageManager;
      String str2;
      if (this.f == null)
      {
        localzzpq = new zzpq();
        localPackageManager = this.b.getPackageManager();
        str2 = this.b.getPackageName();
        localzzpq.c(str2);
        localzzpq.d(localPackageManager.getInstallerPackageName(str2));
        localObject5 = null;
        localObject3 = str2;
      }
      try
      {
        PackageInfo localPackageInfo = localPackageManager.getPackageInfo(this.b.getPackageName(), 0);
        localObject4 = localObject5;
        str1 = str2;
        if (localPackageInfo != null)
        {
          localObject3 = str2;
          localObject4 = localPackageManager.getApplicationLabel(localPackageInfo.applicationInfo);
          str1 = str2;
          localObject3 = str2;
          if (!TextUtils.isEmpty((CharSequence)localObject4))
          {
            localObject3 = str2;
            str1 = ((CharSequence)localObject4).toString();
          }
          localObject3 = str1;
          localObject4 = localPackageInfo.versionName;
        }
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException)
      {
        for (;;)
        {
          String str1;
          Log.e("GAv4", "Error retrieving package info: appName set to " + (String)localObject3);
          Object localObject4 = localObject5;
          Object localObject1 = localObject3;
        }
      }
      localzzpq.a(str1);
      localzzpq.b((String)localObject4);
      this.f = localzzpq;
      return this.f;
    }
    finally {}
  }
  
  public <V> Future<V> a(Callable<V> paramCallable)
  {
    zzx.a(paramCallable);
    if ((Thread.currentThread() instanceof zzc))
    {
      paramCallable = new FutureTask(paramCallable);
      paramCallable.run();
      return paramCallable;
    }
    return this.e.submit(paramCallable);
  }
  
  void a(final zzc paramzzc)
  {
    if (paramzzc.j()) {
      throw new IllegalStateException("Measurement prototype can't be submitted");
    }
    if (paramzzc.f()) {
      throw new IllegalStateException("Measurement can only be submitted once");
    }
    paramzzc = paramzzc.a();
    paramzzc.g();
    this.e.execute(new Runnable()
    {
      public void run()
      {
        paramzzc.h().a(paramzzc);
        Iterator localIterator = zzg.a(zzg.this).iterator();
        while (localIterator.hasNext()) {
          ((zzh)localIterator.next()).a(paramzzc);
        }
        zzg.a(zzg.this, paramzzc);
      }
    });
  }
  
  public void a(Runnable paramRunnable)
  {
    zzx.a(paramRunnable);
    this.e.submit(paramRunnable);
  }
  
  public void a(Thread.UncaughtExceptionHandler paramUncaughtExceptionHandler)
  {
    this.g = paramUncaughtExceptionHandler;
  }
  
  public zzps b()
  {
    DisplayMetrics localDisplayMetrics = this.b.getResources().getDisplayMetrics();
    zzps localzzps = new zzps();
    localzzps.a(zzam.a(Locale.getDefault()));
    localzzps.b(localDisplayMetrics.widthPixels);
    localzzps.c(localDisplayMetrics.heightPixels);
    return localzzps;
  }
  
  public Context c()
  {
    return this.b;
  }
  
  private class zza
    extends ThreadPoolExecutor
  {
    public zza()
    {
      super(1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue());
      setThreadFactory(new zzg.zzb(null));
    }
    
    protected <T> RunnableFuture<T> newTaskFor(Runnable paramRunnable, T paramT)
    {
      new FutureTask(paramRunnable, paramT)
      {
        protected void setException(Throwable paramAnonymousThrowable)
        {
          Thread.UncaughtExceptionHandler localUncaughtExceptionHandler = zzg.b(zzg.this);
          if (localUncaughtExceptionHandler != null) {
            localUncaughtExceptionHandler.uncaughtException(Thread.currentThread(), paramAnonymousThrowable);
          }
          for (;;)
          {
            super.setException(paramAnonymousThrowable);
            return;
            if (Log.isLoggable("GAv4", 6)) {
              Log.e("GAv4", "MeasurementExecutor: job failed with " + paramAnonymousThrowable);
            }
          }
        }
      };
    }
  }
  
  private static class zzb
    implements ThreadFactory
  {
    private static final AtomicInteger a = new AtomicInteger();
    
    public Thread newThread(Runnable paramRunnable)
    {
      return new zzg.zzc(paramRunnable, "measurement-" + a.incrementAndGet());
    }
  }
  
  private static class zzc
    extends Thread
  {
    zzc(Runnable paramRunnable, String paramString)
    {
      super(paramString);
    }
    
    public void run()
    {
      Process.setThreadPriority(10);
      super.run();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */