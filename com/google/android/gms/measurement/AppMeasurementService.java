package com.google.android.gms.measurement;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.MainThread;
import com.google.android.gms.internal.zzrp;
import com.google.android.gms.measurement.internal.zzaj;
import com.google.android.gms.measurement.internal.zzd;
import com.google.android.gms.measurement.internal.zzp;
import com.google.android.gms.measurement.internal.zzp.zza;
import com.google.android.gms.measurement.internal.zzv;
import com.google.android.gms.measurement.internal.zzw;

public final class AppMeasurementService
  extends Service
{
  private static Boolean b;
  private final Handler a = new Handler();
  
  private void a()
  {
    try
    {
      synchronized (AppMeasurementReceiver.a)
      {
        zzrp localzzrp = AppMeasurementReceiver.b;
        if ((localzzrp != null) && (localzzrp.b())) {
          localzzrp.a();
        }
        return;
      }
      return;
    }
    catch (SecurityException localSecurityException) {}
  }
  
  public static boolean a(Context paramContext)
  {
    com.google.android.gms.common.internal.zzx.a(paramContext);
    if (b != null) {
      return b.booleanValue();
    }
    boolean bool = zzaj.a(paramContext, AppMeasurementService.class);
    b = Boolean.valueOf(bool);
    return bool;
  }
  
  private zzp b()
  {
    return zzw.a(this).f();
  }
  
  @MainThread
  public IBinder onBind(Intent paramIntent)
  {
    if (paramIntent == null)
    {
      b().b().a("onBind called with null intent");
      return null;
    }
    paramIntent = paramIntent.getAction();
    if ("com.google.android.gms.measurement.START".equals(paramIntent)) {
      return new com.google.android.gms.measurement.internal.zzx(zzw.a(this));
    }
    b().c().a("onBind received unknown action", paramIntent);
    return null;
  }
  
  @MainThread
  public void onCreate()
  {
    super.onCreate();
    zzw localzzw = zzw.a(this);
    zzp localzzp = localzzw.f();
    if (localzzw.d().N())
    {
      localzzp.z().a("Device AppMeasurementService is starting up");
      return;
    }
    localzzp.z().a("Local AppMeasurementService is starting up");
  }
  
  @MainThread
  public void onDestroy()
  {
    zzw localzzw = zzw.a(this);
    zzp localzzp = localzzw.f();
    if (localzzw.d().N()) {
      localzzp.z().a("Device AppMeasurementService is shutting down");
    }
    for (;;)
    {
      super.onDestroy();
      return;
      localzzp.z().a("Local AppMeasurementService is shutting down");
    }
  }
  
  @MainThread
  public void onRebind(Intent paramIntent)
  {
    if (paramIntent == null)
    {
      b().b().a("onRebind called with null intent");
      return;
    }
    paramIntent = paramIntent.getAction();
    b().z().a("onRebind called. action", paramIntent);
  }
  
  @MainThread
  public int onStartCommand(Intent paramIntent, int paramInt1, final int paramInt2)
  {
    a();
    final zzw localzzw = zzw.a(this);
    final zzp localzzp = localzzw.f();
    paramIntent = paramIntent.getAction();
    if (localzzw.d().N()) {
      localzzp.z().a("Device AppMeasurementService called. startId, action", Integer.valueOf(paramInt2), paramIntent);
    }
    for (;;)
    {
      if ("com.google.android.gms.measurement.UPLOAD".equals(paramIntent)) {
        localzzw.h().a(new Runnable()
        {
          public void run()
          {
            localzzw.C();
            AppMeasurementService.a(AppMeasurementService.this).post(new Runnable()
            {
              public void run()
              {
                if (AppMeasurementService.this.stopSelfResult(AppMeasurementService.1.this.b))
                {
                  if (AppMeasurementService.1.this.a.d().N()) {
                    AppMeasurementService.1.this.c.z().a("Device AppMeasurementService processed last upload request");
                  }
                }
                else {
                  return;
                }
                AppMeasurementService.1.this.c.z().a("Local AppMeasurementService processed last upload request");
              }
            });
          }
        });
      }
      return 2;
      localzzp.z().a("Local AppMeasurementService called. startId, action", Integer.valueOf(paramInt2), paramIntent);
    }
  }
  
  @MainThread
  public boolean onUnbind(Intent paramIntent)
  {
    if (paramIntent == null)
    {
      b().b().a("onUnbind called with null intent");
      return true;
    }
    paramIntent = paramIntent.getAction();
    b().z().a("onUnbind called for intent. action", paramIntent);
    return true;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/AppMeasurementService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */