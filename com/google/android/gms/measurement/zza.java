package com.google.android.gms.measurement;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import com.google.android.gms.R.string;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzx;

public final class zza
{
  private static volatile zza e;
  private final String a;
  private final Status b;
  private final boolean c;
  private final boolean d;
  
  zza(Context paramContext)
  {
    paramContext = paramContext.getResources();
    String str = paramContext.getResourcePackageName(R.string.common_google_play_services_unknown_issue);
    int i = paramContext.getIdentifier("google_app_measurement_enable", "integer", str);
    if (i != 0) {
      if (paramContext.getInteger(i) != 0)
      {
        bool1 = true;
        if (bool1) {
          break label111;
        }
        label52:
        this.d = bool2;
        label58:
        this.c = bool1;
        i = paramContext.getIdentifier("google_app_id", "string", str);
        if (i != 0) {
          break label135;
        }
        if (!this.c) {
          break label125;
        }
      }
    }
    label111:
    label125:
    for (this.b = new Status(10, "Missing an expected resource: 'R.string.google_app_id' for initializing Google services.  Possible causes are missing google-services.json or com.google.gms.google-services gradle plugin.");; this.b = Status.a)
    {
      this.a = null;
      return;
      bool1 = false;
      break;
      bool2 = false;
      break label52;
      this.d = false;
      break label58;
    }
    label135:
    paramContext = paramContext.getString(i);
    if (TextUtils.isEmpty(paramContext))
    {
      if (this.c) {}
      for (this.b = new Status(10, "The resource 'R.string.google_app_id' is invalid, expected an app  identifier and found: '" + paramContext + "'.");; this.b = Status.a)
      {
        this.a = null;
        return;
      }
    }
    this.a = paramContext;
    this.b = Status.a;
  }
  
  zza(Context paramContext, String paramString, boolean paramBoolean)
  {
    this.a = paramString;
    this.b = Status.a;
    this.c = paramBoolean;
    if (!paramBoolean) {}
    for (paramBoolean = true;; paramBoolean = false)
    {
      this.d = paramBoolean;
      return;
    }
  }
  
  public static Status a(Context paramContext)
  {
    zzx.a(paramContext, "Context must not be null.");
    if (e == null) {}
    try
    {
      if (e == null) {
        e = new zza(paramContext);
      }
      return e.b;
    }
    finally {}
  }
  
  public static Status a(Context paramContext, String paramString, boolean paramBoolean)
  {
    zzx.a(paramContext, "Context must not be null.");
    zzx.a(paramString, "App ID must be nonempty.");
    try
    {
      if (e != null)
      {
        paramContext = e.a(paramString);
        return paramContext;
      }
      e = new zza(paramContext, paramString, paramBoolean);
      return e.b;
    }
    finally {}
  }
  
  public static String a()
  {
    if (e == null) {
      try
      {
        if (e == null) {
          throw new IllegalStateException("Initialize must be called before getGoogleAppId.");
        }
      }
      finally {}
    }
    return e.b();
  }
  
  public static boolean c()
  {
    if (e == null) {
      try
      {
        if (e == null) {
          throw new IllegalStateException("Initialize must be called before isMeasurementEnabled.");
        }
      }
      finally {}
    }
    return e.e();
  }
  
  public static boolean d()
  {
    if (e == null) {
      try
      {
        if (e == null) {
          throw new IllegalStateException("Initialize must be called before isMeasurementExplicitlyDisabled.");
        }
      }
      finally {}
    }
    return e.d;
  }
  
  Status a(String paramString)
  {
    if ((this.a != null) && (!this.a.equals(paramString))) {
      return new Status(10, "Initialize was called with two different Google App IDs.  Only the first app ID will be used: '" + this.a + "'.");
    }
    return Status.a;
  }
  
  String b()
  {
    return this.a;
  }
  
  boolean e()
  {
    return (this.b.d()) && (this.c);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */