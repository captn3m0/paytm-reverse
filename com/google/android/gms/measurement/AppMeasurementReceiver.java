package com.google.android.gms.measurement;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.MainThread;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzrp;
import com.google.android.gms.measurement.internal.zzaj;
import com.google.android.gms.measurement.internal.zzd;
import com.google.android.gms.measurement.internal.zzp;
import com.google.android.gms.measurement.internal.zzp.zza;
import com.google.android.gms.measurement.internal.zzw;

public final class AppMeasurementReceiver
  extends BroadcastReceiver
{
  static final Object a = new Object();
  static zzrp b;
  static Boolean c;
  
  public static boolean a(Context paramContext)
  {
    zzx.a(paramContext);
    if (c != null) {
      return c.booleanValue();
    }
    boolean bool = zzaj.a(paramContext, AppMeasurementReceiver.class, false);
    c = Boolean.valueOf(bool);
    return bool;
  }
  
  @MainThread
  public void onReceive(Context paramContext, Intent arg2)
  {
    Object localObject = zzw.a(paramContext);
    localzzp = ((zzw)localObject).f();
    ??? = ???.getAction();
    if (((zzw)localObject).d().N()) {
      localzzp.z().a("Device AppMeasurementReceiver got", ???);
    }
    for (;;)
    {
      boolean bool;
      if ("com.google.android.gms.measurement.UPLOAD".equals(???))
      {
        bool = AppMeasurementService.a(paramContext);
        localObject = new Intent(paramContext, AppMeasurementService.class);
        ((Intent)localObject).setAction("com.google.android.gms.measurement.UPLOAD");
      }
      synchronized (a)
      {
        paramContext.startService((Intent)localObject);
        if (!bool)
        {
          return;
          localzzp.z().a("Local AppMeasurementReceiver got", ???);
          continue;
        }
        try
        {
          if (b == null)
          {
            b = new zzrp(paramContext, 1, "AppMeasurement WakeLock");
            b.a(false);
          }
          b.a(1000L);
        }
        catch (SecurityException paramContext)
        {
          for (;;)
          {
            localzzp.c().a("AppMeasurementService at risk of not starting. For more reliable app measurements, add the WAKE_LOCK permission to your manifest.");
          }
        }
        return;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/AppMeasurementReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */