package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzmq;
import com.google.android.gms.measurement.zza;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Locale;

class zzt
  extends zzz
{
  static final Pair<String, Long> a = new Pair("", Long.valueOf(0L));
  public final zzc b = new zzc("health_monitor", u().S(), null);
  public final zzb c = new zzb("last_upload", 0L);
  public final zzb d = new zzb("last_upload_attempt", 0L);
  public final zzb e = new zzb("backoff", 0L);
  public final zzb f = new zzb("last_delete_stale", 0L);
  public final zzb g = new zzb("midnight_offset", 0L);
  public final zzb h = new zzb("time_before_start", 10000L);
  public final zzb i = new zzb("session_timeout", 1800000L);
  public final zza j = new zza("start_new_session", true);
  public final zzb k = new zzb("last_pause_time", 0L);
  public final zzb l = new zzb("time_active", 0L);
  public boolean m;
  private SharedPreferences o;
  private String p;
  private boolean q;
  private long r;
  private final SecureRandom s = new SecureRandom();
  
  zzt(zzw paramzzw)
  {
    super(paramzzw);
  }
  
  @WorkerThread
  private SharedPreferences y()
  {
    f();
    G();
    return this.o;
  }
  
  @WorkerThread
  Pair<String, Boolean> a(String paramString)
  {
    f();
    long l1 = l().b();
    if ((this.p != null) && (l1 < this.r)) {
      return new Pair(this.p, Boolean.valueOf(this.q));
    }
    this.r = (l1 + u().a(paramString));
    AdvertisingIdClient.b(true);
    try
    {
      paramString = AdvertisingIdClient.b(m());
      this.p = paramString.a();
      this.q = paramString.b();
      AdvertisingIdClient.b(false);
      return new Pair(this.p, Boolean.valueOf(this.q));
    }
    catch (Throwable paramString)
    {
      for (;;)
      {
        s().y().a("Unable to get advertising id", paramString);
        this.p = "";
      }
    }
  }
  
  protected void a()
  {
    this.o = m().getSharedPreferences("com.google.android.gms.measurement.prefs", 0);
    this.m = this.o.getBoolean("has_been_opened", false);
    if (!this.m)
    {
      SharedPreferences.Editor localEditor = this.o.edit();
      localEditor.putBoolean("has_been_opened", true);
      localEditor.apply();
    }
  }
  
  @WorkerThread
  void a(boolean paramBoolean)
  {
    f();
    s().z().a("Setting useService", Boolean.valueOf(paramBoolean));
    SharedPreferences.Editor localEditor = y().edit();
    localEditor.putBoolean("use_service", paramBoolean);
    localEditor.apply();
  }
  
  String b()
  {
    byte[] arrayOfByte = new byte[16];
    this.s.nextBytes(arrayOfByte);
    return String.format(Locale.US, "%032x", new Object[] { new BigInteger(1, arrayOfByte) });
  }
  
  String b(String paramString)
  {
    paramString = (String)a(paramString).first;
    MessageDigest localMessageDigest = zzaj.e("MD5");
    if (localMessageDigest == null) {
      return null;
    }
    return String.format(Locale.US, "%032X", new Object[] { new BigInteger(1, localMessageDigest.digest(paramString.getBytes())) });
  }
  
  @WorkerThread
  void b(boolean paramBoolean)
  {
    f();
    s().z().a("Setting measurementEnabled", Boolean.valueOf(paramBoolean));
    SharedPreferences.Editor localEditor = y().edit();
    localEditor.putBoolean("measurement_enabled", paramBoolean);
    localEditor.apply();
  }
  
  @WorkerThread
  long c()
  {
    G();
    f();
    long l2 = this.g.a();
    long l1 = l2;
    if (l2 == 0L)
    {
      l1 = this.s.nextInt(86400000) + 1;
      this.g.a(l1);
    }
    return l1;
  }
  
  @WorkerThread
  Boolean v()
  {
    f();
    if (!y().contains("use_service")) {
      return null;
    }
    return Boolean.valueOf(y().getBoolean("use_service", false));
  }
  
  @WorkerThread
  boolean w()
  {
    f();
    SharedPreferences localSharedPreferences = y();
    if (!zza.d()) {}
    for (boolean bool = true;; bool = false) {
      return localSharedPreferences.getBoolean("measurement_enabled", bool);
    }
  }
  
  @WorkerThread
  protected String x()
  {
    f();
    String str1 = y().getString("previous_os_version", null);
    String str2 = j().c();
    if ((!TextUtils.isEmpty(str2)) && (!str2.equals(str1)))
    {
      SharedPreferences.Editor localEditor = y().edit();
      localEditor.putString("previous_os_version", str2);
      localEditor.apply();
    }
    return str1;
  }
  
  public final class zza
  {
    private final String b;
    private final boolean c;
    private boolean d;
    private boolean e;
    
    public zza(String paramString, boolean paramBoolean)
    {
      zzx.a(paramString);
      this.b = paramString;
      this.c = paramBoolean;
    }
    
    @WorkerThread
    private void b()
    {
      if (this.d) {
        return;
      }
      this.d = true;
      this.e = zzt.a(zzt.this).getBoolean(this.b, this.c);
    }
    
    @WorkerThread
    public void a(boolean paramBoolean)
    {
      SharedPreferences.Editor localEditor = zzt.a(zzt.this).edit();
      localEditor.putBoolean(this.b, paramBoolean);
      localEditor.apply();
      this.e = paramBoolean;
    }
    
    @WorkerThread
    public boolean a()
    {
      b();
      return this.e;
    }
  }
  
  public final class zzb
  {
    private final String b;
    private final long c;
    private boolean d;
    private long e;
    
    public zzb(String paramString, long paramLong)
    {
      zzx.a(paramString);
      this.b = paramString;
      this.c = paramLong;
    }
    
    @WorkerThread
    private void b()
    {
      if (this.d) {
        return;
      }
      this.d = true;
      this.e = zzt.a(zzt.this).getLong(this.b, this.c);
    }
    
    @WorkerThread
    public long a()
    {
      b();
      return this.e;
    }
    
    @WorkerThread
    public void a(long paramLong)
    {
      SharedPreferences.Editor localEditor = zzt.a(zzt.this).edit();
      localEditor.putLong(this.b, paramLong);
      localEditor.apply();
      this.e = paramLong;
    }
  }
  
  public final class zzc
  {
    final String a;
    private final String c;
    private final String d;
    private final long e;
    
    private zzc(String paramString, long paramLong)
    {
      zzx.a(paramString);
      if (paramLong > 0L) {}
      for (boolean bool = true;; bool = false)
      {
        zzx.b(bool);
        this.a = (paramString + ":start");
        this.c = (paramString + ":count");
        this.d = (paramString + ":value");
        this.e = paramLong;
        return;
      }
    }
    
    @WorkerThread
    private void b()
    {
      zzt.this.f();
      long l = zzt.this.l().a();
      SharedPreferences.Editor localEditor = zzt.a(zzt.this).edit();
      localEditor.remove(this.c);
      localEditor.remove(this.d);
      localEditor.putLong(this.a, l);
      localEditor.apply();
    }
    
    @WorkerThread
    private long c()
    {
      zzt.this.f();
      long l = d();
      if (l == 0L)
      {
        b();
        return 0L;
      }
      return Math.abs(l - zzt.this.l().a());
    }
    
    @WorkerThread
    private long d()
    {
      return zzt.c(zzt.this).getLong(this.a, 0L);
    }
    
    @WorkerThread
    public Pair<String, Long> a()
    {
      zzt.this.f();
      long l = c();
      if (l < this.e) {
        return null;
      }
      if (l > this.e * 2L)
      {
        b();
        return null;
      }
      String str = zzt.c(zzt.this).getString(this.d, null);
      l = zzt.c(zzt.this).getLong(this.c, 0L);
      b();
      if ((str == null) || (l <= 0L)) {
        return zzt.a;
      }
      return new Pair(str, Long.valueOf(l));
    }
    
    @WorkerThread
    public void a(String paramString)
    {
      a(paramString, 1L);
    }
    
    @WorkerThread
    public void a(String paramString, long paramLong)
    {
      zzt.this.f();
      if (d() == 0L) {
        b();
      }
      String str = paramString;
      if (paramString == null) {
        str = "";
      }
      long l = zzt.a(zzt.this).getLong(this.c, 0L);
      if (l <= 0L)
      {
        paramString = zzt.a(zzt.this).edit();
        paramString.putString(this.d, str);
        paramString.putLong(this.c, paramLong);
        paramString.apply();
        return;
      }
      if ((zzt.b(zzt.this).nextLong() & 0x7FFFFFFFFFFFFFFF) < Long.MAX_VALUE / (l + paramLong) * paramLong) {}
      for (int i = 1;; i = 0)
      {
        paramString = zzt.a(zzt.this).edit();
        if (i != 0) {
          paramString.putString(this.d, str);
        }
        paramString.putLong(this.c, l + paramLong);
        paramString.apply();
        return;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */