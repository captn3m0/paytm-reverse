package com.google.android.gms.measurement.internal;

import android.os.Binder;
import android.os.Process;
import android.support.annotation.BinderThread;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import java.util.List;
import java.util.concurrent.Callable;

public class zzx
  extends zzm.zza
{
  private final zzw a;
  private final boolean b;
  
  public zzx(zzw paramzzw)
  {
    com.google.android.gms.common.internal.zzx.a(paramzzw);
    this.a = paramzzw;
    this.b = false;
  }
  
  public zzx(zzw paramzzw, boolean paramBoolean)
  {
    com.google.android.gms.common.internal.zzx.a(paramzzw);
    this.a = paramzzw;
    this.b = paramBoolean;
  }
  
  @BinderThread
  private void b(String paramString)
    throws SecurityException
  {
    if (TextUtils.isEmpty(paramString))
    {
      this.a.f().b().a("Measurement Service called without app package");
      throw new SecurityException("Measurement Service called without app package");
    }
    try
    {
      c(paramString);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      this.a.f().b().a("Measurement Service called with invalid calling package", paramString);
      throw localSecurityException;
    }
  }
  
  private void c(String paramString)
    throws SecurityException
  {
    int i;
    if (this.b)
    {
      i = Process.myUid();
      if (!com.google.android.gms.common.zze.a(this.a.q(), i, paramString)) {
        break label34;
      }
    }
    label34:
    while ((com.google.android.gms.common.zze.b(this.a.q(), i)) && (!this.a.z()))
    {
      return;
      i = Binder.getCallingUid();
      break;
    }
    throw new SecurityException(String.format("Unknown calling package name '%s'.", new Object[] { paramString }));
  }
  
  /* Error */
  @BinderThread
  public List<UserAttributeParcel> a(final AppMetadata paramAppMetadata, boolean paramBoolean)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic 32	com/google/android/gms/common/internal/zzx:a	(Ljava/lang/Object;)Ljava/lang/Object;
    //   4: pop
    //   5: aload_0
    //   6: aload_1
    //   7: getfield 125	com/google/android/gms/measurement/internal/AppMetadata:b	Ljava/lang/String;
    //   10: invokespecial 127	com/google/android/gms/measurement/internal/zzx:b	(Ljava/lang/String;)V
    //   13: aload_0
    //   14: getfield 34	com/google/android/gms/measurement/internal/zzx:a	Lcom/google/android/gms/measurement/internal/zzw;
    //   17: invokevirtual 131	com/google/android/gms/measurement/internal/zzw:h	()Lcom/google/android/gms/measurement/internal/zzv;
    //   20: new 16	com/google/android/gms/measurement/internal/zzx$6
    //   23: dup
    //   24: aload_0
    //   25: aload_1
    //   26: invokespecial 134	com/google/android/gms/measurement/internal/zzx$6:<init>	(Lcom/google/android/gms/measurement/internal/zzx;Lcom/google/android/gms/measurement/internal/AppMetadata;)V
    //   29: invokevirtual 139	com/google/android/gms/measurement/internal/zzv:a	(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    //   32: astore_1
    //   33: aload_1
    //   34: invokeinterface 145 1 0
    //   39: checkcast 147	java/util/List
    //   42: astore_3
    //   43: new 149	java/util/ArrayList
    //   46: dup
    //   47: aload_3
    //   48: invokeinterface 152 1 0
    //   53: invokespecial 155	java/util/ArrayList:<init>	(I)V
    //   56: astore_1
    //   57: aload_3
    //   58: invokeinterface 159 1 0
    //   63: astore_3
    //   64: aload_3
    //   65: invokeinterface 164 1 0
    //   70: ifeq +67 -> 137
    //   73: aload_3
    //   74: invokeinterface 167 1 0
    //   79: checkcast 169	com/google/android/gms/measurement/internal/zzai
    //   82: astore 4
    //   84: iload_2
    //   85: ifne +14 -> 99
    //   88: aload 4
    //   90: getfield 170	com/google/android/gms/measurement/internal/zzai:b	Ljava/lang/String;
    //   93: invokestatic 176	com/google/android/gms/measurement/internal/zzaj:g	(Ljava/lang/String;)Z
    //   96: ifne -32 -> 64
    //   99: aload_1
    //   100: new 178	com/google/android/gms/measurement/internal/UserAttributeParcel
    //   103: dup
    //   104: aload 4
    //   106: invokespecial 181	com/google/android/gms/measurement/internal/UserAttributeParcel:<init>	(Lcom/google/android/gms/measurement/internal/zzai;)V
    //   109: invokeinterface 185 2 0
    //   114: pop
    //   115: goto -51 -> 64
    //   118: astore_1
    //   119: aload_0
    //   120: getfield 34	com/google/android/gms/measurement/internal/zzx:a	Lcom/google/android/gms/measurement/internal/zzw;
    //   123: invokevirtual 55	com/google/android/gms/measurement/internal/zzw:f	()Lcom/google/android/gms/measurement/internal/zzp;
    //   126: invokevirtual 60	com/google/android/gms/measurement/internal/zzp:b	()Lcom/google/android/gms/measurement/internal/zzp$zza;
    //   129: ldc -69
    //   131: aload_1
    //   132: invokevirtual 76	com/google/android/gms/measurement/internal/zzp$zza:a	(Ljava/lang/String;Ljava/lang/Object;)V
    //   135: aconst_null
    //   136: areturn
    //   137: aload_1
    //   138: areturn
    //   139: astore_1
    //   140: goto -21 -> 119
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	143	0	this	zzx
    //   0	143	1	paramAppMetadata	AppMetadata
    //   0	143	2	paramBoolean	boolean
    //   42	32	3	localObject	Object
    //   82	23	4	localzzai	zzai
    // Exception table:
    //   from	to	target	type
    //   33	64	118	java/lang/InterruptedException
    //   64	84	118	java/lang/InterruptedException
    //   88	99	118	java/lang/InterruptedException
    //   99	115	118	java/lang/InterruptedException
    //   33	64	139	java/util/concurrent/ExecutionException
    //   64	84	139	java/util/concurrent/ExecutionException
    //   88	99	139	java/util/concurrent/ExecutionException
    //   99	115	139	java/util/concurrent/ExecutionException
  }
  
  @BinderThread
  public void a(final AppMetadata paramAppMetadata)
  {
    com.google.android.gms.common.internal.zzx.a(paramAppMetadata);
    b(paramAppMetadata.b);
    this.a.h().a(new Runnable()
    {
      public void run()
      {
        zzx.this.a(paramAppMetadata.h);
        zzx.a(zzx.this).b(paramAppMetadata);
      }
    });
  }
  
  @BinderThread
  public void a(final EventParcel paramEventParcel, final AppMetadata paramAppMetadata)
  {
    com.google.android.gms.common.internal.zzx.a(paramEventParcel);
    com.google.android.gms.common.internal.zzx.a(paramAppMetadata);
    b(paramAppMetadata.b);
    this.a.h().a(new Runnable()
    {
      public void run()
      {
        zzx.this.a(paramAppMetadata.h);
        zzx.a(zzx.this).a(paramEventParcel, paramAppMetadata);
      }
    });
  }
  
  @BinderThread
  public void a(final EventParcel paramEventParcel, final String paramString1, final String paramString2)
  {
    com.google.android.gms.common.internal.zzx.a(paramEventParcel);
    com.google.android.gms.common.internal.zzx.a(paramString1);
    b(paramString1);
    this.a.h().a(new Runnable()
    {
      public void run()
      {
        zzx.this.a(paramString2);
        zzx.a(zzx.this).a(paramEventParcel, paramString1);
      }
    });
  }
  
  @BinderThread
  public void a(final UserAttributeParcel paramUserAttributeParcel, final AppMetadata paramAppMetadata)
  {
    com.google.android.gms.common.internal.zzx.a(paramUserAttributeParcel);
    com.google.android.gms.common.internal.zzx.a(paramAppMetadata);
    b(paramAppMetadata.b);
    if (paramUserAttributeParcel.a() == null)
    {
      this.a.h().a(new Runnable()
      {
        public void run()
        {
          zzx.this.a(paramAppMetadata.h);
          zzx.a(zzx.this).b(paramUserAttributeParcel, paramAppMetadata);
        }
      });
      return;
    }
    this.a.h().a(new Runnable()
    {
      public void run()
      {
        zzx.this.a(paramAppMetadata.h);
        zzx.a(zzx.this).a(paramUserAttributeParcel, paramAppMetadata);
      }
    });
  }
  
  @WorkerThread
  void a(String paramString)
  {
    if (!TextUtils.isEmpty(paramString))
    {
      paramString = paramString.split(":", 2);
      if (paramString.length != 2) {}
    }
    long l;
    try
    {
      l = Long.valueOf(paramString[0]).longValue();
      if (l > 0L)
      {
        this.a.e().b.a(paramString[1], l);
        return;
      }
    }
    catch (NumberFormatException localNumberFormatException)
    {
      this.a.f().c().a("Combining sample with a non-number weight", paramString[0]);
      return;
    }
    this.a.f().c().a("Combining sample with a non-positive weight", Long.valueOf(l));
  }
  
  @BinderThread
  public void b(final AppMetadata paramAppMetadata)
  {
    com.google.android.gms.common.internal.zzx.a(paramAppMetadata);
    b(paramAppMetadata.b);
    this.a.h().a(new Runnable()
    {
      public void run()
      {
        zzx.this.a(paramAppMetadata.h);
        zzx.a(zzx.this).a(paramAppMetadata);
      }
    });
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */