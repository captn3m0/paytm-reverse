package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class zzb
  implements Parcelable.Creator<AppMetadata>
{
  static void a(AppMetadata paramAppMetadata, Parcel paramParcel, int paramInt)
  {
    paramInt = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramAppMetadata.a);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramAppMetadata.b, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 3, paramAppMetadata.c, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 4, paramAppMetadata.d, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 5, paramAppMetadata.e, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 6, paramAppMetadata.f);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 7, paramAppMetadata.g);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 8, paramAppMetadata.h, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 9, paramAppMetadata.i);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 10, paramAppMetadata.j);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, paramInt);
  }
  
  public AppMetadata a(Parcel paramParcel)
  {
    int j = zza.b(paramParcel);
    int i = 0;
    String str5 = null;
    String str4 = null;
    String str3 = null;
    String str2 = null;
    long l2 = 0L;
    long l1 = 0L;
    String str1 = null;
    boolean bool2 = false;
    boolean bool1 = false;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str5 = zza.p(paramParcel, k);
        break;
      case 3: 
        str4 = zza.p(paramParcel, k);
        break;
      case 4: 
        str3 = zza.p(paramParcel, k);
        break;
      case 5: 
        str2 = zza.p(paramParcel, k);
        break;
      case 6: 
        l2 = zza.i(paramParcel, k);
        break;
      case 7: 
        l1 = zza.i(paramParcel, k);
        break;
      case 8: 
        str1 = zza.p(paramParcel, k);
        break;
      case 9: 
        bool2 = zza.c(paramParcel, k);
        break;
      case 10: 
        bool1 = zza.c(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new AppMetadata(i, str5, str4, str3, str2, l2, l1, str1, bool2, bool1);
  }
  
  public AppMetadata[] a(int paramInt)
  {
    return new AppMetadata[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */