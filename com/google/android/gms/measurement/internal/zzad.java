package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.MainThread;
import android.support.annotation.WorkerThread;
import com.google.android.gms.internal.zzmq;

public class zzad
  extends zzz
{
  private Handler a;
  private long b;
  private final Runnable c = new Runnable()
  {
    @MainThread
    public void run()
    {
      zzad.this.r().a(new Runnable()
      {
        public void run()
        {
          zzad.this.v();
        }
      });
    }
  };
  private final zzf d = new zzf(this.n)
  {
    @WorkerThread
    public void a()
    {
      zzad.a(zzad.this);
    }
  };
  private final zzf e = new zzf(this.n)
  {
    @WorkerThread
    public void a()
    {
      zzad.b(zzad.this);
    }
  };
  
  zzad(zzw paramzzw)
  {
    super(paramzzw);
  }
  
  @WorkerThread
  private void a(long paramLong)
  {
    f();
    w();
    this.d.cancel();
    this.e.cancel();
    s().z().a("Activity resumed, time", Long.valueOf(paramLong));
    this.b = paramLong;
    if (l().a() - t().i.a() > t().k.a())
    {
      t().j.a(true);
      t().l.a(0L);
    }
    if (t().j.a())
    {
      this.d.a(Math.max(0L, t().h.a() - t().l.a()));
      return;
    }
    this.e.a(Math.max(0L, 3600000L - t().l.a()));
  }
  
  @WorkerThread
  private void b(long paramLong)
  {
    f();
    w();
    this.d.cancel();
    this.e.cancel();
    s().z().a("Activity paused, time", Long.valueOf(paramLong));
    if (this.b != 0L) {
      t().l.a(t().l.a() + (paramLong - this.b));
    }
    t().k.a(l().a());
    try
    {
      if (!t().j.a()) {
        this.a.postDelayed(this.c, 1000L);
      }
      return;
    }
    finally {}
  }
  
  private void w()
  {
    try
    {
      if (this.a == null) {
        this.a = new Handler(Looper.getMainLooper());
      }
      return;
    }
    finally {}
  }
  
  @WorkerThread
  private void x()
  {
    f();
    long l = l().b();
    s().z().a("Session started, time", Long.valueOf(l));
    t().j.a(false);
    h().a("auto", "_s", new Bundle());
  }
  
  @WorkerThread
  private void y()
  {
    f();
    long l1 = l().b();
    if (this.b == 0L) {
      this.b = (l1 - 3600000L);
    }
    long l2 = t().l.a() + (l1 - this.b);
    t().l.a(l2);
    s().z().a("Recording user engagement, ms", Long.valueOf(l2));
    Bundle localBundle = new Bundle();
    localBundle.putLong("_et", l2);
    h().a("auto", "_e", localBundle);
    t().l.a(0L);
    this.b = l1;
    this.e.a(Math.max(0L, 3600000L - t().l.a()));
  }
  
  protected void a() {}
  
  @MainThread
  protected void b()
  {
    try
    {
      w();
      this.a.removeCallbacks(this.c);
      final long l = l().b();
      r().a(new Runnable()
      {
        public void run()
        {
          zzad.a(zzad.this, l);
        }
      });
      return;
    }
    finally {}
  }
  
  @MainThread
  protected void c()
  {
    final long l = l().b();
    r().a(new Runnable()
    {
      public void run()
      {
        zzad.b(zzad.this, l);
      }
    });
  }
  
  @WorkerThread
  public void v()
  {
    f();
    s().y().a("Application backgrounded. Logging engagement");
    long l = t().l.a();
    if (l > 0L)
    {
      Bundle localBundle = new Bundle();
      localBundle.putLong("_et", l);
      h().a("auto", "_e", localBundle);
      t().l.a(0L);
      return;
    }
    s().c().a("Not logging non-positive engagement time", Long.valueOf(l));
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzad.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */