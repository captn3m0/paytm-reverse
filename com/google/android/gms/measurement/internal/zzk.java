package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzk
  implements Parcelable.Creator<EventParcel>
{
  static void a(EventParcel paramEventParcel, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramEventParcel.a);
    zzb.a(paramParcel, 2, paramEventParcel.b, false);
    zzb.a(paramParcel, 3, paramEventParcel.c, paramInt, false);
    zzb.a(paramParcel, 4, paramEventParcel.d, false);
    zzb.a(paramParcel, 5, paramEventParcel.e);
    zzb.a(paramParcel, i);
  }
  
  public EventParcel a(Parcel paramParcel)
  {
    String str1 = null;
    int j = zza.b(paramParcel);
    int i = 0;
    long l = 0L;
    EventParams localEventParams = null;
    String str2 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str2 = zza.p(paramParcel, k);
        break;
      case 3: 
        localEventParams = (EventParams)zza.a(paramParcel, k, EventParams.CREATOR);
        break;
      case 4: 
        str1 = zza.p(paramParcel, k);
        break;
      case 5: 
        l = zza.i(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new EventParcel(i, str2, localEventParams, str1, l);
  }
  
  public EventParcel[] a(int paramInt)
  {
    return new EventParcel[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */