package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzpz.zzd;

class zzs
{
  final boolean a;
  final int b;
  long c;
  float d;
  long e;
  float f;
  long g;
  float h;
  final boolean i;
  
  public zzs(zzpz.zzd paramzzd)
  {
    zzx.a(paramzzd);
    boolean bool1;
    if ((paramzzd.a == null) || (paramzzd.a.intValue() == 0)) {
      bool1 = false;
    }
    for (;;)
    {
      if (bool1)
      {
        this.b = paramzzd.a.intValue();
        if ((paramzzd.b != null) && (paramzzd.b.booleanValue()))
        {
          label62:
          this.a = bool2;
          if (paramzzd.a.intValue() != 4) {
            break label185;
          }
          if (!this.a) {
            break label160;
          }
          this.f = Float.parseFloat(paramzzd.d);
          this.h = Float.parseFloat(paramzzd.e);
        }
      }
      for (;;)
      {
        this.i = bool1;
        return;
        if (paramzzd.a.intValue() != 4)
        {
          if (paramzzd.c != null) {
            break label233;
          }
          bool1 = false;
          break;
        }
        if ((paramzzd.d != null) && (paramzzd.e != null)) {
          break label233;
        }
        bool1 = false;
        break;
        bool2 = false;
        break label62;
        label160:
        this.e = Long.parseLong(paramzzd.d);
        this.g = Long.parseLong(paramzzd.e);
        continue;
        label185:
        if (this.a)
        {
          this.d = Float.parseFloat(paramzzd.c);
        }
        else
        {
          this.c = Long.parseLong(paramzzd.c);
          continue;
          this.b = 0;
          this.a = false;
        }
      }
      label233:
      bool1 = true;
    }
  }
  
  public Boolean a(float paramFloat)
  {
    boolean bool3 = true;
    boolean bool4 = true;
    boolean bool1 = true;
    boolean bool2 = false;
    if (!this.i) {
      return null;
    }
    if (!this.a) {
      return null;
    }
    switch (this.b)
    {
    default: 
      return null;
    case 1: 
      if (paramFloat < this.d) {}
      for (;;)
      {
        return Boolean.valueOf(bool1);
        bool1 = false;
      }
    case 2: 
      if (paramFloat > this.d) {}
      for (bool1 = bool3;; bool1 = false) {
        return Boolean.valueOf(bool1);
      }
    case 3: 
      if (paramFloat != this.d)
      {
        bool1 = bool2;
        if (Math.abs(paramFloat - this.d) >= 2.0F * Math.max(Math.ulp(paramFloat), Math.ulp(this.d))) {}
      }
      else
      {
        bool1 = true;
      }
      return Boolean.valueOf(bool1);
    }
    if ((paramFloat >= this.f) && (paramFloat <= this.h)) {}
    for (bool1 = bool4;; bool1 = false) {
      return Boolean.valueOf(bool1);
    }
  }
  
  public Boolean a(long paramLong)
  {
    boolean bool2 = true;
    boolean bool3 = true;
    boolean bool4 = true;
    boolean bool1 = true;
    if (!this.i) {
      return null;
    }
    if (this.a) {
      return null;
    }
    switch (this.b)
    {
    default: 
      return null;
    case 1: 
      if (paramLong < this.c) {}
      for (;;)
      {
        return Boolean.valueOf(bool1);
        bool1 = false;
      }
    case 2: 
      if (paramLong > this.c) {}
      for (bool1 = bool2;; bool1 = false) {
        return Boolean.valueOf(bool1);
      }
    case 3: 
      if (paramLong == this.c) {}
      for (bool1 = bool3;; bool1 = false) {
        return Boolean.valueOf(bool1);
      }
    }
    if ((paramLong >= this.e) && (paramLong <= this.g)) {}
    for (bool1 = bool4;; bool1 = false) {
      return Boolean.valueOf(bool1);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */