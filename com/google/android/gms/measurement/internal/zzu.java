package com.google.android.gms.measurement.internal;

import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzqa.zza;
import com.google.android.gms.internal.zzqa.zzb;
import com.google.android.gms.internal.zzqa.zzc;
import com.google.android.gms.internal.zzsm;
import com.google.android.gms.internal.zzsn;
import java.io.IOException;
import java.util.Map;

public class zzu
  extends zzz
{
  private final Map<String, Map<String, String>> a = new ArrayMap();
  private final Map<String, Map<String, Boolean>> b = new ArrayMap();
  private final Map<String, zzqa.zzb> c = new ArrayMap();
  
  zzu(zzw paramzzw)
  {
    super(paramzzw);
  }
  
  private Map<String, String> a(zzqa.zzb paramzzb)
  {
    ArrayMap localArrayMap = new ArrayMap();
    if ((paramzzb != null) && (paramzzb.d != null))
    {
      paramzzb = paramzzb.d;
      int j = paramzzb.length;
      int i = 0;
      while (i < j)
      {
        Object localObject = paramzzb[i];
        if (localObject != null) {
          localArrayMap.put(((zzqa.zzc)localObject).a, ((zzqa.zzc)localObject).b);
        }
        i += 1;
      }
    }
    return localArrayMap;
  }
  
  @WorkerThread
  private zzqa.zzb b(String paramString, byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte == null) {
      return new zzqa.zzb();
    }
    paramArrayOfByte = zzsm.a(paramArrayOfByte);
    zzqa.zzb localzzb = new zzqa.zzb();
    try
    {
      localzzb.a(paramArrayOfByte);
      s().z().a("Parsed config. version, gmp_app_id", localzzb.a, localzzb.b);
      return localzzb;
    }
    catch (IOException paramArrayOfByte)
    {
      s().c().a("Unable to merge remote config", paramString, paramArrayOfByte);
    }
    return null;
  }
  
  private Map<String, Boolean> b(zzqa.zzb paramzzb)
  {
    ArrayMap localArrayMap = new ArrayMap();
    if ((paramzzb != null) && (paramzzb.e != null))
    {
      paramzzb = paramzzb.e;
      int j = paramzzb.length;
      int i = 0;
      while (i < j)
      {
        Object localObject = paramzzb[i];
        if (localObject != null) {
          localArrayMap.put(((zzqa.zza)localObject).a, ((zzqa.zza)localObject).b);
        }
        i += 1;
      }
    }
    return localArrayMap;
  }
  
  @WorkerThread
  private void b(String paramString)
  {
    G();
    f();
    zzx.a(paramString);
    if (!this.c.containsKey(paramString))
    {
      localObject = n().d(paramString);
      if (localObject == null)
      {
        this.a.put(paramString, null);
        this.b.put(paramString, null);
        this.c.put(paramString, null);
      }
    }
    else
    {
      return;
    }
    Object localObject = b(paramString, (byte[])localObject);
    this.a.put(paramString, a((zzqa.zzb)localObject));
    this.b.put(paramString, b((zzqa.zzb)localObject));
    this.c.put(paramString, localObject);
  }
  
  @WorkerThread
  protected zzqa.zzb a(String paramString)
  {
    G();
    f();
    zzx.a(paramString);
    b(paramString);
    return (zzqa.zzb)this.c.get(paramString);
  }
  
  @WorkerThread
  String a(String paramString1, String paramString2)
  {
    f();
    b(paramString1);
    paramString1 = (Map)this.a.get(paramString1);
    if (paramString1 != null) {
      return (String)paramString1.get(paramString2);
    }
    return null;
  }
  
  protected void a() {}
  
  @WorkerThread
  protected boolean a(String paramString, byte[] paramArrayOfByte)
  {
    G();
    f();
    zzx.a(paramString);
    zzqa.zzb localzzb = b(paramString, paramArrayOfByte);
    if (localzzb == null) {
      return false;
    }
    this.b.put(paramString, b(localzzb));
    this.c.put(paramString, localzzb);
    this.a.put(paramString, a(localzzb));
    g().a(paramString, localzzb.f);
    try
    {
      localzzb.f = null;
      byte[] arrayOfByte = new byte[localzzb.g()];
      localzzb.a(zzsn.a(arrayOfByte));
      paramArrayOfByte = arrayOfByte;
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        s().c().a("Unable to serialize reduced-size config.  Storing full config instead.", localIOException);
      }
    }
    n().a(paramString, paramArrayOfByte);
    return true;
  }
  
  @WorkerThread
  boolean b(String paramString1, String paramString2)
  {
    f();
    b(paramString1);
    paramString1 = (Map)this.b.get(paramString1);
    if (paramString1 != null)
    {
      paramString1 = (Boolean)paramString1.get(paramString2);
      if (paramString1 == null) {
        return false;
      }
      return paramString1.booleanValue();
    }
    return false;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */