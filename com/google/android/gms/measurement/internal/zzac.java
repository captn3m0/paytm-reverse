package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Looper;
import android.os.RemoteException;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.stats.zzb;
import com.google.android.gms.common.zzc;
import com.google.android.gms.measurement.AppMeasurementService;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class zzac
  extends zzz
{
  private final zza a;
  private zzm b;
  private Boolean c;
  private final zzf d;
  private final zzaf e;
  private final List<Runnable> f = new ArrayList();
  private final zzf g;
  
  protected zzac(zzw paramzzw)
  {
    super(paramzzw);
    this.e = new zzaf(paramzzw.r());
    this.a = new zza();
    this.d = new zzf(paramzzw)
    {
      public void a()
      {
        zzac.b(zzac.this);
      }
    };
    this.g = new zzf(paramzzw)
    {
      public void a()
      {
        zzac.this.s().c().a("Tasks have been queued for a long time");
      }
    };
  }
  
  @WorkerThread
  private boolean A()
  {
    f();
    G();
    if (u().N()) {
      return true;
    }
    s().z().a("Checking service availability");
    switch (zzc.b().a(m()))
    {
    default: 
      return false;
    case 0: 
      s().z().a("Service available");
      return true;
    case 1: 
      s().z().a("Service missing");
      return false;
    case 18: 
      s().z().a("Service updating");
      return true;
    case 2: 
      s().z().a("Service version update required");
      return false;
    case 3: 
      s().z().a("Service disabled");
      return false;
    }
    s().z().a("Service invalid");
    return false;
  }
  
  @WorkerThread
  private void B()
  {
    f();
    if (!b()) {
      return;
    }
    s().z().a("Inactivity, disconnecting from AppMeasurementService");
    w();
  }
  
  @WorkerThread
  private void C()
  {
    f();
    y();
  }
  
  @WorkerThread
  private void D()
  {
    f();
    s().z().a("Processing queued up service tasks", Integer.valueOf(this.f.size()));
    Iterator localIterator = this.f.iterator();
    while (localIterator.hasNext())
    {
      Runnable localRunnable = (Runnable)localIterator.next();
      r().a(localRunnable);
    }
    this.f.clear();
    this.g.cancel();
  }
  
  @WorkerThread
  private void a(ComponentName paramComponentName)
  {
    f();
    if (this.b != null)
    {
      this.b = null;
      s().z().a("Disconnected from device MeasurementService", paramComponentName);
      C();
    }
  }
  
  @WorkerThread
  private void a(zzm paramzzm)
  {
    f();
    com.google.android.gms.common.internal.zzx.a(paramzzm);
    this.b = paramzzm;
    x();
    D();
  }
  
  @WorkerThread
  private void a(Runnable paramRunnable)
    throws IllegalStateException
  {
    f();
    if (b())
    {
      paramRunnable.run();
      return;
    }
    if (this.f.size() >= u().R())
    {
      s().b().a("Discarding data. Max runnable queue size reached");
      return;
    }
    this.f.add(paramRunnable);
    if (!this.n.z()) {
      this.g.a(60000L);
    }
    y();
  }
  
  @WorkerThread
  private void x()
  {
    f();
    this.e.a();
    if (!this.n.z()) {
      this.d.a(u().J());
    }
  }
  
  @WorkerThread
  private void y()
  {
    f();
    G();
    if (b()) {
      return;
    }
    if (this.c == null)
    {
      this.c = t().v();
      if (this.c == null)
      {
        s().z().a("State of service unknown");
        this.c = Boolean.valueOf(A());
        t().a(this.c.booleanValue());
      }
    }
    if (this.c.booleanValue())
    {
      s().z().a("Using measurement service");
      this.a.a();
      return;
    }
    if ((z()) && (!this.n.z()))
    {
      s().z().a("Using local app measurement service");
      Intent localIntent = new Intent("com.google.android.gms.measurement.START");
      localIntent.setComponent(new ComponentName(m(), AppMeasurementService.class));
      this.a.a(localIntent);
      return;
    }
    if (u().O())
    {
      s().z().a("Using direct local measurement implementation");
      a(new zzx(this.n, true));
      return;
    }
    s().b().a("Not in main process. Unable to use local measurement implementation. Please register the AppMeasurementService service in the app manifest");
  }
  
  private boolean z()
  {
    List localList = m().getPackageManager().queryIntentServices(new Intent(m(), AppMeasurementService.class), 65536);
    return (localList != null) && (localList.size() > 0);
  }
  
  protected void a() {}
  
  @WorkerThread
  protected void a(final EventParcel paramEventParcel, final String paramString)
  {
    com.google.android.gms.common.internal.zzx.a(paramEventParcel);
    f();
    G();
    a(new Runnable()
    {
      public void run()
      {
        zzm localzzm = zzac.c(zzac.this);
        if (localzzm == null)
        {
          zzac.this.s().b().a("Discarding data. Failed to send event to service");
          return;
        }
        for (;;)
        {
          try
          {
            if (TextUtils.isEmpty(paramString))
            {
              localzzm.a(paramEventParcel, zzac.this.i().a(zzac.this.s().A()));
              zzac.d(zzac.this);
              return;
            }
          }
          catch (RemoteException localRemoteException)
          {
            zzac.this.s().b().a("Failed to send event to AppMeasurementService", localRemoteException);
            return;
          }
          localRemoteException.a(paramEventParcel, paramString, zzac.this.s().A());
        }
      }
    });
  }
  
  @WorkerThread
  protected void a(final UserAttributeParcel paramUserAttributeParcel)
  {
    f();
    G();
    a(new Runnable()
    {
      public void run()
      {
        zzm localzzm = zzac.c(zzac.this);
        if (localzzm == null)
        {
          zzac.this.s().b().a("Discarding data. Failed to set user attribute");
          return;
        }
        try
        {
          localzzm.a(paramUserAttributeParcel, zzac.this.i().a(zzac.this.s().A()));
          zzac.d(zzac.this);
          return;
        }
        catch (RemoteException localRemoteException)
        {
          zzac.this.s().b().a("Failed to send attribute to AppMeasurementService", localRemoteException);
        }
      }
    });
  }
  
  @WorkerThread
  public boolean b()
  {
    f();
    G();
    return this.b != null;
  }
  
  @WorkerThread
  protected void c()
  {
    f();
    G();
    a(new Runnable()
    {
      public void run()
      {
        zzm localzzm = zzac.c(zzac.this);
        if (localzzm == null)
        {
          zzac.this.s().b().a("Failed to send measurementEnabled to service");
          return;
        }
        try
        {
          localzzm.b(zzac.this.i().a(zzac.this.s().A()));
          zzac.d(zzac.this);
          return;
        }
        catch (RemoteException localRemoteException)
        {
          zzac.this.s().b().a("Failed to send measurementEnabled to AppMeasurementService", localRemoteException);
        }
      }
    });
  }
  
  @WorkerThread
  protected void v()
  {
    f();
    G();
    a(new Runnable()
    {
      public void run()
      {
        zzm localzzm = zzac.c(zzac.this);
        if (localzzm == null)
        {
          zzac.this.s().b().a("Discarding data. Failed to send app launch");
          return;
        }
        try
        {
          localzzm.a(zzac.this.i().a(zzac.this.s().A()));
          zzac.d(zzac.this);
          return;
        }
        catch (RemoteException localRemoteException)
        {
          zzac.this.s().b().a("Failed to send app launch to AppMeasurementService", localRemoteException);
        }
      }
    });
  }
  
  @WorkerThread
  public void w()
  {
    f();
    G();
    try
    {
      zzb.a().a(m(), this.a);
      this.b = null;
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      for (;;) {}
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      for (;;) {}
    }
  }
  
  protected class zza
    implements ServiceConnection, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
  {
    private volatile boolean b;
    private volatile zzo c;
    
    protected zza() {}
    
    @WorkerThread
    public void a()
    {
      zzac.this.f();
      Context localContext1 = zzac.this.m();
      try
      {
        if (this.b)
        {
          zzac.this.s().z().a("Connection attempt already in progress");
          return;
        }
        if (this.c != null)
        {
          zzac.this.s().z().a("Already awaiting connection attempt");
          return;
        }
      }
      finally {}
      this.c = new zzo(localContext2, Looper.getMainLooper(), com.google.android.gms.common.internal.zzf.a(localContext2), this, this);
      zzac.this.s().z().a("Connecting to remote service");
      this.b = true;
      this.c.o();
    }
    
    @WorkerThread
    public void a(Intent paramIntent)
    {
      zzac.this.f();
      Context localContext = zzac.this.m();
      zzb localzzb = zzb.a();
      try
      {
        if (this.b)
        {
          zzac.this.s().z().a("Connection attempt already in progress");
          return;
        }
        this.b = true;
        localzzb.a(localContext, paramIntent, zzac.a(zzac.this), 129);
        return;
      }
      finally {}
    }
    
    /* Error */
    @MainThread
    public void onConnected(@android.support.annotation.Nullable final android.os.Bundle paramBundle)
    {
      // Byte code:
      //   0: ldc 111
      //   2: invokestatic 115	com/google/android/gms/common/internal/zzx:b	(Ljava/lang/String;)V
      //   5: aload_0
      //   6: monitorenter
      //   7: aload_0
      //   8: getfield 65	com/google/android/gms/measurement/internal/zzac$zza:c	Lcom/google/android/gms/measurement/internal/zzo;
      //   11: invokevirtual 119	com/google/android/gms/measurement/internal/zzo:v	()Landroid/os/IInterface;
      //   14: checkcast 121	com/google/android/gms/measurement/internal/zzm
      //   17: astore_1
      //   18: aload_0
      //   19: aconst_null
      //   20: putfield 65	com/google/android/gms/measurement/internal/zzac$zza:c	Lcom/google/android/gms/measurement/internal/zzo;
      //   23: aload_0
      //   24: getfield 31	com/google/android/gms/measurement/internal/zzac$zza:a	Lcom/google/android/gms/measurement/internal/zzac;
      //   27: invokevirtual 125	com/google/android/gms/measurement/internal/zzac:r	()Lcom/google/android/gms/measurement/internal/zzv;
      //   30: new 19	com/google/android/gms/measurement/internal/zzac$zza$3
      //   33: dup
      //   34: aload_0
      //   35: aload_1
      //   36: invokespecial 128	com/google/android/gms/measurement/internal/zzac$zza$3:<init>	(Lcom/google/android/gms/measurement/internal/zzac$zza;Lcom/google/android/gms/measurement/internal/zzm;)V
      //   39: invokevirtual 133	com/google/android/gms/measurement/internal/zzv:a	(Ljava/lang/Runnable;)V
      //   42: aload_0
      //   43: monitorexit
      //   44: return
      //   45: aload_0
      //   46: aconst_null
      //   47: putfield 65	com/google/android/gms/measurement/internal/zzac$zza:c	Lcom/google/android/gms/measurement/internal/zzo;
      //   50: aload_0
      //   51: iconst_0
      //   52: putfield 38	com/google/android/gms/measurement/internal/zzac$zza:b	Z
      //   55: goto -13 -> 42
      //   58: astore_1
      //   59: aload_0
      //   60: monitorexit
      //   61: aload_1
      //   62: athrow
      //   63: astore_1
      //   64: goto -19 -> 45
      //   67: astore_1
      //   68: goto -23 -> 45
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	71	0	this	zza
      //   0	71	1	paramBundle	android.os.Bundle
      // Exception table:
      //   from	to	target	type
      //   7	42	58	finally
      //   42	44	58	finally
      //   45	55	58	finally
      //   59	61	58	finally
      //   7	42	63	android/os/DeadObjectException
      //   7	42	67	java/lang/IllegalStateException
    }
    
    @MainThread
    public void onConnectionFailed(@NonNull ConnectionResult paramConnectionResult)
    {
      com.google.android.gms.common.internal.zzx.b("MeasurementServiceConnection.onConnectionFailed");
      zzp localzzp = zzac.this.n.g();
      if (localzzp != null) {
        localzzp.c().a("Service connection failed", paramConnectionResult);
      }
      try
      {
        this.b = false;
        this.c = null;
        return;
      }
      finally {}
    }
    
    @MainThread
    public void onConnectionSuspended(int paramInt)
    {
      com.google.android.gms.common.internal.zzx.b("MeasurementServiceConnection.onConnectionSuspended");
      zzac.this.s().y().a("Service connection suspended");
      zzac.this.r().a(new Runnable()
      {
        public void run()
        {
          zzac.a(zzac.this, new ComponentName(zzac.this.m(), AppMeasurementService.class));
        }
      });
    }
    
    /* Error */
    @MainThread
    public void onServiceConnected(final ComponentName paramComponentName, android.os.IBinder paramIBinder)
    {
      // Byte code:
      //   0: ldc -81
      //   2: invokestatic 115	com/google/android/gms/common/internal/zzx:b	(Ljava/lang/String;)V
      //   5: aload_0
      //   6: monitorenter
      //   7: aload_2
      //   8: ifnonnull +26 -> 34
      //   11: aload_0
      //   12: iconst_0
      //   13: putfield 38	com/google/android/gms/measurement/internal/zzac$zza:b	Z
      //   16: aload_0
      //   17: getfield 31	com/google/android/gms/measurement/internal/zzac$zza:a	Lcom/google/android/gms/measurement/internal/zzac;
      //   20: invokevirtual 50	com/google/android/gms/measurement/internal/zzac:s	()Lcom/google/android/gms/measurement/internal/zzp;
      //   23: invokevirtual 177	com/google/android/gms/measurement/internal/zzp:b	()Lcom/google/android/gms/measurement/internal/zzp$zza;
      //   26: ldc -77
      //   28: invokevirtual 63	com/google/android/gms/measurement/internal/zzp$zza:a	(Ljava/lang/String;)V
      //   31: aload_0
      //   32: monitorexit
      //   33: return
      //   34: aconst_null
      //   35: astore 4
      //   37: aconst_null
      //   38: astore_3
      //   39: aload 4
      //   41: astore_1
      //   42: aload_2
      //   43: invokeinterface 185 1 0
      //   48: astore 5
      //   50: aload 4
      //   52: astore_1
      //   53: ldc -69
      //   55: aload 5
      //   57: invokevirtual 193	java/lang/String:equals	(Ljava/lang/Object;)Z
      //   60: ifeq +67 -> 127
      //   63: aload 4
      //   65: astore_1
      //   66: aload_2
      //   67: invokestatic 198	com/google/android/gms/measurement/internal/zzm$zza:a	(Landroid/os/IBinder;)Lcom/google/android/gms/measurement/internal/zzm;
      //   70: astore_2
      //   71: aload_2
      //   72: astore_1
      //   73: aload_0
      //   74: getfield 31	com/google/android/gms/measurement/internal/zzac$zza:a	Lcom/google/android/gms/measurement/internal/zzac;
      //   77: invokevirtual 50	com/google/android/gms/measurement/internal/zzac:s	()Lcom/google/android/gms/measurement/internal/zzp;
      //   80: invokevirtual 56	com/google/android/gms/measurement/internal/zzp:z	()Lcom/google/android/gms/measurement/internal/zzp$zza;
      //   83: ldc -56
      //   85: invokevirtual 63	com/google/android/gms/measurement/internal/zzp$zza:a	(Ljava/lang/String;)V
      //   88: aload_2
      //   89: astore_1
      //   90: aload_1
      //   91: ifnonnull +80 -> 171
      //   94: aload_0
      //   95: iconst_0
      //   96: putfield 38	com/google/android/gms/measurement/internal/zzac$zza:b	Z
      //   99: invokestatic 95	com/google/android/gms/common/stats/zzb:a	()Lcom/google/android/gms/common/stats/zzb;
      //   102: aload_0
      //   103: getfield 31	com/google/android/gms/measurement/internal/zzac$zza:a	Lcom/google/android/gms/measurement/internal/zzac;
      //   106: invokevirtual 46	com/google/android/gms/measurement/internal/zzac:m	()Landroid/content/Context;
      //   109: aload_0
      //   110: getfield 31	com/google/android/gms/measurement/internal/zzac$zza:a	Lcom/google/android/gms/measurement/internal/zzac;
      //   113: invokestatic 98	com/google/android/gms/measurement/internal/zzac:a	(Lcom/google/android/gms/measurement/internal/zzac;)Lcom/google/android/gms/measurement/internal/zzac$zza;
      //   116: invokevirtual 203	com/google/android/gms/common/stats/zzb:a	(Landroid/content/Context;Landroid/content/ServiceConnection;)V
      //   119: aload_0
      //   120: monitorexit
      //   121: return
      //   122: astore_1
      //   123: aload_0
      //   124: monitorexit
      //   125: aload_1
      //   126: athrow
      //   127: aload 4
      //   129: astore_1
      //   130: aload_0
      //   131: getfield 31	com/google/android/gms/measurement/internal/zzac$zza:a	Lcom/google/android/gms/measurement/internal/zzac;
      //   134: invokevirtual 50	com/google/android/gms/measurement/internal/zzac:s	()Lcom/google/android/gms/measurement/internal/zzp;
      //   137: invokevirtual 177	com/google/android/gms/measurement/internal/zzp:b	()Lcom/google/android/gms/measurement/internal/zzp$zza;
      //   140: ldc -51
      //   142: aload 5
      //   144: invokevirtual 155	com/google/android/gms/measurement/internal/zzp$zza:a	(Ljava/lang/String;Ljava/lang/Object;)V
      //   147: aload_3
      //   148: astore_1
      //   149: goto -59 -> 90
      //   152: astore_2
      //   153: aload_0
      //   154: getfield 31	com/google/android/gms/measurement/internal/zzac$zza:a	Lcom/google/android/gms/measurement/internal/zzac;
      //   157: invokevirtual 50	com/google/android/gms/measurement/internal/zzac:s	()Lcom/google/android/gms/measurement/internal/zzp;
      //   160: invokevirtual 177	com/google/android/gms/measurement/internal/zzp:b	()Lcom/google/android/gms/measurement/internal/zzp$zza;
      //   163: ldc -49
      //   165: invokevirtual 63	com/google/android/gms/measurement/internal/zzp$zza:a	(Ljava/lang/String;)V
      //   168: goto -78 -> 90
      //   171: aload_0
      //   172: getfield 31	com/google/android/gms/measurement/internal/zzac$zza:a	Lcom/google/android/gms/measurement/internal/zzac;
      //   175: invokevirtual 125	com/google/android/gms/measurement/internal/zzac:r	()Lcom/google/android/gms/measurement/internal/zzv;
      //   178: new 15	com/google/android/gms/measurement/internal/zzac$zza$1
      //   181: dup
      //   182: aload_0
      //   183: aload_1
      //   184: invokespecial 208	com/google/android/gms/measurement/internal/zzac$zza$1:<init>	(Lcom/google/android/gms/measurement/internal/zzac$zza;Lcom/google/android/gms/measurement/internal/zzm;)V
      //   187: invokevirtual 133	com/google/android/gms/measurement/internal/zzv:a	(Ljava/lang/Runnable;)V
      //   190: goto -71 -> 119
      //   193: astore_1
      //   194: goto -75 -> 119
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	197	0	this	zza
      //   0	197	1	paramComponentName	ComponentName
      //   0	197	2	paramIBinder	android.os.IBinder
      //   38	110	3	localObject1	Object
      //   35	93	4	localObject2	Object
      //   48	95	5	str	String
      // Exception table:
      //   from	to	target	type
      //   11	33	122	finally
      //   42	50	122	finally
      //   53	63	122	finally
      //   66	71	122	finally
      //   73	88	122	finally
      //   94	99	122	finally
      //   99	119	122	finally
      //   119	121	122	finally
      //   123	125	122	finally
      //   130	147	122	finally
      //   153	168	122	finally
      //   171	190	122	finally
      //   42	50	152	android/os/RemoteException
      //   53	63	152	android/os/RemoteException
      //   66	71	152	android/os/RemoteException
      //   73	88	152	android/os/RemoteException
      //   130	147	152	android/os/RemoteException
      //   99	119	193	java/lang/IllegalArgumentException
    }
    
    @MainThread
    public void onServiceDisconnected(final ComponentName paramComponentName)
    {
      com.google.android.gms.common.internal.zzx.b("MeasurementServiceConnection.onServiceDisconnected");
      zzac.this.s().y().a("Service disconnected");
      zzac.this.r().a(new Runnable()
      {
        public void run()
        {
          zzac.a(zzac.this, paramComponentName);
        }
      });
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzac.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */