package com.google.android.gms.measurement.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.MainThread;
import android.support.annotation.WorkerThread;
import com.google.android.gms.common.internal.zzx;

class zzr
  extends BroadcastReceiver
{
  static final String a = zzr.class.getName();
  private final zzw b;
  private boolean c;
  private boolean d;
  
  zzr(zzw paramzzw)
  {
    zzx.a(paramzzw);
    this.b = paramzzw;
  }
  
  private Context d()
  {
    return this.b.q();
  }
  
  private zzp e()
  {
    return this.b.f();
  }
  
  @WorkerThread
  public void a()
  {
    this.b.a();
    this.b.y();
    if (this.c) {
      return;
    }
    d().registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    this.d = this.b.p().b();
    e().z().a("Registering connectivity change receiver. Network connected", Boolean.valueOf(this.d));
    this.c = true;
  }
  
  @WorkerThread
  public void b()
  {
    this.b.a();
    this.b.y();
    if (!c()) {
      return;
    }
    e().z().a("Unregistering connectivity change receiver");
    this.c = false;
    this.d = false;
    Context localContext = d();
    try
    {
      localContext.unregisterReceiver(this);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      e().b().a("Failed to unregister the network broadcast receiver", localIllegalArgumentException);
    }
  }
  
  @WorkerThread
  public boolean c()
  {
    this.b.y();
    return this.c;
  }
  
  @MainThread
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    this.b.a();
    paramContext = paramIntent.getAction();
    e().z().a("NetworkBroadcastReceiver received action", paramContext);
    if ("android.net.conn.CONNECTIVITY_CHANGE".equals(paramContext))
    {
      final boolean bool = this.b.p().b();
      if (this.d != bool)
      {
        this.d = bool;
        this.b.h().a(new Runnable()
        {
          public void run()
          {
            zzr.a(zzr.this).a(bool);
          }
        });
      }
      return;
    }
    e().c().a("NetworkBroadcastReceiver received unknown action", paramContext);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */