package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;

public class UserAttributeParcel
  implements SafeParcelable
{
  public static final zzah CREATOR = new zzah();
  public final int a;
  public final String b;
  public final long c;
  public final Long d;
  public final Float e;
  public final String f;
  public final String g;
  
  UserAttributeParcel(int paramInt, String paramString1, long paramLong, Long paramLong1, Float paramFloat, String paramString2, String paramString3)
  {
    this.a = paramInt;
    this.b = paramString1;
    this.c = paramLong;
    this.d = paramLong1;
    this.e = paramFloat;
    this.f = paramString2;
    this.g = paramString3;
  }
  
  UserAttributeParcel(zzai paramzzai)
  {
    this(paramzzai.b, paramzzai.c, paramzzai.d, paramzzai.a);
  }
  
  UserAttributeParcel(String paramString1, long paramLong, Object paramObject, String paramString2)
  {
    zzx.a(paramString1);
    this.a = 1;
    this.b = paramString1;
    this.c = paramLong;
    this.g = paramString2;
    if (paramObject == null)
    {
      this.d = null;
      this.e = null;
      this.f = null;
      return;
    }
    if ((paramObject instanceof Long))
    {
      this.d = ((Long)paramObject);
      this.e = null;
      this.f = null;
      return;
    }
    if ((paramObject instanceof Float))
    {
      this.d = null;
      this.e = ((Float)paramObject);
      this.f = null;
      return;
    }
    if ((paramObject instanceof String))
    {
      this.d = null;
      this.e = null;
      this.f = ((String)paramObject);
      return;
    }
    throw new IllegalArgumentException("User attribute given of un-supported type");
  }
  
  public Object a()
  {
    if (this.d != null) {
      return this.d;
    }
    if (this.e != null) {
      return this.e;
    }
    if (this.f != null) {
      return this.f;
    }
    return null;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzah.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/UserAttributeParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */