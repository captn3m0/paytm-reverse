package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzpz.zzf;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class zzae
{
  final int a;
  final boolean b;
  final String c;
  final List<String> d;
  final String e;
  final boolean f;
  
  public zzae(zzpz.zzf paramzzf)
  {
    zzx.a(paramzzf);
    boolean bool1;
    if ((paramzzf.a == null) || (paramzzf.a.intValue() == 0)) {
      bool1 = false;
    }
    for (;;)
    {
      if (bool1)
      {
        this.a = paramzzf.a.intValue();
        boolean bool2 = bool3;
        if (paramzzf.c != null)
        {
          bool2 = bool3;
          if (paramzzf.c.booleanValue()) {
            bool2 = true;
          }
        }
        this.b = bool2;
        if ((this.b) || (this.a == 1) || (this.a == 6))
        {
          this.c = paramzzf.b;
          label108:
          if (paramzzf.d != null) {
            break label205;
          }
          paramzzf = null;
          label117:
          this.d = paramzzf;
          if (this.a != 1) {
            break label221;
          }
          this.e = this.c;
        }
      }
      for (;;)
      {
        this.f = bool1;
        return;
        if (paramzzf.a.intValue() == 6)
        {
          if ((paramzzf.d != null) && (paramzzf.d.length != 0)) {
            break label257;
          }
          bool1 = false;
          break;
        }
        if (paramzzf.b != null) {
          break label257;
        }
        bool1 = false;
        break;
        this.c = paramzzf.b.toUpperCase(Locale.ENGLISH);
        break label108;
        label205:
        paramzzf = a(paramzzf.d, this.b);
        break label117;
        label221:
        this.e = null;
        continue;
        this.a = 0;
        this.b = false;
        this.c = null;
        this.d = null;
        this.e = null;
      }
      label257:
      bool1 = true;
    }
  }
  
  private List<String> a(String[] paramArrayOfString, boolean paramBoolean)
  {
    Object localObject;
    if (paramBoolean)
    {
      localObject = Arrays.asList(paramArrayOfString);
      return (List<String>)localObject;
    }
    ArrayList localArrayList = new ArrayList();
    int j = paramArrayOfString.length;
    int i = 0;
    for (;;)
    {
      localObject = localArrayList;
      if (i >= j) {
        break;
      }
      localArrayList.add(paramArrayOfString[i].toUpperCase(Locale.ENGLISH));
      i += 1;
    }
  }
  
  public Boolean a(String paramString)
  {
    if (!this.f) {
      return null;
    }
    String str = paramString;
    if (!this.b)
    {
      if (this.a != 1) {
        break label102;
      }
      str = paramString;
    }
    switch (this.a)
    {
    default: 
      return null;
    case 1: 
      if (this.b) {}
      for (int i = 0;; i = 66)
      {
        return Boolean.valueOf(Pattern.compile(this.e, i).matcher(str).matches());
        str = paramString.toUpperCase(Locale.ENGLISH);
        break;
      }
    case 2: 
      return Boolean.valueOf(str.startsWith(this.c));
    case 3: 
      return Boolean.valueOf(str.endsWith(this.c));
    case 4: 
      return Boolean.valueOf(str.contains(this.c));
    case 5: 
      label102:
      return Boolean.valueOf(str.equals(this.c));
    }
    return Boolean.valueOf(this.d.contains(str));
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzae.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */