package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.zzd;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzlz;

public final class zzl
{
  public static zza<Long> A = zza.a("measurement.service_client.idle_disconnect_millis", 5000L);
  public static zza<Boolean> a = zza.a("measurement.service_enabled", true);
  public static zza<Boolean> b = zza.a("measurement.service_client_enabled", true);
  public static zza<String> c = zza.a("measurement.log_tag", "GMPM", "GMPM-SVC");
  public static zza<Long> d = zza.a("measurement.ad_id_cache_time", 10000L);
  public static zza<Long> e = zza.a("measurement.monitoring.sample_period_millis", 86400000L);
  public static zza<Long> f = zza.a("measurement.config.cache_time", 86400000L);
  public static zza<String> g = zza.a("measurement.config.url_scheme", "https");
  public static zza<String> h = zza.a("measurement.config.url_authority", "app-measurement.com");
  public static zza<Integer> i = zza.a("measurement.upload.max_bundles", 100);
  public static zza<Integer> j = zza.a("measurement.upload.max_batch_size", 65536);
  public static zza<Integer> k = zza.a("measurement.upload.max_bundle_size", 65536);
  public static zza<Integer> l = zza.a("measurement.upload.max_events_per_bundle", 1000);
  public static zza<Integer> m = zza.a("measurement.upload.max_events_per_day", 100000);
  public static zza<Integer> n = zza.a("measurement.upload.max_public_events_per_day", 50000);
  public static zza<Integer> o = zza.a("measurement.upload.max_conversions_per_day", 500);
  public static zza<Integer> p = zza.a("measurement.store.max_stored_events_per_app", 100000);
  public static zza<String> q = zza.a("measurement.upload.url", "https://app-measurement.com/a");
  public static zza<Long> r = zza.a("measurement.upload.backoff_period", 43200000L);
  public static zza<Long> s = zza.a("measurement.upload.window_interval", 3600000L);
  public static zza<Long> t = zza.a("measurement.upload.interval", 3600000L);
  public static zza<Long> u = zza.a("measurement.upload.stale_data_deletion_interval", 86400000L);
  public static zza<Long> v = zza.a("measurement.upload.initial_upload_delay_time", 15000L);
  public static zza<Long> w = zza.a("measurement.upload.retry_time", 1800000L);
  public static zza<Integer> x = zza.a("measurement.upload.retry_count", 6);
  public static zza<Long> y = zza.a("measurement.upload.max_queue_time", 2419200000L);
  public static zza<Integer> z = zza.a("measurement.lifetimevalue.max_currency_tracked", 4);
  
  public static final class zza<V>
  {
    private final V a;
    private final zzlz<V> b;
    private V c;
    private final String d;
    
    private zza(String paramString, zzlz<V> paramzzlz, V paramV)
    {
      zzx.a(paramzzlz);
      this.b = paramzzlz;
      this.a = paramV;
      this.d = paramString;
    }
    
    static zza<Integer> a(String paramString, int paramInt)
    {
      return a(paramString, paramInt, paramInt);
    }
    
    static zza<Integer> a(String paramString, int paramInt1, int paramInt2)
    {
      return new zza(paramString, zzlz.a(paramString, Integer.valueOf(paramInt2)), Integer.valueOf(paramInt1));
    }
    
    static zza<Long> a(String paramString, long paramLong)
    {
      return a(paramString, paramLong, paramLong);
    }
    
    static zza<Long> a(String paramString, long paramLong1, long paramLong2)
    {
      return new zza(paramString, zzlz.a(paramString, Long.valueOf(paramLong2)), Long.valueOf(paramLong1));
    }
    
    static zza<String> a(String paramString1, String paramString2)
    {
      return a(paramString1, paramString2, paramString2);
    }
    
    static zza<String> a(String paramString1, String paramString2, String paramString3)
    {
      return new zza(paramString1, zzlz.a(paramString1, paramString3), paramString2);
    }
    
    static zza<Boolean> a(String paramString, boolean paramBoolean)
    {
      return a(paramString, paramBoolean, paramBoolean);
    }
    
    static zza<Boolean> a(String paramString, boolean paramBoolean1, boolean paramBoolean2)
    {
      return new zza(paramString, zzlz.a(paramString, paramBoolean2), Boolean.valueOf(paramBoolean1));
    }
    
    public V a(V paramV)
    {
      Object localObject;
      if (this.c != null) {
        localObject = this.c;
      }
      do
      {
        return (V)localObject;
        localObject = paramV;
      } while (paramV != null);
      if ((zzd.a) && (zzlz.b())) {
        return (V)this.b.d();
      }
      return (V)this.a;
    }
    
    public String a()
    {
      return this.d;
    }
    
    public V b()
    {
      if (this.c != null) {
        return (V)this.c;
      }
      if ((zzd.a) && (zzlz.b())) {
        return (V)this.b.d();
      }
      return (V)this.a;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */