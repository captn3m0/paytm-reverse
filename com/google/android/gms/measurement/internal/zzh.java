package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzx;
import java.util.Iterator;
import java.util.Set;

public class zzh
{
  final String a;
  final String b;
  final String c;
  final long d;
  final long e;
  final EventParams f;
  
  zzh(zzw paramzzw, String paramString1, String paramString2, String paramString3, long paramLong1, long paramLong2, Bundle paramBundle)
  {
    zzx.a(paramString2);
    zzx.a(paramString3);
    this.a = paramString2;
    this.b = paramString3;
    paramString2 = paramString1;
    if (TextUtils.isEmpty(paramString1)) {
      paramString2 = null;
    }
    this.c = paramString2;
    this.d = paramLong1;
    this.e = paramLong2;
    if ((this.e != 0L) && (this.e > this.d)) {
      paramzzw.f().c().a("Event created with reverse previous/current timestamps");
    }
    this.f = a(paramzzw, paramBundle);
  }
  
  private zzh(zzw paramzzw, String paramString1, String paramString2, String paramString3, long paramLong1, long paramLong2, EventParams paramEventParams)
  {
    zzx.a(paramString2);
    zzx.a(paramString3);
    zzx.a(paramEventParams);
    this.a = paramString2;
    this.b = paramString3;
    paramString2 = paramString1;
    if (TextUtils.isEmpty(paramString1)) {
      paramString2 = null;
    }
    this.c = paramString2;
    this.d = paramLong1;
    this.e = paramLong2;
    if ((this.e != 0L) && (this.e > this.d)) {
      paramzzw.f().c().a("Event created with reverse previous/current timestamps");
    }
    this.f = paramEventParams;
  }
  
  private EventParams a(zzw paramzzw, Bundle paramBundle)
  {
    if ((paramBundle != null) && (!paramBundle.isEmpty()))
    {
      paramBundle = new Bundle(paramBundle);
      Iterator localIterator = paramBundle.keySet().iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        if (str == null)
        {
          localIterator.remove();
        }
        else
        {
          Object localObject = paramzzw.n().a(str, paramBundle.get(str));
          if (localObject == null) {
            localIterator.remove();
          } else {
            paramzzw.n().a(paramBundle, str, localObject);
          }
        }
      }
      return new EventParams(paramBundle);
    }
    return new EventParams(new Bundle());
  }
  
  zzh a(zzw paramzzw, long paramLong)
  {
    return new zzh(paramzzw, this.c, this.a, this.b, this.d, paramLong, this.f);
  }
  
  public String toString()
  {
    return "Event{appId='" + this.a + '\'' + ", name='" + this.b + '\'' + ", params=" + this.f + '}';
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */