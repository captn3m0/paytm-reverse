package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzmq;

abstract class zzf
{
  private static volatile Handler b;
  private final zzw a;
  private final Runnable c;
  private volatile long d;
  private boolean e;
  
  zzf(zzw paramzzw)
  {
    zzx.a(paramzzw);
    this.a = paramzzw;
    this.e = true;
    this.c = new Runnable()
    {
      public void run()
      {
        if (Looper.myLooper() == Looper.getMainLooper()) {
          zzf.a(zzf.this).h().a(this);
        }
        boolean bool;
        do
        {
          return;
          bool = zzf.this.b();
          zzf.a(zzf.this, 0L);
        } while ((!bool) || (!zzf.b(zzf.this)));
        zzf.this.a();
      }
    };
  }
  
  private Handler c()
  {
    if (b != null) {
      return b;
    }
    try
    {
      if (b == null) {
        b = new Handler(this.a.q().getMainLooper());
      }
      Handler localHandler = b;
      return localHandler;
    }
    finally {}
  }
  
  public abstract void a();
  
  public void a(long paramLong)
  {
    cancel();
    if (paramLong >= 0L)
    {
      this.d = this.a.r().a();
      if (!c().postDelayed(this.c, paramLong)) {
        this.a.f().b().a("Failed to schedule delayed post. time", Long.valueOf(paramLong));
      }
    }
  }
  
  public boolean b()
  {
    return this.d != 0L;
  }
  
  public void cancel()
  {
    this.d = 0L;
    c().removeCallbacks(this.c);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */