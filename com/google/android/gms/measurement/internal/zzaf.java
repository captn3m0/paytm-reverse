package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzmq;

class zzaf
{
  private final zzmq a;
  private long b;
  
  public zzaf(zzmq paramzzmq)
  {
    zzx.a(paramzzmq);
    this.a = paramzzmq;
  }
  
  public void a()
  {
    this.b = this.a.b();
  }
  
  public boolean a(long paramLong)
  {
    if (this.b == 0L) {}
    while (this.a.b() - this.b >= paramLong) {
      return true;
    }
    return false;
  }
  
  public void b()
  {
    this.b = 0L;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzaf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */