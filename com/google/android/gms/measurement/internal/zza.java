package com.google.android.gms.measurement.internal;

import android.text.TextUtils;
import com.google.android.gms.common.internal.zzx;

class zza
{
  private final zzw a;
  private final String b;
  private String c;
  private String d;
  private String e;
  private long f;
  private long g;
  private long h;
  private String i;
  private String j;
  private long k;
  private long l;
  private boolean m;
  private long n;
  private long o;
  private long p;
  private long q;
  private boolean r;
  private long s;
  private long t;
  
  zza(zzw paramzzw, String paramString)
  {
    zzx.a(paramzzw);
    zzx.a(paramString);
    this.a = paramzzw;
    this.b = paramString;
    this.a.y();
  }
  
  public void a()
  {
    this.a.y();
    this.r = false;
  }
  
  public void a(long paramLong)
  {
    this.a.y();
    boolean bool2 = this.r;
    if (this.g != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.r = (bool1 | bool2);
      this.g = paramLong;
      return;
    }
  }
  
  public void a(String paramString)
  {
    this.a.y();
    this.r |= zzaj.a(this.c, paramString);
    this.c = paramString;
  }
  
  public void a(boolean paramBoolean)
  {
    this.a.y();
    boolean bool2 = this.r;
    if (this.m != paramBoolean) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.r = (bool1 | bool2);
      this.m = paramBoolean;
      return;
    }
  }
  
  public String b()
  {
    this.a.y();
    return this.b;
  }
  
  public void b(long paramLong)
  {
    this.a.y();
    boolean bool2 = this.r;
    if (this.h != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.r = (bool1 | bool2);
      this.h = paramLong;
      return;
    }
  }
  
  public void b(String paramString)
  {
    this.a.y();
    String str = paramString;
    if (TextUtils.isEmpty(paramString)) {
      str = null;
    }
    this.r |= zzaj.a(this.d, str);
    this.d = str;
  }
  
  public String c()
  {
    this.a.y();
    return this.c;
  }
  
  public void c(long paramLong)
  {
    this.a.y();
    boolean bool2 = this.r;
    if (this.k != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.r = (bool1 | bool2);
      this.k = paramLong;
      return;
    }
  }
  
  public void c(String paramString)
  {
    this.a.y();
    this.r |= zzaj.a(this.e, paramString);
    this.e = paramString;
  }
  
  public String d()
  {
    this.a.y();
    return this.d;
  }
  
  public void d(long paramLong)
  {
    this.a.y();
    boolean bool2 = this.r;
    if (this.l != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.r = (bool1 | bool2);
      this.l = paramLong;
      return;
    }
  }
  
  public void d(String paramString)
  {
    this.a.y();
    this.r |= zzaj.a(this.i, paramString);
    this.i = paramString;
  }
  
  public String e()
  {
    this.a.y();
    return this.e;
  }
  
  public void e(long paramLong)
  {
    boolean bool1 = true;
    boolean bool2;
    if (paramLong >= 0L)
    {
      bool2 = true;
      zzx.b(bool2);
      this.a.y();
      bool2 = this.r;
      if (this.f == paramLong) {
        break label58;
      }
    }
    for (;;)
    {
      this.r = (bool2 | bool1);
      this.f = paramLong;
      return;
      bool2 = false;
      break;
      label58:
      bool1 = false;
    }
  }
  
  public void e(String paramString)
  {
    this.a.y();
    this.r |= zzaj.a(this.j, paramString);
    this.j = paramString;
  }
  
  public long f()
  {
    this.a.y();
    return this.g;
  }
  
  public void f(long paramLong)
  {
    this.a.y();
    boolean bool2 = this.r;
    if (this.s != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.r = (bool1 | bool2);
      this.s = paramLong;
      return;
    }
  }
  
  public long g()
  {
    this.a.y();
    return this.h;
  }
  
  public void g(long paramLong)
  {
    this.a.y();
    boolean bool2 = this.r;
    if (this.t != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.r = (bool1 | bool2);
      this.t = paramLong;
      return;
    }
  }
  
  public String h()
  {
    this.a.y();
    return this.i;
  }
  
  public void h(long paramLong)
  {
    this.a.y();
    boolean bool2 = this.r;
    if (this.n != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.r = (bool1 | bool2);
      this.n = paramLong;
      return;
    }
  }
  
  public String i()
  {
    this.a.y();
    return this.j;
  }
  
  public void i(long paramLong)
  {
    this.a.y();
    boolean bool2 = this.r;
    if (this.o != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.r = (bool1 | bool2);
      this.o = paramLong;
      return;
    }
  }
  
  public long j()
  {
    this.a.y();
    return this.k;
  }
  
  public void j(long paramLong)
  {
    this.a.y();
    boolean bool2 = this.r;
    if (this.p != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.r = (bool1 | bool2);
      this.p = paramLong;
      return;
    }
  }
  
  public long k()
  {
    this.a.y();
    return this.l;
  }
  
  public void k(long paramLong)
  {
    this.a.y();
    boolean bool2 = this.r;
    if (this.q != paramLong) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.r = (bool1 | bool2);
      this.q = paramLong;
      return;
    }
  }
  
  public boolean l()
  {
    this.a.y();
    return this.m;
  }
  
  public long m()
  {
    this.a.y();
    return this.f;
  }
  
  public long n()
  {
    this.a.y();
    return this.s;
  }
  
  public long o()
  {
    this.a.y();
    return this.t;
  }
  
  public void p()
  {
    this.a.y();
    long l2 = this.f + 1L;
    long l1 = l2;
    if (l2 > 2147483647L)
    {
      this.a.f().c().a("Bundle index overflow");
      l1 = 0L;
    }
    this.r = true;
    this.f = l1;
  }
  
  public long q()
  {
    this.a.y();
    return this.n;
  }
  
  public long r()
  {
    this.a.y();
    return this.o;
  }
  
  public long s()
  {
    this.a.y();
    return this.p;
  }
  
  public long t()
  {
    this.a.y();
    return this.q;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */