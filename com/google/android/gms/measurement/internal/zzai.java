package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.zzx;

class zzai
{
  final String a;
  final String b;
  final long c;
  final Object d;
  
  zzai(String paramString1, String paramString2, long paramLong, Object paramObject)
  {
    zzx.a(paramString1);
    zzx.a(paramString2);
    zzx.a(paramObject);
    this.a = paramString1;
    this.b = paramString2;
    this.c = paramLong;
    this.d = paramObject;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzai.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */