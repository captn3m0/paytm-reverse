package com.google.android.gms.measurement.internal;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzmq;
import com.google.android.gms.internal.zzqa.zzb;
import com.google.android.gms.internal.zzqb.zza;
import com.google.android.gms.internal.zzqb.zzb;
import com.google.android.gms.internal.zzqb.zzd;
import com.google.android.gms.internal.zzqb.zze;
import com.google.android.gms.internal.zzqb.zzg;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.android.gms.measurement.AppMeasurementReceiver;
import com.google.android.gms.measurement.AppMeasurementService;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class zzw
{
  private static zzaa a;
  private static volatile zzw b;
  private final Context c;
  private final zzd d;
  private final zzt e;
  private final zzp f;
  private final zzv g;
  private final zzad h;
  private final zzu i;
  private final AppMeasurement j;
  private final zzaj k;
  private final zze l;
  private final zzq m;
  private final zzmq n;
  private final zzac o;
  private final zzg p;
  private final zzab q;
  private final zzn r;
  private final zzr s;
  private final zzag t;
  private final zzc u;
  private final boolean v;
  private Boolean w;
  private List<Long> x;
  private int y;
  private int z;
  
  zzw(zzaa paramzzaa)
  {
    zzx.a(paramzzaa);
    this.c = paramzzaa.a;
    this.n = paramzzaa.l(this);
    this.d = paramzzaa.a(this);
    Object localObject = paramzzaa.b(this);
    ((zzt)localObject).H();
    this.e = ((zzt)localObject);
    localObject = paramzzaa.c(this);
    ((zzp)localObject).H();
    this.f = ((zzp)localObject);
    f().x().a("App measurement is starting up, version", Long.valueOf(d().M()));
    f().x().a("To enable debug logging run: adb shell setprop log.tag.GMPM VERBOSE");
    f().y().a("Debug logging enabled");
    this.k = paramzzaa.i(this);
    localObject = paramzzaa.n(this);
    ((zzg)localObject).H();
    this.p = ((zzg)localObject);
    localObject = paramzzaa.o(this);
    ((zzn)localObject).H();
    this.r = ((zzn)localObject);
    localObject = paramzzaa.j(this);
    ((zze)localObject).H();
    this.l = ((zze)localObject);
    localObject = paramzzaa.r(this);
    ((zzc)localObject).H();
    this.u = ((zzc)localObject);
    localObject = paramzzaa.k(this);
    ((zzq)localObject).H();
    this.m = ((zzq)localObject);
    localObject = paramzzaa.m(this);
    ((zzac)localObject).H();
    this.o = ((zzac)localObject);
    localObject = paramzzaa.h(this);
    ((zzab)localObject).H();
    this.q = ((zzab)localObject);
    localObject = paramzzaa.q(this);
    ((zzag)localObject).H();
    this.t = ((zzag)localObject);
    this.s = paramzzaa.p(this);
    this.j = paramzzaa.g(this);
    localObject = paramzzaa.e(this);
    ((zzad)localObject).H();
    this.h = ((zzad)localObject);
    localObject = paramzzaa.f(this);
    ((zzu)localObject).H();
    this.i = ((zzu)localObject);
    paramzzaa = paramzzaa.d(this);
    paramzzaa.H();
    this.g = paramzzaa;
    if (this.y != this.z) {
      f().b().a("Not all components initialized", Integer.valueOf(this.y), Integer.valueOf(this.z));
    }
    this.v = true;
    if ((!this.d.N()) && (!z()))
    {
      if (!(this.c.getApplicationContext() instanceof Application)) {
        break label423;
      }
      if (Build.VERSION.SDK_INT < 14) {
        break label407;
      }
      l().b();
    }
    for (;;)
    {
      this.g.a(new Runnable()
      {
        public void run()
        {
          zzw.this.c();
        }
      });
      return;
      label407:
      f().y().a("Not tracking deep linking pre-ICS");
      continue;
      label423:
      f().c().a("Application context is not an Application");
    }
  }
  
  @WorkerThread
  private boolean E()
  {
    y();
    return this.x != null;
  }
  
  private boolean F()
  {
    y();
    a();
    return (o().C()) || (!TextUtils.isEmpty(o().x()));
  }
  
  @WorkerThread
  private void G()
  {
    y();
    a();
    if ((!b()) || (!F()))
    {
      v().b();
      w().cancel();
      return;
    }
    long l2 = H();
    if (l2 == 0L)
    {
      v().b();
      w().cancel();
      return;
    }
    if (!p().b())
    {
      v().a();
      w().cancel();
      return;
    }
    long l3 = e().e.a();
    long l4 = d().X();
    long l1 = l2;
    if (!n().a(l3, l4)) {
      l1 = Math.max(l2, l3 + l4);
    }
    v().b();
    l1 -= r().a();
    if (l1 <= 0L)
    {
      w().a(1L);
      return;
    }
    f().z().a("Upload scheduled in approximately ms", Long.valueOf(l1));
    w().a(l1);
  }
  
  private long H()
  {
    long l5 = r().a();
    long l1 = d().aa();
    long l2 = d().Y();
    long l6 = e().c.a();
    long l4 = e().d.a();
    long l3 = Math.max(o().A(), o().B());
    if (l3 == 0L) {
      l2 = 0L;
    }
    do
    {
      do
      {
        return l2;
        l3 = l5 - Math.abs(l3 - l5);
        l6 = Math.abs(l6 - l5);
        l4 = l5 - Math.abs(l4 - l5);
        l5 = Math.max(l5 - l6, l4);
        l1 += l3;
        if (!n().a(l5, l2)) {
          l1 = l5 + l2;
        }
        l2 = l1;
      } while (l4 == 0L);
      l2 = l1;
    } while (l4 < l3);
    int i1 = 0;
    for (;;)
    {
      if (i1 >= d().ac()) {
        break label223;
      }
      l1 += (1 << i1) * d().ab();
      l2 = l1;
      if (l1 > l4) {
        break;
      }
      i1 += 1;
    }
    label223:
    return 0L;
  }
  
  /* Error */
  public static zzw a(Context paramContext)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic 75	com/google/android/gms/common/internal/zzx:a	(Ljava/lang/Object;)Ljava/lang/Object;
    //   4: pop
    //   5: aload_0
    //   6: invokevirtual 279	android/content/Context:getApplicationContext	()Landroid/content/Context;
    //   9: invokestatic 75	com/google/android/gms/common/internal/zzx:a	(Ljava/lang/Object;)Ljava/lang/Object;
    //   12: pop
    //   13: getstatic 423	com/google/android/gms/measurement/internal/zzw:b	Lcom/google/android/gms/measurement/internal/zzw;
    //   16: ifnonnull +32 -> 48
    //   19: ldc 2
    //   21: monitorenter
    //   22: getstatic 423	com/google/android/gms/measurement/internal/zzw:b	Lcom/google/android/gms/measurement/internal/zzw;
    //   25: ifnonnull +20 -> 45
    //   28: getstatic 425	com/google/android/gms/measurement/internal/zzw:a	Lcom/google/android/gms/measurement/internal/zzaa;
    //   31: ifnull +21 -> 52
    //   34: getstatic 425	com/google/android/gms/measurement/internal/zzw:a	Lcom/google/android/gms/measurement/internal/zzaa;
    //   37: astore_0
    //   38: aload_0
    //   39: invokevirtual 428	com/google/android/gms/measurement/internal/zzaa:a	()Lcom/google/android/gms/measurement/internal/zzw;
    //   42: putstatic 423	com/google/android/gms/measurement/internal/zzw:b	Lcom/google/android/gms/measurement/internal/zzw;
    //   45: ldc 2
    //   47: monitorexit
    //   48: getstatic 423	com/google/android/gms/measurement/internal/zzw:b	Lcom/google/android/gms/measurement/internal/zzw;
    //   51: areturn
    //   52: new 77	com/google/android/gms/measurement/internal/zzaa
    //   55: dup
    //   56: aload_0
    //   57: invokespecial 431	com/google/android/gms/measurement/internal/zzaa:<init>	(Landroid/content/Context;)V
    //   60: astore_0
    //   61: goto -23 -> 38
    //   64: astore_0
    //   65: ldc 2
    //   67: monitorexit
    //   68: aload_0
    //   69: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	70	0	paramContext	Context
    // Exception table:
    //   from	to	target	type
    //   22	38	64	finally
    //   38	45	64	finally
    //   45	48	64	finally
    //   52	61	64	finally
    //   65	68	64	finally
  }
  
  @WorkerThread
  private void a(int paramInt, Throwable paramThrowable, byte[] paramArrayOfByte)
  {
    int i1 = 0;
    y();
    a();
    byte[] arrayOfByte = paramArrayOfByte;
    if (paramArrayOfByte == null) {
      arrayOfByte = new byte[0];
    }
    paramArrayOfByte = this.x;
    this.x = null;
    if (((paramInt == 200) || (paramInt == 204)) && (paramThrowable == null))
    {
      e().c.a(r().a());
      e().d.a(0L);
      G();
      f().z().a("Successful upload. Got network response. code, size", Integer.valueOf(paramInt), Integer.valueOf(arrayOfByte.length));
      o().b();
      try
      {
        paramThrowable = paramArrayOfByte.iterator();
        while (paramThrowable.hasNext())
        {
          paramArrayOfByte = (Long)paramThrowable.next();
          o().a(paramArrayOfByte.longValue());
        }
      }
      finally
      {
        o().v();
      }
      o().v();
      if ((p().b()) && (F()))
      {
        C();
        return;
      }
      G();
      return;
    }
    f().z().a("Network upload failed. Will retry later. code, error", Integer.valueOf(paramInt), paramThrowable);
    e().d.a(r().a());
    if ((paramInt == 503) || (paramInt == 429)) {
      i1 = 1;
    }
    if (i1 != 0) {
      e().e.a(r().a());
    }
    G();
  }
  
  private void a(Bundle paramBundle, int paramInt)
  {
    if (paramBundle.getLong("_err") == 0L) {
      paramBundle.putLong("_err", paramInt);
    }
  }
  
  private void a(zzy paramzzy)
  {
    if (paramzzy == null) {
      throw new IllegalStateException("Component not created");
    }
  }
  
  @WorkerThread
  private void a(String paramString, int paramInt, Throwable paramThrowable, byte[] paramArrayOfByte)
  {
    int i2 = 0;
    y();
    a();
    zzx.a(paramString);
    byte[] arrayOfByte = paramArrayOfByte;
    if (paramArrayOfByte == null) {
      arrayOfByte = new byte[0];
    }
    o().b();
    label106:
    int i1;
    for (;;)
    {
      try
      {
        paramArrayOfByte = o().b(paramString);
        if ((paramInt == 200) || (paramInt == 204)) {
          break;
        }
        boolean bool;
        if (paramInt == 304)
        {
          break;
          if (j().a(paramString) == null)
          {
            bool = j().a(paramString, null);
            if (bool) {}
          }
        }
        else
        {
          i1 = 0;
          break label378;
          label112:
          bool = j().a(paramString, arrayOfByte);
          if (!bool) {
            return;
          }
        }
        paramArrayOfByte.f(r().a());
        o().a(paramArrayOfByte);
        if (paramInt == 404)
        {
          f().c().a("Config not found. Using empty config");
          if ((p().b()) && (F()))
          {
            C();
            label201:
            o().c();
          }
        }
        else
        {
          f().z().a("Successfully fetched config. Got network response. code, size", Integer.valueOf(paramInt), Integer.valueOf(arrayOfByte.length));
          continue;
        }
        G();
      }
      finally
      {
        o().v();
      }
    }
    label259:
    paramArrayOfByte.g(r().a());
    o().a(paramArrayOfByte);
    f().z().a("Fetching config failed. code, error", Integer.valueOf(paramInt), paramThrowable);
    e().d.a(r().a());
    if (paramInt != 503)
    {
      i1 = i2;
      if (paramInt == 429) {}
    }
    for (;;)
    {
      if (i1 != 0) {
        e().e.a(r().a());
      }
      G();
      break label201;
      if (paramThrowable != null) {
        break label106;
      }
      i1 = 1;
      label378:
      if ((i1 == 0) && (paramInt != 404)) {
        break label259;
      }
      if (paramInt == 404) {
        break;
      }
      if (paramInt != 304) {
        break label112;
      }
      break;
      i1 = 1;
    }
  }
  
  private void a(List<Long> paramList)
  {
    if (!paramList.isEmpty()) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.b(bool);
      if (this.x == null) {
        break;
      }
      f().b().a("Set uploading progress before finishing the previous upload");
      return;
    }
    this.x = new ArrayList(paramList);
  }
  
  private boolean a(String paramString, long paramLong)
  {
    o().b();
    for (;;)
    {
      int i1;
      int i2;
      long l1;
      try
      {
        zza localzza = new zza(null);
        o().a(paramString, paramLong, localzza);
        if (localzza.a()) {
          break label588;
        }
        zzqb.zze localzze = localzza.a;
        localzze.b = new zzqb.zzb[localzza.c.size()];
        i1 = 0;
        i2 = 0;
        if (i2 < localzza.c.size())
        {
          if (j().b(localzza.a.o, ((zzqb.zzb)localzza.c.get(i2)).b))
          {
            f().z().a("Dropping blacklisted raw event", ((zzqb.zzb)localzza.c.get(i2)).b);
            break label610;
          }
          localzze.b[i1] = ((zzqb.zzb)localzza.c.get(i2));
          i1 += 1;
          break label610;
        }
        if (i1 < localzza.c.size()) {
          localzze.b = ((zzqb.zzb[])Arrays.copyOf(localzze.b, i1));
        }
        localzze.A = a(localzza.a.o, localzza.a.c, localzze.b);
        localzze.e = localzze.b[0].c;
        localzze.f = localzze.b[0].c;
        i1 = 1;
        if (i1 < localzze.b.length)
        {
          paramString = localzze.b[i1];
          if (paramString.c.longValue() < localzze.e.longValue()) {
            localzze.e = paramString.c;
          }
          if (paramString.c.longValue() <= localzze.f.longValue()) {
            break label619;
          }
          localzze.f = paramString.c;
          break label619;
        }
        String str = localzza.a.o;
        zza localzza1 = o().b(str);
        if (localzza1 == null)
        {
          f().b().a("Bundling raw events w/o app info");
          localzze.x = f().A();
          o().a(localzze);
          o().a(localzza.b);
          o().f(str);
          o().c();
          return true;
        }
        paramLong = localzza1.g();
        if (paramLong != 0L)
        {
          paramString = Long.valueOf(paramLong);
          localzze.h = paramString;
          l1 = localzza1.f();
          if (l1 != 0L) {
            break label604;
          }
          if (paramLong == 0L) {
            break label583;
          }
          paramString = Long.valueOf(paramLong);
          localzze.g = paramString;
          localzza1.p();
          localzze.w = Integer.valueOf((int)localzza1.m());
          localzza1.a(localzze.e.longValue());
          localzza1.b(localzze.f.longValue());
          o().a(localzza1);
          continue;
        }
        paramString = null;
      }
      finally
      {
        o().v();
      }
      continue;
      label583:
      paramString = null;
      continue;
      label588:
      o().c();
      o().v();
      return false;
      label604:
      paramLong = l1;
      continue;
      label610:
      i2 += 1;
      continue;
      label619:
      i1 += 1;
    }
  }
  
  private zzqb.zza[] a(String paramString, zzqb.zzg[] paramArrayOfzzg, zzqb.zzb[] paramArrayOfzzb)
  {
    zzx.a(paramString);
    return x().a(paramString, paramArrayOfzzb, paramArrayOfzzg);
  }
  
  private void b(zzz paramzzz)
  {
    if (paramzzz == null) {
      throw new IllegalStateException("Component not created");
    }
    if (!paramzzz.E()) {
      throw new IllegalStateException("Component not initialized");
    }
  }
  
  @WorkerThread
  private void c(AppMetadata paramAppMetadata)
  {
    int i3 = 1;
    y();
    a();
    zzx.a(paramAppMetadata);
    zzx.a(paramAppMetadata.b);
    zza localzza2 = o().b(paramAppMetadata.b);
    String str = e().b(paramAppMetadata.b);
    int i1 = 0;
    zza localzza1;
    if (localzza2 == null)
    {
      localzza1 = new zza(this, paramAppMetadata.b);
      localzza1.a(e().b());
      localzza1.c(str);
      i1 = 1;
      int i2 = i1;
      if (!TextUtils.isEmpty(paramAppMetadata.c))
      {
        i2 = i1;
        if (!paramAppMetadata.c.equals(localzza1.d()))
        {
          localzza1.b(paramAppMetadata.c);
          i2 = 1;
        }
      }
      i1 = i2;
      if (paramAppMetadata.f != 0L)
      {
        i1 = i2;
        if (paramAppMetadata.f != localzza1.j())
        {
          localzza1.c(paramAppMetadata.f);
          i1 = 1;
        }
      }
      i2 = i1;
      if (!TextUtils.isEmpty(paramAppMetadata.d))
      {
        i2 = i1;
        if (!paramAppMetadata.d.equals(localzza1.h()))
        {
          localzza1.d(paramAppMetadata.d);
          i2 = 1;
        }
      }
      i1 = i2;
      if (!TextUtils.isEmpty(paramAppMetadata.e))
      {
        i1 = i2;
        if (!paramAppMetadata.e.equals(localzza1.i()))
        {
          localzza1.e(paramAppMetadata.e);
          i1 = 1;
        }
      }
      if (paramAppMetadata.g != localzza1.k())
      {
        localzza1.d(paramAppMetadata.g);
        i1 = 1;
      }
      if (paramAppMetadata.i == localzza1.l()) {
        break label356;
      }
      localzza1.a(paramAppMetadata.i);
      i1 = i3;
    }
    label356:
    for (;;)
    {
      if (i1 != 0) {
        o().a(localzza1);
      }
      return;
      localzza1 = localzza2;
      if (str.equals(localzza2.e())) {
        break;
      }
      localzza2.c(str);
      localzza2.a(e().b());
      i1 = 1;
      localzza1 = localzza2;
      break;
    }
  }
  
  long A()
  {
    return (r().a() + e().c()) / 1000L / 60L / 60L / 24L;
  }
  
  void B()
  {
    if (d().N()) {
      throw new IllegalStateException("Unexpected call on package side");
    }
  }
  
  @WorkerThread
  public void C()
  {
    Object localObject4 = null;
    int i2 = 0;
    y();
    a();
    if (!d().N())
    {
      localObject1 = e().v();
      if (localObject1 == null) {
        f().c().a("Upload data called on the client side before use of service was decided");
      }
    }
    long l1;
    String str;
    int i1;
    do
    {
      return;
      if (((Boolean)localObject1).booleanValue())
      {
        f().b().a("Upload called in the client side when service should be used");
        return;
      }
      if (E())
      {
        f().c().a("Uploading requested multiple times");
        return;
      }
      if (!p().b())
      {
        f().c().a("Network not connected, ignoring upload request");
        G();
        return;
      }
      l1 = r().a();
      a(l1 - d().W());
      long l2 = e().c.a();
      if (l2 != 0L) {
        f().y().a("Uploading events. Elapsed time since last upload attempt (ms)", Long.valueOf(Math.abs(l1 - l2)));
      }
      str = o().x();
      if (TextUtils.isEmpty(str)) {
        break;
      }
      i1 = d().c(str);
      int i3 = d().d(str);
      localObject4 = o().a(str, i1, i3);
    } while (((List)localObject4).isEmpty());
    Object localObject1 = ((List)localObject4).iterator();
    Object localObject5;
    do
    {
      if (!((Iterator)localObject1).hasNext()) {
        break;
      }
      localObject5 = (zzqb.zze)((Pair)((Iterator)localObject1).next()).first;
    } while (TextUtils.isEmpty(((zzqb.zze)localObject5).s));
    Object localObject3;
    for (localObject1 = ((zzqb.zze)localObject5).s;; localObject3 = null)
    {
      if (localObject1 != null)
      {
        i1 = 0;
        if (i1 < ((List)localObject4).size())
        {
          localObject5 = (zzqb.zze)((Pair)((List)localObject4).get(i1)).first;
          if (TextUtils.isEmpty(((zzqb.zze)localObject5).s)) {}
          while (((zzqb.zze)localObject5).s.equals(localObject1))
          {
            i1 += 1;
            break;
          }
        }
      }
      for (localObject1 = ((List)localObject4).subList(0, i1);; localObject3 = localObject4)
      {
        localObject5 = new zzqb.zzd();
        ((zzqb.zzd)localObject5).a = new zzqb.zze[((List)localObject1).size()];
        localObject4 = new ArrayList(((List)localObject1).size());
        i1 = i2;
        while (i1 < ((zzqb.zzd)localObject5).a.length)
        {
          ((zzqb.zzd)localObject5).a[i1] = ((zzqb.zze)((Pair)((List)localObject1).get(i1)).first);
          ((List)localObject4).add(((Pair)((List)localObject1).get(i1)).second);
          localObject5.a[i1].r = Long.valueOf(d().M());
          localObject5.a[i1].d = Long.valueOf(l1);
          localObject5.a[i1].z = Boolean.valueOf(d().N());
          i1 += 1;
        }
        if (f().a(2)) {}
        for (localObject1 = zzaj.b((zzqb.zzd)localObject5);; localObject3 = null)
        {
          Object localObject7 = n().a((zzqb.zzd)localObject5);
          Object localObject6 = d().V();
          try
          {
            URL localURL = new URL((String)localObject6);
            a((List)localObject4);
            e().d.a(l1);
            localObject4 = "?";
            if (((zzqb.zzd)localObject5).a.length > 0) {
              localObject4 = localObject5.a[0].o;
            }
            f().z().a("Uploading data. app, uncompressed size, data", localObject4, Integer.valueOf(localObject7.length), localObject1);
            p().a(str, localURL, (byte[])localObject7, null, new zzq.zza()
            {
              public void a(String paramAnonymousString, int paramAnonymousInt, Throwable paramAnonymousThrowable, byte[] paramAnonymousArrayOfByte)
              {
                zzw.a(zzw.this, paramAnonymousInt, paramAnonymousThrowable, paramAnonymousArrayOfByte);
              }
            });
            return;
          }
          catch (MalformedURLException localMalformedURLException1)
          {
            f().b().a("Failed to parse upload URL. Not uploading", localObject6);
            return;
          }
          localObject5 = o().b(l1 - d().W());
          if (TextUtils.isEmpty((CharSequence)localObject5)) {
            break;
          }
          Object localObject2 = o().b((String)localObject5);
          if (localObject2 == null) {
            break;
          }
          str = d().a(((zza)localObject2).d(), ((zza)localObject2).c());
          try
          {
            localObject6 = new URL(str);
            f().z().a("Fetching remote configuration", ((zza)localObject2).b());
            localObject7 = j().a(((zza)localObject2).b());
            localObject2 = localObject4;
            if (localObject7 != null)
            {
              localObject2 = localObject4;
              if (((zzqa.zzb)localObject7).a != null)
              {
                localObject2 = new ArrayMap();
                ((Map)localObject2).put("Config-Version", String.valueOf(((zzqa.zzb)localObject7).a));
              }
            }
            p().a((String)localObject5, (URL)localObject6, (Map)localObject2, new zzq.zza()
            {
              public void a(String paramAnonymousString, int paramAnonymousInt, Throwable paramAnonymousThrowable, byte[] paramAnonymousArrayOfByte)
              {
                zzw.a(zzw.this, paramAnonymousString, paramAnonymousInt, paramAnonymousThrowable, paramAnonymousArrayOfByte);
              }
            });
            return;
          }
          catch (MalformedURLException localMalformedURLException2)
          {
            f().b().a("Failed to parse config URL. Not fetching", str);
            return;
          }
        }
      }
    }
  }
  
  void D()
  {
    this.z += 1;
  }
  
  void a()
  {
    if (!this.v) {
      throw new IllegalStateException("AppMeasurement is not initialized");
    }
  }
  
  void a(AppMetadata paramAppMetadata)
  {
    y();
    a();
    zzx.a(paramAppMetadata.b);
    c(paramAppMetadata);
  }
  
  void a(EventParcel paramEventParcel, AppMetadata paramAppMetadata)
  {
    long l1 = System.nanoTime();
    y();
    a();
    String str = paramAppMetadata.b;
    zzx.a(str);
    if (TextUtils.isEmpty(paramAppMetadata.c)) {
      return;
    }
    if (!paramAppMetadata.i)
    {
      c(paramAppMetadata);
      return;
    }
    if (j().b(str, paramEventParcel.b))
    {
      f().z().a("Dropping blacklisted event", paramEventParcel.b);
      return;
    }
    if (f().a(2)) {
      f().z().a("Logging event", paramEventParcel);
    }
    o().b();
    Bundle localBundle;
    boolean bool2;
    boolean bool3;
    for (;;)
    {
      try
      {
        localBundle = paramEventParcel.c.b();
        c(paramAppMetadata);
        zzai localzzai;
        if (("_iap".equals(paramEventParcel.b)) || ("ecommerce_purchase".equals(paramEventParcel.b)))
        {
          localObject = localBundle.getString("currency");
          l2 = localBundle.getLong("value");
          if ((!TextUtils.isEmpty((CharSequence)localObject)) && (l2 > 0L))
          {
            localObject = ((String)localObject).toUpperCase(Locale.US);
            if (((String)localObject).matches("[A-Z]{3}"))
            {
              localObject = "_ltv_" + (String)localObject;
              localzzai = o().c(str, (String)localObject);
              if ((localzzai != null) && ((localzzai.d instanceof Long))) {
                continue;
              }
              o().a(str, d().b(str) - 1);
              localObject = new zzai(str, (String)localObject, r().a(), Long.valueOf(l2));
              o().a((zzai)localObject);
            }
          }
        }
        bool2 = zzaj.a(paramEventParcel.b);
        bool3 = zzaj.a(localBundle);
        localObject = o();
        l2 = A();
        boolean bool1;
        if ((bool2) && (bool3))
        {
          bool1 = true;
          localObject = ((zze)localObject).a(l2, str, bool2, bool1);
          l2 = ((zze.zza)localObject).b - d().B();
          if (l2 <= 0L) {
            break;
          }
          if (l2 % 1000L == 1L) {
            f().c().a("Data loss. Too many events logged. count", Long.valueOf(((zze.zza)localObject).b));
          }
          o().c();
          return;
          long l3 = ((Long)localzzai.d).longValue();
          localObject = new zzai(str, (String)localObject, r().a(), Long.valueOf(l2 + l3));
        }
        else
        {
          bool1 = false;
        }
      }
      finally
      {
        o().v();
      }
    }
    if (bool2)
    {
      l2 = ((zze.zza)localObject).a - d().C();
      if (l2 > 0L)
      {
        a(str, 2);
        if (l2 % 1000L == 1L) {
          f().c().a("Data loss. Too many public events logged. count", Long.valueOf(((zze.zza)localObject).a));
        }
        o().c();
        o().v();
        return;
      }
    }
    if ((bool2) && (bool3) && (((zze.zza)localObject).c - d().D() > 0L))
    {
      localBundle.remove("_c");
      a(localBundle, 4);
    }
    long l2 = o().c(str);
    if (l2 > 0L) {
      f().c().a("Data lost. Too many events stored on disk, deleted", Long.valueOf(l2));
    }
    paramEventParcel = new zzh(this, paramEventParcel.d, str, paramEventParcel.b, paramEventParcel.e, 0L, localBundle);
    Object localObject = o().a(str, paramEventParcel.b);
    if (localObject == null) {
      if (o().g(str) >= d().A())
      {
        f().c().a("Too many event names used, ignoring event. name, supported count", paramEventParcel.b, Integer.valueOf(d().A()));
        a(str, 1);
        o().v();
        return;
      }
    }
    for (localObject = new zzi(str, paramEventParcel.b, 0L, 0L, paramEventParcel.d);; localObject = ((zzi)localObject).a(paramEventParcel.d))
    {
      o().a((zzi)localObject);
      a(paramEventParcel, paramAppMetadata);
      o().c();
      if (f().a(2)) {
        f().z().a("Event recorded", paramEventParcel);
      }
      o().v();
      G();
      f().z().a("Background event processing time, ms", Long.valueOf((System.nanoTime() - l1 + 500000L) / 1000000L));
      return;
      paramEventParcel = paramEventParcel.a(this, ((zzi)localObject).e);
    }
  }
  
  void a(EventParcel paramEventParcel, String paramString)
  {
    zza localzza = o().b(paramString);
    if ((localzza == null) || (TextUtils.isEmpty(localzza.h())))
    {
      f().y().a("No app data available; dropping event", paramString);
      return;
    }
    try
    {
      String str = q().getPackageManager().getPackageInfo(paramString, 0).versionName;
      if ((localzza.h() != null) && (!localzza.h().equals(str)))
      {
        f().c().a("App version does not match; dropping event", paramString);
        return;
      }
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      f().c().a("Could not find package", paramString);
      a(paramEventParcel, new AppMetadata(paramString, localzza.d(), localzza.h(), localzza.i(), localzza.j(), localzza.k(), null, localzza.l(), false));
    }
  }
  
  /* Error */
  @WorkerThread
  void a(UserAttributeParcel paramUserAttributeParcel, AppMetadata paramAppMetadata)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 308	com/google/android/gms/measurement/internal/zzw:y	()V
    //   4: aload_0
    //   5: invokevirtual 314	com/google/android/gms/measurement/internal/zzw:a	()V
    //   8: aload_2
    //   9: getfield 660	com/google/android/gms/measurement/internal/AppMetadata:c	Ljava/lang/String;
    //   12: invokestatic 329	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   15: ifeq +4 -> 19
    //   18: return
    //   19: aload_2
    //   20: getfield 697	com/google/android/gms/measurement/internal/AppMetadata:i	Z
    //   23: ifne +9 -> 32
    //   26: aload_0
    //   27: aload_2
    //   28: invokespecial 853	com/google/android/gms/measurement/internal/zzw:c	(Lcom/google/android/gms/measurement/internal/AppMetadata;)V
    //   31: return
    //   32: aload_0
    //   33: invokevirtual 369	com/google/android/gms/measurement/internal/zzw:n	()Lcom/google/android/gms/measurement/internal/zzaj;
    //   36: aload_1
    //   37: getfield 1050	com/google/android/gms/measurement/internal/UserAttributeParcel:b	Ljava/lang/String;
    //   40: invokevirtual 1051	com/google/android/gms/measurement/internal/zzaj:c	(Ljava/lang/String;)V
    //   43: aload_0
    //   44: invokevirtual 369	com/google/android/gms/measurement/internal/zzw:n	()Lcom/google/android/gms/measurement/internal/zzaj;
    //   47: aload_1
    //   48: getfield 1050	com/google/android/gms/measurement/internal/UserAttributeParcel:b	Ljava/lang/String;
    //   51: aload_1
    //   52: invokevirtual 1053	com/google/android/gms/measurement/internal/UserAttributeParcel:a	()Ljava/lang/Object;
    //   55: invokevirtual 1056	com/google/android/gms/measurement/internal/zzaj:c	(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    //   58: astore 4
    //   60: aload 4
    //   62: ifnull -44 -> 18
    //   65: new 918	com/google/android/gms/measurement/internal/zzai
    //   68: dup
    //   69: aload_2
    //   70: getfield 648	com/google/android/gms/measurement/internal/AppMetadata:b	Ljava/lang/String;
    //   73: aload_1
    //   74: getfield 1050	com/google/android/gms/measurement/internal/UserAttributeParcel:b	Ljava/lang/String;
    //   77: aload_1
    //   78: getfield 1057	com/google/android/gms/measurement/internal/UserAttributeParcel:c	J
    //   81: aload 4
    //   83: invokespecial 928	com/google/android/gms/measurement/internal/zzai:<init>	(Ljava/lang/String;Ljava/lang/String;JLjava/lang/Object;)V
    //   86: astore_1
    //   87: aload_0
    //   88: invokevirtual 112	com/google/android/gms/measurement/internal/zzw:f	()Lcom/google/android/gms/measurement/internal/zzp;
    //   91: invokevirtual 144	com/google/android/gms/measurement/internal/zzp:y	()Lcom/google/android/gms/measurement/internal/zzp$zza;
    //   94: ldc_w 1059
    //   97: aload_1
    //   98: getfield 1060	com/google/android/gms/measurement/internal/zzai:b	Ljava/lang/String;
    //   101: aload 4
    //   103: invokevirtual 265	com/google/android/gms/measurement/internal/zzp$zza:a	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   106: aload_0
    //   107: invokevirtual 317	com/google/android/gms/measurement/internal/zzw:o	()Lcom/google/android/gms/measurement/internal/zze;
    //   110: invokevirtual 438	com/google/android/gms/measurement/internal/zze:b	()V
    //   113: aload_0
    //   114: aload_2
    //   115: invokespecial 853	com/google/android/gms/measurement/internal/zzw:c	(Lcom/google/android/gms/measurement/internal/AppMetadata;)V
    //   118: aload_0
    //   119: invokevirtual 317	com/google/android/gms/measurement/internal/zzw:o	()Lcom/google/android/gms/measurement/internal/zze;
    //   122: aload_1
    //   123: invokevirtual 931	com/google/android/gms/measurement/internal/zze:a	(Lcom/google/android/gms/measurement/internal/zzai;)Z
    //   126: istore_3
    //   127: aload_0
    //   128: invokevirtual 317	com/google/android/gms/measurement/internal/zzw:o	()Lcom/google/android/gms/measurement/internal/zze;
    //   131: invokevirtual 461	com/google/android/gms/measurement/internal/zze:c	()V
    //   134: iload_3
    //   135: ifeq +32 -> 167
    //   138: aload_0
    //   139: invokevirtual 112	com/google/android/gms/measurement/internal/zzw:f	()Lcom/google/android/gms/measurement/internal/zzp;
    //   142: invokevirtual 144	com/google/android/gms/measurement/internal/zzp:y	()Lcom/google/android/gms/measurement/internal/zzp$zza;
    //   145: ldc_w 1062
    //   148: aload_1
    //   149: getfield 1060	com/google/android/gms/measurement/internal/zzai:b	Ljava/lang/String;
    //   152: aload_1
    //   153: getfield 920	com/google/android/gms/measurement/internal/zzai:d	Ljava/lang/Object;
    //   156: invokevirtual 265	com/google/android/gms/measurement/internal/zzp$zza:a	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   159: aload_0
    //   160: invokevirtual 317	com/google/android/gms/measurement/internal/zzw:o	()Lcom/google/android/gms/measurement/internal/zze;
    //   163: invokevirtual 459	com/google/android/gms/measurement/internal/zze:v	()V
    //   166: return
    //   167: aload_0
    //   168: invokevirtual 112	com/google/android/gms/measurement/internal/zzw:f	()Lcom/google/android/gms/measurement/internal/zzp;
    //   171: invokevirtual 1064	com/google/android/gms/measurement/internal/zzp:w	()Lcom/google/android/gms/measurement/internal/zzp$zza;
    //   174: ldc_w 1066
    //   177: aload_1
    //   178: getfield 1060	com/google/android/gms/measurement/internal/zzai:b	Ljava/lang/String;
    //   181: aload_1
    //   182: getfield 920	com/google/android/gms/measurement/internal/zzai:d	Ljava/lang/Object;
    //   185: invokevirtual 265	com/google/android/gms/measurement/internal/zzp$zza:a	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   188: goto -29 -> 159
    //   191: astore_1
    //   192: aload_0
    //   193: invokevirtual 317	com/google/android/gms/measurement/internal/zzw:o	()Lcom/google/android/gms/measurement/internal/zze;
    //   196: invokevirtual 459	com/google/android/gms/measurement/internal/zze:v	()V
    //   199: aload_1
    //   200: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	201	0	this	zzw
    //   0	201	1	paramUserAttributeParcel	UserAttributeParcel
    //   0	201	2	paramAppMetadata	AppMetadata
    //   126	9	3	bool	boolean
    //   58	44	4	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   113	134	191	finally
    //   138	159	191	finally
    //   167	188	191	finally
  }
  
  void a(zzh paramzzh, AppMetadata paramAppMetadata)
  {
    y();
    a();
    zzx.a(paramzzh);
    zzx.a(paramAppMetadata);
    zzx.a(paramzzh.a);
    zzx.b(paramzzh.a.equals(paramAppMetadata.b));
    zzqb.zze localzze = new zzqb.zze();
    localzze.a = Integer.valueOf(1);
    localzze.i = "android";
    localzze.o = paramAppMetadata.b;
    localzze.n = paramAppMetadata.e;
    localzze.p = paramAppMetadata.d;
    localzze.q = Long.valueOf(paramAppMetadata.f);
    localzze.y = paramAppMetadata.c;
    if (paramAppMetadata.g == 0L) {}
    for (Object localObject = null;; localObject = Long.valueOf(paramAppMetadata.g))
    {
      localzze.v = ((Long)localObject);
      localObject = e().a(paramAppMetadata.b);
      if ((localObject != null) && (((Pair)localObject).first != null) && (((Pair)localObject).second != null))
      {
        localzze.s = ((String)((Pair)localObject).first);
        localzze.t = ((Boolean)((Pair)localObject).second);
      }
      localzze.k = t().b();
      localzze.j = t().c();
      localzze.m = Integer.valueOf((int)t().v());
      localzze.l = t().w();
      localzze.r = null;
      localzze.d = null;
      localzze.e = null;
      localzze.f = null;
      zza localzza = o().b(paramAppMetadata.b);
      localObject = localzza;
      if (localzza == null)
      {
        localObject = new zza(this, paramAppMetadata.b);
        ((zza)localObject).a(e().b());
        ((zza)localObject).b(paramAppMetadata.c);
        ((zza)localObject).c(e().b(paramAppMetadata.b));
        ((zza)localObject).e(0L);
        ((zza)localObject).a(0L);
        ((zza)localObject).b(0L);
        ((zza)localObject).d(paramAppMetadata.d);
        ((zza)localObject).e(paramAppMetadata.e);
        ((zza)localObject).c(paramAppMetadata.f);
        ((zza)localObject).d(paramAppMetadata.g);
        ((zza)localObject).a(paramAppMetadata.i);
        o().a((zza)localObject);
      }
      localzze.u = ((zza)localObject).c();
      paramAppMetadata = o().a(paramAppMetadata.b);
      localzze.c = new zzqb.zzg[paramAppMetadata.size()];
      int i1 = 0;
      while (i1 < paramAppMetadata.size())
      {
        localObject = new zzqb.zzg();
        localzze.c[i1] = localObject;
        ((zzqb.zzg)localObject).b = ((zzai)paramAppMetadata.get(i1)).b;
        ((zzqb.zzg)localObject).a = Long.valueOf(((zzai)paramAppMetadata.get(i1)).c);
        n().a((zzqb.zzg)localObject, ((zzai)paramAppMetadata.get(i1)).d);
        i1 += 1;
      }
    }
    try
    {
      long l1 = o().b(localzze);
      o().a(paramzzh, l1);
      return;
    }
    catch (IOException paramzzh)
    {
      f().b().a("Data loss. Failed to insert raw event metadata", paramzzh);
    }
  }
  
  void a(zzz paramzzz)
  {
    this.y += 1;
  }
  
  void a(String paramString, int paramInt) {}
  
  public void a(boolean paramBoolean)
  {
    G();
  }
  
  boolean a(long paramLong)
  {
    return a(null, paramLong);
  }
  
  /* Error */
  @WorkerThread
  public void b(AppMetadata paramAppMetadata)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 308	com/google/android/gms/measurement/internal/zzw:y	()V
    //   4: aload_0
    //   5: invokevirtual 314	com/google/android/gms/measurement/internal/zzw:a	()V
    //   8: aload_1
    //   9: invokestatic 75	com/google/android/gms/common/internal/zzx:a	(Ljava/lang/Object;)Ljava/lang/Object;
    //   12: pop
    //   13: aload_1
    //   14: getfield 648	com/google/android/gms/measurement/internal/AppMetadata:b	Ljava/lang/String;
    //   17: invokestatic 495	com/google/android/gms/common/internal/zzx:a	(Ljava/lang/String;)Ljava/lang/String;
    //   20: pop
    //   21: aload_1
    //   22: getfield 660	com/google/android/gms/measurement/internal/AppMetadata:c	Ljava/lang/String;
    //   25: invokestatic 329	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   28: ifeq +4 -> 32
    //   31: return
    //   32: aload_1
    //   33: getfield 697	com/google/android/gms/measurement/internal/AppMetadata:i	Z
    //   36: ifne +9 -> 45
    //   39: aload_0
    //   40: aload_1
    //   41: invokespecial 853	com/google/android/gms/measurement/internal/zzw:c	(Lcom/google/android/gms/measurement/internal/AppMetadata;)V
    //   44: return
    //   45: aload_0
    //   46: invokevirtual 383	com/google/android/gms/measurement/internal/zzw:r	()Lcom/google/android/gms/internal/zzmq;
    //   49: invokeinterface 386 1 0
    //   54: lstore_2
    //   55: aload_0
    //   56: invokevirtual 317	com/google/android/gms/measurement/internal/zzw:o	()Lcom/google/android/gms/measurement/internal/zze;
    //   59: invokevirtual 438	com/google/android/gms/measurement/internal/zze:b	()V
    //   62: aload_0
    //   63: invokevirtual 317	com/google/android/gms/measurement/internal/zzw:o	()Lcom/google/android/gms/measurement/internal/zze;
    //   66: aload_1
    //   67: getfield 648	com/google/android/gms/measurement/internal/AppMetadata:b	Ljava/lang/String;
    //   70: invokevirtual 498	com/google/android/gms/measurement/internal/zze:b	(Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/zza;
    //   73: astore 4
    //   75: aload 4
    //   77: ifnull +76 -> 153
    //   80: aload 4
    //   82: invokevirtual 681	com/google/android/gms/measurement/internal/zza:h	()Ljava/lang/String;
    //   85: ifnull +68 -> 153
    //   88: aload 4
    //   90: invokevirtual 681	com/google/android/gms/measurement/internal/zza:h	()Ljava/lang/String;
    //   93: aload_1
    //   94: getfield 679	com/google/android/gms/measurement/internal/AppMetadata:d	Ljava/lang/String;
    //   97: invokevirtual 668	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   100: ifne +53 -> 153
    //   103: new 470	android/os/Bundle
    //   106: dup
    //   107: invokespecial 1136	android/os/Bundle:<init>	()V
    //   110: astore 5
    //   112: aload 5
    //   114: ldc_w 1138
    //   117: aload 4
    //   119: invokevirtual 681	com/google/android/gms/measurement/internal/zza:h	()Ljava/lang/String;
    //   122: invokevirtual 1142	android/os/Bundle:putString	(Ljava/lang/String;Ljava/lang/String;)V
    //   125: aload_0
    //   126: new 861	com/google/android/gms/measurement/internal/EventParcel
    //   129: dup
    //   130: ldc_w 1144
    //   133: new 871	com/google/android/gms/measurement/internal/EventParams
    //   136: dup
    //   137: aload 5
    //   139: invokespecial 1147	com/google/android/gms/measurement/internal/EventParams:<init>	(Landroid/os/Bundle;)V
    //   142: ldc_w 1149
    //   145: lload_2
    //   146: invokespecial 1152	com/google/android/gms/measurement/internal/EventParcel:<init>	(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/EventParams;Ljava/lang/String;J)V
    //   149: aload_1
    //   150: invokevirtual 1046	com/google/android/gms/measurement/internal/zzw:a	(Lcom/google/android/gms/measurement/internal/EventParcel;Lcom/google/android/gms/measurement/internal/AppMetadata;)V
    //   153: aload_0
    //   154: aload_1
    //   155: invokespecial 853	com/google/android/gms/measurement/internal/zzw:c	(Lcom/google/android/gms/measurement/internal/AppMetadata;)V
    //   158: aload_0
    //   159: invokevirtual 317	com/google/android/gms/measurement/internal/zzw:o	()Lcom/google/android/gms/measurement/internal/zze;
    //   162: aload_1
    //   163: getfield 648	com/google/android/gms/measurement/internal/AppMetadata:b	Ljava/lang/String;
    //   166: ldc_w 1154
    //   169: invokevirtual 981	com/google/android/gms/measurement/internal/zze:a	(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/zzi;
    //   172: ifnonnull +97 -> 269
    //   175: aload_0
    //   176: new 1049	com/google/android/gms/measurement/internal/UserAttributeParcel
    //   179: dup
    //   180: ldc_w 1156
    //   183: lload_2
    //   184: lload_2
    //   185: ldc2_w 1157
    //   188: ldiv
    //   189: lconst_1
    //   190: ladd
    //   191: ldc2_w 1157
    //   194: lmul
    //   195: invokestatic 132	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   198: ldc_w 1149
    //   201: invokespecial 1161	com/google/android/gms/measurement/internal/UserAttributeParcel:<init>	(Ljava/lang/String;JLjava/lang/Object;Ljava/lang/String;)V
    //   204: aload_1
    //   205: invokevirtual 1163	com/google/android/gms/measurement/internal/zzw:a	(Lcom/google/android/gms/measurement/internal/UserAttributeParcel;Lcom/google/android/gms/measurement/internal/AppMetadata;)V
    //   208: new 470	android/os/Bundle
    //   211: dup
    //   212: invokespecial 1136	android/os/Bundle:<init>	()V
    //   215: astore 4
    //   217: aload 4
    //   219: ldc_w 960
    //   222: lconst_1
    //   223: invokevirtual 478	android/os/Bundle:putLong	(Ljava/lang/String;J)V
    //   226: aload_0
    //   227: new 861	com/google/android/gms/measurement/internal/EventParcel
    //   230: dup
    //   231: ldc_w 1154
    //   234: new 871	com/google/android/gms/measurement/internal/EventParams
    //   237: dup
    //   238: aload 4
    //   240: invokespecial 1147	com/google/android/gms/measurement/internal/EventParams:<init>	(Landroid/os/Bundle;)V
    //   243: ldc_w 1149
    //   246: lload_2
    //   247: invokespecial 1152	com/google/android/gms/measurement/internal/EventParcel:<init>	(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/EventParams;Ljava/lang/String;J)V
    //   250: aload_1
    //   251: invokevirtual 1046	com/google/android/gms/measurement/internal/zzw:a	(Lcom/google/android/gms/measurement/internal/EventParcel;Lcom/google/android/gms/measurement/internal/AppMetadata;)V
    //   254: aload_0
    //   255: invokevirtual 317	com/google/android/gms/measurement/internal/zzw:o	()Lcom/google/android/gms/measurement/internal/zze;
    //   258: invokevirtual 461	com/google/android/gms/measurement/internal/zze:c	()V
    //   261: aload_0
    //   262: invokevirtual 317	com/google/android/gms/measurement/internal/zzw:o	()Lcom/google/android/gms/measurement/internal/zze;
    //   265: invokevirtual 459	com/google/android/gms/measurement/internal/zze:v	()V
    //   268: return
    //   269: aload_1
    //   270: getfield 1165	com/google/android/gms/measurement/internal/AppMetadata:j	Z
    //   273: ifeq -19 -> 254
    //   276: aload_0
    //   277: new 861	com/google/android/gms/measurement/internal/EventParcel
    //   280: dup
    //   281: ldc_w 1167
    //   284: new 871	com/google/android/gms/measurement/internal/EventParams
    //   287: dup
    //   288: new 470	android/os/Bundle
    //   291: dup
    //   292: invokespecial 1136	android/os/Bundle:<init>	()V
    //   295: invokespecial 1147	com/google/android/gms/measurement/internal/EventParams:<init>	(Landroid/os/Bundle;)V
    //   298: ldc_w 1149
    //   301: lload_2
    //   302: invokespecial 1152	com/google/android/gms/measurement/internal/EventParcel:<init>	(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/EventParams;Ljava/lang/String;J)V
    //   305: aload_1
    //   306: invokevirtual 1046	com/google/android/gms/measurement/internal/zzw:a	(Lcom/google/android/gms/measurement/internal/EventParcel;Lcom/google/android/gms/measurement/internal/AppMetadata;)V
    //   309: goto -55 -> 254
    //   312: astore_1
    //   313: aload_0
    //   314: invokevirtual 317	com/google/android/gms/measurement/internal/zzw:o	()Lcom/google/android/gms/measurement/internal/zze;
    //   317: invokevirtual 459	com/google/android/gms/measurement/internal/zze:v	()V
    //   320: aload_1
    //   321: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	322	0	this	zzw
    //   0	322	1	paramAppMetadata	AppMetadata
    //   54	248	2	l1	long
    //   73	166	4	localObject	Object
    //   110	28	5	localBundle	Bundle
    // Exception table:
    //   from	to	target	type
    //   62	75	312	finally
    //   80	153	312	finally
    //   153	254	312	finally
    //   254	261	312	finally
    //   269	309	312	finally
  }
  
  @WorkerThread
  void b(UserAttributeParcel paramUserAttributeParcel, AppMetadata paramAppMetadata)
  {
    y();
    a();
    if (TextUtils.isEmpty(paramAppMetadata.c)) {
      return;
    }
    if (!paramAppMetadata.i)
    {
      c(paramAppMetadata);
      return;
    }
    f().y().a("Removing user property", paramUserAttributeParcel.b);
    o().b();
    try
    {
      c(paramAppMetadata);
      o().b(paramAppMetadata.b, paramUserAttributeParcel.b);
      o().c();
      f().y().a("User property removed", paramUserAttributeParcel.b);
      return;
    }
    finally
    {
      o().v();
    }
  }
  
  @WorkerThread
  protected boolean b()
  {
    boolean bool2 = true;
    a();
    y();
    if (this.w == null)
    {
      if ((!n().f("android.permission.INTERNET")) || (!n().f("android.permission.ACCESS_NETWORK_STATE")) || (!AppMeasurementReceiver.a(q())) || (!AppMeasurementService.a(q()))) {
        break label124;
      }
      bool1 = true;
      this.w = Boolean.valueOf(bool1);
      if ((this.w.booleanValue()) && (!d().N())) {
        if (TextUtils.isEmpty(u().c())) {
          break label129;
        }
      }
    }
    label124:
    label129:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      this.w = Boolean.valueOf(bool1);
      return this.w.booleanValue();
      bool1 = false;
      break;
    }
  }
  
  @WorkerThread
  protected void c()
  {
    y();
    if ((z()) && ((!this.g.E()) || (this.g.F())))
    {
      f().b().a("Scheduler shutting down before Scion.start() called");
      return;
    }
    o().y();
    if (!b()) {
      if (e().w())
      {
        if (!n().f("android.permission.INTERNET")) {
          f().b().a("App is missing INTERNET permission");
        }
        if (!n().f("android.permission.ACCESS_NETWORK_STATE")) {
          f().b().a("App is missing ACCESS_NETWORK_STATE permission");
        }
        if (!AppMeasurementReceiver.a(q())) {
          f().b().a("AppMeasurementReceiver not registered/enabled");
        }
        if (!AppMeasurementService.a(q())) {
          f().b().a("AppMeasurementService not registered/enabled");
        }
        f().b().a("Uploading is not possible. App measurement disabled");
      }
    }
    for (;;)
    {
      G();
      return;
      if ((!d().N()) && (!z()) && (!TextUtils.isEmpty(u().c()))) {
        l().c();
      }
    }
  }
  
  public zzd d()
  {
    return this.d;
  }
  
  public zzt e()
  {
    a(this.e);
    return this.e;
  }
  
  public zzp f()
  {
    b(this.f);
    return this.f;
  }
  
  public zzp g()
  {
    if ((this.f != null) && (this.f.E())) {
      return this.f;
    }
    return null;
  }
  
  public zzv h()
  {
    b(this.g);
    return this.g;
  }
  
  public zzad i()
  {
    b(this.h);
    return this.h;
  }
  
  public zzu j()
  {
    b(this.i);
    return this.i;
  }
  
  zzv k()
  {
    return this.g;
  }
  
  public zzab l()
  {
    b(this.q);
    return this.q;
  }
  
  public AppMeasurement m()
  {
    return this.j;
  }
  
  public zzaj n()
  {
    a(this.k);
    return this.k;
  }
  
  public zze o()
  {
    b(this.l);
    return this.l;
  }
  
  public zzq p()
  {
    b(this.m);
    return this.m;
  }
  
  public Context q()
  {
    return this.c;
  }
  
  public zzmq r()
  {
    return this.n;
  }
  
  public zzac s()
  {
    b(this.o);
    return this.o;
  }
  
  public zzg t()
  {
    b(this.p);
    return this.p;
  }
  
  public zzn u()
  {
    b(this.r);
    return this.r;
  }
  
  public zzr v()
  {
    if (this.s == null) {
      throw new IllegalStateException("Network broadcast receiver not created");
    }
    return this.s;
  }
  
  public zzag w()
  {
    b(this.t);
    return this.t;
  }
  
  public zzc x()
  {
    b(this.u);
    return this.u;
  }
  
  @WorkerThread
  public void y()
  {
    h().f();
  }
  
  protected boolean z()
  {
    return false;
  }
  
  private class zza
    implements zze.zzb
  {
    zzqb.zze a;
    List<Long> b;
    List<zzqb.zzb> c;
    long d;
    
    private zza() {}
    
    private long a(zzqb.zzb paramzzb)
    {
      return paramzzb.c.longValue() / 1000L / 60L / 60L;
    }
    
    public void a(zzqb.zze paramzze)
    {
      zzx.a(paramzze);
      this.a = paramzze;
    }
    
    boolean a()
    {
      return (this.c == null) || (this.c.isEmpty());
    }
    
    public boolean a(long paramLong, zzqb.zzb paramzzb)
    {
      zzx.a(paramzzb);
      if (this.c == null) {
        this.c = new ArrayList();
      }
      if (this.b == null) {
        this.b = new ArrayList();
      }
      if ((this.c.size() > 0) && (a((zzqb.zzb)this.c.get(0)) != a(paramzzb))) {
        return false;
      }
      long l = this.d + paramzzb.g();
      if (l >= zzw.this.d().T()) {
        return false;
      }
      this.d = l;
      this.c.add(paramzzb);
      this.b.add(Long.valueOf(paramLong));
      return this.c.size() < zzw.this.d().U();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */