package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzah
  implements Parcelable.Creator<UserAttributeParcel>
{
  static void a(UserAttributeParcel paramUserAttributeParcel, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramUserAttributeParcel.a);
    zzb.a(paramParcel, 2, paramUserAttributeParcel.b, false);
    zzb.a(paramParcel, 3, paramUserAttributeParcel.c);
    zzb.a(paramParcel, 4, paramUserAttributeParcel.d, false);
    zzb.a(paramParcel, 5, paramUserAttributeParcel.e, false);
    zzb.a(paramParcel, 6, paramUserAttributeParcel.f, false);
    zzb.a(paramParcel, 7, paramUserAttributeParcel.g, false);
    zzb.a(paramParcel, paramInt);
  }
  
  public UserAttributeParcel a(Parcel paramParcel)
  {
    String str1 = null;
    int j = zza.b(paramParcel);
    int i = 0;
    long l = 0L;
    String str2 = null;
    Float localFloat = null;
    Long localLong = null;
    String str3 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str3 = zza.p(paramParcel, k);
        break;
      case 3: 
        l = zza.i(paramParcel, k);
        break;
      case 4: 
        localLong = zza.j(paramParcel, k);
        break;
      case 5: 
        localFloat = zza.m(paramParcel, k);
        break;
      case 6: 
        str2 = zza.p(paramParcel, k);
        break;
      case 7: 
        str1 = zza.p(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new UserAttributeParcel(i, str3, l, localLong, localFloat, str2, str1);
  }
  
  public UserAttributeParcel[] a(int paramInt)
  {
    return new UserAttributeParcel[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzah.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */