package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class EventParcel
  implements SafeParcelable
{
  public static final zzk CREATOR = new zzk();
  public final int a;
  public final String b;
  public final EventParams c;
  public final String d;
  public final long e;
  
  EventParcel(int paramInt, String paramString1, EventParams paramEventParams, String paramString2, long paramLong)
  {
    this.a = paramInt;
    this.b = paramString1;
    this.c = paramEventParams;
    this.d = paramString2;
    this.e = paramLong;
  }
  
  public EventParcel(String paramString1, EventParams paramEventParams, String paramString2, long paramLong)
  {
    this.a = 1;
    this.b = paramString1;
    this.c = paramEventParams;
    this.d = paramString2;
    this.e = paramLong;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String toString()
  {
    return "origin=" + this.d + ",name=" + this.b + ",params=" + this.c;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzk.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/EventParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */