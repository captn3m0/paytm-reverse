package com.google.android.gms.measurement.internal;

import android.content.Context;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzmq;

class zzy
{
  protected final zzw n;
  
  zzy(zzw paramzzw)
  {
    zzx.a(paramzzw);
    this.n = paramzzw;
  }
  
  public void d()
  {
    this.n.B();
  }
  
  public void e()
  {
    this.n.h().e();
  }
  
  public void f()
  {
    this.n.h().f();
  }
  
  public zzc g()
  {
    return this.n.x();
  }
  
  public zzab h()
  {
    return this.n.l();
  }
  
  public zzn i()
  {
    return this.n.u();
  }
  
  public zzg j()
  {
    return this.n.t();
  }
  
  public zzac k()
  {
    return this.n.s();
  }
  
  public zzmq l()
  {
    return this.n.r();
  }
  
  public Context m()
  {
    return this.n.q();
  }
  
  public zze n()
  {
    return this.n.o();
  }
  
  public zzaj o()
  {
    return this.n.n();
  }
  
  public zzu p()
  {
    return this.n.j();
  }
  
  public zzad q()
  {
    return this.n.i();
  }
  
  public zzv r()
  {
    return this.n.h();
  }
  
  public zzp s()
  {
    return this.n.f();
  }
  
  public zzt t()
  {
    return this.n.e();
  }
  
  public zzd u()
  {
    return this.n.d();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */