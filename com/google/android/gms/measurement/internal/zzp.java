package com.google.android.gms.measurement.internal;

import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.measurement.AppMeasurement;

public class zzp
  extends zzz
{
  private final String a = u().a();
  private final char b;
  private final long c = u().M();
  private final zza d;
  private final zza e;
  private final zza f;
  private final zza g;
  private final zza h;
  private final zza i;
  private final zza j;
  private final zza k;
  private final zza l;
  
  zzp(zzw paramzzw)
  {
    super(paramzzw);
    if (u().O())
    {
      if (u().N()) {}
      for (c1 = 'P';; c1 = 'C')
      {
        this.b = c1;
        this.d = new zza(6, false, false);
        this.e = new zza(6, true, false);
        this.f = new zza(6, false, true);
        this.g = new zza(5, false, false);
        this.h = new zza(5, true, false);
        this.i = new zza(5, false, true);
        this.j = new zza(4, false, false);
        this.k = new zza(3, false, false);
        this.l = new zza(2, false, false);
        return;
      }
    }
    if (u().N()) {}
    for (char c1 = 'p';; c1 = 'c')
    {
      this.b = c1;
      break;
    }
  }
  
  private static String a(String paramString)
  {
    String str;
    if (TextUtils.isEmpty(paramString)) {
      str = "";
    }
    int m;
    do
    {
      return str;
      m = paramString.lastIndexOf('.');
      str = paramString;
    } while (m == -1);
    return paramString.substring(0, m);
  }
  
  static String a(boolean paramBoolean, Object paramObject)
  {
    if (paramObject == null) {
      return "";
    }
    if ((paramObject instanceof Integer)) {
      paramObject = Long.valueOf(((Integer)paramObject).intValue());
    }
    for (;;)
    {
      String str1;
      if ((paramObject instanceof Long))
      {
        if (!paramBoolean) {
          return String.valueOf(paramObject);
        }
        if (Math.abs(((Long)paramObject).longValue()) < 100L) {
          return String.valueOf(paramObject);
        }
        if (String.valueOf(paramObject).charAt(0) == '-') {}
        for (str1 = "-";; str1 = "")
        {
          paramObject = String.valueOf(Math.abs(((Long)paramObject).longValue()));
          return str1 + Math.round(Math.pow(10.0D, ((String)paramObject).length() - 1)) + "..." + str1 + Math.round(Math.pow(10.0D, ((String)paramObject).length()) - 1.0D);
        }
      }
      if ((paramObject instanceof Boolean)) {
        return String.valueOf(paramObject);
      }
      if ((paramObject instanceof Throwable))
      {
        Object localObject1 = (Throwable)paramObject;
        paramObject = new StringBuilder(((Throwable)localObject1).toString());
        str1 = a(AppMeasurement.class.getCanonicalName());
        String str2 = a(zzw.class.getCanonicalName());
        localObject1 = ((Throwable)localObject1).getStackTrace();
        int n = localObject1.length;
        int m = 0;
        if (m < n)
        {
          Object localObject2 = localObject1[m];
          if (((StackTraceElement)localObject2).isNativeMethod()) {}
          String str3;
          do
          {
            do
            {
              m += 1;
              break;
              str3 = ((StackTraceElement)localObject2).getClassName();
            } while (str3 == null);
            str3 = a(str3);
          } while ((!str3.equals(str1)) && (!str3.equals(str2)));
          ((StringBuilder)paramObject).append(": ");
          ((StringBuilder)paramObject).append(localObject2);
        }
        return ((StringBuilder)paramObject).toString();
      }
      if (paramBoolean) {
        return "-";
      }
      return String.valueOf(paramObject);
    }
  }
  
  static String a(boolean paramBoolean, String paramString, Object paramObject1, Object paramObject2, Object paramObject3)
  {
    String str1 = paramString;
    if (paramString == null) {
      str1 = "";
    }
    String str2 = a(paramBoolean, paramObject1);
    paramObject2 = a(paramBoolean, paramObject2);
    paramObject3 = a(paramBoolean, paramObject3);
    StringBuilder localStringBuilder = new StringBuilder();
    paramString = "";
    if (!TextUtils.isEmpty(str1))
    {
      localStringBuilder.append(str1);
      paramString = ": ";
    }
    paramObject1 = paramString;
    if (!TextUtils.isEmpty(str2))
    {
      localStringBuilder.append(paramString);
      localStringBuilder.append(str2);
      paramObject1 = ", ";
    }
    paramString = (String)paramObject1;
    if (!TextUtils.isEmpty((CharSequence)paramObject2))
    {
      localStringBuilder.append((String)paramObject1);
      localStringBuilder.append((String)paramObject2);
      paramString = ", ";
    }
    if (!TextUtils.isEmpty((CharSequence)paramObject3))
    {
      localStringBuilder.append(paramString);
      localStringBuilder.append((String)paramObject3);
    }
    return localStringBuilder.toString();
  }
  
  public String A()
  {
    Pair localPair = t().b.a();
    if (localPair == null) {
      return null;
    }
    return String.valueOf(localPair.second) + ":" + (String)localPair.first;
  }
  
  protected void a() {}
  
  protected void a(int paramInt, String paramString)
  {
    Log.println(paramInt, this.a, paramString);
  }
  
  public void a(int paramInt, final String paramString, Object paramObject1, Object paramObject2, Object paramObject3)
  {
    zzx.a(paramString);
    zzv localzzv = this.n.k();
    if (localzzv == null)
    {
      a(6, "Scheduler not set. Not logging error/warn.");
      return;
    }
    if (!localzzv.E())
    {
      a(6, "Scheduler not initialized. Not logging error/warn.");
      return;
    }
    if (localzzv.F())
    {
      a(6, "Scheduler shutdown. Not logging error/warn.");
      return;
    }
    int m = paramInt;
    if (paramInt < 0) {
      m = 0;
    }
    paramInt = m;
    if (m >= "01VDIWEA?".length()) {
      paramInt = "01VDIWEA?".length() - 1;
    }
    paramObject1 = "1" + "01VDIWEA?".charAt(paramInt) + this.b + this.c + ":" + a(true, paramString, paramObject1, paramObject2, paramObject3);
    if (((String)paramObject1).length() > 1024) {}
    for (paramString = paramString.substring(0, 1024);; paramString = (String)paramObject1)
    {
      localzzv.a(new Runnable()
      {
        public void run()
        {
          zzt localzzt = zzp.this.n.e();
          if ((!localzzt.E()) || (localzzt.F()))
          {
            zzp.this.a(6, "Persisted config not initialized . Not logging error/warn.");
            return;
          }
          localzzt.b.a(paramString);
        }
      });
      return;
    }
  }
  
  protected void a(int paramInt, boolean paramBoolean1, boolean paramBoolean2, String paramString, Object paramObject1, Object paramObject2, Object paramObject3)
  {
    if ((!paramBoolean1) && (a(paramInt))) {
      a(paramInt, a(false, paramString, paramObject1, paramObject2, paramObject3));
    }
    if ((!paramBoolean2) && (paramInt >= 5)) {
      a(paramInt, paramString, paramObject1, paramObject2, paramObject3);
    }
  }
  
  protected boolean a(int paramInt)
  {
    return Log.isLoggable(this.a, paramInt);
  }
  
  public zza b()
  {
    return this.d;
  }
  
  public zza c()
  {
    return this.g;
  }
  
  public zza v()
  {
    return this.h;
  }
  
  public zza w()
  {
    return this.i;
  }
  
  public zza x()
  {
    return this.j;
  }
  
  public zza y()
  {
    return this.k;
  }
  
  public zza z()
  {
    return this.l;
  }
  
  public class zza
  {
    private final int b;
    private final boolean c;
    private final boolean d;
    
    zza(int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    {
      this.b = paramInt;
      this.c = paramBoolean1;
      this.d = paramBoolean2;
    }
    
    public void a(String paramString)
    {
      zzp.this.a(this.b, this.c, this.d, paramString, null, null, null);
    }
    
    public void a(String paramString, Object paramObject)
    {
      zzp.this.a(this.b, this.c, this.d, paramString, paramObject, null, null);
    }
    
    public void a(String paramString, Object paramObject1, Object paramObject2)
    {
      zzp.this.a(this.b, this.c, this.d, paramString, paramObject1, paramObject2, null);
    }
    
    public void a(String paramString, Object paramObject1, Object paramObject2, Object paramObject3)
    {
      zzp.this.a(this.b, this.c, this.d, paramString, paramObject1, paramObject2, paramObject3);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */