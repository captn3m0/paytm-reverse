package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;
import java.util.Iterator;
import java.util.Set;

public class EventParams
  implements SafeParcelable, Iterable<String>
{
  public static final zzj CREATOR = new zzj();
  public final int a;
  private final Bundle b;
  
  EventParams(int paramInt, Bundle paramBundle)
  {
    this.a = paramInt;
    this.b = paramBundle;
  }
  
  EventParams(Bundle paramBundle)
  {
    zzx.a(paramBundle);
    this.b = paramBundle;
    this.a = 1;
  }
  
  public int a()
  {
    return this.b.size();
  }
  
  Object a(String paramString)
  {
    return this.b.get(paramString);
  }
  
  public Bundle b()
  {
    return new Bundle(this.b);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Iterator<String> iterator()
  {
    new Iterator()
    {
      Iterator<String> a = EventParams.a(EventParams.this).keySet().iterator();
      
      public String a()
      {
        return (String)this.a.next();
      }
      
      public boolean hasNext()
      {
        return this.a.hasNext();
      }
      
      public void remove()
      {
        throw new UnsupportedOperationException("Remove not supported");
      }
    };
  }
  
  public String toString()
  {
    return this.b.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzj.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/EventParams.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */