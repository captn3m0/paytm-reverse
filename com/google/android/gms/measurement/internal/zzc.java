package com.google.android.gms.measurement.internal;

import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzpz.zza;
import com.google.android.gms.internal.zzpz.zzb;
import com.google.android.gms.internal.zzpz.zzc;
import com.google.android.gms.internal.zzpz.zzd;
import com.google.android.gms.internal.zzpz.zze;
import com.google.android.gms.internal.zzqb.zza;
import com.google.android.gms.internal.zzqb.zzb;
import com.google.android.gms.internal.zzqb.zzc;
import com.google.android.gms.internal.zzqb.zzf;
import com.google.android.gms.internal.zzqb.zzg;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

class zzc
  extends zzz
{
  zzc(zzw paramzzw)
  {
    super(paramzzw);
  }
  
  private Boolean a(zzpz.zzb paramzzb, zzqb.zzb paramzzb1, long paramLong)
  {
    if (paramzzb.e != null)
    {
      localObject1 = new zzs(paramzzb.e).a(paramLong);
      if (localObject1 == null) {
        return null;
      }
      if (!((Boolean)localObject1).booleanValue()) {
        return Boolean.valueOf(false);
      }
    }
    Object localObject2 = new HashSet();
    Object localObject1 = paramzzb.c;
    int j = localObject1.length;
    int i = 0;
    while (i < j)
    {
      localObject3 = localObject1[i];
      if (TextUtils.isEmpty(((zzpz.zzc)localObject3).d))
      {
        s().c().a("null or empty param name in filter. event", paramzzb1.b);
        return null;
      }
      ((Set)localObject2).add(((zzpz.zzc)localObject3).d);
      i += 1;
    }
    localObject1 = new ArrayMap();
    Object localObject3 = paramzzb1.a;
    j = localObject3.length;
    i = 0;
    Object localObject4;
    if (i < j)
    {
      localObject4 = localObject3[i];
      if (((Set)localObject2).contains(((zzqb.zzc)localObject4).a))
      {
        if (((zzqb.zzc)localObject4).c == null) {
          break label219;
        }
        ((Map)localObject1).put(((zzqb.zzc)localObject4).a, ((zzqb.zzc)localObject4).c);
      }
      for (;;)
      {
        i += 1;
        break;
        label219:
        if (((zzqb.zzc)localObject4).d != null)
        {
          ((Map)localObject1).put(((zzqb.zzc)localObject4).a, ((zzqb.zzc)localObject4).d);
        }
        else
        {
          if (((zzqb.zzc)localObject4).b == null) {
            break label277;
          }
          ((Map)localObject1).put(((zzqb.zzc)localObject4).a, ((zzqb.zzc)localObject4).b);
        }
      }
      label277:
      s().c().a("Unknown value for param. event, param", paramzzb1.b, ((zzqb.zzc)localObject4).a);
      return null;
    }
    paramzzb = paramzzb.c;
    j = paramzzb.length;
    i = 0;
    while (i < j)
    {
      localObject2 = paramzzb[i];
      localObject3 = ((zzpz.zzc)localObject2).d;
      if (TextUtils.isEmpty((CharSequence)localObject3))
      {
        s().c().a("Event has empty param name. event", paramzzb1.b);
        return null;
      }
      localObject4 = ((Map)localObject1).get(localObject3);
      if ((localObject4 instanceof Long))
      {
        if (((zzpz.zzc)localObject2).b == null)
        {
          s().c().a("No number filter for long param. event, param", paramzzb1.b, localObject3);
          return null;
        }
        localObject2 = new zzs(((zzpz.zzc)localObject2).b).a(((Long)localObject4).longValue());
        if (localObject2 == null) {
          return null;
        }
        if (!((Boolean)localObject2).booleanValue()) {
          return Boolean.valueOf(false);
        }
      }
      else if ((localObject4 instanceof Float))
      {
        if (((zzpz.zzc)localObject2).b == null)
        {
          s().c().a("No number filter for float param. event, param", paramzzb1.b, localObject3);
          return null;
        }
        localObject2 = new zzs(((zzpz.zzc)localObject2).b).a(((Float)localObject4).floatValue());
        if (localObject2 == null) {
          return null;
        }
        if (!((Boolean)localObject2).booleanValue()) {
          return Boolean.valueOf(false);
        }
      }
      else if ((localObject4 instanceof String))
      {
        if (((zzpz.zzc)localObject2).a == null)
        {
          s().c().a("No string filter for String param. event, param", paramzzb1.b, localObject3);
          return null;
        }
        localObject2 = new zzae(((zzpz.zzc)localObject2).a).a((String)localObject4);
        if (localObject2 == null) {
          return null;
        }
        if (!((Boolean)localObject2).booleanValue()) {
          return Boolean.valueOf(false);
        }
      }
      else
      {
        if (localObject4 == null)
        {
          s().z().a("Missing param for filter. event, param", paramzzb1.b, localObject3);
          return Boolean.valueOf(false);
        }
        s().c().a("Unknown param type. event, param", paramzzb1.b, localObject3);
        return null;
      }
      i += 1;
    }
    return Boolean.valueOf(true);
  }
  
  private Boolean a(zzpz.zze paramzze, zzqb.zzg paramzzg)
  {
    paramzze = paramzze.c;
    if (paramzze == null)
    {
      s().c().a("Missing property filter. property", paramzzg.b);
      return null;
    }
    if (paramzzg.d != null)
    {
      if (paramzze.b == null)
      {
        s().c().a("No number filter for long property. property", paramzzg.b);
        return null;
      }
      return new zzs(paramzze.b).a(paramzzg.d.longValue());
    }
    if (paramzzg.e != null)
    {
      if (paramzze.b == null)
      {
        s().c().a("No number filter for float property. property", paramzzg.b);
        return null;
      }
      return new zzs(paramzze.b).a(paramzzg.e.floatValue());
    }
    if (paramzzg.c != null)
    {
      if (paramzze.a == null)
      {
        if (paramzze.b == null)
        {
          s().c().a("No string or number filter defined. property", paramzzg.b);
          return null;
        }
        zzs localzzs = new zzs(paramzze.b);
        if (!paramzze.b.b.booleanValue())
        {
          if (a(paramzzg.c)) {
            try
            {
              paramzze = localzzs.a(Long.parseLong(paramzzg.c));
              return paramzze;
            }
            catch (NumberFormatException paramzze)
            {
              s().c().a("User property value exceeded Long value range. property, value", paramzzg.b, paramzzg.c);
              return null;
            }
          }
          s().c().a("Invalid user property value for Long number filter. property, value", paramzzg.b, paramzzg.c);
          return null;
        }
        if (b(paramzzg.c)) {
          try
          {
            float f = Float.parseFloat(paramzzg.c);
            if (!Float.isInfinite(f)) {
              return localzzs.a(f);
            }
            s().c().a("User property value exceeded Float value range. property, value", paramzzg.b, paramzzg.c);
            return null;
          }
          catch (NumberFormatException paramzze)
          {
            s().c().a("User property value exceeded Float value range. property, value", paramzzg.b, paramzzg.c);
            return null;
          }
        }
        s().c().a("Invalid user property value for Float number filter. property, value", paramzzg.b, paramzzg.c);
        return null;
      }
      return new zzae(paramzze.a).a(paramzzg.c);
    }
    s().c().a("User property has no value, property", paramzzg.b);
    return null;
  }
  
  protected void a() {}
  
  void a(String paramString, zzpz.zza[] paramArrayOfzza)
  {
    n().a(paramString, paramArrayOfzza);
  }
  
  boolean a(String paramString)
  {
    return Pattern.matches("[+-]?[0-9]+", paramString);
  }
  
  zzqb.zza[] a(String paramString, zzqb.zzb[] paramArrayOfzzb, zzqb.zzg[] paramArrayOfzzg)
  {
    zzx.a(paramString);
    HashSet localHashSet = new HashSet();
    ArrayMap localArrayMap1 = new ArrayMap();
    ArrayMap localArrayMap2 = new ArrayMap();
    ArrayMap localArrayMap3 = new ArrayMap();
    Object localObject6;
    int k;
    int i;
    Object localObject7;
    Object localObject1;
    long l;
    Object localObject2;
    if (paramArrayOfzzb != null)
    {
      localObject6 = new ArrayMap();
      k = paramArrayOfzzb.length;
      i = 0;
      if (i < k)
      {
        localObject7 = paramArrayOfzzb[i];
        localObject1 = n().a(paramString, ((zzqb.zzb)localObject7).b);
        if (localObject1 == null)
        {
          s().c().a("Event aggregate wasn't created during raw event logging. event", ((zzqb.zzb)localObject7).b);
          localObject1 = new zzi(paramString, ((zzqb.zzb)localObject7).b, 1L, 1L, ((zzqb.zzb)localObject7).c.longValue());
          n().a((zzi)localObject1);
          l = ((zzi)localObject1).c;
          localObject1 = (Map)((Map)localObject6).get(((zzqb.zzb)localObject7).b);
          if (localObject1 != null) {
            break label1816;
          }
          localObject2 = n().d(paramString, ((zzqb.zzb)localObject7).b);
          localObject1 = localObject2;
          if (localObject2 == null) {
            localObject1 = new ArrayMap();
          }
          ((Map)localObject6).put(((zzqb.zzb)localObject7).b, localObject1);
        }
      }
    }
    label264:
    label845:
    label994:
    label1262:
    label1589:
    label1804:
    label1807:
    label1810:
    label1813:
    label1816:
    for (;;)
    {
      s().z().a("Found audiences. event, audience count", ((zzqb.zzb)localObject7).b, Integer.valueOf(((Map)localObject1).size()));
      Object localObject8 = ((Map)localObject1).keySet().iterator();
      int m;
      Object localObject4;
      for (;;)
      {
        if (((Iterator)localObject8).hasNext())
        {
          m = ((Integer)((Iterator)localObject8).next()).intValue();
          if (localHashSet.contains(Integer.valueOf(m)))
          {
            s().z().a("Skipping failed audience ID", Integer.valueOf(m));
            continue;
            localObject1 = ((zzi)localObject1).a();
            break;
          }
          localObject4 = (zzqb.zza)localArrayMap1.get(Integer.valueOf(m));
          if (localObject4 != null) {
            break label1813;
          }
          localObject4 = new zzqb.zza();
          localArrayMap1.put(Integer.valueOf(m), localObject4);
          ((zzqb.zza)localObject4).d = Boolean.valueOf(false);
        }
      }
      for (;;)
      {
        Object localObject9 = (List)((Map)localObject1).get(Integer.valueOf(m));
        Object localObject5 = (BitSet)localArrayMap2.get(Integer.valueOf(m));
        Object localObject3 = (BitSet)localArrayMap3.get(Integer.valueOf(m));
        localObject2 = localObject5;
        if (localObject5 == null)
        {
          localObject2 = new BitSet();
          localArrayMap2.put(Integer.valueOf(m), localObject2);
          localObject3 = new BitSet();
          localArrayMap3.put(Integer.valueOf(m), localObject3);
        }
        if ((((zzqb.zza)localObject4).c == null) && (!((zzqb.zza)localObject4).d.booleanValue()))
        {
          localObject5 = n().c(paramString, m);
          if (localObject5 == null) {
            ((zzqb.zza)localObject4).d = Boolean.valueOf(true);
          }
        }
        else
        {
          localObject4 = ((List)localObject9).iterator();
        }
        int j;
        for (;;)
        {
          if (!((Iterator)localObject4).hasNext()) {
            break label845;
          }
          localObject5 = (zzpz.zzb)((Iterator)localObject4).next();
          if (s().a(2))
          {
            s().z().a("Evaluating filter. audience, filter, event", Integer.valueOf(m), ((zzpz.zzb)localObject5).a, ((zzpz.zzb)localObject5).b);
            s().z().a("Filter definition", localObject5);
          }
          if (((zzpz.zzb)localObject5).a.intValue() > 256)
          {
            s().c().a("Invalid event filter ID > 256. id", ((zzpz.zzb)localObject5).a);
            continue;
            ((zzqb.zza)localObject4).c = ((zzqb.zzf)localObject5);
            j = 0;
            while (j < ((zzqb.zzf)localObject5).b.length * 64)
            {
              if (zzaj.a(((zzqb.zzf)localObject5).b, j))
              {
                s().z().a("Event filter already evaluated true. audience ID, filter ID", Integer.valueOf(m), Integer.valueOf(j));
                ((BitSet)localObject2).set(j);
                ((BitSet)localObject3).set(j);
              }
              j += 1;
            }
            break;
          }
          if (!((BitSet)localObject3).get(((zzpz.zzb)localObject5).a.intValue()))
          {
            localObject9 = a((zzpz.zzb)localObject5, (zzqb.zzb)localObject7, l);
            s().z().a("Event filter result", localObject9);
            if (localObject9 == null)
            {
              localHashSet.add(Integer.valueOf(m));
            }
            else
            {
              ((BitSet)localObject3).set(((zzpz.zzb)localObject5).a.intValue());
              if (((Boolean)localObject9).booleanValue()) {
                ((BitSet)localObject2).set(((zzpz.zzb)localObject5).a.intValue());
              }
            }
          }
        }
        break label264;
        i += 1;
        break;
        if (paramArrayOfzzg != null)
        {
          localObject5 = new ArrayMap();
          k = paramArrayOfzzg.length;
          i = 0;
          if (i < k)
          {
            localObject6 = paramArrayOfzzg[i];
            paramArrayOfzzb = (Map)((Map)localObject5).get(((zzqb.zzg)localObject6).b);
            if (paramArrayOfzzb != null) {
              break label1810;
            }
            localObject1 = n().e(paramString, ((zzqb.zzg)localObject6).b);
            paramArrayOfzzb = (zzqb.zzb[])localObject1;
            if (localObject1 == null) {
              paramArrayOfzzb = new ArrayMap();
            }
            ((Map)localObject5).put(((zzqb.zzg)localObject6).b, paramArrayOfzzb);
          }
        }
        for (;;)
        {
          s().z().a("Found audiences. property, audience count", ((zzqb.zzg)localObject6).b, Integer.valueOf(paramArrayOfzzb.size()));
          localObject7 = paramArrayOfzzb.keySet().iterator();
          while (((Iterator)localObject7).hasNext())
          {
            m = ((Integer)((Iterator)localObject7).next()).intValue();
            if (localHashSet.contains(Integer.valueOf(m)))
            {
              s().z().a("Skipping failed audience ID", Integer.valueOf(m));
            }
            else
            {
              localObject3 = (zzqb.zza)localArrayMap1.get(Integer.valueOf(m));
              if (localObject3 != null) {
                break label1807;
              }
              localObject3 = new zzqb.zza();
              localArrayMap1.put(Integer.valueOf(m), localObject3);
              ((zzqb.zza)localObject3).d = Boolean.valueOf(false);
            }
          }
          for (;;)
          {
            localObject8 = (List)paramArrayOfzzb.get(Integer.valueOf(m));
            localObject4 = (BitSet)localArrayMap2.get(Integer.valueOf(m));
            localObject2 = (BitSet)localArrayMap3.get(Integer.valueOf(m));
            localObject1 = localObject4;
            if (localObject4 == null)
            {
              localObject1 = new BitSet();
              localArrayMap2.put(Integer.valueOf(m), localObject1);
              localObject2 = new BitSet();
              localArrayMap3.put(Integer.valueOf(m), localObject2);
            }
            if ((((zzqb.zza)localObject3).c == null) && (!((zzqb.zza)localObject3).d.booleanValue()))
            {
              localObject4 = n().c(paramString, m);
              if (localObject4 == null) {
                ((zzqb.zza)localObject3).d = Boolean.valueOf(true);
              }
            }
            else
            {
              localObject3 = ((List)localObject8).iterator();
            }
            for (;;)
            {
              if (!((Iterator)localObject3).hasNext()) {
                break label1589;
              }
              localObject4 = (zzpz.zze)((Iterator)localObject3).next();
              if (s().a(2))
              {
                s().z().a("Evaluating filter. audience, filter, property", Integer.valueOf(m), ((zzpz.zze)localObject4).a, ((zzpz.zze)localObject4).b);
                s().z().a("Filter definition", localObject4);
              }
              if ((((zzpz.zze)localObject4).a == null) || (((zzpz.zze)localObject4).a.intValue() > 256))
              {
                s().c().a("Invalid property filter ID. id", String.valueOf(((zzpz.zze)localObject4).a));
                localHashSet.add(Integer.valueOf(m));
                break;
                ((zzqb.zza)localObject3).c = ((zzqb.zzf)localObject4);
                j = 0;
                while (j < ((zzqb.zzf)localObject4).b.length * 64)
                {
                  if (zzaj.a(((zzqb.zzf)localObject4).b, j))
                  {
                    ((BitSet)localObject1).set(j);
                    ((BitSet)localObject2).set(j);
                  }
                  j += 1;
                }
                break label1262;
              }
              if (((BitSet)localObject2).get(((zzpz.zze)localObject4).a.intValue()))
              {
                s().z().a("Property filter already evaluated true. audience ID, filter ID", Integer.valueOf(m), ((zzpz.zze)localObject4).a);
              }
              else
              {
                localObject8 = a((zzpz.zze)localObject4, (zzqb.zzg)localObject6);
                s().z().a("Property filter result", localObject8);
                if (localObject8 == null)
                {
                  localHashSet.add(Integer.valueOf(m));
                }
                else
                {
                  ((BitSet)localObject2).set(((zzpz.zze)localObject4).a.intValue());
                  if (((Boolean)localObject8).booleanValue()) {
                    ((BitSet)localObject1).set(((zzpz.zze)localObject4).a.intValue());
                  }
                }
              }
            }
            break label994;
            i += 1;
            break;
            paramArrayOfzzg = new zzqb.zza[localArrayMap2.size()];
            localObject1 = localArrayMap2.keySet().iterator();
            i = 0;
            while (((Iterator)localObject1).hasNext())
            {
              j = ((Integer)((Iterator)localObject1).next()).intValue();
              if (!localHashSet.contains(Integer.valueOf(j)))
              {
                paramArrayOfzzb = (zzqb.zza)localArrayMap1.get(Integer.valueOf(j));
                if (paramArrayOfzzb != null) {
                  break label1804;
                }
                paramArrayOfzzb = new zzqb.zza();
              }
            }
            for (;;)
            {
              paramArrayOfzzg[i] = paramArrayOfzzb;
              paramArrayOfzzb.a = Integer.valueOf(j);
              paramArrayOfzzb.b = new zzqb.zzf();
              paramArrayOfzzb.b.b = zzaj.a((BitSet)localArrayMap2.get(Integer.valueOf(j)));
              paramArrayOfzzb.b.a = zzaj.a((BitSet)localArrayMap3.get(Integer.valueOf(j)));
              n().a(paramString, j, paramArrayOfzzb.b);
              i += 1;
              break;
              return (zzqb.zza[])Arrays.copyOf(paramArrayOfzzg, i);
            }
          }
        }
      }
    }
  }
  
  boolean b(String paramString)
  {
    return Pattern.matches("[+-]?(([0-9]+\\.?)|([0-9]*\\.[0-9]+))", paramString);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */