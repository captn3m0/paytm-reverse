package com.google.android.gms.measurement.internal;

abstract class zzz
  extends zzy
{
  private boolean a;
  private boolean b;
  private boolean c;
  
  zzz(zzw paramzzw)
  {
    super(paramzzw);
    this.n.a(this);
  }
  
  boolean E()
  {
    return (this.a) && (!this.b);
  }
  
  boolean F()
  {
    return this.c;
  }
  
  protected void G()
  {
    if (!E()) {
      throw new IllegalStateException("Not initialized");
    }
  }
  
  public final void H()
  {
    if (this.a) {
      throw new IllegalStateException("Can't initialize twice");
    }
    a();
    this.n.D();
    this.a = true;
  }
  
  protected abstract void a();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */