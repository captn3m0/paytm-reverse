package com.google.android.gms.measurement.internal;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzmq;
import com.google.android.gms.measurement.AppMeasurementReceiver;
import com.google.android.gms.measurement.AppMeasurementService;

public class zzag
  extends zzz
{
  private boolean a;
  private final AlarmManager b = (AlarmManager)m().getSystemService("alarm");
  
  protected zzag(zzw paramzzw)
  {
    super(paramzzw);
  }
  
  private PendingIntent b()
  {
    Intent localIntent = new Intent(m(), AppMeasurementReceiver.class);
    localIntent.setAction("com.google.android.gms.measurement.UPLOAD");
    return PendingIntent.getBroadcast(m(), 0, localIntent, 0);
  }
  
  protected void a()
  {
    this.b.cancel(b());
  }
  
  public void a(long paramLong)
  {
    G();
    if (paramLong > 0L) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.b(bool);
      zzx.a(AppMeasurementReceiver.a(m()), "Receiver not registered/enabled");
      zzx.a(AppMeasurementService.a(m()), "Service not registered/enabled");
      cancel();
      long l = l().b();
      this.a = true;
      this.b.setInexactRepeating(2, l + paramLong, Math.max(u().Z(), paramLong), b());
      return;
    }
  }
  
  public void cancel()
  {
    G();
    this.a = false;
    this.b.cancel(b());
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzag.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */