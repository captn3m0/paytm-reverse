package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.zzx;

class zzi
{
  final String a;
  final String b;
  final long c;
  final long d;
  final long e;
  
  zzi(String paramString1, String paramString2, long paramLong1, long paramLong2, long paramLong3)
  {
    zzx.a(paramString1);
    zzx.a(paramString2);
    if (paramLong1 >= 0L)
    {
      bool1 = true;
      zzx.b(bool1);
      if (paramLong2 < 0L) {
        break label81;
      }
    }
    label81:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzx.b(bool1);
      this.a = paramString1;
      this.b = paramString2;
      this.c = paramLong1;
      this.d = paramLong2;
      this.e = paramLong3;
      return;
      bool1 = false;
      break;
    }
  }
  
  zzi a()
  {
    return new zzi(this.a, this.b, this.c + 1L, this.d + 1L, this.e);
  }
  
  zzi a(long paramLong)
  {
    return new zzi(this.a, this.b, this.c, this.d, paramLong);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */