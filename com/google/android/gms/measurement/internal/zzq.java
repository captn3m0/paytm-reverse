package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.WorkerThread;
import com.google.android.gms.common.internal.zzx;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class zzq
  extends zzz
{
  public zzq(zzw paramzzw)
  {
    super(paramzzw);
  }
  
  @WorkerThread
  private byte[] a(HttpURLConnection paramHttpURLConnection)
    throws IOException
  {
    byte[] arrayOfByte = null;
    Object localObject = arrayOfByte;
    ByteArrayOutputStream localByteArrayOutputStream;
    try
    {
      localByteArrayOutputStream = new ByteArrayOutputStream();
      localObject = arrayOfByte;
      paramHttpURLConnection = paramHttpURLConnection.getInputStream();
      localObject = paramHttpURLConnection;
      arrayOfByte = new byte['Ѐ'];
      for (;;)
      {
        localObject = paramHttpURLConnection;
        int i = paramHttpURLConnection.read(arrayOfByte);
        if (i <= 0) {
          break;
        }
        localObject = paramHttpURLConnection;
        localByteArrayOutputStream.write(arrayOfByte, 0, i);
      }
      localObject = paramHttpURLConnection;
    }
    finally
    {
      if (localObject != null) {
        ((InputStream)localObject).close();
      }
    }
    arrayOfByte = localByteArrayOutputStream.toByteArray();
    if (paramHttpURLConnection != null) {
      paramHttpURLConnection.close();
    }
    return arrayOfByte;
  }
  
  @WorkerThread
  protected HttpURLConnection a(URL paramURL)
    throws IOException
  {
    paramURL = paramURL.openConnection();
    if (!(paramURL instanceof HttpURLConnection)) {
      throw new IOException("Failed to obtain HTTP connection");
    }
    paramURL = (HttpURLConnection)paramURL;
    paramURL.setDefaultUseCaches(false);
    paramURL.setConnectTimeout((int)u().H());
    paramURL.setReadTimeout((int)u().I());
    paramURL.setInstanceFollowRedirects(false);
    paramURL.setDoInput(true);
    return paramURL;
  }
  
  protected void a() {}
  
  @WorkerThread
  public void a(String paramString, URL paramURL, Map<String, String> paramMap, zza paramzza)
  {
    f();
    G();
    zzx.a(paramURL);
    zzx.a(paramzza);
    r().b(new zzc(paramString, paramURL, null, paramMap, paramzza));
  }
  
  @WorkerThread
  public void a(String paramString, URL paramURL, byte[] paramArrayOfByte, Map<String, String> paramMap, zza paramzza)
  {
    f();
    G();
    zzx.a(paramURL);
    zzx.a(paramArrayOfByte);
    zzx.a(paramzza);
    r().b(new zzc(paramString, paramURL, paramArrayOfByte, paramMap, paramzza));
  }
  
  public boolean b()
  {
    G();
    Object localObject1 = (ConnectivityManager)m().getSystemService("connectivity");
    try
    {
      localObject1 = ((ConnectivityManager)localObject1).getActiveNetworkInfo();
      if ((localObject1 != null) && (((NetworkInfo)localObject1).isConnected())) {
        return true;
      }
    }
    catch (SecurityException localSecurityException)
    {
      for (;;)
      {
        Object localObject2 = null;
      }
    }
    return false;
  }
  
  @WorkerThread
  static abstract interface zza
  {
    public abstract void a(String paramString, int paramInt, Throwable paramThrowable, byte[] paramArrayOfByte);
  }
  
  @WorkerThread
  private static class zzb
    implements Runnable
  {
    private final zzq.zza a;
    private final int b;
    private final Throwable c;
    private final byte[] d;
    private final String e;
    
    private zzb(String paramString, zzq.zza paramzza, int paramInt, Throwable paramThrowable, byte[] paramArrayOfByte)
    {
      zzx.a(paramzza);
      this.a = paramzza;
      this.b = paramInt;
      this.c = paramThrowable;
      this.d = paramArrayOfByte;
      this.e = paramString;
    }
    
    public void run()
    {
      this.a.a(this.e, this.b, this.c, this.d);
    }
  }
  
  @WorkerThread
  private class zzc
    implements Runnable
  {
    private final URL b;
    private final byte[] c;
    private final zzq.zza d;
    private final String e;
    private final Map<String, String> f;
    
    public zzc(URL paramURL, byte[] paramArrayOfByte, Map<String, String> paramMap, zzq.zza paramzza)
    {
      zzx.a(paramURL);
      zzx.a(paramArrayOfByte);
      Object localObject;
      zzx.a(localObject);
      this.b = paramArrayOfByte;
      this.c = paramMap;
      this.d = ((zzq.zza)localObject);
      this.e = paramURL;
      this.f = paramzza;
    }
    
    /* Error */
    public void run()
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 27	com/google/android/gms/measurement/internal/zzq$zzc:a	Lcom/google/android/gms/measurement/internal/zzq;
      //   4: invokevirtual 56	com/google/android/gms/measurement/internal/zzq:e	()V
      //   7: iconst_0
      //   8: istore 5
      //   10: iconst_0
      //   11: istore_3
      //   12: iconst_0
      //   13: istore 4
      //   15: aload_0
      //   16: getfield 27	com/google/android/gms/measurement/internal/zzq$zzc:a	Lcom/google/android/gms/measurement/internal/zzq;
      //   19: aload_0
      //   20: getfield 40	com/google/android/gms/measurement/internal/zzq$zzc:b	Ljava/net/URL;
      //   23: invokevirtual 59	com/google/android/gms/measurement/internal/zzq:a	(Ljava/net/URL;)Ljava/net/HttpURLConnection;
      //   26: astore 6
      //   28: iload 4
      //   30: istore_1
      //   31: iload_3
      //   32: istore_2
      //   33: aload_0
      //   34: getfield 48	com/google/android/gms/measurement/internal/zzq$zzc:f	Ljava/util/Map;
      //   37: ifnull +145 -> 182
      //   40: iload 4
      //   42: istore_1
      //   43: iload_3
      //   44: istore_2
      //   45: aload_0
      //   46: getfield 48	com/google/android/gms/measurement/internal/zzq$zzc:f	Ljava/util/Map;
      //   49: invokeinterface 65 1 0
      //   54: invokeinterface 71 1 0
      //   59: astore 8
      //   61: iload 4
      //   63: istore_1
      //   64: iload_3
      //   65: istore_2
      //   66: aload 8
      //   68: invokeinterface 77 1 0
      //   73: ifeq +109 -> 182
      //   76: iload 4
      //   78: istore_1
      //   79: iload_3
      //   80: istore_2
      //   81: aload 8
      //   83: invokeinterface 81 1 0
      //   88: checkcast 83	java/util/Map$Entry
      //   91: astore 7
      //   93: iload 4
      //   95: istore_1
      //   96: iload_3
      //   97: istore_2
      //   98: aload 6
      //   100: aload 7
      //   102: invokeinterface 86 1 0
      //   107: checkcast 88	java/lang/String
      //   110: aload 7
      //   112: invokeinterface 91 1 0
      //   117: checkcast 88	java/lang/String
      //   120: invokevirtual 97	java/net/HttpURLConnection:addRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
      //   123: goto -62 -> 61
      //   126: astore 8
      //   128: aconst_null
      //   129: astore 7
      //   131: aload 7
      //   133: ifnull +8 -> 141
      //   136: aload 7
      //   138: invokevirtual 102	java/io/OutputStream:close	()V
      //   141: aload 6
      //   143: ifnull +8 -> 151
      //   146: aload 6
      //   148: invokevirtual 105	java/net/HttpURLConnection:disconnect	()V
      //   151: aload_0
      //   152: getfield 27	com/google/android/gms/measurement/internal/zzq$zzc:a	Lcom/google/android/gms/measurement/internal/zzq;
      //   155: invokevirtual 109	com/google/android/gms/measurement/internal/zzq:r	()Lcom/google/android/gms/measurement/internal/zzv;
      //   158: new 111	com/google/android/gms/measurement/internal/zzq$zzb
      //   161: dup
      //   162: aload_0
      //   163: getfield 46	com/google/android/gms/measurement/internal/zzq$zzc:e	Ljava/lang/String;
      //   166: aload_0
      //   167: getfield 44	com/google/android/gms/measurement/internal/zzq$zzc:d	Lcom/google/android/gms/measurement/internal/zzq$zza;
      //   170: iload_1
      //   171: aload 8
      //   173: aconst_null
      //   174: aconst_null
      //   175: invokespecial 114	com/google/android/gms/measurement/internal/zzq$zzb:<init>	(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/zzq$zza;ILjava/lang/Throwable;[BLcom/google/android/gms/measurement/internal/zzq$1;)V
      //   178: invokevirtual 119	com/google/android/gms/measurement/internal/zzv:a	(Ljava/lang/Runnable;)V
      //   181: return
      //   182: iload 4
      //   184: istore_1
      //   185: iload_3
      //   186: istore_2
      //   187: aload_0
      //   188: getfield 42	com/google/android/gms/measurement/internal/zzq$zzc:c	[B
      //   191: ifnull +382 -> 573
      //   194: iload 4
      //   196: istore_1
      //   197: iload_3
      //   198: istore_2
      //   199: aload_0
      //   200: getfield 27	com/google/android/gms/measurement/internal/zzq$zzc:a	Lcom/google/android/gms/measurement/internal/zzq;
      //   203: invokevirtual 123	com/google/android/gms/measurement/internal/zzq:o	()Lcom/google/android/gms/measurement/internal/zzaj;
      //   206: aload_0
      //   207: getfield 42	com/google/android/gms/measurement/internal/zzq$zzc:c	[B
      //   210: invokevirtual 128	com/google/android/gms/measurement/internal/zzaj:a	([B)[B
      //   213: astore 8
      //   215: iload 4
      //   217: istore_1
      //   218: iload_3
      //   219: istore_2
      //   220: aload_0
      //   221: getfield 27	com/google/android/gms/measurement/internal/zzq$zzc:a	Lcom/google/android/gms/measurement/internal/zzq;
      //   224: invokevirtual 132	com/google/android/gms/measurement/internal/zzq:s	()Lcom/google/android/gms/measurement/internal/zzp;
      //   227: invokevirtual 138	com/google/android/gms/measurement/internal/zzp:z	()Lcom/google/android/gms/measurement/internal/zzp$zza;
      //   230: ldc -116
      //   232: aload 8
      //   234: arraylength
      //   235: invokestatic 146	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
      //   238: invokevirtual 151	com/google/android/gms/measurement/internal/zzp$zza:a	(Ljava/lang/String;Ljava/lang/Object;)V
      //   241: iload 4
      //   243: istore_1
      //   244: iload_3
      //   245: istore_2
      //   246: aload 6
      //   248: iconst_1
      //   249: invokevirtual 155	java/net/HttpURLConnection:setDoOutput	(Z)V
      //   252: iload 4
      //   254: istore_1
      //   255: iload_3
      //   256: istore_2
      //   257: aload 6
      //   259: ldc -99
      //   261: ldc -97
      //   263: invokevirtual 97	java/net/HttpURLConnection:addRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
      //   266: iload 4
      //   268: istore_1
      //   269: iload_3
      //   270: istore_2
      //   271: aload 6
      //   273: aload 8
      //   275: arraylength
      //   276: invokevirtual 163	java/net/HttpURLConnection:setFixedLengthStreamingMode	(I)V
      //   279: iload 4
      //   281: istore_1
      //   282: iload_3
      //   283: istore_2
      //   284: aload 6
      //   286: invokevirtual 166	java/net/HttpURLConnection:connect	()V
      //   289: iload 4
      //   291: istore_1
      //   292: iload_3
      //   293: istore_2
      //   294: aload 6
      //   296: invokevirtual 170	java/net/HttpURLConnection:getOutputStream	()Ljava/io/OutputStream;
      //   299: astore 7
      //   301: aload 7
      //   303: aload 8
      //   305: invokevirtual 174	java/io/OutputStream:write	([B)V
      //   308: aload 7
      //   310: invokevirtual 102	java/io/OutputStream:close	()V
      //   313: iload 4
      //   315: istore_1
      //   316: iload_3
      //   317: istore_2
      //   318: aload 6
      //   320: invokevirtual 178	java/net/HttpURLConnection:getResponseCode	()I
      //   323: istore_3
      //   324: iload_3
      //   325: istore_1
      //   326: iload_3
      //   327: istore_2
      //   328: aload_0
      //   329: getfield 27	com/google/android/gms/measurement/internal/zzq$zzc:a	Lcom/google/android/gms/measurement/internal/zzq;
      //   332: aload 6
      //   334: invokestatic 181	com/google/android/gms/measurement/internal/zzq:a	(Lcom/google/android/gms/measurement/internal/zzq;Ljava/net/HttpURLConnection;)[B
      //   337: astore 8
      //   339: iconst_0
      //   340: ifeq +11 -> 351
      //   343: new 183	java/lang/NullPointerException
      //   346: dup
      //   347: invokespecial 184	java/lang/NullPointerException:<init>	()V
      //   350: athrow
      //   351: aload 6
      //   353: ifnull +8 -> 361
      //   356: aload 6
      //   358: invokevirtual 105	java/net/HttpURLConnection:disconnect	()V
      //   361: aload_0
      //   362: getfield 27	com/google/android/gms/measurement/internal/zzq$zzc:a	Lcom/google/android/gms/measurement/internal/zzq;
      //   365: invokevirtual 109	com/google/android/gms/measurement/internal/zzq:r	()Lcom/google/android/gms/measurement/internal/zzv;
      //   368: new 111	com/google/android/gms/measurement/internal/zzq$zzb
      //   371: dup
      //   372: aload_0
      //   373: getfield 46	com/google/android/gms/measurement/internal/zzq$zzc:e	Ljava/lang/String;
      //   376: aload_0
      //   377: getfield 44	com/google/android/gms/measurement/internal/zzq$zzc:d	Lcom/google/android/gms/measurement/internal/zzq$zza;
      //   380: iload_3
      //   381: aconst_null
      //   382: aload 8
      //   384: aconst_null
      //   385: invokespecial 114	com/google/android/gms/measurement/internal/zzq$zzb:<init>	(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/zzq$zza;ILjava/lang/Throwable;[BLcom/google/android/gms/measurement/internal/zzq$1;)V
      //   388: invokevirtual 119	com/google/android/gms/measurement/internal/zzv:a	(Ljava/lang/Runnable;)V
      //   391: return
      //   392: astore 7
      //   394: aload_0
      //   395: getfield 27	com/google/android/gms/measurement/internal/zzq$zzc:a	Lcom/google/android/gms/measurement/internal/zzq;
      //   398: invokevirtual 132	com/google/android/gms/measurement/internal/zzq:s	()Lcom/google/android/gms/measurement/internal/zzp;
      //   401: invokevirtual 186	com/google/android/gms/measurement/internal/zzp:b	()Lcom/google/android/gms/measurement/internal/zzp$zza;
      //   404: ldc -68
      //   406: aload 7
      //   408: invokevirtual 151	com/google/android/gms/measurement/internal/zzp$zza:a	(Ljava/lang/String;Ljava/lang/Object;)V
      //   411: goto -60 -> 351
      //   414: astore 7
      //   416: aload_0
      //   417: getfield 27	com/google/android/gms/measurement/internal/zzq$zzc:a	Lcom/google/android/gms/measurement/internal/zzq;
      //   420: invokevirtual 132	com/google/android/gms/measurement/internal/zzq:s	()Lcom/google/android/gms/measurement/internal/zzp;
      //   423: invokevirtual 186	com/google/android/gms/measurement/internal/zzp:b	()Lcom/google/android/gms/measurement/internal/zzp$zza;
      //   426: ldc -68
      //   428: aload 7
      //   430: invokevirtual 151	com/google/android/gms/measurement/internal/zzp$zza:a	(Ljava/lang/String;Ljava/lang/Object;)V
      //   433: goto -292 -> 141
      //   436: astore 6
      //   438: aconst_null
      //   439: astore 8
      //   441: aconst_null
      //   442: astore 7
      //   444: iload 5
      //   446: istore_2
      //   447: aload 7
      //   449: ifnull +8 -> 457
      //   452: aload 7
      //   454: invokevirtual 102	java/io/OutputStream:close	()V
      //   457: aload 8
      //   459: ifnull +8 -> 467
      //   462: aload 8
      //   464: invokevirtual 105	java/net/HttpURLConnection:disconnect	()V
      //   467: aload_0
      //   468: getfield 27	com/google/android/gms/measurement/internal/zzq$zzc:a	Lcom/google/android/gms/measurement/internal/zzq;
      //   471: invokevirtual 109	com/google/android/gms/measurement/internal/zzq:r	()Lcom/google/android/gms/measurement/internal/zzv;
      //   474: new 111	com/google/android/gms/measurement/internal/zzq$zzb
      //   477: dup
      //   478: aload_0
      //   479: getfield 46	com/google/android/gms/measurement/internal/zzq$zzc:e	Ljava/lang/String;
      //   482: aload_0
      //   483: getfield 44	com/google/android/gms/measurement/internal/zzq$zzc:d	Lcom/google/android/gms/measurement/internal/zzq$zza;
      //   486: iload_2
      //   487: aconst_null
      //   488: aconst_null
      //   489: aconst_null
      //   490: invokespecial 114	com/google/android/gms/measurement/internal/zzq$zzb:<init>	(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/zzq$zza;ILjava/lang/Throwable;[BLcom/google/android/gms/measurement/internal/zzq$1;)V
      //   493: invokevirtual 119	com/google/android/gms/measurement/internal/zzv:a	(Ljava/lang/Runnable;)V
      //   496: aload 6
      //   498: athrow
      //   499: astore 7
      //   501: aload_0
      //   502: getfield 27	com/google/android/gms/measurement/internal/zzq$zzc:a	Lcom/google/android/gms/measurement/internal/zzq;
      //   505: invokevirtual 132	com/google/android/gms/measurement/internal/zzq:s	()Lcom/google/android/gms/measurement/internal/zzp;
      //   508: invokevirtual 186	com/google/android/gms/measurement/internal/zzp:b	()Lcom/google/android/gms/measurement/internal/zzp$zza;
      //   511: ldc -68
      //   513: aload 7
      //   515: invokevirtual 151	com/google/android/gms/measurement/internal/zzp$zza:a	(Ljava/lang/String;Ljava/lang/Object;)V
      //   518: goto -61 -> 457
      //   521: astore 9
      //   523: aconst_null
      //   524: astore 7
      //   526: aload 6
      //   528: astore 8
      //   530: aload 9
      //   532: astore 6
      //   534: goto -87 -> 447
      //   537: astore 9
      //   539: aload 6
      //   541: astore 8
      //   543: iload 5
      //   545: istore_2
      //   546: aload 9
      //   548: astore 6
      //   550: goto -103 -> 447
      //   553: astore 8
      //   555: iconst_0
      //   556: istore_1
      //   557: aconst_null
      //   558: astore 7
      //   560: aconst_null
      //   561: astore 6
      //   563: goto -432 -> 131
      //   566: astore 8
      //   568: iconst_0
      //   569: istore_1
      //   570: goto -439 -> 131
      //   573: goto -260 -> 313
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	576	0	this	zzc
      //   30	540	1	i	int
      //   32	514	2	j	int
      //   11	370	3	k	int
      //   13	301	4	m	int
      //   8	536	5	n	int
      //   26	331	6	localHttpURLConnection	HttpURLConnection
      //   436	91	6	localObject1	Object
      //   532	30	6	localObject2	Object
      //   91	218	7	localObject3	Object
      //   392	15	7	localIOException1	IOException
      //   414	15	7	localIOException2	IOException
      //   442	11	7	localObject4	Object
      //   499	15	7	localIOException3	IOException
      //   524	35	7	localObject5	Object
      //   59	23	8	localIterator	java.util.Iterator
      //   126	46	8	localIOException4	IOException
      //   213	329	8	localObject6	Object
      //   553	1	8	localIOException5	IOException
      //   566	1	8	localIOException6	IOException
      //   521	10	9	localObject7	Object
      //   537	10	9	localObject8	Object
      // Exception table:
      //   from	to	target	type
      //   33	40	126	java/io/IOException
      //   45	61	126	java/io/IOException
      //   66	76	126	java/io/IOException
      //   81	93	126	java/io/IOException
      //   98	123	126	java/io/IOException
      //   187	194	126	java/io/IOException
      //   199	215	126	java/io/IOException
      //   220	241	126	java/io/IOException
      //   246	252	126	java/io/IOException
      //   257	266	126	java/io/IOException
      //   271	279	126	java/io/IOException
      //   284	289	126	java/io/IOException
      //   294	301	126	java/io/IOException
      //   318	324	126	java/io/IOException
      //   328	339	126	java/io/IOException
      //   343	351	392	java/io/IOException
      //   136	141	414	java/io/IOException
      //   15	28	436	finally
      //   452	457	499	java/io/IOException
      //   33	40	521	finally
      //   45	61	521	finally
      //   66	76	521	finally
      //   81	93	521	finally
      //   98	123	521	finally
      //   187	194	521	finally
      //   199	215	521	finally
      //   220	241	521	finally
      //   246	252	521	finally
      //   257	266	521	finally
      //   271	279	521	finally
      //   284	289	521	finally
      //   294	301	521	finally
      //   318	324	521	finally
      //   328	339	521	finally
      //   301	313	537	finally
      //   15	28	553	java/io/IOException
      //   301	313	566	java/io/IOException
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/internal/zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */