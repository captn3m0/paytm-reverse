package com.google.android.gms.measurement;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.WorkerThread;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.measurement.internal.zzab;
import com.google.android.gms.measurement.internal.zzw;

public class AppMeasurement
{
  private final zzw a;
  
  public AppMeasurement(zzw paramzzw)
  {
    zzx.a(paramzzw);
    this.a = paramzzw;
  }
  
  public static AppMeasurement a(Context paramContext)
  {
    return zzw.a(paramContext).m();
  }
  
  public void a(String paramString1, String paramString2, Bundle paramBundle)
  {
    Bundle localBundle = paramBundle;
    if (paramBundle == null) {
      localBundle = new Bundle();
    }
    this.a.l().a(paramString1, paramString2, localBundle);
  }
  
  public static abstract interface zza
  {
    @WorkerThread
    public abstract void a(String paramString1, String paramString2, Bundle paramBundle, long paramLong);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/measurement/AppMeasurement.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */