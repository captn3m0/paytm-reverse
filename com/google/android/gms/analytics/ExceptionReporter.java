package com.google.android.gms.analytics;

import android.content.Context;
import com.google.android.gms.analytics.internal.zzae;

public class ExceptionReporter
  implements Thread.UncaughtExceptionHandler
{
  private final Thread.UncaughtExceptionHandler a;
  private final Tracker b;
  private final Context c;
  private ExceptionParser d;
  private GoogleAnalytics e;
  
  GoogleAnalytics a()
  {
    if (this.e == null) {
      this.e = GoogleAnalytics.a(this.c);
    }
    return this.e;
  }
  
  public void uncaughtException(Thread paramThread, Throwable paramThrowable)
  {
    Object localObject = "UncaughtException";
    if (this.d != null) {
      if (paramThread == null) {
        break label115;
      }
    }
    label115:
    for (localObject = paramThread.getName();; localObject = null)
    {
      localObject = this.d.a((String)localObject, paramThrowable);
      zzae.a("Reporting uncaught exception: " + (String)localObject);
      this.b.a(new HitBuilders.ExceptionBuilder().a((String)localObject).a(true).a());
      localObject = a();
      ((GoogleAnalytics)localObject).i();
      ((GoogleAnalytics)localObject).j();
      if (this.a != null)
      {
        zzae.a("Passing exception to the original handler");
        this.a.uncaughtException(paramThread, paramThrowable);
      }
      return;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/ExceptionReporter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */