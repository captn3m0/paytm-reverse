package com.google.android.gms.analytics.ecommerce;

import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.measurement.zze;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Promotion
{
  Map<String, String> a = new HashMap();
  
  public Promotion a(String paramString)
  {
    a("id", paramString);
    return this;
  }
  
  void a(String paramString1, String paramString2)
  {
    zzx.a(paramString1, "Name should be non-null");
    this.a.put(paramString1, paramString2);
  }
  
  public Promotion b(String paramString)
  {
    a("nm", paramString);
    return this;
  }
  
  public Promotion c(String paramString)
  {
    a("cr", paramString);
    return this;
  }
  
  public Promotion d(String paramString)
  {
    a("ps", paramString);
    return this;
  }
  
  public Map<String, String> e(String paramString)
  {
    HashMap localHashMap = new HashMap();
    Iterator localIterator = this.a.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      localHashMap.put(paramString + (String)localEntry.getKey(), localEntry.getValue());
    }
    return localHashMap;
  }
  
  public String toString()
  {
    return zze.a(this.a);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/ecommerce/Promotion.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */