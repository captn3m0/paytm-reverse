package com.google.android.gms.analytics.ecommerce;

import com.google.android.gms.analytics.zzc;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.measurement.zze;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Product
{
  Map<String, String> a = new HashMap();
  
  public Product a(double paramDouble)
  {
    a("pr", Double.toString(paramDouble));
    return this;
  }
  
  public Product a(int paramInt)
  {
    a("ps", Integer.toString(paramInt));
    return this;
  }
  
  public Product a(int paramInt1, int paramInt2)
  {
    a(zzc.k(paramInt1), Integer.toString(paramInt2));
    return this;
  }
  
  public Product a(int paramInt, String paramString)
  {
    a(zzc.j(paramInt), paramString);
    return this;
  }
  
  public Product a(String paramString)
  {
    a("id", paramString);
    return this;
  }
  
  void a(String paramString1, String paramString2)
  {
    zzx.a(paramString1, "Name should be non-null");
    this.a.put(paramString1, paramString2);
  }
  
  public Product b(int paramInt)
  {
    a("qt", Integer.toString(paramInt));
    return this;
  }
  
  public Product b(String paramString)
  {
    a("nm", paramString);
    return this;
  }
  
  public Product c(String paramString)
  {
    a("br", paramString);
    return this;
  }
  
  public Product d(String paramString)
  {
    a("ca", paramString);
    return this;
  }
  
  public Product e(String paramString)
  {
    a("va", paramString);
    return this;
  }
  
  public Product f(String paramString)
  {
    a("cc", paramString);
    return this;
  }
  
  public Map<String, String> g(String paramString)
  {
    HashMap localHashMap = new HashMap();
    Iterator localIterator = this.a.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      localHashMap.put(paramString + (String)localEntry.getKey(), localEntry.getValue());
    }
    return localHashMap;
  }
  
  public String toString()
  {
    return zze.a(this.a);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/ecommerce/Product.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */