package com.google.android.gms.analytics.ecommerce;

import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.measurement.zze;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class ProductAction
{
  Map<String, String> a = new HashMap();
  
  public ProductAction(String paramString)
  {
    a("&pa", paramString);
  }
  
  public ProductAction a(double paramDouble)
  {
    a("&tr", Double.toString(paramDouble));
    return this;
  }
  
  public ProductAction a(int paramInt)
  {
    a("&cos", Integer.toString(paramInt));
    return this;
  }
  
  public ProductAction a(String paramString)
  {
    a("&ti", paramString);
    return this;
  }
  
  public Map<String, String> a()
  {
    return new HashMap(this.a);
  }
  
  void a(String paramString1, String paramString2)
  {
    zzx.a(paramString1, "Name should be non-null");
    this.a.put(paramString1, paramString2);
  }
  
  public ProductAction b(double paramDouble)
  {
    a("&tt", Double.toString(paramDouble));
    return this;
  }
  
  public ProductAction b(String paramString)
  {
    a("&ta", paramString);
    return this;
  }
  
  public ProductAction c(double paramDouble)
  {
    a("&ts", Double.toString(paramDouble));
    return this;
  }
  
  public ProductAction c(String paramString)
  {
    a("&tcc", paramString);
    return this;
  }
  
  public ProductAction d(String paramString)
  {
    a("&col", paramString);
    return this;
  }
  
  public ProductAction e(String paramString)
  {
    a("&pal", paramString);
    return this;
  }
  
  public String toString()
  {
    HashMap localHashMap = new HashMap();
    Iterator localIterator = this.a.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      if (((String)localEntry.getKey()).startsWith("&")) {
        localHashMap.put(((String)localEntry.getKey()).substring(1), localEntry.getValue());
      } else {
        localHashMap.put(localEntry.getKey(), localEntry.getValue());
      }
    }
    return zze.a(localHashMap);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/ecommerce/ProductAction.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */