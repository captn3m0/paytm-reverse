package com.google.android.gms.analytics;

@Deprecated
public abstract interface Logger
{
  @Deprecated
  public abstract void error(String paramString);
  
  @Deprecated
  public abstract int getLogLevel();
  
  @Deprecated
  public abstract void setLogLevel(int paramInt);
  
  @Deprecated
  public abstract void verbose(String paramString);
  
  @Deprecated
  public abstract void warn(String paramString);
  
  @Deprecated
  public static class LogLevel {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/Logger.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */