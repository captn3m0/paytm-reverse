package com.google.android.gms.analytics;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.RequiresPermission;
import com.google.android.gms.analytics.internal.zzaf;
import com.google.android.gms.analytics.internal.zzam;
import com.google.android.gms.analytics.internal.zzb;
import com.google.android.gms.analytics.internal.zzf;
import com.google.android.gms.analytics.internal.zzr;
import com.google.android.gms.analytics.internal.zzw;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzrp;

public final class AnalyticsService
  extends Service
{
  private static Boolean b;
  private final Handler a = new Handler();
  
  private void a()
  {
    try
    {
      synchronized (AnalyticsReceiver.a)
      {
        zzrp localzzrp = AnalyticsReceiver.b;
        if ((localzzrp != null) && (localzzrp.b())) {
          localzzrp.a();
        }
        return;
      }
      return;
    }
    catch (SecurityException localSecurityException) {}
  }
  
  public static boolean a(Context paramContext)
  {
    zzx.a(paramContext);
    if (b != null) {
      return b.booleanValue();
    }
    boolean bool = zzam.a(paramContext, AnalyticsService.class);
    b = Boolean.valueOf(bool);
    return bool;
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    return null;
  }
  
  @RequiresPermission
  public void onCreate()
  {
    super.onCreate();
    zzf localzzf = zzf.a(this);
    zzaf localzzaf = localzzf.f();
    if (localzzf.e().a())
    {
      localzzaf.b("Device AnalyticsService is starting up");
      return;
    }
    localzzaf.b("Local AnalyticsService is starting up");
  }
  
  @RequiresPermission
  public void onDestroy()
  {
    zzf localzzf = zzf.a(this);
    zzaf localzzaf = localzzf.f();
    if (localzzf.e().a()) {
      localzzaf.b("Device AnalyticsService is shutting down");
    }
    for (;;)
    {
      super.onDestroy();
      return;
      localzzaf.b("Local AnalyticsService is shutting down");
    }
  }
  
  @RequiresPermission
  public int onStartCommand(Intent paramIntent, int paramInt1, final int paramInt2)
  {
    a();
    final zzf localzzf = zzf.a(this);
    final zzaf localzzaf = localzzf.f();
    paramIntent = paramIntent.getAction();
    if (localzzf.e().a()) {
      localzzaf.a("Device AnalyticsService called. startId, action", Integer.valueOf(paramInt2), paramIntent);
    }
    for (;;)
    {
      if ("com.google.android.gms.analytics.ANALYTICS_DISPATCH".equals(paramIntent)) {
        localzzf.i().a(new zzw()
        {
          public void a(Throwable paramAnonymousThrowable)
          {
            AnalyticsService.a(AnalyticsService.this).post(new Runnable()
            {
              public void run()
              {
                if (AnalyticsService.this.stopSelfResult(AnalyticsService.1.this.a))
                {
                  if (AnalyticsService.1.this.b.e().a()) {
                    AnalyticsService.1.this.c.b("Device AnalyticsService processed last dispatch request");
                  }
                }
                else {
                  return;
                }
                AnalyticsService.1.this.c.b("Local AnalyticsService processed last dispatch request");
              }
            });
          }
        });
      }
      return 2;
      localzzaf.a("Local AnalyticsService called. startId, action", Integer.valueOf(paramInt2), paramIntent);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/AnalyticsService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */