package com.google.android.gms.analytics;

import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.google.android.gms.analytics.ecommerce.Promotion;
import com.google.android.gms.analytics.internal.zzab;
import com.google.android.gms.analytics.internal.zzaf;
import com.google.android.gms.analytics.internal.zzam;
import com.google.android.gms.analytics.internal.zze;
import com.google.android.gms.analytics.internal.zzf;
import com.google.android.gms.analytics.internal.zzh;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzkb;
import com.google.android.gms.internal.zzkc;
import com.google.android.gms.internal.zzkd;
import com.google.android.gms.internal.zzke;
import com.google.android.gms.internal.zzpq;
import com.google.android.gms.internal.zzpr;
import com.google.android.gms.internal.zzps;
import com.google.android.gms.internal.zzpt;
import com.google.android.gms.internal.zzpu;
import com.google.android.gms.internal.zzpv;
import com.google.android.gms.internal.zzpw;
import com.google.android.gms.internal.zzpx;
import com.google.android.gms.internal.zzpy;
import com.google.android.gms.measurement.zzi;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class zzb
  extends com.google.android.gms.analytics.internal.zzc
  implements zzi
{
  private static DecimalFormat a;
  private final zzf b;
  private final String c;
  private final Uri d;
  private final boolean e;
  private final boolean f;
  
  public zzb(zzf paramzzf, String paramString)
  {
    this(paramzzf, paramString, true, false);
  }
  
  public zzb(zzf paramzzf, String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    super(paramzzf);
    zzx.a(paramString);
    this.b = paramzzf;
    this.c = paramString;
    this.e = paramBoolean1;
    this.f = paramBoolean2;
    this.d = a(this.c);
  }
  
  static Uri a(String paramString)
  {
    zzx.a(paramString);
    Uri.Builder localBuilder = new Uri.Builder();
    localBuilder.scheme("uri");
    localBuilder.authority("google-analytics.com");
    localBuilder.path(paramString);
    return localBuilder.build();
  }
  
  static String a(double paramDouble)
  {
    if (a == null) {
      a = new DecimalFormat("0.######");
    }
    return a.format(paramDouble);
  }
  
  private static String a(Object paramObject)
  {
    if (paramObject == null) {
      paramObject = null;
    }
    String str;
    do
    {
      return (String)paramObject;
      if (!(paramObject instanceof String)) {
        break;
      }
      str = (String)paramObject;
      paramObject = str;
    } while (!TextUtils.isEmpty(str));
    return null;
    if ((paramObject instanceof Double))
    {
      paramObject = (Double)paramObject;
      if (((Double)paramObject).doubleValue() != 0.0D) {
        return a(((Double)paramObject).doubleValue());
      }
      return null;
    }
    if ((paramObject instanceof Boolean))
    {
      if (paramObject != Boolean.FALSE) {
        return "1";
      }
      return null;
    }
    return String.valueOf(paramObject);
  }
  
  private static String a(Map<String, String> paramMap)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    paramMap = paramMap.entrySet().iterator();
    while (paramMap.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)paramMap.next();
      if (localStringBuilder.length() != 0) {
        localStringBuilder.append(", ");
      }
      localStringBuilder.append((String)localEntry.getKey());
      localStringBuilder.append("=");
      localStringBuilder.append((String)localEntry.getValue());
    }
    return localStringBuilder.toString();
  }
  
  private static void a(Map<String, String> paramMap, String paramString, double paramDouble)
  {
    if (paramDouble != 0.0D) {
      paramMap.put(paramString, a(paramDouble));
    }
  }
  
  private static void a(Map<String, String> paramMap, String paramString, int paramInt1, int paramInt2)
  {
    if ((paramInt1 > 0) && (paramInt2 > 0)) {
      paramMap.put(paramString, paramInt1 + "x" + paramInt2);
    }
  }
  
  private static void a(Map<String, String> paramMap, String paramString1, String paramString2)
  {
    if (!TextUtils.isEmpty(paramString2)) {
      paramMap.put(paramString1, paramString2);
    }
  }
  
  private static void a(Map<String, String> paramMap, String paramString, boolean paramBoolean)
  {
    if (paramBoolean) {
      paramMap.put(paramString, "1");
    }
  }
  
  public static Map<String, String> b(com.google.android.gms.measurement.zzc paramzzc)
  {
    HashMap localHashMap = new HashMap();
    Object localObject1 = (zzkd)paramzzc.a(zzkd.class);
    Object localObject2;
    Object localObject3;
    if (localObject1 != null)
    {
      localObject1 = ((zzkd)localObject1).a().entrySet().iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (Map.Entry)((Iterator)localObject1).next();
        localObject3 = a(((Map.Entry)localObject2).getValue());
        if (localObject3 != null) {
          localHashMap.put(((Map.Entry)localObject2).getKey(), localObject3);
        }
      }
    }
    localObject1 = (zzke)paramzzc.a(zzke.class);
    if (localObject1 != null)
    {
      a(localHashMap, "t", ((zzke)localObject1).a());
      a(localHashMap, "cid", ((zzke)localObject1).b());
      a(localHashMap, "uid", ((zzke)localObject1).c());
      a(localHashMap, "sc", ((zzke)localObject1).f());
      a(localHashMap, "sf", ((zzke)localObject1).h());
      a(localHashMap, "ni", ((zzke)localObject1).g());
      a(localHashMap, "adid", ((zzke)localObject1).d());
      a(localHashMap, "ate", ((zzke)localObject1).e());
    }
    localObject1 = (zzpw)paramzzc.a(zzpw.class);
    if (localObject1 != null)
    {
      a(localHashMap, "cd", ((zzpw)localObject1).b());
      a(localHashMap, "a", ((zzpw)localObject1).c());
      a(localHashMap, "dr", ((zzpw)localObject1).d());
    }
    localObject1 = (zzpu)paramzzc.a(zzpu.class);
    if (localObject1 != null)
    {
      a(localHashMap, "ec", ((zzpu)localObject1).a());
      a(localHashMap, "ea", ((zzpu)localObject1).b());
      a(localHashMap, "el", ((zzpu)localObject1).c());
      a(localHashMap, "ev", ((zzpu)localObject1).d());
    }
    localObject1 = (zzpr)paramzzc.a(zzpr.class);
    if (localObject1 != null)
    {
      a(localHashMap, "cn", ((zzpr)localObject1).a());
      a(localHashMap, "cs", ((zzpr)localObject1).b());
      a(localHashMap, "cm", ((zzpr)localObject1).c());
      a(localHashMap, "ck", ((zzpr)localObject1).d());
      a(localHashMap, "cc", ((zzpr)localObject1).e());
      a(localHashMap, "ci", ((zzpr)localObject1).f());
      a(localHashMap, "anid", ((zzpr)localObject1).g());
      a(localHashMap, "gclid", ((zzpr)localObject1).h());
      a(localHashMap, "dclid", ((zzpr)localObject1).i());
      a(localHashMap, "aclid", ((zzpr)localObject1).j());
    }
    localObject1 = (zzpv)paramzzc.a(zzpv.class);
    if (localObject1 != null)
    {
      a(localHashMap, "exd", ((zzpv)localObject1).a());
      a(localHashMap, "exf", ((zzpv)localObject1).b());
    }
    localObject1 = (zzpx)paramzzc.a(zzpx.class);
    if (localObject1 != null)
    {
      a(localHashMap, "sn", ((zzpx)localObject1).a());
      a(localHashMap, "sa", ((zzpx)localObject1).b());
      a(localHashMap, "st", ((zzpx)localObject1).c());
    }
    localObject1 = (zzpy)paramzzc.a(zzpy.class);
    if (localObject1 != null)
    {
      a(localHashMap, "utv", ((zzpy)localObject1).a());
      a(localHashMap, "utt", ((zzpy)localObject1).b());
      a(localHashMap, "utc", ((zzpy)localObject1).c());
      a(localHashMap, "utl", ((zzpy)localObject1).d());
    }
    localObject1 = (zzkb)paramzzc.a(zzkb.class);
    if (localObject1 != null)
    {
      localObject1 = ((zzkb)localObject1).a().entrySet().iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (Map.Entry)((Iterator)localObject1).next();
        localObject3 = zzc.a(((Integer)((Map.Entry)localObject2).getKey()).intValue());
        if (!TextUtils.isEmpty((CharSequence)localObject3)) {
          localHashMap.put(localObject3, ((Map.Entry)localObject2).getValue());
        }
      }
    }
    localObject1 = (zzkc)paramzzc.a(zzkc.class);
    if (localObject1 != null)
    {
      localObject1 = ((zzkc)localObject1).a().entrySet().iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (Map.Entry)((Iterator)localObject1).next();
        localObject3 = zzc.b(((Integer)((Map.Entry)localObject2).getKey()).intValue());
        if (!TextUtils.isEmpty((CharSequence)localObject3)) {
          localHashMap.put(localObject3, a(((Double)((Map.Entry)localObject2).getValue()).doubleValue()));
        }
      }
    }
    localObject1 = (zzpt)paramzzc.a(zzpt.class);
    if (localObject1 != null)
    {
      localObject2 = ((zzpt)localObject1).a();
      if (localObject2 != null)
      {
        localObject2 = ((ProductAction)localObject2).a().entrySet().iterator();
        while (((Iterator)localObject2).hasNext())
        {
          localObject3 = (Map.Entry)((Iterator)localObject2).next();
          if (((String)((Map.Entry)localObject3).getKey()).startsWith("&")) {
            localHashMap.put(((String)((Map.Entry)localObject3).getKey()).substring(1), ((Map.Entry)localObject3).getValue());
          } else {
            localHashMap.put(((Map.Entry)localObject3).getKey(), ((Map.Entry)localObject3).getValue());
          }
        }
      }
      localObject2 = ((zzpt)localObject1).d().iterator();
      int i = 1;
      while (((Iterator)localObject2).hasNext())
      {
        localHashMap.putAll(((Promotion)((Iterator)localObject2).next()).e(zzc.f(i)));
        i += 1;
      }
      localObject2 = ((zzpt)localObject1).b().iterator();
      i = 1;
      while (((Iterator)localObject2).hasNext())
      {
        localHashMap.putAll(((Product)((Iterator)localObject2).next()).g(zzc.d(i)));
        i += 1;
      }
      localObject1 = ((zzpt)localObject1).c().entrySet().iterator();
      i = 1;
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (Map.Entry)((Iterator)localObject1).next();
        Object localObject4 = (List)((Map.Entry)localObject2).getValue();
        localObject3 = zzc.i(i);
        localObject4 = ((List)localObject4).iterator();
        int j = 1;
        while (((Iterator)localObject4).hasNext())
        {
          localHashMap.putAll(((Product)((Iterator)localObject4).next()).g((String)localObject3 + zzc.g(j)));
          j += 1;
        }
        if (!TextUtils.isEmpty((CharSequence)((Map.Entry)localObject2).getKey())) {
          localHashMap.put((String)localObject3 + "nm", ((Map.Entry)localObject2).getKey());
        }
        i += 1;
      }
    }
    localObject1 = (zzps)paramzzc.a(zzps.class);
    if (localObject1 != null)
    {
      a(localHashMap, "ul", ((zzps)localObject1).f());
      a(localHashMap, "sd", ((zzps)localObject1).a());
      a(localHashMap, "sr", ((zzps)localObject1).b(), ((zzps)localObject1).c());
      a(localHashMap, "vp", ((zzps)localObject1).d(), ((zzps)localObject1).e());
    }
    paramzzc = (zzpq)paramzzc.a(zzpq.class);
    if (paramzzc != null)
    {
      a(localHashMap, "an", paramzzc.a());
      a(localHashMap, "aid", paramzzc.c());
      a(localHashMap, "aiid", paramzzc.d());
      a(localHashMap, "av", paramzzc.b());
    }
    return localHashMap;
  }
  
  public Uri a()
  {
    return this.d;
  }
  
  public void a(com.google.android.gms.measurement.zzc paramzzc)
  {
    zzx.a(paramzzc);
    zzx.b(paramzzc.f(), "Can't deliver not submitted measurement");
    zzx.c("deliver should be called on worker thread");
    Object localObject2 = paramzzc.a();
    Object localObject1 = (zzke)((com.google.android.gms.measurement.zzc)localObject2).b(zzke.class);
    if (TextUtils.isEmpty(((zzke)localObject1).a())) {
      p().a(b((com.google.android.gms.measurement.zzc)localObject2), "Ignoring measurement without type");
    }
    do
    {
      return;
      if (TextUtils.isEmpty(((zzke)localObject1).b()))
      {
        p().a(b((com.google.android.gms.measurement.zzc)localObject2), "Ignoring measurement without client id");
        return;
      }
    } while (this.b.k().f());
    double d1 = ((zzke)localObject1).h();
    if (zzam.a(d1, ((zzke)localObject1).b()))
    {
      b("Sampling enabled. Hit sampled out. sampling rate", Double.valueOf(d1));
      return;
    }
    localObject2 = b((com.google.android.gms.measurement.zzc)localObject2);
    ((Map)localObject2).put("v", "1");
    ((Map)localObject2).put("_v", zze.b);
    ((Map)localObject2).put("tid", this.c);
    if (this.b.k().e())
    {
      c("Dry run is enabled. GoogleAnalytics would have sent", a((Map)localObject2));
      return;
    }
    HashMap localHashMap = new HashMap();
    zzam.a(localHashMap, "uid", ((zzke)localObject1).c());
    Object localObject3 = (zzpq)paramzzc.a(zzpq.class);
    if (localObject3 != null)
    {
      zzam.a(localHashMap, "an", ((zzpq)localObject3).a());
      zzam.a(localHashMap, "aid", ((zzpq)localObject3).c());
      zzam.a(localHashMap, "av", ((zzpq)localObject3).b());
      zzam.a(localHashMap, "aiid", ((zzpq)localObject3).d());
    }
    localObject3 = ((zzke)localObject1).b();
    String str = this.c;
    if (!TextUtils.isEmpty(((zzke)localObject1).d())) {}
    for (boolean bool = true;; bool = false)
    {
      localObject1 = new zzh(0L, (String)localObject3, str, bool, 0L, localHashMap);
      ((Map)localObject2).put("_s", String.valueOf(t().a((zzh)localObject1)));
      paramzzc = new zzab(p(), (Map)localObject2, paramzzc.d(), true);
      t().a(paramzzc);
      return;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */