package com.google.android.gms.analytics;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.analytics.internal.zza;
import com.google.android.gms.analytics.internal.zzab;
import com.google.android.gms.analytics.internal.zzad;
import com.google.android.gms.analytics.internal.zzaf;
import com.google.android.gms.analytics.internal.zzal;
import com.google.android.gms.analytics.internal.zzam;
import com.google.android.gms.analytics.internal.zzan;
import com.google.android.gms.analytics.internal.zzb;
import com.google.android.gms.analytics.internal.zzd;
import com.google.android.gms.analytics.internal.zze;
import com.google.android.gms.analytics.internal.zzf;
import com.google.android.gms.analytics.internal.zzh;
import com.google.android.gms.analytics.internal.zzk;
import com.google.android.gms.analytics.internal.zzu;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzmq;
import com.google.android.gms.internal.zzpq;
import com.google.android.gms.internal.zzps;
import com.google.android.gms.measurement.zzg;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

public class Tracker
  extends zzd
{
  private boolean a;
  private final Map<String, String> b = new HashMap();
  private final Map<String, String> c = new HashMap();
  private final zzad d;
  private final zza e;
  private zzal f;
  
  Tracker(zzf paramzzf, String paramString, zzad paramzzad)
  {
    super(paramzzf);
    if (paramString != null) {
      this.b.put("&tid", paramString);
    }
    this.b.put("useSecure", "1");
    this.b.put("&a", Integer.toString(new Random().nextInt(Integer.MAX_VALUE) + 1));
    if (paramzzad == null) {}
    for (this.d = new zzad("tracking", n());; this.d = paramzzad)
    {
      this.e = new zza(paramzzf);
      return;
    }
  }
  
  static String a(Activity paramActivity)
  {
    zzx.a(paramActivity);
    paramActivity = paramActivity.getIntent();
    if (paramActivity == null) {}
    do
    {
      return null;
      paramActivity = paramActivity.getStringExtra("android.intent.extra.REFERRER_NAME");
    } while (TextUtils.isEmpty(paramActivity));
    return paramActivity;
  }
  
  private static void a(Map<String, String> paramMap1, Map<String, String> paramMap2)
  {
    zzx.a(paramMap2);
    if (paramMap1 == null) {}
    for (;;)
    {
      return;
      paramMap1 = paramMap1.entrySet().iterator();
      while (paramMap1.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)paramMap1.next();
        String str = b(localEntry);
        if (str != null) {
          paramMap2.put(str, localEntry.getValue());
        }
      }
    }
  }
  
  private static boolean a(Map.Entry<String, String> paramEntry)
  {
    String str = (String)paramEntry.getKey();
    paramEntry = (String)paramEntry.getValue();
    return (str.startsWith("&")) && (str.length() >= 2);
  }
  
  private static String b(Map.Entry<String, String> paramEntry)
  {
    if (!a(paramEntry)) {
      return null;
    }
    return ((String)paramEntry.getKey()).substring(1);
  }
  
  private static void b(Map<String, String> paramMap1, Map<String, String> paramMap2)
  {
    zzx.a(paramMap2);
    if (paramMap1 == null) {}
    for (;;)
    {
      return;
      paramMap1 = paramMap1.entrySet().iterator();
      while (paramMap1.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)paramMap1.next();
        String str = b(localEntry);
        if ((str != null) && (!paramMap2.containsKey(str))) {
          paramMap2.put(str, localEntry.getValue());
        }
      }
    }
  }
  
  protected void a()
  {
    this.e.E();
    String str = v().c();
    if (str != null) {
      a("&an", str);
    }
    str = v().b();
    if (str != null) {
      a("&av", str);
    }
  }
  
  public void a(Uri paramUri)
  {
    if ((paramUri == null) || (paramUri.isOpaque())) {}
    do
    {
      do
      {
        return;
        paramUri = paramUri.getQueryParameter("referrer");
      } while (TextUtils.isEmpty(paramUri));
      paramUri = Uri.parse("http://hostname/?" + paramUri);
      String str = paramUri.getQueryParameter("utm_id");
      if (str != null) {
        this.c.put("&ci", str);
      }
      str = paramUri.getQueryParameter("anid");
      if (str != null) {
        this.c.put("&anid", str);
      }
      str = paramUri.getQueryParameter("utm_campaign");
      if (str != null) {
        this.c.put("&cn", str);
      }
      str = paramUri.getQueryParameter("utm_content");
      if (str != null) {
        this.c.put("&cc", str);
      }
      str = paramUri.getQueryParameter("utm_medium");
      if (str != null) {
        this.c.put("&cm", str);
      }
      str = paramUri.getQueryParameter("utm_source");
      if (str != null) {
        this.c.put("&cs", str);
      }
      str = paramUri.getQueryParameter("utm_term");
      if (str != null) {
        this.c.put("&ck", str);
      }
      str = paramUri.getQueryParameter("dclid");
      if (str != null) {
        this.c.put("&dclid", str);
      }
      str = paramUri.getQueryParameter("gclid");
      if (str != null) {
        this.c.put("&gclid", str);
      }
      paramUri = paramUri.getQueryParameter("aclid");
    } while (paramUri == null);
    this.c.put("&aclid", paramUri);
  }
  
  public void a(String paramString1, String paramString2)
  {
    zzx.a(paramString1, "Key should be non-null");
    if (TextUtils.isEmpty(paramString1)) {
      return;
    }
    this.b.put(paramString1, paramString2);
  }
  
  public void a(final Map<String, String> paramMap)
  {
    final long l = n().a();
    if (s().f())
    {
      c("AppOptOut is set to true. Not sending Google Analytics hit");
      return;
    }
    boolean bool1 = s().e();
    final HashMap localHashMap = new HashMap();
    a(this.b, localHashMap);
    a(paramMap, localHashMap);
    final boolean bool2 = zzam.a((String)this.b.get("useSecure"), true);
    b(this.c, localHashMap);
    this.c.clear();
    paramMap = (String)localHashMap.get("t");
    if (TextUtils.isEmpty(paramMap))
    {
      p().a(localHashMap, "Missing hit type parameter");
      return;
    }
    final String str = (String)localHashMap.get("tid");
    if (TextUtils.isEmpty(str))
    {
      p().a(localHashMap, "Missing tracking id parameter");
      return;
    }
    final boolean bool3 = b();
    try
    {
      if (("screenview".equalsIgnoreCase(paramMap)) || ("pageview".equalsIgnoreCase(paramMap)) || ("appview".equalsIgnoreCase(paramMap)) || (TextUtils.isEmpty(paramMap)))
      {
        int j = Integer.parseInt((String)this.b.get("&a")) + 1;
        int i = j;
        if (j >= Integer.MAX_VALUE) {
          i = 1;
        }
        this.b.put("&a", Integer.toString(i));
      }
      r().a(new Runnable()
      {
        public void run()
        {
          boolean bool = true;
          if (Tracker.a(Tracker.this).b()) {
            localHashMap.put("sc", "start");
          }
          zzam.b(localHashMap, "cid", Tracker.this.s().h());
          Object localObject = (String)localHashMap.get("sf");
          if (localObject != null)
          {
            double d1 = zzam.a((String)localObject, 100.0D);
            if (zzam.a(d1, (String)localHashMap.get("cid")))
            {
              Tracker.this.b("Sampling enabled. Hit sampled out. sample rate", Double.valueOf(d1));
              return;
            }
          }
          localObject = Tracker.b(Tracker.this);
          if (bool3)
          {
            zzam.a(localHashMap, "ate", ((zza)localObject).b());
            zzam.a(localHashMap, "adid", ((zza)localObject).c());
            localObject = Tracker.c(Tracker.this).c();
            zzam.a(localHashMap, "an", ((zzpq)localObject).a());
            zzam.a(localHashMap, "av", ((zzpq)localObject).b());
            zzam.a(localHashMap, "aid", ((zzpq)localObject).c());
            zzam.a(localHashMap, "aiid", ((zzpq)localObject).d());
            localHashMap.put("v", "1");
            localHashMap.put("_v", zze.b);
            zzam.a(localHashMap, "ul", Tracker.d(Tracker.this).b().f());
            zzam.a(localHashMap, "sr", Tracker.e(Tracker.this).c());
            if ((!paramMap.equals("transaction")) && (!paramMap.equals("item"))) {
              break label383;
            }
          }
          label383:
          for (int i = 1;; i = 0)
          {
            if ((i != 0) || (Tracker.f(Tracker.this).a())) {
              break label388;
            }
            Tracker.g(Tracker.this).a(localHashMap, "Too many hits sent too quickly, rate limiting invoked");
            return;
            localHashMap.remove("ate");
            localHashMap.remove("adid");
            break;
          }
          label388:
          long l2 = zzam.a((String)localHashMap.get("ht"));
          long l1 = l2;
          if (l2 == 0L) {
            l1 = l;
          }
          if (bool2)
          {
            localObject = new zzab(Tracker.this, localHashMap, l1, str);
            Tracker.h(Tracker.this).c("Dry run enabled. Would have sent hit", localObject);
            return;
          }
          localObject = (String)localHashMap.get("cid");
          HashMap localHashMap = new HashMap();
          zzam.a(localHashMap, "uid", localHashMap);
          zzam.a(localHashMap, "an", localHashMap);
          zzam.a(localHashMap, "aid", localHashMap);
          zzam.a(localHashMap, "av", localHashMap);
          zzam.a(localHashMap, "aiid", localHashMap);
          String str = this.g;
          if (!TextUtils.isEmpty((CharSequence)localHashMap.get("adid"))) {}
          for (;;)
          {
            localObject = new zzh(0L, (String)localObject, str, bool, 0L, localHashMap);
            l2 = Tracker.i(Tracker.this).a((zzh)localObject);
            localHashMap.put("_s", String.valueOf(l2));
            localObject = new zzab(Tracker.this, localHashMap, l1, str);
            Tracker.j(Tracker.this).a((zzab)localObject);
            return;
            bool = false;
          }
        }
      });
      return;
    }
    finally {}
  }
  
  public void a(boolean paramBoolean)
  {
    this.a = paramBoolean;
  }
  
  boolean b()
  {
    return this.a;
  }
  
  private class zza
    extends zzd
    implements GoogleAnalytics.zza
  {
    private boolean b;
    private int c;
    private long d = -1L;
    private boolean e;
    private long f;
    
    protected zza(zzf paramzzf)
    {
      super();
    }
    
    protected void a() {}
    
    public void a(Activity paramActivity)
    {
      if ((this.c == 0) && (c())) {
        this.e = true;
      }
      this.c += 1;
      HashMap localHashMap;
      Tracker localTracker;
      if (this.b)
      {
        localObject = paramActivity.getIntent();
        if (localObject != null) {
          Tracker.this.a(((Intent)localObject).getData());
        }
        localHashMap = new HashMap();
        localHashMap.put("&t", "screenview");
        localTracker = Tracker.this;
        if (Tracker.k(Tracker.this) == null) {
          break label159;
        }
      }
      label159:
      for (Object localObject = Tracker.k(Tracker.this).a(paramActivity);; localObject = paramActivity.getClass().getCanonicalName())
      {
        localTracker.a("&cd", (String)localObject);
        if (TextUtils.isEmpty((CharSequence)localHashMap.get("&dr")))
        {
          paramActivity = Tracker.a(paramActivity);
          if (!TextUtils.isEmpty(paramActivity)) {
            localHashMap.put("&dr", paramActivity);
          }
        }
        Tracker.this.a(localHashMap);
        return;
      }
    }
    
    public void b(Activity paramActivity)
    {
      this.c -= 1;
      this.c = Math.max(0, this.c);
      if (this.c == 0) {
        this.f = n().b();
      }
    }
    
    public boolean b()
    {
      try
      {
        boolean bool = this.e;
        this.e = false;
        return bool;
      }
      finally
      {
        localObject = finally;
        throw ((Throwable)localObject);
      }
    }
    
    boolean c()
    {
      return n().b() >= this.f + Math.max(1000L, this.d);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/Tracker.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */