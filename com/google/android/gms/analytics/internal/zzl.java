package com.google.android.gms.analytics.internal;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.analytics.AnalyticsReceiver;
import com.google.android.gms.analytics.AnalyticsService;
import com.google.android.gms.analytics.CampaignTrackingReceiver;
import com.google.android.gms.analytics.CampaignTrackingService;
import com.google.android.gms.analytics.zza;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzkd;
import com.google.android.gms.internal.zzke;
import com.google.android.gms.internal.zzmq;
import com.google.android.gms.internal.zzpq;
import com.google.android.gms.internal.zzpr;
import com.google.android.gms.measurement.zzc;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class zzl
  extends zzd
{
  private boolean a;
  private final zzj b;
  private final zzah c;
  private final zzag d;
  private final zzi e;
  private long f;
  private final zzt g;
  private final zzt h;
  private final zzaj i;
  private long j;
  private boolean k;
  
  protected zzl(zzf paramzzf, zzg paramzzg)
  {
    super(paramzzf);
    zzx.a(paramzzg);
    this.f = Long.MIN_VALUE;
    this.d = paramzzg.k(paramzzf);
    this.b = paramzzg.m(paramzzf);
    this.c = paramzzg.n(paramzzf);
    this.e = paramzzg.o(paramzzf);
    this.i = new zzaj(n());
    this.g = new zzt(paramzzf)
    {
      public void a()
      {
        zzl.a(zzl.this);
      }
    };
    this.h = new zzt(paramzzf)
    {
      public void a()
      {
        zzl.b(zzl.this);
      }
    };
  }
  
  private void K()
  {
    Context localContext = k().b();
    if (!AnalyticsReceiver.a(localContext)) {
      e("AnalyticsReceiver is not registered or is disabled. Register the receiver for reliable dispatching on non-Google Play devices. See http://goo.gl/8Rd3yj for instructions.");
    }
    do
    {
      while (!CampaignTrackingReceiver.zzY(localContext))
      {
        e("CampaignTrackingReceiver is not registered, not exported or is disabled. Installation campaign tracking is not possible. See http://goo.gl/8Rd3yj for instructions.");
        return;
        if (!AnalyticsService.a(localContext)) {
          f("AnalyticsService is not registered or is disabled. Analytics service at risk of not starting. See http://goo.gl/8Rd3yj for instructions.");
        }
      }
    } while (CampaignTrackingService.zzZ(localContext));
    e("CampaignTrackingService is not registered or is disabled. Installation campaign tracking is not possible. See http://goo.gl/8Rd3yj for instructions.");
  }
  
  private void L()
  {
    a(new zzw()
    {
      public void a(Throwable paramAnonymousThrowable)
      {
        zzl.this.H();
      }
    });
  }
  
  private void M()
  {
    try
    {
      this.b.i();
      H();
      this.h.a(q().C());
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      for (;;)
      {
        d("Failed to delete stale hits", localSQLiteException);
      }
    }
  }
  
  private boolean N()
  {
    if (this.k) {}
    while (((q().a()) && (!q().b())) || (I() <= 0L)) {
      return false;
    }
    return true;
  }
  
  private void O()
  {
    zzv localzzv = u();
    if (!localzzv.b()) {}
    long l;
    do
    {
      do
      {
        return;
      } while (localzzv.c());
      l = G();
    } while ((l == 0L) || (Math.abs(n().a() - l) > q().k()));
    a("Dispatch alarm scheduled (ms)", Long.valueOf(q().j()));
    localzzv.d();
  }
  
  private void P()
  {
    O();
    long l2 = I();
    long l1 = w().d();
    if (l1 != 0L)
    {
      l1 = l2 - Math.abs(n().a() - l1);
      if (l1 <= 0L) {}
    }
    for (;;)
    {
      a("Dispatch scheduled (ms)", Long.valueOf(l1));
      if (!this.g.c()) {
        break;
      }
      l1 = Math.max(1L, l1 + this.g.b());
      this.g.b(l1);
      return;
      l1 = Math.min(q().h(), l2);
      continue;
      l1 = Math.min(q().h(), l2);
    }
    this.g.a(l1);
  }
  
  private void Q()
  {
    R();
    S();
  }
  
  private void R()
  {
    if (this.g.c()) {
      b("All hits dispatched or no network/service. Going to power save mode");
    }
    this.g.cancel();
  }
  
  private void S()
  {
    zzv localzzv = u();
    if (localzzv.c()) {
      localzzv.cancel();
    }
  }
  
  private void a(zzh paramzzh, zzpr paramzzpr)
  {
    zzx.a(paramzzh);
    zzx.a(paramzzpr);
    Object localObject1 = new zza(k());
    ((zza)localObject1).b(paramzzh.c());
    ((zza)localObject1).b(paramzzh.d());
    localObject1 = ((zza)localObject1).l();
    zzke localzzke = (zzke)((zzc)localObject1).b(zzke.class);
    localzzke.a("data");
    localzzke.b(true);
    ((zzc)localObject1).a(paramzzpr);
    zzkd localzzkd = (zzkd)((zzc)localObject1).b(zzkd.class);
    zzpq localzzpq = (zzpq)((zzc)localObject1).b(zzpq.class);
    Iterator localIterator = paramzzh.f().entrySet().iterator();
    while (localIterator.hasNext())
    {
      Object localObject2 = (Map.Entry)localIterator.next();
      String str = (String)((Map.Entry)localObject2).getKey();
      localObject2 = (String)((Map.Entry)localObject2).getValue();
      if ("an".equals(str)) {
        localzzpq.a((String)localObject2);
      } else if ("av".equals(str)) {
        localzzpq.b((String)localObject2);
      } else if ("aid".equals(str)) {
        localzzpq.c((String)localObject2);
      } else if ("aiid".equals(str)) {
        localzzpq.d((String)localObject2);
      } else if ("uid".equals(str)) {
        localzzke.c((String)localObject2);
      } else {
        localzzkd.a(str, (String)localObject2);
      }
    }
    b("Sending installation campaign to", paramzzh.c(), paramzzpr);
    ((zzc)localObject1).a(w().b());
    ((zzc)localObject1).e();
  }
  
  private boolean g(String paramString)
  {
    return o().checkCallingOrSelfPermission(paramString) == 0;
  }
  
  public void F()
  {
    com.google.android.gms.measurement.zzg.d();
    D();
    c("Sync dispatching local hits");
    long l = this.j;
    if (!q().a()) {
      g();
    }
    try
    {
      while (j()) {}
      w().e();
      H();
      if (this.j != l) {
        this.d.c();
      }
      return;
    }
    catch (Throwable localThrowable)
    {
      e("Sync local dispatch failed", localThrowable);
      H();
    }
  }
  
  public long G()
  {
    com.google.android.gms.measurement.zzg.d();
    D();
    try
    {
      long l = this.b.j();
      return l;
    }
    catch (SQLiteException localSQLiteException)
    {
      e("Failed to get min/max hit times from local store", localSQLiteException);
    }
    return 0L;
  }
  
  public void H()
  {
    k().s();
    D();
    if (!N())
    {
      this.d.b();
      Q();
      return;
    }
    if (this.b.h())
    {
      this.d.b();
      Q();
      return;
    }
    if (!((Boolean)zzy.J.a()).booleanValue()) {
      this.d.a();
    }
    for (boolean bool = this.d.e(); bool; bool = true)
    {
      P();
      return;
    }
    Q();
    O();
  }
  
  public long I()
  {
    long l;
    if (this.f != Long.MIN_VALUE) {
      l = this.f;
    }
    do
    {
      return l;
      l = q().i();
    } while (!v().f());
    return v().g() * 1000L;
  }
  
  public void J()
  {
    D();
    m();
    this.k = true;
    this.e.e();
    H();
  }
  
  public long a(zzh paramzzh, boolean paramBoolean)
  {
    zzx.a(paramzzh);
    D();
    m();
    long l;
    for (;;)
    {
      try
      {
        this.b.b();
        this.b.a(paramzzh.a(), paramzzh.b());
        l = this.b.a(paramzzh.a(), paramzzh.b(), paramzzh.c());
        if (!paramBoolean)
        {
          paramzzh.a(l);
          this.b.a(paramzzh);
          this.b.c();
        }
      }
      catch (SQLiteException paramzzh)
      {
        paramzzh = paramzzh;
        e("Failed to update Analytics property", paramzzh);
        try
        {
          this.b.d();
          return -1L;
        }
        catch (SQLiteException paramzzh)
        {
          e("Failed to end transaction", paramzzh);
          return -1L;
        }
      }
      finally {}
      try
      {
        this.b.d();
        return l;
      }
      catch (SQLiteException paramzzh)
      {
        e("Failed to end transaction", paramzzh);
        return l;
      }
      paramzzh.a(1L + l);
    }
    try
    {
      this.b.d();
      throw paramzzh;
    }
    catch (SQLiteException localSQLiteException)
    {
      for (;;)
      {
        e("Failed to end transaction", localSQLiteException);
      }
    }
  }
  
  protected void a()
  {
    this.b.E();
    this.c.E();
    this.e.E();
  }
  
  public void a(long paramLong)
  {
    com.google.android.gms.measurement.zzg.d();
    D();
    long l = paramLong;
    if (paramLong < 0L) {
      l = 0L;
    }
    this.f = l;
    H();
  }
  
  public void a(zzab paramzzab)
  {
    zzx.a(paramzzab);
    com.google.android.gms.measurement.zzg.d();
    D();
    if (this.k) {
      c("Hit delivery not possible. Missing network permissions. See http://goo.gl/8Rd3yj for instructions");
    }
    for (;;)
    {
      paramzzab = b(paramzzab);
      g();
      if (!this.e.a(paramzzab)) {
        break;
      }
      c("Hit sent to the device AnalyticsService for delivery");
      return;
      a("Delivering hit", paramzzab);
    }
    if (q().a())
    {
      p().a(paramzzab, "Service unavailable on package side");
      return;
    }
    try
    {
      this.b.a(paramzzab);
      H();
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      e("Delivery failed to save hit to a database", localSQLiteException);
      p().a(paramzzab, "deliver: failed to insert hit to database");
    }
  }
  
  protected void a(zzh paramzzh)
  {
    m();
    b("Sending first hit to property", paramzzh.c());
    if (w().c().a(q().F())) {}
    do
    {
      return;
      localObject = w().f();
    } while (TextUtils.isEmpty((CharSequence)localObject));
    Object localObject = zzam.a(p(), (String)localObject);
    b("Found relevant installation campaign", localObject);
    a(paramzzh, (zzpr)localObject);
  }
  
  public void a(zzw paramzzw)
  {
    a(paramzzw, this.j);
  }
  
  public void a(final zzw paramzzw, final long paramLong)
  {
    com.google.android.gms.measurement.zzg.d();
    D();
    long l1 = -1L;
    long l2 = w().d();
    if (l2 != 0L) {
      l1 = Math.abs(n().a() - l2);
    }
    b("Dispatching local hits. Elapsed time since last dispatch (ms)", Long.valueOf(l1));
    if (!q().a()) {
      g();
    }
    try
    {
      if (j())
      {
        r().a(new Runnable()
        {
          public void run()
          {
            zzl.this.a(paramzzw, paramLong);
          }
        });
        return;
      }
      w().e();
      H();
      if (paramzzw != null) {
        paramzzw.a(null);
      }
      if (this.j != paramLong)
      {
        this.d.c();
        return;
      }
    }
    catch (Throwable localThrowable)
    {
      e("Local dispatch failed", localThrowable);
      w().e();
      H();
      if (paramzzw != null) {
        paramzzw.a(localThrowable);
      }
    }
  }
  
  public void a(String paramString)
  {
    zzx.a(paramString);
    m();
    l();
    zzpr localzzpr = zzam.a(p(), paramString);
    if (localzzpr == null) {
      d("Parsing failed. Ignoring invalid campaign data", paramString);
    }
    for (;;)
    {
      return;
      String str = w().f();
      if (paramString.equals(str))
      {
        e("Ignoring duplicate install campaign");
        return;
      }
      if (!TextUtils.isEmpty(str))
      {
        d("Ignoring multiple install campaigns. original, new", str, paramString);
        return;
      }
      w().a(paramString);
      if (w().c().a(q().F()))
      {
        d("Campaign received too late, ignoring", localzzpr);
        return;
      }
      b("Received installation campaign", localzzpr);
      paramString = this.b.d(0L).iterator();
      while (paramString.hasNext()) {
        a((zzh)paramString.next(), localzzpr);
      }
    }
  }
  
  public void a(boolean paramBoolean)
  {
    H();
  }
  
  zzab b(zzab paramzzab)
  {
    if (!TextUtils.isEmpty(paramzzab.h())) {}
    do
    {
      return paramzzab;
      localObject2 = w().g().a();
    } while (localObject2 == null);
    Object localObject1 = (Long)((Pair)localObject2).second;
    Object localObject2 = (String)((Pair)localObject2).first;
    localObject1 = localObject1 + ":" + (String)localObject2;
    localObject2 = new HashMap(paramzzab.b());
    ((Map)localObject2).put("_m", localObject1);
    return zzab.a(this, paramzzab, (Map)localObject2);
  }
  
  void b()
  {
    D();
    if (!this.a) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.a(bool, "Analytics backend already started");
      this.a = true;
      if (!q().a()) {
        K();
      }
      r().a(new Runnable()
      {
        public void run()
        {
          zzl.this.c();
        }
      });
      return;
    }
  }
  
  protected void c()
  {
    D();
    w().b();
    if (!g("android.permission.ACCESS_NETWORK_STATE"))
    {
      f("Missing required android.permission.ACCESS_NETWORK_STATE. Google Analytics disabled. See http://goo.gl/8Rd3yj for instructions");
      J();
    }
    if (!g("android.permission.INTERNET"))
    {
      f("Missing required android.permission.INTERNET. Google Analytics disabled. See http://goo.gl/8Rd3yj for instructions");
      J();
    }
    if (AnalyticsService.a(o())) {
      b("AnalyticsService registered in the app manifest and enabled");
    }
    for (;;)
    {
      if ((!this.k) && (!q().a()) && (!this.b.h())) {
        g();
      }
      H();
      return;
      if (q().a()) {
        f("Device AnalyticsService not registered! Hits will not be delivered reliably.");
      } else {
        e("AnalyticsService not registered in the app manifest. Hits might not be delivered reliably. See http://goo.gl/8Rd3yj for instructions.");
      }
    }
  }
  
  void d()
  {
    m();
    this.j = n().a();
  }
  
  protected void e()
  {
    m();
    if (!q().a()) {
      i();
    }
  }
  
  public void f()
  {
    com.google.android.gms.measurement.zzg.d();
    D();
    b("Service disconnected");
  }
  
  protected void g()
  {
    if (this.k) {}
    do
    {
      long l;
      do
      {
        do
        {
          return;
        } while ((!q().c()) || (this.e.b()));
        l = q().x();
      } while (!this.i.a(l));
      this.i.a();
      b("Connecting to service");
    } while (!this.e.d());
    b("Connected to service");
    this.i.b();
    e();
  }
  
  public void h()
  {
    com.google.android.gms.measurement.zzg.d();
    D();
    if (!q().a()) {
      b("Delete all hits from local store");
    }
    try
    {
      this.b.e();
      this.b.f();
      H();
      g();
      if (this.e.c()) {
        b("Device service unavailable. Can't clear hits stored on the device service.");
      }
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      for (;;)
      {
        d("Failed to delete hits from store", localSQLiteException);
      }
    }
  }
  
  public void i()
  {
    com.google.android.gms.measurement.zzg.d();
    D();
    l();
    if (!q().c()) {
      e("Service client disabled. Can't dispatch local hits to device AnalyticsService");
    }
    if (!this.e.b()) {
      b("Service not connected");
    }
    while (this.b.h()) {
      return;
    }
    b("Dispatching local hits to device AnalyticsService");
    for (;;)
    {
      try
      {
        List localList = this.b.b(q().l());
        if (!localList.isEmpty()) {
          break label126;
        }
        H();
        return;
      }
      catch (SQLiteException localSQLiteException1)
      {
        e("Failed to read hits from store", localSQLiteException1);
        Q();
        return;
      }
      label107:
      Object localObject;
      localSQLiteException1.remove(localObject);
      try
      {
        this.b.c(((zzab)localObject).c());
        label126:
        if (!localSQLiteException1.isEmpty())
        {
          localObject = (zzab)localSQLiteException1.get(0);
          if (this.e.a((zzab)localObject)) {
            break label107;
          }
          H();
          return;
        }
      }
      catch (SQLiteException localSQLiteException2)
      {
        e("Failed to remove hit that was send for delivery", localSQLiteException2);
        Q();
      }
    }
  }
  
  protected boolean j()
  {
    int n = 1;
    com.google.android.gms.measurement.zzg.d();
    D();
    b("Dispatching a batch of local hits");
    int m;
    if ((!this.e.b()) && (!q().a()))
    {
      m = 1;
      if (this.c.b()) {
        break label70;
      }
    }
    for (;;)
    {
      if ((m == 0) || (n == 0)) {
        break label75;
      }
      b("No network or service available. Will retry later");
      return false;
      m = 0;
      break;
      label70:
      n = 0;
    }
    label75:
    long l3 = Math.max(q().l(), q().m());
    ArrayList localArrayList = new ArrayList();
    l1 = 0L;
    for (;;)
    {
      try
      {
        this.b.b();
        localArrayList.clear();
        try
        {
          localList = this.b.b(l3);
          if (localList.isEmpty())
          {
            b("Store is empty, nothing to dispatch");
            Q();
            try
            {
              this.b.c();
              this.b.d();
              return false;
            }
            catch (SQLiteException localSQLiteException1)
            {
              e("Failed to commit local dispatch transaction", localSQLiteException1);
              Q();
              return false;
            }
          }
          a("Hits loaded from store. count", Integer.valueOf(localList.size()));
          localObject2 = localList.iterator();
          if (((Iterator)localObject2).hasNext())
          {
            if (((zzab)((Iterator)localObject2).next()).c() != l1) {
              continue;
            }
            d("Database contains successfully uploaded hit", Long.valueOf(l1), Integer.valueOf(localList.size()));
            Q();
            try
            {
              this.b.c();
              this.b.d();
              return false;
            }
            catch (SQLiteException localSQLiteException2)
            {
              e("Failed to commit local dispatch transaction", localSQLiteException2);
              Q();
              return false;
            }
          }
          l2 = l1;
        }
        catch (SQLiteException localSQLiteException3)
        {
          d("Failed to read hits from persisted store", localSQLiteException3);
          Q();
          try
          {
            this.b.c();
            this.b.d();
            return false;
          }
          catch (SQLiteException localSQLiteException4)
          {
            e("Failed to commit local dispatch transaction", localSQLiteException4);
            Q();
            return false;
          }
          l2 = l1;
          if (!this.e.b()) {
            continue;
          }
        }
        if (q().a()) {
          continue;
        }
        b("Service connected, sending hits to the service");
        l2 = l1;
        if (localList.isEmpty()) {
          continue;
        }
        localObject2 = (zzab)localList.get(0);
        if (this.e.a((zzab)localObject2)) {
          continue;
        }
      }
      finally
      {
        long l2;
        try
        {
          List localList;
          Object localObject2;
          Iterator localIterator;
          this.b.c();
          this.b.d();
          throw ((Throwable)localObject1);
        }
        catch (SQLiteException localSQLiteException11)
        {
          e("Failed to commit local dispatch transaction", localSQLiteException11);
          Q();
          return false;
        }
        l1 = l2;
        continue;
      }
      l2 = l1;
      if (this.c.b())
      {
        localObject2 = this.c.a(localList);
        localIterator = ((List)localObject2).iterator();
        if (localIterator.hasNext())
        {
          l1 = Math.max(l1, ((Long)localIterator.next()).longValue());
          continue;
          l1 = Math.max(l1, ((zzab)localObject2).c());
          localList.remove(localObject2);
          b("Hit sent do device AnalyticsService for delivery", localObject2);
          try
          {
            this.b.c(((zzab)localObject2).c());
            localSQLiteException4.add(Long.valueOf(((zzab)localObject2).c()));
          }
          catch (SQLiteException localSQLiteException5)
          {
            e("Failed to remove hit that was send for delivery", localSQLiteException5);
            Q();
            try
            {
              this.b.c();
              this.b.d();
              return false;
            }
            catch (SQLiteException localSQLiteException6)
            {
              e("Failed to commit local dispatch transaction", localSQLiteException6);
              Q();
              return false;
            }
          }
        }
        localList.removeAll((Collection)localObject2);
      }
      try
      {
        this.b.a((List)localObject2);
        localSQLiteException6.addAll((Collection)localObject2);
        l2 = l1;
        boolean bool = localSQLiteException6.isEmpty();
        if (bool) {
          try
          {
            this.b.c();
            this.b.d();
            return false;
          }
          catch (SQLiteException localSQLiteException7)
          {
            e("Failed to commit local dispatch transaction", localSQLiteException7);
            Q();
            return false;
          }
        }
      }
      catch (SQLiteException localSQLiteException8)
      {
        e("Failed to remove successfully uploaded hits", localSQLiteException8);
        Q();
        try
        {
          this.b.c();
          this.b.d();
          return false;
        }
        catch (SQLiteException localSQLiteException9)
        {
          e("Failed to commit local dispatch transaction", localSQLiteException9);
          Q();
          return false;
        }
        try
        {
          this.b.c();
          this.b.d();
          l1 = l2;
        }
        catch (SQLiteException localSQLiteException10)
        {
          e("Failed to commit local dispatch transaction", localSQLiteException10);
          Q();
          return false;
        }
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/internal/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */