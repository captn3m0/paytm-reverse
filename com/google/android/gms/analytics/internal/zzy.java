package com.google.android.gms.analytics.internal;

import com.google.android.gms.common.internal.zzd;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzlz;

public final class zzy
{
  public static zza<Integer> A = zza.a("analytics.max_batch_post_length", 8192);
  public static zza<String> B = zza.a("analytics.fallback_responses.k", "404,502");
  public static zza<Integer> C = zza.a("analytics.batch_retry_interval.seconds.k", 3600);
  public static zza<Long> D = zza.a("analytics.service_monitor_interval", 86400000L);
  public static zza<Integer> E = zza.a("analytics.http_connection.connect_timeout_millis", 60000);
  public static zza<Integer> F = zza.a("analytics.http_connection.read_timeout_millis", 61000);
  public static zza<Long> G = zza.a("analytics.campaigns.time_limit", 86400000L);
  public static zza<String> H = zza.a("analytics.first_party_experiment_id", "");
  public static zza<Integer> I = zza.a("analytics.first_party_experiment_variant", 0);
  public static zza<Boolean> J = zza.a("analytics.test.disable_receiver", false);
  public static zza<Long> K = zza.a("analytics.service_client.idle_disconnect_millis", 10000L, 10000L);
  public static zza<Long> L = zza.a("analytics.service_client.connect_timeout_millis", 5000L);
  public static zza<Long> M = zza.a("analytics.service_client.second_connect_delay_millis", 5000L);
  public static zza<Long> N = zza.a("analytics.service_client.unexpected_reconnect_millis", 60000L);
  public static zza<Long> O = zza.a("analytics.service_client.reconnect_throttle_millis", 1800000L);
  public static zza<Long> P = zza.a("analytics.monitoring.sample_period_millis", 86400000L);
  public static zza<Long> Q = zza.a("analytics.initialization_warning_threshold", 5000L);
  public static zza<Boolean> a = zza.a("analytics.service_enabled", false);
  public static zza<Boolean> b = zza.a("analytics.service_client_enabled", true);
  public static zza<String> c = zza.a("analytics.log_tag", "GAv4", "GAv4-SVC");
  public static zza<Long> d = zza.a("analytics.max_tokens", 60L);
  public static zza<Float> e = zza.a("analytics.tokens_per_sec", 0.5F);
  public static zza<Integer> f = zza.a("analytics.max_stored_hits", 2000, 20000);
  public static zza<Integer> g = zza.a("analytics.max_stored_hits_per_app", 2000);
  public static zza<Integer> h = zza.a("analytics.max_stored_properties_per_app", 100);
  public static zza<Long> i = zza.a("analytics.local_dispatch_millis", 1800000L, 120000L);
  public static zza<Long> j = zza.a("analytics.initial_local_dispatch_millis", 5000L, 5000L);
  public static zza<Long> k = zza.a("analytics.min_local_dispatch_millis", 120000L);
  public static zza<Long> l = zza.a("analytics.max_local_dispatch_millis", 7200000L);
  public static zza<Long> m = zza.a("analytics.dispatch_alarm_millis", 7200000L);
  public static zza<Long> n = zza.a("analytics.max_dispatch_alarm_millis", 32400000L);
  public static zza<Integer> o = zza.a("analytics.max_hits_per_dispatch", 20);
  public static zza<Integer> p = zza.a("analytics.max_hits_per_batch", 20);
  public static zza<String> q = zza.a("analytics.insecure_host", "http://www.google-analytics.com");
  public static zza<String> r = zza.a("analytics.secure_host", "https://ssl.google-analytics.com");
  public static zza<String> s = zza.a("analytics.simple_endpoint", "/collect");
  public static zza<String> t = zza.a("analytics.batching_endpoint", "/batch");
  public static zza<Integer> u = zza.a("analytics.max_get_length", 2036);
  public static zza<String> v = zza.a("analytics.batching_strategy.k", zzm.e.name(), zzm.e.name());
  public static zza<String> w = zza.a("analytics.compression_strategy.k", zzo.b.name());
  public static zza<Integer> x = zza.a("analytics.max_hits_per_request.k", 20);
  public static zza<Integer> y = zza.a("analytics.max_hit_length.k", 8192);
  public static zza<Integer> z = zza.a("analytics.max_post_length.k", 8192);
  
  public static final class zza<V>
  {
    private final V a;
    private final zzlz<V> b;
    private V c;
    
    private zza(zzlz<V> paramzzlz, V paramV)
    {
      zzx.a(paramzzlz);
      this.b = paramzzlz;
      this.a = paramV;
    }
    
    static zza<Float> a(String paramString, float paramFloat)
    {
      return a(paramString, paramFloat, paramFloat);
    }
    
    static zza<Float> a(String paramString, float paramFloat1, float paramFloat2)
    {
      return new zza(zzlz.a(paramString, Float.valueOf(paramFloat2)), Float.valueOf(paramFloat1));
    }
    
    static zza<Integer> a(String paramString, int paramInt)
    {
      return a(paramString, paramInt, paramInt);
    }
    
    static zza<Integer> a(String paramString, int paramInt1, int paramInt2)
    {
      return new zza(zzlz.a(paramString, Integer.valueOf(paramInt2)), Integer.valueOf(paramInt1));
    }
    
    static zza<Long> a(String paramString, long paramLong)
    {
      return a(paramString, paramLong, paramLong);
    }
    
    static zza<Long> a(String paramString, long paramLong1, long paramLong2)
    {
      return new zza(zzlz.a(paramString, Long.valueOf(paramLong2)), Long.valueOf(paramLong1));
    }
    
    static zza<String> a(String paramString1, String paramString2)
    {
      return a(paramString1, paramString2, paramString2);
    }
    
    static zza<String> a(String paramString1, String paramString2, String paramString3)
    {
      return new zza(zzlz.a(paramString1, paramString3), paramString2);
    }
    
    static zza<Boolean> a(String paramString, boolean paramBoolean)
    {
      return a(paramString, paramBoolean, paramBoolean);
    }
    
    static zza<Boolean> a(String paramString, boolean paramBoolean1, boolean paramBoolean2)
    {
      return new zza(zzlz.a(paramString, paramBoolean2), Boolean.valueOf(paramBoolean1));
    }
    
    public V a()
    {
      if (this.c != null) {
        return (V)this.c;
      }
      if ((zzd.a) && (zzlz.b())) {
        return (V)this.b.d();
      }
      return (V)this.a;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/internal/zzy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */