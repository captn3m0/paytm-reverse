package com.google.android.gms.analytics.internal;

public abstract class zzd
  extends zzc
{
  private boolean a;
  private boolean b;
  
  protected zzd(zzf paramzzf)
  {
    super(paramzzf);
  }
  
  public boolean C()
  {
    return (this.a) && (!this.b);
  }
  
  protected void D()
  {
    if (!C()) {
      throw new IllegalStateException("Not initialized");
    }
  }
  
  public void E()
  {
    a();
    this.a = true;
  }
  
  protected abstract void a();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/internal/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */