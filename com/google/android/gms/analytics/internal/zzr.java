package com.google.android.gms.analytics.internal;

import android.text.TextUtils;
import com.google.android.gms.common.internal.zzd;
import com.google.android.gms.common.internal.zzx;
import java.util.HashSet;
import java.util.Set;

public class zzr
{
  private final zzf a;
  private volatile Boolean b;
  private String c;
  private Set<Integer> d;
  
  protected zzr(zzf paramzzf)
  {
    zzx.a(paramzzf);
    this.a = paramzzf;
  }
  
  public String A()
  {
    return "google_analytics_v4.db";
  }
  
  public String B()
  {
    return "google_analytics2_v4.db";
  }
  
  public long C()
  {
    return 86400000L;
  }
  
  public int D()
  {
    return ((Integer)zzy.E.a()).intValue();
  }
  
  public int E()
  {
    return ((Integer)zzy.F.a()).intValue();
  }
  
  public long F()
  {
    return ((Long)zzy.G.a()).longValue();
  }
  
  public long G()
  {
    return ((Long)zzy.P.a()).longValue();
  }
  
  public boolean a()
  {
    return zzd.a;
  }
  
  /* Error */
  public boolean b()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 77	com/google/android/gms/analytics/internal/zzr:b	Ljava/lang/Boolean;
    //   4: ifnonnull +129 -> 133
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield 77	com/google/android/gms/analytics/internal/zzr:b	Ljava/lang/Boolean;
    //   13: ifnonnull +118 -> 131
    //   16: aload_0
    //   17: getfield 25	com/google/android/gms/analytics/internal/zzr:a	Lcom/google/android/gms/analytics/internal/zzf;
    //   20: invokevirtual 82	com/google/android/gms/analytics/internal/zzf:b	()Landroid/content/Context;
    //   23: invokevirtual 88	android/content/Context:getApplicationInfo	()Landroid/content/pm/ApplicationInfo;
    //   26: astore_3
    //   27: aload_0
    //   28: getfield 25	com/google/android/gms/analytics/internal/zzr:a	Lcom/google/android/gms/analytics/internal/zzf;
    //   31: invokevirtual 82	com/google/android/gms/analytics/internal/zzf:b	()Landroid/content/Context;
    //   34: invokestatic 93	android/os/Process:myPid	()I
    //   37: invokestatic 98	com/google/android/gms/internal/zznf:a	(Landroid/content/Context;I)Ljava/lang/String;
    //   40: astore_2
    //   41: aload_3
    //   42: ifnull +30 -> 72
    //   45: aload_3
    //   46: getfield 103	android/content/pm/ApplicationInfo:processName	Ljava/lang/String;
    //   49: astore_3
    //   50: aload_3
    //   51: ifnull +90 -> 141
    //   54: aload_3
    //   55: aload_2
    //   56: invokevirtual 109	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   59: ifeq +82 -> 141
    //   62: iconst_1
    //   63: istore_1
    //   64: aload_0
    //   65: iload_1
    //   66: invokestatic 115	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   69: putfield 77	com/google/android/gms/analytics/internal/zzr:b	Ljava/lang/Boolean;
    //   72: aload_0
    //   73: getfield 77	com/google/android/gms/analytics/internal/zzr:b	Ljava/lang/Boolean;
    //   76: ifnull +13 -> 89
    //   79: aload_0
    //   80: getfield 77	com/google/android/gms/analytics/internal/zzr:b	Ljava/lang/Boolean;
    //   83: invokevirtual 118	java/lang/Boolean:booleanValue	()Z
    //   86: ifne +19 -> 105
    //   89: ldc 120
    //   91: aload_2
    //   92: invokevirtual 109	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   95: ifeq +10 -> 105
    //   98: aload_0
    //   99: getstatic 123	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   102: putfield 77	com/google/android/gms/analytics/internal/zzr:b	Ljava/lang/Boolean;
    //   105: aload_0
    //   106: getfield 77	com/google/android/gms/analytics/internal/zzr:b	Ljava/lang/Boolean;
    //   109: ifnonnull +22 -> 131
    //   112: aload_0
    //   113: getstatic 123	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   116: putfield 77	com/google/android/gms/analytics/internal/zzr:b	Ljava/lang/Boolean;
    //   119: aload_0
    //   120: getfield 25	com/google/android/gms/analytics/internal/zzr:a	Lcom/google/android/gms/analytics/internal/zzf;
    //   123: invokevirtual 127	com/google/android/gms/analytics/internal/zzf:f	()Lcom/google/android/gms/analytics/internal/zzaf;
    //   126: ldc -127
    //   128: invokevirtual 134	com/google/android/gms/analytics/internal/zzaf:f	(Ljava/lang/String;)V
    //   131: aload_0
    //   132: monitorexit
    //   133: aload_0
    //   134: getfield 77	com/google/android/gms/analytics/internal/zzr:b	Ljava/lang/Boolean;
    //   137: invokevirtual 118	java/lang/Boolean:booleanValue	()Z
    //   140: ireturn
    //   141: iconst_0
    //   142: istore_1
    //   143: goto -79 -> 64
    //   146: astore_2
    //   147: aload_0
    //   148: monitorexit
    //   149: aload_2
    //   150: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	151	0	this	zzr
    //   63	80	1	bool	boolean
    //   40	52	2	str	String
    //   146	4	2	localObject1	Object
    //   26	29	3	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   9	41	146	finally
    //   45	50	146	finally
    //   54	62	146	finally
    //   64	72	146	finally
    //   72	89	146	finally
    //   89	105	146	finally
    //   105	131	146	finally
    //   131	133	146	finally
    //   147	149	146	finally
  }
  
  public boolean c()
  {
    return ((Boolean)zzy.b.a()).booleanValue();
  }
  
  public int d()
  {
    return ((Integer)zzy.u.a()).intValue();
  }
  
  public int e()
  {
    return ((Integer)zzy.y.a()).intValue();
  }
  
  public int f()
  {
    return ((Integer)zzy.z.a()).intValue();
  }
  
  public int g()
  {
    return ((Integer)zzy.A.a()).intValue();
  }
  
  public long h()
  {
    return ((Long)zzy.j.a()).longValue();
  }
  
  public long i()
  {
    return ((Long)zzy.i.a()).longValue();
  }
  
  public long j()
  {
    return ((Long)zzy.m.a()).longValue();
  }
  
  public long k()
  {
    return ((Long)zzy.n.a()).longValue();
  }
  
  public int l()
  {
    return ((Integer)zzy.o.a()).intValue();
  }
  
  public int m()
  {
    return ((Integer)zzy.p.a()).intValue();
  }
  
  public long n()
  {
    return ((Integer)zzy.C.a()).intValue();
  }
  
  public String o()
  {
    return (String)zzy.r.a();
  }
  
  public String p()
  {
    return (String)zzy.q.a();
  }
  
  public String q()
  {
    return (String)zzy.s.a();
  }
  
  public String r()
  {
    return (String)zzy.t.a();
  }
  
  public zzm s()
  {
    return zzm.a((String)zzy.v.a());
  }
  
  public zzo t()
  {
    return zzo.a((String)zzy.w.a());
  }
  
  public Set<Integer> u()
  {
    String str1 = (String)zzy.B.a();
    String[] arrayOfString;
    HashSet localHashSet;
    int j;
    int i;
    if ((this.d == null) || (this.c == null) || (!this.c.equals(str1)))
    {
      arrayOfString = TextUtils.split(str1, ",");
      localHashSet = new HashSet();
      j = arrayOfString.length;
      i = 0;
    }
    for (;;)
    {
      String str2;
      if (i < j) {
        str2 = arrayOfString[i];
      }
      try
      {
        localHashSet.add(Integer.valueOf(Integer.parseInt(str2)));
        i += 1;
        continue;
        this.c = str1;
        this.d = localHashSet;
        return this.d;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        for (;;) {}
      }
    }
  }
  
  public long v()
  {
    return ((Long)zzy.K.a()).longValue();
  }
  
  public long w()
  {
    return ((Long)zzy.L.a()).longValue();
  }
  
  public long x()
  {
    return ((Long)zzy.O.a()).longValue();
  }
  
  public int y()
  {
    return ((Integer)zzy.f.a()).intValue();
  }
  
  public int z()
  {
    return ((Integer)zzy.h.a()).intValue();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/internal/zzr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */