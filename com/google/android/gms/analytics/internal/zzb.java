package com.google.android.gms.analytics.internal;

import android.content.Context;
import android.content.Intent;
import com.google.android.gms.analytics.AnalyticsReceiver;
import com.google.android.gms.analytics.AnalyticsService;
import com.google.android.gms.common.internal.zzx;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class zzb
  extends zzd
{
  private final zzl a;
  
  public zzb(zzf paramzzf, zzg paramzzg)
  {
    super(paramzzf);
    zzx.a(paramzzg);
    this.a = paramzzg.j(paramzzf);
  }
  
  public long a(zzh paramzzh)
  {
    D();
    zzx.a(paramzzh);
    m();
    long l = this.a.a(paramzzh, true);
    if (l == 0L) {
      this.a.a(paramzzh);
    }
    return l;
  }
  
  protected void a()
  {
    this.a.E();
  }
  
  public void a(final zzab paramzzab)
  {
    zzx.a(paramzzab);
    D();
    b("Hit delivery requested", paramzzab);
    r().a(new Runnable()
    {
      public void run()
      {
        zzb.a(zzb.this).a(paramzzab);
      }
    });
  }
  
  public void a(final zzw paramzzw)
  {
    D();
    r().a(new Runnable()
    {
      public void run()
      {
        zzb.a(zzb.this).a(paramzzw);
      }
    });
  }
  
  public void a(final String paramString, final Runnable paramRunnable)
  {
    zzx.a(paramString, "campaign param can't be empty");
    r().a(new Runnable()
    {
      public void run()
      {
        zzb.a(zzb.this).a(paramString);
        if (paramRunnable != null) {
          paramRunnable.run();
        }
      }
    });
  }
  
  public void a(final boolean paramBoolean)
  {
    a("Network connectivity status changed", Boolean.valueOf(paramBoolean));
    r().a(new Runnable()
    {
      public void run()
      {
        zzb.a(zzb.this).a(paramBoolean);
      }
    });
  }
  
  public void b()
  {
    this.a.b();
  }
  
  public void c()
  {
    D();
    Context localContext = o();
    if ((AnalyticsReceiver.a(localContext)) && (AnalyticsService.a(localContext)))
    {
      Intent localIntent = new Intent(localContext, AnalyticsService.class);
      localIntent.setAction("com.google.android.gms.analytics.ANALYTICS_DISPATCH");
      localContext.startService(localIntent);
      return;
    }
    a(null);
  }
  
  public boolean d()
  {
    D();
    Future localFuture = r().a(new Callable()
    {
      public Void a()
        throws Exception
      {
        zzb.a(zzb.this).F();
        return null;
      }
    });
    try
    {
      localFuture.get(4L, TimeUnit.SECONDS);
      return true;
    }
    catch (InterruptedException localInterruptedException)
    {
      d("syncDispatchLocalHits interrupted", localInterruptedException);
      return false;
    }
    catch (ExecutionException localExecutionException)
    {
      e("syncDispatchLocalHits failed", localExecutionException);
      return false;
    }
    catch (TimeoutException localTimeoutException)
    {
      d("syncDispatchLocalHits timed out", localTimeoutException);
    }
    return false;
  }
  
  public void e()
  {
    D();
    com.google.android.gms.measurement.zzg.d();
    this.a.f();
  }
  
  public void f()
  {
    b("Radio powered up");
    c();
  }
  
  void g()
  {
    m();
    this.a.e();
  }
  
  void h()
  {
    m();
    this.a.d();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/internal/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */