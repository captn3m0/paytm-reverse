package com.google.android.gms.analytics.internal;

import android.util.Log;
import com.google.android.gms.analytics.Logger;

class zzs
  implements Logger
{
  private int a = 2;
  private boolean b;
  
  public void error(String paramString) {}
  
  public int getLogLevel()
  {
    return this.a;
  }
  
  public void setLogLevel(int paramInt)
  {
    this.a = paramInt;
    if (!this.b)
    {
      Log.i((String)zzy.c.a(), "Logger is deprecated. To enable debug logging, please run:\nadb shell setprop log.tag." + (String)zzy.c.a() + " DEBUG");
      this.b = true;
    }
  }
  
  public void verbose(String paramString) {}
  
  public void warn(String paramString) {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/internal/zzs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */