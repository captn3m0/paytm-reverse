package com.google.android.gms.analytics.internal;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzmq;
import com.google.android.gms.measurement.zzg;

abstract class zzt
{
  private static volatile Handler b;
  private final zzf a;
  private final Runnable c;
  private volatile long d;
  private boolean e;
  
  zzt(zzf paramzzf)
  {
    zzx.a(paramzzf);
    this.a = paramzzf;
    this.c = new Runnable()
    {
      public void run()
      {
        if (Looper.myLooper() == Looper.getMainLooper()) {
          zzt.a(zzt.this).h().a(this);
        }
        boolean bool;
        do
        {
          return;
          bool = zzt.this.c();
          zzt.a(zzt.this, 0L);
        } while ((!bool) || (zzt.b(zzt.this)));
        zzt.this.a();
      }
    };
  }
  
  private Handler d()
  {
    if (b != null) {
      return b;
    }
    try
    {
      if (b == null) {
        b = new Handler(this.a.b().getMainLooper());
      }
      Handler localHandler = b;
      return localHandler;
    }
    finally {}
  }
  
  public abstract void a();
  
  public void a(long paramLong)
  {
    cancel();
    if (paramLong >= 0L)
    {
      this.d = this.a.d().a();
      if (!d().postDelayed(this.c, paramLong)) {
        this.a.f().e("Failed to schedule delayed post. time", Long.valueOf(paramLong));
      }
    }
  }
  
  public long b()
  {
    if (this.d == 0L) {
      return 0L;
    }
    return Math.abs(this.a.d().a() - this.d);
  }
  
  public void b(long paramLong)
  {
    long l = 0L;
    if (!c()) {
      return;
    }
    if (paramLong < 0L)
    {
      cancel();
      return;
    }
    paramLong -= Math.abs(this.a.d().a() - this.d);
    if (paramLong < 0L) {
      paramLong = l;
    }
    for (;;)
    {
      d().removeCallbacks(this.c);
      if (d().postDelayed(this.c, paramLong)) {
        break;
      }
      this.a.f().e("Failed to adjust delayed post. time", Long.valueOf(paramLong));
      return;
    }
  }
  
  public boolean c()
  {
    return this.d != 0L;
  }
  
  public void cancel()
  {
    this.d = 0L;
    d().removeCallbacks(this.c);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/internal/zzt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */