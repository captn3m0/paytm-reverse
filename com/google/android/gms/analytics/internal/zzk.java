package com.google.android.gms.analytics.internal;

import com.google.android.gms.internal.zzpq;
import com.google.android.gms.measurement.zzg;

public class zzk
  extends zzd
{
  private final zzpq a = new zzpq();
  
  zzk(zzf paramzzf)
  {
    super(paramzzf);
  }
  
  protected void a()
  {
    r().a().a(this.a);
    b();
  }
  
  public void b()
  {
    Object localObject = v();
    String str = ((zzan)localObject).c();
    if (str != null) {
      this.a.a(str);
    }
    localObject = ((zzan)localObject).b();
    if (localObject != null) {
      this.a.b((String)localObject);
    }
  }
  
  public zzpq c()
  {
    D();
    return this.a;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/internal/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */