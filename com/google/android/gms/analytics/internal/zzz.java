package com.google.android.gms.analytics.internal;

public class zzz
  extends zzq<zzaa>
{
  public zzz(zzf paramzzf)
  {
    super(paramzzf, new zza(paramzzf));
  }
  
  private static class zza
    implements zzq.zza<zzaa>
  {
    private final zzf a;
    private final zzaa b;
    
    public zza(zzf paramzzf)
    {
      this.a = paramzzf;
      this.b = new zzaa();
    }
    
    public zzaa a()
    {
      return this.b;
    }
    
    public void a(String paramString, int paramInt)
    {
      if ("ga_dispatchPeriod".equals(paramString))
      {
        this.b.d = paramInt;
        return;
      }
      this.a.f().d("Int xml configuration name not recognized", paramString);
    }
    
    public void a(String paramString1, String paramString2) {}
    
    public void a(String paramString, boolean paramBoolean)
    {
      if ("ga_dryRun".equals(paramString))
      {
        paramString = this.b;
        if (paramBoolean) {}
        for (int i = 1;; i = 0)
        {
          paramString.e = i;
          return;
        }
      }
      this.a.f().d("Bool xml configuration name not recognized", paramString);
    }
    
    public void b(String paramString1, String paramString2)
    {
      if ("ga_appName".equals(paramString1))
      {
        this.b.a = paramString2;
        return;
      }
      if ("ga_appVersion".equals(paramString1))
      {
        this.b.b = paramString2;
        return;
      }
      if ("ga_logLevel".equals(paramString1))
      {
        this.b.c = paramString2;
        return;
      }
      this.a.f().d("String xml configuration name not recognized", paramString1);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/internal/zzz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */