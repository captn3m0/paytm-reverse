package com.google.android.gms.analytics.internal;

import com.google.android.gms.internal.zzps;
import com.google.android.gms.measurement.zzg;

public class zzu
  extends zzd
{
  zzu(zzf paramzzf)
  {
    super(paramzzf);
  }
  
  protected void a() {}
  
  public zzps b()
  {
    D();
    return r().b();
  }
  
  public String c()
  {
    D();
    zzps localzzps = b();
    return localzzps.b() + "x" + localzzps.c();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/internal/zzu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */