package com.google.android.gms.analytics.internal;

public class zzaa
  implements zzp
{
  public String a;
  public String b;
  public String c;
  public int d = -1;
  public int e = -1;
  
  public boolean a()
  {
    return this.a != null;
  }
  
  public String b()
  {
    return this.a;
  }
  
  public boolean c()
  {
    return this.b != null;
  }
  
  public String d()
  {
    return this.b;
  }
  
  public boolean e()
  {
    return this.c != null;
  }
  
  public String f()
  {
    return this.c;
  }
  
  public boolean g()
  {
    return this.d >= 0;
  }
  
  public int h()
  {
    return this.d;
  }
  
  public boolean i()
  {
    return this.e != -1;
  }
  
  public boolean j()
  {
    return this.e == 1;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/internal/zzaa.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */