package com.google.android.gms.analytics.internal;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;

public class zzan
  extends zzd
{
  protected String a;
  protected String b;
  protected boolean c;
  protected int d;
  protected boolean e;
  protected int f;
  protected boolean g;
  protected boolean h;
  
  public zzan(zzf paramzzf)
  {
    super(paramzzf);
  }
  
  private static int a(String paramString)
  {
    paramString = paramString.toLowerCase();
    if ("verbose".equals(paramString)) {
      return 0;
    }
    if ("info".equals(paramString)) {
      return 1;
    }
    if ("warning".equals(paramString)) {
      return 2;
    }
    if ("error".equals(paramString)) {
      return 3;
    }
    return -1;
  }
  
  protected void a()
  {
    j();
  }
  
  void a(zzaa paramzzaa)
  {
    b("Loading global XML config values");
    String str;
    if (paramzzaa.a())
    {
      str = paramzzaa.b();
      this.b = str;
      b("XML config - app name", str);
    }
    if (paramzzaa.c())
    {
      str = paramzzaa.d();
      this.a = str;
      b("XML config - app version", str);
    }
    int i;
    if (paramzzaa.e())
    {
      i = a(paramzzaa.f());
      if (i >= 0)
      {
        this.d = i;
        a("XML config - log level", Integer.valueOf(i));
      }
    }
    if (paramzzaa.g())
    {
      i = paramzzaa.h();
      this.f = i;
      this.e = true;
      b("XML config - dispatch period (sec)", Integer.valueOf(i));
    }
    if (paramzzaa.i())
    {
      boolean bool = paramzzaa.j();
      this.h = bool;
      this.g = true;
      b("XML config - dry run", Boolean.valueOf(bool));
    }
  }
  
  public String b()
  {
    D();
    return this.a;
  }
  
  public String c()
  {
    D();
    return this.b;
  }
  
  public boolean d()
  {
    D();
    return this.c;
  }
  
  public int e()
  {
    D();
    return this.d;
  }
  
  public boolean f()
  {
    D();
    return this.e;
  }
  
  public int g()
  {
    D();
    return this.f;
  }
  
  public boolean h()
  {
    D();
    return this.g;
  }
  
  public boolean i()
  {
    D();
    return this.h;
  }
  
  protected void j()
  {
    Object localObject1 = o();
    try
    {
      localObject1 = ((Context)localObject1).getPackageManager().getApplicationInfo(((Context)localObject1).getPackageName(), 129);
      if (localObject1 == null)
      {
        e("Couldn't get ApplicationInfo to load global config");
        return;
      }
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      Object localObject2;
      do
      {
        int i;
        do
        {
          do
          {
            for (;;)
            {
              d("PackageManager doesn't know about the app package", localNameNotFoundException);
              localObject2 = null;
            }
            localObject2 = ((ApplicationInfo)localObject2).metaData;
          } while (localObject2 == null);
          i = ((Bundle)localObject2).getInt("com.google.android.gms.analytics.globalConfigResource");
        } while (i <= 0);
        localObject2 = (zzaa)new zzz(k()).a(i);
      } while (localObject2 == null);
      a((zzaa)localObject2);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/internal/zzan.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */