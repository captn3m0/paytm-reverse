package com.google.android.gms.analytics.internal;

import android.content.Context;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzmq;
import com.google.android.gms.internal.zzmt;

public class zzg
{
  private final Context a;
  private final Context b;
  
  public zzg(Context paramContext)
  {
    zzx.a(paramContext);
    paramContext = paramContext.getApplicationContext();
    zzx.a(paramContext, "Application context can't be null");
    this.a = paramContext;
    this.b = paramContext;
  }
  
  public Context a()
  {
    return this.a;
  }
  
  protected zzu a(zzf paramzzf)
  {
    return new zzu(paramzzf);
  }
  
  protected com.google.android.gms.measurement.zzg a(Context paramContext)
  {
    return com.google.android.gms.measurement.zzg.a(paramContext);
  }
  
  public Context b()
  {
    return this.b;
  }
  
  protected zzk b(zzf paramzzf)
  {
    return new zzk(paramzzf);
  }
  
  protected zza c(zzf paramzzf)
  {
    return new zza(paramzzf);
  }
  
  protected zzn d(zzf paramzzf)
  {
    return new zzn(paramzzf);
  }
  
  protected zzan e(zzf paramzzf)
  {
    return new zzan(paramzzf);
  }
  
  protected zzaf f(zzf paramzzf)
  {
    return new zzaf(paramzzf);
  }
  
  protected zzr g(zzf paramzzf)
  {
    return new zzr(paramzzf);
  }
  
  protected zzmq h(zzf paramzzf)
  {
    return zzmt.d();
  }
  
  protected GoogleAnalytics i(zzf paramzzf)
  {
    return new GoogleAnalytics(paramzzf);
  }
  
  zzl j(zzf paramzzf)
  {
    return new zzl(paramzzf, this);
  }
  
  zzag k(zzf paramzzf)
  {
    return new zzag(paramzzf);
  }
  
  protected zzb l(zzf paramzzf)
  {
    return new zzb(paramzzf, this);
  }
  
  public zzj m(zzf paramzzf)
  {
    return new zzj(paramzzf);
  }
  
  public zzah n(zzf paramzzf)
  {
    return new zzah(paramzzf);
  }
  
  public zzi o(zzf paramzzf)
  {
    return new zzi(paramzzf);
  }
  
  public zzv p(zzf paramzzf)
  {
    return new zzv(paramzzf);
  }
  
  public zzai q(zzf paramzzf)
  {
    return new zzai(paramzzf);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/internal/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */