package com.google.android.gms.analytics.internal;

import com.google.android.gms.internal.zzmq;

public class zzad
{
  private final long a;
  private final int b;
  private double c;
  private long d;
  private final Object e = new Object();
  private final String f;
  private final zzmq g;
  
  public zzad(int paramInt, long paramLong, String paramString, zzmq paramzzmq)
  {
    this.b = paramInt;
    this.c = this.b;
    this.a = paramLong;
    this.f = paramString;
    this.g = paramzzmq;
  }
  
  public zzad(String paramString, zzmq paramzzmq)
  {
    this(60, 2000L, paramString, paramzzmq);
  }
  
  public boolean a()
  {
    synchronized (this.e)
    {
      long l = this.g.a();
      if (this.c < this.b)
      {
        double d1 = (l - this.d) / this.a;
        if (d1 > 0.0D) {
          this.c = Math.min(this.b, d1 + this.c);
        }
      }
      this.d = l;
      if (this.c >= 1.0D)
      {
        this.c -= 1.0D;
        return true;
      }
      zzae.b("Excessive " + this.f + " detected; call ignored.");
      return false;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/internal/zzad.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */