package com.google.android.gms.analytics.internal;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import com.google.android.gms.analytics.AnalyticsReceiver;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzmq;

public class zzv
  extends zzd
{
  private boolean a;
  private boolean b;
  private AlarmManager c = (AlarmManager)o().getSystemService("alarm");
  
  protected zzv(zzf paramzzf)
  {
    super(paramzzf);
  }
  
  private PendingIntent e()
  {
    Intent localIntent = new Intent(o(), AnalyticsReceiver.class);
    localIntent.setAction("com.google.android.gms.analytics.ANALYTICS_DISPATCH");
    return PendingIntent.getBroadcast(o(), 0, localIntent, 0);
  }
  
  protected void a()
  {
    try
    {
      this.c.cancel(e());
      if (q().j() > 0L)
      {
        ActivityInfo localActivityInfo = o().getPackageManager().getReceiverInfo(new ComponentName(o(), AnalyticsReceiver.class), 2);
        if ((localActivityInfo != null) && (localActivityInfo.enabled))
        {
          b("Receiver registered. Using alarm for local dispatch.");
          this.a = true;
        }
      }
      return;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
  }
  
  public boolean b()
  {
    return this.a;
  }
  
  public boolean c()
  {
    return this.b;
  }
  
  public void cancel()
  {
    D();
    this.b = false;
    this.c.cancel(e());
  }
  
  public void d()
  {
    D();
    zzx.a(b(), "Receiver not registered");
    long l1 = q().j();
    if (l1 > 0L)
    {
      cancel();
      long l2 = n().b();
      this.b = true;
      this.c.setInexactRepeating(2, l2 + l1, 0L, e());
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/internal/zzv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */