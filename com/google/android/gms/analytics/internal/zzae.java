package com.google.android.gms.analytics.internal;

import android.util.Log;
import com.google.android.gms.analytics.Logger;

@Deprecated
public class zzae
{
  private static volatile Logger a;
  
  static
  {
    a(new zzs());
  }
  
  public static Logger a()
  {
    return a;
  }
  
  public static void a(Logger paramLogger)
  {
    a = paramLogger;
  }
  
  public static void a(String paramString)
  {
    Object localObject = zzaf.b();
    if (localObject != null) {
      ((zzaf)localObject).b(paramString);
    }
    for (;;)
    {
      localObject = a;
      if (localObject != null) {
        ((Logger)localObject).verbose(paramString);
      }
      return;
      if (a(0)) {
        Log.v((String)zzy.c.a(), paramString);
      }
    }
  }
  
  public static void a(String paramString, Object paramObject)
  {
    zzaf localzzaf = zzaf.b();
    if (localzzaf != null) {
      localzzaf.e(paramString, paramObject);
    }
    while (!a(3))
    {
      paramObject = a;
      if (paramObject != null) {
        ((Logger)paramObject).error(paramString);
      }
      return;
    }
    if (paramObject != null) {}
    for (paramObject = paramString + ":" + paramObject;; paramObject = paramString)
    {
      Log.e((String)zzy.c.a(), (String)paramObject);
      break;
    }
  }
  
  public static boolean a(int paramInt)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (a() != null)
    {
      bool1 = bool2;
      if (a().getLogLevel() <= paramInt) {
        bool1 = true;
      }
    }
    return bool1;
  }
  
  public static void b(String paramString)
  {
    Object localObject = zzaf.b();
    if (localObject != null) {
      ((zzaf)localObject).e(paramString);
    }
    for (;;)
    {
      localObject = a;
      if (localObject != null) {
        ((Logger)localObject).warn(paramString);
      }
      return;
      if (a(2)) {
        Log.w((String)zzy.c.a(), paramString);
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/internal/zzae.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */