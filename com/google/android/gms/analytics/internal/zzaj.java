package com.google.android.gms.analytics.internal;

import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzmq;

class zzaj
{
  private final zzmq a;
  private long b;
  
  public zzaj(zzmq paramzzmq)
  {
    zzx.a(paramzzmq);
    this.a = paramzzmq;
  }
  
  public zzaj(zzmq paramzzmq, long paramLong)
  {
    zzx.a(paramzzmq);
    this.a = paramzzmq;
    this.b = paramLong;
  }
  
  public void a()
  {
    this.b = this.a.b();
  }
  
  public boolean a(long paramLong)
  {
    if (this.b == 0L) {}
    while (this.a.b() - this.b > paramLong) {
      return true;
    }
    return false;
  }
  
  public void b()
  {
    this.b = 0L;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/internal/zzaj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */