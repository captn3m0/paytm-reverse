package com.google.android.gms.analytics.internal;

import android.app.Application;
import android.content.Context;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzmq;
import com.google.android.gms.internal.zzmt;

public class zzf
{
  private static zzf a;
  private final Context b;
  private final Context c;
  private final zzmq d;
  private final zzr e;
  private final zzaf f;
  private final com.google.android.gms.measurement.zzg g;
  private final zzb h;
  private final zzv i;
  private final zzan j;
  private final zzai k;
  private final GoogleAnalytics l;
  private final zzn m;
  private final zza n;
  private final zzk o;
  private final zzu p;
  
  protected zzf(zzg paramzzg)
  {
    Object localObject1 = paramzzg.a();
    zzx.a(localObject1, "Application context can't be null");
    zzx.b(localObject1 instanceof Application, "getApplicationContext didn't return the application");
    Object localObject2 = paramzzg.b();
    zzx.a(localObject2);
    this.b = ((Context)localObject1);
    this.c = ((Context)localObject2);
    this.d = paramzzg.h(this);
    this.e = paramzzg.g(this);
    localObject2 = paramzzg.f(this);
    ((zzaf)localObject2).E();
    this.f = ((zzaf)localObject2);
    if (e().a()) {
      f().d("Google Analytics " + zze.a + " is starting up.");
    }
    for (;;)
    {
      localObject2 = paramzzg.q(this);
      ((zzai)localObject2).E();
      this.k = ((zzai)localObject2);
      localObject2 = paramzzg.e(this);
      ((zzan)localObject2).E();
      this.j = ((zzan)localObject2);
      localObject2 = paramzzg.l(this);
      zzn localzzn = paramzzg.d(this);
      zza localzza = paramzzg.c(this);
      zzk localzzk = paramzzg.b(this);
      zzu localzzu = paramzzg.a(this);
      localObject1 = paramzzg.a((Context)localObject1);
      ((com.google.android.gms.measurement.zzg)localObject1).a(a());
      this.g = ((com.google.android.gms.measurement.zzg)localObject1);
      localObject1 = paramzzg.i(this);
      localzzn.E();
      this.m = localzzn;
      localzza.E();
      this.n = localzza;
      localzzk.E();
      this.o = localzzk;
      localzzu.E();
      this.p = localzzu;
      paramzzg = paramzzg.p(this);
      paramzzg.E();
      this.i = paramzzg;
      ((zzb)localObject2).E();
      this.h = ((zzb)localObject2);
      if (e().a()) {
        f().b("Device AnalyticsService version", zze.a);
      }
      ((GoogleAnalytics)localObject1).a();
      this.l = ((GoogleAnalytics)localObject1);
      ((zzb)localObject2).b();
      return;
      f().d("Google Analytics " + zze.a + " is starting up. " + "To enable debug logging on a device run:\n" + "  adb shell setprop log.tag.GAv4 DEBUG\n" + "  adb logcat -s GAv4");
    }
  }
  
  public static zzf a(Context paramContext)
  {
    zzx.a(paramContext);
    if (a == null) {}
    try
    {
      if (a == null)
      {
        zzmq localzzmq = zzmt.d();
        long l1 = localzzmq.b();
        paramContext = new zzf(new zzg(paramContext.getApplicationContext()));
        a = paramContext;
        GoogleAnalytics.d();
        l1 = localzzmq.b() - l1;
        long l2 = ((Long)zzy.Q.a()).longValue();
        if (l1 > l2) {
          paramContext.f().c("Slow initialization (ms)", Long.valueOf(l1), Long.valueOf(l2));
        }
      }
      return a;
    }
    finally {}
  }
  
  private void a(zzd paramzzd)
  {
    zzx.a(paramzzd, "Analytics service not created/initialized");
    zzx.b(paramzzd.C(), "Analytics service not initialized");
  }
  
  protected Thread.UncaughtExceptionHandler a()
  {
    new Thread.UncaughtExceptionHandler()
    {
      public void uncaughtException(Thread paramAnonymousThread, Throwable paramAnonymousThrowable)
      {
        paramAnonymousThread = zzf.this.g();
        if (paramAnonymousThread != null) {
          paramAnonymousThread.e("Job execution failed", paramAnonymousThrowable);
        }
      }
    };
  }
  
  public Context b()
  {
    return this.b;
  }
  
  public Context c()
  {
    return this.c;
  }
  
  public zzmq d()
  {
    return this.d;
  }
  
  public zzr e()
  {
    return this.e;
  }
  
  public zzaf f()
  {
    a(this.f);
    return this.f;
  }
  
  public zzaf g()
  {
    return this.f;
  }
  
  public com.google.android.gms.measurement.zzg h()
  {
    zzx.a(this.g);
    return this.g;
  }
  
  public zzb i()
  {
    a(this.h);
    return this.h;
  }
  
  public zzv j()
  {
    a(this.i);
    return this.i;
  }
  
  public GoogleAnalytics k()
  {
    zzx.a(this.l);
    zzx.b(this.l.c(), "Analytics instance not initialized");
    return this.l;
  }
  
  public zzan l()
  {
    a(this.j);
    return this.j;
  }
  
  public zzai m()
  {
    a(this.k);
    return this.k;
  }
  
  public zzai n()
  {
    if ((this.k == null) || (!this.k.C())) {
      return null;
    }
    return this.k;
  }
  
  public zza o()
  {
    a(this.n);
    return this.n;
  }
  
  public zzn p()
  {
    a(this.m);
    return this.m;
  }
  
  public zzk q()
  {
    a(this.o);
    return this.o;
  }
  
  public zzu r()
  {
    return this.p;
  }
  
  public void s() {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/internal/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */