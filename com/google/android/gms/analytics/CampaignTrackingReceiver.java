package com.google.android.gms.analytics;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.RequiresPermission;
import android.text.TextUtils;
import com.google.android.gms.analytics.internal.zzaf;
import com.google.android.gms.analytics.internal.zzam;
import com.google.android.gms.analytics.internal.zzf;
import com.google.android.gms.analytics.internal.zzr;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzrp;

public class CampaignTrackingReceiver
  extends BroadcastReceiver
{
  static zzrp zzOM;
  static Boolean zzON;
  static Object zzqy = new Object();
  
  public static boolean zzY(Context paramContext)
  {
    zzx.a(paramContext);
    if (zzON != null) {
      return zzON.booleanValue();
    }
    boolean bool = zzam.a(paramContext, CampaignTrackingReceiver.class, true);
    zzON = Boolean.valueOf(bool);
    return bool;
  }
  
  @RequiresPermission
  public void onReceive(Context paramContext, Intent arg2)
  {
    Object localObject = zzf.a(paramContext);
    localzzaf = ((zzf)localObject).f();
    String str = ???.getStringExtra("referrer");
    ??? = ???.getAction();
    localzzaf.a("CampaignTrackingReceiver received", ???);
    if ((!"com.android.vending.INSTALL_REFERRER".equals(???)) || (TextUtils.isEmpty(str)))
    {
      localzzaf.e("CampaignTrackingReceiver received unexpected intent without referrer extra");
      return;
    }
    boolean bool = CampaignTrackingService.zzZ(paramContext);
    if (!bool) {
      localzzaf.e("CampaignTrackingService not registered or disabled. Installation tracking not possible. See http://goo.gl/8Rd3yj for instructions.");
    }
    zzaV(str);
    if (((zzf)localObject).e().a())
    {
      localzzaf.f("Received unexpected installation campaign on package side");
      return;
    }
    ??? = zziB();
    zzx.a(???);
    localObject = new Intent(paramContext, ???);
    ((Intent)localObject).putExtra("referrer", str);
    synchronized (zzqy)
    {
      paramContext.startService((Intent)localObject);
      if (!bool) {
        return;
      }
    }
    try
    {
      if (zzOM == null)
      {
        zzOM = new zzrp(paramContext, 1, "Analytics campaign WakeLock");
        zzOM.a(false);
      }
      zzOM.a(1000L);
    }
    catch (SecurityException paramContext)
    {
      for (;;)
      {
        localzzaf.e("CampaignTrackingService service at risk of not starting. For more reliable installation campaign reports, add the WAKE_LOCK permission to your manifest. See http://goo.gl/8Rd3yj for instructions.");
      }
    }
  }
  
  protected void zzaV(String paramString) {}
  
  protected Class<? extends CampaignTrackingService> zziB()
  {
    return CampaignTrackingService.class;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/CampaignTrackingReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */