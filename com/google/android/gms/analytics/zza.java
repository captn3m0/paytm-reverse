package com.google.android.gms.analytics;

import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.analytics.internal.zzk;
import com.google.android.gms.analytics.internal.zzn;
import com.google.android.gms.analytics.internal.zzu;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzke;
import com.google.android.gms.measurement.zzc;
import com.google.android.gms.measurement.zzi;
import java.util.List;
import java.util.ListIterator;

public class zza
  extends com.google.android.gms.measurement.zzf<zza>
{
  private final com.google.android.gms.analytics.internal.zzf b;
  private boolean c;
  
  public zza(com.google.android.gms.analytics.internal.zzf paramzzf)
  {
    super(paramzzf.h(), paramzzf.d());
    this.b = paramzzf;
  }
  
  protected void a(zzc paramzzc)
  {
    paramzzc = (zzke)paramzzc.b(zzke.class);
    if (TextUtils.isEmpty(paramzzc.b())) {
      paramzzc.b(this.b.p().b());
    }
    if ((this.c) && (TextUtils.isEmpty(paramzzc.d())))
    {
      com.google.android.gms.analytics.internal.zza localzza = this.b.o();
      paramzzc.d(localzza.c());
      paramzzc.a(localzza.b());
    }
  }
  
  public void b(String paramString)
  {
    zzx.a(paramString);
    c(paramString);
    n().add(new zzb(this.b, paramString));
  }
  
  public void b(boolean paramBoolean)
  {
    this.c = paramBoolean;
  }
  
  public void c(String paramString)
  {
    paramString = zzb.a(paramString);
    ListIterator localListIterator = n().listIterator();
    while (localListIterator.hasNext()) {
      if (paramString.equals(((zzi)localListIterator.next()).a())) {
        localListIterator.remove();
      }
    }
  }
  
  com.google.android.gms.analytics.internal.zzf k()
  {
    return this.b;
  }
  
  public zzc l()
  {
    zzc localzzc = m().a();
    localzzc.a(this.b.q().c());
    localzzc.a(this.b.r().b());
    b(localzzc);
    return localzzc;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */