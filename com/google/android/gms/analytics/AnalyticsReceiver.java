package com.google.android.gms.analytics;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.RequiresPermission;
import com.google.android.gms.analytics.internal.zzaf;
import com.google.android.gms.analytics.internal.zzam;
import com.google.android.gms.analytics.internal.zzf;
import com.google.android.gms.analytics.internal.zzr;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzrp;

public final class AnalyticsReceiver
  extends BroadcastReceiver
{
  static Object a = new Object();
  static zzrp b;
  static Boolean c;
  
  public static boolean a(Context paramContext)
  {
    zzx.a(paramContext);
    if (c != null) {
      return c.booleanValue();
    }
    boolean bool = zzam.a(paramContext, AnalyticsReceiver.class, false);
    c = Boolean.valueOf(bool);
    return bool;
  }
  
  @RequiresPermission
  public void onReceive(Context paramContext, Intent arg2)
  {
    Object localObject = zzf.a(paramContext);
    localzzaf = ((zzf)localObject).f();
    ??? = ???.getAction();
    if (((zzf)localObject).e().a()) {
      localzzaf.a("Device AnalyticsReceiver got", ???);
    }
    for (;;)
    {
      boolean bool;
      if ("com.google.android.gms.analytics.ANALYTICS_DISPATCH".equals(???))
      {
        bool = AnalyticsService.a(paramContext);
        localObject = new Intent(paramContext, AnalyticsService.class);
        ((Intent)localObject).setAction("com.google.android.gms.analytics.ANALYTICS_DISPATCH");
      }
      synchronized (a)
      {
        paramContext.startService((Intent)localObject);
        if (!bool)
        {
          return;
          localzzaf.a("Local AnalyticsReceiver got", ???);
          continue;
        }
        try
        {
          if (b == null)
          {
            b = new zzrp(paramContext, 1, "Analytics WakeLock");
            b.a(false);
          }
          b.a(1000L);
        }
        catch (SecurityException paramContext)
        {
          for (;;)
          {
            localzzaf.e("Analytics service at risk of not starting. For more reliable analytics, add the WAKE_LOCK permission to your manifest. See http://goo.gl/8Rd3yj for instructions.");
          }
        }
        return;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/AnalyticsReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */