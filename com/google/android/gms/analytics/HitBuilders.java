package com.google.android.gms.analytics;

import android.text.TextUtils;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.google.android.gms.analytics.ecommerce.Promotion;
import com.google.android.gms.analytics.internal.zzae;
import com.google.android.gms.analytics.internal.zzam;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class HitBuilders
{
  @Deprecated
  public static class AppViewBuilder
    extends HitBuilders.HitBuilder<AppViewBuilder>
  {
    public AppViewBuilder()
    {
      a("&t", "screenview");
    }
  }
  
  public static class EventBuilder
    extends HitBuilders.HitBuilder<EventBuilder>
  {
    public EventBuilder()
    {
      a("&t", "event");
    }
  }
  
  public static class ExceptionBuilder
    extends HitBuilders.HitBuilder<ExceptionBuilder>
  {
    public ExceptionBuilder()
    {
      a("&t", "exception");
    }
    
    public ExceptionBuilder a(String paramString)
    {
      a("&exd", paramString);
      return this;
    }
    
    public ExceptionBuilder a(boolean paramBoolean)
    {
      a("&exf", zzam.a(paramBoolean));
      return this;
    }
  }
  
  protected static class HitBuilder<T extends HitBuilder>
  {
    ProductAction a;
    Map<String, List<Product>> b = new HashMap();
    List<Promotion> c = new ArrayList();
    List<Product> d = new ArrayList();
    private Map<String, String> e = new HashMap();
    
    public T a(Product paramProduct)
    {
      if (paramProduct == null)
      {
        zzae.b("product should be non-null");
        return this;
      }
      this.d.add(paramProduct);
      return this;
    }
    
    public T a(Product paramProduct, String paramString)
    {
      if (paramProduct == null)
      {
        zzae.b("product should be non-null");
        return this;
      }
      String str = paramString;
      if (paramString == null) {
        str = "";
      }
      if (!this.b.containsKey(str)) {
        this.b.put(str, new ArrayList());
      }
      ((List)this.b.get(str)).add(paramProduct);
      return this;
    }
    
    public T a(ProductAction paramProductAction)
    {
      this.a = paramProductAction;
      return this;
    }
    
    public T a(Promotion paramPromotion)
    {
      if (paramPromotion == null)
      {
        zzae.b("promotion should be non-null");
        return this;
      }
      this.c.add(paramPromotion);
      return this;
    }
    
    public final T a(String paramString1, String paramString2)
    {
      if (paramString1 != null)
      {
        this.e.put(paramString1, paramString2);
        return this;
      }
      zzae.b(" HitBuilder.set() called with a null paramName.");
      return this;
    }
    
    public final T a(Map<String, String> paramMap)
    {
      if (paramMap == null) {
        return this;
      }
      this.e.putAll(new HashMap(paramMap));
      return this;
    }
    
    public Map<String, String> a()
    {
      HashMap localHashMap = new HashMap(this.e);
      if (this.a != null) {
        localHashMap.putAll(this.a.a());
      }
      Iterator localIterator = this.c.iterator();
      int i = 1;
      while (localIterator.hasNext())
      {
        localHashMap.putAll(((Promotion)localIterator.next()).e(zzc.e(i)));
        i += 1;
      }
      localIterator = this.d.iterator();
      i = 1;
      while (localIterator.hasNext())
      {
        localHashMap.putAll(((Product)localIterator.next()).g(zzc.c(i)));
        i += 1;
      }
      localIterator = this.b.entrySet().iterator();
      i = 1;
      while (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        Object localObject = (List)localEntry.getValue();
        String str = zzc.h(i);
        localObject = ((List)localObject).iterator();
        int j = 1;
        while (((Iterator)localObject).hasNext())
        {
          localHashMap.putAll(((Product)((Iterator)localObject).next()).g(str + zzc.g(j)));
          j += 1;
        }
        if (!TextUtils.isEmpty((CharSequence)localEntry.getKey())) {
          localHashMap.put(str + "nm", localEntry.getKey());
        }
        i += 1;
      }
      return localHashMap;
    }
  }
  
  @Deprecated
  public static class ItemBuilder
    extends HitBuilders.HitBuilder<ItemBuilder>
  {
    public ItemBuilder()
    {
      a("&t", "item");
    }
  }
  
  public static class ScreenViewBuilder
    extends HitBuilders.HitBuilder<ScreenViewBuilder>
  {
    public ScreenViewBuilder()
    {
      a("&t", "screenview");
    }
  }
  
  public static class SocialBuilder
    extends HitBuilders.HitBuilder<SocialBuilder>
  {
    public SocialBuilder()
    {
      a("&t", "social");
    }
  }
  
  public static class TimingBuilder
    extends HitBuilders.HitBuilder<TimingBuilder>
  {
    public TimingBuilder()
    {
      a("&t", "timing");
    }
  }
  
  @Deprecated
  public static class TransactionBuilder
    extends HitBuilders.HitBuilder<TransactionBuilder>
  {
    public TransactionBuilder()
    {
      a("&t", "transaction");
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/HitBuilders.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */