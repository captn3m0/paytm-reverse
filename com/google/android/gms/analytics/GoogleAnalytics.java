package com.google.android.gms.analytics;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.RequiresPermission;
import android.util.Log;
import com.google.android.gms.analytics.internal.zzae;
import com.google.android.gms.analytics.internal.zzan;
import com.google.android.gms.analytics.internal.zzb;
import com.google.android.gms.analytics.internal.zzf;
import com.google.android.gms.analytics.internal.zzn;
import com.google.android.gms.analytics.internal.zzy;
import com.google.android.gms.analytics.internal.zzy.zza;
import com.google.android.gms.common.internal.zzx;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class GoogleAnalytics
  extends zza
{
  private static List<Runnable> b = new ArrayList();
  private boolean c;
  private boolean d;
  private Set<zza> e = new HashSet();
  private boolean f;
  private volatile boolean g;
  private boolean h;
  
  public GoogleAnalytics(zzf paramzzf)
  {
    super(paramzzf);
  }
  
  @RequiresPermission
  public static GoogleAnalytics a(Context paramContext)
  {
    return zzf.a(paramContext).k();
  }
  
  public static void d()
  {
    try
    {
      if (b != null)
      {
        Iterator localIterator = b.iterator();
        while (localIterator.hasNext()) {
          ((Runnable)localIterator.next()).run();
        }
        b = null;
      }
    }
    finally {}
  }
  
  private zzb p()
  {
    return k().i();
  }
  
  private zzan q()
  {
    return k().l();
  }
  
  public Tracker a(String paramString)
  {
    try
    {
      paramString = new Tracker(k(), paramString, null);
      paramString.E();
      return paramString;
    }
    finally {}
  }
  
  public void a()
  {
    b();
    this.c = true;
  }
  
  void a(Activity paramActivity)
  {
    Iterator localIterator = this.e.iterator();
    while (localIterator.hasNext()) {
      ((zza)localIterator.next()).a(paramActivity);
    }
  }
  
  @Deprecated
  public void a(Logger paramLogger)
  {
    zzae.a(paramLogger);
    if (!this.h)
    {
      Log.i((String)zzy.c.a(), "GoogleAnalytics.setLogger() is deprecated. To enable debug logging, please run:\nadb shell setprop log.tag." + (String)zzy.c.a() + " DEBUG");
      this.h = true;
    }
  }
  
  public void a(boolean paramBoolean)
  {
    this.f = paramBoolean;
  }
  
  void b()
  {
    zzan localzzan = q();
    if (localzzan.d()) {
      g().setLogLevel(localzzan.e());
    }
    if (localzzan.h()) {
      a(localzzan.i());
    }
    if (localzzan.d())
    {
      Logger localLogger = zzae.a();
      if (localLogger != null) {
        localLogger.setLogLevel(localzzan.e());
      }
    }
  }
  
  void b(Activity paramActivity)
  {
    Iterator localIterator = this.e.iterator();
    while (localIterator.hasNext()) {
      ((zza)localIterator.next()).b(paramActivity);
    }
  }
  
  public boolean c()
  {
    return (this.c) && (!this.d);
  }
  
  public boolean e()
  {
    return this.f;
  }
  
  public boolean f()
  {
    return this.g;
  }
  
  @Deprecated
  public Logger g()
  {
    return zzae.a();
  }
  
  public String h()
  {
    zzx.c("getClientId can not be called from the main thread");
    return k().p().b();
  }
  
  public void i()
  {
    p().c();
  }
  
  void j()
  {
    p().d();
  }
  
  static abstract interface zza
  {
    public abstract void a(Activity paramActivity);
    
    public abstract void b(Activity paramActivity);
  }
  
  @TargetApi(14)
  class zzb
    implements Application.ActivityLifecycleCallbacks
  {
    public void onActivityCreated(Activity paramActivity, Bundle paramBundle) {}
    
    public void onActivityDestroyed(Activity paramActivity) {}
    
    public void onActivityPaused(Activity paramActivity) {}
    
    public void onActivityResumed(Activity paramActivity) {}
    
    public void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {}
    
    public void onActivityStarted(Activity paramActivity)
    {
      this.a.a(paramActivity);
    }
    
    public void onActivityStopped(Activity paramActivity)
    {
      this.a.b(paramActivity);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/analytics/GoogleAnalytics.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */