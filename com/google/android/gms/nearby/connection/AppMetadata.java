package com.google.android.gms.nearby.connection;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;
import java.util.List;

public final class AppMetadata
  implements SafeParcelable
{
  public static final Parcelable.Creator<AppMetadata> CREATOR = new zzb();
  private final int a;
  private final List<AppIdentifier> b;
  
  AppMetadata(int paramInt, List<AppIdentifier> paramList)
  {
    this.a = paramInt;
    this.b = ((List)zzx.a(paramList, "Must specify application identifiers"));
    zzx.a(paramList.size(), "Application identifiers cannot be empty");
  }
  
  public int a()
  {
    return this.a;
  }
  
  public List<AppIdentifier> b()
  {
    return this.b;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/connection/AppMetadata.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */