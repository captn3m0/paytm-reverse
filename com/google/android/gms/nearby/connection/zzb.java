package com.google.android.gms.nearby.connection;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import java.util.ArrayList;

public class zzb
  implements Parcelable.Creator<AppMetadata>
{
  static void a(AppMetadata paramAppMetadata, Parcel paramParcel, int paramInt)
  {
    paramInt = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.c(paramParcel, 1, paramAppMetadata.b(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1000, paramAppMetadata.a());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, paramInt);
  }
  
  public AppMetadata a(Parcel paramParcel)
  {
    int j = zza.b(paramParcel);
    int i = 0;
    ArrayList localArrayList = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localArrayList = zza.c(paramParcel, k, AppIdentifier.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new AppMetadata(i, localArrayList);
  }
  
  public AppMetadata[] a(int paramInt)
  {
    return new AppMetadata[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/connection/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */