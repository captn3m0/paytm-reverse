package com.google.android.gms.nearby.connection;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzmr;
import java.util.List;

public abstract interface Connections
{
  public static final List<Integer> c = zzmr.a(Integer.valueOf(1), Integer.valueOf(2));
  
  public static abstract interface ConnectionRequestListener
  {
    public abstract void a(String paramString1, String paramString2, String paramString3, byte[] paramArrayOfByte);
  }
  
  public static abstract interface ConnectionResponseCallback
  {
    public abstract void a(String paramString, Status paramStatus, byte[] paramArrayOfByte);
  }
  
  public static abstract interface EndpointDiscoveryListener
  {
    public abstract void a(String paramString);
    
    public abstract void a(String paramString1, String paramString2, String paramString3, String paramString4);
  }
  
  public static abstract interface MessageListener
  {
    public abstract void a(String paramString);
    
    public abstract void a(String paramString, byte[] paramArrayOfByte, boolean paramBoolean);
  }
  
  public static abstract interface StartAdvertisingResult
    extends Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/connection/Connections.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */