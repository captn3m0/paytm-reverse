package com.google.android.gms.nearby.sharing;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zza
  implements Parcelable.Creator<AppContentReceivedResult>
{
  static void a(AppContentReceivedResult paramAppContentReceivedResult, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramAppContentReceivedResult.a());
    zzb.a(paramParcel, 2, paramAppContentReceivedResult.b(), paramInt, false);
    zzb.a(paramParcel, 3, paramAppContentReceivedResult.c());
    zzb.a(paramParcel, i);
  }
  
  public AppContentReceivedResult a(Parcel paramParcel)
  {
    int i = 0;
    int k = com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel);
    Uri localUri = null;
    int j = 0;
    if (paramParcel.dataPosition() < k)
    {
      int m = com.google.android.gms.common.internal.safeparcel.zza.a(paramParcel);
      switch (com.google.android.gms.common.internal.safeparcel.zza.a(m))
      {
      default: 
        com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel, m);
      }
      for (;;)
      {
        break;
        j = com.google.android.gms.common.internal.safeparcel.zza.g(paramParcel, m);
        continue;
        localUri = (Uri)com.google.android.gms.common.internal.safeparcel.zza.a(paramParcel, m, Uri.CREATOR);
        continue;
        i = com.google.android.gms.common.internal.safeparcel.zza.g(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new AppContentReceivedResult(j, localUri, i);
  }
  
  public AppContentReceivedResult[] a(int paramInt)
  {
    return new AppContentReceivedResult[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/sharing/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */