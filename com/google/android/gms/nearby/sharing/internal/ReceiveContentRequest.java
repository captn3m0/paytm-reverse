package com.google.android.gms.nearby.sharing.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class ReceiveContentRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<ReceiveContentRequest> CREATOR = new zzg();
  final int a;
  public IBinder b;
  public zza c;
  public String d;
  public zzc e;
  
  ReceiveContentRequest()
  {
    this.a = 1;
  }
  
  ReceiveContentRequest(int paramInt, IBinder paramIBinder1, IBinder paramIBinder2, String paramString, IBinder paramIBinder3)
  {
    this.a = paramInt;
    this.b = paramIBinder1;
    this.c = zza.zza.a(paramIBinder2);
    this.d = paramString;
    this.e = zzc.zza.a(paramIBinder3);
  }
  
  IBinder a()
  {
    if (this.c == null) {
      return null;
    }
    return this.c.asBinder();
  }
  
  IBinder b()
  {
    return this.e.asBinder();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzg.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/sharing/internal/ReceiveContentRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */