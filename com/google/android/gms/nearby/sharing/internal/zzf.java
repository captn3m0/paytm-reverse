package com.google.android.gms.nearby.sharing.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.nearby.sharing.SharedContent;
import java.util.ArrayList;

public class zzf
  implements Parcelable.Creator<ProvideContentRequest>
{
  static void a(ProvideContentRequest paramProvideContentRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramProvideContentRequest.a);
    zzb.a(paramParcel, 2, paramProvideContentRequest.b, false);
    zzb.a(paramParcel, 3, paramProvideContentRequest.a(), false);
    zzb.c(paramParcel, 4, paramProvideContentRequest.d, false);
    zzb.a(paramParcel, 5, paramProvideContentRequest.e);
    zzb.a(paramParcel, 6, paramProvideContentRequest.b(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public ProvideContentRequest a(Parcel paramParcel)
  {
    IBinder localIBinder1 = null;
    int j = zza.b(paramParcel);
    int i = 0;
    long l = 0L;
    ArrayList localArrayList = null;
    IBinder localIBinder2 = null;
    IBinder localIBinder3 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localIBinder3 = zza.q(paramParcel, k);
        break;
      case 3: 
        localIBinder2 = zza.q(paramParcel, k);
        break;
      case 4: 
        localArrayList = zza.c(paramParcel, k, SharedContent.CREATOR);
        break;
      case 5: 
        l = zza.i(paramParcel, k);
        break;
      case 6: 
        localIBinder1 = zza.q(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new ProvideContentRequest(i, localIBinder3, localIBinder2, localArrayList, l, localIBinder1);
  }
  
  public ProvideContentRequest[] a(int paramInt)
  {
    return new ProvideContentRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/sharing/internal/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */