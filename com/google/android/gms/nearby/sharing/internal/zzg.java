package com.google.android.gms.nearby.sharing.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzg
  implements Parcelable.Creator<ReceiveContentRequest>
{
  static void a(ReceiveContentRequest paramReceiveContentRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramReceiveContentRequest.a);
    zzb.a(paramParcel, 2, paramReceiveContentRequest.b, false);
    zzb.a(paramParcel, 3, paramReceiveContentRequest.a(), false);
    zzb.a(paramParcel, 4, paramReceiveContentRequest.d, false);
    zzb.a(paramParcel, 5, paramReceiveContentRequest.b(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public ReceiveContentRequest a(Parcel paramParcel)
  {
    IBinder localIBinder1 = null;
    int j = zza.b(paramParcel);
    int i = 0;
    String str = null;
    IBinder localIBinder2 = null;
    IBinder localIBinder3 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localIBinder3 = zza.q(paramParcel, k);
        break;
      case 3: 
        localIBinder2 = zza.q(paramParcel, k);
        break;
      case 4: 
        str = zza.p(paramParcel, k);
        break;
      case 5: 
        localIBinder1 = zza.q(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new ReceiveContentRequest(i, localIBinder3, localIBinder2, str, localIBinder1);
  }
  
  public ReceiveContentRequest[] a(int paramInt)
  {
    return new ReceiveContentRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/sharing/internal/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */