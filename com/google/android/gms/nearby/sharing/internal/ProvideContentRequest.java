package com.google.android.gms.nearby.sharing.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.nearby.sharing.SharedContent;
import java.util.List;

public final class ProvideContentRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<ProvideContentRequest> CREATOR = new zzf();
  final int a;
  public IBinder b;
  public zzb c;
  @Deprecated
  public List<SharedContent> d;
  public long e;
  public zzc f;
  
  ProvideContentRequest()
  {
    this.a = 1;
  }
  
  ProvideContentRequest(int paramInt, IBinder paramIBinder1, IBinder paramIBinder2, List<SharedContent> paramList, long paramLong, IBinder paramIBinder3)
  {
    this.a = paramInt;
    this.b = paramIBinder1;
    this.c = zzb.zza.a(paramIBinder2);
    this.d = paramList;
    this.e = paramLong;
    this.f = zzc.zza.a(paramIBinder3);
  }
  
  IBinder a()
  {
    if (this.c == null) {
      return null;
    }
    return this.c.asBinder();
  }
  
  IBinder b()
  {
    return this.f.asBinder();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/sharing/internal/ProvideContentRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */