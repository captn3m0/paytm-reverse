package com.google.android.gms.nearby.sharing;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import java.util.Arrays;

public class ViewableItem
  implements SafeParcelable
{
  public static final Parcelable.Creator<ViewableItem> CREATOR = new zzf();
  private final int a;
  private String[] b;
  
  private ViewableItem()
  {
    this.a = 1;
  }
  
  ViewableItem(int paramInt, String[] paramArrayOfString)
  {
    this.a = paramInt;
    this.b = paramArrayOfString;
  }
  
  int a()
  {
    return this.a;
  }
  
  public String[] b()
  {
    return this.b;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {
      return true;
    }
    if (!(paramObject instanceof ViewableItem)) {
      return false;
    }
    paramObject = (ViewableItem)paramObject;
    return zzw.a(this.b, ((ViewableItem)paramObject).b);
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b });
  }
  
  public String toString()
  {
    return "ViewableItem[uris=" + Arrays.toString(this.b) + "]";
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/sharing/ViewableItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */