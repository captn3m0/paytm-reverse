package com.google.android.gms.nearby.sharing;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class zzb
  implements Parcelable.Creator<LocalContent>
{
  static void a(LocalContent paramLocalContent, Parcel paramParcel, int paramInt)
  {
    paramInt = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramLocalContent.a());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramLocalContent.b());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 3, paramLocalContent.c(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, paramInt);
  }
  
  public LocalContent a(Parcel paramParcel)
  {
    int j = 0;
    int k = zza.b(paramParcel);
    String str = null;
    int i = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        i = zza.g(paramParcel, m);
        break;
      case 2: 
        j = zza.g(paramParcel, m);
        break;
      case 3: 
        str = zza.p(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new LocalContent(i, j, str);
  }
  
  public LocalContent[] a(int paramInt)
  {
    return new LocalContent[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/sharing/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */