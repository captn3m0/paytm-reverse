package com.google.android.gms.nearby.sharing;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzc
  implements Parcelable.Creator<SharedContent>
{
  static void a(SharedContent paramSharedContent, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramSharedContent.a());
    zzb.a(paramParcel, 3, paramSharedContent.b(), false);
    zzb.a(paramParcel, 8, paramSharedContent.c(), paramInt, false);
    zzb.a(paramParcel, 9, paramSharedContent.d(), paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public SharedContent a(Parcel paramParcel)
  {
    LocalContent[] arrayOfLocalContent = null;
    int j = zza.b(paramParcel);
    int i = 0;
    Object localObject2 = null;
    Object localObject1 = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      Object localObject3;
      switch (zza.a(k))
      {
      case 2: 
      case 4: 
      case 5: 
      case 6: 
      case 7: 
      default: 
        zza.b(paramParcel, k);
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject3;
      }
      for (;;)
      {
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject3;
        break;
        i = zza.g(paramParcel, k);
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject3;
        continue;
        localObject3 = zza.p(paramParcel, k);
        localObject1 = localObject2;
        localObject2 = localObject3;
        continue;
        localObject3 = (ViewableItem[])zza.b(paramParcel, k, ViewableItem.CREATOR);
        localObject2 = localObject1;
        localObject1 = localObject3;
        continue;
        arrayOfLocalContent = (LocalContent[])zza.b(paramParcel, k, LocalContent.CREATOR);
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject3;
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new SharedContent(i, (String)localObject1, (ViewableItem[])localObject2, arrayOfLocalContent);
  }
  
  public SharedContent[] a(int paramInt)
  {
    return new SharedContent[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/sharing/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */