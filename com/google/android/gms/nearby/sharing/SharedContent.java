package com.google.android.gms.nearby.sharing;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import java.util.Arrays;

public class SharedContent
  implements SafeParcelable
{
  public static final Parcelable.Creator<SharedContent> CREATOR = new zzc();
  private final int a;
  @Deprecated
  private String b;
  private ViewableItem[] c;
  private LocalContent[] d;
  
  private SharedContent()
  {
    this.a = 2;
  }
  
  SharedContent(int paramInt, String paramString, ViewableItem[] paramArrayOfViewableItem, LocalContent[] paramArrayOfLocalContent)
  {
    this.a = paramInt;
    this.b = paramString;
    this.c = paramArrayOfViewableItem;
    this.d = paramArrayOfLocalContent;
  }
  
  int a()
  {
    return this.a;
  }
  
  public String b()
  {
    return this.b;
  }
  
  public ViewableItem[] c()
  {
    return this.c;
  }
  
  public LocalContent[] d()
  {
    return this.d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof SharedContent)) {
        return false;
      }
      paramObject = (SharedContent)paramObject;
    } while ((zzw.a(this.c, ((SharedContent)paramObject).c)) && (zzw.a(this.d, ((SharedContent)paramObject).d)) && (zzw.a(this.b, ((SharedContent)paramObject).b)));
    return false;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.c, this.d, this.b });
  }
  
  public String toString()
  {
    return "SharedContent[viewableItems=" + Arrays.toString(this.c) + ", localContents=" + Arrays.toString(this.d) + "]";
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/sharing/SharedContent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */