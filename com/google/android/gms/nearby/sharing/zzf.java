package com.google.android.gms.nearby.sharing;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzf
  implements Parcelable.Creator<ViewableItem>
{
  static void a(ViewableItem paramViewableItem, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramViewableItem.a());
    zzb.a(paramParcel, 2, paramViewableItem.b(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public ViewableItem a(Parcel paramParcel)
  {
    int j = zza.b(paramParcel);
    int i = 0;
    String[] arrayOfString = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        arrayOfString = zza.B(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new ViewableItem(i, arrayOfString);
  }
  
  public ViewableItem[] a(int paramInt)
  {
    return new ViewableItem[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/sharing/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */