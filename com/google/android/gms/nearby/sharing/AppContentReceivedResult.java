package com.google.android.gms.nearby.sharing;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;

public class AppContentReceivedResult
  implements SafeParcelable
{
  public static final Parcelable.Creator<AppContentReceivedResult> CREATOR = new zza();
  private final int a;
  private Uri b;
  private int c;
  
  private AppContentReceivedResult()
  {
    this.a = 1;
  }
  
  AppContentReceivedResult(int paramInt1, Uri paramUri, int paramInt2)
  {
    this.a = paramInt1;
    this.b = paramUri;
    this.c = paramInt2;
  }
  
  int a()
  {
    return this.a;
  }
  
  public Uri b()
  {
    return this.b;
  }
  
  public int c()
  {
    return this.c;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof AppContentReceivedResult)) {
        return false;
      }
      paramObject = (AppContentReceivedResult)paramObject;
    } while ((zzw.a(this.b, ((AppContentReceivedResult)paramObject).b)) && (zzw.a(Integer.valueOf(this.c), Integer.valueOf(((AppContentReceivedResult)paramObject).c))));
    return false;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b, Integer.valueOf(this.c) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/sharing/AppContentReceivedResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */