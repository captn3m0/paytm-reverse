package com.google.android.gms.nearby;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.internal.zzqd;
import com.google.android.gms.internal.zzql;
import com.google.android.gms.nearby.bootstrap.zza;
import com.google.android.gms.nearby.connection.Connections;
import com.google.android.gms.nearby.messages.Messages;
import com.google.android.gms.nearby.messages.MessagesOptions;
import com.google.android.gms.nearby.messages.internal.zzn;
import com.google.android.gms.nearby.messages.internal.zzo;
import com.google.android.gms.nearby.messages.zzc;
import com.google.android.gms.nearby.sharing.internal.zzh;
import com.google.android.gms.nearby.sharing.internal.zzi;
import com.google.android.gms.nearby.sharing.zzd;
import com.google.android.gms.nearby.sharing.zze;

public final class Nearby
{
  public static final Api<Api.ApiOptions.NoOptions> a = new Api("Nearby.CONNECTIONS_API", zzql.b, zzql.a);
  public static final Connections b = new zzql();
  public static final Api<MessagesOptions> c = new Api("Nearby.MESSAGES_API", zzn.b, zzn.a);
  public static final Messages d = new zzn();
  public static final zzc e = new zzo();
  public static final Api<Api.ApiOptions.NoOptions> f = new Api("Nearby.SHARING_API", zzh.b, zzh.a);
  public static final zzd g = new zzh();
  public static final zze h = new zzi();
  public static final Api<Api.ApiOptions.NoOptions> i = new Api("Nearby.BOOTSTRAP_API", zzqd.b, zzqd.a);
  public static final zza j = new zzqd();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/Nearby.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */