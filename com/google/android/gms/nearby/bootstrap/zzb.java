package com.google.android.gms.nearby.bootstrap;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class zzb
  implements Parcelable.Creator<Device>
{
  static void a(Device paramDevice, Parcel paramParcel, int paramInt)
  {
    paramInt = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramDevice.a(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1000, paramDevice.a);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramDevice.b(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 3, paramDevice.c(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 4, paramDevice.d());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, paramInt);
  }
  
  public Device a(Parcel paramParcel)
  {
    byte b = 0;
    String str1 = null;
    int j = zza.b(paramParcel);
    String str2 = null;
    String str3 = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        str3 = zza.p(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str2 = zza.p(paramParcel, k);
        break;
      case 3: 
        str1 = zza.p(paramParcel, k);
        break;
      case 4: 
        b = zza.e(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new Device(i, str3, str2, str1, b);
  }
  
  public Device[] a(int paramInt)
  {
    return new Device[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/bootstrap/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */