package com.google.android.gms.nearby.bootstrap;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;

public class Device
  implements SafeParcelable
{
  public static final Parcelable.Creator<Device> CREATOR = new zzb();
  final int a;
  private final String b;
  private final String c;
  private final String d;
  private final byte e;
  
  Device(int paramInt, String paramString1, String paramString2, String paramString3, byte paramByte)
  {
    this.a = paramInt;
    this.b = zzx.a(paramString1);
    this.c = ((String)zzx.a(paramString2));
    this.d = ((String)zzx.a(paramString3));
    if (paramByte <= 4) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.b(bool, "Unknown device type");
      this.e = paramByte;
      return;
    }
  }
  
  public String a()
  {
    return this.b;
  }
  
  public String b()
  {
    return this.c;
  }
  
  public String c()
  {
    return this.d;
  }
  
  public byte d()
  {
    return this.e;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String toString()
  {
    return this.b + ": " + this.c + "[" + this.e + "]";
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/bootstrap/Device.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */