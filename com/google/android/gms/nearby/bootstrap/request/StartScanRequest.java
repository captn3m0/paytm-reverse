package com.google.android.gms.nearby.bootstrap.request;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzqg;
import com.google.android.gms.internal.zzqg.zza;
import com.google.android.gms.internal.zzqi;
import com.google.android.gms.internal.zzqi.zza;

public class StartScanRequest
  implements SafeParcelable
{
  public static final zzg CREATOR = new zzg();
  final int a;
  private final zzqi b;
  private final zzqg c;
  
  StartScanRequest(int paramInt, IBinder paramIBinder1, IBinder paramIBinder2)
  {
    this.a = paramInt;
    zzx.a(paramIBinder1);
    this.b = zzqi.zza.a(paramIBinder1);
    zzx.a(paramIBinder2);
    this.c = zzqg.zza.a(paramIBinder2);
  }
  
  public IBinder a()
  {
    if (this.b == null) {
      return null;
    }
    return this.b.asBinder();
  }
  
  public IBinder b()
  {
    if (this.c == null) {
      return null;
    }
    return this.c.asBinder();
  }
  
  public int describeContents()
  {
    zzg localzzg = CREATOR;
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzg localzzg = CREATOR;
    zzg.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/bootstrap/request/StartScanRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */