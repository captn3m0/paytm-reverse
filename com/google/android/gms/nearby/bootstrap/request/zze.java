package com.google.android.gms.nearby.bootstrap.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zze
  implements Parcelable.Creator<EnableTargetRequest>
{
  static void a(EnableTargetRequest paramEnableTargetRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramEnableTargetRequest.a(), false);
    zzb.a(paramParcel, 1000, paramEnableTargetRequest.a);
    zzb.a(paramParcel, 2, paramEnableTargetRequest.b(), false);
    zzb.a(paramParcel, 3, paramEnableTargetRequest.d(), false);
    zzb.a(paramParcel, 4, paramEnableTargetRequest.e(), false);
    zzb.a(paramParcel, 5, paramEnableTargetRequest.f(), false);
    zzb.a(paramParcel, 6, paramEnableTargetRequest.c());
    zzb.a(paramParcel, paramInt);
  }
  
  public EnableTargetRequest a(Parcel paramParcel)
  {
    byte b = 0;
    IBinder localIBinder1 = null;
    int j = zza.b(paramParcel);
    IBinder localIBinder2 = null;
    IBinder localIBinder3 = null;
    String str1 = null;
    String str2 = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        str2 = zza.p(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str1 = zza.p(paramParcel, k);
        break;
      case 3: 
        localIBinder3 = zza.q(paramParcel, k);
        break;
      case 4: 
        localIBinder2 = zza.q(paramParcel, k);
        break;
      case 5: 
        localIBinder1 = zza.q(paramParcel, k);
        break;
      case 6: 
        b = zza.e(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new EnableTargetRequest(i, str2, str1, b, localIBinder3, localIBinder2, localIBinder1);
  }
  
  public EnableTargetRequest[] a(int paramInt)
  {
    return new EnableTargetRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/bootstrap/request/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */