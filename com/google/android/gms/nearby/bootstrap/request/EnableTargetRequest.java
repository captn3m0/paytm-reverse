package com.google.android.gms.nearby.bootstrap.request;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzqe;
import com.google.android.gms.internal.zzqe.zza;
import com.google.android.gms.internal.zzqf;
import com.google.android.gms.internal.zzqf.zza;
import com.google.android.gms.internal.zzqg;
import com.google.android.gms.internal.zzqg.zza;

public class EnableTargetRequest
  implements SafeParcelable
{
  public static final zze CREATOR = new zze();
  final int a;
  private final String b;
  private final String c;
  private final zzqe d;
  private final zzqf e;
  private final zzqg f;
  private final byte g;
  
  EnableTargetRequest(int paramInt, String paramString1, String paramString2, byte paramByte, IBinder paramIBinder1, IBinder paramIBinder2, IBinder paramIBinder3)
  {
    this.a = paramInt;
    this.b = zzx.a(paramString1);
    this.c = ((String)zzx.a(paramString2));
    this.g = paramByte;
    zzx.a(paramIBinder1);
    this.d = zzqe.zza.a(paramIBinder1);
    zzx.a(paramIBinder2);
    this.e = zzqf.zza.a(paramIBinder2);
    zzx.a(paramIBinder3);
    this.f = zzqg.zza.a(paramIBinder3);
  }
  
  public String a()
  {
    return this.b;
  }
  
  public String b()
  {
    return this.c;
  }
  
  public byte c()
  {
    return this.g;
  }
  
  public IBinder d()
  {
    if (this.d == null) {
      return null;
    }
    return this.d.asBinder();
  }
  
  public int describeContents()
  {
    zze localzze = CREATOR;
    return 0;
  }
  
  public IBinder e()
  {
    if (this.e == null) {
      return null;
    }
    return this.e.asBinder();
  }
  
  public IBinder f()
  {
    if (this.f == null) {
      return null;
    }
    return this.f.asBinder();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zze localzze = CREATOR;
    zze.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/bootstrap/request/EnableTargetRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */