package com.google.android.gms.nearby.bootstrap.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.nearby.bootstrap.Device;

public class zza
  implements Parcelable.Creator<ConnectRequest>
{
  static void a(ConnectRequest paramConnectRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramConnectRequest.a(), paramInt, false);
    zzb.a(paramParcel, 1000, paramConnectRequest.a);
    zzb.a(paramParcel, 2, paramConnectRequest.b(), false);
    zzb.a(paramParcel, 3, paramConnectRequest.c(), false);
    zzb.a(paramParcel, 4, paramConnectRequest.h(), false);
    zzb.a(paramParcel, 5, paramConnectRequest.i(), false);
    zzb.a(paramParcel, 6, paramConnectRequest.j(), false);
    zzb.a(paramParcel, 7, paramConnectRequest.d());
    zzb.a(paramParcel, 8, paramConnectRequest.e());
    zzb.a(paramParcel, 9, paramConnectRequest.f(), false);
    zzb.a(paramParcel, 10, paramConnectRequest.g());
    zzb.a(paramParcel, i);
  }
  
  public ConnectRequest a(Parcel paramParcel)
  {
    int j = com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel);
    int i = 0;
    Device localDevice = null;
    String str3 = null;
    String str2 = null;
    byte b2 = 0;
    long l = 0L;
    String str1 = null;
    byte b1 = 0;
    IBinder localIBinder3 = null;
    IBinder localIBinder2 = null;
    IBinder localIBinder1 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = com.google.android.gms.common.internal.safeparcel.zza.a(paramParcel);
      switch (com.google.android.gms.common.internal.safeparcel.zza.a(k))
      {
      default: 
        com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel, k);
        break;
      case 1: 
        localDevice = (Device)com.google.android.gms.common.internal.safeparcel.zza.a(paramParcel, k, Device.CREATOR);
        break;
      case 1000: 
        i = com.google.android.gms.common.internal.safeparcel.zza.g(paramParcel, k);
        break;
      case 2: 
        str3 = com.google.android.gms.common.internal.safeparcel.zza.p(paramParcel, k);
        break;
      case 3: 
        str2 = com.google.android.gms.common.internal.safeparcel.zza.p(paramParcel, k);
        break;
      case 4: 
        localIBinder3 = com.google.android.gms.common.internal.safeparcel.zza.q(paramParcel, k);
        break;
      case 5: 
        localIBinder2 = com.google.android.gms.common.internal.safeparcel.zza.q(paramParcel, k);
        break;
      case 6: 
        localIBinder1 = com.google.android.gms.common.internal.safeparcel.zza.q(paramParcel, k);
        break;
      case 7: 
        b2 = com.google.android.gms.common.internal.safeparcel.zza.e(paramParcel, k);
        break;
      case 8: 
        l = com.google.android.gms.common.internal.safeparcel.zza.i(paramParcel, k);
        break;
      case 9: 
        str1 = com.google.android.gms.common.internal.safeparcel.zza.p(paramParcel, k);
        break;
      case 10: 
        b1 = com.google.android.gms.common.internal.safeparcel.zza.e(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new ConnectRequest(i, localDevice, str3, str2, b2, l, str1, b1, localIBinder3, localIBinder2, localIBinder1);
  }
  
  public ConnectRequest[] a(int paramInt)
  {
    return new ConnectRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/bootstrap/request/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */