package com.google.android.gms.nearby.bootstrap.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.nearby.bootstrap.Device;

public class zzf
  implements Parcelable.Creator<SendDataRequest>
{
  static void a(SendDataRequest paramSendDataRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramSendDataRequest.a(), paramInt, false);
    zzb.a(paramParcel, 1000, paramSendDataRequest.a);
    zzb.a(paramParcel, 2, paramSendDataRequest.b(), false);
    zzb.a(paramParcel, 3, paramSendDataRequest.c(), false);
    zzb.a(paramParcel, i);
  }
  
  public SendDataRequest a(Parcel paramParcel)
  {
    IBinder localIBinder = null;
    int j = zza.b(paramParcel);
    int i = 0;
    Object localObject2 = null;
    Object localObject1 = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      Object localObject3;
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject3;
      }
      for (;;)
      {
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject3;
        break;
        localObject3 = (Device)zza.a(paramParcel, k, Device.CREATOR);
        localObject1 = localObject2;
        localObject2 = localObject3;
        continue;
        i = zza.g(paramParcel, k);
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject3;
        continue;
        localObject3 = zza.s(paramParcel, k);
        localObject2 = localObject1;
        localObject1 = localObject3;
        continue;
        localIBinder = zza.q(paramParcel, k);
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject3;
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new SendDataRequest(i, (Device)localObject1, (byte[])localObject2, localIBinder);
  }
  
  public SendDataRequest[] a(int paramInt)
  {
    return new SendDataRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/bootstrap/request/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */