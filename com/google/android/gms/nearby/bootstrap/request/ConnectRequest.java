package com.google.android.gms.nearby.bootstrap.request;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzqe;
import com.google.android.gms.internal.zzqe.zza;
import com.google.android.gms.internal.zzqf;
import com.google.android.gms.internal.zzqf.zza;
import com.google.android.gms.internal.zzqg;
import com.google.android.gms.internal.zzqg.zza;
import com.google.android.gms.nearby.bootstrap.Device;

public class ConnectRequest
  implements SafeParcelable
{
  public static final zza CREATOR = new zza();
  final int a;
  private final Device b;
  private final String c;
  private final String d;
  private final zzqe e;
  private final zzqf f;
  private final zzqg g;
  private final byte h;
  private final long i;
  private final String j;
  private final byte k;
  
  ConnectRequest(int paramInt, Device paramDevice, String paramString1, String paramString2, byte paramByte1, long paramLong, String paramString3, byte paramByte2, IBinder paramIBinder1, IBinder paramIBinder2, IBinder paramIBinder3)
  {
    this.a = paramInt;
    this.b = ((Device)zzx.a(paramDevice));
    this.c = zzx.a(paramString1);
    this.d = ((String)zzx.a(paramString2));
    this.h = paramByte1;
    this.i = paramLong;
    this.k = paramByte2;
    this.j = paramString3;
    zzx.a(paramIBinder1);
    this.e = zzqe.zza.a(paramIBinder1);
    zzx.a(paramIBinder2);
    this.f = zzqf.zza.a(paramIBinder2);
    zzx.a(paramIBinder3);
    this.g = zzqg.zza.a(paramIBinder3);
  }
  
  public Device a()
  {
    return this.b;
  }
  
  public String b()
  {
    return this.c;
  }
  
  public String c()
  {
    return this.d;
  }
  
  public byte d()
  {
    return this.h;
  }
  
  public int describeContents()
  {
    zza localzza = CREATOR;
    return 0;
  }
  
  public long e()
  {
    return this.i;
  }
  
  public String f()
  {
    return this.j;
  }
  
  public byte g()
  {
    return this.k;
  }
  
  public IBinder h()
  {
    if (this.e == null) {
      return null;
    }
    return this.e.asBinder();
  }
  
  public IBinder i()
  {
    if (this.f == null) {
      return null;
    }
    return this.f.asBinder();
  }
  
  public IBinder j()
  {
    if (this.g == null) {
      return null;
    }
    return this.g.asBinder();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza localzza = CREATOR;
    zza.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/bootstrap/request/ConnectRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */