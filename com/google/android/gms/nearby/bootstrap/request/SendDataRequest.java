package com.google.android.gms.nearby.bootstrap.request;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzqg;
import com.google.android.gms.internal.zzqg.zza;
import com.google.android.gms.nearby.bootstrap.Device;

public class SendDataRequest
  implements SafeParcelable
{
  public static final zzf CREATOR = new zzf();
  final int a;
  private final Device b;
  private final byte[] c;
  private final zzqg d;
  
  SendDataRequest(int paramInt, Device paramDevice, byte[] paramArrayOfByte, IBinder paramIBinder)
  {
    this.a = paramInt;
    this.b = ((Device)zzx.a(paramDevice));
    this.c = ((byte[])zzx.a(paramArrayOfByte));
    zzx.a(paramIBinder);
    this.d = zzqg.zza.a(paramIBinder);
  }
  
  public Device a()
  {
    return this.b;
  }
  
  public byte[] b()
  {
    return this.c;
  }
  
  public IBinder c()
  {
    if (this.d == null) {
      return null;
    }
    return this.d.asBinder();
  }
  
  public int describeContents()
  {
    zzf localzzf = CREATOR;
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf localzzf = CREATOR;
    zzf.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/bootstrap/request/SendDataRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */