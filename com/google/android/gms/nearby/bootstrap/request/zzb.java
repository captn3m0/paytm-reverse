package com.google.android.gms.nearby.bootstrap.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class zzb
  implements Parcelable.Creator<ContinueConnectRequest>
{
  static void a(ContinueConnectRequest paramContinueConnectRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramContinueConnectRequest.a(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1000, paramContinueConnectRequest.a);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramContinueConnectRequest.b(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, paramInt);
  }
  
  public ContinueConnectRequest a(Parcel paramParcel)
  {
    IBinder localIBinder = null;
    int j = zza.b(paramParcel);
    int i = 0;
    String str = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        str = zza.p(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localIBinder = zza.q(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new ContinueConnectRequest(i, str, localIBinder);
  }
  
  public ContinueConnectRequest[] a(int paramInt)
  {
    return new ContinueConnectRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/bootstrap/request/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */