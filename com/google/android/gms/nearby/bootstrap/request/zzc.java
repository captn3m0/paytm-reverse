package com.google.android.gms.nearby.bootstrap.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzc
  implements Parcelable.Creator<DisableTargetRequest>
{
  static void a(DisableTargetRequest paramDisableTargetRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramDisableTargetRequest.a(), false);
    zzb.a(paramParcel, 1000, paramDisableTargetRequest.a);
    zzb.a(paramParcel, paramInt);
  }
  
  public DisableTargetRequest a(Parcel paramParcel)
  {
    int j = zza.b(paramParcel);
    int i = 0;
    IBinder localIBinder = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localIBinder = zza.q(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new DisableTargetRequest(i, localIBinder);
  }
  
  public DisableTargetRequest[] a(int paramInt)
  {
    return new DisableTargetRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/bootstrap/request/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */