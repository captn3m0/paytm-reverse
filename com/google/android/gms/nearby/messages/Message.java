package com.google.android.gms.nearby.messages;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.nearby.messages.devices.NearbyDevice;
import java.util.Arrays;

public class Message
  implements SafeParcelable
{
  public static final Parcelable.Creator<Message> CREATOR = new zza();
  private static final NearbyDevice[] b = { NearbyDevice.a };
  final int a;
  private final byte[] c;
  private final String d;
  private final String e;
  private final NearbyDevice[] f;
  
  Message(int paramInt, byte[] paramArrayOfByte, String paramString1, String paramString2, NearbyDevice[] paramArrayOfNearbyDevice)
  {
    this.a = paramInt;
    this.d = ((String)zzx.a(paramString2));
    String str = paramString1;
    if (paramString1 == null) {
      str = "";
    }
    this.e = str;
    if (a(this.e, this.d)) {
      if (paramArrayOfByte == null)
      {
        bool = true;
        zzx.b(bool, "Content must be null for a device presence message.");
        this.c = paramArrayOfByte;
        if (paramArrayOfNearbyDevice != null)
        {
          paramArrayOfByte = paramArrayOfNearbyDevice;
          if (paramArrayOfNearbyDevice.length != 0) {}
        }
        else
        {
          paramArrayOfByte = b;
        }
        this.f = paramArrayOfByte;
        if (paramString2.length() > 32) {
          break label195;
        }
      }
    }
    label195:
    for (boolean bool = true;; bool = false)
    {
      zzx.b(bool, "Type length(%d) must not exceed MAX_TYPE_LENGTH(%d)", new Object[] { Integer.valueOf(paramString2.length()), Integer.valueOf(32) });
      return;
      bool = false;
      break;
      zzx.a(paramArrayOfByte);
      if (paramArrayOfByte.length <= 102400) {}
      for (bool = true;; bool = false)
      {
        zzx.b(bool, "Content length(%d) must not exceed MAX_CONTENT_SIZE_BYTES(%d)", new Object[] { Integer.valueOf(paramArrayOfByte.length), Integer.valueOf(102400) });
        break;
      }
    }
  }
  
  public static boolean a(String paramString1, String paramString2)
  {
    return (paramString1.equals("__reserved_namespace")) && (paramString2.equals("__device_presence"));
  }
  
  public String a()
  {
    return this.d;
  }
  
  public String b()
  {
    return this.e;
  }
  
  public byte[] c()
  {
    return this.c;
  }
  
  public NearbyDevice[] d()
  {
    return this.f;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof Message)) {
        return false;
      }
      paramObject = (Message)paramObject;
    } while ((TextUtils.equals(this.e, ((Message)paramObject).e)) && (TextUtils.equals(this.d, ((Message)paramObject).d)) && (Arrays.equals(this.c, ((Message)paramObject).c)));
    return false;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.e, this.d, Integer.valueOf(Arrays.hashCode(this.c)) });
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder().append("Message{namespace='").append(this.e).append("'").append(", type='").append(this.d).append("'").append(", content=[");
    if (this.c == null) {}
    for (int i = 0;; i = this.c.length) {
      return i + " bytes]" + ", devices=" + Arrays.toString(d()) + "}";
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/Message.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */