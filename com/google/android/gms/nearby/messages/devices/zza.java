package com.google.android.gms.nearby.messages.devices;

import java.util.Arrays;

abstract class zza
{
  private static final char[] a = "0123456789abcdef".toCharArray();
  private final byte[] b;
  
  protected zza(byte[] paramArrayOfByte)
  {
    this.b = paramArrayOfByte;
  }
  
  static String a(byte[] paramArrayOfByte)
  {
    StringBuilder localStringBuilder = new StringBuilder(paramArrayOfByte.length * 2);
    int j = paramArrayOfByte.length;
    int i = 0;
    while (i < j)
    {
      int k = paramArrayOfByte[i];
      localStringBuilder.append(a[(k >> 4 & 0xF)]).append(a[(k & 0xF)]);
      i += 1;
    }
    return localStringBuilder.toString();
  }
  
  byte[] a()
  {
    return this.b;
  }
  
  String b()
  {
    return a(this.b);
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!paramObject.getClass().isAssignableFrom(getClass())) {
      return false;
    }
    paramObject = (zza)paramObject;
    return Arrays.equals(this.b, ((zza)paramObject).b);
  }
  
  public int hashCode()
  {
    return Arrays.hashCode(this.b);
  }
  
  public String toString()
  {
    return a(this.b);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/devices/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */