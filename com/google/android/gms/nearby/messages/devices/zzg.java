package com.google.android.gms.nearby.messages.devices;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzg
  implements Parcelable.Creator<NearbyDeviceFilter>
{
  static void a(NearbyDeviceFilter paramNearbyDeviceFilter, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramNearbyDeviceFilter.b);
    zzb.a(paramParcel, 1000, paramNearbyDeviceFilter.a);
    zzb.a(paramParcel, 2, paramNearbyDeviceFilter.c, false);
    zzb.a(paramParcel, 3, paramNearbyDeviceFilter.d);
    zzb.a(paramParcel, paramInt);
  }
  
  public NearbyDeviceFilter a(Parcel paramParcel)
  {
    boolean bool = false;
    int k = zza.b(paramParcel);
    byte[] arrayOfByte = null;
    int j = 0;
    int i = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        j = zza.g(paramParcel, m);
        break;
      case 1000: 
        i = zza.g(paramParcel, m);
        break;
      case 2: 
        arrayOfByte = zza.s(paramParcel, m);
        break;
      case 3: 
        bool = zza.c(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new NearbyDeviceFilter(i, j, arrayOfByte, bool);
  }
  
  public NearbyDeviceFilter[] a(int paramInt)
  {
    return new NearbyDeviceFilter[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/devices/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */