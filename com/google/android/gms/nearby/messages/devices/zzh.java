package com.google.android.gms.nearby.messages.devices;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzh
  implements Parcelable.Creator<NearbyDeviceId>
{
  static void a(NearbyDeviceId paramNearbyDeviceId, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramNearbyDeviceId.a());
    zzb.a(paramParcel, 1000, paramNearbyDeviceId.b);
    zzb.a(paramParcel, 2, paramNearbyDeviceId.c, false);
    zzb.a(paramParcel, paramInt);
  }
  
  public NearbyDeviceId a(Parcel paramParcel)
  {
    int j = 0;
    int k = zza.b(paramParcel);
    byte[] arrayOfByte = null;
    int i = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        j = zza.g(paramParcel, m);
        break;
      case 1000: 
        i = zza.g(paramParcel, m);
        break;
      case 2: 
        arrayOfByte = zza.s(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new NearbyDeviceId(i, j, arrayOfByte);
  }
  
  public NearbyDeviceId[] a(int paramInt)
  {
    return new NearbyDeviceId[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/devices/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */