package com.google.android.gms.nearby.messages.devices;

import com.google.android.gms.common.internal.zzx;
import java.nio.ByteBuffer;
import java.util.UUID;

class zze
  extends zza
{
  public zze(byte[] paramArrayOfByte)
  {
    super(b(paramArrayOfByte));
  }
  
  private static byte[] b(byte[] paramArrayOfByte)
  {
    if ((paramArrayOfByte.length == 16) || (paramArrayOfByte.length == 18) || (paramArrayOfByte.length == 20)) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.b(bool, "Prefix must be a UUID, a UUID and a major, or a UUID, a major, and a minor.");
      return paramArrayOfByte;
    }
  }
  
  public UUID c()
  {
    ByteBuffer localByteBuffer = ByteBuffer.wrap(a());
    return new UUID(localByteBuffer.getLong(), localByteBuffer.getLong());
  }
  
  public Short d()
  {
    byte[] arrayOfByte = a();
    if (arrayOfByte.length >= 18) {
      return Short.valueOf(ByteBuffer.wrap(arrayOfByte).getShort(16));
    }
    return null;
  }
  
  public Short e()
  {
    byte[] arrayOfByte = a();
    if (arrayOfByte.length == 20) {
      return Short.valueOf(ByteBuffer.wrap(arrayOfByte).getShort(18));
    }
    return null;
  }
  
  public String toString()
  {
    return "IBeaconIdPrefix{proximityUuid=" + c() + ", major=" + d() + ", minor=" + e() + '}';
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/devices/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */