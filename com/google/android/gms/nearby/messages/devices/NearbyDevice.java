package com.google.android.gms.nearby.messages.devices;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzx;
import java.util.Arrays;

public class NearbyDevice
  implements SafeParcelable
{
  public static final Parcelable.Creator<NearbyDevice> CREATOR = new zzf();
  public static final NearbyDevice a = new NearbyDevice("", c, d);
  private static final NearbyDeviceId[] c = new NearbyDeviceId[0];
  private static final String[] d = new String[0];
  final int b;
  @Deprecated
  private final NearbyDeviceId e;
  @Deprecated
  @Nullable
  private final String f;
  private final String g;
  private final NearbyDeviceId[] h;
  private final String[] i;
  
  NearbyDevice(int paramInt, @Nullable NearbyDeviceId paramNearbyDeviceId, @Nullable String paramString1, @Nullable String paramString2, @Nullable NearbyDeviceId[] paramArrayOfNearbyDeviceId, @Nullable String[] paramArrayOfString)
  {
    this.b = ((Integer)zzx.a(Integer.valueOf(paramInt))).intValue();
    paramNearbyDeviceId = paramString2;
    if (paramString2 == null) {
      paramNearbyDeviceId = "";
    }
    this.g = paramNearbyDeviceId;
    paramNearbyDeviceId = paramArrayOfNearbyDeviceId;
    if (paramArrayOfNearbyDeviceId == null) {
      paramNearbyDeviceId = c;
    }
    this.h = paramNearbyDeviceId;
    paramNearbyDeviceId = paramArrayOfString;
    if (paramArrayOfString == null) {
      paramNearbyDeviceId = d;
    }
    this.i = paramNearbyDeviceId;
    if (this.h.length == 0)
    {
      paramNearbyDeviceId = NearbyDeviceId.a;
      this.e = paramNearbyDeviceId;
      if (this.i.length != 0) {
        break label114;
      }
    }
    label114:
    for (paramNearbyDeviceId = null;; paramNearbyDeviceId = this.i[0])
    {
      this.f = paramNearbyDeviceId;
      return;
      paramNearbyDeviceId = this.h[0];
      break;
    }
  }
  
  public NearbyDevice(String paramString, NearbyDeviceId[] paramArrayOfNearbyDeviceId, String[] paramArrayOfString)
  {
    this(1, null, null, paramString, paramArrayOfNearbyDeviceId, paramArrayOfString);
  }
  
  @Deprecated
  public NearbyDeviceId a()
  {
    if (this.h.length == 0) {
      return NearbyDeviceId.a;
    }
    return this.h[0];
  }
  
  public NearbyDeviceId[] b()
  {
    return this.h;
  }
  
  @Deprecated
  @Nullable
  public String c()
  {
    if (this.i.length == 0) {
      return null;
    }
    return this.i[0];
  }
  
  public String[] d()
  {
    return this.i;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String e()
  {
    return this.g;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof NearbyDevice)) {
      return false;
    }
    paramObject = (NearbyDevice)paramObject;
    return zzw.a(this.g, ((NearbyDevice)paramObject).g);
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.g });
  }
  
  public String toString()
  {
    return "NearbyDevice{deviceHandle=" + this.g + ", ids=" + Arrays.toString(this.h) + ", urls=" + Arrays.toString(this.i) + "}";
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/devices/NearbyDevice.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */