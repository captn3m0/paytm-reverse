package com.google.android.gms.nearby.messages.devices;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;

public class NearbyDeviceId
  implements SafeParcelable
{
  public static final Parcelable.Creator<NearbyDeviceId> CREATOR = new zzh();
  public static final NearbyDeviceId a = new NearbyDeviceId();
  final int b;
  final byte[] c;
  private final int d;
  private final zzb e;
  private final zzd f;
  
  private NearbyDeviceId()
  {
    this(1, 1, null);
  }
  
  NearbyDeviceId(int paramInt1, int paramInt2, byte[] paramArrayOfByte)
  {
    this.b = paramInt1;
    this.d = paramInt2;
    this.c = paramArrayOfByte;
    if (paramInt2 == 2) {}
    for (Object localObject1 = new zzb(paramArrayOfByte);; localObject1 = null)
    {
      this.e = ((zzb)localObject1);
      localObject1 = localObject2;
      if (paramInt2 == 3) {
        localObject1 = new zzd(paramArrayOfByte);
      }
      this.f = ((zzd)localObject1);
      return;
    }
  }
  
  public int a()
  {
    return this.d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof NearbyDeviceId)) {
        return false;
      }
      paramObject = (NearbyDeviceId)paramObject;
    } while ((zzw.a(Integer.valueOf(this.d), Integer.valueOf(((NearbyDeviceId)paramObject).d))) && (zzw.a(this.c, ((NearbyDeviceId)paramObject).c)));
    return false;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Integer.valueOf(this.d), this.c });
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder().append("NearbyDeviceId{");
    switch (this.d)
    {
    }
    for (;;)
    {
      localStringBuilder.append("}");
      return localStringBuilder.toString();
      localStringBuilder.append("eddystoneUid=").append(this.e);
      continue;
      localStringBuilder.append("iBeaconId=").append(this.f);
      continue;
      localStringBuilder.append("UNKNOWN");
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzh.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/devices/NearbyDeviceId.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */