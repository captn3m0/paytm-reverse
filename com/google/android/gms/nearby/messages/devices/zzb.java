package com.google.android.gms.nearby.messages.devices;

import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzx;

public class zzb
{
  private final zzc a;
  
  public zzb(byte[] paramArrayOfByte)
  {
    this.a = new zzc(a(paramArrayOfByte));
  }
  
  private static byte[] a(byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte.length == 16) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.b(bool, "Bytes must be a namespace plus instance (16 bytes).");
      return paramArrayOfByte;
    }
  }
  
  public String a()
  {
    return this.a.b();
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof zzb)) {
      return false;
    }
    paramObject = (zzb)paramObject;
    return zzw.a(this.a, ((zzb)paramObject).a);
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.a });
  }
  
  public String toString()
  {
    return "EddystoneUid{id=" + a() + '}';
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/devices/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */