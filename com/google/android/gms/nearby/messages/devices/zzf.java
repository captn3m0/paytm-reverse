package com.google.android.gms.nearby.messages.devices;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzf
  implements Parcelable.Creator<NearbyDevice>
{
  static void a(NearbyDevice paramNearbyDevice, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramNearbyDevice.a(), paramInt, false);
    zzb.a(paramParcel, 1000, paramNearbyDevice.b);
    zzb.a(paramParcel, 2, paramNearbyDevice.c(), false);
    zzb.a(paramParcel, 3, paramNearbyDevice.e(), false);
    zzb.a(paramParcel, 4, paramNearbyDevice.b(), paramInt, false);
    zzb.a(paramParcel, 5, paramNearbyDevice.d(), false);
    zzb.a(paramParcel, i);
  }
  
  public NearbyDevice a(Parcel paramParcel)
  {
    String[] arrayOfString = null;
    int j = zza.b(paramParcel);
    int i = 0;
    NearbyDeviceId[] arrayOfNearbyDeviceId = null;
    String str1 = null;
    String str2 = null;
    NearbyDeviceId localNearbyDeviceId = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localNearbyDeviceId = (NearbyDeviceId)zza.a(paramParcel, k, NearbyDeviceId.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str2 = zza.p(paramParcel, k);
        break;
      case 3: 
        str1 = zza.p(paramParcel, k);
        break;
      case 4: 
        arrayOfNearbyDeviceId = (NearbyDeviceId[])zza.b(paramParcel, k, NearbyDeviceId.CREATOR);
        break;
      case 5: 
        arrayOfString = zza.B(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new NearbyDevice(i, localNearbyDeviceId, str2, str1, arrayOfNearbyDeviceId, arrayOfString);
  }
  
  public NearbyDevice[] a(int paramInt)
  {
    return new NearbyDevice[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/devices/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */