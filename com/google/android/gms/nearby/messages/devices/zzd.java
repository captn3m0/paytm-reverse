package com.google.android.gms.nearby.messages.devices;

import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzx;
import java.util.UUID;

public class zzd
{
  private final zze a;
  
  public zzd(byte[] paramArrayOfByte)
  {
    this.a = new zze(a(paramArrayOfByte));
  }
  
  private static byte[] a(byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte.length == 20) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.b(bool, "iBeacon ID must be a UUID, a major, and a minor (20 total bytes).");
      return paramArrayOfByte;
    }
  }
  
  public UUID a()
  {
    return this.a.c();
  }
  
  public short b()
  {
    return this.a.d().shortValue();
  }
  
  public short c()
  {
    return this.a.e().shortValue();
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof zzd)) {
      return false;
    }
    paramObject = (zzd)paramObject;
    return zzw.a(this.a, ((zzd)paramObject).a);
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.a });
  }
  
  public String toString()
  {
    return "IBeaconId{proximityUuid=" + a() + ", major=" + b() + ", minor=" + c() + '}';
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/devices/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */