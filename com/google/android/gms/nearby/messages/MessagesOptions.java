package com.google.android.gms.nearby.messages;

import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Api.ApiOptions.Optional;

public class MessagesOptions
  implements Api.ApiOptions.Optional
{
  @Nullable
  public final String a;
  public final boolean b;
  public final boolean c;
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/MessagesOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */