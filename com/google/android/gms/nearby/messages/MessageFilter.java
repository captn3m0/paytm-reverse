package com.google.android.gms.nearby.messages;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.nearby.messages.devices.NearbyDeviceFilter;
import com.google.android.gms.nearby.messages.internal.MessageType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MessageFilter
  implements SafeParcelable
{
  public static final Parcelable.Creator<MessageFilter> CREATOR = new zzb();
  public static final MessageFilter a = new Builder().a().b();
  final int b;
  private final List<MessageType> c;
  private final List<NearbyDeviceFilter> d;
  private final boolean e;
  
  MessageFilter(int paramInt, List<MessageType> paramList, List<NearbyDeviceFilter> paramList1, boolean paramBoolean)
  {
    this.b = paramInt;
    this.c = Collections.unmodifiableList((List)zzx.a(paramList));
    this.e = paramBoolean;
    paramList = paramList1;
    if (paramList1 == null) {
      paramList = Collections.emptyList();
    }
    this.d = Collections.unmodifiableList(paramList);
  }
  
  private MessageFilter(List<MessageType> paramList, List<NearbyDeviceFilter> paramList1, boolean paramBoolean)
  {
    this(1, paramList, paramList1, paramBoolean);
  }
  
  List<MessageType> a()
  {
    return this.c;
  }
  
  boolean b()
  {
    return this.e;
  }
  
  List<NearbyDeviceFilter> c()
  {
    return this.d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof MessageFilter)) {
        return false;
      }
      paramObject = (MessageFilter)paramObject;
    } while ((this.e == ((MessageFilter)paramObject).e) && (zzw.a(this.c, ((MessageFilter)paramObject).c)) && (zzw.a(this.d, ((MessageFilter)paramObject).d)));
    return false;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.c, this.d, Boolean.valueOf(this.e) });
  }
  
  public String toString()
  {
    return "MessageFilter{includeAllMyTypes=" + this.e + ", messageTypes=" + this.c + "}";
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.a(this, paramParcel, paramInt);
  }
  
  public static final class Builder
  {
    private final List<MessageType> a = new ArrayList();
    private final List<NearbyDeviceFilter> b = new ArrayList();
    private boolean c;
    
    public Builder a()
    {
      this.c = true;
      return this;
    }
    
    public MessageFilter b()
    {
      if ((this.c) || (!this.a.isEmpty())) {}
      for (boolean bool = true;; bool = false)
      {
        zzx.a(bool, "At least one of the include methods must be called.");
        return new MessageFilter(this.a, this.b, this.c, null);
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/MessageFilter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */