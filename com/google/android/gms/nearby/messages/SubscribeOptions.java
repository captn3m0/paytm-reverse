package com.google.android.gms.nearby.messages;

import android.support.annotation.Nullable;

public final class SubscribeOptions
{
  public static final SubscribeOptions a = new Builder().a();
  private final Strategy b;
  private final MessageFilter c;
  @Nullable
  private final SubscribeCallback d;
  
  private SubscribeOptions(Strategy paramStrategy, MessageFilter paramMessageFilter, @Nullable SubscribeCallback paramSubscribeCallback)
  {
    this.b = paramStrategy;
    this.c = paramMessageFilter;
    this.d = paramSubscribeCallback;
  }
  
  public Strategy a()
  {
    return this.b;
  }
  
  public MessageFilter b()
  {
    return this.c;
  }
  
  @Nullable
  public SubscribeCallback c()
  {
    return this.d;
  }
  
  public static class Builder
  {
    private Strategy a = Strategy.a;
    private MessageFilter b = MessageFilter.a;
    @Nullable
    private SubscribeCallback c;
    
    public SubscribeOptions a()
    {
      return new SubscribeOptions(this.a, this.b, this.c, null);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/SubscribeOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */