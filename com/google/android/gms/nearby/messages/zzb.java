package com.google.android.gms.nearby.messages;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.nearby.messages.devices.NearbyDeviceFilter;
import com.google.android.gms.nearby.messages.internal.MessageType;
import java.util.ArrayList;

public class zzb
  implements Parcelable.Creator<MessageFilter>
{
  static void a(MessageFilter paramMessageFilter, Parcel paramParcel, int paramInt)
  {
    paramInt = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.c(paramParcel, 1, paramMessageFilter.a(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1000, paramMessageFilter.b);
    com.google.android.gms.common.internal.safeparcel.zzb.c(paramParcel, 2, paramMessageFilter.c(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 3, paramMessageFilter.b());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, paramInt);
  }
  
  public MessageFilter a(Parcel paramParcel)
  {
    ArrayList localArrayList2 = null;
    boolean bool = false;
    int j = zza.b(paramParcel);
    ArrayList localArrayList1 = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localArrayList1 = zza.c(paramParcel, k, MessageType.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localArrayList2 = zza.c(paramParcel, k, NearbyDeviceFilter.CREATOR);
        break;
      case 3: 
        bool = zza.c(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new MessageFilter(i, localArrayList1, localArrayList2, bool);
  }
  
  public MessageFilter[] a(int paramInt)
  {
    return new MessageFilter[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */