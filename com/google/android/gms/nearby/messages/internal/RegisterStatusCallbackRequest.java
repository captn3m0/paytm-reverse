package com.google.android.gms.nearby.messages.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class RegisterStatusCallbackRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<RegisterStatusCallbackRequest> CREATOR = new zzr();
  final int a;
  public final zze b;
  public final zzh c;
  public boolean d;
  @Deprecated
  public String e;
  public final ClientAppContext f;
  
  RegisterStatusCallbackRequest(int paramInt, IBinder paramIBinder1, IBinder paramIBinder2, boolean paramBoolean, String paramString, ClientAppContext paramClientAppContext)
  {
    this.a = paramInt;
    this.b = zze.zza.a(paramIBinder1);
    this.c = zzh.zza.a(paramIBinder2);
    this.d = paramBoolean;
    this.e = paramString;
    if (paramClientAppContext != null) {}
    for (;;)
    {
      this.f = paramClientAppContext;
      return;
      paramClientAppContext = new ClientAppContext(null, this.e);
    }
  }
  
  RegisterStatusCallbackRequest(IBinder paramIBinder1, IBinder paramIBinder2, ClientAppContext paramClientAppContext)
  {
    this(1, paramIBinder1, paramIBinder2, false, null, paramClientAppContext);
  }
  
  IBinder a()
  {
    return this.b.asBinder();
  }
  
  IBinder b()
  {
    return this.c.asBinder();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzr.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/RegisterStatusCallbackRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */