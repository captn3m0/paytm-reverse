package com.google.android.gms.nearby.messages.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzr
  implements Parcelable.Creator<RegisterStatusCallbackRequest>
{
  static void a(RegisterStatusCallbackRequest paramRegisterStatusCallbackRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramRegisterStatusCallbackRequest.a);
    zzb.a(paramParcel, 2, paramRegisterStatusCallbackRequest.a(), false);
    zzb.a(paramParcel, 3, paramRegisterStatusCallbackRequest.b(), false);
    zzb.a(paramParcel, 4, paramRegisterStatusCallbackRequest.d);
    zzb.a(paramParcel, 5, paramRegisterStatusCallbackRequest.e, false);
    zzb.a(paramParcel, 6, paramRegisterStatusCallbackRequest.f, paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public RegisterStatusCallbackRequest a(Parcel paramParcel)
  {
    boolean bool = false;
    ClientAppContext localClientAppContext = null;
    int j = zza.b(paramParcel);
    String str = null;
    IBinder localIBinder1 = null;
    IBinder localIBinder2 = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localIBinder2 = zza.q(paramParcel, k);
        break;
      case 3: 
        localIBinder1 = zza.q(paramParcel, k);
        break;
      case 4: 
        bool = zza.c(paramParcel, k);
        break;
      case 5: 
        str = zza.p(paramParcel, k);
        break;
      case 6: 
        localClientAppContext = (ClientAppContext)zza.a(paramParcel, k, ClientAppContext.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new RegisterStatusCallbackRequest(i, localIBinder2, localIBinder1, bool, str, localClientAppContext);
  }
  
  public RegisterStatusCallbackRequest[] a(int paramInt)
  {
    return new RegisterStatusCallbackRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/zzr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */