package com.google.android.gms.nearby.messages.internal;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.nearby.messages.MessageFilter;
import com.google.android.gms.nearby.messages.Strategy;

public final class SubscribeRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<SubscribeRequest> CREATOR = new zzs();
  final int a;
  public final zzd b;
  public final Strategy c;
  public final zze d;
  public final MessageFilter e;
  public final PendingIntent f;
  public final int g;
  @Deprecated
  public final String h;
  @Deprecated
  public final String i;
  public final byte[] j;
  public final boolean k;
  public final zzi l;
  @Deprecated
  public final boolean m;
  public final ClientAppContext n;
  
  SubscribeRequest(int paramInt1, IBinder paramIBinder1, Strategy paramStrategy, IBinder paramIBinder2, MessageFilter paramMessageFilter, PendingIntent paramPendingIntent, int paramInt2, String paramString1, String paramString2, byte[] paramArrayOfByte, boolean paramBoolean1, IBinder paramIBinder3, boolean paramBoolean2, ClientAppContext paramClientAppContext)
  {
    this.a = paramInt1;
    this.b = zzd.zza.a(paramIBinder1);
    this.c = paramStrategy;
    this.d = zze.zza.a(paramIBinder2);
    this.e = paramMessageFilter;
    this.f = paramPendingIntent;
    this.g = paramInt2;
    this.h = paramString1;
    this.i = paramString2;
    this.j = paramArrayOfByte;
    this.k = paramBoolean1;
    if (paramIBinder3 == null)
    {
      paramIBinder1 = null;
      this.l = paramIBinder1;
      this.m = paramBoolean2;
      if (paramClientAppContext == null) {
        break label112;
      }
    }
    for (;;)
    {
      this.n = paramClientAppContext;
      return;
      paramIBinder1 = zzi.zza.a(paramIBinder3);
      break;
      label112:
      paramClientAppContext = new ClientAppContext(this.i, this.h, this.m);
    }
  }
  
  public SubscribeRequest(IBinder paramIBinder1, Strategy paramStrategy, IBinder paramIBinder2, MessageFilter paramMessageFilter, PendingIntent paramPendingIntent, int paramInt, byte[] paramArrayOfByte, boolean paramBoolean, IBinder paramIBinder3, ClientAppContext paramClientAppContext)
  {
    this(3, paramIBinder1, paramStrategy, paramIBinder2, paramMessageFilter, paramPendingIntent, paramInt, null, null, paramArrayOfByte, paramBoolean, paramIBinder3, false, paramClientAppContext);
  }
  
  IBinder a()
  {
    if (this.d == null) {
      return null;
    }
    return this.d.asBinder();
  }
  
  IBinder b()
  {
    if (this.b == null) {
      return null;
    }
    return this.b.asBinder();
  }
  
  IBinder c()
  {
    if (this.l == null) {
      return null;
    }
    return this.l.asBinder();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzs.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/SubscribeRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */