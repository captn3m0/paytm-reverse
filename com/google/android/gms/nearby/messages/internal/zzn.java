package com.google.android.gms.nearby.messages.internal;

import android.content.Context;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zza;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.nearby.messages.Messages;
import com.google.android.gms.nearby.messages.MessagesOptions;

public class zzn
  implements Messages
{
  public static final Api.zzc<zzm> a = new Api.zzc();
  public static final Api.zza<zzm, MessagesOptions> b = new Api.zza()
  {
    public int a()
    {
      return Integer.MAX_VALUE;
    }
    
    public zzm a(Context paramAnonymousContext, Looper paramAnonymousLooper, zzf paramAnonymouszzf, MessagesOptions paramAnonymousMessagesOptions, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      return new zzm(paramAnonymousContext, paramAnonymousLooper, paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener, paramAnonymouszzf, paramAnonymousMessagesOptions);
    }
  };
  
  static abstract class zza
    extends zza.zza<Status, zzm>
  {
    public Status a(Status paramStatus)
    {
      return paramStatus;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */