package com.google.android.gms.nearby.messages.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.nearby.messages.Message;

public class MessageWrapper
  implements SafeParcelable
{
  public static final zzl CREATOR = new zzl();
  final int a;
  public final Message b;
  
  MessageWrapper(int paramInt, Message paramMessage)
  {
    this.a = paramInt;
    this.b = ((Message)zzx.a(paramMessage));
  }
  
  public static final MessageWrapper a(Message paramMessage)
  {
    return new MessageWrapper(1, paramMessage);
  }
  
  public int describeContents()
  {
    zzl localzzl = CREATOR;
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof MessageWrapper)) {
      return false;
    }
    paramObject = (MessageWrapper)paramObject;
    return zzw.a(this.b, ((MessageWrapper)paramObject).b);
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzl localzzl = CREATOR;
    zzl.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/MessageWrapper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */