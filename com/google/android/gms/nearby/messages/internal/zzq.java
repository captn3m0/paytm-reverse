package com.google.android.gms.nearby.messages.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.nearby.messages.Strategy;

public class zzq
  implements Parcelable.Creator<PublishRequest>
{
  static void a(PublishRequest paramPublishRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramPublishRequest.a);
    zzb.a(paramParcel, 2, paramPublishRequest.b, paramInt, false);
    zzb.a(paramParcel, 3, paramPublishRequest.c, paramInt, false);
    zzb.a(paramParcel, 4, paramPublishRequest.a(), false);
    zzb.a(paramParcel, 5, paramPublishRequest.e, false);
    zzb.a(paramParcel, 6, paramPublishRequest.f, false);
    zzb.a(paramParcel, 7, paramPublishRequest.g);
    zzb.a(paramParcel, 8, paramPublishRequest.b(), false);
    zzb.a(paramParcel, 9, paramPublishRequest.i);
    zzb.a(paramParcel, 10, paramPublishRequest.j, paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public PublishRequest a(Parcel paramParcel)
  {
    boolean bool1 = false;
    ClientAppContext localClientAppContext = null;
    int j = zza.b(paramParcel);
    IBinder localIBinder1 = null;
    boolean bool2 = false;
    String str1 = null;
    String str2 = null;
    IBinder localIBinder2 = null;
    Strategy localStrategy = null;
    MessageWrapper localMessageWrapper = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localMessageWrapper = (MessageWrapper)zza.a(paramParcel, k, MessageWrapper.CREATOR);
        break;
      case 3: 
        localStrategy = (Strategy)zza.a(paramParcel, k, Strategy.CREATOR);
        break;
      case 4: 
        localIBinder2 = zza.q(paramParcel, k);
        break;
      case 5: 
        str2 = zza.p(paramParcel, k);
        break;
      case 6: 
        str1 = zza.p(paramParcel, k);
        break;
      case 7: 
        bool2 = zza.c(paramParcel, k);
        break;
      case 8: 
        localIBinder1 = zza.q(paramParcel, k);
        break;
      case 9: 
        bool1 = zza.c(paramParcel, k);
        break;
      case 10: 
        localClientAppContext = (ClientAppContext)zza.a(paramParcel, k, ClientAppContext.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new PublishRequest(i, localMessageWrapper, localStrategy, localIBinder2, str2, str1, bool2, localIBinder1, bool1, localClientAppContext);
  }
  
  public PublishRequest[] a(int paramInt)
  {
    return new PublishRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */