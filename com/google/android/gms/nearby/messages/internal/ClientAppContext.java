package com.google.android.gms.nearby.messages.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;

public final class ClientAppContext
  implements SafeParcelable
{
  public static final Parcelable.Creator<ClientAppContext> CREATOR = new zza();
  final int a;
  public final String b;
  public final String c;
  public final boolean d;
  public final int e;
  
  ClientAppContext(int paramInt1, String paramString1, String paramString2, boolean paramBoolean, int paramInt2)
  {
    this.a = paramInt1;
    this.b = paramString1;
    this.c = paramString2;
    this.d = paramBoolean;
    this.e = paramInt2;
  }
  
  public ClientAppContext(String paramString1, String paramString2)
  {
    this(paramString1, paramString2, false);
  }
  
  public ClientAppContext(String paramString1, String paramString2, boolean paramBoolean)
  {
    this(paramString1, paramString2, paramBoolean, 0);
  }
  
  public ClientAppContext(String paramString1, String paramString2, boolean paramBoolean, int paramInt)
  {
    this(1, paramString1, paramString2, paramBoolean, paramInt);
  }
  
  private static boolean a(String paramString1, String paramString2)
  {
    if (TextUtils.isEmpty(paramString1)) {
      return TextUtils.isEmpty(paramString2);
    }
    return paramString1.equals(paramString2);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof ClientAppContext)) {
        return false;
      }
      paramObject = (ClientAppContext)paramObject;
    } while ((a(this.b, ((ClientAppContext)paramObject).b)) && (a(this.c, ((ClientAppContext)paramObject).c)) && (this.d == ((ClientAppContext)paramObject).d) && (this.e == ((ClientAppContext)paramObject).e));
    return false;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b, this.c, Boolean.valueOf(this.d), Integer.valueOf(this.e) });
  }
  
  public String toString()
  {
    return String.format("{realClientPackageName: %s, zeroPartyIdentifier: %s, useRealClientApiKey: %b, callingContext: %d}", new Object[] { this.b, this.c, Boolean.valueOf(this.d), Integer.valueOf(this.e) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/ClientAppContext.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */