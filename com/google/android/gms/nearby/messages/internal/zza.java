package com.google.android.gms.nearby.messages.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zza
  implements Parcelable.Creator<ClientAppContext>
{
  static void a(ClientAppContext paramClientAppContext, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramClientAppContext.a);
    zzb.a(paramParcel, 2, paramClientAppContext.b, false);
    zzb.a(paramParcel, 3, paramClientAppContext.c, false);
    zzb.a(paramParcel, 4, paramClientAppContext.d);
    zzb.a(paramParcel, 5, paramClientAppContext.e);
    zzb.a(paramParcel, paramInt);
  }
  
  public ClientAppContext a(Parcel paramParcel)
  {
    String str1 = null;
    int i = 0;
    int k = com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel);
    boolean bool = false;
    String str2 = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = com.google.android.gms.common.internal.safeparcel.zza.a(paramParcel);
      switch (com.google.android.gms.common.internal.safeparcel.zza.a(m))
      {
      default: 
        com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel, m);
        break;
      case 1: 
        j = com.google.android.gms.common.internal.safeparcel.zza.g(paramParcel, m);
        break;
      case 2: 
        str2 = com.google.android.gms.common.internal.safeparcel.zza.p(paramParcel, m);
        break;
      case 3: 
        str1 = com.google.android.gms.common.internal.safeparcel.zza.p(paramParcel, m);
        break;
      case 4: 
        bool = com.google.android.gms.common.internal.safeparcel.zza.c(paramParcel, m);
        break;
      case 5: 
        i = com.google.android.gms.common.internal.safeparcel.zza.g(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new ClientAppContext(j, str2, str1, bool, i);
  }
  
  public ClientAppContext[] a(int paramInt)
  {
    return new ClientAppContext[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */