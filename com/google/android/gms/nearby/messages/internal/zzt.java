package com.google.android.gms.nearby.messages.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzt
  implements Parcelable.Creator<UnpublishRequest>
{
  static void a(UnpublishRequest paramUnpublishRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramUnpublishRequest.a);
    zzb.a(paramParcel, 2, paramUnpublishRequest.b, paramInt, false);
    zzb.a(paramParcel, 3, paramUnpublishRequest.a(), false);
    zzb.a(paramParcel, 4, paramUnpublishRequest.d, false);
    zzb.a(paramParcel, 5, paramUnpublishRequest.e, false);
    zzb.a(paramParcel, 6, paramUnpublishRequest.f);
    zzb.a(paramParcel, 7, paramUnpublishRequest.g, paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public UnpublishRequest a(Parcel paramParcel)
  {
    boolean bool = false;
    ClientAppContext localClientAppContext = null;
    int j = zza.b(paramParcel);
    String str1 = null;
    String str2 = null;
    IBinder localIBinder = null;
    MessageWrapper localMessageWrapper = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localMessageWrapper = (MessageWrapper)zza.a(paramParcel, k, MessageWrapper.CREATOR);
        break;
      case 3: 
        localIBinder = zza.q(paramParcel, k);
        break;
      case 4: 
        str2 = zza.p(paramParcel, k);
        break;
      case 5: 
        str1 = zza.p(paramParcel, k);
        break;
      case 6: 
        bool = zza.c(paramParcel, k);
        break;
      case 7: 
        localClientAppContext = (ClientAppContext)zza.a(paramParcel, k, ClientAppContext.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new UnpublishRequest(i, localMessageWrapper, localIBinder, str2, str1, bool, localClientAppContext);
  }
  
  public UnpublishRequest[] a(int paramInt)
  {
    return new UnpublishRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/zzt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */