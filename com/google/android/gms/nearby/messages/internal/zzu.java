package com.google.android.gms.nearby.messages.internal;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzu
  implements Parcelable.Creator<UnsubscribeRequest>
{
  static void a(UnsubscribeRequest paramUnsubscribeRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramUnsubscribeRequest.a);
    zzb.a(paramParcel, 2, paramUnsubscribeRequest.b(), false);
    zzb.a(paramParcel, 3, paramUnsubscribeRequest.a(), false);
    zzb.a(paramParcel, 4, paramUnsubscribeRequest.d, paramInt, false);
    zzb.a(paramParcel, 5, paramUnsubscribeRequest.e);
    zzb.a(paramParcel, 6, paramUnsubscribeRequest.f, false);
    zzb.a(paramParcel, 7, paramUnsubscribeRequest.g, false);
    zzb.a(paramParcel, 8, paramUnsubscribeRequest.h);
    zzb.a(paramParcel, 9, paramUnsubscribeRequest.i, paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public UnsubscribeRequest a(Parcel paramParcel)
  {
    boolean bool = false;
    ClientAppContext localClientAppContext = null;
    int k = zza.b(paramParcel);
    String str1 = null;
    String str2 = null;
    int i = 0;
    PendingIntent localPendingIntent = null;
    IBinder localIBinder1 = null;
    IBinder localIBinder2 = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        localIBinder2 = zza.q(paramParcel, m);
        break;
      case 3: 
        localIBinder1 = zza.q(paramParcel, m);
        break;
      case 4: 
        localPendingIntent = (PendingIntent)zza.a(paramParcel, m, PendingIntent.CREATOR);
        break;
      case 5: 
        i = zza.g(paramParcel, m);
        break;
      case 6: 
        str2 = zza.p(paramParcel, m);
        break;
      case 7: 
        str1 = zza.p(paramParcel, m);
        break;
      case 8: 
        bool = zza.c(paramParcel, m);
        break;
      case 9: 
        localClientAppContext = (ClientAppContext)zza.a(paramParcel, m, ClientAppContext.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new UnsubscribeRequest(j, localIBinder2, localIBinder1, localPendingIntent, i, str2, str1, bool, localClientAppContext);
  }
  
  public UnsubscribeRequest[] a(int paramInt)
  {
    return new UnsubscribeRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/zzu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */