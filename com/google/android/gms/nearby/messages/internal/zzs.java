package com.google.android.gms.nearby.messages.internal;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.nearby.messages.MessageFilter;
import com.google.android.gms.nearby.messages.Strategy;

public class zzs
  implements Parcelable.Creator<SubscribeRequest>
{
  static void a(SubscribeRequest paramSubscribeRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramSubscribeRequest.a);
    zzb.a(paramParcel, 2, paramSubscribeRequest.b(), false);
    zzb.a(paramParcel, 3, paramSubscribeRequest.c, paramInt, false);
    zzb.a(paramParcel, 4, paramSubscribeRequest.a(), false);
    zzb.a(paramParcel, 5, paramSubscribeRequest.e, paramInt, false);
    zzb.a(paramParcel, 6, paramSubscribeRequest.f, paramInt, false);
    zzb.a(paramParcel, 7, paramSubscribeRequest.g);
    zzb.a(paramParcel, 8, paramSubscribeRequest.h, false);
    zzb.a(paramParcel, 9, paramSubscribeRequest.i, false);
    zzb.a(paramParcel, 10, paramSubscribeRequest.j, false);
    zzb.a(paramParcel, 11, paramSubscribeRequest.k);
    zzb.a(paramParcel, 12, paramSubscribeRequest.c(), false);
    zzb.a(paramParcel, 13, paramSubscribeRequest.m);
    zzb.a(paramParcel, 14, paramSubscribeRequest.n, paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public SubscribeRequest a(Parcel paramParcel)
  {
    int k = zza.b(paramParcel);
    int j = 0;
    IBinder localIBinder3 = null;
    Strategy localStrategy = null;
    IBinder localIBinder2 = null;
    MessageFilter localMessageFilter = null;
    PendingIntent localPendingIntent = null;
    int i = 0;
    String str2 = null;
    String str1 = null;
    byte[] arrayOfByte = null;
    boolean bool2 = false;
    IBinder localIBinder1 = null;
    boolean bool1 = false;
    ClientAppContext localClientAppContext = null;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        localIBinder3 = zza.q(paramParcel, m);
        break;
      case 3: 
        localStrategy = (Strategy)zza.a(paramParcel, m, Strategy.CREATOR);
        break;
      case 4: 
        localIBinder2 = zza.q(paramParcel, m);
        break;
      case 5: 
        localMessageFilter = (MessageFilter)zza.a(paramParcel, m, MessageFilter.CREATOR);
        break;
      case 6: 
        localPendingIntent = (PendingIntent)zza.a(paramParcel, m, PendingIntent.CREATOR);
        break;
      case 7: 
        i = zza.g(paramParcel, m);
        break;
      case 8: 
        str2 = zza.p(paramParcel, m);
        break;
      case 9: 
        str1 = zza.p(paramParcel, m);
        break;
      case 10: 
        arrayOfByte = zza.s(paramParcel, m);
        break;
      case 11: 
        bool2 = zza.c(paramParcel, m);
        break;
      case 12: 
        localIBinder1 = zza.q(paramParcel, m);
        break;
      case 13: 
        bool1 = zza.c(paramParcel, m);
        break;
      case 14: 
        localClientAppContext = (ClientAppContext)zza.a(paramParcel, m, ClientAppContext.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new SubscribeRequest(j, localIBinder3, localStrategy, localIBinder2, localMessageFilter, localPendingIntent, i, str2, str1, arrayOfByte, bool2, localIBinder1, bool1, localClientAppContext);
  }
  
  public SubscribeRequest[] a(int paramInt)
  {
    return new SubscribeRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/zzs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */