package com.google.android.gms.nearby.messages.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class zzb
  implements Parcelable.Creator<GetPermissionStatusRequest>
{
  static void a(GetPermissionStatusRequest paramGetPermissionStatusRequest, Parcel paramParcel, int paramInt)
  {
    int i = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramGetPermissionStatusRequest.a);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramGetPermissionStatusRequest.a(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 3, paramGetPermissionStatusRequest.c, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 4, paramGetPermissionStatusRequest.d, paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, i);
  }
  
  public GetPermissionStatusRequest a(Parcel paramParcel)
  {
    ClientAppContext localClientAppContext = null;
    int j = zza.b(paramParcel);
    int i = 0;
    String str = null;
    IBinder localIBinder = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localIBinder = zza.q(paramParcel, k);
        break;
      case 3: 
        str = zza.p(paramParcel, k);
        break;
      case 4: 
        localClientAppContext = (ClientAppContext)zza.a(paramParcel, k, ClientAppContext.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new GetPermissionStatusRequest(i, localIBinder, str, localClientAppContext);
  }
  
  public GetPermissionStatusRequest[] a(int paramInt)
  {
    return new GetPermissionStatusRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */