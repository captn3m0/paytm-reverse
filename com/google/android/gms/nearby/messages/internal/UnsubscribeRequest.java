package com.google.android.gms.nearby.messages.internal;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class UnsubscribeRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<UnsubscribeRequest> CREATOR = new zzu();
  final int a;
  public final zzd b;
  public final zze c;
  public final PendingIntent d;
  public final int e;
  @Deprecated
  public final String f;
  @Deprecated
  public final String g;
  @Deprecated
  public final boolean h;
  public final ClientAppContext i;
  
  UnsubscribeRequest(int paramInt1, IBinder paramIBinder1, IBinder paramIBinder2, PendingIntent paramPendingIntent, int paramInt2, String paramString1, String paramString2, boolean paramBoolean, ClientAppContext paramClientAppContext)
  {
    this.a = paramInt1;
    this.b = zzd.zza.a(paramIBinder1);
    this.c = zze.zza.a(paramIBinder2);
    this.d = paramPendingIntent;
    this.e = paramInt2;
    this.f = paramString1;
    this.g = paramString2;
    this.h = paramBoolean;
    if (paramClientAppContext != null) {}
    for (;;)
    {
      this.i = paramClientAppContext;
      return;
      paramClientAppContext = new ClientAppContext(this.g, this.f, this.h);
    }
  }
  
  UnsubscribeRequest(IBinder paramIBinder1, IBinder paramIBinder2, PendingIntent paramPendingIntent, int paramInt, ClientAppContext paramClientAppContext)
  {
    this(1, paramIBinder1, paramIBinder2, paramPendingIntent, paramInt, null, null, false, paramClientAppContext);
  }
  
  IBinder a()
  {
    return this.c.asBinder();
  }
  
  IBinder b()
  {
    if (this.b == null) {
      return null;
    }
    return this.b.asBinder();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzu.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/UnsubscribeRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */