package com.google.android.gms.nearby.messages.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzc
  implements Parcelable.Creator<HandleClientLifecycleEventRequest>
{
  static void a(HandleClientLifecycleEventRequest paramHandleClientLifecycleEventRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramHandleClientLifecycleEventRequest.a);
    zzb.a(paramParcel, 2, paramHandleClientLifecycleEventRequest.b, paramInt, false);
    zzb.a(paramParcel, 3, paramHandleClientLifecycleEventRequest.c);
    zzb.a(paramParcel, i);
  }
  
  public HandleClientLifecycleEventRequest a(Parcel paramParcel)
  {
    int i = 0;
    int k = zza.b(paramParcel);
    ClientAppContext localClientAppContext = null;
    int j = 0;
    if (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
      }
      for (;;)
      {
        break;
        j = zza.g(paramParcel, m);
        continue;
        localClientAppContext = (ClientAppContext)zza.a(paramParcel, m, ClientAppContext.CREATOR);
        continue;
        i = zza.g(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new HandleClientLifecycleEventRequest(j, localClientAppContext, i);
  }
  
  public HandleClientLifecycleEventRequest[] a(int paramInt)
  {
    return new HandleClientLifecycleEventRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */