package com.google.android.gms.nearby.messages.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class HandleClientLifecycleEventRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<HandleClientLifecycleEventRequest> CREATOR = new zzc();
  public final int a;
  public final ClientAppContext b;
  public final int c;
  
  HandleClientLifecycleEventRequest(int paramInt1, ClientAppContext paramClientAppContext, int paramInt2)
  {
    this.a = paramInt1;
    this.b = paramClientAppContext;
    this.c = paramInt2;
  }
  
  public HandleClientLifecycleEventRequest(ClientAppContext paramClientAppContext, int paramInt)
  {
    this(1, paramClientAppContext, paramInt);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/HandleClientLifecycleEventRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */