package com.google.android.gms.nearby.messages.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class GetPermissionStatusRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<GetPermissionStatusRequest> CREATOR = new zzb();
  final int a;
  public final zze b;
  @Deprecated
  public final String c;
  public final ClientAppContext d;
  
  GetPermissionStatusRequest(int paramInt, IBinder paramIBinder, String paramString, ClientAppContext paramClientAppContext)
  {
    this.a = paramInt;
    this.b = zze.zza.a(paramIBinder);
    this.c = paramString;
    if (paramClientAppContext != null) {}
    for (;;)
    {
      this.d = paramClientAppContext;
      return;
      paramClientAppContext = new ClientAppContext(null, this.c);
    }
  }
  
  GetPermissionStatusRequest(IBinder paramIBinder, ClientAppContext paramClientAppContext)
  {
    this(1, paramIBinder, null, paramClientAppContext);
  }
  
  IBinder a()
  {
    return this.b.asBinder();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/GetPermissionStatusRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */