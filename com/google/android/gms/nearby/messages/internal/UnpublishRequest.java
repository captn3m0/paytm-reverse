package com.google.android.gms.nearby.messages.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class UnpublishRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<UnpublishRequest> CREATOR = new zzt();
  final int a;
  public final MessageWrapper b;
  public final zze c;
  @Deprecated
  public final String d;
  @Deprecated
  public final String e;
  @Deprecated
  public final boolean f;
  public final ClientAppContext g;
  
  UnpublishRequest(int paramInt, MessageWrapper paramMessageWrapper, IBinder paramIBinder, String paramString1, String paramString2, boolean paramBoolean, ClientAppContext paramClientAppContext)
  {
    this.a = paramInt;
    this.b = paramMessageWrapper;
    this.c = zze.zza.a(paramIBinder);
    this.d = paramString1;
    this.e = paramString2;
    this.f = paramBoolean;
    if (paramClientAppContext != null) {}
    for (;;)
    {
      this.g = paramClientAppContext;
      return;
      paramClientAppContext = new ClientAppContext(this.e, this.d, this.f);
    }
  }
  
  UnpublishRequest(MessageWrapper paramMessageWrapper, IBinder paramIBinder, ClientAppContext paramClientAppContext)
  {
    this(1, paramMessageWrapper, paramIBinder, null, null, false, paramClientAppContext);
  }
  
  IBinder a()
  {
    return this.c.asBinder();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzt.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/UnpublishRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */