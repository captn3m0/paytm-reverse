package com.google.android.gms.nearby.messages.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zzb;

final class zzp
  extends zze.zza
{
  private final zza.zzb<Status> a;
  
  private zzp(zza.zzb<Status> paramzzb)
  {
    this.a = paramzzb;
  }
  
  static zzp a(zza.zzb<Status> paramzzb)
  {
    return new zzp(paramzzb);
  }
  
  public void a(Status paramStatus)
    throws RemoteException
  {
    this.a.a(paramStatus);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */