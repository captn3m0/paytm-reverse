package com.google.android.gms.nearby.messages.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.nearby.messages.Message;

public class zzl
  implements Parcelable.Creator<MessageWrapper>
{
  static void a(MessageWrapper paramMessageWrapper, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramMessageWrapper.b, paramInt, false);
    zzb.a(paramParcel, 1000, paramMessageWrapper.a);
    zzb.a(paramParcel, i);
  }
  
  public MessageWrapper a(Parcel paramParcel)
  {
    int j = zza.b(paramParcel);
    int i = 0;
    Message localMessage = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localMessage = (Message)zza.a(paramParcel, k, Message.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new MessageWrapper(i, localMessage);
  }
  
  public MessageWrapper[] a(int paramInt)
  {
    return new MessageWrapper[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */