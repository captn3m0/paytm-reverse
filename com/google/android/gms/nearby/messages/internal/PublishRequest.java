package com.google.android.gms.nearby.messages.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.nearby.messages.Strategy;

public final class PublishRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<PublishRequest> CREATOR = new zzq();
  final int a;
  public final MessageWrapper b;
  public final Strategy c;
  public final zze d;
  @Deprecated
  public final String e;
  @Deprecated
  public final String f;
  public final boolean g;
  public final zzg h;
  @Deprecated
  public final boolean i;
  public final ClientAppContext j;
  
  PublishRequest(int paramInt, MessageWrapper paramMessageWrapper, Strategy paramStrategy, IBinder paramIBinder1, String paramString1, String paramString2, boolean paramBoolean1, IBinder paramIBinder2, boolean paramBoolean2, ClientAppContext paramClientAppContext)
  {
    this.a = paramInt;
    this.b = paramMessageWrapper;
    this.c = paramStrategy;
    this.d = zze.zza.a(paramIBinder1);
    this.e = paramString1;
    this.f = paramString2;
    this.g = paramBoolean1;
    if (paramIBinder2 == null)
    {
      paramMessageWrapper = null;
      this.h = paramMessageWrapper;
      this.i = paramBoolean2;
      if (paramClientAppContext == null) {
        break label85;
      }
    }
    for (;;)
    {
      this.j = paramClientAppContext;
      return;
      paramMessageWrapper = zzg.zza.a(paramIBinder2);
      break;
      label85:
      paramClientAppContext = new ClientAppContext(this.f, this.e, this.i);
    }
  }
  
  PublishRequest(MessageWrapper paramMessageWrapper, Strategy paramStrategy, IBinder paramIBinder1, boolean paramBoolean, IBinder paramIBinder2, ClientAppContext paramClientAppContext)
  {
    this(2, paramMessageWrapper, paramStrategy, paramIBinder1, null, null, paramBoolean, paramIBinder2, false, paramClientAppContext);
  }
  
  IBinder a()
  {
    return this.d.asBinder();
  }
  
  IBinder b()
  {
    if (this.h == null) {
      return null;
    }
    return this.h.asBinder();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzq.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/internal/PublishRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */