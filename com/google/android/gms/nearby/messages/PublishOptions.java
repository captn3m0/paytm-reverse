package com.google.android.gms.nearby.messages;

import android.support.annotation.Nullable;

public final class PublishOptions
{
  public static final PublishOptions a = new Builder().a();
  private final Strategy b;
  @Nullable
  private final PublishCallback c;
  
  private PublishOptions(Strategy paramStrategy, @Nullable PublishCallback paramPublishCallback)
  {
    this.b = paramStrategy;
    this.c = paramPublishCallback;
  }
  
  public Strategy a()
  {
    return this.b;
  }
  
  @Nullable
  public PublishCallback b()
  {
    return this.c;
  }
  
  public static class Builder
  {
    private Strategy a = Strategy.a;
    @Nullable
    private PublishCallback b;
    
    public PublishOptions a()
    {
      return new PublishOptions(this.a, this.b, null);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/PublishOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */