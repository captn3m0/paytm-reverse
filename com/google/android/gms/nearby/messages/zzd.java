package com.google.android.gms.nearby.messages;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzd
  implements Parcelable.Creator<Strategy>
{
  static void a(Strategy paramStrategy, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramStrategy.e);
    zzb.a(paramParcel, 1000, paramStrategy.d);
    zzb.a(paramParcel, 2, paramStrategy.f);
    zzb.a(paramParcel, 3, paramStrategy.g);
    zzb.a(paramParcel, 4, paramStrategy.h);
    zzb.a(paramParcel, 5, paramStrategy.a());
    zzb.a(paramParcel, 6, paramStrategy.b());
    zzb.a(paramParcel, paramInt);
  }
  
  public Strategy a(Parcel paramParcel)
  {
    int i = 0;
    int i2 = zza.b(paramParcel);
    int j = 0;
    boolean bool = false;
    int k = 0;
    int m = 0;
    int n = 0;
    int i1 = 0;
    while (paramParcel.dataPosition() < i2)
    {
      int i3 = zza.a(paramParcel);
      switch (zza.a(i3))
      {
      default: 
        zza.b(paramParcel, i3);
        break;
      case 1: 
        n = zza.g(paramParcel, i3);
        break;
      case 1000: 
        i1 = zza.g(paramParcel, i3);
        break;
      case 2: 
        m = zza.g(paramParcel, i3);
        break;
      case 3: 
        k = zza.g(paramParcel, i3);
        break;
      case 4: 
        bool = zza.c(paramParcel, i3);
        break;
      case 5: 
        j = zza.g(paramParcel, i3);
        break;
      case 6: 
        i = zza.g(paramParcel, i3);
      }
    }
    if (paramParcel.dataPosition() != i2) {
      throw new zza.zza("Overread allowed size end=" + i2, paramParcel);
    }
    return new Strategy(i1, n, m, k, bool, j, i);
  }
  
  public Strategy[] a(int paramInt)
  {
    return new Strategy[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */