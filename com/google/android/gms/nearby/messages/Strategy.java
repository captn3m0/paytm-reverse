package com.google.android.gms.nearby.messages;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;
import java.util.ArrayList;
import java.util.List;

public class Strategy
  implements SafeParcelable
{
  public static final Parcelable.Creator<Strategy> CREATOR = new zzd();
  public static final Strategy a = new Builder().a();
  public static final Strategy b = new Builder().a(2).b(Integer.MAX_VALUE).a();
  @Deprecated
  public static final Strategy c = b;
  final int d;
  @Deprecated
  final int e;
  final int f;
  final int g;
  @Deprecated
  final boolean h;
  final int i;
  final int j;
  
  Strategy(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean, int paramInt5, int paramInt6)
  {
    this.d = paramInt1;
    this.e = paramInt2;
    if (paramInt2 == 0) {
      this.j = paramInt6;
    }
    for (;;)
    {
      this.g = paramInt4;
      this.h = paramBoolean;
      if (!paramBoolean) {
        break;
      }
      this.i = 2;
      this.f = Integer.MAX_VALUE;
      return;
      switch (paramInt2)
      {
      default: 
        this.j = 3;
        break;
      case 2: 
        this.j = 1;
        break;
      case 3: 
        this.j = 2;
      }
    }
    this.f = paramInt3;
    switch (paramInt5)
    {
    case 2: 
    case 3: 
    case 4: 
    case 5: 
    default: 
      this.i = paramInt5;
      return;
    }
    this.i = -1;
  }
  
  private static String a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return "UNKNOWN:" + paramInt;
    case 0: 
      return "DEFAULT";
    }
    return "EARSHOT";
  }
  
  private static String b(int paramInt)
  {
    if (paramInt == -1) {
      return "DEFAULT";
    }
    ArrayList localArrayList = new ArrayList();
    if ((paramInt & 0x4) > 0) {
      localArrayList.add("AUDIO");
    }
    if ((paramInt & 0x2) > 0) {
      localArrayList.add("BLE");
    }
    if (localArrayList.isEmpty()) {
      return "UNKNOWN:" + paramInt;
    }
    return localArrayList.toString();
  }
  
  private static String c(int paramInt)
  {
    if (paramInt == 3) {
      return "DEFAULT";
    }
    ArrayList localArrayList = new ArrayList();
    if ((paramInt & 0x1) > 0) {
      localArrayList.add("BROADCAST");
    }
    if ((paramInt & 0x2) > 0) {
      localArrayList.add("SCAN");
    }
    if (localArrayList.isEmpty()) {
      return "UNKNOWN:" + paramInt;
    }
    return localArrayList.toString();
  }
  
  public int a()
  {
    return this.i;
  }
  
  public int b()
  {
    return this.j;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof Strategy)) {
        return false;
      }
      paramObject = (Strategy)paramObject;
    } while ((this.d == ((Strategy)paramObject).d) && (this.j == ((Strategy)paramObject).j) && (this.f == ((Strategy)paramObject).f) && (this.g == ((Strategy)paramObject).g) && (this.i == ((Strategy)paramObject).i));
    return false;
  }
  
  public int hashCode()
  {
    return (((this.d * 31 + this.j) * 31 + this.f) * 31 + this.g) * 31 + this.i;
  }
  
  public String toString()
  {
    return "Strategy{ttlSeconds=" + this.f + ", distanceType=" + a(this.g) + ", discoveryMedium=" + b(this.i) + ", discoveryMode=" + c(this.j) + '}';
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd.a(this, paramParcel, paramInt);
  }
  
  public static class Builder
  {
    private int a = 3;
    private int b = 300;
    private int c = 0;
    private int d = -1;
    
    public Builder a(int paramInt)
    {
      this.d = paramInt;
      return this;
    }
    
    public Strategy a()
    {
      if ((this.d == 2) && (this.c == 1)) {
        throw new IllegalStateException("Cannot set EARSHOT with BLE only mode.");
      }
      return new Strategy(2, 0, this.b, this.c, false, this.d, this.a);
    }
    
    public Builder b(int paramInt)
    {
      if ((paramInt == Integer.MAX_VALUE) || ((paramInt > 0) && (paramInt <= 86400))) {}
      for (boolean bool = true;; bool = false)
      {
        zzx.b(bool, "mTtlSeconds(%d) must either be TTL_SECONDS_INFINITE, or it must be between 1 and TTL_SECONDS_MAX(%d) inclusive", new Object[] { Integer.valueOf(paramInt), Integer.valueOf(86400) });
        this.b = paramInt;
        return this;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/Strategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */