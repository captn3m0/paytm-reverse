package com.google.android.gms.nearby.messages;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.nearby.messages.devices.NearbyDevice;

public class zza
  implements Parcelable.Creator<Message>
{
  static void a(Message paramMessage, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramMessage.c(), false);
    zzb.a(paramParcel, 1000, paramMessage.a);
    zzb.a(paramParcel, 2, paramMessage.a(), false);
    zzb.a(paramParcel, 3, paramMessage.b(), false);
    zzb.a(paramParcel, 4, paramMessage.d(), paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public Message a(Parcel paramParcel)
  {
    NearbyDevice[] arrayOfNearbyDevice = null;
    int j = com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel);
    int i = 0;
    String str1 = null;
    String str2 = null;
    byte[] arrayOfByte = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = com.google.android.gms.common.internal.safeparcel.zza.a(paramParcel);
      switch (com.google.android.gms.common.internal.safeparcel.zza.a(k))
      {
      default: 
        com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel, k);
        break;
      case 1: 
        arrayOfByte = com.google.android.gms.common.internal.safeparcel.zza.s(paramParcel, k);
        break;
      case 1000: 
        i = com.google.android.gms.common.internal.safeparcel.zza.g(paramParcel, k);
        break;
      case 2: 
        str1 = com.google.android.gms.common.internal.safeparcel.zza.p(paramParcel, k);
        break;
      case 3: 
        str2 = com.google.android.gms.common.internal.safeparcel.zza.p(paramParcel, k);
        break;
      case 4: 
        arrayOfNearbyDevice = (NearbyDevice[])com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel, k, NearbyDevice.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new Message(i, arrayOfByte, str2, str1, arrayOfNearbyDevice);
  }
  
  public Message[] a(int paramInt)
  {
    return new Message[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/nearby/messages/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */