package com.google.android.gms.signin.internal;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.internal.zzq;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.ResolveAccountRequest;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.common.internal.zzj.zzf;
import com.google.android.gms.common.internal.zzp;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzrn;
import com.google.android.gms.internal.zzro;

public class zzh
  extends zzj<zze>
  implements zzrn
{
  private final boolean a;
  private final zzf e;
  private final Bundle f;
  private Integer g;
  
  public zzh(Context paramContext, Looper paramLooper, boolean paramBoolean, zzf paramzzf, Bundle paramBundle, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramContext, paramLooper, 44, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    this.a = paramBoolean;
    this.e = paramzzf;
    this.f = paramBundle;
    this.g = paramzzf.l();
  }
  
  public zzh(Context paramContext, Looper paramLooper, boolean paramBoolean, zzf paramzzf, zzro paramzzro, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    this(paramContext, paramLooper, paramBoolean, paramzzf, a(paramzzf), paramConnectionCallbacks, paramOnConnectionFailedListener);
  }
  
  public static Bundle a(zzf paramzzf)
  {
    zzro localzzro = paramzzf.k();
    Integer localInteger = paramzzf.l();
    Bundle localBundle = new Bundle();
    localBundle.putParcelable("com.google.android.gms.signin.internal.clientRequestedAccount", paramzzf.b());
    if (localInteger != null) {
      localBundle.putInt("com.google.android.gms.common.internal.ClientSettings.sessionId", localInteger.intValue());
    }
    if (localzzro != null)
    {
      localBundle.putBoolean("com.google.android.gms.signin.internal.offlineAccessRequested", localzzro.a());
      localBundle.putBoolean("com.google.android.gms.signin.internal.idTokenRequested", localzzro.b());
      localBundle.putString("com.google.android.gms.signin.internal.serverClientId", localzzro.c());
      localBundle.putBoolean("com.google.android.gms.signin.internal.usePromptModeForAuthCode", true);
      localBundle.putBoolean("com.google.android.gms.signin.internal.forceCodeForRefreshToken", localzzro.d());
      localBundle.putString("com.google.android.gms.signin.internal.hostedDomain", localzzro.e());
      localBundle.putBoolean("com.google.android.gms.signin.internal.waitForAccessTokenRefresh", localzzro.f());
    }
    return localBundle;
  }
  
  private ResolveAccountRequest i()
  {
    Account localAccount = this.e.c();
    GoogleSignInAccount localGoogleSignInAccount = null;
    if ("<<default account>>".equals(localAccount.name)) {
      localGoogleSignInAccount = zzq.a(q()).a();
    }
    return new ResolveAccountRequest(localAccount, this.g.intValue(), localGoogleSignInAccount);
  }
  
  protected zze a(IBinder paramIBinder)
  {
    return zze.zza.a(paramIBinder);
  }
  
  protected String a()
  {
    return "com.google.android.gms.signin.service.START";
  }
  
  public void a(zzp paramzzp, boolean paramBoolean)
  {
    try
    {
      ((zze)v()).a(paramzzp, this.g.intValue(), paramBoolean);
      return;
    }
    catch (RemoteException paramzzp)
    {
      Log.w("SignInClientImpl", "Remote service probably died when saveDefaultAccount is called");
    }
  }
  
  public void a(zzd paramzzd)
  {
    zzx.a(paramzzd, "Expecting a valid ISignInCallbacks");
    try
    {
      ResolveAccountRequest localResolveAccountRequest = i();
      ((zze)v()).a(new SignInRequest(localResolveAccountRequest), paramzzd);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      Log.w("SignInClientImpl", "Remote service probably died when signIn is called");
      try
      {
        paramzzd.a(new SignInResponse(8));
        return;
      }
      catch (RemoteException paramzzd)
      {
        Log.wtf("SignInClientImpl", "ISignInCallbacks#onSignInComplete should be executed from the same process, unexpected RemoteException.", localRemoteException);
      }
    }
  }
  
  protected Bundle a_()
  {
    String str = this.e.h();
    if (!q().getPackageName().equals(str)) {
      this.f.putString("com.google.android.gms.signin.internal.realClientPackageName", this.e.h());
    }
    return this.f;
  }
  
  protected String b()
  {
    return "com.google.android.gms.signin.internal.ISignInService";
  }
  
  public void g()
  {
    try
    {
      ((zze)v()).a(this.g.intValue());
      return;
    }
    catch (RemoteException localRemoteException)
    {
      Log.w("SignInClientImpl", "Remote service probably died when clearAccountFromSessionStore is called");
    }
  }
  
  public void h()
  {
    a(new zzj.zzf(this));
  }
  
  public boolean l()
  {
    return this.a;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/signin/internal/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */