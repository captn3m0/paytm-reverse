package com.google.android.gms.clearcut;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzv;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.internal.zzsz.zzd;
import com.google.android.gms.playlog.internal.PlayLoggerContext;
import java.util.Arrays;

public class LogEventParcelable
  implements SafeParcelable
{
  public static final zzd CREATOR = new zzd();
  public final int a;
  public PlayLoggerContext b;
  public byte[] c;
  public int[] d;
  public final zzsz.zzd e;
  public final zzb.zzb f;
  public final zzb.zzb g;
  
  LogEventParcelable(int paramInt, PlayLoggerContext paramPlayLoggerContext, byte[] paramArrayOfByte, int[] paramArrayOfInt)
  {
    this.a = paramInt;
    this.b = paramPlayLoggerContext;
    this.c = paramArrayOfByte;
    this.d = paramArrayOfInt;
    this.e = null;
    this.f = null;
    this.g = null;
  }
  
  public LogEventParcelable(PlayLoggerContext paramPlayLoggerContext, zzsz.zzd paramzzd, zzb.zzb paramzzb1, zzb.zzb paramzzb2, int[] paramArrayOfInt)
  {
    this.a = 1;
    this.b = paramPlayLoggerContext;
    this.e = paramzzd;
    this.f = paramzzb1;
    this.g = paramzzb2;
    this.d = paramArrayOfInt;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof LogEventParcelable)) {
        break;
      }
      paramObject = (LogEventParcelable)paramObject;
    } while ((this.a == ((LogEventParcelable)paramObject).a) && (zzw.a(this.b, ((LogEventParcelable)paramObject).b)) && (Arrays.equals(this.c, ((LogEventParcelable)paramObject).c)) && (Arrays.equals(this.d, ((LogEventParcelable)paramObject).d)) && (zzw.a(this.e, ((LogEventParcelable)paramObject).e)) && (zzw.a(this.f, ((LogEventParcelable)paramObject).f)) && (zzw.a(this.g, ((LogEventParcelable)paramObject).g)));
    return false;
    return false;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Integer.valueOf(this.a), this.b, this.c, this.d, this.e, this.f, this.g });
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("LogEventParcelable[");
    localStringBuilder.append(this.a);
    localStringBuilder.append(", ");
    localStringBuilder.append(this.b);
    localStringBuilder.append(", ");
    if (this.c == null)
    {
      str = null;
      localStringBuilder.append(str);
      localStringBuilder.append(", ");
      if (this.d != null) {
        break label157;
      }
    }
    label157:
    for (String str = (String)null;; str = zzv.a(", ").a(Arrays.asList(new int[][] { this.d })))
    {
      localStringBuilder.append(str);
      localStringBuilder.append(", ");
      localStringBuilder.append(this.e);
      localStringBuilder.append(", ");
      localStringBuilder.append(this.f);
      localStringBuilder.append(", ");
      localStringBuilder.append(this.g);
      localStringBuilder.append("]");
      return localStringBuilder.toString();
      str = new String(this.c);
      break;
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/clearcut/LogEventParcelable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */