package com.google.android.gms.clearcut;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import java.util.concurrent.TimeUnit;

public abstract interface zzc
{
  public abstract PendingResult<Status> a(GoogleApiClient paramGoogleApiClient, LogEventParcelable paramLogEventParcelable);
  
  public abstract boolean a(GoogleApiClient paramGoogleApiClient, long paramLong, TimeUnit paramTimeUnit);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/clearcut/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */