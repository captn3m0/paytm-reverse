package com.google.android.gms.clearcut;

import android.content.Context;
import android.content.SharedPreferences;

public class zza
{
  public static final zza a = new zza();
  private static int b = -1;
  
  public int a(Context paramContext)
  {
    if (b < 0) {
      b = paramContext.getSharedPreferences("bootCount", 0).getInt("bootCount", 1);
    }
    return b;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/clearcut/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */