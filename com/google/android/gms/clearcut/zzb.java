package com.google.android.gms.clearcut;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Looper;
import android.util.Log;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzlv;
import com.google.android.gms.internal.zzlw;
import com.google.android.gms.internal.zzmq;
import com.google.android.gms.internal.zzmt;
import com.google.android.gms.internal.zzsz.zzd;
import com.google.android.gms.playlog.internal.PlayLoggerContext;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public final class zzb
{
  public static final Api.zzc<zzlw> a = new Api.zzc();
  public static final Api.zza<zzlw, Api.ApiOptions.NoOptions> b = new Api.zza()
  {
    public zzlw a(Context paramAnonymousContext, Looper paramAnonymousLooper, zzf paramAnonymouszzf, Api.ApiOptions.NoOptions paramAnonymousNoOptions, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      return new zzlw(paramAnonymousContext, paramAnonymousLooper, paramAnonymouszzf, paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
    }
  };
  public static final Api<Api.ApiOptions.NoOptions> c = new Api("ClearcutLogger.API", b, a);
  public static final zzc d = new zzlv();
  private final Context e;
  private final String f;
  private final int g;
  private String h;
  private int i = -1;
  private String j;
  private String k;
  private final boolean l;
  private int m = 0;
  private final zzc n;
  private final zzmq o;
  private final zza p;
  private zzc q;
  
  public zzb(Context paramContext, int paramInt, String paramString1, String paramString2, String paramString3, boolean paramBoolean, zzc paramzzc, zzmq paramzzmq, zzc paramzzc1, zza paramzza)
  {
    Context localContext = paramContext.getApplicationContext();
    if (localContext != null)
    {
      this.e = localContext;
      this.f = paramContext.getPackageName();
      this.g = a(paramContext);
      this.i = paramInt;
      this.h = paramString1;
      this.j = paramString2;
      this.k = paramString3;
      this.l = paramBoolean;
      this.n = paramzzc;
      this.o = paramzzmq;
      if (paramzzc1 == null) {
        break label141;
      }
      label93:
      this.q = paramzzc1;
      this.p = paramzza;
      this.m = 0;
      if (this.l) {
        if (this.j != null) {
          break label153;
        }
      }
    }
    label141:
    label153:
    for (paramBoolean = true;; paramBoolean = false)
    {
      zzx.b(paramBoolean, "can't be anonymous with an upload account");
      return;
      localContext = paramContext;
      break;
      paramzzc1 = new zzc();
      break label93;
    }
  }
  
  @Deprecated
  public zzb(Context paramContext, String paramString1, String paramString2, String paramString3)
  {
    this(paramContext, -1, paramString1, paramString2, paramString3, false, d, zzmt.d(), null, zza.a);
  }
  
  private int a(Context paramContext)
  {
    try
    {
      int i1 = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 0).versionCode;
      return i1;
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      Log.wtf("ClearcutLogger", "This can't happen.");
    }
    return 0;
  }
  
  private static int[] b(ArrayList<Integer> paramArrayList)
  {
    if (paramArrayList == null) {
      return null;
    }
    int[] arrayOfInt = new int[paramArrayList.size()];
    paramArrayList = paramArrayList.iterator();
    int i1 = 0;
    while (paramArrayList.hasNext())
    {
      arrayOfInt[i1] = ((Integer)paramArrayList.next()).intValue();
      i1 += 1;
    }
    return arrayOfInt;
  }
  
  public zza a(byte[] paramArrayOfByte)
  {
    return new zza(paramArrayOfByte, null);
  }
  
  public boolean a(GoogleApiClient paramGoogleApiClient, long paramLong, TimeUnit paramTimeUnit)
  {
    return this.n.a(paramGoogleApiClient, paramLong, paramTimeUnit);
  }
  
  public class zza
  {
    private int b = zzb.a(zzb.this);
    private String c = zzb.b(zzb.this);
    private String d = zzb.c(zzb.this);
    private String e = zzb.d(zzb.this);
    private int f = zzb.e(zzb.this);
    private final zzb.zzb g;
    private zzb.zzb h;
    private ArrayList<Integer> i = null;
    private final zzsz.zzd j = new zzsz.zzd();
    private boolean k = false;
    
    private zza(byte[] paramArrayOfByte)
    {
      this(paramArrayOfByte, null);
    }
    
    private zza(byte[] paramArrayOfByte, zzb.zzb paramzzb)
    {
      this.j.a = zzb.f(zzb.this).a();
      this.j.b = zzb.f(zzb.this).b();
      this.j.u = zzb.h(zzb.this).a(zzb.g(zzb.this));
      this.j.o = zzb.i(zzb.this).a(this.j.a);
      if (paramArrayOfByte != null) {
        this.j.j = paramArrayOfByte;
      }
      this.g = paramzzb;
    }
    
    public LogEventParcelable a()
    {
      return new LogEventParcelable(new PlayLoggerContext(zzb.k(zzb.this), zzb.l(zzb.this), this.b, this.c, this.d, this.e, zzb.j(zzb.this), this.f), this.j, this.g, this.h, zzb.a(this.i));
    }
    
    public zza a(int paramInt)
    {
      this.j.e = paramInt;
      return this;
    }
    
    public PendingResult<Status> a(GoogleApiClient paramGoogleApiClient)
    {
      if (this.k) {
        throw new IllegalStateException("do not reuse LogEventBuilder");
      }
      this.k = true;
      return zzb.m(zzb.this).a(paramGoogleApiClient, a());
    }
    
    public zza b(int paramInt)
    {
      this.j.f = paramInt;
      return this;
    }
  }
  
  public static abstract interface zzb
  {
    public abstract byte[] a();
  }
  
  public static class zzc
  {
    public long a(long paramLong)
    {
      return TimeZone.getDefault().getOffset(paramLong) / 1000;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/clearcut/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */