package com.google.android.gms.clearcut;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.playlog.internal.PlayLoggerContext;

public class zzd
  implements Parcelable.Creator<LogEventParcelable>
{
  static void a(LogEventParcelable paramLogEventParcelable, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramLogEventParcelable.a);
    zzb.a(paramParcel, 2, paramLogEventParcelable.b, paramInt, false);
    zzb.a(paramParcel, 3, paramLogEventParcelable.c, false);
    zzb.a(paramParcel, 4, paramLogEventParcelable.d, false);
    zzb.a(paramParcel, i);
  }
  
  public LogEventParcelable a(Parcel paramParcel)
  {
    int[] arrayOfInt = null;
    int j = zza.b(paramParcel);
    int i = 0;
    Object localObject2 = null;
    Object localObject1 = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      Object localObject3;
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject3;
      }
      for (;;)
      {
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject3;
        break;
        i = zza.g(paramParcel, k);
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject3;
        continue;
        localObject3 = (PlayLoggerContext)zza.a(paramParcel, k, PlayLoggerContext.CREATOR);
        localObject1 = localObject2;
        localObject2 = localObject3;
        continue;
        localObject3 = zza.s(paramParcel, k);
        localObject2 = localObject1;
        localObject1 = localObject3;
        continue;
        arrayOfInt = zza.v(paramParcel, k);
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject3;
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new LogEventParcelable(i, (PlayLoggerContext)localObject1, (byte[])localObject2, arrayOfInt);
  }
  
  public LogEventParcelable[] a(int paramInt)
  {
    return new LogEventParcelable[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/clearcut/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */