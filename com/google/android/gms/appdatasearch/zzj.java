package com.google.android.gms.appdatasearch;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzj
  implements Parcelable.Creator<UsageInfo>
{
  static void a(UsageInfo paramUsageInfo, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramUsageInfo.b, paramInt, false);
    zzb.a(paramParcel, 1000, paramUsageInfo.a);
    zzb.a(paramParcel, 2, paramUsageInfo.c);
    zzb.a(paramParcel, 3, paramUsageInfo.d);
    zzb.a(paramParcel, 4, paramUsageInfo.e, false);
    zzb.a(paramParcel, 5, paramUsageInfo.f, paramInt, false);
    zzb.a(paramParcel, 6, paramUsageInfo.g);
    zzb.a(paramParcel, 7, paramUsageInfo.h);
    zzb.a(paramParcel, 8, paramUsageInfo.i);
    zzb.a(paramParcel, i);
  }
  
  public UsageInfo a(Parcel paramParcel)
  {
    DocumentContents localDocumentContents = null;
    int i = 0;
    int n = zza.b(paramParcel);
    long l = 0L;
    int j = -1;
    boolean bool = false;
    String str = null;
    int k = 0;
    DocumentId localDocumentId = null;
    int m = 0;
    while (paramParcel.dataPosition() < n)
    {
      int i1 = zza.a(paramParcel);
      switch (zza.a(i1))
      {
      default: 
        zza.b(paramParcel, i1);
        break;
      case 1: 
        localDocumentId = (DocumentId)zza.a(paramParcel, i1, DocumentId.CREATOR);
        break;
      case 1000: 
        m = zza.g(paramParcel, i1);
        break;
      case 2: 
        l = zza.i(paramParcel, i1);
        break;
      case 3: 
        k = zza.g(paramParcel, i1);
        break;
      case 4: 
        str = zza.p(paramParcel, i1);
        break;
      case 5: 
        localDocumentContents = (DocumentContents)zza.a(paramParcel, i1, DocumentContents.CREATOR);
        break;
      case 6: 
        bool = zza.c(paramParcel, i1);
        break;
      case 7: 
        j = zza.g(paramParcel, i1);
        break;
      case 8: 
        i = zza.g(paramParcel, i1);
      }
    }
    if (paramParcel.dataPosition() != n) {
      throw new zza.zza("Overread allowed size end=" + n, paramParcel);
    }
    return new UsageInfo(m, localDocumentId, l, k, str, localDocumentContents, bool, j, i);
  }
  
  public UsageInfo[] a(int paramInt)
  {
    return new UsageInfo[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/appdatasearch/zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */