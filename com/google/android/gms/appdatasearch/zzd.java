package com.google.android.gms.appdatasearch;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzd
  implements Parcelable.Creator<DocumentSection>
{
  static void a(DocumentSection paramDocumentSection, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramDocumentSection.c, false);
    zzb.a(paramParcel, 1000, paramDocumentSection.b);
    zzb.a(paramParcel, 3, paramDocumentSection.d, paramInt, false);
    zzb.a(paramParcel, 4, paramDocumentSection.e);
    zzb.a(paramParcel, 5, paramDocumentSection.f, false);
    zzb.a(paramParcel, i);
  }
  
  public DocumentSection a(Parcel paramParcel)
  {
    byte[] arrayOfByte = null;
    int k = zza.b(paramParcel);
    int j = 0;
    int i = -1;
    RegisterSectionInfo localRegisterSectionInfo = null;
    String str = null;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        str = zza.p(paramParcel, m);
        break;
      case 1000: 
        j = zza.g(paramParcel, m);
        break;
      case 3: 
        localRegisterSectionInfo = (RegisterSectionInfo)zza.a(paramParcel, m, RegisterSectionInfo.CREATOR);
        break;
      case 4: 
        i = zza.g(paramParcel, m);
        break;
      case 5: 
        arrayOfByte = zza.s(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new DocumentSection(j, str, localRegisterSectionInfo, i, arrayOfByte);
  }
  
  public DocumentSection[] a(int paramInt)
  {
    return new DocumentSection[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/appdatasearch/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */