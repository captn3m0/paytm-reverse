package com.google.android.gms.appdatasearch;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzc
  implements Parcelable.Creator<DocumentId>
{
  static void a(DocumentId paramDocumentId, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramDocumentId.b, false);
    zzb.a(paramParcel, 1000, paramDocumentId.a);
    zzb.a(paramParcel, 2, paramDocumentId.c, false);
    zzb.a(paramParcel, 3, paramDocumentId.d, false);
    zzb.a(paramParcel, paramInt);
  }
  
  public DocumentId a(Parcel paramParcel)
  {
    String str3 = null;
    int j = zza.b(paramParcel);
    int i = 0;
    String str2 = null;
    String str1 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        str1 = zza.p(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str2 = zza.p(paramParcel, k);
        break;
      case 3: 
        str3 = zza.p(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new DocumentId(i, str1, str2, str3);
  }
  
  public DocumentId[] a(int paramInt)
  {
    return new DocumentId[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/appdatasearch/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */