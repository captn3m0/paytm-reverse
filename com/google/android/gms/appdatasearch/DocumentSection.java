package com.google.android.gms.appdatasearch;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;

public class DocumentSection
  implements SafeParcelable
{
  public static final zzd CREATOR = new zzd();
  public static final int a = Integer.parseInt("-1");
  private static final RegisterSectionInfo g = new RegisterSectionInfo.zza("SsbContext").a(true).a("blob").a();
  final int b;
  public final String c;
  final RegisterSectionInfo d;
  public final int e;
  public final byte[] f;
  
  DocumentSection(int paramInt1, String paramString, RegisterSectionInfo paramRegisterSectionInfo, int paramInt2, byte[] paramArrayOfByte)
  {
    if ((paramInt2 == a) || (zzh.a(paramInt2) != null)) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.b(bool, "Invalid section type " + paramInt2);
      this.b = paramInt1;
      this.c = paramString;
      this.d = paramRegisterSectionInfo;
      this.e = paramInt2;
      this.f = paramArrayOfByte;
      paramString = a();
      if (paramString == null) {
        break;
      }
      throw new IllegalArgumentException(paramString);
    }
  }
  
  public DocumentSection(String paramString, RegisterSectionInfo paramRegisterSectionInfo)
  {
    this(1, paramString, paramRegisterSectionInfo, a, null);
  }
  
  public DocumentSection(String paramString1, RegisterSectionInfo paramRegisterSectionInfo, String paramString2)
  {
    this(1, paramString1, paramRegisterSectionInfo, zzh.a(paramString2), null);
  }
  
  public DocumentSection(byte[] paramArrayOfByte, RegisterSectionInfo paramRegisterSectionInfo)
  {
    this(1, null, paramRegisterSectionInfo, a, paramArrayOfByte);
  }
  
  public static DocumentSection a(byte[] paramArrayOfByte)
  {
    return new DocumentSection(paramArrayOfByte, g);
  }
  
  public String a()
  {
    if ((this.e != a) && (zzh.a(this.e) == null)) {
      return "Invalid section type " + this.e;
    }
    if ((this.c != null) && (this.f != null)) {
      return "Both content and blobContent set";
    }
    return null;
  }
  
  public int describeContents()
  {
    zzd localzzd = CREATOR;
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd localzzd = CREATOR;
    zzd.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/appdatasearch/DocumentSection.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */