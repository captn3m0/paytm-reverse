package com.google.android.gms.appdatasearch;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzf
  implements Parcelable.Creator<GetRecentContextCall.Request>
{
  static void a(GetRecentContextCall.Request paramRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramRequest.b, paramInt, false);
    zzb.a(paramParcel, 1000, paramRequest.a);
    zzb.a(paramParcel, 2, paramRequest.c);
    zzb.a(paramParcel, 3, paramRequest.d);
    zzb.a(paramParcel, 4, paramRequest.e);
    zzb.a(paramParcel, 5, paramRequest.f, false);
    zzb.a(paramParcel, i);
  }
  
  public GetRecentContextCall.Request a(Parcel paramParcel)
  {
    String str = null;
    boolean bool1 = false;
    int j = zza.b(paramParcel);
    boolean bool2 = false;
    boolean bool3 = false;
    Account localAccount = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localAccount = (Account)zza.a(paramParcel, k, Account.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        bool3 = zza.c(paramParcel, k);
        break;
      case 3: 
        bool2 = zza.c(paramParcel, k);
        break;
      case 4: 
        bool1 = zza.c(paramParcel, k);
        break;
      case 5: 
        str = zza.p(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new GetRecentContextCall.Request(i, localAccount, bool3, bool2, bool1, str);
  }
  
  public GetRecentContextCall.Request[] a(int paramInt)
  {
    return new GetRecentContextCall.Request[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/appdatasearch/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */