package com.google.android.gms.appdatasearch;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.appindexing.AppIndexApi.AppIndexingLink;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.zzpm.zza;
import com.google.android.gms.internal.zzpm.zza.zza;
import com.google.android.gms.internal.zzsu;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.zip.CRC32;

public class UsageInfo
  implements SafeParcelable
{
  public static final zzj CREATOR = new zzj();
  final int a;
  final DocumentId b;
  final long c;
  int d;
  public final String e;
  final DocumentContents f;
  final boolean g;
  int h;
  int i;
  
  UsageInfo(int paramInt1, DocumentId paramDocumentId, long paramLong, int paramInt2, String paramString, DocumentContents paramDocumentContents, boolean paramBoolean, int paramInt3, int paramInt4)
  {
    this.a = paramInt1;
    this.b = paramDocumentId;
    this.c = paramLong;
    this.d = paramInt2;
    this.e = paramString;
    this.f = paramDocumentContents;
    this.g = paramBoolean;
    this.h = paramInt3;
    this.i = paramInt4;
  }
  
  private UsageInfo(DocumentId paramDocumentId, long paramLong, int paramInt1, String paramString, DocumentContents paramDocumentContents, boolean paramBoolean, int paramInt2, int paramInt3)
  {
    this(1, paramDocumentId, paramLong, paramInt1, paramString, paramDocumentContents, paramBoolean, paramInt2, paramInt3);
  }
  
  public static DocumentContents.zza a(Intent paramIntent, String paramString1, Uri paramUri, String paramString2, List<AppIndexApi.AppIndexingLink> paramList)
  {
    DocumentContents.zza localzza = new DocumentContents.zza();
    localzza.a(a(paramString1));
    if (paramUri != null) {
      localzza.a(a(paramUri));
    }
    if (paramList != null) {
      localzza.a(a(paramList));
    }
    paramString1 = paramIntent.getAction();
    if (paramString1 != null) {
      localzza.a(b("intent_action", paramString1));
    }
    paramString1 = paramIntent.getDataString();
    if (paramString1 != null) {
      localzza.a(b("intent_data", paramString1));
    }
    paramString1 = paramIntent.getComponent();
    if (paramString1 != null) {
      localzza.a(b("intent_activity", paramString1.getClassName()));
    }
    paramIntent = paramIntent.getExtras();
    if (paramIntent != null)
    {
      paramIntent = paramIntent.getString("intent_extra_data_key");
      if (paramIntent != null) {
        localzza.a(b("intent_extra_data", paramIntent));
      }
    }
    return localzza.a(paramString2).a(true);
  }
  
  public static DocumentId a(String paramString, Intent paramIntent)
  {
    return a(paramString, a(paramIntent));
  }
  
  private static DocumentId a(String paramString1, String paramString2)
  {
    return new DocumentId(paramString1, "", paramString2);
  }
  
  private static DocumentSection a(Uri paramUri)
  {
    return new DocumentSection(paramUri.toString(), new RegisterSectionInfo.zza("web_url").a(4).a(true).b("url").a());
  }
  
  private static DocumentSection a(String paramString)
  {
    return new DocumentSection(paramString, new RegisterSectionInfo.zza("title").a(1).b(true).b("name").a(), "text1");
  }
  
  private static DocumentSection a(List<AppIndexApi.AppIndexingLink> paramList)
  {
    zzpm.zza localzza = new zzpm.zza();
    zzpm.zza.zza[] arrayOfzza = new zzpm.zza.zza[paramList.size()];
    int j = 0;
    while (j < arrayOfzza.length)
    {
      arrayOfzza[j] = new zzpm.zza.zza();
      AppIndexApi.AppIndexingLink localAppIndexingLink = (AppIndexApi.AppIndexingLink)paramList.get(j);
      arrayOfzza[j].a = localAppIndexingLink.a.toString();
      arrayOfzza[j].c = localAppIndexingLink.c;
      if (localAppIndexingLink.b != null) {
        arrayOfzza[j].b = localAppIndexingLink.b.toString();
      }
      j += 1;
    }
    localzza.a = arrayOfzza;
    return new DocumentSection(zzsu.a(localzza), new RegisterSectionInfo.zza("outlinks").a(true).b(".private:outLinks").a("blob").a());
  }
  
  private static String a(Intent paramIntent)
  {
    paramIntent = paramIntent.toUri(1);
    CRC32 localCRC32 = new CRC32();
    try
    {
      localCRC32.update(paramIntent.getBytes("UTF-8"));
      return Long.toHexString(localCRC32.getValue());
    }
    catch (UnsupportedEncodingException paramIntent)
    {
      throw new IllegalStateException(paramIntent);
    }
  }
  
  private static DocumentSection b(String paramString1, String paramString2)
  {
    return new DocumentSection(paramString2, new RegisterSectionInfo.zza(paramString1).a(true).a(), paramString1);
  }
  
  public int describeContents()
  {
    zzj localzzj = CREATOR;
    return 0;
  }
  
  public String toString()
  {
    return String.format("UsageInfo[documentId=%s, timestamp=%d, usageType=%d, status=%d]", new Object[] { this.b, Long.valueOf(this.c), Integer.valueOf(this.d), Integer.valueOf(this.i) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzj localzzj = CREATOR;
    zzj.a(this, paramParcel, paramInt);
  }
  
  public static final class zza
  {
    private DocumentId a;
    private long b = -1L;
    private int c = -1;
    private String d;
    private DocumentContents e;
    private boolean f = false;
    private int g = -1;
    private int h = 0;
    
    public zza a(int paramInt)
    {
      this.c = paramInt;
      return this;
    }
    
    public zza a(long paramLong)
    {
      this.b = paramLong;
      return this;
    }
    
    public zza a(DocumentContents paramDocumentContents)
    {
      this.e = paramDocumentContents;
      return this;
    }
    
    public zza a(DocumentId paramDocumentId)
    {
      this.a = paramDocumentId;
      return this;
    }
    
    public zza a(boolean paramBoolean)
    {
      this.f = paramBoolean;
      return this;
    }
    
    public UsageInfo a()
    {
      return new UsageInfo(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, null);
    }
    
    public zza b(int paramInt)
    {
      this.h = paramInt;
      return this;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/appdatasearch/UsageInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */