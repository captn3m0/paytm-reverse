package com.google.android.gms.appdatasearch;

import android.accounts.Account;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public class DocumentContents
  implements SafeParcelable
{
  public static final zzb CREATOR = new zzb();
  final int a;
  final DocumentSection[] b;
  public final String c;
  public final boolean d;
  public final Account e;
  
  DocumentContents(int paramInt, DocumentSection[] paramArrayOfDocumentSection, String paramString, boolean paramBoolean, Account paramAccount)
  {
    this.a = paramInt;
    this.b = paramArrayOfDocumentSection;
    this.c = paramString;
    this.d = paramBoolean;
    this.e = paramAccount;
  }
  
  DocumentContents(String paramString, boolean paramBoolean, Account paramAccount, DocumentSection... paramVarArgs)
  {
    this(1, paramVarArgs, paramString, paramBoolean, paramAccount);
    paramString = new BitSet(zzh.a());
    int i = 0;
    while (i < paramVarArgs.length)
    {
      int j = paramVarArgs[i].e;
      if (j != -1)
      {
        if (paramString.get(j)) {
          throw new IllegalArgumentException("Duplicate global search section type " + zzh.a(j));
        }
        paramString.set(j);
      }
      i += 1;
    }
  }
  
  public int describeContents()
  {
    zzb localzzb = CREATOR;
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb localzzb = CREATOR;
    zzb.a(this, paramParcel, paramInt);
  }
  
  public static class zza
  {
    private List<DocumentSection> a;
    private String b;
    private boolean c;
    private Account d;
    
    public zza a(Account paramAccount)
    {
      this.d = paramAccount;
      return this;
    }
    
    public zza a(DocumentSection paramDocumentSection)
    {
      if (this.a == null) {
        this.a = new ArrayList();
      }
      this.a.add(paramDocumentSection);
      return this;
    }
    
    public zza a(String paramString)
    {
      this.b = paramString;
      return this;
    }
    
    public zza a(boolean paramBoolean)
    {
      this.c = paramBoolean;
      return this;
    }
    
    public DocumentContents a()
    {
      String str = this.b;
      boolean bool = this.c;
      Account localAccount = this.d;
      if (this.a != null) {}
      for (DocumentSection[] arrayOfDocumentSection = (DocumentSection[])this.a.toArray(new DocumentSection[this.a.size()]);; arrayOfDocumentSection = null) {
        return new DocumentContents(str, bool, localAccount, arrayOfDocumentSection);
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/appdatasearch/DocumentContents.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */