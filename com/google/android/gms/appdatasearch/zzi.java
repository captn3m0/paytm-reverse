package com.google.android.gms.appdatasearch;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzi
  implements Parcelable.Creator<RegisterSectionInfo>
{
  static void a(RegisterSectionInfo paramRegisterSectionInfo, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramRegisterSectionInfo.b, false);
    zzb.a(paramParcel, 1000, paramRegisterSectionInfo.a);
    zzb.a(paramParcel, 2, paramRegisterSectionInfo.c, false);
    zzb.a(paramParcel, 3, paramRegisterSectionInfo.d);
    zzb.a(paramParcel, 4, paramRegisterSectionInfo.e);
    zzb.a(paramParcel, 5, paramRegisterSectionInfo.f);
    zzb.a(paramParcel, 6, paramRegisterSectionInfo.g, false);
    zzb.a(paramParcel, 7, paramRegisterSectionInfo.h, paramInt, false);
    zzb.a(paramParcel, 8, paramRegisterSectionInfo.i, false);
    zzb.a(paramParcel, 11, paramRegisterSectionInfo.j, false);
    zzb.a(paramParcel, i);
  }
  
  public RegisterSectionInfo a(Parcel paramParcel)
  {
    boolean bool1 = false;
    String str1 = null;
    int k = zza.b(paramParcel);
    int i = 1;
    int[] arrayOfInt = null;
    Feature[] arrayOfFeature = null;
    String str2 = null;
    boolean bool2 = false;
    String str3 = null;
    String str4 = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        str4 = zza.p(paramParcel, m);
        break;
      case 1000: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        str3 = zza.p(paramParcel, m);
        break;
      case 3: 
        bool2 = zza.c(paramParcel, m);
        break;
      case 4: 
        i = zza.g(paramParcel, m);
        break;
      case 5: 
        bool1 = zza.c(paramParcel, m);
        break;
      case 6: 
        str2 = zza.p(paramParcel, m);
        break;
      case 7: 
        arrayOfFeature = (Feature[])zza.b(paramParcel, m, Feature.CREATOR);
        break;
      case 8: 
        arrayOfInt = zza.v(paramParcel, m);
        break;
      case 11: 
        str1 = zza.p(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new RegisterSectionInfo(j, str4, str3, bool2, i, bool1, str2, arrayOfFeature, arrayOfInt, str1);
  }
  
  public RegisterSectionInfo[] a(int paramInt)
  {
    return new RegisterSectionInfo[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/appdatasearch/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */