package com.google.android.gms.appdatasearch;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class zzb
  implements Parcelable.Creator<DocumentContents>
{
  static void a(DocumentContents paramDocumentContents, Parcel paramParcel, int paramInt)
  {
    int i = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramDocumentContents.b, paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1000, paramDocumentContents.a);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramDocumentContents.c, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 3, paramDocumentContents.d);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 4, paramDocumentContents.e, paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, i);
  }
  
  public DocumentContents a(Parcel paramParcel)
  {
    boolean bool = false;
    Account localAccount = null;
    int j = zza.b(paramParcel);
    String str = null;
    DocumentSection[] arrayOfDocumentSection = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        arrayOfDocumentSection = (DocumentSection[])zza.b(paramParcel, k, DocumentSection.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str = zza.p(paramParcel, k);
        break;
      case 3: 
        bool = zza.c(paramParcel, k);
        break;
      case 4: 
        localAccount = (Account)zza.a(paramParcel, k, Account.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new DocumentContents(i, arrayOfDocumentSection, str, bool, localAccount);
  }
  
  public DocumentContents[] a(int paramInt)
  {
    return new DocumentContents[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/appdatasearch/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */