package com.google.android.gms.appdatasearch;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public class RegisterSectionInfo
  implements SafeParcelable
{
  public static final zzi CREATOR = new zzi();
  final int a;
  public final String b;
  public final String c;
  public final boolean d;
  public final int e;
  public final boolean f;
  public final String g;
  public final Feature[] h;
  final int[] i;
  public final String j;
  
  RegisterSectionInfo(int paramInt1, String paramString1, String paramString2, boolean paramBoolean1, int paramInt2, boolean paramBoolean2, String paramString3, Feature[] paramArrayOfFeature, int[] paramArrayOfInt, String paramString4)
  {
    this.a = paramInt1;
    this.b = paramString1;
    this.c = paramString2;
    this.d = paramBoolean1;
    this.e = paramInt2;
    this.f = paramBoolean2;
    this.g = paramString3;
    this.h = paramArrayOfFeature;
    this.i = paramArrayOfInt;
    this.j = paramString4;
  }
  
  RegisterSectionInfo(String paramString1, String paramString2, boolean paramBoolean1, int paramInt, boolean paramBoolean2, String paramString3, Feature[] paramArrayOfFeature, int[] paramArrayOfInt, String paramString4)
  {
    this(2, paramString1, paramString2, paramBoolean1, paramInt, paramBoolean2, paramString3, paramArrayOfFeature, paramArrayOfInt, paramString4);
  }
  
  public int describeContents()
  {
    zzi localzzi = CREATOR;
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzi localzzi = CREATOR;
    zzi.a(this, paramParcel, paramInt);
  }
  
  public static final class zza
  {
    private final String a;
    private String b;
    private boolean c;
    private int d;
    private boolean e;
    private String f;
    private final List<Feature> g;
    private BitSet h;
    private String i;
    
    public zza(String paramString)
    {
      this.a = paramString;
      this.d = 1;
      this.g = new ArrayList();
    }
    
    public zza a(int paramInt)
    {
      if (this.h == null) {
        this.h = new BitSet();
      }
      this.h.set(paramInt);
      return this;
    }
    
    public zza a(String paramString)
    {
      this.b = paramString;
      return this;
    }
    
    public zza a(boolean paramBoolean)
    {
      this.c = paramBoolean;
      return this;
    }
    
    public RegisterSectionInfo a()
    {
      int j = 0;
      Object localObject = null;
      if (this.h != null)
      {
        int[] arrayOfInt = new int[this.h.cardinality()];
        int k = this.h.nextSetBit(0);
        for (;;)
        {
          localObject = arrayOfInt;
          if (k < 0) {
            break;
          }
          arrayOfInt[j] = k;
          k = this.h.nextSetBit(k + 1);
          j += 1;
        }
      }
      return new RegisterSectionInfo(this.a, this.b, this.c, this.d, this.e, this.f, (Feature[])this.g.toArray(new Feature[this.g.size()]), (int[])localObject, this.i);
    }
    
    public zza b(String paramString)
    {
      this.i = paramString;
      return this;
    }
    
    public zza b(boolean paramBoolean)
    {
      this.e = paramBoolean;
      return this;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/appdatasearch/RegisterSectionInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */