package com.google.android.gms.appdatasearch;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zze
  implements Parcelable.Creator<Feature>
{
  static void a(Feature paramFeature, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramFeature.b);
    zzb.a(paramParcel, 1000, paramFeature.a);
    zzb.a(paramParcel, 2, paramFeature.c, false);
    zzb.a(paramParcel, paramInt);
  }
  
  public Feature a(Parcel paramParcel)
  {
    int j = 0;
    int k = zza.b(paramParcel);
    Bundle localBundle = null;
    int i = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        j = zza.g(paramParcel, m);
        break;
      case 1000: 
        i = zza.g(paramParcel, m);
        break;
      case 2: 
        localBundle = zza.r(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new Feature(i, j, localBundle);
  }
  
  public Feature[] a(int paramInt)
  {
    return new Feature[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/appdatasearch/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */