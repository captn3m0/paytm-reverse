package com.google.android.gms.fitness.internal.service;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;

public class zza
  implements Parcelable.Creator<FitnessDataSourcesRequest>
{
  static void a(FitnessDataSourcesRequest paramFitnessDataSourcesRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.c(paramParcel, 1, paramFitnessDataSourcesRequest.a(), false);
    zzb.a(paramParcel, 1000, paramFitnessDataSourcesRequest.b());
    zzb.a(paramParcel, paramInt);
  }
  
  public FitnessDataSourcesRequest a(Parcel paramParcel)
  {
    int j = com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel);
    int i = 0;
    ArrayList localArrayList = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = com.google.android.gms.common.internal.safeparcel.zza.a(paramParcel);
      switch (com.google.android.gms.common.internal.safeparcel.zza.a(k))
      {
      default: 
        com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel, k);
        break;
      case 1: 
        localArrayList = com.google.android.gms.common.internal.safeparcel.zza.c(paramParcel, k, DataType.CREATOR);
        break;
      case 1000: 
        i = com.google.android.gms.common.internal.safeparcel.zza.g(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new FitnessDataSourcesRequest(i, localArrayList);
  }
  
  public FitnessDataSourcesRequest[] a(int paramInt)
  {
    return new FitnessDataSourcesRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/internal/service/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */