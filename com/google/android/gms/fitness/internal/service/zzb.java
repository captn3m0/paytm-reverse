package com.google.android.gms.fitness.internal.service;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.fitness.data.DataSource;

public class zzb
  implements Parcelable.Creator<FitnessUnregistrationRequest>
{
  static void a(FitnessUnregistrationRequest paramFitnessUnregistrationRequest, Parcel paramParcel, int paramInt)
  {
    int i = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramFitnessUnregistrationRequest.a(), paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1000, paramFitnessUnregistrationRequest.b());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, i);
  }
  
  public FitnessUnregistrationRequest a(Parcel paramParcel)
  {
    int j = zza.b(paramParcel);
    int i = 0;
    DataSource localDataSource = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localDataSource = (DataSource)zza.a(paramParcel, k, DataSource.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new FitnessUnregistrationRequest(i, localDataSource);
  }
  
  public FitnessUnregistrationRequest[] a(int paramInt)
  {
    return new FitnessUnregistrationRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/internal/service/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */