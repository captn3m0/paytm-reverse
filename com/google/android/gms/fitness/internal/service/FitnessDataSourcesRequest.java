package com.google.android.gms.fitness.internal.service;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.fitness.data.DataType;
import java.util.Collections;
import java.util.List;

public class FitnessDataSourcesRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<FitnessDataSourcesRequest> CREATOR = new zza();
  private final int a;
  private final List<DataType> b;
  
  FitnessDataSourcesRequest(int paramInt, List<DataType> paramList)
  {
    this.a = paramInt;
    this.b = paramList;
  }
  
  public List<DataType> a()
  {
    return Collections.unmodifiableList(this.b);
  }
  
  int b()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String toString()
  {
    return zzw.a(this).a("dataTypes", this.b).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/internal/service/FitnessDataSourcesRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */