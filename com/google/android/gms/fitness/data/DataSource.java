package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;

public class DataSource
  implements SafeParcelable
{
  public static final Parcelable.Creator<DataSource> CREATOR = new zzf();
  private final int a;
  private final DataType b;
  private final String c;
  private final int d;
  private final Device e;
  private final Application f;
  private final String g;
  private final String h;
  
  DataSource(int paramInt1, DataType paramDataType, String paramString1, int paramInt2, Device paramDevice, Application paramApplication, String paramString2)
  {
    this.a = paramInt1;
    this.b = paramDataType;
    this.d = paramInt2;
    this.c = paramString1;
    this.e = paramDevice;
    this.f = paramApplication;
    this.g = paramString2;
    this.h = i();
  }
  
  private DataSource(Builder paramBuilder)
  {
    this.a = 3;
    this.b = Builder.a(paramBuilder);
    this.d = Builder.b(paramBuilder);
    this.c = Builder.c(paramBuilder);
    this.e = Builder.d(paramBuilder);
    this.f = Builder.e(paramBuilder);
    this.g = Builder.f(paramBuilder);
    this.h = i();
  }
  
  private boolean a(DataSource paramDataSource)
  {
    return this.h.equals(paramDataSource.h);
  }
  
  private String i()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(j());
    localStringBuilder.append(":").append(this.b.a());
    if (this.f != null) {
      localStringBuilder.append(":").append(this.f.a());
    }
    if (this.e != null) {
      localStringBuilder.append(":").append(this.e.g());
    }
    if (this.g != null) {
      localStringBuilder.append(":").append(this.g);
    }
    return localStringBuilder.toString();
  }
  
  private String j()
  {
    switch (this.d)
    {
    default: 
      throw new IllegalArgumentException("invalid type value");
    case 0: 
      return "raw";
    }
    return "derived";
  }
  
  public DataType a()
  {
    return this.b;
  }
  
  public int b()
  {
    return this.d;
  }
  
  public String c()
  {
    return this.c;
  }
  
  public Application d()
  {
    return this.f;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Device e()
  {
    return this.e;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof DataSource)) && (a((DataSource)paramObject)));
  }
  
  public String f()
  {
    return this.g;
  }
  
  public String g()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if (this.d == 0)
    {
      str = "r";
      localStringBuilder = localStringBuilder.append(str).append(":").append(this.b.c());
      if (this.f != null) {
        break label154;
      }
      str = "";
      label49:
      localStringBuilder = localStringBuilder.append(str);
      if (this.e == null) {
        break label202;
      }
      str = ":" + this.e.b() + ":" + this.e.d();
      label103:
      localStringBuilder = localStringBuilder.append(str);
      if (this.g == null) {
        break label208;
      }
    }
    label154:
    label202:
    label208:
    for (String str = ":" + this.g;; str = "")
    {
      return str;
      str = "d";
      break;
      if (this.f.equals(Application.a))
      {
        str = ":gms";
        break label49;
      }
      str = ":" + this.f.a();
      break label49;
      str = "";
      break label103;
    }
  }
  
  int h()
  {
    return this.a;
  }
  
  public int hashCode()
  {
    return this.h.hashCode();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("DataSource{");
    localStringBuilder.append(j());
    if (this.c != null) {
      localStringBuilder.append(":").append(this.c);
    }
    if (this.f != null) {
      localStringBuilder.append(":").append(this.f);
    }
    if (this.e != null) {
      localStringBuilder.append(":").append(this.e);
    }
    if (this.g != null) {
      localStringBuilder.append(":").append(this.g);
    }
    localStringBuilder.append(":").append(this.b);
    return "}";
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf.a(this, paramParcel, paramInt);
  }
  
  public static final class Builder
  {
    private DataType a;
    private int b = -1;
    private String c;
    private Device d;
    private Application e;
    private String f = "";
    
    public Builder a(int paramInt)
    {
      this.b = paramInt;
      return this;
    }
    
    public Builder a(DataType paramDataType)
    {
      this.a = paramDataType;
      return this;
    }
    
    public Builder a(String paramString)
    {
      this.c = paramString;
      return this;
    }
    
    public DataSource a()
    {
      boolean bool2 = true;
      if (this.a != null)
      {
        bool1 = true;
        zzx.a(bool1, "Must set data type");
        if (this.b < 0) {
          break label47;
        }
      }
      label47:
      for (boolean bool1 = bool2;; bool1 = false)
      {
        zzx.a(bool1, "Must set data source type");
        return new DataSource(this, null);
        bool1 = false;
        break;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/DataSource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */