package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import java.util.ArrayList;

public class zzb
  implements Parcelable.Creator<BleDevice>
{
  static void a(BleDevice paramBleDevice, Parcel paramParcel, int paramInt)
  {
    paramInt = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramBleDevice.a(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1000, paramBleDevice.e());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramBleDevice.b(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.b(paramParcel, 3, paramBleDevice.c(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.c(paramParcel, 4, paramBleDevice.d(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, paramInt);
  }
  
  public BleDevice a(Parcel paramParcel)
  {
    ArrayList localArrayList1 = null;
    int j = zza.b(paramParcel);
    int i = 0;
    ArrayList localArrayList2 = null;
    String str1 = null;
    String str2 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        str2 = zza.p(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str1 = zza.p(paramParcel, k);
        break;
      case 3: 
        localArrayList2 = zza.D(paramParcel, k);
        break;
      case 4: 
        localArrayList1 = zza.c(paramParcel, k, DataType.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new BleDevice(i, str2, str1, localArrayList2, localArrayList1);
  }
  
  public BleDevice[] a(int paramInt)
  {
    return new BleDevice[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */