package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Bucket
  implements SafeParcelable
{
  public static final Parcelable.Creator<Bucket> CREATOR = new zzc();
  private final int a;
  private final long b;
  private final long c;
  private final Session d;
  private final int e;
  private final List<DataSet> f;
  private final int g;
  private boolean h = false;
  
  Bucket(int paramInt1, long paramLong1, long paramLong2, Session paramSession, int paramInt2, List<DataSet> paramList, int paramInt3, boolean paramBoolean)
  {
    this.a = paramInt1;
    this.b = paramLong1;
    this.c = paramLong2;
    this.d = paramSession;
    this.e = paramInt2;
    this.f = paramList;
    this.g = paramInt3;
    this.h = paramBoolean;
  }
  
  public Bucket(RawBucket paramRawBucket, List<DataSource> paramList)
  {
    this(2, paramRawBucket.b, paramRawBucket.c, paramRawBucket.d, paramRawBucket.e, a(paramRawBucket.f, paramList), paramRawBucket.g, paramRawBucket.h);
  }
  
  public static String a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return "bug";
    case 1: 
      return "time";
    case 3: 
      return "type";
    case 4: 
      return "segment";
    case 2: 
      return "session";
    }
    return "unknown";
  }
  
  private static List<DataSet> a(Collection<RawDataSet> paramCollection, List<DataSource> paramList)
  {
    ArrayList localArrayList = new ArrayList(paramCollection.size());
    paramCollection = paramCollection.iterator();
    while (paramCollection.hasNext()) {
      localArrayList.add(new DataSet((RawDataSet)paramCollection.next(), paramList));
    }
    return localArrayList;
  }
  
  private boolean b(Bucket paramBucket)
  {
    return (this.b == paramBucket.b) && (this.c == paramBucket.c) && (this.e == paramBucket.e) && (zzw.a(this.f, paramBucket.f)) && (this.g == paramBucket.g) && (this.h == paramBucket.h);
  }
  
  public long a(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.b, TimeUnit.MILLISECONDS);
  }
  
  public Session a()
  {
    return this.d;
  }
  
  public boolean a(Bucket paramBucket)
  {
    return (this.b == paramBucket.b) && (this.c == paramBucket.c) && (this.e == paramBucket.e) && (this.g == paramBucket.g);
  }
  
  public int b()
  {
    return this.e;
  }
  
  public long b(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.c, TimeUnit.MILLISECONDS);
  }
  
  public List<DataSet> c()
  {
    return this.f;
  }
  
  public int d()
  {
    return this.g;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean e()
  {
    if (this.h) {
      return true;
    }
    Iterator localIterator = this.f.iterator();
    while (localIterator.hasNext()) {
      if (((DataSet)localIterator.next()).d()) {
        return true;
      }
    }
    return false;
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof Bucket)) && (b((Bucket)paramObject)));
  }
  
  int f()
  {
    return this.a;
  }
  
  public long g()
  {
    return this.b;
  }
  
  public long h()
  {
    return this.c;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Long.valueOf(this.b), Long.valueOf(this.c), Integer.valueOf(this.e), Integer.valueOf(this.g) });
  }
  
  public String toString()
  {
    return zzw.a(this).a("startTime", Long.valueOf(this.b)).a("endTime", Long.valueOf(this.c)).a("activity", Integer.valueOf(this.e)).a("dataSets", this.f).a("bucketType", a(this.g)).a("serverHasMoreData", Boolean.valueOf(this.h)).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/Bucket.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */