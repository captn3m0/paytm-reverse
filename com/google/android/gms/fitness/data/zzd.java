package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzd
  implements Parcelable.Creator<DataPoint>
{
  static void a(DataPoint paramDataPoint, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramDataPoint.b(), paramInt, false);
    zzb.a(paramParcel, 1000, paramDataPoint.f());
    zzb.a(paramParcel, 3, paramDataPoint.g());
    zzb.a(paramParcel, 4, paramDataPoint.h());
    zzb.a(paramParcel, 5, paramDataPoint.a(), paramInt, false);
    zzb.a(paramParcel, 6, paramDataPoint.c(), paramInt, false);
    zzb.a(paramParcel, 7, paramDataPoint.d());
    zzb.a(paramParcel, 8, paramDataPoint.e());
    zzb.a(paramParcel, i);
  }
  
  public DataPoint a(Parcel paramParcel)
  {
    int j = zza.b(paramParcel);
    int i = 0;
    DataSource localDataSource2 = null;
    long l4 = 0L;
    long l3 = 0L;
    Value[] arrayOfValue = null;
    DataSource localDataSource1 = null;
    long l2 = 0L;
    long l1 = 0L;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localDataSource2 = (DataSource)zza.a(paramParcel, k, DataSource.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 3: 
        l4 = zza.i(paramParcel, k);
        break;
      case 4: 
        l3 = zza.i(paramParcel, k);
        break;
      case 5: 
        arrayOfValue = (Value[])zza.b(paramParcel, k, Value.CREATOR);
        break;
      case 6: 
        localDataSource1 = (DataSource)zza.a(paramParcel, k, DataSource.CREATOR);
        break;
      case 7: 
        l2 = zza.i(paramParcel, k);
        break;
      case 8: 
        l1 = zza.i(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new DataPoint(i, localDataSource2, l4, l3, arrayOfValue, localDataSource1, l2, l1);
  }
  
  public DataPoint[] a(int paramInt)
  {
    return new DataPoint[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */