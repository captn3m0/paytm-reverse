package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzf
  implements Parcelable.Creator<DataSource>
{
  static void a(DataSource paramDataSource, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramDataSource.a(), paramInt, false);
    zzb.a(paramParcel, 1000, paramDataSource.h());
    zzb.a(paramParcel, 2, paramDataSource.c(), false);
    zzb.a(paramParcel, 3, paramDataSource.b());
    zzb.a(paramParcel, 4, paramDataSource.e(), paramInt, false);
    zzb.a(paramParcel, 5, paramDataSource.d(), paramInt, false);
    zzb.a(paramParcel, 6, paramDataSource.f(), false);
    zzb.a(paramParcel, i);
  }
  
  public DataSource a(Parcel paramParcel)
  {
    int i = 0;
    String str1 = null;
    int k = zza.b(paramParcel);
    Application localApplication = null;
    Device localDevice = null;
    String str2 = null;
    DataType localDataType = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        localDataType = (DataType)zza.a(paramParcel, m, DataType.CREATOR);
        break;
      case 1000: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        str2 = zza.p(paramParcel, m);
        break;
      case 3: 
        i = zza.g(paramParcel, m);
        break;
      case 4: 
        localDevice = (Device)zza.a(paramParcel, m, Device.CREATOR);
        break;
      case 5: 
        localApplication = (Application)zza.a(paramParcel, m, Application.CREATOR);
        break;
      case 6: 
        str1 = zza.p(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new DataSource(j, localDataType, str2, i, localDevice, localApplication, str1);
  }
  
  public DataSource[] a(int paramInt)
  {
    return new DataSource[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */