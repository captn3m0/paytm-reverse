package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import java.util.concurrent.TimeUnit;

public class Session
  implements SafeParcelable
{
  public static final Parcelable.Creator<Session> CREATOR = new zzq();
  private final int a;
  private final long b;
  private final long c;
  private final String d;
  private final String e;
  private final String f;
  private final int g;
  private final Application h;
  private final Long i;
  
  Session(int paramInt1, long paramLong1, long paramLong2, String paramString1, String paramString2, String paramString3, int paramInt2, Application paramApplication, Long paramLong)
  {
    this.a = paramInt1;
    this.b = paramLong1;
    this.c = paramLong2;
    this.d = paramString1;
    this.e = paramString2;
    this.f = paramString3;
    this.g = paramInt2;
    this.h = paramApplication;
    this.i = paramLong;
  }
  
  private boolean a(Session paramSession)
  {
    return (this.b == paramSession.b) && (this.c == paramSession.c) && (zzw.a(this.d, paramSession.d)) && (zzw.a(this.e, paramSession.e)) && (zzw.a(this.f, paramSession.f)) && (zzw.a(this.h, paramSession.h)) && (this.g == paramSession.g);
  }
  
  public long a(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.b, TimeUnit.MILLISECONDS);
  }
  
  public boolean a()
  {
    return this.c == 0L;
  }
  
  public String b()
  {
    return this.d;
  }
  
  public String c()
  {
    return this.e;
  }
  
  public String d()
  {
    return this.f;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public int e()
  {
    return this.g;
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof Session)) && (a((Session)paramObject)));
  }
  
  public Application f()
  {
    return this.h;
  }
  
  int g()
  {
    return this.a;
  }
  
  public long h()
  {
    return this.b;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Long.valueOf(this.b), Long.valueOf(this.c), this.e });
  }
  
  public long i()
  {
    return this.c;
  }
  
  public Long j()
  {
    return this.i;
  }
  
  public String toString()
  {
    return zzw.a(this).a("startTime", Long.valueOf(this.b)).a("endTime", Long.valueOf(this.c)).a("name", this.d).a("identifier", this.e).a("description", this.f).a("activity", Integer.valueOf(this.g)).a("application", this.h).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzq.a(this, paramParcel, paramInt);
  }
  
  public static class Builder
  {
    private long a = 0L;
    private long b = 0L;
    private String c = null;
    private int d = 4;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/Session.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */