package com.google.android.gms.fitness.data;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzmy;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public final class Value
  implements SafeParcelable
{
  public static final Parcelable.Creator<Value> CREATOR = new zzu();
  private final int a;
  private final int b;
  private boolean c;
  private float d;
  private String e;
  private Map<String, MapValue> f;
  private int[] g;
  private float[] h;
  private byte[] i;
  
  Value(int paramInt1, int paramInt2, boolean paramBoolean, float paramFloat, String paramString, Bundle paramBundle, int[] paramArrayOfInt, float[] paramArrayOfFloat, byte[] paramArrayOfByte)
  {
    this.a = paramInt1;
    this.b = paramInt2;
    this.c = paramBoolean;
    this.d = paramFloat;
    this.e = paramString;
    this.f = a(paramBundle);
    this.g = paramArrayOfInt;
    this.h = paramArrayOfFloat;
    this.i = paramArrayOfByte;
  }
  
  private static Map<String, MapValue> a(Bundle paramBundle)
  {
    if (paramBundle == null) {
      return null;
    }
    paramBundle.setClassLoader(MapValue.class.getClassLoader());
    ArrayMap localArrayMap = new ArrayMap(paramBundle.size());
    Iterator localIterator = paramBundle.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      localArrayMap.put(str, paramBundle.getParcelable(str));
    }
    return localArrayMap;
  }
  
  private boolean a(Value paramValue)
  {
    if ((this.b == paramValue.b) && (this.c == paramValue.c))
    {
      switch (this.b)
      {
      default: 
        if (this.d != paramValue.d) {
          break;
        }
      case 1: 
      case 2: 
        do
        {
          do
          {
            return true;
          } while (c() == paramValue.c());
          return false;
        } while (this.d == paramValue.d);
        return false;
      case 3: 
        return zzw.a(this.e, paramValue.e);
      case 4: 
        return zzw.a(this.f, paramValue.f);
      case 5: 
        return Arrays.equals(this.g, paramValue.g);
      case 6: 
        return Arrays.equals(this.h, paramValue.h);
      case 7: 
        return Arrays.equals(this.i, paramValue.i);
      }
      return false;
    }
    return false;
  }
  
  public boolean a()
  {
    return this.c;
  }
  
  public int b()
  {
    return this.b;
  }
  
  public int c()
  {
    boolean bool = true;
    if (this.b == 1) {}
    for (;;)
    {
      zzx.a(bool, "Value is not in int format");
      return Float.floatToRawIntBits(this.d);
      bool = false;
    }
  }
  
  int d()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  float e()
  {
    return this.d;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof Value)) && (a((Value)paramObject)));
  }
  
  String f()
  {
    return this.e;
  }
  
  Bundle g()
  {
    if (this.f == null) {
      return null;
    }
    Bundle localBundle = new Bundle(this.f.size());
    Iterator localIterator = this.f.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      localBundle.putParcelable((String)localEntry.getKey(), (Parcelable)localEntry.getValue());
    }
    return localBundle;
  }
  
  int[] h()
  {
    return this.g;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Float.valueOf(this.d), this.e, this.f, this.g, this.h, this.i });
  }
  
  float[] i()
  {
    return this.h;
  }
  
  byte[] j()
  {
    return this.i;
  }
  
  public String toString()
  {
    if (!this.c) {
      return "unset";
    }
    switch (this.b)
    {
    default: 
      return "unknown";
    case 1: 
      return Integer.toString(c());
    case 2: 
      return Float.toString(this.d);
    case 3: 
      return this.e;
    case 4: 
      return new TreeMap(this.f).toString();
    case 5: 
      return Arrays.toString(this.g);
    case 6: 
      return Arrays.toString(this.h);
    }
    return zzmy.a(this.i, 0, this.i.length, false);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzu.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/Value.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */