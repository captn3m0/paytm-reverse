package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzi
  implements Parcelable.Creator<Device>
{
  static void a(Device paramDevice, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramDevice.a(), false);
    zzb.a(paramParcel, 1000, paramDevice.i());
    zzb.a(paramParcel, 2, paramDevice.b(), false);
    zzb.a(paramParcel, 3, paramDevice.c(), false);
    zzb.a(paramParcel, 4, paramDevice.h(), false);
    zzb.a(paramParcel, 5, paramDevice.e());
    zzb.a(paramParcel, 6, paramDevice.f());
    zzb.a(paramParcel, paramInt);
  }
  
  public Device a(Parcel paramParcel)
  {
    int i = 0;
    String str1 = null;
    int m = zza.b(paramParcel);
    int j = 0;
    String str2 = null;
    String str3 = null;
    String str4 = null;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        str4 = zza.p(paramParcel, n);
        break;
      case 1000: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        str3 = zza.p(paramParcel, n);
        break;
      case 3: 
        str2 = zza.p(paramParcel, n);
        break;
      case 4: 
        str1 = zza.p(paramParcel, n);
        break;
      case 5: 
        j = zza.g(paramParcel, n);
        break;
      case 6: 
        i = zza.g(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new Device(k, str4, str3, str2, str1, j, i);
  }
  
  public Device[] a(int paramInt)
  {
    return new Device[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */