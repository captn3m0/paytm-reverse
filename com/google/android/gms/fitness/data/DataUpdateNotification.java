package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;

public class DataUpdateNotification
  implements SafeParcelable
{
  public static final Parcelable.Creator<DataUpdateNotification> CREATOR = new zzh();
  final int a;
  private final long b;
  private final long c;
  private final int d;
  private final DataSource e;
  private final DataType f;
  
  DataUpdateNotification(int paramInt1, long paramLong1, long paramLong2, int paramInt2, DataSource paramDataSource, DataType paramDataType)
  {
    this.a = paramInt1;
    this.b = paramLong1;
    this.c = paramLong2;
    this.d = paramInt2;
    this.e = paramDataSource;
    this.f = paramDataType;
  }
  
  private boolean a(DataUpdateNotification paramDataUpdateNotification)
  {
    return (this.b == paramDataUpdateNotification.b) && (this.c == paramDataUpdateNotification.c) && (this.d == paramDataUpdateNotification.d) && (zzw.a(this.e, paramDataUpdateNotification.e)) && (zzw.a(this.f, paramDataUpdateNotification.f));
  }
  
  public long a()
  {
    return this.b;
  }
  
  public long b()
  {
    return this.c;
  }
  
  public int c()
  {
    return this.d;
  }
  
  public DataSource d()
  {
    return this.e;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public DataType e()
  {
    return this.f;
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof DataUpdateNotification)) && (a((DataUpdateNotification)paramObject)));
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Long.valueOf(this.b), Long.valueOf(this.c), Integer.valueOf(this.d), this.e, this.f });
  }
  
  public String toString()
  {
    return zzw.a(this).a("updateStartTimeNanos", Long.valueOf(this.b)).a("updateEndTimeNanos", Long.valueOf(this.c)).a("operationType", Integer.valueOf(this.d)).a("dataSource", this.e).a("dataType", this.f).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzh.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/DataUpdateNotification.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */