package com.google.android.gms.fitness.data;

import android.os.RemoteException;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.fitness.request.OnDataPointListener;
import java.util.HashMap;
import java.util.Map;

public class zzl
  extends zzk.zza
{
  private final OnDataPointListener a;
  
  private zzl(OnDataPointListener paramOnDataPointListener)
  {
    this.a = ((OnDataPointListener)zzx.a(paramOnDataPointListener));
  }
  
  public void a(DataPoint paramDataPoint)
    throws RemoteException
  {
    this.a.a(paramDataPoint);
  }
  
  public static class zza
  {
    private static final zza a = new zza();
    private final Map<OnDataPointListener, zzl> b = new HashMap();
    
    public static zza a()
    {
      return a;
    }
    
    public zzl a(OnDataPointListener paramOnDataPointListener)
    {
      synchronized (this.b)
      {
        zzl localzzl = (zzl)this.b.remove(paramOnDataPointListener);
        if (localzzl != null) {
          return localzzl;
        }
        paramOnDataPointListener = new zzl(paramOnDataPointListener, null);
        return paramOnDataPointListener;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */