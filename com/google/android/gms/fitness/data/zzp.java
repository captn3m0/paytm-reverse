package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzp
  implements Parcelable.Creator<RawDataSet>
{
  static void a(RawDataSet paramRawDataSet, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramRawDataSet.b);
    zzb.a(paramParcel, 1000, paramRawDataSet.a);
    zzb.a(paramParcel, 2, paramRawDataSet.c);
    zzb.c(paramParcel, 3, paramRawDataSet.d, false);
    zzb.a(paramParcel, 4, paramRawDataSet.e);
    zzb.a(paramParcel, paramInt);
  }
  
  public RawDataSet a(Parcel paramParcel)
  {
    boolean bool = false;
    int m = zza.b(paramParcel);
    ArrayList localArrayList = null;
    int i = 0;
    int j = 0;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        j = zza.g(paramParcel, n);
        break;
      case 1000: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        i = zza.g(paramParcel, n);
        break;
      case 3: 
        localArrayList = zza.c(paramParcel, n, RawDataPoint.CREATOR);
        break;
      case 4: 
        bool = zza.c(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new RawDataSet(k, j, i, localArrayList, bool);
  }
  
  public RawDataSet[] a(int paramInt)
  {
    return new RawDataSet[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */