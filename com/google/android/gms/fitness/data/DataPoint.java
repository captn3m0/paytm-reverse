package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public final class DataPoint
  implements SafeParcelable
{
  public static final Parcelable.Creator<DataPoint> CREATOR = new zzd();
  private final int a;
  private final DataSource b;
  private long c;
  private long d;
  private final Value[] e;
  private DataSource f;
  private long g;
  private long h;
  
  DataPoint(int paramInt, DataSource paramDataSource1, long paramLong1, long paramLong2, Value[] paramArrayOfValue, DataSource paramDataSource2, long paramLong3, long paramLong4)
  {
    this.a = paramInt;
    this.b = paramDataSource1;
    this.f = paramDataSource2;
    this.c = paramLong1;
    this.d = paramLong2;
    this.e = paramArrayOfValue;
    this.g = paramLong3;
    this.h = paramLong4;
  }
  
  public DataPoint(DataSource paramDataSource1, DataSource paramDataSource2, RawDataPoint paramRawDataPoint)
  {
    this(4, paramDataSource1, a(Long.valueOf(paramRawDataPoint.b), 0L), a(Long.valueOf(paramRawDataPoint.c), 0L), paramRawDataPoint.d, paramDataSource2, a(Long.valueOf(paramRawDataPoint.g), 0L), a(Long.valueOf(paramRawDataPoint.h), 0L));
  }
  
  DataPoint(List<DataSource> paramList, RawDataPoint paramRawDataPoint)
  {
    this(a(paramList, paramRawDataPoint.e), a(paramList, paramRawDataPoint.f), paramRawDataPoint);
  }
  
  private static long a(Long paramLong, long paramLong1)
  {
    if (paramLong != null) {
      paramLong1 = paramLong.longValue();
    }
    return paramLong1;
  }
  
  private static DataSource a(List<DataSource> paramList, int paramInt)
  {
    if ((paramInt >= 0) && (paramInt < paramList.size())) {
      return (DataSource)paramList.get(paramInt);
    }
    return null;
  }
  
  private boolean a(DataPoint paramDataPoint)
  {
    return (zzw.a(this.b, paramDataPoint.b)) && (this.c == paramDataPoint.c) && (this.d == paramDataPoint.d) && (Arrays.equals(this.e, paramDataPoint.e)) && (zzw.a(c(), paramDataPoint.c()));
  }
  
  public long a(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.c, TimeUnit.NANOSECONDS);
  }
  
  public Value[] a()
  {
    return this.e;
  }
  
  public long b(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.d, TimeUnit.NANOSECONDS);
  }
  
  public DataSource b()
  {
    return this.b;
  }
  
  public DataSource c()
  {
    if (this.f != null) {
      return this.f;
    }
    return this.b;
  }
  
  public long d()
  {
    return this.g;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public long e()
  {
    return this.h;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof DataPoint)) && (a((DataPoint)paramObject)));
  }
  
  public int f()
  {
    return this.a;
  }
  
  public long g()
  {
    return this.c;
  }
  
  public long h()
  {
    return this.d;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b, Long.valueOf(this.c), Long.valueOf(this.d) });
  }
  
  public String toString()
  {
    String str2 = Arrays.toString(this.e);
    long l1 = this.d;
    long l2 = this.c;
    long l3 = this.g;
    long l4 = this.h;
    String str3 = this.b.g();
    if (this.f != null) {}
    for (String str1 = this.f.g();; str1 = "N/A") {
      return String.format("DataPoint{%s@[%s, %s,raw=%s,insert=%s](%s %s)}", new Object[] { str2, Long.valueOf(l1), Long.valueOf(l2), Long.valueOf(l3), Long.valueOf(l4), str3, str1 });
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/DataPoint.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */