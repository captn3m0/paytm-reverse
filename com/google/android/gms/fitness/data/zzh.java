package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzh
  implements Parcelable.Creator<DataUpdateNotification>
{
  static void a(DataUpdateNotification paramDataUpdateNotification, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramDataUpdateNotification.a());
    zzb.a(paramParcel, 1000, paramDataUpdateNotification.a);
    zzb.a(paramParcel, 2, paramDataUpdateNotification.b());
    zzb.a(paramParcel, 3, paramDataUpdateNotification.c());
    zzb.a(paramParcel, 4, paramDataUpdateNotification.d(), paramInt, false);
    zzb.a(paramParcel, 5, paramDataUpdateNotification.e(), paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public DataUpdateNotification a(Parcel paramParcel)
  {
    long l1 = 0L;
    DataType localDataType = null;
    int i = 0;
    int k = zza.b(paramParcel);
    DataSource localDataSource = null;
    long l2 = 0L;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        l2 = zza.i(paramParcel, m);
        break;
      case 1000: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        l1 = zza.i(paramParcel, m);
        break;
      case 3: 
        i = zza.g(paramParcel, m);
        break;
      case 4: 
        localDataSource = (DataSource)zza.a(paramParcel, m, DataSource.CREATOR);
        break;
      case 5: 
        localDataType = (DataType)zza.a(paramParcel, m, DataType.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new DataUpdateNotification(j, l2, l1, i, localDataSource, localDataType);
  }
  
  public DataUpdateNotification[] a(int paramInt)
  {
    return new DataUpdateNotification[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */