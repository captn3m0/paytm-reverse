package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzj
  implements Parcelable.Creator<Field>
{
  static void a(Field paramField, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramField.a(), false);
    zzb.a(paramParcel, 1000, paramField.d());
    zzb.a(paramParcel, 2, paramField.b());
    zzb.a(paramParcel, 3, paramField.c(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public Field a(Parcel paramParcel)
  {
    Boolean localBoolean = null;
    int j = 0;
    int k = zza.b(paramParcel);
    String str = null;
    int i = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        str = zza.p(paramParcel, m);
        break;
      case 1000: 
        i = zza.g(paramParcel, m);
        break;
      case 2: 
        j = zza.g(paramParcel, m);
        break;
      case 3: 
        localBoolean = zza.d(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new Field(i, str, j, localBoolean);
  }
  
  public Field[] a(int paramInt)
  {
    return new Field[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */