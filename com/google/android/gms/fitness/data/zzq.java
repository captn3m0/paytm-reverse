package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzq
  implements Parcelable.Creator<Session>
{
  static void a(Session paramSession, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramSession.h());
    zzb.a(paramParcel, 1000, paramSession.g());
    zzb.a(paramParcel, 2, paramSession.i());
    zzb.a(paramParcel, 3, paramSession.b(), false);
    zzb.a(paramParcel, 4, paramSession.c(), false);
    zzb.a(paramParcel, 5, paramSession.d(), false);
    zzb.a(paramParcel, 7, paramSession.e());
    zzb.a(paramParcel, 8, paramSession.f(), paramInt, false);
    zzb.a(paramParcel, 9, paramSession.j(), false);
    zzb.a(paramParcel, i);
  }
  
  public Session a(Parcel paramParcel)
  {
    long l1 = 0L;
    int i = 0;
    Long localLong = null;
    int k = zza.b(paramParcel);
    Application localApplication = null;
    String str1 = null;
    String str2 = null;
    String str3 = null;
    long l2 = 0L;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        l2 = zza.i(paramParcel, m);
        break;
      case 1000: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        l1 = zza.i(paramParcel, m);
        break;
      case 3: 
        str3 = zza.p(paramParcel, m);
        break;
      case 4: 
        str2 = zza.p(paramParcel, m);
        break;
      case 5: 
        str1 = zza.p(paramParcel, m);
        break;
      case 7: 
        i = zza.g(paramParcel, m);
        break;
      case 8: 
        localApplication = (Application)zza.a(paramParcel, m, Application.CREATOR);
        break;
      case 9: 
        localLong = zza.j(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new Session(j, l2, l1, str3, str2, str1, i, localApplication, localLong);
  }
  
  public Session[] a(int paramInt)
  {
    return new Session[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */