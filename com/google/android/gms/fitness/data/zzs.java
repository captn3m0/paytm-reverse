package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzs
  implements Parcelable.Creator<Subscription>
{
  static void a(Subscription paramSubscription, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramSubscription.a(), paramInt, false);
    zzb.a(paramParcel, 1000, paramSubscription.e());
    zzb.a(paramParcel, 2, paramSubscription.b(), paramInt, false);
    zzb.a(paramParcel, 3, paramSubscription.d());
    zzb.a(paramParcel, 4, paramSubscription.c());
    zzb.a(paramParcel, i);
  }
  
  public Subscription a(Parcel paramParcel)
  {
    DataType localDataType = null;
    int i = 0;
    int k = zza.b(paramParcel);
    long l = 0L;
    DataSource localDataSource = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        localDataSource = (DataSource)zza.a(paramParcel, m, DataSource.CREATOR);
        break;
      case 1000: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        localDataType = (DataType)zza.a(paramParcel, m, DataType.CREATOR);
        break;
      case 3: 
        l = zza.i(paramParcel, m);
        break;
      case 4: 
        i = zza.g(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new Subscription(j, localDataSource, localDataType, l, i);
  }
  
  public Subscription[] a(int paramInt)
  {
    return new Subscription[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/zzs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */