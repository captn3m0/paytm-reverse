package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

@KeepName
public final class RawDataPoint
  implements SafeParcelable
{
  public static final Parcelable.Creator<RawDataPoint> CREATOR = new zzo();
  final int a;
  public final long b;
  public final long c;
  public final Value[] d;
  public final int e;
  public final int f;
  public final long g;
  public final long h;
  
  public RawDataPoint(int paramInt1, long paramLong1, long paramLong2, Value[] paramArrayOfValue, int paramInt2, int paramInt3, long paramLong3, long paramLong4)
  {
    this.a = paramInt1;
    this.b = paramLong1;
    this.c = paramLong2;
    this.e = paramInt2;
    this.f = paramInt3;
    this.g = paramLong3;
    this.h = paramLong4;
    this.d = paramArrayOfValue;
  }
  
  RawDataPoint(DataPoint paramDataPoint, List<DataSource> paramList)
  {
    this.a = 4;
    this.b = paramDataPoint.a(TimeUnit.NANOSECONDS);
    this.c = paramDataPoint.b(TimeUnit.NANOSECONDS);
    this.d = paramDataPoint.a();
    this.e = zzt.a(paramDataPoint.b(), paramList);
    this.f = zzt.a(paramDataPoint.c(), paramList);
    this.g = paramDataPoint.d();
    this.h = paramDataPoint.e();
  }
  
  private boolean a(RawDataPoint paramRawDataPoint)
  {
    return (this.b == paramRawDataPoint.b) && (this.c == paramRawDataPoint.c) && (Arrays.equals(this.d, paramRawDataPoint.d)) && (this.e == paramRawDataPoint.e) && (this.f == paramRawDataPoint.f) && (this.g == paramRawDataPoint.g);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof RawDataPoint)) && (a((RawDataPoint)paramObject)));
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Long.valueOf(this.b), Long.valueOf(this.c) });
  }
  
  public String toString()
  {
    return String.format("RawDataPoint{%s@[%s, %s](%d,%d)}", new Object[] { Arrays.toString(this.d), Long.valueOf(this.c), Long.valueOf(this.b), Integer.valueOf(this.e), Integer.valueOf(this.f) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzo.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/RawDataPoint.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */