package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zze
  implements Parcelable.Creator<DataSet>
{
  static void a(DataSet paramDataSet, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramDataSet.a(), paramInt, false);
    zzb.a(paramParcel, 1000, paramDataSet.e());
    zzb.a(paramParcel, 2, paramDataSet.b(), paramInt, false);
    zzb.d(paramParcel, 3, paramDataSet.f(), false);
    zzb.c(paramParcel, 4, paramDataSet.g(), false);
    zzb.a(paramParcel, 5, paramDataSet.d());
    zzb.a(paramParcel, i);
  }
  
  public DataSet a(Parcel paramParcel)
  {
    boolean bool = false;
    ArrayList localArrayList1 = null;
    int j = zza.b(paramParcel);
    ArrayList localArrayList2 = new ArrayList();
    DataType localDataType = null;
    DataSource localDataSource = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localDataSource = (DataSource)zza.a(paramParcel, k, DataSource.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localDataType = (DataType)zza.a(paramParcel, k, DataType.CREATOR);
        break;
      case 3: 
        zza.a(paramParcel, k, localArrayList2, getClass().getClassLoader());
        break;
      case 4: 
        localArrayList1 = zza.c(paramParcel, k, DataSource.CREATOR);
        break;
      case 5: 
        bool = zza.c(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new DataSet(i, localDataSource, localDataType, localArrayList2, localArrayList1, bool);
  }
  
  public DataSet[] a(int paramInt)
  {
    return new DataSet[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */