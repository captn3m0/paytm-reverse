package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzr
  implements Parcelable.Creator<SessionDataSet>
{
  static void a(SessionDataSet paramSessionDataSet, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramSessionDataSet.a(), paramInt, false);
    zzb.a(paramParcel, 1000, paramSessionDataSet.a);
    zzb.a(paramParcel, 2, paramSessionDataSet.b(), paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public SessionDataSet a(Parcel paramParcel)
  {
    DataSet localDataSet = null;
    int j = zza.b(paramParcel);
    int i = 0;
    Session localSession = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
      }
      for (;;)
      {
        break;
        localSession = (Session)zza.a(paramParcel, k, Session.CREATOR);
        continue;
        i = zza.g(paramParcel, k);
        continue;
        localDataSet = (DataSet)zza.a(paramParcel, k, DataSet.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new SessionDataSet(i, localSession, localDataSet);
  }
  
  public SessionDataSet[] a(int paramInt)
  {
    return new SessionDataSet[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/zzr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */