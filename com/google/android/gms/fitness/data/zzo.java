package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzo
  implements Parcelable.Creator<RawDataPoint>
{
  static void a(RawDataPoint paramRawDataPoint, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramRawDataPoint.b);
    zzb.a(paramParcel, 1000, paramRawDataPoint.a);
    zzb.a(paramParcel, 2, paramRawDataPoint.c);
    zzb.a(paramParcel, 3, paramRawDataPoint.d, paramInt, false);
    zzb.a(paramParcel, 4, paramRawDataPoint.e);
    zzb.a(paramParcel, 5, paramRawDataPoint.f);
    zzb.a(paramParcel, 6, paramRawDataPoint.g);
    zzb.a(paramParcel, 7, paramRawDataPoint.h);
    zzb.a(paramParcel, i);
  }
  
  public RawDataPoint a(Parcel paramParcel)
  {
    int m = zza.b(paramParcel);
    int k = 0;
    long l4 = 0L;
    long l3 = 0L;
    Value[] arrayOfValue = null;
    int j = 0;
    int i = 0;
    long l2 = 0L;
    long l1 = 0L;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        l4 = zza.i(paramParcel, n);
        break;
      case 1000: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        l3 = zza.i(paramParcel, n);
        break;
      case 3: 
        arrayOfValue = (Value[])zza.b(paramParcel, n, Value.CREATOR);
        break;
      case 4: 
        j = zza.g(paramParcel, n);
        break;
      case 5: 
        i = zza.g(paramParcel, n);
        break;
      case 6: 
        l2 = zza.i(paramParcel, n);
        break;
      case 7: 
        l1 = zza.i(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new RawDataPoint(k, l4, l3, arrayOfValue, j, i, l2, l1);
  }
  
  public RawDataPoint[] a(int paramInt)
  {
    return new RawDataPoint[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/zzo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */