package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;

public final class Field
  implements SafeParcelable
{
  public static final Field A;
  public static final Field B;
  public static final Field C;
  public static final Parcelable.Creator<Field> CREATOR = new zzj();
  public static final Field D;
  public static final Field E;
  public static final Field F;
  public static final Field G;
  public static final Field H;
  public static final Field I;
  public static final Field J;
  public static final Field K;
  public static final Field L;
  public static final Field M;
  public static final Field N;
  public static final Field O;
  public static final Field P;
  public static final Field Q;
  public static final Field R;
  public static final Field S;
  public static final Field T;
  public static final Field U;
  public static final Field a = a("activity");
  public static final Field b = b("confidence");
  public static final Field c = e("activity_confidence");
  public static final Field d = a("steps");
  public static final Field e = a("duration");
  public static final Field f = e("activity_duration");
  public static final Field g = e("activity_duration.ascending");
  public static final Field h = e("activity_duration.descending");
  public static final Field i = b("bpm");
  public static final Field j = b("latitude");
  public static final Field k = b("longitude");
  public static final Field l = b("accuracy");
  public static final Field m = c("altitude");
  public static final Field n = b("distance");
  public static final Field o = b("height");
  public static final Field p = b("weight");
  public static final Field q = b("circumference");
  public static final Field r = b("percentage");
  public static final Field s = b("speed");
  public static final Field t = b("rpm");
  public static final Field u = a("revolutions");
  public static final Field v = b("calories");
  public static final Field w = b("watts");
  public static final Field x = a("meal_type");
  public static final Field y = d("food_item");
  public static final Field z = e("nutrients");
  private final int V;
  private final String W;
  private final int X;
  private final Boolean Y;
  
  static
  {
    A = b("elevation.change");
    B = e("elevation.gain");
    C = e("elevation.loss");
    D = b("floors");
    E = e("floor.gain");
    F = e("floor.loss");
    G = d("exercise");
    H = a("repetitions");
    I = b("resistance");
    J = a("resistance_type");
    K = a("num_segments");
    L = b("average");
    M = b("max");
    N = b("min");
    O = b("low_latitude");
    P = b("low_longitude");
    Q = b("high_latitude");
    R = b("high_longitude");
    S = b("x");
    T = b("y");
    U = b("z");
  }
  
  Field(int paramInt1, String paramString, int paramInt2, Boolean paramBoolean)
  {
    this.V = paramInt1;
    this.W = ((String)zzx.a(paramString));
    this.X = paramInt2;
    this.Y = paramBoolean;
  }
  
  private Field(String paramString, int paramInt)
  {
    this(2, paramString, paramInt, null);
  }
  
  private Field(String paramString, int paramInt, Boolean paramBoolean)
  {
    this(2, paramString, paramInt, paramBoolean);
  }
  
  private static Field a(String paramString)
  {
    return new Field(paramString, 1);
  }
  
  private boolean a(Field paramField)
  {
    return (this.W.equals(paramField.W)) && (this.X == paramField.X);
  }
  
  private static Field b(String paramString)
  {
    return new Field(paramString, 2);
  }
  
  private static Field c(String paramString)
  {
    return new Field(paramString, 2, Boolean.valueOf(true));
  }
  
  private static Field d(String paramString)
  {
    return new Field(paramString, 3);
  }
  
  private static Field e(String paramString)
  {
    return new Field(paramString, 4);
  }
  
  public String a()
  {
    return this.W;
  }
  
  public int b()
  {
    return this.X;
  }
  
  public Boolean c()
  {
    return this.Y;
  }
  
  int d()
  {
    return this.V;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof Field)) && (a((Field)paramObject)));
  }
  
  public int hashCode()
  {
    return this.W.hashCode();
  }
  
  public String toString()
  {
    String str2 = this.W;
    if (this.X == 1) {}
    for (String str1 = "i";; str1 = "f") {
      return String.format("%s(%s)", new Object[] { str2, str1 });
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzj.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/Field.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */