package com.google.android.gms.fitness.data;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzu
  implements Parcelable.Creator<Value>
{
  static void a(Value paramValue, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramValue.b());
    zzb.a(paramParcel, 1000, paramValue.d());
    zzb.a(paramParcel, 2, paramValue.a());
    zzb.a(paramParcel, 3, paramValue.e());
    zzb.a(paramParcel, 4, paramValue.f(), false);
    zzb.a(paramParcel, 5, paramValue.g(), false);
    zzb.a(paramParcel, 6, paramValue.h(), false);
    zzb.a(paramParcel, 7, paramValue.i(), false);
    zzb.a(paramParcel, 8, paramValue.j(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public Value a(Parcel paramParcel)
  {
    boolean bool = false;
    byte[] arrayOfByte = null;
    int k = zza.b(paramParcel);
    float f = 0.0F;
    float[] arrayOfFloat = null;
    int[] arrayOfInt = null;
    Bundle localBundle = null;
    String str = null;
    int i = 0;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        i = zza.g(paramParcel, m);
        break;
      case 1000: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        bool = zza.c(paramParcel, m);
        break;
      case 3: 
        f = zza.l(paramParcel, m);
        break;
      case 4: 
        str = zza.p(paramParcel, m);
        break;
      case 5: 
        localBundle = zza.r(paramParcel, m);
        break;
      case 6: 
        arrayOfInt = zza.v(paramParcel, m);
        break;
      case 7: 
        arrayOfFloat = zza.y(paramParcel, m);
        break;
      case 8: 
        arrayOfByte = zza.s(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new Value(j, i, bool, f, str, localBundle, arrayOfInt, arrayOfFloat, arrayOfByte);
  }
  
  public Value[] a(int paramInt)
  {
    return new Value[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/zzu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */