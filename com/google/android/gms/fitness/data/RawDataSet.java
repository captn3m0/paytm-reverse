package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import java.util.List;

@KeepName
public final class RawDataSet
  implements SafeParcelable
{
  public static final Parcelable.Creator<RawDataSet> CREATOR = new zzp();
  final int a;
  public final int b;
  public final int c;
  public final List<RawDataPoint> d;
  public final boolean e;
  
  public RawDataSet(int paramInt1, int paramInt2, int paramInt3, List<RawDataPoint> paramList, boolean paramBoolean)
  {
    this.a = paramInt1;
    this.b = paramInt2;
    this.c = paramInt3;
    this.d = paramList;
    this.e = paramBoolean;
  }
  
  public RawDataSet(DataSet paramDataSet, List<DataSource> paramList, List<DataType> paramList1)
  {
    this.a = 3;
    this.d = paramDataSet.a(paramList);
    this.e = paramDataSet.d();
    this.b = zzt.a(paramDataSet.a(), paramList);
    this.c = zzt.a(paramDataSet.b(), paramList1);
  }
  
  private boolean a(RawDataSet paramRawDataSet)
  {
    return (this.b == paramRawDataSet.b) && (this.e == paramRawDataSet.e) && (zzw.a(this.d, paramRawDataSet.d));
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof RawDataSet)) && (a((RawDataSet)paramObject)));
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Integer.valueOf(this.b) });
  }
  
  public String toString()
  {
    return String.format("RawDataSet{%s@[%s]}", new Object[] { Integer.valueOf(this.b), this.d });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzp.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/RawDataSet.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */