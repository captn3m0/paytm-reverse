package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzx;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class DataSet
  implements SafeParcelable
{
  public static final Parcelable.Creator<DataSet> CREATOR = new zze();
  private final int a;
  private final DataSource b;
  private final DataType c;
  private final List<DataPoint> d;
  private final List<DataSource> e;
  private boolean f = false;
  
  DataSet(int paramInt, DataSource paramDataSource, DataType paramDataType, List<RawDataPoint> paramList, List<DataSource> paramList1, boolean paramBoolean)
  {
    this.a = paramInt;
    this.b = paramDataSource;
    this.c = paramDataSource.a();
    this.f = paramBoolean;
    this.d = new ArrayList(paramList.size());
    if (paramInt >= 2) {}
    for (;;)
    {
      this.e = paramList1;
      paramDataSource = paramList.iterator();
      while (paramDataSource.hasNext())
      {
        paramDataType = (RawDataPoint)paramDataSource.next();
        this.d.add(new DataPoint(this.e, paramDataType));
      }
      paramList1 = Collections.singletonList(paramDataSource);
    }
  }
  
  public DataSet(DataSource paramDataSource)
  {
    this.a = 3;
    this.b = ((DataSource)zzx.a(paramDataSource));
    this.c = paramDataSource.a();
    this.d = new ArrayList();
    this.e = new ArrayList();
    this.e.add(this.b);
  }
  
  public DataSet(RawDataSet paramRawDataSet, List<DataSource> paramList)
  {
    this.a = 3;
    this.b = ((DataSource)a(paramList, paramRawDataSet.b));
    this.c = this.b.a();
    this.e = paramList;
    this.f = paramRawDataSet.e;
    paramRawDataSet = paramRawDataSet.d;
    this.d = new ArrayList(paramRawDataSet.size());
    paramRawDataSet = paramRawDataSet.iterator();
    while (paramRawDataSet.hasNext())
    {
      paramList = (RawDataPoint)paramRawDataSet.next();
      this.d.add(new DataPoint(this.e, paramList));
    }
  }
  
  public static DataSet a(DataSource paramDataSource)
  {
    zzx.a(paramDataSource, "DataSource should be specified");
    return new DataSet(paramDataSource);
  }
  
  private static <T> T a(List<T> paramList, int paramInt)
  {
    if ((paramInt >= 0) && (paramInt < paramList.size())) {
      return (T)paramList.get(paramInt);
    }
    return null;
  }
  
  private boolean a(DataSet paramDataSet)
  {
    return (zzw.a(b(), paramDataSet.b())) && (zzw.a(this.b, paramDataSet.b)) && (zzw.a(this.d, paramDataSet.d)) && (this.f == paramDataSet.f);
  }
  
  public DataSource a()
  {
    return this.b;
  }
  
  List<RawDataPoint> a(List<DataSource> paramList)
  {
    ArrayList localArrayList = new ArrayList(this.d.size());
    Iterator localIterator = this.d.iterator();
    while (localIterator.hasNext()) {
      localArrayList.add(new RawDataPoint((DataPoint)localIterator.next(), paramList));
    }
    return localArrayList;
  }
  
  public void a(DataPoint paramDataPoint)
  {
    this.d.add(paramDataPoint);
    paramDataPoint = paramDataPoint.c();
    if ((paramDataPoint != null) && (!this.e.contains(paramDataPoint))) {
      this.e.add(paramDataPoint);
    }
  }
  
  public void a(Iterable<DataPoint> paramIterable)
  {
    paramIterable = paramIterable.iterator();
    while (paramIterable.hasNext()) {
      a((DataPoint)paramIterable.next());
    }
  }
  
  public DataType b()
  {
    return this.b.a();
  }
  
  public List<DataPoint> c()
  {
    return Collections.unmodifiableList(this.d);
  }
  
  public boolean d()
  {
    return this.f;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  int e()
  {
    return this.a;
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof DataSet)) && (a((DataSet)paramObject)));
  }
  
  List<RawDataPoint> f()
  {
    return a(this.e);
  }
  
  List<DataSource> g()
  {
    return this.e;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b });
  }
  
  public String toString()
  {
    Object localObject = f();
    String str = this.b.g();
    if (this.d.size() < 10) {}
    for (;;)
    {
      return String.format("DataSet{%s %s}", new Object[] { str, localObject });
      localObject = String.format("%d data points, first 5: %s", new Object[] { Integer.valueOf(this.d.size()), ((List)localObject).subList(0, 5) });
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zze.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/DataSet.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */