package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzx;

public final class Application
  implements SafeParcelable
{
  public static final Parcelable.Creator<Application> CREATOR = new zza();
  public static final Application a = new Application("com.google.android.gms", String.valueOf(GooglePlayServicesUtil.a), null);
  private final int b;
  private final String c;
  private final String d;
  private final String e;
  
  Application(int paramInt, String paramString1, String paramString2, String paramString3)
  {
    this.b = paramInt;
    this.c = ((String)zzx.a(paramString1));
    this.d = "";
    this.e = paramString3;
  }
  
  public Application(String paramString1, String paramString2, String paramString3)
  {
    this(1, paramString1, "", paramString3);
  }
  
  private boolean a(Application paramApplication)
  {
    return (this.c.equals(paramApplication.c)) && (zzw.a(this.d, paramApplication.d)) && (zzw.a(this.e, paramApplication.e));
  }
  
  public String a()
  {
    return this.c;
  }
  
  public String b()
  {
    return this.d;
  }
  
  public String c()
  {
    return this.e;
  }
  
  int d()
  {
    return this.b;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof Application)) && (a((Application)paramObject)));
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.c, this.d, this.e });
  }
  
  public String toString()
  {
    return String.format("Application{%s:%s:%s}", new Object[] { this.c, this.d, this.e });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/Application.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */