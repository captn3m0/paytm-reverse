package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzoz;

public final class Device
  implements SafeParcelable
{
  public static final Parcelable.Creator<Device> CREATOR = new zzi();
  private final int a;
  private final String b;
  private final String c;
  private final String d;
  private final String e;
  private final int f;
  private final int g;
  
  Device(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, int paramInt2, int paramInt3)
  {
    this.a = paramInt1;
    this.b = ((String)zzx.a(paramString1));
    this.c = ((String)zzx.a(paramString2));
    this.d = "";
    this.e = ((String)zzx.a(paramString4));
    this.f = paramInt2;
    this.g = paramInt3;
  }
  
  private boolean a(Device paramDevice)
  {
    return (zzw.a(this.b, paramDevice.b)) && (zzw.a(this.c, paramDevice.c)) && (zzw.a(this.d, paramDevice.d)) && (zzw.a(this.e, paramDevice.e)) && (this.f == paramDevice.f) && (this.g == paramDevice.g);
  }
  
  private boolean j()
  {
    return f() == 1;
  }
  
  public String a()
  {
    return this.b;
  }
  
  public String b()
  {
    return this.c;
  }
  
  public String c()
  {
    return this.d;
  }
  
  public String d()
  {
    return this.e;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public int e()
  {
    return this.f;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof Device)) && (a((Device)paramObject)));
  }
  
  public int f()
  {
    return this.g;
  }
  
  String g()
  {
    return String.format("%s:%s:%s", new Object[] { this.b, this.c, this.e });
  }
  
  public String h()
  {
    if (j()) {
      return this.e;
    }
    return zzoz.a(this.e);
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b, this.c, this.d, this.e, Integer.valueOf(this.f) });
  }
  
  int i()
  {
    return this.a;
  }
  
  public String toString()
  {
    return String.format("Device{%s:%s:%s:%s}", new Object[] { g(), this.d, Integer.valueOf(this.f), Integer.valueOf(this.g) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzi.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/Device.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */