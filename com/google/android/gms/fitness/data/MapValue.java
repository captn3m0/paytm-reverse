package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;

public class MapValue
  implements SafeParcelable
{
  public static final Parcelable.Creator<MapValue> CREATOR = new zzm();
  private final int a;
  private final int b;
  private final float c;
  
  MapValue(int paramInt1, int paramInt2, float paramFloat)
  {
    this.a = paramInt1;
    this.b = paramInt2;
    this.c = paramFloat;
  }
  
  private boolean a(MapValue paramMapValue)
  {
    if (this.b == paramMapValue.b)
    {
      switch (this.b)
      {
      default: 
        if (this.c != paramMapValue.c) {
          break;
        }
      case 2: 
        do
        {
          return true;
        } while (a() == paramMapValue.a());
        return false;
      }
      return false;
    }
    return false;
  }
  
  public float a()
  {
    if (this.b == 2) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.a(bool, "Value is not in float format");
      return this.c;
    }
  }
  
  int b()
  {
    return this.a;
  }
  
  int c()
  {
    return this.b;
  }
  
  float d()
  {
    return this.c;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof MapValue)) && (a((MapValue)paramObject)));
  }
  
  public int hashCode()
  {
    return (int)this.c;
  }
  
  public String toString()
  {
    switch (this.b)
    {
    default: 
      return "unknown";
    }
    return Float.toString(a());
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzm.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/MapValue.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */