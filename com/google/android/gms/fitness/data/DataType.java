package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.RequiresPermission;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.zzmn;
import com.google.android.gms.internal.zzmr;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class DataType
  implements SafeParcelable
{
  public static final Set<DataType> A;
  public static final DataType B;
  public static final DataType C;
  public static final Parcelable.Creator<DataType> CREATOR = new zzg();
  public static final DataType D;
  public static final DataType E;
  public static final DataType F;
  @Deprecated
  public static final DataType G;
  public static final DataType H;
  @RequiresPermission
  public static final DataType I;
  @RequiresPermission
  public static final DataType J;
  public static final DataType K;
  public static final DataType L;
  public static final DataType M;
  public static final DataType N;
  public static final DataType O;
  public static final DataType P;
  public static final DataType Q;
  public static final DataType[] R;
  private static final Map<DataType, List<DataType>> S;
  @RequiresPermission
  @KeepName
  public static final DataType TYPE_DISTANCE_CUMULATIVE;
  @KeepName
  public static final DataType TYPE_STEP_COUNT_CUMULATIVE;
  public static final DataType a = new DataType("com.google.step_count.delta", new Field[] { Field.d });
  public static final DataType b;
  public static final DataType c;
  public static final DataType d;
  public static final DataType e;
  public static final DataType f;
  public static final DataType g;
  public static final DataType h;
  public static final DataType i;
  public static final DataType j;
  @RequiresPermission
  public static final DataType k;
  @RequiresPermission
  public static final DataType l;
  @RequiresPermission
  public static final DataType m;
  @RequiresPermission
  public static final DataType n;
  @RequiresPermission
  public static final DataType o;
  public static final DataType p;
  public static final DataType q;
  public static final DataType r;
  public static final DataType s;
  public static final DataType t;
  public static final DataType u;
  public static final DataType v;
  public static final DataType w;
  public static final DataType x;
  public static final DataType y;
  public static final DataType z;
  private final int T;
  private final String U;
  private final List<Field> V;
  
  static
  {
    TYPE_STEP_COUNT_CUMULATIVE = new DataType("com.google.step_count.cumulative", new Field[] { Field.d });
    b = new DataType("com.google.step_count.cadence", new Field[] { Field.t });
    c = new DataType("com.google.activity.segment", new Field[] { Field.a });
    d = new DataType("com.google.floor_change", new Field[] { Field.a, Field.b, Field.A, Field.D });
    e = new DataType("com.google.calories.consumed", new Field[] { Field.v });
    f = new DataType("com.google.calories.expended", new Field[] { Field.v });
    g = new DataType("com.google.calories.bmr", new Field[] { Field.v });
    h = new DataType("com.google.power.sample", new Field[] { Field.w });
    i = new DataType("com.google.activity.sample", new Field[] { Field.a, Field.b });
    j = new DataType("com.google.accelerometer", new Field[] { Field.S, Field.T, Field.U });
    k = new DataType("com.google.heart_rate.bpm", new Field[] { Field.i });
    l = new DataType("com.google.location.sample", new Field[] { Field.j, Field.k, Field.l, Field.m });
    m = new DataType("com.google.location.track", new Field[] { Field.j, Field.k, Field.l, Field.m });
    n = new DataType("com.google.distance.delta", new Field[] { Field.n });
    TYPE_DISTANCE_CUMULATIVE = new DataType("com.google.distance.cumulative", new Field[] { Field.n });
    o = new DataType("com.google.speed", new Field[] { Field.s });
    p = new DataType("com.google.cycling.wheel_revolution.cumulative", new Field[] { Field.u });
    q = new DataType("com.google.cycling.wheel_revolution.rpm", new Field[] { Field.t });
    r = new DataType("com.google.cycling.pedaling.cumulative", new Field[] { Field.u });
    s = new DataType("com.google.cycling.pedaling.cadence", new Field[] { Field.t });
    t = new DataType("com.google.height", new Field[] { Field.o });
    u = new DataType("com.google.weight", new Field[] { Field.p });
    v = new DataType("com.google.body.fat.percentage", new Field[] { Field.r });
    w = new DataType("com.google.body.waist.circumference", new Field[] { Field.q });
    x = new DataType("com.google.body.hip.circumference", new Field[] { Field.q });
    y = new DataType("com.google.nutrition", new Field[] { Field.z, Field.x, Field.y });
    z = new DataType("com.google.activity.exercise", new Field[] { Field.G, Field.H, Field.e, Field.J, Field.I });
    A = zzmr.a(new DataType[] { a, n, c, d, o, k, u, l, e, f, v, x, w, y });
    B = new DataType("com.google.activity.summary", new Field[] { Field.a, Field.e, Field.K });
    C = new DataType("com.google.floor_change.summary", new Field[] { Field.g, Field.h, Field.B, Field.C, Field.E, Field.F });
    D = new DataType("com.google.calories.bmr.summary", new Field[] { Field.L, Field.M, Field.N });
    E = a;
    F = n;
    G = e;
    H = f;
    I = new DataType("com.google.heart_rate.summary", new Field[] { Field.L, Field.M, Field.N });
    J = new DataType("com.google.location.bounding_box", new Field[] { Field.O, Field.P, Field.Q, Field.R });
    K = new DataType("com.google.power.summary", new Field[] { Field.L, Field.M, Field.N });
    L = new DataType("com.google.speed.summary", new Field[] { Field.L, Field.M, Field.N });
    M = new DataType("com.google.body.fat.percentage.summary", new Field[] { Field.L, Field.M, Field.N });
    N = new DataType("com.google.body.hip.circumference.summary", new Field[] { Field.L, Field.M, Field.N });
    O = new DataType("com.google.body.waist.circumference.summary", new Field[] { Field.L, Field.M, Field.N });
    P = new DataType("com.google.weight.summary", new Field[] { Field.L, Field.M, Field.N });
    Q = new DataType("com.google.nutrition.summary", new Field[] { Field.z, Field.x });
    S = new HashMap();
    S.put(c, Collections.singletonList(B));
    S.put(g, Collections.singletonList(D));
    S.put(v, Collections.singletonList(M));
    S.put(x, Collections.singletonList(N));
    S.put(w, Collections.singletonList(O));
    S.put(e, Collections.singletonList(G));
    S.put(f, Collections.singletonList(H));
    S.put(n, Collections.singletonList(F));
    S.put(d, Collections.singletonList(C));
    S.put(l, Collections.singletonList(J));
    S.put(y, Collections.singletonList(Q));
    S.put(h, Collections.singletonList(K));
    S.put(k, Collections.singletonList(I));
    S.put(o, Collections.singletonList(L));
    S.put(a, Collections.singletonList(E));
    S.put(u, Collections.singletonList(P));
    R = new DataType[] { j, z, i, c, B, v, M, x, N, w, O, g, D, e, f, s, r, p, q, TYPE_DISTANCE_CUMULATIVE, n, d, C, k, I, t, J, l, m, y, Q, h, K, o, L, b, TYPE_STEP_COUNT_CUMULATIVE, a, u, P };
  }
  
  DataType(int paramInt, String paramString, List<Field> paramList)
  {
    this.T = paramInt;
    this.U = paramString;
    this.V = Collections.unmodifiableList(paramList);
  }
  
  public DataType(String paramString, Field... paramVarArgs)
  {
    this(1, paramString, zzmn.a(paramVarArgs));
  }
  
  private boolean a(DataType paramDataType)
  {
    return (this.U.equals(paramDataType.U)) && (this.V.equals(paramDataType.V));
  }
  
  public String a()
  {
    return this.U;
  }
  
  public List<Field> b()
  {
    return this.V;
  }
  
  public String c()
  {
    if (this.U.startsWith("com.google.")) {
      return this.U.substring(11);
    }
    return this.U;
  }
  
  int d()
  {
    return this.T;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof DataType)) && (a((DataType)paramObject)));
  }
  
  public int hashCode()
  {
    return this.U.hashCode();
  }
  
  public String toString()
  {
    return String.format("DataType{%s%s}", new Object[] { this.U, this.V });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzg.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/DataType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */