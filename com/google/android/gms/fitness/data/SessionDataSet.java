package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;

public class SessionDataSet
  implements SafeParcelable
{
  public static final Parcelable.Creator<SessionDataSet> CREATOR = new zzr();
  final int a;
  private final Session b;
  private final DataSet c;
  
  SessionDataSet(int paramInt, Session paramSession, DataSet paramDataSet)
  {
    this.a = paramInt;
    this.b = paramSession;
    this.c = paramDataSet;
  }
  
  private boolean a(SessionDataSet paramSessionDataSet)
  {
    return (zzw.a(this.b, paramSessionDataSet.b)) && (zzw.a(this.c, paramSessionDataSet.c));
  }
  
  public Session a()
  {
    return this.b;
  }
  
  public DataSet b()
  {
    return this.c;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof SessionDataSet)) && (a((SessionDataSet)paramObject)));
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b, this.c });
  }
  
  public String toString()
  {
    return zzw.a(this).a("session", this.b).a("dataSet", this.c).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzr.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/SessionDataSet.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */