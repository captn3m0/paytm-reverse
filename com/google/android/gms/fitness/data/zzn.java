package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzn
  implements Parcelable.Creator<RawBucket>
{
  static void a(RawBucket paramRawBucket, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramRawBucket.b);
    zzb.a(paramParcel, 1000, paramRawBucket.a);
    zzb.a(paramParcel, 2, paramRawBucket.c);
    zzb.a(paramParcel, 3, paramRawBucket.d, paramInt, false);
    zzb.a(paramParcel, 4, paramRawBucket.e);
    zzb.c(paramParcel, 5, paramRawBucket.f, false);
    zzb.a(paramParcel, 6, paramRawBucket.g);
    zzb.a(paramParcel, 7, paramRawBucket.h);
    zzb.a(paramParcel, i);
  }
  
  public RawBucket a(Parcel paramParcel)
  {
    long l1 = 0L;
    ArrayList localArrayList = null;
    boolean bool = false;
    int m = zza.b(paramParcel);
    int i = 0;
    int j = 0;
    Session localSession = null;
    long l2 = 0L;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        l2 = zza.i(paramParcel, n);
        break;
      case 1000: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        l1 = zza.i(paramParcel, n);
        break;
      case 3: 
        localSession = (Session)zza.a(paramParcel, n, Session.CREATOR);
        break;
      case 4: 
        j = zza.g(paramParcel, n);
        break;
      case 5: 
        localArrayList = zza.c(paramParcel, n, RawDataSet.CREATOR);
        break;
      case 6: 
        i = zza.g(paramParcel, n);
        break;
      case 7: 
        bool = zza.c(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new RawBucket(k, l2, l1, localSession, j, localArrayList, i, bool);
  }
  
  public RawBucket[] a(int paramInt)
  {
    return new RawBucket[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/data/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */