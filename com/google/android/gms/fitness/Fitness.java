package com.google.android.gms.fitness;

import android.os.Build.VERSION;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.internal.zznz;
import com.google.android.gms.internal.zznz.zzb;
import com.google.android.gms.internal.zzoa;
import com.google.android.gms.internal.zzoa.zzb;
import com.google.android.gms.internal.zzob;
import com.google.android.gms.internal.zzob.zzb;
import com.google.android.gms.internal.zzoc;
import com.google.android.gms.internal.zzoc.zza;
import com.google.android.gms.internal.zzod;
import com.google.android.gms.internal.zzod.zzb;
import com.google.android.gms.internal.zzoe;
import com.google.android.gms.internal.zzoe.zzb;
import com.google.android.gms.internal.zzof;
import com.google.android.gms.internal.zzof.zzb;
import com.google.android.gms.internal.zzoy;
import com.google.android.gms.internal.zzpa;
import com.google.android.gms.internal.zzpb;
import com.google.android.gms.internal.zzpc;
import com.google.android.gms.internal.zzpd;
import com.google.android.gms.internal.zzpe;
import com.google.android.gms.internal.zzpf;
import com.google.android.gms.internal.zzpg;
import com.google.android.gms.internal.zzpi;

public class Fitness
{
  public static final Scope A = new Scope("https://www.googleapis.com/auth/fitness.body.read");
  public static final Scope B = new Scope("https://www.googleapis.com/auth/fitness.body.write");
  public static final Scope C = new Scope("https://www.googleapis.com/auth/fitness.nutrition.read");
  public static final Scope D = new Scope("https://www.googleapis.com/auth/fitness.nutrition.write");
  public static final Api.zzc<zznz> a = new Api.zzc();
  public static final Api.zzc<zzoa> b = new Api.zzc();
  public static final Api.zzc<zzob> c = new Api.zzc();
  public static final Api.zzc<zzoc> d = new Api.zzc();
  public static final Api.zzc<zzod> e = new Api.zzc();
  public static final Api.zzc<zzoe> f = new Api.zzc();
  public static final Api.zzc<zzof> g = new Api.zzc();
  @Deprecated
  public static final Void h = null;
  public static final Api<Api.ApiOptions.NoOptions> i = new Api("Fitness.SENSORS_API", new zzoe.zzb(), f);
  public static final SensorsApi j = new zzpf();
  public static final Api<Api.ApiOptions.NoOptions> k = new Api("Fitness.RECORDING_API", new zzod.zzb(), e);
  public static final RecordingApi l = new zzpe();
  public static final Api<Api.ApiOptions.NoOptions> m = new Api("Fitness.SESSIONS_API", new zzof.zzb(), g);
  public static final SessionsApi n = new zzpg();
  public static final Api<Api.ApiOptions.NoOptions> o = new Api("Fitness.HISTORY_API", new zzob.zzb(), c);
  public static final HistoryApi p = new zzpc();
  public static final Api<Api.ApiOptions.NoOptions> q = new Api("Fitness.CONFIG_API", new zzoa.zzb(), b);
  public static final ConfigApi r = new zzpb();
  public static final Api<Api.ApiOptions.NoOptions> s = new Api("Fitness.BLE_API", new zznz.zzb(), a);
  public static final BleApi t = a();
  public static final Api<Api.ApiOptions.NoOptions> u = new Api("Fitness.INTERNAL_API", new zzoc.zza(), d);
  public static final zzoy v = new zzpd();
  public static final Scope w = new Scope("https://www.googleapis.com/auth/fitness.activity.read");
  public static final Scope x = new Scope("https://www.googleapis.com/auth/fitness.activity.write");
  public static final Scope y = new Scope("https://www.googleapis.com/auth/fitness.location.read");
  public static final Scope z = new Scope("https://www.googleapis.com/auth/fitness.location.write");
  
  private static BleApi a()
  {
    if (Build.VERSION.SDK_INT >= 18) {
      return new zzpa();
    }
    return new zzpi();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/Fitness.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */