package com.google.android.gms.fitness.service;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.zzk;
import com.google.android.gms.fitness.data.zzk.zza;

public class FitnessSensorServiceRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<FitnessSensorServiceRequest> CREATOR = new zza();
  private final int a;
  private final DataSource b;
  private final zzk c;
  private final long d;
  private final long e;
  
  FitnessSensorServiceRequest(int paramInt, DataSource paramDataSource, IBinder paramIBinder, long paramLong1, long paramLong2)
  {
    this.a = paramInt;
    this.b = paramDataSource;
    this.c = zzk.zza.a(paramIBinder);
    this.d = paramLong1;
    this.e = paramLong2;
  }
  
  private boolean a(FitnessSensorServiceRequest paramFitnessSensorServiceRequest)
  {
    return (zzw.a(this.b, paramFitnessSensorServiceRequest.b)) && (this.d == paramFitnessSensorServiceRequest.d) && (this.e == paramFitnessSensorServiceRequest.e);
  }
  
  public DataSource a()
  {
    return this.b;
  }
  
  int b()
  {
    return this.a;
  }
  
  IBinder c()
  {
    return this.c.asBinder();
  }
  
  public long d()
  {
    return this.d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public long e()
  {
    return this.e;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof FitnessSensorServiceRequest)) && (a((FitnessSensorServiceRequest)paramObject)));
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b, Long.valueOf(this.d), Long.valueOf(this.e) });
  }
  
  public String toString()
  {
    return String.format("FitnessSensorServiceRequest{%s}", new Object[] { this.b });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/service/FitnessSensorServiceRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */