package com.google.android.gms.fitness.service;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataSource;

public class zza
  implements Parcelable.Creator<FitnessSensorServiceRequest>
{
  static void a(FitnessSensorServiceRequest paramFitnessSensorServiceRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramFitnessSensorServiceRequest.a(), paramInt, false);
    zzb.a(paramParcel, 1000, paramFitnessSensorServiceRequest.b());
    zzb.a(paramParcel, 2, paramFitnessSensorServiceRequest.c(), false);
    zzb.a(paramParcel, 3, paramFitnessSensorServiceRequest.d());
    zzb.a(paramParcel, 4, paramFitnessSensorServiceRequest.e());
    zzb.a(paramParcel, i);
  }
  
  public FitnessSensorServiceRequest a(Parcel paramParcel)
  {
    long l1 = 0L;
    IBinder localIBinder = null;
    int j = com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel);
    int i = 0;
    long l2 = 0L;
    DataSource localDataSource = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = com.google.android.gms.common.internal.safeparcel.zza.a(paramParcel);
      switch (com.google.android.gms.common.internal.safeparcel.zza.a(k))
      {
      default: 
        com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel, k);
        break;
      case 1: 
        localDataSource = (DataSource)com.google.android.gms.common.internal.safeparcel.zza.a(paramParcel, k, DataSource.CREATOR);
        break;
      case 1000: 
        i = com.google.android.gms.common.internal.safeparcel.zza.g(paramParcel, k);
        break;
      case 2: 
        localIBinder = com.google.android.gms.common.internal.safeparcel.zza.q(paramParcel, k);
        break;
      case 3: 
        l2 = com.google.android.gms.common.internal.safeparcel.zza.i(paramParcel, k);
        break;
      case 4: 
        l1 = com.google.android.gms.common.internal.safeparcel.zza.i(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new FitnessSensorServiceRequest(i, localDataSource, localIBinder, l2, l1);
  }
  
  public FitnessSensorServiceRequest[] a(int paramInt)
  {
    return new FitnessSensorServiceRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/service/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */