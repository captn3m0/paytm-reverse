package com.google.android.gms.fitness.service;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.app.Service;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.internal.service.FitnessDataSourcesRequest;
import com.google.android.gms.fitness.internal.service.FitnessUnregistrationRequest;
import com.google.android.gms.fitness.internal.service.zzc.zza;
import com.google.android.gms.fitness.result.DataSourcesResult;
import com.google.android.gms.internal.zzne;
import com.google.android.gms.internal.zzoi;
import com.google.android.gms.internal.zzow;
import java.util.List;

public abstract class FitnessSensorService
  extends Service
{
  public abstract List<DataSource> a(List<DataType> paramList);
  
  @TargetApi(19)
  protected void a()
    throws SecurityException
  {
    int i = Binder.getCallingUid();
    if (zzne.h())
    {
      ((AppOpsManager)getSystemService("appops")).checkPackage(i, "com.google.android.gms");
      return;
    }
    String[] arrayOfString = getPackageManager().getPackagesForUid(i);
    if (arrayOfString != null)
    {
      int j = arrayOfString.length;
      i = 0;
      for (;;)
      {
        if (i >= j) {
          break label67;
        }
        if (arrayOfString[i].equals("com.google.android.gms")) {
          break;
        }
        i += 1;
      }
    }
    label67:
    throw new SecurityException("Unauthorized caller");
  }
  
  public abstract boolean a(DataSource paramDataSource);
  
  public abstract boolean a(FitnessSensorServiceRequest paramFitnessSensorServiceRequest);
  
  private static class zza
    extends zzc.zza
  {
    private final FitnessSensorService a;
    
    public void a(FitnessDataSourcesRequest paramFitnessDataSourcesRequest, zzoi paramzzoi)
      throws RemoteException
    {
      this.a.a();
      paramzzoi.a(new DataSourcesResult(this.a.a(paramFitnessDataSourcesRequest.a()), Status.a));
    }
    
    public void a(FitnessUnregistrationRequest paramFitnessUnregistrationRequest, zzow paramzzow)
      throws RemoteException
    {
      this.a.a();
      if (this.a.a(paramFitnessUnregistrationRequest.a()))
      {
        paramzzow.a(Status.a);
        return;
      }
      paramzzow.a(new Status(13));
    }
    
    public void a(FitnessSensorServiceRequest paramFitnessSensorServiceRequest, zzow paramzzow)
      throws RemoteException
    {
      this.a.a();
      if (this.a.a(paramFitnessSensorServiceRequest))
      {
        paramzzow.a(Status.a);
        return;
      }
      paramzzow.a(new Status(13));
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/service/FitnessSensorService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */