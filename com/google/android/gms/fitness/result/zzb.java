package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.fitness.data.DataSet;

public class zzb
  implements Parcelable.Creator<DailyTotalResult>
{
  static void a(DailyTotalResult paramDailyTotalResult, Parcel paramParcel, int paramInt)
  {
    int i = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramDailyTotalResult.getStatus(), paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1000, paramDailyTotalResult.b());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramDailyTotalResult.a(), paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, i);
  }
  
  public DailyTotalResult a(Parcel paramParcel)
  {
    DataSet localDataSet = null;
    int j = zza.b(paramParcel);
    int i = 0;
    Status localStatus = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
      }
      for (;;)
      {
        break;
        localStatus = (Status)zza.a(paramParcel, k, Status.CREATOR);
        continue;
        i = zza.g(paramParcel, k);
        continue;
        localDataSet = (DataSet)zza.a(paramParcel, k, DataSet.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new DailyTotalResult(i, localStatus, localDataSet);
  }
  
  public DailyTotalResult[] a(int paramInt)
  {
    return new DailyTotalResult[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/result/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */