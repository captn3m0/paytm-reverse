package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.Collections;
import java.util.List;

public class ReadRawResult
  implements Result, SafeParcelable
{
  public static final Parcelable.Creator<ReadRawResult> CREATOR = new zzi();
  private final int a;
  private final DataHolder b;
  private final List<DataHolder> c;
  
  ReadRawResult(int paramInt, DataHolder paramDataHolder, List<DataHolder> paramList)
  {
    this.a = paramInt;
    this.b = paramDataHolder;
    Object localObject = paramList;
    if (paramList == null) {
      localObject = Collections.singletonList(paramDataHolder);
    }
    this.c = ((List)localObject);
  }
  
  int a()
  {
    return this.a;
  }
  
  DataHolder b()
  {
    return this.b;
  }
  
  public List<DataHolder> c()
  {
    return this.c;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Status getStatus()
  {
    return new Status(this.b.e());
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzi.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/result/ReadRawResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */