package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.fitness.data.DataSource;
import java.util.Collections;
import java.util.List;

public class DataSourcesResult
  implements Result, SafeParcelable
{
  public static final Parcelable.Creator<DataSourcesResult> CREATOR = new zze();
  private final int a;
  private final List<DataSource> b;
  private final Status c;
  
  DataSourcesResult(int paramInt, List<DataSource> paramList, Status paramStatus)
  {
    this.a = paramInt;
    this.b = Collections.unmodifiableList(paramList);
    this.c = paramStatus;
  }
  
  public DataSourcesResult(List<DataSource> paramList, Status paramStatus)
  {
    this.a = 3;
    this.b = Collections.unmodifiableList(paramList);
    this.c = paramStatus;
  }
  
  public static DataSourcesResult a(Status paramStatus)
  {
    return new DataSourcesResult(Collections.emptyList(), paramStatus);
  }
  
  private boolean a(DataSourcesResult paramDataSourcesResult)
  {
    return (this.c.equals(paramDataSourcesResult.c)) && (zzw.a(this.b, paramDataSourcesResult.b));
  }
  
  public List<DataSource> a()
  {
    return this.b;
  }
  
  int b()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof DataSourcesResult)) && (a((DataSourcesResult)paramObject)));
  }
  
  public Status getStatus()
  {
    return this.c;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.c, this.b });
  }
  
  public String toString()
  {
    return zzw.a(this).a("status", this.c).a("dataSets", this.b).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zze.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/result/DataSourcesResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */