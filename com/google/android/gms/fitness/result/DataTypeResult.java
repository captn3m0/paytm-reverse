package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.fitness.data.DataType;

public class DataTypeResult
  implements Result, SafeParcelable
{
  public static final Parcelable.Creator<DataTypeResult> CREATOR = new zzg();
  private final int a;
  private final Status b;
  private final DataType c;
  
  DataTypeResult(int paramInt, Status paramStatus, DataType paramDataType)
  {
    this.a = paramInt;
    this.b = paramStatus;
    this.c = paramDataType;
  }
  
  public DataTypeResult(Status paramStatus, DataType paramDataType)
  {
    this.a = 2;
    this.b = paramStatus;
    this.c = paramDataType;
  }
  
  public static DataTypeResult a(Status paramStatus)
  {
    return new DataTypeResult(paramStatus, null);
  }
  
  private boolean a(DataTypeResult paramDataTypeResult)
  {
    return (this.b.equals(paramDataTypeResult.b)) && (zzw.a(this.c, paramDataTypeResult.c));
  }
  
  public DataType a()
  {
    return this.c;
  }
  
  int b()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof DataTypeResult)) && (a((DataTypeResult)paramObject)));
  }
  
  public Status getStatus()
  {
    return this.b;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b, this.c });
  }
  
  public String toString()
  {
    return zzw.a(this).a("status", this.b).a("dataType", this.c).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzg.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/result/DataTypeResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */