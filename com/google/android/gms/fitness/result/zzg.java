package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataType;

public class zzg
  implements Parcelable.Creator<DataTypeResult>
{
  static void a(DataTypeResult paramDataTypeResult, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramDataTypeResult.getStatus(), paramInt, false);
    zzb.a(paramParcel, 1000, paramDataTypeResult.b());
    zzb.a(paramParcel, 3, paramDataTypeResult.a(), paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public DataTypeResult a(Parcel paramParcel)
  {
    DataType localDataType = null;
    int j = zza.b(paramParcel);
    int i = 0;
    Status localStatus = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
      }
      for (;;)
      {
        break;
        localStatus = (Status)zza.a(paramParcel, k, Status.CREATOR);
        continue;
        i = zza.g(paramParcel, k);
        continue;
        localDataType = (DataType)zza.a(paramParcel, k, DataType.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new DataTypeResult(i, localStatus, localDataType);
  }
  
  public DataTypeResult[] a(int paramInt)
  {
    return new DataTypeResult[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/result/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */