package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzi
  implements Parcelable.Creator<ReadRawResult>
{
  static void a(ReadRawResult paramReadRawResult, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramReadRawResult.b(), paramInt, false);
    zzb.a(paramParcel, 1000, paramReadRawResult.a());
    zzb.c(paramParcel, 2, paramReadRawResult.c(), false);
    zzb.a(paramParcel, i);
  }
  
  public ReadRawResult a(Parcel paramParcel)
  {
    ArrayList localArrayList = null;
    int j = zza.b(paramParcel);
    int i = 0;
    DataHolder localDataHolder = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
      }
      for (;;)
      {
        break;
        localDataHolder = (DataHolder)zza.a(paramParcel, k, DataHolder.CREATOR);
        continue;
        i = zza.g(paramParcel, k);
        continue;
        localArrayList = zza.c(paramParcel, k, DataHolder.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new ReadRawResult(i, localDataHolder, localArrayList);
  }
  
  public ReadRawResult[] a(int paramInt)
  {
    return new ReadRawResult[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/result/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */