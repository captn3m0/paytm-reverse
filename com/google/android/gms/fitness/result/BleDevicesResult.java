package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.fitness.data.BleDevice;
import java.util.Collections;
import java.util.List;

public class BleDevicesResult
  implements Result, SafeParcelable
{
  public static final Parcelable.Creator<BleDevicesResult> CREATOR = new zza();
  private final int a;
  private final List<BleDevice> b;
  private final Status c;
  
  BleDevicesResult(int paramInt, List<BleDevice> paramList, Status paramStatus)
  {
    this.a = paramInt;
    this.b = Collections.unmodifiableList(paramList);
    this.c = paramStatus;
  }
  
  public BleDevicesResult(List<BleDevice> paramList, Status paramStatus)
  {
    this.a = 3;
    this.b = Collections.unmodifiableList(paramList);
    this.c = paramStatus;
  }
  
  public static BleDevicesResult a(Status paramStatus)
  {
    return new BleDevicesResult(Collections.emptyList(), paramStatus);
  }
  
  private boolean a(BleDevicesResult paramBleDevicesResult)
  {
    return (this.c.equals(paramBleDevicesResult.c)) && (zzw.a(this.b, paramBleDevicesResult.b));
  }
  
  public List<BleDevice> a()
  {
    return this.b;
  }
  
  int b()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof BleDevicesResult)) && (a((BleDevicesResult)paramObject)));
  }
  
  public Status getStatus()
  {
    return this.c;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.c, this.b });
  }
  
  public String toString()
  {
    return zzw.a(this).a("status", this.c).a("bleDevices", this.b).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/result/BleDevicesResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */