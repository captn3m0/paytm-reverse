package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzl
  implements Parcelable.Creator<SyncInfoResult>
{
  static void a(SyncInfoResult paramSyncInfoResult, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramSyncInfoResult.getStatus(), paramInt, false);
    zzb.a(paramParcel, 1000, paramSyncInfoResult.b());
    zzb.a(paramParcel, 2, paramSyncInfoResult.a());
    zzb.a(paramParcel, i);
  }
  
  public SyncInfoResult a(Parcel paramParcel)
  {
    int j = zza.b(paramParcel);
    int i = 0;
    Status localStatus = null;
    long l = 0L;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
      }
      for (;;)
      {
        break;
        localStatus = (Status)zza.a(paramParcel, k, Status.CREATOR);
        continue;
        i = zza.g(paramParcel, k);
        continue;
        l = zza.i(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new SyncInfoResult(i, localStatus, l);
  }
  
  public SyncInfoResult[] a(int paramInt)
  {
    return new SyncInfoResult[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/result/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */