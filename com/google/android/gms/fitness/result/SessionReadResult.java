package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.fitness.data.Session;
import com.google.android.gms.fitness.data.SessionDataSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SessionReadResult
  implements Result, SafeParcelable
{
  public static final Parcelable.Creator<SessionReadResult> CREATOR = new zzj();
  private final int a;
  private final List<Session> b;
  private final List<SessionDataSet> c;
  private final Status d;
  
  SessionReadResult(int paramInt, List<Session> paramList, List<SessionDataSet> paramList1, Status paramStatus)
  {
    this.a = paramInt;
    this.b = paramList;
    this.c = Collections.unmodifiableList(paramList1);
    this.d = paramStatus;
  }
  
  public SessionReadResult(List<Session> paramList, List<SessionDataSet> paramList1, Status paramStatus)
  {
    this.a = 3;
    this.b = paramList;
    this.c = Collections.unmodifiableList(paramList1);
    this.d = paramStatus;
  }
  
  public static SessionReadResult a(Status paramStatus)
  {
    return new SessionReadResult(new ArrayList(), new ArrayList(), paramStatus);
  }
  
  private boolean a(SessionReadResult paramSessionReadResult)
  {
    return (this.d.equals(paramSessionReadResult.d)) && (zzw.a(this.b, paramSessionReadResult.b)) && (zzw.a(this.c, paramSessionReadResult.c));
  }
  
  public List<Session> a()
  {
    return this.b;
  }
  
  public List<SessionDataSet> b()
  {
    return this.c;
  }
  
  int c()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof SessionReadResult)) && (a((SessionReadResult)paramObject)));
  }
  
  public Status getStatus()
  {
    return this.d;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.d, this.b, this.c });
  }
  
  public String toString()
  {
    return zzw.a(this).a("status", this.d).a("sessions", this.b).a("sessionDataSets", this.c).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzj.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/result/SessionReadResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */