package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;

public class SyncInfoResult
  implements Result, SafeParcelable
{
  public static final Parcelable.Creator<SyncInfoResult> CREATOR = new zzl();
  private final int a;
  private final Status b;
  private final long c;
  
  SyncInfoResult(int paramInt, Status paramStatus, long paramLong)
  {
    this.a = paramInt;
    this.b = paramStatus;
    this.c = paramLong;
  }
  
  private boolean a(SyncInfoResult paramSyncInfoResult)
  {
    return (this.b.equals(paramSyncInfoResult.b)) && (zzw.a(Long.valueOf(this.c), Long.valueOf(paramSyncInfoResult.c)));
  }
  
  public long a()
  {
    return this.c;
  }
  
  int b()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof SyncInfoResult)) && (a((SyncInfoResult)paramObject)));
  }
  
  public Status getStatus()
  {
    return this.b;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b, Long.valueOf(this.c) });
  }
  
  public String toString()
  {
    return zzw.a(this).a("status", this.b).a("timestamp", Long.valueOf(this.c)).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzl.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/result/SyncInfoResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */