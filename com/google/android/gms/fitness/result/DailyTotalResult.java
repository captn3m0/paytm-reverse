package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource.Builder;
import com.google.android.gms.fitness.data.DataType;

public class DailyTotalResult
  implements Result, SafeParcelable
{
  public static final Parcelable.Creator<DailyTotalResult> CREATOR = new zzb();
  private final int a;
  private final Status b;
  private final DataSet c;
  
  DailyTotalResult(int paramInt, Status paramStatus, DataSet paramDataSet)
  {
    this.a = paramInt;
    this.b = paramStatus;
    this.c = paramDataSet;
  }
  
  public DailyTotalResult(DataSet paramDataSet, Status paramStatus)
  {
    this.a = 1;
    this.b = paramStatus;
    this.c = paramDataSet;
  }
  
  public static DailyTotalResult a(Status paramStatus, DataType paramDataType)
  {
    return new DailyTotalResult(DataSet.a(new DataSource.Builder().a(paramDataType).a(1).a()), paramStatus);
  }
  
  private boolean a(DailyTotalResult paramDailyTotalResult)
  {
    return (this.b.equals(paramDailyTotalResult.b)) && (zzw.a(this.c, paramDailyTotalResult.c));
  }
  
  @Nullable
  public DataSet a()
  {
    return this.c;
  }
  
  int b()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof DailyTotalResult)) && (a((DailyTotalResult)paramObject)));
  }
  
  public Status getStatus()
  {
    return this.b;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b, this.c });
  }
  
  public String toString()
  {
    return zzw.a(this).a("status", this.b).a("dataPoint", this.c).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/result/DailyTotalResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */