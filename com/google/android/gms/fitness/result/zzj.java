package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.Session;
import com.google.android.gms.fitness.data.SessionDataSet;
import java.util.ArrayList;

public class zzj
  implements Parcelable.Creator<SessionReadResult>
{
  static void a(SessionReadResult paramSessionReadResult, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.c(paramParcel, 1, paramSessionReadResult.a(), false);
    zzb.a(paramParcel, 1000, paramSessionReadResult.c());
    zzb.c(paramParcel, 2, paramSessionReadResult.b(), false);
    zzb.a(paramParcel, 3, paramSessionReadResult.getStatus(), paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public SessionReadResult a(Parcel paramParcel)
  {
    Status localStatus = null;
    int j = zza.b(paramParcel);
    int i = 0;
    ArrayList localArrayList2 = null;
    ArrayList localArrayList1 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localArrayList1 = zza.c(paramParcel, k, Session.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localArrayList2 = zza.c(paramParcel, k, SessionDataSet.CREATOR);
        break;
      case 3: 
        localStatus = (Status)zza.a(paramParcel, k, Status.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new SessionReadResult(i, localArrayList1, localArrayList2, localStatus);
  }
  
  public SessionReadResult[] a(int paramInt)
  {
    return new SessionReadResult[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/result/zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */