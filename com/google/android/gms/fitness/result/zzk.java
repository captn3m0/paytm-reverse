package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.Session;
import java.util.ArrayList;

public class zzk
  implements Parcelable.Creator<SessionStopResult>
{
  static void a(SessionStopResult paramSessionStopResult, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1000, paramSessionStopResult.b());
    zzb.a(paramParcel, 2, paramSessionStopResult.getStatus(), paramInt, false);
    zzb.c(paramParcel, 3, paramSessionStopResult.a(), false);
    zzb.a(paramParcel, i);
  }
  
  public SessionStopResult a(Parcel paramParcel)
  {
    ArrayList localArrayList = null;
    int j = zza.b(paramParcel);
    int i = 0;
    Status localStatus = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
      }
      for (;;)
      {
        break;
        i = zza.g(paramParcel, k);
        continue;
        localStatus = (Status)zza.a(paramParcel, k, Status.CREATOR);
        continue;
        localArrayList = zza.c(paramParcel, k, Session.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new SessionStopResult(i, localStatus, localArrayList);
  }
  
  public SessionStopResult[] a(int paramInt)
  {
    return new SessionStopResult[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/result/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */