package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataSource;

public class zzd
  implements Parcelable.Creator<DataSourceStatsResult>
{
  static void a(DataSourceStatsResult paramDataSourceStatsResult, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramDataSourceStatsResult.b, paramInt, false);
    zzb.a(paramParcel, 1000, paramDataSourceStatsResult.a);
    zzb.a(paramParcel, 2, paramDataSourceStatsResult.c);
    zzb.a(paramParcel, 3, paramDataSourceStatsResult.d);
    zzb.a(paramParcel, 4, paramDataSourceStatsResult.e);
    zzb.a(paramParcel, 5, paramDataSourceStatsResult.f);
    zzb.a(paramParcel, i);
  }
  
  public DataSourceStatsResult a(Parcel paramParcel)
  {
    boolean bool = false;
    long l1 = 0L;
    int j = zza.b(paramParcel);
    DataSource localDataSource = null;
    long l2 = 0L;
    long l3 = 0L;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localDataSource = (DataSource)zza.a(paramParcel, k, DataSource.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        l3 = zza.i(paramParcel, k);
        break;
      case 3: 
        bool = zza.c(paramParcel, k);
        break;
      case 4: 
        l2 = zza.i(paramParcel, k);
        break;
      case 5: 
        l1 = zza.i(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new DataSourceStatsResult(i, localDataSource, l3, bool, l2, l1);
  }
  
  public DataSourceStatsResult[] a(int paramInt)
  {
    return new DataSourceStatsResult[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/result/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */