package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.fitness.data.Subscription;
import java.util.Collections;
import java.util.List;

public class ListSubscriptionsResult
  implements Result, SafeParcelable
{
  public static final Parcelable.Creator<ListSubscriptionsResult> CREATOR = new zzh();
  private final int a;
  private final List<Subscription> b;
  private final Status c;
  
  ListSubscriptionsResult(int paramInt, List<Subscription> paramList, Status paramStatus)
  {
    this.a = paramInt;
    this.b = paramList;
    this.c = paramStatus;
  }
  
  public ListSubscriptionsResult(List<Subscription> paramList, Status paramStatus)
  {
    this.a = 3;
    this.b = Collections.unmodifiableList(paramList);
    this.c = ((Status)zzx.a(paramStatus, "status"));
  }
  
  public static ListSubscriptionsResult a(Status paramStatus)
  {
    return new ListSubscriptionsResult(Collections.emptyList(), paramStatus);
  }
  
  private boolean a(ListSubscriptionsResult paramListSubscriptionsResult)
  {
    return (this.c.equals(paramListSubscriptionsResult.c)) && (zzw.a(this.b, paramListSubscriptionsResult.b));
  }
  
  public List<Subscription> a()
  {
    return this.b;
  }
  
  int b()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof ListSubscriptionsResult)) && (a((ListSubscriptionsResult)paramObject)));
  }
  
  public Status getStatus()
  {
    return this.c;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.c, this.b });
  }
  
  public String toString()
  {
    return zzw.a(this).a("status", this.c).a("subscriptions", this.b).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzh.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/result/ListSubscriptionsResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */