package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataSource;
import java.util.ArrayList;

public class zze
  implements Parcelable.Creator<DataSourcesResult>
{
  static void a(DataSourcesResult paramDataSourcesResult, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.c(paramParcel, 1, paramDataSourcesResult.a(), false);
    zzb.a(paramParcel, 1000, paramDataSourcesResult.b());
    zzb.a(paramParcel, 2, paramDataSourcesResult.getStatus(), paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public DataSourcesResult a(Parcel paramParcel)
  {
    Status localStatus = null;
    int j = zza.b(paramParcel);
    int i = 0;
    ArrayList localArrayList = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localArrayList = zza.c(paramParcel, k, DataSource.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localStatus = (Status)zza.a(paramParcel, k, Status.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new DataSourcesResult(i, localArrayList, localStatus);
  }
  
  public DataSourcesResult[] a(int paramInt)
  {
    return new DataSourcesResult[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/result/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */