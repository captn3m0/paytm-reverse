package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.Subscription;
import java.util.ArrayList;

public class zzh
  implements Parcelable.Creator<ListSubscriptionsResult>
{
  static void a(ListSubscriptionsResult paramListSubscriptionsResult, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.c(paramParcel, 1, paramListSubscriptionsResult.a(), false);
    zzb.a(paramParcel, 1000, paramListSubscriptionsResult.b());
    zzb.a(paramParcel, 2, paramListSubscriptionsResult.getStatus(), paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public ListSubscriptionsResult a(Parcel paramParcel)
  {
    Status localStatus = null;
    int j = zza.b(paramParcel);
    int i = 0;
    ArrayList localArrayList = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localArrayList = zza.c(paramParcel, k, Subscription.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localStatus = (Status)zza.a(paramParcel, k, Status.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new ListSubscriptionsResult(i, localArrayList, localStatus);
  }
  
  public ListSubscriptionsResult[] a(int paramInt)
  {
    return new ListSubscriptionsResult[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/result/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */