package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataSource.Builder;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.RawBucket;
import com.google.android.gms.fitness.data.RawDataSet;
import com.google.android.gms.fitness.request.DataReadRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class DataReadResult
  implements Result, SafeParcelable
{
  public static final Parcelable.Creator<DataReadResult> CREATOR = new zzc();
  private final int a;
  private final List<DataSet> b;
  private final Status c;
  private final List<Bucket> d;
  private int e;
  private final List<DataSource> f;
  private final List<DataType> g;
  
  DataReadResult(int paramInt1, List<RawDataSet> paramList, Status paramStatus, List<RawBucket> paramList1, int paramInt2, List<DataSource> paramList2, List<DataType> paramList3)
  {
    this.a = paramInt1;
    this.c = paramStatus;
    this.e = paramInt2;
    this.f = paramList2;
    this.g = paramList3;
    this.b = new ArrayList(paramList.size());
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      paramStatus = (RawDataSet)paramList.next();
      this.b.add(new DataSet(paramStatus, paramList2));
    }
    this.d = new ArrayList(paramList1.size());
    paramList = paramList1.iterator();
    while (paramList.hasNext())
    {
      paramStatus = (RawBucket)paramList.next();
      this.d.add(new Bucket(paramStatus, paramList2));
    }
  }
  
  public DataReadResult(List<DataSet> paramList, List<Bucket> paramList1, Status paramStatus)
  {
    this.a = 5;
    this.b = paramList;
    this.c = paramStatus;
    this.d = paramList1;
    this.e = 1;
    this.f = new ArrayList();
    this.g = new ArrayList();
  }
  
  public static DataReadResult a(Status paramStatus, DataReadRequest paramDataReadRequest)
  {
    ArrayList localArrayList = new ArrayList();
    Object localObject = paramDataReadRequest.b().iterator();
    while (((Iterator)localObject).hasNext()) {
      localArrayList.add(DataSet.a((DataSource)((Iterator)localObject).next()));
    }
    paramDataReadRequest = paramDataReadRequest.a().iterator();
    while (paramDataReadRequest.hasNext())
    {
      localObject = (DataType)paramDataReadRequest.next();
      localArrayList.add(DataSet.a(new DataSource.Builder().a((DataType)localObject).a(1).a("Default").a()));
    }
    return new DataReadResult(localArrayList, Collections.emptyList(), paramStatus);
  }
  
  private void a(Bucket paramBucket, List<Bucket> paramList)
  {
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      paramList = (Bucket)localIterator.next();
      if (paramList.a(paramBucket))
      {
        paramBucket = paramBucket.c().iterator();
        while (paramBucket.hasNext()) {
          a((DataSet)paramBucket.next(), paramList.c());
        }
      }
    }
    this.d.add(paramBucket);
  }
  
  private void a(DataSet paramDataSet, List<DataSet> paramList)
  {
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      DataSet localDataSet = (DataSet)localIterator.next();
      if (localDataSet.a().equals(paramDataSet.a()))
      {
        localDataSet.a(paramDataSet.c());
        return;
      }
    }
    paramList.add(paramDataSet);
  }
  
  private boolean b(DataReadResult paramDataReadResult)
  {
    return (this.c.equals(paramDataReadResult.c)) && (zzw.a(this.b, paramDataReadResult.b)) && (zzw.a(this.d, paramDataReadResult.d));
  }
  
  public List<DataSet> a()
  {
    return this.b;
  }
  
  public void a(DataReadResult paramDataReadResult)
  {
    Iterator localIterator = paramDataReadResult.a().iterator();
    while (localIterator.hasNext()) {
      a((DataSet)localIterator.next(), this.b);
    }
    paramDataReadResult = paramDataReadResult.b().iterator();
    while (paramDataReadResult.hasNext()) {
      a((Bucket)paramDataReadResult.next(), this.d);
    }
  }
  
  public List<Bucket> b()
  {
    return this.d;
  }
  
  public int c()
  {
    return this.e;
  }
  
  int d()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  List<RawBucket> e()
  {
    ArrayList localArrayList = new ArrayList(this.d.size());
    Iterator localIterator = this.d.iterator();
    while (localIterator.hasNext()) {
      localArrayList.add(new RawBucket((Bucket)localIterator.next(), this.f, this.g));
    }
    return localArrayList;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof DataReadResult)) && (b((DataReadResult)paramObject)));
  }
  
  List<RawDataSet> f()
  {
    ArrayList localArrayList = new ArrayList(this.b.size());
    Iterator localIterator = this.b.iterator();
    while (localIterator.hasNext()) {
      localArrayList.add(new RawDataSet((DataSet)localIterator.next(), this.f, this.g));
    }
    return localArrayList;
  }
  
  List<DataSource> g()
  {
    return this.f;
  }
  
  public Status getStatus()
  {
    return this.c;
  }
  
  List<DataType> h()
  {
    return this.g;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.c, this.b, this.d });
  }
  
  public String toString()
  {
    zzw.zza localzza = zzw.a(this).a("status", this.c);
    if (this.b.size() > 5)
    {
      localObject = this.b.size() + " data sets";
      localzza = localzza.a("dataSets", localObject);
      if (this.d.size() <= 5) {
        break label123;
      }
    }
    label123:
    for (Object localObject = this.d.size() + " buckets";; localObject = this.d)
    {
      return localzza.a("buckets", localObject).toString();
      localObject = this.b;
      break;
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/result/DataReadResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */