package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;

public class zzc
  implements Parcelable.Creator<DataReadResult>
{
  static void a(DataReadResult paramDataReadResult, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.d(paramParcel, 1, paramDataReadResult.f(), false);
    zzb.a(paramParcel, 1000, paramDataReadResult.d());
    zzb.a(paramParcel, 2, paramDataReadResult.getStatus(), paramInt, false);
    zzb.d(paramParcel, 3, paramDataReadResult.e(), false);
    zzb.a(paramParcel, 5, paramDataReadResult.c());
    zzb.c(paramParcel, 6, paramDataReadResult.g(), false);
    zzb.c(paramParcel, 7, paramDataReadResult.h(), false);
    zzb.a(paramParcel, i);
  }
  
  public DataReadResult a(Parcel paramParcel)
  {
    int i = 0;
    ArrayList localArrayList1 = null;
    int k = zza.b(paramParcel);
    ArrayList localArrayList3 = new ArrayList();
    ArrayList localArrayList4 = new ArrayList();
    ArrayList localArrayList2 = null;
    Status localStatus = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        zza.a(paramParcel, m, localArrayList3, getClass().getClassLoader());
        break;
      case 1000: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        localStatus = (Status)zza.a(paramParcel, m, Status.CREATOR);
        break;
      case 3: 
        zza.a(paramParcel, m, localArrayList4, getClass().getClassLoader());
        break;
      case 5: 
        i = zza.g(paramParcel, m);
        break;
      case 6: 
        localArrayList2 = zza.c(paramParcel, m, DataSource.CREATOR);
        break;
      case 7: 
        localArrayList1 = zza.c(paramParcel, m, DataType.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new DataReadResult(j, localArrayList3, localStatus, localArrayList4, i, localArrayList2, localArrayList1);
  }
  
  public DataReadResult[] a(int paramInt)
  {
    return new DataReadResult[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/result/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */