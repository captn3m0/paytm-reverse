package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.List;

public class DataStatsResult
  implements Result, SafeParcelable
{
  public static final Parcelable.Creator<DataStatsResult> CREATOR = new zzf();
  private final int a;
  private final Status b;
  private final List<DataSourceStatsResult> c;
  
  DataStatsResult(int paramInt, Status paramStatus, List<DataSourceStatsResult> paramList)
  {
    this.a = paramInt;
    this.b = paramStatus;
    this.c = paramList;
  }
  
  int a()
  {
    return this.a;
  }
  
  List<DataSourceStatsResult> b()
  {
    return this.c;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Status getStatus()
  {
    return this.b;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/result/DataStatsResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */