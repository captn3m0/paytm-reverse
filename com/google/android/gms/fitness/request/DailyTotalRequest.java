package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.internal.zzog;
import com.google.android.gms.internal.zzog.zza;

public class DailyTotalRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<DailyTotalRequest> CREATOR = new zzc();
  private final int a;
  private final zzog b;
  private DataType c;
  
  DailyTotalRequest(int paramInt, IBinder paramIBinder, DataType paramDataType)
  {
    this.a = paramInt;
    this.b = zzog.zza.a(paramIBinder);
    this.c = paramDataType;
  }
  
  public DailyTotalRequest(zzog paramzzog, DataType paramDataType)
  {
    this.a = 2;
    this.b = paramzzog;
    this.c = paramDataType;
  }
  
  int a()
  {
    return this.a;
  }
  
  public DataType b()
  {
    return this.c;
  }
  
  public IBinder c()
  {
    return this.b.asBinder();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String toString()
  {
    return String.format("DailyTotalRequest{%s}", new Object[] { this.c.c() });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/DailyTotalRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */