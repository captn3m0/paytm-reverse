package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzr
  implements Parcelable.Creator<ListClaimedBleDevicesRequest>
{
  static void a(ListClaimedBleDevicesRequest paramListClaimedBleDevicesRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramListClaimedBleDevicesRequest.b(), false);
    zzb.a(paramParcel, 1000, paramListClaimedBleDevicesRequest.a());
    zzb.a(paramParcel, paramInt);
  }
  
  public ListClaimedBleDevicesRequest a(Parcel paramParcel)
  {
    int j = zza.b(paramParcel);
    int i = 0;
    IBinder localIBinder = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localIBinder = zza.q(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new ListClaimedBleDevicesRequest(i, localIBinder);
  }
  
  public ListClaimedBleDevicesRequest[] a(int paramInt)
  {
    return new ListClaimedBleDevicesRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */