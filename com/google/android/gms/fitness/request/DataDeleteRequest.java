package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Session;
import com.google.android.gms.internal.zzow;
import com.google.android.gms.internal.zzow.zza;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DataDeleteRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<DataDeleteRequest> CREATOR = new zzd();
  private final int a;
  private final long b;
  private final long c;
  private final List<DataSource> d;
  private final List<DataType> e;
  private final List<Session> f;
  private final boolean g;
  private final boolean h;
  private final zzow i;
  
  DataDeleteRequest(int paramInt, long paramLong1, long paramLong2, List<DataSource> paramList, List<DataType> paramList1, List<Session> paramList2, boolean paramBoolean1, boolean paramBoolean2, IBinder paramIBinder)
  {
    this.a = paramInt;
    this.b = paramLong1;
    this.c = paramLong2;
    this.d = Collections.unmodifiableList(paramList);
    this.e = Collections.unmodifiableList(paramList1);
    this.f = paramList2;
    this.g = paramBoolean1;
    this.h = paramBoolean2;
    this.i = zzow.zza.a(paramIBinder);
  }
  
  public DataDeleteRequest(long paramLong1, long paramLong2, List<DataSource> paramList, List<DataType> paramList1, List<Session> paramList2, boolean paramBoolean1, boolean paramBoolean2, zzow paramzzow)
  {
    this.a = 3;
    this.b = paramLong1;
    this.c = paramLong2;
    this.d = Collections.unmodifiableList(paramList);
    this.e = Collections.unmodifiableList(paramList1);
    this.f = paramList2;
    this.g = paramBoolean1;
    this.h = paramBoolean2;
    this.i = paramzzow;
  }
  
  public DataDeleteRequest(DataDeleteRequest paramDataDeleteRequest, zzow paramzzow)
  {
    this(paramDataDeleteRequest.b, paramDataDeleteRequest.c, paramDataDeleteRequest.d, paramDataDeleteRequest.e, paramDataDeleteRequest.f, paramDataDeleteRequest.g, paramDataDeleteRequest.h, paramzzow);
  }
  
  private boolean a(DataDeleteRequest paramDataDeleteRequest)
  {
    return (this.b == paramDataDeleteRequest.b) && (this.c == paramDataDeleteRequest.c) && (zzw.a(this.d, paramDataDeleteRequest.d)) && (zzw.a(this.e, paramDataDeleteRequest.e)) && (zzw.a(this.f, paramDataDeleteRequest.f)) && (this.g == paramDataDeleteRequest.g) && (this.h == paramDataDeleteRequest.h);
  }
  
  public List<DataSource> a()
  {
    return this.d;
  }
  
  public List<DataType> b()
  {
    return this.e;
  }
  
  public List<Session> c()
  {
    return this.f;
  }
  
  int d()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean e()
  {
    return this.g;
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof DataDeleteRequest)) && (a((DataDeleteRequest)paramObject)));
  }
  
  public boolean f()
  {
    return this.h;
  }
  
  public long g()
  {
    return this.c;
  }
  
  public long h()
  {
    return this.b;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Long.valueOf(this.b), Long.valueOf(this.c) });
  }
  
  public IBinder i()
  {
    if (this.i == null) {
      return null;
    }
    return this.i.asBinder();
  }
  
  public String toString()
  {
    return zzw.a(this).a("startTimeMillis", Long.valueOf(this.b)).a("endTimeMillis", Long.valueOf(this.c)).a("dataSources", this.d).a("dateTypes", this.e).a("sessions", this.f).a("deleteAllData", Boolean.valueOf(this.g)).a("deleteAllSessions", Boolean.valueOf(this.h)).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd.a(this, paramParcel, paramInt);
  }
  
  public static class Builder
  {
    private List<DataSource> a = new ArrayList();
    private List<DataType> b = new ArrayList();
    private List<Session> c = new ArrayList();
    private boolean d = false;
    private boolean e = false;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/DataDeleteRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */