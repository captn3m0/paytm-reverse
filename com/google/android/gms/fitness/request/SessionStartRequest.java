package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.fitness.data.Session;
import com.google.android.gms.internal.zzow;
import com.google.android.gms.internal.zzow.zza;
import java.util.concurrent.TimeUnit;

public class SessionStartRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<SessionStartRequest> CREATOR = new zzaa();
  private final int a;
  private final Session b;
  private final zzow c;
  
  SessionStartRequest(int paramInt, Session paramSession, IBinder paramIBinder)
  {
    this.a = paramInt;
    this.b = paramSession;
    this.c = zzow.zza.a(paramIBinder);
  }
  
  public SessionStartRequest(Session paramSession, zzow paramzzow)
  {
    if (paramSession.a(TimeUnit.MILLISECONDS) < System.currentTimeMillis()) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.b(bool, "Cannot start a session in the future");
      zzx.b(paramSession.a(), "Cannot start a session which has already ended");
      this.a = 3;
      this.b = paramSession;
      this.c = paramzzow;
      return;
    }
  }
  
  private boolean a(SessionStartRequest paramSessionStartRequest)
  {
    return zzw.a(this.b, paramSessionStartRequest.b);
  }
  
  public Session a()
  {
    return this.b;
  }
  
  public IBinder b()
  {
    if (this.c == null) {
      return null;
    }
    return this.c.asBinder();
  }
  
  int c()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof SessionStartRequest)) && (a((SessionStartRequest)paramObject)));
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b });
  }
  
  public String toString()
  {
    return zzw.a(this).a("session", this.b).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzaa.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/SessionStartRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */