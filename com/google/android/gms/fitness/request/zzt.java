package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzt
  implements Parcelable.Creator<ReadRawRequest>
{
  static void a(ReadRawRequest paramReadRawRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramReadRawRequest.c(), false);
    zzb.a(paramParcel, 1000, paramReadRawRequest.b());
    zzb.c(paramParcel, 3, paramReadRawRequest.a(), false);
    zzb.a(paramParcel, 4, paramReadRawRequest.e());
    zzb.a(paramParcel, 5, paramReadRawRequest.d());
    zzb.a(paramParcel, paramInt);
  }
  
  public ReadRawRequest a(Parcel paramParcel)
  {
    ArrayList localArrayList = null;
    boolean bool1 = false;
    int j = zza.b(paramParcel);
    boolean bool2 = false;
    IBinder localIBinder = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localIBinder = zza.q(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 3: 
        localArrayList = zza.c(paramParcel, k, DataSourceQueryParams.CREATOR);
        break;
      case 4: 
        bool2 = zza.c(paramParcel, k);
        break;
      case 5: 
        bool1 = zza.c(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new ReadRawRequest(i, localIBinder, localArrayList, bool2, bool1);
  }
  
  public ReadRawRequest[] a(int paramInt)
  {
    return new ReadRawRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */