package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzab
  implements Parcelable.Creator<SessionStopRequest>
{
  static void a(SessionStopRequest paramSessionStopRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramSessionStopRequest.a(), false);
    zzb.a(paramParcel, 1000, paramSessionStopRequest.d());
    zzb.a(paramParcel, 2, paramSessionStopRequest.b(), false);
    zzb.a(paramParcel, 3, paramSessionStopRequest.c(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public SessionStopRequest a(Parcel paramParcel)
  {
    IBinder localIBinder = null;
    int j = zza.b(paramParcel);
    int i = 0;
    String str2 = null;
    String str1 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        str1 = zza.p(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str2 = zza.p(paramParcel, k);
        break;
      case 3: 
        localIBinder = zza.q(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new SessionStopRequest(i, str1, str2, localIBinder);
  }
  
  public SessionStopRequest[] a(int paramInt)
  {
    return new SessionStopRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzab.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */