package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Device;
import com.google.android.gms.internal.zzoh;
import com.google.android.gms.internal.zzoh.zza;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class DataReadRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<DataReadRequest> CREATOR = new zzf();
  private final int a;
  private final List<DataType> b;
  private final List<DataSource> c;
  private final long d;
  private final long e;
  private final List<DataType> f;
  private final List<DataSource> g;
  private final int h;
  private final long i;
  private final DataSource j;
  private final int k;
  private final boolean l;
  private final boolean m;
  private final zzoh n;
  private final List<Device> o;
  
  DataReadRequest(int paramInt1, List<DataType> paramList1, List<DataSource> paramList2, long paramLong1, long paramLong2, List<DataType> paramList3, List<DataSource> paramList4, int paramInt2, long paramLong3, DataSource paramDataSource, int paramInt3, boolean paramBoolean1, boolean paramBoolean2, IBinder paramIBinder, List<Device> paramList)
  {
    this.a = paramInt1;
    this.b = paramList1;
    this.c = paramList2;
    this.d = paramLong1;
    this.e = paramLong2;
    this.f = paramList3;
    this.g = paramList4;
    this.h = paramInt2;
    this.i = paramLong3;
    this.j = paramDataSource;
    this.k = paramInt3;
    this.l = paramBoolean1;
    this.m = paramBoolean2;
    if (paramIBinder == null) {}
    for (paramList1 = null;; paramList1 = zzoh.zza.a(paramIBinder))
    {
      this.n = paramList1;
      paramList1 = paramList;
      if (paramList == null) {
        paramList1 = Collections.emptyList();
      }
      this.o = paramList1;
      return;
    }
  }
  
  public DataReadRequest(DataReadRequest paramDataReadRequest, zzoh paramzzoh)
  {
    this(paramDataReadRequest.b, paramDataReadRequest.c, paramDataReadRequest.d, paramDataReadRequest.e, paramDataReadRequest.f, paramDataReadRequest.g, paramDataReadRequest.h, paramDataReadRequest.i, paramDataReadRequest.j, paramDataReadRequest.k, paramDataReadRequest.l, paramDataReadRequest.m, paramzzoh, paramDataReadRequest.o);
  }
  
  public DataReadRequest(List<DataType> paramList1, List<DataSource> paramList2, long paramLong1, long paramLong2, List<DataType> paramList3, List<DataSource> paramList4, int paramInt1, long paramLong3, DataSource paramDataSource, int paramInt2, boolean paramBoolean1, boolean paramBoolean2, zzoh paramzzoh, List<Device> paramList) {}
  
  private boolean a(DataReadRequest paramDataReadRequest)
  {
    return (this.b.equals(paramDataReadRequest.b)) && (this.c.equals(paramDataReadRequest.c)) && (this.d == paramDataReadRequest.d) && (this.e == paramDataReadRequest.e) && (this.h == paramDataReadRequest.h) && (this.g.equals(paramDataReadRequest.g)) && (this.f.equals(paramDataReadRequest.f)) && (zzw.a(this.j, paramDataReadRequest.j)) && (this.i == paramDataReadRequest.i) && (this.m == paramDataReadRequest.m);
  }
  
  public List<DataType> a()
  {
    return this.b;
  }
  
  public List<DataSource> b()
  {
    return this.c;
  }
  
  public List<DataType> c()
  {
    return this.f;
  }
  
  public List<DataSource> d()
  {
    return this.g;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public int e()
  {
    return this.h;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof DataReadRequest)) && (a((DataReadRequest)paramObject)));
  }
  
  public DataSource f()
  {
    return this.j;
  }
  
  public int g()
  {
    return this.k;
  }
  
  public boolean h()
  {
    return this.m;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Integer.valueOf(this.h), Long.valueOf(this.d), Long.valueOf(this.e) });
  }
  
  public boolean i()
  {
    return this.l;
  }
  
  int j()
  {
    return this.a;
  }
  
  public long k()
  {
    return this.e;
  }
  
  public long l()
  {
    return this.d;
  }
  
  public long m()
  {
    return this.i;
  }
  
  public IBinder n()
  {
    if (this.n == null) {
      return null;
    }
    return this.n.asBinder();
  }
  
  public List<Device> o()
  {
    return this.o;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("DataReadRequest{");
    Iterator localIterator;
    if (!this.b.isEmpty())
    {
      localIterator = this.b.iterator();
      while (localIterator.hasNext()) {
        localStringBuilder.append(((DataType)localIterator.next()).c()).append(" ");
      }
    }
    if (!this.c.isEmpty())
    {
      localIterator = this.c.iterator();
      while (localIterator.hasNext()) {
        localStringBuilder.append(((DataSource)localIterator.next()).g()).append(" ");
      }
    }
    if (this.h != 0)
    {
      localStringBuilder.append("bucket by ").append(Bucket.a(this.h));
      if (this.i > 0L) {
        localStringBuilder.append(" >").append(this.i).append("ms");
      }
      localStringBuilder.append(": ");
    }
    if (!this.f.isEmpty())
    {
      localIterator = this.f.iterator();
      while (localIterator.hasNext()) {
        localStringBuilder.append(((DataType)localIterator.next()).c()).append(" ");
      }
    }
    if (!this.g.isEmpty())
    {
      localIterator = this.g.iterator();
      while (localIterator.hasNext()) {
        localStringBuilder.append(((DataSource)localIterator.next()).g()).append(" ");
      }
    }
    localStringBuilder.append(String.format("(%tF %tT - %tF %tT)", new Object[] { Long.valueOf(this.d), Long.valueOf(this.d), Long.valueOf(this.e), Long.valueOf(this.e) }));
    if (this.j != null) {
      localStringBuilder.append("activities: ").append(this.j.g());
    }
    if (this.m) {
      localStringBuilder.append(" +server");
    }
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzf.a(this, paramParcel, paramInt);
  }
  
  public static class Builder
  {
    private List<DataType> a = new ArrayList();
    private List<DataSource> b = new ArrayList();
    private List<DataType> c = new ArrayList();
    private List<DataSource> d = new ArrayList();
    private int e = 0;
    private long f = 0L;
    private int g = 0;
    private boolean h = false;
    private boolean i = false;
    private List<Device> j = new ArrayList();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/DataReadRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */