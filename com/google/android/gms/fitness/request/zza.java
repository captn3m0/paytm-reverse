package com.google.android.gms.fitness.request;

import android.os.RemoteException;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.fitness.data.BleDevice;
import java.util.HashMap;
import java.util.Map;

public class zza
  extends zzq.zza
{
  private final BleScanCallback a;
  
  private zza(BleScanCallback paramBleScanCallback)
  {
    this.a = ((BleScanCallback)zzx.a(paramBleScanCallback));
  }
  
  public void a()
    throws RemoteException
  {
    this.a.a();
  }
  
  public void a(BleDevice paramBleDevice)
    throws RemoteException
  {
    this.a.a(paramBleDevice);
  }
  
  public static class zza
  {
    private static final zza a = new zza();
    private final Map<BleScanCallback, zza> b = new HashMap();
    
    public static zza a()
    {
      return a;
    }
    
    public zza a(BleScanCallback paramBleScanCallback)
    {
      synchronized (this.b)
      {
        zza localzza = (zza)this.b.get(paramBleScanCallback);
        if (localzza != null) {
          return localzza;
        }
        paramBleScanCallback = new zza(paramBleScanCallback, null);
        return paramBleScanCallback;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */