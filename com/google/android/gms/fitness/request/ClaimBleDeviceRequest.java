package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.fitness.data.BleDevice;
import com.google.android.gms.internal.zzow;
import com.google.android.gms.internal.zzow.zza;

public class ClaimBleDeviceRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<ClaimBleDeviceRequest> CREATOR = new zzb();
  private final int a;
  private final String b;
  private final BleDevice c;
  private final zzow d;
  
  ClaimBleDeviceRequest(int paramInt, String paramString, BleDevice paramBleDevice, IBinder paramIBinder)
  {
    this.a = paramInt;
    this.b = paramString;
    this.c = paramBleDevice;
    this.d = zzow.zza.a(paramIBinder);
  }
  
  public ClaimBleDeviceRequest(String paramString, BleDevice paramBleDevice, zzow paramzzow)
  {
    this.a = 4;
    this.b = paramString;
    this.c = paramBleDevice;
    this.d = paramzzow;
  }
  
  public String a()
  {
    return this.b;
  }
  
  public BleDevice b()
  {
    return this.c;
  }
  
  public IBinder c()
  {
    if (this.d == null) {
      return null;
    }
    return this.d.asBinder();
  }
  
  int d()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String toString()
  {
    return String.format("ClaimBleDeviceRequest{%s %s}", new Object[] { this.b, this.c });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/ClaimBleDeviceRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */