package com.google.android.gms.fitness.request;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzl
  implements Parcelable.Creator<DataUpdateListenerUnregistrationRequest>
{
  static void a(DataUpdateListenerUnregistrationRequest paramDataUpdateListenerUnregistrationRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramDataUpdateListenerUnregistrationRequest.a(), paramInt, false);
    zzb.a(paramParcel, 1000, paramDataUpdateListenerUnregistrationRequest.c());
    zzb.a(paramParcel, 2, paramDataUpdateListenerUnregistrationRequest.b(), false);
    zzb.a(paramParcel, i);
  }
  
  public DataUpdateListenerUnregistrationRequest a(Parcel paramParcel)
  {
    IBinder localIBinder = null;
    int j = zza.b(paramParcel);
    int i = 0;
    PendingIntent localPendingIntent = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
      }
      for (;;)
      {
        break;
        localPendingIntent = (PendingIntent)zza.a(paramParcel, k, PendingIntent.CREATOR);
        continue;
        i = zza.g(paramParcel, k);
        continue;
        localIBinder = zza.q(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new DataUpdateListenerUnregistrationRequest(i, localPendingIntent, localIBinder);
  }
  
  public DataUpdateListenerUnregistrationRequest[] a(int paramInt)
  {
    return new DataUpdateListenerUnregistrationRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */