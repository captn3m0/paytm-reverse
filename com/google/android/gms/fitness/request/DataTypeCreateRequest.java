package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.internal.zzoj;
import com.google.android.gms.internal.zzoj.zza;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DataTypeCreateRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<DataTypeCreateRequest> CREATOR = new zzi();
  private final int a;
  private final String b;
  private final List<Field> c;
  private final zzoj d;
  
  DataTypeCreateRequest(int paramInt, String paramString, List<Field> paramList, IBinder paramIBinder)
  {
    this.a = paramInt;
    this.b = paramString;
    this.c = Collections.unmodifiableList(paramList);
    this.d = zzoj.zza.a(paramIBinder);
  }
  
  public DataTypeCreateRequest(DataTypeCreateRequest paramDataTypeCreateRequest, zzoj paramzzoj)
  {
    this(paramDataTypeCreateRequest.b, paramDataTypeCreateRequest.c, paramzzoj);
  }
  
  public DataTypeCreateRequest(String paramString, List<Field> paramList, zzoj paramzzoj)
  {
    this.a = 3;
    this.b = paramString;
    this.c = Collections.unmodifiableList(paramList);
    this.d = paramzzoj;
  }
  
  private boolean a(DataTypeCreateRequest paramDataTypeCreateRequest)
  {
    return (zzw.a(this.b, paramDataTypeCreateRequest.b)) && (zzw.a(this.c, paramDataTypeCreateRequest.c));
  }
  
  public String a()
  {
    return this.b;
  }
  
  public List<Field> b()
  {
    return this.c;
  }
  
  public IBinder c()
  {
    if (this.d == null) {
      return null;
    }
    return this.d.asBinder();
  }
  
  int d()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof DataTypeCreateRequest)) && (a((DataTypeCreateRequest)paramObject)));
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b, this.c });
  }
  
  public String toString()
  {
    return zzw.a(this).a("name", this.b).a("fields", this.c).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzi.a(this, paramParcel, paramInt);
  }
  
  public static class Builder
  {
    private List<Field> a = new ArrayList();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/DataTypeCreateRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */