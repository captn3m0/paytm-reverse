package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataType;

public class zzs
  implements Parcelable.Creator<ListSubscriptionsRequest>
{
  static void a(ListSubscriptionsRequest paramListSubscriptionsRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramListSubscriptionsRequest.a(), paramInt, false);
    zzb.a(paramParcel, 1000, paramListSubscriptionsRequest.c());
    zzb.a(paramParcel, 2, paramListSubscriptionsRequest.b(), false);
    zzb.a(paramParcel, i);
  }
  
  public ListSubscriptionsRequest a(Parcel paramParcel)
  {
    IBinder localIBinder = null;
    int j = zza.b(paramParcel);
    int i = 0;
    DataType localDataType = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
      }
      for (;;)
      {
        break;
        localDataType = (DataType)zza.a(paramParcel, k, DataType.CREATOR);
        continue;
        i = zza.g(paramParcel, k);
        continue;
        localIBinder = zza.q(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new ListSubscriptionsRequest(i, localDataType, localIBinder);
  }
  
  public ListSubscriptionsRequest[] a(int paramInt)
  {
    return new ListSubscriptionsRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */