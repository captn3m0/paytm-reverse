package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.zzos;
import com.google.android.gms.internal.zzos.zza;
import java.util.List;

public class ReadRawRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<ReadRawRequest> CREATOR = new zzt();
  private final int a;
  private final zzos b;
  private final List<DataSourceQueryParams> c;
  private final boolean d;
  private final boolean e;
  
  ReadRawRequest(int paramInt, IBinder paramIBinder, List<DataSourceQueryParams> paramList, boolean paramBoolean1, boolean paramBoolean2)
  {
    this.a = paramInt;
    this.b = zzos.zza.a(paramIBinder);
    this.c = paramList;
    this.d = paramBoolean1;
    this.e = paramBoolean2;
  }
  
  public List<DataSourceQueryParams> a()
  {
    return this.c;
  }
  
  int b()
  {
    return this.a;
  }
  
  public IBinder c()
  {
    if (this.b != null) {
      return this.b.asBinder();
    }
    return null;
  }
  
  public boolean d()
  {
    return this.e;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean e()
  {
    return this.d;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzt.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/ReadRawRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */