package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Session;
import java.util.ArrayList;

public class zzd
  implements Parcelable.Creator<DataDeleteRequest>
{
  static void a(DataDeleteRequest paramDataDeleteRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramDataDeleteRequest.h());
    zzb.a(paramParcel, 1000, paramDataDeleteRequest.d());
    zzb.a(paramParcel, 2, paramDataDeleteRequest.g());
    zzb.c(paramParcel, 3, paramDataDeleteRequest.a(), false);
    zzb.c(paramParcel, 4, paramDataDeleteRequest.b(), false);
    zzb.c(paramParcel, 5, paramDataDeleteRequest.c(), false);
    zzb.a(paramParcel, 6, paramDataDeleteRequest.e());
    zzb.a(paramParcel, 7, paramDataDeleteRequest.f());
    zzb.a(paramParcel, 8, paramDataDeleteRequest.i(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public DataDeleteRequest a(Parcel paramParcel)
  {
    long l1 = 0L;
    boolean bool1 = false;
    IBinder localIBinder = null;
    int j = zza.b(paramParcel);
    boolean bool2 = false;
    ArrayList localArrayList1 = null;
    ArrayList localArrayList2 = null;
    ArrayList localArrayList3 = null;
    long l2 = 0L;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        l2 = zza.i(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        l1 = zza.i(paramParcel, k);
        break;
      case 3: 
        localArrayList3 = zza.c(paramParcel, k, DataSource.CREATOR);
        break;
      case 4: 
        localArrayList2 = zza.c(paramParcel, k, DataType.CREATOR);
        break;
      case 5: 
        localArrayList1 = zza.c(paramParcel, k, Session.CREATOR);
        break;
      case 6: 
        bool2 = zza.c(paramParcel, k);
        break;
      case 7: 
        bool1 = zza.c(paramParcel, k);
        break;
      case 8: 
        localIBinder = zza.q(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new DataDeleteRequest(i, l2, l1, localArrayList3, localArrayList2, localArrayList1, bool2, bool1, localIBinder);
  }
  
  public DataDeleteRequest[] a(int paramInt)
  {
    return new DataDeleteRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */