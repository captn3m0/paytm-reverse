package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.internal.zzow;
import com.google.android.gms.internal.zzow.zza;

public class UnsubscribeRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<UnsubscribeRequest> CREATOR = new zzah();
  private final int a;
  private final DataType b;
  private final DataSource c;
  private final zzow d;
  
  UnsubscribeRequest(int paramInt, DataType paramDataType, DataSource paramDataSource, IBinder paramIBinder)
  {
    this.a = paramInt;
    this.b = paramDataType;
    this.c = paramDataSource;
    this.d = zzow.zza.a(paramIBinder);
  }
  
  public UnsubscribeRequest(DataType paramDataType, DataSource paramDataSource, zzow paramzzow)
  {
    this.a = 3;
    this.b = paramDataType;
    this.c = paramDataSource;
    this.d = paramzzow;
  }
  
  private boolean a(UnsubscribeRequest paramUnsubscribeRequest)
  {
    return (zzw.a(this.c, paramUnsubscribeRequest.c)) && (zzw.a(this.b, paramUnsubscribeRequest.b));
  }
  
  int a()
  {
    return this.a;
  }
  
  public DataType b()
  {
    return this.b;
  }
  
  public DataSource c()
  {
    return this.c;
  }
  
  public IBinder d()
  {
    if (this.d == null) {
      return null;
    }
    return this.d.asBinder();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof UnsubscribeRequest)) && (a((UnsubscribeRequest)paramObject)));
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.c, this.b });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzah.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/UnsubscribeRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */