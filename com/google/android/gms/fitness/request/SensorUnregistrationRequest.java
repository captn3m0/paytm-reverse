package com.google.android.gms.fitness.request;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.fitness.data.zzk;
import com.google.android.gms.fitness.data.zzk.zza;
import com.google.android.gms.internal.zzow;
import com.google.android.gms.internal.zzow.zza;

public class SensorUnregistrationRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<SensorUnregistrationRequest> CREATOR = new zzw();
  private final int a;
  private final zzk b;
  private final PendingIntent c;
  private final zzow d;
  
  SensorUnregistrationRequest(int paramInt, IBinder paramIBinder1, PendingIntent paramPendingIntent, IBinder paramIBinder2)
  {
    this.a = paramInt;
    if (paramIBinder1 == null) {}
    for (paramIBinder1 = null;; paramIBinder1 = zzk.zza.a(paramIBinder1))
    {
      this.b = paramIBinder1;
      this.c = paramPendingIntent;
      this.d = zzow.zza.a(paramIBinder2);
      return;
    }
  }
  
  public SensorUnregistrationRequest(zzk paramzzk, PendingIntent paramPendingIntent, zzow paramzzow)
  {
    this.a = 4;
    this.b = paramzzk;
    this.c = paramPendingIntent;
    this.d = paramzzow;
  }
  
  public PendingIntent a()
  {
    return this.c;
  }
  
  public IBinder b()
  {
    if (this.d == null) {
      return null;
    }
    return this.d.asBinder();
  }
  
  int c()
  {
    return this.a;
  }
  
  IBinder d()
  {
    if (this.b == null) {
      return null;
    }
    return this.b.asBinder();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String toString()
  {
    return String.format("SensorUnregistrationRequest{%s}", new Object[] { this.b });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzw.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/SensorUnregistrationRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */