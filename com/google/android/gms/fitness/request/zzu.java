package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataSource;
import java.util.ArrayList;

public class zzu
  implements Parcelable.Creator<ReadStatsRequest>
{
  static void a(ReadStatsRequest paramReadStatsRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramReadStatsRequest.b(), false);
    zzb.a(paramParcel, 1000, paramReadStatsRequest.a());
    zzb.c(paramParcel, 3, paramReadStatsRequest.c(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public ReadStatsRequest a(Parcel paramParcel)
  {
    ArrayList localArrayList = null;
    int j = zza.b(paramParcel);
    int i = 0;
    IBinder localIBinder = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localIBinder = zza.q(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 3: 
        localArrayList = zza.c(paramParcel, k, DataSource.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new ReadStatsRequest(i, localIBinder, localArrayList);
  }
  
  public ReadStatsRequest[] a(int paramInt)
  {
    return new ReadStatsRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */