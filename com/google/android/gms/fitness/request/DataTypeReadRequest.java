package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.internal.zzoj;
import com.google.android.gms.internal.zzoj.zza;

public class DataTypeReadRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<DataTypeReadRequest> CREATOR = new zzj();
  private final int a;
  private final String b;
  private final zzoj c;
  
  DataTypeReadRequest(int paramInt, String paramString, IBinder paramIBinder)
  {
    this.a = paramInt;
    this.b = paramString;
    this.c = zzoj.zza.a(paramIBinder);
  }
  
  public DataTypeReadRequest(String paramString, zzoj paramzzoj)
  {
    this.a = 3;
    this.b = paramString;
    this.c = paramzzoj;
  }
  
  private boolean a(DataTypeReadRequest paramDataTypeReadRequest)
  {
    return zzw.a(this.b, paramDataTypeReadRequest.b);
  }
  
  public String a()
  {
    return this.b;
  }
  
  public IBinder b()
  {
    return this.c.asBinder();
  }
  
  int c()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof DataTypeReadRequest)) && (a((DataTypeReadRequest)paramObject)));
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b });
  }
  
  public String toString()
  {
    return zzw.a(this).a("name", this.b).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzj.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/DataTypeReadRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */