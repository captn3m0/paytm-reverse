package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzj
  implements Parcelable.Creator<DataTypeReadRequest>
{
  static void a(DataTypeReadRequest paramDataTypeReadRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramDataTypeReadRequest.a(), false);
    zzb.a(paramParcel, 1000, paramDataTypeReadRequest.c());
    zzb.a(paramParcel, 3, paramDataTypeReadRequest.b(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public DataTypeReadRequest a(Parcel paramParcel)
  {
    IBinder localIBinder = null;
    int j = zza.b(paramParcel);
    int i = 0;
    String str = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        str = zza.p(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 3: 
        localIBinder = zza.q(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new DataTypeReadRequest(i, str, localIBinder);
  }
  
  public DataTypeReadRequest[] a(int paramInt)
  {
    return new DataTypeReadRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */