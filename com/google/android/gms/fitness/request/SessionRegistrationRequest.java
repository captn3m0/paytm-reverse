package com.google.android.gms.fitness.request;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.internal.zzow;
import com.google.android.gms.internal.zzow.zza;

public class SessionRegistrationRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<SessionRegistrationRequest> CREATOR = new zzz();
  private final int a;
  private final PendingIntent b;
  private final zzow c;
  private final int d;
  
  SessionRegistrationRequest(int paramInt1, PendingIntent paramPendingIntent, IBinder paramIBinder, int paramInt2)
  {
    this.a = paramInt1;
    this.b = paramPendingIntent;
    if (paramIBinder == null) {}
    for (paramPendingIntent = null;; paramPendingIntent = zzow.zza.a(paramIBinder))
    {
      this.c = paramPendingIntent;
      this.d = paramInt2;
      return;
    }
  }
  
  public SessionRegistrationRequest(PendingIntent paramPendingIntent, zzow paramzzow, int paramInt)
  {
    this.a = 6;
    this.b = paramPendingIntent;
    this.c = paramzzow;
    this.d = paramInt;
  }
  
  private boolean a(SessionRegistrationRequest paramSessionRegistrationRequest)
  {
    return (this.d == paramSessionRegistrationRequest.d) && (zzw.a(this.b, paramSessionRegistrationRequest.b));
  }
  
  public PendingIntent a()
  {
    return this.b;
  }
  
  public IBinder b()
  {
    if (this.c == null) {
      return null;
    }
    return this.c.asBinder();
  }
  
  public int c()
  {
    return this.d;
  }
  
  int d()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof SessionRegistrationRequest)) && (a((SessionRegistrationRequest)paramObject)));
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b, Integer.valueOf(this.d) });
  }
  
  public String toString()
  {
    return zzw.a(this).a("pendingIntent", this.b).a("sessionRegistrationOption", Integer.valueOf(this.d)).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzz.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/SessionRegistrationRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */