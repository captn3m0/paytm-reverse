package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.zzox;
import com.google.android.gms.internal.zzox.zza;

public class GetSyncInfoRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<GetSyncInfoRequest> CREATOR = new zzp();
  private final int a;
  private final zzox b;
  
  GetSyncInfoRequest(int paramInt, IBinder paramIBinder)
  {
    this.a = paramInt;
    this.b = zzox.zza.a(paramIBinder);
  }
  
  int a()
  {
    return this.a;
  }
  
  public IBinder b()
  {
    return this.b.asBinder();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String toString()
  {
    return String.format("GetSyncInfoRequest {%d, %s, %s}", new Object[] { Integer.valueOf(this.a), this.b });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzp.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/GetSyncInfoRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */