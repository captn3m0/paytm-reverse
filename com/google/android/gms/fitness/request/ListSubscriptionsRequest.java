package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.internal.zzor;
import com.google.android.gms.internal.zzor.zza;

public class ListSubscriptionsRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<ListSubscriptionsRequest> CREATOR = new zzs();
  private final int a;
  private final DataType b;
  private final zzor c;
  
  ListSubscriptionsRequest(int paramInt, DataType paramDataType, IBinder paramIBinder)
  {
    this.a = paramInt;
    this.b = paramDataType;
    this.c = zzor.zza.a(paramIBinder);
  }
  
  public ListSubscriptionsRequest(DataType paramDataType, zzor paramzzor)
  {
    this.a = 3;
    this.b = paramDataType;
    this.c = paramzzor;
  }
  
  public DataType a()
  {
    return this.b;
  }
  
  public IBinder b()
  {
    if (this.c == null) {
      return null;
    }
    return this.c.asBinder();
  }
  
  int c()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzs.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/ListSubscriptionsRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */