package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataSet;

public class zzm
  implements Parcelable.Creator<DataUpdateRequest>
{
  static void a(DataUpdateRequest paramDataUpdateRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramDataUpdateRequest.a());
    zzb.a(paramParcel, 1000, paramDataUpdateRequest.e());
    zzb.a(paramParcel, 2, paramDataUpdateRequest.b());
    zzb.a(paramParcel, 3, paramDataUpdateRequest.c(), paramInt, false);
    zzb.a(paramParcel, 4, paramDataUpdateRequest.d(), false);
    zzb.a(paramParcel, i);
  }
  
  public DataUpdateRequest a(Parcel paramParcel)
  {
    long l1 = 0L;
    IBinder localIBinder = null;
    int j = zza.b(paramParcel);
    int i = 0;
    DataSet localDataSet = null;
    long l2 = 0L;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        l2 = zza.i(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        l1 = zza.i(paramParcel, k);
        break;
      case 3: 
        localDataSet = (DataSet)zza.a(paramParcel, k, DataSet.CREATOR);
        break;
      case 4: 
        localIBinder = zza.q(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new DataUpdateRequest(i, l2, l1, localDataSet, localIBinder);
  }
  
  public DataUpdateRequest[] a(int paramInt)
  {
    return new DataUpdateRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */