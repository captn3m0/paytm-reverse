package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;

public class zzh
  implements Parcelable.Creator<DataSourcesRequest>
{
  static void a(DataSourcesRequest paramDataSourcesRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.c(paramParcel, 1, paramDataSourcesRequest.a(), false);
    zzb.a(paramParcel, 1000, paramDataSourcesRequest.e());
    zzb.a(paramParcel, 2, paramDataSourcesRequest.b(), false);
    zzb.a(paramParcel, 3, paramDataSourcesRequest.c());
    zzb.a(paramParcel, 4, paramDataSourcesRequest.d(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public DataSourcesRequest a(Parcel paramParcel)
  {
    boolean bool = false;
    IBinder localIBinder = null;
    int j = zza.b(paramParcel);
    ArrayList localArrayList1 = null;
    ArrayList localArrayList2 = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localArrayList2 = zza.c(paramParcel, k, DataType.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localArrayList1 = zza.C(paramParcel, k);
        break;
      case 3: 
        bool = zza.c(paramParcel, k);
        break;
      case 4: 
        localIBinder = zza.q(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new DataSourcesRequest(i, localArrayList2, localArrayList1, bool, localIBinder);
  }
  
  public DataSourcesRequest[] a(int paramInt)
  {
    return new DataSourcesRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */