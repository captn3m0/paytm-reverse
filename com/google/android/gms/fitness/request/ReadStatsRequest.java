package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.internal.zzot;
import com.google.android.gms.internal.zzot.zza;
import java.util.List;

public class ReadStatsRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<ReadStatsRequest> CREATOR = new zzu();
  private final int a;
  private final zzot b;
  private final List<DataSource> c;
  
  ReadStatsRequest(int paramInt, IBinder paramIBinder, List<DataSource> paramList)
  {
    this.a = paramInt;
    this.b = zzot.zza.a(paramIBinder);
    this.c = paramList;
  }
  
  int a()
  {
    return this.a;
  }
  
  public IBinder b()
  {
    return this.b.asBinder();
  }
  
  public List<DataSource> c()
  {
    return this.c;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String toString()
  {
    return String.format("ReadStatsRequest", new Object[0]);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzu.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/ReadStatsRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */