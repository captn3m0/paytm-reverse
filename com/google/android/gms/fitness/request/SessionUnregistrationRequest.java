package com.google.android.gms.fitness.request;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.internal.zzow;
import com.google.android.gms.internal.zzow.zza;

public class SessionUnregistrationRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<SessionUnregistrationRequest> CREATOR = new zzac();
  private final int a;
  private final PendingIntent b;
  private final zzow c;
  
  SessionUnregistrationRequest(int paramInt, PendingIntent paramPendingIntent, IBinder paramIBinder)
  {
    this.a = paramInt;
    this.b = paramPendingIntent;
    this.c = zzow.zza.a(paramIBinder);
  }
  
  public SessionUnregistrationRequest(PendingIntent paramPendingIntent, zzow paramzzow)
  {
    this.a = 5;
    this.b = paramPendingIntent;
    this.c = paramzzow;
  }
  
  private boolean a(SessionUnregistrationRequest paramSessionUnregistrationRequest)
  {
    return zzw.a(this.b, paramSessionUnregistrationRequest.b);
  }
  
  public PendingIntent a()
  {
    return this.b;
  }
  
  public IBinder b()
  {
    if (this.c == null) {
      return null;
    }
    return this.c.asBinder();
  }
  
  int c()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof SessionUnregistrationRequest)) && (a((SessionUnregistrationRequest)paramObject)));
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b });
  }
  
  public String toString()
  {
    return zzw.a(this).a("pendingIntent", this.b).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzac.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/SessionUnregistrationRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */