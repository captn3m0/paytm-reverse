package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Device;
import java.util.ArrayList;

public class zzf
  implements Parcelable.Creator<DataReadRequest>
{
  static void a(DataReadRequest paramDataReadRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.c(paramParcel, 1, paramDataReadRequest.a(), false);
    zzb.c(paramParcel, 2, paramDataReadRequest.b(), false);
    zzb.a(paramParcel, 3, paramDataReadRequest.l());
    zzb.a(paramParcel, 4, paramDataReadRequest.k());
    zzb.c(paramParcel, 5, paramDataReadRequest.c(), false);
    zzb.c(paramParcel, 6, paramDataReadRequest.d(), false);
    zzb.a(paramParcel, 7, paramDataReadRequest.e());
    zzb.a(paramParcel, 8, paramDataReadRequest.m());
    zzb.a(paramParcel, 9, paramDataReadRequest.f(), paramInt, false);
    zzb.a(paramParcel, 10, paramDataReadRequest.g());
    zzb.a(paramParcel, 12, paramDataReadRequest.i());
    zzb.a(paramParcel, 13, paramDataReadRequest.h());
    zzb.a(paramParcel, 14, paramDataReadRequest.n(), false);
    zzb.c(paramParcel, 16, paramDataReadRequest.o(), false);
    zzb.a(paramParcel, 1000, paramDataReadRequest.j());
    zzb.a(paramParcel, i);
  }
  
  public DataReadRequest a(Parcel paramParcel)
  {
    int m = zza.b(paramParcel);
    int k = 0;
    ArrayList localArrayList5 = null;
    ArrayList localArrayList4 = null;
    long l3 = 0L;
    long l2 = 0L;
    ArrayList localArrayList3 = null;
    ArrayList localArrayList2 = null;
    int j = 0;
    long l1 = 0L;
    DataSource localDataSource = null;
    int i = 0;
    boolean bool2 = false;
    boolean bool1 = false;
    IBinder localIBinder = null;
    ArrayList localArrayList1 = null;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        localArrayList5 = zza.c(paramParcel, n, DataType.CREATOR);
        break;
      case 2: 
        localArrayList4 = zza.c(paramParcel, n, DataSource.CREATOR);
        break;
      case 3: 
        l3 = zza.i(paramParcel, n);
        break;
      case 4: 
        l2 = zza.i(paramParcel, n);
        break;
      case 5: 
        localArrayList3 = zza.c(paramParcel, n, DataType.CREATOR);
        break;
      case 6: 
        localArrayList2 = zza.c(paramParcel, n, DataSource.CREATOR);
        break;
      case 7: 
        j = zza.g(paramParcel, n);
        break;
      case 8: 
        l1 = zza.i(paramParcel, n);
        break;
      case 9: 
        localDataSource = (DataSource)zza.a(paramParcel, n, DataSource.CREATOR);
        break;
      case 10: 
        i = zza.g(paramParcel, n);
        break;
      case 12: 
        bool2 = zza.c(paramParcel, n);
        break;
      case 13: 
        bool1 = zza.c(paramParcel, n);
        break;
      case 14: 
        localIBinder = zza.q(paramParcel, n);
        break;
      case 16: 
        localArrayList1 = zza.c(paramParcel, n, Device.CREATOR);
        break;
      case 1000: 
        k = zza.g(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new DataReadRequest(k, localArrayList5, localArrayList4, l3, l2, localArrayList3, localArrayList2, j, l1, localDataSource, i, bool2, bool1, localIBinder, localArrayList1);
  }
  
  public DataReadRequest[] a(int paramInt)
  {
    return new DataReadRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */