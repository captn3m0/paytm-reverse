package com.google.android.gms.fitness.request;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.location.LocationRequest;
import java.util.ArrayList;

public class zzv
  implements Parcelable.Creator<SensorRegistrationRequest>
{
  static void a(SensorRegistrationRequest paramSensorRegistrationRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramSensorRegistrationRequest.a(), paramInt, false);
    zzb.a(paramParcel, 1000, paramSensorRegistrationRequest.k());
    zzb.a(paramParcel, 2, paramSensorRegistrationRequest.b(), paramInt, false);
    zzb.a(paramParcel, 3, paramSensorRegistrationRequest.l(), false);
    zzb.a(paramParcel, 4, paramSensorRegistrationRequest.a);
    zzb.a(paramParcel, 5, paramSensorRegistrationRequest.b);
    zzb.a(paramParcel, 6, paramSensorRegistrationRequest.e());
    zzb.a(paramParcel, 7, paramSensorRegistrationRequest.f());
    zzb.a(paramParcel, 8, paramSensorRegistrationRequest.c(), paramInt, false);
    zzb.a(paramParcel, 9, paramSensorRegistrationRequest.d());
    zzb.a(paramParcel, 10, paramSensorRegistrationRequest.h());
    zzb.c(paramParcel, 11, paramSensorRegistrationRequest.g(), false);
    zzb.a(paramParcel, 12, paramSensorRegistrationRequest.i());
    zzb.a(paramParcel, 13, paramSensorRegistrationRequest.j(), false);
    zzb.a(paramParcel, i);
  }
  
  public SensorRegistrationRequest a(Parcel paramParcel)
  {
    int n = zza.b(paramParcel);
    int m = 0;
    DataSource localDataSource = null;
    DataType localDataType = null;
    IBinder localIBinder2 = null;
    int k = 0;
    int j = 0;
    long l4 = 0L;
    long l3 = 0L;
    PendingIntent localPendingIntent = null;
    long l2 = 0L;
    int i = 0;
    ArrayList localArrayList = null;
    long l1 = 0L;
    IBinder localIBinder1 = null;
    while (paramParcel.dataPosition() < n)
    {
      int i1 = zza.a(paramParcel);
      switch (zza.a(i1))
      {
      default: 
        zza.b(paramParcel, i1);
        break;
      case 1: 
        localDataSource = (DataSource)zza.a(paramParcel, i1, DataSource.CREATOR);
        break;
      case 1000: 
        m = zza.g(paramParcel, i1);
        break;
      case 2: 
        localDataType = (DataType)zza.a(paramParcel, i1, DataType.CREATOR);
        break;
      case 3: 
        localIBinder2 = zza.q(paramParcel, i1);
        break;
      case 4: 
        k = zza.g(paramParcel, i1);
        break;
      case 5: 
        j = zza.g(paramParcel, i1);
        break;
      case 6: 
        l4 = zza.i(paramParcel, i1);
        break;
      case 7: 
        l3 = zza.i(paramParcel, i1);
        break;
      case 8: 
        localPendingIntent = (PendingIntent)zza.a(paramParcel, i1, PendingIntent.CREATOR);
        break;
      case 9: 
        l2 = zza.i(paramParcel, i1);
        break;
      case 10: 
        i = zza.g(paramParcel, i1);
        break;
      case 11: 
        localArrayList = zza.c(paramParcel, i1, LocationRequest.CREATOR);
        break;
      case 12: 
        l1 = zza.i(paramParcel, i1);
        break;
      case 13: 
        localIBinder1 = zza.q(paramParcel, i1);
      }
    }
    if (paramParcel.dataPosition() != n) {
      throw new zza.zza("Overread allowed size end=" + n, paramParcel);
    }
    return new SensorRegistrationRequest(m, localDataSource, localDataType, localIBinder2, k, j, l4, l3, localPendingIntent, l2, i, localArrayList, l1, localIBinder1);
  }
  
  public SensorRegistrationRequest[] a(int paramInt)
  {
    return new SensorRegistrationRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */