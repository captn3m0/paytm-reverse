package com.google.android.gms.fitness.request;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.zzk;
import com.google.android.gms.fitness.data.zzk.zza;
import com.google.android.gms.internal.zzow;
import com.google.android.gms.internal.zzow.zza;
import com.google.android.gms.location.LocationRequest;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SensorRegistrationRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<SensorRegistrationRequest> CREATOR = new zzv();
  int a;
  int b;
  private final int c;
  private DataSource d;
  private DataType e;
  private zzk f;
  private final long g;
  private final long h;
  private final PendingIntent i;
  private final long j;
  private final int k;
  private final List<LocationRequest> l;
  private final long m;
  private final List<Object> n;
  private final zzow o;
  
  SensorRegistrationRequest(int paramInt1, DataSource paramDataSource, DataType paramDataType, IBinder paramIBinder1, int paramInt2, int paramInt3, long paramLong1, long paramLong2, PendingIntent paramPendingIntent, long paramLong3, int paramInt4, List<LocationRequest> paramList, long paramLong4, IBinder paramIBinder2)
  {
    this.c = paramInt1;
    this.d = paramDataSource;
    this.e = paramDataType;
    if (paramIBinder1 == null) {}
    for (paramDataSource = null;; paramDataSource = zzk.zza.a(paramIBinder1))
    {
      this.f = paramDataSource;
      long l1 = paramLong1;
      if (paramLong1 == 0L) {
        l1 = paramInt2;
      }
      this.g = l1;
      this.j = paramLong3;
      paramLong1 = paramLong2;
      if (paramLong2 == 0L) {
        paramLong1 = paramInt3;
      }
      this.h = paramLong1;
      this.l = paramList;
      this.i = paramPendingIntent;
      this.k = paramInt4;
      this.n = Collections.emptyList();
      this.m = paramLong4;
      this.o = zzow.zza.a(paramIBinder2);
      return;
    }
  }
  
  public SensorRegistrationRequest(DataSource paramDataSource, DataType paramDataType, zzk paramzzk, PendingIntent paramPendingIntent, long paramLong1, long paramLong2, long paramLong3, int paramInt, List<LocationRequest> paramList, List<Object> paramList1, long paramLong4, zzow paramzzow)
  {
    this.c = 6;
    this.d = paramDataSource;
    this.e = paramDataType;
    this.f = paramzzk;
    this.i = paramPendingIntent;
    this.g = paramLong1;
    this.j = paramLong2;
    this.h = paramLong3;
    this.k = paramInt;
    this.l = paramList;
    this.n = paramList1;
    this.m = paramLong4;
    this.o = paramzzow;
  }
  
  public SensorRegistrationRequest(SensorRequest paramSensorRequest, zzk paramzzk, PendingIntent paramPendingIntent, zzow paramzzow)
  {
    this(paramSensorRequest.a(), paramSensorRequest.b(), paramzzk, paramPendingIntent, paramSensorRequest.a(TimeUnit.MICROSECONDS), paramSensorRequest.b(TimeUnit.MICROSECONDS), paramSensorRequest.c(TimeUnit.MICROSECONDS), paramSensorRequest.c(), null, Collections.emptyList(), paramSensorRequest.d(), paramzzow);
  }
  
  private boolean a(SensorRegistrationRequest paramSensorRegistrationRequest)
  {
    return (zzw.a(this.d, paramSensorRegistrationRequest.d)) && (zzw.a(this.e, paramSensorRegistrationRequest.e)) && (this.g == paramSensorRegistrationRequest.g) && (this.j == paramSensorRegistrationRequest.j) && (this.h == paramSensorRegistrationRequest.h) && (this.k == paramSensorRegistrationRequest.k) && (zzw.a(this.l, paramSensorRegistrationRequest.l));
  }
  
  public DataSource a()
  {
    return this.d;
  }
  
  public DataType b()
  {
    return this.e;
  }
  
  public PendingIntent c()
  {
    return this.i;
  }
  
  public long d()
  {
    return this.j;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public long e()
  {
    return this.g;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof SensorRegistrationRequest)) && (a((SensorRegistrationRequest)paramObject)));
  }
  
  public long f()
  {
    return this.h;
  }
  
  public List<LocationRequest> g()
  {
    return this.l;
  }
  
  public int h()
  {
    return this.k;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.d, this.e, this.f, Long.valueOf(this.g), Long.valueOf(this.j), Long.valueOf(this.h), Integer.valueOf(this.k), this.l });
  }
  
  public long i()
  {
    return this.m;
  }
  
  public IBinder j()
  {
    if (this.o == null) {
      return null;
    }
    return this.o.asBinder();
  }
  
  int k()
  {
    return this.c;
  }
  
  IBinder l()
  {
    if (this.f == null) {
      return null;
    }
    return this.f.asBinder();
  }
  
  public String toString()
  {
    return String.format("SensorRegistrationRequest{type %s source %s interval %s fastest %s latency %s}", new Object[] { this.e, this.d, Long.valueOf(this.g), Long.valueOf(this.j), Long.valueOf(this.h) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzv.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/SensorRegistrationRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */