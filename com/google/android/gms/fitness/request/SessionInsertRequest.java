package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.Session;
import com.google.android.gms.internal.zzow;
import com.google.android.gms.internal.zzow.zza;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SessionInsertRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<SessionInsertRequest> CREATOR = new zzx();
  private final int a;
  private final Session b;
  private final List<DataSet> c;
  private final List<DataPoint> d;
  private final zzow e;
  
  SessionInsertRequest(int paramInt, Session paramSession, List<DataSet> paramList, List<DataPoint> paramList1, IBinder paramIBinder)
  {
    this.a = paramInt;
    this.b = paramSession;
    this.c = Collections.unmodifiableList(paramList);
    this.d = Collections.unmodifiableList(paramList1);
    this.e = zzow.zza.a(paramIBinder);
  }
  
  public SessionInsertRequest(Session paramSession, List<DataSet> paramList, List<DataPoint> paramList1, zzow paramzzow)
  {
    this.a = 3;
    this.b = paramSession;
    this.c = Collections.unmodifiableList(paramList);
    this.d = Collections.unmodifiableList(paramList1);
    this.e = paramzzow;
  }
  
  public SessionInsertRequest(SessionInsertRequest paramSessionInsertRequest, zzow paramzzow)
  {
    this(paramSessionInsertRequest.b, paramSessionInsertRequest.c, paramSessionInsertRequest.d, paramzzow);
  }
  
  private boolean a(SessionInsertRequest paramSessionInsertRequest)
  {
    return (zzw.a(this.b, paramSessionInsertRequest.b)) && (zzw.a(this.c, paramSessionInsertRequest.c)) && (zzw.a(this.d, paramSessionInsertRequest.d));
  }
  
  public Session a()
  {
    return this.b;
  }
  
  public List<DataSet> b()
  {
    return this.c;
  }
  
  public List<DataPoint> c()
  {
    return this.d;
  }
  
  public IBinder d()
  {
    if (this.e == null) {
      return null;
    }
    return this.e.asBinder();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  int e()
  {
    return this.a;
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof SessionInsertRequest)) && (a((SessionInsertRequest)paramObject)));
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b, this.c, this.d });
  }
  
  public String toString()
  {
    return zzw.a(this).a("session", this.b).a("dataSets", this.c).a("aggregateDataPoints", this.d).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzx.a(this, paramParcel, paramInt);
  }
  
  public static class Builder
  {
    private List<DataSet> a = new ArrayList();
    private List<DataPoint> b = new ArrayList();
    private List<DataSource> c = new ArrayList();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/SessionInsertRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */