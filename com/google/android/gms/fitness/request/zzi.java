package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.Field;
import java.util.ArrayList;

public class zzi
  implements Parcelable.Creator<DataTypeCreateRequest>
{
  static void a(DataTypeCreateRequest paramDataTypeCreateRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramDataTypeCreateRequest.a(), false);
    zzb.a(paramParcel, 1000, paramDataTypeCreateRequest.d());
    zzb.c(paramParcel, 2, paramDataTypeCreateRequest.b(), false);
    zzb.a(paramParcel, 3, paramDataTypeCreateRequest.c(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public DataTypeCreateRequest a(Parcel paramParcel)
  {
    IBinder localIBinder = null;
    int j = zza.b(paramParcel);
    int i = 0;
    ArrayList localArrayList = null;
    String str = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        str = zza.p(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localArrayList = zza.c(paramParcel, k, Field.CREATOR);
        break;
      case 3: 
        localIBinder = zza.q(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new DataTypeCreateRequest(i, str, localArrayList, localIBinder);
  }
  
  public DataTypeCreateRequest[] a(int paramInt)
  {
    return new DataTypeCreateRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */