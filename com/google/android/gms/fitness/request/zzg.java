package com.google.android.gms.fitness.request;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataSource;

public class zzg
  implements Parcelable.Creator<DataSourceQueryParams>
{
  static void a(DataSourceQueryParams paramDataSourceQueryParams, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramDataSourceQueryParams.b, paramInt, false);
    zzb.a(paramParcel, 1000, paramDataSourceQueryParams.a);
    zzb.a(paramParcel, 2, paramDataSourceQueryParams.c);
    zzb.a(paramParcel, 3, paramDataSourceQueryParams.d);
    zzb.a(paramParcel, 4, paramDataSourceQueryParams.e);
    zzb.a(paramParcel, 5, paramDataSourceQueryParams.f);
    zzb.a(paramParcel, 6, paramDataSourceQueryParams.g);
    zzb.a(paramParcel, i);
  }
  
  public DataSourceQueryParams a(Parcel paramParcel)
  {
    long l1 = 0L;
    int i = 0;
    int m = zza.b(paramParcel);
    DataSource localDataSource = null;
    int j = 0;
    long l2 = 0L;
    long l3 = 0L;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        localDataSource = (DataSource)zza.a(paramParcel, n, DataSource.CREATOR);
        break;
      case 1000: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        l3 = zza.i(paramParcel, n);
        break;
      case 3: 
        l2 = zza.i(paramParcel, n);
        break;
      case 4: 
        l1 = zza.i(paramParcel, n);
        break;
      case 5: 
        j = zza.g(paramParcel, n);
        break;
      case 6: 
        i = zza.g(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new DataSourceQueryParams(k, localDataSource, l3, l2, l1, j, i);
  }
  
  public DataSourceQueryParams[] a(int paramInt)
  {
    return new DataSourceQueryParams[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */