package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.fitness.data.Subscription;
import com.google.android.gms.internal.zzow;
import com.google.android.gms.internal.zzow.zza;

public class SubscribeRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<SubscribeRequest> CREATOR = new zzaf();
  private final int a;
  private Subscription b;
  private final boolean c;
  private final zzow d;
  
  SubscribeRequest(int paramInt, Subscription paramSubscription, boolean paramBoolean, IBinder paramIBinder)
  {
    this.a = paramInt;
    this.b = paramSubscription;
    this.c = paramBoolean;
    this.d = zzow.zza.a(paramIBinder);
  }
  
  public SubscribeRequest(Subscription paramSubscription, boolean paramBoolean, zzow paramzzow)
  {
    this.a = 3;
    this.b = paramSubscription;
    this.c = paramBoolean;
    this.d = paramzzow;
  }
  
  public Subscription a()
  {
    return this.b;
  }
  
  public boolean b()
  {
    return this.c;
  }
  
  public IBinder c()
  {
    if (this.d == null) {
      return null;
    }
    return this.d.asBinder();
  }
  
  int d()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String toString()
  {
    return zzw.a(this).a("subscription", this.b).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzaf.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/SubscribeRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */