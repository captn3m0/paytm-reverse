package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.zzow;
import com.google.android.gms.internal.zzow.zza;

public class DisableFitRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<DisableFitRequest> CREATOR = new zzo();
  private final int a;
  private final zzow b;
  
  DisableFitRequest(int paramInt, IBinder paramIBinder)
  {
    this.a = paramInt;
    this.b = zzow.zza.a(paramIBinder);
  }
  
  public DisableFitRequest(zzow paramzzow)
  {
    this.a = 2;
    this.b = paramzzow;
  }
  
  int a()
  {
    return this.a;
  }
  
  public IBinder b()
  {
    return this.b.asBinder();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String toString()
  {
    return String.format("DisableFitRequest", new Object[0]);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzo.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/DisableFitRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */