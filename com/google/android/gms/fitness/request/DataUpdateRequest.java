package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.internal.zzow;
import com.google.android.gms.internal.zzow.zza;

public class DataUpdateRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<DataUpdateRequest> CREATOR = new zzm();
  private final int a;
  private final long b;
  private final long c;
  private final DataSet d;
  private final zzow e;
  
  DataUpdateRequest(int paramInt, long paramLong1, long paramLong2, DataSet paramDataSet, IBinder paramIBinder)
  {
    this.a = paramInt;
    this.b = paramLong1;
    this.c = paramLong2;
    this.d = paramDataSet;
    this.e = zzow.zza.a(paramIBinder);
  }
  
  public DataUpdateRequest(long paramLong1, long paramLong2, DataSet paramDataSet, IBinder paramIBinder)
  {
    this.a = 1;
    this.b = paramLong1;
    this.c = paramLong2;
    this.d = paramDataSet;
    this.e = zzow.zza.a(paramIBinder);
  }
  
  public DataUpdateRequest(DataUpdateRequest paramDataUpdateRequest, IBinder paramIBinder)
  {
    this(paramDataUpdateRequest.a(), paramDataUpdateRequest.b(), paramDataUpdateRequest.c(), paramIBinder);
  }
  
  private boolean a(DataUpdateRequest paramDataUpdateRequest)
  {
    return (this.b == paramDataUpdateRequest.b) && (this.c == paramDataUpdateRequest.c) && (zzw.a(this.d, paramDataUpdateRequest.d));
  }
  
  public long a()
  {
    return this.b;
  }
  
  public long b()
  {
    return this.c;
  }
  
  public DataSet c()
  {
    return this.d;
  }
  
  public IBinder d()
  {
    if (this.e == null) {
      return null;
    }
    return this.e.asBinder();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  int e()
  {
    return this.a;
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof DataUpdateRequest)) && (a((DataUpdateRequest)paramObject)));
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Long.valueOf(this.b), Long.valueOf(this.c), this.d });
  }
  
  public String toString()
  {
    return zzw.a(this).a("startTimeMillis", Long.valueOf(this.b)).a("endTimeMillis", Long.valueOf(this.c)).a("dataSet", this.d).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzm.a(this, paramParcel, paramInt);
  }
  
  public static class Builder {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/DataUpdateRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */