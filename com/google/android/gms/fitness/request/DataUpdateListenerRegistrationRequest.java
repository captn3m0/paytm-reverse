package com.google.android.gms.fitness.request;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.internal.zzow;
import com.google.android.gms.internal.zzow.zza;

public class DataUpdateListenerRegistrationRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<DataUpdateListenerRegistrationRequest> CREATOR = new zzk();
  private final int a;
  private DataSource b;
  private DataType c;
  private final PendingIntent d;
  private final zzow e;
  
  DataUpdateListenerRegistrationRequest(int paramInt, DataSource paramDataSource, DataType paramDataType, PendingIntent paramPendingIntent, IBinder paramIBinder)
  {
    this.a = paramInt;
    this.b = paramDataSource;
    this.c = paramDataType;
    this.d = paramPendingIntent;
    this.e = zzow.zza.a(paramIBinder);
  }
  
  private boolean a(DataUpdateListenerRegistrationRequest paramDataUpdateListenerRegistrationRequest)
  {
    return (zzw.a(this.b, paramDataUpdateListenerRegistrationRequest.b)) && (zzw.a(this.c, paramDataUpdateListenerRegistrationRequest.c)) && (zzw.a(this.d, paramDataUpdateListenerRegistrationRequest.d));
  }
  
  public DataSource a()
  {
    return this.b;
  }
  
  public DataType b()
  {
    return this.c;
  }
  
  public PendingIntent c()
  {
    return this.d;
  }
  
  public IBinder d()
  {
    if (this.e == null) {
      return null;
    }
    return this.e.asBinder();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  int e()
  {
    return this.a;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof DataUpdateListenerRegistrationRequest)) && (a((DataUpdateListenerRegistrationRequest)paramObject)));
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b, this.c, this.d });
  }
  
  public String toString()
  {
    return zzw.a(this).a("dataSource", this.b).a("dataType", this.c).a("pendingIntent", this.d).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzk.a(this, paramParcel, paramInt);
  }
  
  public static class Builder {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/DataUpdateListenerRegistrationRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */