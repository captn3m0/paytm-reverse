package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.zzow;
import com.google.android.gms.internal.zzow.zza;

public class StopBleScanRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<StopBleScanRequest> CREATOR = new zzae();
  private final int a;
  private final zzq b;
  private final zzow c;
  
  StopBleScanRequest(int paramInt, IBinder paramIBinder1, IBinder paramIBinder2)
  {
    this.a = paramInt;
    this.b = zzq.zza.a(paramIBinder1);
    this.c = zzow.zza.a(paramIBinder2);
  }
  
  public StopBleScanRequest(BleScanCallback paramBleScanCallback, zzow paramzzow)
  {
    this(zza.zza.a().a(paramBleScanCallback), paramzzow);
  }
  
  public StopBleScanRequest(zzq paramzzq, zzow paramzzow)
  {
    this.a = 3;
    this.b = paramzzq;
    this.c = paramzzow;
  }
  
  int a()
  {
    return this.a;
  }
  
  public IBinder b()
  {
    return this.b.asBinder();
  }
  
  public IBinder c()
  {
    if (this.c == null) {
      return null;
    }
    return this.c.asBinder();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzae.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/StopBleScanRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */