package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.internal.zzou;
import com.google.android.gms.internal.zzou.zza;
import java.util.ArrayList;
import java.util.List;

public class SessionReadRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<SessionReadRequest> CREATOR = new zzy();
  private final int a;
  private final String b;
  private final String c;
  private final long d;
  private final long e;
  private final List<DataType> f;
  private final List<DataSource> g;
  private boolean h;
  private final boolean i;
  private final List<String> j;
  private final zzou k;
  
  SessionReadRequest(int paramInt, String paramString1, String paramString2, long paramLong1, long paramLong2, List<DataType> paramList, List<DataSource> paramList1, boolean paramBoolean1, boolean paramBoolean2, List<String> paramList2, IBinder paramIBinder)
  {
    this.a = paramInt;
    this.b = paramString1;
    this.c = paramString2;
    this.d = paramLong1;
    this.e = paramLong2;
    this.f = paramList;
    this.g = paramList1;
    this.h = paramBoolean1;
    this.i = paramBoolean2;
    this.j = paramList2;
    this.k = zzou.zza.a(paramIBinder);
  }
  
  public SessionReadRequest(SessionReadRequest paramSessionReadRequest, zzou paramzzou)
  {
    this(paramSessionReadRequest.b, paramSessionReadRequest.c, paramSessionReadRequest.d, paramSessionReadRequest.e, paramSessionReadRequest.f, paramSessionReadRequest.g, paramSessionReadRequest.h, paramSessionReadRequest.i, paramSessionReadRequest.j, paramzzou);
  }
  
  public SessionReadRequest(String paramString1, String paramString2, long paramLong1, long paramLong2, List<DataType> paramList, List<DataSource> paramList1, boolean paramBoolean1, boolean paramBoolean2, List<String> paramList2, zzou paramzzou) {}
  
  private boolean a(SessionReadRequest paramSessionReadRequest)
  {
    return (zzw.a(this.b, paramSessionReadRequest.b)) && (this.c.equals(paramSessionReadRequest.c)) && (this.d == paramSessionReadRequest.d) && (this.e == paramSessionReadRequest.e) && (zzw.a(this.f, paramSessionReadRequest.f)) && (zzw.a(this.g, paramSessionReadRequest.g)) && (this.h == paramSessionReadRequest.h) && (this.j.equals(paramSessionReadRequest.j)) && (this.i == paramSessionReadRequest.i);
  }
  
  public String a()
  {
    return this.b;
  }
  
  public String b()
  {
    return this.c;
  }
  
  public List<DataType> c()
  {
    return this.f;
  }
  
  public List<DataSource> d()
  {
    return this.g;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public List<String> e()
  {
    return this.j;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof SessionReadRequest)) && (a((SessionReadRequest)paramObject)));
  }
  
  public boolean f()
  {
    return this.i;
  }
  
  public long g()
  {
    return this.e;
  }
  
  public long h()
  {
    return this.d;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b, this.c, Long.valueOf(this.d), Long.valueOf(this.e) });
  }
  
  public boolean i()
  {
    return this.h;
  }
  
  public IBinder j()
  {
    if (this.k == null) {
      return null;
    }
    return this.k.asBinder();
  }
  
  int k()
  {
    return this.a;
  }
  
  public String toString()
  {
    return zzw.a(this).a("sessionName", this.b).a("sessionId", this.c).a("startTimeMillis", Long.valueOf(this.d)).a("endTimeMillis", Long.valueOf(this.e)).a("dataTypes", this.f).a("dataSources", this.g).a("sessionsFromAllApps", Boolean.valueOf(this.h)).a("excludedPackages", this.j).a("useServer", Boolean.valueOf(this.i)).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzy.a(this, paramParcel, paramInt);
  }
  
  public static class Builder
  {
    private long a = 0L;
    private long b = 0L;
    private List<DataType> c = new ArrayList();
    private List<DataSource> d = new ArrayList();
    private boolean e = false;
    private boolean f = false;
    private List<String> g = new ArrayList();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/SessionReadRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */