package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.internal.zzoi;
import com.google.android.gms.internal.zzoi.zza;
import java.util.List;

public class DataSourcesRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<DataSourcesRequest> CREATOR = new zzh();
  private final int a;
  private final List<DataType> b;
  private final List<Integer> c;
  private final boolean d;
  private final zzoi e;
  
  DataSourcesRequest(int paramInt, List<DataType> paramList, List<Integer> paramList1, boolean paramBoolean, IBinder paramIBinder)
  {
    this.a = paramInt;
    this.b = paramList;
    this.c = paramList1;
    this.d = paramBoolean;
    this.e = zzoi.zza.a(paramIBinder);
  }
  
  public DataSourcesRequest(DataSourcesRequest paramDataSourcesRequest, zzoi paramzzoi)
  {
    this(paramDataSourcesRequest.b, paramDataSourcesRequest.c, paramDataSourcesRequest.d, paramzzoi);
  }
  
  public DataSourcesRequest(List<DataType> paramList, List<Integer> paramList1, boolean paramBoolean, zzoi paramzzoi)
  {
    this.a = 4;
    this.b = paramList;
    this.c = paramList1;
    this.d = paramBoolean;
    this.e = paramzzoi;
  }
  
  public List<DataType> a()
  {
    return this.b;
  }
  
  public List<Integer> b()
  {
    return this.c;
  }
  
  public boolean c()
  {
    return this.d;
  }
  
  public IBinder d()
  {
    if (this.e == null) {
      return null;
    }
    return this.e.asBinder();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  int e()
  {
    return this.a;
  }
  
  public String toString()
  {
    zzw.zza localzza = zzw.a(this).a("dataTypes", this.b).a("sourceTypes", this.c);
    if (this.d) {
      localzza.a("includeDbOnlySources", "true");
    }
    return localzza.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzh.a(this, paramParcel, paramInt);
  }
  
  public static class Builder
  {
    private DataType[] a = new DataType[0];
    private int[] b = { 0, 1 };
    private boolean c = false;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/DataSourcesRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */