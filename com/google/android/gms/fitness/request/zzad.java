package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;

public class zzad
  implements Parcelable.Creator<StartBleScanRequest>
{
  static void a(StartBleScanRequest paramStartBleScanRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.c(paramParcel, 1, paramStartBleScanRequest.a(), false);
    zzb.a(paramParcel, 1000, paramStartBleScanRequest.e());
    zzb.a(paramParcel, 2, paramStartBleScanRequest.c(), false);
    zzb.a(paramParcel, 3, paramStartBleScanRequest.b());
    zzb.a(paramParcel, 4, paramStartBleScanRequest.d(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public StartBleScanRequest a(Parcel paramParcel)
  {
    int i = 0;
    IBinder localIBinder1 = null;
    int k = zza.b(paramParcel);
    IBinder localIBinder2 = null;
    ArrayList localArrayList = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        localArrayList = zza.c(paramParcel, m, DataType.CREATOR);
        break;
      case 1000: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        localIBinder2 = zza.q(paramParcel, m);
        break;
      case 3: 
        i = zza.g(paramParcel, m);
        break;
      case 4: 
        localIBinder1 = zza.q(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new StartBleScanRequest(j, localArrayList, localIBinder2, i, localIBinder1);
  }
  
  public StartBleScanRequest[] a(int paramInt)
  {
    return new StartBleScanRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzad.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */