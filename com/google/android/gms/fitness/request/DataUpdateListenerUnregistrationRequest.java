package com.google.android.gms.fitness.request;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.internal.zzow;
import com.google.android.gms.internal.zzow.zza;

public class DataUpdateListenerUnregistrationRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<DataUpdateListenerUnregistrationRequest> CREATOR = new zzl();
  private final int a;
  private final PendingIntent b;
  private final zzow c;
  
  DataUpdateListenerUnregistrationRequest(int paramInt, PendingIntent paramPendingIntent, IBinder paramIBinder)
  {
    this.a = paramInt;
    this.b = paramPendingIntent;
    this.c = zzow.zza.a(paramIBinder);
  }
  
  private boolean a(DataUpdateListenerUnregistrationRequest paramDataUpdateListenerUnregistrationRequest)
  {
    return zzw.a(this.b, paramDataUpdateListenerUnregistrationRequest.b);
  }
  
  public PendingIntent a()
  {
    return this.b;
  }
  
  public IBinder b()
  {
    if (this.c == null) {
      return null;
    }
    return this.c.asBinder();
  }
  
  int c()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof DataUpdateListenerUnregistrationRequest)) && (a((DataUpdateListenerUnregistrationRequest)paramObject)));
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b });
  }
  
  public String toString()
  {
    return String.format("DataUpdateListenerUnregistrationRequest", new Object[0]);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzl.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/DataUpdateListenerUnregistrationRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */