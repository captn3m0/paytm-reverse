package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzag
  implements Parcelable.Creator<UnclaimBleDeviceRequest>
{
  static void a(UnclaimBleDeviceRequest paramUnclaimBleDeviceRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1000, paramUnclaimBleDeviceRequest.c());
    zzb.a(paramParcel, 2, paramUnclaimBleDeviceRequest.a(), false);
    zzb.a(paramParcel, 3, paramUnclaimBleDeviceRequest.b(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public UnclaimBleDeviceRequest a(Parcel paramParcel)
  {
    IBinder localIBinder = null;
    int j = zza.b(paramParcel);
    int i = 0;
    String str = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str = zza.p(paramParcel, k);
        break;
      case 3: 
        localIBinder = zza.q(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new UnclaimBleDeviceRequest(i, str, localIBinder);
  }
  
  public UnclaimBleDeviceRequest[] a(int paramInt)
  {
    return new UnclaimBleDeviceRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzag.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */