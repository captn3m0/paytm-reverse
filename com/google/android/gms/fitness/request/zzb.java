package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.fitness.data.BleDevice;

public class zzb
  implements Parcelable.Creator<ClaimBleDeviceRequest>
{
  static void a(ClaimBleDeviceRequest paramClaimBleDeviceRequest, Parcel paramParcel, int paramInt)
  {
    int i = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramClaimBleDeviceRequest.a(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1000, paramClaimBleDeviceRequest.d());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramClaimBleDeviceRequest.b(), paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 3, paramClaimBleDeviceRequest.c(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, i);
  }
  
  public ClaimBleDeviceRequest a(Parcel paramParcel)
  {
    IBinder localIBinder = null;
    int j = zza.b(paramParcel);
    int i = 0;
    Object localObject2 = null;
    Object localObject1 = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      Object localObject3;
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject3;
      }
      for (;;)
      {
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject3;
        break;
        localObject3 = zza.p(paramParcel, k);
        localObject1 = localObject2;
        localObject2 = localObject3;
        continue;
        i = zza.g(paramParcel, k);
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject3;
        continue;
        localObject3 = (BleDevice)zza.a(paramParcel, k, BleDevice.CREATOR);
        localObject2 = localObject1;
        localObject1 = localObject3;
        continue;
        localIBinder = zza.q(paramParcel, k);
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject3;
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new ClaimBleDeviceRequest(i, (String)localObject1, (BleDevice)localObject2, localIBinder);
  }
  
  public ClaimBleDeviceRequest[] a(int paramInt)
  {
    return new ClaimBleDeviceRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */