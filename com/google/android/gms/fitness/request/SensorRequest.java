package com.google.android.gms.fitness.request;

import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.location.LocationRequest;
import java.util.concurrent.TimeUnit;

public class SensorRequest
{
  private final DataSource a;
  private final DataType b;
  private final long c;
  private final long d;
  private final long e;
  private final int f;
  private final LocationRequest g;
  private final long h;
  
  private boolean a(SensorRequest paramSensorRequest)
  {
    return (zzw.a(this.a, paramSensorRequest.a)) && (zzw.a(this.b, paramSensorRequest.b)) && (this.c == paramSensorRequest.c) && (this.d == paramSensorRequest.d) && (this.e == paramSensorRequest.e) && (this.f == paramSensorRequest.f) && (zzw.a(this.g, paramSensorRequest.g)) && (this.h == paramSensorRequest.h);
  }
  
  public long a(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.c, TimeUnit.MICROSECONDS);
  }
  
  public DataSource a()
  {
    return this.a;
  }
  
  public long b(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.d, TimeUnit.MICROSECONDS);
  }
  
  public DataType b()
  {
    return this.b;
  }
  
  public int c()
  {
    return this.f;
  }
  
  public long c(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(this.e, TimeUnit.MICROSECONDS);
  }
  
  public long d()
  {
    return this.h;
  }
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || (((paramObject instanceof SensorRequest)) && (a((SensorRequest)paramObject)));
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.a, this.b, Long.valueOf(this.c), Long.valueOf(this.d), Long.valueOf(this.e), Integer.valueOf(this.f), this.g, Long.valueOf(this.h) });
  }
  
  public String toString()
  {
    return zzw.a(this).a("dataSource", this.a).a("dataType", this.b).a("samplingRateMicros", Long.valueOf(this.c)).a("deliveryLatencyMicros", Long.valueOf(this.e)).a("timeOutMicros", Long.valueOf(this.h)).toString();
  }
  
  public static class Builder
  {
    private long a = -1L;
    private long b = 0L;
    private long c = 0L;
    private boolean d = false;
    private int e = 2;
    private long f = Long.MAX_VALUE;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/SensorRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */