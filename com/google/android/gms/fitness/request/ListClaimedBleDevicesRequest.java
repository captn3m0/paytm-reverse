package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.zzpj;
import com.google.android.gms.internal.zzpj.zza;

public class ListClaimedBleDevicesRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<ListClaimedBleDevicesRequest> CREATOR = new zzr();
  private final int a;
  private final zzpj b;
  
  ListClaimedBleDevicesRequest(int paramInt, IBinder paramIBinder)
  {
    this.a = paramInt;
    this.b = zzpj.zza.a(paramIBinder);
  }
  
  public ListClaimedBleDevicesRequest(zzpj paramzzpj)
  {
    this.a = 2;
    this.b = paramzzpj;
  }
  
  int a()
  {
    return this.a;
  }
  
  public IBinder b()
  {
    return this.b.asBinder();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzr.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/ListClaimedBleDevicesRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */