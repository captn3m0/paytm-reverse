package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataType;

public class zzc
  implements Parcelable.Creator<DailyTotalRequest>
{
  static void a(DailyTotalRequest paramDailyTotalRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramDailyTotalRequest.c(), false);
    zzb.a(paramParcel, 1000, paramDailyTotalRequest.a());
    zzb.a(paramParcel, 2, paramDailyTotalRequest.b(), paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public DailyTotalRequest a(Parcel paramParcel)
  {
    DataType localDataType = null;
    int j = zza.b(paramParcel);
    int i = 0;
    IBinder localIBinder = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localIBinder = zza.q(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localDataType = (DataType)zza.a(paramParcel, k, DataType.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new DailyTotalRequest(i, localIBinder, localDataType);
  }
  
  public DailyTotalRequest[] a(int paramInt)
  {
    return new DailyTotalRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */