package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.internal.zzow;
import com.google.android.gms.internal.zzow.zza;
import java.util.Collections;
import java.util.List;

public class StartBleScanRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<StartBleScanRequest> CREATOR = new zzad();
  private final int a;
  private final List<DataType> b;
  private final zzq c;
  private final int d;
  private final zzow e;
  
  StartBleScanRequest(int paramInt1, List<DataType> paramList, IBinder paramIBinder1, int paramInt2, IBinder paramIBinder2)
  {
    this.a = paramInt1;
    this.b = paramList;
    this.c = zzq.zza.a(paramIBinder1);
    this.d = paramInt2;
    this.e = zzow.zza.a(paramIBinder2);
  }
  
  public StartBleScanRequest(StartBleScanRequest paramStartBleScanRequest, zzow paramzzow)
  {
    this(paramStartBleScanRequest.b, paramStartBleScanRequest.c, paramStartBleScanRequest.d, paramzzow);
  }
  
  public StartBleScanRequest(List<DataType> paramList, zzq paramzzq, int paramInt, zzow paramzzow)
  {
    this.a = 4;
    this.b = paramList;
    this.c = paramzzq;
    this.d = paramInt;
    this.e = paramzzow;
  }
  
  public List<DataType> a()
  {
    return Collections.unmodifiableList(this.b);
  }
  
  public int b()
  {
    return this.d;
  }
  
  public IBinder c()
  {
    return this.c.asBinder();
  }
  
  public IBinder d()
  {
    if (this.e == null) {
      return null;
    }
    return this.e.asBinder();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  int e()
  {
    return this.a;
  }
  
  public String toString()
  {
    return zzw.a(this).a("dataTypes", this.b).a("timeoutSecs", Integer.valueOf(this.d)).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzad.a(this, paramParcel, paramInt);
  }
  
  public static class Builder
  {
    private DataType[] a = new DataType[0];
    private int b = 10;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/StartBleScanRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */