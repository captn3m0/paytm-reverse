package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.Subscription;

public class zzaf
  implements Parcelable.Creator<SubscribeRequest>
{
  static void a(SubscribeRequest paramSubscribeRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramSubscribeRequest.a(), paramInt, false);
    zzb.a(paramParcel, 1000, paramSubscribeRequest.d());
    zzb.a(paramParcel, 2, paramSubscribeRequest.b());
    zzb.a(paramParcel, 3, paramSubscribeRequest.c(), false);
    zzb.a(paramParcel, i);
  }
  
  public SubscribeRequest a(Parcel paramParcel)
  {
    IBinder localIBinder = null;
    boolean bool = false;
    int j = zza.b(paramParcel);
    Subscription localSubscription = null;
    int i = 0;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
      }
      for (;;)
      {
        break;
        localSubscription = (Subscription)zza.a(paramParcel, k, Subscription.CREATOR);
        continue;
        i = zza.g(paramParcel, k);
        continue;
        bool = zza.c(paramParcel, k);
        continue;
        localIBinder = zza.q(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new SubscribeRequest(i, localSubscription, bool, localIBinder);
  }
  
  public SubscribeRequest[] a(int paramInt)
  {
    return new SubscribeRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzaf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */