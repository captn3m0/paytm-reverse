package com.google.android.gms.fitness.request;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;

public class zzk
  implements Parcelable.Creator<DataUpdateListenerRegistrationRequest>
{
  static void a(DataUpdateListenerRegistrationRequest paramDataUpdateListenerRegistrationRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramDataUpdateListenerRegistrationRequest.a(), paramInt, false);
    zzb.a(paramParcel, 1000, paramDataUpdateListenerRegistrationRequest.e());
    zzb.a(paramParcel, 2, paramDataUpdateListenerRegistrationRequest.b(), paramInt, false);
    zzb.a(paramParcel, 3, paramDataUpdateListenerRegistrationRequest.c(), paramInt, false);
    zzb.a(paramParcel, 4, paramDataUpdateListenerRegistrationRequest.d(), false);
    zzb.a(paramParcel, i);
  }
  
  public DataUpdateListenerRegistrationRequest a(Parcel paramParcel)
  {
    IBinder localIBinder = null;
    int j = zza.b(paramParcel);
    int i = 0;
    PendingIntent localPendingIntent = null;
    DataType localDataType = null;
    DataSource localDataSource = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localDataSource = (DataSource)zza.a(paramParcel, k, DataSource.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localDataType = (DataType)zza.a(paramParcel, k, DataType.CREATOR);
        break;
      case 3: 
        localPendingIntent = (PendingIntent)zza.a(paramParcel, k, PendingIntent.CREATOR);
        break;
      case 4: 
        localIBinder = zza.q(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new DataUpdateListenerRegistrationRequest(i, localDataSource, localDataType, localPendingIntent, localIBinder);
  }
  
  public DataUpdateListenerRegistrationRequest[] a(int paramInt)
  {
    return new DataUpdateListenerRegistrationRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */