package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.internal.zzov;
import com.google.android.gms.internal.zzov.zza;

public class SessionStopRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<SessionStopRequest> CREATOR = new zzab();
  private final int a;
  private final String b;
  private final String c;
  private final zzov d;
  
  SessionStopRequest(int paramInt, String paramString1, String paramString2, IBinder paramIBinder)
  {
    this.a = paramInt;
    this.b = paramString1;
    this.c = paramString2;
    this.d = zzov.zza.a(paramIBinder);
  }
  
  public SessionStopRequest(String paramString1, String paramString2, zzov paramzzov)
  {
    this.a = 3;
    this.b = paramString1;
    this.c = paramString2;
    this.d = paramzzov;
  }
  
  private boolean a(SessionStopRequest paramSessionStopRequest)
  {
    return (zzw.a(this.b, paramSessionStopRequest.b)) && (zzw.a(this.c, paramSessionStopRequest.c));
  }
  
  public String a()
  {
    return this.b;
  }
  
  public String b()
  {
    return this.c;
  }
  
  public IBinder c()
  {
    if (this.d == null) {
      return null;
    }
    return this.d.asBinder();
  }
  
  int d()
  {
    return this.a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof SessionStopRequest)) && (a((SessionStopRequest)paramObject)));
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b, this.c });
  }
  
  public String toString()
  {
    return zzw.a(this).a("name", this.b).a("identifier", this.c).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzab.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/SessionStopRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */