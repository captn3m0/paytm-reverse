package com.google.android.gms.fitness.request;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzz
  implements Parcelable.Creator<SessionRegistrationRequest>
{
  static void a(SessionRegistrationRequest paramSessionRegistrationRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramSessionRegistrationRequest.a(), paramInt, false);
    zzb.a(paramParcel, 1000, paramSessionRegistrationRequest.d());
    zzb.a(paramParcel, 2, paramSessionRegistrationRequest.b(), false);
    zzb.a(paramParcel, 4, paramSessionRegistrationRequest.c());
    zzb.a(paramParcel, i);
  }
  
  public SessionRegistrationRequest a(Parcel paramParcel)
  {
    Object localObject2 = null;
    int i = 0;
    int k = zza.b(paramParcel);
    Object localObject1 = null;
    int j = 0;
    if (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      Object localObject3;
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject3;
      }
      for (;;)
      {
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject3;
        break;
        localObject3 = (PendingIntent)zza.a(paramParcel, m, PendingIntent.CREATOR);
        localObject1 = localObject2;
        localObject2 = localObject3;
        continue;
        j = zza.g(paramParcel, m);
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject3;
        continue;
        localObject3 = zza.q(paramParcel, m);
        localObject2 = localObject1;
        localObject1 = localObject3;
        continue;
        i = zza.g(paramParcel, m);
        localObject3 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject3;
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new SessionRegistrationRequest(j, (PendingIntent)localObject1, (IBinder)localObject2, i);
  }
  
  public SessionRegistrationRequest[] a(int paramInt)
  {
    return new SessionRegistrationRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */