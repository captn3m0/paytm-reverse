package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;

public class zzy
  implements Parcelable.Creator<SessionReadRequest>
{
  static void a(SessionReadRequest paramSessionReadRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramSessionReadRequest.a(), false);
    zzb.a(paramParcel, 1000, paramSessionReadRequest.k());
    zzb.a(paramParcel, 2, paramSessionReadRequest.b(), false);
    zzb.a(paramParcel, 3, paramSessionReadRequest.h());
    zzb.a(paramParcel, 4, paramSessionReadRequest.g());
    zzb.c(paramParcel, 5, paramSessionReadRequest.c(), false);
    zzb.c(paramParcel, 6, paramSessionReadRequest.d(), false);
    zzb.a(paramParcel, 7, paramSessionReadRequest.i());
    zzb.a(paramParcel, 8, paramSessionReadRequest.f());
    zzb.b(paramParcel, 9, paramSessionReadRequest.e(), false);
    zzb.a(paramParcel, 10, paramSessionReadRequest.j(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public SessionReadRequest a(Parcel paramParcel)
  {
    int j = zza.b(paramParcel);
    int i = 0;
    String str2 = null;
    String str1 = null;
    long l2 = 0L;
    long l1 = 0L;
    ArrayList localArrayList3 = null;
    ArrayList localArrayList2 = null;
    boolean bool2 = false;
    boolean bool1 = false;
    ArrayList localArrayList1 = null;
    IBinder localIBinder = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        str2 = zza.p(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str1 = zza.p(paramParcel, k);
        break;
      case 3: 
        l2 = zza.i(paramParcel, k);
        break;
      case 4: 
        l1 = zza.i(paramParcel, k);
        break;
      case 5: 
        localArrayList3 = zza.c(paramParcel, k, DataType.CREATOR);
        break;
      case 6: 
        localArrayList2 = zza.c(paramParcel, k, DataSource.CREATOR);
        break;
      case 7: 
        bool2 = zza.c(paramParcel, k);
        break;
      case 8: 
        bool1 = zza.c(paramParcel, k);
        break;
      case 9: 
        localArrayList1 = zza.D(paramParcel, k);
        break;
      case 10: 
        localIBinder = zza.q(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new SessionReadRequest(i, str2, str1, l2, l1, localArrayList3, localArrayList2, bool2, bool1, localArrayList1, localIBinder);
  }
  
  public SessionReadRequest[] a(int paramInt)
  {
    return new SessionReadRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/fitness/request/zzy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */