package com.google.android.gms.safetynet;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zza
  implements Parcelable.Creator<AttestationData>
{
  static void a(AttestationData paramAttestationData, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramAttestationData.a);
    zzb.a(paramParcel, 2, paramAttestationData.a(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public AttestationData a(Parcel paramParcel)
  {
    int j = com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel);
    int i = 0;
    String str = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = com.google.android.gms.common.internal.safeparcel.zza.a(paramParcel);
      switch (com.google.android.gms.common.internal.safeparcel.zza.a(k))
      {
      default: 
        com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel, k);
        break;
      case 1: 
        i = com.google.android.gms.common.internal.safeparcel.zza.g(paramParcel, k);
        break;
      case 2: 
        str = com.google.android.gms.common.internal.safeparcel.zza.p(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new AttestationData(i, str);
  }
  
  public AttestationData[] a(int paramInt)
  {
    return new AttestationData[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/safetynet/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */