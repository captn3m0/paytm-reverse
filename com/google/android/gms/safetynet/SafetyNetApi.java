package com.google.android.gms.safetynet;

import com.google.android.gms.common.api.Result;

public abstract interface SafetyNetApi
{
  public static abstract interface AttestationResult
    extends Result
  {}
  
  public static abstract interface SafeBrowsingResult
    extends Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/safetynet/SafetyNetApi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */