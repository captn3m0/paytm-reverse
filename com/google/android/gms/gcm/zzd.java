package com.google.android.gms.gcm;

import android.os.Bundle;

public class zzd
{
  public static final zzd a = new zzd(0, 30, 3600);
  public static final zzd b = new zzd(1, 30, 3600);
  private final int c;
  private final int d;
  private final int e;
  
  private zzd(int paramInt1, int paramInt2, int paramInt3)
  {
    this.c = paramInt1;
    this.d = paramInt2;
    this.e = paramInt3;
  }
  
  public int a()
  {
    return this.c;
  }
  
  public Bundle a(Bundle paramBundle)
  {
    paramBundle.putInt("retry_policy", this.c);
    paramBundle.putInt("initial_backoff_seconds", this.d);
    paramBundle.putInt("maximum_backoff_seconds", this.e);
    return paramBundle;
  }
  
  public int b()
  {
    return this.d;
  }
  
  public int c()
  {
    return this.e;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/gcm/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */