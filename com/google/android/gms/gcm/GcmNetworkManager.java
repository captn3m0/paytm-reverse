package com.google.android.gms.gcm;

import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.internal.zzx;
import java.util.Iterator;
import java.util.List;

public class GcmNetworkManager
{
  private static GcmNetworkManager a;
  private Context b;
  private final PendingIntent c;
  
  private GcmNetworkManager(Context paramContext)
  {
    this.b = paramContext;
    this.c = PendingIntent.getBroadcast(this.b, 0, new Intent(), 0);
  }
  
  private Intent a()
  {
    int i = GoogleCloudMessaging.c(this.b);
    if (i < GoogleCloudMessaging.a)
    {
      Log.e("GcmNetworkManager", "Google Play Services is not available, dropping GcmNetworkManager request. code=" + i);
      return null;
    }
    Intent localIntent = new Intent("com.google.android.gms.gcm.ACTION_SCHEDULE");
    localIntent.setPackage(GoogleCloudMessaging.b(this.b));
    localIntent.putExtra("app", this.c);
    return localIntent;
  }
  
  public static GcmNetworkManager a(Context paramContext)
  {
    try
    {
      if (a == null) {
        a = new GcmNetworkManager(paramContext.getApplicationContext());
      }
      paramContext = a;
      return paramContext;
    }
    finally {}
  }
  
  static void a(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      throw new IllegalArgumentException("Must provide a valid tag.");
    }
    if (100 < paramString.length()) {
      throw new IllegalArgumentException("Tag is larger than max permissible tag length (100)");
    }
  }
  
  private void b(String paramString)
  {
    boolean bool2 = true;
    zzx.a(paramString, "GcmTaskService must not be null.");
    Object localObject = new Intent("com.google.android.gms.gcm.ACTION_TASK_READY");
    ((Intent)localObject).setPackage(this.b.getPackageName());
    localObject = this.b.getPackageManager().queryIntentServices((Intent)localObject, 0);
    if ((localObject != null) && (((List)localObject).size() != 0))
    {
      bool1 = true;
      zzx.b(bool1, "There is no GcmTaskService component registered within this package. Have you extended GcmTaskService correctly?");
      localObject = ((List)localObject).iterator();
      do
      {
        if (!((Iterator)localObject).hasNext()) {
          break;
        }
      } while (!((ResolveInfo)((Iterator)localObject).next()).serviceInfo.name.equals(paramString));
    }
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzx.b(bool1, "The GcmTaskService class you provided " + paramString + " does not seem to support receiving" + " com.google.android.gms.gcm.ACTION_TASK_READY.");
      return;
      bool1 = false;
      break;
    }
  }
  
  public void a(Task paramTask)
  {
    b(paramTask.c());
    Intent localIntent = a();
    if (localIntent == null) {
      return;
    }
    Bundle localBundle = localIntent.getExtras();
    localBundle.putString("scheduler_action", "SCHEDULE_TASK");
    paramTask.a(localBundle);
    localIntent.putExtras(localBundle);
    this.b.sendBroadcast(localIntent);
  }
  
  public void a(String paramString, Class<? extends GcmTaskService> paramClass)
  {
    b(paramString, paramClass);
  }
  
  public void b(String paramString, Class<? extends Service> paramClass)
  {
    a(paramString);
    b(paramClass.getName());
    Intent localIntent = a();
    if (localIntent == null) {
      return;
    }
    localIntent.putExtra("scheduler_action", "CANCEL_TASK");
    localIntent.putExtra("tag", paramString);
    localIntent.putExtra("component", new ComponentName(this.b, paramClass));
    this.b.sendBroadcast(localIntent);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/gcm/GcmNetworkManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */