package com.google.android.gms.gcm;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public class OneoffTask
  extends Task
{
  public static final Parcelable.Creator<OneoffTask> CREATOR = new Parcelable.Creator()
  {
    public OneoffTask a(Parcel paramAnonymousParcel)
    {
      return new OneoffTask(paramAnonymousParcel, null);
    }
    
    public OneoffTask[] a(int paramAnonymousInt)
    {
      return new OneoffTask[paramAnonymousInt];
    }
  };
  private final long a;
  private final long b;
  
  @Deprecated
  private OneoffTask(Parcel paramParcel)
  {
    super(paramParcel);
    this.a = paramParcel.readLong();
    this.b = paramParcel.readLong();
  }
  
  private OneoffTask(Builder paramBuilder)
  {
    super(paramBuilder);
    this.a = Builder.a(paramBuilder);
    this.b = Builder.b(paramBuilder);
  }
  
  public long a()
  {
    return this.a;
  }
  
  public void a(Bundle paramBundle)
  {
    super.a(paramBundle);
    paramBundle.putLong("window_start", this.a);
    paramBundle.putLong("window_end", this.b);
  }
  
  public long b()
  {
    return this.b;
  }
  
  public String toString()
  {
    return super.toString() + " " + "windowStart=" + a() + " " + "windowEnd=" + b();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    paramParcel.writeLong(this.a);
    paramParcel.writeLong(this.b);
  }
  
  public static class Builder
    extends Task.Builder
  {
    private long i = -1L;
    private long j = -1L;
    
    public Builder()
    {
      this.e = false;
    }
    
    public Builder a(int paramInt)
    {
      this.a = paramInt;
      return this;
    }
    
    public Builder a(long paramLong1, long paramLong2)
    {
      this.i = paramLong1;
      this.j = paramLong2;
      return this;
    }
    
    public Builder a(Class<? extends GcmTaskService> paramClass)
    {
      this.b = paramClass.getName();
      return this;
    }
    
    public Builder a(String paramString)
    {
      this.c = paramString;
      return this;
    }
    
    public Builder a(boolean paramBoolean)
    {
      this.f = paramBoolean;
      return this;
    }
    
    protected void a()
    {
      super.a();
      if ((this.i == -1L) || (this.j == -1L)) {
        throw new IllegalArgumentException("Must specify an execution window using setExecutionWindow.");
      }
      if (this.i >= this.j) {
        throw new IllegalArgumentException("Window start must be shorter than window end.");
      }
    }
    
    public Builder b(boolean paramBoolean)
    {
      this.e = paramBoolean;
      return this;
    }
    
    public OneoffTask b()
    {
      a();
      return new OneoffTask(this, null);
    }
    
    public Builder c(boolean paramBoolean)
    {
      this.d = paramBoolean;
      return this;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/gcm/OneoffTask.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */