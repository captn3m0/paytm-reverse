package com.google.android.gms.gcm;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import java.util.Iterator;
import java.util.Set;

public abstract class GcmListenerService
  extends Service
{
  private final Object a = new Object();
  private int b;
  private int c = 0;
  
  private void a(Intent paramIntent)
  {
    for (;;)
    {
      int i;
      try
      {
        str = paramIntent.getAction();
        i = -1;
        switch (str.hashCode())
        {
        case 366519424: 
          Log.d("GcmListenerService", "Unknown intent action: " + paramIntent.getAction());
          b();
          return;
        }
      }
      finally
      {
        String str;
        GcmReceiver.a(paramIntent);
      }
      if (str.equals("com.google.android.c2dm.intent.RECEIVE"))
      {
        i = 0;
        break label136;
        if (str.equals("com.google.android.gms.gcm.NOTIFICATION_DISMISS"))
        {
          i = 1;
          break label136;
          b(paramIntent);
          continue;
          if (!b(paramIntent.getExtras())) {
            continue;
          }
          zza.b(this, paramIntent);
          continue;
        }
      }
      label136:
      switch (i)
      {
      }
    }
  }
  
  static void a(Bundle paramBundle)
  {
    paramBundle = paramBundle.keySet().iterator();
    while (paramBundle.hasNext())
    {
      String str = (String)paramBundle.next();
      if ((str != null) && (str.startsWith("google.c."))) {
        paramBundle.remove();
      }
    }
  }
  
  private void b()
  {
    synchronized (this.a)
    {
      this.c -= 1;
      if (this.c == 0) {
        a(this.b);
      }
      return;
    }
  }
  
  private void b(Intent paramIntent)
  {
    String str2 = paramIntent.getStringExtra("message_type");
    String str1 = str2;
    if (str2 == null) {
      str1 = "gcm";
    }
    int i = -1;
    switch (str1.hashCode())
    {
    }
    for (;;)
    {
      switch (i)
      {
      default: 
        Log.w("GcmListenerService", "Received message with unknown type: " + str1);
        return;
        if (str1.equals("gcm"))
        {
          i = 0;
          continue;
          if (str1.equals("deleted_messages"))
          {
            i = 1;
            continue;
            if (str1.equals("send_event"))
            {
              i = 2;
              continue;
              if (str1.equals("send_error")) {
                i = 3;
              }
            }
          }
        }
        break;
      }
    }
    if (b(paramIntent.getExtras())) {
      zza.a(this, paramIntent);
    }
    c(paramIntent);
    return;
    a();
    return;
    a(paramIntent.getStringExtra("google.message_id"));
    return;
    a(paramIntent.getStringExtra("google.message_id"), paramIntent.getStringExtra("error"));
  }
  
  static boolean b(Bundle paramBundle)
  {
    return "1".equals(paramBundle.getString("google.c.a.e"));
  }
  
  private void c(Intent paramIntent)
  {
    Bundle localBundle = paramIntent.getExtras();
    localBundle.remove("message_type");
    localBundle.remove("android.support.content.wakelockid");
    if (zzb.a(localBundle))
    {
      if (!zzb.a(this))
      {
        zzb.a(this, getClass()).c(localBundle);
        return;
      }
      if (b(paramIntent.getExtras())) {
        zza.c(this, paramIntent);
      }
      zzb.b(localBundle);
    }
    paramIntent = localBundle.getString("from");
    localBundle.remove("from");
    a(localBundle);
    a(paramIntent, localBundle);
  }
  
  public void a() {}
  
  public void a(String paramString) {}
  
  public void a(String paramString, Bundle paramBundle) {}
  
  public void a(String paramString1, String paramString2) {}
  
  boolean a(int paramInt)
  {
    return stopSelfResult(paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/gcm/GcmListenerService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */