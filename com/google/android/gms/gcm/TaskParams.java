package com.google.android.gms.gcm;

import android.os.Bundle;

public class TaskParams
{
  private final String a;
  private final Bundle b;
  
  public TaskParams(String paramString, Bundle paramBundle)
  {
    this.a = paramString;
    this.b = paramBundle;
  }
  
  public String a()
  {
    return this.a;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/gcm/TaskParams.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */