package com.google.android.gms.gcm;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public class PeriodicTask
  extends Task
{
  public static final Parcelable.Creator<PeriodicTask> CREATOR = new Parcelable.Creator()
  {
    public PeriodicTask a(Parcel paramAnonymousParcel)
    {
      return new PeriodicTask(paramAnonymousParcel, null);
    }
    
    public PeriodicTask[] a(int paramAnonymousInt)
    {
      return new PeriodicTask[paramAnonymousInt];
    }
  };
  protected long a = -1L;
  protected long b = -1L;
  
  @Deprecated
  private PeriodicTask(Parcel paramParcel)
  {
    super(paramParcel);
    this.a = paramParcel.readLong();
    this.b = Math.min(paramParcel.readLong(), this.a);
  }
  
  private PeriodicTask(Builder paramBuilder)
  {
    super(paramBuilder);
    this.a = Builder.a(paramBuilder);
    this.b = Math.min(Builder.b(paramBuilder), this.a);
  }
  
  public long a()
  {
    return this.a;
  }
  
  public void a(Bundle paramBundle)
  {
    super.a(paramBundle);
    paramBundle.putLong("period", this.a);
    paramBundle.putLong("period_flex", this.b);
  }
  
  public long b()
  {
    return this.b;
  }
  
  public String toString()
  {
    return super.toString() + " " + "period=" + a() + " " + "flex=" + b();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    paramParcel.writeLong(this.a);
    paramParcel.writeLong(this.b);
  }
  
  public static class Builder
    extends Task.Builder
  {
    private long i = -1L;
    private long j = -1L;
    
    public Builder()
    {
      this.e = true;
    }
    
    public Builder a(int paramInt)
    {
      this.a = paramInt;
      return this;
    }
    
    public Builder a(long paramLong)
    {
      this.i = paramLong;
      return this;
    }
    
    public Builder a(Class<? extends GcmTaskService> paramClass)
    {
      this.b = paramClass.getName();
      return this;
    }
    
    public Builder a(String paramString)
    {
      this.c = paramString;
      return this;
    }
    
    public Builder a(boolean paramBoolean)
    {
      this.f = paramBoolean;
      return this;
    }
    
    protected void a()
    {
      super.a();
      if (this.i == -1L) {
        throw new IllegalArgumentException("Must call setPeriod(long) to establish an execution interval for this periodic task.");
      }
      if (this.i <= 0L) {
        throw new IllegalArgumentException("Period set cannot be less or equal to 0: " + this.i);
      }
      if (this.j == -1L) {
        this.j = (((float)this.i * 0.1F));
      }
      while (this.j <= this.i) {
        return;
      }
      this.j = this.i;
    }
    
    public Builder b(long paramLong)
    {
      this.j = paramLong;
      return this;
    }
    
    public Builder b(boolean paramBoolean)
    {
      this.e = paramBoolean;
      return this;
    }
    
    public PeriodicTask b()
    {
      a();
      return new PeriodicTask(this, null);
    }
    
    public Builder c(boolean paramBoolean)
    {
      this.d = paramBoolean;
      return this;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/gcm/PeriodicTask.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */