package com.google.android.gms.gcm;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.os.Process;
import android.os.RemoteException;
import android.util.Log;
import java.util.HashSet;
import java.util.Set;

public abstract class GcmTaskService
  extends Service
{
  private final Set<String> a = new HashSet();
  private int b;
  
  private void a(String paramString)
  {
    synchronized (this.a)
    {
      this.a.remove(paramString);
      if (this.a.size() == 0) {
        stopSelf(this.b);
      }
      return;
    }
  }
  
  public abstract int a(TaskParams paramTaskParams);
  
  public void a() {}
  
  public IBinder onBind(Intent paramIntent)
  {
    return null;
  }
  
  public int onStartCommand(Intent arg1, int paramInt1, int paramInt2)
  {
    ???.setExtrasClassLoader(PendingCallback.class.getClassLoader());
    if ("com.google.android.gms.gcm.ACTION_TASK_READY".equals(???.getAction()))
    {
      str = ???.getStringExtra("tag");
      localParcelable = ???.getParcelableExtra("callback");
      localBundle = (Bundle)???.getParcelableExtra("extras");
      if ((localParcelable == null) || (!(localParcelable instanceof PendingCallback))) {
        Log.e("GcmTaskService", getPackageName() + " " + str + ": Could not process request, invalid callback.");
      }
    }
    while (!"com.google.android.gms.gcm.SERVICE_ACTION_INITIALIZE".equals(???.getAction()))
    {
      String str;
      Parcelable localParcelable;
      Bundle localBundle;
      return 2;
      synchronized (this.a)
      {
        this.a.add(str);
        stopSelf(this.b);
        this.b = paramInt2;
        new zza(str, ((PendingCallback)localParcelable).a(), localBundle).start();
        return 2;
      }
    }
    a();
    synchronized (this.a)
    {
      this.b = paramInt2;
      if (this.a.size() == 0) {
        stopSelf(this.b);
      }
      return 2;
    }
  }
  
  private class zza
    extends Thread
  {
    private final String b;
    private final zzc c;
    private final Bundle d;
    
    zza(String paramString, IBinder paramIBinder, Bundle paramBundle)
    {
      this.b = paramString;
      this.c = zzc.zza.a(paramIBinder);
      this.d = paramBundle;
    }
    
    public void run()
    {
      Process.setThreadPriority(10);
      int i = GcmTaskService.this.a(new TaskParams(this.b, this.d));
      try
      {
        this.c.a(i);
        return;
      }
      catch (RemoteException localRemoteException)
      {
        Log.e("GcmTaskService", "Error reporting result of operation to scheduler for " + this.b);
        return;
      }
      finally
      {
        GcmTaskService.a(GcmTaskService.this, this.b);
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/gcm/GcmTaskService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */