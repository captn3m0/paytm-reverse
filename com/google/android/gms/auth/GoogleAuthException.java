package com.google.android.gms.auth;

public class GoogleAuthException
  extends Exception
{
  public GoogleAuthException() {}
  
  public GoogleAuthException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/GoogleAuthException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */