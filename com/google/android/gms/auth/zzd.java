package com.google.android.gms.auth;

import android.accounts.Account;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.common.zze;
import com.google.android.gms.internal.zzas;
import com.google.android.gms.internal.zzas.zza;
import java.io.IOException;
import java.util.List;

public class zzd
{
  private static final ComponentName a;
  private static final ComponentName b;
  public static final String c;
  public static final String d;
  
  static
  {
    if (Build.VERSION.SDK_INT >= 11)
    {
      c = "callerUid";
      if (Build.VERSION.SDK_INT < 14) {
        break label58;
      }
    }
    label58:
    for (;;)
    {
      d = "androidPackageName";
      a = new ComponentName("com.google.android.gms", "com.google.android.gms.auth.GetToken");
      b = new ComponentName("com.google.android.gms", "com.google.android.gms.recovery.RecoveryService");
      return;
      break;
    }
  }
  
  /* Error */
  private static <T> T a(Context paramContext, ComponentName paramComponentName, zza<T> paramzza)
    throws IOException, GoogleAuthException
  {
    // Byte code:
    //   0: new 65	com/google/android/gms/common/zza
    //   3: dup
    //   4: invokespecial 66	com/google/android/gms/common/zza:<init>	()V
    //   7: astore_3
    //   8: aload_0
    //   9: invokestatic 71	com/google/android/gms/common/internal/zzl:a	(Landroid/content/Context;)Lcom/google/android/gms/common/internal/zzl;
    //   12: astore 4
    //   14: aload 4
    //   16: aload_1
    //   17: aload_3
    //   18: ldc 73
    //   20: invokevirtual 76	com/google/android/gms/common/internal/zzl:a	(Landroid/content/ComponentName;Landroid/content/ServiceConnection;Ljava/lang/String;)Z
    //   23: ifeq +58 -> 81
    //   26: aload_2
    //   27: aload_3
    //   28: invokevirtual 79	com/google/android/gms/common/zza:a	()Landroid/os/IBinder;
    //   31: invokeinterface 82 2 0
    //   36: astore_0
    //   37: aload 4
    //   39: aload_1
    //   40: aload_3
    //   41: ldc 73
    //   43: invokevirtual 85	com/google/android/gms/common/internal/zzl:b	(Landroid/content/ComponentName;Landroid/content/ServiceConnection;Ljava/lang/String;)V
    //   46: aload_0
    //   47: areturn
    //   48: astore_0
    //   49: ldc 73
    //   51: ldc 87
    //   53: aload_0
    //   54: invokestatic 93	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   57: pop
    //   58: new 57	java/io/IOException
    //   61: dup
    //   62: ldc 87
    //   64: aload_0
    //   65: invokespecial 96	java/io/IOException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   68: athrow
    //   69: astore_0
    //   70: aload 4
    //   72: aload_1
    //   73: aload_3
    //   74: ldc 73
    //   76: invokevirtual 85	com/google/android/gms/common/internal/zzl:b	(Landroid/content/ComponentName;Landroid/content/ServiceConnection;Ljava/lang/String;)V
    //   79: aload_0
    //   80: athrow
    //   81: new 57	java/io/IOException
    //   84: dup
    //   85: ldc 98
    //   87: invokespecial 101	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   90: athrow
    //   91: astore_0
    //   92: goto -43 -> 49
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	95	0	paramContext	Context
    //   0	95	1	paramComponentName	ComponentName
    //   0	95	2	paramzza	zza<T>
    //   7	67	3	localzza	com.google.android.gms.common.zza
    //   12	59	4	localzzl	com.google.android.gms.common.internal.zzl
    // Exception table:
    //   from	to	target	type
    //   26	37	48	java/lang/InterruptedException
    //   26	37	69	finally
    //   49	69	69	finally
    //   26	37	91	android/os/RemoteException
  }
  
  public static String a(Context paramContext, Account paramAccount, String paramString)
    throws IOException, UserRecoverableAuthException, GoogleAuthException
  {
    return a(paramContext, paramAccount, paramString, new Bundle());
  }
  
  public static String a(Context paramContext, Account paramAccount, String paramString, Bundle paramBundle)
    throws IOException, UserRecoverableAuthException, GoogleAuthException
  {
    return b(paramContext, paramAccount, paramString, paramBundle).a();
  }
  
  private static void a(Context paramContext)
    throws GoogleAuthException
  {
    try
    {
      zze.d(paramContext.getApplicationContext());
      return;
    }
    catch (GooglePlayServicesRepairableException paramContext)
    {
      throw new GooglePlayServicesAvailabilityException(paramContext.a(), paramContext.getMessage(), paramContext.b());
    }
    catch (GooglePlayServicesNotAvailableException paramContext)
    {
      throw new GoogleAuthException(paramContext.getMessage());
    }
  }
  
  public static TokenData b(Context paramContext, Account paramAccount, final String paramString, final Bundle paramBundle)
    throws IOException, UserRecoverableAuthException, GoogleAuthException
  {
    zzx.c("Calling this from your main thread can lead to deadlock");
    a(paramContext);
    if (paramBundle == null) {}
    for (paramBundle = new Bundle();; paramBundle = new Bundle(paramBundle))
    {
      String str = paramContext.getApplicationInfo().packageName;
      paramBundle.putString("clientPackageName", str);
      if (TextUtils.isEmpty(paramBundle.getString(d))) {
        paramBundle.putString(d, str);
      }
      paramBundle.putLong("service_connection_start_time_millis", SystemClock.elapsedRealtime());
      paramAccount = new zza()
      {
        public TokenData a(IBinder paramAnonymousIBinder)
          throws RemoteException, IOException, GoogleAuthException
        {
          Object localObject = (Bundle)zzd.a(zzas.zza.a(paramAnonymousIBinder).a(this.a, paramString, paramBundle));
          paramAnonymousIBinder = TokenData.a((Bundle)localObject, "tokenDetails");
          if (paramAnonymousIBinder != null) {
            return paramAnonymousIBinder;
          }
          paramAnonymousIBinder = ((Bundle)localObject).getString("Error");
          localObject = (Intent)((Bundle)localObject).getParcelable("userRecoveryIntent");
          com.google.android.gms.auth.firstparty.shared.zzd localzzd = com.google.android.gms.auth.firstparty.shared.zzd.a(paramAnonymousIBinder);
          if (com.google.android.gms.auth.firstparty.shared.zzd.a(localzzd)) {
            throw new UserRecoverableAuthException(paramAnonymousIBinder, (Intent)localObject);
          }
          if (com.google.android.gms.auth.firstparty.shared.zzd.c(localzzd)) {
            throw new IOException(paramAnonymousIBinder);
          }
          throw new GoogleAuthException(paramAnonymousIBinder);
        }
      };
      return (TokenData)a(paramContext, a, paramAccount);
    }
  }
  
  private static <T> T b(T paramT)
    throws IOException
  {
    if (paramT == null)
    {
      Log.w("GoogleAuthUtil", "Binder call returned null.");
      throw new IOException("Service unavailable.");
    }
    return paramT;
  }
  
  @Deprecated
  public static String b(Context paramContext, String paramString1, String paramString2)
    throws IOException, UserRecoverableAuthException, GoogleAuthException
  {
    return a(paramContext, new Account(paramString1, "com.google"), paramString2);
  }
  
  public static void b(Context paramContext, String paramString)
    throws GooglePlayServicesAvailabilityException, GoogleAuthException, IOException
  {
    zzx.c("Calling this from your main thread can lead to deadlock");
    a(paramContext);
    final Bundle localBundle = new Bundle();
    String str = paramContext.getApplicationInfo().packageName;
    localBundle.putString("clientPackageName", str);
    if (!localBundle.containsKey(d)) {
      localBundle.putString(d, str);
    }
    paramString = new zza()
    {
      public Void a(IBinder paramAnonymousIBinder)
        throws RemoteException, IOException, GoogleAuthException
      {
        paramAnonymousIBinder = (Bundle)zzd.a(zzas.zza.a(paramAnonymousIBinder).a(this.a, localBundle));
        String str = paramAnonymousIBinder.getString("Error");
        if (!paramAnonymousIBinder.getBoolean("booleanResult")) {
          throw new GoogleAuthException(str);
        }
        return null;
      }
    };
    a(paramContext, a, paramString);
  }
  
  private static abstract interface zza<T>
  {
    public abstract T b(IBinder paramIBinder)
      throws RemoteException, IOException, GoogleAuthException;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */