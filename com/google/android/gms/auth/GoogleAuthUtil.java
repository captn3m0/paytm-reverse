package com.google.android.gms.auth;

import android.content.Context;
import java.io.IOException;

public final class GoogleAuthUtil
  extends zzd
{
  public static final String a = zzd.c;
  public static final String b = zzd.d;
  
  @Deprecated
  public static String a(Context paramContext, String paramString1, String paramString2)
    throws IOException, UserRecoverableAuthException, GoogleAuthException
  {
    return zzd.b(paramContext, paramString1, paramString2);
  }
  
  public static void a(Context paramContext, String paramString)
    throws GooglePlayServicesAvailabilityException, GoogleAuthException, IOException
  {
    zzd.b(paramContext, paramString);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/GoogleAuthUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */