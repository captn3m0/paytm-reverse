package com.google.android.gms.auth.api;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.auth.api.consent.zza;
import com.google.android.gms.auth.api.credentials.CredentialsApi;
import com.google.android.gms.auth.api.credentials.PasswordSpecification;
import com.google.android.gms.auth.api.proxy.ProxyApi;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.internal.zzc;
import com.google.android.gms.auth.api.signin.internal.zzn;
import com.google.android.gms.auth.api.signin.internal.zzo;
import com.google.android.gms.auth.api.signin.zzg;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.ApiOptions.NoOptions;
import com.google.android.gms.common.api.Api.ApiOptions.Optional;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.internal.zzkq;
import com.google.android.gms.internal.zzkr;
import com.google.android.gms.internal.zzks;
import com.google.android.gms.internal.zzkv;
import com.google.android.gms.internal.zzkw;
import com.google.android.gms.internal.zzkz;
import com.google.android.gms.internal.zzld;
import java.util.Collections;
import java.util.List;

public final class Auth
{
  public static final Api.zzc<zzkz> a = new Api.zzc();
  public static final Api.zzc<com.google.android.gms.auth.api.credentials.internal.zzf> b = new Api.zzc();
  public static final Api.zzc<zzks> c = new Api.zzc();
  public static final Api.zzc<zzo> d = new Api.zzc();
  public static final Api.zzc<com.google.android.gms.auth.api.signin.internal.zzd> e = new Api.zzc();
  public static final Api.zzc<zzkw> f = new Api.zzc();
  public static final Api<zza> g = new Api("Auth.PROXY_API", s, a);
  public static final Api<AuthCredentialsOptions> h = new Api("Auth.CREDENTIALS_API", t, b);
  public static final Api<zzg> i = new Api("Auth.SIGN_IN_API", w, d);
  public static final Api<GoogleSignInOptions> j = new Api("Auth.GOOGLE_SIGN_IN_API", x, e);
  public static final Api<Api.ApiOptions.NoOptions> k = new Api("Auth.ACCOUNT_STATUS_API", u, c);
  public static final Api<Api.ApiOptions.NoOptions> l = new Api("Auth.CONSENT_API", v, f);
  public static final ProxyApi m = new zzld();
  public static final CredentialsApi n = new com.google.android.gms.auth.api.credentials.internal.zzd();
  public static final zzkq o = new zzkr();
  public static final com.google.android.gms.auth.api.signin.zzf p = new zzn();
  public static final GoogleSignInApi q = new zzc();
  public static final zza r = new zzkv();
  private static final Api.zza<zzkz, zza> s = new Api.zza()
  {
    public zzkz a(Context paramAnonymousContext, Looper paramAnonymousLooper, com.google.android.gms.common.internal.zzf paramAnonymouszzf, Auth.zza paramAnonymouszza, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      return new zzkz(paramAnonymousContext, paramAnonymousLooper, paramAnonymouszzf, paramAnonymouszza, paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
    }
  };
  private static final Api.zza<com.google.android.gms.auth.api.credentials.internal.zzf, AuthCredentialsOptions> t = new Api.zza()
  {
    public com.google.android.gms.auth.api.credentials.internal.zzf a(Context paramAnonymousContext, Looper paramAnonymousLooper, com.google.android.gms.common.internal.zzf paramAnonymouszzf, Auth.AuthCredentialsOptions paramAnonymousAuthCredentialsOptions, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      return new com.google.android.gms.auth.api.credentials.internal.zzf(paramAnonymousContext, paramAnonymousLooper, paramAnonymouszzf, paramAnonymousAuthCredentialsOptions, paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
    }
  };
  private static final Api.zza<zzks, Api.ApiOptions.NoOptions> u = new Api.zza()
  {
    public zzks a(Context paramAnonymousContext, Looper paramAnonymousLooper, com.google.android.gms.common.internal.zzf paramAnonymouszzf, Api.ApiOptions.NoOptions paramAnonymousNoOptions, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      return new zzks(paramAnonymousContext, paramAnonymousLooper, paramAnonymouszzf, paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
    }
  };
  private static final Api.zza<zzkw, Api.ApiOptions.NoOptions> v = new Api.zza()
  {
    public zzkw a(Context paramAnonymousContext, Looper paramAnonymousLooper, com.google.android.gms.common.internal.zzf paramAnonymouszzf, Api.ApiOptions.NoOptions paramAnonymousNoOptions, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      return new zzkw(paramAnonymousContext, paramAnonymousLooper, paramAnonymouszzf, paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
    }
  };
  private static final Api.zza<zzo, zzg> w = new Api.zza()
  {
    public zzo a(Context paramAnonymousContext, Looper paramAnonymousLooper, com.google.android.gms.common.internal.zzf paramAnonymouszzf, zzg paramAnonymouszzg, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      return new zzo(paramAnonymousContext, paramAnonymousLooper, paramAnonymouszzf, paramAnonymouszzg, paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
    }
  };
  private static final Api.zza<com.google.android.gms.auth.api.signin.internal.zzd, GoogleSignInOptions> x = new Api.zza()
  {
    public com.google.android.gms.auth.api.signin.internal.zzd a(Context paramAnonymousContext, Looper paramAnonymousLooper, com.google.android.gms.common.internal.zzf paramAnonymouszzf, @Nullable GoogleSignInOptions paramAnonymousGoogleSignInOptions, GoogleApiClient.ConnectionCallbacks paramAnonymousConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramAnonymousOnConnectionFailedListener)
    {
      return new com.google.android.gms.auth.api.signin.internal.zzd(paramAnonymousContext, paramAnonymousLooper, paramAnonymouszzf, paramAnonymousGoogleSignInOptions, paramAnonymousConnectionCallbacks, paramAnonymousOnConnectionFailedListener);
    }
    
    public List<Scope> a(@Nullable GoogleSignInOptions paramAnonymousGoogleSignInOptions)
    {
      if (paramAnonymousGoogleSignInOptions == null) {
        return Collections.emptyList();
      }
      return paramAnonymousGoogleSignInOptions.a();
    }
  };
  
  public static final class AuthCredentialsOptions
    implements Api.ApiOptions.Optional
  {
    private final String a;
    private final PasswordSpecification b;
    
    public Bundle a()
    {
      Bundle localBundle = new Bundle();
      localBundle.putString("consumer_package", this.a);
      localBundle.putParcelable("password_specification", this.b);
      return localBundle;
    }
    
    public static class Builder
    {
      @NonNull
      private PasswordSpecification a = PasswordSpecification.a;
    }
  }
  
  public static final class zza
    implements Api.ApiOptions.Optional
  {
    private final Bundle a;
    
    public Bundle a()
    {
      return new Bundle(this.a);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/Auth.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */