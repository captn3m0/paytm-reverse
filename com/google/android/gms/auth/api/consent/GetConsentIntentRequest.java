package com.google.android.gms.auth.api.consent;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.auth.firstparty.shared.ScopeDetail;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;

public class GetConsentIntentRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<GetConsentIntentRequest> CREATOR = new zzb();
  final ScopeDetail[] a;
  private final int b;
  private final String c;
  private final int d;
  private final String e;
  private final Account f;
  private final boolean g;
  private final int h;
  private final String i;
  
  GetConsentIntentRequest(int paramInt1, String paramString1, int paramInt2, String paramString2, Account paramAccount, ScopeDetail[] paramArrayOfScopeDetail, boolean paramBoolean, int paramInt3, String paramString3)
  {
    this.b = paramInt1;
    this.c = paramString1;
    this.d = paramInt2;
    this.e = paramString2;
    this.f = ((Account)zzx.a(paramAccount));
    this.a = paramArrayOfScopeDetail;
    this.g = paramBoolean;
    this.h = paramInt3;
    this.i = paramString3;
  }
  
  public int a()
  {
    return this.b;
  }
  
  public String b()
  {
    return this.c;
  }
  
  public int c()
  {
    return this.d;
  }
  
  public String d()
  {
    return this.e;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Account e()
  {
    return this.f;
  }
  
  public boolean f()
  {
    return this.g;
  }
  
  public int g()
  {
    return this.h;
  }
  
  public String h()
  {
    return this.i;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/consent/GetConsentIntentRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */