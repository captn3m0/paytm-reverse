package com.google.android.gms.auth.api.consent;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.auth.firstparty.shared.ScopeDetail;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class zzb
  implements Parcelable.Creator<GetConsentIntentRequest>
{
  static void a(GetConsentIntentRequest paramGetConsentIntentRequest, Parcel paramParcel, int paramInt)
  {
    int i = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramGetConsentIntentRequest.a());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramGetConsentIntentRequest.b(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 3, paramGetConsentIntentRequest.c());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 4, paramGetConsentIntentRequest.d(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 5, paramGetConsentIntentRequest.e(), paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 6, paramGetConsentIntentRequest.a, paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 7, paramGetConsentIntentRequest.f());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 8, paramGetConsentIntentRequest.g());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 9, paramGetConsentIntentRequest.h(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, i);
  }
  
  public GetConsentIntentRequest a(Parcel paramParcel)
  {
    int i = 0;
    String str1 = null;
    int m = zza.b(paramParcel);
    boolean bool = false;
    ScopeDetail[] arrayOfScopeDetail = null;
    Account localAccount = null;
    String str2 = null;
    int j = 0;
    String str3 = null;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        str3 = zza.p(paramParcel, n);
        break;
      case 3: 
        j = zza.g(paramParcel, n);
        break;
      case 4: 
        str2 = zza.p(paramParcel, n);
        break;
      case 5: 
        localAccount = (Account)zza.a(paramParcel, n, Account.CREATOR);
        break;
      case 6: 
        arrayOfScopeDetail = (ScopeDetail[])zza.b(paramParcel, n, ScopeDetail.CREATOR);
        break;
      case 7: 
        bool = zza.c(paramParcel, n);
        break;
      case 8: 
        i = zza.g(paramParcel, n);
        break;
      case 9: 
        str1 = zza.p(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new GetConsentIntentRequest(k, str3, j, str2, localAccount, arrayOfScopeDetail, bool, i, str1);
  }
  
  public GetConsentIntentRequest[] a(int paramInt)
  {
    return new GetConsentIntentRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/consent/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */