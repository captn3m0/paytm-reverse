package com.google.android.gms.auth.api.credentials;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzf
  implements Parcelable.Creator<PasswordSpecification>
{
  static void a(PasswordSpecification paramPasswordSpecification, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramPasswordSpecification.d, false);
    zzb.a(paramParcel, 1000, paramPasswordSpecification.c);
    zzb.b(paramParcel, 2, paramPasswordSpecification.e, false);
    zzb.a(paramParcel, 3, paramPasswordSpecification.f, false);
    zzb.a(paramParcel, 4, paramPasswordSpecification.g);
    zzb.a(paramParcel, 5, paramPasswordSpecification.h);
    zzb.a(paramParcel, paramInt);
  }
  
  public PasswordSpecification a(Parcel paramParcel)
  {
    ArrayList localArrayList1 = null;
    int i = 0;
    int m = zza.b(paramParcel);
    int j = 0;
    ArrayList localArrayList2 = null;
    String str = null;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        str = zza.p(paramParcel, n);
        break;
      case 1000: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        localArrayList2 = zza.D(paramParcel, n);
        break;
      case 3: 
        localArrayList1 = zza.C(paramParcel, n);
        break;
      case 4: 
        j = zza.g(paramParcel, n);
        break;
      case 5: 
        i = zza.g(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new PasswordSpecification(k, str, localArrayList2, localArrayList1, j, i);
  }
  
  public PasswordSpecification[] a(int paramInt)
  {
    return new PasswordSpecification[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/credentials/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */