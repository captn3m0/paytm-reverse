package com.google.android.gms.auth.api.credentials;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzx;
import java.util.Collections;
import java.util.List;

public class Credential
  implements SafeParcelable
{
  public static final Parcelable.Creator<Credential> CREATOR = new zza();
  final int a;
  private final String b;
  @Nullable
  private final String c;
  @Nullable
  private final Uri d;
  private final List<IdToken> e;
  @Nullable
  private final String f;
  @Nullable
  private final String g;
  @Nullable
  private final String h;
  @Nullable
  private final String i;
  
  Credential(int paramInt, String paramString1, String paramString2, Uri paramUri, List<IdToken> paramList, String paramString3, String paramString4, String paramString5, String paramString6)
  {
    this.a = paramInt;
    paramString1 = ((String)zzx.a(paramString1, "credential identifier cannot be null")).trim();
    zzx.a(paramString1, "credential identifier cannot be empty");
    this.b = paramString1;
    paramString1 = paramString2;
    if (paramString2 != null)
    {
      paramString1 = paramString2;
      if (TextUtils.isEmpty(paramString2.trim())) {
        paramString1 = null;
      }
    }
    this.c = paramString1;
    this.d = paramUri;
    if (paramList == null) {}
    for (paramString1 = Collections.emptyList();; paramString1 = Collections.unmodifiableList(paramList))
    {
      this.e = paramString1;
      this.f = paramString3;
      if ((paramString3 == null) || (!paramString3.isEmpty())) {
        break;
      }
      throw new IllegalArgumentException("password cannot be empty");
    }
    if (!TextUtils.isEmpty(paramString4))
    {
      paramString1 = Uri.parse(paramString4).getScheme();
      if ((!"http".equalsIgnoreCase(paramString1)) && (!"https".equalsIgnoreCase(paramString1))) {
        break label208;
      }
    }
    label208:
    for (boolean bool = true;; bool = false)
    {
      zzx.b(bool);
      this.g = paramString4;
      this.h = paramString5;
      this.i = paramString6;
      if ((TextUtils.isEmpty(this.f)) || (TextUtils.isEmpty(this.g))) {
        break;
      }
      throw new IllegalStateException("password and accountType cannot both be set");
    }
  }
  
  public String a()
  {
    return this.b;
  }
  
  @Nullable
  public String b()
  {
    return this.c;
  }
  
  @Nullable
  public Uri c()
  {
    return this.d;
  }
  
  public List<IdToken> d()
  {
    return this.e;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  @Nullable
  public String e()
  {
    return this.f;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof Credential)) {
        return false;
      }
      paramObject = (Credential)paramObject;
    } while ((TextUtils.equals(this.b, ((Credential)paramObject).b)) && (TextUtils.equals(this.c, ((Credential)paramObject).c)) && (zzw.a(this.d, ((Credential)paramObject).d)) && (TextUtils.equals(this.f, ((Credential)paramObject).f)) && (TextUtils.equals(this.g, ((Credential)paramObject).g)) && (TextUtils.equals(this.h, ((Credential)paramObject).h)));
    return false;
  }
  
  @Nullable
  public String f()
  {
    return this.h;
  }
  
  @Nullable
  public String g()
  {
    return this.g;
  }
  
  public String h()
  {
    return this.i;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b, this.c, this.d, this.f, this.g, this.h });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.a(this, paramParcel, paramInt);
  }
  
  public static class Builder {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/credentials/Credential.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */