package com.google.android.gms.auth.api.credentials;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzd
  implements Parcelable.Creator<HintRequest>
{
  static void a(HintRequest paramHintRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramHintRequest.a(), paramInt, false);
    zzb.a(paramParcel, 1000, paramHintRequest.a);
    zzb.a(paramParcel, 2, paramHintRequest.b());
    zzb.a(paramParcel, 3, paramHintRequest.c());
    zzb.a(paramParcel, 4, paramHintRequest.d(), false);
    zzb.a(paramParcel, i);
  }
  
  public HintRequest a(Parcel paramParcel)
  {
    String[] arrayOfString = null;
    boolean bool1 = false;
    int j = zza.b(paramParcel);
    boolean bool2 = false;
    CredentialPickerConfig localCredentialPickerConfig = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localCredentialPickerConfig = (CredentialPickerConfig)zza.a(paramParcel, k, CredentialPickerConfig.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        bool2 = zza.c(paramParcel, k);
        break;
      case 3: 
        bool1 = zza.c(paramParcel, k);
        break;
      case 4: 
        arrayOfString = zza.B(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new HintRequest(i, localCredentialPickerConfig, bool2, bool1, arrayOfString);
  }
  
  public HintRequest[] a(int paramInt)
  {
    return new HintRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/credentials/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */