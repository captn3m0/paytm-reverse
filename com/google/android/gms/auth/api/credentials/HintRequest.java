package com.google.android.gms.auth.api.credentials;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;

public final class HintRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<HintRequest> CREATOR = new zzd();
  final int a;
  private final CredentialPickerConfig b;
  private final boolean c;
  private final boolean d;
  private final String[] e;
  
  HintRequest(int paramInt, CredentialPickerConfig paramCredentialPickerConfig, boolean paramBoolean1, boolean paramBoolean2, String[] paramArrayOfString)
  {
    this.a = paramInt;
    this.b = ((CredentialPickerConfig)zzx.a(paramCredentialPickerConfig));
    this.c = paramBoolean1;
    this.d = paramBoolean2;
    this.e = ((String[])zzx.a(paramArrayOfString));
  }
  
  @NonNull
  public CredentialPickerConfig a()
  {
    return this.b;
  }
  
  public boolean b()
  {
    return this.c;
  }
  
  public boolean c()
  {
    return this.d;
  }
  
  @NonNull
  public String[] d()
  {
    return this.e;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzd.a(this, paramParcel, paramInt);
  }
  
  public static final class Builder
  {
    private CredentialPickerConfig a = new CredentialPickerConfig.Builder().a();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/credentials/HintRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */