package com.google.android.gms.auth.api.credentials;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class zzb
  implements Parcelable.Creator<CredentialPickerConfig>
{
  static void a(CredentialPickerConfig paramCredentialPickerConfig, Parcel paramParcel, int paramInt)
  {
    paramInt = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramCredentialPickerConfig.a());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1000, paramCredentialPickerConfig.a);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramCredentialPickerConfig.b());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 3, paramCredentialPickerConfig.c());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, paramInt);
  }
  
  public CredentialPickerConfig a(Parcel paramParcel)
  {
    boolean bool3 = false;
    int j = zza.b(paramParcel);
    boolean bool2 = false;
    boolean bool1 = false;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        bool1 = zza.c(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        bool2 = zza.c(paramParcel, k);
        break;
      case 3: 
        bool3 = zza.c(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new CredentialPickerConfig(i, bool1, bool2, bool3);
  }
  
  public CredentialPickerConfig[] a(int paramInt)
  {
    return new CredentialPickerConfig[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/credentials/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */