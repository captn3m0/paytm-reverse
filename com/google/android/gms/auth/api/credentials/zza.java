package com.google.android.gms.auth.api.credentials;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zza
  implements Parcelable.Creator<Credential>
{
  static void a(Credential paramCredential, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramCredential.a(), false);
    zzb.a(paramParcel, 1000, paramCredential.a);
    zzb.a(paramParcel, 2, paramCredential.b(), false);
    zzb.a(paramParcel, 3, paramCredential.c(), paramInt, false);
    zzb.c(paramParcel, 4, paramCredential.d(), false);
    zzb.a(paramParcel, 5, paramCredential.e(), false);
    zzb.a(paramParcel, 6, paramCredential.g(), false);
    zzb.a(paramParcel, 7, paramCredential.f(), false);
    zzb.a(paramParcel, 8, paramCredential.h(), false);
    zzb.a(paramParcel, i);
  }
  
  public Credential a(Parcel paramParcel)
  {
    String str1 = null;
    int j = com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel);
    int i = 0;
    String str2 = null;
    String str3 = null;
    String str4 = null;
    ArrayList localArrayList = null;
    Uri localUri = null;
    String str5 = null;
    String str6 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = com.google.android.gms.common.internal.safeparcel.zza.a(paramParcel);
      switch (com.google.android.gms.common.internal.safeparcel.zza.a(k))
      {
      default: 
        com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel, k);
        break;
      case 1: 
        str6 = com.google.android.gms.common.internal.safeparcel.zza.p(paramParcel, k);
        break;
      case 1000: 
        i = com.google.android.gms.common.internal.safeparcel.zza.g(paramParcel, k);
        break;
      case 2: 
        str5 = com.google.android.gms.common.internal.safeparcel.zza.p(paramParcel, k);
        break;
      case 3: 
        localUri = (Uri)com.google.android.gms.common.internal.safeparcel.zza.a(paramParcel, k, Uri.CREATOR);
        break;
      case 4: 
        localArrayList = com.google.android.gms.common.internal.safeparcel.zza.c(paramParcel, k, IdToken.CREATOR);
        break;
      case 5: 
        str4 = com.google.android.gms.common.internal.safeparcel.zza.p(paramParcel, k);
        break;
      case 6: 
        str3 = com.google.android.gms.common.internal.safeparcel.zza.p(paramParcel, k);
        break;
      case 7: 
        str2 = com.google.android.gms.common.internal.safeparcel.zza.p(paramParcel, k);
        break;
      case 8: 
        str1 = com.google.android.gms.common.internal.safeparcel.zza.p(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new Credential(i, str6, str5, localUri, localArrayList, str4, str3, str2, str1);
  }
  
  public Credential[] a(int paramInt)
  {
    return new Credential[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/credentials/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */