package com.google.android.gms.auth.api.credentials.internal;

import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.common.api.Status;

public class zza
  extends zzi.zza
{
  public void a(Status paramStatus)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(Status paramStatus, Credential paramCredential)
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(Status paramStatus, String paramString)
  {
    throw new UnsupportedOperationException();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/credentials/internal/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */