package com.google.android.gms.auth.api.credentials.internal;

import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.CredentialRequestResult;
import com.google.android.gms.common.api.Status;

public final class zzc
  implements CredentialRequestResult
{
  private final Status a;
  private final Credential b;
  
  public zzc(Status paramStatus, Credential paramCredential)
  {
    this.a = paramStatus;
    this.b = paramCredential;
  }
  
  public static zzc a(Status paramStatus)
  {
    return new zzc(paramStatus, null);
  }
  
  public Status getStatus()
  {
    return this.a;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/credentials/internal/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */