package com.google.android.gms.auth.api.credentials.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import com.google.android.gms.auth.api.Auth.AuthCredentialsOptions;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;

public final class zzf
  extends com.google.android.gms.common.internal.zzj<zzj>
{
  @Nullable
  private final Auth.AuthCredentialsOptions a;
  
  public zzf(Context paramContext, Looper paramLooper, com.google.android.gms.common.internal.zzf paramzzf, Auth.AuthCredentialsOptions paramAuthCredentialsOptions, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
  {
    super(paramContext, paramLooper, 68, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    this.a = paramAuthCredentialsOptions;
  }
  
  protected zzj a(IBinder paramIBinder)
  {
    return zzj.zza.a(paramIBinder);
  }
  
  protected String a()
  {
    return "com.google.android.gms.auth.api.credentials.service.START";
  }
  
  protected Bundle a_()
  {
    if (this.a == null) {
      return new Bundle();
    }
    return this.a.a();
  }
  
  protected String b()
  {
    return "com.google.android.gms.auth.api.credentials.internal.ICredentialsService";
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/credentials/internal/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */