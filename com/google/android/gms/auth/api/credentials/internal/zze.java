package com.google.android.gms.auth.api.credentials.internal;

import android.content.Context;
import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.internal.zza.zza;

abstract class zze<R extends Result>
  extends zza.zza<R, zzf>
{
  protected abstract void a(Context paramContext, zzj paramzzj)
    throws DeadObjectException, RemoteException;
  
  protected final void a(zzf paramzzf)
    throws DeadObjectException, RemoteException
  {
    a(paramzzf.q(), (zzj)paramzzf.v());
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/credentials/internal/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */