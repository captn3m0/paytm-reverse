package com.google.android.gms.auth.api.proxy;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class zzb
  implements Parcelable.Creator<ProxyRequest>
{
  static void a(ProxyRequest paramProxyRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramProxyRequest.k, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1000, paramProxyRequest.j);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramProxyRequest.l);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 3, paramProxyRequest.m);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 4, paramProxyRequest.n, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 5, paramProxyRequest.o, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, paramInt);
  }
  
  public ProxyRequest a(Parcel paramParcel)
  {
    int i = 0;
    Bundle localBundle = null;
    int k = zza.b(paramParcel);
    long l = 0L;
    byte[] arrayOfByte = null;
    String str = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        str = zza.p(paramParcel, m);
        break;
      case 1000: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        i = zza.g(paramParcel, m);
        break;
      case 3: 
        l = zza.i(paramParcel, m);
        break;
      case 4: 
        arrayOfByte = zza.s(paramParcel, m);
        break;
      case 5: 
        localBundle = zza.r(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new ProxyRequest(j, str, i, l, arrayOfByte, localBundle);
  }
  
  public ProxyRequest[] a(int paramInt)
  {
    return new ProxyRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/proxy/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */