package com.google.android.gms.auth.api.proxy;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zza
  implements Parcelable.Creator<ProxyGrpcRequest>
{
  static void a(ProxyGrpcRequest paramProxyGrpcRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramProxyGrpcRequest.b, false);
    zzb.a(paramParcel, 1000, paramProxyGrpcRequest.a);
    zzb.a(paramParcel, 2, paramProxyGrpcRequest.c);
    zzb.a(paramParcel, 3, paramProxyGrpcRequest.d);
    zzb.a(paramParcel, 4, paramProxyGrpcRequest.e, false);
    zzb.a(paramParcel, 5, paramProxyGrpcRequest.f, false);
    zzb.a(paramParcel, paramInt);
  }
  
  public ProxyGrpcRequest a(Parcel paramParcel)
  {
    int i = 0;
    String str1 = null;
    int k = com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel);
    long l = 0L;
    byte[] arrayOfByte = null;
    String str2 = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = com.google.android.gms.common.internal.safeparcel.zza.a(paramParcel);
      switch (com.google.android.gms.common.internal.safeparcel.zza.a(m))
      {
      default: 
        com.google.android.gms.common.internal.safeparcel.zza.b(paramParcel, m);
        break;
      case 1: 
        str2 = com.google.android.gms.common.internal.safeparcel.zza.p(paramParcel, m);
        break;
      case 1000: 
        j = com.google.android.gms.common.internal.safeparcel.zza.g(paramParcel, m);
        break;
      case 2: 
        i = com.google.android.gms.common.internal.safeparcel.zza.g(paramParcel, m);
        break;
      case 3: 
        l = com.google.android.gms.common.internal.safeparcel.zza.i(paramParcel, m);
        break;
      case 4: 
        arrayOfByte = com.google.android.gms.common.internal.safeparcel.zza.s(paramParcel, m);
        break;
      case 5: 
        str1 = com.google.android.gms.common.internal.safeparcel.zza.p(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new ProxyGrpcRequest(j, str2, i, l, arrayOfByte, str1);
  }
  
  public ProxyGrpcRequest[] a(int paramInt)
  {
    return new ProxyGrpcRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/proxy/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */