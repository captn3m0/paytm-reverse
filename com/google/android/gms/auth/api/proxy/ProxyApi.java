package com.google.android.gms.auth.api.proxy;

import com.google.android.gms.common.api.Result;

public abstract interface ProxyApi
{
  public static abstract interface ProxyResult
    extends Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/proxy/ProxyApi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */