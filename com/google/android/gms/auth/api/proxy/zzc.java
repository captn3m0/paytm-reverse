package com.google.android.gms.auth.api.proxy;

import android.app.PendingIntent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzc
  implements Parcelable.Creator<ProxyResponse>
{
  static void a(ProxyResponse paramProxyResponse, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramProxyResponse.b);
    zzb.a(paramParcel, 1000, paramProxyResponse.a);
    zzb.a(paramParcel, 2, paramProxyResponse.c, paramInt, false);
    zzb.a(paramParcel, 3, paramProxyResponse.d);
    zzb.a(paramParcel, 4, paramProxyResponse.e, false);
    zzb.a(paramParcel, 5, paramProxyResponse.f, false);
    zzb.a(paramParcel, i);
  }
  
  public ProxyResponse a(Parcel paramParcel)
  {
    byte[] arrayOfByte = null;
    int i = 0;
    int m = zza.b(paramParcel);
    Bundle localBundle = null;
    PendingIntent localPendingIntent = null;
    int j = 0;
    int k = 0;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        j = zza.g(paramParcel, n);
        break;
      case 1000: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        localPendingIntent = (PendingIntent)zza.a(paramParcel, n, PendingIntent.CREATOR);
        break;
      case 3: 
        i = zza.g(paramParcel, n);
        break;
      case 4: 
        localBundle = zza.r(paramParcel, n);
        break;
      case 5: 
        arrayOfByte = zza.s(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new ProxyResponse(k, j, localPendingIntent, i, localBundle, arrayOfByte);
  }
  
  public ProxyResponse[] a(int paramInt)
  {
    return new ProxyResponse[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/proxy/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */