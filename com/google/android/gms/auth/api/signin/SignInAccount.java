package com.google.android.gms.auth.api.signin;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;
import org.json.JSONException;
import org.json.JSONObject;

public class SignInAccount
  implements SafeParcelable
{
  public static final Parcelable.Creator<SignInAccount> CREATOR = new zze();
  final int a;
  private String b;
  private String c;
  private String d;
  private String e;
  private Uri f;
  private GoogleSignInAccount g;
  private String h;
  private String i;
  
  SignInAccount(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4, Uri paramUri, GoogleSignInAccount paramGoogleSignInAccount, String paramString5, String paramString6)
  {
    this.a = paramInt;
    this.d = zzx.a(paramString3, "Email cannot be empty.");
    this.e = paramString4;
    this.f = paramUri;
    this.b = paramString1;
    this.c = paramString2;
    this.g = paramGoogleSignInAccount;
    this.h = zzx.a(paramString5);
    this.i = paramString6;
  }
  
  public static SignInAccount a(zzd paramzzd, String paramString1, String paramString2, String paramString3, Uri paramUri, String paramString4, String paramString5)
  {
    String str = null;
    if (paramzzd != null) {
      str = paramzzd.a();
    }
    return new SignInAccount(2, str, paramString1, paramString2, paramString3, paramUri, null, paramString4, paramString5);
  }
  
  public static SignInAccount a(String paramString)
    throws JSONException
  {
    if (TextUtils.isEmpty(paramString)) {
      return null;
    }
    JSONObject localJSONObject = new JSONObject(paramString);
    paramString = localJSONObject.optString("photoUrl", null);
    if (!TextUtils.isEmpty(paramString)) {}
    for (paramString = Uri.parse(paramString);; paramString = null) {
      return a(zzd.a(localJSONObject.optString("providerId", null)), localJSONObject.optString("tokenId", null), localJSONObject.getString("email"), localJSONObject.optString("displayName", null), paramString, localJSONObject.getString("localId"), localJSONObject.optString("refreshToken")).a(GoogleSignInAccount.a(localJSONObject.optString("googleSignInAccount")));
    }
  }
  
  private JSONObject k()
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("email", b());
      if (!TextUtils.isEmpty(this.e)) {
        localJSONObject.put("displayName", this.e);
      }
      if (this.f != null) {
        localJSONObject.put("photoUrl", this.f.toString());
      }
      if (!TextUtils.isEmpty(this.b)) {
        localJSONObject.put("providerId", this.b);
      }
      if (!TextUtils.isEmpty(this.c)) {
        localJSONObject.put("tokenId", this.c);
      }
      if (this.g != null) {
        localJSONObject.put("googleSignInAccount", this.g.i());
      }
      if (!TextUtils.isEmpty(this.i)) {
        localJSONObject.put("refreshToken", this.i);
      }
      localJSONObject.put("localId", g());
      return localJSONObject;
    }
    catch (JSONException localJSONException)
    {
      throw new RuntimeException(localJSONException);
    }
  }
  
  public SignInAccount a(GoogleSignInAccount paramGoogleSignInAccount)
  {
    this.g = paramGoogleSignInAccount;
    return this;
  }
  
  public String a()
  {
    return this.c;
  }
  
  public String b()
  {
    return this.d;
  }
  
  public String c()
  {
    return this.e;
  }
  
  public Uri d()
  {
    return this.f;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public zzd e()
  {
    return zzd.a(this.b);
  }
  
  public GoogleSignInAccount f()
  {
    return this.g;
  }
  
  public String g()
  {
    return this.h;
  }
  
  public String h()
  {
    return this.i;
  }
  
  public String i()
  {
    return k().toString();
  }
  
  String j()
  {
    return this.b;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zze.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/signin/SignInAccount.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */