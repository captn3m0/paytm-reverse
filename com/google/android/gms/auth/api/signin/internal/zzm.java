package com.google.android.gms.auth.api.signin.internal;

import android.text.TextUtils;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzmq;
import com.google.android.gms.internal.zzmt;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class zzm
{
  private static final WeakHashMap<String, zzm> a = new WeakHashMap();
  private static final Lock b = new ReentrantLock();
  private final Lock c = new ReentrantLock();
  private final Map<String, zza> d;
  
  private zzm(Map<String, zza> paramMap)
  {
    this.d = paramMap;
  }
  
  public static zzm a(String paramString)
  {
    zzx.a(paramString);
    b.lock();
    try
    {
      zzm localzzm2 = (zzm)a.get(paramString);
      zzm localzzm1 = localzzm2;
      if (localzzm2 == null)
      {
        localzzm1 = new zzm(new zzb(20));
        a.put(paramString, localzzm1);
      }
      return localzzm1;
    }
    finally
    {
      b.unlock();
    }
  }
  
  public boolean a(Set<String> paramSet, zza paramzza)
  {
    zzx.a(paramSet);
    zzx.a(paramzza);
    if ((paramSet.size() == 0) || (paramzza.a())) {
      return false;
    }
    paramSet = new ArrayList(paramSet);
    Collections.sort(paramSet);
    this.c.lock();
    try
    {
      paramSet = TextUtils.join(" ", paramSet);
      this.d.put(paramSet, paramzza);
      return true;
    }
    finally
    {
      this.c.unlock();
    }
  }
  
  public static class zza
  {
    private final String a;
    private final long b;
    private final zzmq c;
    
    public zza(String paramString, long paramLong)
    {
      this(paramString, paramLong, zzmt.d());
    }
    
    private zza(String paramString, long paramLong, zzmq paramzzmq)
    {
      this.a = zzx.a(paramString);
      if (paramLong > 0L) {}
      for (boolean bool = true;; bool = false)
      {
        zzx.b(bool);
        this.b = paramLong;
        this.c = ((zzmq)zzx.a(paramzzmq));
        return;
      }
    }
    
    public boolean a()
    {
      return this.c.a() / 1000L >= this.b - 300L;
    }
  }
  
  static class zzb<K, V>
    extends LinkedHashMap<K, V>
  {
    private final int a;
    
    public zzb(int paramInt)
    {
      this.a = paramInt;
    }
    
    protected boolean removeEldestEntry(Map.Entry<K, V> paramEntry)
    {
      return size() > this.a;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/signin/internal/zzm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */