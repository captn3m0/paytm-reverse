package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.SignInAccount;
import com.google.android.gms.common.internal.zzx;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.json.JSONException;

public class zzq
{
  private static final Lock a = new ReentrantLock();
  private static zzq b;
  private final Lock c = new ReentrantLock();
  private final SharedPreferences d;
  
  zzq(Context paramContext)
  {
    this.d = paramContext.getSharedPreferences("com.google.android.gms.signin", 0);
  }
  
  public static zzq a(Context paramContext)
  {
    zzx.a(paramContext);
    a.lock();
    try
    {
      if (b == null) {
        b = new zzq(paramContext.getApplicationContext());
      }
      paramContext = b;
      return paramContext;
    }
    finally
    {
      a.unlock();
    }
  }
  
  private String b(String paramString1, String paramString2)
  {
    return paramString1 + ":" + paramString2;
  }
  
  public GoogleSignInAccount a()
  {
    return b(d("defaultGoogleSignInAccount"));
  }
  
  SignInAccount a(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {}
    do
    {
      return null;
      paramString = d(b("signInAccount", paramString));
    } while (TextUtils.isEmpty(paramString));
    try
    {
      paramString = SignInAccount.a(paramString);
      if (paramString.f() != null)
      {
        GoogleSignInAccount localGoogleSignInAccount = b(paramString.f().h());
        if (localGoogleSignInAccount != null) {
          paramString.a(localGoogleSignInAccount);
        }
      }
      return paramString;
    }
    catch (JSONException paramString) {}
    return null;
  }
  
  void a(GoogleSignInAccount paramGoogleSignInAccount, GoogleSignInOptions paramGoogleSignInOptions)
  {
    zzx.a(paramGoogleSignInAccount);
    zzx.a(paramGoogleSignInOptions);
    String str = paramGoogleSignInAccount.h();
    a(b("googleSignInAccount", str), paramGoogleSignInAccount.j());
    a(b("googleSignInOptions", str), paramGoogleSignInOptions.h());
  }
  
  void a(SignInAccount paramSignInAccount, SignInConfiguration paramSignInConfiguration)
  {
    zzx.a(paramSignInAccount);
    zzx.a(paramSignInConfiguration);
    String str = paramSignInAccount.g();
    SignInAccount localSignInAccount = a(str);
    if ((localSignInAccount != null) && (localSignInAccount.f() != null)) {
      f(localSignInAccount.f().h());
    }
    a(b("signInConfiguration", str), paramSignInConfiguration.f());
    a(b("signInAccount", str), paramSignInAccount.i());
    if (paramSignInAccount.f() != null) {
      a(paramSignInAccount.f(), paramSignInConfiguration.d());
    }
  }
  
  protected void a(String paramString1, String paramString2)
  {
    this.c.lock();
    try
    {
      this.d.edit().putString(paramString1, paramString2).apply();
      return;
    }
    finally
    {
      this.c.unlock();
    }
  }
  
  GoogleSignInAccount b(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {}
    do
    {
      return null;
      paramString = d(b("googleSignInAccount", paramString));
    } while (paramString == null);
    try
    {
      paramString = GoogleSignInAccount.a(paramString);
      return paramString;
    }
    catch (JSONException paramString) {}
    return null;
  }
  
  public GoogleSignInOptions b()
  {
    return c(d("defaultGoogleSignInAccount"));
  }
  
  public void b(GoogleSignInAccount paramGoogleSignInAccount, GoogleSignInOptions paramGoogleSignInOptions)
  {
    zzx.a(paramGoogleSignInAccount);
    zzx.a(paramGoogleSignInOptions);
    a("defaultGoogleSignInAccount", paramGoogleSignInAccount.h());
    a(paramGoogleSignInAccount, paramGoogleSignInOptions);
  }
  
  public void b(SignInAccount paramSignInAccount, SignInConfiguration paramSignInConfiguration)
  {
    zzx.a(paramSignInAccount);
    zzx.a(paramSignInConfiguration);
    c();
    a("defaultSignInAccount", paramSignInAccount.g());
    if (paramSignInAccount.f() != null) {
      a("defaultGoogleSignInAccount", paramSignInAccount.f().h());
    }
    a(paramSignInAccount, paramSignInConfiguration);
  }
  
  GoogleSignInOptions c(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {}
    do
    {
      return null;
      paramString = d(b("googleSignInOptions", paramString));
    } while (paramString == null);
    try
    {
      paramString = GoogleSignInOptions.a(paramString);
      return paramString;
    }
    catch (JSONException paramString) {}
    return null;
  }
  
  public void c()
  {
    String str = d("defaultSignInAccount");
    g("defaultSignInAccount");
    d();
    e(str);
  }
  
  protected String d(String paramString)
  {
    this.c.lock();
    try
    {
      paramString = this.d.getString(paramString, null);
      return paramString;
    }
    finally
    {
      this.c.unlock();
    }
  }
  
  public void d()
  {
    String str = d("defaultGoogleSignInAccount");
    g("defaultGoogleSignInAccount");
    f(str);
  }
  
  void e(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {}
    SignInAccount localSignInAccount;
    do
    {
      return;
      localSignInAccount = a(paramString);
      g(b("signInAccount", paramString));
      g(b("signInConfiguration", paramString));
    } while ((localSignInAccount == null) || (localSignInAccount.f() == null));
    f(localSignInAccount.f().h());
  }
  
  void f(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return;
    }
    g(b("googleSignInAccount", paramString));
    g(b("googleSignInOptions", paramString));
  }
  
  protected void g(String paramString)
  {
    this.c.lock();
    try
    {
      this.d.edit().remove(paramString).apply();
      return;
    }
    finally
    {
      this.c.unlock();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/signin/internal/zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */