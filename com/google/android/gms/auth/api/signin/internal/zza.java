package com.google.android.gms.auth.api.signin.internal;

import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.SignInAccount;
import com.google.android.gms.common.api.Status;

public class zza
  extends zzg.zza
{
  public void a(GoogleSignInAccount paramGoogleSignInAccount, Status paramStatus)
    throws RemoteException
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(Status paramStatus)
    throws RemoteException
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(Status paramStatus, SignInAccount paramSignInAccount)
    throws RemoteException
  {
    throw new UnsupportedOperationException();
  }
  
  public void a(Status paramStatus, String paramString1, String paramString2, long paramLong)
    throws RemoteException
  {
    throw new UnsupportedOperationException();
  }
  
  public void b(Status paramStatus)
    throws RemoteException
  {
    throw new UnsupportedOperationException();
  }
  
  public void c(Status paramStatus)
    throws RemoteException
  {
    throw new UnsupportedOperationException();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/signin/internal/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */