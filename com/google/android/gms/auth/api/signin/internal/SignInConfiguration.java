package com.google.android.gms.auth.api.signin.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.gms.auth.api.signin.EmailSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;
import org.json.JSONException;
import org.json.JSONObject;

public final class SignInConfiguration
  implements SafeParcelable
{
  public static final Parcelable.Creator<SignInConfiguration> CREATOR = new zzp();
  final int a;
  private final String b;
  private String c;
  private EmailSignInOptions d;
  private GoogleSignInOptions e;
  private String f;
  
  SignInConfiguration(int paramInt, String paramString1, String paramString2, EmailSignInOptions paramEmailSignInOptions, GoogleSignInOptions paramGoogleSignInOptions, String paramString3)
  {
    this.a = paramInt;
    this.b = zzx.a(paramString1);
    this.c = paramString2;
    this.d = paramEmailSignInOptions;
    this.e = paramGoogleSignInOptions;
    this.f = paramString3;
  }
  
  public SignInConfiguration(String paramString)
  {
    this(2, paramString, null, null, null, null);
  }
  
  private JSONObject g()
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("consumerPackageName", this.b);
      if (!TextUtils.isEmpty(this.c)) {
        localJSONObject.put("serverClientId", this.c);
      }
      if (this.d != null) {
        localJSONObject.put("emailSignInOptions", this.d.d());
      }
      if (this.e != null) {
        localJSONObject.put("googleSignInOptions", this.e.h());
      }
      if (!TextUtils.isEmpty(this.f)) {
        localJSONObject.put("apiKey", this.f);
      }
      return localJSONObject;
    }
    catch (JSONException localJSONException)
    {
      throw new RuntimeException(localJSONException);
    }
  }
  
  public SignInConfiguration a(GoogleSignInOptions paramGoogleSignInOptions)
  {
    this.e = ((GoogleSignInOptions)zzx.a(paramGoogleSignInOptions, "GoogleSignInOptions cannot be null."));
    return this;
  }
  
  public String a()
  {
    return this.b;
  }
  
  public String b()
  {
    return this.c;
  }
  
  public EmailSignInOptions c()
  {
    return this.d;
  }
  
  public GoogleSignInOptions d()
  {
    return this.e;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String e()
  {
    return this.f;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {}
    for (;;)
    {
      return false;
      try
      {
        paramObject = (SignInConfiguration)paramObject;
        if (this.b.equals(((SignInConfiguration)paramObject).a()))
        {
          if (TextUtils.isEmpty(this.c))
          {
            if (!TextUtils.isEmpty(((SignInConfiguration)paramObject).b())) {
              continue;
            }
            label45:
            if (!TextUtils.isEmpty(this.f)) {
              break label113;
            }
            if (!TextUtils.isEmpty(((SignInConfiguration)paramObject).e())) {
              continue;
            }
            label65:
            if (this.d != null) {
              break label130;
            }
            if (((SignInConfiguration)paramObject).c() != null) {
              continue;
            }
          }
          for (;;)
          {
            if (this.e != null) {
              break label147;
            }
            if (((SignInConfiguration)paramObject).d() != null) {
              break;
            }
            break label169;
            if (!this.c.equals(((SignInConfiguration)paramObject).b())) {
              break;
            }
            break label45;
            label113:
            if (!this.f.equals(((SignInConfiguration)paramObject).e())) {
              break;
            }
            break label65;
            label130:
            if (!this.d.equals(((SignInConfiguration)paramObject).c())) {
              break;
            }
          }
          label147:
          boolean bool = this.e.equals(((SignInConfiguration)paramObject).d());
          if (!bool) {}
        }
      }
      catch (ClassCastException paramObject)
      {
        return false;
      }
    }
    label169:
    return true;
  }
  
  public String f()
  {
    return g().toString();
  }
  
  public int hashCode()
  {
    return new zze().a(this.b).a(this.c).a(this.f).a(this.d).a(this.e).a();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzp.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/signin/internal/SignInConfiguration.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */