package com.google.android.gms.auth.api.signin.internal;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.SignInAccount;
import com.google.android.gms.auth.api.signin.zzd;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzlf;
import com.google.android.gms.internal.zzlf.zza;
import com.google.android.gms.internal.zzlh;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

@KeepName
public class SignInHubActivity
  extends FragmentActivity
{
  private zzq a;
  private zzk b;
  private SignInConfiguration c;
  private boolean d;
  private String e;
  private String f;
  private boolean g;
  private int h;
  private Intent i;
  
  private zzlf.zza a(final String paramString)
  {
    new zzlf.zza()
    {
      public void a(Intent paramAnonymousIntent)
      {
        if (paramAnonymousIntent != null)
        {
          if (!TextUtils.isEmpty(paramString)) {
            paramAnonymousIntent.putExtra("scopes", paramString);
          }
          SignInHubActivity.a(SignInHubActivity.this, paramAnonymousIntent);
          return;
        }
        Log.w("AuthSignInClient", "Idp signin failed!");
        SignInHubActivity.a(SignInHubActivity.this, 4);
      }
    };
  }
  
  private void a(int paramInt)
  {
    Intent localIntent = new Intent();
    localIntent.putExtra("errorCode", paramInt);
    setResult(0, localIntent);
    finish();
  }
  
  private void a(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Iterator localIterator = this.b.a().iterator();
    while ((localIterator.hasNext()) && (!((zzlf)localIterator.next()).a(paramInt1, paramInt2, paramIntent, a(this.f)))) {}
    if (paramInt2 == 0) {
      finish();
    }
  }
  
  private void a(int paramInt, Intent paramIntent)
  {
    if (paramIntent != null)
    {
      Object localObject = (SignInAccount)paramIntent.getParcelableExtra("signInAccount");
      if ((localObject != null) && (((SignInAccount)localObject).f() != null))
      {
        localObject = ((SignInAccount)localObject).f();
        this.a.b((GoogleSignInAccount)localObject, this.c.d());
        paramIntent.removeExtra("signInAccount");
        paramIntent.putExtra("googleSignInAccount", (Parcelable)localObject);
        this.g = true;
        this.h = paramInt;
        this.i = paramIntent;
        d(paramInt, paramIntent);
        return;
      }
      if (paramIntent.hasExtra("errorCode"))
      {
        b(paramIntent.getIntExtra("errorCode", 8));
        return;
      }
    }
    b(8);
  }
  
  private void a(Intent paramIntent)
  {
    paramIntent.setPackage("com.google.android.gms");
    paramIntent.putExtra("config", this.c);
    if (this.d) {}
    for (int j = 40962;; j = 40961) {
      try
      {
        startActivityForResult(paramIntent, j);
        return;
      }
      catch (ActivityNotFoundException paramIntent)
      {
        Log.w("AuthSignInClient", "Could not launch sign in Intent. Google Play Service is probably being updated...");
        if (!this.d) {
          break;
        }
        b(8);
        return;
        a(2);
      }
    }
  }
  
  private void b(int paramInt)
  {
    Status localStatus = new Status(paramInt);
    Intent localIntent = new Intent();
    localIntent.putExtra("googleSignInStatus", localStatus);
    setResult(0, localIntent);
    finish();
  }
  
  private void b(int paramInt, Intent paramIntent)
  {
    if (paramInt == -1)
    {
      localObject1 = (SignInAccount)paramIntent.getParcelableExtra("signInAccount");
      if (localObject1 != null)
      {
        this.a.b((SignInAccount)localObject1, this.c);
        localObject2 = paramIntent.getStringExtra("accessToken");
        if ((!TextUtils.isEmpty((CharSequence)localObject2)) && (!TextUtils.isEmpty(this.f)))
        {
          localObject3 = new HashSet(Arrays.asList(TextUtils.split(this.f, " ")));
          zzm.a(((SignInAccount)localObject1).g()).a((Set)localObject3, new zzm.zza((String)localObject2, paramIntent.getLongExtra("accessTokenExpiresAtSecs", 0L)));
          paramIntent.removeExtra("accessTokenExpiresAtSecs");
        }
        setResult(-1, paramIntent);
        finish();
        return;
      }
      Log.w("AuthSignInClient", "[SignInHubActivity] SignInAccount is null.");
      a(2);
      return;
    }
    if (paramIntent == null)
    {
      finish();
      return;
    }
    Object localObject1 = paramIntent.getStringExtra("email");
    Object localObject2 = zzd.a(paramIntent.getStringExtra("idProvider"));
    if (localObject2 == null)
    {
      setResult(paramInt, paramIntent);
      finish();
      return;
    }
    this.e = paramIntent.getStringExtra("pendingToken");
    Object localObject3 = this.b.a((zzd)localObject2);
    if (localObject3 == null)
    {
      paramIntent = ((zzd)localObject2).a(this);
      Log.w("AuthSignInClient", paramIntent + " is not supported. Please check your configuration");
      a(1);
      return;
    }
    paramInt = paramIntent.getIntExtra("idpAction", -1);
    if (paramInt == 0)
    {
      if (TextUtils.isEmpty((CharSequence)localObject1))
      {
        ((zzlf)localObject3).a(a(this.f));
        return;
      }
      ((zzlf)localObject3).a((String)localObject1, a(this.f));
      return;
    }
    if ((paramInt == 1) && (!TextUtils.isEmpty(this.e)) && (!TextUtils.isEmpty((CharSequence)localObject1)))
    {
      ((zzlf)localObject3).a((String)localObject1, this.e, a(this.f));
      return;
    }
    Log.w("AuthSignInClient", "Internal error!");
    a(2);
  }
  
  private void c(int paramInt, Intent paramIntent)
  {
    if (paramInt == 0)
    {
      setResult(0, paramIntent);
      finish();
      return;
    }
    Intent localIntent = new Intent("com.google.android.gms.auth.VERIFY_ASSERTION");
    localIntent.putExtra("idpTokenType", IdpTokenType.a);
    localIntent.putExtra("idpToken", paramIntent.getStringExtra("idpToken"));
    localIntent.putExtra("pendingToken", this.e);
    localIntent.putExtra("idProvider", zzd.b.a());
    a(localIntent);
  }
  
  private void d(int paramInt, Intent paramIntent)
  {
    getSupportLoaderManager().initLoader(0, null, new zza(null));
  }
  
  public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
  {
    return true;
  }
  
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    setResult(0);
    switch (paramInt1)
    {
    default: 
      a(paramInt1, paramInt2, paramIntent);
      return;
    case 40962: 
      a(paramInt2, paramIntent);
      return;
    case 40961: 
      b(paramInt2, paramIntent);
      return;
    }
    c(paramInt2, paramIntent);
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.a = zzq.a(this);
    Intent localIntent = getIntent();
    this.c = ((SignInConfiguration)localIntent.getParcelableExtra("config"));
    this.d = "com.google.android.gms.auth.GOOGLE_SIGN_IN".equals(localIntent.getAction());
    this.f = localIntent.getStringExtra("scopes");
    if (this.c == null)
    {
      Log.e("AuthSignInClient", "Activity started with invalid configuration.");
      setResult(0);
      finish();
      return;
    }
    LinkedList localLinkedList = new LinkedList();
    Object localObject = new HashMap();
    zzi.a(this.c, localLinkedList, (Map)localObject);
    this.b = new zzk(this, localLinkedList, (Map)localObject);
    if (paramBundle == null) {
      if (this.d)
      {
        localObject = new Intent("com.google.android.gms.auth.GOOGLE_SIGN_IN");
        paramBundle = null;
      }
    }
    for (;;)
    {
      if ((paramBundle != null) && (paramBundle.e() == zzd.b))
      {
        a(this.f);
        throw new NullPointerException();
        localObject = new Intent("com.google.android.gms.auth.LOGIN_PICKER");
        if ("com.google.android.gms.auth.RESOLVE_CREDENTIAL".equals(localIntent.getAction()))
        {
          ((Intent)localObject).fillIn(localIntent, 3);
          paramBundle = (SignInAccount)localIntent.getParcelableExtra("signInAccount");
          continue;
        }
        this.a.c();
        if (0 != 0) {
          try
          {
            zzlh.a(this);
            paramBundle = null;
          }
          catch (IllegalStateException paramBundle)
          {
            paramBundle = null;
          }
        }
      }
      else
      {
        a((Intent)localObject);
        return;
        this.e = paramBundle.getString("pendingToken");
        this.g = paramBundle.getBoolean("signingInGoogleApiClients");
        if (!this.g) {
          break;
        }
        this.h = paramBundle.getInt("signInResultCode");
        this.i = ((Intent)paramBundle.getParcelable("signInResultData"));
        d(this.h, this.i);
        return;
      }
      paramBundle = null;
    }
  }
  
  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putString("pendingToken", this.e);
    paramBundle.putBoolean("signingInGoogleApiClients", this.g);
    if (this.g)
    {
      paramBundle.putInt("signInResultCode", this.h);
      paramBundle.putParcelable("signInResultData", this.i);
    }
  }
  
  private class zza
    implements LoaderManager.LoaderCallbacks<Void>
  {
    private zza() {}
    
    public void a(Loader<Void> paramLoader, Void paramVoid)
    {
      SignInHubActivity.this.setResult(SignInHubActivity.a(SignInHubActivity.this), SignInHubActivity.b(SignInHubActivity.this));
      SignInHubActivity.this.finish();
    }
    
    public Loader<Void> onCreateLoader(int paramInt, Bundle paramBundle)
    {
      return new zzb(SignInHubActivity.this, GoogleApiClient.a());
    }
    
    public void onLoaderReset(Loader<Void> paramLoader) {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/signin/internal/SignInHubActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */