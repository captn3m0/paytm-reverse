package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.zzd;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.zzx;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public final class zzi
{
  public static void a(SignInConfiguration paramSignInConfiguration, List<zzd> paramList, Map<zzd, List<String>> paramMap)
  {
    zzx.a(paramSignInConfiguration);
    zzx.a(paramList);
    zzx.a(paramMap);
    paramSignInConfiguration = paramSignInConfiguration.d();
    if (paramSignInConfiguration != null)
    {
      paramList.add(zzd.a);
      paramList = new LinkedList();
      paramSignInConfiguration = paramSignInConfiguration.a().iterator();
      while (paramSignInConfiguration.hasNext()) {
        paramList.add(((Scope)paramSignInConfiguration.next()).a());
      }
      paramMap.put(zzd.a, paramList);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/signin/internal/zzi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */