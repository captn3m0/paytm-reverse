package com.google.android.gms.auth.api.signin.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;

public final class IdpTokenType
  implements SafeParcelable
{
  public static final Parcelable.Creator<IdpTokenType> CREATOR = new zzj();
  public static final IdpTokenType a = new IdpTokenType("accessToken");
  public static final IdpTokenType b = new IdpTokenType("idToken");
  final int c;
  private final String d;
  
  IdpTokenType(int paramInt, String paramString)
  {
    this.c = paramInt;
    this.d = zzx.a(paramString);
  }
  
  private IdpTokenType(String paramString)
  {
    this(1, paramString);
  }
  
  public String a()
  {
    return this.d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (paramObject != null) {}
    try
    {
      boolean bool3 = this.d.equals(((IdpTokenType)paramObject).a());
      bool1 = bool2;
      if (bool3) {
        bool1 = true;
      }
      return bool1;
    }
    catch (ClassCastException paramObject) {}
    return false;
  }
  
  public int hashCode()
  {
    return this.d.hashCode();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzj.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/signin/internal/IdpTokenType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */