package com.google.android.gms.auth.api.signin.internal;

import android.app.Activity;
import com.google.android.gms.auth.api.signin.zzd;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzlf;
import com.google.android.gms.internal.zzlh;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class zzk
{
  private final Map<zzd, zzlf> a;
  
  public zzk(Activity paramActivity, List<zzd> paramList, Map<zzd, List<String>> paramMap)
  {
    zzx.a(paramActivity);
    zzx.a(paramList);
    zzx.a(paramMap);
    HashMap localHashMap = new HashMap();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      zzd localzzd = (zzd)localIterator.next();
      List localList = (List)paramMap.get(localzzd);
      paramList = localList;
      if (localList == null) {
        paramList = Collections.emptyList();
      }
      paramList = a(localzzd, paramActivity, paramList);
      if (paramList != null) {
        localHashMap.put(localzzd, paramList);
      }
    }
    this.a = Collections.unmodifiableMap(localHashMap);
  }
  
  private zzlf a(zzd paramzzd, Activity paramActivity, List<String> paramList)
  {
    if (zzd.b.equals(paramzzd)) {
      return new zzlh(paramActivity, paramList);
    }
    return null;
  }
  
  public zzlf a(zzd paramzzd)
  {
    zzx.a(paramzzd);
    return (zzlf)this.a.get(paramzzd);
  }
  
  public Collection<zzlf> a()
  {
    return this.a.values();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/signin/internal/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */