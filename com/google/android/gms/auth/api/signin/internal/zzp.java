package com.google.android.gms.auth.api.signin.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.auth.api.signin.EmailSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzp
  implements Parcelable.Creator<SignInConfiguration>
{
  static void a(SignInConfiguration paramSignInConfiguration, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramSignInConfiguration.a);
    zzb.a(paramParcel, 2, paramSignInConfiguration.a(), false);
    zzb.a(paramParcel, 3, paramSignInConfiguration.b(), false);
    zzb.a(paramParcel, 4, paramSignInConfiguration.c(), paramInt, false);
    zzb.a(paramParcel, 5, paramSignInConfiguration.d(), paramInt, false);
    zzb.a(paramParcel, 7, paramSignInConfiguration.e(), false);
    zzb.a(paramParcel, i);
  }
  
  public SignInConfiguration a(Parcel paramParcel)
  {
    String str1 = null;
    int j = zza.b(paramParcel);
    int i = 0;
    GoogleSignInOptions localGoogleSignInOptions = null;
    EmailSignInOptions localEmailSignInOptions = null;
    String str2 = null;
    String str3 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      case 6: 
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str3 = zza.p(paramParcel, k);
        break;
      case 3: 
        str2 = zza.p(paramParcel, k);
        break;
      case 4: 
        localEmailSignInOptions = (EmailSignInOptions)zza.a(paramParcel, k, EmailSignInOptions.CREATOR);
        break;
      case 5: 
        localGoogleSignInOptions = (GoogleSignInOptions)zza.a(paramParcel, k, GoogleSignInOptions.CREATOR);
        break;
      case 7: 
        str1 = zza.p(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new SignInConfiguration(i, str3, str2, localEmailSignInOptions, localGoogleSignInOptions, str1);
  }
  
  public SignInConfiguration[] a(int paramInt)
  {
    return new SignInConfiguration[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/signin/internal/zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */