package com.google.android.gms.auth.api.signin;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.auth.api.signin.internal.zze;
import com.google.android.gms.common.api.Api.ApiOptions.Optional;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GoogleSignInOptions
  implements Api.ApiOptions.Optional, SafeParcelable
{
  public static final Parcelable.Creator<GoogleSignInOptions> CREATOR = new zzc();
  public static final Scope a = new Scope("profile");
  public static final Scope b = new Scope("email");
  public static final Scope c = new Scope("openid");
  public static final GoogleSignInOptions d = new Builder().a().b().c();
  private static Comparator<Scope> m = new Comparator()
  {
    public int a(Scope paramAnonymousScope1, Scope paramAnonymousScope2)
    {
      return paramAnonymousScope1.a().compareTo(paramAnonymousScope2.a());
    }
  };
  final int e;
  private final ArrayList<Scope> f;
  private Account g;
  private boolean h;
  private final boolean i;
  private final boolean j;
  private String k;
  private String l;
  
  GoogleSignInOptions(int paramInt, ArrayList<Scope> paramArrayList, Account paramAccount, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, String paramString1, String paramString2)
  {
    this.e = paramInt;
    this.f = paramArrayList;
    this.g = paramAccount;
    this.h = paramBoolean1;
    this.i = paramBoolean2;
    this.j = paramBoolean3;
    this.k = paramString1;
    this.l = paramString2;
  }
  
  private GoogleSignInOptions(Set<Scope> paramSet, Account paramAccount, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, String paramString1, String paramString2)
  {
    this(2, new ArrayList(paramSet), paramAccount, paramBoolean1, paramBoolean2, paramBoolean3, paramString1, paramString2);
  }
  
  @Nullable
  public static GoogleSignInOptions a(@Nullable String paramString)
    throws JSONException
  {
    if (TextUtils.isEmpty(paramString)) {
      return null;
    }
    JSONObject localJSONObject = new JSONObject(paramString);
    HashSet localHashSet = new HashSet();
    paramString = localJSONObject.getJSONArray("scopes");
    int i1 = paramString.length();
    int n = 0;
    while (n < i1)
    {
      localHashSet.add(new Scope(paramString.getString(n)));
      n += 1;
    }
    paramString = localJSONObject.optString("accountName", null);
    if (!TextUtils.isEmpty(paramString)) {}
    for (paramString = new Account(paramString, "com.google");; paramString = null) {
      return new GoogleSignInOptions(localHashSet, paramString, localJSONObject.getBoolean("idTokenRequested"), localJSONObject.getBoolean("serverAuthRequested"), localJSONObject.getBoolean("forceCodeForRefreshToken"), localJSONObject.optString("serverClientId", null), localJSONObject.optString("hostedDomain", null));
    }
  }
  
  private JSONObject i()
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      JSONArray localJSONArray = new JSONArray();
      Collections.sort(this.f, m);
      Iterator localIterator = this.f.iterator();
      while (localIterator.hasNext()) {
        localJSONArray.put(((Scope)localIterator.next()).a());
      }
      localJSONException.put("scopes", localJSONArray);
    }
    catch (JSONException localJSONException)
    {
      throw new RuntimeException(localJSONException);
    }
    if (this.g != null) {
      localJSONException.put("accountName", this.g.name);
    }
    localJSONException.put("idTokenRequested", this.h);
    localJSONException.put("forceCodeForRefreshToken", this.j);
    localJSONException.put("serverAuthRequested", this.i);
    if (!TextUtils.isEmpty(this.k)) {
      localJSONException.put("serverClientId", this.k);
    }
    if (!TextUtils.isEmpty(this.l)) {
      localJSONException.put("hostedDomain", this.l);
    }
    return localJSONException;
  }
  
  public ArrayList<Scope> a()
  {
    return new ArrayList(this.f);
  }
  
  public Account b()
  {
    return this.g;
  }
  
  public boolean c()
  {
    return this.h;
  }
  
  public boolean d()
  {
    return this.i;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean e()
  {
    return this.j;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {}
    for (;;)
    {
      return false;
      try
      {
        paramObject = (GoogleSignInOptions)paramObject;
        if ((this.f.size() != ((GoogleSignInOptions)paramObject).a().size()) || (!this.f.containsAll(((GoogleSignInOptions)paramObject).a()))) {
          continue;
        }
        if (this.g == null)
        {
          if (((GoogleSignInOptions)paramObject).b() != null) {
            continue;
          }
          label56:
          if (!TextUtils.isEmpty(this.k)) {
            break label128;
          }
          if (!TextUtils.isEmpty(((GoogleSignInOptions)paramObject).f())) {
            continue;
          }
        }
        while ((this.j == ((GoogleSignInOptions)paramObject).e()) && (this.h == ((GoogleSignInOptions)paramObject).c()) && (this.i == ((GoogleSignInOptions)paramObject).d()))
        {
          return true;
          if (!this.g.equals(((GoogleSignInOptions)paramObject).b())) {
            break;
          }
          break label56;
          label128:
          boolean bool = this.k.equals(((GoogleSignInOptions)paramObject).f());
          if (!bool) {
            break;
          }
        }
        return false;
      }
      catch (ClassCastException paramObject) {}
    }
  }
  
  public String f()
  {
    return this.k;
  }
  
  public String g()
  {
    return this.l;
  }
  
  public String h()
  {
    return i().toString();
  }
  
  public int hashCode()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.f.iterator();
    while (localIterator.hasNext()) {
      localArrayList.add(((Scope)localIterator.next()).a());
    }
    Collections.sort(localArrayList);
    return new zze().a(localArrayList).a(this.g).a(this.k).a(this.j).a(this.h).a(this.i).a();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.a(this, paramParcel, paramInt);
  }
  
  public static final class Builder
  {
    private Set<Scope> a = new HashSet();
    private boolean b;
    private boolean c;
    private boolean d;
    private String e;
    private Account f;
    private String g;
    
    public Builder() {}
    
    public Builder(@NonNull GoogleSignInOptions paramGoogleSignInOptions)
    {
      zzx.a(paramGoogleSignInOptions);
      this.a = new HashSet(GoogleSignInOptions.a(paramGoogleSignInOptions));
      this.b = GoogleSignInOptions.b(paramGoogleSignInOptions);
      this.c = GoogleSignInOptions.c(paramGoogleSignInOptions);
      this.d = GoogleSignInOptions.d(paramGoogleSignInOptions);
      this.e = GoogleSignInOptions.e(paramGoogleSignInOptions);
      this.f = GoogleSignInOptions.f(paramGoogleSignInOptions);
      this.g = GoogleSignInOptions.g(paramGoogleSignInOptions);
    }
    
    public Builder a()
    {
      this.a.add(GoogleSignInOptions.c);
      return this;
    }
    
    public Builder a(Scope paramScope, Scope... paramVarArgs)
    {
      this.a.add(paramScope);
      this.a.addAll(Arrays.asList(paramVarArgs));
      return this;
    }
    
    public Builder b()
    {
      this.a.add(GoogleSignInOptions.a);
      return this;
    }
    
    public GoogleSignInOptions c()
    {
      if ((this.d) && ((this.f == null) || (!this.a.isEmpty()))) {
        a();
      }
      return new GoogleSignInOptions(this.a, this.f, this.d, this.b, this.c, this.e, this.g, null);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/signin/GoogleSignInOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */