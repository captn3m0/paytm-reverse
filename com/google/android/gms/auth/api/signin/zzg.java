package com.google.android.gms.auth.api.signin;

import com.google.android.gms.auth.api.signin.internal.SignInConfiguration;
import com.google.android.gms.common.api.Api.ApiOptions.HasOptions;
import com.google.android.gms.common.internal.zzx;

public class zzg
  implements Api.ApiOptions.HasOptions
{
  private final SignInConfiguration a;
  
  private zzg(SignInConfiguration paramSignInConfiguration)
  {
    this.a = paramSignInConfiguration;
  }
  
  public SignInConfiguration a()
  {
    return this.a;
  }
  
  public static class zza
  {
    private final SignInConfiguration a;
    
    public zza(String paramString)
    {
      zzx.a(paramString);
      this.a = new SignInConfiguration(paramString);
    }
    
    public zza a(GoogleSignInOptions paramGoogleSignInOptions)
    {
      zzx.a(paramGoogleSignInOptions);
      this.a.a(paramGoogleSignInOptions);
      return this;
    }
    
    public zzg a()
    {
      if ((this.a.c() != null) || (this.a.d() != null)) {}
      for (boolean bool = true;; bool = false)
      {
        zzx.a(bool, "Must support either Facebook, Google or Email sign-in.");
        return new zzg(this.a, null);
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/signin/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */