package com.google.android.gms.auth.api.signin;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import com.google.android.gms.R.string;

public enum zzd
{
  private final String c;
  private final int d;
  private final String e;
  
  private zzd(String paramString1, int paramInt, String paramString2)
  {
    this.c = paramString1;
    this.d = paramInt;
    this.e = paramString2;
  }
  
  public static zzd a(String paramString)
  {
    if (paramString != null)
    {
      zzd[] arrayOfzzd = values();
      int j = arrayOfzzd.length;
      int i = 0;
      while (i < j)
      {
        zzd localzzd = arrayOfzzd[i];
        if (localzzd.a().equals(paramString)) {
          return localzzd;
        }
        i += 1;
      }
      Log.w("IdProvider", "Unrecognized providerId: " + paramString);
    }
    return null;
  }
  
  public CharSequence a(Context paramContext)
  {
    return paramContext.getResources().getString(this.d);
  }
  
  public String a()
  {
    return this.c;
  }
  
  public String toString()
  {
    return this.c;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/signin/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */