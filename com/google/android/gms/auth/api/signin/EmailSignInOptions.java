package com.google.android.gms.auth.api.signin;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Patterns;
import com.google.android.gms.auth.api.signin.internal.zze;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzx;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;

public class EmailSignInOptions
  implements SafeParcelable
{
  public static final Parcelable.Creator<EmailSignInOptions> CREATOR = new zza();
  final int a;
  private final Uri b;
  private String c;
  private Uri d;
  
  EmailSignInOptions(int paramInt, Uri paramUri1, String paramString, Uri paramUri2)
  {
    zzx.a(paramUri1, "Server widget url cannot be null in order to use email/password sign in.");
    zzx.a(paramUri1.toString(), "Server widget url cannot be null in order to use email/password sign in.");
    zzx.b(Patterns.WEB_URL.matcher(paramUri1.toString()).matches(), "Invalid server widget url");
    this.a = paramInt;
    this.b = paramUri1;
    this.c = paramString;
    this.d = paramUri2;
  }
  
  private JSONObject e()
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("serverWidgetUrl", this.b.toString());
      if (!TextUtils.isEmpty(this.c)) {
        localJSONObject.put("modeQueryName", this.c);
      }
      if (this.d != null) {
        localJSONObject.put("tosUrl", this.d.toString());
      }
      return localJSONObject;
    }
    catch (JSONException localJSONException)
    {
      throw new RuntimeException(localJSONException);
    }
  }
  
  public Uri a()
  {
    return this.b;
  }
  
  public Uri b()
  {
    return this.d;
  }
  
  public String c()
  {
    return this.c;
  }
  
  public String d()
  {
    return e().toString();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {}
    for (;;)
    {
      return false;
      try
      {
        paramObject = (EmailSignInOptions)paramObject;
        if (this.b.equals(((EmailSignInOptions)paramObject).a())) {
          if (this.d == null)
          {
            if (((EmailSignInOptions)paramObject).b() != null) {}
          }
          else
          {
            for (;;)
            {
              if (!TextUtils.isEmpty(this.c)) {
                break label79;
              }
              if (!TextUtils.isEmpty(((EmailSignInOptions)paramObject).c())) {
                break;
              }
              break label101;
              if (!this.d.equals(((EmailSignInOptions)paramObject).b())) {
                break;
              }
            }
            label79:
            boolean bool = this.c.equals(((EmailSignInOptions)paramObject).c());
            if (!bool) {}
          }
        }
      }
      catch (ClassCastException paramObject)
      {
        return false;
      }
    }
    label101:
    return true;
  }
  
  public int hashCode()
  {
    return new zze().a(this.b).a(this.d).a(this.c).a();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/api/signin/EmailSignInOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */