package com.google.android.gms.auth;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zze
  implements Parcelable.Creator<TokenData>
{
  static void a(TokenData paramTokenData, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramTokenData.a);
    zzb.a(paramParcel, 2, paramTokenData.a(), false);
    zzb.a(paramParcel, 3, paramTokenData.b(), false);
    zzb.a(paramParcel, 4, paramTokenData.c());
    zzb.a(paramParcel, 5, paramTokenData.d());
    zzb.b(paramParcel, 6, paramTokenData.e(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public TokenData a(Parcel paramParcel)
  {
    ArrayList localArrayList = null;
    boolean bool1 = false;
    int j = zza.b(paramParcel);
    boolean bool2 = false;
    Long localLong = null;
    String str = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str = zza.p(paramParcel, k);
        break;
      case 3: 
        localLong = zza.j(paramParcel, k);
        break;
      case 4: 
        bool2 = zza.c(paramParcel, k);
        break;
      case 5: 
        bool1 = zza.c(paramParcel, k);
        break;
      case 6: 
        localArrayList = zza.D(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new TokenData(i, str, localLong, bool2, bool1, localArrayList);
  }
  
  public TokenData[] a(int paramInt)
  {
    return new TokenData[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */