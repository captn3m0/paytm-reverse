package com.google.android.gms.auth.firstparty.shared;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzc
  implements Parcelable.Creator<ScopeDetail>
{
  static void a(ScopeDetail paramScopeDetail, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramScopeDetail.a);
    zzb.a(paramParcel, 2, paramScopeDetail.b, false);
    zzb.a(paramParcel, 3, paramScopeDetail.c, false);
    zzb.a(paramParcel, 4, paramScopeDetail.d, false);
    zzb.a(paramParcel, 5, paramScopeDetail.e, false);
    zzb.a(paramParcel, 6, paramScopeDetail.f, false);
    zzb.b(paramParcel, 7, paramScopeDetail.g, false);
    zzb.a(paramParcel, 8, paramScopeDetail.h, paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public ScopeDetail a(Parcel paramParcel)
  {
    FACLData localFACLData = null;
    int j = zza.b(paramParcel);
    int i = 0;
    ArrayList localArrayList = new ArrayList();
    String str1 = null;
    String str2 = null;
    String str3 = null;
    String str4 = null;
    String str5 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str5 = zza.p(paramParcel, k);
        break;
      case 3: 
        str4 = zza.p(paramParcel, k);
        break;
      case 4: 
        str3 = zza.p(paramParcel, k);
        break;
      case 5: 
        str2 = zza.p(paramParcel, k);
        break;
      case 6: 
        str1 = zza.p(paramParcel, k);
        break;
      case 7: 
        localArrayList = zza.D(paramParcel, k);
        break;
      case 8: 
        localFACLData = (FACLData)zza.a(paramParcel, k, FACLData.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new ScopeDetail(i, str5, str4, str3, str2, str1, localArrayList, localFACLData);
  }
  
  public ScopeDetail[] a(int paramInt)
  {
    return new ScopeDetail[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/firstparty/shared/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */