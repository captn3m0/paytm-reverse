package com.google.android.gms.auth.firstparty.shared;

import android.os.Parcel;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;

public class FACLConfig
  implements SafeParcelable
{
  public static final zza CREATOR = new zza();
  final int a;
  boolean b;
  String c;
  boolean d;
  boolean e;
  boolean f;
  boolean g;
  
  FACLConfig(int paramInt, boolean paramBoolean1, String paramString, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5)
  {
    this.a = paramInt;
    this.b = paramBoolean1;
    this.c = paramString;
    this.d = paramBoolean2;
    this.e = paramBoolean3;
    this.f = paramBoolean4;
    this.g = paramBoolean5;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if ((paramObject instanceof FACLConfig))
    {
      paramObject = (FACLConfig)paramObject;
      bool1 = bool2;
      if (this.b == ((FACLConfig)paramObject).b)
      {
        bool1 = bool2;
        if (TextUtils.equals(this.c, ((FACLConfig)paramObject).c))
        {
          bool1 = bool2;
          if (this.d == ((FACLConfig)paramObject).d)
          {
            bool1 = bool2;
            if (this.e == ((FACLConfig)paramObject).e)
            {
              bool1 = bool2;
              if (this.f == ((FACLConfig)paramObject).f)
              {
                bool1 = bool2;
                if (this.g == ((FACLConfig)paramObject).g) {
                  bool1 = true;
                }
              }
            }
          }
        }
      }
    }
    return bool1;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Boolean.valueOf(this.b), this.c, Boolean.valueOf(this.d), Boolean.valueOf(this.e), Boolean.valueOf(this.f), Boolean.valueOf(this.g) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/firstparty/shared/FACLConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */