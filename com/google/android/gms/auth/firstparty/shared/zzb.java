package com.google.android.gms.auth.firstparty.shared;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class zzb
  implements Parcelable.Creator<FACLData>
{
  static void a(FACLData paramFACLData, Parcel paramParcel, int paramInt)
  {
    int i = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramFACLData.a);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramFACLData.b, paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 3, paramFACLData.c, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 4, paramFACLData.d);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 5, paramFACLData.e, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, i);
  }
  
  public FACLData a(Parcel paramParcel)
  {
    boolean bool = false;
    String str1 = null;
    int j = zza.b(paramParcel);
    String str2 = null;
    FACLConfig localFACLConfig = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localFACLConfig = (FACLConfig)zza.a(paramParcel, k, FACLConfig.CREATOR);
        break;
      case 3: 
        str2 = zza.p(paramParcel, k);
        break;
      case 4: 
        bool = zza.c(paramParcel, k);
        break;
      case 5: 
        str1 = zza.p(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new FACLData(i, localFACLConfig, str2, bool, str1);
  }
  
  public FACLData[] a(int paramInt)
  {
    return new FACLData[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/auth/firstparty/shared/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */