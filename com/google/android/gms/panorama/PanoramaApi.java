package com.google.android.gms.panorama;

import com.google.android.gms.common.api.Result;

public abstract interface PanoramaApi
{
  public static abstract interface PanoramaResult
    extends Result
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/panorama/PanoramaApi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */