package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzh
  implements Parcelable.Creator<LocationSettingsStates>
{
  static void a(LocationSettingsStates paramLocationSettingsStates, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramLocationSettingsStates.b());
    zzb.a(paramParcel, 1000, paramLocationSettingsStates.a());
    zzb.a(paramParcel, 2, paramLocationSettingsStates.d());
    zzb.a(paramParcel, 3, paramLocationSettingsStates.f());
    zzb.a(paramParcel, 4, paramLocationSettingsStates.c());
    zzb.a(paramParcel, 5, paramLocationSettingsStates.e());
    zzb.a(paramParcel, 6, paramLocationSettingsStates.g());
    zzb.a(paramParcel, paramInt);
  }
  
  public LocationSettingsStates a(Parcel paramParcel)
  {
    boolean bool1 = false;
    int j = zza.b(paramParcel);
    boolean bool2 = false;
    boolean bool3 = false;
    boolean bool4 = false;
    boolean bool5 = false;
    boolean bool6 = false;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        bool6 = zza.c(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        bool5 = zza.c(paramParcel, k);
        break;
      case 3: 
        bool4 = zza.c(paramParcel, k);
        break;
      case 4: 
        bool3 = zza.c(paramParcel, k);
        break;
      case 5: 
        bool2 = zza.c(paramParcel, k);
        break;
      case 6: 
        bool1 = zza.c(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new LocationSettingsStates(i, bool6, bool5, bool4, bool3, bool2, bool1);
  }
  
  public LocationSettingsStates[] a(int paramInt)
  {
    return new LocationSettingsStates[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/zzh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */