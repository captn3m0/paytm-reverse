package com.google.android.gms.location.places.personalized;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzc
  implements Parcelable.Creator<PlaceAliasResult>
{
  static void a(PlaceAliasResult paramPlaceAliasResult, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramPlaceAliasResult.getStatus(), paramInt, false);
    zzb.a(paramParcel, 1000, paramPlaceAliasResult.a);
    zzb.a(paramParcel, 2, paramPlaceAliasResult.a(), paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public PlaceAliasResult a(Parcel paramParcel)
  {
    PlaceUserData localPlaceUserData = null;
    int j = zza.b(paramParcel);
    int i = 0;
    Status localStatus = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
      }
      for (;;)
      {
        break;
        localStatus = (Status)zza.a(paramParcel, k, Status.CREATOR);
        continue;
        i = zza.g(paramParcel, k);
        continue;
        localPlaceUserData = (PlaceUserData)zza.a(paramParcel, k, PlaceUserData.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new PlaceAliasResult(i, localStatus, localPlaceUserData);
  }
  
  public PlaceAliasResult[] a(int paramInt)
  {
    return new PlaceAliasResult[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/personalized/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */