package com.google.android.gms.location.places.personalized;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;

public class PlaceAlias
  implements SafeParcelable
{
  public static final zzb CREATOR = new zzb();
  public static final PlaceAlias a = new PlaceAlias(0, "Home");
  public static final PlaceAlias b = new PlaceAlias(0, "Work");
  final int c;
  private final String d;
  
  PlaceAlias(int paramInt, String paramString)
  {
    this.c = paramInt;
    this.d = paramString;
  }
  
  public String a()
  {
    return this.d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof PlaceAlias)) {
      return false;
    }
    paramObject = (PlaceAlias)paramObject;
    return zzw.a(this.d, ((PlaceAlias)paramObject).d);
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.d });
  }
  
  public String toString()
  {
    return zzw.a(this).a("alias", this.d).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzb.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/personalized/PlaceAlias.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */