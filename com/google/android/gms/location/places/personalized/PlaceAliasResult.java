package com.google.android.gms.location.places.personalized;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class PlaceAliasResult
  implements Result, SafeParcelable
{
  public static final Parcelable.Creator<PlaceAliasResult> CREATOR = new zzc();
  final int a;
  final PlaceUserData b;
  private final Status c;
  
  PlaceAliasResult(int paramInt, Status paramStatus, PlaceUserData paramPlaceUserData)
  {
    this.a = paramInt;
    this.c = paramStatus;
    this.b = paramPlaceUserData;
  }
  
  public PlaceUserData a()
  {
    return this.b;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Status getStatus()
  {
    return this.c;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/personalized/PlaceAliasResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */