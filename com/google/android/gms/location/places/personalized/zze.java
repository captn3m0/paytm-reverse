package com.google.android.gms.location.places.personalized;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zze
  implements Parcelable.Creator<PlaceUserData>
{
  static void a(PlaceUserData paramPlaceUserData, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramPlaceUserData.a(), false);
    zzb.a(paramParcel, 1000, paramPlaceUserData.a);
    zzb.a(paramParcel, 2, paramPlaceUserData.b(), false);
    zzb.c(paramParcel, 6, paramPlaceUserData.c(), false);
    zzb.a(paramParcel, paramInt);
  }
  
  public PlaceUserData a(Parcel paramParcel)
  {
    ArrayList localArrayList = null;
    int j = zza.b(paramParcel);
    int i = 0;
    String str2 = null;
    String str1 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        str1 = zza.p(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str2 = zza.p(paramParcel, k);
        break;
      case 6: 
        localArrayList = zza.c(paramParcel, k, PlaceAlias.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new PlaceUserData(i, str1, str2, localArrayList);
  }
  
  public PlaceUserData[] a(int paramInt)
  {
    return new PlaceUserData[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/personalized/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */