package com.google.android.gms.location.places;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;

public class PlacePhotoResult
  implements Result, SafeParcelable
{
  public static final Parcelable.Creator<PlacePhotoResult> CREATOR = new zzi();
  final int a;
  final BitmapTeleporter b;
  private final Status c;
  private final Bitmap d;
  
  PlacePhotoResult(int paramInt, Status paramStatus, BitmapTeleporter paramBitmapTeleporter)
  {
    this.a = paramInt;
    this.c = paramStatus;
    this.b = paramBitmapTeleporter;
    if (this.b != null)
    {
      this.d = paramBitmapTeleporter.a();
      return;
    }
    this.d = null;
  }
  
  public PlacePhotoResult(Status paramStatus, @Nullable BitmapTeleporter paramBitmapTeleporter)
  {
    this.a = 0;
    this.c = paramStatus;
    this.b = paramBitmapTeleporter;
    if (this.b != null)
    {
      this.d = paramBitmapTeleporter.a();
      return;
    }
    this.d = null;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Status getStatus()
  {
    return this.c;
  }
  
  public String toString()
  {
    return zzw.a(this).a("status", this.c).a("bitmap", this.d).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzi.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/PlacePhotoResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */