package com.google.android.gms.location.places;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;

public class zzb
  implements Parcelable.Creator<AddPlaceRequest>
{
  static void a(AddPlaceRequest paramAddPlaceRequest, Parcel paramParcel, int paramInt)
  {
    int i = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramAddPlaceRequest.a(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1000, paramAddPlaceRequest.a);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramAddPlaceRequest.b(), paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 3, paramAddPlaceRequest.c(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 4, paramAddPlaceRequest.d(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 5, paramAddPlaceRequest.e(), false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 6, paramAddPlaceRequest.f(), paramInt, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, i);
  }
  
  public AddPlaceRequest a(Parcel paramParcel)
  {
    Uri localUri = null;
    int j = zza.b(paramParcel);
    int i = 0;
    String str1 = null;
    ArrayList localArrayList = null;
    String str2 = null;
    LatLng localLatLng = null;
    String str3 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        str3 = zza.p(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localLatLng = (LatLng)zza.a(paramParcel, k, LatLng.CREATOR);
        break;
      case 3: 
        str2 = zza.p(paramParcel, k);
        break;
      case 4: 
        localArrayList = zza.C(paramParcel, k);
        break;
      case 5: 
        str1 = zza.p(paramParcel, k);
        break;
      case 6: 
        localUri = (Uri)zza.a(paramParcel, k, Uri.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new AddPlaceRequest(i, str3, localLatLng, str2, localArrayList, str1, localUri);
  }
  
  public AddPlaceRequest[] a(int paramInt)
  {
    return new AddPlaceRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */