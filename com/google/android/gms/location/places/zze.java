package com.google.android.gms.location.places;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zze
  implements Parcelable.Creator<NearbyAlertRequest>
{
  static void a(NearbyAlertRequest paramNearbyAlertRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramNearbyAlertRequest.b());
    zzb.a(paramParcel, 1000, paramNearbyAlertRequest.a());
    zzb.a(paramParcel, 2, paramNearbyAlertRequest.c());
    zzb.a(paramParcel, 3, paramNearbyAlertRequest.d(), paramInt, false);
    zzb.a(paramParcel, 4, paramNearbyAlertRequest.e(), paramInt, false);
    zzb.a(paramParcel, 5, paramNearbyAlertRequest.f());
    zzb.a(paramParcel, 6, paramNearbyAlertRequest.g());
    zzb.a(paramParcel, 7, paramNearbyAlertRequest.h());
    zzb.a(paramParcel, i);
  }
  
  public NearbyAlertRequest a(Parcel paramParcel)
  {
    NearbyAlertFilter localNearbyAlertFilter = null;
    int i = 0;
    int i1 = zza.b(paramParcel);
    int k = -1;
    int j = 0;
    boolean bool = false;
    PlaceFilter localPlaceFilter = null;
    int m = 0;
    int n = 0;
    while (paramParcel.dataPosition() < i1)
    {
      int i2 = zza.a(paramParcel);
      switch (zza.a(i2))
      {
      default: 
        zza.b(paramParcel, i2);
        break;
      case 1: 
        m = zza.g(paramParcel, i2);
        break;
      case 1000: 
        n = zza.g(paramParcel, i2);
        break;
      case 2: 
        k = zza.g(paramParcel, i2);
        break;
      case 3: 
        localPlaceFilter = (PlaceFilter)zza.a(paramParcel, i2, PlaceFilter.CREATOR);
        break;
      case 4: 
        localNearbyAlertFilter = (NearbyAlertFilter)zza.a(paramParcel, i2, NearbyAlertFilter.CREATOR);
        break;
      case 5: 
        bool = zza.c(paramParcel, i2);
        break;
      case 6: 
        j = zza.g(paramParcel, i2);
        break;
      case 7: 
        i = zza.g(paramParcel, i2);
      }
    }
    if (paramParcel.dataPosition() != i1) {
      throw new zza.zza("Overread allowed size end=" + i1, paramParcel);
    }
    return new NearbyAlertRequest(n, m, k, localPlaceFilter, localNearbyAlertFilter, bool, j, i);
  }
  
  public NearbyAlertRequest[] a(int paramInt)
  {
    return new NearbyAlertRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */