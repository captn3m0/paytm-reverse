package com.google.android.gms.location.places;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;

public class PlaceReport
  implements SafeParcelable
{
  public static final Parcelable.Creator<PlaceReport> CREATOR = new zzj();
  final int a;
  private final String b;
  private final String c;
  private final String d;
  
  PlaceReport(int paramInt, String paramString1, String paramString2, String paramString3)
  {
    this.a = paramInt;
    this.b = paramString1;
    this.c = paramString2;
    this.d = paramString3;
  }
  
  public String a()
  {
    return this.b;
  }
  
  public String b()
  {
    return this.c;
  }
  
  public String c()
  {
    return this.d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof PlaceReport)) {}
    do
    {
      return false;
      paramObject = (PlaceReport)paramObject;
    } while ((!zzw.a(this.b, ((PlaceReport)paramObject).b)) || (!zzw.a(this.c, ((PlaceReport)paramObject).c)) || (!zzw.a(this.d, ((PlaceReport)paramObject).d)));
    return true;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b, this.c, this.d });
  }
  
  public String toString()
  {
    zzw.zza localzza = zzw.a(this);
    localzza.a("placeId", this.b);
    localzza.a("tag", this.c);
    if (!"unknown".equals(this.d)) {
      localzza.a("source", this.d);
    }
    return localzza.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzj.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/PlaceReport.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */