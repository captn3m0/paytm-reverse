package com.google.android.gms.location.places;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.Api.zzb;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zza;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzng;
import com.google.android.gms.location.places.internal.zzi.zza;
import com.google.android.gms.location.places.personalized.zzd;

public class zzl
  extends zzi.zza
{
  private static final String a = zzl.class.getSimpleName();
  private final zzd b;
  private final zza c;
  private final zze d;
  private final zzf e;
  private final zzc f;
  private final Context g;
  
  public zzl(zza paramzza)
  {
    this.b = null;
    this.c = paramzza;
    this.d = null;
    this.e = null;
    this.f = null;
    this.g = null;
  }
  
  public zzl(zzc paramzzc, Context paramContext)
  {
    this.b = null;
    this.c = null;
    this.d = null;
    this.e = null;
    this.f = paramzzc;
    this.g = paramContext.getApplicationContext();
  }
  
  public zzl(zzd paramzzd, Context paramContext)
  {
    this.b = paramzzd;
    this.c = null;
    this.d = null;
    this.e = null;
    this.f = null;
    this.g = paramContext.getApplicationContext();
  }
  
  public zzl(zzf paramzzf)
  {
    this.b = null;
    this.c = null;
    this.d = null;
    this.e = paramzzf;
    this.f = null;
    this.g = null;
  }
  
  public void a(Status paramStatus)
    throws RemoteException
  {
    this.e.zza(paramStatus);
  }
  
  public void a(DataHolder paramDataHolder)
    throws RemoteException
  {
    if (this.b != null) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.a(bool, "placeEstimator cannot be null");
      if (paramDataHolder != null) {
        break;
      }
      if (Log.isLoggable(a, 6)) {
        Log.e(a, "onPlaceEstimated received null DataHolder: " + zzng.a());
      }
      this.b.b(Status.c);
      return;
    }
    Bundle localBundle = paramDataHolder.f();
    if (localBundle == null) {}
    for (int i = 100;; i = PlaceLikelihoodBuffer.a(localBundle))
    {
      paramDataHolder = new PlaceLikelihoodBuffer(paramDataHolder, i, this.g);
      this.b.zza(paramDataHolder);
      return;
    }
  }
  
  public void b(DataHolder paramDataHolder)
    throws RemoteException
  {
    if (paramDataHolder == null)
    {
      if (Log.isLoggable(a, 6)) {
        Log.e(a, "onAutocompletePrediction received null DataHolder: " + zzng.a());
      }
      this.c.b(Status.c);
      return;
    }
    this.c.zza(new AutocompletePredictionBuffer(paramDataHolder));
  }
  
  public void c(DataHolder paramDataHolder)
    throws RemoteException
  {
    if (paramDataHolder == null)
    {
      if (Log.isLoggable(a, 6)) {
        Log.e(a, "onPlaceUserDataFetched received null DataHolder: " + zzng.a());
      }
      this.d.b(Status.c);
      return;
    }
    this.d.zza(new zzd(paramDataHolder));
  }
  
  public void d(DataHolder paramDataHolder)
    throws RemoteException
  {
    paramDataHolder = new PlaceBuffer(paramDataHolder, this.g);
    this.f.zza(paramDataHolder);
  }
  
  public static abstract class zza<A extends Api.zzb>
    extends zzl.zzb<AutocompletePredictionBuffer, A>
  {
    protected AutocompletePredictionBuffer a(Status paramStatus)
    {
      return new AutocompletePredictionBuffer(DataHolder.b(paramStatus.f()));
    }
  }
  
  public static abstract class zzb<R extends Result, A extends Api.zzb>
    extends zza.zza<R, A>
  {}
  
  public static abstract class zzc<A extends Api.zzb>
    extends zzl.zzb<PlaceBuffer, A>
  {
    protected PlaceBuffer a(Status paramStatus)
    {
      return new PlaceBuffer(DataHolder.b(paramStatus.f()), null);
    }
  }
  
  public static abstract class zzd<A extends Api.zzb>
    extends zzl.zzb<PlaceLikelihoodBuffer, A>
  {
    protected PlaceLikelihoodBuffer a(Status paramStatus)
    {
      return new PlaceLikelihoodBuffer(DataHolder.b(paramStatus.f()), 100, null);
    }
  }
  
  public static abstract class zze<A extends Api.zzb>
    extends zzl.zzb<zzd, A>
  {}
  
  public static abstract class zzf<A extends Api.zzb>
    extends zzl.zzb<Status, A>
  {
    protected Status a(Status paramStatus)
    {
      return paramStatus;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */