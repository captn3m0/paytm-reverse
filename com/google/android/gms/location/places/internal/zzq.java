package com.google.android.gms.location.places.internal;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.location.places.PlacePhotoMetadata;

public class zzq
  extends zzt
  implements PlacePhotoMetadata
{
  private final String c = e("photo_fife_url");
  
  public zzq(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
  }
  
  public PlacePhotoMetadata c()
  {
    return new zzp(this.c, d(), e(), f(), this.b);
  }
  
  public int d()
  {
    return a("photo_max_width", 0);
  }
  
  public int e()
  {
    return a("photo_max_height", 0);
  }
  
  public CharSequence f()
  {
    return a("photo_attributions", null);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/internal/zzq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */