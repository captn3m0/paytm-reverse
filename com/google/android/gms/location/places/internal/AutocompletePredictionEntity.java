package com.google.android.gms.location.places.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePrediction.Substring;
import java.util.Collections;
import java.util.List;

public class AutocompletePredictionEntity
  implements SafeParcelable, AutocompletePrediction
{
  public static final Parcelable.Creator<AutocompletePredictionEntity> CREATOR = new zza();
  private static final List<SubstringEntity> k = Collections.emptyList();
  final int a;
  final String b;
  final String c;
  final List<Integer> d;
  final List<SubstringEntity> e;
  final int f;
  final String g;
  final List<SubstringEntity> h;
  final String i;
  final List<SubstringEntity> j;
  
  AutocompletePredictionEntity(int paramInt1, String paramString1, List<Integer> paramList, int paramInt2, String paramString2, List<SubstringEntity> paramList1, String paramString3, List<SubstringEntity> paramList2, String paramString4, List<SubstringEntity> paramList3)
  {
    this.a = paramInt1;
    this.c = paramString1;
    this.d = paramList;
    this.f = paramInt2;
    this.b = paramString2;
    this.e = paramList1;
    this.g = paramString3;
    this.h = paramList2;
    this.i = paramString4;
    this.j = paramList3;
  }
  
  public static AutocompletePredictionEntity a(String paramString1, List<Integer> paramList, int paramInt, String paramString2, List<SubstringEntity> paramList1, String paramString3, List<SubstringEntity> paramList2, String paramString4, List<SubstringEntity> paramList3)
  {
    return new AutocompletePredictionEntity(0, paramString1, paramList, paramInt, (String)zzx.a(paramString2), paramList1, paramString3, paramList2, paramString4, paramList3);
  }
  
  public AutocompletePrediction b()
  {
    return this;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof AutocompletePredictionEntity)) {
        return false;
      }
      paramObject = (AutocompletePredictionEntity)paramObject;
    } while ((zzw.a(this.c, ((AutocompletePredictionEntity)paramObject).c)) && (zzw.a(this.d, ((AutocompletePredictionEntity)paramObject).d)) && (zzw.a(Integer.valueOf(this.f), Integer.valueOf(((AutocompletePredictionEntity)paramObject).f))) && (zzw.a(this.b, ((AutocompletePredictionEntity)paramObject).b)) && (zzw.a(this.e, ((AutocompletePredictionEntity)paramObject).e)) && (zzw.a(this.g, ((AutocompletePredictionEntity)paramObject).g)) && (zzw.a(this.h, ((AutocompletePredictionEntity)paramObject).h)) && (zzw.a(this.i, ((AutocompletePredictionEntity)paramObject).i)) && (zzw.a(this.j, ((AutocompletePredictionEntity)paramObject).j)));
    return false;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.c, this.d, Integer.valueOf(this.f), this.b, this.e, this.g, this.h, this.i, this.j });
  }
  
  public String toString()
  {
    return zzw.a(this).a("placeId", this.c).a("placeTypes", this.d).a("fullText", this.b).a("fullTextMatchedSubstrings", this.e).a("primaryText", this.g).a("primaryTextMatchedSubstrings", this.h).a("secondaryText", this.i).a("secondaryTextMatchedSubstrings", this.j).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zza.a(this, paramParcel, paramInt);
  }
  
  public static class SubstringEntity
    implements SafeParcelable, AutocompletePrediction.Substring
  {
    public static final Parcelable.Creator<SubstringEntity> CREATOR = new zzu();
    final int a;
    final int b;
    final int c;
    
    public SubstringEntity(int paramInt1, int paramInt2, int paramInt3)
    {
      this.a = paramInt1;
      this.b = paramInt2;
      this.c = paramInt3;
    }
    
    public int describeContents()
    {
      return 0;
    }
    
    public boolean equals(Object paramObject)
    {
      if (this == paramObject) {}
      do
      {
        return true;
        if (!(paramObject instanceof SubstringEntity)) {
          return false;
        }
        paramObject = (SubstringEntity)paramObject;
      } while ((zzw.a(Integer.valueOf(this.b), Integer.valueOf(((SubstringEntity)paramObject).b))) && (zzw.a(Integer.valueOf(this.c), Integer.valueOf(((SubstringEntity)paramObject).c))));
      return false;
    }
    
    public int hashCode()
    {
      return zzw.a(new Object[] { Integer.valueOf(this.b), Integer.valueOf(this.c) });
    }
    
    public String toString()
    {
      return zzw.a(this).a("offset", Integer.valueOf(this.b)).a("length", Integer.valueOf(this.c)).toString();
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      zzu.a(this, paramParcel, paramInt);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/internal/AutocompletePredictionEntity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */