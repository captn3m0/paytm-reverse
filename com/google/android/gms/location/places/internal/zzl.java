package com.google.android.gms.location.places.internal;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import java.util.ArrayList;

public class zzl
  implements Parcelable.Creator<PlaceImpl>
{
  static void a(PlaceImpl paramPlaceImpl, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramPlaceImpl.c(), false);
    zzb.a(paramParcel, 2, paramPlaceImpl.s(), false);
    zzb.a(paramParcel, 3, paramPlaceImpl.u(), paramInt, false);
    zzb.a(paramParcel, 4, paramPlaceImpl.h(), paramInt, false);
    zzb.a(paramParcel, 5, paramPlaceImpl.i());
    zzb.a(paramParcel, 6, paramPlaceImpl.j(), paramInt, false);
    zzb.a(paramParcel, 7, paramPlaceImpl.t(), false);
    zzb.a(paramParcel, 8, paramPlaceImpl.k(), paramInt, false);
    zzb.a(paramParcel, 9, paramPlaceImpl.o());
    zzb.a(paramParcel, 10, paramPlaceImpl.p());
    zzb.a(paramParcel, 11, paramPlaceImpl.q());
    zzb.a(paramParcel, 12, paramPlaceImpl.r());
    zzb.a(paramParcel, 13, paramPlaceImpl.e(), false);
    zzb.a(paramParcel, 14, paramPlaceImpl.g(), false);
    zzb.a(paramParcel, 15, paramPlaceImpl.l(), false);
    zzb.b(paramParcel, 17, paramPlaceImpl.n(), false);
    zzb.a(paramParcel, 16, paramPlaceImpl.m(), false);
    zzb.a(paramParcel, 1000, paramPlaceImpl.a);
    zzb.a(paramParcel, 19, paramPlaceImpl.f(), false);
    zzb.a(paramParcel, 20, paramPlaceImpl.d(), false);
    zzb.a(paramParcel, i);
  }
  
  public PlaceImpl a(Parcel paramParcel)
  {
    int k = zza.b(paramParcel);
    int j = 0;
    String str6 = null;
    ArrayList localArrayList3 = null;
    ArrayList localArrayList2 = null;
    Bundle localBundle = null;
    String str5 = null;
    String str4 = null;
    String str3 = null;
    String str2 = null;
    ArrayList localArrayList1 = null;
    LatLng localLatLng = null;
    float f2 = 0.0F;
    LatLngBounds localLatLngBounds = null;
    String str1 = null;
    Uri localUri = null;
    boolean bool = false;
    float f1 = 0.0F;
    int i = 0;
    long l = 0L;
    PlaceLocalization localPlaceLocalization = null;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        str6 = zza.p(paramParcel, m);
        break;
      case 2: 
        localBundle = zza.r(paramParcel, m);
        break;
      case 3: 
        localPlaceLocalization = (PlaceLocalization)zza.a(paramParcel, m, PlaceLocalization.CREATOR);
        break;
      case 4: 
        localLatLng = (LatLng)zza.a(paramParcel, m, LatLng.CREATOR);
        break;
      case 5: 
        f2 = zza.l(paramParcel, m);
        break;
      case 6: 
        localLatLngBounds = (LatLngBounds)zza.a(paramParcel, m, LatLngBounds.CREATOR);
        break;
      case 7: 
        str1 = zza.p(paramParcel, m);
        break;
      case 8: 
        localUri = (Uri)zza.a(paramParcel, m, Uri.CREATOR);
        break;
      case 9: 
        bool = zza.c(paramParcel, m);
        break;
      case 10: 
        f1 = zza.l(paramParcel, m);
        break;
      case 11: 
        i = zza.g(paramParcel, m);
        break;
      case 12: 
        l = zza.i(paramParcel, m);
        break;
      case 13: 
        localArrayList2 = zza.C(paramParcel, m);
        break;
      case 14: 
        str4 = zza.p(paramParcel, m);
        break;
      case 15: 
        str3 = zza.p(paramParcel, m);
        break;
      case 17: 
        localArrayList1 = zza.D(paramParcel, m);
        break;
      case 16: 
        str2 = zza.p(paramParcel, m);
        break;
      case 1000: 
        j = zza.g(paramParcel, m);
        break;
      case 19: 
        str5 = zza.p(paramParcel, m);
        break;
      case 20: 
        localArrayList3 = zza.C(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new PlaceImpl(j, str6, localArrayList3, localArrayList2, localBundle, str5, str4, str3, str2, localArrayList1, localLatLng, f2, localLatLngBounds, str1, localUri, bool, f1, i, l, localPlaceLocalization);
  }
  
  public PlaceImpl[] a(int paramInt)
  {
    return new PlaceImpl[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/internal/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */