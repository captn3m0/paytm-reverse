package com.google.android.gms.location.places.internal;

import android.accounts.Account;
import android.content.Context;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.location.places.PlaceFilter;
import com.google.android.gms.location.places.PlaceReport;
import com.google.android.gms.location.places.PlacesOptions;
import com.google.android.gms.location.places.PlacesOptions.Builder;
import com.google.android.gms.location.places.zzl;
import java.util.Locale;

public class zzk
  extends zzj<zzf>
{
  private final PlacesParams a;
  private final Locale e = Locale.getDefault();
  
  public zzk(Context paramContext, Looper paramLooper, com.google.android.gms.common.internal.zzf paramzzf, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener, String paramString, PlacesOptions paramPlacesOptions)
  {
    super(paramContext, paramLooper, 67, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    paramContext = null;
    if (paramzzf.b() != null) {
      paramContext = paramzzf.b().name;
    }
    this.a = new PlacesParams(paramString, this.e, paramContext, paramPlacesOptions.a, paramPlacesOptions.b);
  }
  
  protected zzf a(IBinder paramIBinder)
  {
    return zzf.zza.a(paramIBinder);
  }
  
  protected String a()
  {
    return "com.google.android.gms.location.places.PlaceDetectionApi";
  }
  
  public void a(zzl paramzzl, PlaceFilter paramPlaceFilter)
    throws RemoteException
  {
    PlaceFilter localPlaceFilter = paramPlaceFilter;
    if (paramPlaceFilter == null) {
      localPlaceFilter = PlaceFilter.c();
    }
    ((zzf)v()).a(localPlaceFilter, this.a, paramzzl);
  }
  
  public void a(zzl paramzzl, PlaceReport paramPlaceReport)
    throws RemoteException
  {
    zzx.a(paramPlaceReport);
    ((zzf)v()).a(paramPlaceReport, this.a, paramzzl);
  }
  
  protected String b()
  {
    return "com.google.android.gms.location.places.internal.IGooglePlaceDetectionService";
  }
  
  public static class zza
    extends Api.zza<zzk, PlacesOptions>
  {
    private final String a;
    
    public zza(String paramString)
    {
      this.a = paramString;
    }
    
    public zzk a(Context paramContext, Looper paramLooper, com.google.android.gms.common.internal.zzf paramzzf, PlacesOptions paramPlacesOptions, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
    {
      String str;
      if (this.a != null)
      {
        str = this.a;
        if (paramPlacesOptions != null) {
          break label58;
        }
        paramPlacesOptions = new PlacesOptions.Builder().a();
      }
      label58:
      for (;;)
      {
        return new zzk(paramContext, paramLooper, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener, str, paramPlacesOptions);
        str = paramContext.getPackageName();
        break;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/internal/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */