package com.google.android.gms.location.places.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzo
  implements Parcelable.Creator<PlaceLocalization>
{
  static void a(PlaceLocalization paramPlaceLocalization, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramPlaceLocalization.b, false);
    zzb.a(paramParcel, 1000, paramPlaceLocalization.a);
    zzb.a(paramParcel, 2, paramPlaceLocalization.c, false);
    zzb.a(paramParcel, 3, paramPlaceLocalization.d, false);
    zzb.a(paramParcel, 4, paramPlaceLocalization.e, false);
    zzb.b(paramParcel, 5, paramPlaceLocalization.f, false);
    zzb.a(paramParcel, paramInt);
  }
  
  public PlaceLocalization a(Parcel paramParcel)
  {
    ArrayList localArrayList = null;
    int j = zza.b(paramParcel);
    int i = 0;
    String str1 = null;
    String str2 = null;
    String str3 = null;
    String str4 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        str4 = zza.p(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str3 = zza.p(paramParcel, k);
        break;
      case 3: 
        str2 = zza.p(paramParcel, k);
        break;
      case 4: 
        str1 = zza.p(paramParcel, k);
        break;
      case 5: 
        localArrayList = zza.D(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new PlaceLocalization(i, str4, str3, str2, str1, localArrayList);
  }
  
  public PlaceLocalization[] a(int paramInt)
  {
    return new PlaceLocalization[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/internal/zzo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */