package com.google.android.gms.location.places.internal;

import android.os.Parcelable.Creator;
import android.util.Log;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.zzsk;
import com.google.android.gms.internal.zzst;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class zzt
  extends com.google.android.gms.common.data.zzc
{
  private final String c = "SafeDataBufferRef";
  
  public zzt(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
  }
  
  protected float a(String paramString, float paramFloat)
  {
    float f = paramFloat;
    if (a_(paramString))
    {
      f = paramFloat;
      if (!i(paramString)) {
        f = f(paramString);
      }
    }
    return f;
  }
  
  protected int a(String paramString, int paramInt)
  {
    int i = paramInt;
    if (a_(paramString))
    {
      i = paramInt;
      if (!i(paramString)) {
        i = c(paramString);
      }
    }
    return i;
  }
  
  protected <E extends SafeParcelable> E a(String paramString, Parcelable.Creator<E> paramCreator)
  {
    paramString = a(paramString, null);
    if (paramString == null) {
      return null;
    }
    return com.google.android.gms.common.internal.safeparcel.zzc.a(paramString, paramCreator);
  }
  
  protected String a(String paramString1, String paramString2)
  {
    String str = paramString2;
    if (a_(paramString1))
    {
      str = paramString2;
      if (!i(paramString1)) {
        str = e(paramString1);
      }
    }
    return str;
  }
  
  protected <E extends SafeParcelable> List<E> a(String paramString, Parcelable.Creator<E> paramCreator, List<E> paramList)
  {
    paramString = a(paramString, null);
    if (paramString == null) {}
    do
    {
      for (;;)
      {
        return paramList;
        try
        {
          Object localObject = zzsk.a(paramString);
          if (((zzsk)localObject).c != null)
          {
            paramString = new ArrayList(((zzsk)localObject).c.length);
            localObject = ((zzsk)localObject).c;
            int j = localObject.length;
            int i = 0;
            while (i < j)
            {
              paramString.add(com.google.android.gms.common.internal.safeparcel.zzc.a(localObject[i], paramCreator));
              i += 1;
            }
            return paramString;
          }
        }
        catch (zzst paramString) {}
      }
    } while (!Log.isLoggable("SafeDataBufferRef", 6));
    Log.e("SafeDataBufferRef", "Cannot parse byte[]", paramString);
    return paramList;
  }
  
  protected List<Integer> a(String paramString, List<Integer> paramList)
  {
    paramString = a(paramString, null);
    if (paramString == null) {}
    do
    {
      for (;;)
      {
        return paramList;
        try
        {
          paramString = zzsk.a(paramString);
          if (paramString.b != null)
          {
            ArrayList localArrayList = new ArrayList(paramString.b.length);
            int i = 0;
            while (i < paramString.b.length)
            {
              localArrayList.add(Integer.valueOf(paramString.b[i]));
              i += 1;
            }
            return localArrayList;
          }
        }
        catch (zzst paramString) {}
      }
    } while (!Log.isLoggable("SafeDataBufferRef", 6));
    Log.e("SafeDataBufferRef", "Cannot parse byte[]", paramString);
    return paramList;
  }
  
  protected boolean a(String paramString, boolean paramBoolean)
  {
    boolean bool = paramBoolean;
    if (a_(paramString))
    {
      bool = paramBoolean;
      if (!i(paramString)) {
        bool = d(paramString);
      }
    }
    return bool;
  }
  
  protected byte[] a(String paramString, byte[] paramArrayOfByte)
  {
    byte[] arrayOfByte = paramArrayOfByte;
    if (a_(paramString))
    {
      arrayOfByte = paramArrayOfByte;
      if (!i(paramString)) {
        arrayOfByte = g(paramString);
      }
    }
    return arrayOfByte;
  }
  
  protected List<String> b(String paramString, List<String> paramList)
  {
    paramString = a(paramString, null);
    if (paramString == null) {}
    do
    {
      for (;;)
      {
        return paramList;
        try
        {
          paramString = zzsk.a(paramString);
          if (paramString.a != null)
          {
            paramString = Arrays.asList(paramString.a);
            return paramString;
          }
        }
        catch (zzst paramString) {}
      }
    } while (!Log.isLoggable("SafeDataBufferRef", 6));
    Log.e("SafeDataBufferRef", "Cannot parse byte[]", paramString);
    return paramList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/internal/zzt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */