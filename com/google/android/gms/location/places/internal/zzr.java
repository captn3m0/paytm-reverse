package com.google.android.gms.location.places.internal;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class zzr
  extends zzt
  implements Place
{
  private final String c = a("place_id", "");
  
  public zzr(DataHolder paramDataHolder, int paramInt, Context paramContext)
  {
    super(paramDataHolder, paramInt);
  }
  
  private PlaceImpl p()
  {
    PlaceImpl localPlaceImpl = new PlaceImpl.zza().c(d().toString()).b(q()).a(e()).a(o()).a(f()).a(g()).b(b().toString()).d(i().toString()).a(k()).b(l()).a(j()).a(m()).a(n()).a();
    localPlaceImpl.a(h());
    return localPlaceImpl;
  }
  
  private List<String> q()
  {
    return b("place_attributions", Collections.emptyList());
  }
  
  public CharSequence b()
  {
    return a("place_name", "");
  }
  
  public Place c()
  {
    return p();
  }
  
  public CharSequence d()
  {
    return a("place_address", "");
  }
  
  public String e()
  {
    return this.c;
  }
  
  public LatLng f()
  {
    return (LatLng)a("place_lat_lng", LatLng.CREATOR);
  }
  
  public float g()
  {
    return a("place_level_number", 0.0F);
  }
  
  public Locale h()
  {
    String str = a("place_locale", "");
    if (!TextUtils.isEmpty(str)) {
      return new Locale(str);
    }
    return Locale.getDefault();
  }
  
  public CharSequence i()
  {
    return a("place_phone_number", "");
  }
  
  public List<Integer> j()
  {
    return a("place_types", Collections.emptyList());
  }
  
  public int k()
  {
    return a("place_price_level", -1);
  }
  
  public float l()
  {
    return a("place_rating", -1.0F);
  }
  
  public LatLngBounds m()
  {
    return (LatLngBounds)a("place_viewport", LatLngBounds.CREATOR);
  }
  
  public Uri n()
  {
    String str = a("place_website_uri", null);
    if (str == null) {
      return null;
    }
    return Uri.parse(str);
  }
  
  public boolean o()
  {
    return a("place_is_permanently_closed", false);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/internal/zzr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */