package com.google.android.gms.location.places.internal;

import android.accounts.Account;
import android.content.Context;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import com.google.android.gms.common.api.Api.zza;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.location.places.AddPlaceRequest;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.PlacesOptions;
import com.google.android.gms.location.places.PlacesOptions.Builder;
import com.google.android.gms.location.places.zzl;
import com.google.android.gms.maps.model.LatLngBounds;
import java.util.List;
import java.util.Locale;

public class zze
  extends zzj<zzg>
{
  private final PlacesParams a;
  private final Locale e = Locale.getDefault();
  
  public zze(Context paramContext, Looper paramLooper, com.google.android.gms.common.internal.zzf paramzzf, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener, String paramString, PlacesOptions paramPlacesOptions)
  {
    super(paramContext, paramLooper, 65, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener);
    paramContext = null;
    if (paramzzf.b() != null) {
      paramContext = paramzzf.b().name;
    }
    this.a = new PlacesParams(paramString, this.e, paramContext, paramPlacesOptions.a, paramPlacesOptions.b);
  }
  
  protected zzg a(IBinder paramIBinder)
  {
    return zzg.zza.a(paramIBinder);
  }
  
  protected String a()
  {
    return "com.google.android.gms.location.places.GeoDataApi";
  }
  
  public void a(com.google.android.gms.location.places.zzf paramzzf, String paramString)
    throws RemoteException
  {
    zzx.a(paramString, "placeId cannot be null");
    ((zzg)v()).a(paramString, this.a, paramzzf);
  }
  
  public void a(com.google.android.gms.location.places.zzf paramzzf, String paramString, int paramInt1, int paramInt2, int paramInt3)
    throws RemoteException
  {
    boolean bool2 = true;
    zzx.a(paramString, "fifeUrl cannot be null");
    if (paramInt1 > 0)
    {
      bool1 = true;
      zzx.b(bool1, "width should be > 0");
      if (paramInt1 <= 0) {
        break label69;
      }
    }
    label69:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzx.b(bool1, "height should be > 0");
      ((zzg)v()).a(paramString, paramInt1, paramInt2, paramInt3, this.a, paramzzf);
      return;
      bool1 = false;
      break;
    }
  }
  
  public void a(zzl paramzzl, AddPlaceRequest paramAddPlaceRequest)
    throws RemoteException
  {
    zzx.a(paramAddPlaceRequest, "userAddedPlace == null");
    ((zzg)v()).a(paramAddPlaceRequest, this.a, paramzzl);
  }
  
  public void a(zzl paramzzl, String paramString, @Nullable LatLngBounds paramLatLngBounds, @Nullable AutocompleteFilter paramAutocompleteFilter)
    throws RemoteException
  {
    zzx.a(paramzzl, "callback == null");
    if (paramString == null) {
      paramString = "";
    }
    for (;;)
    {
      if (paramAutocompleteFilter == null) {
        paramAutocompleteFilter = AutocompleteFilter.a(null);
      }
      for (;;)
      {
        ((zzg)v()).a(paramString, paramLatLngBounds, paramAutocompleteFilter, this.a, paramzzl);
        return;
      }
    }
  }
  
  public void a(zzl paramzzl, List<String> paramList)
    throws RemoteException
  {
    ((zzg)v()).b(paramList, this.a, paramzzl);
  }
  
  protected String b()
  {
    return "com.google.android.gms.location.places.internal.IGooglePlacesService";
  }
  
  public static class zza
    extends Api.zza<zze, PlacesOptions>
  {
    private final String a;
    
    public zza(String paramString)
    {
      this.a = paramString;
    }
    
    public zze a(Context paramContext, Looper paramLooper, com.google.android.gms.common.internal.zzf paramzzf, PlacesOptions paramPlacesOptions, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener)
    {
      String str;
      if (this.a != null)
      {
        str = this.a;
        if (paramPlacesOptions != null) {
          break label58;
        }
        paramPlacesOptions = new PlacesOptions.Builder().a();
      }
      label58:
      for (;;)
      {
        return new zze(paramContext, paramLooper, paramzzf, paramConnectionCallbacks, paramOnConnectionFailedListener, str, paramPlacesOptions);
        str = paramContext.getPackageName();
        break;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/internal/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */