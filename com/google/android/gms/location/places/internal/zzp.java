package com.google.android.gms.location.places.internal;

import android.os.RemoteException;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.location.places.PlacePhotoMetadata;
import com.google.android.gms.location.places.zzf;
import com.google.android.gms.location.places.zzf.zza;

public class zzp
  implements PlacePhotoMetadata
{
  private final String a;
  private final int b;
  private final int c;
  private final CharSequence d;
  private int e;
  
  public zzp(String paramString, int paramInt1, int paramInt2, CharSequence paramCharSequence, int paramInt3)
  {
    this.a = paramString;
    this.b = paramInt1;
    this.c = paramInt2;
    this.d = paramCharSequence;
    this.e = paramInt3;
  }
  
  public PlacePhotoMetadata b()
  {
    return this;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    do
    {
      return true;
      if (!(paramObject instanceof zzp)) {
        return false;
      }
      paramObject = (zzp)paramObject;
    } while ((((zzp)paramObject).b == this.b) && (((zzp)paramObject).c == this.c) && (zzw.a(((zzp)paramObject).a, this.a)) && (zzw.a(((zzp)paramObject).d, this.d)));
    return false;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Integer.valueOf(this.b), Integer.valueOf(this.c), this.a, this.d });
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/internal/zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */