package com.google.android.gms.location.places.internal;

import android.annotation.SuppressLint;
import android.os.Parcel;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import java.util.Locale;

public class PlacesParams
  implements SafeParcelable
{
  public static final zzs CREATOR = new zzs();
  public static final PlacesParams a = new PlacesParams("com.google.android.gms", Locale.getDefault(), null);
  public final int b;
  public final String c;
  public final String d;
  public final String e;
  public final String f;
  public final int g;
  public final int h;
  
  public PlacesParams(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, int paramInt2, int paramInt3)
  {
    this.b = paramInt1;
    this.c = paramString1;
    this.d = paramString2;
    this.e = paramString3;
    this.f = paramString4;
    this.g = paramInt2;
    this.h = paramInt3;
  }
  
  public PlacesParams(String paramString1, Locale paramLocale, String paramString2)
  {
    this(3, paramString1, paramLocale.toString(), paramString2, null, GoogleApiAvailability.a, 0);
  }
  
  public PlacesParams(String paramString1, Locale paramLocale, String paramString2, String paramString3, int paramInt)
  {
    this(3, paramString1, paramLocale.toString(), paramString2, paramString3, GoogleApiAvailability.a, paramInt);
  }
  
  public int describeContents()
  {
    zzs localzzs = CREATOR;
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if ((paramObject == null) || (!(paramObject instanceof PlacesParams))) {
        return false;
      }
      paramObject = (PlacesParams)paramObject;
    } while ((this.g == ((PlacesParams)paramObject).g) && (this.h == ((PlacesParams)paramObject).h) && (this.d.equals(((PlacesParams)paramObject).d)) && (this.c.equals(((PlacesParams)paramObject).c)) && (zzw.a(this.e, ((PlacesParams)paramObject).e)) && (zzw.a(this.f, ((PlacesParams)paramObject).f)));
    return false;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.c, this.d, this.e, this.f, Integer.valueOf(this.g), Integer.valueOf(this.h) });
  }
  
  @SuppressLint({"DefaultLocale"})
  public String toString()
  {
    return zzw.a(this).a("clientPackageName", this.c).a("locale", this.d).a("accountName", this.e).a("gCoreClientName", this.f).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzs localzzs = CREATOR;
    zzs.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/internal/PlacesParams.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */