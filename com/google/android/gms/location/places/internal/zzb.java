package com.google.android.gms.location.places.internal;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.location.places.AutocompletePrediction;
import java.util.Collections;
import java.util.List;

public class zzb
  extends zzt
  implements AutocompletePrediction
{
  public zzb(DataHolder paramDataHolder, int paramInt)
  {
    super(paramDataHolder, paramInt);
  }
  
  private String g()
  {
    return a("ap_description", "");
  }
  
  private String h()
  {
    return a("ap_primary_text", "");
  }
  
  private String i()
  {
    return a("ap_secondary_text", "");
  }
  
  private List<AutocompletePredictionEntity.SubstringEntity> j()
  {
    return a("ap_matched_subscriptions", AutocompletePredictionEntity.SubstringEntity.CREATOR, Collections.emptyList());
  }
  
  private List<AutocompletePredictionEntity.SubstringEntity> k()
  {
    return a("ap_primary_text_matched", AutocompletePredictionEntity.SubstringEntity.CREATOR, Collections.emptyList());
  }
  
  private List<AutocompletePredictionEntity.SubstringEntity> l()
  {
    return a("ap_secondary_text_matched", AutocompletePredictionEntity.SubstringEntity.CREATOR, Collections.emptyList());
  }
  
  public AutocompletePrediction c()
  {
    return AutocompletePredictionEntity.a(e(), f(), d(), g(), j(), h(), k(), i(), l());
  }
  
  public int d()
  {
    return a("ap_personalization_type", 6);
  }
  
  public String e()
  {
    return a("ap_place_id", null);
  }
  
  public List<Integer> f()
  {
    return a("ap_place_types", Collections.emptyList());
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/internal/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */