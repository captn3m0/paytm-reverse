package com.google.android.gms.location.places.internal;

import android.content.Context;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceLikelihood;

public class zzn
  extends zzt
  implements PlaceLikelihood
{
  private final Context c;
  
  public zzn(DataHolder paramDataHolder, int paramInt, Context paramContext)
  {
    super(paramDataHolder, paramInt);
    this.c = paramContext;
  }
  
  public PlaceLikelihood c()
  {
    return PlaceLikelihoodEntity.a((PlaceImpl)e().a(), d());
  }
  
  public float d()
  {
    return a("place_likelihood", -1.0F);
  }
  
  public Place e()
  {
    return new zzr(this.a, this.b, this.c);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/internal/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */