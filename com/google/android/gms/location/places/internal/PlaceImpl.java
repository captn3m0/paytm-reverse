package com.google.android.gms.location.places.internal;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public final class PlaceImpl
  implements SafeParcelable, Place
{
  public static final zzl CREATOR = new zzl();
  final int a;
  private final String b;
  private final Bundle c;
  @Deprecated
  private final PlaceLocalization d;
  private final LatLng e;
  private final float f;
  private final LatLngBounds g;
  private final String h;
  private final Uri i;
  private final boolean j;
  private final float k;
  private final int l;
  private final long m;
  private final List<Integer> n;
  private final List<Integer> o;
  private final String p;
  private final String q;
  private final String r;
  private final String s;
  private final List<String> t;
  private final Map<Integer, String> u;
  private final TimeZone v;
  private Locale w;
  
  PlaceImpl(int paramInt1, String paramString1, List<Integer> paramList1, List<Integer> paramList2, Bundle paramBundle, String paramString2, String paramString3, String paramString4, String paramString5, List<String> paramList, LatLng paramLatLng, float paramFloat1, LatLngBounds paramLatLngBounds, String paramString6, Uri paramUri, boolean paramBoolean, float paramFloat2, int paramInt2, long paramLong, PlaceLocalization paramPlaceLocalization)
  {
    this.a = paramInt1;
    this.b = paramString1;
    this.o = Collections.unmodifiableList(paramList1);
    this.n = paramList2;
    if (paramBundle != null)
    {
      this.c = paramBundle;
      this.p = paramString2;
      this.q = paramString3;
      this.r = paramString4;
      this.s = paramString5;
      if (paramList == null) {
        break label176;
      }
      label68:
      this.t = paramList;
      this.e = paramLatLng;
      this.f = paramFloat1;
      this.g = paramLatLngBounds;
      if (paramString6 == null) {
        break label184;
      }
    }
    for (;;)
    {
      this.h = paramString6;
      this.i = paramUri;
      this.j = paramBoolean;
      this.k = paramFloat2;
      this.l = paramInt2;
      this.m = paramLong;
      this.u = Collections.unmodifiableMap(new HashMap());
      this.v = null;
      this.w = null;
      this.d = paramPlaceLocalization;
      return;
      paramBundle = new Bundle();
      break;
      label176:
      paramList = Collections.emptyList();
      break label68;
      label184:
      paramString6 = "UTC";
    }
  }
  
  public void a(Locale paramLocale)
  {
    this.w = paramLocale;
  }
  
  public String c()
  {
    return this.b;
  }
  
  public List<Integer> d()
  {
    return this.o;
  }
  
  public int describeContents()
  {
    zzl localzzl = CREATOR;
    return 0;
  }
  
  public List<Integer> e()
  {
    return this.n;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof PlaceImpl)) {
        return false;
      }
      paramObject = (PlaceImpl)paramObject;
    } while ((this.b.equals(((PlaceImpl)paramObject).b)) && (zzw.a(this.w, ((PlaceImpl)paramObject).w)) && (this.m == ((PlaceImpl)paramObject).m));
    return false;
  }
  
  public String f()
  {
    return this.p;
  }
  
  public String g()
  {
    return this.q;
  }
  
  public LatLng h()
  {
    return this.e;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.b, this.w, Long.valueOf(this.m) });
  }
  
  public float i()
  {
    return this.f;
  }
  
  public LatLngBounds j()
  {
    return this.g;
  }
  
  public Uri k()
  {
    return this.i;
  }
  
  public String l()
  {
    return this.r;
  }
  
  public String m()
  {
    return this.s;
  }
  
  public List<String> n()
  {
    return this.t;
  }
  
  public boolean o()
  {
    return this.j;
  }
  
  public float p()
  {
    return this.k;
  }
  
  public int q()
  {
    return this.l;
  }
  
  public long r()
  {
    return this.m;
  }
  
  public Bundle s()
  {
    return this.c;
  }
  
  public String t()
  {
    return this.h;
  }
  
  @SuppressLint({"DefaultLocale"})
  public String toString()
  {
    return zzw.a(this).a("id", this.b).a("placeTypes", this.o).a("locale", this.w).a("name", this.p).a("address", this.q).a("phoneNumber", this.r).a("latlng", this.e).a("viewport", this.g).a("websiteUri", this.i).a("isPermanentlyClosed", Boolean.valueOf(this.j)).a("priceLevel", Integer.valueOf(this.l)).a("timestampSecs", Long.valueOf(this.m)).toString();
  }
  
  @Deprecated
  public PlaceLocalization u()
  {
    return this.d;
  }
  
  public Place v()
  {
    return this;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzl localzzl = CREATOR;
    zzl.a(this, paramParcel, paramInt);
  }
  
  public static class zza
  {
    private int a = 0;
    private String b;
    private Bundle c;
    private String d;
    private LatLng e;
    private float f;
    private LatLngBounds g;
    private String h;
    private Uri i;
    private boolean j;
    private float k;
    private int l;
    private long m;
    private List<Integer> n;
    private String o;
    private String p;
    private String q;
    private List<String> r;
    
    public zza a(float paramFloat)
    {
      this.f = paramFloat;
      return this;
    }
    
    public zza a(int paramInt)
    {
      this.l = paramInt;
      return this;
    }
    
    public zza a(Uri paramUri)
    {
      this.i = paramUri;
      return this;
    }
    
    public zza a(LatLng paramLatLng)
    {
      this.e = paramLatLng;
      return this;
    }
    
    public zza a(LatLngBounds paramLatLngBounds)
    {
      this.g = paramLatLngBounds;
      return this;
    }
    
    public zza a(String paramString)
    {
      this.b = paramString;
      return this;
    }
    
    public zza a(List<Integer> paramList)
    {
      this.n = paramList;
      return this;
    }
    
    public zza a(boolean paramBoolean)
    {
      this.j = paramBoolean;
      return this;
    }
    
    public PlaceImpl a()
    {
      return new PlaceImpl(this.a, this.b, this.n, Collections.emptyList(), this.c, this.d, this.o, this.p, this.q, this.r, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, PlaceLocalization.a(this.d, this.o, this.p, this.q, this.r));
    }
    
    public zza b(float paramFloat)
    {
      this.k = paramFloat;
      return this;
    }
    
    public zza b(String paramString)
    {
      this.d = paramString;
      return this;
    }
    
    public zza b(List<String> paramList)
    {
      this.r = paramList;
      return this;
    }
    
    public zza c(String paramString)
    {
      this.o = paramString;
      return this;
    }
    
    public zza d(String paramString)
    {
      this.p = paramString;
      return this;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/internal/PlaceImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */