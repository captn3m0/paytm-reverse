package com.google.android.gms.location.places.ui;

import android.app.Activity;
import android.content.Intent;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

public class PlacePicker
  extends zza
{
  public static class IntentBuilder
    extends zza.zza
  {
    public IntentBuilder()
    {
      super();
      this.a.putExtra("gmscore_client_jar_version", GoogleApiAvailability.a);
    }
    
    public Intent a(Activity paramActivity)
      throws GooglePlayServicesRepairableException, GooglePlayServicesNotAvailableException
    {
      return super.a(paramActivity);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/ui/PlacePicker.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */