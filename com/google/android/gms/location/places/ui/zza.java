package com.google.android.gms.location.places.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources.Theme;
import android.util.TypedValue;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.zzc;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.internal.PlaceImpl;

abstract class zza
{
  public static Place c(Context paramContext, Intent paramIntent)
  {
    zzx.a(paramIntent, "intent must not be null");
    zzx.a(paramContext, "context must not be null");
    return (Place)zzc.a(paramIntent, "selected_place", PlaceImpl.CREATOR);
  }
  
  public static Status d(Context paramContext, Intent paramIntent)
  {
    zzx.a(paramIntent, "intent must not be null");
    zzx.a(paramContext, "context must not be null");
    return (Status)zzc.a(paramIntent, "status", Status.CREATOR);
  }
  
  protected static abstract class zza
  {
    protected final Intent a;
    
    public zza(String paramString)
    {
      this.a = new Intent(paramString);
      this.a.setPackage("com.google.android.gms");
    }
    
    protected Intent a(Activity paramActivity)
      throws GooglePlayServicesRepairableException, GooglePlayServicesNotAvailableException
    {
      Resources.Theme localTheme = paramActivity.getTheme();
      TypedValue localTypedValue1 = new TypedValue();
      TypedValue localTypedValue2 = new TypedValue();
      if ((localTheme.resolveAttribute(16843827, localTypedValue1, true)) && (!this.a.hasExtra("primary_color"))) {
        this.a.putExtra("primary_color", localTypedValue1.data);
      }
      if ((localTheme.resolveAttribute(16843828, localTypedValue2, true)) && (!this.a.hasExtra("primary_color_dark"))) {
        this.a.putExtra("primary_color_dark", localTypedValue2.data);
      }
      GoogleApiAvailability.a().c(paramActivity);
      return this.a;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/ui/zza.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */