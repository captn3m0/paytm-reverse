package com.google.android.gms.location.places.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import com.google.android.gms.R.id;
import com.google.android.gms.R.layout;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLngBounds;

public class SupportPlaceAutocompleteFragment
  extends Fragment
{
  private View a;
  private View b;
  private EditText c;
  @Nullable
  private LatLngBounds d;
  @Nullable
  private AutocompleteFilter e;
  @Nullable
  private PlaceSelectionListener f;
  
  private void a()
  {
    int j = 0;
    View localView;
    if (!this.c.getText().toString().isEmpty())
    {
      i = 1;
      localView = this.b;
      if (i == 0) {
        break label42;
      }
    }
    label42:
    for (int i = j;; i = 8)
    {
      localView.setVisibility(i);
      return;
      i = 0;
      break;
    }
  }
  
  private void b()
  {
    try
    {
      startActivityForResult(new PlaceAutocomplete.IntentBuilder(2).a(this.d).a(this.e).a(this.c.getText().toString()).a(1).a(getActivity()), 1);
      i = -1;
    }
    catch (GooglePlayServicesRepairableException localGooglePlayServicesRepairableException)
    {
      for (;;)
      {
        i = localGooglePlayServicesRepairableException.a();
        Log.e("Places", "Could not open autocomplete activity", localGooglePlayServicesRepairableException);
      }
    }
    catch (GooglePlayServicesNotAvailableException localGooglePlayServicesNotAvailableException)
    {
      for (;;)
      {
        int i = localGooglePlayServicesNotAvailableException.a;
        Log.e("Places", "Could not open autocomplete activity", localGooglePlayServicesNotAvailableException);
      }
    }
    if (i != -1) {
      GoogleApiAvailability.a().b(getActivity(), i, 2);
    }
  }
  
  public void a(CharSequence paramCharSequence)
  {
    this.c.setText(paramCharSequence);
    a();
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Object localObject;
    if (paramInt1 == 1)
    {
      if (paramInt2 != -1) {
        break label62;
      }
      localObject = PlaceAutocomplete.a(getActivity(), paramIntent);
      if (this.f != null) {
        this.f.a((Place)localObject);
      }
      a(((Place)localObject).b().toString());
    }
    for (;;)
    {
      super.onActivityResult(paramInt1, paramInt2, paramIntent);
      return;
      label62:
      if (paramInt2 == 2)
      {
        localObject = PlaceAutocomplete.b(getActivity(), paramIntent);
        if (this.f != null) {
          this.f.a((Status)localObject);
        }
      }
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(R.layout.place_autocomplete_fragment, paramViewGroup, false);
    this.a = paramLayoutInflater.findViewById(R.id.place_autocomplete_search_button);
    this.b = paramLayoutInflater.findViewById(R.id.place_autocomplete_clear_button);
    this.c = ((EditText)paramLayoutInflater.findViewById(R.id.place_autocomplete_search_input));
    paramViewGroup = new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        SupportPlaceAutocompleteFragment.a(SupportPlaceAutocompleteFragment.this);
      }
    };
    this.a.setOnClickListener(paramViewGroup);
    this.c.setOnClickListener(paramViewGroup);
    this.b.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        SupportPlaceAutocompleteFragment.this.a("");
      }
    });
    a();
    return paramLayoutInflater;
  }
  
  public void onDestroyView()
  {
    this.a = null;
    this.b = null;
    this.c = null;
    super.onDestroyView();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/ui/SupportPlaceAutocompleteFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */