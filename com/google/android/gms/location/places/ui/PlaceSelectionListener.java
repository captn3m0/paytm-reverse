package com.google.android.gms.location.places.ui;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;

public abstract interface PlaceSelectionListener
{
  public abstract void a(Status paramStatus);
  
  public abstract void a(Place paramPlace);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/ui/PlaceSelectionListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */