package com.google.android.gms.location.places.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLngBounds;

public class PlaceAutocomplete
  extends zza
{
  public static Place a(Context paramContext, Intent paramIntent)
  {
    return zza.c(paramContext, paramIntent);
  }
  
  public static Status b(Context paramContext, Intent paramIntent)
  {
    return zza.d(paramContext, paramIntent);
  }
  
  public static class IntentBuilder
    extends zza.zza
  {
    public IntentBuilder(int paramInt)
    {
      super();
      this.a.putExtra("gmscore_client_jar_version", GoogleApiAvailability.a);
      this.a.putExtra("mode", paramInt);
      this.a.putExtra("origin", 2);
    }
    
    public Intent a(Activity paramActivity)
      throws GooglePlayServicesRepairableException, GooglePlayServicesNotAvailableException
    {
      return super.a(paramActivity);
    }
    
    public IntentBuilder a(int paramInt)
    {
      this.a.putExtra("origin", paramInt);
      return this;
    }
    
    public IntentBuilder a(@Nullable AutocompleteFilter paramAutocompleteFilter)
    {
      if (paramAutocompleteFilter != null)
      {
        this.a.putExtra("filter", paramAutocompleteFilter);
        return this;
      }
      this.a.removeExtra("filter");
      return this;
    }
    
    public IntentBuilder a(@Nullable LatLngBounds paramLatLngBounds)
    {
      if (paramLatLngBounds != null)
      {
        this.a.putExtra("bounds", paramLatLngBounds);
        return this;
      }
      this.a.removeExtra("bounds");
      return this;
    }
    
    public IntentBuilder a(@Nullable String paramString)
    {
      if (paramString != null)
      {
        this.a.putExtra("initial_query", paramString);
        return this;
      }
      this.a.removeExtra("initial_query");
      return this;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/ui/PlaceAutocomplete.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */