package com.google.android.gms.location.places;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Api.zzc;
import com.google.android.gms.location.places.internal.zzd;
import com.google.android.gms.location.places.internal.zze;
import com.google.android.gms.location.places.internal.zze.zza;
import com.google.android.gms.location.places.internal.zzj;
import com.google.android.gms.location.places.internal.zzk;
import com.google.android.gms.location.places.internal.zzk.zza;

public class Places
{
  public static final Api.zzc<zze> a = new Api.zzc();
  public static final Api.zzc<zzk> b = new Api.zzc();
  public static final Api<PlacesOptions> c = new Api("Places.GEO_DATA_API", new zze.zza(null), a);
  public static final Api<PlacesOptions> d = new Api("Places.PLACE_DETECTION_API", new zzk.zza(null), b);
  public static final GeoDataApi e = new zzd();
  public static final PlaceDetectionApi f = new zzj();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/Places.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */