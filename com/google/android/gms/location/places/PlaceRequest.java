package com.google.android.gms.location.places;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import java.util.concurrent.TimeUnit;

public final class PlaceRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator<PlaceRequest> CREATOR = new zzk();
  static final long a = TimeUnit.HOURS.toMillis(1L);
  final int b;
  private final PlaceFilter c;
  private final long d;
  private final int e;
  private final long f;
  
  public PlaceRequest(int paramInt1, PlaceFilter paramPlaceFilter, long paramLong1, int paramInt2, long paramLong2)
  {
    this.b = paramInt1;
    this.c = paramPlaceFilter;
    this.d = paramLong1;
    this.e = paramInt2;
    this.f = paramLong2;
  }
  
  public PlaceFilter a()
  {
    return this.c;
  }
  
  public long b()
  {
    return this.d;
  }
  
  public int c()
  {
    return this.e;
  }
  
  public long d()
  {
    return this.f;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof PlaceRequest)) {
        return false;
      }
      paramObject = (PlaceRequest)paramObject;
    } while ((zzw.a(this.c, ((PlaceRequest)paramObject).c)) && (this.d == ((PlaceRequest)paramObject).d) && (this.e == ((PlaceRequest)paramObject).e) && (this.f == ((PlaceRequest)paramObject).f));
    return false;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { this.c, Long.valueOf(this.d), Integer.valueOf(this.e), Long.valueOf(this.f) });
  }
  
  @SuppressLint({"DefaultLocale"})
  public String toString()
  {
    return zzw.a(this).a("filter", this.c).a("interval", Long.valueOf(this.d)).a("priority", Integer.valueOf(this.e)).a("expireAt", Long.valueOf(this.f)).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzk.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/PlaceRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */