package com.google.android.gms.location.places;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api.zzb;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.internal.zzh.zza;

public class zzf
  extends zzh.zza
{
  private final zzb a;
  private final zza b;
  
  public zzf(zza paramzza)
  {
    this.a = null;
    this.b = paramzza;
  }
  
  public zzf(zzb paramzzb)
  {
    this.a = paramzzb;
    this.b = null;
  }
  
  public void a(PlacePhotoMetadataResult paramPlacePhotoMetadataResult)
    throws RemoteException
  {
    this.a.zza(paramPlacePhotoMetadataResult);
  }
  
  public void a(PlacePhotoResult paramPlacePhotoResult)
    throws RemoteException
  {
    this.b.zza(paramPlacePhotoResult);
  }
  
  public static abstract class zza<A extends Api.zzb>
    extends zzl.zzb<PlacePhotoResult, A>
  {
    protected PlacePhotoResult a(Status paramStatus)
    {
      return new PlacePhotoResult(paramStatus, null);
    }
  }
  
  public static abstract class zzb<A extends Api.zzb>
    extends zzl.zzb<PlacePhotoMetadataResult, A>
  {
    protected PlacePhotoMetadataResult a(Status paramStatus)
    {
      return new PlacePhotoMetadataResult(paramStatus, null);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */