package com.google.android.gms.location.places;

import android.os.Parcel;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.common.internal.zzw.zza;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class AutocompleteFilter
  implements SafeParcelable
{
  public static final zzc CREATOR = new zzc();
  final int a;
  final boolean b;
  final List<Integer> c;
  final int d;
  
  AutocompleteFilter(int paramInt, boolean paramBoolean, List<Integer> paramList)
  {
    this.a = paramInt;
    this.c = paramList;
    this.d = b(paramList);
    if (this.a < 1)
    {
      if (!paramBoolean) {}
      for (paramBoolean = bool;; paramBoolean = false)
      {
        this.b = paramBoolean;
        return;
      }
    }
    this.b = paramBoolean;
  }
  
  @Deprecated
  public static AutocompleteFilter a(@Nullable Collection<Integer> paramCollection)
  {
    return new Builder().a(b(paramCollection)).a();
  }
  
  private static int b(@Nullable Collection<Integer> paramCollection)
  {
    if ((paramCollection == null) || (paramCollection.isEmpty())) {
      return 0;
    }
    return ((Integer)paramCollection.iterator().next()).intValue();
  }
  
  private static List<Integer> b(int paramInt)
  {
    return Arrays.asList(new Integer[] { Integer.valueOf(paramInt) });
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (!(paramObject instanceof AutocompleteFilter)) {
        return false;
      }
      paramObject = (AutocompleteFilter)paramObject;
    } while ((this.d == this.d) && (this.b == ((AutocompleteFilter)paramObject).b));
    return false;
  }
  
  public int hashCode()
  {
    return zzw.a(new Object[] { Boolean.valueOf(this.b), Integer.valueOf(this.d) });
  }
  
  public String toString()
  {
    return zzw.a(this).a("includeQueryPredictions", Boolean.valueOf(this.b)).a("typeFilter", Integer.valueOf(this.d)).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzc.a(this, paramParcel, paramInt);
  }
  
  public static final class Builder
  {
    private boolean a = false;
    private int b = 0;
    
    public Builder a(int paramInt)
    {
      this.b = paramInt;
      return this;
    }
    
    public AutocompleteFilter a()
    {
      return new AutocompleteFilter(1, this.a, AutocompleteFilter.a(this.b));
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/AutocompleteFilter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */