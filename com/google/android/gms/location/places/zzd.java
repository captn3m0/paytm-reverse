package com.google.android.gms.location.places;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzd
  implements Parcelable.Creator<NearbyAlertFilter>
{
  static void a(NearbyAlertFilter paramNearbyAlertFilter, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.b(paramParcel, 1, paramNearbyAlertFilter.b, false);
    zzb.a(paramParcel, 1000, paramNearbyAlertFilter.a);
    zzb.a(paramParcel, 2, paramNearbyAlertFilter.c, false);
    zzb.c(paramParcel, 3, paramNearbyAlertFilter.d, false);
    zzb.a(paramParcel, 4, paramNearbyAlertFilter.e, false);
    zzb.a(paramParcel, 5, paramNearbyAlertFilter.f);
    zzb.a(paramParcel, paramInt);
  }
  
  public NearbyAlertFilter a(Parcel paramParcel)
  {
    boolean bool = false;
    String str = null;
    int j = zza.b(paramParcel);
    ArrayList localArrayList1 = null;
    ArrayList localArrayList2 = null;
    ArrayList localArrayList3 = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localArrayList3 = zza.D(paramParcel, k);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        localArrayList2 = zza.C(paramParcel, k);
        break;
      case 3: 
        localArrayList1 = zza.c(paramParcel, k, UserDataType.CREATOR);
        break;
      case 4: 
        str = zza.p(paramParcel, k);
        break;
      case 5: 
        bool = zza.c(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new NearbyAlertFilter(i, localArrayList3, localArrayList2, localArrayList1, str, bool);
  }
  
  public NearbyAlertFilter[] a(int paramInt)
  {
    return new NearbyAlertFilter[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */