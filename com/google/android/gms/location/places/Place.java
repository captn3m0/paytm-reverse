package com.google.android.gms.location.places;

import com.google.android.gms.common.data.Freezable;

public abstract interface Place
  extends Freezable<Place>
{
  public abstract CharSequence b();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/Place.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */