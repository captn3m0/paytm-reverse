package com.google.android.gms.location.places;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.AbstractDataBuffer;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.location.places.internal.zzr;

public class PlaceBuffer
  extends AbstractDataBuffer<Place>
  implements Result
{
  private final Context b;
  private final Status c;
  private final String d;
  
  public PlaceBuffer(DataHolder paramDataHolder, Context paramContext)
  {
    super(paramDataHolder);
    this.b = paramContext;
    this.c = PlacesStatusCodes.c(paramDataHolder.e());
    if ((paramDataHolder != null) && (paramDataHolder.f() != null))
    {
      this.d = paramDataHolder.f().getString("com.google.android.gms.location.places.PlaceBuffer.ATTRIBUTIONS_EXTRA_KEY");
      return;
    }
    this.d = null;
  }
  
  public Place b(int paramInt)
  {
    return new zzr(this.a, paramInt, this.b);
  }
  
  public Status getStatus()
  {
    return this.c;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/PlaceBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */