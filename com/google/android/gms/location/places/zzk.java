package com.google.android.gms.location.places;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzk
  implements Parcelable.Creator<PlaceRequest>
{
  static void a(PlaceRequest paramPlaceRequest, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1000, paramPlaceRequest.b);
    zzb.a(paramParcel, 2, paramPlaceRequest.a(), paramInt, false);
    zzb.a(paramParcel, 3, paramPlaceRequest.b());
    zzb.a(paramParcel, 4, paramPlaceRequest.c());
    zzb.a(paramParcel, 5, paramPlaceRequest.d());
    zzb.a(paramParcel, i);
  }
  
  public PlaceRequest a(Parcel paramParcel)
  {
    int k = zza.b(paramParcel);
    int j = 0;
    PlaceFilter localPlaceFilter = null;
    long l2 = PlaceRequest.a;
    int i = 102;
    long l1 = Long.MAX_VALUE;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1000: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        localPlaceFilter = (PlaceFilter)zza.a(paramParcel, m, PlaceFilter.CREATOR);
        break;
      case 3: 
        l2 = zza.i(paramParcel, m);
        break;
      case 4: 
        i = zza.g(paramParcel, m);
        break;
      case 5: 
        l1 = zza.i(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new PlaceRequest(j, localPlaceFilter, l2, i, l1);
  }
  
  public PlaceRequest[] a(int paramInt)
  {
    return new PlaceRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/places/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */