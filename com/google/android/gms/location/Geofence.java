package com.google.android.gms.location;

public abstract interface Geofence
{
  public static final class Builder
  {
    private String a = null;
    private int b = 0;
    private long c = Long.MIN_VALUE;
    private short d = -1;
    private int e = 0;
    private int f = -1;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/Geofence.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */