package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class LocationRequestCreator
  implements Parcelable.Creator<LocationRequest>
{
  static void a(LocationRequest paramLocationRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramLocationRequest.a);
    zzb.a(paramParcel, 1000, paramLocationRequest.b());
    zzb.a(paramParcel, 2, paramLocationRequest.b);
    zzb.a(paramParcel, 3, paramLocationRequest.c);
    zzb.a(paramParcel, 4, paramLocationRequest.d);
    zzb.a(paramParcel, 5, paramLocationRequest.e);
    zzb.a(paramParcel, 6, paramLocationRequest.f);
    zzb.a(paramParcel, 7, paramLocationRequest.g);
    zzb.a(paramParcel, 8, paramLocationRequest.h);
    zzb.a(paramParcel, paramInt);
  }
  
  public LocationRequest a(Parcel paramParcel)
  {
    int m = zza.b(paramParcel);
    int k = 0;
    int j = 102;
    long l4 = 3600000L;
    long l3 = 600000L;
    boolean bool = false;
    long l2 = Long.MAX_VALUE;
    int i = Integer.MAX_VALUE;
    float f = 0.0F;
    long l1 = 0L;
    while (paramParcel.dataPosition() < m)
    {
      int n = zza.a(paramParcel);
      switch (zza.a(n))
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        j = zza.g(paramParcel, n);
        break;
      case 1000: 
        k = zza.g(paramParcel, n);
        break;
      case 2: 
        l4 = zza.i(paramParcel, n);
        break;
      case 3: 
        l3 = zza.i(paramParcel, n);
        break;
      case 4: 
        bool = zza.c(paramParcel, n);
        break;
      case 5: 
        l2 = zza.i(paramParcel, n);
        break;
      case 6: 
        i = zza.g(paramParcel, n);
        break;
      case 7: 
        f = zza.l(paramParcel, n);
        break;
      case 8: 
        l1 = zza.i(paramParcel, n);
      }
    }
    if (paramParcel.dataPosition() != m) {
      throw new zza.zza("Overread allowed size end=" + m, paramParcel);
    }
    return new LocationRequest(k, j, l4, l3, bool, l2, i, f, l1);
  }
  
  public LocationRequest[] a(int paramInt)
  {
    return new LocationRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/LocationRequestCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */