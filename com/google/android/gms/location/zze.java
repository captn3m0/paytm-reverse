package com.google.android.gms.location;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.List;

public class zze
  implements Parcelable.Creator<LocationResult>
{
  static void a(LocationResult paramLocationResult, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.c(paramParcel, 1, paramLocationResult.a(), false);
    zzb.a(paramParcel, 1000, paramLocationResult.b());
    zzb.a(paramParcel, paramInt);
  }
  
  public LocationResult a(Parcel paramParcel)
  {
    int j = zza.b(paramParcel);
    int i = 0;
    Object localObject = LocationResult.a;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localObject = zza.c(paramParcel, k, Location.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new LocationResult(i, (List)localObject);
  }
  
  public LocationResult[] a(int paramInt)
  {
    return new LocationResult[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/zze.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */