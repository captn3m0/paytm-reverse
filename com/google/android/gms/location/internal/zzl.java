package com.google.android.gms.location.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.location.Location;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zzb;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationStatusCodes;
import java.util.List;

public class zzl
  extends zzb
{
  private final zzk e = new zzk(paramContext, this.a);
  
  public zzl(Context paramContext, Looper paramLooper, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener, String paramString)
  {
    this(paramContext, paramLooper, paramConnectionCallbacks, paramOnConnectionFailedListener, paramString, zzf.a(paramContext));
  }
  
  public zzl(Context paramContext, Looper paramLooper, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener paramOnConnectionFailedListener, String paramString, zzf paramzzf)
  {
    super(paramContext, paramLooper, paramConnectionCallbacks, paramOnConnectionFailedListener, paramString, paramzzf);
  }
  
  public void a(long paramLong, PendingIntent paramPendingIntent)
    throws RemoteException
  {
    u();
    zzx.a(paramPendingIntent);
    if (paramLong >= 0L) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.b(bool, "detectionIntervalMillis must be >= 0");
      ((zzi)v()).a(paramLong, true, paramPendingIntent);
      return;
    }
  }
  
  public void a(PendingIntent paramPendingIntent)
    throws RemoteException
  {
    u();
    zzx.a(paramPendingIntent);
    ((zzi)v()).a(paramPendingIntent);
  }
  
  public void a(PendingIntent paramPendingIntent, zza.zzb<Status> paramzzb)
    throws RemoteException
  {
    u();
    zzx.a(paramPendingIntent, "PendingIntent must be specified.");
    zzx.a(paramzzb, "ResultHolder not provided.");
    paramzzb = new zzb(paramzzb);
    ((zzi)v()).a(paramPendingIntent, paramzzb, q().getPackageName());
  }
  
  public void a(PendingIntent paramPendingIntent, zzg paramzzg)
    throws RemoteException
  {
    this.e.a(paramPendingIntent, paramzzg);
  }
  
  public void a(Location paramLocation)
    throws RemoteException
  {
    this.e.a(paramLocation);
  }
  
  public void a(GeofencingRequest paramGeofencingRequest, PendingIntent paramPendingIntent, zza.zzb<Status> paramzzb)
    throws RemoteException
  {
    u();
    zzx.a(paramGeofencingRequest, "geofencingRequest can't be null.");
    zzx.a(paramPendingIntent, "PendingIntent must be specified.");
    zzx.a(paramzzb, "ResultHolder not provided.");
    paramzzb = new zza(paramzzb);
    ((zzi)v()).a(paramGeofencingRequest, paramPendingIntent, paramzzb);
  }
  
  public void a(LocationCallback paramLocationCallback, zzg paramzzg)
    throws RemoteException
  {
    this.e.a(paramLocationCallback, paramzzg);
  }
  
  public void a(LocationListener paramLocationListener, zzg paramzzg)
    throws RemoteException
  {
    this.e.a(paramLocationListener, paramzzg);
  }
  
  public void a(LocationRequest paramLocationRequest, PendingIntent paramPendingIntent, zzg paramzzg)
    throws RemoteException
  {
    this.e.a(paramLocationRequest, paramPendingIntent, paramzzg);
  }
  
  public void a(LocationRequest paramLocationRequest, LocationListener paramLocationListener, Looper paramLooper, zzg paramzzg)
    throws RemoteException
  {
    synchronized (this.e)
    {
      this.e.a(paramLocationRequest, paramLocationListener, paramLooper, paramzzg);
      return;
    }
  }
  
  public void a(LocationSettingsRequest paramLocationSettingsRequest, zza.zzb<LocationSettingsResult> paramzzb, String paramString)
    throws RemoteException
  {
    boolean bool2 = true;
    u();
    if (paramLocationSettingsRequest != null)
    {
      bool1 = true;
      zzx.b(bool1, "locationSettingsRequest can't be null nor empty.");
      if (paramzzb == null) {
        break label67;
      }
    }
    label67:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      zzx.b(bool1, "listener can't be null.");
      paramzzb = new zzc(paramzzb);
      ((zzi)v()).a(paramLocationSettingsRequest, paramzzb, paramString);
      return;
      bool1 = false;
      break;
    }
  }
  
  public void a(LocationRequestInternal paramLocationRequestInternal, LocationCallback paramLocationCallback, Looper paramLooper, zzg paramzzg)
    throws RemoteException
  {
    synchronized (this.e)
    {
      this.e.a(paramLocationRequestInternal, paramLocationCallback, paramLooper, paramzzg);
      return;
    }
  }
  
  public void a(zzg paramzzg)
    throws RemoteException
  {
    this.e.a(paramzzg);
  }
  
  public void a(List<String> paramList, zza.zzb<Status> paramzzb)
    throws RemoteException
  {
    u();
    if ((paramList != null) && (paramList.size() > 0)) {}
    for (boolean bool = true;; bool = false)
    {
      zzx.b(bool, "geofenceRequestIds can't be null nor empty.");
      zzx.a(paramzzb, "ResultHolder not provided.");
      paramList = (String[])paramList.toArray(new String[0]);
      paramzzb = new zzb(paramzzb);
      ((zzi)v()).a(paramList, paramzzb, q().getPackageName());
      return;
    }
  }
  
  public void a(boolean paramBoolean)
    throws RemoteException
  {
    this.e.a(paramBoolean);
  }
  
  public void f()
  {
    synchronized (this.e)
    {
      boolean bool = k();
      if (bool) {}
      try
      {
        this.e.b();
        this.e.c();
        super.f();
        return;
      }
      catch (Exception localException)
      {
        for (;;)
        {
          Log.e("LocationClientImpl", "Client disconnected before listeners could be cleaned up", localException);
        }
      }
    }
  }
  
  public Location h()
  {
    return this.e.a();
  }
  
  private static final class zza
    extends zzh.zza
  {
    private zza.zzb<Status> a;
    
    public zza(zza.zzb<Status> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void a(int paramInt, PendingIntent paramPendingIntent)
    {
      Log.wtf("LocationClientImpl", "Unexpected call to onRemoveGeofencesByPendingIntentResult");
    }
    
    public void a(int paramInt, String[] paramArrayOfString)
    {
      if (this.a == null)
      {
        Log.wtf("LocationClientImpl", "onAddGeofenceResult called multiple times");
        return;
      }
      paramArrayOfString = LocationStatusCodes.b(LocationStatusCodes.a(paramInt));
      this.a.a(paramArrayOfString);
      this.a = null;
    }
    
    public void b(int paramInt, String[] paramArrayOfString)
    {
      Log.wtf("LocationClientImpl", "Unexpected call to onRemoveGeofencesByRequestIdsResult");
    }
  }
  
  private static final class zzb
    extends zzh.zza
  {
    private zza.zzb<Status> a;
    
    public zzb(zza.zzb<Status> paramzzb)
    {
      this.a = paramzzb;
    }
    
    private void a(int paramInt)
    {
      if (this.a == null)
      {
        Log.wtf("LocationClientImpl", "onRemoveGeofencesResult called multiple times");
        return;
      }
      Status localStatus = LocationStatusCodes.b(LocationStatusCodes.a(paramInt));
      this.a.a(localStatus);
      this.a = null;
    }
    
    public void a(int paramInt, PendingIntent paramPendingIntent)
    {
      a(paramInt);
    }
    
    public void a(int paramInt, String[] paramArrayOfString)
    {
      Log.wtf("LocationClientImpl", "Unexpected call to onAddGeofencesResult");
    }
    
    public void b(int paramInt, String[] paramArrayOfString)
    {
      a(paramInt);
    }
  }
  
  private static final class zzc
    extends zzj.zza
  {
    private zza.zzb<LocationSettingsResult> a;
    
    public zzc(zza.zzb<LocationSettingsResult> paramzzb)
    {
      if (paramzzb != null) {}
      for (boolean bool = true;; bool = false)
      {
        zzx.b(bool, "listener can't be null.");
        this.a = paramzzb;
        return;
      }
    }
    
    public void a(LocationSettingsResult paramLocationSettingsResult)
      throws RemoteException
    {
      this.a.a(paramLocationSettingsResult);
      this.a = null;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/internal/zzl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */