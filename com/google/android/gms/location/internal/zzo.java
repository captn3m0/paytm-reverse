package com.google.android.gms.location.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzo
  implements Parcelable.Creator<ParcelableGeofence>
{
  static void a(ParcelableGeofence paramParcelableGeofence, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramParcelableGeofence.f(), false);
    zzb.a(paramParcel, 1000, paramParcelableGeofence.a());
    zzb.a(paramParcel, 2, paramParcelableGeofence.g());
    zzb.a(paramParcel, 3, paramParcelableGeofence.b());
    zzb.a(paramParcel, 4, paramParcelableGeofence.c());
    zzb.a(paramParcel, 5, paramParcelableGeofence.d());
    zzb.a(paramParcel, 6, paramParcelableGeofence.e());
    zzb.a(paramParcel, 7, paramParcelableGeofence.h());
    zzb.a(paramParcel, 8, paramParcelableGeofence.i());
    zzb.a(paramParcel, 9, paramParcelableGeofence.j());
    zzb.a(paramParcel, paramInt);
  }
  
  public ParcelableGeofence a(Parcel paramParcel)
  {
    int n = zza.b(paramParcel);
    int m = 0;
    String str = null;
    int k = 0;
    short s = 0;
    double d2 = 0.0D;
    double d1 = 0.0D;
    float f = 0.0F;
    long l = 0L;
    int j = 0;
    int i = -1;
    while (paramParcel.dataPosition() < n)
    {
      int i1 = zza.a(paramParcel);
      switch (zza.a(i1))
      {
      default: 
        zza.b(paramParcel, i1);
        break;
      case 1: 
        str = zza.p(paramParcel, i1);
        break;
      case 1000: 
        m = zza.g(paramParcel, i1);
        break;
      case 2: 
        l = zza.i(paramParcel, i1);
        break;
      case 3: 
        s = zza.f(paramParcel, i1);
        break;
      case 4: 
        d2 = zza.n(paramParcel, i1);
        break;
      case 5: 
        d1 = zza.n(paramParcel, i1);
        break;
      case 6: 
        f = zza.l(paramParcel, i1);
        break;
      case 7: 
        k = zza.g(paramParcel, i1);
        break;
      case 8: 
        j = zza.g(paramParcel, i1);
        break;
      case 9: 
        i = zza.g(paramParcel, i1);
      }
    }
    if (paramParcel.dataPosition() != n) {
      throw new zza.zza("Overread allowed size end=" + n, paramParcel);
    }
    return new ParcelableGeofence(m, str, k, s, d2, d1, f, l, j, i);
  }
  
  public ParcelableGeofence[] a(int paramInt)
  {
    return new ParcelableGeofence[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/internal/zzo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */