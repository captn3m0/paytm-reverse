package com.google.android.gms.location.internal;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzn
  implements Parcelable.Creator<LocationRequestUpdateData>
{
  static void a(LocationRequestUpdateData paramLocationRequestUpdateData, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramLocationRequestUpdateData.a);
    zzb.a(paramParcel, 1000, paramLocationRequestUpdateData.a());
    zzb.a(paramParcel, 2, paramLocationRequestUpdateData.b, paramInt, false);
    zzb.a(paramParcel, 3, paramLocationRequestUpdateData.b(), false);
    zzb.a(paramParcel, 4, paramLocationRequestUpdateData.d, paramInt, false);
    zzb.a(paramParcel, 5, paramLocationRequestUpdateData.c(), false);
    zzb.a(paramParcel, 6, paramLocationRequestUpdateData.d(), false);
    zzb.a(paramParcel, i);
  }
  
  public LocationRequestUpdateData a(Parcel paramParcel)
  {
    IBinder localIBinder1 = null;
    int k = zza.b(paramParcel);
    int j = 0;
    int i = 1;
    IBinder localIBinder2 = null;
    PendingIntent localPendingIntent = null;
    IBinder localIBinder3 = null;
    LocationRequestInternal localLocationRequestInternal = null;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        i = zza.g(paramParcel, m);
        break;
      case 1000: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        localLocationRequestInternal = (LocationRequestInternal)zza.a(paramParcel, m, LocationRequestInternal.CREATOR);
        break;
      case 3: 
        localIBinder3 = zza.q(paramParcel, m);
        break;
      case 4: 
        localPendingIntent = (PendingIntent)zza.a(paramParcel, m, PendingIntent.CREATOR);
        break;
      case 5: 
        localIBinder2 = zza.q(paramParcel, m);
        break;
      case 6: 
        localIBinder1 = zza.q(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new LocationRequestUpdateData(j, i, localLocationRequestInternal, localIBinder3, localPendingIntent, localIBinder2, localIBinder1);
  }
  
  public LocationRequestUpdateData[] a(int paramInt)
  {
    return new LocationRequestUpdateData[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/internal/zzn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */