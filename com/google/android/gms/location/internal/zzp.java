package com.google.android.gms.location.internal;

import android.os.DeadObjectException;
import android.os.IInterface;

public abstract interface zzp<T extends IInterface>
{
  public abstract void a();
  
  public abstract T c()
    throws DeadObjectException;
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/internal/zzp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */