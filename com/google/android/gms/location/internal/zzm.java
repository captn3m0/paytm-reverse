package com.google.android.gms.location.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.location.LocationRequest;
import java.util.List;

public class zzm
  implements Parcelable.Creator<LocationRequestInternal>
{
  static void a(LocationRequestInternal paramLocationRequestInternal, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramLocationRequestInternal.b, paramInt, false);
    zzb.a(paramParcel, 1000, paramLocationRequestInternal.a());
    zzb.a(paramParcel, 2, paramLocationRequestInternal.c);
    zzb.a(paramParcel, 3, paramLocationRequestInternal.d);
    zzb.a(paramParcel, 4, paramLocationRequestInternal.e);
    zzb.c(paramParcel, 5, paramLocationRequestInternal.f, false);
    zzb.a(paramParcel, 6, paramLocationRequestInternal.g, false);
    zzb.a(paramParcel, 7, paramLocationRequestInternal.h);
    zzb.a(paramParcel, i);
  }
  
  public LocationRequestInternal a(Parcel paramParcel)
  {
    String str = null;
    boolean bool2 = true;
    boolean bool1 = false;
    int j = zza.b(paramParcel);
    Object localObject = LocationRequestInternal.a;
    boolean bool3 = true;
    boolean bool4 = false;
    LocationRequest localLocationRequest = null;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localLocationRequest = (LocationRequest)zza.a(paramParcel, k, LocationRequest.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        bool4 = zza.c(paramParcel, k);
        break;
      case 3: 
        bool3 = zza.c(paramParcel, k);
        break;
      case 4: 
        bool2 = zza.c(paramParcel, k);
        break;
      case 5: 
        localObject = zza.c(paramParcel, k, ClientIdentity.CREATOR);
        break;
      case 6: 
        str = zza.p(paramParcel, k);
        break;
      case 7: 
        bool1 = zza.c(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new LocationRequestInternal(i, localLocationRequest, bool4, bool3, bool2, (List)localObject, str, bool1);
  }
  
  public LocationRequestInternal[] a(int paramInt)
  {
    return new LocationRequestInternal[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/internal/zzm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */