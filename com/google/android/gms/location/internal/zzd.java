package com.google.android.gms.location.internal;

import android.app.PendingIntent;
import android.location.Location;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.zza.zzb;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationServices.zza;

public class zzd
  implements FusedLocationProviderApi
{
  public Location a(GoogleApiClient paramGoogleApiClient)
  {
    paramGoogleApiClient = LocationServices.a(paramGoogleApiClient);
    try
    {
      paramGoogleApiClient = paramGoogleApiClient.h();
      return paramGoogleApiClient;
    }
    catch (Exception paramGoogleApiClient) {}
    return null;
  }
  
  public PendingResult<Status> a(GoogleApiClient paramGoogleApiClient, final PendingIntent paramPendingIntent)
  {
    paramGoogleApiClient.b(new zza(paramGoogleApiClient)
    {
      protected void a(zzl paramAnonymouszzl)
        throws RemoteException
      {
        zzd.zzb localzzb = new zzd.zzb(this);
        paramAnonymouszzl.a(paramPendingIntent, localzzb);
      }
    });
  }
  
  public PendingResult<Status> a(GoogleApiClient paramGoogleApiClient, final LocationListener paramLocationListener)
  {
    paramGoogleApiClient.b(new zza(paramGoogleApiClient)
    {
      protected void a(zzl paramAnonymouszzl)
        throws RemoteException
      {
        zzd.zzb localzzb = new zzd.zzb(this);
        paramAnonymouszzl.a(paramLocationListener, localzzb);
      }
    });
  }
  
  public PendingResult<Status> a(GoogleApiClient paramGoogleApiClient, final LocationRequest paramLocationRequest, final PendingIntent paramPendingIntent)
  {
    paramGoogleApiClient.b(new zza(paramGoogleApiClient)
    {
      protected void a(zzl paramAnonymouszzl)
        throws RemoteException
      {
        zzd.zzb localzzb = new zzd.zzb(this);
        paramAnonymouszzl.a(paramLocationRequest, paramPendingIntent, localzzb);
      }
    });
  }
  
  public PendingResult<Status> a(GoogleApiClient paramGoogleApiClient, final LocationRequest paramLocationRequest, final LocationListener paramLocationListener)
  {
    paramGoogleApiClient.b(new zza(paramGoogleApiClient)
    {
      protected void a(zzl paramAnonymouszzl)
        throws RemoteException
      {
        zzd.zzb localzzb = new zzd.zzb(this);
        paramAnonymouszzl.a(paramLocationRequest, paramLocationListener, null, localzzb);
      }
    });
  }
  
  public PendingResult<Status> a(GoogleApiClient paramGoogleApiClient, final LocationRequest paramLocationRequest, final LocationListener paramLocationListener, final Looper paramLooper)
  {
    paramGoogleApiClient.b(new zza(paramGoogleApiClient)
    {
      protected void a(zzl paramAnonymouszzl)
        throws RemoteException
      {
        zzd.zzb localzzb = new zzd.zzb(this);
        paramAnonymouszzl.a(paramLocationRequest, paramLocationListener, paramLooper, localzzb);
      }
    });
  }
  
  private static abstract class zza
    extends LocationServices.zza<Status>
  {
    public zza(GoogleApiClient paramGoogleApiClient)
    {
      super();
    }
    
    public Status a(Status paramStatus)
    {
      return paramStatus;
    }
  }
  
  private static class zzb
    extends zzg.zza
  {
    private final zza.zzb<Status> a;
    
    public zzb(zza.zzb<Status> paramzzb)
    {
      this.a = paramzzb;
    }
    
    public void a(FusedLocationProviderResult paramFusedLocationProviderResult)
    {
      this.a.a(paramFusedLocationProviderResult.getStatus());
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/internal/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */