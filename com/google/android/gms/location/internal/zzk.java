package com.google.android.gms.location.internal;

import android.app.PendingIntent;
import android.content.ContentProviderClient;
import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.zzc;
import com.google.android.gms.location.zzc.zza;
import com.google.android.gms.location.zzd;
import com.google.android.gms.location.zzd.zza;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class zzk
{
  private final zzp<zzi> a;
  private final Context b;
  private ContentProviderClient c = null;
  private boolean d = false;
  private Map<LocationListener, zzc> e = new HashMap();
  private Map<LocationCallback, zza> f = new HashMap();
  
  public zzk(Context paramContext, zzp<zzi> paramzzp)
  {
    this.b = paramContext;
    this.a = paramzzp;
  }
  
  private zza a(LocationCallback paramLocationCallback, Looper paramLooper)
  {
    synchronized (this.f)
    {
      zza localzza2 = (zza)this.f.get(paramLocationCallback);
      zza localzza1 = localzza2;
      if (localzza2 == null) {
        localzza1 = new zza(paramLocationCallback, paramLooper);
      }
      this.f.put(paramLocationCallback, localzza1);
      return localzza1;
    }
  }
  
  private zzc a(LocationListener paramLocationListener, Looper paramLooper)
  {
    synchronized (this.e)
    {
      zzc localzzc2 = (zzc)this.e.get(paramLocationListener);
      zzc localzzc1 = localzzc2;
      if (localzzc2 == null) {
        localzzc1 = new zzc(paramLocationListener, paramLooper);
      }
      this.e.put(paramLocationListener, localzzc1);
      return localzzc1;
    }
  }
  
  public Location a()
  {
    this.a.a();
    try
    {
      Location localLocation = ((zzi)this.a.c()).b(this.b.getPackageName());
      return localLocation;
    }
    catch (RemoteException localRemoteException)
    {
      throw new IllegalStateException(localRemoteException);
    }
  }
  
  public void a(PendingIntent paramPendingIntent, zzg paramzzg)
    throws RemoteException
  {
    this.a.a();
    ((zzi)this.a.c()).a(LocationRequestUpdateData.a(paramPendingIntent, paramzzg));
  }
  
  public void a(Location paramLocation)
    throws RemoteException
  {
    this.a.a();
    ((zzi)this.a.c()).a(paramLocation);
  }
  
  public void a(LocationCallback paramLocationCallback, zzg paramzzg)
    throws RemoteException
  {
    this.a.a();
    zzx.a(paramLocationCallback, "Invalid null callback");
    synchronized (this.f)
    {
      paramLocationCallback = (zza)this.f.remove(paramLocationCallback);
      if (paramLocationCallback != null)
      {
        paramLocationCallback.a();
        ((zzi)this.a.c()).a(LocationRequestUpdateData.a(paramLocationCallback, paramzzg));
      }
      return;
    }
  }
  
  public void a(LocationListener paramLocationListener, zzg paramzzg)
    throws RemoteException
  {
    this.a.a();
    zzx.a(paramLocationListener, "Invalid null listener");
    synchronized (this.e)
    {
      paramLocationListener = (zzc)this.e.remove(paramLocationListener);
      if ((this.c != null) && (this.e.isEmpty()))
      {
        this.c.release();
        this.c = null;
      }
      if (paramLocationListener != null)
      {
        paramLocationListener.a();
        ((zzi)this.a.c()).a(LocationRequestUpdateData.a(paramLocationListener, paramzzg));
      }
      return;
    }
  }
  
  public void a(LocationRequest paramLocationRequest, PendingIntent paramPendingIntent, zzg paramzzg)
    throws RemoteException
  {
    this.a.a();
    ((zzi)this.a.c()).a(LocationRequestUpdateData.a(LocationRequestInternal.a(paramLocationRequest), paramPendingIntent, paramzzg));
  }
  
  public void a(LocationRequest paramLocationRequest, LocationListener paramLocationListener, Looper paramLooper, zzg paramzzg)
    throws RemoteException
  {
    this.a.a();
    paramLocationListener = a(paramLocationListener, paramLooper);
    ((zzi)this.a.c()).a(LocationRequestUpdateData.a(LocationRequestInternal.a(paramLocationRequest), paramLocationListener, paramzzg));
  }
  
  public void a(LocationRequestInternal paramLocationRequestInternal, LocationCallback paramLocationCallback, Looper paramLooper, zzg paramzzg)
    throws RemoteException
  {
    this.a.a();
    paramLocationCallback = a(paramLocationCallback, paramLooper);
    ((zzi)this.a.c()).a(LocationRequestUpdateData.a(paramLocationRequestInternal, paramLocationCallback, paramzzg));
  }
  
  public void a(zzg paramzzg)
    throws RemoteException
  {
    this.a.a();
    ((zzi)this.a.c()).a(paramzzg);
  }
  
  public void a(boolean paramBoolean)
    throws RemoteException
  {
    this.a.a();
    ((zzi)this.a.c()).a(paramBoolean);
    this.d = paramBoolean;
  }
  
  public void b()
  {
    Object localObject3;
    try
    {
      synchronized (this.e)
      {
        Iterator localIterator1 = this.e.values().iterator();
        while (localIterator1.hasNext())
        {
          localObject3 = (zzc)localIterator1.next();
          if (localObject3 != null) {
            ((zzi)this.a.c()).a(LocationRequestUpdateData.a((zzd)localObject3, null));
          }
        }
      }
      this.e.clear();
    }
    catch (RemoteException localRemoteException)
    {
      throw new IllegalStateException(localRemoteException);
    }
    synchronized (this.f)
    {
      Iterator localIterator2 = this.f.values().iterator();
      while (localIterator2.hasNext())
      {
        localObject3 = (zza)localIterator2.next();
        if (localObject3 != null) {
          ((zzi)this.a.c()).a(LocationRequestUpdateData.a((zzc)localObject3, null));
        }
      }
    }
    this.f.clear();
  }
  
  public void c()
  {
    if (this.d) {}
    try
    {
      a(false);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      throw new IllegalStateException(localRemoteException);
    }
  }
  
  private static class zza
    extends zzc.zza
  {
    private Handler a;
    
    zza(final LocationCallback paramLocationCallback, Looper paramLooper)
    {
      Looper localLooper = paramLooper;
      if (paramLooper == null)
      {
        localLooper = Looper.myLooper();
        if (localLooper == null) {
          break label45;
        }
      }
      label45:
      for (boolean bool = true;; bool = false)
      {
        zzx.a(bool, "Can't create handler inside thread that has not called Looper.prepare()");
        this.a = new Handler(localLooper)
        {
          public void handleMessage(Message paramAnonymousMessage)
          {
            switch (paramAnonymousMessage.what)
            {
            default: 
              return;
            case 0: 
              paramLocationCallback.a((LocationResult)paramAnonymousMessage.obj);
              return;
            }
            paramLocationCallback.a((LocationAvailability)paramAnonymousMessage.obj);
          }
        };
        return;
      }
    }
    
    private void a(int paramInt, Object paramObject)
    {
      if (this.a == null)
      {
        Log.e("LocationClientHelper", "Received a data in client after calling removeLocationUpdates.");
        return;
      }
      Message localMessage = Message.obtain();
      localMessage.what = paramInt;
      localMessage.obj = paramObject;
      this.a.sendMessage(localMessage);
    }
    
    public void a()
    {
      this.a = null;
    }
    
    public void a(LocationAvailability paramLocationAvailability)
    {
      a(1, paramLocationAvailability);
    }
    
    public void a(LocationResult paramLocationResult)
    {
      a(0, paramLocationResult);
    }
  }
  
  private static class zzb
    extends Handler
  {
    private final LocationListener a;
    
    public zzb(LocationListener paramLocationListener)
    {
      this.a = paramLocationListener;
    }
    
    public zzb(LocationListener paramLocationListener, Looper paramLooper)
    {
      super();
      this.a = paramLocationListener;
    }
    
    public void handleMessage(Message paramMessage)
    {
      switch (paramMessage.what)
      {
      default: 
        Log.e("LocationClientHelper", "unknown message in LocationHandler.handleMessage");
        return;
      }
      paramMessage = new Location((Location)paramMessage.obj);
      this.a.onLocationChanged(paramMessage);
    }
  }
  
  private static class zzc
    extends zzd.zza
  {
    private Handler a;
    
    zzc(LocationListener paramLocationListener, Looper paramLooper)
    {
      boolean bool;
      if (paramLooper == null)
      {
        if (Looper.myLooper() != null)
        {
          bool = true;
          zzx.a(bool, "Can't create handler inside thread that has not called Looper.prepare()");
        }
      }
      else {
        if (paramLooper != null) {
          break label46;
        }
      }
      label46:
      for (paramLocationListener = new zzk.zzb(paramLocationListener);; paramLocationListener = new zzk.zzb(paramLocationListener, paramLooper))
      {
        this.a = paramLocationListener;
        return;
        bool = false;
        break;
      }
    }
    
    public void a()
    {
      this.a = null;
    }
    
    public void a(Location paramLocation)
    {
      if (this.a == null)
      {
        Log.e("LocationClientHelper", "Received a location in client after calling removeLocationUpdates.");
        return;
      }
      Message localMessage = Message.obtain();
      localMessage.what = 1;
      localMessage.obj = paramLocation;
      this.a.sendMessage(localMessage);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/internal/zzk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */