package com.google.android.gms.location.internal;

import android.os.Parcel;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.google.android.gms.location.LocationRequest;
import java.util.Collections;
import java.util.List;

public class LocationRequestInternal
  implements SafeParcelable
{
  public static final zzm CREATOR = new zzm();
  static final List<ClientIdentity> a = ;
  LocationRequest b;
  boolean c;
  boolean d;
  boolean e;
  List<ClientIdentity> f;
  @Nullable
  String g;
  boolean h;
  private final int i;
  
  LocationRequestInternal(int paramInt, LocationRequest paramLocationRequest, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, List<ClientIdentity> paramList, @Nullable String paramString, boolean paramBoolean4)
  {
    this.i = paramInt;
    this.b = paramLocationRequest;
    this.c = paramBoolean1;
    this.d = paramBoolean2;
    this.e = paramBoolean3;
    this.f = paramList;
    this.g = paramString;
    this.h = paramBoolean4;
  }
  
  @Deprecated
  public static LocationRequestInternal a(LocationRequest paramLocationRequest)
  {
    return a(null, paramLocationRequest);
  }
  
  public static LocationRequestInternal a(@Nullable String paramString, LocationRequest paramLocationRequest)
  {
    return new LocationRequestInternal(1, paramLocationRequest, false, true, true, a, paramString, false);
  }
  
  int a()
  {
    return this.i;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof LocationRequestInternal)) {}
    do
    {
      return false;
      paramObject = (LocationRequestInternal)paramObject;
    } while ((!zzw.a(this.b, ((LocationRequestInternal)paramObject).b)) || (this.c != ((LocationRequestInternal)paramObject).c) || (this.d != ((LocationRequestInternal)paramObject).d) || (this.e != ((LocationRequestInternal)paramObject).e) || (this.h != ((LocationRequestInternal)paramObject).h) || (!zzw.a(this.f, ((LocationRequestInternal)paramObject).f)));
    return true;
  }
  
  public int hashCode()
  {
    return this.b.hashCode();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.b.toString());
    if (this.g != null) {
      localStringBuilder.append(" tag=").append(this.g);
    }
    localStringBuilder.append(" nlpDebug=").append(this.c);
    localStringBuilder.append(" trigger=").append(this.e);
    localStringBuilder.append(" restorePIListeners=").append(this.d);
    localStringBuilder.append(" hideAppOps=").append(this.h);
    localStringBuilder.append(" clients=").append(this.f);
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    zzm.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/internal/LocationRequestInternal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */