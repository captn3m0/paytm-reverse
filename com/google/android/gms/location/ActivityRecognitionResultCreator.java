package com.google.android.gms.location;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class ActivityRecognitionResultCreator
  implements Parcelable.Creator<ActivityRecognitionResult>
{
  static void a(ActivityRecognitionResult paramActivityRecognitionResult, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.c(paramParcel, 1, paramActivityRecognitionResult.a, false);
    zzb.a(paramParcel, 1000, paramActivityRecognitionResult.a());
    zzb.a(paramParcel, 2, paramActivityRecognitionResult.b);
    zzb.a(paramParcel, 3, paramActivityRecognitionResult.c);
    zzb.a(paramParcel, 4, paramActivityRecognitionResult.d);
    zzb.a(paramParcel, 5, paramActivityRecognitionResult.e, false);
    zzb.a(paramParcel, paramInt);
  }
  
  public ActivityRecognitionResult a(Parcel paramParcel)
  {
    long l1 = 0L;
    Bundle localBundle = null;
    int i = 0;
    int k = zza.b(paramParcel);
    long l2 = 0L;
    ArrayList localArrayList = null;
    int j = 0;
    while (paramParcel.dataPosition() < k)
    {
      int m = zza.a(paramParcel);
      switch (zza.a(m))
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        localArrayList = zza.c(paramParcel, m, DetectedActivity.CREATOR);
        break;
      case 1000: 
        j = zza.g(paramParcel, m);
        break;
      case 2: 
        l2 = zza.i(paramParcel, m);
        break;
      case 3: 
        l1 = zza.i(paramParcel, m);
        break;
      case 4: 
        i = zza.g(paramParcel, m);
        break;
      case 5: 
        localBundle = zza.r(paramParcel, m);
      }
    }
    if (paramParcel.dataPosition() != k) {
      throw new zza.zza("Overread allowed size end=" + k, paramParcel);
    }
    return new ActivityRecognitionResult(j, localArrayList, l2, l1, i, localBundle);
  }
  
  public ActivityRecognitionResult[] a(int paramInt)
  {
    return new ActivityRecognitionResult[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/ActivityRecognitionResultCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */