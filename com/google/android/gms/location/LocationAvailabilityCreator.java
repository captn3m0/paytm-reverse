package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class LocationAvailabilityCreator
  implements Parcelable.Creator<LocationAvailability>
{
  static void a(LocationAvailability paramLocationAvailability, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramLocationAvailability.a);
    zzb.a(paramParcel, 1000, paramLocationAvailability.b());
    zzb.a(paramParcel, 2, paramLocationAvailability.b);
    zzb.a(paramParcel, 3, paramLocationAvailability.c);
    zzb.a(paramParcel, 4, paramLocationAvailability.d);
    zzb.a(paramParcel, paramInt);
  }
  
  public LocationAvailability a(Parcel paramParcel)
  {
    int i = 1;
    int n = zza.b(paramParcel);
    int m = 0;
    int k = 1000;
    long l = 0L;
    int j = 1;
    while (paramParcel.dataPosition() < n)
    {
      int i1 = zza.a(paramParcel);
      switch (zza.a(i1))
      {
      default: 
        zza.b(paramParcel, i1);
        break;
      case 1: 
        j = zza.g(paramParcel, i1);
        break;
      case 1000: 
        m = zza.g(paramParcel, i1);
        break;
      case 2: 
        i = zza.g(paramParcel, i1);
        break;
      case 3: 
        l = zza.i(paramParcel, i1);
        break;
      case 4: 
        k = zza.g(paramParcel, i1);
      }
    }
    if (paramParcel.dataPosition() != n) {
      throw new zza.zza("Overread allowed size end=" + n, paramParcel);
    }
    return new LocationAvailability(m, k, j, i, l);
  }
  
  public LocationAvailability[] a(int paramInt)
  {
    return new LocationAvailability[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/LocationAvailabilityCreator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */