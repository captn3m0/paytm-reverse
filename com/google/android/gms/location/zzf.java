package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import java.util.ArrayList;

public class zzf
  implements Parcelable.Creator<LocationSettingsRequest>
{
  static void a(LocationSettingsRequest paramLocationSettingsRequest, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.c(paramParcel, 1, paramLocationSettingsRequest.b(), false);
    zzb.a(paramParcel, 1000, paramLocationSettingsRequest.a());
    zzb.a(paramParcel, 2, paramLocationSettingsRequest.c());
    zzb.a(paramParcel, 3, paramLocationSettingsRequest.d());
    zzb.a(paramParcel, paramInt);
  }
  
  public LocationSettingsRequest a(Parcel paramParcel)
  {
    boolean bool2 = false;
    int j = zza.b(paramParcel);
    ArrayList localArrayList = null;
    boolean bool1 = false;
    int i = 0;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        localArrayList = zza.c(paramParcel, k, LocationRequest.CREATOR);
        break;
      case 1000: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        bool1 = zza.c(paramParcel, k);
        break;
      case 3: 
        bool2 = zza.c(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new LocationSettingsRequest(i, localArrayList, bool1, bool2);
  }
  
  public LocationSettingsRequest[] a(int paramInt)
  {
    return new LocationSettingsRequest[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */