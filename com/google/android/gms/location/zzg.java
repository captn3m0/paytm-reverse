package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzg
  implements Parcelable.Creator<LocationSettingsResult>
{
  static void a(LocationSettingsResult paramLocationSettingsResult, Parcel paramParcel, int paramInt)
  {
    int i = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramLocationSettingsResult.getStatus(), paramInt, false);
    zzb.a(paramParcel, 1000, paramLocationSettingsResult.a());
    zzb.a(paramParcel, 2, paramLocationSettingsResult.b(), paramInt, false);
    zzb.a(paramParcel, i);
  }
  
  public LocationSettingsResult a(Parcel paramParcel)
  {
    LocationSettingsStates localLocationSettingsStates = null;
    int j = zza.b(paramParcel);
    int i = 0;
    Status localStatus = null;
    if (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
      }
      for (;;)
      {
        break;
        localStatus = (Status)zza.a(paramParcel, k, Status.CREATOR);
        continue;
        i = zza.g(paramParcel, k);
        continue;
        localLocationSettingsStates = (LocationSettingsStates)zza.a(paramParcel, k, LocationSettingsStates.CREATOR);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new LocationSettingsResult(i, localStatus, localLocationSettingsStates);
  }
  
  public LocationSettingsResult[] a(int paramInt)
  {
    return new LocationSettingsResult[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/location/zzg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */