package com.google.android.gms.playlog.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzsu;
import java.util.ArrayList;
import java.util.Iterator;

public class zzf
  extends zzj<zza>
{
  private final String a;
  private final zzd e;
  private final zzb f;
  private final Object g;
  private boolean h;
  
  public zzf(Context paramContext, Looper paramLooper, zzd paramzzd, com.google.android.gms.common.internal.zzf paramzzf)
  {
    super(paramContext, paramLooper, 24, paramzzf, paramzzd, paramzzd);
    this.a = paramContext.getPackageName();
    this.e = ((zzd)zzx.a(paramzzd));
    this.e.a(this);
    this.f = new zzb();
    this.g = new Object();
    this.h = true;
  }
  
  private void b(PlayLoggerContext paramPlayLoggerContext, LogEvent paramLogEvent)
  {
    this.f.a(paramPlayLoggerContext, paramLogEvent);
  }
  
  private void c(PlayLoggerContext paramPlayLoggerContext, LogEvent paramLogEvent)
  {
    try
    {
      j();
      ((zza)v()).a(this.a, paramPlayLoggerContext, paramLogEvent);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      Log.e("PlayLoggerImpl", "Couldn't send log event.  Will try caching.");
      b(paramPlayLoggerContext, paramLogEvent);
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      Log.e("PlayLoggerImpl", "Service was disconnected.  Will try caching.");
      b(paramPlayLoggerContext, paramLogEvent);
    }
  }
  
  private void j()
  {
    boolean bool;
    if (!this.h)
    {
      bool = true;
      com.google.android.gms.common.internal.zzb.a(bool);
      if (!this.f.e()) {
        Object localObject = null;
      }
    }
    label122:
    label195:
    label228:
    for (;;)
    {
      ArrayList localArrayList;
      zzb.zza localzza;
      try
      {
        localArrayList = new ArrayList();
        Iterator localIterator = this.f.a().iterator();
        if (!localIterator.hasNext()) {
          break label195;
        }
        localzza = (zzb.zza)localIterator.next();
        if (localzza.c == null) {
          break label122;
        }
        ((zza)v()).a(this.a, localzza.a, zzsu.a(localzza.c));
        continue;
        return;
      }
      catch (RemoteException localRemoteException)
      {
        Log.e("PlayLoggerImpl", "Couldn't send cached log events to AndroidLog service.  Retaining in memory cache.");
      }
      bool = false;
      break;
      if (localzza.a.equals(localRemoteException))
      {
        localArrayList.add(localzza.b);
      }
      else
      {
        if (!localArrayList.isEmpty())
        {
          ((zza)v()).a(this.a, localRemoteException, localArrayList);
          localArrayList.clear();
        }
        PlayLoggerContext localPlayLoggerContext = localzza.a;
        localArrayList.add(localzza.b);
        break label228;
        if (!localArrayList.isEmpty()) {
          ((zza)v()).a(this.a, localPlayLoggerContext, localArrayList);
        }
        this.f.b();
        return;
      }
    }
  }
  
  protected zza a(IBinder paramIBinder)
  {
    return zza.zza.a(paramIBinder);
  }
  
  protected String a()
  {
    return "com.google.android.gms.playlog.service.START";
  }
  
  public void a(PlayLoggerContext paramPlayLoggerContext, LogEvent paramLogEvent)
  {
    synchronized (this.g)
    {
      if (this.h)
      {
        b(paramPlayLoggerContext, paramLogEvent);
        return;
      }
      c(paramPlayLoggerContext, paramLogEvent);
    }
  }
  
  void a(boolean paramBoolean)
  {
    synchronized (this.g)
    {
      boolean bool = this.h;
      this.h = paramBoolean;
      if ((bool) && (!this.h)) {
        j();
      }
      return;
    }
  }
  
  protected String b()
  {
    return "com.google.android.gms.playlog.internal.IPlayLogService";
  }
  
  public void h()
  {
    synchronized (this.g)
    {
      if ((p()) || (k())) {
        return;
      }
      this.e.a(true);
      o();
      return;
    }
  }
  
  public void i()
  {
    synchronized (this.g)
    {
      this.e.a(false);
      f();
      return;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/playlog/internal/zzf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */