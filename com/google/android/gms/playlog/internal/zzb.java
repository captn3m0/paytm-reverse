package com.google.android.gms.playlog.internal;

import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzsz.zzd;
import java.util.ArrayList;

public class zzb
{
  private final ArrayList<zza> a = new ArrayList();
  private int b;
  
  public zzb()
  {
    this(100);
  }
  
  public zzb(int paramInt)
  {
    this.b = paramInt;
  }
  
  private void f()
  {
    while (c() > d()) {
      this.a.remove(0);
    }
  }
  
  public ArrayList<zza> a()
  {
    return this.a;
  }
  
  public void a(PlayLoggerContext paramPlayLoggerContext, LogEvent paramLogEvent)
  {
    this.a.add(new zza(paramPlayLoggerContext, paramLogEvent, null));
    f();
  }
  
  public void b()
  {
    this.a.clear();
  }
  
  public int c()
  {
    return this.a.size();
  }
  
  public int d()
  {
    return this.b;
  }
  
  public boolean e()
  {
    return this.a.isEmpty();
  }
  
  public static class zza
  {
    public final PlayLoggerContext a;
    public final LogEvent b;
    public final zzsz.zzd c;
    
    private zza(PlayLoggerContext paramPlayLoggerContext, LogEvent paramLogEvent)
    {
      this.a = ((PlayLoggerContext)zzx.a(paramPlayLoggerContext));
      this.b = ((LogEvent)zzx.a(paramLogEvent));
      this.c = null;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/playlog/internal/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */