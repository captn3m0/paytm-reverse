package com.google.android.gms.playlog.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzc
  implements Parcelable.Creator<LogEvent>
{
  static void a(LogEvent paramLogEvent, Parcel paramParcel, int paramInt)
  {
    paramInt = zzb.a(paramParcel);
    zzb.a(paramParcel, 1, paramLogEvent.a);
    zzb.a(paramParcel, 2, paramLogEvent.b);
    zzb.a(paramParcel, 3, paramLogEvent.d, false);
    zzb.a(paramParcel, 4, paramLogEvent.e, false);
    zzb.a(paramParcel, 5, paramLogEvent.f, false);
    zzb.a(paramParcel, 6, paramLogEvent.c);
    zzb.a(paramParcel, paramInt);
  }
  
  public LogEvent a(Parcel paramParcel)
  {
    long l1 = 0L;
    Bundle localBundle = null;
    int j = zza.b(paramParcel);
    int i = 0;
    byte[] arrayOfByte = null;
    String str = null;
    long l2 = 0L;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        l2 = zza.i(paramParcel, k);
        break;
      case 3: 
        str = zza.p(paramParcel, k);
        break;
      case 4: 
        arrayOfByte = zza.s(paramParcel, k);
        break;
      case 5: 
        localBundle = zza.r(paramParcel, k);
        break;
      case 6: 
        l1 = zza.i(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new LogEvent(i, l2, l1, str, arrayOfByte, localBundle);
  }
  
  public LogEvent[] a(int paramInt)
  {
    return new LogEvent[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/playlog/internal/zzc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */