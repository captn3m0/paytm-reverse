package com.google.android.gms.playlog.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.internal.zzqu.zza;

public class zzd
  implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
{
  private final zzqu.zza a;
  private zzf b;
  private boolean c;
  
  public zzd(zzqu.zza paramzza)
  {
    this.a = paramzza;
    this.b = null;
    this.c = true;
  }
  
  public void a(zzf paramzzf)
  {
    this.b = paramzzf;
  }
  
  public void a(boolean paramBoolean)
  {
    this.c = paramBoolean;
  }
  
  public void onConnected(Bundle paramBundle)
  {
    this.b.a(false);
    if ((this.c) && (this.a != null)) {
      this.a.b();
    }
    this.c = false;
  }
  
  public void onConnectionFailed(ConnectionResult paramConnectionResult)
  {
    this.b.a(true);
    if ((this.c) && (this.a != null))
    {
      if (!paramConnectionResult.a()) {
        break label48;
      }
      this.a.a(paramConnectionResult.d());
    }
    for (;;)
    {
      this.c = false;
      return;
      label48:
      this.a.c();
    }
  }
  
  public void onConnectionSuspended(int paramInt)
  {
    this.b.a(true);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/playlog/internal/zzd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */