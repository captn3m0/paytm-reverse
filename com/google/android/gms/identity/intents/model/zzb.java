package com.google.android.gms.identity.intents.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class zzb
  implements Parcelable.Creator<UserAddress>
{
  static void a(UserAddress paramUserAddress, Parcel paramParcel, int paramInt)
  {
    paramInt = com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 1, paramUserAddress.a());
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 2, paramUserAddress.a, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 3, paramUserAddress.b, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 4, paramUserAddress.c, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 5, paramUserAddress.d, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 6, paramUserAddress.e, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 7, paramUserAddress.f, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 8, paramUserAddress.g, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 9, paramUserAddress.h, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 10, paramUserAddress.i, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 11, paramUserAddress.j, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 12, paramUserAddress.k, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 13, paramUserAddress.l, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 14, paramUserAddress.m);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 15, paramUserAddress.n, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, 16, paramUserAddress.o, false);
    com.google.android.gms.common.internal.safeparcel.zzb.a(paramParcel, paramInt);
  }
  
  public UserAddress a(Parcel paramParcel)
  {
    int j = zza.b(paramParcel);
    int i = 0;
    String str14 = null;
    String str13 = null;
    String str12 = null;
    String str11 = null;
    String str10 = null;
    String str9 = null;
    String str8 = null;
    String str7 = null;
    String str6 = null;
    String str5 = null;
    String str4 = null;
    String str3 = null;
    boolean bool = false;
    String str2 = null;
    String str1 = null;
    while (paramParcel.dataPosition() < j)
    {
      int k = zza.a(paramParcel);
      switch (zza.a(k))
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        i = zza.g(paramParcel, k);
        break;
      case 2: 
        str14 = zza.p(paramParcel, k);
        break;
      case 3: 
        str13 = zza.p(paramParcel, k);
        break;
      case 4: 
        str12 = zza.p(paramParcel, k);
        break;
      case 5: 
        str11 = zza.p(paramParcel, k);
        break;
      case 6: 
        str10 = zza.p(paramParcel, k);
        break;
      case 7: 
        str9 = zza.p(paramParcel, k);
        break;
      case 8: 
        str8 = zza.p(paramParcel, k);
        break;
      case 9: 
        str7 = zza.p(paramParcel, k);
        break;
      case 10: 
        str6 = zza.p(paramParcel, k);
        break;
      case 11: 
        str5 = zza.p(paramParcel, k);
        break;
      case 12: 
        str4 = zza.p(paramParcel, k);
        break;
      case 13: 
        str3 = zza.p(paramParcel, k);
        break;
      case 14: 
        bool = zza.c(paramParcel, k);
        break;
      case 15: 
        str2 = zza.p(paramParcel, k);
        break;
      case 16: 
        str1 = zza.p(paramParcel, k);
      }
    }
    if (paramParcel.dataPosition() != j) {
      throw new zza.zza("Overread allowed size end=" + j, paramParcel);
    }
    return new UserAddress(i, str14, str13, str12, str11, str10, str9, str8, str7, str6, str5, str4, str3, bool, str2, str1);
  }
  
  public UserAddress[] a(int paramInt)
  {
    return new UserAddress[paramInt];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/identity/intents/model/zzb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */