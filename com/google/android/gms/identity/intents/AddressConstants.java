package com.google.android.gms.identity.intents;

public abstract interface AddressConstants
{
  public static abstract interface ErrorCodes {}
  
  public static abstract interface Extras {}
  
  public static abstract interface ResultCodes {}
  
  public static abstract interface Themes {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/gms/identity/intents/AddressConstants.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */