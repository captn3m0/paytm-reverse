package com.google.android.a;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

class a
  implements ServiceConnection
{
  private boolean a = false;
  private AtomicBoolean b = new AtomicBoolean(false);
  private final BlockingQueue<IBinder> c = new LinkedBlockingQueue();
  
  public IBinder a()
    throws InterruptedException
  {
    if (this.b.get()) {
      throw new IllegalStateException();
    }
    this.b.set(true);
    return (IBinder)this.c.take();
  }
  
  public void a(final Context paramContext)
  {
    paramContext = new Runnable()
    {
      public void run()
      {
        if (a.a(a.this))
        {
          paramContext.unbindService(a.this);
          return;
        }
        Log.w("BlockingServiceConnection", "Service disconnected before unbinding");
      }
    };
    new Handler(Looper.getMainLooper()).post(paramContext);
  }
  
  public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    this.a = true;
    this.c.clear();
    this.c.add(paramIBinder);
  }
  
  public void onServiceDisconnected(ComponentName paramComponentName)
  {
    this.a = false;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */