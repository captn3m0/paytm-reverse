package com.google.android.a;

import android.content.Intent;
import android.os.Looper;

public class c
{
  static boolean a;
  private static final Intent b = new Intent("com.google.android.now.NOW_AUTH_SERVICE").setPackage("com.google.android.googlequicksearchbox");
  
  /* Error */
  public static String a(android.content.Context paramContext, String paramString)
    throws java.io.IOException, c.d, c.c, c.b, c.a
  {
    // Byte code:
    //   0: invokestatic 46	com/google/android/a/c:a	()V
    //   3: new 48	com/google/android/a/a
    //   6: dup
    //   7: invokespecial 50	com/google/android/a/a:<init>	()V
    //   10: astore_3
    //   11: aload_0
    //   12: getstatic 36	com/google/android/a/c:b	Landroid/content/Intent;
    //   15: aload_3
    //   16: iconst_1
    //   17: invokevirtual 56	android/content/Context:bindService	(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    //   20: ifeq +238 -> 258
    //   23: aload_3
    //   24: invokevirtual 59	com/google/android/a/a:a	()Landroid/os/IBinder;
    //   27: invokestatic 64	com/google/android/a/b$a:a	(Landroid/os/IBinder;)Lcom/google/android/a/b;
    //   30: aload_1
    //   31: aload_0
    //   32: invokevirtual 68	android/content/Context:getPackageName	()Ljava/lang/String;
    //   35: invokeinterface 73 3 0
    //   40: astore_1
    //   41: aload_1
    //   42: ifnonnull +33 -> 75
    //   45: new 40	java/io/IOException
    //   48: dup
    //   49: ldc 75
    //   51: invokespecial 76	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   54: athrow
    //   55: astore_1
    //   56: new 40	java/io/IOException
    //   59: dup
    //   60: ldc 78
    //   62: aload_1
    //   63: invokespecial 81	java/io/IOException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   66: athrow
    //   67: astore_1
    //   68: aload_3
    //   69: aload_0
    //   70: invokevirtual 84	com/google/android/a/a:a	(Landroid/content/Context;)V
    //   73: aload_1
    //   74: athrow
    //   75: aload_1
    //   76: ldc 86
    //   78: invokevirtual 92	android/os/Bundle:containsKey	(Ljava/lang/String;)Z
    //   81: ifeq +163 -> 244
    //   84: aload_1
    //   85: ldc 86
    //   87: invokevirtual 96	android/os/Bundle:getInt	(Ljava/lang/String;)I
    //   90: istore_2
    //   91: iload_2
    //   92: tableswitch	default:+176->268, 0:+108->200, 1:+116->208, 2:+130->222, 3:+144->236
    //   124: ldc 98
    //   126: new 100	java/lang/StringBuilder
    //   129: dup
    //   130: bipush 26
    //   132: invokespecial 103	java/lang/StringBuilder:<init>	(I)V
    //   135: ldc 105
    //   137: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   140: iload_2
    //   141: invokevirtual 112	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   144: invokevirtual 115	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   147: invokestatic 121	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   150: pop
    //   151: new 40	java/io/IOException
    //   154: dup
    //   155: new 100	java/lang/StringBuilder
    //   158: dup
    //   159: bipush 49
    //   161: invokespecial 103	java/lang/StringBuilder:<init>	(I)V
    //   164: ldc 123
    //   166: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   169: iload_2
    //   170: invokevirtual 112	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   173: invokevirtual 115	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   176: invokespecial 76	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   179: athrow
    //   180: astore_1
    //   181: ldc 98
    //   183: ldc 125
    //   185: aload_1
    //   186: invokestatic 129	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   189: pop
    //   190: new 131	java/io/InterruptedIOException
    //   193: dup
    //   194: ldc -123
    //   196: invokespecial 134	java/io/InterruptedIOException:<init>	(Ljava/lang/String;)V
    //   199: athrow
    //   200: new 15	com/google/android/a/c$d
    //   203: dup
    //   204: invokespecial 135	com/google/android/a/c$d:<init>	()V
    //   207: athrow
    //   208: new 12	com/google/android/a/c$c
    //   211: dup
    //   212: aload_1
    //   213: ldc -119
    //   215: invokevirtual 141	android/os/Bundle:getLong	(Ljava/lang/String;)J
    //   218: invokespecial 144	com/google/android/a/c$c:<init>	(J)V
    //   221: athrow
    //   222: new 9	com/google/android/a/c$b
    //   225: dup
    //   226: aload_1
    //   227: ldc -110
    //   229: invokevirtual 150	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   232: invokespecial 151	com/google/android/a/c$b:<init>	(Ljava/lang/String;)V
    //   235: athrow
    //   236: new 6	com/google/android/a/c$a
    //   239: dup
    //   240: invokespecial 152	com/google/android/a/c$a:<init>	()V
    //   243: athrow
    //   244: aload_1
    //   245: ldc -102
    //   247: invokevirtual 150	android/os/Bundle:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   250: astore_1
    //   251: aload_3
    //   252: aload_0
    //   253: invokevirtual 84	com/google/android/a/a:a	(Landroid/content/Context;)V
    //   256: aload_1
    //   257: areturn
    //   258: new 40	java/io/IOException
    //   261: dup
    //   262: ldc -100
    //   264: invokespecial 76	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   267: athrow
    //   268: goto -144 -> 124
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	271	0	paramContext	android.content.Context
    //   0	271	1	paramString	String
    //   90	80	2	i	int
    //   10	242	3	locala	a
    // Exception table:
    //   from	to	target	type
    //   23	41	55	android/os/RemoteException
    //   45	55	55	android/os/RemoteException
    //   75	91	55	android/os/RemoteException
    //   124	180	55	android/os/RemoteException
    //   200	208	55	android/os/RemoteException
    //   208	222	55	android/os/RemoteException
    //   222	236	55	android/os/RemoteException
    //   236	244	55	android/os/RemoteException
    //   244	251	55	android/os/RemoteException
    //   23	41	67	finally
    //   45	55	67	finally
    //   56	67	67	finally
    //   75	91	67	finally
    //   124	180	67	finally
    //   181	200	67	finally
    //   200	208	67	finally
    //   208	222	67	finally
    //   222	236	67	finally
    //   236	244	67	finally
    //   244	251	67	finally
    //   23	41	180	java/lang/InterruptedException
    //   45	55	180	java/lang/InterruptedException
    //   75	91	180	java/lang/InterruptedException
    //   124	180	180	java/lang/InterruptedException
    //   200	208	180	java/lang/InterruptedException
    //   208	222	180	java/lang/InterruptedException
    //   222	236	180	java/lang/InterruptedException
    //   236	244	180	java/lang/InterruptedException
    //   244	251	180	java/lang/InterruptedException
  }
  
  private static void a()
  {
    if ((Looper.myLooper() == Looper.getMainLooper()) && (!a)) {
      throw new IllegalStateException("Cannot call this API from the main thread");
    }
  }
  
  public static class a
    extends Exception
  {}
  
  public static class b
    extends Exception
  {
    private final String a;
    
    public b(String paramString)
    {
      this.a = paramString;
    }
    
    public String a()
    {
      return this.a;
    }
  }
  
  public static class c
    extends Exception
  {
    private final long a;
    
    public c(long paramLong)
    {
      this.a = paramLong;
    }
  }
  
  public static class d
    extends Exception
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/android/a/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */