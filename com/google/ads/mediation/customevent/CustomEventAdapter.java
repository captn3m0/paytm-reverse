package com.google.ads.mediation.customevent;

import android.app.Activity;
import android.view.View;
import com.google.ads.a.a;
import com.google.ads.mediation.g;
import com.google.android.gms.ads.mediation.customevent.CustomEventExtras;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.internal.zzin;

@KeepName
public final class CustomEventAdapter
  implements com.google.ads.mediation.d<CustomEventExtras, f>, com.google.ads.mediation.f<CustomEventExtras, f>
{
  b a;
  d b;
  private View c;
  
  private static <T> T a(String paramString)
  {
    try
    {
      Object localObject = Class.forName(paramString).newInstance();
      return (T)localObject;
    }
    catch (Throwable localThrowable)
    {
      zzin.d("Could not instantiate custom event adapter: " + paramString + ". " + localThrowable.getMessage());
    }
    return null;
  }
  
  b a(g paramg)
  {
    return new b(this, paramg);
  }
  
  public void a()
  {
    if (this.a != null) {
      this.a.a();
    }
    if (this.b != null) {
      this.b.a();
    }
  }
  
  public void a(com.google.ads.mediation.e parame, Activity paramActivity, f paramf, com.google.ads.b paramb, com.google.ads.mediation.b paramb1, CustomEventExtras paramCustomEventExtras)
  {
    this.a = ((b)a(paramf.b));
    if (this.a == null)
    {
      parame.a(this, a.a.d);
      return;
    }
    if (paramCustomEventExtras == null) {}
    for (paramCustomEventExtras = null;; paramCustomEventExtras = paramCustomEventExtras.a(paramf.a))
    {
      this.a.a(new a(this, parame), paramActivity, paramf.a, paramf.c, paramb, paramb1, paramCustomEventExtras);
      return;
    }
  }
  
  public void a(g paramg, Activity paramActivity, f paramf, com.google.ads.mediation.b paramb, CustomEventExtras paramCustomEventExtras)
  {
    this.b = ((d)a(paramf.b));
    if (this.b == null)
    {
      paramg.a(this, a.a.d);
      return;
    }
    if (paramCustomEventExtras == null) {}
    for (paramCustomEventExtras = null;; paramCustomEventExtras = paramCustomEventExtras.a(paramf.a))
    {
      this.b.a(a(paramg), paramActivity, paramf.a, paramf.c, paramb, paramCustomEventExtras);
      return;
    }
  }
  
  public Class<CustomEventExtras> b()
  {
    return CustomEventExtras.class;
  }
  
  public Class<f> c()
  {
    return f.class;
  }
  
  public View d()
  {
    return this.c;
  }
  
  public void e()
  {
    this.b.b();
  }
  
  static final class a
    implements c
  {
    private final CustomEventAdapter a;
    private final com.google.ads.mediation.e b;
    
    public a(CustomEventAdapter paramCustomEventAdapter, com.google.ads.mediation.e parame)
    {
      this.a = paramCustomEventAdapter;
      this.b = parame;
    }
    
    public void onClick()
    {
      zzin.a("Custom event adapter called onFailedToReceiveAd.");
      this.b.onClick(this.a);
    }
  }
  
  class b
    implements e
  {
    private final CustomEventAdapter b;
    private final g c;
    
    public b(CustomEventAdapter paramCustomEventAdapter, g paramg)
    {
      this.b = paramCustomEventAdapter;
      this.c = paramg;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/ads/mediation/customevent/CustomEventAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */