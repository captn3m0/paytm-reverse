package com.google.ads.a.a;

import com.google.android.gms.internal.zzsm;
import com.google.android.gms.internal.zzsn;
import com.google.android.gms.internal.zzsu;
import com.google.android.gms.internal.zzsx;
import java.io.IOException;

public abstract interface a
{
  public static final class a
    extends zzsu
  {
    public String a;
    
    public a()
    {
      a();
    }
    
    public a a()
    {
      this.a = "";
      this.S = -1;
      return this;
    }
    
    public a a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (zzsx.a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 10: 
          this.a = paramzzsm.i();
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (!this.a.equals("")) {
        paramzzsn.a(1, this.a);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b();
      int i = j;
      if (!this.a.equals("")) {
        i = j + zzsn.b(1, this.a);
      }
      return i;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/ads/a/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */