package com.google.ads.a.a;

import com.google.android.gms.internal.zzsm;
import com.google.android.gms.internal.zzsn;
import com.google.android.gms.internal.zzss;
import com.google.android.gms.internal.zzsu;
import com.google.android.gms.internal.zzsx;
import java.io.IOException;

public abstract interface b
{
  public static final class a
    extends zzsu
  {
    public Long A;
    public String B;
    public Long C;
    public Long D;
    public Long E;
    public b.b F;
    public Long G;
    public Long H;
    public Long I;
    public Long J;
    public a[] K;
    public Long L;
    public String M;
    public Integer N;
    public Boolean O;
    public String P;
    public Long Q;
    public b.c R;
    public String a;
    public String b;
    public Long c;
    public Long d;
    public Long e;
    public Long f;
    public Long g;
    public Long h;
    public Long i;
    public Long j;
    public Long k;
    public Long l;
    public String m;
    public Long n;
    public Long o;
    public Long p;
    public Long q;
    public Long r;
    public Long s;
    public Long t;
    public Long u;
    public Long v;
    public String w;
    public String x;
    public Long y;
    public Long z;
    
    public a()
    {
      a();
    }
    
    public a a()
    {
      this.a = null;
      this.b = null;
      this.c = null;
      this.d = null;
      this.e = null;
      this.f = null;
      this.g = null;
      this.h = null;
      this.i = null;
      this.j = null;
      this.k = null;
      this.l = null;
      this.m = null;
      this.n = null;
      this.o = null;
      this.p = null;
      this.q = null;
      this.r = null;
      this.s = null;
      this.t = null;
      this.u = null;
      this.v = null;
      this.w = null;
      this.x = null;
      this.y = null;
      this.z = null;
      this.A = null;
      this.B = null;
      this.C = null;
      this.D = null;
      this.E = null;
      this.F = null;
      this.G = null;
      this.H = null;
      this.I = null;
      this.J = null;
      this.K = a.a();
      this.L = null;
      this.M = null;
      this.N = null;
      this.O = null;
      this.P = null;
      this.Q = null;
      this.R = null;
      this.S = -1;
      return this;
    }
    
    public a a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i1 = paramzzsm.a();
        switch (i1)
        {
        default: 
          if (zzsx.a(paramzzsm, i1)) {}
          break;
        case 0: 
          return this;
        case 10: 
          this.a = paramzzsm.i();
          break;
        case 18: 
          this.b = paramzzsm.i();
          break;
        case 24: 
          this.c = Long.valueOf(paramzzsm.f());
          break;
        case 32: 
          this.d = Long.valueOf(paramzzsm.f());
          break;
        case 40: 
          this.e = Long.valueOf(paramzzsm.f());
          break;
        case 48: 
          this.f = Long.valueOf(paramzzsm.f());
          break;
        case 56: 
          this.g = Long.valueOf(paramzzsm.f());
          break;
        case 64: 
          this.h = Long.valueOf(paramzzsm.f());
          break;
        case 72: 
          this.i = Long.valueOf(paramzzsm.f());
          break;
        case 80: 
          this.j = Long.valueOf(paramzzsm.f());
          break;
        case 88: 
          this.k = Long.valueOf(paramzzsm.f());
          break;
        case 96: 
          this.l = Long.valueOf(paramzzsm.f());
          break;
        case 106: 
          this.m = paramzzsm.i();
          break;
        case 112: 
          this.n = Long.valueOf(paramzzsm.f());
          break;
        case 120: 
          this.o = Long.valueOf(paramzzsm.f());
          break;
        case 128: 
          this.p = Long.valueOf(paramzzsm.f());
          break;
        case 136: 
          this.q = Long.valueOf(paramzzsm.f());
          break;
        case 144: 
          this.r = Long.valueOf(paramzzsm.f());
          break;
        case 152: 
          this.s = Long.valueOf(paramzzsm.f());
          break;
        case 160: 
          this.t = Long.valueOf(paramzzsm.f());
          break;
        case 168: 
          this.L = Long.valueOf(paramzzsm.f());
          break;
        case 176: 
          this.u = Long.valueOf(paramzzsm.f());
          break;
        case 184: 
          this.v = Long.valueOf(paramzzsm.f());
          break;
        case 194: 
          this.M = paramzzsm.i();
          break;
        case 200: 
          this.Q = Long.valueOf(paramzzsm.f());
          break;
        case 208: 
          i1 = paramzzsm.g();
          switch (i1)
          {
          default: 
            break;
          case 0: 
          case 1: 
          case 2: 
          case 3: 
          case 4: 
          case 5: 
          case 6: 
            this.N = Integer.valueOf(i1);
          }
          break;
        case 218: 
          this.w = paramzzsm.i();
          break;
        case 224: 
          this.O = Boolean.valueOf(paramzzsm.h());
          break;
        case 234: 
          this.x = paramzzsm.i();
          break;
        case 242: 
          this.P = paramzzsm.i();
          break;
        case 248: 
          this.y = Long.valueOf(paramzzsm.f());
          break;
        case 256: 
          this.z = Long.valueOf(paramzzsm.f());
          break;
        case 264: 
          this.A = Long.valueOf(paramzzsm.f());
          break;
        case 274: 
          this.B = paramzzsm.i();
          break;
        case 280: 
          this.C = Long.valueOf(paramzzsm.f());
          break;
        case 288: 
          this.D = Long.valueOf(paramzzsm.f());
          break;
        case 296: 
          this.E = Long.valueOf(paramzzsm.f());
          break;
        case 306: 
          if (this.F == null) {
            this.F = new b.b();
          }
          paramzzsm.a(this.F);
          break;
        case 312: 
          this.G = Long.valueOf(paramzzsm.f());
          break;
        case 320: 
          this.H = Long.valueOf(paramzzsm.f());
          break;
        case 328: 
          this.I = Long.valueOf(paramzzsm.f());
          break;
        case 336: 
          this.J = Long.valueOf(paramzzsm.f());
          break;
        case 346: 
          int i2 = zzsx.b(paramzzsm, 346);
          if (this.K == null) {}
          a[] arrayOfa;
          for (i1 = 0;; i1 = this.K.length)
          {
            arrayOfa = new a[i2 + i1];
            i2 = i1;
            if (i1 != 0)
            {
              System.arraycopy(this.K, 0, arrayOfa, 0, i1);
              i2 = i1;
            }
            while (i2 < arrayOfa.length - 1)
            {
              arrayOfa[i2] = new a();
              paramzzsm.a(arrayOfa[i2]);
              paramzzsm.a();
              i2 += 1;
            }
          }
          arrayOfa[i2] = new a();
          paramzzsm.a(arrayOfa[i2]);
          this.K = arrayOfa;
          break;
        case 1610: 
          if (this.R == null) {
            this.R = new b.c();
          }
          paramzzsm.a(this.R);
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (this.a != null) {
        paramzzsn.a(1, this.a);
      }
      if (this.b != null) {
        paramzzsn.a(2, this.b);
      }
      if (this.c != null) {
        paramzzsn.b(3, this.c.longValue());
      }
      if (this.d != null) {
        paramzzsn.b(4, this.d.longValue());
      }
      if (this.e != null) {
        paramzzsn.b(5, this.e.longValue());
      }
      if (this.f != null) {
        paramzzsn.b(6, this.f.longValue());
      }
      if (this.g != null) {
        paramzzsn.b(7, this.g.longValue());
      }
      if (this.h != null) {
        paramzzsn.b(8, this.h.longValue());
      }
      if (this.i != null) {
        paramzzsn.b(9, this.i.longValue());
      }
      if (this.j != null) {
        paramzzsn.b(10, this.j.longValue());
      }
      if (this.k != null) {
        paramzzsn.b(11, this.k.longValue());
      }
      if (this.l != null) {
        paramzzsn.b(12, this.l.longValue());
      }
      if (this.m != null) {
        paramzzsn.a(13, this.m);
      }
      if (this.n != null) {
        paramzzsn.b(14, this.n.longValue());
      }
      if (this.o != null) {
        paramzzsn.b(15, this.o.longValue());
      }
      if (this.p != null) {
        paramzzsn.b(16, this.p.longValue());
      }
      if (this.q != null) {
        paramzzsn.b(17, this.q.longValue());
      }
      if (this.r != null) {
        paramzzsn.b(18, this.r.longValue());
      }
      if (this.s != null) {
        paramzzsn.b(19, this.s.longValue());
      }
      if (this.t != null) {
        paramzzsn.b(20, this.t.longValue());
      }
      if (this.L != null) {
        paramzzsn.b(21, this.L.longValue());
      }
      if (this.u != null) {
        paramzzsn.b(22, this.u.longValue());
      }
      if (this.v != null) {
        paramzzsn.b(23, this.v.longValue());
      }
      if (this.M != null) {
        paramzzsn.a(24, this.M);
      }
      if (this.Q != null) {
        paramzzsn.b(25, this.Q.longValue());
      }
      if (this.N != null) {
        paramzzsn.a(26, this.N.intValue());
      }
      if (this.w != null) {
        paramzzsn.a(27, this.w);
      }
      if (this.O != null) {
        paramzzsn.a(28, this.O.booleanValue());
      }
      if (this.x != null) {
        paramzzsn.a(29, this.x);
      }
      if (this.P != null) {
        paramzzsn.a(30, this.P);
      }
      if (this.y != null) {
        paramzzsn.b(31, this.y.longValue());
      }
      if (this.z != null) {
        paramzzsn.b(32, this.z.longValue());
      }
      if (this.A != null) {
        paramzzsn.b(33, this.A.longValue());
      }
      if (this.B != null) {
        paramzzsn.a(34, this.B);
      }
      if (this.C != null) {
        paramzzsn.b(35, this.C.longValue());
      }
      if (this.D != null) {
        paramzzsn.b(36, this.D.longValue());
      }
      if (this.E != null) {
        paramzzsn.b(37, this.E.longValue());
      }
      if (this.F != null) {
        paramzzsn.a(38, this.F);
      }
      if (this.G != null) {
        paramzzsn.b(39, this.G.longValue());
      }
      if (this.H != null) {
        paramzzsn.b(40, this.H.longValue());
      }
      if (this.I != null) {
        paramzzsn.b(41, this.I.longValue());
      }
      if (this.J != null) {
        paramzzsn.b(42, this.J.longValue());
      }
      if ((this.K != null) && (this.K.length > 0))
      {
        int i1 = 0;
        while (i1 < this.K.length)
        {
          a locala = this.K[i1];
          if (locala != null) {
            paramzzsn.a(43, locala);
          }
          i1 += 1;
        }
      }
      if (this.R != null) {
        paramzzsn.a(201, this.R);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int i2 = super.b();
      int i1 = i2;
      if (this.a != null) {
        i1 = i2 + zzsn.b(1, this.a);
      }
      i2 = i1;
      if (this.b != null) {
        i2 = i1 + zzsn.b(2, this.b);
      }
      i1 = i2;
      if (this.c != null) {
        i1 = i2 + zzsn.d(3, this.c.longValue());
      }
      i2 = i1;
      if (this.d != null) {
        i2 = i1 + zzsn.d(4, this.d.longValue());
      }
      i1 = i2;
      if (this.e != null) {
        i1 = i2 + zzsn.d(5, this.e.longValue());
      }
      i2 = i1;
      if (this.f != null) {
        i2 = i1 + zzsn.d(6, this.f.longValue());
      }
      i1 = i2;
      if (this.g != null) {
        i1 = i2 + zzsn.d(7, this.g.longValue());
      }
      i2 = i1;
      if (this.h != null) {
        i2 = i1 + zzsn.d(8, this.h.longValue());
      }
      i1 = i2;
      if (this.i != null) {
        i1 = i2 + zzsn.d(9, this.i.longValue());
      }
      i2 = i1;
      if (this.j != null) {
        i2 = i1 + zzsn.d(10, this.j.longValue());
      }
      i1 = i2;
      if (this.k != null) {
        i1 = i2 + zzsn.d(11, this.k.longValue());
      }
      i2 = i1;
      if (this.l != null) {
        i2 = i1 + zzsn.d(12, this.l.longValue());
      }
      i1 = i2;
      if (this.m != null) {
        i1 = i2 + zzsn.b(13, this.m);
      }
      i2 = i1;
      if (this.n != null) {
        i2 = i1 + zzsn.d(14, this.n.longValue());
      }
      i1 = i2;
      if (this.o != null) {
        i1 = i2 + zzsn.d(15, this.o.longValue());
      }
      i2 = i1;
      if (this.p != null) {
        i2 = i1 + zzsn.d(16, this.p.longValue());
      }
      i1 = i2;
      if (this.q != null) {
        i1 = i2 + zzsn.d(17, this.q.longValue());
      }
      i2 = i1;
      if (this.r != null) {
        i2 = i1 + zzsn.d(18, this.r.longValue());
      }
      i1 = i2;
      if (this.s != null) {
        i1 = i2 + zzsn.d(19, this.s.longValue());
      }
      i2 = i1;
      if (this.t != null) {
        i2 = i1 + zzsn.d(20, this.t.longValue());
      }
      i1 = i2;
      if (this.L != null) {
        i1 = i2 + zzsn.d(21, this.L.longValue());
      }
      i2 = i1;
      if (this.u != null) {
        i2 = i1 + zzsn.d(22, this.u.longValue());
      }
      i1 = i2;
      if (this.v != null) {
        i1 = i2 + zzsn.d(23, this.v.longValue());
      }
      i2 = i1;
      if (this.M != null) {
        i2 = i1 + zzsn.b(24, this.M);
      }
      i1 = i2;
      if (this.Q != null) {
        i1 = i2 + zzsn.d(25, this.Q.longValue());
      }
      i2 = i1;
      if (this.N != null) {
        i2 = i1 + zzsn.c(26, this.N.intValue());
      }
      i1 = i2;
      if (this.w != null) {
        i1 = i2 + zzsn.b(27, this.w);
      }
      i2 = i1;
      if (this.O != null) {
        i2 = i1 + zzsn.b(28, this.O.booleanValue());
      }
      i1 = i2;
      if (this.x != null) {
        i1 = i2 + zzsn.b(29, this.x);
      }
      i2 = i1;
      if (this.P != null) {
        i2 = i1 + zzsn.b(30, this.P);
      }
      i1 = i2;
      if (this.y != null) {
        i1 = i2 + zzsn.d(31, this.y.longValue());
      }
      i2 = i1;
      if (this.z != null) {
        i2 = i1 + zzsn.d(32, this.z.longValue());
      }
      i1 = i2;
      if (this.A != null) {
        i1 = i2 + zzsn.d(33, this.A.longValue());
      }
      i2 = i1;
      if (this.B != null) {
        i2 = i1 + zzsn.b(34, this.B);
      }
      i1 = i2;
      if (this.C != null) {
        i1 = i2 + zzsn.d(35, this.C.longValue());
      }
      i2 = i1;
      if (this.D != null) {
        i2 = i1 + zzsn.d(36, this.D.longValue());
      }
      i1 = i2;
      if (this.E != null) {
        i1 = i2 + zzsn.d(37, this.E.longValue());
      }
      i2 = i1;
      if (this.F != null) {
        i2 = i1 + zzsn.c(38, this.F);
      }
      i1 = i2;
      if (this.G != null) {
        i1 = i2 + zzsn.d(39, this.G.longValue());
      }
      i2 = i1;
      if (this.H != null) {
        i2 = i1 + zzsn.d(40, this.H.longValue());
      }
      int i3 = i2;
      if (this.I != null) {
        i3 = i2 + zzsn.d(41, this.I.longValue());
      }
      i1 = i3;
      if (this.J != null) {
        i1 = i3 + zzsn.d(42, this.J.longValue());
      }
      i2 = i1;
      if (this.K != null)
      {
        i2 = i1;
        if (this.K.length > 0)
        {
          i2 = 0;
          while (i2 < this.K.length)
          {
            a locala = this.K[i2];
            i3 = i1;
            if (locala != null) {
              i3 = i1 + zzsn.c(43, locala);
            }
            i2 += 1;
            i1 = i3;
          }
          i2 = i1;
        }
      }
      i1 = i2;
      if (this.R != null) {
        i1 = i2 + zzsn.c(201, this.R);
      }
      return i1;
    }
    
    public static final class a
      extends zzsu
    {
      private static volatile a[] c;
      public Long a;
      public Long b;
      
      public a()
      {
        c();
      }
      
      public static a[] a()
      {
        if (c == null) {}
        synchronized (zzss.a)
        {
          if (c == null) {
            c = new a[0];
          }
          return c;
        }
      }
      
      public a a(zzsm paramzzsm)
        throws IOException
      {
        for (;;)
        {
          int i = paramzzsm.a();
          switch (i)
          {
          default: 
            if (zzsx.a(paramzzsm, i)) {}
            break;
          case 0: 
            return this;
          case 8: 
            this.a = Long.valueOf(paramzzsm.f());
            break;
          case 16: 
            this.b = Long.valueOf(paramzzsm.f());
          }
        }
      }
      
      public void a(zzsn paramzzsn)
        throws IOException
      {
        if (this.a != null) {
          paramzzsn.b(1, this.a.longValue());
        }
        if (this.b != null) {
          paramzzsn.b(2, this.b.longValue());
        }
        super.a(paramzzsn);
      }
      
      protected int b()
      {
        int j = super.b();
        int i = j;
        if (this.a != null) {
          i = j + zzsn.d(1, this.a.longValue());
        }
        j = i;
        if (this.b != null) {
          j = i + zzsn.d(2, this.b.longValue());
        }
        return j;
      }
      
      public a c()
      {
        this.a = null;
        this.b = null;
        this.S = -1;
        return this;
      }
    }
  }
  
  public static final class b
    extends zzsu
  {
    public Long a;
    public Integer b;
    public Boolean c;
    public Integer d;
    
    public b()
    {
      a();
    }
    
    public b a()
    {
      this.a = null;
      this.b = null;
      this.c = null;
      this.d = null;
      this.S = -1;
      return this;
    }
    
    public b a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (zzsx.a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 8: 
          this.a = Long.valueOf(paramzzsm.f());
          break;
        case 16: 
          i = paramzzsm.g();
          switch (i)
          {
          default: 
            break;
          case 0: 
          case 1: 
          case 2: 
          case 3: 
            this.b = Integer.valueOf(i);
          }
          break;
        case 24: 
          this.c = Boolean.valueOf(paramzzsm.h());
          break;
        case 32: 
          i = paramzzsm.g();
          switch (i)
          {
          default: 
            break;
          case 1: 
          case 2: 
          case 3: 
          case 4: 
          case 5: 
          case 6: 
          case 7: 
          case 8: 
          case 9: 
          case 10: 
          case 11: 
          case 12: 
          case 13: 
          case 14: 
          case 15: 
          case 16: 
          case 17: 
          case 18: 
          case 19: 
          case 20: 
          case 21: 
          case 22: 
          case 23: 
          case 24: 
          case 25: 
          case 26: 
          case 27: 
          case 28: 
          case 29: 
          case 30: 
          case 31: 
            this.d = Integer.valueOf(i);
          }
          break;
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (this.a != null) {
        paramzzsn.b(1, this.a.longValue());
      }
      if (this.b != null) {
        paramzzsn.a(2, this.b.intValue());
      }
      if (this.c != null) {
        paramzzsn.a(3, this.c.booleanValue());
      }
      if (this.d != null) {
        paramzzsn.a(4, this.d.intValue());
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b();
      int i = j;
      if (this.a != null) {
        i = j + zzsn.d(1, this.a.longValue());
      }
      j = i;
      if (this.b != null) {
        j = i + zzsn.c(2, this.b.intValue());
      }
      i = j;
      if (this.c != null) {
        i = j + zzsn.b(3, this.c.booleanValue());
      }
      j = i;
      if (this.d != null) {
        j = i + zzsn.c(4, this.d.intValue());
      }
      return j;
    }
  }
  
  public static final class c
    extends zzsu
  {
    public Long a;
    public String b;
    public byte[] c;
    
    public c()
    {
      a();
    }
    
    public c a()
    {
      this.a = null;
      this.b = null;
      this.c = null;
      this.S = -1;
      return this;
    }
    
    public c a(zzsm paramzzsm)
      throws IOException
    {
      for (;;)
      {
        int i = paramzzsm.a();
        switch (i)
        {
        default: 
          if (zzsx.a(paramzzsm, i)) {}
          break;
        case 0: 
          return this;
        case 8: 
          this.a = Long.valueOf(paramzzsm.f());
          break;
        case 26: 
          this.b = paramzzsm.i();
          break;
        case 34: 
          this.c = paramzzsm.j();
        }
      }
    }
    
    public void a(zzsn paramzzsn)
      throws IOException
    {
      if (this.a != null) {
        paramzzsn.b(1, this.a.longValue());
      }
      if (this.b != null) {
        paramzzsn.a(3, this.b);
      }
      if (this.c != null) {
        paramzzsn.a(4, this.c);
      }
      super.a(paramzzsn);
    }
    
    protected int b()
    {
      int j = super.b();
      int i = j;
      if (this.a != null) {
        i = j + zzsn.d(1, this.a.longValue());
      }
      j = i;
      if (this.b != null) {
        j = i + zzsn.b(3, this.b);
      }
      i = j;
      if (this.c != null) {
        i = j + zzsn.b(4, this.c);
      }
      return i;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/ads/a/a/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */