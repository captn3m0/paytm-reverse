package com.google.a.a.c;

import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.NoSuchElementException;

final class h
  extends AbstractMap<String, Object>
{
  final Object a;
  final f b;
  
  h(Object paramObject, boolean paramBoolean)
  {
    this.a = paramObject;
    this.b = f.a(paramObject.getClass(), paramBoolean);
    if (!this.b.b()) {}
    for (paramBoolean = true;; paramBoolean = false)
    {
      t.a(paramBoolean);
      return;
    }
  }
  
  public c a()
  {
    return new c();
  }
  
  public Object a(String paramString, Object paramObject)
  {
    j localj = this.b.a(paramString);
    t.a(localj, "no field of key " + paramString);
    paramString = localj.a(this.a);
    localj.a(this.a, t.a(paramObject));
    return paramString;
  }
  
  public boolean containsKey(Object paramObject)
  {
    return get(paramObject) != null;
  }
  
  public Object get(Object paramObject)
  {
    if (!(paramObject instanceof String)) {}
    do
    {
      return null;
      paramObject = this.b.a((String)paramObject);
    } while (paramObject == null);
    return ((j)paramObject).a(this.a);
  }
  
  final class a
    implements Map.Entry<String, Object>
  {
    private Object b;
    private final j c;
    
    a(j paramj, Object paramObject)
    {
      this.c = paramj;
      this.b = t.a(paramObject);
    }
    
    public String a()
    {
      String str2 = this.c.b();
      String str1 = str2;
      if (h.this.b.a()) {
        str1 = str2.toLowerCase();
      }
      return str1;
    }
    
    public boolean equals(Object paramObject)
    {
      if (this == paramObject) {}
      do
      {
        return true;
        if (!(paramObject instanceof Map.Entry)) {
          return false;
        }
        paramObject = (Map.Entry)paramObject;
      } while ((a().equals(((Map.Entry)paramObject).getKey())) && (getValue().equals(((Map.Entry)paramObject).getValue())));
      return false;
    }
    
    public Object getValue()
    {
      return this.b;
    }
    
    public int hashCode()
    {
      return a().hashCode() ^ getValue().hashCode();
    }
    
    public Object setValue(Object paramObject)
    {
      Object localObject = this.b;
      this.b = t.a(paramObject);
      this.c.a(h.this.a, paramObject);
      return localObject;
    }
  }
  
  final class b
    implements Iterator<Map.Entry<String, Object>>
  {
    private int b = -1;
    private j c;
    private Object d;
    private boolean e;
    private boolean f;
    private j g;
    
    b() {}
    
    public Map.Entry<String, Object> a()
    {
      if (!hasNext()) {
        throw new NoSuchElementException();
      }
      this.g = this.c;
      Object localObject = this.d;
      this.f = false;
      this.e = false;
      this.c = null;
      this.d = null;
      return new h.a(h.this, this.g, localObject);
    }
    
    public boolean hasNext()
    {
      if (!this.f)
      {
        this.f = true;
        for (this.d = null; this.d == null; this.d = this.c.a(h.this.a))
        {
          int i = this.b + 1;
          this.b = i;
          if (i >= h.this.b.a.size()) {
            break;
          }
          this.c = h.this.b.a((String)h.this.b.a.get(this.b));
        }
      }
      return this.d != null;
    }
    
    public void remove()
    {
      if ((this.g != null) && (!this.e)) {}
      for (boolean bool = true;; bool = false)
      {
        t.b(bool);
        this.e = true;
        this.g.a(h.this.a, null);
        return;
      }
    }
  }
  
  final class c
    extends AbstractSet<Map.Entry<String, Object>>
  {
    c() {}
    
    public h.b a()
    {
      return new h.b(h.this);
    }
    
    public void clear()
    {
      Iterator localIterator = h.this.b.a.iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        h.this.b.a(str).a(h.this.a, null);
      }
    }
    
    public boolean isEmpty()
    {
      Iterator localIterator = h.this.b.a.iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        if (h.this.b.a(str).a(h.this.a) != null) {
          return false;
        }
      }
      return true;
    }
    
    public int size()
    {
      int i = 0;
      Iterator localIterator = h.this.b.a.iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        if (h.this.b.a(str).a(h.this.a) != null) {
          i += 1;
        }
      }
      return i;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/c/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */