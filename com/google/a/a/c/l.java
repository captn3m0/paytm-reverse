package com.google.a.a.c;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class l
{
  public static long a(v paramv)
    throws IOException
  {
    c localc = new c();
    try
    {
      paramv.a(localc);
      return localc.a;
    }
    finally
    {
      localc.close();
    }
  }
  
  public static void a(InputStream paramInputStream, OutputStream paramOutputStream)
    throws IOException
  {
    a(paramInputStream, paramOutputStream, true);
  }
  
  public static void a(InputStream paramInputStream, OutputStream paramOutputStream, boolean paramBoolean)
    throws IOException
  {
    try
    {
      d.a(paramInputStream, paramOutputStream);
      return;
    }
    finally
    {
      if (paramBoolean) {
        paramInputStream.close();
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/c/l.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */