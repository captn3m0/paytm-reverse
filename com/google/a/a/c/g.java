package com.google.a.a.c;

import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

public class g
{
  public static final Boolean a = new Boolean(true);
  public static final String b = new String();
  public static final Character c = new Character('\000');
  public static final Byte d = new Byte((byte)0);
  public static final Short e = new Short((short)0);
  public static final Integer f = new Integer(0);
  public static final Float g = new Float(0.0F);
  public static final Long h = new Long(0L);
  public static final Double i = new Double(0.0D);
  public static final BigInteger j = new BigInteger("0");
  public static final BigDecimal k = new BigDecimal("0");
  public static final i l = new i(0L);
  private static final ConcurrentHashMap<Class<?>, Object> m = new ConcurrentHashMap();
  
  static
  {
    m.put(Boolean.class, a);
    m.put(String.class, b);
    m.put(Character.class, c);
    m.put(Byte.class, d);
    m.put(Short.class, e);
    m.put(Integer.class, f);
    m.put(Float.class, g);
    m.put(Long.class, h);
    m.put(Double.class, i);
    m.put(BigInteger.class, j);
    m.put(BigDecimal.class, k);
    m.put(i.class, l);
  }
  
  public static Object a(Type paramType, String paramString)
  {
    Class localClass;
    if ((paramType instanceof Class)) {
      localClass = (Class)paramType;
    }
    while ((paramType == null) || (localClass != null)) {
      if (localClass == Void.class)
      {
        return null;
        localClass = null;
      }
      else
      {
        if ((paramString == null) || (localClass == null) || (localClass.isAssignableFrom(String.class))) {
          return paramString;
        }
        if ((localClass == Character.class) || (localClass == Character.TYPE))
        {
          if (paramString.length() != 1) {
            throw new IllegalArgumentException("expected type Character/char but got " + localClass);
          }
          return Character.valueOf(paramString.charAt(0));
        }
        if ((localClass == Boolean.class) || (localClass == Boolean.TYPE)) {
          return Boolean.valueOf(paramString);
        }
        if ((localClass == Byte.class) || (localClass == Byte.TYPE)) {
          return Byte.valueOf(paramString);
        }
        if ((localClass == Short.class) || (localClass == Short.TYPE)) {
          return Short.valueOf(paramString);
        }
        if ((localClass == Integer.class) || (localClass == Integer.TYPE)) {
          return Integer.valueOf(paramString);
        }
        if ((localClass == Long.class) || (localClass == Long.TYPE)) {
          return Long.valueOf(paramString);
        }
        if ((localClass == Float.class) || (localClass == Float.TYPE)) {
          return Float.valueOf(paramString);
        }
        if ((localClass == Double.class) || (localClass == Double.TYPE)) {
          return Double.valueOf(paramString);
        }
        if (localClass == i.class) {
          return i.a(paramString);
        }
        if (localClass == BigInteger.class) {
          return new BigInteger(paramString);
        }
        if (localClass == BigDecimal.class) {
          return new BigDecimal(paramString);
        }
        if (localClass.isEnum()) {
          return f.a(localClass).a(paramString).g();
        }
      }
    }
    throw new IllegalArgumentException("expected primitive class, but got: " + paramType);
  }
  
  public static Type a(List<Type> paramList, Type paramType)
  {
    Type localType1 = paramType;
    if ((paramType instanceof WildcardType)) {
      localType1 = y.a((WildcardType)paramType);
    }
    while ((localType1 instanceof TypeVariable))
    {
      Type localType2 = y.a(paramList, (TypeVariable)localType1);
      paramType = localType1;
      if (localType2 != null) {
        paramType = localType2;
      }
      localType1 = paramType;
      if ((paramType instanceof TypeVariable)) {
        localType1 = ((TypeVariable)paramType).getBounds()[0];
      }
    }
    return localType1;
  }
  
  public static void a(Object paramObject1, Object paramObject2)
  {
    Object localObject1 = paramObject1.getClass();
    if (localObject1 == paramObject2.getClass())
    {
      bool = true;
      t.a(bool);
      if (!((Class)localObject1).isArray()) {
        break label104;
      }
      if (Array.getLength(paramObject1) != Array.getLength(paramObject2)) {
        break label98;
      }
    }
    int n;
    label98:
    for (boolean bool = true;; bool = false)
    {
      t.a(bool);
      n = 0;
      paramObject1 = y.a(paramObject1).iterator();
      while (((Iterator)paramObject1).hasNext())
      {
        Array.set(paramObject2, n, c(((Iterator)paramObject1).next()));
        n += 1;
      }
      bool = false;
      break;
    }
    label104:
    if (Collection.class.isAssignableFrom((Class)localObject1))
    {
      paramObject1 = (Collection)paramObject1;
      if (ArrayList.class.isAssignableFrom((Class)localObject1)) {
        ((ArrayList)paramObject2).ensureCapacity(((Collection)paramObject1).size());
      }
      paramObject2 = (Collection)paramObject2;
      paramObject1 = ((Collection)paramObject1).iterator();
      while (((Iterator)paramObject1).hasNext()) {
        ((Collection)paramObject2).add(c(((Iterator)paramObject1).next()));
      }
    }
    bool = k.class.isAssignableFrom((Class)localObject1);
    if ((bool) || (!Map.class.isAssignableFrom((Class)localObject1)))
    {
      if (bool) {}
      for (localObject1 = ((k)paramObject1).b;; localObject1 = f.a((Class)localObject1))
      {
        Iterator localIterator = ((f)localObject1).a.iterator();
        while (localIterator.hasNext())
        {
          j localj = ((f)localObject1).a((String)localIterator.next());
          if ((!localj.e()) && ((!bool) || (!localj.f())))
          {
            Object localObject2 = localj.a(paramObject1);
            if (localObject2 != null) {
              localj.a(paramObject2, c(localObject2));
            }
          }
        }
      }
    }
    if (a.class.isAssignableFrom((Class)localObject1))
    {
      paramObject2 = (a)paramObject2;
      paramObject1 = (a)paramObject1;
      int i1 = ((a)paramObject1).size();
      n = 0;
      while (n < i1)
      {
        ((a)paramObject2).a(n, c(((a)paramObject1).b(n)));
        n += 1;
      }
    }
    paramObject2 = (Map)paramObject2;
    paramObject1 = ((Map)paramObject1).entrySet().iterator();
    while (((Iterator)paramObject1).hasNext())
    {
      localObject1 = (Map.Entry)((Iterator)paramObject1).next();
      ((Map)paramObject2).put(((Map.Entry)localObject1).getKey(), c(((Map.Entry)localObject1).getValue()));
    }
  }
  
  public static boolean a(Object paramObject)
  {
    return (paramObject != null) && (paramObject == m.get(paramObject.getClass()));
  }
  
  public static boolean a(Type paramType)
  {
    Type localType = paramType;
    if ((paramType instanceof WildcardType)) {
      localType = y.a((WildcardType)paramType);
    }
    if (!(localType instanceof Class)) {}
    do
    {
      return false;
      paramType = (Class)localType;
    } while ((!paramType.isPrimitive()) && (paramType != Character.class) && (paramType != String.class) && (paramType != Integer.class) && (paramType != Long.class) && (paramType != Short.class) && (paramType != Byte.class) && (paramType != Float.class) && (paramType != Double.class) && (paramType != BigInteger.class) && (paramType != BigDecimal.class) && (paramType != i.class) && (paramType != Boolean.class));
    return true;
  }
  
  public static Collection<Object> b(Type paramType)
  {
    Object localObject = paramType;
    if ((paramType instanceof WildcardType)) {
      localObject = y.a((WildcardType)paramType);
    }
    paramType = (Type)localObject;
    if ((localObject instanceof ParameterizedType)) {
      paramType = ((ParameterizedType)localObject).getRawType();
    }
    if ((paramType instanceof Class)) {}
    for (localObject = (Class)paramType; (paramType == null) || ((paramType instanceof GenericArrayType)) || ((localObject != null) && ((((Class)localObject).isArray()) || (((Class)localObject).isAssignableFrom(ArrayList.class)))); localObject = null) {
      return new ArrayList();
    }
    if (localObject == null) {
      throw new IllegalArgumentException("unable to create new instance of type: " + paramType);
    }
    if (((Class)localObject).isAssignableFrom(HashSet.class)) {
      return new HashSet();
    }
    if (((Class)localObject).isAssignableFrom(TreeSet.class)) {
      return new TreeSet();
    }
    return (Collection)y.a((Class)localObject);
  }
  
  public static Map<String, Object> b(Object paramObject)
  {
    if ((paramObject == null) || (a(paramObject))) {
      return Collections.emptyMap();
    }
    if ((paramObject instanceof Map)) {
      return (Map)paramObject;
    }
    return new h(paramObject, false);
  }
  
  public static <T> T c(T paramT)
  {
    if ((paramT == null) || (a(paramT.getClass()))) {
      return paramT;
    }
    if ((paramT instanceof k)) {
      return ((k)paramT).f();
    }
    Object localObject = paramT.getClass();
    if (((Class)localObject).isArray()) {
      localObject = Array.newInstance(((Class)localObject).getComponentType(), Array.getLength(paramT));
    }
    for (;;)
    {
      a(paramT, localObject);
      return (T)localObject;
      if ((paramT instanceof a)) {
        localObject = ((a)paramT).b();
      } else {
        localObject = y.a((Class)localObject);
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/c/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */