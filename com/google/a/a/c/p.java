package com.google.a.a.c;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class p
  extends FilterOutputStream
{
  private final n a;
  
  public p(OutputStream paramOutputStream, Logger paramLogger, Level paramLevel, int paramInt)
  {
    super(paramOutputStream);
    this.a = new n(paramLogger, paramLevel, paramInt);
  }
  
  public final n a()
  {
    return this.a;
  }
  
  public void close()
    throws IOException
  {
    this.a.close();
    super.close();
  }
  
  public void write(int paramInt)
    throws IOException
  {
    this.out.write(paramInt);
    this.a.write(paramInt);
  }
  
  public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    this.out.write(paramArrayOfByte, paramInt1, paramInt2);
    this.a.write(paramArrayOfByte, paramInt1, paramInt2);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/c/p.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */