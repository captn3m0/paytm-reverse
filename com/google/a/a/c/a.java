package com.google.a.a.c;

import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;

public class a<K, V>
  extends AbstractMap<K, V>
  implements Cloneable
{
  int a;
  private Object[] b;
  
  public static <K, V> a<K, V> a()
  {
    return new a();
  }
  
  private int b(Object paramObject)
  {
    int j = this.a;
    Object[] arrayOfObject = this.b;
    int i = 0;
    while (i < j << 1)
    {
      Object localObject = arrayOfObject[i];
      if (paramObject == null)
      {
        if (localObject != null) {}
      }
      else {
        while (paramObject.equals(localObject)) {
          return i;
        }
      }
      i += 2;
    }
    return -2;
  }
  
  private void b(int paramInt, K paramK, V paramV)
  {
    Object[] arrayOfObject = this.b;
    arrayOfObject[paramInt] = paramK;
    arrayOfObject[(paramInt + 1)] = paramV;
  }
  
  private void e(int paramInt)
  {
    if (paramInt == 0) {
      this.b = null;
    }
    int i;
    Object[] arrayOfObject1;
    Object[] arrayOfObject2;
    do
    {
      do
      {
        return;
        i = this.a;
        arrayOfObject1 = this.b;
      } while ((i != 0) && (paramInt == arrayOfObject1.length));
      arrayOfObject2 = new Object[paramInt];
      this.b = arrayOfObject2;
    } while (i == 0);
    System.arraycopy(arrayOfObject1, 0, arrayOfObject2, 0, i << 1);
  }
  
  private V f(int paramInt)
  {
    if (paramInt < 0) {
      return null;
    }
    return (V)this.b[paramInt];
  }
  
  private V g(int paramInt)
  {
    int i = this.a << 1;
    if ((paramInt < 0) || (paramInt >= i)) {
      return null;
    }
    Object localObject = f(paramInt + 1);
    Object[] arrayOfObject = this.b;
    int j = i - paramInt - 2;
    if (j != 0) {
      System.arraycopy(arrayOfObject, paramInt + 2, arrayOfObject, paramInt, j);
    }
    this.a -= 1;
    b(i - 2, null, null);
    return (V)localObject;
  }
  
  public final int a(K paramK)
  {
    return b(paramK) >> 1;
  }
  
  public final K a(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= this.a)) {
      return null;
    }
    return (K)this.b[(paramInt << 1)];
  }
  
  public final V a(int paramInt, V paramV)
  {
    int i = this.a;
    if ((paramInt < 0) || (paramInt >= i)) {
      throw new IndexOutOfBoundsException();
    }
    paramInt = (paramInt << 1) + 1;
    Object localObject = f(paramInt);
    this.b[paramInt] = paramV;
    return (V)localObject;
  }
  
  public final V a(int paramInt, K paramK, V paramV)
  {
    if (paramInt < 0) {
      throw new IndexOutOfBoundsException();
    }
    int i = paramInt + 1;
    d(i);
    paramInt <<= 1;
    Object localObject = f(paramInt + 1);
    b(paramInt, paramK, paramV);
    if (i > this.a) {
      this.a = i;
    }
    return (V)localObject;
  }
  
  public a<K, V> b()
  {
    try
    {
      a locala = (a)super.clone();
      Object[] arrayOfObject1 = this.b;
      if (arrayOfObject1 != null)
      {
        int i = arrayOfObject1.length;
        Object[] arrayOfObject2 = new Object[i];
        locala.b = arrayOfObject2;
        System.arraycopy(arrayOfObject1, 0, arrayOfObject2, 0, i);
      }
      return locala;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException) {}
    return null;
  }
  
  public final V b(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= this.a)) {
      return null;
    }
    return (V)f((paramInt << 1) + 1);
  }
  
  public final V c(int paramInt)
  {
    return (V)g(paramInt << 1);
  }
  
  public void clear()
  {
    this.a = 0;
    this.b = null;
  }
  
  public final boolean containsKey(Object paramObject)
  {
    return -2 != b(paramObject);
  }
  
  public final boolean containsValue(Object paramObject)
  {
    int j = this.a;
    Object[] arrayOfObject = this.b;
    int i = 1;
    while (i < j << 1)
    {
      Object localObject = arrayOfObject[i];
      if (paramObject == null)
      {
        if (localObject != null) {}
      }
      else {
        while (paramObject.equals(localObject)) {
          return true;
        }
      }
      i += 2;
    }
    return false;
  }
  
  public final void d(int paramInt)
  {
    if (paramInt < 0) {
      throw new IndexOutOfBoundsException();
    }
    Object[] arrayOfObject = this.b;
    int j = paramInt << 1;
    if (arrayOfObject == null) {}
    for (paramInt = 0;; paramInt = arrayOfObject.length)
    {
      if (j > paramInt)
      {
        int i = paramInt / 2 * 3 + 1;
        paramInt = i;
        if (i % 2 != 0) {
          paramInt = i + 1;
        }
        i = paramInt;
        if (paramInt < j) {
          i = j;
        }
        e(i);
      }
      return;
    }
  }
  
  public final Set<Map.Entry<K, V>> entrySet()
  {
    return new c();
  }
  
  public final V get(Object paramObject)
  {
    return (V)f(b(paramObject) + 1);
  }
  
  public final V put(K paramK, V paramV)
  {
    int j = a(paramK);
    int i = j;
    if (j == -1) {
      i = this.a;
    }
    return (V)a(i, paramK, paramV);
  }
  
  public final V remove(Object paramObject)
  {
    return (V)g(b(paramObject));
  }
  
  public final int size()
  {
    return this.a;
  }
  
  final class a
    implements Map.Entry<K, V>
  {
    private int b;
    
    a(int paramInt)
    {
      this.b = paramInt;
    }
    
    public boolean equals(Object paramObject)
    {
      if (this == paramObject) {}
      do
      {
        return true;
        if (!(paramObject instanceof Map.Entry)) {
          return false;
        }
        paramObject = (Map.Entry)paramObject;
      } while ((s.a(getKey(), ((Map.Entry)paramObject).getKey())) && (s.a(getValue(), ((Map.Entry)paramObject).getValue())));
      return false;
    }
    
    public K getKey()
    {
      return (K)a.this.a(this.b);
    }
    
    public V getValue()
    {
      return (V)a.this.b(this.b);
    }
    
    public int hashCode()
    {
      return getKey().hashCode() ^ getValue().hashCode();
    }
    
    public V setValue(V paramV)
    {
      return (V)a.this.a(this.b, paramV);
    }
  }
  
  final class b
    implements Iterator<Map.Entry<K, V>>
  {
    private boolean b;
    private int c;
    
    b() {}
    
    public Map.Entry<K, V> a()
    {
      int i = this.c;
      if (i == a.this.a) {
        throw new NoSuchElementException();
      }
      this.c += 1;
      return new a.a(a.this, i);
    }
    
    public boolean hasNext()
    {
      return this.c < a.this.a;
    }
    
    public void remove()
    {
      int i = this.c - 1;
      if ((this.b) || (i < 0)) {
        throw new IllegalArgumentException();
      }
      a.this.c(i);
      this.b = true;
    }
  }
  
  final class c
    extends AbstractSet<Map.Entry<K, V>>
  {
    c() {}
    
    public Iterator<Map.Entry<K, V>> iterator()
    {
      return new a.b(a.this);
    }
    
    public int size()
    {
      return a.this.a;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/c/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */