package com.google.a.a.c.a;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public final class a
{
  private static final b a = new c("-_.*", true);
  private static final b b = new c("-_.!~*'()@:$&,;=", false);
  private static final b c = new c("-_.!~*'()@:$&,;=+/?", false);
  private static final b d = new c("-_.!~*'():$&,;=", false);
  private static final b e = new c("-_.!~*'()@:$,;/?:", false);
  
  public static String a(String paramString)
  {
    return a.a(paramString);
  }
  
  public static String b(String paramString)
  {
    try
    {
      paramString = URLDecoder.decode(paramString, "UTF-8");
      return paramString;
    }
    catch (UnsupportedEncodingException paramString)
    {
      throw new RuntimeException(paramString);
    }
  }
  
  public static String c(String paramString)
  {
    return b.a(paramString);
  }
  
  public static String d(String paramString)
  {
    return d.a(paramString);
  }
  
  public static String e(String paramString)
  {
    return e.a(paramString);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/c/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */