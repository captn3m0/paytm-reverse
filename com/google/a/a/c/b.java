package com.google.a.a.c;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class b
{
  private final Map<String, a> a = a.a();
  private final Map<Field, a> b = a.a();
  private final Object c;
  
  public b(Object paramObject)
  {
    this.c = paramObject;
  }
  
  public void a()
  {
    Iterator localIterator = this.a.entrySet().iterator();
    Map.Entry localEntry;
    while (localIterator.hasNext())
    {
      localEntry = (Map.Entry)localIterator.next();
      ((Map)this.c).put(localEntry.getKey(), ((a)localEntry.getValue()).a());
    }
    localIterator = this.b.entrySet().iterator();
    while (localIterator.hasNext())
    {
      localEntry = (Map.Entry)localIterator.next();
      j.a((Field)localEntry.getKey(), this.c, ((a)localEntry.getValue()).a());
    }
  }
  
  public void a(Field paramField, Class<?> paramClass, Object paramObject)
  {
    a locala2 = (a)this.b.get(paramField);
    a locala1 = locala2;
    if (locala2 == null)
    {
      locala1 = new a(paramClass);
      this.b.put(paramField, locala1);
    }
    locala1.a(paramClass, paramObject);
  }
  
  static class a
  {
    final Class<?> a;
    final ArrayList<Object> b = new ArrayList();
    
    a(Class<?> paramClass)
    {
      this.a = paramClass;
    }
    
    Object a()
    {
      return y.a(this.b, this.a);
    }
    
    void a(Class<?> paramClass, Object paramObject)
    {
      if (paramClass == this.a) {}
      for (boolean bool = true;; bool = false)
      {
        t.a(bool);
        this.b.add(paramObject);
        return;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/c/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */