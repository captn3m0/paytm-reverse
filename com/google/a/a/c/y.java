package com.google.a.a.c;

import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class y
{
  public static Class<?> a(ParameterizedType paramParameterizedType)
  {
    return (Class)paramParameterizedType.getRawType();
  }
  
  public static Class<?> a(List<Type> paramList, Type paramType)
  {
    Type localType = paramType;
    if ((paramType instanceof TypeVariable)) {
      localType = a(paramList, (TypeVariable)paramType);
    }
    if ((localType instanceof GenericArrayType)) {
      return Array.newInstance(a(paramList, b(localType)), 0).getClass();
    }
    if ((localType instanceof Class)) {
      return (Class)localType;
    }
    if ((localType instanceof ParameterizedType)) {
      return a((ParameterizedType)localType);
    }
    if (localType == null) {}
    for (boolean bool = true;; bool = false)
    {
      t.a(bool, "wildcard type is not supported: %s", new Object[] { localType });
      return Object.class;
    }
  }
  
  private static IllegalArgumentException a(Exception paramException, Class<?> paramClass)
  {
    StringBuilder localStringBuilder = new StringBuilder("unable to create new instance of class ").append(paramClass.getName());
    Object localObject = new ArrayList();
    int i;
    if (paramClass.isArray())
    {
      ((ArrayList)localObject).add("because it is an array");
      i = 0;
      paramClass = ((ArrayList)localObject).iterator();
      label49:
      if (!paramClass.hasNext()) {
        break label244;
      }
      localObject = (String)paramClass.next();
      if (i == 0) {
        break label239;
      }
      localStringBuilder.append(" and");
    }
    for (;;)
    {
      localStringBuilder.append(" ").append((String)localObject);
      break label49;
      if (paramClass.isPrimitive())
      {
        ((ArrayList)localObject).add("because it is primitive");
        break;
      }
      if (paramClass == Void.class)
      {
        ((ArrayList)localObject).add("because it is void");
        break;
      }
      if (Modifier.isInterface(paramClass.getModifiers())) {
        ((ArrayList)localObject).add("because it is an interface");
      }
      for (;;)
      {
        if ((paramClass.getEnclosingClass() != null) && (!Modifier.isStatic(paramClass.getModifiers()))) {
          ((ArrayList)localObject).add("because it is not static");
        }
        if (Modifier.isPublic(paramClass.getModifiers())) {
          break label215;
        }
        ((ArrayList)localObject).add("possibly because it is not public");
        break;
        if (Modifier.isAbstract(paramClass.getModifiers())) {
          ((ArrayList)localObject).add("because it is abstract");
        }
      }
      try
      {
        label215:
        paramClass.getConstructor(new Class[0]);
      }
      catch (NoSuchMethodException paramClass)
      {
        ((ArrayList)localObject).add("because it has no accessible default constructor");
      }
      break;
      label239:
      i = 1;
    }
    label244:
    return new IllegalArgumentException(localStringBuilder.toString(), paramException);
  }
  
  public static <T> Iterable<T> a(Object paramObject)
  {
    if ((paramObject instanceof Iterable)) {
      return (Iterable)paramObject;
    }
    Class localClass = paramObject.getClass();
    t.a(localClass.isArray(), "not an array or Iterable: %s", new Object[] { localClass });
    if (!localClass.getComponentType().isPrimitive()) {
      return Arrays.asList((Object[])paramObject);
    }
    new Iterable()
    {
      public Iterator<T> iterator()
      {
        new Iterator()
        {
          final int a = Array.getLength(y.1.this.a);
          int b = 0;
          
          public boolean hasNext()
          {
            return this.b < this.a;
          }
          
          public T next()
          {
            if (!hasNext()) {
              throw new NoSuchElementException();
            }
            Object localObject = y.1.this.a;
            int i = this.b;
            this.b = (i + 1);
            return (T)Array.get(localObject, i);
          }
          
          public void remove()
          {
            throw new UnsupportedOperationException();
          }
        };
      }
    };
  }
  
  public static <T> T a(Class<T> paramClass)
  {
    try
    {
      Object localObject = paramClass.newInstance();
      return (T)localObject;
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      throw a(localIllegalAccessException, paramClass);
    }
    catch (InstantiationException localInstantiationException)
    {
      throw a(localInstantiationException, paramClass);
    }
  }
  
  public static Object a(Collection<?> paramCollection, Class<?> paramClass)
  {
    if (paramClass.isPrimitive())
    {
      paramClass = Array.newInstance(paramClass, paramCollection.size());
      int i = 0;
      Iterator localIterator = paramCollection.iterator();
      for (;;)
      {
        paramCollection = paramClass;
        if (!localIterator.hasNext()) {
          break;
        }
        Array.set(paramClass, i, localIterator.next());
        i += 1;
      }
    }
    paramCollection = paramCollection.toArray((Object[])Array.newInstance(paramClass, paramCollection.size()));
    return paramCollection;
  }
  
  public static ParameterizedType a(Type paramType, Class<?> paramClass)
  {
    Object localObject = paramType;
    if (!(paramType instanceof Class))
    {
      if ((paramType instanceof ParameterizedType)) {
        localObject = paramType;
      }
    }
    else if ((localObject != null) && (localObject != Object.class))
    {
      if ((localObject instanceof Class)) {
        paramType = (Class)localObject;
      }
      label46:
      do
      {
        localObject = paramType.getGenericSuperclass();
        break;
        paramType = (ParameterizedType)localObject;
        localObject = a(paramType);
        if (localObject == paramClass) {
          return paramType;
        }
        paramType = (Type)localObject;
      } while (!paramClass.isInterface());
      Type[] arrayOfType = ((Class)localObject).getGenericInterfaces();
      int j = arrayOfType.length;
      int i = 0;
      for (;;)
      {
        paramType = (Type)localObject;
        if (i >= j) {
          break label46;
        }
        Type localType = arrayOfType[i];
        if ((localType instanceof Class)) {}
        for (paramType = (Class)localType;; paramType = a((ParameterizedType)localType))
        {
          if (!paramClass.isAssignableFrom(paramType)) {
            break label153;
          }
          localObject = localType;
          break;
        }
        label153:
        i += 1;
      }
    }
    return null;
  }
  
  private static Type a(Type paramType, Class<?> paramClass, int paramInt)
  {
    paramClass = a(paramType, paramClass).getActualTypeArguments()[paramInt];
    if ((paramClass instanceof TypeVariable))
    {
      paramType = a(Arrays.asList(new Type[] { paramType }), (TypeVariable)paramClass);
      if (paramType != null) {
        return paramType;
      }
    }
    return paramClass;
  }
  
  public static Type a(WildcardType paramWildcardType)
  {
    Type[] arrayOfType = paramWildcardType.getLowerBounds();
    if (arrayOfType.length != 0) {
      return arrayOfType[0];
    }
    return paramWildcardType.getUpperBounds()[0];
  }
  
  public static Type a(List<Type> paramList, TypeVariable<?> paramTypeVariable)
  {
    Object localObject = paramTypeVariable.getGenericDeclaration();
    if ((localObject instanceof Class))
    {
      Class localClass = (Class)localObject;
      int i = paramList.size();
      for (ParameterizedType localParameterizedType = null; localParameterizedType == null; localParameterizedType = a((Type)paramList.get(i), localClass))
      {
        i -= 1;
        if (i < 0) {
          break;
        }
      }
      if (localParameterizedType != null)
      {
        localObject = ((GenericDeclaration)localObject).getTypeParameters();
        i = 0;
        for (;;)
        {
          if ((i >= localObject.length) || (localObject[i].equals(paramTypeVariable)))
          {
            paramTypeVariable = localParameterizedType.getActualTypeArguments()[i];
            if (!(paramTypeVariable instanceof TypeVariable)) {
              break;
            }
            paramList = a(paramList, (TypeVariable)paramTypeVariable);
            if (paramList == null) {
              break;
            }
            return paramList;
          }
          i += 1;
        }
        return paramTypeVariable;
      }
    }
    return null;
  }
  
  public static boolean a(Class<?> paramClass1, Class<?> paramClass2)
  {
    return (paramClass1.isAssignableFrom(paramClass2)) || (paramClass2.isAssignableFrom(paramClass1));
  }
  
  public static boolean a(Type paramType)
  {
    return ((paramType instanceof GenericArrayType)) || (((paramType instanceof Class)) && (((Class)paramType).isArray()));
  }
  
  public static Type b(Type paramType)
  {
    if ((paramType instanceof GenericArrayType)) {
      return ((GenericArrayType)paramType).getGenericComponentType();
    }
    return ((Class)paramType).getComponentType();
  }
  
  public static Type c(Type paramType)
  {
    return a(paramType, Iterable.class, 0);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/c/y.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */