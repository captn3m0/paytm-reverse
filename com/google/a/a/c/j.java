package com.google.a.a.c;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.WeakHashMap;

public class j
{
  private static final Map<Field, j> a = new WeakHashMap();
  private final boolean b;
  private final Field c;
  private final String d;
  
  j(Field paramField, String paramString)
  {
    this.c = paramField;
    if (paramString == null) {}
    for (paramField = null;; paramField = paramString.intern())
    {
      this.d = paramField;
      this.b = g.a(c());
      return;
    }
  }
  
  /* Error */
  public static j a(Enum<?> paramEnum)
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_1
    //   2: aload_0
    //   3: invokevirtual 51	java/lang/Object:getClass	()Ljava/lang/Class;
    //   6: aload_0
    //   7: invokevirtual 56	java/lang/Enum:name	()Ljava/lang/String;
    //   10: invokevirtual 62	java/lang/Class:getField	(Ljava/lang/String;)Ljava/lang/reflect/Field;
    //   13: invokestatic 65	com/google/a/a/c/j:a	(Ljava/lang/reflect/Field;)Lcom/google/a/a/c/j;
    //   16: astore_2
    //   17: aload_2
    //   18: ifnull +19 -> 37
    //   21: iload_1
    //   22: ldc 67
    //   24: iconst_1
    //   25: anewarray 4	java/lang/Object
    //   28: dup
    //   29: iconst_0
    //   30: aload_0
    //   31: aastore
    //   32: invokestatic 72	com/google/a/a/c/t:a	(ZLjava/lang/String;[Ljava/lang/Object;)V
    //   35: aload_2
    //   36: areturn
    //   37: iconst_0
    //   38: istore_1
    //   39: goto -18 -> 21
    //   42: astore_0
    //   43: new 74	java/lang/RuntimeException
    //   46: dup
    //   47: aload_0
    //   48: invokespecial 77	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   51: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	52	0	paramEnum	Enum<?>
    //   1	38	1	bool	boolean
    //   16	20	2	localj	j
    // Exception table:
    //   from	to	target	type
    //   2	17	42	java/lang/NoSuchFieldException
    //   21	35	42	java/lang/NoSuchFieldException
  }
  
  public static j a(Field paramField)
  {
    if (paramField == null) {
      return null;
    }
    for (;;)
    {
      Object localObject1;
      synchronized (a)
      {
        Object localObject2 = (j)a.get(paramField);
        boolean bool = paramField.isEnumConstant();
        localObject1 = localObject2;
        if (localObject2 == null) {
          if (!bool)
          {
            localObject1 = localObject2;
            if (Modifier.isStatic(paramField.getModifiers())) {}
          }
          else
          {
            if (!bool) {
              break label149;
            }
            localObject1 = (z)paramField.getAnnotation(z.class);
            if (localObject1 == null) {
              break label127;
            }
            localObject1 = ((z)localObject1).a();
            localObject2 = localObject1;
            if ("##default".equals(localObject1)) {
              localObject2 = paramField.getName();
            }
            localObject1 = new j(paramField, (String)localObject2);
            a.put(paramField, localObject1);
          }
        }
        return (j)localObject1;
      }
      label127:
      if ((r)paramField.getAnnotation(r.class) != null)
      {
        localObject1 = null;
      }
      else
      {
        return null;
        label149:
        localObject1 = (m)paramField.getAnnotation(m.class);
        if (localObject1 == null) {
          return null;
        }
        localObject1 = ((m)localObject1).a();
        paramField.setAccessible(true);
      }
    }
  }
  
  public static Object a(Field paramField, Object paramObject)
  {
    try
    {
      paramField = paramField.get(paramObject);
      return paramField;
    }
    catch (IllegalAccessException paramField)
    {
      throw new IllegalArgumentException(paramField);
    }
  }
  
  public static void a(Field paramField, Object paramObject1, Object paramObject2)
  {
    if (Modifier.isFinal(paramField.getModifiers()))
    {
      Object localObject = a(paramField, paramObject1);
      if (paramObject2 == null)
      {
        if (localObject == null) {}
      }
      else {
        while (!paramObject2.equals(localObject)) {
          throw new IllegalArgumentException("expected final value <" + localObject + "> but was <" + paramObject2 + "> on " + paramField.getName() + " field in " + paramObject1.getClass().getName());
        }
      }
      return;
    }
    try
    {
      paramField.set(paramObject1, paramObject2);
      return;
    }
    catch (SecurityException paramField)
    {
      throw new IllegalArgumentException(paramField);
    }
    catch (IllegalAccessException paramField)
    {
      throw new IllegalArgumentException(paramField);
    }
  }
  
  public Object a(Object paramObject)
  {
    return a(this.c, paramObject);
  }
  
  public Field a()
  {
    return this.c;
  }
  
  public void a(Object paramObject1, Object paramObject2)
  {
    a(this.c, paramObject1, paramObject2);
  }
  
  public String b()
  {
    return this.d;
  }
  
  public Class<?> c()
  {
    return this.c.getType();
  }
  
  public Type d()
  {
    return this.c.getGenericType();
  }
  
  public boolean e()
  {
    return Modifier.isFinal(this.c.getModifiers());
  }
  
  public boolean f()
  {
    return this.b;
  }
  
  public <T extends Enum<T>> T g()
  {
    return Enum.valueOf(this.c.getDeclaringClass(), this.c.getName());
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/c/j.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */