package com.google.a.a.c;

import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class q
  implements v
{
  private final v a;
  private final int b;
  private final Level c;
  private final Logger d;
  
  public q(v paramv, Logger paramLogger, Level paramLevel, int paramInt)
  {
    this.a = paramv;
    this.d = paramLogger;
    this.c = paramLevel;
    this.b = paramInt;
  }
  
  public void a(OutputStream paramOutputStream)
    throws IOException
  {
    p localp = new p(paramOutputStream, this.d, this.c, this.b);
    try
    {
      this.a.a(localp);
      localp.a().close();
      paramOutputStream.flush();
      return;
    }
    finally
    {
      localp.a().close();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/c/q.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */