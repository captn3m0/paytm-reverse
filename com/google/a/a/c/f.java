package com.google.a.a.c;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.WeakHashMap;

public final class f
{
  private static final Map<Class<?>, f> b = new WeakHashMap();
  private static final Map<Class<?>, f> c = new WeakHashMap();
  final List<String> a;
  private final Class<?> d;
  private final boolean e;
  private final IdentityHashMap<String, j> f = new IdentityHashMap();
  
  private f(Class<?> paramClass, boolean paramBoolean)
  {
    this.d = paramClass;
    this.e = paramBoolean;
    if ((!paramBoolean) || (!paramClass.isEnum())) {}
    TreeSet localTreeSet;
    Field localField;
    j localj;
    for (boolean bool = true;; bool = false)
    {
      t.a(bool, "cannot ignore case on an enum: " + paramClass);
      localTreeSet = new TreeSet(new Comparator()
      {
        public int a(String paramAnonymousString1, String paramAnonymousString2)
        {
          if (paramAnonymousString1 == paramAnonymousString2) {
            return 0;
          }
          if (paramAnonymousString1 == null) {
            return -1;
          }
          if (paramAnonymousString2 == null) {
            return 1;
          }
          return paramAnonymousString1.compareTo(paramAnonymousString2);
        }
      });
      Field[] arrayOfField = paramClass.getDeclaredFields();
      int j = arrayOfField.length;
      int i = 0;
      for (;;)
      {
        if (i >= j) {
          break label270;
        }
        localField = arrayOfField[i];
        localj = j.a(localField);
        if (localj != null) {
          break;
        }
        i += 1;
      }
    }
    String str = localj.b();
    Object localObject1 = str;
    if (paramBoolean) {
      localObject1 = str.toLowerCase().intern();
    }
    Object localObject2 = (j)this.f.get(localObject1);
    if (localObject2 == null)
    {
      bool = true;
      label177:
      if (!paramBoolean) {
        break label253;
      }
      str = "case-insensitive ";
      label185:
      if (localObject2 != null) {
        break label260;
      }
    }
    label253:
    label260:
    for (localObject2 = null;; localObject2 = ((j)localObject2).a())
    {
      t.a(bool, "two fields have the same %sname <%s>: %s and %s", new Object[] { str, localObject1, localField, localObject2 });
      this.f.put(localObject1, localj);
      localTreeSet.add(localObject1);
      break;
      bool = false;
      break label177;
      str = "";
      break label185;
    }
    label270:
    paramClass = paramClass.getSuperclass();
    if (paramClass != null)
    {
      paramClass = a(paramClass, paramBoolean);
      localTreeSet.addAll(paramClass.a);
      paramClass = paramClass.f.entrySet().iterator();
      while (paramClass.hasNext())
      {
        localObject1 = (Map.Entry)paramClass.next();
        str = (String)((Map.Entry)localObject1).getKey();
        if (!this.f.containsKey(str)) {
          this.f.put(str, ((Map.Entry)localObject1).getValue());
        }
      }
    }
    if (localTreeSet.isEmpty()) {}
    for (paramClass = Collections.emptyList();; paramClass = Collections.unmodifiableList(new ArrayList(localTreeSet)))
    {
      this.a = paramClass;
      return;
    }
  }
  
  public static f a(Class<?> paramClass)
  {
    return a(paramClass, false);
  }
  
  public static f a(Class<?> paramClass, boolean paramBoolean)
  {
    if (paramClass == null) {
      return null;
    }
    if (paramBoolean) {}
    for (;;)
    {
      synchronized (c)
      {
        f localf2 = (f)???.get(paramClass);
        f localf1 = localf2;
        if (localf2 == null)
        {
          localf1 = new f(paramClass, paramBoolean);
          ???.put(paramClass, localf1);
        }
        return localf1;
      }
      ??? = b;
    }
  }
  
  public j a(String paramString)
  {
    String str = paramString;
    if (paramString != null)
    {
      str = paramString;
      if (this.e) {
        str = paramString.toLowerCase();
      }
      str = str.intern();
    }
    return (j)this.f.get(str);
  }
  
  public final boolean a()
  {
    return this.e;
  }
  
  public boolean b()
  {
    return this.d.isEnum();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/c/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */