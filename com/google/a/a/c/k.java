package com.google.a.a.c;

import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class k
  extends AbstractMap<String, Object>
  implements Cloneable
{
  Map<String, Object> a = a.a();
  final f b;
  
  public k()
  {
    this(EnumSet.noneOf(c.class));
  }
  
  public k(EnumSet<c> paramEnumSet)
  {
    this.b = f.a(getClass(), paramEnumSet.contains(c.a));
  }
  
  public k b(String paramString, Object paramObject)
  {
    Object localObject = this.b.a(paramString);
    if (localObject != null)
    {
      ((j)localObject).a(this, paramObject);
      return this;
    }
    localObject = paramString;
    if (this.b.a()) {
      localObject = paramString.toLowerCase();
    }
    this.a.put(localObject, paramObject);
    return this;
  }
  
  public final Object c(String paramString, Object paramObject)
  {
    Object localObject = this.b.a(paramString);
    if (localObject != null)
    {
      paramString = ((j)localObject).a(this);
      ((j)localObject).a(this, paramObject);
      return paramString;
    }
    localObject = paramString;
    if (this.b.a()) {
      localObject = paramString.toLowerCase();
    }
    return this.a.put(localObject, paramObject);
  }
  
  public Set<Map.Entry<String, Object>> entrySet()
  {
    return new b();
  }
  
  public k f()
  {
    try
    {
      k localk = (k)super.clone();
      g.a(this, localk);
      localk.a = ((Map)g.c(this.a));
      return localk;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      throw new IllegalStateException(localCloneNotSupportedException);
    }
  }
  
  public final f g()
  {
    return this.b;
  }
  
  public final Object get(Object paramObject)
  {
    if (!(paramObject instanceof String)) {
      return null;
    }
    String str = (String)paramObject;
    paramObject = this.b.a(str);
    if (paramObject != null) {
      return ((j)paramObject).a(this);
    }
    paramObject = str;
    if (this.b.a()) {
      paramObject = str.toLowerCase();
    }
    return this.a.get(paramObject);
  }
  
  public final void putAll(Map<? extends String, ?> paramMap)
  {
    paramMap = paramMap.entrySet().iterator();
    while (paramMap.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)paramMap.next();
      b((String)localEntry.getKey(), localEntry.getValue());
    }
  }
  
  public final Object remove(Object paramObject)
  {
    if (!(paramObject instanceof String)) {
      return null;
    }
    String str = (String)paramObject;
    if (this.b.a(str) != null) {
      throw new UnsupportedOperationException();
    }
    paramObject = str;
    if (this.b.a()) {
      paramObject = str.toLowerCase();
    }
    return this.a.remove(paramObject);
  }
  
  final class a
    implements Iterator<Map.Entry<String, Object>>
  {
    private boolean b;
    private final Iterator<Map.Entry<String, Object>> c;
    private final Iterator<Map.Entry<String, Object>> d;
    
    a(h.c paramc)
    {
      this.c = paramc.a();
      this.d = k.this.a.entrySet().iterator();
    }
    
    public Map.Entry<String, Object> a()
    {
      if (!this.b)
      {
        if (this.c.hasNext()) {
          return (Map.Entry)this.c.next();
        }
        this.b = true;
      }
      return (Map.Entry)this.d.next();
    }
    
    public boolean hasNext()
    {
      return (this.c.hasNext()) || (this.d.hasNext());
    }
    
    public void remove()
    {
      if (this.b) {
        this.d.remove();
      }
      this.c.remove();
    }
  }
  
  final class b
    extends AbstractSet<Map.Entry<String, Object>>
  {
    private final h.c b = new h(k.this, k.this.b.a()).a();
    
    b() {}
    
    public void clear()
    {
      k.this.a.clear();
      this.b.clear();
    }
    
    public Iterator<Map.Entry<String, Object>> iterator()
    {
      return new k.a(k.this, this.b);
    }
    
    public int size()
    {
      return k.this.a.size() + this.b.size();
    }
  }
  
  public static enum c
  {
    private c() {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/c/k.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */