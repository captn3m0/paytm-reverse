package com.google.a.a.c;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class o
  extends FilterInputStream
{
  private final n a;
  
  public o(InputStream paramInputStream, Logger paramLogger, Level paramLevel, int paramInt)
  {
    super(paramInputStream);
    this.a = new n(paramLogger, paramLevel, paramInt);
  }
  
  public void close()
    throws IOException
  {
    this.a.close();
    super.close();
  }
  
  public int read()
    throws IOException
  {
    int i = super.read();
    this.a.write(i);
    return i;
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    paramInt2 = super.read(paramArrayOfByte, paramInt1, paramInt2);
    if (paramInt2 > 0) {
      this.a.write(paramArrayOfByte, paramInt1, paramInt2);
    }
    return paramInt2;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/c/o.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */