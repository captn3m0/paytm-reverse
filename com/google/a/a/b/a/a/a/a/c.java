package com.google.a.a.b.a.a.a.a;

import javax.annotation.Nullable;

public final class c
{
  public static void a(@Nullable Throwable paramThrowable)
  {
    a(paramThrowable, Error.class);
    a(paramThrowable, RuntimeException.class);
  }
  
  public static <X extends Throwable> void a(@Nullable Throwable paramThrowable, Class<X> paramClass)
    throws Throwable
  {
    if ((paramThrowable != null) && (paramClass.isInstance(paramThrowable))) {
      throw ((Throwable)paramClass.cast(paramThrowable));
    }
  }
  
  public static RuntimeException b(Throwable paramThrowable)
  {
    a((Throwable)b.a(paramThrowable));
    throw new RuntimeException(paramThrowable);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/b/a/a/a/a/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */