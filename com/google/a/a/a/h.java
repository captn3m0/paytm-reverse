package com.google.a.a.a;

import com.google.a.a.c.b;
import com.google.a.a.c.f;
import com.google.a.a.c.g;
import com.google.a.a.c.j;
import com.google.a.a.c.k;
import com.google.a.a.c.k.c;
import com.google.a.a.c.m;
import com.google.a.a.c.w;
import com.google.a.a.c.y;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class h
  extends k
{
  @m(a="Accept-Encoding")
  private List<String> c = new ArrayList(Collections.singleton("gzip"));
  @m(a="Authorization")
  private List<String> d;
  @m(a="Content-Type")
  private List<String> e;
  @m(a="If-Modified-Since")
  private List<String> f;
  @m(a="If-Match")
  private List<String> g;
  @m(a="If-None-Match")
  private List<String> h;
  @m(a="If-Unmodified-Since")
  private List<String> i;
  @m(a="If-Range")
  private List<String> j;
  @m(a="Location")
  private List<String> k;
  @m(a="User-Agent")
  private List<String> l;
  
  public h()
  {
    super(EnumSet.of(k.c.a));
  }
  
  private static Object a(Type paramType, List<Type> paramList, String paramString)
  {
    return g.a(g.a(paramList, paramType), paramString);
  }
  
  private static String a(Object paramObject)
  {
    if ((paramObject instanceof Enum)) {
      return j.a((Enum)paramObject).b();
    }
    return paramObject.toString();
  }
  
  static void a(h paramh, StringBuilder paramStringBuilder1, StringBuilder paramStringBuilder2, Logger paramLogger, t paramt)
    throws IOException
  {
    a(paramh, paramStringBuilder1, paramStringBuilder2, paramLogger, paramt, null);
  }
  
  static void a(h paramh, StringBuilder paramStringBuilder1, StringBuilder paramStringBuilder2, Logger paramLogger, t paramt, Writer paramWriter)
    throws IOException
  {
    HashSet localHashSet = new HashSet();
    Iterator localIterator = paramh.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Object localObject1 = (Map.Entry)localIterator.next();
      Object localObject2 = (String)((Map.Entry)localObject1).getKey();
      com.google.a.a.c.t.a(localHashSet.add(localObject2), "multiple headers of the same name (headers are case insensitive): %s", new Object[] { localObject2 });
      Object localObject3 = ((Map.Entry)localObject1).getValue();
      if (localObject3 != null)
      {
        localObject1 = localObject2;
        localObject2 = paramh.g().a((String)localObject2);
        if (localObject2 != null) {
          localObject1 = ((j)localObject2).b();
        }
        localObject2 = localObject3.getClass();
        if (((localObject3 instanceof Iterable)) || (((Class)localObject2).isArray()))
        {
          localObject2 = y.a(localObject3).iterator();
          while (((Iterator)localObject2).hasNext()) {
            a(paramLogger, paramStringBuilder1, paramStringBuilder2, paramt, (String)localObject1, ((Iterator)localObject2).next(), paramWriter);
          }
        }
        else
        {
          a(paramLogger, paramStringBuilder1, paramStringBuilder2, paramt, (String)localObject1, localObject3, paramWriter);
        }
      }
    }
    if (paramWriter != null) {
      paramWriter.flush();
    }
  }
  
  private static void a(Logger paramLogger, StringBuilder paramStringBuilder1, StringBuilder paramStringBuilder2, t paramt, String paramString, Object paramObject, Writer paramWriter)
    throws IOException
  {
    if ((paramObject == null) || (g.a(paramObject))) {}
    String str;
    do
    {
      return;
      str = a(paramObject);
      paramObject = str;
      Object localObject;
      if (!"Authorization".equalsIgnoreCase(paramString))
      {
        localObject = paramObject;
        if (!"Cookie".equalsIgnoreCase(paramString)) {}
      }
      else if (paramLogger != null)
      {
        localObject = paramObject;
        if (paramLogger.isLoggable(Level.ALL)) {}
      }
      else
      {
        localObject = "<Not Logged>";
      }
      if (paramStringBuilder1 != null)
      {
        paramStringBuilder1.append(paramString).append(": ");
        paramStringBuilder1.append((String)localObject);
        paramStringBuilder1.append(w.a);
      }
      if (paramStringBuilder2 != null) {
        paramStringBuilder2.append(" -H '").append(paramString).append(": ").append((String)localObject).append("'");
      }
      if (paramt != null) {
        paramt.a(paramString, str);
      }
    } while (paramWriter == null);
    paramWriter.write(paramString);
    paramWriter.write(": ");
    paramWriter.write(str);
    paramWriter.write("\r\n");
  }
  
  private <T> T b(List<T> paramList)
  {
    if (paramList == null) {
      return null;
    }
    return (T)paramList.get(0);
  }
  
  private <T> List<T> b(T paramT)
  {
    if (paramT == null) {
      return null;
    }
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(paramT);
    return localArrayList;
  }
  
  public h a()
  {
    return (h)super.f();
  }
  
  public h a(String paramString)
  {
    return a(b(paramString));
  }
  
  public h a(String paramString, Object paramObject)
  {
    return (h)super.b(paramString, paramObject);
  }
  
  public h a(List<String> paramList)
  {
    this.d = paramList;
    return this;
  }
  
  public final void a(u paramu, StringBuilder paramStringBuilder)
    throws IOException
  {
    clear();
    paramStringBuilder = new a(this, paramStringBuilder);
    int n = paramu.g();
    int m = 0;
    while (m < n)
    {
      a(paramu.a(m), paramu.b(m), paramStringBuilder);
      m += 1;
    }
    paramStringBuilder.a();
  }
  
  void a(String paramString1, String paramString2, a parama)
  {
    Object localObject1 = parama.d;
    Object localObject2 = parama.c;
    b localb = parama.a;
    parama = parama.b;
    if (parama != null) {
      parama.append(paramString1 + ": " + paramString2).append(w.a);
    }
    localObject2 = ((f)localObject2).a(paramString1);
    if (localObject2 != null)
    {
      Type localType = g.a((List)localObject1, ((j)localObject2).d());
      if (y.a(localType))
      {
        paramString1 = y.a((List)localObject1, y.b(localType));
        localb.a(((j)localObject2).a(), paramString1, a(paramString1, (List)localObject1, paramString2));
        return;
      }
      if (y.a(y.a((List)localObject1, localType), Iterable.class))
      {
        parama = (Collection)((j)localObject2).a(this);
        paramString1 = parama;
        if (parama == null)
        {
          paramString1 = g.b(localType);
          ((j)localObject2).a(this, paramString1);
        }
        if (localType == Object.class) {}
        for (parama = null;; parama = y.c(localType))
        {
          paramString1.add(a(parama, (List)localObject1, paramString2));
          return;
        }
      }
      ((j)localObject2).a(this, a(localType, (List)localObject1, paramString2));
      return;
    }
    localObject1 = (ArrayList)get(paramString1);
    parama = (a)localObject1;
    if (localObject1 == null)
    {
      parama = new ArrayList();
      a(paramString1, parama);
    }
    parama.add(paramString2);
  }
  
  public h b(String paramString)
  {
    this.f = b(paramString);
    return this;
  }
  
  public final String b()
  {
    return (String)b(this.e);
  }
  
  public h c(String paramString)
  {
    this.g = b(paramString);
    return this;
  }
  
  public final String c()
  {
    return (String)b(this.k);
  }
  
  public h d(String paramString)
  {
    this.h = b(paramString);
    return this;
  }
  
  public final String d()
  {
    return (String)b(this.l);
  }
  
  public h e(String paramString)
  {
    this.i = b(paramString);
    return this;
  }
  
  public h f(String paramString)
  {
    this.j = b(paramString);
    return this;
  }
  
  public h g(String paramString)
  {
    this.l = b(paramString);
    return this;
  }
  
  private static final class a
  {
    final b a;
    final StringBuilder b;
    final f c;
    final List<Type> d;
    
    public a(h paramh, StringBuilder paramStringBuilder)
    {
      Class localClass = paramh.getClass();
      this.d = Arrays.asList(new Type[] { localClass });
      this.c = f.a(localClass, true);
      this.b = paramStringBuilder;
      this.a = new b(paramh);
    }
    
    void a()
    {
      this.a.a();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/a/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */