package com.google.a.a.a;

import com.google.a.a.c.a.a;
import com.google.a.a.c.a.b;
import com.google.a.a.c.k;
import com.google.a.a.c.t;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

public class c
  extends k
{
  private static final b c = new com.google.a.a.c.a.c("=&-_.!~*'()@:$,;/?:", false);
  private String d;
  private String e;
  private String f;
  private int g = -1;
  private List<String> h;
  private String i;
  
  public c() {}
  
  public c(String paramString)
  {
    this(c(paramString));
  }
  
  private c(String paramString1, String paramString2, int paramInt, String paramString3, String paramString4, String paramString5, String paramString6)
  {
    this.d = paramString1.toLowerCase();
    this.e = paramString2;
    this.g = paramInt;
    this.h = b(paramString3);
    if (paramString4 != null) {}
    for (paramString1 = a.b(paramString4);; paramString1 = null)
    {
      this.i = paramString1;
      if (paramString5 != null) {
        w.a(paramString5, this);
      }
      paramString1 = (String)localObject;
      if (paramString6 != null) {
        paramString1 = a.b(paramString6);
      }
      this.f = paramString1;
      return;
    }
  }
  
  public c(URL paramURL)
  {
    this(paramURL.getProtocol(), paramURL.getHost(), paramURL.getPort(), paramURL.getPath(), paramURL.getRef(), paramURL.getQuery(), paramURL.getUserInfo());
  }
  
  private void a(StringBuilder paramStringBuilder)
  {
    int k = this.h.size();
    int j = 0;
    while (j < k)
    {
      String str = (String)this.h.get(j);
      if (j != 0) {
        paramStringBuilder.append('/');
      }
      if (str.length() != 0) {
        paramStringBuilder.append(a.c(str));
      }
      j += 1;
    }
  }
  
  static void a(Set<Map.Entry<String, Object>> paramSet, StringBuilder paramStringBuilder)
  {
    boolean bool1 = true;
    paramSet = paramSet.iterator();
    while (paramSet.hasNext())
    {
      Object localObject2 = (Map.Entry)paramSet.next();
      Object localObject1 = ((Map.Entry)localObject2).getValue();
      if (localObject1 != null)
      {
        localObject2 = a.e((String)((Map.Entry)localObject2).getKey());
        if ((localObject1 instanceof Collection))
        {
          localObject1 = ((Collection)localObject1).iterator();
          for (boolean bool2 = bool1;; bool2 = a(bool2, paramStringBuilder, (String)localObject2, ((Iterator)localObject1).next()))
          {
            bool1 = bool2;
            if (!((Iterator)localObject1).hasNext()) {
              break;
            }
          }
        }
        bool1 = a(bool1, paramStringBuilder, (String)localObject2, localObject1);
      }
    }
  }
  
  private static boolean a(boolean paramBoolean, StringBuilder paramStringBuilder, String paramString, Object paramObject)
  {
    if (paramBoolean)
    {
      paramBoolean = false;
      paramStringBuilder.append('?');
    }
    for (;;)
    {
      paramStringBuilder.append(paramString);
      paramString = a.e(paramObject.toString());
      if (paramString.length() != 0) {
        paramStringBuilder.append('=').append(paramString);
      }
      return paramBoolean;
      paramStringBuilder.append('&');
    }
  }
  
  public static List<String> b(String paramString)
  {
    if ((paramString == null) || (paramString.length() == 0)) {
      localObject = null;
    }
    ArrayList localArrayList;
    int k;
    int j;
    do
    {
      return (List<String>)localObject;
      localArrayList = new ArrayList();
      k = 0;
      j = 1;
      localObject = localArrayList;
    } while (j == 0);
    int m = paramString.indexOf('/', k);
    if (m != -1)
    {
      j = 1;
      label53:
      if (j == 0) {
        break label90;
      }
    }
    label90:
    for (Object localObject = paramString.substring(k, m);; localObject = paramString.substring(k))
    {
      localArrayList.add(a.b((String)localObject));
      k = m + 1;
      break;
      j = 0;
      break label53;
    }
  }
  
  private static URL c(String paramString)
  {
    try
    {
      paramString = new URL(paramString);
      return paramString;
    }
    catch (MalformedURLException paramString)
    {
      throw new IllegalArgumentException(paramString);
    }
  }
  
  public c a()
  {
    c localc = (c)super.f();
    if (this.h != null) {
      localc.h = new ArrayList(this.h);
    }
    return localc;
  }
  
  public c a(String paramString, Object paramObject)
  {
    return (c)super.b(paramString, paramObject);
  }
  
  public final URL a(String paramString)
  {
    try
    {
      paramString = new URL(e(), paramString);
      return paramString;
    }
    catch (MalformedURLException paramString)
    {
      throw new IllegalArgumentException(paramString);
    }
  }
  
  public final String b()
  {
    return c() + d();
  }
  
  public final String c()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append((String)t.a(this.d));
    localStringBuilder.append("://");
    if (this.f != null) {
      localStringBuilder.append(a.d(this.f)).append('@');
    }
    localStringBuilder.append((String)t.a(this.e));
    int j = this.g;
    if (j != -1) {
      localStringBuilder.append(':').append(j);
    }
    return localStringBuilder.toString();
  }
  
  public final String d()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if (this.h != null) {
      a(localStringBuilder);
    }
    a(entrySet(), localStringBuilder);
    String str = this.i;
    if (str != null) {
      localStringBuilder.append('#').append(c.a(str));
    }
    return localStringBuilder.toString();
  }
  
  public final URL e()
  {
    return c(b());
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((!super.equals(paramObject)) || (!(paramObject instanceof c))) {
      return false;
    }
    paramObject = (c)paramObject;
    return b().equals(((c)paramObject).toString());
  }
  
  public int hashCode()
  {
    return b().hashCode();
  }
  
  public String toString()
  {
    return b();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/a/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */