package com.google.a.a.a;

import com.google.a.a.c.g;
import com.google.a.a.c.j;
import com.google.a.a.c.t;
import com.google.a.a.c.y;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class v
  extends a
{
  private Object a;
  
  public v(Object paramObject)
  {
    super(w.a);
    a(paramObject);
  }
  
  private static boolean a(boolean paramBoolean, Writer paramWriter, String paramString, Object paramObject)
    throws IOException
  {
    boolean bool = paramBoolean;
    if (paramObject != null)
    {
      if (g.a(paramObject)) {
        bool = paramBoolean;
      }
    }
    else {
      return bool;
    }
    if (paramBoolean)
    {
      paramBoolean = false;
      label26:
      paramWriter.write(paramString);
      if (!(paramObject instanceof Enum)) {
        break label86;
      }
    }
    label86:
    for (paramString = j.a((Enum)paramObject).b();; paramString = paramObject.toString())
    {
      paramString = com.google.a.a.c.a.a.a(paramString);
      bool = paramBoolean;
      if (paramString.length() == 0) {
        break;
      }
      paramWriter.write("=");
      paramWriter.write(paramString);
      return paramBoolean;
      paramWriter.write("&");
      break label26;
    }
  }
  
  public v a(Object paramObject)
  {
    this.a = t.a(paramObject);
    return this;
  }
  
  public void a(OutputStream paramOutputStream)
    throws IOException
  {
    paramOutputStream = new BufferedWriter(new OutputStreamWriter(paramOutputStream, b()));
    boolean bool1 = true;
    Iterator localIterator = g.b(this.a).entrySet().iterator();
    while (localIterator.hasNext())
    {
      Object localObject2 = (Map.Entry)localIterator.next();
      Object localObject1 = ((Map.Entry)localObject2).getValue();
      if (localObject1 != null)
      {
        localObject2 = com.google.a.a.c.a.a.a((String)((Map.Entry)localObject2).getKey());
        Class localClass = localObject1.getClass();
        if (((localObject1 instanceof Iterable)) || (localClass.isArray()))
        {
          localObject1 = y.a(localObject1).iterator();
          for (boolean bool2 = bool1;; bool2 = a(bool2, paramOutputStream, (String)localObject2, ((Iterator)localObject1).next()))
          {
            bool1 = bool2;
            if (!((Iterator)localObject1).hasNext()) {
              break;
            }
          }
        }
        bool1 = a(bool1, paramOutputStream, (String)localObject2, localObject1);
      }
    }
    paramOutputStream.flush();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/a/v.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */