package com.google.a.a.a;

import com.google.a.a.c.e;
import com.google.a.a.c.l;
import com.google.a.a.c.w;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class n
{
  u a;
  private InputStream b;
  private final String c;
  private final String d;
  private final j e;
  private final int f;
  private final String g;
  private final k h;
  private int i;
  private boolean j;
  private boolean k;
  
  n(k paramk, u paramu)
    throws IOException
  {
    this.h = paramk;
    this.i = paramk.a();
    this.j = paramk.b();
    this.a = paramu;
    this.c = paramu.b();
    int n = paramu.e();
    int m = n;
    if (n < 0) {
      m = 0;
    }
    this.f = m;
    Object localObject1 = paramu.f();
    this.g = ((String)localObject1);
    Logger localLogger = r.a;
    StringBuilder localStringBuilder;
    if ((this.j) && (localLogger.isLoggable(Level.CONFIG)))
    {
      m = 1;
      localStringBuilder = null;
      if (m != 0)
      {
        localStringBuilder = new StringBuilder();
        localStringBuilder.append("-------------- RESPONSE --------------").append(w.a);
        localObject3 = paramu.d();
        if (localObject3 == null) {
          break label238;
        }
        localStringBuilder.append((String)localObject3);
        label148:
        localStringBuilder.append(w.a);
      }
      Object localObject3 = paramk.c();
      if (m == 0) {
        break label269;
      }
      localObject1 = localStringBuilder;
      label171:
      ((h)localObject3).a(paramu, (StringBuilder)localObject1);
      localObject1 = paramu.c();
      paramu = (u)localObject1;
      if (localObject1 == null) {
        paramu = paramk.c().b();
      }
      this.d = paramu;
      if (paramu != null) {
        break label275;
      }
    }
    label238:
    label269:
    label275:
    for (paramk = (k)localObject2;; paramk = new j(paramu))
    {
      this.e = paramk;
      if (m != 0) {
        localLogger.config(localStringBuilder.toString());
      }
      return;
      m = 0;
      break;
      localStringBuilder.append(this.f);
      if (localObject1 == null) {
        break label148;
      }
      localStringBuilder.append(' ').append((String)localObject1);
      break label148;
      localObject1 = null;
      break label171;
    }
  }
  
  public h a()
  {
    return this.h.c();
  }
  
  public boolean b()
  {
    return q.a(this.f);
  }
  
  public int c()
  {
    return this.f;
  }
  
  public String d()
  {
    return this.g;
  }
  
  /* Error */
  public InputStream e()
    throws IOException
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 136	com/google/a/a/a/n:k	Z
    //   4: ifne +119 -> 123
    //   7: aload_0
    //   8: getfield 45	com/google/a/a/a/n:a	Lcom/google/a/a/a/u;
    //   11: invokevirtual 138	com/google/a/a/a/u:a	()Ljava/io/InputStream;
    //   14: astore_1
    //   15: aload_1
    //   16: ifnull +102 -> 118
    //   19: aload_1
    //   20: astore_3
    //   21: aload_1
    //   22: astore_2
    //   23: aload_0
    //   24: getfield 52	com/google/a/a/a/n:c	Ljava/lang/String;
    //   27: astore 4
    //   29: aload 4
    //   31: ifnull +137 -> 168
    //   34: aload_1
    //   35: astore_3
    //   36: aload_1
    //   37: astore_2
    //   38: aload 4
    //   40: ldc -116
    //   42: invokevirtual 146	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   45: ifeq +123 -> 168
    //   48: aload_1
    //   49: astore_3
    //   50: aload_1
    //   51: astore_2
    //   52: new 148	java/util/zip/GZIPInputStream
    //   55: dup
    //   56: aload_1
    //   57: invokespecial 151	java/util/zip/GZIPInputStream:<init>	(Ljava/io/InputStream;)V
    //   60: astore_1
    //   61: getstatic 65	com/google/a/a/a/r:a	Ljava/util/logging/Logger;
    //   64: astore_2
    //   65: aload_0
    //   66: getfield 43	com/google/a/a/a/n:j	Z
    //   69: ifeq +96 -> 165
    //   72: aload_2
    //   73: getstatic 71	java/util/logging/Level:CONFIG	Ljava/util/logging/Level;
    //   76: invokevirtual 77	java/util/logging/Logger:isLoggable	(Ljava/util/logging/Level;)Z
    //   79: ifeq +86 -> 165
    //   82: new 153	com/google/a/a/c/o
    //   85: dup
    //   86: aload_1
    //   87: aload_2
    //   88: getstatic 71	java/util/logging/Level:CONFIG	Ljava/util/logging/Level;
    //   91: aload_0
    //   92: getfield 38	com/google/a/a/a/n:i	I
    //   95: invokespecial 156	com/google/a/a/c/o:<init>	(Ljava/io/InputStream;Ljava/util/logging/Logger;Ljava/util/logging/Level;I)V
    //   98: astore_2
    //   99: aload_2
    //   100: astore_1
    //   101: aload_1
    //   102: astore_3
    //   103: aload_1
    //   104: astore_2
    //   105: aload_0
    //   106: aload_1
    //   107: putfield 158	com/google/a/a/a/n:b	Ljava/io/InputStream;
    //   110: iconst_1
    //   111: ifne +7 -> 118
    //   114: aload_1
    //   115: invokevirtual 163	java/io/InputStream:close	()V
    //   118: aload_0
    //   119: iconst_1
    //   120: putfield 136	com/google/a/a/a/n:k	Z
    //   123: aload_0
    //   124: getfield 158	com/google/a/a/a/n:b	Ljava/io/InputStream;
    //   127: areturn
    //   128: astore_1
    //   129: iconst_0
    //   130: ifne -12 -> 118
    //   133: aload_3
    //   134: invokevirtual 163	java/io/InputStream:close	()V
    //   137: goto -19 -> 118
    //   140: astore_1
    //   141: iconst_0
    //   142: ifne +7 -> 149
    //   145: aload_2
    //   146: invokevirtual 163	java/io/InputStream:close	()V
    //   149: aload_1
    //   150: athrow
    //   151: astore_3
    //   152: aload_1
    //   153: astore_2
    //   154: aload_3
    //   155: astore_1
    //   156: goto -15 -> 141
    //   159: astore_2
    //   160: aload_1
    //   161: astore_3
    //   162: goto -33 -> 129
    //   165: goto -64 -> 101
    //   168: goto -107 -> 61
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	171	0	this	n
    //   14	101	1	localObject1	Object
    //   128	1	1	localEOFException1	java.io.EOFException
    //   140	13	1	localObject2	Object
    //   155	6	1	localObject3	Object
    //   22	132	2	localObject4	Object
    //   159	1	2	localEOFException2	java.io.EOFException
    //   20	114	3	localObject5	Object
    //   151	4	3	localObject6	Object
    //   161	1	3	localObject7	Object
    //   27	12	4	str	String
    // Exception table:
    //   from	to	target	type
    //   23	29	128	java/io/EOFException
    //   38	48	128	java/io/EOFException
    //   52	61	128	java/io/EOFException
    //   105	110	128	java/io/EOFException
    //   23	29	140	finally
    //   38	48	140	finally
    //   52	61	140	finally
    //   105	110	140	finally
    //   61	99	151	finally
    //   61	99	159	java/io/EOFException
  }
  
  public void f()
    throws IOException
  {
    InputStream localInputStream = e();
    if (localInputStream != null) {
      localInputStream.close();
    }
  }
  
  public void g()
    throws IOException
  {
    f();
    this.a.h();
  }
  
  public String h()
    throws IOException
  {
    InputStream localInputStream = e();
    if (localInputStream == null) {
      return "";
    }
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    l.a(localInputStream, localByteArrayOutputStream);
    return localByteArrayOutputStream.toString(i().name());
  }
  
  public Charset i()
  {
    if ((this.e == null) || (this.e.d() == null)) {
      return e.b;
    }
    return this.e.d();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/a/n.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */