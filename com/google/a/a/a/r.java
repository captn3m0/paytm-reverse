package com.google.a.a.a;

import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Logger;

public abstract class r
{
  static final Logger a = Logger.getLogger(r.class.getName());
  private static final String[] b = { "DELETE", "GET", "POST", "PUT" };
  
  static
  {
    Arrays.sort(b);
  }
  
  public final l a()
  {
    return a(null);
  }
  
  public final l a(m paramm)
  {
    return new l(this, paramm);
  }
  
  protected abstract t a(String paramString1, String paramString2)
    throws IOException;
  
  public boolean a(String paramString)
    throws IOException
  {
    return Arrays.binarySearch(b, paramString) >= 0;
  }
  
  k b()
  {
    return new k(this, null);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/a/r.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */