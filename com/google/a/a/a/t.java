package com.google.a.a.a;

import com.google.a.a.c.v;
import java.io.IOException;

public abstract class t
{
  private long a = -1L;
  private String b;
  private String c;
  private v d;
  
  public final long a()
  {
    return this.a;
  }
  
  public void a(int paramInt1, int paramInt2)
    throws IOException
  {}
  
  public final void a(long paramLong)
    throws IOException
  {
    this.a = paramLong;
  }
  
  public final void a(v paramv)
    throws IOException
  {
    this.d = paramv;
  }
  
  public final void a(String paramString)
    throws IOException
  {
    this.b = paramString;
  }
  
  public abstract void a(String paramString1, String paramString2)
    throws IOException;
  
  public final String b()
  {
    return this.b;
  }
  
  public final void b(String paramString)
    throws IOException
  {
    this.c = paramString;
  }
  
  public final String c()
  {
    return this.c;
  }
  
  public final v d()
  {
    return this.d;
  }
  
  public abstract u e()
    throws IOException;
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/a/t.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */