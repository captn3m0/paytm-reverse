package com.google.a.a.a.a;

import com.google.a.a.a.r;
import com.google.a.a.c.t;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.util.Arrays;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;

public final class c
  extends r
{
  private static final String[] b = { "DELETE", "GET", "HEAD", "OPTIONS", "POST", "PUT", "TRACE" };
  private final Proxy c;
  private final SSLSocketFactory d;
  private final HostnameVerifier e;
  
  static
  {
    Arrays.sort(b);
  }
  
  public c()
  {
    this(null, null, null);
  }
  
  c(Proxy paramProxy, SSLSocketFactory paramSSLSocketFactory, HostnameVerifier paramHostnameVerifier)
  {
    this.c = paramProxy;
    this.d = paramSSLSocketFactory;
    this.e = paramHostnameVerifier;
  }
  
  public boolean a(String paramString)
  {
    return Arrays.binarySearch(b, paramString) >= 0;
  }
  
  protected a b(String paramString1, String paramString2)
    throws IOException
  {
    t.a(a(paramString1), "HTTP method %s not supported", new Object[] { paramString1 });
    paramString2 = new URL(paramString2);
    if (this.c == null) {}
    for (paramString2 = paramString2.openConnection();; paramString2 = paramString2.openConnection(this.c))
    {
      paramString2 = (HttpURLConnection)paramString2;
      paramString2.setRequestMethod(paramString1);
      if ((paramString2 instanceof HttpsURLConnection))
      {
        paramString1 = (HttpsURLConnection)paramString2;
        if (this.e != null) {
          paramString1.setHostnameVerifier(this.e);
        }
        if (this.d != null) {
          paramString1.setSSLSocketFactory(this.d);
        }
      }
      return new a(paramString2);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/a/a/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */