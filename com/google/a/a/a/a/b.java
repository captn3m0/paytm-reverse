package com.google.a.a.a.a;

import com.google.a.a.a.u;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

final class b
  extends u
{
  private final HttpURLConnection a;
  private final int b;
  private final String c;
  private final ArrayList<String> d = new ArrayList();
  private final ArrayList<String> e = new ArrayList();
  
  b(HttpURLConnection paramHttpURLConnection)
    throws IOException
  {
    this.a = paramHttpURLConnection;
    int j = paramHttpURLConnection.getResponseCode();
    int i = j;
    if (j == -1) {
      i = 0;
    }
    this.b = i;
    this.c = paramHttpURLConnection.getResponseMessage();
    ArrayList localArrayList1 = this.d;
    ArrayList localArrayList2 = this.e;
    paramHttpURLConnection = paramHttpURLConnection.getHeaderFields().entrySet().iterator();
    while (paramHttpURLConnection.hasNext())
    {
      Object localObject = (Map.Entry)paramHttpURLConnection.next();
      String str1 = (String)((Map.Entry)localObject).getKey();
      if (str1 != null)
      {
        localObject = ((List)((Map.Entry)localObject).getValue()).iterator();
        while (((Iterator)localObject).hasNext())
        {
          String str2 = (String)((Iterator)localObject).next();
          if (str2 != null)
          {
            localArrayList1.add(str1);
            localArrayList2.add(str2);
          }
        }
      }
    }
  }
  
  public InputStream a()
    throws IOException
  {
    try
    {
      InputStream localInputStream = this.a.getInputStream();
      return localInputStream;
    }
    catch (IOException localIOException) {}
    return this.a.getErrorStream();
  }
  
  public String a(int paramInt)
  {
    return (String)this.d.get(paramInt);
  }
  
  public String b()
  {
    return this.a.getContentEncoding();
  }
  
  public String b(int paramInt)
  {
    return (String)this.e.get(paramInt);
  }
  
  public String c()
  {
    return this.a.getHeaderField("Content-Type");
  }
  
  public String d()
  {
    String str = this.a.getHeaderField(0);
    if ((str != null) && (str.startsWith("HTTP/1."))) {
      return str;
    }
    return null;
  }
  
  public int e()
  {
    return this.b;
  }
  
  public String f()
  {
    return this.c;
  }
  
  public int g()
  {
    return this.d.size();
  }
  
  public void h()
  {
    this.a.disconnect();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/a/a/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */