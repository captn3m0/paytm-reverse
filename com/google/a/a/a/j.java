package com.google.a.a.a;

import com.google.a.a.c.t;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class j
{
  private static final Pattern a = Pattern.compile("[\\w!#$&.+\\-\\^_]+|[*]");
  private static final Pattern b = Pattern.compile("[\\p{ASCII}&&[^\\p{Cntrl} ;/=\\[\\]\\(\\)\\<\\>\\@\\,\\:\\\"\\?\\=]]+");
  private static final Pattern c = Pattern.compile("\\s*(" + "[^\\s/=;\"]+" + ")/(" + "[^\\s/=;\"]+" + ")" + "\\s*(" + ";.*" + ")?", 32);
  private static final Pattern d;
  private String e = "application";
  private String f = "octet-stream";
  private final SortedMap<String, String> g = new TreeMap();
  private String h;
  
  static
  {
    String str = "\"([^\"]*)\"" + "|" + "[^\\s;\"]*";
    d = Pattern.compile("\\s*;\\s*(" + "[^\\s/=;\"]+" + ")" + "=(" + str + ")");
  }
  
  public j(String paramString)
  {
    f(paramString);
  }
  
  static boolean e(String paramString)
  {
    return b.matcher(paramString).matches();
  }
  
  private j f(String paramString)
  {
    paramString = c.matcher(paramString);
    t.a(paramString.matches(), "Type must be in the 'maintype/subtype; parameter=value' format");
    a(paramString.group(1));
    b(paramString.group(2));
    paramString = paramString.group(3);
    if (paramString != null)
    {
      Matcher localMatcher = d.matcher(paramString);
      while (localMatcher.find())
      {
        String str2 = localMatcher.group(1);
        String str1 = localMatcher.group(3);
        paramString = str1;
        if (str1 == null) {
          paramString = localMatcher.group(2);
        }
        a(str2, paramString);
      }
    }
    return this;
  }
  
  private static String g(String paramString)
  {
    paramString = paramString.replace("\\", "\\\\").replace("\"", "\\\"");
    return "\"" + paramString + "\"";
  }
  
  public j a(String paramString)
  {
    t.a(a.matcher(paramString).matches(), "Type contains reserved characters");
    this.e = paramString;
    this.h = null;
    return this;
  }
  
  public j a(String paramString1, String paramString2)
  {
    if (paramString2 == null)
    {
      d(paramString1);
      return this;
    }
    t.a(b.matcher(paramString1).matches(), "Name contains reserved characters");
    this.h = null;
    this.g.put(paramString1.toLowerCase(), paramString2);
    return this;
  }
  
  public j a(Charset paramCharset)
  {
    if (paramCharset == null) {}
    for (paramCharset = null;; paramCharset = paramCharset.name())
    {
      a("charset", paramCharset);
      return this;
    }
  }
  
  public String a()
  {
    return this.e;
  }
  
  public boolean a(j paramj)
  {
    return (paramj != null) && (a().equalsIgnoreCase(paramj.a())) && (b().equalsIgnoreCase(paramj.b()));
  }
  
  public j b(String paramString)
  {
    t.a(a.matcher(paramString).matches(), "Subtype contains reserved characters");
    this.f = paramString;
    this.h = null;
    return this;
  }
  
  public String b()
  {
    return this.f;
  }
  
  public String c()
  {
    if (this.h != null) {
      return this.h;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.e);
    localStringBuilder.append('/');
    localStringBuilder.append(this.f);
    if (this.g != null)
    {
      Iterator localIterator = this.g.entrySet().iterator();
      while (localIterator.hasNext())
      {
        Object localObject = (Map.Entry)localIterator.next();
        String str = (String)((Map.Entry)localObject).getValue();
        localStringBuilder.append("; ");
        localStringBuilder.append((String)((Map.Entry)localObject).getKey());
        localStringBuilder.append("=");
        localObject = str;
        if (!e(str)) {
          localObject = g(str);
        }
        localStringBuilder.append((String)localObject);
      }
    }
    this.h = localStringBuilder.toString();
    return this.h;
  }
  
  public String c(String paramString)
  {
    return (String)this.g.get(paramString.toLowerCase());
  }
  
  public j d(String paramString)
  {
    this.h = null;
    this.g.remove(paramString.toLowerCase());
    return this;
  }
  
  public Charset d()
  {
    String str = c("charset");
    if (str == null) {
      return null;
    }
    return Charset.forName(str);
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof j)) {}
    do
    {
      return false;
      paramObject = (j)paramObject;
    } while ((!a((j)paramObject)) || (!this.g.equals(((j)paramObject).g)));
    return true;
  }
  
  public int hashCode()
  {
    return c().hashCode();
  }
  
  public String toString()
  {
    return c();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/a/j.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */