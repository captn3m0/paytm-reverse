package com.google.a.a.a;

import java.io.IOException;

public final class l
{
  private final r a;
  private final m b;
  
  l(r paramr, m paramm)
  {
    this.a = paramr;
    this.b = paramm;
  }
  
  public k a(c paramc)
    throws IOException
  {
    return a("GET", paramc, null);
  }
  
  public k a(c paramc, d paramd)
    throws IOException
  {
    return a("POST", paramc, paramd);
  }
  
  public k a(String paramString, c paramc, d paramd)
    throws IOException
  {
    k localk = this.a.b();
    if (this.b != null) {
      this.b.a(localk);
    }
    localk.a(paramString);
    if (paramc != null) {
      localk.a(paramc);
    }
    if (paramd != null) {
      localk.a(paramd);
    }
    return localk;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/a/l.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */