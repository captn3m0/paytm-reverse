package com.google.a.a.a;

import com.google.a.a.c.a.a;
import com.google.a.a.c.b;
import com.google.a.a.c.e;
import com.google.a.a.c.f;
import com.google.a.a.c.g;
import com.google.a.a.c.k;
import com.google.a.a.c.x;
import com.google.a.a.c.y;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class w
{
  public static final String a = new j("application/x-www-form-urlencoded").a(e.a).c();
  
  private static Object a(Type paramType, List<Type> paramList, String paramString)
  {
    return g.a(g.a(paramList, paramType), paramString);
  }
  
  public static void a(Reader paramReader, Object paramObject)
    throws IOException
  {
    Object localObject1 = paramObject.getClass();
    f localf = f.a((Class)localObject1);
    List localList = Arrays.asList(new Type[] { localObject1 });
    k localk;
    b localb;
    Object localObject3;
    Object localObject2;
    int i;
    if (k.class.isAssignableFrom((Class)localObject1))
    {
      localk = (k)paramObject;
      if (!Map.class.isAssignableFrom((Class)localObject1)) {
        break label147;
      }
      localObject1 = (Map)paramObject;
      localb = new b(paramObject);
      localObject3 = new StringWriter();
      localObject2 = new StringWriter();
      i = 1;
    }
    for (;;)
    {
      int j = paramReader.read();
      switch (j)
      {
      default: 
        if (i != 0)
        {
          ((StringWriter)localObject3).write(j);
          continue;
          localk = null;
          break;
          localObject1 = null;
        }
        break;
      case -1: 
      case 38: 
        Object localObject4 = a.b(((StringWriter)localObject3).toString());
        String str;
        com.google.a.a.c.j localj;
        if (((String)localObject4).length() != 0)
        {
          str = a.b(((StringWriter)localObject2).toString());
          localj = localf.a((String)localObject4);
          if (localj == null) {
            break label387;
          }
          localObject4 = g.a(localList, localj.d());
          if (!y.a((Type)localObject4)) {
            break label279;
          }
          localObject2 = y.a(localList, y.b((Type)localObject4));
          localb.a(localj.a(), (Class)localObject2, a((Type)localObject2, localList, str));
        }
        while (localObject1 == null) {
          for (;;)
          {
            i = 1;
            localObject3 = new StringWriter();
            localObject2 = new StringWriter();
            if (j != -1) {
              break;
            }
            localb.a();
            return;
            if (y.a(y.a(localList, (Type)localObject4), Iterable.class))
            {
              localObject3 = (Collection)localj.a(paramObject);
              localObject2 = localObject3;
              if (localObject3 == null)
              {
                localObject2 = g.b((Type)localObject4);
                localj.a(paramObject, localObject2);
              }
              if (localObject4 == Object.class) {}
              for (localObject3 = null;; localObject3 = y.c((Type)localObject4))
              {
                ((Collection)localObject2).add(a((Type)localObject3, localList, str));
                break;
              }
            }
            localj.a(paramObject, a((Type)localObject4, localList, str));
          }
        }
        localObject3 = (ArrayList)((Map)localObject1).get(localObject4);
        localObject2 = localObject3;
        if (localObject3 == null)
        {
          localObject2 = new ArrayList();
          if (localk == null) {
            break label450;
          }
          localk.b((String)localObject4, localObject2);
        }
        for (;;)
        {
          ((ArrayList)localObject2).add(str);
          break;
          ((Map)localObject1).put(localObject4, localObject2);
        }
      case 61: 
        label147:
        label279:
        label387:
        label450:
        i = 0;
        continue;
        ((StringWriter)localObject2).write(j);
      }
    }
  }
  
  public static void a(String paramString, Object paramObject)
  {
    if (paramString == null) {
      return;
    }
    try
    {
      a(new StringReader(paramString), paramObject);
      return;
    }
    catch (IOException paramString)
    {
      throw x.a(paramString);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/a/w.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */