package com.google.a.a.a;

import com.google.a.a.c.e;
import com.google.a.a.c.l;
import java.io.IOException;
import java.nio.charset.Charset;

public abstract class a
  implements d
{
  private j a;
  private long b = -1L;
  
  protected a(j paramj)
  {
    this.a = paramj;
  }
  
  protected a(String paramString) {}
  
  public static long a(d paramd)
    throws IOException
  {
    if (!paramd.e()) {
      return -1L;
    }
    return l.a(paramd);
  }
  
  public long a()
    throws IOException
  {
    if (this.b == -1L) {
      this.b = d();
    }
    return this.b;
  }
  
  protected final Charset b()
  {
    if ((this.a == null) || (this.a.d() == null)) {
      return e.a;
    }
    return this.a.d();
  }
  
  public String c()
  {
    if (this.a == null) {
      return null;
    }
    return this.a.c();
  }
  
  protected long d()
    throws IOException
  {
    return a(this);
  }
  
  public boolean e()
  {
    return true;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */