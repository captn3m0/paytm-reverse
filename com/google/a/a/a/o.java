package com.google.a.a.a;

import com.google.a.a.c.t;
import com.google.a.a.c.w;
import java.io.IOException;

public class o
  extends IOException
{
  private static final long serialVersionUID = -1875819453475890043L;
  private final int a;
  private final String b;
  private final transient h c;
  private final String d;
  
  public o(n paramn)
  {
    this(new a(paramn));
  }
  
  protected o(a parama)
  {
    super(parama.e);
    this.a = parama.a;
    this.b = parama.b;
    this.c = parama.c;
    this.d = parama.d;
  }
  
  public static StringBuilder a(n paramn)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    int i = paramn.c();
    if (i != 0) {
      localStringBuilder.append(i);
    }
    paramn = paramn.d();
    if (paramn != null)
    {
      if (i != 0) {
        localStringBuilder.append(' ');
      }
      localStringBuilder.append(paramn);
    }
    return localStringBuilder;
  }
  
  public static class a
  {
    int a;
    String b;
    h c;
    String d;
    String e;
    
    public a(int paramInt, String paramString, h paramh)
    {
      a(paramInt);
      a(paramString);
      a(paramh);
    }
    
    public a(n paramn)
    {
      this(paramn.c(), paramn.d(), paramn.a());
      try
      {
        this.d = paramn.h();
        if (this.d.length() == 0) {
          this.d = null;
        }
        paramn = o.a(paramn);
        if (this.d != null) {
          paramn.append(w.a).append(this.d);
        }
        this.e = paramn.toString();
        return;
      }
      catch (IOException localIOException)
      {
        for (;;)
        {
          localIOException.printStackTrace();
        }
      }
    }
    
    public a a(int paramInt)
    {
      if (paramInt >= 0) {}
      for (boolean bool = true;; bool = false)
      {
        t.a(bool);
        this.a = paramInt;
        return this;
      }
    }
    
    public a a(h paramh)
    {
      this.c = ((h)t.a(paramh));
      return this;
    }
    
    public a a(String paramString)
    {
      this.b = paramString;
      return this;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/a/a/a/o.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */