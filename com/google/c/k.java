package com.google.c;

public final class k
  extends i
{
  public static final k a = new k();
  
  public boolean equals(Object paramObject)
  {
    return (this == paramObject) || ((paramObject instanceof k));
  }
  
  public int hashCode()
  {
    return k.class.hashCode();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/google/c/k.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */