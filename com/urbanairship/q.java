package com.urbanairship;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Looper;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class q
{
  static volatile boolean a = false;
  static volatile boolean b = false;
  static Application c;
  static q d;
  public static boolean e = false;
  private static final Object r = new Object();
  private static List<f> s;
  com.urbanairship.actions.d f;
  b g;
  com.urbanairship.analytics.b h;
  c i;
  l j;
  com.urbanairship.push.j k;
  com.urbanairship.richpush.b l;
  com.urbanairship.location.f m;
  com.urbanairship.c.b n;
  com.urbanairship.push.iam.d o;
  g p;
  com.urbanairship.messagecenter.d q;
  
  q(@NonNull b paramb)
  {
    this.g = paramb;
  }
  
  @NonNull
  public static e a(a parama)
  {
    return a(parama, null);
  }
  
  @NonNull
  public static e a(a arg0, Looper paramLooper)
  {
    paramLooper = new q.1(paramLooper, ???);
    synchronized (r)
    {
      if (a)
      {
        paramLooper.run();
        return paramLooper;
      }
      if (s == null) {
        s = new ArrayList();
      }
      s.add(paramLooper);
    }
  }
  
  @NonNull
  public static q a()
  {
    synchronized (r)
    {
      if (a)
      {
        q localq1 = d;
        return localq1;
      }
      if (!b) {
        throw new IllegalStateException("Take off must be called before shared()");
      }
    }
    int i1 = 0;
    try
    {
      for (;;)
      {
        boolean bool = a;
        if (bool) {
          break;
        }
        try
        {
          r.wait();
        }
        catch (InterruptedException localInterruptedException)
        {
          i1 = 1;
        }
      }
      q localq2 = d;
      if (i1 != 0) {
        Thread.currentThread().interrupt();
      }
      return localq2;
    }
    finally
    {
      if (i1 != 0) {
        Thread.currentThread().interrupt();
      }
    }
  }
  
  @MainThread
  public static void a(@NonNull Application paramApplication, @Nullable b paramb, @Nullable a parama)
  {
    if (paramApplication == null) {
      throw new IllegalArgumentException("Application argument must not be null");
    }
    if ((Looper.myLooper() != null) && (Looper.getMainLooper() == Looper.myLooper())) {
      if (Build.VERSION.SDK_INT >= 16) {}
    }
    for (;;)
    {
      try
      {
        Class.forName("android.os.AsyncTask");
        if (!e) {
          break label165;
        }
        StringBuilder localStringBuilder = new StringBuilder();
        StackTraceElement[] arrayOfStackTraceElement = new Exception().getStackTrace();
        int i2 = arrayOfStackTraceElement.length;
        int i1 = 0;
        if (i1 >= i2) {
          break;
        }
        StackTraceElement localStackTraceElement = arrayOfStackTraceElement[i1];
        localStringBuilder.append("\n\tat ");
        localStringBuilder.append(localStackTraceElement.toString());
        i1 += 1;
        continue;
      }
      catch (ClassNotFoundException localClassNotFoundException)
      {
        j.c("AsyncTask workaround failed.", localClassNotFoundException);
        continue;
      }
      j.e("takeOff() must be called on the main thread!");
    }
    Log.d(j.b, "Takeoff stack trace: " + localClassNotFoundException.toString());
    synchronized (r)
    {
      label165:
      if ((a) || (b))
      {
        j.e("You can only call takeOff() once.");
        return;
      }
      j.d("Airship taking off!");
      b = true;
      c = paramApplication;
      if (Build.VERSION.SDK_INT >= 14)
      {
        com.urbanairship.analytics.b.a(paramApplication);
        com.urbanairship.push.iam.d.a(paramApplication);
      }
      new Thread(new q.2(paramApplication, paramb, parama)).start();
      return;
    }
  }
  
  @MainThread
  public static void a(@NonNull Application paramApplication, @Nullable a parama)
  {
    a(paramApplication, null, parama);
  }
  
  public static String b()
  {
    return h().getPackageName();
  }
  
  public static String c()
  {
    return h().getPackageName() + ".permission.UA_DATA";
  }
  
  private static void c(@NonNull Application arg0, @Nullable b paramb, @Nullable a parama)
  {
    b localb = paramb;
    if (paramb == null) {
      localb = new b.a().a(???.getApplicationContext()).a();
    }
    j.a = localb.c();
    j.b = g() + " - UALib";
    j.d("Airship taking off!");
    j.d("Airship log level: " + j.a);
    j.d("UA Version: " + k() + " / App key = " + localb.a() + " Production = " + localb.k);
    d = new q(localb);
    synchronized (r)
    {
      a = true;
      b = false;
      d.w();
      if (!localb.k) {
        d.x();
      }
      j.d("Airship ready!");
      if (parama != null) {
        parama.a(d);
      }
      if (s == null) {
        break label256;
      }
      paramb = new ArrayList(s).iterator();
      if (paramb.hasNext()) {
        ((Runnable)paramb.next()).run();
      }
    }
    s = null;
    label256:
    r.notifyAll();
  }
  
  public static PackageManager d()
  {
    return h().getPackageManager();
  }
  
  public static PackageInfo e()
  {
    try
    {
      PackageInfo localPackageInfo = d().getPackageInfo(b(), 0);
      return localPackageInfo;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      j.a("UAirship - Unable to get package info.", localNameNotFoundException);
    }
    return null;
  }
  
  public static ApplicationInfo f()
  {
    return h().getApplicationInfo();
  }
  
  public static String g()
  {
    if (f() != null) {
      return d().getApplicationLabel(f()).toString();
    }
    return null;
  }
  
  public static Context h()
  {
    if (c == null) {
      throw new IllegalStateException("TakeOff must be called first.");
    }
    return c.getApplicationContext();
  }
  
  public static boolean i()
  {
    return a;
  }
  
  public static boolean j()
  {
    return b;
  }
  
  public static String k()
  {
    return "7.1.5";
  }
  
  private void w()
  {
    this.j = new l(c);
    this.j.a();
    this.h = new com.urbanairship.analytics.b(c, this.j, this.g);
    this.i = new c(c, this.j);
    this.l = new com.urbanairship.richpush.b(c, this.j);
    this.m = new com.urbanairship.location.f(c, this.j);
    this.o = new com.urbanairship.push.iam.d(this.j);
    this.k = new com.urbanairship.push.j(c, this.j, this.g);
    this.p = new g(c, this.g, this.k);
    this.n = com.urbanairship.c.b.a(this.g);
    this.f = new com.urbanairship.actions.d();
    this.f.a();
    this.q = new com.urbanairship.messagecenter.d();
    this.l.a();
    this.k.a();
    this.m.a();
    this.o.a();
    this.p.a();
    this.i.a();
    this.h.a();
    this.q.a();
    String str1 = k();
    String str2 = this.j.a("com.urbanairship.application.device.LIBRARY_VERSION", null);
    if ((str2 != null) && (!str2.equals(str1))) {
      j.d("Urban Airship library changed from " + str2 + " to " + str1 + ".");
    }
    this.j.b("com.urbanairship.application.device.LIBRARY_VERSION", k());
  }
  
  private void x()
  {
    com.urbanairship.d.d.a(this.g);
    switch (d.v())
    {
    default: 
      return;
    case 2: 
      if (this.g.a("GCM"))
      {
        com.urbanairship.google.a.a(this.g);
        return;
      }
      j.e("Android platform detected but GCM transport is disabled. The device will not be able to receive push notifications.");
      return;
    }
    if (this.g.a("ADM"))
    {
      com.urbanairship.a.a.c();
      return;
    }
    j.e("Amazon platform detected but ADM transport is disabled. The device will not be able to receive push notifications.");
  }
  
  public b l()
  {
    return this.g;
  }
  
  public com.urbanairship.push.j m()
  {
    return this.k;
  }
  
  public com.urbanairship.richpush.b n()
  {
    return this.l;
  }
  
  public com.urbanairship.location.f o()
  {
    return this.m;
  }
  
  public com.urbanairship.push.iam.d p()
  {
    return this.o;
  }
  
  public com.urbanairship.analytics.b q()
  {
    return this.h;
  }
  
  public c r()
  {
    return this.i;
  }
  
  public com.urbanairship.c.b s()
  {
    return this.n;
  }
  
  public com.urbanairship.actions.d t()
  {
    return this.f;
  }
  
  public com.urbanairship.messagecenter.d u()
  {
    return this.q;
  }
  
  public int v()
  {
    int i1;
    switch (this.j.a("com.urbanairship.application.device.PLATFORM", -1))
    {
    default: 
      if (com.urbanairship.a.a.a())
      {
        j.d("ADM available. Setting platform to Amazon.");
        i1 = 1;
      }
      break;
    }
    for (;;)
    {
      this.j.b("com.urbanairship.application.device.PLATFORM", i1);
      return i1;
      return 1;
      return 2;
      if (com.urbanairship.google.c.c(h()))
      {
        j.d("Google Play Store available. Setting platform to Android.");
        i1 = 2;
      }
      else if ("amazon".equalsIgnoreCase(Build.MANUFACTURER))
      {
        j.d("Build.MANUFACTURER is AMAZON. Setting platform to Amazon.");
        i1 = 1;
      }
      else
      {
        j.d("Defaulting platform to Android.");
        i1 = 2;
      }
    }
  }
  
  public static abstract interface a
  {
    public abstract void a(q paramq);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/urbanairship/q.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */