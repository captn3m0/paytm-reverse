package com.etsy.android.grid;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v4.util.SparseArrayCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.widget.AbsListView;
import android.widget.AbsListView.LayoutParams;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListAdapter;
import android.widget.Scroller;
import java.util.ArrayList;
import java.util.Iterator;

public abstract class ExtendableListView
  extends AbsListView
{
  private int A;
  private int B;
  private g C;
  private a D;
  private int E;
  private e F;
  private f G;
  private Runnable H;
  private b I;
  private ArrayList<d> J;
  private ArrayList<d> K;
  private AbsListView.OnScrollListener L;
  private ListSavedState M;
  ListAdapter a;
  protected int b;
  final boolean[] c = new boolean[1];
  protected boolean d;
  protected int e;
  protected int f;
  long g = Long.MIN_VALUE;
  long h;
  boolean i = false;
  private int j;
  private int k;
  private int l = 0;
  private VelocityTracker m = null;
  private int n;
  private int o;
  private int p;
  private boolean q;
  private int r;
  private int s;
  private int t;
  private int u;
  private int v;
  private int w = -1;
  private boolean x;
  private boolean y = false;
  private boolean z;
  
  public ExtendableListView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    setWillNotDraw(false);
    setClipToPadding(false);
    setFocusableInTouchMode(false);
    paramContext = ViewConfiguration.get(paramContext);
    this.n = paramContext.getScaledTouchSlop();
    this.o = paramContext.getScaledMaximumFlingVelocity();
    this.p = paramContext.getScaledMinimumFlingVelocity();
    this.C = new g();
    this.D = new a();
    this.J = new ArrayList();
    this.K = new ArrayList();
    this.j = 0;
  }
  
  private View a(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
  {
    a(paramInt1, paramBoolean1);
    if (!this.z)
    {
      localView = this.C.c(paramInt1);
      if (localView != null)
      {
        a(localView, paramInt1, paramInt2, paramBoolean1, paramBoolean2, true);
        return localView;
      }
    }
    View localView = a(paramInt1, this.c);
    a(localView, paramInt1, paramInt2, paramBoolean1, paramBoolean2, this.c[0]);
    return localView;
  }
  
  private View a(int paramInt, boolean[] paramArrayOfBoolean)
  {
    paramArrayOfBoolean[0] = false;
    View localView1 = this.C.d(paramInt);
    if (localView1 != null)
    {
      View localView2 = this.a.getView(paramInt, localView1, this);
      if (localView2 != localView1)
      {
        this.C.a(localView1, paramInt);
        return localView2;
      }
      paramArrayOfBoolean[0] = true;
      return localView2;
    }
    return this.a.getView(paramInt, null, this);
  }
  
  static View a(ArrayList<View> paramArrayList, int paramInt)
  {
    int i2 = paramArrayList.size();
    if (i2 > 0)
    {
      int i1 = 0;
      while (i1 < i2)
      {
        View localView = (View)paramArrayList.get(i1);
        if (((LayoutParams)localView.getLayoutParams()).b == paramInt)
        {
          paramArrayList.remove(i1);
          return localView;
        }
        i1 += 1;
      }
      return (View)paramArrayList.remove(i2 - 1);
    }
    return null;
  }
  
  private void a(float paramFloat)
  {
    if (this.F == null) {
      this.F = new e();
    }
    this.F.a((int)-paramFloat);
  }
  
  private void a(View paramView, int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    int i2;
    int i1;
    label39:
    int i3;
    label51:
    label71:
    int i4;
    LayoutParams localLayoutParams;
    if (paramView.isSelected())
    {
      i2 = 1;
      i1 = this.k;
      if ((i1 <= 3) || (i1 >= 1) || (this.u != paramInt1)) {
        break label239;
      }
      paramBoolean2 = true;
      if (paramBoolean2 == paramView.isPressed()) {
        break label245;
      }
      i3 = 1;
      if ((paramBoolean3) && (i2 == 0) && (!paramView.isLayoutRequested())) {
        break label251;
      }
      i1 = 1;
      i4 = this.a.getItemViewType(paramInt1);
      if (i4 != -2) {
        break label257;
      }
      localLayoutParams = c(paramView);
      label97:
      localLayoutParams.d = i4;
      localLayoutParams.b = paramInt1;
      if ((!paramBoolean3) && ((!localLayoutParams.a) || (localLayoutParams.d != -2))) {
        break label273;
      }
      if (!paramBoolean1) {
        break label267;
      }
      i4 = -1;
      label141:
      attachViewToParent(paramView, i4, localLayoutParams);
      if (i2 != 0) {
        paramView.setSelected(false);
      }
      if (i3 != 0) {
        paramView.setPressed(paramBoolean2);
      }
      if (i1 == 0) {
        break label317;
      }
      a(paramView, localLayoutParams);
      label183:
      i2 = paramView.getMeasuredWidth();
      i3 = paramView.getMeasuredHeight();
      if (!paramBoolean1) {
        break label325;
      }
    }
    for (;;)
    {
      i4 = a(paramInt1);
      if (i1 == 0) {
        break label333;
      }
      a(paramView, paramInt1, paramBoolean1, i4, paramInt2, i4 + i2, paramInt2 + i3);
      return;
      i2 = 0;
      break;
      label239:
      paramBoolean2 = false;
      break label39;
      label245:
      i3 = 0;
      break label51;
      label251:
      i1 = 0;
      break label71;
      label257:
      localLayoutParams = b(paramView);
      break label97;
      label267:
      i4 = 0;
      break label141;
      label273:
      if (localLayoutParams.d == -2) {
        localLayoutParams.a = true;
      }
      if (paramBoolean1) {}
      for (i4 = -1;; i4 = 0)
      {
        addViewInLayout(paramView, i4, localLayoutParams, true);
        break;
      }
      label317:
      cleanupLayoutState(paramView);
      break label183;
      label325:
      paramInt2 -= i3;
    }
    label333:
    a(paramView, paramInt1, paramBoolean1, i4, paramInt2);
  }
  
  private void a(Runnable paramRunnable)
  {
    ViewCompat.a(this, paramRunnable);
  }
  
  private void a(ArrayList<d> paramArrayList)
  {
    if (paramArrayList == null) {}
    for (;;)
    {
      return;
      paramArrayList = paramArrayList.iterator();
      while (paramArrayList.hasNext())
      {
        ViewGroup.LayoutParams localLayoutParams = ((d)paramArrayList.next()).a.getLayoutParams();
        if ((localLayoutParams instanceof LayoutParams)) {
          ((LayoutParams)localLayoutParams).a = false;
        }
      }
    }
  }
  
  private boolean a(MotionEvent paramMotionEvent)
  {
    int i3 = (int)paramMotionEvent.getX();
    int i4 = (int)paramMotionEvent.getY();
    int i2 = pointToPosition(i3, i4);
    this.m.clear();
    this.w = MotionEventCompat.b(paramMotionEvent, 0);
    int i1;
    if ((this.k != 2) && (!this.z) && (i2 >= 0) && (getAdapter().isEnabled(i2)))
    {
      this.k = 3;
      if (this.H == null) {
        this.H = new c();
      }
      postDelayed(this.H, ViewConfiguration.getTapTimeout());
      i1 = i2;
      if (paramMotionEvent.getEdgeFlags() != 0)
      {
        i1 = i2;
        if (i2 < 0) {
          return false;
        }
      }
    }
    else
    {
      i1 = i2;
      if (this.k == 2)
      {
        this.k = 1;
        this.t = 0;
        i1 = j(i4);
      }
    }
    this.s = i3;
    this.r = i4;
    this.u = i1;
    this.v = Integer.MIN_VALUE;
    return true;
  }
  
  private boolean a(View paramView, int paramInt, long paramLong)
  {
    boolean bool = false;
    AdapterView.OnItemLongClickListener localOnItemLongClickListener = getOnItemLongClickListener();
    if (localOnItemLongClickListener != null) {
      bool = localOnItemLongClickListener.onItemLongClick(this, paramView, paramInt, paramLong);
    }
    if (bool) {
      performHapticFeedback(0);
    }
    return bool;
  }
  
  private boolean b(MotionEvent paramMotionEvent)
  {
    int i1 = MotionEventCompat.a(paramMotionEvent, this.w);
    if (i1 < 0)
    {
      Log.e("ExtendableListView", "onTouchMove could not find pointer with id " + this.w + " - did ExtendableListView receive an inconsistent event stream?");
      return false;
    }
    i1 = (int)MotionEventCompat.d(paramMotionEvent, i1);
    if (this.z) {
      layoutChildren();
    }
    switch (this.k)
    {
    }
    for (;;)
    {
      return true;
      h(i1);
      continue;
      i(i1);
    }
  }
  
  private boolean c(int paramInt1, int paramInt2)
  {
    if (!e()) {
      return true;
    }
    paramInt1 = getHighestChildTop();
    int i1 = getLowestChildBottom();
    int i3 = 0;
    int i2 = 0;
    if (this.d)
    {
      i3 = getListPaddingTop();
      i2 = getListPaddingBottom();
    }
    int i8 = getHeight();
    int i9 = getFirstChildTop();
    int i10 = getLastChildBottom();
    int i4 = i8 - getListPaddingBottom() - getListPaddingTop();
    int i11;
    int i12;
    if (paramInt2 < 0)
    {
      i4 = Math.max(-(i4 - 1), paramInt2);
      i11 = this.b;
      i5 = getListPaddingTop();
      paramInt2 = getListPaddingBottom();
      i12 = getChildCount();
      if ((i11 != 0) || (paramInt1 < i5) || (i4 < 0)) {
        break label183;
      }
      paramInt1 = 1;
      label132:
      if ((i11 + i12 != this.A) || (i1 > i8 - paramInt2) || (i4 > 0)) {
        break label188;
      }
      paramInt2 = 1;
    }
    for (;;)
    {
      if (paramInt1 != 0)
      {
        if (i4 != 0)
        {
          return true;
          i4 = Math.min(i4 - 1, paramInt2);
          break;
          label183:
          paramInt1 = 0;
          break label132;
          label188:
          paramInt2 = 0;
          continue;
        }
        return false;
      }
    }
    if (paramInt2 != 0) {
      return i4 != 0;
    }
    boolean bool;
    int i13;
    int i14;
    int i6;
    int i7;
    View localView;
    if (i4 < 0)
    {
      bool = true;
      i13 = getHeaderViewsCount();
      i14 = this.A - getFooterViewsCount();
      i1 = 0;
      i5 = 0;
      paramInt2 = 0;
      paramInt1 = 0;
      if (bool)
      {
        i1 = -i4;
        paramInt2 = i1;
        if (this.d) {
          paramInt2 = i1 + getListPaddingTop();
        }
        i1 = 0;
      }
    }
    else
    {
      for (;;)
      {
        i6 = paramInt1;
        i7 = i5;
        if (i1 < i12)
        {
          localView = getChildAt(i1);
          if (localView.getBottom() >= paramInt2)
          {
            i7 = i5;
            i6 = paramInt1;
          }
        }
        else
        {
          this.y = true;
          if (i6 > 0)
          {
            detachViewsFromParent(i7, i6);
            this.C.d();
            b(i7, i6);
          }
          if (!awakenScrollBars()) {
            invalidate();
          }
          f(i4);
          if (bool) {
            this.b += i6;
          }
          paramInt1 = Math.abs(i4);
          if ((i3 - i9 < paramInt1) || (i10 - (i8 - i2) < paramInt1)) {
            a(bool);
          }
          this.y = false;
          g();
          return false;
          bool = false;
          break;
        }
        paramInt1 += 1;
        i6 = i11 + i1;
        if ((i6 >= i13) && (i6 < i14)) {
          this.C.a(localView, i6);
        }
        i1 += 1;
      }
    }
    paramInt1 = i8 - i4;
    int i5 = paramInt1;
    if (this.d) {
      i5 = paramInt1 - getListPaddingBottom();
    }
    paramInt1 = i12 - 1;
    for (;;)
    {
      i6 = paramInt2;
      i7 = i1;
      if (paramInt1 < 0) {
        break;
      }
      localView = getChildAt(paramInt1);
      i6 = paramInt2;
      i7 = i1;
      if (localView.getTop() <= i5) {
        break;
      }
      i1 = paramInt1;
      paramInt2 += 1;
      i6 = i11 + paramInt1;
      if ((i6 >= i13) && (i6 < i14)) {
        this.C.a(localView, i6);
      }
      paramInt1 -= 1;
    }
  }
  
  private boolean c(MotionEvent paramMotionEvent)
  {
    this.k = 0;
    setPressed(false);
    invalidate();
    paramMotionEvent = getHandler();
    if (paramMotionEvent != null) {
      paramMotionEvent.removeCallbacks(this.I);
    }
    l();
    this.w = -1;
    return true;
  }
  
  private View d(int paramInt1, int paramInt2)
  {
    int i4 = getHeight();
    int i1 = i4;
    int i2 = paramInt1;
    int i3 = paramInt2;
    if (this.d)
    {
      i1 = i4 - getListPaddingBottom();
      i3 = paramInt2;
      i2 = paramInt1;
    }
    while (((i3 < i1) || (b())) && (i2 < this.A))
    {
      a(i2, i3, true, false);
      i2 += 1;
      i3 = d(i2);
    }
    return null;
  }
  
  private boolean d(MotionEvent paramMotionEvent)
  {
    switch (this.k)
    {
    case 2: 
    default: 
      setPressed(false);
      invalidate();
      paramMotionEvent = getHandler();
      if (paramMotionEvent != null) {
        paramMotionEvent.removeCallbacks(this.I);
      }
      l();
      this.w = -1;
      return true;
    case 3: 
    case 4: 
    case 5: 
      return f(paramMotionEvent);
    }
    return e(paramMotionEvent);
  }
  
  private View e(int paramInt1, int paramInt2)
  {
    int i1;
    if (this.d) {
      i1 = getListPaddingTop();
    }
    while (((paramInt2 > i1) || (c())) && (paramInt1 >= 0))
    {
      a(paramInt1, paramInt2, false, false);
      paramInt1 -= 1;
      paramInt2 = e(paramInt1);
      continue;
      i1 = 0;
    }
    this.b = (paramInt1 + 1);
    return null;
  }
  
  private boolean e(MotionEvent paramMotionEvent)
  {
    if (e())
    {
      int i1 = getFirstChildTop();
      int i2 = getLastChildBottom();
      if ((this.b == 0) && (i1 >= getListPaddingTop()) && (this.b + getChildCount() < this.A) && (i2 <= getHeight() - getListPaddingBottom())) {}
      for (i1 = 1; i1 == 0; i1 = 0)
      {
        this.m.computeCurrentVelocity(1000, this.o);
        float f1 = this.m.getYVelocity(this.w);
        if (Math.abs(f1) <= this.p) {
          break;
        }
        a(f1);
        this.k = 2;
        this.r = 0;
        invalidate();
        return true;
      }
    }
    m();
    l();
    this.k = 0;
    return true;
  }
  
  private View f(int paramInt1, int paramInt2)
  {
    View localView1 = a(paramInt1, paramInt2, true, false);
    this.b = paramInt1;
    paramInt2 = e(paramInt1 - 1);
    int i1 = d(paramInt1 + 1);
    View localView2 = e(paramInt1 - 1, paramInt2);
    i();
    View localView3 = d(paramInt1 + 1, i1);
    paramInt1 = getChildCount();
    if (paramInt1 > 0) {
      l(paramInt1);
    }
    if (0 != 0) {
      return localView1;
    }
    if (localView2 != null) {
      return localView2;
    }
    return localView3;
  }
  
  private boolean f(MotionEvent paramMotionEvent)
  {
    int i1 = this.u;
    if (i1 >= 0)
    {
      final View localView = getChildAt(i1);
      if ((localView != null) && (!localView.hasFocusable()))
      {
        if (this.k != 3) {
          localView.setPressed(false);
        }
        if (this.G == null)
        {
          invalidate();
          this.G = new f(null);
        }
        final f localf = this.G;
        localf.a = i1;
        localf.a();
        if ((this.k == 3) || (this.k == 4))
        {
          Handler localHandler = getHandler();
          if (localHandler != null) {
            if (this.k != 3) {
              break label196;
            }
          }
          label196:
          for (paramMotionEvent = this.H;; paramMotionEvent = this.I)
          {
            localHandler.removeCallbacks(paramMotionEvent);
            this.j = 0;
            if ((this.z) || (i1 < 0) || (!this.a.isEnabled(i1))) {
              break;
            }
            this.k = 4;
            layoutChildren();
            localView.setPressed(true);
            setPressed(true);
            postDelayed(new Runnable()
            {
              public void run()
              {
                localView.setPressed(false);
                ExtendableListView.this.setPressed(false);
                if (!ExtendableListView.b(ExtendableListView.this)) {
                  ExtendableListView.this.post(localf);
                }
                ExtendableListView.a(ExtendableListView.this, 0);
              }
            }, ViewConfiguration.getPressedStateDuration());
            return true;
          }
          this.k = 0;
          return true;
        }
        if ((!this.z) && (i1 >= 0) && (this.a.isEnabled(i1))) {
          post(localf);
        }
      }
    }
    this.k = 0;
    return true;
  }
  
  private boolean g(MotionEvent paramMotionEvent)
  {
    h(paramMotionEvent);
    int i2 = this.s;
    int i1 = this.r;
    i2 = pointToPosition(i2, i1);
    if (i2 >= 0) {
      this.u = i2;
    }
    this.v = i1;
    return true;
  }
  
  private void h(MotionEvent paramMotionEvent)
  {
    int i1 = (paramMotionEvent.getAction() & 0xFF00) >> 8;
    if (paramMotionEvent.getPointerId(i1) == this.w) {
      if (i1 != 0) {
        break label64;
      }
    }
    label64:
    for (i1 = 1;; i1 = 0)
    {
      this.s = ((int)paramMotionEvent.getX(i1));
      this.r = ((int)paramMotionEvent.getY(i1));
      this.w = paramMotionEvent.getPointerId(i1);
      l();
      return;
    }
  }
  
  private boolean h(int paramInt)
  {
    int i1 = paramInt - this.r;
    if (Math.abs(i1) > this.n)
    {
      this.k = 1;
      if (i1 > 0) {}
      for (i1 = this.n;; i1 = -this.n)
      {
        this.t = i1;
        Object localObject = getHandler();
        if (localObject != null) {
          ((Handler)localObject).removeCallbacks(this.I);
        }
        setPressed(false);
        localObject = getChildAt(this.u - this.b);
        if (localObject != null) {
          ((View)localObject).setPressed(false);
        }
        localObject = getParent();
        if (localObject != null) {
          ((ViewParent)localObject).requestDisallowInterceptTouchEvent(true);
        }
        i(paramInt);
        return true;
      }
    }
    return false;
  }
  
  private void i()
  {
    if (getChildCount() > 0)
    {
      int i2 = getHighestChildTop() - getListPaddingTop();
      int i1 = i2;
      if (i2 < 0) {
        i1 = 0;
      }
      if (i1 != 0) {
        f(-i1);
      }
    }
  }
  
  private void i(int paramInt)
  {
    int i2 = paramInt - this.r;
    int i3 = i2 - this.t;
    int i1;
    if (this.v != Integer.MIN_VALUE)
    {
      i1 = paramInt - this.v;
      if ((this.k == 1) && (paramInt != this.v))
      {
        if (Math.abs(i2) > this.n)
        {
          ViewParent localViewParent = getParent();
          if (localViewParent != null) {
            localViewParent.requestDisallowInterceptTouchEvent(true);
          }
        }
        if (this.u < 0) {
          break label141;
        }
      }
    }
    label141:
    for (i2 = this.u - this.b;; i2 = getChildCount() / 2)
    {
      boolean bool = false;
      if (i1 != 0) {
        bool = c(i3, i1);
      }
      if (getChildAt(i2) != null)
      {
        if (bool) {}
        this.r = paramInt;
      }
      this.v = paramInt;
      return;
      i1 = i3;
      break;
    }
  }
  
  private int j(int paramInt)
  {
    int i2 = getChildCount();
    if (i2 > 0)
    {
      int i1 = 0;
      while (i1 < i2)
      {
        if (paramInt <= getChildAt(i1).getBottom()) {
          return this.b + i1;
        }
        i1 += 1;
      }
    }
    return -1;
  }
  
  private void j()
  {
    if (this.m == null)
    {
      this.m = VelocityTracker.obtain();
      return;
    }
    this.m.clear();
  }
  
  private View k(int paramInt)
  {
    this.b = Math.min(this.b, this.A - 1);
    if (this.b < 0) {
      this.b = 0;
    }
    return d(this.b, paramInt);
  }
  
  private void k()
  {
    if (this.m == null) {
      this.m = VelocityTracker.obtain();
    }
  }
  
  private void l()
  {
    if (this.m != null)
    {
      this.m.recycle();
      this.m = null;
    }
  }
  
  private void l(int paramInt)
  {
    if ((this.b + paramInt - 1 == this.A - 1) && (paramInt > 0))
    {
      paramInt = getLowestChildBottom();
      int i1 = getBottom() - getTop() - getListPaddingBottom() - paramInt;
      int i2 = getHighestChildTop();
      if ((i1 > 0) && ((this.b > 0) || (i2 < getListPaddingTop())))
      {
        paramInt = i1;
        if (this.b == 0) {
          paramInt = Math.min(i1, getListPaddingTop() - i2);
        }
        f(paramInt);
        if (this.b > 0)
        {
          paramInt = this.b - 1;
          e(paramInt, e(paramInt));
          i();
        }
      }
    }
  }
  
  private void m()
  {
    if (this.F != null) {
      e.a(this.F);
    }
  }
  
  private void m(int paramInt)
  {
    int i4;
    if ((this.b == 0) && (paramInt > 0))
    {
      int i1 = getHighestChildTop();
      int i3 = getListPaddingTop();
      int i2 = getTop() - getBottom() - getListPaddingBottom();
      i1 -= i3;
      i3 = getLowestChildBottom();
      i4 = this.b + paramInt - 1;
      if (i1 > 0)
      {
        if ((i4 >= this.A - 1) && (i3 <= i2)) {
          break label139;
        }
        paramInt = i1;
        if (i4 == this.A - 1) {
          paramInt = Math.min(i1, i3 - i2);
        }
        f(-paramInt);
        if (i4 < this.A - 1)
        {
          paramInt = i4 + 1;
          d(paramInt, d(paramInt));
          i();
        }
      }
    }
    label139:
    while (i4 != this.A - 1) {
      return;
    }
    i();
  }
  
  private void n()
  {
    int i1;
    View localView;
    if ((getAdapter() == null) || (getAdapter().isEmpty()))
    {
      i1 = 1;
      if (isInFilterMode()) {
        i1 = 0;
      }
      localView = getEmptyView();
      if (i1 == 0) {
        break label96;
      }
      if (localView == null) {
        break label88;
      }
      localView.setVisibility(0);
      setVisibility(8);
    }
    for (;;)
    {
      if (this.z) {
        onLayout(false, getLeft(), getTop(), getRight(), getBottom());
      }
      return;
      i1 = 0;
      break;
      label88:
      setVisibility(0);
    }
    label96:
    if (localView != null) {
      localView.setVisibility(8);
    }
    setVisibility(0);
  }
  
  private void o()
  {
    a(this.J);
    a(this.K);
    removeAllViewsInLayout();
    this.b = 0;
    this.z = false;
    this.C.b();
    this.i = false;
    this.M = null;
    this.j = 0;
    invalidate();
  }
  
  protected int a(int paramInt)
  {
    return getListPaddingLeft();
  }
  
  public void a() {}
  
  protected void a(int paramInt1, int paramInt2)
  {
    if (getChildCount() > 0)
    {
      m();
      this.C.b();
      this.z = true;
      h();
    }
  }
  
  protected void a(int paramInt, boolean paramBoolean) {}
  
  public void a(View paramView)
  {
    a(paramView, null, true);
  }
  
  protected void a(View paramView, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3)
  {
    paramView.offsetLeftAndRight(paramInt2 - paramView.getLeft());
    paramView.offsetTopAndBottom(paramInt3 - paramView.getTop());
  }
  
  protected void a(View paramView, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    paramView.layout(paramInt2, paramInt3, paramInt4, paramInt5);
  }
  
  protected void a(View paramView, LayoutParams paramLayoutParams)
  {
    int i2 = ViewGroup.getChildMeasureSpec(this.E, getListPaddingLeft() + getListPaddingRight(), paramLayoutParams.width);
    int i1 = paramLayoutParams.height;
    if (i1 > 0) {}
    for (i1 = View.MeasureSpec.makeMeasureSpec(i1, 1073741824);; i1 = View.MeasureSpec.makeMeasureSpec(0, 0))
    {
      paramView.measure(i2, i1);
      return;
    }
  }
  
  public void a(View paramView, Object paramObject, boolean paramBoolean)
  {
    if ((this.a != null) && (!(this.a instanceof a))) {
      throw new IllegalStateException("Cannot add header view to list -- setAdapter has already been called.");
    }
    d locald = new d();
    locald.a = paramView;
    locald.b = paramObject;
    locald.c = paramBoolean;
    this.J.add(locald);
    if ((this.a != null) && (this.D != null)) {
      this.D.onChanged();
    }
  }
  
  protected void a(boolean paramBoolean)
  {
    int i1 = getChildCount();
    if (paramBoolean)
    {
      i1 = this.b + i1;
      d(i1, b(i1));
    }
    for (;;)
    {
      b(paramBoolean);
      return;
      i1 = this.b - 1;
      e(i1, c(i1));
    }
  }
  
  protected int b(int paramInt)
  {
    int i1 = getChildCount();
    paramInt = 0;
    if (this.d) {
      paramInt = getListPaddingTop();
    }
    if (i1 > 0) {
      paramInt = getChildAt(i1 - 1).getBottom();
    }
    return paramInt;
  }
  
  protected LayoutParams b(View paramView)
  {
    return c(paramView);
  }
  
  protected void b(int paramInt1, int paramInt2) {}
  
  protected void b(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      l(getChildCount());
      return;
    }
    m(getChildCount());
  }
  
  protected boolean b()
  {
    return false;
  }
  
  protected int c(int paramInt)
  {
    int i1 = getChildCount();
    paramInt = 0;
    if (this.d) {
      paramInt = getListPaddingBottom();
    }
    if (i1 > 0) {
      return getChildAt(0).getTop();
    }
    return getHeight() - paramInt;
  }
  
  protected LayoutParams c(View paramView)
  {
    Object localObject = null;
    ViewGroup.LayoutParams localLayoutParams = paramView.getLayoutParams();
    paramView = (View)localObject;
    if (localLayoutParams != null) {
      if (!(localLayoutParams instanceof LayoutParams)) {
        break label38;
      }
    }
    label38:
    for (paramView = (LayoutParams)localLayoutParams;; paramView = new LayoutParams(localLayoutParams))
    {
      localObject = paramView;
      if (paramView == null) {
        localObject = d();
      }
      return (LayoutParams)localObject;
    }
  }
  
  protected boolean c()
  {
    return false;
  }
  
  protected int d(int paramInt)
  {
    paramInt = getChildCount();
    if (paramInt > 0) {
      return getChildAt(paramInt - 1).getBottom();
    }
    return 0;
  }
  
  protected LayoutParams d()
  {
    return new LayoutParams(-1, -2, 0);
  }
  
  protected int e(int paramInt)
  {
    paramInt = getChildCount();
    if (paramInt == 0) {}
    while (paramInt <= 0) {
      return 0;
    }
    return getChildAt(0).getTop();
  }
  
  protected boolean e()
  {
    return getChildCount() > 0;
  }
  
  public void f()
  {
    switch (this.k)
    {
    default: 
      return;
    case 1: 
      g(1);
      return;
    case 2: 
      g(2);
      return;
    }
    g(0);
  }
  
  protected void f(int paramInt)
  {
    int i2 = getChildCount();
    int i1 = 0;
    while (i1 < i2)
    {
      getChildAt(i1).offsetTopAndBottom(paramInt);
      i1 += 1;
    }
  }
  
  void g()
  {
    if (this.L != null) {
      this.L.onScroll(this, this.b, getChildCount(), this.A);
    }
  }
  
  void g(int paramInt)
  {
    if (paramInt != this.l)
    {
      this.l = paramInt;
      if (this.L != null) {
        this.L.onScrollStateChanged(this, paramInt);
      }
    }
  }
  
  public ListAdapter getAdapter()
  {
    return this.a;
  }
  
  public int getCount()
  {
    return this.A;
  }
  
  protected int getFirstChildTop()
  {
    int i1 = 0;
    if (e()) {
      i1 = getChildAt(0).getTop();
    }
    return i1;
  }
  
  public int getFirstVisiblePosition()
  {
    return Math.max(0, this.b - getHeaderViewsCount());
  }
  
  public int getFooterViewsCount()
  {
    return this.K.size();
  }
  
  public int getHeaderViewsCount()
  {
    return this.J.size();
  }
  
  protected int getHighestChildTop()
  {
    int i1 = 0;
    if (e()) {
      i1 = getChildAt(0).getTop();
    }
    return i1;
  }
  
  protected int getLastChildBottom()
  {
    if (e()) {
      return getChildAt(getChildCount() - 1).getBottom();
    }
    return 0;
  }
  
  public int getLastVisiblePosition()
  {
    int i2 = this.b;
    int i3 = getChildCount();
    if (this.a != null) {}
    for (int i1 = this.a.getCount() - 1;; i1 = 0) {
      return Math.min(i2 + i3 - 1, i1);
    }
  }
  
  protected int getLowestChildBottom()
  {
    if (e()) {
      return getChildAt(getChildCount() - 1).getBottom();
    }
    return 0;
  }
  
  public View getSelectedView()
  {
    return null;
  }
  
  void h()
  {
    View localView;
    ListAdapter localListAdapter;
    if (getChildCount() > 0)
    {
      this.i = true;
      this.h = getHeight();
      localView = getChildAt(0);
      localListAdapter = getAdapter();
      if ((this.b < 0) || (this.b >= localListAdapter.getCount())) {
        break label87;
      }
    }
    label87:
    for (this.g = localListAdapter.getItemId(this.b);; this.g = -1L)
    {
      if (localView != null) {
        this.f = localView.getTop();
      }
      this.e = this.b;
      return;
    }
  }
  
  protected void handleDataChanged()
  {
    super.handleDataChanged();
    int i1 = this.A;
    if ((i1 > 0) && (this.i))
    {
      this.i = false;
      this.M = null;
      this.j = 2;
      this.e = Math.min(Math.max(0, this.e), i1 - 1);
      return;
    }
    this.j = 1;
    this.i = false;
    this.M = null;
  }
  
  protected void layoutChildren()
  {
    if (this.y) {
      return;
    }
    this.y = true;
    int i2;
    int i3;
    boolean bool;
    try
    {
      super.layoutChildren();
      invalidate();
      if (this.a == null)
      {
        o();
        g();
        return;
      }
      i2 = getListPaddingTop();
      i3 = getChildCount();
      View localView = null;
      if (this.j == 0) {
        localView = getChildAt(0);
      }
      bool = this.z;
      if (bool) {
        handleDataChanged();
      }
      if (this.A == 0)
      {
        o();
        g();
        return;
      }
      if (this.A != this.a.getCount()) {
        throw new IllegalStateException("The content of the adapter has changed but ExtendableListView did not receive a notification. Make sure the content of your adapter is not modified from a background thread, but only from the UI thread. [in ExtendableListView(" + getId() + ", " + getClass() + ") with Adapter(" + this.a.getClass() + ")]");
      }
    }
    finally
    {
      this.y = false;
    }
    int i4 = this.b;
    g localg = this.C;
    int i1;
    if (bool)
    {
      i1 = 0;
      while (i1 < i3)
      {
        localg.a(getChildAt(i1), i4 + i1);
        i1 += 1;
      }
    }
    localg.a(i3, i4);
    detachAllViewsFromParent();
    localg.d();
    switch (this.j)
    {
    }
    for (;;)
    {
      if (i3 == 0) {
        k(i2);
      }
      for (;;)
      {
        localg.e();
        this.z = false;
        this.i = false;
        this.j = 0;
        g();
        this.y = false;
        return;
        this.b = 0;
        a();
        i();
        k(i2);
        i();
        continue;
        f(this.e, this.f);
        continue;
        if (this.b < this.A)
        {
          i3 = this.b;
          if (localObject == null) {}
          for (i1 = i2;; i1 = ((View)localObject).getTop())
          {
            f(i3, i1);
            break;
          }
        }
        f(0, i2);
      }
    }
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    if (this.a != null)
    {
      this.z = true;
      this.B = this.A;
      this.A = this.a.getCount();
    }
    this.x = true;
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    this.C.b();
    if (this.F != null) {
      removeCallbacks(this.F);
    }
    this.x = false;
  }
  
  protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect) {}
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    int i1 = paramMotionEvent.getAction();
    if (!this.x) {}
    do
    {
      do
      {
        return false;
        switch (i1 & 0xFF)
        {
        case 4: 
        case 5: 
        default: 
          return false;
        case 0: 
          i1 = this.k;
          i2 = (int)paramMotionEvent.getX();
          int i3 = (int)paramMotionEvent.getY();
          this.w = paramMotionEvent.getPointerId(0);
          int i4 = j(i3);
          if ((i1 != 2) && (i4 >= 0))
          {
            this.s = i2;
            this.r = i3;
            this.u = i4;
            this.k = 3;
          }
          this.v = Integer.MIN_VALUE;
          j();
          this.m.addMovement(paramMotionEvent);
        }
      } while (i1 != 2);
      return true;
      switch (this.k)
      {
      default: 
        return false;
      }
      int i2 = paramMotionEvent.findPointerIndex(this.w);
      i1 = i2;
      if (i2 == -1)
      {
        i1 = 0;
        this.w = paramMotionEvent.getPointerId(0);
      }
      i1 = (int)paramMotionEvent.getY(i1);
      k();
      this.m.addMovement(paramMotionEvent);
    } while (!h(i1));
    return true;
    this.k = 0;
    this.w = -1;
    l();
    g(0);
    return false;
    h(paramMotionEvent);
    return false;
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (this.a == null) {
      return;
    }
    if (paramBoolean)
    {
      paramInt2 = getChildCount();
      paramInt1 = 0;
      while (paramInt1 < paramInt2)
      {
        getChildAt(paramInt1).forceLayout();
        paramInt1 += 1;
      }
      this.C.a();
    }
    this.q = true;
    layoutChildren();
    this.q = false;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    setMeasuredDimension(View.MeasureSpec.getSize(paramInt1), View.MeasureSpec.getSize(paramInt2));
    this.E = paramInt1;
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable)
  {
    paramParcelable = (ListSavedState)paramParcelable;
    super.onRestoreInstanceState(paramParcelable.a());
    this.z = true;
    this.h = paramParcelable.f;
    if (paramParcelable.c >= 0L)
    {
      this.i = true;
      this.M = paramParcelable;
      this.g = paramParcelable.c;
      this.e = paramParcelable.e;
      this.f = paramParcelable.d;
    }
    requestLayout();
  }
  
  public Parcelable onSaveInstanceState()
  {
    ListSavedState localListSavedState = new ListSavedState(super.onSaveInstanceState());
    if (this.M != null)
    {
      localListSavedState.b = this.M.b;
      localListSavedState.c = this.M.c;
      localListSavedState.d = this.M.d;
      localListSavedState.e = this.M.e;
      localListSavedState.f = this.M.f;
      return localListSavedState;
    }
    if ((getChildCount() > 0) && (this.A > 0)) {}
    for (int i1 = 1;; i1 = 0)
    {
      localListSavedState.b = getSelectedItemId();
      localListSavedState.f = getHeight();
      if ((i1 == 0) || (this.b <= 0)) {
        break;
      }
      localListSavedState.d = getChildAt(0).getTop();
      int i2 = this.b;
      i1 = i2;
      if (i2 >= this.A) {
        i1 = this.A - 1;
      }
      localListSavedState.e = i1;
      localListSavedState.c = this.a.getItemId(i1);
      return localListSavedState;
    }
    localListSavedState.d = 0;
    localListSavedState.c = -1L;
    localListSavedState.e = 0;
    return localListSavedState;
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    a(paramInt1, paramInt2);
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    boolean bool = false;
    if (!isEnabled()) {
      if ((isClickable()) || (isLongClickable())) {
        bool = true;
      }
    }
    do
    {
      return bool;
      k();
      this.m.addMovement(paramMotionEvent);
    } while (!e());
    switch (paramMotionEvent.getAction() & 0xFF)
    {
    case 4: 
    case 5: 
    default: 
      bool = false;
    }
    for (;;)
    {
      f();
      return bool;
      bool = a(paramMotionEvent);
      continue;
      bool = b(paramMotionEvent);
      continue;
      bool = c(paramMotionEvent);
      continue;
      bool = g(paramMotionEvent);
      continue;
      bool = d(paramMotionEvent);
    }
  }
  
  public void onWindowFocusChanged(boolean paramBoolean) {}
  
  public void requestDisallowInterceptTouchEvent(boolean paramBoolean)
  {
    if (paramBoolean) {
      l();
    }
    super.requestDisallowInterceptTouchEvent(paramBoolean);
  }
  
  public void requestLayout()
  {
    if ((!this.y) && (!this.q)) {
      super.requestLayout();
    }
  }
  
  public void setAdapter(ListAdapter paramListAdapter)
  {
    if (this.a != null) {
      this.a.unregisterDataSetObserver(this.D);
    }
    if ((this.J.size() > 0) || (this.K.size() > 0))
    {
      this.a = new a(this.J, this.K, paramListAdapter);
      this.z = true;
      if (this.a == null) {
        break label136;
      }
    }
    label136:
    for (int i1 = this.a.getCount();; i1 = 0)
    {
      this.A = i1;
      if (this.a != null)
      {
        this.a.registerDataSetObserver(this.D);
        this.C.a(this.a.getViewTypeCount());
      }
      requestLayout();
      return;
      this.a = paramListAdapter;
      break;
    }
  }
  
  public void setClipToPadding(boolean paramBoolean)
  {
    super.setClipToPadding(paramBoolean);
    this.d = paramBoolean;
  }
  
  public void setOnScrollListener(AbsListView.OnScrollListener paramOnScrollListener)
  {
    super.setOnScrollListener(paramOnScrollListener);
    this.L = paramOnScrollListener;
  }
  
  public void setSelection(int paramInt)
  {
    if (paramInt >= 0)
    {
      this.j = 2;
      this.f = getListPaddingTop();
      this.b = 0;
      if (this.i)
      {
        this.e = paramInt;
        this.g = this.a.getItemId(paramInt);
      }
      requestLayout();
    }
  }
  
  public static class LayoutParams
    extends AbsListView.LayoutParams
  {
    boolean a;
    int b;
    long c = -1L;
    int d;
    
    public LayoutParams(int paramInt1, int paramInt2)
    {
      super(paramInt2);
    }
    
    public LayoutParams(int paramInt1, int paramInt2, int paramInt3)
    {
      super(paramInt2);
      this.d = paramInt3;
    }
    
    public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
    }
    
    public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
    }
  }
  
  public static class ListSavedState
    extends ClassLoaderSavedState
  {
    public static final Parcelable.Creator<ListSavedState> CREATOR = new Parcelable.Creator()
    {
      public ExtendableListView.ListSavedState a(Parcel paramAnonymousParcel)
      {
        return new ExtendableListView.ListSavedState(paramAnonymousParcel);
      }
      
      public ExtendableListView.ListSavedState[] a(int paramAnonymousInt)
      {
        return new ExtendableListView.ListSavedState[paramAnonymousInt];
      }
    };
    protected long b;
    protected long c;
    protected int d;
    protected int e;
    protected int f;
    
    public ListSavedState(Parcel paramParcel)
    {
      super();
      this.b = paramParcel.readLong();
      this.c = paramParcel.readLong();
      this.d = paramParcel.readInt();
      this.e = paramParcel.readInt();
      this.f = paramParcel.readInt();
    }
    
    public ListSavedState(Parcelable paramParcelable)
    {
      super(AbsListView.class.getClassLoader());
    }
    
    public String toString()
    {
      return "ExtendableListView.ListSavedState{" + Integer.toHexString(System.identityHashCode(this)) + " selectedId=" + this.b + " firstId=" + this.c + " viewTop=" + this.d + " position=" + this.e + " height=" + this.f + "}";
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      super.writeToParcel(paramParcel, paramInt);
      paramParcel.writeLong(this.b);
      paramParcel.writeLong(this.c);
      paramParcel.writeInt(this.d);
      paramParcel.writeInt(this.e);
      paramParcel.writeInt(this.f);
    }
  }
  
  class a
    extends DataSetObserver
  {
    private Parcelable b = null;
    
    a() {}
    
    public void onChanged()
    {
      ExtendableListView.a(ExtendableListView.this, true);
      ExtendableListView.d(ExtendableListView.this, ExtendableListView.e(ExtendableListView.this));
      ExtendableListView.e(ExtendableListView.this, ExtendableListView.this.getAdapter().getCount());
      ExtendableListView.f(ExtendableListView.this).c();
      if ((ExtendableListView.this.getAdapter().hasStableIds()) && (this.b != null) && (ExtendableListView.g(ExtendableListView.this) == 0) && (ExtendableListView.e(ExtendableListView.this) > 0))
      {
        ExtendableListView.this.onRestoreInstanceState(this.b);
        this.b = null;
      }
      for (;;)
      {
        ExtendableListView.h(ExtendableListView.this);
        ExtendableListView.this.requestLayout();
        return;
        ExtendableListView.this.h();
      }
    }
    
    public void onInvalidated()
    {
      ExtendableListView.a(ExtendableListView.this, true);
      if (ExtendableListView.this.getAdapter().hasStableIds()) {
        this.b = ExtendableListView.this.onSaveInstanceState();
      }
      ExtendableListView.d(ExtendableListView.this, ExtendableListView.e(ExtendableListView.this));
      ExtendableListView.e(ExtendableListView.this, 0);
      ExtendableListView.this.i = false;
      ExtendableListView.h(ExtendableListView.this);
      ExtendableListView.this.requestLayout();
    }
  }
  
  private class b
    extends ExtendableListView.h
    implements Runnable
  {
    private b()
    {
      super(null);
    }
    
    public void run()
    {
      int i = ExtendableListView.a(ExtendableListView.this);
      View localView = ExtendableListView.this.getChildAt(i);
      if (localView != null)
      {
        i = ExtendableListView.a(ExtendableListView.this);
        long l = ExtendableListView.this.a.getItemId(ExtendableListView.a(ExtendableListView.this) + ExtendableListView.this.b);
        boolean bool2 = false;
        boolean bool1 = bool2;
        if (b())
        {
          bool1 = bool2;
          if (!ExtendableListView.b(ExtendableListView.this)) {
            bool1 = ExtendableListView.a(ExtendableListView.this, localView, ExtendableListView.this.b + i, l);
          }
        }
        if (bool1)
        {
          ExtendableListView.a(ExtendableListView.this, 0);
          ExtendableListView.this.setPressed(false);
          localView.setPressed(false);
        }
      }
      else
      {
        return;
      }
      ExtendableListView.a(ExtendableListView.this, 5);
    }
  }
  
  final class c
    implements Runnable
  {
    c() {}
    
    public void run()
    {
      if (ExtendableListView.c(ExtendableListView.this) == 3)
      {
        ExtendableListView.a(ExtendableListView.this, 4);
        View localView = ExtendableListView.this.getChildAt(ExtendableListView.a(ExtendableListView.this));
        if ((localView != null) && (!localView.hasFocusable()))
        {
          ExtendableListView.b(ExtendableListView.this, 0);
          if (ExtendableListView.b(ExtendableListView.this)) {
            break label167;
          }
          ExtendableListView.this.layoutChildren();
          localView.setPressed(true);
          ExtendableListView.this.setPressed(true);
          int i = ViewConfiguration.getLongPressTimeout();
          if (!ExtendableListView.this.isLongClickable()) {
            break label157;
          }
          if (ExtendableListView.d(ExtendableListView.this) == null) {
            ExtendableListView.a(ExtendableListView.this, new ExtendableListView.b(ExtendableListView.this, null));
          }
          ExtendableListView.d(ExtendableListView.this).a();
          ExtendableListView.this.postDelayed(ExtendableListView.d(ExtendableListView.this), i);
        }
      }
      return;
      label157:
      ExtendableListView.a(ExtendableListView.this, 5);
      return;
      label167:
      ExtendableListView.a(ExtendableListView.this, 5);
    }
  }
  
  public class d
  {
    public View a;
    public Object b;
    public boolean c;
    
    public d() {}
  }
  
  private class e
    implements Runnable
  {
    private final Scroller b = new Scroller(ExtendableListView.this.getContext());
    private int c;
    
    e() {}
    
    private void a()
    {
      this.c = 0;
      ExtendableListView.a(ExtendableListView.this, 0);
      ExtendableListView.this.g(0);
      ExtendableListView.this.removeCallbacks(this);
      this.b.forceFinished(true);
    }
    
    void a(int paramInt)
    {
      if (paramInt < 0) {}
      for (int i = Integer.MAX_VALUE;; i = 0)
      {
        this.c = i;
        this.b.forceFinished(true);
        this.b.fling(0, i, 0, paramInt, 0, Integer.MAX_VALUE, 0, Integer.MAX_VALUE);
        ExtendableListView.a(ExtendableListView.this, 2);
        ExtendableListView.a(ExtendableListView.this, this);
        return;
      }
    }
    
    public void run()
    {
      switch (ExtendableListView.c(ExtendableListView.this))
      {
      default: 
        return;
      }
      if ((ExtendableListView.e(ExtendableListView.this) == 0) || (ExtendableListView.this.getChildCount() == 0))
      {
        a();
        return;
      }
      Scroller localScroller = this.b;
      boolean bool1 = localScroller.computeScrollOffset();
      int j = localScroller.getCurrY();
      int i = this.c - j;
      if (i > 0) {
        ExtendableListView.c(ExtendableListView.this, ExtendableListView.this.b);
      }
      for (i = Math.min(ExtendableListView.this.getHeight() - ExtendableListView.this.getPaddingBottom() - ExtendableListView.this.getPaddingTop() - 1, i);; i = Math.max(-(ExtendableListView.this.getHeight() - ExtendableListView.this.getPaddingBottom() - ExtendableListView.this.getPaddingTop() - 1), i))
      {
        boolean bool2 = ExtendableListView.a(ExtendableListView.this, i, i);
        if ((!bool1) || (bool2)) {
          break;
        }
        ExtendableListView.this.invalidate();
        this.c = j;
        ExtendableListView.a(ExtendableListView.this, this);
        return;
        int k = ExtendableListView.this.getChildCount();
        ExtendableListView.c(ExtendableListView.this, ExtendableListView.this.b + (k - 1));
      }
      a();
    }
  }
  
  private class f
    extends ExtendableListView.h
    implements Runnable
  {
    int a;
    
    private f()
    {
      super(null);
    }
    
    public void run()
    {
      if (ExtendableListView.b(ExtendableListView.this)) {}
      ListAdapter localListAdapter;
      int i;
      View localView;
      do
      {
        do
        {
          return;
          localListAdapter = ExtendableListView.this.a;
          i = this.a;
        } while ((localListAdapter == null) || (ExtendableListView.e(ExtendableListView.this) <= 0) || (i == -1) || (i >= localListAdapter.getCount()) || (!b()));
        localView = ExtendableListView.this.getChildAt(i);
      } while (localView == null);
      i += ExtendableListView.this.b;
      ExtendableListView.this.performItemClick(localView, i, localListAdapter.getItemId(i));
    }
  }
  
  class g
  {
    private int b;
    private View[] c = new View[0];
    private ArrayList<View>[] d;
    private int e;
    private ArrayList<View> f;
    private ArrayList<View> g;
    private SparseArrayCompat<View> h;
    
    g() {}
    
    private void f()
    {
      int m = this.c.length;
      int n = this.e;
      ArrayList[] arrayOfArrayList = this.d;
      int i = 0;
      int j;
      while (i < n)
      {
        ArrayList localArrayList = arrayOfArrayList[i];
        int i1 = localArrayList.size();
        int k = 0;
        j = i1 - 1;
        while (k < i1 - m)
        {
          ExtendableListView.e(ExtendableListView.this, (View)localArrayList.remove(j), false);
          k += 1;
          j -= 1;
        }
        i += 1;
      }
      if (this.h != null) {
        for (i = 0; i < this.h.b(); i = j + 1)
        {
          j = i;
          if (!ViewCompat.c((View)this.h.f(i)))
          {
            this.h.d(i);
            j = i - 1;
          }
        }
      }
    }
    
    public void a()
    {
      ArrayList localArrayList;
      int j;
      if (this.e == 1)
      {
        localArrayList = this.f;
        j = localArrayList.size();
        i = 0;
        while (i < j)
        {
          ((View)localArrayList.get(i)).forceLayout();
          i += 1;
        }
      }
      int k = this.e;
      int i = 0;
      while (i < k)
      {
        localArrayList = this.d[i];
        int m = localArrayList.size();
        j = 0;
        while (j < m)
        {
          ((View)localArrayList.get(j)).forceLayout();
          j += 1;
        }
        i += 1;
      }
      if (this.h != null)
      {
        j = this.h.b();
        i = 0;
        while (i < j)
        {
          ((View)this.h.f(i)).forceLayout();
          i += 1;
        }
      }
    }
    
    public void a(int paramInt)
    {
      if (paramInt < 1) {
        throw new IllegalArgumentException("Can't have a viewTypeCount < 1");
      }
      ArrayList[] arrayOfArrayList = new ArrayList[paramInt];
      int i = 0;
      while (i < paramInt)
      {
        arrayOfArrayList[i] = new ArrayList();
        i += 1;
      }
      this.e = paramInt;
      this.f = arrayOfArrayList[0];
      this.d = arrayOfArrayList;
    }
    
    void a(int paramInt1, int paramInt2)
    {
      if (this.c.length < paramInt1) {
        this.c = new View[paramInt1];
      }
      this.b = paramInt2;
      View[] arrayOfView = this.c;
      paramInt2 = 0;
      while (paramInt2 < paramInt1)
      {
        View localView = ExtendableListView.this.getChildAt(paramInt2);
        ExtendableListView.LayoutParams localLayoutParams = (ExtendableListView.LayoutParams)localView.getLayoutParams();
        if ((localLayoutParams != null) && (localLayoutParams.d != -2)) {
          arrayOfView[paramInt2] = localView;
        }
        paramInt2 += 1;
      }
    }
    
    void a(View paramView, int paramInt)
    {
      ExtendableListView.LayoutParams localLayoutParams = (ExtendableListView.LayoutParams)paramView.getLayoutParams();
      if (localLayoutParams == null) {}
      int i;
      boolean bool;
      do
      {
        return;
        localLayoutParams.b = paramInt;
        i = localLayoutParams.d;
        bool = ViewCompat.c(paramView);
        if ((b(i)) && (!bool)) {
          break;
        }
        if ((i != -2) || (bool))
        {
          if (this.g == null) {
            this.g = new ArrayList();
          }
          this.g.add(paramView);
        }
      } while (!bool);
      if (this.h == null) {
        this.h = new SparseArrayCompat();
      }
      this.h.b(paramInt, paramView);
      return;
      if (this.e == 1)
      {
        this.f.add(paramView);
        return;
      }
      this.d[i].add(paramView);
    }
    
    void b()
    {
      ArrayList localArrayList;
      int j;
      if (this.e == 1)
      {
        localArrayList = this.f;
        j = localArrayList.size();
        i = 0;
        while (i < j)
        {
          ExtendableListView.a(ExtendableListView.this, (View)localArrayList.remove(j - 1 - i), false);
          i += 1;
        }
      }
      int k = this.e;
      int i = 0;
      while (i < k)
      {
        localArrayList = this.d[i];
        int m = localArrayList.size();
        j = 0;
        while (j < m)
        {
          ExtendableListView.b(ExtendableListView.this, (View)localArrayList.remove(m - 1 - j), false);
          j += 1;
        }
        i += 1;
      }
      if (this.h != null) {
        this.h.c();
      }
    }
    
    public boolean b(int paramInt)
    {
      return paramInt >= 0;
    }
    
    View c(int paramInt)
    {
      paramInt -= this.b;
      View[] arrayOfView = this.c;
      if ((paramInt >= 0) && (paramInt < arrayOfView.length))
      {
        View localView = arrayOfView[paramInt];
        arrayOfView[paramInt] = null;
        return localView;
      }
      return null;
    }
    
    void c()
    {
      if (this.h != null) {
        this.h.c();
      }
    }
    
    View d(int paramInt)
    {
      if (this.e == 1) {
        return ExtendableListView.a(this.f, paramInt);
      }
      int i = ExtendableListView.this.a.getItemViewType(paramInt);
      if ((i >= 0) && (i < this.d.length)) {
        return ExtendableListView.a(this.d[i], paramInt);
      }
      return null;
    }
    
    void d()
    {
      if (this.g == null) {
        return;
      }
      int j = this.g.size();
      int i = 0;
      while (i < j)
      {
        ExtendableListView.c(ExtendableListView.this, (View)this.g.get(i), false);
        i += 1;
      }
      this.g.clear();
    }
    
    void e()
    {
      int i = 1;
      View[] arrayOfView = this.c;
      Object localObject1;
      int j;
      label28:
      View localView;
      int k;
      if (this.e > 1)
      {
        localObject1 = this.f;
        j = arrayOfView.length - 1;
        if (j < 0) {
          break label209;
        }
        localView = arrayOfView[j];
        localObject2 = localObject1;
        if (localView != null)
        {
          localObject2 = (ExtendableListView.LayoutParams)localView.getLayoutParams();
          arrayOfView[j] = null;
          boolean bool = ViewCompat.c(localView);
          k = ((ExtendableListView.LayoutParams)localObject2).d;
          if ((b(k)) && (!bool)) {
            break label171;
          }
          if ((k != -2) || (bool)) {
            ExtendableListView.d(ExtendableListView.this, localView, false);
          }
          localObject2 = localObject1;
          if (bool)
          {
            if (this.h == null) {
              this.h = new SparseArrayCompat();
            }
            this.h.b(this.b + j, localView);
          }
        }
      }
      for (Object localObject2 = localObject1;; localObject2 = localObject1)
      {
        j -= 1;
        localObject1 = localObject2;
        break label28;
        i = 0;
        break;
        label171:
        if (i != 0) {
          localObject1 = this.d[k];
        }
        ((ExtendableListView.LayoutParams)localObject2).b = (this.b + j);
        ((ArrayList)localObject1).add(localView);
      }
      label209:
      f();
    }
  }
  
  private class h
  {
    private int a;
    
    private h() {}
    
    public void a()
    {
      this.a = ExtendableListView.i(ExtendableListView.this);
    }
    
    public boolean b()
    {
      return (ExtendableListView.this.hasWindowFocus()) && (ExtendableListView.j(ExtendableListView.this) == this.a);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/etsy/android/grid/ExtendableListView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */