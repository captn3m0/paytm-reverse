package com.etsy.android.grid;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.widget.ListAdapter;
import java.util.Arrays;

public class StaggeredGridView
  extends ExtendableListView
{
  private int j;
  private int k;
  private int l;
  private boolean m;
  private int n = 2;
  private int o = 2;
  private SparseArray<GridItemRecord> p;
  private int q;
  private int r;
  private int s;
  private int t;
  private int[] u;
  private int[] v;
  private int[] w;
  private int x;
  
  public StaggeredGridView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public StaggeredGridView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public StaggeredGridView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    if (paramAttributeSet != null)
    {
      paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, b.a.StaggeredGridView, paramInt, 0);
      this.j = paramContext.getInteger(b.a.StaggeredGridView_column_count, 0);
      if (this.j <= 0) {
        break label169;
      }
      this.n = this.j;
    }
    for (this.o = this.j;; this.o = paramContext.getInteger(b.a.StaggeredGridView_column_count_landscape, 2))
    {
      this.k = paramContext.getDimensionPixelSize(b.a.StaggeredGridView_item_margin, 0);
      this.q = paramContext.getDimensionPixelSize(b.a.StaggeredGridView_grid_paddingLeft, 0);
      this.r = paramContext.getDimensionPixelSize(b.a.StaggeredGridView_grid_paddingRight, 0);
      this.s = paramContext.getDimensionPixelSize(b.a.StaggeredGridView_grid_paddingTop, 0);
      this.t = paramContext.getDimensionPixelSize(b.a.StaggeredGridView_grid_paddingBottom, 0);
      paramContext.recycle();
      this.j = 0;
      this.u = new int[0];
      this.v = new int[0];
      this.w = new int[0];
      this.p = new SparseArray();
      return;
      label169:
      this.n = paramContext.getInteger(b.a.StaggeredGridView_column_count_portrait, 2);
    }
  }
  
  private int b(int paramInt, boolean paramBoolean)
  {
    int i = n(paramInt);
    int i1 = this.j;
    if (i >= 0)
    {
      paramInt = i;
      if (i < i1) {}
    }
    else
    {
      if (!paramBoolean) {
        break label35;
      }
      paramInt = getHighestPositionedBottomColumn();
    }
    return paramInt;
    label35:
    return getLowestPositionedTopColumn();
  }
  
  private void b(View paramView, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3)
  {
    int i1 = n(paramInt1);
    int i2 = h(paramInt1);
    int i3 = getChildBottomMargin();
    int i4 = i2 + i3;
    int i;
    if (paramBoolean)
    {
      i = this.v[i1];
      paramInt1 = i + (d(paramView) + i4);
    }
    for (;;)
    {
      ((GridLayoutParams)paramView.getLayoutParams()).e = i1;
      e(i1, paramInt1);
      d(i1, i);
      paramView.layout(paramInt2, i + i2, paramInt3, paramInt1 - i3);
      return;
      paramInt1 = this.u[i1];
      i = paramInt1 - (d(paramView) + i4);
    }
  }
  
  private void b(View paramView, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    if (paramBoolean)
    {
      paramInt3 = getLowestPositionedBottom();
      paramInt5 = paramInt3 + d(paramView);
    }
    for (;;)
    {
      int i = 0;
      while (i < this.j)
      {
        d(i, paramInt3);
        e(i, paramInt5);
        i += 1;
      }
      paramInt5 = getHighestPositionedTop();
      paramInt3 = paramInt5 - d(paramView);
    }
    super.a(paramView, paramInt1, paramBoolean, paramInt2, paramInt3, paramInt4, paramInt5);
  }
  
  private void c(View paramView, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3)
  {
    int i;
    if (paramBoolean)
    {
      paramInt3 = getLowestPositionedBottom();
      i = paramInt3 + d(paramView);
    }
    for (;;)
    {
      int i1 = 0;
      while (i1 < this.j)
      {
        d(i1, paramInt3);
        e(i1, i);
        i1 += 1;
      }
      i = getHighestPositionedTop();
      paramInt3 = i - d(paramView);
    }
    super.a(paramView, paramInt1, paramBoolean, paramInt2, paramInt3);
  }
  
  private int d(View paramView)
  {
    return paramView.getMeasuredHeight();
  }
  
  private void d(int paramInt1, int paramInt2)
  {
    if (paramInt2 < this.u[paramInt1]) {
      this.u[paramInt1] = paramInt2;
    }
  }
  
  private void d(View paramView, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3)
  {
    int i1 = n(paramInt1);
    int i2 = h(paramInt1);
    int i3 = i2 + getChildBottomMargin();
    int i;
    if (paramBoolean)
    {
      i = this.v[i1];
      paramInt3 = i + (d(paramView) + i3);
    }
    for (;;)
    {
      ((GridLayoutParams)paramView.getLayoutParams()).e = i1;
      e(i1, paramInt3);
      d(i1, i);
      super.a(paramView, paramInt1, paramBoolean, paramInt2, i + i2);
      return;
      paramInt3 = this.u[i1];
      i = paramInt3 - (d(paramView) + i3);
    }
  }
  
  private void e(int paramInt1, int paramInt2)
  {
    if (paramInt2 > this.v[paramInt1]) {
      this.v[paramInt1] = paramInt2;
    }
  }
  
  private void f(int paramInt1, int paramInt2)
  {
    if (paramInt1 != 0)
    {
      int[] arrayOfInt = this.u;
      arrayOfInt[paramInt2] += paramInt1;
      arrayOfInt = this.v;
      arrayOfInt[paramInt2] += paramInt1;
    }
  }
  
  private void g(int paramInt1, int paramInt2)
  {
    m(paramInt1).a = paramInt2;
  }
  
  private int getChildBottomMargin()
  {
    return this.k;
  }
  
  private int[] getHighestNonHeaderTops()
  {
    int[] arrayOfInt = new int[this.j];
    int i1 = getChildCount();
    if (i1 > 0)
    {
      int i = 0;
      while (i < i1)
      {
        View localView = getChildAt(i);
        if ((localView != null) && (localView.getLayoutParams() != null) && ((localView.getLayoutParams() instanceof GridLayoutParams)))
        {
          GridLayoutParams localGridLayoutParams = (GridLayoutParams)localView.getLayoutParams();
          if ((localGridLayoutParams.d != -2) && (localView.getTop() < arrayOfInt[localGridLayoutParams.e])) {
            arrayOfInt[localGridLayoutParams.e] = localView.getTop();
          }
        }
        i += 1;
      }
    }
    return arrayOfInt;
  }
  
  private int getHighestPositionedBottom()
  {
    int i = getHighestPositionedBottomColumn();
    return this.v[i];
  }
  
  private int getHighestPositionedBottomColumn()
  {
    int i3 = 0;
    int i1 = Integer.MAX_VALUE;
    int i = 0;
    while (i < this.j)
    {
      int i4 = this.v[i];
      int i2 = i1;
      if (i4 < i1)
      {
        i2 = i4;
        i3 = i;
      }
      i += 1;
      i1 = i2;
    }
    return i3;
  }
  
  private int getHighestPositionedTop()
  {
    int i = getHighestPositionedTopColumn();
    return this.u[i];
  }
  
  private int getHighestPositionedTopColumn()
  {
    int i3 = 0;
    int i1 = Integer.MAX_VALUE;
    int i = 0;
    while (i < this.j)
    {
      int i4 = this.u[i];
      int i2 = i1;
      if (i4 < i1)
      {
        i2 = i4;
        i3 = i;
      }
      i += 1;
      i1 = i2;
    }
    return i3;
  }
  
  private int getLowestPositionedBottom()
  {
    int i = getLowestPositionedBottomColumn();
    return this.v[i];
  }
  
  private int getLowestPositionedBottomColumn()
  {
    int i3 = 0;
    int i1 = Integer.MIN_VALUE;
    int i = 0;
    while (i < this.j)
    {
      int i4 = this.v[i];
      int i2 = i1;
      if (i4 > i1)
      {
        i2 = i4;
        i3 = i;
      }
      i += 1;
      i1 = i2;
    }
    return i3;
  }
  
  private int getLowestPositionedTop()
  {
    int i = getLowestPositionedTopColumn();
    return this.u[i];
  }
  
  private int getLowestPositionedTopColumn()
  {
    int i3 = 0;
    int i1 = Integer.MIN_VALUE;
    int i = 0;
    while (i < this.j)
    {
      int i4 = this.u[i];
      int i2 = i1;
      if (i4 > i1)
      {
        i2 = i4;
        i3 = i;
      }
      i += 1;
      i1 = i2;
    }
    return i3;
  }
  
  private int h(int paramInt)
  {
    int i = 0;
    if (paramInt < getHeaderViewsCount() + this.j) {}
    for (paramInt = 1;; paramInt = 0)
    {
      if (paramInt != 0) {
        i = this.k;
      }
      return i;
    }
  }
  
  private void h(int paramInt1, int paramInt2)
  {
    m(paramInt1).b = (paramInt2 / this.l);
  }
  
  private void i(int paramInt)
  {
    this.x += paramInt;
  }
  
  private boolean i()
  {
    return getResources().getConfiguration().orientation == 2;
  }
  
  private void j()
  {
    int i1 = getChildCount();
    int i = 0;
    while (i < i1)
    {
      View localView = getChildAt(i);
      if (localView != null) {
        localView.requestLayout();
      }
      i += 1;
    }
  }
  
  private void j(int paramInt)
  {
    if (paramInt != 0)
    {
      int i = 0;
      while (i < this.j)
      {
        f(paramInt, i);
        i += 1;
      }
    }
  }
  
  private int k(int paramInt)
  {
    return (paramInt - (getRowPaddingLeft() + getRowPaddingRight()) - this.k * (this.j + 1)) / this.j;
  }
  
  private void k()
  {
    if (!this.m) {
      Arrays.fill(this.v, 0);
    }
    for (;;)
    {
      System.arraycopy(this.u, 0, this.v, 0, this.j);
      return;
      this.m = false;
    }
  }
  
  private int l(int paramInt)
  {
    return getRowPaddingLeft() + this.k + (this.k + this.l) * paramInt;
  }
  
  private void l()
  {
    int[] arrayOfInt;
    int i2;
    int i1;
    if (this.b == getHeaderViewsCount())
    {
      arrayOfInt = getHighestNonHeaderTops();
      int i4 = 1;
      i2 = -1;
      i1 = Integer.MAX_VALUE;
      i = 0;
      while (i < arrayOfInt.length)
      {
        int i3 = i4;
        if (i4 != 0)
        {
          i3 = i4;
          if (i > 0)
          {
            i3 = i4;
            if (arrayOfInt[i] != i1) {
              i3 = 0;
            }
          }
        }
        i4 = i1;
        if (arrayOfInt[i] < i1)
        {
          i4 = arrayOfInt[i];
          i2 = i;
        }
        i += 1;
        i1 = i4;
        i4 = i3;
      }
      if (i4 == 0) {}
    }
    else
    {
      return;
    }
    int i = 0;
    while (i < arrayOfInt.length)
    {
      if (i != i2) {
        c(i1 - arrayOfInt[i], i);
      }
      i += 1;
    }
    invalidate();
  }
  
  private GridItemRecord m(int paramInt)
  {
    GridItemRecord localGridItemRecord2 = (GridItemRecord)this.p.get(paramInt, null);
    GridItemRecord localGridItemRecord1 = localGridItemRecord2;
    if (localGridItemRecord2 == null)
    {
      localGridItemRecord1 = new GridItemRecord();
      this.p.append(paramInt, localGridItemRecord1);
    }
    return localGridItemRecord1;
  }
  
  private void m()
  {
    int i2 = Math.min(this.e, getCount() - 1);
    SparseArray localSparseArray = new SparseArray(i2);
    int i = 0;
    Object localObject;
    if (i < i2)
    {
      localObject = (GridItemRecord)this.p.get(i);
      if (localObject != null) {}
    }
    else
    {
      this.p.clear();
      i = 0;
    }
    for (;;)
    {
      if (i < i2)
      {
        localObject = (Double)localSparseArray.get(i);
        if (localObject != null) {}
      }
      else
      {
        i = getHighestPositionedBottomColumn();
        g(i2, i);
        i = this.v[i];
        j(-i + this.f);
        this.x = (-i);
        System.arraycopy(this.v, 0, this.u, 0, this.j);
        return;
        Log.d("StaggeredGridView", "onColumnSync:" + i + " ratio:" + ((GridItemRecord)localObject).b);
        localSparseArray.append(i, Double.valueOf(((GridItemRecord)localObject).b));
        i += 1;
        break;
      }
      GridItemRecord localGridItemRecord = m(i);
      int i3 = (int)(this.l * ((Double)localObject).doubleValue());
      localGridItemRecord.b = ((Double)localObject).doubleValue();
      if (o(i))
      {
        i4 = getLowestPositionedBottom();
        i1 = 0;
        while (i1 < this.j)
        {
          this.u[i1] = i4;
          this.v[i1] = (i4 + i3);
          i1 += 1;
        }
      }
      int i1 = getHighestPositionedBottomColumn();
      int i4 = this.v[i1];
      int i5 = h(i);
      int i6 = getChildBottomMargin();
      this.u[i1] = i4;
      this.v[i1] = (i4 + i3 + i5 + i6);
      localGridItemRecord.a = i1;
      i += 1;
    }
  }
  
  private int n(int paramInt)
  {
    GridItemRecord localGridItemRecord = (GridItemRecord)this.p.get(paramInt, null);
    if (localGridItemRecord != null) {
      return localGridItemRecord.a;
    }
    return -1;
  }
  
  private void n()
  {
    o();
    p();
  }
  
  private void o()
  {
    Arrays.fill(this.u, getPaddingTop() + this.s);
  }
  
  private boolean o(int paramInt)
  {
    return this.a.getItemViewType(paramInt) == -2;
  }
  
  private void p()
  {
    Arrays.fill(this.v, getPaddingTop() + this.s);
  }
  
  private void q()
  {
    int i = 0;
    while (i < this.j)
    {
      this.w[i] = l(i);
      i += 1;
    }
  }
  
  private void setPositionIsHeaderFooter(int paramInt)
  {
    m(paramInt).c = true;
  }
  
  protected int a(int paramInt)
  {
    if (o(paramInt)) {
      return super.a(paramInt);
    }
    paramInt = n(paramInt);
    return this.w[paramInt];
  }
  
  public void a()
  {
    if (this.j > 0)
    {
      if (this.u == null) {
        this.u = new int[this.j];
      }
      if (this.v == null) {
        this.v = new int[this.j];
      }
      n();
      this.p.clear();
      this.m = false;
      this.x = 0;
      setSelection(0);
    }
  }
  
  protected void a(int paramInt1, int paramInt2)
  {
    super.a(paramInt1, paramInt2);
    if (i()) {}
    for (paramInt2 = this.o;; paramInt2 = this.n)
    {
      this.j = paramInt2;
      this.l = k(paramInt1);
      this.u = new int[this.j];
      this.v = new int[this.j];
      this.w = new int[this.j];
      this.x = 0;
      n();
      q();
      if ((getCount() > 0) && (this.p.size() > 0)) {
        m();
      }
      requestLayout();
      return;
    }
  }
  
  protected void a(int paramInt, boolean paramBoolean)
  {
    super.a(paramInt, paramBoolean);
    if (!o(paramInt))
    {
      g(paramInt, b(paramInt, paramBoolean));
      return;
    }
    setPositionIsHeaderFooter(paramInt);
  }
  
  protected void a(View paramView, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3)
  {
    if (o(paramInt1))
    {
      c(paramView, paramInt1, paramBoolean, paramInt2, paramInt3);
      return;
    }
    d(paramView, paramInt1, paramBoolean, paramInt2, paramInt3);
  }
  
  protected void a(View paramView, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    if (o(paramInt1))
    {
      b(paramView, paramInt1, paramBoolean, paramInt2, paramInt3, paramInt4, paramInt5);
      return;
    }
    b(paramView, paramInt1, paramBoolean, paramInt2, paramInt4);
  }
  
  protected void a(View paramView, ExtendableListView.LayoutParams paramLayoutParams)
  {
    int i = paramLayoutParams.d;
    int i1 = paramLayoutParams.b;
    if ((i == -2) || (i == -1))
    {
      super.a(paramView, paramLayoutParams);
      h(i1, d(paramView));
      return;
    }
    int i2 = View.MeasureSpec.makeMeasureSpec(this.l, 1073741824);
    if (paramLayoutParams.height > 0) {}
    for (i = View.MeasureSpec.makeMeasureSpec(paramLayoutParams.height, 1073741824);; i = View.MeasureSpec.makeMeasureSpec(-2, 0))
    {
      paramView.measure(i2, i);
      break;
    }
  }
  
  protected int b(int paramInt)
  {
    if (o(paramInt)) {
      return super.b(paramInt);
    }
    paramInt = n(paramInt);
    if (paramInt == -1) {
      return getHighestPositionedBottom();
    }
    return this.v[paramInt];
  }
  
  protected ExtendableListView.LayoutParams b(View paramView)
  {
    Object localObject = null;
    ViewGroup.LayoutParams localLayoutParams = paramView.getLayoutParams();
    paramView = (View)localObject;
    if (localLayoutParams != null) {
      if (!(localLayoutParams instanceof GridLayoutParams)) {
        break label47;
      }
    }
    label47:
    for (paramView = (GridLayoutParams)localLayoutParams;; paramView = new GridLayoutParams(localLayoutParams))
    {
      localObject = paramView;
      if (paramView == null) {
        localObject = new GridLayoutParams(this.l, -2);
      }
      return (ExtendableListView.LayoutParams)localObject;
    }
  }
  
  protected void b(int paramInt1, int paramInt2)
  {
    super.b(paramInt1, paramInt2);
    Arrays.fill(this.u, Integer.MAX_VALUE);
    Arrays.fill(this.v, 0);
    paramInt1 = 0;
    if (paramInt1 < getChildCount())
    {
      View localView = getChildAt(paramInt1);
      int i;
      int i1;
      if (localView != null)
      {
        Object localObject = (ExtendableListView.LayoutParams)localView.getLayoutParams();
        if ((((ExtendableListView.LayoutParams)localObject).d == -2) || (!(localObject instanceof GridLayoutParams))) {
          break label159;
        }
        localObject = (GridLayoutParams)localObject;
        paramInt2 = ((GridLayoutParams)localObject).e;
        i = ((GridLayoutParams)localObject).b;
        i1 = localView.getTop();
        if (i1 < this.u[paramInt2]) {
          this.u[paramInt2] = (i1 - h(i));
        }
        i = localView.getBottom();
        if (i > this.v[paramInt2]) {
          this.v[paramInt2] = (getChildBottomMargin() + i);
        }
      }
      for (;;)
      {
        paramInt1 += 1;
        break;
        label159:
        i = localView.getTop();
        i1 = localView.getBottom();
        paramInt2 = 0;
        while (paramInt2 < this.j)
        {
          if (i < this.u[paramInt2]) {
            this.u[paramInt2] = i;
          }
          if (i1 > this.v[paramInt2]) {
            this.v[paramInt2] = i1;
          }
          paramInt2 += 1;
        }
      }
    }
  }
  
  protected void b(boolean paramBoolean)
  {
    super.b(paramBoolean);
    if (!paramBoolean) {
      l();
    }
  }
  
  protected int c(int paramInt)
  {
    if (o(paramInt)) {
      return super.c(paramInt);
    }
    paramInt = n(paramInt);
    if (paramInt == -1) {
      return getLowestPositionedTop();
    }
    return this.u[paramInt];
  }
  
  protected void c(int paramInt1, int paramInt2)
  {
    int i1 = getChildCount();
    int i = 0;
    while (i < i1)
    {
      View localView = getChildAt(i);
      if ((localView != null) && (localView.getLayoutParams() != null) && ((localView.getLayoutParams() instanceof GridLayoutParams)) && (((GridLayoutParams)localView.getLayoutParams()).e == paramInt2)) {
        localView.offsetTopAndBottom(paramInt1);
      }
      i += 1;
    }
    f(paramInt1, paramInt2);
  }
  
  protected boolean c()
  {
    boolean bool = false;
    if (this.d) {}
    for (int i = getRowPaddingTop();; i = 0)
    {
      if (getLowestPositionedTop() > i) {
        bool = true;
      }
      return bool;
    }
  }
  
  protected int d(int paramInt)
  {
    if (o(paramInt)) {
      return super.d(paramInt);
    }
    return getHighestPositionedBottom();
  }
  
  protected int e(int paramInt)
  {
    if (o(paramInt)) {
      return super.e(paramInt);
    }
    return getLowestPositionedTop();
  }
  
  protected void f(int paramInt)
  {
    super.f(paramInt);
    j(paramInt);
    i(paramInt);
  }
  
  public int getColumnWidth()
  {
    return this.l;
  }
  
  public int getDistanceToTop()
  {
    return this.x;
  }
  
  protected int getFirstChildTop()
  {
    if (o(this.b)) {
      return super.getFirstChildTop();
    }
    return getLowestPositionedTop();
  }
  
  protected int getHighestChildTop()
  {
    if (o(this.b)) {
      return super.getHighestChildTop();
    }
    return getHighestPositionedTop();
  }
  
  protected int getLastChildBottom()
  {
    if (o(this.b + (getChildCount() - 1))) {
      return super.getLastChildBottom();
    }
    return getHighestPositionedBottom();
  }
  
  protected int getLowestChildBottom()
  {
    if (o(this.b + (getChildCount() - 1))) {
      return super.getLowestChildBottom();
    }
    return getLowestPositionedBottom();
  }
  
  public int getRowPaddingBottom()
  {
    return getListPaddingBottom() + this.t;
  }
  
  public int getRowPaddingLeft()
  {
    return getListPaddingLeft() + this.q;
  }
  
  public int getRowPaddingRight()
  {
    return getListPaddingRight() + this.r;
  }
  
  public int getRowPaddingTop()
  {
    return getListPaddingTop() + this.s;
  }
  
  protected void layoutChildren()
  {
    k();
    super.layoutChildren();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    if (this.j <= 0) {
      if (!i()) {
        break label142;
      }
    }
    label142:
    for (paramInt1 = this.o;; paramInt1 = this.n)
    {
      this.j = paramInt1;
      this.l = k(getMeasuredWidth());
      if ((this.u == null) || (this.u.length != this.j))
      {
        this.u = new int[this.j];
        o();
      }
      if ((this.v == null) || (this.v.length != this.j))
      {
        this.v = new int[this.j];
        p();
      }
      if ((this.w == null) || (this.w.length != this.j))
      {
        this.w = new int[this.j];
        q();
      }
      return;
    }
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable)
  {
    paramParcelable = (GridListSavedState)paramParcelable;
    this.j = paramParcelable.g;
    this.u = paramParcelable.h;
    this.v = new int[this.j];
    this.p = paramParcelable.i;
    this.m = true;
    super.onRestoreInstanceState(paramParcelable);
  }
  
  public Parcelable onSaveInstanceState()
  {
    int i1 = 0;
    ExtendableListView.ListSavedState localListSavedState = (ExtendableListView.ListSavedState)super.onSaveInstanceState();
    GridListSavedState localGridListSavedState = new GridListSavedState(localListSavedState.a());
    localGridListSavedState.b = localListSavedState.b;
    localGridListSavedState.c = localListSavedState.c;
    localGridListSavedState.d = localListSavedState.d;
    localGridListSavedState.e = localListSavedState.e;
    localGridListSavedState.f = localListSavedState.f;
    if ((getChildCount() > 0) && (getCount() > 0)) {}
    for (int i = 1; (i != 0) && (this.b > 0); i = 0)
    {
      localGridListSavedState.g = this.j;
      localGridListSavedState.h = this.u;
      localGridListSavedState.i = this.p;
      return localGridListSavedState;
    }
    i = i1;
    if (this.j >= 0) {
      i = this.j;
    }
    localGridListSavedState.g = i;
    localGridListSavedState.h = new int[localGridListSavedState.g];
    localGridListSavedState.i = new SparseArray();
    return localGridListSavedState;
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    a(paramInt1, paramInt2);
  }
  
  public void setColumnCount(int paramInt)
  {
    this.n = paramInt;
    this.o = paramInt;
    a(getWidth(), getHeight());
    j();
  }
  
  public void setColumnCountLandscape(int paramInt)
  {
    this.o = paramInt;
    a(getWidth(), getHeight());
    j();
  }
  
  public void setColumnCountPortrait(int paramInt)
  {
    this.n = paramInt;
    a(getWidth(), getHeight());
    j();
  }
  
  public void setGridPadding(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.q = paramInt1;
    this.s = paramInt2;
    this.r = paramInt3;
    this.t = paramInt4;
  }
  
  static class GridItemRecord
    implements Parcelable
  {
    public static final Parcelable.Creator<GridItemRecord> CREATOR = new Parcelable.Creator()
    {
      public StaggeredGridView.GridItemRecord a(Parcel paramAnonymousParcel)
      {
        return new StaggeredGridView.GridItemRecord(paramAnonymousParcel, null);
      }
      
      public StaggeredGridView.GridItemRecord[] a(int paramAnonymousInt)
      {
        return new StaggeredGridView.GridItemRecord[paramAnonymousInt];
      }
    };
    int a;
    double b;
    boolean c;
    
    GridItemRecord() {}
    
    private GridItemRecord(Parcel paramParcel)
    {
      this.a = paramParcel.readInt();
      this.b = paramParcel.readDouble();
      if (paramParcel.readByte() == 1) {}
      for (;;)
      {
        this.c = bool;
        return;
        bool = false;
      }
    }
    
    public int describeContents()
    {
      return 0;
    }
    
    public String toString()
    {
      return "GridItemRecord.ListSavedState{" + Integer.toHexString(System.identityHashCode(this)) + " column:" + this.a + " heightRatio:" + this.b + " isHeaderFooter:" + this.c + "}";
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      paramParcel.writeInt(this.a);
      paramParcel.writeDouble(this.b);
      if (this.c) {}
      for (paramInt = 1;; paramInt = 0)
      {
        paramParcel.writeByte((byte)paramInt);
        return;
      }
    }
  }
  
  public static class GridLayoutParams
    extends ExtendableListView.LayoutParams
  {
    int e;
    
    public GridLayoutParams(int paramInt1, int paramInt2)
    {
      super(paramInt2);
      a();
    }
    
    public GridLayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
      a();
    }
    
    public GridLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
      a();
    }
    
    private void a()
    {
      if (this.width != -1) {
        this.width = -1;
      }
      if (this.height == -1) {
        this.height = -2;
      }
    }
  }
  
  public static class GridListSavedState
    extends ExtendableListView.ListSavedState
  {
    public static final Parcelable.Creator<GridListSavedState> CREATOR = new Parcelable.Creator()
    {
      public StaggeredGridView.GridListSavedState a(Parcel paramAnonymousParcel)
      {
        return new StaggeredGridView.GridListSavedState(paramAnonymousParcel);
      }
      
      public StaggeredGridView.GridListSavedState[] a(int paramAnonymousInt)
      {
        return new StaggeredGridView.GridListSavedState[paramAnonymousInt];
      }
    };
    int g;
    int[] h;
    SparseArray i;
    
    public GridListSavedState(Parcel paramParcel)
    {
      super();
      this.g = paramParcel.readInt();
      if (this.g >= 0) {}
      for (int j = this.g;; j = 0)
      {
        this.h = new int[j];
        paramParcel.readIntArray(this.h);
        this.i = paramParcel.readSparseArray(StaggeredGridView.GridItemRecord.class.getClassLoader());
        return;
      }
    }
    
    public GridListSavedState(Parcelable paramParcelable)
    {
      super();
    }
    
    public String toString()
    {
      return "StaggeredGridView.GridListSavedState{" + Integer.toHexString(System.identityHashCode(this)) + "}";
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      super.writeToParcel(paramParcel, paramInt);
      paramParcel.writeInt(this.g);
      paramParcel.writeIntArray(this.h);
      paramParcel.writeSparseArray(this.i);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/com/etsy/android/grid/StaggeredGridView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */