package android.support.v13.app;

import android.app.Fragment;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import java.util.Arrays;

public class FragmentCompat
{
  static final FragmentCompatImpl a = new BaseFragmentCompatImpl();
  
  static
  {
    if (Build.VERSION.SDK_INT >= 23)
    {
      a = new MncFragmentCompatImpl();
      return;
    }
    if (Build.VERSION.SDK_INT >= 15)
    {
      a = new ICSMR1FragmentCompatImpl();
      return;
    }
    if (Build.VERSION.SDK_INT >= 14)
    {
      a = new ICSFragmentCompatImpl();
      return;
    }
  }
  
  public static void a(Fragment paramFragment, boolean paramBoolean)
  {
    a.a(paramFragment, paramBoolean);
  }
  
  public static void b(Fragment paramFragment, boolean paramBoolean)
  {
    a.b(paramFragment, paramBoolean);
  }
  
  static class BaseFragmentCompatImpl
    implements FragmentCompat.FragmentCompatImpl
  {
    public void a(Fragment paramFragment, boolean paramBoolean) {}
    
    public void b(Fragment paramFragment, boolean paramBoolean) {}
  }
  
  static abstract interface FragmentCompatImpl
  {
    public abstract void a(Fragment paramFragment, boolean paramBoolean);
    
    public abstract void b(Fragment paramFragment, boolean paramBoolean);
  }
  
  static class ICSFragmentCompatImpl
    extends FragmentCompat.BaseFragmentCompatImpl
  {
    public void a(Fragment paramFragment, boolean paramBoolean)
    {
      FragmentCompatICS.a(paramFragment, paramBoolean);
    }
  }
  
  static class ICSMR1FragmentCompatImpl
    extends FragmentCompat.ICSFragmentCompatImpl
  {
    public void b(Fragment paramFragment, boolean paramBoolean)
    {
      FragmentCompatICSMR1.a(paramFragment, paramBoolean);
    }
  }
  
  static class MncFragmentCompatImpl
    extends FragmentCompat.ICSMR1FragmentCompatImpl
  {}
  
  public static abstract interface OnRequestPermissionsResultCallback
  {
    public abstract void a(int paramInt, @NonNull String[] paramArrayOfString, @NonNull int[] paramArrayOfInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v13/app/FragmentCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */