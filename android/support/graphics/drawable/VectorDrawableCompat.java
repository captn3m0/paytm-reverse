package android.support.graphics.drawable;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.Region.Op;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.graphics.drawable.VectorDrawable;
import android.os.Build.VERSION;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.util.ArrayMap;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

@TargetApi(21)
public class VectorDrawableCompat
  extends VectorDrawableCommon
{
  static final PorterDuff.Mode b = PorterDuff.Mode.SRC_IN;
  private VectorDrawableCompatState c;
  private PorterDuffColorFilter d;
  private ColorFilter e;
  private boolean f;
  private boolean g = true;
  private Drawable.ConstantState h;
  private final float[] i = new float[9];
  private final Matrix j = new Matrix();
  private final Rect k = new Rect();
  
  private VectorDrawableCompat()
  {
    this.c = new VectorDrawableCompatState();
  }
  
  private VectorDrawableCompat(@NonNull VectorDrawableCompatState paramVectorDrawableCompatState)
  {
    this.c = paramVectorDrawableCompatState;
    this.d = a(this.d, paramVectorDrawableCompatState.c, paramVectorDrawableCompatState.d);
  }
  
  private static PorterDuff.Mode a(int paramInt, PorterDuff.Mode paramMode)
  {
    switch (paramInt)
    {
    case 4: 
    case 6: 
    case 7: 
    case 8: 
    case 10: 
    case 11: 
    case 12: 
    case 13: 
    default: 
      return paramMode;
    case 3: 
      return PorterDuff.Mode.SRC_OVER;
    case 5: 
      return PorterDuff.Mode.SRC_IN;
    case 9: 
      return PorterDuff.Mode.SRC_ATOP;
    case 14: 
      return PorterDuff.Mode.MULTIPLY;
    case 15: 
      return PorterDuff.Mode.SCREEN;
    }
    return PorterDuff.Mode.ADD;
  }
  
  @Nullable
  public static VectorDrawableCompat a(@NonNull Resources paramResources, @DrawableRes int paramInt, @Nullable Resources.Theme paramTheme)
  {
    Object localObject;
    if (Build.VERSION.SDK_INT >= 23)
    {
      localObject = new VectorDrawableCompat();
      ((VectorDrawableCompat)localObject).a = ResourcesCompat.a(paramResources, paramInt, paramTheme);
      ((VectorDrawableCompat)localObject).h = new VectorDrawableDelegateState(((VectorDrawableCompat)localObject).a.getConstantState());
      return (VectorDrawableCompat)localObject;
    }
    try
    {
      localObject = paramResources.getXml(paramInt);
      localAttributeSet = Xml.asAttributeSet((XmlPullParser)localObject);
      do
      {
        paramInt = ((XmlPullParser)localObject).next();
      } while ((paramInt != 2) && (paramInt != 1));
      if (paramInt != 2) {
        throw new XmlPullParserException("No start tag found");
      }
    }
    catch (XmlPullParserException paramResources)
    {
      AttributeSet localAttributeSet;
      Log.e("VectorDrawableCompat", "parser error", paramResources);
      return null;
      paramResources = a(paramResources, (XmlPullParser)localObject, localAttributeSet, paramTheme);
      return paramResources;
    }
    catch (IOException paramResources)
    {
      for (;;)
      {
        Log.e("VectorDrawableCompat", "parser error", paramResources);
      }
    }
  }
  
  public static VectorDrawableCompat a(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme)
    throws XmlPullParserException, IOException
  {
    VectorDrawableCompat localVectorDrawableCompat = new VectorDrawableCompat();
    localVectorDrawableCompat.inflate(paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
    return localVectorDrawableCompat;
  }
  
  private void a(TypedArray paramTypedArray, XmlPullParser paramXmlPullParser)
    throws XmlPullParserException
  {
    VectorDrawableCompatState localVectorDrawableCompatState = this.c;
    VPathRenderer localVPathRenderer = localVectorDrawableCompatState.b;
    localVectorDrawableCompatState.d = a(TypedArrayUtils.a(paramTypedArray, paramXmlPullParser, "tintMode", 6, -1), PorterDuff.Mode.SRC_IN);
    ColorStateList localColorStateList = paramTypedArray.getColorStateList(1);
    if (localColorStateList != null) {
      localVectorDrawableCompatState.c = localColorStateList;
    }
    localVectorDrawableCompatState.e = TypedArrayUtils.a(paramTypedArray, paramXmlPullParser, "autoMirrored", 5, localVectorDrawableCompatState.e);
    localVPathRenderer.c = TypedArrayUtils.a(paramTypedArray, paramXmlPullParser, "viewportWidth", 7, localVPathRenderer.c);
    localVPathRenderer.d = TypedArrayUtils.a(paramTypedArray, paramXmlPullParser, "viewportHeight", 8, localVPathRenderer.d);
    if (localVPathRenderer.c <= 0.0F) {
      throw new XmlPullParserException(paramTypedArray.getPositionDescription() + "<vector> tag requires viewportWidth > 0");
    }
    if (localVPathRenderer.d <= 0.0F) {
      throw new XmlPullParserException(paramTypedArray.getPositionDescription() + "<vector> tag requires viewportHeight > 0");
    }
    localVPathRenderer.a = paramTypedArray.getDimension(3, localVPathRenderer.a);
    localVPathRenderer.b = paramTypedArray.getDimension(2, localVPathRenderer.b);
    if (localVPathRenderer.a <= 0.0F) {
      throw new XmlPullParserException(paramTypedArray.getPositionDescription() + "<vector> tag requires width > 0");
    }
    if (localVPathRenderer.b <= 0.0F) {
      throw new XmlPullParserException(paramTypedArray.getPositionDescription() + "<vector> tag requires height > 0");
    }
    localVPathRenderer.a(TypedArrayUtils.a(paramTypedArray, paramXmlPullParser, "alpha", 4, localVPathRenderer.b()));
    paramTypedArray = paramTypedArray.getString(0);
    if (paramTypedArray != null)
    {
      localVPathRenderer.f = paramTypedArray;
      localVPathRenderer.g.put(paramTypedArray, localVPathRenderer);
    }
  }
  
  private boolean a()
  {
    return false;
  }
  
  private static int b(int paramInt, float paramFloat)
  {
    return paramInt & 0xFFFFFF | (int)(Color.alpha(paramInt) * paramFloat) << 24;
  }
  
  private void b(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme)
    throws XmlPullParserException, IOException
  {
    VectorDrawableCompatState localVectorDrawableCompatState = this.c;
    VPathRenderer localVPathRenderer = localVectorDrawableCompatState.b;
    int m = 1;
    Stack localStack = new Stack();
    localStack.push(VPathRenderer.a(localVPathRenderer));
    int i1 = paramXmlPullParser.getEventType();
    if (i1 != 1)
    {
      Object localObject;
      VGroup localVGroup;
      int n;
      if (i1 == 2)
      {
        localObject = paramXmlPullParser.getName();
        localVGroup = (VGroup)localStack.peek();
        if ("path".equals(localObject))
        {
          localObject = new VFullPath();
          ((VFullPath)localObject).a(paramResources, paramAttributeSet, paramTheme, paramXmlPullParser);
          localVGroup.a.add(localObject);
          if (((VFullPath)localObject).b() != null) {
            localVPathRenderer.g.put(((VFullPath)localObject).b(), localObject);
          }
          n = 0;
          localVectorDrawableCompatState.a |= ((VFullPath)localObject).o;
        }
      }
      for (;;)
      {
        i1 = paramXmlPullParser.next();
        m = n;
        break;
        if ("clip-path".equals(localObject))
        {
          localObject = new VClipPath();
          ((VClipPath)localObject).a(paramResources, paramAttributeSet, paramTheme, paramXmlPullParser);
          localVGroup.a.add(localObject);
          if (((VClipPath)localObject).b() != null) {
            localVPathRenderer.g.put(((VClipPath)localObject).b(), localObject);
          }
          localVectorDrawableCompatState.a |= ((VClipPath)localObject).o;
          n = m;
        }
        else
        {
          n = m;
          if ("group".equals(localObject))
          {
            localObject = new VGroup();
            ((VGroup)localObject).a(paramResources, paramAttributeSet, paramTheme, paramXmlPullParser);
            localVGroup.a.add(localObject);
            localStack.push(localObject);
            if (((VGroup)localObject).a() != null) {
              localVPathRenderer.g.put(((VGroup)localObject).a(), localObject);
            }
            localVectorDrawableCompatState.a |= VGroup.a((VGroup)localObject);
            n = m;
            continue;
            n = m;
            if (i1 == 3)
            {
              n = m;
              if ("group".equals(paramXmlPullParser.getName()))
              {
                localStack.pop();
                n = m;
              }
            }
          }
        }
      }
    }
    if (m != 0)
    {
      paramResources = new StringBuffer();
      if (paramResources.length() > 0) {
        paramResources.append(" or ");
      }
      paramResources.append("path");
      throw new XmlPullParserException("no " + paramResources + " defined");
    }
  }
  
  PorterDuffColorFilter a(PorterDuffColorFilter paramPorterDuffColorFilter, ColorStateList paramColorStateList, PorterDuff.Mode paramMode)
  {
    if ((paramColorStateList == null) || (paramMode == null)) {
      return null;
    }
    return new PorterDuffColorFilter(paramColorStateList.getColorForState(getState(), 0), paramMode);
  }
  
  Object a(String paramString)
  {
    return this.c.b.g.get(paramString);
  }
  
  void a(boolean paramBoolean)
  {
    this.g = paramBoolean;
  }
  
  public boolean canApplyTheme()
  {
    if (this.a != null) {
      DrawableCompat.d(this.a);
    }
    return false;
  }
  
  public void draw(Canvas paramCanvas)
  {
    if (this.a != null) {
      this.a.draw(paramCanvas);
    }
    Object localObject;
    int m;
    int n;
    do
    {
      do
      {
        return;
        copyBounds(this.k);
      } while ((this.k.width() <= 0) || (this.k.height() <= 0));
      if (this.e != null) {
        break;
      }
      localObject = this.d;
      paramCanvas.getMatrix(this.j);
      this.j.getValues(this.i);
      float f1 = Math.abs(this.i[0]);
      float f2 = Math.abs(this.i[4]);
      float f4 = Math.abs(this.i[1]);
      float f3 = Math.abs(this.i[3]);
      if ((f4 != 0.0F) || (f3 != 0.0F))
      {
        f1 = 1.0F;
        f2 = 1.0F;
      }
      m = (int)(this.k.width() * f1);
      n = (int)(this.k.height() * f2);
      m = Math.min(2048, m);
      n = Math.min(2048, n);
    } while ((m <= 0) || (n <= 0));
    int i1 = paramCanvas.save();
    paramCanvas.translate(this.k.left, this.k.top);
    if (a())
    {
      paramCanvas.translate(this.k.width(), 0.0F);
      paramCanvas.scale(-1.0F, 1.0F);
    }
    this.k.offsetTo(0, 0);
    this.c.b(m, n);
    if (!this.g) {
      this.c.a(m, n);
    }
    for (;;)
    {
      this.c.a(paramCanvas, (ColorFilter)localObject, this.k);
      paramCanvas.restoreToCount(i1);
      return;
      localObject = this.e;
      break;
      if (!this.c.b())
      {
        this.c.a(m, n);
        this.c.c();
      }
    }
  }
  
  public int getAlpha()
  {
    if (this.a != null) {
      return DrawableCompat.c(this.a);
    }
    return this.c.b.a();
  }
  
  public int getChangingConfigurations()
  {
    if (this.a != null) {
      return this.a.getChangingConfigurations();
    }
    return super.getChangingConfigurations() | this.c.getChangingConfigurations();
  }
  
  public Drawable.ConstantState getConstantState()
  {
    if (this.a != null) {
      return new VectorDrawableDelegateState(this.a.getConstantState());
    }
    this.c.a = getChangingConfigurations();
    return this.c;
  }
  
  public int getIntrinsicHeight()
  {
    if (this.a != null) {
      return this.a.getIntrinsicHeight();
    }
    return (int)this.c.b.b;
  }
  
  public int getIntrinsicWidth()
  {
    if (this.a != null) {
      return this.a.getIntrinsicWidth();
    }
    return (int)this.c.b.a;
  }
  
  public int getOpacity()
  {
    if (this.a != null) {
      return this.a.getOpacity();
    }
    return -3;
  }
  
  public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet)
    throws XmlPullParserException, IOException
  {
    if (this.a != null)
    {
      this.a.inflate(paramResources, paramXmlPullParser, paramAttributeSet);
      return;
    }
    inflate(paramResources, paramXmlPullParser, paramAttributeSet, null);
  }
  
  public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme)
    throws XmlPullParserException, IOException
  {
    if (this.a != null)
    {
      DrawableCompat.a(this.a, paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
      return;
    }
    VectorDrawableCompatState localVectorDrawableCompatState = this.c;
    localVectorDrawableCompatState.b = new VPathRenderer();
    TypedArray localTypedArray = b(paramResources, paramTheme, paramAttributeSet, AndroidResources.a);
    a(localTypedArray, paramXmlPullParser);
    localTypedArray.recycle();
    localVectorDrawableCompatState.a = getChangingConfigurations();
    localVectorDrawableCompatState.k = true;
    b(paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
    this.d = a(this.d, localVectorDrawableCompatState.c, localVectorDrawableCompatState.d);
  }
  
  public void invalidateSelf()
  {
    if (this.a != null)
    {
      this.a.invalidateSelf();
      return;
    }
    super.invalidateSelf();
  }
  
  public boolean isStateful()
  {
    if (this.a != null) {
      return this.a.isStateful();
    }
    return (super.isStateful()) || ((this.c != null) && (this.c.c != null) && (this.c.c.isStateful()));
  }
  
  public Drawable mutate()
  {
    if (this.a != null) {
      this.a.mutate();
    }
    while ((this.f) || (super.mutate() != this)) {
      return this;
    }
    this.c = new VectorDrawableCompatState(this.c);
    this.f = true;
    return this;
  }
  
  protected void onBoundsChange(Rect paramRect)
  {
    if (this.a != null) {
      this.a.setBounds(paramRect);
    }
  }
  
  protected boolean onStateChange(int[] paramArrayOfInt)
  {
    if (this.a != null) {
      return this.a.setState(paramArrayOfInt);
    }
    paramArrayOfInt = this.c;
    if ((paramArrayOfInt.c != null) && (paramArrayOfInt.d != null))
    {
      this.d = a(this.d, paramArrayOfInt.c, paramArrayOfInt.d);
      invalidateSelf();
      return true;
    }
    return false;
  }
  
  public void scheduleSelf(Runnable paramRunnable, long paramLong)
  {
    if (this.a != null)
    {
      this.a.scheduleSelf(paramRunnable, paramLong);
      return;
    }
    super.scheduleSelf(paramRunnable, paramLong);
  }
  
  public void setAlpha(int paramInt)
  {
    if (this.a != null) {
      this.a.setAlpha(paramInt);
    }
    while (this.c.b.a() == paramInt) {
      return;
    }
    this.c.b.a(paramInt);
    invalidateSelf();
  }
  
  public void setColorFilter(ColorFilter paramColorFilter)
  {
    if (this.a != null)
    {
      this.a.setColorFilter(paramColorFilter);
      return;
    }
    this.e = paramColorFilter;
    invalidateSelf();
  }
  
  public void setTint(int paramInt)
  {
    if (this.a != null)
    {
      DrawableCompat.a(this.a, paramInt);
      return;
    }
    setTintList(ColorStateList.valueOf(paramInt));
  }
  
  public void setTintList(ColorStateList paramColorStateList)
  {
    if (this.a != null) {
      DrawableCompat.a(this.a, paramColorStateList);
    }
    VectorDrawableCompatState localVectorDrawableCompatState;
    do
    {
      return;
      localVectorDrawableCompatState = this.c;
    } while (localVectorDrawableCompatState.c == paramColorStateList);
    localVectorDrawableCompatState.c = paramColorStateList;
    this.d = a(this.d, paramColorStateList, localVectorDrawableCompatState.d);
    invalidateSelf();
  }
  
  public void setTintMode(PorterDuff.Mode paramMode)
  {
    if (this.a != null) {
      DrawableCompat.a(this.a, paramMode);
    }
    VectorDrawableCompatState localVectorDrawableCompatState;
    do
    {
      return;
      localVectorDrawableCompatState = this.c;
    } while (localVectorDrawableCompatState.d == paramMode);
    localVectorDrawableCompatState.d = paramMode;
    this.d = a(this.d, localVectorDrawableCompatState.c, paramMode);
    invalidateSelf();
  }
  
  public boolean setVisible(boolean paramBoolean1, boolean paramBoolean2)
  {
    if (this.a != null) {
      return this.a.setVisible(paramBoolean1, paramBoolean2);
    }
    return super.setVisible(paramBoolean1, paramBoolean2);
  }
  
  public void unscheduleSelf(Runnable paramRunnable)
  {
    if (this.a != null)
    {
      this.a.unscheduleSelf(paramRunnable);
      return;
    }
    super.unscheduleSelf(paramRunnable);
  }
  
  private static class VClipPath
    extends VectorDrawableCompat.VPath
  {
    public VClipPath() {}
    
    public VClipPath(VClipPath paramVClipPath)
    {
      super();
    }
    
    private void a(TypedArray paramTypedArray)
    {
      String str = paramTypedArray.getString(0);
      if (str != null) {
        this.n = str;
      }
      paramTypedArray = paramTypedArray.getString(1);
      if (paramTypedArray != null) {
        this.m = PathParser.a(paramTypedArray);
      }
    }
    
    public void a(Resources paramResources, AttributeSet paramAttributeSet, Resources.Theme paramTheme, XmlPullParser paramXmlPullParser)
    {
      if (!TypedArrayUtils.a(paramXmlPullParser, "pathData")) {
        return;
      }
      paramResources = VectorDrawableCommon.b(paramResources, paramTheme, paramAttributeSet, AndroidResources.d);
      a(paramResources);
      paramResources.recycle();
    }
    
    public boolean a()
    {
      return true;
    }
  }
  
  private static class VFullPath
    extends VectorDrawableCompat.VPath
  {
    int a = 0;
    float b = 0.0F;
    int c = 0;
    float d = 1.0F;
    int e;
    float f = 1.0F;
    float g = 0.0F;
    float h = 1.0F;
    float i = 0.0F;
    Paint.Cap j = Paint.Cap.BUTT;
    Paint.Join k = Paint.Join.MITER;
    float l = 4.0F;
    private int[] p;
    
    public VFullPath() {}
    
    public VFullPath(VFullPath paramVFullPath)
    {
      super();
      this.p = paramVFullPath.p;
      this.a = paramVFullPath.a;
      this.b = paramVFullPath.b;
      this.d = paramVFullPath.d;
      this.c = paramVFullPath.c;
      this.e = paramVFullPath.e;
      this.f = paramVFullPath.f;
      this.g = paramVFullPath.g;
      this.h = paramVFullPath.h;
      this.i = paramVFullPath.i;
      this.j = paramVFullPath.j;
      this.k = paramVFullPath.k;
      this.l = paramVFullPath.l;
    }
    
    private Paint.Cap a(int paramInt, Paint.Cap paramCap)
    {
      switch (paramInt)
      {
      default: 
        return paramCap;
      case 0: 
        return Paint.Cap.BUTT;
      case 1: 
        return Paint.Cap.ROUND;
      }
      return Paint.Cap.SQUARE;
    }
    
    private Paint.Join a(int paramInt, Paint.Join paramJoin)
    {
      switch (paramInt)
      {
      default: 
        return paramJoin;
      case 0: 
        return Paint.Join.MITER;
      case 1: 
        return Paint.Join.ROUND;
      }
      return Paint.Join.BEVEL;
    }
    
    private void a(TypedArray paramTypedArray, XmlPullParser paramXmlPullParser)
    {
      this.p = null;
      if (!TypedArrayUtils.a(paramXmlPullParser, "pathData")) {
        return;
      }
      String str = paramTypedArray.getString(0);
      if (str != null) {
        this.n = str;
      }
      str = paramTypedArray.getString(2);
      if (str != null) {
        this.m = PathParser.a(str);
      }
      this.c = TypedArrayUtils.b(paramTypedArray, paramXmlPullParser, "fillColor", 1, this.c);
      this.f = TypedArrayUtils.a(paramTypedArray, paramXmlPullParser, "fillAlpha", 12, this.f);
      this.j = a(TypedArrayUtils.a(paramTypedArray, paramXmlPullParser, "strokeLineCap", 8, -1), this.j);
      this.k = a(TypedArrayUtils.a(paramTypedArray, paramXmlPullParser, "strokeLineJoin", 9, -1), this.k);
      this.l = TypedArrayUtils.a(paramTypedArray, paramXmlPullParser, "strokeMiterLimit", 10, this.l);
      this.a = TypedArrayUtils.b(paramTypedArray, paramXmlPullParser, "strokeColor", 3, this.a);
      this.d = TypedArrayUtils.a(paramTypedArray, paramXmlPullParser, "strokeAlpha", 11, this.d);
      this.b = TypedArrayUtils.a(paramTypedArray, paramXmlPullParser, "strokeWidth", 4, this.b);
      this.h = TypedArrayUtils.a(paramTypedArray, paramXmlPullParser, "trimPathEnd", 6, this.h);
      this.i = TypedArrayUtils.a(paramTypedArray, paramXmlPullParser, "trimPathOffset", 7, this.i);
      this.g = TypedArrayUtils.a(paramTypedArray, paramXmlPullParser, "trimPathStart", 5, this.g);
    }
    
    public void a(Resources paramResources, AttributeSet paramAttributeSet, Resources.Theme paramTheme, XmlPullParser paramXmlPullParser)
    {
      paramResources = VectorDrawableCommon.b(paramResources, paramTheme, paramAttributeSet, AndroidResources.c);
      a(paramResources, paramXmlPullParser);
      paramResources.recycle();
    }
  }
  
  private static class VGroup
  {
    final ArrayList<Object> a = new ArrayList();
    private final Matrix b = new Matrix();
    private float c = 0.0F;
    private float d = 0.0F;
    private float e = 0.0F;
    private float f = 1.0F;
    private float g = 1.0F;
    private float h = 0.0F;
    private float i = 0.0F;
    private final Matrix j = new Matrix();
    private int k;
    private int[] l;
    private String m = null;
    
    public VGroup() {}
    
    public VGroup(VGroup paramVGroup, ArrayMap<String, Object> paramArrayMap)
    {
      this.c = paramVGroup.c;
      this.d = paramVGroup.d;
      this.e = paramVGroup.e;
      this.f = paramVGroup.f;
      this.g = paramVGroup.g;
      this.h = paramVGroup.h;
      this.i = paramVGroup.i;
      this.l = paramVGroup.l;
      this.m = paramVGroup.m;
      this.k = paramVGroup.k;
      if (this.m != null) {
        paramArrayMap.put(this.m, this);
      }
      this.j.set(paramVGroup.j);
      ArrayList localArrayList = paramVGroup.a;
      int n = 0;
      while (n < localArrayList.size())
      {
        paramVGroup = localArrayList.get(n);
        if ((paramVGroup instanceof VGroup))
        {
          paramVGroup = (VGroup)paramVGroup;
          this.a.add(new VGroup(paramVGroup, paramArrayMap));
          n += 1;
        }
        else
        {
          if ((paramVGroup instanceof VectorDrawableCompat.VFullPath)) {}
          for (paramVGroup = new VectorDrawableCompat.VFullPath((VectorDrawableCompat.VFullPath)paramVGroup);; paramVGroup = new VectorDrawableCompat.VClipPath((VectorDrawableCompat.VClipPath)paramVGroup))
          {
            this.a.add(paramVGroup);
            if (paramVGroup.n == null) {
              break;
            }
            paramArrayMap.put(paramVGroup.n, paramVGroup);
            break;
            if (!(paramVGroup instanceof VectorDrawableCompat.VClipPath)) {
              break label315;
            }
          }
          label315:
          throw new IllegalStateException("Unknown object in the tree!");
        }
      }
    }
    
    private void a(TypedArray paramTypedArray, XmlPullParser paramXmlPullParser)
    {
      this.l = null;
      this.c = TypedArrayUtils.a(paramTypedArray, paramXmlPullParser, "rotation", 5, this.c);
      this.d = paramTypedArray.getFloat(1, this.d);
      this.e = paramTypedArray.getFloat(2, this.e);
      this.f = TypedArrayUtils.a(paramTypedArray, paramXmlPullParser, "scaleX", 3, this.f);
      this.g = TypedArrayUtils.a(paramTypedArray, paramXmlPullParser, "scaleY", 4, this.g);
      this.h = TypedArrayUtils.a(paramTypedArray, paramXmlPullParser, "translateX", 6, this.h);
      this.i = TypedArrayUtils.a(paramTypedArray, paramXmlPullParser, "translateY", 7, this.i);
      paramTypedArray = paramTypedArray.getString(0);
      if (paramTypedArray != null) {
        this.m = paramTypedArray;
      }
      b();
    }
    
    private void b()
    {
      this.j.reset();
      this.j.postTranslate(-this.d, -this.e);
      this.j.postScale(this.f, this.g);
      this.j.postRotate(this.c, 0.0F, 0.0F);
      this.j.postTranslate(this.h + this.d, this.i + this.e);
    }
    
    public String a()
    {
      return this.m;
    }
    
    public void a(Resources paramResources, AttributeSet paramAttributeSet, Resources.Theme paramTheme, XmlPullParser paramXmlPullParser)
    {
      paramResources = VectorDrawableCommon.b(paramResources, paramTheme, paramAttributeSet, AndroidResources.b);
      a(paramResources, paramXmlPullParser);
      paramResources.recycle();
    }
  }
  
  private static class VPath
  {
    protected PathParser.PathDataNode[] m = null;
    String n;
    int o;
    
    public VPath() {}
    
    public VPath(VPath paramVPath)
    {
      this.n = paramVPath.n;
      this.o = paramVPath.o;
      this.m = PathParser.a(paramVPath.m);
    }
    
    public void a(Path paramPath)
    {
      paramPath.reset();
      if (this.m != null) {
        PathParser.PathDataNode.a(this.m, paramPath);
      }
    }
    
    public boolean a()
    {
      return false;
    }
    
    public String b()
    {
      return this.n;
    }
  }
  
  private static class VPathRenderer
  {
    private static final Matrix j = new Matrix();
    float a = 0.0F;
    float b = 0.0F;
    float c = 0.0F;
    float d = 0.0F;
    int e = 255;
    String f = null;
    final ArrayMap<String, Object> g = new ArrayMap();
    private final Path h;
    private final Path i;
    private final Matrix k = new Matrix();
    private Paint l;
    private Paint m;
    private PathMeasure n;
    private int o;
    private final VectorDrawableCompat.VGroup p;
    
    public VPathRenderer()
    {
      this.p = new VectorDrawableCompat.VGroup();
      this.h = new Path();
      this.i = new Path();
    }
    
    public VPathRenderer(VPathRenderer paramVPathRenderer)
    {
      this.p = new VectorDrawableCompat.VGroup(paramVPathRenderer.p, this.g);
      this.h = new Path(paramVPathRenderer.h);
      this.i = new Path(paramVPathRenderer.i);
      this.a = paramVPathRenderer.a;
      this.b = paramVPathRenderer.b;
      this.c = paramVPathRenderer.c;
      this.d = paramVPathRenderer.d;
      this.o = paramVPathRenderer.o;
      this.e = paramVPathRenderer.e;
      this.f = paramVPathRenderer.f;
      if (paramVPathRenderer.f != null) {
        this.g.put(paramVPathRenderer.f, this);
      }
    }
    
    private static float a(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
      return paramFloat1 * paramFloat4 - paramFloat2 * paramFloat3;
    }
    
    private float a(Matrix paramMatrix)
    {
      float[] arrayOfFloat = new float[4];
      float[] tmp7_5 = arrayOfFloat;
      tmp7_5[0] = 0.0F;
      float[] tmp11_7 = tmp7_5;
      tmp11_7[1] = 1.0F;
      float[] tmp15_11 = tmp11_7;
      tmp15_11[2] = 1.0F;
      float[] tmp19_15 = tmp15_11;
      tmp19_15[3] = 0.0F;
      tmp19_15;
      paramMatrix.mapVectors(arrayOfFloat);
      float f1 = (float)Math.hypot(arrayOfFloat[0], arrayOfFloat[1]);
      float f3 = (float)Math.hypot(arrayOfFloat[2], arrayOfFloat[3]);
      float f2 = a(arrayOfFloat[0], arrayOfFloat[1], arrayOfFloat[2], arrayOfFloat[3]);
      f3 = Math.max(f1, f3);
      f1 = 0.0F;
      if (f3 > 0.0F) {
        f1 = Math.abs(f2) / f3;
      }
      return f1;
    }
    
    private void a(VectorDrawableCompat.VGroup paramVGroup, Matrix paramMatrix, Canvas paramCanvas, int paramInt1, int paramInt2, ColorFilter paramColorFilter)
    {
      VectorDrawableCompat.VGroup.b(paramVGroup).set(paramMatrix);
      VectorDrawableCompat.VGroup.b(paramVGroup).preConcat(VectorDrawableCompat.VGroup.c(paramVGroup));
      int i1 = 0;
      if (i1 < paramVGroup.a.size())
      {
        paramMatrix = paramVGroup.a.get(i1);
        if ((paramMatrix instanceof VectorDrawableCompat.VGroup)) {
          a((VectorDrawableCompat.VGroup)paramMatrix, VectorDrawableCompat.VGroup.b(paramVGroup), paramCanvas, paramInt1, paramInt2, paramColorFilter);
        }
        for (;;)
        {
          i1 += 1;
          break;
          if ((paramMatrix instanceof VectorDrawableCompat.VPath)) {
            a(paramVGroup, (VectorDrawableCompat.VPath)paramMatrix, paramCanvas, paramInt1, paramInt2, paramColorFilter);
          }
        }
      }
    }
    
    private void a(VectorDrawableCompat.VGroup paramVGroup, VectorDrawableCompat.VPath paramVPath, Canvas paramCanvas, int paramInt1, int paramInt2, ColorFilter paramColorFilter)
    {
      float f2 = paramInt1 / this.c;
      float f3 = paramInt2 / this.d;
      float f1 = Math.min(f2, f3);
      paramVGroup = VectorDrawableCompat.VGroup.b(paramVGroup);
      this.k.set(paramVGroup);
      this.k.postScale(f2, f3);
      f2 = a(paramVGroup);
      if (f2 == 0.0F) {
        return;
      }
      paramVPath.a(this.h);
      Path localPath = this.h;
      this.i.reset();
      if (paramVPath.a())
      {
        this.i.addPath(localPath, this.k);
        paramCanvas.clipPath(this.i, Region.Op.REPLACE);
        return;
      }
      paramVGroup = (VectorDrawableCompat.VFullPath)paramVPath;
      float f6;
      float f4;
      if ((paramVGroup.g != 0.0F) || (paramVGroup.h != 1.0F))
      {
        f6 = paramVGroup.g;
        float f7 = paramVGroup.i;
        f4 = paramVGroup.h;
        float f5 = paramVGroup.i;
        if (this.n == null) {
          this.n = new PathMeasure();
        }
        this.n.setPath(this.h, false);
        f3 = this.n.getLength();
        f6 = (f6 + f7) % 1.0F * f3;
        f4 = (f4 + f5) % 1.0F * f3;
        localPath.reset();
        if (f6 <= f4) {
          break label506;
        }
        this.n.getSegment(f6, f3, localPath, true);
        this.n.getSegment(0.0F, f4, localPath, true);
      }
      for (;;)
      {
        localPath.rLineTo(0.0F, 0.0F);
        this.i.addPath(localPath, this.k);
        if (paramVGroup.c != 0)
        {
          if (this.m == null)
          {
            this.m = new Paint();
            this.m.setStyle(Paint.Style.FILL);
            this.m.setAntiAlias(true);
          }
          paramVPath = this.m;
          paramVPath.setColor(VectorDrawableCompat.a(paramVGroup.c, paramVGroup.f));
          paramVPath.setColorFilter(paramColorFilter);
          paramCanvas.drawPath(this.i, paramVPath);
        }
        if (paramVGroup.a == 0) {
          break;
        }
        if (this.l == null)
        {
          this.l = new Paint();
          this.l.setStyle(Paint.Style.STROKE);
          this.l.setAntiAlias(true);
        }
        paramVPath = this.l;
        if (paramVGroup.k != null) {
          paramVPath.setStrokeJoin(paramVGroup.k);
        }
        if (paramVGroup.j != null) {
          paramVPath.setStrokeCap(paramVGroup.j);
        }
        paramVPath.setStrokeMiter(paramVGroup.l);
        paramVPath.setColor(VectorDrawableCompat.a(paramVGroup.a, paramVGroup.d));
        paramVPath.setColorFilter(paramColorFilter);
        paramVPath.setStrokeWidth(paramVGroup.b * (f1 * f2));
        paramCanvas.drawPath(this.i, paramVPath);
        return;
        label506:
        this.n.getSegment(f6, f4, localPath, true);
      }
    }
    
    public int a()
    {
      return this.e;
    }
    
    public void a(float paramFloat)
    {
      a((int)(255.0F * paramFloat));
    }
    
    public void a(int paramInt)
    {
      this.e = paramInt;
    }
    
    public void a(Canvas paramCanvas, int paramInt1, int paramInt2, ColorFilter paramColorFilter)
    {
      a(this.p, j, paramCanvas, paramInt1, paramInt2, paramColorFilter);
    }
    
    public float b()
    {
      return a() / 255.0F;
    }
  }
  
  private static class VectorDrawableCompatState
    extends Drawable.ConstantState
  {
    int a;
    VectorDrawableCompat.VPathRenderer b;
    ColorStateList c = null;
    PorterDuff.Mode d = VectorDrawableCompat.b;
    boolean e;
    Bitmap f;
    ColorStateList g;
    PorterDuff.Mode h;
    int i;
    boolean j;
    boolean k;
    Paint l;
    
    public VectorDrawableCompatState()
    {
      this.b = new VectorDrawableCompat.VPathRenderer();
    }
    
    public VectorDrawableCompatState(VectorDrawableCompatState paramVectorDrawableCompatState)
    {
      if (paramVectorDrawableCompatState != null)
      {
        this.a = paramVectorDrawableCompatState.a;
        this.b = new VectorDrawableCompat.VPathRenderer(paramVectorDrawableCompatState.b);
        if (VectorDrawableCompat.VPathRenderer.b(paramVectorDrawableCompatState.b) != null) {
          VectorDrawableCompat.VPathRenderer.a(this.b, new Paint(VectorDrawableCompat.VPathRenderer.b(paramVectorDrawableCompatState.b)));
        }
        if (VectorDrawableCompat.VPathRenderer.c(paramVectorDrawableCompatState.b) != null) {
          VectorDrawableCompat.VPathRenderer.b(this.b, new Paint(VectorDrawableCompat.VPathRenderer.c(paramVectorDrawableCompatState.b)));
        }
        this.c = paramVectorDrawableCompatState.c;
        this.d = paramVectorDrawableCompatState.d;
        this.e = paramVectorDrawableCompatState.e;
      }
    }
    
    public Paint a(ColorFilter paramColorFilter)
    {
      if ((!a()) && (paramColorFilter == null)) {
        return null;
      }
      if (this.l == null)
      {
        this.l = new Paint();
        this.l.setFilterBitmap(true);
      }
      this.l.setAlpha(this.b.a());
      this.l.setColorFilter(paramColorFilter);
      return this.l;
    }
    
    public void a(int paramInt1, int paramInt2)
    {
      this.f.eraseColor(0);
      Canvas localCanvas = new Canvas(this.f);
      this.b.a(localCanvas, paramInt1, paramInt2, null);
    }
    
    public void a(Canvas paramCanvas, ColorFilter paramColorFilter, Rect paramRect)
    {
      paramColorFilter = a(paramColorFilter);
      paramCanvas.drawBitmap(this.f, null, paramRect, paramColorFilter);
    }
    
    public boolean a()
    {
      return this.b.a() < 255;
    }
    
    public void b(int paramInt1, int paramInt2)
    {
      if ((this.f == null) || (!c(paramInt1, paramInt2)))
      {
        this.f = Bitmap.createBitmap(paramInt1, paramInt2, Bitmap.Config.ARGB_8888);
        this.k = true;
      }
    }
    
    public boolean b()
    {
      return (!this.k) && (this.g == this.c) && (this.h == this.d) && (this.j == this.e) && (this.i == this.b.a());
    }
    
    public void c()
    {
      this.g = this.c;
      this.h = this.d;
      this.i = this.b.a();
      this.j = this.e;
      this.k = false;
    }
    
    public boolean c(int paramInt1, int paramInt2)
    {
      return (paramInt1 == this.f.getWidth()) && (paramInt2 == this.f.getHeight());
    }
    
    public int getChangingConfigurations()
    {
      return this.a;
    }
    
    public Drawable newDrawable()
    {
      return new VectorDrawableCompat(this, null);
    }
    
    public Drawable newDrawable(Resources paramResources)
    {
      return new VectorDrawableCompat(this, null);
    }
  }
  
  private static class VectorDrawableDelegateState
    extends Drawable.ConstantState
  {
    private final Drawable.ConstantState a;
    
    public VectorDrawableDelegateState(Drawable.ConstantState paramConstantState)
    {
      this.a = paramConstantState;
    }
    
    public boolean canApplyTheme()
    {
      return this.a.canApplyTheme();
    }
    
    public int getChangingConfigurations()
    {
      return this.a.getChangingConfigurations();
    }
    
    public Drawable newDrawable()
    {
      VectorDrawableCompat localVectorDrawableCompat = new VectorDrawableCompat(null);
      localVectorDrawableCompat.a = ((VectorDrawable)this.a.newDrawable());
      return localVectorDrawableCompat;
    }
    
    public Drawable newDrawable(Resources paramResources)
    {
      VectorDrawableCompat localVectorDrawableCompat = new VectorDrawableCompat(null);
      localVectorDrawableCompat.a = ((VectorDrawable)this.a.newDrawable(paramResources));
      return localVectorDrawableCompat;
    }
    
    public Drawable newDrawable(Resources paramResources, Resources.Theme paramTheme)
    {
      VectorDrawableCompat localVectorDrawableCompat = new VectorDrawableCompat(null);
      localVectorDrawableCompat.a = ((VectorDrawable)this.a.newDrawable(paramResources, paramTheme));
      return localVectorDrawableCompat;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/graphics/drawable/VectorDrawableCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */