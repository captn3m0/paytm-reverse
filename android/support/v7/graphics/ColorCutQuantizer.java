package android.support.v7.graphics;

import android.graphics.Color;
import android.support.v4.graphics.ColorUtils;
import android.util.TimingLogger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

final class ColorCutQuantizer
{
  private static final Comparator<Vbox> g = new Comparator()
  {
    public int a(ColorCutQuantizer.Vbox paramAnonymousVbox1, ColorCutQuantizer.Vbox paramAnonymousVbox2)
    {
      return paramAnonymousVbox2.a() - paramAnonymousVbox1.a();
    }
  };
  final int[] a;
  final int[] b;
  final List<Palette.Swatch> c;
  final TimingLogger d = null;
  final Palette.Filter[] e;
  private final float[] f = new float[3];
  
  ColorCutQuantizer(int[] paramArrayOfInt, int paramInt, Palette.Filter[] paramArrayOfFilter)
  {
    this.e = paramArrayOfFilter;
    paramArrayOfFilter = new int[32768];
    this.b = paramArrayOfFilter;
    int i = 0;
    while (i < paramArrayOfInt.length)
    {
      j = f(paramArrayOfInt[i]);
      paramArrayOfInt[i] = j;
      paramArrayOfFilter[j] += 1;
      i += 1;
    }
    i = 0;
    int j = 0;
    while (j < paramArrayOfFilter.length)
    {
      if ((paramArrayOfFilter[j] > 0) && (e(j))) {
        paramArrayOfFilter[j] = 0;
      }
      k = i;
      if (paramArrayOfFilter[j] > 0) {
        k = i + 1;
      }
      j += 1;
      i = k;
    }
    paramArrayOfInt = new int[i];
    this.a = paramArrayOfInt;
    int k = 0;
    j = 0;
    while (j < paramArrayOfFilter.length)
    {
      int m = k;
      if (paramArrayOfFilter[j] > 0)
      {
        paramArrayOfInt[k] = j;
        m = k + 1;
      }
      j += 1;
      k = m;
    }
    if (i <= paramInt)
    {
      this.c = new ArrayList();
      i = paramArrayOfInt.length;
      paramInt = 0;
      while (paramInt < i)
      {
        j = paramArrayOfInt[paramInt];
        this.c.add(new Palette.Swatch(g(j), paramArrayOfFilter[j]));
        paramInt += 1;
      }
    }
    this.c = d(paramInt);
  }
  
  private List<Palette.Swatch> a(Collection<Vbox> paramCollection)
  {
    ArrayList localArrayList = new ArrayList(paramCollection.size());
    paramCollection = paramCollection.iterator();
    while (paramCollection.hasNext())
    {
      Palette.Swatch localSwatch = ((Vbox)paramCollection.next()).h();
      if (!a(localSwatch)) {
        localArrayList.add(localSwatch);
      }
    }
    return localArrayList;
  }
  
  private void a(PriorityQueue<Vbox> paramPriorityQueue, int paramInt)
  {
    while (paramPriorityQueue.size() < paramInt)
    {
      Vbox localVbox = (Vbox)paramPriorityQueue.poll();
      if ((localVbox == null) || (!localVbox.b())) {
        break;
      }
      paramPriorityQueue.offer(localVbox.e());
      paramPriorityQueue.offer(localVbox);
    }
  }
  
  private boolean a(int paramInt, float[] paramArrayOfFloat)
  {
    if ((this.e != null) && (this.e.length > 0))
    {
      int i = 0;
      int j = this.e.length;
      while (i < j)
      {
        if (!this.e[i].a(paramInt, paramArrayOfFloat)) {
          return true;
        }
        i += 1;
      }
    }
    return false;
  }
  
  private boolean a(Palette.Swatch paramSwatch)
  {
    return a(paramSwatch.a(), paramSwatch.b());
  }
  
  private static int b(int paramInt1, int paramInt2, int paramInt3)
  {
    return Color.rgb(c(paramInt1, 5, 8), c(paramInt2, 5, 8), c(paramInt3, 5, 8));
  }
  
  private static void b(int[] paramArrayOfInt, int paramInt1, int paramInt2, int paramInt3)
  {
    switch (paramInt1)
    {
    }
    for (;;)
    {
      return;
      paramInt1 = paramInt2;
      while (paramInt1 <= paramInt3)
      {
        paramInt2 = paramArrayOfInt[paramInt1];
        paramArrayOfInt[paramInt1] = (i(paramInt2) << 10 | h(paramInt2) << 5 | j(paramInt2));
        paramInt1 += 1;
      }
      continue;
      paramInt1 = paramInt2;
      while (paramInt1 <= paramInt3)
      {
        paramInt2 = paramArrayOfInt[paramInt1];
        paramArrayOfInt[paramInt1] = (j(paramInt2) << 10 | i(paramInt2) << 5 | h(paramInt2));
        paramInt1 += 1;
      }
    }
  }
  
  private static int c(int paramInt1, int paramInt2, int paramInt3)
  {
    if (paramInt3 > paramInt2) {
      paramInt1 = ((1 << paramInt3) - 1) * paramInt1 / ((1 << paramInt2) - 1);
    }
    for (;;)
    {
      return (1 << paramInt3) - 1 & paramInt1;
      paramInt1 >>= paramInt2 - paramInt3;
    }
  }
  
  private List<Palette.Swatch> d(int paramInt)
  {
    PriorityQueue localPriorityQueue = new PriorityQueue(paramInt, g);
    localPriorityQueue.offer(new Vbox(0, this.a.length - 1));
    a(localPriorityQueue, paramInt);
    return a(localPriorityQueue);
  }
  
  private boolean e(int paramInt)
  {
    paramInt = g(paramInt);
    ColorUtils.a(paramInt, this.f);
    return a(paramInt, this.f);
  }
  
  private static int f(int paramInt)
  {
    return c(Color.red(paramInt), 8, 5) << 10 | c(Color.green(paramInt), 8, 5) << 5 | c(Color.blue(paramInt), 8, 5);
  }
  
  private static int g(int paramInt)
  {
    return b(h(paramInt), i(paramInt), j(paramInt));
  }
  
  private static int h(int paramInt)
  {
    return paramInt >> 10 & 0x1F;
  }
  
  private static int i(int paramInt)
  {
    return paramInt >> 5 & 0x1F;
  }
  
  private static int j(int paramInt)
  {
    return paramInt & 0x1F;
  }
  
  List<Palette.Swatch> a()
  {
    return this.c;
  }
  
  private class Vbox
  {
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j;
    
    Vbox(int paramInt1, int paramInt2)
    {
      this.b = paramInt1;
      this.c = paramInt2;
      d();
    }
    
    final int a()
    {
      return (this.f - this.e + 1) * (this.h - this.g + 1) * (this.j - this.i + 1);
    }
    
    final boolean b()
    {
      return c() > 1;
    }
    
    final int c()
    {
      return this.c + 1 - this.b;
    }
    
    final void d()
    {
      int[] arrayOfInt1 = ColorCutQuantizer.this.a;
      int[] arrayOfInt2 = ColorCutQuantizer.this.b;
      int k = Integer.MAX_VALUE;
      int n = Integer.MAX_VALUE;
      int i4 = Integer.MAX_VALUE;
      int i1 = Integer.MIN_VALUE;
      int i3 = Integer.MIN_VALUE;
      int i7 = Integer.MIN_VALUE;
      int i6 = 0;
      int m = this.b;
      while (m <= this.c)
      {
        int i2 = arrayOfInt1[m];
        int i10 = i6 + arrayOfInt2[i2];
        int i9 = ColorCutQuantizer.a(i2);
        int i8 = ColorCutQuantizer.b(i2);
        i6 = ColorCutQuantizer.c(i2);
        i2 = i7;
        if (i9 > i7) {
          i2 = i9;
        }
        int i5 = i4;
        if (i9 < i4) {
          i5 = i9;
        }
        i4 = i3;
        if (i8 > i3) {
          i4 = i8;
        }
        i9 = n;
        if (i8 < n) {
          i9 = i8;
        }
        i3 = i1;
        if (i6 > i1) {
          i3 = i6;
        }
        n = k;
        if (i6 < k) {
          n = i6;
        }
        m += 1;
        i6 = i10;
        i1 = i3;
        i3 = i4;
        i7 = i2;
        k = n;
        n = i9;
        i4 = i5;
      }
      this.e = i4;
      this.f = i7;
      this.g = n;
      this.h = i3;
      this.i = k;
      this.j = i1;
      this.d = i6;
    }
    
    final Vbox e()
    {
      if (!b()) {
        throw new IllegalStateException("Can not split a box with only 1 color");
      }
      int k = g();
      Vbox localVbox = new Vbox(ColorCutQuantizer.this, k + 1, this.c);
      this.c = k;
      d();
      return localVbox;
    }
    
    final int f()
    {
      int k = this.f - this.e;
      int m = this.h - this.g;
      int n = this.j - this.i;
      if ((k >= m) && (k >= n)) {
        return -3;
      }
      if ((m >= k) && (m >= n)) {
        return -2;
      }
      return -1;
    }
    
    final int g()
    {
      int k = f();
      int[] arrayOfInt1 = ColorCutQuantizer.this.a;
      int[] arrayOfInt2 = ColorCutQuantizer.this.b;
      ColorCutQuantizer.a(arrayOfInt1, k, this.b, this.c);
      Arrays.sort(arrayOfInt1, this.b, this.c + 1);
      ColorCutQuantizer.a(arrayOfInt1, k, this.b, this.c);
      int n = this.d / 2;
      k = this.b;
      int m = 0;
      while (k <= this.c)
      {
        m += arrayOfInt2[arrayOfInt1[k]];
        if (m >= n) {
          return k;
        }
        k += 1;
      }
      return this.b;
    }
    
    final Palette.Swatch h()
    {
      int[] arrayOfInt1 = ColorCutQuantizer.this.a;
      int[] arrayOfInt2 = ColorCutQuantizer.this.b;
      int i1 = 0;
      int n = 0;
      int m = 0;
      int i2 = 0;
      int k = this.b;
      while (k <= this.c)
      {
        int i3 = arrayOfInt1[k];
        int i4 = arrayOfInt2[i3];
        i2 += i4;
        i1 += ColorCutQuantizer.a(i3) * i4;
        n += ColorCutQuantizer.b(i3) * i4;
        m += ColorCutQuantizer.c(i3) * i4;
        k += 1;
      }
      return new Palette.Swatch(ColorCutQuantizer.a(Math.round(i1 / i2), Math.round(n / i2), Math.round(m / i2)), i2);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/graphics/ColorCutQuantizer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */