package android.support.v7.graphics;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v4.graphics.ColorUtils;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import android.util.SparseBooleanArray;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public final class Palette
{
  private static final Filter f = new Filter()
  {
    private boolean a(float[] paramAnonymousArrayOfFloat)
    {
      return paramAnonymousArrayOfFloat[2] <= 0.05F;
    }
    
    private boolean b(float[] paramAnonymousArrayOfFloat)
    {
      return paramAnonymousArrayOfFloat[2] >= 0.95F;
    }
    
    private boolean c(float[] paramAnonymousArrayOfFloat)
    {
      return (paramAnonymousArrayOfFloat[0] >= 10.0F) && (paramAnonymousArrayOfFloat[0] <= 37.0F) && (paramAnonymousArrayOfFloat[1] <= 0.82F);
    }
    
    public boolean a(int paramAnonymousInt, float[] paramAnonymousArrayOfFloat)
    {
      return (!b(paramAnonymousArrayOfFloat)) && (!a(paramAnonymousArrayOfFloat)) && (!c(paramAnonymousArrayOfFloat));
    }
  };
  private final List<Swatch> a;
  private final List<Target> b;
  private final Map<Target, Swatch> c;
  private final SparseBooleanArray d;
  private final int e;
  
  private Palette(List<Swatch> paramList, List<Target> paramList1)
  {
    this.a = paramList;
    this.b = paramList1;
    this.d = new SparseBooleanArray();
    this.c = new ArrayMap();
    this.e = b();
  }
  
  private Swatch a(Target paramTarget)
  {
    Swatch localSwatch = b(paramTarget);
    if ((localSwatch != null) && (paramTarget.j())) {
      this.d.append(localSwatch.a(), true);
    }
    return localSwatch;
  }
  
  private void a()
  {
    int i = 0;
    int j = this.b.size();
    while (i < j)
    {
      Target localTarget = (Target)this.b.get(i);
      localTarget.k();
      this.c.put(localTarget, a(localTarget));
      i += 1;
    }
    this.d.clear();
  }
  
  private boolean a(Swatch paramSwatch, Target paramTarget)
  {
    float[] arrayOfFloat = paramSwatch.b();
    return (arrayOfFloat[1] >= paramTarget.a()) && (arrayOfFloat[1] <= paramTarget.c()) && (arrayOfFloat[2] >= paramTarget.d()) && (arrayOfFloat[2] <= paramTarget.f()) && (!this.d.get(paramSwatch.a()));
  }
  
  private float b(Swatch paramSwatch, Target paramTarget)
  {
    float[] arrayOfFloat = paramSwatch.b();
    float f1 = 0.0F;
    float f2 = 0.0F;
    float f3 = 0.0F;
    if (paramTarget.g() > 0.0F) {
      f1 = paramTarget.g() * (1.0F - Math.abs(arrayOfFloat[1] - paramTarget.b()));
    }
    if (paramTarget.h() > 0.0F) {
      f2 = paramTarget.h() * (1.0F - Math.abs(arrayOfFloat[2] - paramTarget.e()));
    }
    if (paramTarget.i() > 0.0F) {
      f3 = paramTarget.i() * (paramSwatch.c() / this.e);
    }
    return f1 + f2 + f3;
  }
  
  private int b()
  {
    int j = 0;
    int i = 0;
    int k = this.a.size();
    while (i < k)
    {
      j = Math.max(((Swatch)this.a.get(i)).c(), j);
      i += 1;
    }
    return j;
  }
  
  private Swatch b(Target paramTarget)
  {
    float f1 = 0.0F;
    Object localObject1 = null;
    int i = 0;
    int j = this.a.size();
    while (i < j)
    {
      Swatch localSwatch = (Swatch)this.a.get(i);
      float f2 = f1;
      Object localObject2 = localObject1;
      if (a(localSwatch, paramTarget))
      {
        float f3 = b(localSwatch, paramTarget);
        if (localObject1 != null)
        {
          f2 = f1;
          localObject2 = localObject1;
          if (f3 <= f1) {}
        }
        else
        {
          localObject2 = localSwatch;
          f2 = f3;
        }
      }
      i += 1;
      f1 = f2;
      localObject1 = localObject2;
    }
    return (Swatch)localObject1;
  }
  
  public static final class Builder
  {
    private final List<Palette.Swatch> a;
    private final Bitmap b;
    private final List<Target> c;
    private int d;
    private int e;
    private int f;
    private final List<Palette.Filter> g;
    private Rect h;
    
    private int[] a(Bitmap paramBitmap)
    {
      int j = paramBitmap.getWidth();
      int i = paramBitmap.getHeight();
      int[] arrayOfInt = new int[j * i];
      paramBitmap.getPixels(arrayOfInt, 0, j, 0, 0, j, i);
      if (this.h == null) {
        return arrayOfInt;
      }
      int k = this.h.width();
      int m = this.h.height();
      paramBitmap = new int[k * m];
      i = 0;
      while (i < m)
      {
        System.arraycopy(arrayOfInt, (this.h.top + i) * j + this.h.left, paramBitmap, i * k, k);
        i += 1;
      }
      return paramBitmap;
    }
    
    private Bitmap b(Bitmap paramBitmap)
    {
      double d2 = -1.0D;
      int i;
      double d1;
      if (this.e > 0)
      {
        i = paramBitmap.getWidth() * paramBitmap.getHeight();
        d1 = d2;
        if (i > this.e) {
          d1 = this.e / i;
        }
      }
      while (d1 <= 0.0D)
      {
        return paramBitmap;
        d1 = d2;
        if (this.f > 0)
        {
          i = Math.max(paramBitmap.getWidth(), paramBitmap.getHeight());
          d1 = d2;
          if (i > this.f) {
            d1 = this.f / i;
          }
        }
      }
      return Bitmap.createScaledBitmap(paramBitmap, (int)Math.ceil(paramBitmap.getWidth() * d1), (int)Math.ceil(paramBitmap.getHeight() * d1), false);
    }
    
    @NonNull
    public Palette a()
    {
      Object localObject;
      if (this.b != null)
      {
        Bitmap localBitmap = b(this.b);
        if (0 != 0) {
          throw new NullPointerException();
        }
        localObject = this.h;
        if ((localBitmap != this.b) && (localObject != null))
        {
          double d1 = localBitmap.getWidth() / this.b.getWidth();
          ((Rect)localObject).left = ((int)Math.floor(((Rect)localObject).left * d1));
          ((Rect)localObject).top = ((int)Math.floor(((Rect)localObject).top * d1));
          ((Rect)localObject).right = Math.min((int)Math.ceil(((Rect)localObject).right * d1), localBitmap.getWidth());
          ((Rect)localObject).bottom = Math.min((int)Math.ceil(((Rect)localObject).bottom * d1), localBitmap.getHeight());
        }
        int[] arrayOfInt = a(localBitmap);
        int i = this.d;
        if (this.g.isEmpty())
        {
          localObject = null;
          localObject = new ColorCutQuantizer(arrayOfInt, i, (Palette.Filter[])localObject);
          if (localBitmap != this.b) {
            localBitmap.recycle();
          }
          localObject = ((ColorCutQuantizer)localObject).a();
          if (0 != 0) {
            throw new NullPointerException();
          }
        }
      }
      for (;;)
      {
        localObject = new Palette((List)localObject, this.c, null);
        Palette.a((Palette)localObject);
        if (0 != 0) {
          throw new NullPointerException();
        }
        return (Palette)localObject;
        localObject = (Palette.Filter[])this.g.toArray(new Palette.Filter[this.g.size()]);
        break;
        localObject = this.a;
      }
    }
  }
  
  public static abstract interface Filter
  {
    public abstract boolean a(int paramInt, float[] paramArrayOfFloat);
  }
  
  public static abstract interface PaletteAsyncListener
  {
    public abstract void a(Palette paramPalette);
  }
  
  public static final class Swatch
  {
    private final int a;
    private final int b;
    private final int c;
    private final int d;
    private final int e;
    private boolean f;
    private int g;
    private int h;
    private float[] i;
    
    public Swatch(@ColorInt int paramInt1, int paramInt2)
    {
      this.a = Color.red(paramInt1);
      this.b = Color.green(paramInt1);
      this.c = Color.blue(paramInt1);
      this.d = paramInt1;
      this.e = paramInt2;
    }
    
    private void f()
    {
      int k;
      if (!this.f)
      {
        j = ColorUtils.a(-1, this.d, 4.5F);
        k = ColorUtils.a(-1, this.d, 3.0F);
        if ((j != -1) && (k != -1))
        {
          this.h = ColorUtils.c(-1, j);
          this.g = ColorUtils.c(-1, k);
          this.f = true;
        }
      }
      else
      {
        return;
      }
      int n = ColorUtils.a(-16777216, this.d, 4.5F);
      int m = ColorUtils.a(-16777216, this.d, 3.0F);
      if ((n != -1) && (n != -1))
      {
        this.h = ColorUtils.c(-16777216, n);
        this.g = ColorUtils.c(-16777216, m);
        this.f = true;
        return;
      }
      if (j != -1)
      {
        j = ColorUtils.c(-1, j);
        this.h = j;
        if (k == -1) {
          break label176;
        }
      }
      label176:
      for (int j = ColorUtils.c(-1, k);; j = ColorUtils.c(-16777216, m))
      {
        this.g = j;
        this.f = true;
        return;
        j = ColorUtils.c(-16777216, n);
        break;
      }
    }
    
    @ColorInt
    public int a()
    {
      return this.d;
    }
    
    public float[] b()
    {
      if (this.i == null) {
        this.i = new float[3];
      }
      ColorUtils.a(this.a, this.b, this.c, this.i);
      return this.i;
    }
    
    public int c()
    {
      return this.e;
    }
    
    @ColorInt
    public int d()
    {
      f();
      return this.g;
    }
    
    @ColorInt
    public int e()
    {
      f();
      return this.h;
    }
    
    public boolean equals(Object paramObject)
    {
      if (this == paramObject) {}
      do
      {
        return true;
        if ((paramObject == null) || (getClass() != paramObject.getClass())) {
          return false;
        }
        paramObject = (Swatch)paramObject;
      } while ((this.e == ((Swatch)paramObject).e) && (this.d == ((Swatch)paramObject).d));
      return false;
    }
    
    public int hashCode()
    {
      return this.d * 31 + this.e;
    }
    
    public String toString()
    {
      return getClass().getSimpleName() + " [RGB: #" + Integer.toHexString(a()) + ']' + " [HSL: " + Arrays.toString(b()) + ']' + " [Population: " + this.e + ']' + " [Title Text: #" + Integer.toHexString(d()) + ']' + " [Body Text: #" + Integer.toHexString(e()) + ']';
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/graphics/Palette.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */