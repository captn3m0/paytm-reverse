package android.support.v7.graphics;

import android.support.annotation.FloatRange;

public final class Target
{
  public static final Target a = new Target();
  public static final Target b;
  public static final Target c;
  public static final Target d;
  public static final Target e;
  public static final Target f;
  private final float[] g = new float[3];
  private final float[] h = new float[3];
  private final float[] i = new float[3];
  private boolean j = true;
  
  static
  {
    c(a);
    d(a);
    b = new Target();
    b(b);
    d(b);
    c = new Target();
    a(c);
    d(c);
    d = new Target();
    c(d);
    e(d);
    e = new Target();
    b(e);
    e(e);
    f = new Target();
    a(f);
    e(f);
  }
  
  private Target()
  {
    a(this.g);
    a(this.h);
    l();
  }
  
  private static void a(Target paramTarget)
  {
    paramTarget.h[1] = 0.26F;
    paramTarget.h[2] = 0.45F;
  }
  
  private static void a(float[] paramArrayOfFloat)
  {
    paramArrayOfFloat[0] = 0.0F;
    paramArrayOfFloat[1] = 0.5F;
    paramArrayOfFloat[2] = 1.0F;
  }
  
  private static void b(Target paramTarget)
  {
    paramTarget.h[0] = 0.3F;
    paramTarget.h[1] = 0.5F;
    paramTarget.h[2] = 0.7F;
  }
  
  private static void c(Target paramTarget)
  {
    paramTarget.h[0] = 0.55F;
    paramTarget.h[1] = 0.74F;
  }
  
  private static void d(Target paramTarget)
  {
    paramTarget.g[0] = 0.35F;
    paramTarget.g[1] = 1.0F;
  }
  
  private static void e(Target paramTarget)
  {
    paramTarget.g[1] = 0.3F;
    paramTarget.g[2] = 0.4F;
  }
  
  private void l()
  {
    this.i[0] = 0.24F;
    this.i[1] = 0.52F;
    this.i[2] = 0.24F;
  }
  
  @FloatRange
  public float a()
  {
    return this.g[0];
  }
  
  @FloatRange
  public float b()
  {
    return this.g[1];
  }
  
  @FloatRange
  public float c()
  {
    return this.g[2];
  }
  
  @FloatRange
  public float d()
  {
    return this.h[0];
  }
  
  @FloatRange
  public float e()
  {
    return this.h[1];
  }
  
  @FloatRange
  public float f()
  {
    return this.h[2];
  }
  
  public float g()
  {
    return this.i[0];
  }
  
  public float h()
  {
    return this.i[1];
  }
  
  public float i()
  {
    return this.i[2];
  }
  
  public boolean j()
  {
    return this.j;
  }
  
  void k()
  {
    float f1 = 0.0F;
    int k = 0;
    int m = this.i.length;
    while (k < m)
    {
      float f3 = this.i[k];
      float f2 = f1;
      if (f3 > 0.0F) {
        f2 = f1 + f3;
      }
      k += 1;
      f1 = f2;
    }
    if (f1 != 0.0F)
    {
      k = 0;
      m = this.i.length;
      while (k < m)
      {
        if (this.i[k] > 0.0F)
        {
          float[] arrayOfFloat = this.i;
          arrayOfFloat[k] /= f1;
        }
        k += 1;
      }
    }
  }
  
  public static final class Builder
  {
    private final Target a = new Target(null);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/graphics/Target.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */