package android.support.v7.widget;

import android.content.Context;
import android.view.View;

class CardViewApi21
  implements CardViewImpl
{
  private RoundRectDrawable i(CardViewDelegate paramCardViewDelegate)
  {
    return (RoundRectDrawable)paramCardViewDelegate.c();
  }
  
  public float a(CardViewDelegate paramCardViewDelegate)
  {
    return i(paramCardViewDelegate).a();
  }
  
  public void a() {}
  
  public void a(CardViewDelegate paramCardViewDelegate, float paramFloat)
  {
    i(paramCardViewDelegate).a(paramFloat);
  }
  
  public void a(CardViewDelegate paramCardViewDelegate, int paramInt)
  {
    i(paramCardViewDelegate).a(paramInt);
  }
  
  public void a(CardViewDelegate paramCardViewDelegate, Context paramContext, int paramInt, float paramFloat1, float paramFloat2, float paramFloat3)
  {
    paramCardViewDelegate.a(new RoundRectDrawable(paramInt, paramFloat1));
    paramContext = paramCardViewDelegate.d();
    paramContext.setClipToOutline(true);
    paramContext.setElevation(paramFloat2);
    b(paramCardViewDelegate, paramFloat3);
  }
  
  public float b(CardViewDelegate paramCardViewDelegate)
  {
    return d(paramCardViewDelegate) * 2.0F;
  }
  
  public void b(CardViewDelegate paramCardViewDelegate, float paramFloat)
  {
    i(paramCardViewDelegate).a(paramFloat, paramCardViewDelegate.a(), paramCardViewDelegate.b());
    f(paramCardViewDelegate);
  }
  
  public float c(CardViewDelegate paramCardViewDelegate)
  {
    return d(paramCardViewDelegate) * 2.0F;
  }
  
  public void c(CardViewDelegate paramCardViewDelegate, float paramFloat)
  {
    paramCardViewDelegate.d().setElevation(paramFloat);
  }
  
  public float d(CardViewDelegate paramCardViewDelegate)
  {
    return i(paramCardViewDelegate).b();
  }
  
  public float e(CardViewDelegate paramCardViewDelegate)
  {
    return paramCardViewDelegate.d().getElevation();
  }
  
  public void f(CardViewDelegate paramCardViewDelegate)
  {
    if (!paramCardViewDelegate.a())
    {
      paramCardViewDelegate.a(0, 0, 0, 0);
      return;
    }
    float f1 = a(paramCardViewDelegate);
    float f2 = d(paramCardViewDelegate);
    int i = (int)Math.ceil(RoundRectDrawableWithShadow.b(f1, f2, paramCardViewDelegate.b()));
    int j = (int)Math.ceil(RoundRectDrawableWithShadow.a(f1, f2, paramCardViewDelegate.b()));
    paramCardViewDelegate.a(i, j, i, j);
  }
  
  public void g(CardViewDelegate paramCardViewDelegate)
  {
    b(paramCardViewDelegate, a(paramCardViewDelegate));
  }
  
  public void h(CardViewDelegate paramCardViewDelegate)
  {
    b(paramCardViewDelegate, a(paramCardViewDelegate));
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/CardViewApi21.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */