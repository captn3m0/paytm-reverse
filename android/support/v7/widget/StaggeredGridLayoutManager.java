package android.support.v7.widget;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.CollectionItemInfoCompat;
import android.support.v4.view.accessibility.AccessibilityRecordCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.accessibility.AccessibilityEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

public class StaggeredGridLayoutManager
  extends RecyclerView.LayoutManager
{
  private final Runnable A = new Runnable()
  {
    public void run()
    {
      StaggeredGridLayoutManager.a(StaggeredGridLayoutManager.this);
    }
  };
  @NonNull
  OrientationHelper a;
  @NonNull
  OrientationHelper b;
  boolean c = false;
  int d = -1;
  int e = Integer.MIN_VALUE;
  LazySpanLookup f = new LazySpanLookup();
  private int g = -1;
  private Span[] h;
  private int i;
  private int j;
  @NonNull
  private final LayoutState k;
  private boolean l = false;
  private BitSet m;
  private int n = 2;
  private boolean o;
  private boolean t;
  private SavedState u;
  private int v;
  private final Rect w = new Rect();
  private final AnchorInfo x = new AnchorInfo(null);
  private boolean y = false;
  private boolean z = true;
  
  public StaggeredGridLayoutManager(int paramInt1, int paramInt2)
  {
    this.i = paramInt2;
    a(paramInt1);
    if (this.n != 0) {}
    for (;;)
    {
      c(bool);
      this.k = new LayoutState();
      m();
      return;
      bool = false;
    }
  }
  
  public StaggeredGridLayoutManager(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
  {
    paramContext = a(paramContext, paramAttributeSet, paramInt1, paramInt2);
    b(paramContext.a);
    a(paramContext.b);
    a(paramContext.c);
    if (this.n != 0) {}
    for (;;)
    {
      c(bool);
      this.k = new LayoutState();
      m();
      return;
      bool = false;
    }
  }
  
  private void K()
  {
    boolean bool = true;
    if ((this.i == 1) || (!h()))
    {
      this.c = this.l;
      return;
    }
    if (!this.l) {}
    for (;;)
    {
      this.c = bool;
      return;
      bool = false;
    }
  }
  
  private void L()
  {
    if (this.b.h() == 1073741824) {}
    int i3;
    View localView;
    int i4;
    int i2;
    do
    {
      return;
      float f1 = 0.0F;
      i3 = u();
      i1 = 0;
      if (i1 < i3)
      {
        localView = i(i1);
        float f3 = this.b.c(localView);
        if (f3 < f1) {}
        for (;;)
        {
          i1 += 1;
          break;
          float f2 = f3;
          if (((LayoutParams)localView.getLayoutParams()).a()) {
            f2 = 1.0F * f3 / this.g;
          }
          f1 = Math.max(f1, f2);
        }
      }
      i4 = this.j;
      i2 = Math.round(this.g * f1);
      i1 = i2;
      if (this.b.h() == Integer.MIN_VALUE) {
        i1 = Math.min(i2, this.b.f());
      }
      d(i1);
    } while (this.j == i4);
    int i1 = 0;
    label166:
    LayoutParams localLayoutParams;
    if (i1 < i3)
    {
      localView = i(i1);
      localLayoutParams = (LayoutParams)localView.getLayoutParams();
      if (!localLayoutParams.f) {
        break label208;
      }
    }
    for (;;)
    {
      i1 += 1;
      break label166;
      break;
      label208:
      if ((h()) && (this.i == 1))
      {
        localView.offsetLeftAndRight(-(this.g - 1 - localLayoutParams.e.d) * this.j - -(this.g - 1 - localLayoutParams.e.d) * i4);
      }
      else
      {
        i2 = localLayoutParams.e.d * this.j;
        int i5 = localLayoutParams.e.d * i4;
        if (this.i == 1) {
          localView.offsetLeftAndRight(i2 - i5);
        } else {
          localView.offsetTopAndBottom(i2 - i5);
        }
      }
    }
  }
  
  private int M()
  {
    int i1 = u();
    if (i1 == 0) {
      return 0;
    }
    return d(i(i1 - 1));
  }
  
  private int N()
  {
    if (u() == 0) {
      return 0;
    }
    return d(i(0));
  }
  
  private int a(RecyclerView.Recycler paramRecycler, LayoutState paramLayoutState, RecyclerView.State paramState)
  {
    this.m.set(0, this.g, true);
    int i3;
    label61:
    int i2;
    label64:
    View localView;
    LayoutParams localLayoutParams;
    int i8;
    int i6;
    label136:
    Span localSpan;
    label157:
    label168:
    label189:
    label222:
    int i7;
    int i4;
    int i5;
    StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem localFullSpanItem;
    if (this.k.i) {
      if (paramLayoutState.e == 1)
      {
        i1 = Integer.MAX_VALUE;
        a(paramLayoutState.e, i1);
        if (!this.c) {
          break label526;
        }
        i3 = this.a.d();
        i2 = 0;
        if ((!paramLayoutState.a(paramState)) || ((!this.k.i) && (this.m.isEmpty()))) {
          break label916;
        }
        localView = paramLayoutState.a(paramRecycler);
        localLayoutParams = (LayoutParams)localView.getLayoutParams();
        i8 = localLayoutParams.e();
        i2 = this.f.c(i8);
        if (i2 != -1) {
          break label538;
        }
        i6 = 1;
        if (i6 == 0) {
          break label554;
        }
        if (!localLayoutParams.f) {
          break label544;
        }
        localSpan = this.h[0];
        this.f.a(i8, localSpan);
        localLayoutParams.e = localSpan;
        if (paramLayoutState.e != 1) {
          break label566;
        }
        b(localView);
        a(localView, localLayoutParams, false);
        if (paramLayoutState.e != 1) {
          break label588;
        }
        if (!localLayoutParams.f) {
          break label576;
        }
        i2 = q(i3);
        i7 = i2 + this.a.c(localView);
        i4 = i2;
        i5 = i7;
        if (i6 != 0)
        {
          i4 = i2;
          i5 = i7;
          if (localLayoutParams.f)
          {
            localFullSpanItem = m(i2);
            localFullSpanItem.b = -1;
            localFullSpanItem.a = i8;
            this.f.a(localFullSpanItem);
            i5 = i7;
            i4 = i2;
          }
        }
        if ((localLayoutParams.f) && (paramLayoutState.d == -1))
        {
          if (i6 == 0) {
            break label700;
          }
          this.y = true;
        }
        a(localView, localLayoutParams, paramLayoutState);
        if ((!h()) || (this.i != 1)) {
          break label808;
        }
        if (!localLayoutParams.f) {
          break label778;
        }
        i2 = this.b.d();
        label370:
        i7 = i2 - this.b.c(localView);
        i6 = i2;
        i2 = i7;
        if (this.i != 1) {
          break label865;
        }
        b(localView, i2, i4, i6, i5);
        label414:
        if (!localLayoutParams.f) {
          break label882;
        }
        a(this.k.e, i1);
        label435:
        a(paramRecycler, this.k);
        if ((this.k.h) && (localView.isFocusable()))
        {
          if (!localLayoutParams.f) {
            break label900;
          }
          this.m.clear();
        }
      }
    }
    for (;;)
    {
      i2 = 1;
      break label64;
      i1 = Integer.MIN_VALUE;
      break;
      if (paramLayoutState.e == 1)
      {
        i1 = paramLayoutState.g + paramLayoutState.b;
        break;
      }
      i1 = paramLayoutState.f - paramLayoutState.b;
      break;
      label526:
      i3 = this.a.c();
      break label61;
      label538:
      i6 = 0;
      break label136;
      label544:
      localSpan = a(paramLayoutState);
      break label157;
      label554:
      localSpan = this.h[i2];
      break label168;
      label566:
      b(localView, 0);
      break label189;
      label576:
      i2 = localSpan.b(i3);
      break label222;
      label588:
      if (localLayoutParams.f) {}
      for (i2 = p(i3);; i2 = localSpan.a(i3))
      {
        i7 = i2 - this.a.c(localView);
        i4 = i7;
        i5 = i2;
        if (i6 == 0) {
          break;
        }
        i4 = i7;
        i5 = i2;
        if (!localLayoutParams.f) {
          break;
        }
        localFullSpanItem = n(i2);
        localFullSpanItem.b = 1;
        localFullSpanItem.a = i8;
        this.f.a(localFullSpanItem);
        i4 = i7;
        i5 = i2;
        break;
      }
      label700:
      if (paramLayoutState.e == 1)
      {
        if (!j()) {}
        for (i2 = 1;; i2 = 0)
        {
          label718:
          if (i2 == 0) {
            break label770;
          }
          localFullSpanItem = this.f.f(i8);
          if (localFullSpanItem != null) {
            localFullSpanItem.d = true;
          }
          this.y = true;
          break;
        }
      }
      if (!k()) {}
      for (i2 = 1;; i2 = 0)
      {
        break label718;
        label770:
        break;
      }
      label778:
      i2 = this.b.d() - (this.g - 1 - localSpan.d) * this.j;
      break label370;
      label808:
      if (localLayoutParams.f) {}
      for (i2 = this.b.c();; i2 = localSpan.d * this.j + this.b.c())
      {
        i6 = i2 + this.b.c(localView);
        break;
      }
      label865:
      b(localView, i4, i2, i5, i6);
      break label414;
      label882:
      a(localSpan, this.k.e, i1);
      break label435;
      label900:
      this.m.set(localSpan.d, false);
    }
    label916:
    if (i2 == 0) {
      a(paramRecycler, this.k);
    }
    if (this.k.e == -1) {
      i1 = p(this.a.c());
    }
    for (int i1 = this.a.c() - i1; i1 > 0; i1 = q(this.a.d()) - this.a.d()) {
      return Math.min(paramLayoutState.b, i1);
    }
    return 0;
  }
  
  private int a(RecyclerView.State paramState)
  {
    boolean bool2 = false;
    if (u() == 0) {
      return 0;
    }
    OrientationHelper localOrientationHelper = this.a;
    if (!this.z) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      View localView = a(bool1, true);
      bool1 = bool2;
      if (!this.z) {
        bool1 = true;
      }
      return ScrollbarHelper.a(paramState, localOrientationHelper, localView, b(bool1, true), this, this.z, this.c);
    }
  }
  
  private Span a(LayoutState paramLayoutState)
  {
    int i1;
    int i3;
    if (s(paramLayoutState.e))
    {
      i1 = this.g - 1;
      i3 = -1;
    }
    int i6;
    int i5;
    for (int i2 = -1; paramLayoutState.e == 1; i2 = 1)
    {
      paramLayoutState = null;
      i4 = Integer.MAX_VALUE;
      i7 = this.a.c();
      for (;;)
      {
        localObject = paramLayoutState;
        if (i1 == i3) {
          break;
        }
        localObject = this.h[i1];
        i6 = ((Span)localObject).b(i7);
        i5 = i4;
        if (i6 < i4)
        {
          paramLayoutState = (LayoutState)localObject;
          i5 = i6;
        }
        i1 += i2;
        i4 = i5;
      }
      i1 = 0;
      i3 = this.g;
    }
    paramLayoutState = null;
    int i4 = Integer.MIN_VALUE;
    int i7 = this.a.d();
    while (i1 != i3)
    {
      localObject = this.h[i1];
      i6 = ((Span)localObject).a(i7);
      i5 = i4;
      if (i6 > i4)
      {
        paramLayoutState = (LayoutState)localObject;
        i5 = i6;
      }
      i1 += i2;
      i4 = i5;
    }
    Object localObject = paramLayoutState;
    return (Span)localObject;
  }
  
  private void a(int paramInt1, int paramInt2)
  {
    int i1 = 0;
    if (i1 < this.g)
    {
      if (Span.a(this.h[i1]).isEmpty()) {}
      for (;;)
      {
        i1 += 1;
        break;
        a(this.h[i1], paramInt1, paramInt2);
      }
    }
  }
  
  private void a(int paramInt, RecyclerView.State paramState)
  {
    boolean bool2 = true;
    this.k.b = 0;
    this.k.c = paramInt;
    int i3 = 0;
    int i4 = 0;
    int i1 = i4;
    int i2 = i3;
    if (r())
    {
      int i5 = paramState.c();
      i1 = i4;
      i2 = i3;
      if (i5 != -1)
      {
        boolean bool3 = this.c;
        if (i5 >= paramInt) {
          break label184;
        }
        bool1 = true;
        if (bool3 != bool1) {
          break label190;
        }
        i1 = this.a.f();
        i2 = i3;
      }
    }
    label92:
    if (q())
    {
      this.k.f = (this.a.c() - i2);
      this.k.g = (this.a.d() + i1);
      label132:
      this.k.h = false;
      this.k.a = true;
      paramState = this.k;
      if ((this.a.h() != 0) || (this.a.e() != 0)) {
        break label234;
      }
    }
    label184:
    label190:
    label234:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      paramState.i = bool1;
      return;
      bool1 = false;
      break;
      i2 = this.a.f();
      i1 = i4;
      break label92;
      this.k.g = (this.a.e() + i1);
      this.k.f = (-i2);
      break label132;
    }
  }
  
  private void a(RecyclerView.Recycler paramRecycler, int paramInt)
  {
    for (;;)
    {
      View localView;
      LayoutParams localLayoutParams;
      if (u() > 0)
      {
        localView = i(0);
        if (this.a.b(localView) <= paramInt)
        {
          localLayoutParams = (LayoutParams)localView.getLayoutParams();
          if (!localLayoutParams.f) {
            break label105;
          }
          i1 = 0;
          if (i1 >= this.g) {
            break label79;
          }
          if (Span.a(this.h[i1]).size() != 1) {
            break label72;
          }
        }
      }
      label72:
      label79:
      label105:
      while (Span.a(localLayoutParams.e).size() == 1)
      {
        for (;;)
        {
          return;
          i1 += 1;
        }
        int i1 = 0;
        while (i1 < this.g)
        {
          this.h[i1].h();
          i1 += 1;
        }
      }
      localLayoutParams.e.h();
      a(localView, paramRecycler);
    }
  }
  
  private void a(RecyclerView.Recycler paramRecycler, LayoutState paramLayoutState)
  {
    if ((!paramLayoutState.a) || (paramLayoutState.i)) {
      return;
    }
    if (paramLayoutState.b == 0)
    {
      if (paramLayoutState.e == -1)
      {
        b(paramRecycler, paramLayoutState.g);
        return;
      }
      a(paramRecycler, paramLayoutState.f);
      return;
    }
    if (paramLayoutState.e == -1)
    {
      i1 = paramLayoutState.f - o(paramLayoutState.f);
      if (i1 < 0) {}
      for (i1 = paramLayoutState.g;; i1 = paramLayoutState.g - Math.min(i1, paramLayoutState.b))
      {
        b(paramRecycler, i1);
        return;
      }
    }
    int i1 = r(paramLayoutState.g) - paramLayoutState.g;
    if (i1 < 0) {}
    for (i1 = paramLayoutState.f;; i1 = paramLayoutState.f + Math.min(i1, paramLayoutState.b))
    {
      a(paramRecycler, i1);
      return;
    }
  }
  
  private void a(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, boolean paramBoolean)
  {
    int i3 = 1;
    AnchorInfo localAnchorInfo = this.x;
    localAnchorInfo.a();
    if (((this.u != null) || (this.d != -1)) && (paramState.e() == 0))
    {
      c(paramRecycler);
      return;
    }
    if (this.u != null) {
      a(localAnchorInfo);
    }
    for (;;)
    {
      a(paramState, localAnchorInfo);
      if ((this.u == null) && ((localAnchorInfo.c != this.o) || (h() != this.t)))
      {
        this.f.a();
        localAnchorInfo.d = true;
      }
      if ((u() <= 0) || ((this.u != null) && (this.u.c >= 1))) {
        break label250;
      }
      if (!localAnchorInfo.d) {
        break;
      }
      i1 = 0;
      while (i1 < this.g)
      {
        this.h[i1].e();
        if (localAnchorInfo.b != Integer.MIN_VALUE) {
          this.h[i1].c(localAnchorInfo.b);
        }
        i1 += 1;
      }
      K();
      localAnchorInfo.c = this.c;
    }
    int i1 = 0;
    while (i1 < this.g)
    {
      this.h[i1].a(this.c, localAnchorInfo.b);
      i1 += 1;
    }
    label250:
    a(paramRecycler);
    this.k.a = false;
    this.y = false;
    d(this.b.f());
    a(localAnchorInfo.a, paramState);
    label349:
    label381:
    int i4;
    if (localAnchorInfo.c)
    {
      f(-1);
      a(paramRecycler, this.k, paramState);
      f(1);
      this.k.c = (localAnchorInfo.a + this.k.d);
      a(paramRecycler, this.k, paramState);
      L();
      if (u() > 0)
      {
        if (!this.c) {
          break label575;
        }
        b(paramRecycler, paramState, true);
        c(paramRecycler, paramState, false);
      }
      i2 = 0;
      i4 = 0;
      i1 = i2;
      if (paramBoolean)
      {
        i1 = i2;
        if (!paramState.a())
        {
          if ((this.n == 0) || (u() <= 0)) {
            break label592;
          }
          i2 = i3;
          if (!this.y) {
            if (b() == null) {
              break label592;
            }
          }
        }
      }
    }
    label575:
    label592:
    for (int i2 = i3;; i2 = 0)
    {
      i1 = i4;
      if (i2 != 0)
      {
        a(this.A);
        i1 = i4;
        if (n()) {
          i1 = 1;
        }
      }
      this.d = -1;
      this.e = Integer.MIN_VALUE;
      this.o = localAnchorInfo.c;
      this.t = h();
      this.u = null;
      if (i1 == 0) {
        break;
      }
      a(paramRecycler, paramState, false);
      return;
      f(1);
      a(paramRecycler, this.k, paramState);
      f(-1);
      this.k.c = (localAnchorInfo.a + this.k.d);
      a(paramRecycler, this.k, paramState);
      break label349;
      c(paramRecycler, paramState, true);
      b(paramRecycler, paramState, false);
      break label381;
    }
  }
  
  private void a(AnchorInfo paramAnchorInfo)
  {
    if (this.u.c > 0) {
      if (this.u.c == this.g)
      {
        int i2 = 0;
        if (i2 < this.g)
        {
          this.h[i2].e();
          int i3 = this.u.d[i2];
          int i1 = i3;
          if (i3 != Integer.MIN_VALUE) {
            if (!this.u.i) {
              break label102;
            }
          }
          label102:
          for (i1 = i3 + this.a.d();; i1 = i3 + this.a.c())
          {
            this.h[i2].c(i1);
            i2 += 1;
            break;
          }
        }
      }
      else
      {
        this.u.a();
        this.u.a = this.u.b;
      }
    }
    this.t = this.u.j;
    a(this.u.h);
    K();
    if (this.u.a != -1) {
      this.d = this.u.a;
    }
    for (paramAnchorInfo.c = this.u.i;; paramAnchorInfo.c = this.c)
    {
      if (this.u.e > 1)
      {
        this.f.a = this.u.f;
        this.f.b = this.u.g;
      }
      return;
    }
  }
  
  private void a(Span paramSpan, int paramInt1, int paramInt2)
  {
    int i1 = paramSpan.i();
    if (paramInt1 == -1) {
      if (paramSpan.b() + i1 <= paramInt2) {
        this.m.set(paramSpan.d, false);
      }
    }
    while (paramSpan.d() - i1 < paramInt2) {
      return;
    }
    this.m.set(paramSpan.d, false);
  }
  
  private void a(View paramView, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    a(paramView, this.w);
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    paramInt1 = b(paramInt1, localLayoutParams.leftMargin + this.w.left, localLayoutParams.rightMargin + this.w.right);
    paramInt2 = b(paramInt2, localLayoutParams.topMargin + this.w.top, localLayoutParams.bottomMargin + this.w.bottom);
    if (paramBoolean) {}
    for (paramBoolean = a(paramView, paramInt1, paramInt2, localLayoutParams);; paramBoolean = b(paramView, paramInt1, paramInt2, localLayoutParams))
    {
      if (paramBoolean) {
        paramView.measure(paramInt1, paramInt2);
      }
      return;
    }
  }
  
  private void a(View paramView, LayoutParams paramLayoutParams, LayoutState paramLayoutState)
  {
    if (paramLayoutState.e == 1)
    {
      if (paramLayoutParams.f)
      {
        p(paramView);
        return;
      }
      paramLayoutParams.e.b(paramView);
      return;
    }
    if (paramLayoutParams.f)
    {
      q(paramView);
      return;
    }
    paramLayoutParams.e.a(paramView);
  }
  
  private void a(View paramView, LayoutParams paramLayoutParams, boolean paramBoolean)
  {
    if (paramLayoutParams.f)
    {
      if (this.i == 1)
      {
        a(paramView, this.v, a(y(), w(), 0, paramLayoutParams.height, true), paramBoolean);
        return;
      }
      a(paramView, a(x(), v(), 0, paramLayoutParams.width, true), this.v, paramBoolean);
      return;
    }
    if (this.i == 1)
    {
      a(paramView, a(this.j, v(), 0, paramLayoutParams.width, false), a(y(), w(), 0, paramLayoutParams.height, true), paramBoolean);
      return;
    }
    a(paramView, a(x(), v(), 0, paramLayoutParams.width, true), a(this.j, w(), 0, paramLayoutParams.height, false), paramBoolean);
  }
  
  private boolean a(Span paramSpan)
  {
    if (this.c)
    {
      if (paramSpan.d() >= this.a.d()) {
        break label91;
      }
      if (paramSpan.c((View)Span.a(paramSpan).get(Span.a(paramSpan).size() - 1)).f) {}
    }
    do
    {
      return true;
      return false;
      if (paramSpan.b() <= this.a.c()) {
        break;
      }
    } while (!paramSpan.c((View)Span.a(paramSpan).get(0)).f);
    return false;
    label91:
    return false;
  }
  
  private int b(int paramInt1, int paramInt2, int paramInt3)
  {
    if ((paramInt2 == 0) && (paramInt3 == 0)) {}
    int i1;
    do
    {
      return paramInt1;
      i1 = View.MeasureSpec.getMode(paramInt1);
    } while ((i1 != Integer.MIN_VALUE) && (i1 != 1073741824));
    return View.MeasureSpec.makeMeasureSpec(Math.max(0, View.MeasureSpec.getSize(paramInt1) - paramInt2 - paramInt3), i1);
  }
  
  private void b(RecyclerView.Recycler paramRecycler, int paramInt)
  {
    int i1 = u() - 1;
    for (;;)
    {
      View localView;
      LayoutParams localLayoutParams;
      if (i1 >= 0)
      {
        localView = i(i1);
        if (this.a.a(localView) >= paramInt)
        {
          localLayoutParams = (LayoutParams)localView.getLayoutParams();
          if (!localLayoutParams.f) {
            break label119;
          }
          i2 = 0;
          if (i2 >= this.g) {
            break label88;
          }
          if (Span.a(this.h[i2]).size() != 1) {
            break label79;
          }
        }
      }
      label79:
      label88:
      label119:
      while (Span.a(localLayoutParams.e).size() == 1)
      {
        for (;;)
        {
          return;
          i2 += 1;
        }
        int i2 = 0;
        while (i2 < this.g)
        {
          this.h[i2].g();
          i2 += 1;
        }
      }
      localLayoutParams.e.g();
      a(localView, paramRecycler);
      i1 -= 1;
    }
  }
  
  private void b(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, boolean paramBoolean)
  {
    int i1 = q(Integer.MIN_VALUE);
    if (i1 == Integer.MIN_VALUE) {}
    do
    {
      do
      {
        return;
        i1 = this.a.d() - i1;
      } while (i1 <= 0);
      i1 -= -c(-i1, paramRecycler, paramState);
    } while ((!paramBoolean) || (i1 <= 0));
    this.a.a(i1);
  }
  
  private void b(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    a(paramView, paramInt1 + localLayoutParams.leftMargin, paramInt2 + localLayoutParams.topMargin, paramInt3 - localLayoutParams.rightMargin, paramInt4 - localLayoutParams.bottomMargin);
  }
  
  private void c(int paramInt1, int paramInt2, int paramInt3)
  {
    int i3;
    int i2;
    int i1;
    if (this.c)
    {
      i3 = M();
      if (paramInt3 != 8) {
        break label104;
      }
      if (paramInt1 >= paramInt2) {
        break label93;
      }
      i2 = paramInt2 + 1;
      i1 = paramInt1;
      label32:
      this.f.b(i1);
      switch (paramInt3)
      {
      default: 
        label76:
        if (i2 > i3) {
          break;
        }
      }
    }
    for (;;)
    {
      return;
      i3 = N();
      break;
      label93:
      i2 = paramInt1 + 1;
      i1 = paramInt2;
      break label32;
      label104:
      i1 = paramInt1;
      i2 = paramInt1 + paramInt2;
      break label32;
      this.f.b(paramInt1, paramInt2);
      break label76;
      this.f.a(paramInt1, paramInt2);
      break label76;
      this.f.a(paramInt1, 1);
      this.f.b(paramInt2, 1);
      break label76;
      if (this.c) {}
      for (paramInt1 = N(); i1 <= paramInt1; paramInt1 = M())
      {
        o();
        return;
      }
    }
  }
  
  private void c(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, boolean paramBoolean)
  {
    int i1 = p(Integer.MAX_VALUE);
    if (i1 == Integer.MAX_VALUE) {}
    do
    {
      do
      {
        return;
        i1 -= this.a.c();
      } while (i1 <= 0);
      i1 -= c(i1, paramRecycler, paramState);
    } while ((!paramBoolean) || (i1 <= 0));
    this.a.a(-i1);
  }
  
  private boolean c(RecyclerView.State paramState, AnchorInfo paramAnchorInfo)
  {
    if (this.o) {}
    for (int i1 = v(paramState.e());; i1 = u(paramState.e()))
    {
      paramAnchorInfo.a = i1;
      paramAnchorInfo.b = Integer.MIN_VALUE;
      return true;
    }
  }
  
  private void f(int paramInt)
  {
    int i1 = 1;
    this.k.e = paramInt;
    LayoutState localLayoutState = this.k;
    boolean bool2 = this.c;
    boolean bool1;
    if (paramInt == -1)
    {
      bool1 = true;
      if (bool2 != bool1) {
        break label49;
      }
    }
    label49:
    for (paramInt = i1;; paramInt = -1)
    {
      localLayoutState.d = paramInt;
      return;
      bool1 = false;
      break;
    }
  }
  
  private int h(RecyclerView.State paramState)
  {
    boolean bool2 = false;
    if (u() == 0) {
      return 0;
    }
    OrientationHelper localOrientationHelper = this.a;
    if (!this.z) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      View localView = a(bool1, true);
      bool1 = bool2;
      if (!this.z) {
        bool1 = true;
      }
      return ScrollbarHelper.a(paramState, localOrientationHelper, localView, b(bool1, true), this, this.z);
    }
  }
  
  private int i(RecyclerView.State paramState)
  {
    boolean bool2 = false;
    if (u() == 0) {
      return 0;
    }
    OrientationHelper localOrientationHelper = this.a;
    if (!this.z) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      View localView = a(bool1, true);
      bool1 = bool2;
      if (!this.z) {
        bool1 = true;
      }
      return ScrollbarHelper.b(paramState, localOrientationHelper, localView, b(bool1, true), this, this.z);
    }
  }
  
  private StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem m(int paramInt)
  {
    StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem localFullSpanItem = new StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem();
    localFullSpanItem.c = new int[this.g];
    int i1 = 0;
    while (i1 < this.g)
    {
      localFullSpanItem.c[i1] = (paramInt - this.h[i1].b(paramInt));
      i1 += 1;
    }
    return localFullSpanItem;
  }
  
  private void m()
  {
    this.a = OrientationHelper.a(this, this.i);
    this.b = OrientationHelper.a(this, 1 - this.i);
  }
  
  private StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem n(int paramInt)
  {
    StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem localFullSpanItem = new StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem();
    localFullSpanItem.c = new int[this.g];
    int i1 = 0;
    while (i1 < this.g)
    {
      localFullSpanItem.c[i1] = (this.h[i1].a(paramInt) - paramInt);
      i1 += 1;
    }
    return localFullSpanItem;
  }
  
  private boolean n()
  {
    if ((u() == 0) || (this.n == 0) || (!p())) {
      return false;
    }
    int i2;
    if (this.c) {
      i2 = M();
    }
    for (int i1 = N(); (i2 == 0) && (b() != null); i1 = M())
    {
      this.f.a();
      I();
      o();
      return true;
      i2 = N();
    }
    if (!this.y) {
      return false;
    }
    if (this.c) {}
    StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem localFullSpanItem1;
    for (int i3 = -1;; i3 = 1)
    {
      localFullSpanItem1 = this.f.a(i2, i1 + 1, i3, true);
      if (localFullSpanItem1 != null) {
        break;
      }
      this.y = false;
      this.f.a(i1 + 1);
      return false;
    }
    StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem localFullSpanItem2 = this.f.a(i2, localFullSpanItem1.a, i3 * -1, true);
    if (localFullSpanItem2 == null) {
      this.f.a(localFullSpanItem1.a);
    }
    for (;;)
    {
      I();
      o();
      return true;
      this.f.a(localFullSpanItem2.a + 1);
    }
  }
  
  private int o(int paramInt)
  {
    int i2 = this.h[0].a(paramInt);
    int i1 = 1;
    while (i1 < this.g)
    {
      int i4 = this.h[i1].a(paramInt);
      int i3 = i2;
      if (i4 > i2) {
        i3 = i4;
      }
      i1 += 1;
      i2 = i3;
    }
    return i2;
  }
  
  private int p(int paramInt)
  {
    int i2 = this.h[0].a(paramInt);
    int i1 = 1;
    while (i1 < this.g)
    {
      int i4 = this.h[i1].a(paramInt);
      int i3 = i2;
      if (i4 < i2) {
        i3 = i4;
      }
      i1 += 1;
      i2 = i3;
    }
    return i2;
  }
  
  private void p(View paramView)
  {
    int i1 = this.g - 1;
    while (i1 >= 0)
    {
      this.h[i1].b(paramView);
      i1 -= 1;
    }
  }
  
  private int q(int paramInt)
  {
    int i2 = this.h[0].b(paramInt);
    int i1 = 1;
    while (i1 < this.g)
    {
      int i4 = this.h[i1].b(paramInt);
      int i3 = i2;
      if (i4 > i2) {
        i3 = i4;
      }
      i1 += 1;
      i2 = i3;
    }
    return i2;
  }
  
  private void q(View paramView)
  {
    int i1 = this.g - 1;
    while (i1 >= 0)
    {
      this.h[i1].a(paramView);
      i1 -= 1;
    }
  }
  
  private int r(int paramInt)
  {
    int i2 = this.h[0].b(paramInt);
    int i1 = 1;
    while (i1 < this.g)
    {
      int i4 = this.h[i1].b(paramInt);
      int i3 = i2;
      if (i4 < i2) {
        i3 = i4;
      }
      i1 += 1;
      i2 = i3;
    }
    return i2;
  }
  
  private boolean s(int paramInt)
  {
    int i1;
    if (this.i == 0) {
      if (paramInt == -1)
      {
        i1 = 1;
        if (i1 == this.c) {
          break label29;
        }
      }
    }
    label29:
    label63:
    label66:
    for (;;)
    {
      return true;
      i1 = 0;
      break;
      return false;
      if (paramInt == -1)
      {
        i1 = 1;
        if (i1 != this.c) {
          break label63;
        }
      }
      for (i1 = 1;; i1 = 0)
      {
        if (i1 == h()) {
          break label66;
        }
        return false;
        i1 = 0;
        break;
      }
    }
  }
  
  private int t(int paramInt)
  {
    int i1 = -1;
    if (u() == 0)
    {
      if (this.c) {
        return 1;
      }
      return -1;
    }
    int i2;
    if (paramInt < N())
    {
      i2 = 1;
      if (i2 == this.c) {
        break label47;
      }
    }
    label47:
    for (paramInt = i1;; paramInt = 1)
    {
      return paramInt;
      i2 = 0;
      break;
    }
  }
  
  private int u(int paramInt)
  {
    int i2 = u();
    int i1 = 0;
    while (i1 < i2)
    {
      int i3 = d(i(i1));
      if ((i3 >= 0) && (i3 < paramInt)) {
        return i3;
      }
      i1 += 1;
    }
    return 0;
  }
  
  private int v(int paramInt)
  {
    int i1 = u() - 1;
    while (i1 >= 0)
    {
      int i2 = d(i(i1));
      if ((i2 >= 0) && (i2 < paramInt)) {
        return i2;
      }
      i1 -= 1;
    }
    return 0;
  }
  
  private int w(int paramInt)
  {
    int i2 = -1;
    int i3 = 1;
    int i4 = Integer.MIN_VALUE;
    int i1 = i2;
    switch (paramInt)
    {
    default: 
      i1 = Integer.MIN_VALUE;
    case 1: 
    case 2: 
    case 33: 
    case 130: 
    case 17: 
      do
      {
        do
        {
          return i1;
          return 1;
          i1 = i2;
        } while (this.i == 1);
        return Integer.MIN_VALUE;
        paramInt = i4;
        if (this.i == 1) {
          paramInt = 1;
        }
        return paramInt;
        i1 = i2;
      } while (this.i == 0);
      return Integer.MIN_VALUE;
    }
    if (this.i == 0) {}
    for (paramInt = i3;; paramInt = Integer.MIN_VALUE) {
      return paramInt;
    }
  }
  
  public int a(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    return c(paramInt, paramRecycler, paramState);
  }
  
  public int a(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    if (this.i == 0) {
      return this.g;
    }
    return super.a(paramRecycler, paramState);
  }
  
  public RecyclerView.LayoutParams a()
  {
    if (this.i == 0) {
      return new LayoutParams(-2, -1);
    }
    return new LayoutParams(-1, -2);
  }
  
  public RecyclerView.LayoutParams a(Context paramContext, AttributeSet paramAttributeSet)
  {
    return new LayoutParams(paramContext, paramAttributeSet);
  }
  
  public RecyclerView.LayoutParams a(ViewGroup.LayoutParams paramLayoutParams)
  {
    if ((paramLayoutParams instanceof ViewGroup.MarginLayoutParams)) {
      return new LayoutParams((ViewGroup.MarginLayoutParams)paramLayoutParams);
    }
    return new LayoutParams(paramLayoutParams);
  }
  
  @Nullable
  public View a(View paramView, int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    if (u() == 0)
    {
      paramView = null;
      return paramView;
    }
    View localView = e(paramView);
    if (localView == null) {
      return null;
    }
    K();
    int i2 = w(paramInt);
    if (i2 == Integer.MIN_VALUE) {
      return null;
    }
    paramView = (LayoutParams)localView.getLayoutParams();
    boolean bool = paramView.f;
    paramView = paramView.e;
    if (i2 == 1) {}
    for (paramInt = M();; paramInt = N())
    {
      a(paramInt, paramState);
      f(i2);
      this.k.c = (this.k.d + paramInt);
      this.k.b = ((int)(0.33333334F * this.a.f()));
      this.k.h = true;
      this.k.a = false;
      a(paramRecycler, this.k, paramState);
      this.o = this.c;
      if (!bool)
      {
        paramRecycler = paramView.a(paramInt, i2);
        if (paramRecycler != null)
        {
          paramView = paramRecycler;
          if (paramRecycler != localView) {
            break;
          }
        }
      }
      if (!s(i2)) {
        break label251;
      }
      i1 = this.g - 1;
      for (;;)
      {
        if (i1 < 0) {
          break label298;
        }
        paramRecycler = this.h[i1].a(paramInt, i2);
        if (paramRecycler != null)
        {
          paramView = paramRecycler;
          if (paramRecycler != localView) {
            break;
          }
        }
        i1 -= 1;
      }
    }
    label251:
    int i1 = 0;
    for (;;)
    {
      if (i1 >= this.g) {
        break label298;
      }
      paramRecycler = this.h[i1].a(paramInt, i2);
      if (paramRecycler != null)
      {
        paramView = paramRecycler;
        if (paramRecycler != localView) {
          break;
        }
      }
      i1 += 1;
    }
    label298:
    return null;
  }
  
  View a(boolean paramBoolean1, boolean paramBoolean2)
  {
    int i2 = this.a.c();
    int i3 = this.a.d();
    int i4 = u();
    Object localObject1 = null;
    int i1 = 0;
    if (i1 < i4)
    {
      View localView = i(i1);
      int i5 = this.a.a(localView);
      Object localObject2 = localObject1;
      if (this.a.b(localView) > i2)
      {
        if (i5 < i3) {
          break label93;
        }
        localObject2 = localObject1;
      }
      for (;;)
      {
        i1 += 1;
        localObject1 = localObject2;
        break;
        label93:
        if ((i5 >= i2) || (!paramBoolean1)) {
          return localView;
        }
        localObject2 = localObject1;
        if (paramBoolean2)
        {
          localObject2 = localObject1;
          if (localObject1 == null) {
            localObject2 = localView;
          }
        }
      }
    }
    return (View)localObject1;
  }
  
  public void a(int paramInt)
  {
    a(null);
    if (paramInt != this.g)
    {
      g();
      this.g = paramInt;
      this.m = new BitSet(this.g);
      this.h = new Span[this.g];
      paramInt = 0;
      while (paramInt < this.g)
      {
        this.h[paramInt] = new Span(paramInt, null);
        paramInt += 1;
      }
      o();
    }
  }
  
  public void a(Rect paramRect, int paramInt1, int paramInt2)
  {
    int i2 = z() + B();
    int i3 = A() + C();
    int i1;
    if (this.i == 1)
    {
      i1 = a(paramInt2, paramRect.height() + i3, G());
      paramInt2 = a(paramInt1, this.j * this.g + i2, F());
      paramInt1 = i1;
    }
    for (;;)
    {
      e(paramInt2, paramInt1);
      return;
      i1 = a(paramInt1, paramRect.width() + i2, F());
      paramInt1 = a(paramInt2, this.j * this.g + i3, G());
      paramInt2 = i1;
    }
  }
  
  public void a(Parcelable paramParcelable)
  {
    if ((paramParcelable instanceof SavedState))
    {
      this.u = ((SavedState)paramParcelable);
      o();
    }
  }
  
  public void a(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, View paramView, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat)
  {
    paramRecycler = paramView.getLayoutParams();
    if (!(paramRecycler instanceof LayoutParams))
    {
      super.a(paramView, paramAccessibilityNodeInfoCompat);
      return;
    }
    paramRecycler = (LayoutParams)paramRecycler;
    if (this.i == 0)
    {
      i2 = paramRecycler.b();
      if (paramRecycler.f) {}
      for (i1 = this.g;; i1 = 1)
      {
        paramAccessibilityNodeInfoCompat.c(AccessibilityNodeInfoCompat.CollectionItemInfoCompat.a(i2, i1, -1, -1, paramRecycler.f, false));
        return;
      }
    }
    int i2 = paramRecycler.b();
    if (paramRecycler.f) {}
    for (int i1 = this.g;; i1 = 1)
    {
      paramAccessibilityNodeInfoCompat.c(AccessibilityNodeInfoCompat.CollectionItemInfoCompat.a(-1, -1, i2, i1, paramRecycler.f, false));
      return;
    }
  }
  
  void a(RecyclerView.State paramState, AnchorInfo paramAnchorInfo)
  {
    if (b(paramState, paramAnchorInfo)) {}
    while (c(paramState, paramAnchorInfo)) {
      return;
    }
    paramAnchorInfo.b();
    paramAnchorInfo.a = 0;
  }
  
  public void a(RecyclerView paramRecyclerView)
  {
    this.f.a();
    o();
  }
  
  public void a(RecyclerView paramRecyclerView, int paramInt1, int paramInt2)
  {
    c(paramInt1, paramInt2, 1);
  }
  
  public void a(RecyclerView paramRecyclerView, int paramInt1, int paramInt2, int paramInt3)
  {
    c(paramInt1, paramInt2, 8);
  }
  
  public void a(RecyclerView paramRecyclerView, int paramInt1, int paramInt2, Object paramObject)
  {
    c(paramInt1, paramInt2, 4);
  }
  
  public void a(RecyclerView paramRecyclerView, RecyclerView.Recycler paramRecycler)
  {
    a(this.A);
    int i1 = 0;
    while (i1 < this.g)
    {
      this.h[i1].e();
      i1 += 1;
    }
  }
  
  public void a(RecyclerView paramRecyclerView, RecyclerView.State paramState, int paramInt)
  {
    paramRecyclerView = new LinearSmoothScroller(paramRecyclerView.getContext())
    {
      public PointF a(int paramAnonymousInt)
      {
        paramAnonymousInt = StaggeredGridLayoutManager.a(StaggeredGridLayoutManager.this, paramAnonymousInt);
        if (paramAnonymousInt == 0) {
          return null;
        }
        if (StaggeredGridLayoutManager.b(StaggeredGridLayoutManager.this) == 0) {
          return new PointF(paramAnonymousInt, 0.0F);
        }
        return new PointF(0.0F, paramAnonymousInt);
      }
    };
    paramRecyclerView.d(paramInt);
    a(paramRecyclerView);
  }
  
  public void a(AccessibilityEvent paramAccessibilityEvent)
  {
    super.a(paramAccessibilityEvent);
    View localView1;
    View localView2;
    if (u() > 0)
    {
      paramAccessibilityEvent = AccessibilityEventCompat.a(paramAccessibilityEvent);
      localView1 = a(false, true);
      localView2 = b(false, true);
      if ((localView1 != null) && (localView2 != null)) {}
    }
    else
    {
      return;
    }
    int i1 = d(localView1);
    int i2 = d(localView2);
    if (i1 < i2)
    {
      paramAccessibilityEvent.b(i1);
      paramAccessibilityEvent.c(i2);
      return;
    }
    paramAccessibilityEvent.b(i2);
    paramAccessibilityEvent.c(i1);
  }
  
  public void a(String paramString)
  {
    if (this.u == null) {
      super.a(paramString);
    }
  }
  
  public void a(boolean paramBoolean)
  {
    a(null);
    if ((this.u != null) && (this.u.h != paramBoolean)) {
      this.u.h = paramBoolean;
    }
    this.l = paramBoolean;
    o();
  }
  
  public boolean a(RecyclerView.LayoutParams paramLayoutParams)
  {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  public int[] a(int[] paramArrayOfInt)
  {
    int[] arrayOfInt;
    if (paramArrayOfInt == null) {
      arrayOfInt = new int[this.g];
    }
    do
    {
      int i1 = 0;
      while (i1 < this.g)
      {
        arrayOfInt[i1] = this.h[i1].j();
        i1 += 1;
      }
      arrayOfInt = paramArrayOfInt;
    } while (paramArrayOfInt.length >= this.g);
    throw new IllegalArgumentException("Provided int[]'s size must be more than or equal to span count. Expected:" + this.g + ", array size:" + paramArrayOfInt.length);
    return arrayOfInt;
  }
  
  public int b(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    return c(paramInt, paramRecycler, paramState);
  }
  
  public int b(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    if (this.i == 1) {
      return this.g;
    }
    return super.b(paramRecycler, paramState);
  }
  
  public int b(RecyclerView.State paramState)
  {
    return a(paramState);
  }
  
  View b()
  {
    int i1 = u() - 1;
    BitSet localBitSet = new BitSet(this.g);
    localBitSet.set(0, this.g, true);
    int i2;
    int i3;
    if ((this.i == 1) && (h()))
    {
      i2 = 1;
      if (!this.c) {
        break label130;
      }
      i3 = 0 - 1;
      label59:
      if (i1 >= i3) {
        break label143;
      }
    }
    int i5;
    View localView;
    LayoutParams localLayoutParams;
    label122:
    label130:
    label143:
    for (int i4 = 1;; i4 = -1)
    {
      i5 = i1;
      if (i5 == i3) {
        break label351;
      }
      localView = i(i5);
      localLayoutParams = (LayoutParams)localView.getLayoutParams();
      if (!localBitSet.get(localLayoutParams.e.d)) {
        break label162;
      }
      if (!a(localLayoutParams.e)) {
        break label149;
      }
      return localView;
      i2 = -1;
      break;
      i4 = 0;
      i3 = i1 + 1;
      i1 = i4;
      break label59;
    }
    label149:
    localBitSet.clear(localLayoutParams.e.d);
    label162:
    if (localLayoutParams.f) {}
    label170:
    label247:
    label283:
    label338:
    label340:
    label345:
    label349:
    for (;;)
    {
      i5 += i4;
      break;
      if (i5 + i4 != i3)
      {
        Object localObject = i(i5 + i4);
        i1 = 0;
        int i7;
        if (this.c)
        {
          i6 = this.a.b(localView);
          i7 = this.a.b((View)localObject);
          if (i6 < i7) {
            break label122;
          }
          if (i6 == i7) {
            i1 = 1;
          }
          if (i1 == 0) {
            break label338;
          }
          localObject = (LayoutParams)((View)localObject).getLayoutParams();
          if (localLayoutParams.e.d - ((LayoutParams)localObject).e.d >= 0) {
            break label340;
          }
          i1 = 1;
          if (i2 >= 0) {
            break label345;
          }
        }
        for (int i6 = 1;; i6 = 0)
        {
          if (i1 == i6) {
            break label349;
          }
          return localView;
          i6 = this.a.a(localView);
          i7 = this.a.a((View)localObject);
          if (i6 > i7) {
            break;
          }
          if (i6 != i7) {
            break label247;
          }
          i1 = 1;
          break label247;
          break label170;
          i1 = 0;
          break label283;
        }
      }
    }
    label351:
    return null;
  }
  
  View b(boolean paramBoolean1, boolean paramBoolean2)
  {
    int i2 = this.a.c();
    int i3 = this.a.d();
    Object localObject1 = null;
    int i1 = u() - 1;
    if (i1 >= 0)
    {
      View localView = i(i1);
      int i4 = this.a.a(localView);
      int i5 = this.a.b(localView);
      Object localObject2 = localObject1;
      if (i5 > i2)
      {
        if (i4 < i3) {
          break label94;
        }
        localObject2 = localObject1;
      }
      for (;;)
      {
        i1 -= 1;
        localObject1 = localObject2;
        break;
        label94:
        if ((i5 <= i3) || (!paramBoolean1)) {
          return localView;
        }
        localObject2 = localObject1;
        if (paramBoolean2)
        {
          localObject2 = localObject1;
          if (localObject1 == null) {
            localObject2 = localView;
          }
        }
      }
    }
    return (View)localObject1;
  }
  
  public void b(int paramInt)
  {
    if ((paramInt != 0) && (paramInt != 1)) {
      throw new IllegalArgumentException("invalid orientation.");
    }
    a(null);
    if (paramInt == this.i) {
      return;
    }
    this.i = paramInt;
    OrientationHelper localOrientationHelper = this.a;
    this.a = this.b;
    this.b = localOrientationHelper;
    o();
  }
  
  public void b(RecyclerView paramRecyclerView, int paramInt1, int paramInt2)
  {
    c(paramInt1, paramInt2, 2);
  }
  
  boolean b(RecyclerView.State paramState, AnchorInfo paramAnchorInfo)
  {
    boolean bool = false;
    if ((paramState.a()) || (this.d == -1)) {
      return false;
    }
    if ((this.d < 0) || (this.d >= paramState.e()))
    {
      this.d = -1;
      this.e = Integer.MIN_VALUE;
      return false;
    }
    if ((this.u == null) || (this.u.a == -1) || (this.u.c < 1))
    {
      paramState = c(this.d);
      if (paramState != null)
      {
        if (this.c) {}
        for (int i1 = M();; i1 = N())
        {
          paramAnchorInfo.a = i1;
          if (this.e == Integer.MIN_VALUE) {
            break label188;
          }
          if (!paramAnchorInfo.c) {
            break;
          }
          paramAnchorInfo.b = (this.a.d() - this.e - this.a.b(paramState));
          return true;
        }
        paramAnchorInfo.b = (this.a.c() + this.e - this.a.a(paramState));
        return true;
        label188:
        if (this.a.c(paramState) > this.a.f())
        {
          if (paramAnchorInfo.c) {}
          for (i1 = this.a.d();; i1 = this.a.c())
          {
            paramAnchorInfo.b = i1;
            return true;
          }
        }
        i1 = this.a.a(paramState) - this.a.c();
        if (i1 < 0)
        {
          paramAnchorInfo.b = (-i1);
          return true;
        }
        i1 = this.a.d() - this.a.b(paramState);
        if (i1 < 0)
        {
          paramAnchorInfo.b = i1;
          return true;
        }
        paramAnchorInfo.b = Integer.MIN_VALUE;
        return true;
      }
      paramAnchorInfo.a = this.d;
      if (this.e == Integer.MIN_VALUE)
      {
        if (t(paramAnchorInfo.a) == 1) {
          bool = true;
        }
        paramAnchorInfo.c = bool;
        paramAnchorInfo.b();
      }
      for (;;)
      {
        paramAnchorInfo.d = true;
        return true;
        paramAnchorInfo.a(this.e);
      }
    }
    paramAnchorInfo.b = Integer.MIN_VALUE;
    paramAnchorInfo.a = this.d;
    return true;
  }
  
  int c(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    int i1;
    int i2;
    if (paramInt > 0)
    {
      i1 = 1;
      i2 = M();
      this.k.a = true;
      a(i2, paramState);
      f(i1);
      this.k.c = (this.k.d + i2);
      i2 = Math.abs(paramInt);
      this.k.b = i2;
      i1 = a(paramRecycler, this.k, paramState);
      if (i2 >= i1) {
        break label116;
      }
    }
    for (;;)
    {
      this.a.a(-paramInt);
      this.o = this.c;
      return paramInt;
      i1 = -1;
      i2 = N();
      break;
      label116:
      if (paramInt < 0) {
        paramInt = -i1;
      } else {
        paramInt = i1;
      }
    }
  }
  
  public int c(RecyclerView.State paramState)
  {
    return a(paramState);
  }
  
  public void c(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    a(paramRecycler, paramState, true);
  }
  
  public boolean c()
  {
    return this.u == null;
  }
  
  public int d(RecyclerView.State paramState)
  {
    return h(paramState);
  }
  
  public Parcelable d()
  {
    Object localObject;
    if (this.u != null) {
      localObject = new SavedState(this.u);
    }
    SavedState localSavedState;
    int i1;
    label130:
    int i2;
    label167:
    do
    {
      return (Parcelable)localObject;
      localSavedState = new SavedState();
      localSavedState.h = this.l;
      localSavedState.i = this.o;
      localSavedState.j = this.t;
      if ((this.f == null) || (this.f.a == null)) {
        break;
      }
      localSavedState.f = this.f.a;
      localSavedState.e = localSavedState.f.length;
      localSavedState.g = this.f.b;
      if (u() <= 0) {
        break label281;
      }
      if (!this.o) {
        break label240;
      }
      i1 = M();
      localSavedState.a = i1;
      localSavedState.b = i();
      localSavedState.c = this.g;
      localSavedState.d = new int[this.g];
      i2 = 0;
      localObject = localSavedState;
    } while (i2 >= this.g);
    int i3;
    if (this.o)
    {
      i3 = this.h[i2].b(Integer.MIN_VALUE);
      i1 = i3;
      if (i3 != Integer.MIN_VALUE) {
        i1 = i3 - this.a.d();
      }
    }
    for (;;)
    {
      localSavedState.d[i2] = i1;
      i2 += 1;
      break label167;
      localSavedState.e = 0;
      break;
      label240:
      i1 = N();
      break label130;
      i3 = this.h[i2].a(Integer.MIN_VALUE);
      i1 = i3;
      if (i3 != Integer.MIN_VALUE) {
        i1 = i3 - this.a.c();
      }
    }
    label281:
    localSavedState.a = -1;
    localSavedState.b = -1;
    localSavedState.c = 0;
    return localSavedState;
  }
  
  void d(int paramInt)
  {
    this.j = (paramInt / this.g);
    this.v = View.MeasureSpec.makeMeasureSpec(paramInt, this.b.h());
  }
  
  public int e(RecyclerView.State paramState)
  {
    return h(paramState);
  }
  
  public void e(int paramInt)
  {
    if ((this.u != null) && (this.u.a != paramInt)) {
      this.u.b();
    }
    this.d = paramInt;
    this.e = Integer.MIN_VALUE;
    o();
  }
  
  public boolean e()
  {
    return this.i == 0;
  }
  
  public int f(RecyclerView.State paramState)
  {
    return i(paramState);
  }
  
  public boolean f()
  {
    return this.i == 1;
  }
  
  public int g(RecyclerView.State paramState)
  {
    return i(paramState);
  }
  
  public void g()
  {
    this.f.a();
    o();
  }
  
  boolean h()
  {
    return s() == 1;
  }
  
  int i()
  {
    if (this.c) {}
    for (View localView = b(true, true); localView == null; localView = a(true, true)) {
      return -1;
    }
    return d(localView);
  }
  
  public void j(int paramInt)
  {
    super.j(paramInt);
    int i1 = 0;
    while (i1 < this.g)
    {
      this.h[i1].d(paramInt);
      i1 += 1;
    }
  }
  
  boolean j()
  {
    int i2 = this.h[0].b(Integer.MIN_VALUE);
    int i1 = 1;
    while (i1 < this.g)
    {
      if (this.h[i1].b(Integer.MIN_VALUE) != i2) {
        return false;
      }
      i1 += 1;
    }
    return true;
  }
  
  public void k(int paramInt)
  {
    super.k(paramInt);
    int i1 = 0;
    while (i1 < this.g)
    {
      this.h[i1].d(paramInt);
      i1 += 1;
    }
  }
  
  boolean k()
  {
    int i2 = this.h[0].a(Integer.MIN_VALUE);
    int i1 = 1;
    while (i1 < this.g)
    {
      if (this.h[i1].a(Integer.MIN_VALUE) != i2) {
        return false;
      }
      i1 += 1;
    }
    return true;
  }
  
  public void l(int paramInt)
  {
    if (paramInt == 0) {
      n();
    }
  }
  
  private class AnchorInfo
  {
    int a;
    int b;
    boolean c;
    boolean d;
    
    private AnchorInfo() {}
    
    void a()
    {
      this.a = -1;
      this.b = Integer.MIN_VALUE;
      this.c = false;
      this.d = false;
    }
    
    void a(int paramInt)
    {
      if (this.c)
      {
        this.b = (StaggeredGridLayoutManager.this.a.d() - paramInt);
        return;
      }
      this.b = (StaggeredGridLayoutManager.this.a.c() + paramInt);
    }
    
    void b()
    {
      if (this.c) {}
      for (int i = StaggeredGridLayoutManager.this.a.d();; i = StaggeredGridLayoutManager.this.a.c())
      {
        this.b = i;
        return;
      }
    }
  }
  
  public static class LayoutParams
    extends RecyclerView.LayoutParams
  {
    StaggeredGridLayoutManager.Span e;
    boolean f;
    
    public LayoutParams(int paramInt1, int paramInt2)
    {
      super(paramInt2);
    }
    
    public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
    }
    
    public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
    }
    
    public LayoutParams(ViewGroup.MarginLayoutParams paramMarginLayoutParams)
    {
      super();
    }
    
    public boolean a()
    {
      return this.f;
    }
    
    public final int b()
    {
      if (this.e == null) {
        return -1;
      }
      return this.e.d;
    }
  }
  
  static class LazySpanLookup
  {
    int[] a;
    List<FullSpanItem> b;
    
    private void c(int paramInt1, int paramInt2)
    {
      if (this.b == null) {
        return;
      }
      int i = this.b.size() - 1;
      label20:
      FullSpanItem localFullSpanItem;
      if (i >= 0)
      {
        localFullSpanItem = (FullSpanItem)this.b.get(i);
        if (localFullSpanItem.a >= paramInt1) {
          break label55;
        }
      }
      for (;;)
      {
        i -= 1;
        break label20;
        break;
        label55:
        if (localFullSpanItem.a < paramInt1 + paramInt2) {
          this.b.remove(i);
        } else {
          localFullSpanItem.a -= paramInt2;
        }
      }
    }
    
    private void d(int paramInt1, int paramInt2)
    {
      if (this.b == null) {
        return;
      }
      int i = this.b.size() - 1;
      label20:
      FullSpanItem localFullSpanItem;
      if (i >= 0)
      {
        localFullSpanItem = (FullSpanItem)this.b.get(i);
        if (localFullSpanItem.a >= paramInt1) {
          break label55;
        }
      }
      for (;;)
      {
        i -= 1;
        break label20;
        break;
        label55:
        localFullSpanItem.a += paramInt2;
      }
    }
    
    private int g(int paramInt)
    {
      if (this.b == null) {
        return -1;
      }
      FullSpanItem localFullSpanItem = f(paramInt);
      if (localFullSpanItem != null) {
        this.b.remove(localFullSpanItem);
      }
      int k = -1;
      int m = this.b.size();
      int i = 0;
      for (;;)
      {
        int j = k;
        if (i < m)
        {
          if (((FullSpanItem)this.b.get(i)).a >= paramInt) {
            j = i;
          }
        }
        else
        {
          if (j == -1) {
            break;
          }
          localFullSpanItem = (FullSpanItem)this.b.get(j);
          this.b.remove(j);
          return localFullSpanItem.a;
        }
        i += 1;
      }
    }
    
    int a(int paramInt)
    {
      if (this.b != null)
      {
        int i = this.b.size() - 1;
        while (i >= 0)
        {
          if (((FullSpanItem)this.b.get(i)).a >= paramInt) {
            this.b.remove(i);
          }
          i -= 1;
        }
      }
      return b(paramInt);
    }
    
    public FullSpanItem a(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    {
      Object localObject;
      if (this.b == null)
      {
        localObject = null;
        return (FullSpanItem)localObject;
      }
      int j = this.b.size();
      int i = 0;
      for (;;)
      {
        if (i >= j) {
          break label117;
        }
        FullSpanItem localFullSpanItem = (FullSpanItem)this.b.get(i);
        if (localFullSpanItem.a >= paramInt2) {
          return null;
        }
        if (localFullSpanItem.a >= paramInt1)
        {
          localObject = localFullSpanItem;
          if (paramInt3 == 0) {
            break;
          }
          localObject = localFullSpanItem;
          if (localFullSpanItem.b == paramInt3) {
            break;
          }
          if (paramBoolean)
          {
            localObject = localFullSpanItem;
            if (localFullSpanItem.d) {
              break;
            }
          }
        }
        i += 1;
      }
      label117:
      return null;
    }
    
    void a()
    {
      if (this.a != null) {
        Arrays.fill(this.a, -1);
      }
      this.b = null;
    }
    
    void a(int paramInt1, int paramInt2)
    {
      if ((this.a == null) || (paramInt1 >= this.a.length)) {
        return;
      }
      e(paramInt1 + paramInt2);
      System.arraycopy(this.a, paramInt1 + paramInt2, this.a, paramInt1, this.a.length - paramInt1 - paramInt2);
      Arrays.fill(this.a, this.a.length - paramInt2, this.a.length, -1);
      c(paramInt1, paramInt2);
    }
    
    void a(int paramInt, StaggeredGridLayoutManager.Span paramSpan)
    {
      e(paramInt);
      this.a[paramInt] = paramSpan.d;
    }
    
    public void a(FullSpanItem paramFullSpanItem)
    {
      if (this.b == null) {
        this.b = new ArrayList();
      }
      int j = this.b.size();
      int i = 0;
      while (i < j)
      {
        FullSpanItem localFullSpanItem = (FullSpanItem)this.b.get(i);
        if (localFullSpanItem.a == paramFullSpanItem.a) {
          this.b.remove(i);
        }
        if (localFullSpanItem.a >= paramFullSpanItem.a)
        {
          this.b.add(i, paramFullSpanItem);
          return;
        }
        i += 1;
      }
      this.b.add(paramFullSpanItem);
    }
    
    int b(int paramInt)
    {
      if (this.a == null) {}
      while (paramInt >= this.a.length) {
        return -1;
      }
      int i = g(paramInt);
      if (i == -1)
      {
        Arrays.fill(this.a, paramInt, this.a.length, -1);
        return this.a.length;
      }
      Arrays.fill(this.a, paramInt, i + 1, -1);
      return i + 1;
    }
    
    void b(int paramInt1, int paramInt2)
    {
      if ((this.a == null) || (paramInt1 >= this.a.length)) {
        return;
      }
      e(paramInt1 + paramInt2);
      System.arraycopy(this.a, paramInt1, this.a, paramInt1 + paramInt2, this.a.length - paramInt1 - paramInt2);
      Arrays.fill(this.a, paramInt1, paramInt1 + paramInt2, -1);
      d(paramInt1, paramInt2);
    }
    
    int c(int paramInt)
    {
      if ((this.a == null) || (paramInt >= this.a.length)) {
        return -1;
      }
      return this.a[paramInt];
    }
    
    int d(int paramInt)
    {
      int i = this.a.length;
      while (i <= paramInt) {
        i *= 2;
      }
      return i;
    }
    
    void e(int paramInt)
    {
      if (this.a == null)
      {
        this.a = new int[Math.max(paramInt, 10) + 1];
        Arrays.fill(this.a, -1);
      }
      while (paramInt < this.a.length) {
        return;
      }
      int[] arrayOfInt = this.a;
      this.a = new int[d(paramInt)];
      System.arraycopy(arrayOfInt, 0, this.a, 0, arrayOfInt.length);
      Arrays.fill(this.a, arrayOfInt.length, this.a.length, -1);
    }
    
    public FullSpanItem f(int paramInt)
    {
      Object localObject;
      if (this.b == null)
      {
        localObject = null;
        return (FullSpanItem)localObject;
      }
      int i = this.b.size() - 1;
      for (;;)
      {
        if (i < 0) {
          break label61;
        }
        FullSpanItem localFullSpanItem = (FullSpanItem)this.b.get(i);
        localObject = localFullSpanItem;
        if (localFullSpanItem.a == paramInt) {
          break;
        }
        i -= 1;
      }
      label61:
      return null;
    }
    
    static class FullSpanItem
      implements Parcelable
    {
      public static final Parcelable.Creator<FullSpanItem> CREATOR = new Parcelable.Creator()
      {
        public StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem a(Parcel paramAnonymousParcel)
        {
          return new StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem(paramAnonymousParcel);
        }
        
        public StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem[] a(int paramAnonymousInt)
        {
          return new StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem[paramAnonymousInt];
        }
      };
      int a;
      int b;
      int[] c;
      boolean d;
      
      public FullSpanItem() {}
      
      public FullSpanItem(Parcel paramParcel)
      {
        this.a = paramParcel.readInt();
        this.b = paramParcel.readInt();
        if (paramParcel.readInt() == 1) {}
        for (;;)
        {
          this.d = bool;
          int i = paramParcel.readInt();
          if (i > 0)
          {
            this.c = new int[i];
            paramParcel.readIntArray(this.c);
          }
          return;
          bool = false;
        }
      }
      
      int a(int paramInt)
      {
        if (this.c == null) {
          return 0;
        }
        return this.c[paramInt];
      }
      
      public int describeContents()
      {
        return 0;
      }
      
      public String toString()
      {
        return "FullSpanItem{mPosition=" + this.a + ", mGapDir=" + this.b + ", mHasUnwantedGapAfter=" + this.d + ", mGapPerSpan=" + Arrays.toString(this.c) + '}';
      }
      
      public void writeToParcel(Parcel paramParcel, int paramInt)
      {
        paramParcel.writeInt(this.a);
        paramParcel.writeInt(this.b);
        if (this.d) {}
        for (paramInt = 1;; paramInt = 0)
        {
          paramParcel.writeInt(paramInt);
          if ((this.c == null) || (this.c.length <= 0)) {
            break;
          }
          paramParcel.writeInt(this.c.length);
          paramParcel.writeIntArray(this.c);
          return;
        }
        paramParcel.writeInt(0);
      }
    }
  }
  
  public static class SavedState
    implements Parcelable
  {
    public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
    {
      public StaggeredGridLayoutManager.SavedState a(Parcel paramAnonymousParcel)
      {
        return new StaggeredGridLayoutManager.SavedState(paramAnonymousParcel);
      }
      
      public StaggeredGridLayoutManager.SavedState[] a(int paramAnonymousInt)
      {
        return new StaggeredGridLayoutManager.SavedState[paramAnonymousInt];
      }
    };
    int a;
    int b;
    int c;
    int[] d;
    int e;
    int[] f;
    List<StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem> g;
    boolean h;
    boolean i;
    boolean j;
    
    public SavedState() {}
    
    SavedState(Parcel paramParcel)
    {
      this.a = paramParcel.readInt();
      this.b = paramParcel.readInt();
      this.c = paramParcel.readInt();
      if (this.c > 0)
      {
        this.d = new int[this.c];
        paramParcel.readIntArray(this.d);
      }
      this.e = paramParcel.readInt();
      if (this.e > 0)
      {
        this.f = new int[this.e];
        paramParcel.readIntArray(this.f);
      }
      if (paramParcel.readInt() == 1)
      {
        bool1 = true;
        this.h = bool1;
        if (paramParcel.readInt() != 1) {
          break label152;
        }
        bool1 = true;
        label113:
        this.i = bool1;
        if (paramParcel.readInt() != 1) {
          break label157;
        }
      }
      label152:
      label157:
      for (boolean bool1 = bool2;; bool1 = false)
      {
        this.j = bool1;
        this.g = paramParcel.readArrayList(StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem.class.getClassLoader());
        return;
        bool1 = false;
        break;
        bool1 = false;
        break label113;
      }
    }
    
    public SavedState(SavedState paramSavedState)
    {
      this.c = paramSavedState.c;
      this.a = paramSavedState.a;
      this.b = paramSavedState.b;
      this.d = paramSavedState.d;
      this.e = paramSavedState.e;
      this.f = paramSavedState.f;
      this.h = paramSavedState.h;
      this.i = paramSavedState.i;
      this.j = paramSavedState.j;
      this.g = paramSavedState.g;
    }
    
    void a()
    {
      this.d = null;
      this.c = 0;
      this.e = 0;
      this.f = null;
      this.g = null;
    }
    
    void b()
    {
      this.d = null;
      this.c = 0;
      this.a = -1;
      this.b = -1;
    }
    
    public int describeContents()
    {
      return 0;
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      int k = 1;
      paramParcel.writeInt(this.a);
      paramParcel.writeInt(this.b);
      paramParcel.writeInt(this.c);
      if (this.c > 0) {
        paramParcel.writeIntArray(this.d);
      }
      paramParcel.writeInt(this.e);
      if (this.e > 0) {
        paramParcel.writeIntArray(this.f);
      }
      if (this.h)
      {
        paramInt = 1;
        paramParcel.writeInt(paramInt);
        if (!this.i) {
          break label120;
        }
        paramInt = 1;
        label87:
        paramParcel.writeInt(paramInt);
        if (!this.j) {
          break label125;
        }
      }
      label120:
      label125:
      for (paramInt = k;; paramInt = 0)
      {
        paramParcel.writeInt(paramInt);
        paramParcel.writeList(this.g);
        return;
        paramInt = 0;
        break;
        paramInt = 0;
        break label87;
      }
    }
  }
  
  class Span
  {
    int a = Integer.MIN_VALUE;
    int b = Integer.MIN_VALUE;
    int c = 0;
    final int d;
    private ArrayList<View> f = new ArrayList();
    
    private Span(int paramInt)
    {
      this.d = paramInt;
    }
    
    int a(int paramInt)
    {
      if (this.a != Integer.MIN_VALUE) {
        paramInt = this.a;
      }
      while (this.f.size() == 0) {
        return paramInt;
      }
      a();
      return this.a;
    }
    
    int a(int paramInt1, int paramInt2, boolean paramBoolean)
    {
      int k = -1;
      int m = StaggeredGridLayoutManager.this.a.c();
      int n = StaggeredGridLayoutManager.this.a.d();
      int i;
      if (paramInt2 > paramInt1) {
        i = 1;
      }
      for (;;)
      {
        int j = k;
        View localView;
        if (paramInt1 != paramInt2)
        {
          localView = (View)this.f.get(paramInt1);
          j = StaggeredGridLayoutManager.this.a.a(localView);
          int i1 = StaggeredGridLayoutManager.this.a.b(localView);
          if ((j >= n) || (i1 <= m)) {
            break label147;
          }
          if (paramBoolean)
          {
            if ((j < m) || (i1 > n)) {
              break label147;
            }
            j = StaggeredGridLayoutManager.this.d(localView);
          }
        }
        else
        {
          return j;
          i = -1;
          continue;
        }
        return StaggeredGridLayoutManager.this.d(localView);
        label147:
        paramInt1 += i;
      }
    }
    
    public View a(int paramInt1, int paramInt2)
    {
      Object localObject2 = null;
      Object localObject1 = null;
      int i;
      View localView;
      if (paramInt2 == -1)
      {
        i = this.f.size();
        paramInt2 = 0;
        localObject2 = localObject1;
        if (paramInt2 < i)
        {
          localView = (View)this.f.get(paramInt2);
          localObject2 = localObject1;
          if (localView.isFocusable())
          {
            if (StaggeredGridLayoutManager.this.d(localView) > paramInt1) {}
            for (int k = 1;; k = 0)
            {
              localObject2 = localObject1;
              if (k != StaggeredGridLayoutManager.c(StaggeredGridLayoutManager.this)) {
                break label211;
              }
              localObject1 = localView;
              paramInt2 += 1;
              break;
            }
          }
        }
      }
      else
      {
        paramInt2 = this.f.size() - 1;
        localObject1 = localObject2;
        localObject2 = localObject1;
        if (paramInt2 >= 0)
        {
          localView = (View)this.f.get(paramInt2);
          localObject2 = localObject1;
          if (localView.isFocusable())
          {
            if (StaggeredGridLayoutManager.this.d(localView) > paramInt1)
            {
              i = 1;
              label166:
              if (StaggeredGridLayoutManager.c(StaggeredGridLayoutManager.this)) {
                break label205;
              }
            }
            label205:
            for (int j = 1;; j = 0)
            {
              localObject2 = localObject1;
              if (i != j) {
                break label211;
              }
              localObject1 = localView;
              paramInt2 -= 1;
              break;
              i = 0;
              break label166;
            }
          }
        }
      }
      label211:
      return (View)localObject2;
    }
    
    void a()
    {
      Object localObject = (View)this.f.get(0);
      StaggeredGridLayoutManager.LayoutParams localLayoutParams = c((View)localObject);
      this.a = StaggeredGridLayoutManager.this.a.a((View)localObject);
      if (localLayoutParams.f)
      {
        localObject = StaggeredGridLayoutManager.this.f.f(localLayoutParams.e());
        if ((localObject != null) && (((StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem)localObject).b == -1)) {
          this.a -= ((StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem)localObject).a(this.d);
        }
      }
    }
    
    void a(View paramView)
    {
      StaggeredGridLayoutManager.LayoutParams localLayoutParams = c(paramView);
      localLayoutParams.e = this;
      this.f.add(0, paramView);
      this.a = Integer.MIN_VALUE;
      if (this.f.size() == 1) {
        this.b = Integer.MIN_VALUE;
      }
      if ((localLayoutParams.c()) || (localLayoutParams.d())) {
        this.c += StaggeredGridLayoutManager.this.a.c(paramView);
      }
    }
    
    void a(boolean paramBoolean, int paramInt)
    {
      int i;
      if (paramBoolean)
      {
        i = b(Integer.MIN_VALUE);
        e();
        if (i != Integer.MIN_VALUE) {
          break label32;
        }
      }
      label32:
      while (((paramBoolean) && (i < StaggeredGridLayoutManager.this.a.d())) || ((!paramBoolean) && (i > StaggeredGridLayoutManager.this.a.c())))
      {
        return;
        i = a(Integer.MIN_VALUE);
        break;
      }
      int j = i;
      if (paramInt != Integer.MIN_VALUE) {
        j = i + paramInt;
      }
      this.b = j;
      this.a = j;
    }
    
    int b()
    {
      if (this.a != Integer.MIN_VALUE) {
        return this.a;
      }
      a();
      return this.a;
    }
    
    int b(int paramInt)
    {
      if (this.b != Integer.MIN_VALUE) {
        paramInt = this.b;
      }
      while (this.f.size() == 0) {
        return paramInt;
      }
      c();
      return this.b;
    }
    
    void b(View paramView)
    {
      StaggeredGridLayoutManager.LayoutParams localLayoutParams = c(paramView);
      localLayoutParams.e = this;
      this.f.add(paramView);
      this.b = Integer.MIN_VALUE;
      if (this.f.size() == 1) {
        this.a = Integer.MIN_VALUE;
      }
      if ((localLayoutParams.c()) || (localLayoutParams.d())) {
        this.c += StaggeredGridLayoutManager.this.a.c(paramView);
      }
    }
    
    StaggeredGridLayoutManager.LayoutParams c(View paramView)
    {
      return (StaggeredGridLayoutManager.LayoutParams)paramView.getLayoutParams();
    }
    
    void c()
    {
      Object localObject = (View)this.f.get(this.f.size() - 1);
      StaggeredGridLayoutManager.LayoutParams localLayoutParams = c((View)localObject);
      this.b = StaggeredGridLayoutManager.this.a.b((View)localObject);
      if (localLayoutParams.f)
      {
        localObject = StaggeredGridLayoutManager.this.f.f(localLayoutParams.e());
        if ((localObject != null) && (((StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem)localObject).b == 1)) {
          this.b += ((StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem)localObject).a(this.d);
        }
      }
    }
    
    void c(int paramInt)
    {
      this.a = paramInt;
      this.b = paramInt;
    }
    
    int d()
    {
      if (this.b != Integer.MIN_VALUE) {
        return this.b;
      }
      c();
      return this.b;
    }
    
    void d(int paramInt)
    {
      if (this.a != Integer.MIN_VALUE) {
        this.a += paramInt;
      }
      if (this.b != Integer.MIN_VALUE) {
        this.b += paramInt;
      }
    }
    
    void e()
    {
      this.f.clear();
      f();
      this.c = 0;
    }
    
    void f()
    {
      this.a = Integer.MIN_VALUE;
      this.b = Integer.MIN_VALUE;
    }
    
    void g()
    {
      int i = this.f.size();
      View localView = (View)this.f.remove(i - 1);
      StaggeredGridLayoutManager.LayoutParams localLayoutParams = c(localView);
      localLayoutParams.e = null;
      if ((localLayoutParams.c()) || (localLayoutParams.d())) {
        this.c -= StaggeredGridLayoutManager.this.a.c(localView);
      }
      if (i == 1) {
        this.a = Integer.MIN_VALUE;
      }
      this.b = Integer.MIN_VALUE;
    }
    
    void h()
    {
      View localView = (View)this.f.remove(0);
      StaggeredGridLayoutManager.LayoutParams localLayoutParams = c(localView);
      localLayoutParams.e = null;
      if (this.f.size() == 0) {
        this.b = Integer.MIN_VALUE;
      }
      if ((localLayoutParams.c()) || (localLayoutParams.d())) {
        this.c -= StaggeredGridLayoutManager.this.a.c(localView);
      }
      this.a = Integer.MIN_VALUE;
    }
    
    public int i()
    {
      return this.c;
    }
    
    public int j()
    {
      if (StaggeredGridLayoutManager.c(StaggeredGridLayoutManager.this)) {
        return a(0, this.f.size(), false);
      }
      return a(this.f.size() - 1, -1, false);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/StaggeredGridLayoutManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */