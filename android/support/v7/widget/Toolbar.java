package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.annotation.StyleRes;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MarginLayoutParamsCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar.LayoutParams;
import android.support.v7.appcompat.R.attr;
import android.support.v7.appcompat.R.styleable;
import android.support.v7.view.CollapsibleActionView;
import android.support.v7.view.SupportMenuInflater;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuBuilder.Callback;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.MenuPresenter;
import android.support.v7.view.menu.MenuPresenter.Callback;
import android.support.v7.view.menu.SubMenuBuilder;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class Toolbar
  extends ViewGroup
{
  private boolean A;
  private final ArrayList<View> B = new ArrayList();
  private final ArrayList<View> C = new ArrayList();
  private final int[] D = new int[2];
  private OnMenuItemClickListener E;
  private final ActionMenuView.OnMenuItemClickListener F = new ActionMenuView.OnMenuItemClickListener()
  {
    public boolean a(MenuItem paramAnonymousMenuItem)
    {
      if (Toolbar.a(Toolbar.this) != null) {
        return Toolbar.a(Toolbar.this).a(paramAnonymousMenuItem);
      }
      return false;
    }
  };
  private ToolbarWidgetWrapper G;
  private ActionMenuPresenter H;
  private ExpandedActionViewMenuPresenter I;
  private MenuPresenter.Callback J;
  private MenuBuilder.Callback K;
  private boolean L;
  private final Runnable M = new Runnable()
  {
    public void run()
    {
      Toolbar.this.d();
    }
  };
  private final AppCompatDrawableManager N;
  View a;
  private ActionMenuView b;
  private TextView c;
  private TextView d;
  private ImageButton e;
  private ImageView f;
  private Drawable g;
  private CharSequence h;
  private ImageButton i;
  private Context j;
  private int k;
  private int l;
  private int m;
  private int n;
  private int o;
  private int p;
  private int q;
  private int r;
  private int s;
  private final RtlSpacingHelper t = new RtlSpacingHelper();
  private int u = 8388627;
  private CharSequence v;
  private CharSequence w;
  private int x;
  private int y;
  private boolean z;
  
  public Toolbar(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public Toolbar(Context paramContext, @Nullable AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, R.attr.toolbarStyle);
  }
  
  public Toolbar(Context paramContext, @Nullable AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    paramContext = TintTypedArray.a(getContext(), paramAttributeSet, R.styleable.Toolbar, paramInt, 0);
    this.l = paramContext.g(R.styleable.Toolbar_titleTextAppearance, 0);
    this.m = paramContext.g(R.styleable.Toolbar_subtitleTextAppearance, 0);
    this.u = paramContext.c(R.styleable.Toolbar_android_gravity, this.u);
    this.n = 48;
    paramInt = paramContext.d(R.styleable.Toolbar_titleMargins, 0);
    this.s = paramInt;
    this.r = paramInt;
    this.q = paramInt;
    this.p = paramInt;
    paramInt = paramContext.d(R.styleable.Toolbar_titleMarginStart, -1);
    if (paramInt >= 0) {
      this.p = paramInt;
    }
    paramInt = paramContext.d(R.styleable.Toolbar_titleMarginEnd, -1);
    if (paramInt >= 0) {
      this.q = paramInt;
    }
    paramInt = paramContext.d(R.styleable.Toolbar_titleMarginTop, -1);
    if (paramInt >= 0) {
      this.r = paramInt;
    }
    paramInt = paramContext.d(R.styleable.Toolbar_titleMarginBottom, -1);
    if (paramInt >= 0) {
      this.s = paramInt;
    }
    this.o = paramContext.e(R.styleable.Toolbar_maxButtonHeight, -1);
    paramInt = paramContext.d(R.styleable.Toolbar_contentInsetStart, Integer.MIN_VALUE);
    int i1 = paramContext.d(R.styleable.Toolbar_contentInsetEnd, Integer.MIN_VALUE);
    int i2 = paramContext.e(R.styleable.Toolbar_contentInsetLeft, 0);
    int i3 = paramContext.e(R.styleable.Toolbar_contentInsetRight, 0);
    this.t.b(i2, i3);
    if ((paramInt != Integer.MIN_VALUE) || (i1 != Integer.MIN_VALUE)) {
      this.t.a(paramInt, i1);
    }
    this.g = paramContext.a(R.styleable.Toolbar_collapseIcon);
    this.h = paramContext.c(R.styleable.Toolbar_collapseContentDescription);
    paramAttributeSet = paramContext.c(R.styleable.Toolbar_title);
    if (!TextUtils.isEmpty(paramAttributeSet)) {
      setTitle(paramAttributeSet);
    }
    paramAttributeSet = paramContext.c(R.styleable.Toolbar_subtitle);
    if (!TextUtils.isEmpty(paramAttributeSet)) {
      setSubtitle(paramAttributeSet);
    }
    this.j = getContext();
    setPopupTheme(paramContext.g(R.styleable.Toolbar_popupTheme, 0));
    paramAttributeSet = paramContext.a(R.styleable.Toolbar_navigationIcon);
    if (paramAttributeSet != null) {
      setNavigationIcon(paramAttributeSet);
    }
    paramAttributeSet = paramContext.c(R.styleable.Toolbar_navigationContentDescription);
    if (!TextUtils.isEmpty(paramAttributeSet)) {
      setNavigationContentDescription(paramAttributeSet);
    }
    paramAttributeSet = paramContext.a(R.styleable.Toolbar_logo);
    if (paramAttributeSet != null) {
      setLogo(paramAttributeSet);
    }
    paramAttributeSet = paramContext.c(R.styleable.Toolbar_logoDescription);
    if (!TextUtils.isEmpty(paramAttributeSet)) {
      setLogoDescription(paramAttributeSet);
    }
    if (paramContext.f(R.styleable.Toolbar_titleTextColor)) {
      setTitleTextColor(paramContext.b(R.styleable.Toolbar_titleTextColor, -1));
    }
    if (paramContext.f(R.styleable.Toolbar_subtitleTextColor)) {
      setSubtitleTextColor(paramContext.b(R.styleable.Toolbar_subtitleTextColor, -1));
    }
    paramContext.a();
    this.N = AppCompatDrawableManager.a();
  }
  
  private int a(int paramInt)
  {
    int i1 = paramInt & 0x70;
    paramInt = i1;
    switch (i1)
    {
    default: 
      paramInt = this.u & 0x70;
    }
    return paramInt;
  }
  
  private int a(View paramView, int paramInt)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    int i2 = paramView.getMeasuredHeight();
    int i3;
    int i4;
    int i1;
    if (paramInt > 0)
    {
      paramInt = (i2 - paramInt) / 2;
      switch (a(localLayoutParams.a))
      {
      default: 
        i3 = getPaddingTop();
        paramInt = getPaddingBottom();
        i4 = getHeight();
        i1 = (i4 - i3 - paramInt - i2) / 2;
        if (i1 < localLayoutParams.topMargin) {
          paramInt = localLayoutParams.topMargin;
        }
        break;
      }
    }
    for (;;)
    {
      return i3 + paramInt;
      paramInt = 0;
      break;
      return getPaddingTop() - paramInt;
      return getHeight() - getPaddingBottom() - i2 - localLayoutParams.bottomMargin - paramInt;
      i2 = i4 - paramInt - i2 - i1 - i3;
      paramInt = i1;
      if (i2 < localLayoutParams.bottomMargin) {
        paramInt = Math.max(0, i1 - (localLayoutParams.bottomMargin - i2));
      }
    }
  }
  
  private int a(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[] paramArrayOfInt)
  {
    ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)paramView.getLayoutParams();
    int i1 = localMarginLayoutParams.leftMargin - paramArrayOfInt[0];
    int i2 = localMarginLayoutParams.rightMargin - paramArrayOfInt[1];
    int i3 = Math.max(0, i1) + Math.max(0, i2);
    paramArrayOfInt[0] = Math.max(0, -i1);
    paramArrayOfInt[1] = Math.max(0, -i2);
    paramView.measure(getChildMeasureSpec(paramInt1, getPaddingLeft() + getPaddingRight() + i3 + paramInt2, localMarginLayoutParams.width), getChildMeasureSpec(paramInt3, getPaddingTop() + getPaddingBottom() + localMarginLayoutParams.topMargin + localMarginLayoutParams.bottomMargin + paramInt4, localMarginLayoutParams.height));
    return paramView.getMeasuredWidth() + i3;
  }
  
  private int a(View paramView, int paramInt1, int[] paramArrayOfInt, int paramInt2)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    int i1 = localLayoutParams.leftMargin - paramArrayOfInt[0];
    paramInt1 += Math.max(0, i1);
    paramArrayOfInt[0] = Math.max(0, -i1);
    paramInt2 = a(paramView, paramInt2);
    i1 = paramView.getMeasuredWidth();
    paramView.layout(paramInt1, paramInt2, paramInt1 + i1, paramView.getMeasuredHeight() + paramInt2);
    return paramInt1 + (localLayoutParams.rightMargin + i1);
  }
  
  private int a(List<View> paramList, int[] paramArrayOfInt)
  {
    int i4 = paramArrayOfInt[0];
    int i3 = paramArrayOfInt[1];
    int i2 = 0;
    int i5 = paramList.size();
    int i1 = 0;
    while (i1 < i5)
    {
      paramArrayOfInt = (View)paramList.get(i1);
      LayoutParams localLayoutParams = (LayoutParams)paramArrayOfInt.getLayoutParams();
      i4 = localLayoutParams.leftMargin - i4;
      i3 = localLayoutParams.rightMargin - i3;
      int i6 = Math.max(0, i4);
      int i7 = Math.max(0, i3);
      i4 = Math.max(0, -i4);
      i3 = Math.max(0, -i3);
      i2 += paramArrayOfInt.getMeasuredWidth() + i6 + i7;
      i1 += 1;
    }
    return i2;
  }
  
  private void a(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)paramView.getLayoutParams();
    int i1 = getChildMeasureSpec(paramInt1, getPaddingLeft() + getPaddingRight() + localMarginLayoutParams.leftMargin + localMarginLayoutParams.rightMargin + paramInt2, localMarginLayoutParams.width);
    paramInt2 = getChildMeasureSpec(paramInt3, getPaddingTop() + getPaddingBottom() + localMarginLayoutParams.topMargin + localMarginLayoutParams.bottomMargin + paramInt4, localMarginLayoutParams.height);
    paramInt3 = View.MeasureSpec.getMode(paramInt2);
    paramInt1 = paramInt2;
    if (paramInt3 != 1073741824)
    {
      paramInt1 = paramInt2;
      if (paramInt5 >= 0) {
        if (paramInt3 == 0) {
          break label132;
        }
      }
    }
    label132:
    for (paramInt1 = Math.min(View.MeasureSpec.getSize(paramInt2), paramInt5);; paramInt1 = paramInt5)
    {
      paramInt1 = View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824);
      paramView.measure(i1, paramInt1);
      return;
    }
  }
  
  private void a(View paramView, boolean paramBoolean)
  {
    Object localObject = paramView.getLayoutParams();
    if (localObject == null) {
      localObject = i();
    }
    for (;;)
    {
      ((LayoutParams)localObject).b = 1;
      if ((!paramBoolean) || (this.a == null)) {
        break;
      }
      paramView.setLayoutParams((ViewGroup.LayoutParams)localObject);
      this.C.add(paramView);
      return;
      if (!checkLayoutParams((ViewGroup.LayoutParams)localObject)) {
        localObject = a((ViewGroup.LayoutParams)localObject);
      } else {
        localObject = (LayoutParams)localObject;
      }
    }
    addView(paramView, (ViewGroup.LayoutParams)localObject);
  }
  
  private void a(List<View> paramList, int paramInt)
  {
    int i1 = 1;
    if (ViewCompat.h(this) == 1) {}
    int i3;
    int i2;
    View localView;
    LayoutParams localLayoutParams;
    for (;;)
    {
      i3 = getChildCount();
      i2 = GravityCompat.a(paramInt, ViewCompat.h(this));
      paramList.clear();
      if (i1 == 0) {
        break;
      }
      paramInt = i3 - 1;
      while (paramInt >= 0)
      {
        localView = getChildAt(paramInt);
        localLayoutParams = (LayoutParams)localView.getLayoutParams();
        if ((localLayoutParams.b == 0) && (a(localView)) && (b(localLayoutParams.a) == i2)) {
          paramList.add(localView);
        }
        paramInt -= 1;
      }
      i1 = 0;
    }
    paramInt = 0;
    while (paramInt < i3)
    {
      localView = getChildAt(paramInt);
      localLayoutParams = (LayoutParams)localView.getLayoutParams();
      if ((localLayoutParams.b == 0) && (a(localView)) && (b(localLayoutParams.a) == i2)) {
        paramList.add(localView);
      }
      paramInt += 1;
    }
  }
  
  private boolean a(View paramView)
  {
    return (paramView != null) && (paramView.getParent() == this) && (paramView.getVisibility() != 8);
  }
  
  private int b(int paramInt)
  {
    int i2 = ViewCompat.h(this);
    int i1 = GravityCompat.a(paramInt, i2) & 0x7;
    paramInt = i1;
    switch (i1)
    {
    case 2: 
    case 4: 
    default: 
      if (i2 != 1) {
        break;
      }
    }
    for (paramInt = 5;; paramInt = 3) {
      return paramInt;
    }
  }
  
  private int b(View paramView)
  {
    paramView = (ViewGroup.MarginLayoutParams)paramView.getLayoutParams();
    return MarginLayoutParamsCompat.a(paramView) + MarginLayoutParamsCompat.b(paramView);
  }
  
  private int b(View paramView, int paramInt1, int[] paramArrayOfInt, int paramInt2)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    int i1 = localLayoutParams.rightMargin - paramArrayOfInt[1];
    paramInt1 -= Math.max(0, i1);
    paramArrayOfInt[1] = Math.max(0, -i1);
    paramInt2 = a(paramView, paramInt2);
    i1 = paramView.getMeasuredWidth();
    paramView.layout(paramInt1 - i1, paramInt2, paramInt1, paramView.getMeasuredHeight() + paramInt2);
    return paramInt1 - (localLayoutParams.leftMargin + i1);
  }
  
  private int c(View paramView)
  {
    paramView = (ViewGroup.MarginLayoutParams)paramView.getLayoutParams();
    return paramView.topMargin + paramView.bottomMargin;
  }
  
  private boolean d(View paramView)
  {
    return (paramView.getParent() == this) || (this.C.contains(paramView));
  }
  
  private MenuInflater getMenuInflater()
  {
    return new SupportMenuInflater(getContext());
  }
  
  private void l()
  {
    if (this.f == null) {
      this.f = new ImageView(getContext());
    }
  }
  
  private void m()
  {
    n();
    if (this.b.d() == null)
    {
      MenuBuilder localMenuBuilder = (MenuBuilder)this.b.getMenu();
      if (this.I == null) {
        this.I = new ExpandedActionViewMenuPresenter(null);
      }
      this.b.setExpandedActionViewsExclusive(true);
      localMenuBuilder.a(this.I, this.j);
    }
  }
  
  private void n()
  {
    if (this.b == null)
    {
      this.b = new ActionMenuView(getContext());
      this.b.setPopupTheme(this.k);
      this.b.setOnMenuItemClickListener(this.F);
      this.b.setMenuCallbacks(this.J, this.K);
      LayoutParams localLayoutParams = i();
      localLayoutParams.a = (0x800005 | this.n & 0x70);
      this.b.setLayoutParams(localLayoutParams);
      a(this.b, false);
    }
  }
  
  private void o()
  {
    if (this.e == null)
    {
      this.e = new ImageButton(getContext(), null, R.attr.toolbarNavigationButtonStyle);
      LayoutParams localLayoutParams = i();
      localLayoutParams.a = (0x800003 | this.n & 0x70);
      this.e.setLayoutParams(localLayoutParams);
    }
  }
  
  private void p()
  {
    if (this.i == null)
    {
      this.i = new ImageButton(getContext(), null, R.attr.toolbarNavigationButtonStyle);
      this.i.setImageDrawable(this.g);
      this.i.setContentDescription(this.h);
      LayoutParams localLayoutParams = i();
      localLayoutParams.a = (0x800003 | this.n & 0x70);
      localLayoutParams.b = 2;
      this.i.setLayoutParams(localLayoutParams);
      this.i.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          Toolbar.this.h();
        }
      });
    }
  }
  
  private void q()
  {
    removeCallbacks(this.M);
    post(this.M);
  }
  
  private boolean r()
  {
    if (!this.L) {
      return false;
    }
    int i2 = getChildCount();
    int i1 = 0;
    for (;;)
    {
      if (i1 >= i2) {
        break label56;
      }
      View localView = getChildAt(i1);
      if ((a(localView)) && (localView.getMeasuredWidth() > 0) && (localView.getMeasuredHeight() > 0)) {
        break;
      }
      i1 += 1;
    }
    label56:
    return true;
  }
  
  public LayoutParams a(AttributeSet paramAttributeSet)
  {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected LayoutParams a(ViewGroup.LayoutParams paramLayoutParams)
  {
    if ((paramLayoutParams instanceof LayoutParams)) {
      return new LayoutParams((LayoutParams)paramLayoutParams);
    }
    if ((paramLayoutParams instanceof ActionBar.LayoutParams)) {
      return new LayoutParams((ActionBar.LayoutParams)paramLayoutParams);
    }
    if ((paramLayoutParams instanceof ViewGroup.MarginLayoutParams)) {
      return new LayoutParams((ViewGroup.MarginLayoutParams)paramLayoutParams);
    }
    return new LayoutParams(paramLayoutParams);
  }
  
  public boolean a()
  {
    return (getVisibility() == 0) && (this.b != null) && (this.b.a());
  }
  
  public boolean b()
  {
    return (this.b != null) && (this.b.g());
  }
  
  public boolean c()
  {
    return (this.b != null) && (this.b.h());
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return (super.checkLayoutParams(paramLayoutParams)) && ((paramLayoutParams instanceof LayoutParams));
  }
  
  public boolean d()
  {
    return (this.b != null) && (this.b.e());
  }
  
  public boolean e()
  {
    return (this.b != null) && (this.b.f());
  }
  
  public void f()
  {
    if (this.b != null) {
      this.b.i();
    }
  }
  
  public boolean g()
  {
    return (this.I != null) && (this.I.b != null);
  }
  
  public int getContentInsetEnd()
  {
    return this.t.d();
  }
  
  public int getContentInsetLeft()
  {
    return this.t.a();
  }
  
  public int getContentInsetRight()
  {
    return this.t.b();
  }
  
  public int getContentInsetStart()
  {
    return this.t.c();
  }
  
  public Drawable getLogo()
  {
    if (this.f != null) {
      return this.f.getDrawable();
    }
    return null;
  }
  
  public CharSequence getLogoDescription()
  {
    if (this.f != null) {
      return this.f.getContentDescription();
    }
    return null;
  }
  
  public Menu getMenu()
  {
    m();
    return this.b.getMenu();
  }
  
  @Nullable
  public CharSequence getNavigationContentDescription()
  {
    if (this.e != null) {
      return this.e.getContentDescription();
    }
    return null;
  }
  
  @Nullable
  public Drawable getNavigationIcon()
  {
    if (this.e != null) {
      return this.e.getDrawable();
    }
    return null;
  }
  
  @Nullable
  public Drawable getOverflowIcon()
  {
    m();
    return this.b.getOverflowIcon();
  }
  
  public int getPopupTheme()
  {
    return this.k;
  }
  
  public CharSequence getSubtitle()
  {
    return this.w;
  }
  
  public CharSequence getTitle()
  {
    return this.v;
  }
  
  public DecorToolbar getWrapper()
  {
    if (this.G == null) {
      this.G = new ToolbarWidgetWrapper(this, true);
    }
    return this.G;
  }
  
  public void h()
  {
    if (this.I == null) {}
    for (MenuItemImpl localMenuItemImpl = null;; localMenuItemImpl = this.I.b)
    {
      if (localMenuItemImpl != null) {
        localMenuItemImpl.collapseActionView();
      }
      return;
    }
  }
  
  protected LayoutParams i()
  {
    return new LayoutParams(-2, -2);
  }
  
  void j()
  {
    int i1 = getChildCount() - 1;
    while (i1 >= 0)
    {
      View localView = getChildAt(i1);
      if ((((LayoutParams)localView.getLayoutParams()).b != 2) && (localView != this.b))
      {
        removeViewAt(i1);
        this.C.add(localView);
      }
      i1 -= 1;
    }
  }
  
  void k()
  {
    int i1 = this.C.size() - 1;
    while (i1 >= 0)
    {
      addView((View)this.C.get(i1));
      i1 -= 1;
    }
    this.C.clear();
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    removeCallbacks(this.M);
  }
  
  public boolean onHoverEvent(MotionEvent paramMotionEvent)
  {
    int i1 = MotionEventCompat.a(paramMotionEvent);
    if (i1 == 9) {
      this.A = false;
    }
    if (!this.A)
    {
      boolean bool = super.onHoverEvent(paramMotionEvent);
      if ((i1 == 9) && (!bool)) {
        this.A = true;
      }
    }
    if ((i1 == 10) || (i1 == 3)) {
      this.A = false;
    }
    return true;
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i2;
    int i7;
    int i9;
    int i5;
    int i8;
    int i4;
    int i10;
    int[] arrayOfInt;
    int i6;
    label120:
    label160:
    label200:
    label300:
    label340:
    boolean bool;
    Object localObject1;
    int i3;
    label467:
    Object localObject2;
    if (ViewCompat.h(this) == 1)
    {
      i2 = 1;
      i7 = getWidth();
      i9 = getHeight();
      i5 = getPaddingLeft();
      i8 = getPaddingRight();
      i4 = getPaddingTop();
      i10 = getPaddingBottom();
      paramInt3 = i5;
      paramInt4 = i7 - i8;
      arrayOfInt = this.D;
      arrayOfInt[1] = 0;
      arrayOfInt[0] = 0;
      i6 = ViewCompat.r(this);
      paramInt1 = paramInt3;
      paramInt2 = paramInt4;
      if (a(this.e))
      {
        if (i2 == 0) {
          break label901;
        }
        paramInt2 = b(this.e, paramInt4, arrayOfInt, i6);
        paramInt1 = paramInt3;
      }
      paramInt3 = paramInt1;
      paramInt4 = paramInt2;
      if (a(this.i))
      {
        if (i2 == 0) {
          break label922;
        }
        paramInt4 = b(this.i, paramInt2, arrayOfInt, i6);
        paramInt3 = paramInt1;
      }
      paramInt2 = paramInt3;
      paramInt1 = paramInt4;
      if (a(this.b))
      {
        if (i2 == 0) {
          break label943;
        }
        paramInt2 = a(this.b, paramInt3, arrayOfInt, i6);
        paramInt1 = paramInt4;
      }
      arrayOfInt[0] = Math.max(0, getContentInsetLeft() - paramInt2);
      arrayOfInt[1] = Math.max(0, getContentInsetRight() - (i7 - i8 - paramInt1));
      paramInt3 = Math.max(paramInt2, getContentInsetLeft());
      paramInt4 = Math.min(paramInt1, i7 - i8 - getContentInsetRight());
      paramInt1 = paramInt3;
      paramInt2 = paramInt4;
      if (a(this.a))
      {
        if (i2 == 0) {
          break label964;
        }
        paramInt2 = b(this.a, paramInt4, arrayOfInt, i6);
        paramInt1 = paramInt3;
      }
      paramInt3 = paramInt1;
      paramInt4 = paramInt2;
      if (a(this.f))
      {
        if (i2 == 0) {
          break label985;
        }
        paramInt4 = b(this.f, paramInt2, arrayOfInt, i6);
        paramInt3 = paramInt1;
      }
      paramBoolean = a(this.c);
      bool = a(this.d);
      paramInt1 = 0;
      if (paramBoolean)
      {
        localObject1 = (LayoutParams)this.c.getLayoutParams();
        paramInt1 = 0 + (((LayoutParams)localObject1).topMargin + this.c.getMeasuredHeight() + ((LayoutParams)localObject1).bottomMargin);
      }
      i3 = paramInt1;
      if (bool)
      {
        localObject1 = (LayoutParams)this.d.getLayoutParams();
        i3 = paramInt1 + (((LayoutParams)localObject1).topMargin + this.d.getMeasuredHeight() + ((LayoutParams)localObject1).bottomMargin);
      }
      if (!paramBoolean)
      {
        paramInt2 = paramInt3;
        paramInt1 = paramInt4;
        if (!bool) {}
      }
      else
      {
        if (!paramBoolean) {
          break label1006;
        }
        localObject1 = this.c;
        if (!bool) {
          break label1015;
        }
        localObject2 = this.d;
        label478:
        localObject1 = (LayoutParams)((View)localObject1).getLayoutParams();
        localObject2 = (LayoutParams)((View)localObject2).getLayoutParams();
        if (((!paramBoolean) || (this.c.getMeasuredWidth() <= 0)) && ((!bool) || (this.d.getMeasuredWidth() <= 0))) {
          break label1024;
        }
        i1 = 1;
        label530:
        switch (this.u & 0x70)
        {
        default: 
          paramInt2 = (i9 - i4 - i10 - i3) / 2;
          if (paramInt2 < ((LayoutParams)localObject1).topMargin + this.r)
          {
            paramInt1 = ((LayoutParams)localObject1).topMargin + this.r;
            label603:
            paramInt1 = i4 + paramInt1;
            label608:
            if (i2 == 0) {
              break label1132;
            }
            if (i1 == 0) {
              break label1127;
            }
          }
          break;
        }
      }
    }
    label901:
    label922:
    label943:
    label964:
    label985:
    label1006:
    label1015:
    label1024:
    label1127:
    for (paramInt2 = this.p;; paramInt2 = 0)
    {
      paramInt2 -= arrayOfInt[1];
      paramInt4 -= Math.max(0, paramInt2);
      arrayOfInt[1] = Math.max(0, -paramInt2);
      i3 = paramInt4;
      paramInt2 = paramInt4;
      i2 = i3;
      i4 = paramInt1;
      if (paramBoolean)
      {
        localObject1 = (LayoutParams)this.c.getLayoutParams();
        i2 = i3 - this.c.getMeasuredWidth();
        i4 = paramInt1 + this.c.getMeasuredHeight();
        this.c.layout(i2, paramInt1, i3, i4);
        i2 -= this.q;
        i4 += ((LayoutParams)localObject1).bottomMargin;
      }
      i3 = paramInt2;
      if (bool)
      {
        localObject1 = (LayoutParams)this.d.getLayoutParams();
        paramInt1 = i4 + ((LayoutParams)localObject1).topMargin;
        i3 = this.d.getMeasuredWidth();
        i4 = paramInt1 + this.d.getMeasuredHeight();
        this.d.layout(paramInt2 - i3, paramInt1, paramInt2, i4);
        i3 = paramInt2 - this.q;
        paramInt1 = ((LayoutParams)localObject1).bottomMargin;
      }
      paramInt2 = paramInt3;
      paramInt1 = paramInt4;
      if (i1 != 0)
      {
        paramInt1 = Math.min(i2, i3);
        paramInt2 = paramInt3;
      }
      a(this.B, 3);
      paramInt4 = this.B.size();
      paramInt3 = 0;
      while (paramInt3 < paramInt4)
      {
        paramInt2 = a((View)this.B.get(paramInt3), paramInt2, arrayOfInt, i6);
        paramInt3 += 1;
      }
      i2 = 0;
      break;
      paramInt1 = a(this.e, paramInt3, arrayOfInt, i6);
      paramInt2 = paramInt4;
      break label120;
      paramInt3 = a(this.i, paramInt1, arrayOfInt, i6);
      paramInt4 = paramInt2;
      break label160;
      paramInt1 = b(this.b, paramInt4, arrayOfInt, i6);
      paramInt2 = paramInt3;
      break label200;
      paramInt1 = a(this.a, paramInt3, arrayOfInt, i6);
      paramInt2 = paramInt4;
      break label300;
      paramInt3 = a(this.f, paramInt1, arrayOfInt, i6);
      paramInt4 = paramInt2;
      break label340;
      localObject1 = this.d;
      break label467;
      localObject2 = this.c;
      break label478;
      i1 = 0;
      break label530;
      paramInt1 = getPaddingTop() + ((LayoutParams)localObject1).topMargin + this.r;
      break label608;
      i3 = i9 - i10 - i3 - paramInt2 - i4;
      paramInt1 = paramInt2;
      if (i3 >= ((LayoutParams)localObject1).bottomMargin + this.s) {
        break label603;
      }
      paramInt1 = Math.max(0, paramInt2 - (((LayoutParams)localObject2).bottomMargin + this.s - i3));
      break label603;
      paramInt1 = i9 - i10 - ((LayoutParams)localObject2).bottomMargin - this.s - i3;
      break label608;
    }
    label1132:
    if (i1 != 0) {}
    for (paramInt2 = this.p;; paramInt2 = 0)
    {
      i2 = paramInt2 - arrayOfInt[0];
      paramInt2 = paramInt3 + Math.max(0, i2);
      arrayOfInt[0] = Math.max(0, -i2);
      i3 = paramInt2;
      paramInt3 = paramInt2;
      i2 = i3;
      i4 = paramInt1;
      if (paramBoolean)
      {
        localObject1 = (LayoutParams)this.c.getLayoutParams();
        i2 = i3 + this.c.getMeasuredWidth();
        i4 = paramInt1 + this.c.getMeasuredHeight();
        this.c.layout(i3, paramInt1, i2, i4);
        i2 += this.q;
        i4 += ((LayoutParams)localObject1).bottomMargin;
      }
      i3 = paramInt3;
      if (bool)
      {
        localObject1 = (LayoutParams)this.d.getLayoutParams();
        paramInt1 = i4 + ((LayoutParams)localObject1).topMargin;
        i3 = paramInt3 + this.d.getMeasuredWidth();
        i4 = paramInt1 + this.d.getMeasuredHeight();
        this.d.layout(paramInt3, paramInt1, i3, i4);
        i3 += this.q;
        paramInt1 = ((LayoutParams)localObject1).bottomMargin;
      }
      paramInt1 = paramInt4;
      if (i1 == 0) {
        break;
      }
      paramInt2 = Math.max(i2, i3);
      paramInt1 = paramInt4;
      break;
    }
    a(this.B, 5);
    int i1 = this.B.size();
    paramInt4 = 0;
    paramInt3 = paramInt1;
    paramInt1 = paramInt4;
    while (paramInt1 < i1)
    {
      paramInt3 = b((View)this.B.get(paramInt1), paramInt3, arrayOfInt, i6);
      paramInt1 += 1;
    }
    a(this.B, 1);
    paramInt1 = a(this.B, arrayOfInt);
    paramInt4 = i5 + (i7 - i5 - i8) / 2 - paramInt1 / 2;
    i1 = paramInt4 + paramInt1;
    if (paramInt4 < paramInt2) {
      paramInt1 = paramInt2;
    }
    for (;;)
    {
      paramInt3 = this.B.size();
      paramInt2 = 0;
      while (paramInt2 < paramInt3)
      {
        paramInt1 = a((View)this.B.get(paramInt2), paramInt1, arrayOfInt, i6);
        paramInt2 += 1;
      }
      paramInt1 = paramInt4;
      if (i1 > paramInt3) {
        paramInt1 = paramInt4 - (i1 - paramInt3);
      }
    }
    this.B.clear();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i4 = 0;
    int i3 = 0;
    int[] arrayOfInt = this.D;
    int i5;
    label524:
    View localView;
    if (ViewUtils.a(this))
    {
      i7 = 1;
      i6 = 0;
      i5 = 0;
      if (a(this.e))
      {
        a(this.e, paramInt1, 0, paramInt2, 0, this.o);
        i5 = this.e.getMeasuredWidth() + b(this.e);
        i4 = Math.max(0, this.e.getMeasuredHeight() + c(this.e));
        i3 = ViewUtils.a(0, ViewCompat.l(this.e));
      }
      i1 = i3;
      i2 = i4;
      if (a(this.i))
      {
        a(this.i, paramInt1, 0, paramInt2, 0, this.o);
        i5 = this.i.getMeasuredWidth() + b(this.i);
        i2 = Math.max(i4, this.i.getMeasuredHeight() + c(this.i));
        i1 = ViewUtils.a(i3, ViewCompat.l(this.i));
      }
      i3 = getContentInsetStart();
      i8 = 0 + Math.max(i3, i5);
      arrayOfInt[i7] = Math.max(0, i3 - i5);
      i5 = 0;
      i3 = i1;
      i4 = i2;
      if (a(this.b))
      {
        a(this.b, paramInt1, i8, paramInt2, 0, this.o);
        i5 = this.b.getMeasuredWidth() + b(this.b);
        i4 = Math.max(i2, this.b.getMeasuredHeight() + c(this.b));
        i3 = ViewUtils.a(i1, ViewCompat.l(this.b));
      }
      i1 = getContentInsetEnd();
      i7 = i8 + Math.max(i1, i5);
      arrayOfInt[i6] = Math.max(0, i1 - i5);
      i6 = i7;
      i1 = i3;
      i2 = i4;
      if (a(this.a))
      {
        i6 = i7 + a(this.a, paramInt1, i7, paramInt2, 0, arrayOfInt);
        i2 = Math.max(i4, this.a.getMeasuredHeight() + c(this.a));
        i1 = ViewUtils.a(i3, ViewCompat.l(this.a));
      }
      i3 = i6;
      i4 = i1;
      i5 = i2;
      if (a(this.f))
      {
        i3 = i6 + a(this.f, paramInt1, i6, paramInt2, 0, arrayOfInt);
        i5 = Math.max(i2, this.f.getMeasuredHeight() + c(this.f));
        i4 = ViewUtils.a(i1, ViewCompat.l(this.f));
      }
      i8 = getChildCount();
      i2 = 0;
      i6 = i5;
      i1 = i4;
      i5 = i3;
      if (i2 >= i8) {
        break label664;
      }
      localView = getChildAt(i2);
      i3 = i5;
      i4 = i1;
      i7 = i6;
      if (((LayoutParams)localView.getLayoutParams()).b == 0)
      {
        if (a(localView)) {
          break label613;
        }
        i7 = i6;
        i4 = i1;
        i3 = i5;
      }
    }
    for (;;)
    {
      i2 += 1;
      i5 = i3;
      i1 = i4;
      i6 = i7;
      break label524;
      i7 = 0;
      i6 = 1;
      break;
      label613:
      i3 = i5 + a(localView, paramInt1, i5, paramInt2, 0, arrayOfInt);
      i7 = Math.max(i6, localView.getMeasuredHeight() + c(localView));
      i4 = ViewUtils.a(i1, ViewCompat.l(localView));
    }
    label664:
    i4 = 0;
    i3 = 0;
    int i9 = this.r + this.s;
    int i10 = this.p + this.q;
    int i2 = i1;
    if (a(this.c))
    {
      a(this.c, paramInt1, i5 + i10, paramInt2, i9, arrayOfInt);
      i4 = this.c.getMeasuredWidth() + b(this.c);
      i3 = this.c.getMeasuredHeight() + c(this.c);
      i2 = ViewUtils.a(i1, ViewCompat.l(this.c));
    }
    int i7 = i2;
    int i8 = i3;
    int i1 = i4;
    if (a(this.d))
    {
      i1 = Math.max(i4, a(this.d, paramInt1, i5 + i10, paramInt2, i3 + i9, arrayOfInt));
      i8 = i3 + (this.d.getMeasuredHeight() + c(this.d));
      i7 = ViewUtils.a(i2, ViewCompat.l(this.d));
    }
    i2 = Math.max(i6, i8);
    int i6 = getPaddingLeft();
    i8 = getPaddingRight();
    i3 = getPaddingTop();
    i4 = getPaddingBottom();
    i1 = ViewCompat.a(Math.max(i5 + i1 + (i6 + i8), getSuggestedMinimumWidth()), paramInt1, 0xFF000000 & i7);
    paramInt1 = ViewCompat.a(Math.max(i2 + (i3 + i4), getSuggestedMinimumHeight()), paramInt2, i7 << 16);
    if (r()) {
      paramInt1 = 0;
    }
    setMeasuredDimension(i1, paramInt1);
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    if (!(paramParcelable instanceof SavedState))
    {
      super.onRestoreInstanceState(paramParcelable);
      return;
    }
    SavedState localSavedState = (SavedState)paramParcelable;
    super.onRestoreInstanceState(localSavedState.getSuperState());
    if (this.b != null) {}
    for (paramParcelable = this.b.d();; paramParcelable = null)
    {
      if ((localSavedState.a != 0) && (this.I != null) && (paramParcelable != null))
      {
        paramParcelable = paramParcelable.findItem(localSavedState.a);
        if (paramParcelable != null) {
          MenuItemCompat.b(paramParcelable);
        }
      }
      if (!localSavedState.b) {
        break;
      }
      q();
      return;
    }
  }
  
  public void onRtlPropertiesChanged(int paramInt)
  {
    boolean bool = true;
    if (Build.VERSION.SDK_INT >= 17) {
      super.onRtlPropertiesChanged(paramInt);
    }
    RtlSpacingHelper localRtlSpacingHelper = this.t;
    if (paramInt == 1) {}
    for (;;)
    {
      localRtlSpacingHelper.a(bool);
      return;
      bool = false;
    }
  }
  
  protected Parcelable onSaveInstanceState()
  {
    SavedState localSavedState = new SavedState(super.onSaveInstanceState());
    if ((this.I != null) && (this.I.b != null)) {
      localSavedState.a = this.I.b.getItemId();
    }
    localSavedState.b = b();
    return localSavedState;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    int i1 = MotionEventCompat.a(paramMotionEvent);
    if (i1 == 0) {
      this.z = false;
    }
    if (!this.z)
    {
      boolean bool = super.onTouchEvent(paramMotionEvent);
      if ((i1 == 0) && (!bool)) {
        this.z = true;
      }
    }
    if ((i1 == 1) || (i1 == 3)) {
      this.z = false;
    }
    return true;
  }
  
  public void setCollapsible(boolean paramBoolean)
  {
    this.L = paramBoolean;
    requestLayout();
  }
  
  public void setContentInsetsAbsolute(int paramInt1, int paramInt2)
  {
    this.t.b(paramInt1, paramInt2);
  }
  
  public void setContentInsetsRelative(int paramInt1, int paramInt2)
  {
    this.t.a(paramInt1, paramInt2);
  }
  
  public void setLogo(@DrawableRes int paramInt)
  {
    setLogo(this.N.a(getContext(), paramInt));
  }
  
  public void setLogo(Drawable paramDrawable)
  {
    if (paramDrawable != null)
    {
      l();
      if (!d(this.f)) {
        a(this.f, true);
      }
    }
    for (;;)
    {
      if (this.f != null) {
        this.f.setImageDrawable(paramDrawable);
      }
      return;
      if ((this.f != null) && (d(this.f)))
      {
        removeView(this.f);
        this.C.remove(this.f);
      }
    }
  }
  
  public void setLogoDescription(@StringRes int paramInt)
  {
    setLogoDescription(getContext().getText(paramInt));
  }
  
  public void setLogoDescription(CharSequence paramCharSequence)
  {
    if (!TextUtils.isEmpty(paramCharSequence)) {
      l();
    }
    if (this.f != null) {
      this.f.setContentDescription(paramCharSequence);
    }
  }
  
  public void setMenu(MenuBuilder paramMenuBuilder, ActionMenuPresenter paramActionMenuPresenter)
  {
    if ((paramMenuBuilder == null) && (this.b == null)) {}
    MenuBuilder localMenuBuilder;
    do
    {
      return;
      n();
      localMenuBuilder = this.b.d();
    } while (localMenuBuilder == paramMenuBuilder);
    if (localMenuBuilder != null)
    {
      localMenuBuilder.b(this.H);
      localMenuBuilder.b(this.I);
    }
    if (this.I == null) {
      this.I = new ExpandedActionViewMenuPresenter(null);
    }
    paramActionMenuPresenter.d(true);
    if (paramMenuBuilder != null)
    {
      paramMenuBuilder.a(paramActionMenuPresenter, this.j);
      paramMenuBuilder.a(this.I, this.j);
    }
    for (;;)
    {
      this.b.setPopupTheme(this.k);
      this.b.setPresenter(paramActionMenuPresenter);
      this.H = paramActionMenuPresenter;
      return;
      paramActionMenuPresenter.a(this.j, null);
      this.I.a(this.j, null);
      paramActionMenuPresenter.a(true);
      this.I.a(true);
    }
  }
  
  public void setMenuCallbacks(MenuPresenter.Callback paramCallback, MenuBuilder.Callback paramCallback1)
  {
    this.J = paramCallback;
    this.K = paramCallback1;
    if (this.b != null) {
      this.b.setMenuCallbacks(paramCallback, paramCallback1);
    }
  }
  
  public void setNavigationContentDescription(@StringRes int paramInt)
  {
    if (paramInt != 0) {}
    for (CharSequence localCharSequence = getContext().getText(paramInt);; localCharSequence = null)
    {
      setNavigationContentDescription(localCharSequence);
      return;
    }
  }
  
  public void setNavigationContentDescription(@Nullable CharSequence paramCharSequence)
  {
    if (!TextUtils.isEmpty(paramCharSequence)) {
      o();
    }
    if (this.e != null) {
      this.e.setContentDescription(paramCharSequence);
    }
  }
  
  public void setNavigationIcon(@DrawableRes int paramInt)
  {
    setNavigationIcon(this.N.a(getContext(), paramInt));
  }
  
  public void setNavigationIcon(@Nullable Drawable paramDrawable)
  {
    if (paramDrawable != null)
    {
      o();
      if (!d(this.e)) {
        a(this.e, true);
      }
    }
    for (;;)
    {
      if (this.e != null) {
        this.e.setImageDrawable(paramDrawable);
      }
      return;
      if ((this.e != null) && (d(this.e)))
      {
        removeView(this.e);
        this.C.remove(this.e);
      }
    }
  }
  
  public void setNavigationOnClickListener(View.OnClickListener paramOnClickListener)
  {
    o();
    this.e.setOnClickListener(paramOnClickListener);
  }
  
  public void setOnMenuItemClickListener(OnMenuItemClickListener paramOnMenuItemClickListener)
  {
    this.E = paramOnMenuItemClickListener;
  }
  
  public void setOverflowIcon(@Nullable Drawable paramDrawable)
  {
    m();
    this.b.setOverflowIcon(paramDrawable);
  }
  
  public void setPopupTheme(@StyleRes int paramInt)
  {
    if (this.k != paramInt)
    {
      this.k = paramInt;
      if (paramInt == 0) {
        this.j = getContext();
      }
    }
    else
    {
      return;
    }
    this.j = new ContextThemeWrapper(getContext(), paramInt);
  }
  
  public void setSubtitle(@StringRes int paramInt)
  {
    setSubtitle(getContext().getText(paramInt));
  }
  
  public void setSubtitle(CharSequence paramCharSequence)
  {
    if (!TextUtils.isEmpty(paramCharSequence))
    {
      if (this.d == null)
      {
        Context localContext = getContext();
        this.d = new TextView(localContext);
        this.d.setSingleLine();
        this.d.setEllipsize(TextUtils.TruncateAt.END);
        if (this.m != 0) {
          this.d.setTextAppearance(localContext, this.m);
        }
        if (this.y != 0) {
          this.d.setTextColor(this.y);
        }
      }
      if (!d(this.d)) {
        a(this.d, true);
      }
    }
    for (;;)
    {
      if (this.d != null) {
        this.d.setText(paramCharSequence);
      }
      this.w = paramCharSequence;
      return;
      if ((this.d != null) && (d(this.d)))
      {
        removeView(this.d);
        this.C.remove(this.d);
      }
    }
  }
  
  public void setSubtitleTextAppearance(Context paramContext, @StyleRes int paramInt)
  {
    this.m = paramInt;
    if (this.d != null) {
      this.d.setTextAppearance(paramContext, paramInt);
    }
  }
  
  public void setSubtitleTextColor(@ColorInt int paramInt)
  {
    this.y = paramInt;
    if (this.d != null) {
      this.d.setTextColor(paramInt);
    }
  }
  
  public void setTitle(@StringRes int paramInt)
  {
    setTitle(getContext().getText(paramInt));
  }
  
  public void setTitle(CharSequence paramCharSequence)
  {
    if (!TextUtils.isEmpty(paramCharSequence))
    {
      if (this.c == null)
      {
        Context localContext = getContext();
        this.c = new TextView(localContext);
        this.c.setSingleLine();
        this.c.setEllipsize(TextUtils.TruncateAt.END);
        if (this.l != 0) {
          this.c.setTextAppearance(localContext, this.l);
        }
        if (this.x != 0) {
          this.c.setTextColor(this.x);
        }
      }
      if (!d(this.c)) {
        a(this.c, true);
      }
    }
    for (;;)
    {
      if (this.c != null) {
        this.c.setText(paramCharSequence);
      }
      this.v = paramCharSequence;
      return;
      if ((this.c != null) && (d(this.c)))
      {
        removeView(this.c);
        this.C.remove(this.c);
      }
    }
  }
  
  public void setTitleTextAppearance(Context paramContext, @StyleRes int paramInt)
  {
    this.l = paramInt;
    if (this.c != null) {
      this.c.setTextAppearance(paramContext, paramInt);
    }
  }
  
  public void setTitleTextColor(@ColorInt int paramInt)
  {
    this.x = paramInt;
    if (this.c != null) {
      this.c.setTextColor(paramInt);
    }
  }
  
  private class ExpandedActionViewMenuPresenter
    implements MenuPresenter
  {
    MenuBuilder a;
    MenuItemImpl b;
    
    private ExpandedActionViewMenuPresenter() {}
    
    public void a(Context paramContext, MenuBuilder paramMenuBuilder)
    {
      if ((this.a != null) && (this.b != null)) {
        this.a.d(this.b);
      }
      this.a = paramMenuBuilder;
    }
    
    public void a(Parcelable paramParcelable) {}
    
    public void a(MenuBuilder paramMenuBuilder, boolean paramBoolean) {}
    
    public void a(boolean paramBoolean)
    {
      int k;
      int j;
      int m;
      int i;
      if (this.b != null)
      {
        k = 0;
        j = k;
        if (this.a != null)
        {
          m = this.a.size();
          i = 0;
        }
      }
      for (;;)
      {
        j = k;
        if (i < m)
        {
          if (this.a.getItem(i) == this.b) {
            j = 1;
          }
        }
        else
        {
          if (j == 0) {
            b(this.a, this.b);
          }
          return;
        }
        i += 1;
      }
    }
    
    public boolean a()
    {
      return false;
    }
    
    public boolean a(MenuBuilder paramMenuBuilder, MenuItemImpl paramMenuItemImpl)
    {
      Toolbar.b(Toolbar.this);
      if (Toolbar.c(Toolbar.this).getParent() != Toolbar.this) {
        Toolbar.this.addView(Toolbar.c(Toolbar.this));
      }
      Toolbar.this.a = paramMenuItemImpl.getActionView();
      this.b = paramMenuItemImpl;
      if (Toolbar.this.a.getParent() != Toolbar.this)
      {
        paramMenuBuilder = Toolbar.this.i();
        paramMenuBuilder.a = (0x800003 | Toolbar.d(Toolbar.this) & 0x70);
        paramMenuBuilder.b = 2;
        Toolbar.this.a.setLayoutParams(paramMenuBuilder);
        Toolbar.this.addView(Toolbar.this.a);
      }
      Toolbar.this.j();
      Toolbar.this.requestLayout();
      paramMenuItemImpl.e(true);
      if ((Toolbar.this.a instanceof CollapsibleActionView)) {
        ((CollapsibleActionView)Toolbar.this.a).a();
      }
      return true;
    }
    
    public boolean a(SubMenuBuilder paramSubMenuBuilder)
    {
      return false;
    }
    
    public int b()
    {
      return 0;
    }
    
    public boolean b(MenuBuilder paramMenuBuilder, MenuItemImpl paramMenuItemImpl)
    {
      if ((Toolbar.this.a instanceof CollapsibleActionView)) {
        ((CollapsibleActionView)Toolbar.this.a).b();
      }
      Toolbar.this.removeView(Toolbar.this.a);
      Toolbar.this.removeView(Toolbar.c(Toolbar.this));
      Toolbar.this.a = null;
      Toolbar.this.k();
      this.b = null;
      Toolbar.this.requestLayout();
      paramMenuItemImpl.e(false);
      return true;
    }
    
    public Parcelable c()
    {
      return null;
    }
  }
  
  public static class LayoutParams
    extends ActionBar.LayoutParams
  {
    int b = 0;
    
    public LayoutParams(int paramInt1, int paramInt2)
    {
      super(paramInt2);
      this.a = 8388627;
    }
    
    public LayoutParams(@NonNull Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
    }
    
    public LayoutParams(ActionBar.LayoutParams paramLayoutParams)
    {
      super();
    }
    
    public LayoutParams(LayoutParams paramLayoutParams)
    {
      super();
      this.b = paramLayoutParams.b;
    }
    
    public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
    }
    
    public LayoutParams(ViewGroup.MarginLayoutParams paramMarginLayoutParams)
    {
      super();
      a(paramMarginLayoutParams);
    }
    
    void a(ViewGroup.MarginLayoutParams paramMarginLayoutParams)
    {
      this.leftMargin = paramMarginLayoutParams.leftMargin;
      this.topMargin = paramMarginLayoutParams.topMargin;
      this.rightMargin = paramMarginLayoutParams.rightMargin;
      this.bottomMargin = paramMarginLayoutParams.bottomMargin;
    }
  }
  
  public static abstract interface OnMenuItemClickListener
  {
    public abstract boolean a(MenuItem paramMenuItem);
  }
  
  public static class SavedState
    extends View.BaseSavedState
  {
    public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
    {
      public Toolbar.SavedState a(Parcel paramAnonymousParcel)
      {
        return new Toolbar.SavedState(paramAnonymousParcel);
      }
      
      public Toolbar.SavedState[] a(int paramAnonymousInt)
      {
        return new Toolbar.SavedState[paramAnonymousInt];
      }
    };
    int a;
    boolean b;
    
    public SavedState(Parcel paramParcel)
    {
      super();
      this.a = paramParcel.readInt();
      if (paramParcel.readInt() != 0) {}
      for (boolean bool = true;; bool = false)
      {
        this.b = bool;
        return;
      }
    }
    
    public SavedState(Parcelable paramParcelable)
    {
      super();
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      super.writeToParcel(paramParcel, paramInt);
      paramParcel.writeInt(this.a);
      if (this.b) {}
      for (paramInt = 1;; paramInt = 0)
      {
        paramParcel.writeInt(paramInt);
        return;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/Toolbar.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */