package android.support.v7.widget;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.LongSparseArray;
import android.support.v4.util.Pools.Pool;
import android.support.v4.util.Pools.SimplePool;

class ViewInfoStore
{
  @VisibleForTesting
  final ArrayMap<RecyclerView.ViewHolder, InfoRecord> a = new ArrayMap();
  @VisibleForTesting
  final LongSparseArray<RecyclerView.ViewHolder> b = new LongSparseArray();
  
  private RecyclerView.ItemAnimator.ItemHolderInfo a(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    Object localObject2 = null;
    int i = this.a.a(paramViewHolder);
    Object localObject1;
    if (i < 0) {
      localObject1 = localObject2;
    }
    InfoRecord localInfoRecord;
    do
    {
      do
      {
        return (RecyclerView.ItemAnimator.ItemHolderInfo)localObject1;
        localInfoRecord = (InfoRecord)this.a.c(i);
        localObject1 = localObject2;
      } while (localInfoRecord == null);
      localObject1 = localObject2;
    } while ((localInfoRecord.a & paramInt) == 0);
    localInfoRecord.a &= (paramInt ^ 0xFFFFFFFF);
    if (paramInt == 4) {}
    for (paramViewHolder = localInfoRecord.b;; paramViewHolder = localInfoRecord.c)
    {
      localObject1 = paramViewHolder;
      if ((localInfoRecord.a & 0xC) != 0) {
        break;
      }
      this.a.d(i);
      InfoRecord.a(localInfoRecord);
      return paramViewHolder;
      if (paramInt != 8) {
        break label129;
      }
    }
    label129:
    throw new IllegalArgumentException("Must provide flag PRE or POST");
  }
  
  RecyclerView.ViewHolder a(long paramLong)
  {
    return (RecyclerView.ViewHolder)this.b.a(paramLong);
  }
  
  void a()
  {
    this.a.clear();
    this.b.c();
  }
  
  void a(long paramLong, RecyclerView.ViewHolder paramViewHolder)
  {
    this.b.b(paramLong, paramViewHolder);
  }
  
  void a(RecyclerView.ViewHolder paramViewHolder, RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo)
  {
    InfoRecord localInfoRecord2 = (InfoRecord)this.a.get(paramViewHolder);
    InfoRecord localInfoRecord1 = localInfoRecord2;
    if (localInfoRecord2 == null)
    {
      localInfoRecord1 = InfoRecord.a();
      this.a.put(paramViewHolder, localInfoRecord1);
    }
    localInfoRecord1.b = paramItemHolderInfo;
    localInfoRecord1.a |= 0x4;
  }
  
  void a(ProcessCallback paramProcessCallback)
  {
    int i = this.a.size() - 1;
    if (i >= 0)
    {
      RecyclerView.ViewHolder localViewHolder = (RecyclerView.ViewHolder)this.a.b(i);
      InfoRecord localInfoRecord = (InfoRecord)this.a.d(i);
      if ((localInfoRecord.a & 0x3) == 3) {
        paramProcessCallback.a(localViewHolder);
      }
      for (;;)
      {
        InfoRecord.a(localInfoRecord);
        i -= 1;
        break;
        if ((localInfoRecord.a & 0x1) != 0)
        {
          if (localInfoRecord.b == null) {
            paramProcessCallback.a(localViewHolder);
          } else {
            paramProcessCallback.a(localViewHolder, localInfoRecord.b, localInfoRecord.c);
          }
        }
        else if ((localInfoRecord.a & 0xE) == 14) {
          paramProcessCallback.b(localViewHolder, localInfoRecord.b, localInfoRecord.c);
        } else if ((localInfoRecord.a & 0xC) == 12) {
          paramProcessCallback.c(localViewHolder, localInfoRecord.b, localInfoRecord.c);
        } else if ((localInfoRecord.a & 0x4) != 0) {
          paramProcessCallback.a(localViewHolder, localInfoRecord.b, null);
        } else if ((localInfoRecord.a & 0x8) != 0) {
          paramProcessCallback.b(localViewHolder, localInfoRecord.b, localInfoRecord.c);
        } else if ((localInfoRecord.a & 0x2) == 0) {}
      }
    }
  }
  
  boolean a(RecyclerView.ViewHolder paramViewHolder)
  {
    paramViewHolder = (InfoRecord)this.a.get(paramViewHolder);
    return (paramViewHolder != null) && ((paramViewHolder.a & 0x1) != 0);
  }
  
  @Nullable
  RecyclerView.ItemAnimator.ItemHolderInfo b(RecyclerView.ViewHolder paramViewHolder)
  {
    return a(paramViewHolder, 4);
  }
  
  void b() {}
  
  void b(RecyclerView.ViewHolder paramViewHolder, RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo)
  {
    InfoRecord localInfoRecord2 = (InfoRecord)this.a.get(paramViewHolder);
    InfoRecord localInfoRecord1 = localInfoRecord2;
    if (localInfoRecord2 == null)
    {
      localInfoRecord1 = InfoRecord.a();
      this.a.put(paramViewHolder, localInfoRecord1);
    }
    localInfoRecord1.a |= 0x2;
    localInfoRecord1.b = paramItemHolderInfo;
  }
  
  @Nullable
  RecyclerView.ItemAnimator.ItemHolderInfo c(RecyclerView.ViewHolder paramViewHolder)
  {
    return a(paramViewHolder, 8);
  }
  
  void c(RecyclerView.ViewHolder paramViewHolder, RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo)
  {
    InfoRecord localInfoRecord2 = (InfoRecord)this.a.get(paramViewHolder);
    InfoRecord localInfoRecord1 = localInfoRecord2;
    if (localInfoRecord2 == null)
    {
      localInfoRecord1 = InfoRecord.a();
      this.a.put(paramViewHolder, localInfoRecord1);
    }
    localInfoRecord1.c = paramItemHolderInfo;
    localInfoRecord1.a |= 0x8;
  }
  
  boolean d(RecyclerView.ViewHolder paramViewHolder)
  {
    paramViewHolder = (InfoRecord)this.a.get(paramViewHolder);
    return (paramViewHolder != null) && ((paramViewHolder.a & 0x4) != 0);
  }
  
  void e(RecyclerView.ViewHolder paramViewHolder)
  {
    InfoRecord localInfoRecord2 = (InfoRecord)this.a.get(paramViewHolder);
    InfoRecord localInfoRecord1 = localInfoRecord2;
    if (localInfoRecord2 == null)
    {
      localInfoRecord1 = InfoRecord.a();
      this.a.put(paramViewHolder, localInfoRecord1);
    }
    localInfoRecord1.a |= 0x1;
  }
  
  void f(RecyclerView.ViewHolder paramViewHolder)
  {
    paramViewHolder = (InfoRecord)this.a.get(paramViewHolder);
    if (paramViewHolder == null) {
      return;
    }
    paramViewHolder.a &= 0xFFFFFFFE;
  }
  
  void g(RecyclerView.ViewHolder paramViewHolder)
  {
    int i = this.b.b() - 1;
    for (;;)
    {
      if (i >= 0)
      {
        if (paramViewHolder == this.b.c(i)) {
          this.b.a(i);
        }
      }
      else
      {
        paramViewHolder = (InfoRecord)this.a.remove(paramViewHolder);
        if (paramViewHolder != null) {
          InfoRecord.a(paramViewHolder);
        }
        return;
      }
      i -= 1;
    }
  }
  
  public void h(RecyclerView.ViewHolder paramViewHolder)
  {
    f(paramViewHolder);
  }
  
  static class InfoRecord
  {
    static Pools.Pool<InfoRecord> d = new Pools.SimplePool(20);
    int a;
    @Nullable
    RecyclerView.ItemAnimator.ItemHolderInfo b;
    @Nullable
    RecyclerView.ItemAnimator.ItemHolderInfo c;
    
    static InfoRecord a()
    {
      InfoRecord localInfoRecord2 = (InfoRecord)d.a();
      InfoRecord localInfoRecord1 = localInfoRecord2;
      if (localInfoRecord2 == null) {
        localInfoRecord1 = new InfoRecord();
      }
      return localInfoRecord1;
    }
    
    static void a(InfoRecord paramInfoRecord)
    {
      paramInfoRecord.a = 0;
      paramInfoRecord.b = null;
      paramInfoRecord.c = null;
      d.a(paramInfoRecord);
    }
    
    static void b()
    {
      while (d.a() != null) {}
    }
  }
  
  static abstract interface ProcessCallback
  {
    public abstract void a(RecyclerView.ViewHolder paramViewHolder);
    
    public abstract void a(RecyclerView.ViewHolder paramViewHolder, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo1, @Nullable RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo2);
    
    public abstract void b(RecyclerView.ViewHolder paramViewHolder, @Nullable RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo1, RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo2);
    
    public abstract void c(RecyclerView.ViewHolder paramViewHolder, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo1, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo2);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/ViewInfoStore.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */