package android.support.v7.widget;

import java.util.List;

class OpReorderer
{
  final Callback a;
  
  public OpReorderer(Callback paramCallback)
  {
    this.a = paramCallback;
  }
  
  private void a(List<AdapterHelper.UpdateOp> paramList, int paramInt1, int paramInt2)
  {
    AdapterHelper.UpdateOp localUpdateOp1 = (AdapterHelper.UpdateOp)paramList.get(paramInt1);
    AdapterHelper.UpdateOp localUpdateOp2 = (AdapterHelper.UpdateOp)paramList.get(paramInt2);
    switch (localUpdateOp2.a)
    {
    case 3: 
    default: 
      return;
    case 2: 
      a(paramList, paramInt1, localUpdateOp1, paramInt2, localUpdateOp2);
      return;
    case 1: 
      c(paramList, paramInt1, localUpdateOp1, paramInt2, localUpdateOp2);
      return;
    }
    b(paramList, paramInt1, localUpdateOp1, paramInt2, localUpdateOp2);
  }
  
  private int b(List<AdapterHelper.UpdateOp> paramList)
  {
    int j = 0;
    int i = paramList.size() - 1;
    while (i >= 0)
    {
      int k;
      if (((AdapterHelper.UpdateOp)paramList.get(i)).a == 8)
      {
        k = j;
        if (j != 0) {
          return i;
        }
      }
      else
      {
        k = 1;
      }
      i -= 1;
      j = k;
    }
    return -1;
  }
  
  private void c(List<AdapterHelper.UpdateOp> paramList, int paramInt1, AdapterHelper.UpdateOp paramUpdateOp1, int paramInt2, AdapterHelper.UpdateOp paramUpdateOp2)
  {
    int i = 0;
    if (paramUpdateOp1.d < paramUpdateOp2.b) {
      i = 0 - 1;
    }
    int j = i;
    if (paramUpdateOp1.b < paramUpdateOp2.b) {
      j = i + 1;
    }
    if (paramUpdateOp2.b <= paramUpdateOp1.b) {
      paramUpdateOp1.b += paramUpdateOp2.d;
    }
    if (paramUpdateOp2.b <= paramUpdateOp1.d) {
      paramUpdateOp1.d += paramUpdateOp2.d;
    }
    paramUpdateOp2.b += j;
    paramList.set(paramInt1, paramUpdateOp2);
    paramList.set(paramInt2, paramUpdateOp1);
  }
  
  void a(List<AdapterHelper.UpdateOp> paramList)
  {
    for (;;)
    {
      int i = b(paramList);
      if (i == -1) {
        break;
      }
      a(paramList, i, i + 1);
    }
  }
  
  void a(List<AdapterHelper.UpdateOp> paramList, int paramInt1, AdapterHelper.UpdateOp paramUpdateOp1, int paramInt2, AdapterHelper.UpdateOp paramUpdateOp2)
  {
    AdapterHelper.UpdateOp localUpdateOp = null;
    int k = 0;
    int m;
    int j;
    int i;
    if (paramUpdateOp1.b < paramUpdateOp1.d)
    {
      m = 0;
      j = m;
      i = k;
      if (paramUpdateOp2.b == paramUpdateOp1.b)
      {
        j = m;
        i = k;
        if (paramUpdateOp2.d == paramUpdateOp1.d - paramUpdateOp1.b)
        {
          i = 1;
          j = m;
        }
      }
      if (paramUpdateOp1.d >= paramUpdateOp2.b) {
        break label215;
      }
      paramUpdateOp2.b -= 1;
      label96:
      if (paramUpdateOp1.b > paramUpdateOp2.b) {
        break label284;
      }
      paramUpdateOp2.b += 1;
      label120:
      if (i == 0) {
        break label367;
      }
      paramList.set(paramInt1, paramUpdateOp2);
      paramList.remove(paramInt2);
      this.a.a(paramUpdateOp1);
    }
    label215:
    label284:
    label367:
    label639:
    label649:
    for (;;)
    {
      return;
      m = 1;
      j = m;
      i = k;
      if (paramUpdateOp2.b != paramUpdateOp1.d + 1) {
        break;
      }
      j = m;
      i = k;
      if (paramUpdateOp2.d != paramUpdateOp1.b - paramUpdateOp1.d) {
        break;
      }
      i = 1;
      j = m;
      break;
      if (paramUpdateOp1.d >= paramUpdateOp2.b + paramUpdateOp2.d) {
        break label96;
      }
      paramUpdateOp2.d -= 1;
      paramUpdateOp1.a = 2;
      paramUpdateOp1.d = 1;
      if (paramUpdateOp2.d == 0)
      {
        paramList.remove(paramInt2);
        this.a.a(paramUpdateOp2);
        return;
        if (paramUpdateOp1.b >= paramUpdateOp2.b + paramUpdateOp2.d) {
          break label120;
        }
        k = paramUpdateOp2.b;
        m = paramUpdateOp2.d;
        int n = paramUpdateOp1.b;
        localUpdateOp = this.a.a(2, paramUpdateOp1.b + 1, k + m - n, null);
        paramUpdateOp2.d = (paramUpdateOp1.b - paramUpdateOp2.b);
        break label120;
        if (j != 0)
        {
          if (localUpdateOp != null)
          {
            if (paramUpdateOp1.b > localUpdateOp.b) {
              paramUpdateOp1.b -= localUpdateOp.d;
            }
            if (paramUpdateOp1.d > localUpdateOp.b) {
              paramUpdateOp1.d -= localUpdateOp.d;
            }
          }
          if (paramUpdateOp1.b > paramUpdateOp2.b) {
            paramUpdateOp1.b -= paramUpdateOp2.d;
          }
          if (paramUpdateOp1.d > paramUpdateOp2.b) {
            paramUpdateOp1.d -= paramUpdateOp2.d;
          }
          paramList.set(paramInt1, paramUpdateOp2);
          if (paramUpdateOp1.b == paramUpdateOp1.d) {
            break label639;
          }
          paramList.set(paramInt2, paramUpdateOp1);
        }
        for (;;)
        {
          if (localUpdateOp == null) {
            break label649;
          }
          paramList.add(paramInt1, localUpdateOp);
          return;
          if (localUpdateOp != null)
          {
            if (paramUpdateOp1.b >= localUpdateOp.b) {
              paramUpdateOp1.b -= localUpdateOp.d;
            }
            if (paramUpdateOp1.d >= localUpdateOp.b) {
              paramUpdateOp1.d -= localUpdateOp.d;
            }
          }
          if (paramUpdateOp1.b >= paramUpdateOp2.b) {
            paramUpdateOp1.b -= paramUpdateOp2.d;
          }
          if (paramUpdateOp1.d < paramUpdateOp2.b) {
            break;
          }
          paramUpdateOp1.d -= paramUpdateOp2.d;
          break;
          paramList.remove(paramInt2);
        }
      }
    }
  }
  
  void b(List<AdapterHelper.UpdateOp> paramList, int paramInt1, AdapterHelper.UpdateOp paramUpdateOp1, int paramInt2, AdapterHelper.UpdateOp paramUpdateOp2)
  {
    Object localObject1 = null;
    Object localObject2 = null;
    if (paramUpdateOp1.d < paramUpdateOp2.b)
    {
      paramUpdateOp2.b -= 1;
      if (paramUpdateOp1.b > paramUpdateOp2.b) {
        break label166;
      }
      paramUpdateOp2.b += 1;
      label54:
      paramList.set(paramInt2, paramUpdateOp1);
      if (paramUpdateOp2.d <= 0) {
        break label243;
      }
      paramList.set(paramInt1, paramUpdateOp2);
    }
    for (;;)
    {
      if (localObject1 != null) {
        paramList.add(paramInt1, localObject1);
      }
      if (localObject2 != null) {
        paramList.add(paramInt1, localObject2);
      }
      return;
      if (paramUpdateOp1.d >= paramUpdateOp2.b + paramUpdateOp2.d) {
        break;
      }
      paramUpdateOp2.d -= 1;
      localObject1 = this.a.a(4, paramUpdateOp1.b, 1, paramUpdateOp2.c);
      break;
      label166:
      if (paramUpdateOp1.b >= paramUpdateOp2.b + paramUpdateOp2.d) {
        break label54;
      }
      int i = paramUpdateOp2.b + paramUpdateOp2.d - paramUpdateOp1.b;
      localObject2 = this.a.a(4, paramUpdateOp1.b + 1, i, paramUpdateOp2.c);
      paramUpdateOp2.d -= i;
      break label54;
      label243:
      paramList.remove(paramInt1);
      this.a.a(paramUpdateOp2);
    }
  }
  
  static abstract interface Callback
  {
    public abstract AdapterHelper.UpdateOp a(int paramInt1, int paramInt2, int paramInt3, Object paramObject);
    
    public abstract void a(AdapterHelper.UpdateOp paramUpdateOp);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/OpReorderer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */