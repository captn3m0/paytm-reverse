package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ActionProvider;
import android.support.v4.view.ActionProvider.SubUiVisibilityListener;
import android.support.v7.appcompat.R.attr;
import android.support.v7.appcompat.R.integer;
import android.support.v7.appcompat.R.layout;
import android.support.v7.transition.ActionBarTransition;
import android.support.v7.view.ActionBarPolicy;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.view.menu.ActionMenuItemView.PopupCallback;
import android.support.v7.view.menu.BaseMenuPresenter;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.view.menu.MenuPresenter.Callback;
import android.support.v7.view.menu.MenuView;
import android.support.v7.view.menu.MenuView.ItemView;
import android.support.v7.view.menu.SubMenuBuilder;
import android.util.DisplayMetrics;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import java.util.ArrayList;

class ActionMenuPresenter
  extends BaseMenuPresenter
  implements ActionProvider.SubUiVisibilityListener
{
  private ActionMenuPopupCallback A;
  final PopupPresenterCallback g = new PopupPresenterCallback(null);
  int h;
  private OverflowMenuButton i;
  private Drawable j;
  private boolean k;
  private boolean l;
  private boolean m;
  private int n;
  private int o;
  private int p;
  private boolean q;
  private boolean r;
  private boolean s;
  private boolean t;
  private int u;
  private final SparseBooleanArray v = new SparseBooleanArray();
  private View w;
  private OverflowPopup x;
  private ActionButtonSubmenu y;
  private OpenOverflowRunnable z;
  
  public ActionMenuPresenter(Context paramContext)
  {
    super(paramContext, R.layout.abc_action_menu_layout, R.layout.abc_action_menu_item_layout);
  }
  
  private View a(MenuItem paramMenuItem)
  {
    ViewGroup localViewGroup = (ViewGroup)this.f;
    Object localObject;
    if (localViewGroup == null)
    {
      localObject = null;
      return (View)localObject;
    }
    int i2 = localViewGroup.getChildCount();
    int i1 = 0;
    for (;;)
    {
      if (i1 >= i2) {
        break label74;
      }
      View localView = localViewGroup.getChildAt(i1);
      if ((localView instanceof MenuView.ItemView))
      {
        localObject = localView;
        if (((MenuView.ItemView)localView).getItemData() == paramMenuItem) {
          break;
        }
      }
      i1 += 1;
    }
    label74:
    return null;
  }
  
  public MenuView a(ViewGroup paramViewGroup)
  {
    paramViewGroup = super.a(paramViewGroup);
    ((ActionMenuView)paramViewGroup).setPresenter(this);
    return paramViewGroup;
  }
  
  public View a(MenuItemImpl paramMenuItemImpl, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramMenuItemImpl.getActionView();
    if ((localView == null) || (paramMenuItemImpl.n())) {
      localView = super.a(paramMenuItemImpl, paramView, paramViewGroup);
    }
    if (paramMenuItemImpl.isActionViewExpanded()) {}
    for (int i1 = 8;; i1 = 0)
    {
      localView.setVisibility(i1);
      paramMenuItemImpl = (ActionMenuView)paramViewGroup;
      paramView = localView.getLayoutParams();
      if (!paramMenuItemImpl.checkLayoutParams(paramView)) {
        localView.setLayoutParams(paramMenuItemImpl.a(paramView));
      }
      return localView;
    }
  }
  
  public void a(Context paramContext, MenuBuilder paramMenuBuilder)
  {
    super.a(paramContext, paramMenuBuilder);
    paramMenuBuilder = paramContext.getResources();
    paramContext = ActionBarPolicy.a(paramContext);
    if (!this.m) {
      this.l = paramContext.b();
    }
    if (!this.s) {
      this.n = paramContext.c();
    }
    if (!this.q) {
      this.p = paramContext.a();
    }
    int i1 = this.n;
    if (this.l)
    {
      if (this.i == null)
      {
        this.i = new OverflowMenuButton(this.a);
        if (this.k)
        {
          this.i.setImageDrawable(this.j);
          this.j = null;
          this.k = false;
        }
        int i2 = View.MeasureSpec.makeMeasureSpec(0, 0);
        this.i.measure(i2, i2);
      }
      i1 -= this.i.getMeasuredWidth();
    }
    for (;;)
    {
      this.o = i1;
      this.u = ((int)(56.0F * paramMenuBuilder.getDisplayMetrics().density));
      this.w = null;
      return;
      this.i = null;
    }
  }
  
  public void a(Configuration paramConfiguration)
  {
    if (!this.q) {
      this.p = this.b.getResources().getInteger(R.integer.abc_max_action_buttons);
    }
    if (this.c != null) {
      this.c.a(true);
    }
  }
  
  public void a(Drawable paramDrawable)
  {
    if (this.i != null)
    {
      this.i.setImageDrawable(paramDrawable);
      return;
    }
    this.k = true;
    this.j = paramDrawable;
  }
  
  public void a(Parcelable paramParcelable)
  {
    if (!(paramParcelable instanceof SavedState)) {}
    do
    {
      do
      {
        return;
        paramParcelable = (SavedState)paramParcelable;
      } while (paramParcelable.a <= 0);
      paramParcelable = this.c.findItem(paramParcelable.a);
    } while (paramParcelable == null);
    a((SubMenuBuilder)paramParcelable.getSubMenu());
  }
  
  public void a(MenuBuilder paramMenuBuilder, boolean paramBoolean)
  {
    h();
    super.a(paramMenuBuilder, paramBoolean);
  }
  
  public void a(MenuItemImpl paramMenuItemImpl, MenuView.ItemView paramItemView)
  {
    paramItemView.a(paramMenuItemImpl, 0);
    paramMenuItemImpl = (ActionMenuView)this.f;
    paramItemView = (ActionMenuItemView)paramItemView;
    paramItemView.setItemInvoker(paramMenuItemImpl);
    if (this.A == null) {
      this.A = new ActionMenuPopupCallback(null);
    }
    paramItemView.setPopupCallback(this.A);
  }
  
  public void a(ActionMenuView paramActionMenuView)
  {
    this.f = paramActionMenuView;
    paramActionMenuView.a(this.c);
  }
  
  public void a(boolean paramBoolean)
  {
    Object localObject = (ViewGroup)((View)this.f).getParent();
    if (localObject != null) {
      ActionBarTransition.a((ViewGroup)localObject);
    }
    super.a(paramBoolean);
    ((View)this.f).requestLayout();
    int i2;
    int i1;
    if (this.c != null)
    {
      localObject = this.c.k();
      i2 = ((ArrayList)localObject).size();
      i1 = 0;
      while (i1 < i2)
      {
        ActionProvider localActionProvider = ((MenuItemImpl)((ArrayList)localObject).get(i1)).a();
        if (localActionProvider != null) {
          localActionProvider.a(this);
        }
        i1 += 1;
      }
    }
    if (this.c != null)
    {
      localObject = this.c.l();
      i2 = 0;
      i1 = i2;
      if (this.l)
      {
        i1 = i2;
        if (localObject != null)
        {
          i1 = ((ArrayList)localObject).size();
          if (i1 != 1) {
            break label274;
          }
          if (((MenuItemImpl)((ArrayList)localObject).get(0)).isActionViewExpanded()) {
            break label269;
          }
          i1 = 1;
        }
      }
      label163:
      if (i1 == 0) {
        break label288;
      }
      if (this.i == null) {
        this.i = new OverflowMenuButton(this.a);
      }
      localObject = (ViewGroup)this.i.getParent();
      if (localObject != this.f)
      {
        if (localObject != null) {
          ((ViewGroup)localObject).removeView(this.i);
        }
        localObject = (ActionMenuView)this.f;
        ((ActionMenuView)localObject).addView(this.i, ((ActionMenuView)localObject).c());
      }
    }
    for (;;)
    {
      ((ActionMenuView)this.f).setOverflowReserved(this.l);
      return;
      localObject = null;
      break;
      label269:
      i1 = 0;
      break label163;
      label274:
      if (i1 > 0) {}
      for (i1 = 1;; i1 = 0) {
        break;
      }
      label288:
      if ((this.i != null) && (this.i.getParent() == this.f)) {
        ((ViewGroup)this.f).removeView(this.i);
      }
    }
  }
  
  public boolean a()
  {
    ArrayList localArrayList = this.c.i();
    int i10 = localArrayList.size();
    int i1 = this.p;
    int i9 = this.o;
    int i11 = View.MeasureSpec.makeMeasureSpec(0, 0);
    ViewGroup localViewGroup = (ViewGroup)this.f;
    int i2 = 0;
    int i4 = 0;
    int i7 = 0;
    int i5 = 0;
    int i3 = 0;
    int i6;
    if (i3 < i10)
    {
      localObject1 = (MenuItemImpl)localArrayList.get(i3);
      if (((MenuItemImpl)localObject1).l()) {
        i2 += 1;
      }
      for (;;)
      {
        i6 = i1;
        if (this.t)
        {
          i6 = i1;
          if (((MenuItemImpl)localObject1).isActionViewExpanded()) {
            i6 = 0;
          }
        }
        i3 += 1;
        i1 = i6;
        break;
        if (((MenuItemImpl)localObject1).k()) {
          i4 += 1;
        } else {
          i5 = 1;
        }
      }
    }
    i3 = i1;
    if (this.l) {
      if (i5 == 0)
      {
        i3 = i1;
        if (i2 + i4 <= i1) {}
      }
      else
      {
        i3 = i1 - 1;
      }
    }
    i3 -= i2;
    Object localObject1 = this.v;
    ((SparseBooleanArray)localObject1).clear();
    int i8 = 0;
    i2 = 0;
    if (this.r)
    {
      i2 = i9 / this.u;
      i1 = this.u;
      i8 = this.u + i9 % i1 / i2;
    }
    i1 = 0;
    i5 = i9;
    i9 = i1;
    i1 = i7;
    if (i9 < i10)
    {
      MenuItemImpl localMenuItemImpl = (MenuItemImpl)localArrayList.get(i9);
      Object localObject2;
      if (localMenuItemImpl.l())
      {
        localObject2 = a(localMenuItemImpl, this.w, localViewGroup);
        if (this.w == null) {
          this.w = ((View)localObject2);
        }
        if (this.r)
        {
          i2 -= ActionMenuView.a((View)localObject2, i8, i2, i11, 0);
          label310:
          i6 = ((View)localObject2).getMeasuredWidth();
          i4 = i5 - i6;
          i5 = i1;
          if (i1 == 0) {
            i5 = i6;
          }
          i1 = localMenuItemImpl.getGroupId();
          if (i1 != 0) {
            ((SparseBooleanArray)localObject1).put(i1, true);
          }
          localMenuItemImpl.d(true);
          i1 = i5;
        }
      }
      for (;;)
      {
        i9 += 1;
        i5 = i4;
        break;
        ((View)localObject2).measure(i11, i11);
        break label310;
        if (localMenuItemImpl.k())
        {
          int i12 = localMenuItemImpl.getGroupId();
          boolean bool = ((SparseBooleanArray)localObject1).get(i12);
          int i13;
          label438:
          int i14;
          if (((i3 > 0) || (bool)) && (i5 > 0) && ((!this.r) || (i2 > 0)))
          {
            i13 = 1;
            i7 = i2;
            i6 = i1;
            i14 = i13;
            i4 = i5;
            if (i13 != 0)
            {
              localObject2 = a(localMenuItemImpl, this.w, localViewGroup);
              if (this.w == null) {
                this.w = ((View)localObject2);
              }
              if (!this.r) {
                break label625;
              }
              i6 = ActionMenuView.a((View)localObject2, i8, i2, i11, 0);
              i4 = i2 - i6;
              i2 = i4;
              if (i6 == 0)
              {
                i13 = 0;
                i2 = i4;
              }
              label524:
              i7 = ((View)localObject2).getMeasuredWidth();
              i4 = i5 - i7;
              i6 = i1;
              if (i1 == 0) {
                i6 = i7;
              }
              if (!this.r) {
                break label642;
              }
              if (i4 < 0) {
                break label637;
              }
              i1 = 1;
              label563:
              i14 = i13 & i1;
              i7 = i2;
            }
            if ((i14 == 0) || (i12 == 0)) {
              break label669;
            }
            ((SparseBooleanArray)localObject1).put(i12, true);
            i1 = i3;
          }
          label625:
          label637:
          label642:
          label669:
          do
          {
            i3 = i1;
            if (i14 != 0) {
              i3 = i1 - 1;
            }
            localMenuItemImpl.d(i14);
            i2 = i7;
            i1 = i6;
            break;
            i13 = 0;
            break label438;
            ((View)localObject2).measure(i11, i11);
            break label524;
            i1 = 0;
            break label563;
            if (i4 + i6 > 0) {}
            for (i1 = 1;; i1 = 0)
            {
              i14 = i13 & i1;
              i7 = i2;
              break;
            }
            i1 = i3;
          } while (!bool);
          ((SparseBooleanArray)localObject1).put(i12, false);
          i2 = 0;
          for (;;)
          {
            i1 = i3;
            if (i2 >= i9) {
              break;
            }
            localObject2 = (MenuItemImpl)localArrayList.get(i2);
            i1 = i3;
            if (((MenuItemImpl)localObject2).getGroupId() == i12)
            {
              i1 = i3;
              if (((MenuItemImpl)localObject2).j()) {
                i1 = i3 + 1;
              }
              ((MenuItemImpl)localObject2).d(false);
            }
            i2 += 1;
            i3 = i1;
          }
        }
        localMenuItemImpl.d(false);
        i4 = i5;
      }
    }
    return true;
  }
  
  public boolean a(int paramInt, MenuItemImpl paramMenuItemImpl)
  {
    return paramMenuItemImpl.j();
  }
  
  public boolean a(SubMenuBuilder paramSubMenuBuilder)
  {
    if (!paramSubMenuBuilder.hasVisibleItems()) {}
    do
    {
      return false;
      for (localObject = paramSubMenuBuilder; ((SubMenuBuilder)localObject).s() != this.c; localObject = (SubMenuBuilder)((SubMenuBuilder)localObject).s()) {}
      View localView = a(((SubMenuBuilder)localObject).getItem());
      localObject = localView;
      if (localView != null) {
        break;
      }
    } while (this.i == null);
    Object localObject = this.i;
    this.h = paramSubMenuBuilder.getItem().getItemId();
    this.y = new ActionButtonSubmenu(this.b, paramSubMenuBuilder);
    this.y.a((View)localObject);
    this.y.d();
    super.a(paramSubMenuBuilder);
    return true;
  }
  
  public boolean a(ViewGroup paramViewGroup, int paramInt)
  {
    if (paramViewGroup.getChildAt(paramInt) == this.i) {
      return false;
    }
    return super.a(paramViewGroup, paramInt);
  }
  
  public void b(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      super.a(null);
      return;
    }
    this.c.b(false);
  }
  
  public Parcelable c()
  {
    SavedState localSavedState = new SavedState();
    localSavedState.a = this.h;
    return localSavedState;
  }
  
  public void c(boolean paramBoolean)
  {
    this.l = paramBoolean;
    this.m = true;
  }
  
  public void d(boolean paramBoolean)
  {
    this.t = paramBoolean;
  }
  
  public Drawable e()
  {
    if (this.i != null) {
      return this.i.getDrawable();
    }
    if (this.k) {
      return this.j;
    }
    return null;
  }
  
  public boolean f()
  {
    if ((this.l) && (!j()) && (this.c != null) && (this.f != null) && (this.z == null) && (!this.c.l().isEmpty()))
    {
      this.z = new OpenOverflowRunnable(new OverflowPopup(this.b, this.c, this.i, true));
      ((View)this.f).post(this.z);
      super.a(null);
      return true;
    }
    return false;
  }
  
  public boolean g()
  {
    if ((this.z != null) && (this.f != null))
    {
      ((View)this.f).removeCallbacks(this.z);
      this.z = null;
      return true;
    }
    OverflowPopup localOverflowPopup = this.x;
    if (localOverflowPopup != null)
    {
      localOverflowPopup.g();
      return true;
    }
    return false;
  }
  
  public boolean h()
  {
    return g() | i();
  }
  
  public boolean i()
  {
    if (this.y != null)
    {
      this.y.g();
      return true;
    }
    return false;
  }
  
  public boolean j()
  {
    return (this.x != null) && (this.x.h());
  }
  
  public boolean k()
  {
    return (this.z != null) || (j());
  }
  
  private class ActionButtonSubmenu
    extends MenuPopupHelper
  {
    private SubMenuBuilder d;
    
    public ActionButtonSubmenu(Context paramContext, SubMenuBuilder paramSubMenuBuilder)
    {
      super(paramSubMenuBuilder, null, false, R.attr.actionOverflowMenuStyle);
      this.d = paramSubMenuBuilder;
      boolean bool2;
      int j;
      int i;
      if (!((MenuItemImpl)paramSubMenuBuilder.getItem()).j())
      {
        if (ActionMenuPresenter.e(ActionMenuPresenter.this) == null)
        {
          paramContext = (View)ActionMenuPresenter.f(ActionMenuPresenter.this);
          a(paramContext);
        }
      }
      else
      {
        a(ActionMenuPresenter.this.g);
        bool2 = false;
        j = paramSubMenuBuilder.size();
        i = 0;
      }
      for (;;)
      {
        boolean bool1 = bool2;
        if (i < j)
        {
          this$1 = paramSubMenuBuilder.getItem(i);
          if ((ActionMenuPresenter.this.isVisible()) && (ActionMenuPresenter.this.getIcon() != null)) {
            bool1 = true;
          }
        }
        else
        {
          b(bool1);
          return;
          paramContext = ActionMenuPresenter.e(ActionMenuPresenter.this);
          break;
        }
        i += 1;
      }
    }
    
    public void onDismiss()
    {
      super.onDismiss();
      ActionMenuPresenter.a(ActionMenuPresenter.this, null);
      ActionMenuPresenter.this.h = 0;
    }
  }
  
  private class ActionMenuPopupCallback
    extends ActionMenuItemView.PopupCallback
  {
    private ActionMenuPopupCallback() {}
    
    public ListPopupWindow a()
    {
      if (ActionMenuPresenter.i(ActionMenuPresenter.this) != null) {
        return ActionMenuPresenter.i(ActionMenuPresenter.this).e();
      }
      return null;
    }
  }
  
  private class OpenOverflowRunnable
    implements Runnable
  {
    private ActionMenuPresenter.OverflowPopup b;
    
    public OpenOverflowRunnable(ActionMenuPresenter.OverflowPopup paramOverflowPopup)
    {
      this.b = paramOverflowPopup;
    }
    
    public void run()
    {
      ActionMenuPresenter.g(ActionMenuPresenter.this).f();
      View localView = (View)ActionMenuPresenter.h(ActionMenuPresenter.this);
      if ((localView != null) && (localView.getWindowToken() != null) && (this.b.f())) {
        ActionMenuPresenter.a(ActionMenuPresenter.this, this.b);
      }
      ActionMenuPresenter.a(ActionMenuPresenter.this, null);
    }
  }
  
  private class OverflowMenuButton
    extends AppCompatImageView
    implements ActionMenuView.ActionMenuChildView
  {
    private final float[] b = new float[2];
    
    public OverflowMenuButton(Context paramContext)
    {
      super(null, R.attr.actionOverflowButtonStyle);
      setClickable(true);
      setFocusable(true);
      setVisibility(0);
      setEnabled(true);
      setOnTouchListener(new ListPopupWindow.ForwardingListener(this)
      {
        public ListPopupWindow a()
        {
          if (ActionMenuPresenter.a(ActionMenuPresenter.this) == null) {
            return null;
          }
          return ActionMenuPresenter.a(ActionMenuPresenter.this).e();
        }
        
        public boolean b()
        {
          ActionMenuPresenter.this.f();
          return true;
        }
        
        public boolean c()
        {
          if (ActionMenuPresenter.b(ActionMenuPresenter.this) != null) {
            return false;
          }
          ActionMenuPresenter.this.g();
          return true;
        }
      });
    }
    
    public boolean c()
    {
      return false;
    }
    
    public boolean d()
    {
      return false;
    }
    
    public boolean performClick()
    {
      if (super.performClick()) {
        return true;
      }
      playSoundEffect(0);
      ActionMenuPresenter.this.f();
      return true;
    }
    
    protected boolean setFrame(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      boolean bool = super.setFrame(paramInt1, paramInt2, paramInt3, paramInt4);
      Drawable localDrawable1 = getDrawable();
      Drawable localDrawable2 = getBackground();
      if ((localDrawable1 != null) && (localDrawable2 != null))
      {
        int i = getWidth();
        paramInt2 = getHeight();
        paramInt1 = Math.max(i, paramInt2) / 2;
        int j = getPaddingLeft();
        int k = getPaddingRight();
        paramInt3 = getPaddingTop();
        paramInt4 = getPaddingBottom();
        i = (i + (j - k)) / 2;
        paramInt2 = (paramInt2 + (paramInt3 - paramInt4)) / 2;
        DrawableCompat.a(localDrawable2, i - paramInt1, paramInt2 - paramInt1, i + paramInt1, paramInt2 + paramInt1);
      }
      return bool;
    }
  }
  
  private class OverflowPopup
    extends MenuPopupHelper
  {
    public OverflowPopup(Context paramContext, MenuBuilder paramMenuBuilder, View paramView, boolean paramBoolean)
    {
      super(paramMenuBuilder, paramView, paramBoolean, R.attr.actionOverflowMenuStyle);
      a(8388613);
      a(ActionMenuPresenter.this.g);
    }
    
    public void onDismiss()
    {
      super.onDismiss();
      if (ActionMenuPresenter.c(ActionMenuPresenter.this) != null) {
        ActionMenuPresenter.d(ActionMenuPresenter.this).close();
      }
      ActionMenuPresenter.a(ActionMenuPresenter.this, null);
    }
  }
  
  private class PopupPresenterCallback
    implements MenuPresenter.Callback
  {
    private PopupPresenterCallback() {}
    
    public void a(MenuBuilder paramMenuBuilder, boolean paramBoolean)
    {
      if ((paramMenuBuilder instanceof SubMenuBuilder)) {
        ((SubMenuBuilder)paramMenuBuilder).p().b(false);
      }
      MenuPresenter.Callback localCallback = ActionMenuPresenter.this.d();
      if (localCallback != null) {
        localCallback.a(paramMenuBuilder, paramBoolean);
      }
    }
    
    public boolean a_(MenuBuilder paramMenuBuilder)
    {
      if (paramMenuBuilder == null) {
        return false;
      }
      ActionMenuPresenter.this.h = ((SubMenuBuilder)paramMenuBuilder).getItem().getItemId();
      MenuPresenter.Callback localCallback = ActionMenuPresenter.this.d();
      if (localCallback != null) {}
      for (boolean bool = localCallback.a_(paramMenuBuilder);; bool = false) {
        return bool;
      }
    }
  }
  
  private static class SavedState
    implements Parcelable
  {
    public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
    {
      public ActionMenuPresenter.SavedState a(Parcel paramAnonymousParcel)
      {
        return new ActionMenuPresenter.SavedState(paramAnonymousParcel);
      }
      
      public ActionMenuPresenter.SavedState[] a(int paramAnonymousInt)
      {
        return new ActionMenuPresenter.SavedState[paramAnonymousInt];
      }
    };
    public int a;
    
    SavedState() {}
    
    SavedState(Parcel paramParcel)
    {
      this.a = paramParcel.readInt();
    }
    
    public int describeContents()
    {
      return 0;
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      paramParcel.writeInt(this.a);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/ActionMenuPresenter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */