package android.support.v7.widget;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.DataSetObservable;
import android.os.AsyncTask;
import android.support.v4.os.AsyncTaskCompat;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

class ActivityChooserModel
  extends DataSetObservable
{
  private static final String a = ActivityChooserModel.class.getSimpleName();
  private static final Object b = new Object();
  private static final Map<String, ActivityChooserModel> c = new HashMap();
  private final Object d = new Object();
  private final List<ActivityResolveInfo> e = new ArrayList();
  private final List<HistoricalRecord> f = new ArrayList();
  private final Context g;
  private final String h;
  private Intent i;
  private ActivitySorter j = new DefaultSorter(null);
  private int k = 50;
  private boolean l = true;
  private boolean m = false;
  private boolean n = true;
  private boolean o = false;
  private OnChooseActivityListener p;
  
  private ActivityChooserModel(Context paramContext, String paramString)
  {
    this.g = paramContext.getApplicationContext();
    if ((!TextUtils.isEmpty(paramString)) && (!paramString.endsWith(".xml")))
    {
      this.h = (paramString + ".xml");
      return;
    }
    this.h = paramString;
  }
  
  public static ActivityChooserModel a(Context paramContext, String paramString)
  {
    synchronized (b)
    {
      ActivityChooserModel localActivityChooserModel2 = (ActivityChooserModel)c.get(paramString);
      ActivityChooserModel localActivityChooserModel1 = localActivityChooserModel2;
      if (localActivityChooserModel2 == null)
      {
        localActivityChooserModel1 = new ActivityChooserModel(paramContext, paramString);
        c.put(paramString, localActivityChooserModel1);
      }
      return localActivityChooserModel1;
    }
  }
  
  private boolean a(HistoricalRecord paramHistoricalRecord)
  {
    boolean bool = this.f.add(paramHistoricalRecord);
    if (bool)
    {
      this.n = true;
      j();
      e();
      g();
      notifyChanged();
    }
    return bool;
  }
  
  private void e()
  {
    if (!this.m) {
      throw new IllegalStateException("No preceding call to #readHistoricalData");
    }
    if (!this.n) {}
    do
    {
      return;
      this.n = false;
    } while (TextUtils.isEmpty(this.h));
    AsyncTaskCompat.a(new PersistHistoryAsyncTask(null), new Object[] { new ArrayList(this.f), this.h });
  }
  
  private void f()
  {
    boolean bool1 = h();
    boolean bool2 = i();
    j();
    if ((bool1 | bool2))
    {
      g();
      notifyChanged();
    }
  }
  
  private boolean g()
  {
    if ((this.j != null) && (this.i != null) && (!this.e.isEmpty()) && (!this.f.isEmpty()))
    {
      this.j.a(this.i, this.e, Collections.unmodifiableList(this.f));
      return true;
    }
    return false;
  }
  
  private boolean h()
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (this.o)
    {
      bool1 = bool2;
      if (this.i != null)
      {
        this.o = false;
        this.e.clear();
        List localList = this.g.getPackageManager().queryIntentActivities(this.i, 0);
        int i2 = localList.size();
        int i1 = 0;
        while (i1 < i2)
        {
          ResolveInfo localResolveInfo = (ResolveInfo)localList.get(i1);
          this.e.add(new ActivityResolveInfo(localResolveInfo));
          i1 += 1;
        }
        bool1 = true;
      }
    }
    return bool1;
  }
  
  private boolean i()
  {
    if ((this.l) && (this.n) && (!TextUtils.isEmpty(this.h)))
    {
      this.l = false;
      this.m = true;
      k();
      return true;
    }
    return false;
  }
  
  private void j()
  {
    int i2 = this.f.size() - this.k;
    if (i2 <= 0) {}
    for (;;)
    {
      return;
      this.n = true;
      int i1 = 0;
      while (i1 < i2)
      {
        HistoricalRecord localHistoricalRecord = (HistoricalRecord)this.f.remove(0);
        i1 += 1;
      }
    }
  }
  
  private void k()
  {
    try
    {
      FileInputStream localFileInputStream = this.g.openFileInput(this.h);
      try
      {
        XmlPullParser localXmlPullParser = Xml.newPullParser();
        localXmlPullParser.setInput(localFileInputStream, "UTF-8");
        for (i1 = 0; (i1 != 1) && (i1 != 2); i1 = localXmlPullParser.next()) {}
        if (!"historical-records".equals(localXmlPullParser.getName())) {
          throw new XmlPullParserException("Share records file does not start with historical-records tag.");
        }
      }
      catch (XmlPullParserException localXmlPullParserException)
      {
        int i1;
        Log.e(a, "Error reading historical recrod file: " + this.h, localXmlPullParserException);
        if (localFileInputStream != null)
        {
          try
          {
            localFileInputStream.close();
            return;
          }
          catch (IOException localIOException1)
          {
            return;
          }
          localList = this.f;
          localList.clear();
          do
          {
            i1 = localXmlPullParserException.next();
            if (i1 == 1)
            {
              if (localIOException1 == null) {
                break;
              }
              try
              {
                localIOException1.close();
                return;
              }
              catch (IOException localIOException2)
              {
                return;
              }
            }
          } while ((i1 == 3) || (i1 == 4));
          if (!"historical-record".equals(localXmlPullParserException.getName())) {
            throw new XmlPullParserException("Share records file not well-formed.");
          }
        }
      }
      catch (IOException localIOException5)
      {
        for (;;)
        {
          List localList;
          Log.e(a, "Error reading historical recrod file: " + this.h, localIOException5);
          if (localIOException2 == null) {
            break;
          }
          try
          {
            localIOException2.close();
            return;
          }
          catch (IOException localIOException3)
          {
            return;
          }
          localList.add(new HistoricalRecord(localIOException5.getAttributeValue(null, "activity"), Long.parseLong(localIOException5.getAttributeValue(null, "time")), Float.parseFloat(localIOException5.getAttributeValue(null, "weight"))));
        }
      }
      finally
      {
        if (localIOException3 != null) {}
        try
        {
          localIOException3.close();
          throw ((Throwable)localObject);
        }
        catch (IOException localIOException4)
        {
          for (;;) {}
        }
      }
      return;
    }
    catch (FileNotFoundException localFileNotFoundException) {}
  }
  
  public int a()
  {
    synchronized (this.d)
    {
      f();
      int i1 = this.e.size();
      return i1;
    }
  }
  
  public int a(ResolveInfo paramResolveInfo)
  {
    for (;;)
    {
      int i1;
      synchronized (this.d)
      {
        f();
        List localList = this.e;
        int i2 = localList.size();
        i1 = 0;
        if (i1 < i2)
        {
          if (((ActivityResolveInfo)localList.get(i1)).a == paramResolveInfo) {
            return i1;
          }
        }
        else {
          return -1;
        }
      }
      i1 += 1;
    }
  }
  
  public ResolveInfo a(int paramInt)
  {
    synchronized (this.d)
    {
      f();
      ResolveInfo localResolveInfo = ((ActivityResolveInfo)this.e.get(paramInt)).a;
      return localResolveInfo;
    }
  }
  
  public Intent b(int paramInt)
  {
    synchronized (this.d)
    {
      if (this.i == null) {
        return null;
      }
      f();
      Object localObject2 = (ActivityResolveInfo)this.e.get(paramInt);
      localObject2 = new ComponentName(((ActivityResolveInfo)localObject2).a.activityInfo.packageName, ((ActivityResolveInfo)localObject2).a.activityInfo.name);
      Intent localIntent1 = new Intent(this.i);
      localIntent1.setComponent((ComponentName)localObject2);
      if (this.p != null)
      {
        Intent localIntent2 = new Intent(localIntent1);
        if (this.p.a(this, localIntent2)) {
          return null;
        }
      }
      a(new HistoricalRecord((ComponentName)localObject2, System.currentTimeMillis(), 1.0F));
      return localIntent1;
    }
  }
  
  public ResolveInfo b()
  {
    synchronized (this.d)
    {
      f();
      if (!this.e.isEmpty())
      {
        ResolveInfo localResolveInfo = ((ActivityResolveInfo)this.e.get(0)).a;
        return localResolveInfo;
      }
      return null;
    }
  }
  
  public int c()
  {
    synchronized (this.d)
    {
      f();
      int i1 = this.f.size();
      return i1;
    }
  }
  
  public void c(int paramInt)
  {
    for (;;)
    {
      synchronized (this.d)
      {
        f();
        ActivityResolveInfo localActivityResolveInfo1 = (ActivityResolveInfo)this.e.get(paramInt);
        ActivityResolveInfo localActivityResolveInfo2 = (ActivityResolveInfo)this.e.get(0);
        if (localActivityResolveInfo2 != null)
        {
          f1 = localActivityResolveInfo2.b - localActivityResolveInfo1.b + 5.0F;
          a(new HistoricalRecord(new ComponentName(localActivityResolveInfo1.a.activityInfo.packageName, localActivityResolveInfo1.a.activityInfo.name), System.currentTimeMillis(), f1));
          return;
        }
      }
      float f1 = 1.0F;
    }
  }
  
  public static abstract interface ActivityChooserModelClient {}
  
  public final class ActivityResolveInfo
    implements Comparable<ActivityResolveInfo>
  {
    public final ResolveInfo a;
    public float b;
    
    public ActivityResolveInfo(ResolveInfo paramResolveInfo)
    {
      this.a = paramResolveInfo;
    }
    
    public int a(ActivityResolveInfo paramActivityResolveInfo)
    {
      return Float.floatToIntBits(paramActivityResolveInfo.b) - Float.floatToIntBits(this.b);
    }
    
    public boolean equals(Object paramObject)
    {
      if (this == paramObject) {}
      do
      {
        return true;
        if (paramObject == null) {
          return false;
        }
        if (getClass() != paramObject.getClass()) {
          return false;
        }
        paramObject = (ActivityResolveInfo)paramObject;
      } while (Float.floatToIntBits(this.b) == Float.floatToIntBits(((ActivityResolveInfo)paramObject).b));
      return false;
    }
    
    public int hashCode()
    {
      return Float.floatToIntBits(this.b) + 31;
    }
    
    public String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("[");
      localStringBuilder.append("resolveInfo:").append(this.a.toString());
      localStringBuilder.append("; weight:").append(new BigDecimal(this.b));
      localStringBuilder.append("]");
      return localStringBuilder.toString();
    }
  }
  
  public static abstract interface ActivitySorter
  {
    public abstract void a(Intent paramIntent, List<ActivityChooserModel.ActivityResolveInfo> paramList, List<ActivityChooserModel.HistoricalRecord> paramList1);
  }
  
  private final class DefaultSorter
    implements ActivityChooserModel.ActivitySorter
  {
    private final Map<ComponentName, ActivityChooserModel.ActivityResolveInfo> b = new HashMap();
    
    private DefaultSorter() {}
    
    public void a(Intent paramIntent, List<ActivityChooserModel.ActivityResolveInfo> paramList, List<ActivityChooserModel.HistoricalRecord> paramList1)
    {
      paramIntent = this.b;
      paramIntent.clear();
      int j = paramList.size();
      int i = 0;
      Object localObject;
      while (i < j)
      {
        localObject = (ActivityChooserModel.ActivityResolveInfo)paramList.get(i);
        ((ActivityChooserModel.ActivityResolveInfo)localObject).b = 0.0F;
        paramIntent.put(new ComponentName(((ActivityChooserModel.ActivityResolveInfo)localObject).a.activityInfo.packageName, ((ActivityChooserModel.ActivityResolveInfo)localObject).a.activityInfo.name), localObject);
        i += 1;
      }
      i = paramList1.size();
      float f1 = 1.0F;
      i -= 1;
      while (i >= 0)
      {
        localObject = (ActivityChooserModel.HistoricalRecord)paramList1.get(i);
        ActivityChooserModel.ActivityResolveInfo localActivityResolveInfo = (ActivityChooserModel.ActivityResolveInfo)paramIntent.get(((ActivityChooserModel.HistoricalRecord)localObject).a);
        float f2 = f1;
        if (localActivityResolveInfo != null)
        {
          localActivityResolveInfo.b += ((ActivityChooserModel.HistoricalRecord)localObject).c * f1;
          f2 = f1 * 0.95F;
        }
        i -= 1;
        f1 = f2;
      }
      Collections.sort(paramList);
    }
  }
  
  public static final class HistoricalRecord
  {
    public final ComponentName a;
    public final long b;
    public final float c;
    
    public HistoricalRecord(ComponentName paramComponentName, long paramLong, float paramFloat)
    {
      this.a = paramComponentName;
      this.b = paramLong;
      this.c = paramFloat;
    }
    
    public HistoricalRecord(String paramString, long paramLong, float paramFloat)
    {
      this(ComponentName.unflattenFromString(paramString), paramLong, paramFloat);
    }
    
    public boolean equals(Object paramObject)
    {
      if (this == paramObject) {}
      do
      {
        return true;
        if (paramObject == null) {
          return false;
        }
        if (getClass() != paramObject.getClass()) {
          return false;
        }
        paramObject = (HistoricalRecord)paramObject;
        if (this.a == null)
        {
          if (((HistoricalRecord)paramObject).a != null) {
            return false;
          }
        }
        else if (!this.a.equals(((HistoricalRecord)paramObject).a)) {
          return false;
        }
        if (this.b != ((HistoricalRecord)paramObject).b) {
          return false;
        }
      } while (Float.floatToIntBits(this.c) == Float.floatToIntBits(((HistoricalRecord)paramObject).c));
      return false;
    }
    
    public int hashCode()
    {
      if (this.a == null) {}
      for (int i = 0;; i = this.a.hashCode()) {
        return ((i + 31) * 31 + (int)(this.b ^ this.b >>> 32)) * 31 + Float.floatToIntBits(this.c);
      }
    }
    
    public String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("[");
      localStringBuilder.append("; activity:").append(this.a);
      localStringBuilder.append("; time:").append(this.b);
      localStringBuilder.append("; weight:").append(new BigDecimal(this.c));
      localStringBuilder.append("]");
      return localStringBuilder.toString();
    }
  }
  
  public static abstract interface OnChooseActivityListener
  {
    public abstract boolean a(ActivityChooserModel paramActivityChooserModel, Intent paramIntent);
  }
  
  private final class PersistHistoryAsyncTask
    extends AsyncTask<Object, Void, Void>
  {
    private PersistHistoryAsyncTask() {}
    
    /* Error */
    public Void a(Object... paramVarArgs)
    {
      // Byte code:
      //   0: aload_1
      //   1: iconst_0
      //   2: aaload
      //   3: checkcast 32	java/util/List
      //   6: astore 4
      //   8: aload_1
      //   9: iconst_1
      //   10: aaload
      //   11: checkcast 34	java/lang/String
      //   14: astore 5
      //   16: aload_0
      //   17: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:a	Landroid/support/v7/widget/ActivityChooserModel;
      //   20: invokestatic 37	android/support/v7/widget/ActivityChooserModel:a	(Landroid/support/v7/widget/ActivityChooserModel;)Landroid/content/Context;
      //   23: aload 5
      //   25: iconst_0
      //   26: invokevirtual 43	android/content/Context:openFileOutput	(Ljava/lang/String;I)Ljava/io/FileOutputStream;
      //   29: astore_1
      //   30: invokestatic 49	android/util/Xml:newSerializer	()Lorg/xmlpull/v1/XmlSerializer;
      //   33: astore 5
      //   35: aload 5
      //   37: aload_1
      //   38: aconst_null
      //   39: invokeinterface 55 3 0
      //   44: aload 5
      //   46: ldc 57
      //   48: iconst_1
      //   49: invokestatic 63	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
      //   52: invokeinterface 67 3 0
      //   57: aload 5
      //   59: aconst_null
      //   60: ldc 69
      //   62: invokeinterface 73 3 0
      //   67: pop
      //   68: aload 4
      //   70: invokeinterface 77 1 0
      //   75: istore_3
      //   76: iconst_0
      //   77: istore_2
      //   78: iload_2
      //   79: iload_3
      //   80: if_icmpge +133 -> 213
      //   83: aload 4
      //   85: iconst_0
      //   86: invokeinterface 81 2 0
      //   91: checkcast 83	android/support/v7/widget/ActivityChooserModel$HistoricalRecord
      //   94: astore 6
      //   96: aload 5
      //   98: aconst_null
      //   99: ldc 85
      //   101: invokeinterface 73 3 0
      //   106: pop
      //   107: aload 5
      //   109: aconst_null
      //   110: ldc 87
      //   112: aload 6
      //   114: getfield 90	android/support/v7/widget/ActivityChooserModel$HistoricalRecord:a	Landroid/content/ComponentName;
      //   117: invokevirtual 96	android/content/ComponentName:flattenToString	()Ljava/lang/String;
      //   120: invokeinterface 100 4 0
      //   125: pop
      //   126: aload 5
      //   128: aconst_null
      //   129: ldc 102
      //   131: aload 6
      //   133: getfield 106	android/support/v7/widget/ActivityChooserModel$HistoricalRecord:b	J
      //   136: invokestatic 109	java/lang/String:valueOf	(J)Ljava/lang/String;
      //   139: invokeinterface 100 4 0
      //   144: pop
      //   145: aload 5
      //   147: aconst_null
      //   148: ldc 111
      //   150: aload 6
      //   152: getfield 115	android/support/v7/widget/ActivityChooserModel$HistoricalRecord:c	F
      //   155: invokestatic 118	java/lang/String:valueOf	(F)Ljava/lang/String;
      //   158: invokeinterface 100 4 0
      //   163: pop
      //   164: aload 5
      //   166: aconst_null
      //   167: ldc 85
      //   169: invokeinterface 121 3 0
      //   174: pop
      //   175: iload_2
      //   176: iconst_1
      //   177: iadd
      //   178: istore_2
      //   179: goto -101 -> 78
      //   182: astore_1
      //   183: invokestatic 124	android/support/v7/widget/ActivityChooserModel:d	()Ljava/lang/String;
      //   186: new 126	java/lang/StringBuilder
      //   189: dup
      //   190: invokespecial 127	java/lang/StringBuilder:<init>	()V
      //   193: ldc -127
      //   195: invokevirtual 133	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   198: aload 5
      //   200: invokevirtual 133	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   203: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   206: aload_1
      //   207: invokestatic 142	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   210: pop
      //   211: aconst_null
      //   212: areturn
      //   213: aload 5
      //   215: aconst_null
      //   216: ldc 69
      //   218: invokeinterface 121 3 0
      //   223: pop
      //   224: aload 5
      //   226: invokeinterface 145 1 0
      //   231: aload_0
      //   232: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:a	Landroid/support/v7/widget/ActivityChooserModel;
      //   235: iconst_1
      //   236: invokestatic 148	android/support/v7/widget/ActivityChooserModel:a	(Landroid/support/v7/widget/ActivityChooserModel;Z)Z
      //   239: pop
      //   240: aload_1
      //   241: ifnull +7 -> 248
      //   244: aload_1
      //   245: invokevirtual 153	java/io/FileOutputStream:close	()V
      //   248: aconst_null
      //   249: areturn
      //   250: astore 4
      //   252: invokestatic 124	android/support/v7/widget/ActivityChooserModel:d	()Ljava/lang/String;
      //   255: new 126	java/lang/StringBuilder
      //   258: dup
      //   259: invokespecial 127	java/lang/StringBuilder:<init>	()V
      //   262: ldc -127
      //   264: invokevirtual 133	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   267: aload_0
      //   268: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:a	Landroid/support/v7/widget/ActivityChooserModel;
      //   271: invokestatic 156	android/support/v7/widget/ActivityChooserModel:b	(Landroid/support/v7/widget/ActivityChooserModel;)Ljava/lang/String;
      //   274: invokevirtual 133	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   277: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   280: aload 4
      //   282: invokestatic 142	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   285: pop
      //   286: aload_0
      //   287: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:a	Landroid/support/v7/widget/ActivityChooserModel;
      //   290: iconst_1
      //   291: invokestatic 148	android/support/v7/widget/ActivityChooserModel:a	(Landroid/support/v7/widget/ActivityChooserModel;Z)Z
      //   294: pop
      //   295: aload_1
      //   296: ifnull -48 -> 248
      //   299: aload_1
      //   300: invokevirtual 153	java/io/FileOutputStream:close	()V
      //   303: goto -55 -> 248
      //   306: astore_1
      //   307: goto -59 -> 248
      //   310: astore 4
      //   312: invokestatic 124	android/support/v7/widget/ActivityChooserModel:d	()Ljava/lang/String;
      //   315: new 126	java/lang/StringBuilder
      //   318: dup
      //   319: invokespecial 127	java/lang/StringBuilder:<init>	()V
      //   322: ldc -127
      //   324: invokevirtual 133	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   327: aload_0
      //   328: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:a	Landroid/support/v7/widget/ActivityChooserModel;
      //   331: invokestatic 156	android/support/v7/widget/ActivityChooserModel:b	(Landroid/support/v7/widget/ActivityChooserModel;)Ljava/lang/String;
      //   334: invokevirtual 133	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   337: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   340: aload 4
      //   342: invokestatic 142	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   345: pop
      //   346: aload_0
      //   347: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:a	Landroid/support/v7/widget/ActivityChooserModel;
      //   350: iconst_1
      //   351: invokestatic 148	android/support/v7/widget/ActivityChooserModel:a	(Landroid/support/v7/widget/ActivityChooserModel;Z)Z
      //   354: pop
      //   355: aload_1
      //   356: ifnull -108 -> 248
      //   359: aload_1
      //   360: invokevirtual 153	java/io/FileOutputStream:close	()V
      //   363: goto -115 -> 248
      //   366: astore_1
      //   367: goto -119 -> 248
      //   370: astore 4
      //   372: invokestatic 124	android/support/v7/widget/ActivityChooserModel:d	()Ljava/lang/String;
      //   375: new 126	java/lang/StringBuilder
      //   378: dup
      //   379: invokespecial 127	java/lang/StringBuilder:<init>	()V
      //   382: ldc -127
      //   384: invokevirtual 133	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   387: aload_0
      //   388: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:a	Landroid/support/v7/widget/ActivityChooserModel;
      //   391: invokestatic 156	android/support/v7/widget/ActivityChooserModel:b	(Landroid/support/v7/widget/ActivityChooserModel;)Ljava/lang/String;
      //   394: invokevirtual 133	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   397: invokevirtual 136	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   400: aload 4
      //   402: invokestatic 142	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   405: pop
      //   406: aload_0
      //   407: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:a	Landroid/support/v7/widget/ActivityChooserModel;
      //   410: iconst_1
      //   411: invokestatic 148	android/support/v7/widget/ActivityChooserModel:a	(Landroid/support/v7/widget/ActivityChooserModel;Z)Z
      //   414: pop
      //   415: aload_1
      //   416: ifnull -168 -> 248
      //   419: aload_1
      //   420: invokevirtual 153	java/io/FileOutputStream:close	()V
      //   423: goto -175 -> 248
      //   426: astore_1
      //   427: goto -179 -> 248
      //   430: astore 4
      //   432: aload_0
      //   433: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:a	Landroid/support/v7/widget/ActivityChooserModel;
      //   436: iconst_1
      //   437: invokestatic 148	android/support/v7/widget/ActivityChooserModel:a	(Landroid/support/v7/widget/ActivityChooserModel;Z)Z
      //   440: pop
      //   441: aload_1
      //   442: ifnull +7 -> 449
      //   445: aload_1
      //   446: invokevirtual 153	java/io/FileOutputStream:close	()V
      //   449: aload 4
      //   451: athrow
      //   452: astore_1
      //   453: goto -205 -> 248
      //   456: astore_1
      //   457: goto -8 -> 449
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	460	0	this	PersistHistoryAsyncTask
      //   0	460	1	paramVarArgs	Object[]
      //   77	102	2	i	int
      //   75	6	3	j	int
      //   6	78	4	localList	List
      //   250	31	4	localIllegalArgumentException	IllegalArgumentException
      //   310	31	4	localIllegalStateException	IllegalStateException
      //   370	31	4	localIOException	IOException
      //   430	20	4	localObject1	Object
      //   14	211	5	localObject2	Object
      //   94	57	6	localHistoricalRecord	ActivityChooserModel.HistoricalRecord
      // Exception table:
      //   from	to	target	type
      //   16	30	182	java/io/FileNotFoundException
      //   35	76	250	java/lang/IllegalArgumentException
      //   83	175	250	java/lang/IllegalArgumentException
      //   213	231	250	java/lang/IllegalArgumentException
      //   299	303	306	java/io/IOException
      //   35	76	310	java/lang/IllegalStateException
      //   83	175	310	java/lang/IllegalStateException
      //   213	231	310	java/lang/IllegalStateException
      //   359	363	366	java/io/IOException
      //   35	76	370	java/io/IOException
      //   83	175	370	java/io/IOException
      //   213	231	370	java/io/IOException
      //   419	423	426	java/io/IOException
      //   35	76	430	finally
      //   83	175	430	finally
      //   213	231	430	finally
      //   252	286	430	finally
      //   312	346	430	finally
      //   372	406	430	finally
      //   244	248	452	java/io/IOException
      //   445	449	456	java/io/IOException
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/ActivityChooserModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */