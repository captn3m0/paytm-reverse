package android.support.v7.widget;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.view.NestedScrollingParent;
import android.support.v4.view.NestedScrollingParentHelper;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.support.v4.widget.ScrollerCompat;
import android.support.v7.appcompat.R.attr;
import android.support.v7.appcompat.R.id;
import android.support.v7.view.menu.MenuPresenter.Callback;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window.Callback;

public class ActionBarOverlayLayout
  extends ViewGroup
  implements NestedScrollingParent, DecorContentParent
{
  static final int[] a = { R.attr.actionBarSize, 16842841 };
  private final Runnable A = new Runnable()
  {
    public void run()
    {
      ActionBarOverlayLayout.a(ActionBarOverlayLayout.this);
      ActionBarOverlayLayout.a(ActionBarOverlayLayout.this, ViewCompat.s(ActionBarOverlayLayout.c(ActionBarOverlayLayout.this)).c(-ActionBarOverlayLayout.c(ActionBarOverlayLayout.this).getHeight()).a(ActionBarOverlayLayout.b(ActionBarOverlayLayout.this)));
    }
  };
  private final NestedScrollingParentHelper B;
  private int b;
  private int c = 0;
  private ContentFrameLayout d;
  private ActionBarContainer e;
  private DecorToolbar f;
  private Drawable g;
  private boolean h;
  private boolean i;
  private boolean j;
  private boolean k;
  private boolean l;
  private int m;
  private int n;
  private final Rect o = new Rect();
  private final Rect p = new Rect();
  private final Rect q = new Rect();
  private final Rect r = new Rect();
  private final Rect s = new Rect();
  private final Rect t = new Rect();
  private ActionBarVisibilityCallback u;
  private final int v = 600;
  private ScrollerCompat w;
  private ViewPropertyAnimatorCompat x;
  private final ViewPropertyAnimatorListener y = new ViewPropertyAnimatorListenerAdapter()
  {
    public void b(View paramAnonymousView)
    {
      ActionBarOverlayLayout.a(ActionBarOverlayLayout.this, null);
      ActionBarOverlayLayout.a(ActionBarOverlayLayout.this, false);
    }
    
    public void c(View paramAnonymousView)
    {
      ActionBarOverlayLayout.a(ActionBarOverlayLayout.this, null);
      ActionBarOverlayLayout.a(ActionBarOverlayLayout.this, false);
    }
  };
  private final Runnable z = new Runnable()
  {
    public void run()
    {
      ActionBarOverlayLayout.a(ActionBarOverlayLayout.this);
      ActionBarOverlayLayout.a(ActionBarOverlayLayout.this, ViewCompat.s(ActionBarOverlayLayout.c(ActionBarOverlayLayout.this)).c(0.0F).a(ActionBarOverlayLayout.b(ActionBarOverlayLayout.this)));
    }
  };
  
  public ActionBarOverlayLayout(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public ActionBarOverlayLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a(paramContext);
    this.B = new NestedScrollingParentHelper(this);
  }
  
  private DecorToolbar a(View paramView)
  {
    if ((paramView instanceof DecorToolbar)) {
      return (DecorToolbar)paramView;
    }
    if ((paramView instanceof Toolbar)) {
      return ((Toolbar)paramView).getWrapper();
    }
    throw new IllegalStateException("Can't make a decor toolbar out of " + paramView.getClass().getSimpleName());
  }
  
  private void a(Context paramContext)
  {
    boolean bool2 = true;
    TypedArray localTypedArray = getContext().getTheme().obtainStyledAttributes(a);
    this.b = localTypedArray.getDimensionPixelSize(0, 0);
    this.g = localTypedArray.getDrawable(1);
    if (this.g == null)
    {
      bool1 = true;
      setWillNotDraw(bool1);
      localTypedArray.recycle();
      if (paramContext.getApplicationInfo().targetSdkVersion >= 19) {
        break label90;
      }
    }
    label90:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      this.h = bool1;
      this.w = ScrollerCompat.a(paramContext);
      return;
      bool1 = false;
      break;
    }
  }
  
  private boolean a(float paramFloat1, float paramFloat2)
  {
    boolean bool = false;
    this.w.a(0, 0, 0, (int)paramFloat2, 0, 0, Integer.MIN_VALUE, Integer.MAX_VALUE);
    if (this.w.e() > this.e.getHeight()) {
      bool = true;
    }
    return bool;
  }
  
  private boolean a(View paramView, Rect paramRect, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4)
  {
    boolean bool2 = false;
    paramView = (LayoutParams)paramView.getLayoutParams();
    boolean bool1 = bool2;
    if (paramBoolean1)
    {
      bool1 = bool2;
      if (paramView.leftMargin != paramRect.left)
      {
        bool1 = true;
        paramView.leftMargin = paramRect.left;
      }
    }
    paramBoolean1 = bool1;
    if (paramBoolean2)
    {
      paramBoolean1 = bool1;
      if (paramView.topMargin != paramRect.top)
      {
        paramBoolean1 = true;
        paramView.topMargin = paramRect.top;
      }
    }
    paramBoolean2 = paramBoolean1;
    if (paramBoolean4)
    {
      paramBoolean2 = paramBoolean1;
      if (paramView.rightMargin != paramRect.right)
      {
        paramBoolean2 = true;
        paramView.rightMargin = paramRect.right;
      }
    }
    paramBoolean1 = paramBoolean2;
    if (paramBoolean3)
    {
      paramBoolean1 = paramBoolean2;
      if (paramView.bottomMargin != paramRect.bottom)
      {
        paramBoolean1 = true;
        paramView.bottomMargin = paramRect.bottom;
      }
    }
    return paramBoolean1;
  }
  
  private void j()
  {
    removeCallbacks(this.z);
    removeCallbacks(this.A);
    if (this.x != null) {
      this.x.cancel();
    }
  }
  
  private void k()
  {
    j();
    postDelayed(this.z, 600L);
  }
  
  private void l()
  {
    j();
    postDelayed(this.A, 600L);
  }
  
  private void m()
  {
    j();
    this.z.run();
  }
  
  private void n()
  {
    j();
    this.A.run();
  }
  
  public LayoutParams a(AttributeSet paramAttributeSet)
  {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  public void a(int paramInt)
  {
    c();
    switch (paramInt)
    {
    default: 
      return;
    case 2: 
      this.f.f();
      return;
    case 5: 
      this.f.g();
      return;
    }
    setOverlayMode(true);
  }
  
  public boolean a()
  {
    return this.i;
  }
  
  protected LayoutParams b()
  {
    return new LayoutParams(-1, -1);
  }
  
  void c()
  {
    if (this.d == null)
    {
      this.d = ((ContentFrameLayout)findViewById(R.id.action_bar_activity_content));
      this.e = ((ActionBarContainer)findViewById(R.id.action_bar_container));
      this.f = a(findViewById(R.id.action_bar));
    }
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  public boolean d()
  {
    c();
    return this.f.h();
  }
  
  public void draw(Canvas paramCanvas)
  {
    super.draw(paramCanvas);
    if ((this.g != null) && (!this.h)) {
      if (this.e.getVisibility() != 0) {
        break label82;
      }
    }
    label82:
    for (int i1 = (int)(this.e.getBottom() + ViewCompat.p(this.e) + 0.5F);; i1 = 0)
    {
      this.g.setBounds(0, i1, getWidth(), this.g.getIntrinsicHeight() + i1);
      this.g.draw(paramCanvas);
      return;
    }
  }
  
  public boolean e()
  {
    c();
    return this.f.i();
  }
  
  public boolean f()
  {
    c();
    return this.f.j();
  }
  
  protected boolean fitSystemWindows(Rect paramRect)
  {
    c();
    if ((ViewCompat.v(this) & 0x100) != 0) {}
    for (;;)
    {
      boolean bool = a(this.e, paramRect, true, true, false, true);
      this.r.set(paramRect);
      ViewUtils.a(this, this.r, this.o);
      if (!this.p.equals(this.o))
      {
        bool = true;
        this.p.set(this.o);
      }
      if (bool) {
        requestLayout();
      }
      return true;
    }
  }
  
  public boolean g()
  {
    c();
    return this.f.k();
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return new LayoutParams(paramLayoutParams);
  }
  
  public int getActionBarHideOffset()
  {
    if (this.e != null) {
      return -(int)ViewCompat.p(this.e);
    }
    return 0;
  }
  
  public int getNestedScrollAxes()
  {
    return this.B.a();
  }
  
  public CharSequence getTitle()
  {
    c();
    return this.f.e();
  }
  
  public boolean h()
  {
    c();
    return this.f.l();
  }
  
  public void i()
  {
    c();
    this.f.n();
  }
  
  protected void onConfigurationChanged(Configuration paramConfiguration)
  {
    if (Build.VERSION.SDK_INT >= 8) {
      super.onConfigurationChanged(paramConfiguration);
    }
    a(getContext());
    ViewCompat.w(this);
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    j();
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    paramInt2 = getChildCount();
    paramInt3 = getPaddingLeft();
    getPaddingRight();
    paramInt4 = getPaddingTop();
    getPaddingBottom();
    paramInt1 = 0;
    while (paramInt1 < paramInt2)
    {
      View localView = getChildAt(paramInt1);
      if (localView.getVisibility() != 8)
      {
        LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
        int i1 = localView.getMeasuredWidth();
        int i2 = localView.getMeasuredHeight();
        int i3 = paramInt3 + localLayoutParams.leftMargin;
        int i4 = paramInt4 + localLayoutParams.topMargin;
        localView.layout(i3, i4, i3 + i1, i4 + i2);
      }
      paramInt1 += 1;
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    c();
    int i1 = 0;
    measureChildWithMargins(this.e, paramInt1, 0, paramInt2, 0);
    Object localObject = (LayoutParams)this.e.getLayoutParams();
    int i6 = Math.max(0, this.e.getMeasuredWidth() + ((LayoutParams)localObject).leftMargin + ((LayoutParams)localObject).rightMargin);
    int i5 = Math.max(0, this.e.getMeasuredHeight() + ((LayoutParams)localObject).topMargin + ((LayoutParams)localObject).bottomMargin);
    int i4 = ViewUtils.a(0, ViewCompat.l(this.e));
    int i2;
    int i3;
    if ((ViewCompat.v(this) & 0x100) != 0)
    {
      i2 = 1;
      if (i2 == 0) {
        break label436;
      }
      i3 = this.b;
      i1 = i3;
      if (this.j)
      {
        i1 = i3;
        if (this.e.getTabContainer() != null) {
          i1 = i3 + this.b;
        }
      }
      label149:
      this.q.set(this.o);
      this.s.set(this.r);
      if ((this.i) || (i2 != 0)) {
        break label459;
      }
      localObject = this.q;
      ((Rect)localObject).top += i1;
      localObject = this.q;
    }
    for (((Rect)localObject).bottom += 0;; ((Rect)localObject).bottom += 0)
    {
      a(this.d, this.q, true, true, true, true);
      if (!this.t.equals(this.s))
      {
        this.t.set(this.s);
        this.d.a(this.s);
      }
      measureChildWithMargins(this.d, paramInt1, 0, paramInt2, 0);
      localObject = (LayoutParams)this.d.getLayoutParams();
      i1 = Math.max(i6, this.d.getMeasuredWidth() + ((LayoutParams)localObject).leftMargin + ((LayoutParams)localObject).rightMargin);
      i2 = Math.max(i5, this.d.getMeasuredHeight() + ((LayoutParams)localObject).topMargin + ((LayoutParams)localObject).bottomMargin);
      i3 = ViewUtils.a(i4, ViewCompat.l(this.d));
      i4 = getPaddingLeft();
      i5 = getPaddingRight();
      i2 = Math.max(i2 + (getPaddingTop() + getPaddingBottom()), getSuggestedMinimumHeight());
      setMeasuredDimension(ViewCompat.a(Math.max(i1 + (i4 + i5), getSuggestedMinimumWidth()), paramInt1, i3), ViewCompat.a(i2, paramInt2, i3 << 16));
      return;
      i2 = 0;
      break;
      label436:
      if (this.e.getVisibility() == 8) {
        break label149;
      }
      i1 = this.e.getMeasuredHeight();
      break label149;
      label459:
      localObject = this.s;
      ((Rect)localObject).top += i1;
      localObject = this.s;
    }
  }
  
  public boolean onNestedFling(View paramView, float paramFloat1, float paramFloat2, boolean paramBoolean)
  {
    if ((!this.k) || (!paramBoolean)) {
      return false;
    }
    if (a(paramFloat1, paramFloat2)) {
      n();
    }
    for (;;)
    {
      this.l = true;
      return true;
      m();
    }
  }
  
  public boolean onNestedPreFling(View paramView, float paramFloat1, float paramFloat2)
  {
    return false;
  }
  
  public void onNestedPreScroll(View paramView, int paramInt1, int paramInt2, int[] paramArrayOfInt) {}
  
  public void onNestedScroll(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.m += paramInt2;
    setActionBarHideOffset(this.m);
  }
  
  public void onNestedScrollAccepted(View paramView1, View paramView2, int paramInt)
  {
    this.B.a(paramView1, paramView2, paramInt);
    this.m = getActionBarHideOffset();
    j();
    if (this.u != null) {
      this.u.p();
    }
  }
  
  public boolean onStartNestedScroll(View paramView1, View paramView2, int paramInt)
  {
    if (((paramInt & 0x2) == 0) || (this.e.getVisibility() != 0)) {
      return false;
    }
    return this.k;
  }
  
  public void onStopNestedScroll(View paramView)
  {
    if ((this.k) && (!this.l))
    {
      if (this.m > this.e.getHeight()) {
        break label49;
      }
      k();
    }
    for (;;)
    {
      if (this.u != null) {
        this.u.q();
      }
      return;
      label49:
      l();
    }
  }
  
  public void onWindowSystemUiVisibilityChanged(int paramInt)
  {
    boolean bool = true;
    if (Build.VERSION.SDK_INT >= 16) {
      super.onWindowSystemUiVisibilityChanged(paramInt);
    }
    c();
    int i3 = this.n;
    this.n = paramInt;
    int i1;
    int i2;
    if ((paramInt & 0x4) == 0)
    {
      i1 = 1;
      if ((paramInt & 0x100) == 0) {
        break label120;
      }
      i2 = 1;
      label49:
      if (this.u != null)
      {
        ActionBarVisibilityCallback localActionBarVisibilityCallback = this.u;
        if (i2 != 0) {
          break label125;
        }
        label66:
        localActionBarVisibilityCallback.j(bool);
        if ((i1 == 0) && (i2 != 0)) {
          break label131;
        }
        this.u.n();
      }
    }
    for (;;)
    {
      if ((((i3 ^ paramInt) & 0x100) != 0) && (this.u != null)) {
        ViewCompat.w(this);
      }
      return;
      i1 = 0;
      break;
      label120:
      i2 = 0;
      break label49;
      label125:
      bool = false;
      break label66;
      label131:
      this.u.o();
    }
  }
  
  protected void onWindowVisibilityChanged(int paramInt)
  {
    super.onWindowVisibilityChanged(paramInt);
    this.c = paramInt;
    if (this.u != null) {
      this.u.f(paramInt);
    }
  }
  
  public void setActionBarHideOffset(int paramInt)
  {
    j();
    paramInt = Math.max(0, Math.min(paramInt, this.e.getHeight()));
    ViewCompat.b(this.e, -paramInt);
  }
  
  public void setActionBarVisibilityCallback(ActionBarVisibilityCallback paramActionBarVisibilityCallback)
  {
    this.u = paramActionBarVisibilityCallback;
    if (getWindowToken() != null)
    {
      this.u.f(this.c);
      if (this.n != 0)
      {
        onWindowSystemUiVisibilityChanged(this.n);
        ViewCompat.w(this);
      }
    }
  }
  
  public void setHasNonEmbeddedTabs(boolean paramBoolean)
  {
    this.j = paramBoolean;
  }
  
  public void setHideOnContentScrollEnabled(boolean paramBoolean)
  {
    if (paramBoolean != this.k)
    {
      this.k = paramBoolean;
      if (!paramBoolean)
      {
        j();
        setActionBarHideOffset(0);
      }
    }
  }
  
  public void setIcon(int paramInt)
  {
    c();
    this.f.a(paramInt);
  }
  
  public void setIcon(Drawable paramDrawable)
  {
    c();
    this.f.a(paramDrawable);
  }
  
  public void setLogo(int paramInt)
  {
    c();
    this.f.b(paramInt);
  }
  
  public void setMenu(Menu paramMenu, MenuPresenter.Callback paramCallback)
  {
    c();
    this.f.a(paramMenu, paramCallback);
  }
  
  public void setMenuPrepared()
  {
    c();
    this.f.m();
  }
  
  public void setOverlayMode(boolean paramBoolean)
  {
    this.i = paramBoolean;
    if ((paramBoolean) && (getContext().getApplicationInfo().targetSdkVersion < 19)) {}
    for (paramBoolean = true;; paramBoolean = false)
    {
      this.h = paramBoolean;
      return;
    }
  }
  
  public void setShowingForActionMode(boolean paramBoolean) {}
  
  public void setUiOptions(int paramInt) {}
  
  public void setWindowCallback(Window.Callback paramCallback)
  {
    c();
    this.f.a(paramCallback);
  }
  
  public void setWindowTitle(CharSequence paramCharSequence)
  {
    c();
    this.f.a(paramCharSequence);
  }
  
  public boolean shouldDelayChildPressedState()
  {
    return false;
  }
  
  public static abstract interface ActionBarVisibilityCallback
  {
    public abstract void f(int paramInt);
    
    public abstract void j(boolean paramBoolean);
    
    public abstract void n();
    
    public abstract void o();
    
    public abstract void p();
    
    public abstract void q();
  }
  
  public static class LayoutParams
    extends ViewGroup.MarginLayoutParams
  {
    public LayoutParams(int paramInt1, int paramInt2)
    {
      super(paramInt2);
    }
    
    public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
    }
    
    public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/ActionBarOverlayLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */