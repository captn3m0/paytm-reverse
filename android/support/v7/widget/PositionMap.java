package android.support.v7.widget;

class PositionMap<E>
  implements Cloneable
{
  private static final Object a = new Object();
  private boolean b = false;
  private int[] c;
  private Object[] d;
  private int e;
  
  public PositionMap()
  {
    this(10);
  }
  
  public PositionMap(int paramInt)
  {
    if (paramInt == 0) {
      this.c = ContainerHelpers.b;
    }
    for (this.d = ContainerHelpers.d;; this.d = new Object[paramInt])
    {
      this.e = 0;
      return;
      paramInt = d(paramInt);
      this.c = new int[paramInt];
    }
  }
  
  static int c(int paramInt)
  {
    int i = 4;
    for (;;)
    {
      int j = paramInt;
      if (i < 32)
      {
        if (paramInt <= (1 << i) - 12) {
          j = (1 << i) - 12;
        }
      }
      else {
        return j;
      }
      i += 1;
    }
  }
  
  private void c()
  {
    int m = this.e;
    int j = 0;
    int[] arrayOfInt = this.c;
    Object[] arrayOfObject = this.d;
    int i = 0;
    while (i < m)
    {
      Object localObject = arrayOfObject[i];
      int k = j;
      if (localObject != a)
      {
        if (i != j)
        {
          arrayOfInt[j] = arrayOfInt[i];
          arrayOfObject[j] = localObject;
          arrayOfObject[i] = null;
        }
        k = j + 1;
      }
      i += 1;
      j = k;
    }
    this.b = false;
    this.e = j;
  }
  
  static int d(int paramInt)
  {
    return c(paramInt * 4) / 4;
  }
  
  public int a(int paramInt)
  {
    if (this.b) {
      c();
    }
    return this.c[paramInt];
  }
  
  public PositionMap<E> a()
  {
    Object localObject = null;
    try
    {
      PositionMap localPositionMap = (PositionMap)super.clone();
      localObject = localPositionMap;
      localPositionMap.c = ((int[])this.c.clone());
      localObject = localPositionMap;
      localPositionMap.d = ((Object[])this.d.clone());
      return localPositionMap;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException) {}
    return (PositionMap<E>)localObject;
  }
  
  public int b()
  {
    if (this.b) {
      c();
    }
    return this.e;
  }
  
  public E b(int paramInt)
  {
    if (this.b) {
      c();
    }
    return (E)this.d[paramInt];
  }
  
  public String toString()
  {
    if (b() <= 0) {
      return "{}";
    }
    StringBuilder localStringBuilder = new StringBuilder(this.e * 28);
    localStringBuilder.append('{');
    int i = 0;
    if (i < this.e)
    {
      if (i > 0) {
        localStringBuilder.append(", ");
      }
      localStringBuilder.append(a(i));
      localStringBuilder.append('=');
      Object localObject = b(i);
      if (localObject != this) {
        localStringBuilder.append(localObject);
      }
      for (;;)
      {
        i += 1;
        break;
        localStringBuilder.append("(this Map)");
      }
    }
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  static class ContainerHelpers
  {
    static final boolean[] a = new boolean[0];
    static final int[] b = new int[0];
    static final long[] c = new long[0];
    static final Object[] d = new Object[0];
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/PositionMap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */