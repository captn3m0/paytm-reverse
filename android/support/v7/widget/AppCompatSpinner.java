package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.TintableBackgroundView;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R.attr;
import android.support.v7.appcompat.R.layout;
import android.support.v7.appcompat.R.styleable;
import android.support.v7.view.ContextThemeWrapper;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

public class AppCompatSpinner
  extends Spinner
  implements TintableBackgroundView
{
  private static final boolean a;
  private static final boolean b;
  private static final int[] c;
  private AppCompatDrawableManager d;
  private AppCompatBackgroundHelper e;
  private Context f;
  private ListPopupWindow.ForwardingListener g;
  private SpinnerAdapter h;
  private boolean i;
  private DropdownPopup j;
  private int k;
  private final Rect l = new Rect();
  
  static
  {
    if (Build.VERSION.SDK_INT >= 23)
    {
      bool = true;
      a = bool;
      if (Build.VERSION.SDK_INT < 16) {
        break label45;
      }
    }
    label45:
    for (boolean bool = true;; bool = false)
    {
      b = bool;
      c = new int[] { 16843505 };
      return;
      bool = false;
      break;
    }
  }
  
  public AppCompatSpinner(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public AppCompatSpinner(Context paramContext, int paramInt)
  {
    this(paramContext, null, R.attr.spinnerStyle, paramInt);
  }
  
  public AppCompatSpinner(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, R.attr.spinnerStyle);
  }
  
  public AppCompatSpinner(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    this(paramContext, paramAttributeSet, paramInt, -1);
  }
  
  public AppCompatSpinner(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
  {
    this(paramContext, paramAttributeSet, paramInt1, paramInt2, null);
  }
  
  public AppCompatSpinner(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2, final Resources.Theme paramTheme)
  {
    super(paramContext, paramAttributeSet, paramInt1);
    TintTypedArray localTintTypedArray = TintTypedArray.a(paramContext, paramAttributeSet, R.styleable.Spinner, paramInt1, 0);
    this.d = AppCompatDrawableManager.a();
    this.e = new AppCompatBackgroundHelper(this, this.d);
    int n;
    Object localObject;
    if (paramTheme != null)
    {
      this.f = new ContextThemeWrapper(paramContext, paramTheme);
      if (this.f != null)
      {
        n = paramInt2;
        if (paramInt2 == -1)
        {
          if (Build.VERSION.SDK_INT < 11) {
            break label461;
          }
          localObject = null;
          paramTheme = null;
        }
      }
    }
    for (;;)
    {
      try
      {
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, c, paramInt1, 0);
        m = paramInt2;
        paramTheme = localTypedArray;
        localObject = localTypedArray;
        if (localTypedArray.hasValue(0))
        {
          paramTheme = localTypedArray;
          localObject = localTypedArray;
          m = localTypedArray.getInt(0, 0);
        }
        n = m;
        if (localTypedArray != null)
        {
          localTypedArray.recycle();
          n = m;
        }
      }
      catch (Exception localException)
      {
        int m;
        localObject = paramTheme;
        Log.i("AppCompatSpinner", "Could not read android:spinnerMode", localException);
        n = paramInt2;
        if (paramTheme == null) {
          continue;
        }
        paramTheme.recycle();
        n = paramInt2;
        continue;
      }
      finally
      {
        if (localObject == null) {
          continue;
        }
        ((TypedArray)localObject).recycle();
      }
      if (n == 1)
      {
        paramTheme = new DropdownPopup(this.f, paramAttributeSet, paramInt1);
        localObject = TintTypedArray.a(this.f, paramAttributeSet, R.styleable.Spinner, paramInt1, 0);
        this.k = ((TintTypedArray)localObject).f(R.styleable.Spinner_android_dropDownWidth, -2);
        paramTheme.a(((TintTypedArray)localObject).a(R.styleable.Spinner_android_popupBackground));
        paramTheme.a(localTintTypedArray.d(R.styleable.Spinner_android_prompt));
        ((TintTypedArray)localObject).a();
        this.j = paramTheme;
        this.g = new ListPopupWindow.ForwardingListener(this)
        {
          public ListPopupWindow a()
          {
            return paramTheme;
          }
          
          public boolean b()
          {
            if (!AppCompatSpinner.a(AppCompatSpinner.this).k()) {
              AppCompatSpinner.a(AppCompatSpinner.this).c();
            }
            return true;
          }
        };
      }
      paramTheme = localTintTypedArray.e(R.styleable.Spinner_android_entries);
      if (paramTheme != null)
      {
        paramContext = new ArrayAdapter(paramContext, 17367048, paramTheme);
        paramContext.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        setAdapter(paramContext);
      }
      localTintTypedArray.a();
      this.i = true;
      if (this.h != null)
      {
        setAdapter(this.h);
        this.h = null;
      }
      this.e.a(paramAttributeSet, paramInt1);
      return;
      m = localTintTypedArray.g(R.styleable.Spinner_popupTheme, 0);
      if (m != 0)
      {
        this.f = new ContextThemeWrapper(paramContext, m);
        break;
      }
      if (!a)
      {
        paramTheme = paramContext;
        this.f = paramTheme;
        break;
      }
      paramTheme = null;
      continue;
      label461:
      n = 1;
    }
  }
  
  private int a(SpinnerAdapter paramSpinnerAdapter, Drawable paramDrawable)
  {
    int n;
    if (paramSpinnerAdapter == null) {
      n = 0;
    }
    int m;
    do
    {
      return n;
      m = 0;
      View localView = null;
      int i1 = 0;
      int i4 = View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 0);
      int i5 = View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 0);
      n = Math.max(0, getSelectedItemPosition());
      int i6 = Math.min(paramSpinnerAdapter.getCount(), n + 15);
      n = Math.max(0, n - (15 - (i6 - n)));
      while (n < i6)
      {
        int i3 = paramSpinnerAdapter.getItemViewType(n);
        int i2 = i1;
        if (i3 != i1)
        {
          i2 = i3;
          localView = null;
        }
        localView = paramSpinnerAdapter.getView(n, localView, this);
        if (localView.getLayoutParams() == null) {
          localView.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        }
        localView.measure(i4, i5);
        m = Math.max(m, localView.getMeasuredWidth());
        n += 1;
        i1 = i2;
      }
      n = m;
    } while (paramDrawable == null);
    paramDrawable.getPadding(this.l);
    return m + (this.l.left + this.l.right);
  }
  
  protected void drawableStateChanged()
  {
    super.drawableStateChanged();
    if (this.e != null) {
      this.e.c();
    }
  }
  
  public int getDropDownHorizontalOffset()
  {
    if (this.j != null) {
      return this.j.f();
    }
    if (b) {
      return super.getDropDownHorizontalOffset();
    }
    return 0;
  }
  
  public int getDropDownVerticalOffset()
  {
    if (this.j != null) {
      return this.j.g();
    }
    if (b) {
      return super.getDropDownVerticalOffset();
    }
    return 0;
  }
  
  public int getDropDownWidth()
  {
    if (this.j != null) {
      return this.k;
    }
    if (b) {
      return super.getDropDownWidth();
    }
    return 0;
  }
  
  public Drawable getPopupBackground()
  {
    if (this.j != null) {
      return this.j.d();
    }
    if (b) {
      return super.getPopupBackground();
    }
    return null;
  }
  
  public Context getPopupContext()
  {
    if (this.j != null) {
      return this.f;
    }
    if (a) {
      return super.getPopupContext();
    }
    return null;
  }
  
  public CharSequence getPrompt()
  {
    if (this.j != null) {
      return this.j.a();
    }
    return super.getPrompt();
  }
  
  @Nullable
  public ColorStateList getSupportBackgroundTintList()
  {
    if (this.e != null) {
      return this.e.a();
    }
    return null;
  }
  
  @Nullable
  public PorterDuff.Mode getSupportBackgroundTintMode()
  {
    if (this.e != null) {
      return this.e.b();
    }
    return null;
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    if ((this.j != null) && (this.j.k())) {
      this.j.i();
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    if ((this.j != null) && (View.MeasureSpec.getMode(paramInt1) == Integer.MIN_VALUE)) {
      setMeasuredDimension(Math.min(Math.max(getMeasuredWidth(), a(getAdapter(), getBackground())), View.MeasureSpec.getSize(paramInt1)), getMeasuredHeight());
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if ((this.g != null) && (this.g.onTouch(this, paramMotionEvent))) {
      return true;
    }
    return super.onTouchEvent(paramMotionEvent);
  }
  
  public boolean performClick()
  {
    if (this.j != null)
    {
      if (!this.j.k()) {
        this.j.c();
      }
      return true;
    }
    return super.performClick();
  }
  
  public void setAdapter(SpinnerAdapter paramSpinnerAdapter)
  {
    if (!this.i) {
      this.h = paramSpinnerAdapter;
    }
    do
    {
      return;
      super.setAdapter(paramSpinnerAdapter);
    } while (this.j == null);
    if (this.f == null) {}
    for (Context localContext = getContext();; localContext = this.f)
    {
      this.j.a(new DropDownAdapter(paramSpinnerAdapter, localContext.getTheme()));
      return;
    }
  }
  
  public void setBackgroundDrawable(Drawable paramDrawable)
  {
    super.setBackgroundDrawable(paramDrawable);
    if (this.e != null) {
      this.e.a(paramDrawable);
    }
  }
  
  public void setBackgroundResource(@DrawableRes int paramInt)
  {
    super.setBackgroundResource(paramInt);
    if (this.e != null) {
      this.e.a(paramInt);
    }
  }
  
  public void setDropDownHorizontalOffset(int paramInt)
  {
    if (this.j != null) {
      this.j.b(paramInt);
    }
    while (!b) {
      return;
    }
    super.setDropDownHorizontalOffset(paramInt);
  }
  
  public void setDropDownVerticalOffset(int paramInt)
  {
    if (this.j != null) {
      this.j.c(paramInt);
    }
    while (!b) {
      return;
    }
    super.setDropDownVerticalOffset(paramInt);
  }
  
  public void setDropDownWidth(int paramInt)
  {
    if (this.j != null) {
      this.k = paramInt;
    }
    while (!b) {
      return;
    }
    super.setDropDownWidth(paramInt);
  }
  
  public void setPopupBackgroundDrawable(Drawable paramDrawable)
  {
    if (this.j != null) {
      this.j.a(paramDrawable);
    }
    while (!b) {
      return;
    }
    super.setPopupBackgroundDrawable(paramDrawable);
  }
  
  public void setPopupBackgroundResource(@DrawableRes int paramInt)
  {
    setPopupBackgroundDrawable(ContextCompat.getDrawable(getPopupContext(), paramInt));
  }
  
  public void setPrompt(CharSequence paramCharSequence)
  {
    if (this.j != null)
    {
      this.j.a(paramCharSequence);
      return;
    }
    super.setPrompt(paramCharSequence);
  }
  
  public void setSupportBackgroundTintList(@Nullable ColorStateList paramColorStateList)
  {
    if (this.e != null) {
      this.e.a(paramColorStateList);
    }
  }
  
  public void setSupportBackgroundTintMode(@Nullable PorterDuff.Mode paramMode)
  {
    if (this.e != null) {
      this.e.a(paramMode);
    }
  }
  
  private static class DropDownAdapter
    implements ListAdapter, SpinnerAdapter
  {
    private SpinnerAdapter a;
    private ListAdapter b;
    
    public DropDownAdapter(@Nullable SpinnerAdapter paramSpinnerAdapter, @Nullable Resources.Theme paramTheme)
    {
      this.a = paramSpinnerAdapter;
      if ((paramSpinnerAdapter instanceof ListAdapter)) {
        this.b = ((ListAdapter)paramSpinnerAdapter);
      }
      if (paramTheme != null)
      {
        if ((!AppCompatSpinner.a()) || (!(paramSpinnerAdapter instanceof android.widget.ThemedSpinnerAdapter))) {
          break label64;
        }
        paramSpinnerAdapter = (android.widget.ThemedSpinnerAdapter)paramSpinnerAdapter;
        if (paramSpinnerAdapter.getDropDownViewTheme() != paramTheme) {
          paramSpinnerAdapter.setDropDownViewTheme(paramTheme);
        }
      }
      label64:
      do
      {
        do
        {
          return;
        } while (!(paramSpinnerAdapter instanceof ThemedSpinnerAdapter));
        paramSpinnerAdapter = (ThemedSpinnerAdapter)paramSpinnerAdapter;
      } while (paramSpinnerAdapter.a() != null);
      paramSpinnerAdapter.a(paramTheme);
    }
    
    public boolean areAllItemsEnabled()
    {
      ListAdapter localListAdapter = this.b;
      if (localListAdapter != null) {
        return localListAdapter.areAllItemsEnabled();
      }
      return true;
    }
    
    public int getCount()
    {
      if (this.a == null) {
        return 0;
      }
      return this.a.getCount();
    }
    
    public View getDropDownView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (this.a == null) {
        return null;
      }
      return this.a.getDropDownView(paramInt, paramView, paramViewGroup);
    }
    
    public Object getItem(int paramInt)
    {
      if (this.a == null) {
        return null;
      }
      return this.a.getItem(paramInt);
    }
    
    public long getItemId(int paramInt)
    {
      if (this.a == null) {
        return -1L;
      }
      return this.a.getItemId(paramInt);
    }
    
    public int getItemViewType(int paramInt)
    {
      return 0;
    }
    
    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      return getDropDownView(paramInt, paramView, paramViewGroup);
    }
    
    public int getViewTypeCount()
    {
      return 1;
    }
    
    public boolean hasStableIds()
    {
      return (this.a != null) && (this.a.hasStableIds());
    }
    
    public boolean isEmpty()
    {
      return getCount() == 0;
    }
    
    public boolean isEnabled(int paramInt)
    {
      ListAdapter localListAdapter = this.b;
      if (localListAdapter != null) {
        return localListAdapter.isEnabled(paramInt);
      }
      return true;
    }
    
    public void registerDataSetObserver(DataSetObserver paramDataSetObserver)
    {
      if (this.a != null) {
        this.a.registerDataSetObserver(paramDataSetObserver);
      }
    }
    
    public void unregisterDataSetObserver(DataSetObserver paramDataSetObserver)
    {
      if (this.a != null) {
        this.a.unregisterDataSetObserver(paramDataSetObserver);
      }
    }
  }
  
  private class DropdownPopup
    extends ListPopupWindow
  {
    private CharSequence c;
    private ListAdapter d;
    private final Rect e = new Rect();
    
    public DropdownPopup(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
      super(paramAttributeSet, paramInt);
      a(AppCompatSpinner.this);
      a(true);
      a(0);
      a(new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
          AppCompatSpinner.this.setSelection(paramAnonymousInt);
          if (AppCompatSpinner.this.getOnItemClickListener() != null) {
            AppCompatSpinner.this.performItemClick(paramAnonymousView, paramAnonymousInt, AppCompatSpinner.DropdownPopup.a(AppCompatSpinner.DropdownPopup.this).getItemId(paramAnonymousInt));
          }
          AppCompatSpinner.DropdownPopup.this.i();
        }
      });
    }
    
    private boolean b(View paramView)
    {
      return (ViewCompat.H(paramView)) && (paramView.getGlobalVisibleRect(this.e));
    }
    
    public CharSequence a()
    {
      return this.c;
    }
    
    public void a(ListAdapter paramListAdapter)
    {
      super.a(paramListAdapter);
      this.d = paramListAdapter;
    }
    
    public void a(CharSequence paramCharSequence)
    {
      this.c = paramCharSequence;
    }
    
    void b()
    {
      Object localObject = d();
      int i = 0;
      int n;
      int i1;
      int i2;
      if (localObject != null)
      {
        ((Drawable)localObject).getPadding(AppCompatSpinner.b(AppCompatSpinner.this));
        if (ViewUtils.a(AppCompatSpinner.this))
        {
          i = AppCompatSpinner.b(AppCompatSpinner.this).right;
          n = AppCompatSpinner.this.getPaddingLeft();
          i1 = AppCompatSpinner.this.getPaddingRight();
          i2 = AppCompatSpinner.this.getWidth();
          if (AppCompatSpinner.c(AppCompatSpinner.this) != -2) {
            break label245;
          }
          int k = AppCompatSpinner.a(AppCompatSpinner.this, (SpinnerAdapter)this.d, d());
          int m = AppCompatSpinner.this.getContext().getResources().getDisplayMetrics().widthPixels - AppCompatSpinner.b(AppCompatSpinner.this).left - AppCompatSpinner.b(AppCompatSpinner.this).right;
          int j = k;
          if (k > m) {
            j = m;
          }
          f(Math.max(j, i2 - n - i1));
          label172:
          if (!ViewUtils.a(AppCompatSpinner.this)) {
            break label285;
          }
          i += i2 - i1 - h();
        }
      }
      for (;;)
      {
        b(i);
        return;
        i = -AppCompatSpinner.b(AppCompatSpinner.this).left;
        break;
        localObject = AppCompatSpinner.b(AppCompatSpinner.this);
        AppCompatSpinner.b(AppCompatSpinner.this).right = 0;
        ((Rect)localObject).left = 0;
        break;
        label245:
        if (AppCompatSpinner.c(AppCompatSpinner.this) == -1)
        {
          f(i2 - n - i1);
          break label172;
        }
        f(AppCompatSpinner.c(AppCompatSpinner.this));
        break label172;
        label285:
        i += n;
      }
    }
    
    public void c()
    {
      boolean bool = k();
      b();
      h(2);
      super.c();
      m().setChoiceMode(1);
      i(AppCompatSpinner.this.getSelectedItemPosition());
      if (bool) {}
      ViewTreeObserver localViewTreeObserver;
      do
      {
        return;
        localViewTreeObserver = AppCompatSpinner.this.getViewTreeObserver();
      } while (localViewTreeObserver == null);
      final ViewTreeObserver.OnGlobalLayoutListener local2 = new ViewTreeObserver.OnGlobalLayoutListener()
      {
        public void onGlobalLayout()
        {
          if (!AppCompatSpinner.DropdownPopup.a(AppCompatSpinner.DropdownPopup.this, AppCompatSpinner.this))
          {
            AppCompatSpinner.DropdownPopup.this.i();
            return;
          }
          AppCompatSpinner.DropdownPopup.this.b();
          AppCompatSpinner.DropdownPopup.b(AppCompatSpinner.DropdownPopup.this);
        }
      };
      localViewTreeObserver.addOnGlobalLayoutListener(local2);
      a(new PopupWindow.OnDismissListener()
      {
        public void onDismiss()
        {
          ViewTreeObserver localViewTreeObserver = AppCompatSpinner.this.getViewTreeObserver();
          if (localViewTreeObserver != null) {
            localViewTreeObserver.removeGlobalOnLayoutListener(local2);
          }
        }
      });
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/AppCompatSpinner.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */