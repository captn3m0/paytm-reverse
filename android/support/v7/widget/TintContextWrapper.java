package android.support.v7.widget;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDelegate;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class TintContextWrapper
  extends ContextWrapper
{
  private static final ArrayList<WeakReference<TintContextWrapper>> a = new ArrayList();
  private Resources b;
  private final Resources.Theme c;
  
  private TintContextWrapper(@NonNull Context paramContext)
  {
    super(paramContext);
    if (VectorEnabledTintResources.a())
    {
      this.c = getResources().newTheme();
      this.c.setTo(paramContext.getTheme());
      return;
    }
    this.c = null;
  }
  
  public static Context a(@NonNull Context paramContext)
  {
    if (b(paramContext))
    {
      int i = 0;
      int j = a.size();
      while (i < j)
      {
        Object localObject = (WeakReference)a.get(i);
        if (localObject != null) {}
        for (localObject = (TintContextWrapper)((WeakReference)localObject).get(); (localObject != null) && (((TintContextWrapper)localObject).getBaseContext() == paramContext); localObject = null) {
          return (Context)localObject;
        }
        i += 1;
      }
      paramContext = new TintContextWrapper(paramContext);
      a.add(new WeakReference(paramContext));
      return paramContext;
    }
    return paramContext;
  }
  
  private static boolean b(@NonNull Context paramContext)
  {
    if (((paramContext instanceof TintContextWrapper)) || ((paramContext.getResources() instanceof TintResources)) || ((paramContext.getResources() instanceof VectorEnabledTintResources))) {}
    while ((AppCompatDelegate.k()) && (Build.VERSION.SDK_INT > 20)) {
      return false;
    }
    return true;
  }
  
  public Resources getResources()
  {
    if (this.b == null) {
      if (this.c != null) {
        break label37;
      }
    }
    label37:
    for (Object localObject = new TintResources(this, super.getResources());; localObject = new VectorEnabledTintResources(this, super.getResources()))
    {
      this.b = ((Resources)localObject);
      return this.b;
    }
  }
  
  public Resources.Theme getTheme()
  {
    if (this.c == null) {
      return super.getTheme();
    }
    return this.c;
  }
  
  public void setTheme(int paramInt)
  {
    if (this.c == null)
    {
      super.setTheme(paramInt);
      return;
    }
    this.c.applyStyle(paramInt, true);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/TintContextWrapper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */