package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuBuilder.Callback;
import android.support.v7.view.menu.MenuBuilder.ItemInvoker;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.MenuPresenter.Callback;
import android.support.v7.view.menu.MenuView;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup.LayoutParams;
import android.view.accessibility.AccessibilityEvent;

public class ActionMenuView
  extends LinearLayoutCompat
  implements MenuBuilder.ItemInvoker, MenuView
{
  private MenuBuilder a;
  private Context b;
  private int c;
  private boolean d;
  private ActionMenuPresenter e;
  private MenuPresenter.Callback f;
  private MenuBuilder.Callback g;
  private boolean h;
  private int i;
  private int j;
  private int k;
  private OnMenuItemClickListener l;
  
  public ActionMenuView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public ActionMenuView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    setBaselineAligned(false);
    float f1 = paramContext.getResources().getDisplayMetrics().density;
    this.j = ((int)(56.0F * f1));
    this.k = ((int)(4.0F * f1));
    this.b = paramContext;
    this.c = 0;
  }
  
  static int a(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    int n = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(paramInt3) - paramInt4, View.MeasureSpec.getMode(paramInt3));
    ActionMenuItemView localActionMenuItemView;
    if ((paramView instanceof ActionMenuItemView))
    {
      localActionMenuItemView = (ActionMenuItemView)paramView;
      if ((localActionMenuItemView == null) || (!localActionMenuItemView.a())) {
        break label182;
      }
      paramInt4 = 1;
      label54:
      int m = 0;
      paramInt3 = m;
      if (paramInt2 > 0) {
        if (paramInt4 != 0)
        {
          paramInt3 = m;
          if (paramInt2 < 2) {}
        }
        else
        {
          paramView.measure(View.MeasureSpec.makeMeasureSpec(paramInt1 * paramInt2, Integer.MIN_VALUE), n);
          m = paramView.getMeasuredWidth();
          paramInt3 = m / paramInt1;
          paramInt2 = paramInt3;
          if (m % paramInt1 != 0) {
            paramInt2 = paramInt3 + 1;
          }
          paramInt3 = paramInt2;
          if (paramInt4 != 0)
          {
            paramInt3 = paramInt2;
            if (paramInt2 < 2) {
              paramInt3 = 2;
            }
          }
        }
      }
      if ((localLayoutParams.a) || (paramInt4 == 0)) {
        break label188;
      }
    }
    label182:
    label188:
    for (boolean bool = true;; bool = false)
    {
      localLayoutParams.d = bool;
      localLayoutParams.b = paramInt3;
      paramView.measure(View.MeasureSpec.makeMeasureSpec(paramInt3 * paramInt1, 1073741824), n);
      return paramInt3;
      localActionMenuItemView = null;
      break;
      paramInt4 = 0;
      break label54;
    }
  }
  
  private void c(int paramInt1, int paramInt2)
  {
    int i11 = View.MeasureSpec.getMode(paramInt2);
    paramInt1 = View.MeasureSpec.getSize(paramInt1);
    int i10 = View.MeasureSpec.getSize(paramInt2);
    int m = getPaddingLeft();
    int n = getPaddingRight();
    int i16 = getPaddingTop() + getPaddingBottom();
    int i12 = getChildMeasureSpec(paramInt2, i16, -2);
    int i13 = paramInt1 - (m + n);
    paramInt1 = i13 / this.j;
    paramInt2 = this.j;
    if (paramInt1 == 0)
    {
      setMeasuredDimension(i13, 0);
      return;
    }
    int i14 = this.j + i13 % paramInt2 / paramInt1;
    m = 0;
    int i2 = 0;
    int i1 = 0;
    int i3 = 0;
    n = 0;
    long l1 = 0L;
    int i15 = getChildCount();
    int i4 = 0;
    Object localObject;
    long l2;
    int i5;
    int i6;
    LayoutParams localLayoutParams;
    label272:
    int i7;
    int i8;
    int i9;
    while (i4 < i15)
    {
      localObject = getChildAt(i4);
      if (((View)localObject).getVisibility() == 8)
      {
        l2 = l1;
        i5 = n;
        i4 += 1;
        n = i5;
        l1 = l2;
      }
      else
      {
        boolean bool = localObject instanceof ActionMenuItemView;
        i6 = i3 + 1;
        if (bool) {
          ((View)localObject).setPadding(this.k, 0, this.k, 0);
        }
        localLayoutParams = (LayoutParams)((View)localObject).getLayoutParams();
        localLayoutParams.f = false;
        localLayoutParams.c = 0;
        localLayoutParams.b = 0;
        localLayoutParams.d = false;
        localLayoutParams.leftMargin = 0;
        localLayoutParams.rightMargin = 0;
        if ((bool) && (((ActionMenuItemView)localObject).a()))
        {
          bool = true;
          localLayoutParams.e = bool;
          if (!localLayoutParams.a) {
            break label430;
          }
        }
        label430:
        for (paramInt2 = 1;; paramInt2 = paramInt1)
        {
          int i17 = a((View)localObject, i14, paramInt2, i12, i16);
          i7 = Math.max(i2, i17);
          paramInt2 = i1;
          if (localLayoutParams.d) {
            paramInt2 = i1 + 1;
          }
          if (localLayoutParams.a) {
            n = 1;
          }
          i8 = paramInt1 - i17;
          i9 = Math.max(m, ((View)localObject).getMeasuredHeight());
          paramInt1 = i8;
          i1 = paramInt2;
          i5 = n;
          i2 = i7;
          m = i9;
          l2 = l1;
          i3 = i6;
          if (i17 != 1) {
            break;
          }
          l2 = l1 | 1 << i4;
          paramInt1 = i8;
          i1 = paramInt2;
          i5 = n;
          i2 = i7;
          m = i9;
          i3 = i6;
          break;
          bool = false;
          break label272;
        }
      }
    }
    long l3;
    if ((n != 0) && (i3 == 2))
    {
      i4 = 1;
      paramInt2 = 0;
      i5 = paramInt1;
      l2 = l1;
      if (i1 <= 0) {
        break label641;
      }
      l2 = l1;
      if (i5 <= 0) {
        break label641;
      }
      i6 = Integer.MAX_VALUE;
      l3 = 0L;
      i9 = 0;
      i7 = 0;
      label485:
      if (i7 >= i15) {
        break label623;
      }
      localObject = (LayoutParams)getChildAt(i7).getLayoutParams();
      if (((LayoutParams)localObject).d) {
        break label551;
      }
      l2 = l3;
      paramInt1 = i9;
      i8 = i6;
    }
    for (;;)
    {
      i7 += 1;
      i6 = i8;
      i9 = paramInt1;
      l3 = l2;
      break label485;
      i4 = 0;
      break;
      label551:
      if (((LayoutParams)localObject).b < i6)
      {
        i8 = ((LayoutParams)localObject).b;
        l2 = 1 << i7;
        paramInt1 = 1;
      }
      else
      {
        i8 = i6;
        paramInt1 = i9;
        l2 = l3;
        if (((LayoutParams)localObject).b == i6)
        {
          l2 = l3 | 1 << i7;
          paramInt1 = i9 + 1;
          i8 = i6;
        }
      }
    }
    label623:
    l1 |= l3;
    if (i9 > i5)
    {
      l2 = l1;
      label641:
      if ((n != 0) || (i3 != 1)) {
        break label1004;
      }
      paramInt1 = 1;
      label654:
      n = paramInt2;
      if (i5 <= 0) {
        break label1160;
      }
      n = paramInt2;
      if (l2 == 0L) {
        break label1160;
      }
      if ((i5 >= i3 - 1) && (paramInt1 == 0))
      {
        n = paramInt2;
        if (i2 <= 1) {
          break label1160;
        }
      }
      float f3 = Long.bitCount(l2);
      float f2 = f3;
      if (paramInt1 == 0)
      {
        float f1 = f3;
        if ((1L & l2) != 0L)
        {
          f1 = f3;
          if (!((LayoutParams)getChildAt(0).getLayoutParams()).e) {
            f1 = f3 - 0.5F;
          }
        }
        f2 = f1;
        if ((1 << i15 - 1 & l2) != 0L)
        {
          f2 = f1;
          if (!((LayoutParams)getChildAt(i15 - 1).getLayoutParams()).e) {
            f2 = f1 - 0.5F;
          }
        }
      }
      if (f2 <= 0.0F) {
        break label1009;
      }
      n = (int)(i5 * i14 / f2);
      label814:
      i1 = 0;
      label817:
      if (i1 >= i15) {
        break label1157;
      }
      if ((1 << i1 & l2) != 0L) {
        break label1015;
      }
      paramInt1 = paramInt2;
    }
    for (;;)
    {
      i1 += 1;
      paramInt2 = paramInt1;
      break label817;
      paramInt1 = 0;
      if (paramInt1 < i15)
      {
        localObject = getChildAt(paramInt1);
        localLayoutParams = (LayoutParams)((View)localObject).getLayoutParams();
        if ((1 << paramInt1 & l3) == 0L)
        {
          paramInt2 = i5;
          l2 = l1;
          if (localLayoutParams.b == i6 + 1)
          {
            l2 = l1 | 1 << paramInt1;
            paramInt2 = i5;
          }
        }
        for (;;)
        {
          paramInt1 += 1;
          i5 = paramInt2;
          l1 = l2;
          break;
          if ((i4 != 0) && (localLayoutParams.e) && (i5 == 1)) {
            ((View)localObject).setPadding(this.k + i14, 0, this.k, 0);
          }
          localLayoutParams.b += 1;
          localLayoutParams.f = true;
          paramInt2 = i5 - 1;
          l2 = l1;
        }
      }
      paramInt2 = 1;
      break;
      label1004:
      paramInt1 = 0;
      break label654;
      label1009:
      n = 0;
      break label814;
      label1015:
      localObject = getChildAt(i1);
      localLayoutParams = (LayoutParams)((View)localObject).getLayoutParams();
      if ((localObject instanceof ActionMenuItemView))
      {
        localLayoutParams.c = n;
        localLayoutParams.f = true;
        if ((i1 == 0) && (!localLayoutParams.e)) {
          localLayoutParams.leftMargin = (-n / 2);
        }
        paramInt1 = 1;
      }
      else if (localLayoutParams.a)
      {
        localLayoutParams.c = n;
        localLayoutParams.f = true;
        localLayoutParams.rightMargin = (-n / 2);
        paramInt1 = 1;
      }
      else
      {
        if (i1 != 0) {
          localLayoutParams.leftMargin = (n / 2);
        }
        paramInt1 = paramInt2;
        if (i1 != i15 - 1)
        {
          localLayoutParams.rightMargin = (n / 2);
          paramInt1 = paramInt2;
        }
      }
    }
    label1157:
    n = paramInt2;
    label1160:
    if (n != 0)
    {
      paramInt1 = 0;
      if (paramInt1 < i15)
      {
        localObject = getChildAt(paramInt1);
        localLayoutParams = (LayoutParams)((View)localObject).getLayoutParams();
        if (!localLayoutParams.f) {}
        for (;;)
        {
          paramInt1 += 1;
          break;
          ((View)localObject).measure(View.MeasureSpec.makeMeasureSpec(localLayoutParams.b * i14 + localLayoutParams.c, 1073741824), i12);
        }
      }
    }
    paramInt1 = i10;
    if (i11 != 1073741824) {
      paramInt1 = m;
    }
    setMeasuredDimension(i13, paramInt1);
  }
  
  public LayoutParams a(AttributeSet paramAttributeSet)
  {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected LayoutParams a(ViewGroup.LayoutParams paramLayoutParams)
  {
    if (paramLayoutParams != null)
    {
      if ((paramLayoutParams instanceof LayoutParams)) {}
      for (paramLayoutParams = new LayoutParams((LayoutParams)paramLayoutParams);; paramLayoutParams = new LayoutParams(paramLayoutParams))
      {
        if (paramLayoutParams.h <= 0) {
          paramLayoutParams.h = 16;
        }
        return paramLayoutParams;
      }
    }
    return b();
  }
  
  public void a(MenuBuilder paramMenuBuilder)
  {
    this.a = paramMenuBuilder;
  }
  
  public boolean a()
  {
    return this.d;
  }
  
  protected boolean a(int paramInt)
  {
    boolean bool2;
    if (paramInt == 0) {
      bool2 = false;
    }
    View localView2;
    boolean bool1;
    do
    {
      do
      {
        return bool2;
        View localView1 = getChildAt(paramInt - 1);
        localView2 = getChildAt(paramInt);
        bool2 = false;
        bool1 = bool2;
        if (paramInt < getChildCount())
        {
          bool1 = bool2;
          if ((localView1 instanceof ActionMenuChildView)) {
            bool1 = false | ((ActionMenuChildView)localView1).d();
          }
        }
        bool2 = bool1;
      } while (paramInt <= 0);
      bool2 = bool1;
    } while (!(localView2 instanceof ActionMenuChildView));
    return bool1 | ((ActionMenuChildView)localView2).c();
  }
  
  public boolean a(MenuItemImpl paramMenuItemImpl)
  {
    return this.a.a(paramMenuItemImpl, 0);
  }
  
  protected LayoutParams b()
  {
    LayoutParams localLayoutParams = new LayoutParams(-2, -2);
    localLayoutParams.h = 16;
    return localLayoutParams;
  }
  
  public LayoutParams c()
  {
    LayoutParams localLayoutParams = b();
    localLayoutParams.a = true;
    return localLayoutParams;
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return (paramLayoutParams != null) && ((paramLayoutParams instanceof LayoutParams));
  }
  
  public MenuBuilder d()
  {
    return this.a;
  }
  
  public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
  {
    return false;
  }
  
  public boolean e()
  {
    return (this.e != null) && (this.e.f());
  }
  
  public boolean f()
  {
    return (this.e != null) && (this.e.g());
  }
  
  public boolean g()
  {
    return (this.e != null) && (this.e.j());
  }
  
  public Menu getMenu()
  {
    ActionMenuPresenter localActionMenuPresenter;
    if (this.a == null)
    {
      localObject = getContext();
      this.a = new MenuBuilder((Context)localObject);
      this.a.a(new MenuBuilderCallback(null));
      this.e = new ActionMenuPresenter((Context)localObject);
      this.e.c(true);
      localActionMenuPresenter = this.e;
      if (this.f == null) {
        break label110;
      }
    }
    label110:
    for (Object localObject = this.f;; localObject = new ActionMenuPresenterCallback(null))
    {
      localActionMenuPresenter.a((MenuPresenter.Callback)localObject);
      this.a.a(this.e, this.b);
      this.e.a(this);
      return this.a;
    }
  }
  
  @Nullable
  public Drawable getOverflowIcon()
  {
    getMenu();
    return this.e.e();
  }
  
  public int getPopupTheme()
  {
    return this.c;
  }
  
  public int getWindowAnimations()
  {
    return 0;
  }
  
  public boolean h()
  {
    return (this.e != null) && (this.e.k());
  }
  
  public void i()
  {
    if (this.e != null) {
      this.e.h();
    }
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    if (Build.VERSION.SDK_INT >= 8) {
      super.onConfigurationChanged(paramConfiguration);
    }
    if (this.e != null)
    {
      this.e.a(false);
      if (this.e.j())
      {
        this.e.g();
        this.e.f();
      }
    }
  }
  
  public void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    i();
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (!this.h)
    {
      super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
      return;
    }
    int i5 = getChildCount();
    int i4 = (paramInt4 - paramInt2) / 2;
    int i6 = getDividerWidth();
    int n = 0;
    paramInt4 = 0;
    paramInt2 = paramInt3 - paramInt1 - getPaddingRight() - getPaddingLeft();
    int i1 = 0;
    paramBoolean = ViewUtils.a(this);
    int m = 0;
    View localView;
    LayoutParams localLayoutParams;
    if (m < i5)
    {
      localView = getChildAt(m);
      if (localView.getVisibility() == 8) {}
      for (;;)
      {
        m += 1;
        break;
        localLayoutParams = (LayoutParams)localView.getLayoutParams();
        if (localLayoutParams.a)
        {
          i2 = localView.getMeasuredWidth();
          i1 = i2;
          if (a(m)) {
            i1 = i2 + i6;
          }
          int i7 = localView.getMeasuredHeight();
          int i3;
          if (paramBoolean)
          {
            i2 = getPaddingLeft() + localLayoutParams.leftMargin;
            i3 = i2 + i1;
          }
          for (;;)
          {
            int i8 = i4 - i7 / 2;
            localView.layout(i2, i8, i3, i8 + i7);
            paramInt2 -= i1;
            i1 = 1;
            break;
            i3 = getWidth() - getPaddingRight() - localLayoutParams.rightMargin;
            i2 = i3 - i1;
          }
        }
        int i2 = localView.getMeasuredWidth() + localLayoutParams.leftMargin + localLayoutParams.rightMargin;
        n += i2;
        i2 = paramInt2 - i2;
        paramInt2 = n;
        if (a(m)) {
          paramInt2 = n + i6;
        }
        paramInt4 += 1;
        n = paramInt2;
        paramInt2 = i2;
      }
    }
    if ((i5 == 1) && (i1 == 0))
    {
      localView = getChildAt(0);
      paramInt2 = localView.getMeasuredWidth();
      paramInt4 = localView.getMeasuredHeight();
      paramInt1 = (paramInt3 - paramInt1) / 2 - paramInt2 / 2;
      paramInt3 = i4 - paramInt4 / 2;
      localView.layout(paramInt1, paramInt3, paramInt1 + paramInt2, paramInt3 + paramInt4);
      return;
    }
    if (i1 != 0)
    {
      paramInt1 = 0;
      label383:
      paramInt1 = paramInt4 - paramInt1;
      if (paramInt1 <= 0) {
        break label481;
      }
      paramInt1 = paramInt2 / paramInt1;
      label396:
      paramInt4 = Math.max(0, paramInt1);
      if (!paramBoolean) {
        break label552;
      }
      paramInt2 = getWidth() - getPaddingRight();
      paramInt1 = 0;
      label419:
      if (paramInt1 < i5)
      {
        localView = getChildAt(paramInt1);
        localLayoutParams = (LayoutParams)localView.getLayoutParams();
        paramInt3 = paramInt2;
        if (localView.getVisibility() != 8) {
          if (!localLayoutParams.a) {
            break label486;
          }
        }
      }
    }
    for (paramInt3 = paramInt2;; paramInt3 = paramInt2 - (localLayoutParams.leftMargin + paramInt3 + paramInt4))
    {
      paramInt1 += 1;
      paramInt2 = paramInt3;
      break label419;
      break;
      paramInt1 = 1;
      break label383;
      label481:
      paramInt1 = 0;
      break label396;
      label486:
      paramInt2 -= localLayoutParams.rightMargin;
      paramInt3 = localView.getMeasuredWidth();
      m = localView.getMeasuredHeight();
      n = i4 - m / 2;
      localView.layout(paramInt2 - paramInt3, n, paramInt2, n + m);
    }
    label552:
    paramInt2 = getPaddingLeft();
    paramInt1 = 0;
    label559:
    if (paramInt1 < i5)
    {
      localView = getChildAt(paramInt1);
      localLayoutParams = (LayoutParams)localView.getLayoutParams();
      paramInt3 = paramInt2;
      if (localView.getVisibility() != 8) {
        if (!localLayoutParams.a) {
          break label616;
        }
      }
    }
    for (paramInt3 = paramInt2;; paramInt3 = paramInt2 + (localLayoutParams.rightMargin + paramInt3 + paramInt4))
    {
      paramInt1 += 1;
      paramInt2 = paramInt3;
      break label559;
      break;
      label616:
      paramInt2 += localLayoutParams.leftMargin;
      paramInt3 = localView.getMeasuredWidth();
      m = localView.getMeasuredHeight();
      n = i4 - m / 2;
      localView.layout(paramInt2, n, paramInt2 + paramInt3, n + m);
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    boolean bool2 = this.h;
    if (View.MeasureSpec.getMode(paramInt1) == 1073741824) {}
    int n;
    for (boolean bool1 = true;; bool1 = false)
    {
      this.h = bool1;
      if (bool2 != this.h) {
        this.i = 0;
      }
      m = View.MeasureSpec.getSize(paramInt1);
      if ((this.h) && (this.a != null) && (m != this.i))
      {
        this.i = m;
        this.a.a(true);
      }
      n = getChildCount();
      if ((!this.h) || (n <= 0)) {
        break;
      }
      c(paramInt1, paramInt2);
      return;
    }
    int m = 0;
    while (m < n)
    {
      LayoutParams localLayoutParams = (LayoutParams)getChildAt(m).getLayoutParams();
      localLayoutParams.rightMargin = 0;
      localLayoutParams.leftMargin = 0;
      m += 1;
    }
    super.onMeasure(paramInt1, paramInt2);
  }
  
  public void setExpandedActionViewsExclusive(boolean paramBoolean)
  {
    this.e.d(paramBoolean);
  }
  
  public void setMenuCallbacks(MenuPresenter.Callback paramCallback, MenuBuilder.Callback paramCallback1)
  {
    this.f = paramCallback;
    this.g = paramCallback1;
  }
  
  public void setOnMenuItemClickListener(OnMenuItemClickListener paramOnMenuItemClickListener)
  {
    this.l = paramOnMenuItemClickListener;
  }
  
  public void setOverflowIcon(@Nullable Drawable paramDrawable)
  {
    getMenu();
    this.e.a(paramDrawable);
  }
  
  public void setOverflowReserved(boolean paramBoolean)
  {
    this.d = paramBoolean;
  }
  
  public void setPopupTheme(@StyleRes int paramInt)
  {
    if (this.c != paramInt)
    {
      this.c = paramInt;
      if (paramInt == 0) {
        this.b = getContext();
      }
    }
    else
    {
      return;
    }
    this.b = new ContextThemeWrapper(getContext(), paramInt);
  }
  
  public void setPresenter(ActionMenuPresenter paramActionMenuPresenter)
  {
    this.e = paramActionMenuPresenter;
    this.e.a(this);
  }
  
  public static abstract interface ActionMenuChildView
  {
    public abstract boolean c();
    
    public abstract boolean d();
  }
  
  private class ActionMenuPresenterCallback
    implements MenuPresenter.Callback
  {
    private ActionMenuPresenterCallback() {}
    
    public void a(MenuBuilder paramMenuBuilder, boolean paramBoolean) {}
    
    public boolean a_(MenuBuilder paramMenuBuilder)
    {
      return false;
    }
  }
  
  public static class LayoutParams
    extends LinearLayoutCompat.LayoutParams
  {
    @ViewDebug.ExportedProperty
    public boolean a;
    @ViewDebug.ExportedProperty
    public int b;
    @ViewDebug.ExportedProperty
    public int c;
    @ViewDebug.ExportedProperty
    public boolean d;
    @ViewDebug.ExportedProperty
    public boolean e;
    boolean f;
    
    public LayoutParams(int paramInt1, int paramInt2)
    {
      super(paramInt2);
      this.a = false;
    }
    
    public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
    }
    
    public LayoutParams(LayoutParams paramLayoutParams)
    {
      super();
      this.a = paramLayoutParams.a;
    }
    
    public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
    }
  }
  
  private class MenuBuilderCallback
    implements MenuBuilder.Callback
  {
    private MenuBuilderCallback() {}
    
    public void a(MenuBuilder paramMenuBuilder)
    {
      if (ActionMenuView.b(ActionMenuView.this) != null) {
        ActionMenuView.b(ActionMenuView.this).a(paramMenuBuilder);
      }
    }
    
    public boolean a(MenuBuilder paramMenuBuilder, MenuItem paramMenuItem)
    {
      return (ActionMenuView.a(ActionMenuView.this) != null) && (ActionMenuView.a(ActionMenuView.this).a(paramMenuItem));
    }
  }
  
  public static abstract interface OnMenuItemClickListener
  {
    public abstract boolean a(MenuItem paramMenuItem);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/ActionMenuView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */