package android.support.v7.widget;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.app.SearchableInfo;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.ResultReceiver;
import android.support.v4.view.KeyEventCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.appcompat.R.attr;
import android.support.v7.appcompat.R.dimen;
import android.support.v7.appcompat.R.id;
import android.support.v7.appcompat.R.layout;
import android.support.v7.appcompat.R.styleable;
import android.support.v7.view.CollapsibleActionView;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.KeyEvent.DispatcherState;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import java.lang.reflect.Method;
import java.util.WeakHashMap;

public class SearchView
  extends LinearLayoutCompat
  implements CollapsibleActionView
{
  static final AutoCompleteTextViewReflector a;
  private static final boolean c;
  private CursorAdapter A;
  private boolean B;
  private CharSequence C;
  private boolean D;
  private boolean E;
  private int F;
  private boolean G;
  private CharSequence H;
  private CharSequence I;
  private boolean J;
  private int K;
  private SearchableInfo L;
  private Bundle M;
  private final AppCompatDrawableManager N = AppCompatDrawableManager.a();
  private Runnable O = new Runnable()
  {
    public void run()
    {
      InputMethodManager localInputMethodManager = (InputMethodManager)SearchView.this.getContext().getSystemService("input_method");
      if (localInputMethodManager != null) {
        SearchView.a.a(localInputMethodManager, SearchView.this, 0);
      }
    }
  };
  private final Runnable P = new Runnable()
  {
    public void run()
    {
      SearchView.a(SearchView.this);
    }
  };
  private Runnable Q = new Runnable()
  {
    public void run()
    {
      if ((SearchView.b(SearchView.this) != null) && ((SearchView.b(SearchView.this) instanceof SuggestionsAdapter))) {
        SearchView.b(SearchView.this).a(null);
      }
    }
  };
  private final WeakHashMap<String, Drawable.ConstantState> R = new WeakHashMap();
  private final View.OnClickListener S = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      if (paramAnonymousView == SearchView.e(SearchView.this)) {
        SearchView.f(SearchView.this);
      }
      do
      {
        return;
        if (paramAnonymousView == SearchView.g(SearchView.this))
        {
          SearchView.h(SearchView.this);
          return;
        }
        if (paramAnonymousView == SearchView.i(SearchView.this))
        {
          SearchView.j(SearchView.this);
          return;
        }
        if (paramAnonymousView == SearchView.k(SearchView.this))
        {
          SearchView.l(SearchView.this);
          return;
        }
      } while (paramAnonymousView != SearchView.m(SearchView.this));
      SearchView.n(SearchView.this);
    }
  };
  private final TextView.OnEditorActionListener T = new TextView.OnEditorActionListener()
  {
    public boolean onEditorAction(TextView paramAnonymousTextView, int paramAnonymousInt, KeyEvent paramAnonymousKeyEvent)
    {
      SearchView.j(SearchView.this);
      return true;
    }
  };
  private final AdapterView.OnItemClickListener U = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      SearchView.a(SearchView.this, paramAnonymousInt, 0, null);
    }
  };
  private final AdapterView.OnItemSelectedListener V = new AdapterView.OnItemSelectedListener()
  {
    public void onItemSelected(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      SearchView.a(SearchView.this, paramAnonymousInt);
    }
    
    public void onNothingSelected(AdapterView<?> paramAnonymousAdapterView) {}
  };
  private TextWatcher W = new TextWatcher()
  {
    public void afterTextChanged(Editable paramAnonymousEditable) {}
    
    public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
    
    public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
    {
      SearchView.a(SearchView.this, paramAnonymousCharSequence);
    }
  };
  View.OnKeyListener b = new View.OnKeyListener()
  {
    public boolean onKey(View paramAnonymousView, int paramAnonymousInt, KeyEvent paramAnonymousKeyEvent)
    {
      if (SearchView.o(SearchView.this) == null) {}
      do
      {
        return false;
        if ((SearchView.m(SearchView.this).isPopupShowing()) && (SearchView.m(SearchView.this).getListSelection() != -1)) {
          return SearchView.a(SearchView.this, paramAnonymousView, paramAnonymousInt, paramAnonymousKeyEvent);
        }
      } while ((SearchView.SearchAutoComplete.a(SearchView.m(SearchView.this))) || (!KeyEventCompat.a(paramAnonymousKeyEvent)) || (paramAnonymousKeyEvent.getAction() != 1) || (paramAnonymousInt != 66));
      paramAnonymousView.cancelLongPress();
      SearchView.a(SearchView.this, 0, null, SearchView.m(SearchView.this).getText().toString());
      return true;
    }
  };
  private final SearchAutoComplete d;
  private final View e;
  private final View f;
  private final View g;
  private final ImageView h;
  private final ImageView i;
  private final ImageView j;
  private final ImageView k;
  private final View l;
  private final ImageView m;
  private final Drawable n;
  private final int o;
  private final int p;
  private final Intent q;
  private final Intent r;
  private final CharSequence s;
  private OnQueryTextListener t;
  private OnCloseListener u;
  private View.OnFocusChangeListener v;
  private OnSuggestionListener w;
  private View.OnClickListener x;
  private boolean y;
  private boolean z;
  
  static
  {
    if (Build.VERSION.SDK_INT >= 8) {}
    for (boolean bool = true;; bool = false)
    {
      c = bool;
      a = new AutoCompleteTextViewReflector();
      return;
    }
  }
  
  public SearchView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public SearchView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, R.attr.searchViewStyle);
  }
  
  public SearchView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    paramAttributeSet = TintTypedArray.a(paramContext, paramAttributeSet, R.styleable.SearchView, paramInt, 0);
    LayoutInflater.from(paramContext).inflate(paramAttributeSet.g(R.styleable.SearchView_layout, R.layout.abc_search_view), this, true);
    this.d = ((SearchAutoComplete)findViewById(R.id.search_src_text));
    this.d.setSearchView(this);
    this.e = findViewById(R.id.search_edit_frame);
    this.f = findViewById(R.id.search_plate);
    this.g = findViewById(R.id.submit_area);
    this.h = ((ImageView)findViewById(R.id.search_button));
    this.i = ((ImageView)findViewById(R.id.search_go_btn));
    this.j = ((ImageView)findViewById(R.id.search_close_btn));
    this.k = ((ImageView)findViewById(R.id.search_voice_btn));
    this.m = ((ImageView)findViewById(R.id.search_mag_icon));
    this.f.setBackgroundDrawable(paramAttributeSet.a(R.styleable.SearchView_queryBackground));
    this.g.setBackgroundDrawable(paramAttributeSet.a(R.styleable.SearchView_submitBackground));
    this.h.setImageDrawable(paramAttributeSet.a(R.styleable.SearchView_searchIcon));
    this.i.setImageDrawable(paramAttributeSet.a(R.styleable.SearchView_goIcon));
    this.j.setImageDrawable(paramAttributeSet.a(R.styleable.SearchView_closeIcon));
    this.k.setImageDrawable(paramAttributeSet.a(R.styleable.SearchView_voiceIcon));
    this.m.setImageDrawable(paramAttributeSet.a(R.styleable.SearchView_searchIcon));
    this.n = paramAttributeSet.a(R.styleable.SearchView_searchHintIcon);
    this.o = paramAttributeSet.g(R.styleable.SearchView_suggestionRowLayout, R.layout.abc_search_dropdown_item_icons_2line);
    this.p = paramAttributeSet.g(R.styleable.SearchView_commitIcon, 0);
    this.h.setOnClickListener(this.S);
    this.j.setOnClickListener(this.S);
    this.i.setOnClickListener(this.S);
    this.k.setOnClickListener(this.S);
    this.d.setOnClickListener(this.S);
    this.d.addTextChangedListener(this.W);
    this.d.setOnEditorActionListener(this.T);
    this.d.setOnItemClickListener(this.U);
    this.d.setOnItemSelectedListener(this.V);
    this.d.setOnKeyListener(this.b);
    this.d.setOnFocusChangeListener(new View.OnFocusChangeListener()
    {
      public void onFocusChange(View paramAnonymousView, boolean paramAnonymousBoolean)
      {
        if (SearchView.c(SearchView.this) != null) {
          SearchView.c(SearchView.this).onFocusChange(SearchView.this, paramAnonymousBoolean);
        }
      }
    });
    setIconifiedByDefault(paramAttributeSet.a(R.styleable.SearchView_iconifiedByDefault, true));
    paramInt = paramAttributeSet.e(R.styleable.SearchView_android_maxWidth, -1);
    if (paramInt != -1) {
      setMaxWidth(paramInt);
    }
    this.s = paramAttributeSet.c(R.styleable.SearchView_defaultQueryHint);
    this.C = paramAttributeSet.c(R.styleable.SearchView_queryHint);
    paramInt = paramAttributeSet.a(R.styleable.SearchView_android_imeOptions, -1);
    if (paramInt != -1) {
      setImeOptions(paramInt);
    }
    paramInt = paramAttributeSet.a(R.styleable.SearchView_android_inputType, -1);
    if (paramInt != -1) {
      setInputType(paramInt);
    }
    setFocusable(paramAttributeSet.a(R.styleable.SearchView_android_focusable, true));
    paramAttributeSet.a();
    this.q = new Intent("android.speech.action.WEB_SEARCH");
    this.q.addFlags(268435456);
    this.q.putExtra("android.speech.extra.LANGUAGE_MODEL", "web_search");
    this.r = new Intent("android.speech.action.RECOGNIZE_SPEECH");
    this.r.addFlags(268435456);
    this.l = findViewById(this.d.getDropDownAnchor());
    if (this.l != null)
    {
      if (Build.VERSION.SDK_INT < 11) {
        break label767;
      }
      e();
    }
    for (;;)
    {
      a(this.y);
      n();
      return;
      label767:
      f();
    }
  }
  
  @TargetApi(8)
  private Intent a(Intent paramIntent, SearchableInfo paramSearchableInfo)
  {
    Intent localIntent = new Intent(paramIntent);
    paramIntent = paramSearchableInfo.getSearchActivity();
    if (paramIntent == null) {}
    for (paramIntent = null;; paramIntent = paramIntent.flattenToShortString())
    {
      localIntent.putExtra("calling_package", paramIntent);
      return localIntent;
    }
  }
  
  private Intent a(Cursor paramCursor, int paramInt, String paramString)
  {
    try
    {
      localObject2 = SuggestionsAdapter.a(paramCursor, "suggest_intent_action");
      localObject1 = localObject2;
      if (localObject2 != null) {
        break label229;
      }
      localObject1 = localObject2;
      if (Build.VERSION.SDK_INT < 8) {
        break label229;
      }
      localObject1 = this.L.getSuggestIntentAction();
    }
    catch (RuntimeException paramString)
    {
      for (;;)
      {
        Object localObject1;
        Object localObject3;
        try
        {
          String str;
          paramInt = paramCursor.getPosition();
          Log.w("SearchView", "Search suggestions cursor at row " + paramInt + " returned exception.", paramString);
          return null;
        }
        catch (RuntimeException paramCursor)
        {
          paramInt = -1;
          continue;
        }
        label229:
        Object localObject2 = localObject1;
        if (localObject1 == null)
        {
          localObject2 = "android.intent.action.SEARCH";
          continue;
          label246:
          if (localObject3 == null) {
            localObject1 = null;
          }
        }
      }
    }
    localObject3 = SuggestionsAdapter.a(paramCursor, "suggest_intent_data");
    localObject1 = localObject3;
    if (c)
    {
      localObject1 = localObject3;
      if (localObject3 == null) {
        localObject1 = this.L.getSuggestIntentData();
      }
    }
    localObject3 = localObject1;
    if (localObject1 != null)
    {
      str = SuggestionsAdapter.a(paramCursor, "suggest_intent_data_id");
      localObject3 = localObject1;
      if (str != null)
      {
        localObject3 = (String)localObject1 + "/" + Uri.encode(str);
        break label246;
        for (;;)
        {
          localObject3 = SuggestionsAdapter.a(paramCursor, "suggest_intent_query");
          return a((String)localObject2, (Uri)localObject1, SuggestionsAdapter.a(paramCursor, "suggest_intent_extra_data"), (String)localObject3, paramInt, paramString);
          localObject1 = Uri.parse((String)localObject3);
        }
      }
    }
  }
  
  private Intent a(String paramString1, Uri paramUri, String paramString2, String paramString3, int paramInt, String paramString4)
  {
    paramString1 = new Intent(paramString1);
    paramString1.addFlags(268435456);
    if (paramUri != null) {
      paramString1.setData(paramUri);
    }
    paramString1.putExtra("user_query", this.I);
    if (paramString3 != null) {
      paramString1.putExtra("query", paramString3);
    }
    if (paramString2 != null) {
      paramString1.putExtra("intent_extra_data_key", paramString2);
    }
    if (this.M != null) {
      paramString1.putExtra("app_data", this.M);
    }
    if (paramInt != 0)
    {
      paramString1.putExtra("action_key", paramInt);
      paramString1.putExtra("action_msg", paramString4);
    }
    if (c) {
      paramString1.setComponent(this.L.getSearchActivity());
    }
    return paramString1;
  }
  
  private void a(int paramInt, String paramString1, String paramString2)
  {
    paramString1 = a("android.intent.action.SEARCH", null, null, paramString2, paramInt, paramString1);
    getContext().startActivity(paramString1);
  }
  
  private void a(Intent paramIntent)
  {
    if (paramIntent == null) {
      return;
    }
    try
    {
      getContext().startActivity(paramIntent);
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      Log.e("SearchView", "Failed launch activity: " + paramIntent, localRuntimeException);
    }
  }
  
  private void a(boolean paramBoolean)
  {
    int i2 = 8;
    boolean bool2 = true;
    this.z = paramBoolean;
    int i1;
    boolean bool1;
    if (paramBoolean)
    {
      i1 = 0;
      if (TextUtils.isEmpty(this.d.getText())) {
        break label121;
      }
      bool1 = true;
      label33:
      this.h.setVisibility(i1);
      b(bool1);
      View localView = this.e;
      if (!paramBoolean) {
        break label127;
      }
      i1 = i2;
      label59:
      localView.setVisibility(i1);
      if ((this.m.getDrawable() != null) && (!this.y)) {
        break label132;
      }
      i1 = 8;
      label85:
      this.m.setVisibility(i1);
      k();
      if (bool1) {
        break label137;
      }
    }
    label121:
    label127:
    label132:
    label137:
    for (paramBoolean = bool2;; paramBoolean = false)
    {
      c(paramBoolean);
      i();
      return;
      i1 = 8;
      break;
      bool1 = false;
      break label33;
      i1 = 0;
      break label59;
      i1 = 0;
      break label85;
    }
  }
  
  private boolean a(int paramInt)
  {
    if ((this.w == null) || (!this.w.a(paramInt)))
    {
      e(paramInt);
      return true;
    }
    return false;
  }
  
  private boolean a(int paramInt1, int paramInt2, String paramString)
  {
    boolean bool = false;
    if ((this.w == null) || (!this.w.b(paramInt1)))
    {
      b(paramInt1, 0, null);
      setImeVisibility(false);
      q();
      bool = true;
    }
    return bool;
  }
  
  static boolean a(Context paramContext)
  {
    return paramContext.getResources().getConfiguration().orientation == 2;
  }
  
  private boolean a(View paramView, int paramInt, KeyEvent paramKeyEvent)
  {
    if (this.L == null) {}
    do
    {
      do
      {
        return false;
      } while ((this.A == null) || (paramKeyEvent.getAction() != 0) || (!KeyEventCompat.a(paramKeyEvent)));
      if ((paramInt == 66) || (paramInt == 84) || (paramInt == 61)) {
        return a(this.d.getListSelection(), 0, null);
      }
      if ((paramInt == 21) || (paramInt == 22))
      {
        if (paramInt == 21) {}
        for (paramInt = 0;; paramInt = this.d.length())
        {
          this.d.setSelection(paramInt);
          this.d.setListSelection(0);
          this.d.clearListSelection();
          a.a(this.d, true);
          return true;
        }
      }
    } while ((paramInt != 19) || (this.d.getListSelection() != 0));
    return false;
  }
  
  @TargetApi(8)
  private Intent b(Intent paramIntent, SearchableInfo paramSearchableInfo)
  {
    ComponentName localComponentName = paramSearchableInfo.getSearchActivity();
    Object localObject1 = new Intent("android.intent.action.SEARCH");
    ((Intent)localObject1).setComponent(localComponentName);
    PendingIntent localPendingIntent = PendingIntent.getActivity(getContext(), 0, (Intent)localObject1, 1073741824);
    Bundle localBundle = new Bundle();
    if (this.M != null) {
      localBundle.putParcelable("app_data", this.M);
    }
    Intent localIntent2 = new Intent(paramIntent);
    paramIntent = "free_form";
    Object localObject3 = null;
    localObject1 = null;
    Object localObject2 = null;
    String str = null;
    int i2 = 1;
    Intent localIntent1 = paramIntent;
    int i1 = i2;
    if (Build.VERSION.SDK_INT >= 8)
    {
      localObject2 = getResources();
      if (paramSearchableInfo.getVoiceLanguageModeId() != 0) {
        paramIntent = ((Resources)localObject2).getString(paramSearchableInfo.getVoiceLanguageModeId());
      }
      if (paramSearchableInfo.getVoicePromptTextId() != 0) {
        localObject1 = ((Resources)localObject2).getString(paramSearchableInfo.getVoicePromptTextId());
      }
      if (paramSearchableInfo.getVoiceLanguageId() != 0) {
        str = ((Resources)localObject2).getString(paramSearchableInfo.getVoiceLanguageId());
      }
      localObject2 = str;
      localIntent1 = paramIntent;
      i1 = i2;
      localObject3 = localObject1;
      if (paramSearchableInfo.getVoiceMaxResults() != 0)
      {
        i1 = paramSearchableInfo.getVoiceMaxResults();
        localObject3 = localObject1;
        localIntent1 = paramIntent;
        localObject2 = str;
      }
    }
    localIntent2.putExtra("android.speech.extra.LANGUAGE_MODEL", localIntent1);
    localIntent2.putExtra("android.speech.extra.PROMPT", (String)localObject3);
    localIntent2.putExtra("android.speech.extra.LANGUAGE", (String)localObject2);
    localIntent2.putExtra("android.speech.extra.MAX_RESULTS", i1);
    if (localComponentName == null) {}
    for (paramIntent = null;; paramIntent = localComponentName.flattenToShortString())
    {
      localIntent2.putExtra("calling_package", paramIntent);
      localIntent2.putExtra("android.speech.extra.RESULTS_PENDINGINTENT", localPendingIntent);
      localIntent2.putExtra("android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE", localBundle);
      return localIntent2;
    }
  }
  
  private CharSequence b(CharSequence paramCharSequence)
  {
    if ((!this.y) || (this.n == null)) {
      return paramCharSequence;
    }
    int i1 = (int)(this.d.getTextSize() * 1.25D);
    this.n.setBounds(0, 0, i1, i1);
    SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder("   ");
    localSpannableStringBuilder.setSpan(new ImageSpan(this.n), 1, 2, 33);
    localSpannableStringBuilder.append(paramCharSequence);
    return localSpannableStringBuilder;
  }
  
  private void b(boolean paramBoolean)
  {
    int i2 = 8;
    int i1 = i2;
    if (this.B)
    {
      i1 = i2;
      if (h())
      {
        i1 = i2;
        if (hasFocus()) {
          if (!paramBoolean)
          {
            i1 = i2;
            if (this.G) {}
          }
          else
          {
            i1 = 0;
          }
        }
      }
    }
    this.i.setVisibility(i1);
  }
  
  private boolean b(int paramInt1, int paramInt2, String paramString)
  {
    Cursor localCursor = this.A.a();
    if ((localCursor != null) && (localCursor.moveToPosition(paramInt1)))
    {
      a(a(localCursor, paramInt2, paramString));
      return true;
    }
    return false;
  }
  
  private void c(CharSequence paramCharSequence)
  {
    boolean bool2 = true;
    Editable localEditable = this.d.getText();
    this.I = localEditable;
    if (!TextUtils.isEmpty(localEditable))
    {
      bool1 = true;
      b(bool1);
      if (bool1) {
        break label101;
      }
    }
    label101:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      c(bool1);
      k();
      i();
      if ((this.t != null) && (!TextUtils.equals(paramCharSequence, this.H))) {
        this.t.b(paramCharSequence.toString());
      }
      this.H = paramCharSequence.toString();
      return;
      bool1 = false;
      break;
    }
  }
  
  private void c(boolean paramBoolean)
  {
    int i2 = 8;
    int i1 = i2;
    if (this.G)
    {
      i1 = i2;
      if (!c())
      {
        i1 = i2;
        if (paramBoolean)
        {
          i1 = 0;
          this.i.setVisibility(8);
        }
      }
    }
    this.k.setVisibility(i1);
  }
  
  @TargetApi(11)
  private void e()
  {
    this.l.addOnLayoutChangeListener(new View.OnLayoutChangeListener()
    {
      public void onLayoutChange(View paramAnonymousView, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3, int paramAnonymousInt4, int paramAnonymousInt5, int paramAnonymousInt6, int paramAnonymousInt7, int paramAnonymousInt8)
      {
        SearchView.d(SearchView.this);
      }
    });
  }
  
  private void e(int paramInt)
  {
    Editable localEditable = this.d.getText();
    Object localObject = this.A.a();
    if (localObject == null) {
      return;
    }
    if (((Cursor)localObject).moveToPosition(paramInt))
    {
      localObject = this.A.c((Cursor)localObject);
      if (localObject != null)
      {
        setQuery((CharSequence)localObject);
        return;
      }
      setQuery(localEditable);
      return;
    }
    setQuery(localEditable);
  }
  
  private void f()
  {
    this.l.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
    {
      public void onGlobalLayout()
      {
        SearchView.d(SearchView.this);
      }
    });
  }
  
  @TargetApi(8)
  private boolean g()
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    Intent localIntent;
    if (this.L != null)
    {
      bool1 = bool2;
      if (this.L.getVoiceSearchEnabled())
      {
        localIntent = null;
        if (!this.L.getVoiceSearchLaunchWebSearch()) {
          break label69;
        }
        localIntent = this.q;
      }
    }
    for (;;)
    {
      bool1 = bool2;
      if (localIntent != null)
      {
        bool1 = bool2;
        if (getContext().getPackageManager().resolveActivity(localIntent, 65536) != null) {
          bool1 = true;
        }
      }
      return bool1;
      label69:
      if (this.L.getVoiceSearchLaunchRecognizer()) {
        localIntent = this.r;
      }
    }
  }
  
  private int getPreferredWidth()
  {
    return getContext().getResources().getDimensionPixelSize(R.dimen.abc_search_view_preferred_width);
  }
  
  private boolean h()
  {
    return ((this.B) || (this.G)) && (!c());
  }
  
  private void i()
  {
    int i2 = 8;
    int i1 = i2;
    if (h()) {
      if (this.i.getVisibility() != 0)
      {
        i1 = i2;
        if (this.k.getVisibility() != 0) {}
      }
      else
      {
        i1 = 0;
      }
    }
    this.g.setVisibility(i1);
  }
  
  private void k()
  {
    int i4 = 1;
    int i3 = 0;
    int i1;
    int i2;
    label44:
    label56:
    Drawable localDrawable;
    if (!TextUtils.isEmpty(this.d.getText()))
    {
      i1 = 1;
      i2 = i4;
      if (i1 == 0)
      {
        if ((!this.y) || (this.J)) {
          break label99;
        }
        i2 = i4;
      }
      localObject = this.j;
      if (i2 == 0) {
        break label104;
      }
      i2 = i3;
      ((ImageView)localObject).setVisibility(i2);
      localDrawable = this.j.getDrawable();
      if (localDrawable != null) {
        if (i1 == 0) {
          break label110;
        }
      }
    }
    label99:
    label104:
    label110:
    for (Object localObject = ENABLED_STATE_SET;; localObject = EMPTY_STATE_SET)
    {
      localDrawable.setState((int[])localObject);
      return;
      i1 = 0;
      break;
      i2 = 0;
      break label44;
      i2 = 8;
      break label56;
    }
  }
  
  private void l()
  {
    post(this.P);
  }
  
  private void m()
  {
    if (this.d.hasFocus()) {}
    for (int[] arrayOfInt = FOCUSED_STATE_SET;; arrayOfInt = EMPTY_STATE_SET)
    {
      Drawable localDrawable = this.f.getBackground();
      if (localDrawable != null) {
        localDrawable.setState(arrayOfInt);
      }
      localDrawable = this.g.getBackground();
      if (localDrawable != null) {
        localDrawable.setState(arrayOfInt);
      }
      invalidate();
      return;
    }
  }
  
  private void n()
  {
    CharSequence localCharSequence = getQueryHint();
    SearchAutoComplete localSearchAutoComplete = this.d;
    Object localObject = localCharSequence;
    if (localCharSequence == null) {
      localObject = "";
    }
    localSearchAutoComplete.setHint(b((CharSequence)localObject));
  }
  
  @TargetApi(8)
  private void o()
  {
    int i2 = 1;
    this.d.setThreshold(this.L.getSuggestThreshold());
    this.d.setImeOptions(this.L.getImeOptions());
    int i3 = this.L.getInputType();
    int i1 = i3;
    if ((i3 & 0xF) == 1)
    {
      i3 &= 0xFFFEFFFF;
      i1 = i3;
      if (this.L.getSuggestAuthority() != null) {
        i1 = i3 | 0x10000 | 0x80000;
      }
    }
    this.d.setInputType(i1);
    if (this.A != null) {
      this.A.a(null);
    }
    if (this.L.getSuggestAuthority() != null)
    {
      this.A = new SuggestionsAdapter(getContext(), this, this.L, this.R);
      this.d.setAdapter(this.A);
      SuggestionsAdapter localSuggestionsAdapter = (SuggestionsAdapter)this.A;
      i1 = i2;
      if (this.D) {
        i1 = 2;
      }
      localSuggestionsAdapter.a(i1);
    }
  }
  
  private void p()
  {
    Editable localEditable = this.d.getText();
    if ((localEditable != null) && (TextUtils.getTrimmedLength(localEditable) > 0) && ((this.t == null) || (!this.t.a(localEditable.toString()))))
    {
      if (this.L != null) {
        a(0, null, localEditable.toString());
      }
      setImeVisibility(false);
      q();
    }
  }
  
  private void q()
  {
    this.d.dismissDropDown();
  }
  
  private void r()
  {
    if (TextUtils.isEmpty(this.d.getText()))
    {
      if ((this.y) && ((this.u == null) || (!this.u.a())))
      {
        clearFocus();
        a(true);
      }
      return;
    }
    this.d.setText("");
    this.d.requestFocus();
    setImeVisibility(true);
  }
  
  private void s()
  {
    a(false);
    this.d.requestFocus();
    setImeVisibility(true);
    if (this.x != null) {
      this.x.onClick(this);
    }
  }
  
  private void setImeVisibility(boolean paramBoolean)
  {
    if (paramBoolean) {
      post(this.O);
    }
    InputMethodManager localInputMethodManager;
    do
    {
      return;
      removeCallbacks(this.O);
      localInputMethodManager = (InputMethodManager)getContext().getSystemService("input_method");
    } while (localInputMethodManager == null);
    localInputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
  }
  
  private void setQuery(CharSequence paramCharSequence)
  {
    this.d.setText(paramCharSequence);
    SearchAutoComplete localSearchAutoComplete = this.d;
    if (TextUtils.isEmpty(paramCharSequence)) {}
    for (int i1 = 0;; i1 = paramCharSequence.length())
    {
      localSearchAutoComplete.setSelection(i1);
      return;
    }
  }
  
  @TargetApi(8)
  private void t()
  {
    if (this.L == null) {}
    do
    {
      return;
      Object localObject = this.L;
      try
      {
        if (((SearchableInfo)localObject).getVoiceSearchLaunchWebSearch())
        {
          localObject = a(this.q, (SearchableInfo)localObject);
          getContext().startActivity((Intent)localObject);
          return;
        }
      }
      catch (ActivityNotFoundException localActivityNotFoundException)
      {
        Log.w("SearchView", "Could not find voice search activity");
        return;
      }
    } while (!localActivityNotFoundException.getVoiceSearchLaunchRecognizer());
    Intent localIntent = b(this.r, localActivityNotFoundException);
    getContext().startActivity(localIntent);
  }
  
  private void u()
  {
    int i3;
    Rect localRect;
    int i1;
    if (this.l.getWidth() > 1)
    {
      Resources localResources = getContext().getResources();
      i3 = this.f.getPaddingLeft();
      localRect = new Rect();
      boolean bool = ViewUtils.a(this);
      if (!this.y) {
        break label142;
      }
      i1 = localResources.getDimensionPixelSize(R.dimen.abc_dropdownitem_icon_width) + localResources.getDimensionPixelSize(R.dimen.abc_dropdownitem_text_padding_left);
      this.d.getDropDownBackground().getPadding(localRect);
      if (!bool) {
        break label147;
      }
    }
    label142:
    label147:
    for (int i2 = -localRect.left;; i2 = i3 - (localRect.left + i1))
    {
      this.d.setDropDownHorizontalOffset(i2);
      i2 = this.l.getWidth();
      int i4 = localRect.left;
      int i5 = localRect.right;
      this.d.setDropDownWidth(i2 + i4 + i5 + i1 - i3);
      return;
      i1 = 0;
      break;
    }
  }
  
  private void v()
  {
    a.a(this.d);
    a.b(this.d);
  }
  
  public void a()
  {
    if (this.J) {
      return;
    }
    this.J = true;
    this.K = this.d.getImeOptions();
    this.d.setImeOptions(this.K | 0x2000000);
    this.d.setText("");
    setIconified(false);
  }
  
  void a(CharSequence paramCharSequence)
  {
    setQuery(paramCharSequence);
  }
  
  public void b()
  {
    setQuery("", false);
    clearFocus();
    a(true);
    this.d.setImeOptions(this.K);
    this.J = false;
  }
  
  public boolean c()
  {
    return this.z;
  }
  
  public void clearFocus()
  {
    this.E = true;
    setImeVisibility(false);
    super.clearFocus();
    this.d.clearFocus();
    this.E = false;
  }
  
  void d()
  {
    a(c());
    l();
    if (this.d.hasFocus()) {
      v();
    }
  }
  
  public int getImeOptions()
  {
    return this.d.getImeOptions();
  }
  
  public int getInputType()
  {
    return this.d.getInputType();
  }
  
  public int getMaxWidth()
  {
    return this.F;
  }
  
  public CharSequence getQuery()
  {
    return this.d.getText();
  }
  
  public CharSequence getQueryHint()
  {
    if (this.C != null) {
      return this.C;
    }
    if ((c) && (this.L != null) && (this.L.getHintId() != 0)) {
      return getContext().getText(this.L.getHintId());
    }
    return this.s;
  }
  
  int getSuggestionCommitIconResId()
  {
    return this.p;
  }
  
  int getSuggestionRowLayout()
  {
    return this.o;
  }
  
  public CursorAdapter getSuggestionsAdapter()
  {
    return this.A;
  }
  
  protected void onDetachedFromWindow()
  {
    removeCallbacks(this.P);
    post(this.Q);
    super.onDetachedFromWindow();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    if (c())
    {
      super.onMeasure(paramInt1, paramInt2);
      return;
    }
    int i2 = View.MeasureSpec.getMode(paramInt1);
    int i1 = View.MeasureSpec.getSize(paramInt1);
    switch (i2)
    {
    default: 
      paramInt1 = i1;
    case -2147483648: 
    case 1073741824: 
      for (;;)
      {
        super.onMeasure(View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824), paramInt2);
        return;
        if (this.F > 0)
        {
          paramInt1 = Math.min(this.F, i1);
        }
        else
        {
          paramInt1 = Math.min(getPreferredWidth(), i1);
          continue;
          paramInt1 = i1;
          if (this.F > 0) {
            paramInt1 = Math.min(this.F, i1);
          }
        }
      }
    }
    if (this.F > 0) {}
    for (paramInt1 = this.F;; paramInt1 = getPreferredWidth()) {
      break;
    }
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    if (!(paramParcelable instanceof SavedState))
    {
      super.onRestoreInstanceState(paramParcelable);
      return;
    }
    paramParcelable = (SavedState)paramParcelable;
    super.onRestoreInstanceState(paramParcelable.getSuperState());
    a(paramParcelable.a);
    requestLayout();
  }
  
  protected Parcelable onSaveInstanceState()
  {
    SavedState localSavedState = new SavedState(super.onSaveInstanceState());
    localSavedState.a = c();
    return localSavedState;
  }
  
  public void onWindowFocusChanged(boolean paramBoolean)
  {
    super.onWindowFocusChanged(paramBoolean);
    l();
  }
  
  public boolean requestFocus(int paramInt, Rect paramRect)
  {
    boolean bool1;
    if (this.E) {
      bool1 = false;
    }
    boolean bool2;
    do
    {
      return bool1;
      if (!isFocusable()) {
        return false;
      }
      if (c()) {
        break;
      }
      bool2 = this.d.requestFocus(paramInt, paramRect);
      bool1 = bool2;
    } while (!bool2);
    a(false);
    return bool2;
    return super.requestFocus(paramInt, paramRect);
  }
  
  public void setAppSearchData(Bundle paramBundle)
  {
    this.M = paramBundle;
  }
  
  public void setIconified(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      r();
      return;
    }
    s();
  }
  
  public void setIconifiedByDefault(boolean paramBoolean)
  {
    if (this.y == paramBoolean) {
      return;
    }
    this.y = paramBoolean;
    a(paramBoolean);
    n();
  }
  
  public void setImeOptions(int paramInt)
  {
    this.d.setImeOptions(paramInt);
  }
  
  public void setInputType(int paramInt)
  {
    this.d.setInputType(paramInt);
  }
  
  public void setMaxWidth(int paramInt)
  {
    this.F = paramInt;
    requestLayout();
  }
  
  public void setOnCloseListener(OnCloseListener paramOnCloseListener)
  {
    this.u = paramOnCloseListener;
  }
  
  public void setOnQueryTextFocusChangeListener(View.OnFocusChangeListener paramOnFocusChangeListener)
  {
    this.v = paramOnFocusChangeListener;
  }
  
  public void setOnQueryTextListener(OnQueryTextListener paramOnQueryTextListener)
  {
    this.t = paramOnQueryTextListener;
  }
  
  public void setOnSearchClickListener(View.OnClickListener paramOnClickListener)
  {
    this.x = paramOnClickListener;
  }
  
  public void setOnSuggestionListener(OnSuggestionListener paramOnSuggestionListener)
  {
    this.w = paramOnSuggestionListener;
  }
  
  public void setQuery(CharSequence paramCharSequence, boolean paramBoolean)
  {
    this.d.setText(paramCharSequence);
    if (paramCharSequence != null)
    {
      this.d.setSelection(this.d.length());
      this.I = paramCharSequence;
    }
    if ((paramBoolean) && (!TextUtils.isEmpty(paramCharSequence))) {
      p();
    }
  }
  
  public void setQueryHint(CharSequence paramCharSequence)
  {
    this.C = paramCharSequence;
    n();
  }
  
  public void setQueryRefinementEnabled(boolean paramBoolean)
  {
    this.D = paramBoolean;
    SuggestionsAdapter localSuggestionsAdapter;
    if ((this.A instanceof SuggestionsAdapter))
    {
      localSuggestionsAdapter = (SuggestionsAdapter)this.A;
      if (!paramBoolean) {
        break label35;
      }
    }
    label35:
    for (int i1 = 2;; i1 = 1)
    {
      localSuggestionsAdapter.a(i1);
      return;
    }
  }
  
  public void setSearchableInfo(SearchableInfo paramSearchableInfo)
  {
    this.L = paramSearchableInfo;
    if (this.L != null)
    {
      if (c) {
        o();
      }
      n();
    }
    if ((c) && (g())) {}
    for (boolean bool = true;; bool = false)
    {
      this.G = bool;
      if (this.G) {
        this.d.setPrivateImeOptions("nm");
      }
      a(c());
      return;
    }
  }
  
  public void setSubmitButtonEnabled(boolean paramBoolean)
  {
    this.B = paramBoolean;
    a(c());
  }
  
  public void setSuggestionsAdapter(CursorAdapter paramCursorAdapter)
  {
    this.A = paramCursorAdapter;
    this.d.setAdapter(this.A);
  }
  
  private static class AutoCompleteTextViewReflector
  {
    private Method a;
    private Method b;
    private Method c;
    private Method d;
    
    AutoCompleteTextViewReflector()
    {
      try
      {
        this.a = AutoCompleteTextView.class.getDeclaredMethod("doBeforeTextChanged", new Class[0]);
        this.a.setAccessible(true);
        try
        {
          this.b = AutoCompleteTextView.class.getDeclaredMethod("doAfterTextChanged", new Class[0]);
          this.b.setAccessible(true);
          try
          {
            this.c = AutoCompleteTextView.class.getMethod("ensureImeVisible", new Class[] { Boolean.TYPE });
            this.c.setAccessible(true);
            try
            {
              this.d = InputMethodManager.class.getMethod("showSoftInputUnchecked", new Class[] { Integer.TYPE, ResultReceiver.class });
              this.d.setAccessible(true);
              return;
            }
            catch (NoSuchMethodException localNoSuchMethodException1) {}
          }
          catch (NoSuchMethodException localNoSuchMethodException2)
          {
            for (;;) {}
          }
        }
        catch (NoSuchMethodException localNoSuchMethodException3)
        {
          for (;;) {}
        }
      }
      catch (NoSuchMethodException localNoSuchMethodException4)
      {
        for (;;) {}
      }
    }
    
    void a(InputMethodManager paramInputMethodManager, View paramView, int paramInt)
    {
      if (this.d != null) {
        try
        {
          this.d.invoke(paramInputMethodManager, new Object[] { Integer.valueOf(paramInt), null });
          return;
        }
        catch (Exception localException) {}
      }
      paramInputMethodManager.showSoftInput(paramView, paramInt);
    }
    
    void a(AutoCompleteTextView paramAutoCompleteTextView)
    {
      if (this.a != null) {}
      try
      {
        this.a.invoke(paramAutoCompleteTextView, new Object[0]);
        return;
      }
      catch (Exception paramAutoCompleteTextView) {}
    }
    
    void a(AutoCompleteTextView paramAutoCompleteTextView, boolean paramBoolean)
    {
      if (this.c != null) {}
      try
      {
        this.c.invoke(paramAutoCompleteTextView, new Object[] { Boolean.valueOf(paramBoolean) });
        return;
      }
      catch (Exception paramAutoCompleteTextView) {}
    }
    
    void b(AutoCompleteTextView paramAutoCompleteTextView)
    {
      if (this.b != null) {}
      try
      {
        this.b.invoke(paramAutoCompleteTextView, new Object[0]);
        return;
      }
      catch (Exception paramAutoCompleteTextView) {}
    }
  }
  
  public static abstract interface OnCloseListener
  {
    public abstract boolean a();
  }
  
  public static abstract interface OnQueryTextListener
  {
    public abstract boolean a(String paramString);
    
    public abstract boolean b(String paramString);
  }
  
  public static abstract interface OnSuggestionListener
  {
    public abstract boolean a(int paramInt);
    
    public abstract boolean b(int paramInt);
  }
  
  static class SavedState
    extends View.BaseSavedState
  {
    public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
    {
      public SearchView.SavedState a(Parcel paramAnonymousParcel)
      {
        return new SearchView.SavedState(paramAnonymousParcel);
      }
      
      public SearchView.SavedState[] a(int paramAnonymousInt)
      {
        return new SearchView.SavedState[paramAnonymousInt];
      }
    };
    boolean a;
    
    public SavedState(Parcel paramParcel)
    {
      super();
      this.a = ((Boolean)paramParcel.readValue(null)).booleanValue();
    }
    
    SavedState(Parcelable paramParcelable)
    {
      super();
    }
    
    public String toString()
    {
      return "SearchView.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " isIconified=" + this.a + "}";
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      super.writeToParcel(paramParcel, paramInt);
      paramParcel.writeValue(Boolean.valueOf(this.a));
    }
  }
  
  public static class SearchAutoComplete
    extends AppCompatAutoCompleteTextView
  {
    private int a = getThreshold();
    private SearchView b;
    
    public SearchAutoComplete(Context paramContext)
    {
      this(paramContext, null);
    }
    
    public SearchAutoComplete(Context paramContext, AttributeSet paramAttributeSet)
    {
      this(paramContext, paramAttributeSet, R.attr.autoCompleteTextViewStyle);
    }
    
    public SearchAutoComplete(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
      super(paramAttributeSet, paramInt);
    }
    
    private boolean a()
    {
      return TextUtils.getTrimmedLength(getText()) == 0;
    }
    
    public boolean enoughToFilter()
    {
      return (this.a <= 0) || (super.enoughToFilter());
    }
    
    protected void onFocusChanged(boolean paramBoolean, int paramInt, Rect paramRect)
    {
      super.onFocusChanged(paramBoolean, paramInt, paramRect);
      this.b.d();
    }
    
    public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent)
    {
      if (paramInt == 4)
      {
        KeyEvent.DispatcherState localDispatcherState;
        if ((paramKeyEvent.getAction() == 0) && (paramKeyEvent.getRepeatCount() == 0))
        {
          localDispatcherState = getKeyDispatcherState();
          if (localDispatcherState != null) {
            localDispatcherState.startTracking(paramKeyEvent, this);
          }
          return true;
        }
        if (paramKeyEvent.getAction() == 1)
        {
          localDispatcherState = getKeyDispatcherState();
          if (localDispatcherState != null) {
            localDispatcherState.handleUpEvent(paramKeyEvent);
          }
          if ((paramKeyEvent.isTracking()) && (!paramKeyEvent.isCanceled()))
          {
            this.b.clearFocus();
            SearchView.a(this.b, false);
            return true;
          }
        }
      }
      return super.onKeyPreIme(paramInt, paramKeyEvent);
    }
    
    public void onWindowFocusChanged(boolean paramBoolean)
    {
      super.onWindowFocusChanged(paramBoolean);
      if ((paramBoolean) && (this.b.hasFocus()) && (getVisibility() == 0))
      {
        ((InputMethodManager)getContext().getSystemService("input_method")).showSoftInput(this, 0);
        if (SearchView.a(getContext())) {
          SearchView.a.a(this, true);
        }
      }
    }
    
    public void performCompletion() {}
    
    protected void replaceText(CharSequence paramCharSequence) {}
    
    void setSearchView(SearchView paramSearchView)
    {
      this.b = paramSearchView;
    }
    
    public void setThreshold(int paramInt)
    {
      super.setThreshold(paramInt);
      this.a = paramInt;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/SearchView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */