package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.appcompat.R.attr;
import android.support.v7.view.ActionBarPolicy;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.AbsListView.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ScrollingTabContainerView
  extends HorizontalScrollView
  implements AdapterView.OnItemSelectedListener
{
  private static final Interpolator l = new DecelerateInterpolator();
  Runnable a;
  int b;
  int c;
  protected ViewPropertyAnimatorCompat d;
  protected final VisibilityAnimListener e = new VisibilityAnimListener();
  private TabClickListener f;
  private LinearLayoutCompat g;
  private Spinner h;
  private boolean i;
  private int j;
  private int k;
  
  public ScrollingTabContainerView(Context paramContext)
  {
    super(paramContext);
    setHorizontalScrollBarEnabled(false);
    paramContext = ActionBarPolicy.a(paramContext);
    setContentHeight(paramContext.e());
    this.c = paramContext.g();
    this.g = d();
    addView(this.g, new ViewGroup.LayoutParams(-2, -1));
  }
  
  private TabView a(ActionBar.Tab paramTab, boolean paramBoolean)
  {
    paramTab = new TabView(getContext(), paramTab, paramBoolean);
    if (paramBoolean)
    {
      paramTab.setBackgroundDrawable(null);
      paramTab.setLayoutParams(new AbsListView.LayoutParams(-1, this.j));
      return paramTab;
    }
    paramTab.setFocusable(true);
    if (this.f == null) {
      this.f = new TabClickListener(null);
    }
    paramTab.setOnClickListener(this.f);
    return paramTab;
  }
  
  private boolean a()
  {
    return (this.h != null) && (this.h.getParent() == this);
  }
  
  private void b()
  {
    if (a()) {
      return;
    }
    if (this.h == null) {
      this.h = e();
    }
    removeView(this.g);
    addView(this.h, new ViewGroup.LayoutParams(-2, -1));
    if (this.h.getAdapter() == null) {
      this.h.setAdapter(new TabAdapter(null));
    }
    if (this.a != null)
    {
      removeCallbacks(this.a);
      this.a = null;
    }
    this.h.setSelection(this.k);
  }
  
  private boolean c()
  {
    if (!a()) {
      return false;
    }
    removeView(this.h);
    addView(this.g, new ViewGroup.LayoutParams(-2, -1));
    setTabSelected(this.h.getSelectedItemPosition());
    return false;
  }
  
  private LinearLayoutCompat d()
  {
    LinearLayoutCompat localLinearLayoutCompat = new LinearLayoutCompat(getContext(), null, R.attr.actionBarTabBarStyle);
    localLinearLayoutCompat.setMeasureWithLargestChildEnabled(true);
    localLinearLayoutCompat.setGravity(17);
    localLinearLayoutCompat.setLayoutParams(new LinearLayoutCompat.LayoutParams(-2, -1));
    return localLinearLayoutCompat;
  }
  
  private Spinner e()
  {
    AppCompatSpinner localAppCompatSpinner = new AppCompatSpinner(getContext(), null, R.attr.actionDropDownStyle);
    localAppCompatSpinner.setLayoutParams(new LinearLayoutCompat.LayoutParams(-2, -1));
    localAppCompatSpinner.setOnItemSelectedListener(this);
    return localAppCompatSpinner;
  }
  
  public void a(int paramInt)
  {
    final View localView = this.g.getChildAt(paramInt);
    if (this.a != null) {
      removeCallbacks(this.a);
    }
    this.a = new Runnable()
    {
      public void run()
      {
        int i = localView.getLeft();
        int j = (ScrollingTabContainerView.this.getWidth() - localView.getWidth()) / 2;
        ScrollingTabContainerView.this.smoothScrollTo(i - j, 0);
        ScrollingTabContainerView.this.a = null;
      }
    };
    post(this.a);
  }
  
  public void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    if (this.a != null) {
      post(this.a);
    }
  }
  
  protected void onConfigurationChanged(Configuration paramConfiguration)
  {
    if (Build.VERSION.SDK_INT >= 8) {
      super.onConfigurationChanged(paramConfiguration);
    }
    paramConfiguration = ActionBarPolicy.a(getContext());
    setContentHeight(paramConfiguration.e());
    this.c = paramConfiguration.g();
  }
  
  public void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    if (this.a != null) {
      removeCallbacks(this.a);
    }
  }
  
  public void onItemSelected(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    ((TabView)paramView).b().e();
  }
  
  public void onMeasure(int paramInt1, int paramInt2)
  {
    paramInt2 = View.MeasureSpec.getMode(paramInt1);
    boolean bool;
    int m;
    if (paramInt2 == 1073741824)
    {
      bool = true;
      setFillViewport(bool);
      m = this.g.getChildCount();
      if ((m <= 1) || ((paramInt2 != 1073741824) && (paramInt2 != Integer.MIN_VALUE))) {
        break label189;
      }
      if (m <= 2) {
        break label176;
      }
      this.b = ((int)(View.MeasureSpec.getSize(paramInt1) * 0.4F));
      label65:
      this.b = Math.min(this.b, this.c);
      label80:
      m = View.MeasureSpec.makeMeasureSpec(this.j, 1073741824);
      if ((bool) || (!this.i)) {
        break label197;
      }
      paramInt2 = 1;
      label104:
      if (paramInt2 == 0) {
        break label210;
      }
      this.g.measure(0, m);
      if (this.g.getMeasuredWidth() <= View.MeasureSpec.getSize(paramInt1)) {
        break label202;
      }
      b();
    }
    for (;;)
    {
      paramInt2 = getMeasuredWidth();
      super.onMeasure(paramInt1, m);
      paramInt1 = getMeasuredWidth();
      if ((bool) && (paramInt2 != paramInt1)) {
        setTabSelected(this.k);
      }
      return;
      bool = false;
      break;
      label176:
      this.b = (View.MeasureSpec.getSize(paramInt1) / 2);
      break label65;
      label189:
      this.b = -1;
      break label80;
      label197:
      paramInt2 = 0;
      break label104;
      label202:
      c();
      continue;
      label210:
      c();
    }
  }
  
  public void onNothingSelected(AdapterView<?> paramAdapterView) {}
  
  public void setAllowCollapse(boolean paramBoolean)
  {
    this.i = paramBoolean;
  }
  
  public void setContentHeight(int paramInt)
  {
    this.j = paramInt;
    requestLayout();
  }
  
  public void setTabSelected(int paramInt)
  {
    this.k = paramInt;
    int n = this.g.getChildCount();
    int m = 0;
    if (m < n)
    {
      View localView = this.g.getChildAt(m);
      if (m == paramInt) {}
      for (boolean bool = true;; bool = false)
      {
        localView.setSelected(bool);
        if (bool) {
          a(paramInt);
        }
        m += 1;
        break;
      }
    }
    if ((this.h != null) && (paramInt >= 0)) {
      this.h.setSelection(paramInt);
    }
  }
  
  private class TabAdapter
    extends BaseAdapter
  {
    private TabAdapter() {}
    
    public int getCount()
    {
      return ScrollingTabContainerView.a(ScrollingTabContainerView.this).getChildCount();
    }
    
    public Object getItem(int paramInt)
    {
      return ((ScrollingTabContainerView.TabView)ScrollingTabContainerView.a(ScrollingTabContainerView.this).getChildAt(paramInt)).b();
    }
    
    public long getItemId(int paramInt)
    {
      return paramInt;
    }
    
    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null) {
        return ScrollingTabContainerView.a(ScrollingTabContainerView.this, (ActionBar.Tab)getItem(paramInt), true);
      }
      ((ScrollingTabContainerView.TabView)paramView).a((ActionBar.Tab)getItem(paramInt));
      return paramView;
    }
  }
  
  private class TabClickListener
    implements View.OnClickListener
  {
    private TabClickListener() {}
    
    public void onClick(View paramView)
    {
      ((ScrollingTabContainerView.TabView)paramView).b().e();
      int j = ScrollingTabContainerView.a(ScrollingTabContainerView.this).getChildCount();
      int i = 0;
      if (i < j)
      {
        View localView = ScrollingTabContainerView.a(ScrollingTabContainerView.this).getChildAt(i);
        if (localView == paramView) {}
        for (boolean bool = true;; bool = false)
        {
          localView.setSelected(bool);
          i += 1;
          break;
        }
      }
    }
  }
  
  private class TabView
    extends LinearLayoutCompat
    implements View.OnLongClickListener
  {
    private final int[] b = { 16842964 };
    private ActionBar.Tab c;
    private TextView d;
    private ImageView e;
    private View f;
    
    public TabView(Context paramContext, ActionBar.Tab paramTab, boolean paramBoolean)
    {
      super(null, R.attr.actionBarTabStyle);
      this.c = paramTab;
      this$1 = TintTypedArray.a(paramContext, null, this.b, R.attr.actionBarTabStyle, 0);
      if (ScrollingTabContainerView.this.f(0)) {
        setBackgroundDrawable(ScrollingTabContainerView.this.a(0));
      }
      ScrollingTabContainerView.this.a();
      if (paramBoolean) {
        setGravity(8388627);
      }
      a();
    }
    
    public void a()
    {
      Object localObject1 = this.c;
      Object localObject2 = ((ActionBar.Tab)localObject1).d();
      if (localObject2 != null)
      {
        localObject1 = ((View)localObject2).getParent();
        if (localObject1 != this)
        {
          if (localObject1 != null) {
            ((ViewGroup)localObject1).removeView((View)localObject2);
          }
          addView((View)localObject2);
        }
        this.f = ((View)localObject2);
        if (this.d != null) {
          this.d.setVisibility(8);
        }
        if (this.e != null)
        {
          this.e.setVisibility(8);
          this.e.setImageDrawable(null);
        }
        return;
      }
      if (this.f != null)
      {
        removeView(this.f);
        this.f = null;
      }
      Object localObject3 = ((ActionBar.Tab)localObject1).b();
      localObject2 = ((ActionBar.Tab)localObject1).c();
      int i;
      if (localObject3 != null)
      {
        Object localObject4;
        if (this.e == null)
        {
          localObject4 = new ImageView(getContext());
          LinearLayoutCompat.LayoutParams localLayoutParams = new LinearLayoutCompat.LayoutParams(-2, -2);
          localLayoutParams.h = 16;
          ((ImageView)localObject4).setLayoutParams(localLayoutParams);
          addView((View)localObject4, 0);
          this.e = ((ImageView)localObject4);
        }
        this.e.setImageDrawable((Drawable)localObject3);
        this.e.setVisibility(0);
        if (TextUtils.isEmpty((CharSequence)localObject2)) {
          break label365;
        }
        i = 1;
        label209:
        if (i == 0) {
          break label370;
        }
        if (this.d == null)
        {
          localObject3 = new AppCompatTextView(getContext(), null, R.attr.actionBarTabTextStyle);
          ((TextView)localObject3).setEllipsize(TextUtils.TruncateAt.END);
          localObject4 = new LinearLayoutCompat.LayoutParams(-2, -2);
          ((LinearLayoutCompat.LayoutParams)localObject4).h = 16;
          ((TextView)localObject3).setLayoutParams((ViewGroup.LayoutParams)localObject4);
          addView((View)localObject3);
          this.d = ((TextView)localObject3);
        }
        this.d.setText((CharSequence)localObject2);
        this.d.setVisibility(0);
      }
      for (;;)
      {
        if (this.e != null) {
          this.e.setContentDescription(((ActionBar.Tab)localObject1).f());
        }
        if ((i != 0) || (TextUtils.isEmpty(((ActionBar.Tab)localObject1).f()))) {
          break label397;
        }
        setOnLongClickListener(this);
        return;
        if (this.e == null) {
          break;
        }
        this.e.setVisibility(8);
        this.e.setImageDrawable(null);
        break;
        label365:
        i = 0;
        break label209;
        label370:
        if (this.d != null)
        {
          this.d.setVisibility(8);
          this.d.setText(null);
        }
      }
      label397:
      setOnLongClickListener(null);
      setLongClickable(false);
    }
    
    public void a(ActionBar.Tab paramTab)
    {
      this.c = paramTab;
      a();
    }
    
    public ActionBar.Tab b()
    {
      return this.c;
    }
    
    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
      super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
      paramAccessibilityEvent.setClassName(ActionBar.Tab.class.getName());
    }
    
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
      super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
      if (Build.VERSION.SDK_INT >= 14) {
        paramAccessibilityNodeInfo.setClassName(ActionBar.Tab.class.getName());
      }
    }
    
    public boolean onLongClick(View paramView)
    {
      paramView = new int[2];
      getLocationOnScreen(paramView);
      Object localObject = getContext();
      int i = getWidth();
      int j = getHeight();
      int k = ((Context)localObject).getResources().getDisplayMetrics().widthPixels;
      localObject = Toast.makeText((Context)localObject, this.c.f(), 0);
      ((Toast)localObject).setGravity(49, paramView[0] + i / 2 - k / 2, j);
      ((Toast)localObject).show();
      return true;
    }
    
    public void onMeasure(int paramInt1, int paramInt2)
    {
      super.onMeasure(paramInt1, paramInt2);
      if ((ScrollingTabContainerView.this.b > 0) && (getMeasuredWidth() > ScrollingTabContainerView.this.b)) {
        super.onMeasure(View.MeasureSpec.makeMeasureSpec(ScrollingTabContainerView.this.b, 1073741824), paramInt2);
      }
    }
    
    public void setSelected(boolean paramBoolean)
    {
      if (isSelected() != paramBoolean) {}
      for (int i = 1;; i = 0)
      {
        super.setSelected(paramBoolean);
        if ((i != 0) && (paramBoolean)) {
          sendAccessibilityEvent(4);
        }
        return;
      }
    }
  }
  
  protected class VisibilityAnimListener
    implements ViewPropertyAnimatorListener
  {
    private boolean b = false;
    private int c;
    
    protected VisibilityAnimListener() {}
    
    public void a(View paramView)
    {
      ScrollingTabContainerView.this.setVisibility(0);
      this.b = false;
    }
    
    public void b(View paramView)
    {
      if (this.b) {
        return;
      }
      ScrollingTabContainerView.this.d = null;
      ScrollingTabContainerView.this.setVisibility(this.c);
    }
    
    public void c(View paramView)
    {
      this.b = true;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/ScrollingTabContainerView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */