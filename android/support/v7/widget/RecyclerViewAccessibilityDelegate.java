package android.support.v7.widget;

import android.os.Bundle;
import android.support.v4.view.AccessibilityDelegateCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

public class RecyclerViewAccessibilityDelegate
  extends AccessibilityDelegateCompat
{
  final RecyclerView a;
  final AccessibilityDelegateCompat c = new AccessibilityDelegateCompat()
  {
    public void a(View paramAnonymousView, AccessibilityNodeInfoCompat paramAnonymousAccessibilityNodeInfoCompat)
    {
      super.a(paramAnonymousView, paramAnonymousAccessibilityNodeInfoCompat);
      if ((!RecyclerViewAccessibilityDelegate.a(RecyclerViewAccessibilityDelegate.this)) && (RecyclerViewAccessibilityDelegate.this.a.getLayoutManager() != null)) {
        RecyclerViewAccessibilityDelegate.this.a.getLayoutManager().a(paramAnonymousView, paramAnonymousAccessibilityNodeInfoCompat);
      }
    }
    
    public boolean a(View paramAnonymousView, int paramAnonymousInt, Bundle paramAnonymousBundle)
    {
      if (super.a(paramAnonymousView, paramAnonymousInt, paramAnonymousBundle)) {
        return true;
      }
      if ((!RecyclerViewAccessibilityDelegate.a(RecyclerViewAccessibilityDelegate.this)) && (RecyclerViewAccessibilityDelegate.this.a.getLayoutManager() != null)) {
        return RecyclerViewAccessibilityDelegate.this.a.getLayoutManager().a(paramAnonymousView, paramAnonymousInt, paramAnonymousBundle);
      }
      return false;
    }
  };
  
  public RecyclerViewAccessibilityDelegate(RecyclerView paramRecyclerView)
  {
    this.a = paramRecyclerView;
  }
  
  private boolean c()
  {
    return this.a.p();
  }
  
  public void a(View paramView, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat)
  {
    super.a(paramView, paramAccessibilityNodeInfoCompat);
    paramAccessibilityNodeInfoCompat.b(RecyclerView.class.getName());
    if ((!c()) && (this.a.getLayoutManager() != null)) {
      this.a.getLayoutManager().a(paramAccessibilityNodeInfoCompat);
    }
  }
  
  public void a(View paramView, AccessibilityEvent paramAccessibilityEvent)
  {
    super.a(paramView, paramAccessibilityEvent);
    paramAccessibilityEvent.setClassName(RecyclerView.class.getName());
    if (((paramView instanceof RecyclerView)) && (!c()))
    {
      paramView = (RecyclerView)paramView;
      if (paramView.getLayoutManager() != null) {
        paramView.getLayoutManager().a(paramAccessibilityEvent);
      }
    }
  }
  
  public boolean a(View paramView, int paramInt, Bundle paramBundle)
  {
    if (super.a(paramView, paramInt, paramBundle)) {
      return true;
    }
    if ((!c()) && (this.a.getLayoutManager() != null)) {
      return this.a.getLayoutManager().a(paramInt, paramBundle);
    }
    return false;
  }
  
  AccessibilityDelegateCompat b()
  {
    return this.c;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/RecyclerViewAccessibilityDelegate.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */