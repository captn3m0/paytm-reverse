package android.support.v7.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.CollectionItemInfoCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import java.util.Arrays;

public class GridLayoutManager
  extends LinearLayoutManager
{
  boolean a = false;
  int b = -1;
  int[] c;
  View[] d;
  final SparseIntArray e = new SparseIntArray();
  final SparseIntArray f = new SparseIntArray();
  SpanSizeLookup g = new DefaultSpanSizeLookup();
  final Rect h = new Rect();
  
  public GridLayoutManager(Context paramContext, int paramInt)
  {
    super(paramContext);
    a(paramInt);
  }
  
  public GridLayoutManager(Context paramContext, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    super(paramContext, paramInt2, paramBoolean);
    a(paramInt1);
  }
  
  public GridLayoutManager(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
  {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    a(a(paramContext, paramAttributeSet, paramInt1, paramInt2).b);
  }
  
  private void K()
  {
    this.e.clear();
    this.f.clear();
  }
  
  private void L()
  {
    int j = u();
    int i = 0;
    while (i < j)
    {
      LayoutParams localLayoutParams = (LayoutParams)i(i).getLayoutParams();
      int k = localLayoutParams.e();
      this.e.put(k, localLayoutParams.b());
      this.f.put(k, localLayoutParams.a());
      i += 1;
    }
  }
  
  private void M()
  {
    if (g() == 1) {}
    for (int i = x() - B() - z();; i = y() - C() - A())
    {
      m(i);
      return;
    }
  }
  
  private void N()
  {
    if ((this.d == null) || (this.d.length != this.b)) {
      this.d = new View[this.b];
    }
  }
  
  private int a(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, int paramInt)
  {
    if (!paramState.a()) {
      return this.g.c(paramInt, this.b);
    }
    int i = paramRecycler.b(paramInt);
    if (i == -1)
    {
      Log.w("GridLayoutManager", "Cannot find span size for pre layout position. " + paramInt);
      return 0;
    }
    return this.g.c(i, this.b);
  }
  
  private void a(float paramFloat, int paramInt)
  {
    m(Math.max(Math.round(this.b * paramFloat), paramInt));
  }
  
  private void a(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    int j;
    int i;
    int k;
    label43:
    LayoutParams localLayoutParams;
    if (paramBoolean)
    {
      paramInt2 = 0;
      j = paramInt1;
      i = 1;
      paramInt1 = paramInt2;
      if ((this.i != 1) || (!h())) {
        break label150;
      }
      paramInt2 = this.b - 1;
      k = -1;
      if (paramInt1 == j) {
        return;
      }
      View localView = this.d[paramInt1];
      localLayoutParams = (LayoutParams)localView.getLayoutParams();
      LayoutParams.a(localLayoutParams, c(paramRecycler, paramState, d(localView)));
      if ((k != -1) || (LayoutParams.b(localLayoutParams) <= 1)) {
        break label159;
      }
      LayoutParams.b(localLayoutParams, paramInt2 - (LayoutParams.b(localLayoutParams) - 1));
    }
    for (;;)
    {
      paramInt2 += LayoutParams.b(localLayoutParams) * k;
      paramInt1 += i;
      break label43;
      paramInt1 -= 1;
      j = -1;
      i = -1;
      break;
      label150:
      paramInt2 = 0;
      k = 1;
      break label43;
      label159:
      LayoutParams.b(localLayoutParams, paramInt2);
    }
  }
  
  private void a(View paramView, int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
  {
    a(paramView, this.h);
    RecyclerView.LayoutParams localLayoutParams = (RecyclerView.LayoutParams)paramView.getLayoutParams();
    int i;
    if (!paramBoolean1)
    {
      i = paramInt1;
      if (this.i != 1) {}
    }
    else
    {
      i = b(paramInt1, localLayoutParams.leftMargin + this.h.left, localLayoutParams.rightMargin + this.h.right);
    }
    if (!paramBoolean1)
    {
      paramInt1 = paramInt2;
      if (this.i != 0) {}
    }
    else
    {
      paramInt1 = b(paramInt2, localLayoutParams.topMargin + this.h.top, localLayoutParams.bottomMargin + this.h.bottom);
    }
    if (paramBoolean2) {}
    for (paramBoolean1 = a(paramView, i, paramInt1, localLayoutParams);; paramBoolean1 = b(paramView, i, paramInt1, localLayoutParams))
    {
      if (paramBoolean1) {
        paramView.measure(i, paramInt1);
      }
      return;
    }
  }
  
  static int[] a(int[] paramArrayOfInt, int paramInt1, int paramInt2)
  {
    int[] arrayOfInt;
    if ((paramArrayOfInt != null) && (paramArrayOfInt.length == paramInt1 + 1))
    {
      arrayOfInt = paramArrayOfInt;
      if (paramArrayOfInt[(paramArrayOfInt.length - 1)] == paramInt2) {}
    }
    else
    {
      arrayOfInt = new int[paramInt1 + 1];
    }
    arrayOfInt[0] = 0;
    int n = paramInt2 / paramInt1;
    int i2 = paramInt2 % paramInt1;
    int j = 0;
    paramInt2 = 0;
    int i = 1;
    while (i <= paramInt1)
    {
      int k = n;
      int i1 = paramInt2 + i2;
      paramInt2 = i1;
      int m = k;
      if (i1 > 0)
      {
        paramInt2 = i1;
        m = k;
        if (paramInt1 - i1 < i2)
        {
          m = k + 1;
          paramInt2 = i1 - paramInt1;
        }
      }
      j += m;
      arrayOfInt[i] = j;
      i += 1;
    }
    return arrayOfInt;
  }
  
  private int b(int paramInt1, int paramInt2, int paramInt3)
  {
    if ((paramInt2 == 0) && (paramInt3 == 0)) {}
    int i;
    do
    {
      return paramInt1;
      i = View.MeasureSpec.getMode(paramInt1);
    } while ((i != Integer.MIN_VALUE) && (i != 1073741824));
    return View.MeasureSpec.makeMeasureSpec(Math.max(0, View.MeasureSpec.getSize(paramInt1) - paramInt2 - paramInt3), i);
  }
  
  private int b(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, int paramInt)
  {
    if (!paramState.a()) {
      i = this.g.b(paramInt, this.b);
    }
    int j;
    do
    {
      return i;
      j = this.f.get(paramInt, -1);
      i = j;
    } while (j != -1);
    int i = paramRecycler.b(paramInt);
    if (i == -1)
    {
      Log.w("GridLayoutManager", "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:" + paramInt);
      return 0;
    }
    return this.g.b(i, this.b);
  }
  
  private void b(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, LinearLayoutManager.AnchorInfo paramAnchorInfo, int paramInt)
  {
    int i = 1;
    if (paramInt == 1) {}
    for (;;)
    {
      paramInt = b(paramRecycler, paramState, paramAnchorInfo.a);
      if (i == 0) {
        break;
      }
      while ((paramInt > 0) && (paramAnchorInfo.a > 0))
      {
        paramAnchorInfo.a -= 1;
        paramInt = b(paramRecycler, paramState, paramAnchorInfo.a);
      }
      i = 0;
    }
    int k = paramState.e();
    i = paramAnchorInfo.a;
    while (i < k - 1)
    {
      int j = b(paramRecycler, paramState, i + 1);
      if (j <= paramInt) {
        break;
      }
      i += 1;
      paramInt = j;
    }
    paramAnchorInfo.a = i;
  }
  
  private int c(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, int paramInt)
  {
    if (!paramState.a()) {
      i = this.g.a(paramInt);
    }
    int j;
    do
    {
      return i;
      j = this.e.get(paramInt, -1);
      i = j;
    } while (j != -1);
    int i = paramRecycler.b(paramInt);
    if (i == -1)
    {
      Log.w("GridLayoutManager", "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:" + paramInt);
      return 1;
    }
    return this.g.a(i);
  }
  
  private void m(int paramInt)
  {
    this.c = a(this.c, this.b, paramInt);
  }
  
  public int a(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    M();
    N();
    return super.a(paramInt, paramRecycler, paramState);
  }
  
  public int a(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    if (this.i == 0) {
      return this.b;
    }
    if (paramState.e() < 1) {
      return 0;
    }
    return a(paramRecycler, paramState, paramState.e() - 1) + 1;
  }
  
  public RecyclerView.LayoutParams a()
  {
    if (this.i == 0) {
      return new LayoutParams(-2, -1);
    }
    return new LayoutParams(-1, -2);
  }
  
  public RecyclerView.LayoutParams a(Context paramContext, AttributeSet paramAttributeSet)
  {
    return new LayoutParams(paramContext, paramAttributeSet);
  }
  
  public RecyclerView.LayoutParams a(ViewGroup.LayoutParams paramLayoutParams)
  {
    if ((paramLayoutParams instanceof ViewGroup.MarginLayoutParams)) {
      return new LayoutParams((ViewGroup.MarginLayoutParams)paramLayoutParams);
    }
    return new LayoutParams(paramLayoutParams);
  }
  
  View a(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, int paramInt1, int paramInt2, int paramInt3)
  {
    i();
    Object localObject2 = null;
    Object localObject1 = null;
    int j = this.j.c();
    int k = this.j.d();
    int i;
    View localView;
    Object localObject3;
    Object localObject4;
    if (paramInt2 > paramInt1)
    {
      i = 1;
      if (paramInt1 == paramInt2) {
        break label221;
      }
      localView = i(paramInt1);
      int m = d(localView);
      localObject3 = localObject2;
      localObject4 = localObject1;
      if (m >= 0)
      {
        localObject3 = localObject2;
        localObject4 = localObject1;
        if (m < paramInt3)
        {
          if (b(paramRecycler, paramState, m) == 0) {
            break label127;
          }
          localObject4 = localObject1;
          localObject3 = localObject2;
        }
      }
    }
    for (;;)
    {
      paramInt1 += i;
      localObject2 = localObject3;
      localObject1 = localObject4;
      break;
      i = -1;
      break;
      label127:
      if (((RecyclerView.LayoutParams)localView.getLayoutParams()).c())
      {
        localObject3 = localObject2;
        localObject4 = localObject1;
        if (localObject2 == null)
        {
          localObject3 = localView;
          localObject4 = localObject1;
        }
      }
      else
      {
        if (this.j.a(localView) < k)
        {
          localObject3 = localView;
          if (this.j.b(localView) >= j) {
            break label230;
          }
        }
        localObject3 = localObject2;
        localObject4 = localObject1;
        if (localObject1 == null)
        {
          localObject3 = localObject2;
          localObject4 = localView;
        }
      }
    }
    label221:
    if (localObject1 != null) {}
    for (;;)
    {
      localObject3 = localObject1;
      label230:
      return (View)localObject3;
      localObject1 = localObject2;
    }
  }
  
  public View a(View paramView, int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    View localView = e(paramView);
    if (localView == null) {
      paramRecycler = null;
    }
    LayoutParams localLayoutParams;
    int i4;
    int i5;
    label83:
    label100:
    int k;
    label118:
    int n;
    int i1;
    label159:
    label164:
    label178:
    label201:
    int i6;
    int i7;
    do
    {
      return paramRecycler;
      localLayoutParams = (LayoutParams)localView.getLayoutParams();
      i4 = LayoutParams.a(localLayoutParams);
      i5 = LayoutParams.a(localLayoutParams) + LayoutParams.b(localLayoutParams);
      if (super.a(paramView, paramInt, paramRecycler, paramState) == null) {
        return null;
      }
      int i8;
      int i;
      int j;
      int m;
      if (f(paramInt) == 1)
      {
        i8 = 1;
        if (i8 == this.k) {
          break label159;
        }
        paramInt = 1;
        if (paramInt == 0) {
          break label164;
        }
        paramInt = u() - 1;
        i = -1;
        j = -1;
        if ((this.i != 1) || (!h())) {
          break label178;
        }
        k = 1;
        paramState = null;
        n = -1;
        i1 = 0;
        m = paramInt;
      }
      for (;;)
      {
        if (m != j)
        {
          paramView = i(m);
          if (paramView != localView) {}
        }
        else
        {
          return paramState;
          i8 = 0;
          break;
          paramInt = 0;
          break label83;
          paramInt = 0;
          i = 1;
          j = u();
          break label100;
          k = 0;
          break label118;
        }
        if (paramView.isFocusable()) {
          break label201;
        }
        m += i;
      }
      localLayoutParams = (LayoutParams)paramView.getLayoutParams();
      i6 = LayoutParams.a(localLayoutParams);
      i7 = LayoutParams.a(localLayoutParams) + LayoutParams.b(localLayoutParams);
      if (i6 != i4) {
        break;
      }
      paramRecycler = paramView;
    } while (i7 == i5);
    int i3 = 0;
    if (paramState == null) {
      paramInt = 1;
    }
    for (;;)
    {
      label256:
      if (paramInt != 0)
      {
        n = LayoutParams.a(localLayoutParams);
        i1 = Math.min(i7, i5) - Math.max(i6, i4);
        paramState = paramView;
        break;
        paramInt = Math.max(i6, i4);
        i2 = Math.min(i7, i5) - paramInt;
        if (i2 > i1)
        {
          paramInt = 1;
        }
        else
        {
          paramInt = i3;
          if (i2 == i1) {
            if (i6 <= n) {
              break label356;
            }
          }
        }
      }
    }
    label356:
    for (int i2 = 1;; i2 = 0)
    {
      paramInt = i3;
      if (k != i2) {
        break label256;
      }
      paramInt = 1;
      break label256;
      break;
    }
  }
  
  public void a(int paramInt)
  {
    if (paramInt == this.b) {
      return;
    }
    this.a = true;
    if (paramInt < 1) {
      throw new IllegalArgumentException("Span count should be at least 1. Provided " + paramInt);
    }
    this.b = paramInt;
    this.g.a();
  }
  
  public void a(Rect paramRect, int paramInt1, int paramInt2)
  {
    if (this.c == null) {
      super.a(paramRect, paramInt1, paramInt2);
    }
    int j = z() + B();
    int k = A() + C();
    int i;
    if (this.i == 1)
    {
      i = a(paramInt2, paramRect.height() + k, G());
      paramInt2 = a(paramInt1, this.c[(this.c.length - 1)] + j, F());
      paramInt1 = i;
    }
    for (;;)
    {
      e(paramInt2, paramInt1);
      return;
      i = a(paramInt1, paramRect.width() + j, F());
      paramInt1 = a(paramInt2, this.c[(this.c.length - 1)] + k, G());
      paramInt2 = i;
    }
  }
  
  public void a(SpanSizeLookup paramSpanSizeLookup)
  {
    this.g = paramSpanSizeLookup;
  }
  
  void a(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, LinearLayoutManager.AnchorInfo paramAnchorInfo, int paramInt)
  {
    super.a(paramRecycler, paramState, paramAnchorInfo, paramInt);
    M();
    if ((paramState.e() > 0) && (!paramState.a())) {
      b(paramRecycler, paramState, paramAnchorInfo, paramInt);
    }
    N();
  }
  
  void a(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, LinearLayoutManager.LayoutState paramLayoutState, LinearLayoutManager.LayoutChunkResult paramLayoutChunkResult)
  {
    int i3 = this.j.i();
    label37:
    boolean bool1;
    label57:
    int i2;
    int n;
    if (i3 != 1073741824)
    {
      j = 1;
      if (u() <= 0) {
        break label225;
      }
      k = this.c[this.b];
      if (j != 0) {
        M();
      }
      if (paramLayoutState.e != 1) {
        break label231;
      }
      bool1 = true;
      i1 = 0;
      i2 = 0;
      i = this.b;
      n = i1;
      m = i2;
      if (!bool1)
      {
        i = b(paramRecycler, paramState, paramLayoutState.d) + c(paramRecycler, paramState, paramLayoutState.d);
        m = i2;
        n = i1;
      }
    }
    for (;;)
    {
      if ((n < this.b) && (paramLayoutState.a(paramState)) && (i > 0))
      {
        i2 = paramLayoutState.d;
        i1 = c(paramRecycler, paramState, i2);
        if (i1 > this.b)
        {
          throw new IllegalArgumentException("Item at position " + i2 + " requires " + i1 + " spans but GridLayoutManager has only " + this.b + " spans.");
          j = 0;
          break;
          label225:
          k = 0;
          break label37;
          label231:
          bool1 = false;
          break label57;
        }
        i -= i1;
        if (i >= 0) {
          break label261;
        }
      }
      label261:
      View localView;
      do
      {
        if (n != 0) {
          break;
        }
        paramLayoutChunkResult.b = true;
        return;
        localView = paramLayoutState.a(paramRecycler);
      } while (localView == null);
      m += i1;
      this.d[n] = localView;
      n += 1;
    }
    int i = 0;
    float f1 = 0.0F;
    a(paramRecycler, paramState, n, m, bool1);
    int m = 0;
    if (m < n)
    {
      paramRecycler = this.d[m];
      if (paramLayoutState.k == null) {
        if (bool1)
        {
          b(paramRecycler);
          label351:
          paramState = (LayoutParams)paramRecycler.getLayoutParams();
          i2 = this.c[(LayoutParams.a(paramState) + LayoutParams.b(paramState))];
          int i4 = this.c[LayoutParams.a(paramState)];
          if (this.i != 0) {
            break label599;
          }
          i1 = paramState.height;
          label399:
          i2 = a(i2 - i4, i3, 0, i1, false);
          i4 = this.j.f();
          int i5 = this.j.h();
          if (this.i != 1) {
            break label608;
          }
          i1 = paramState.height;
          label447:
          i1 = a(i4, i5, 0, i1, true);
          if (this.i != 1) {
            break label623;
          }
          if (paramState.height != -1) {
            break label617;
          }
        }
      }
      label599:
      label608:
      label617:
      for (boolean bool2 = true;; bool2 = false)
      {
        a(paramRecycler, i2, i1, bool2, false);
        i2 = this.j.c(paramRecycler);
        i1 = i;
        if (i2 > i) {
          i1 = i2;
        }
        float f3 = 1.0F * this.j.d(paramRecycler) / LayoutParams.b(paramState);
        float f2 = f1;
        if (f3 > f1) {
          f2 = f3;
        }
        m += 1;
        i = i1;
        f1 = f2;
        break;
        b(paramRecycler, 0);
        break label351;
        if (bool1)
        {
          a(paramRecycler);
          break label351;
        }
        a(paramRecycler, 0);
        break label351;
        i1 = paramState.width;
        break label399;
        i1 = paramState.width;
        break label447;
      }
      label623:
      if (paramState.width == -1) {}
      for (bool2 = true;; bool2 = false)
      {
        a(paramRecycler, i1, i2, bool2, false);
        break;
      }
    }
    m = i;
    if (j != 0)
    {
      a(f1, k);
      i = 0;
      j = 0;
      m = i;
      if (j < n)
      {
        paramRecycler = this.d[j];
        paramState = (LayoutParams)paramRecycler.getLayoutParams();
        m = this.c[(LayoutParams.a(paramState) + LayoutParams.b(paramState))];
        i1 = this.c[LayoutParams.a(paramState)];
        if (this.i == 0)
        {
          k = paramState.height;
          label745:
          m = a(m - i1, 1073741824, 0, k, false);
          i1 = this.j.f();
          i2 = this.j.h();
          if (this.i != 1) {
            break label872;
          }
          k = paramState.height;
          label793:
          k = a(i1, i2, 0, k, true);
          if (this.i != 1) {
            break label881;
          }
          a(paramRecycler, m, k, false, true);
        }
        for (;;)
        {
          m = this.j.c(paramRecycler);
          k = i;
          if (m > i) {
            k = m;
          }
          j += 1;
          i = k;
          break;
          k = paramState.width;
          break label745;
          label872:
          k = paramState.width;
          break label793;
          label881:
          a(paramRecycler, k, m, false, true);
        }
      }
    }
    int k = View.MeasureSpec.makeMeasureSpec(m, 1073741824);
    i = 0;
    if (i < n)
    {
      paramRecycler = this.d[i];
      if (this.j.c(paramRecycler) != m)
      {
        paramState = (LayoutParams)paramRecycler.getLayoutParams();
        i1 = this.c[(LayoutParams.a(paramState) + LayoutParams.b(paramState))];
        i2 = this.c[LayoutParams.a(paramState)];
        if (this.i != 0) {
          break label1027;
        }
        j = paramState.height;
        label983:
        j = a(i1 - i2, 1073741824, 0, j, false);
        if (this.i != 1) {
          break label1036;
        }
        a(paramRecycler, j, k, true, true);
      }
      for (;;)
      {
        i += 1;
        break;
        label1027:
        j = paramState.width;
        break label983;
        label1036:
        a(paramRecycler, k, j, true, true);
      }
    }
    paramLayoutChunkResult.a = m;
    int j = 0;
    k = 0;
    int i1 = 0;
    i = 0;
    if (this.i == 1) {
      if (paramLayoutState.f == -1)
      {
        i = paramLayoutState.b;
        m = i - m;
        i1 = 0;
        i2 = k;
        i3 = j;
        k = i1;
        i1 = i;
        label1117:
        if (k >= n) {
          break label1414;
        }
        paramRecycler = this.d[k];
        paramState = (LayoutParams)paramRecycler.getLayoutParams();
        if (this.i != 1) {
          break label1374;
        }
        if (!h()) {
          break label1342;
        }
        j = z() + this.c[(LayoutParams.a(paramState) + LayoutParams.b(paramState))];
        i = j - this.j.d(paramRecycler);
      }
    }
    for (;;)
    {
      a(paramRecycler, i + paramState.leftMargin, m + paramState.topMargin, j - paramState.rightMargin, i1 - paramState.bottomMargin);
      if ((paramState.c()) || (paramState.d())) {
        paramLayoutChunkResult.c = true;
      }
      paramLayoutChunkResult.d |= paramRecycler.isFocusable();
      k += 1;
      i3 = i;
      i2 = j;
      break label1117;
      i1 = paramLayoutState.b;
      i = i1 + m;
      m = i1;
      break;
      if (paramLayoutState.f == -1)
      {
        k = paramLayoutState.b;
        j = k - m;
        m = i1;
        break;
      }
      j = paramLayoutState.b;
      k = j + m;
      m = i1;
      break;
      label1342:
      i = z() + this.c[LayoutParams.a(paramState)];
      j = i + this.j.d(paramRecycler);
      continue;
      label1374:
      m = A() + this.c[LayoutParams.a(paramState)];
      i1 = m + this.j.d(paramRecycler);
      i = i3;
      j = i2;
    }
    label1414:
    Arrays.fill(this.d, null);
  }
  
  public void a(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, View paramView, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat)
  {
    ViewGroup.LayoutParams localLayoutParams = paramView.getLayoutParams();
    if (!(localLayoutParams instanceof LayoutParams))
    {
      super.a(paramView, paramAccessibilityNodeInfoCompat);
      return;
    }
    paramView = (LayoutParams)localLayoutParams;
    int i = a(paramRecycler, paramState, paramView.e());
    if (this.i == 0)
    {
      j = paramView.a();
      k = paramView.b();
      if ((this.b > 1) && (paramView.b() == this.b)) {}
      for (bool = true;; bool = false)
      {
        paramAccessibilityNodeInfoCompat.c(AccessibilityNodeInfoCompat.CollectionItemInfoCompat.a(j, k, i, 1, bool, false));
        return;
      }
    }
    int j = paramView.a();
    int k = paramView.b();
    if ((this.b > 1) && (paramView.b() == this.b)) {}
    for (boolean bool = true;; bool = false)
    {
      paramAccessibilityNodeInfoCompat.c(AccessibilityNodeInfoCompat.CollectionItemInfoCompat.a(i, 1, j, k, bool, false));
      return;
    }
  }
  
  public void a(RecyclerView paramRecyclerView)
  {
    this.g.a();
  }
  
  public void a(RecyclerView paramRecyclerView, int paramInt1, int paramInt2)
  {
    this.g.a();
  }
  
  public void a(RecyclerView paramRecyclerView, int paramInt1, int paramInt2, int paramInt3)
  {
    this.g.a();
  }
  
  public void a(RecyclerView paramRecyclerView, int paramInt1, int paramInt2, Object paramObject)
  {
    this.g.a();
  }
  
  public void a(boolean paramBoolean)
  {
    if (paramBoolean) {
      throw new UnsupportedOperationException("GridLayoutManager does not support stack from end. Consider using reverse layout");
    }
    super.a(false);
  }
  
  public boolean a(RecyclerView.LayoutParams paramLayoutParams)
  {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  public int b()
  {
    return this.b;
  }
  
  public int b(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    M();
    N();
    return super.b(paramInt, paramRecycler, paramState);
  }
  
  public int b(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    if (this.i == 1) {
      return this.b;
    }
    if (paramState.e() < 1) {
      return 0;
    }
    return a(paramRecycler, paramState, paramState.e() - 1) + 1;
  }
  
  public void b(RecyclerView paramRecyclerView, int paramInt1, int paramInt2)
  {
    this.g.a();
  }
  
  public void c(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    if (paramState.a()) {
      L();
    }
    super.c(paramRecycler, paramState);
    K();
    if (!paramState.a()) {
      this.a = false;
    }
  }
  
  public boolean c()
  {
    return (this.n == null) && (!this.a);
  }
  
  public static final class DefaultSpanSizeLookup
    extends GridLayoutManager.SpanSizeLookup
  {
    public int a(int paramInt)
    {
      return 1;
    }
    
    public int a(int paramInt1, int paramInt2)
    {
      return paramInt1 % paramInt2;
    }
  }
  
  public static class LayoutParams
    extends RecyclerView.LayoutParams
  {
    private int e = -1;
    private int f = 0;
    
    public LayoutParams(int paramInt1, int paramInt2)
    {
      super(paramInt2);
    }
    
    public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
    }
    
    public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
    }
    
    public LayoutParams(ViewGroup.MarginLayoutParams paramMarginLayoutParams)
    {
      super();
    }
    
    public int a()
    {
      return this.e;
    }
    
    public int b()
    {
      return this.f;
    }
  }
  
  public static abstract class SpanSizeLookup
  {
    final SparseIntArray a = new SparseIntArray();
    private boolean b = false;
    
    public abstract int a(int paramInt);
    
    public int a(int paramInt1, int paramInt2)
    {
      int n = a(paramInt1);
      if (n == paramInt2) {
        paramInt1 = 0;
      }
      int i;
      do
      {
        return paramInt1;
        int k = 0;
        int m = 0;
        i = k;
        int j = m;
        if (this.b)
        {
          i = k;
          j = m;
          if (this.a.size() > 0)
          {
            int i1 = b(paramInt1);
            i = k;
            j = m;
            if (i1 >= 0)
            {
              i = this.a.get(i1) + a(i1);
              j = i1 + 1;
            }
          }
        }
        if (j < paramInt1)
        {
          k = a(j);
          m = i + k;
          if (m == paramInt2) {
            i = 0;
          }
          for (;;)
          {
            j += 1;
            break;
            i = m;
            if (m > paramInt2) {
              i = k;
            }
          }
        }
        paramInt1 = i;
      } while (i + n <= paramInt2);
      return 0;
    }
    
    public void a()
    {
      this.a.clear();
    }
    
    int b(int paramInt)
    {
      int i = 0;
      int j = this.a.size() - 1;
      while (i <= j)
      {
        int k = i + j >>> 1;
        if (this.a.keyAt(k) < paramInt) {
          i = k + 1;
        } else {
          j = k - 1;
        }
      }
      paramInt = i - 1;
      if ((paramInt >= 0) && (paramInt < this.a.size())) {
        return this.a.keyAt(paramInt);
      }
      return -1;
    }
    
    int b(int paramInt1, int paramInt2)
    {
      int i;
      if (!this.b) {
        i = a(paramInt1, paramInt2);
      }
      int j;
      do
      {
        return i;
        j = this.a.get(paramInt1, -1);
        i = j;
      } while (j != -1);
      paramInt2 = a(paramInt1, paramInt2);
      this.a.put(paramInt1, paramInt2);
      return paramInt2;
    }
    
    public int c(int paramInt1, int paramInt2)
    {
      int i = 0;
      int j = 0;
      int i2 = a(paramInt1);
      int m = 0;
      if (m < paramInt1)
      {
        int n = a(m);
        int i1 = i + n;
        int k;
        if (i1 == paramInt2)
        {
          i = 0;
          k = j + 1;
        }
        for (;;)
        {
          m += 1;
          j = k;
          break;
          k = j;
          i = i1;
          if (i1 > paramInt2)
          {
            i = n;
            k = j + 1;
          }
        }
      }
      paramInt1 = j;
      if (i + i2 > paramInt2) {
        paramInt1 = j + 1;
      }
      return paramInt1;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/GridLayoutManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */