package android.support.v7.widget;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ActionProvider;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R.dimen;
import android.support.v7.appcompat.R.id;
import android.support.v7.appcompat.R.layout;
import android.support.v7.appcompat.R.string;
import android.support.v7.appcompat.R.styleable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

public class ActivityChooserView
  extends ViewGroup
  implements ActivityChooserModel.ActivityChooserModelClient
{
  ActionProvider a;
  private final ActivityChooserViewAdapter b;
  private final Callbacks c;
  private final LinearLayoutCompat d;
  private final Drawable e;
  private final FrameLayout f;
  private final ImageView g;
  private final FrameLayout h;
  private final ImageView i;
  private final int j;
  private final DataSetObserver k = new DataSetObserver()
  {
    public void onChanged()
    {
      super.onChanged();
      ActivityChooserView.a(ActivityChooserView.this).notifyDataSetChanged();
    }
    
    public void onInvalidated()
    {
      super.onInvalidated();
      ActivityChooserView.a(ActivityChooserView.this).notifyDataSetInvalidated();
    }
  };
  private final ViewTreeObserver.OnGlobalLayoutListener l = new ViewTreeObserver.OnGlobalLayoutListener()
  {
    public void onGlobalLayout()
    {
      if (ActivityChooserView.this.c())
      {
        if (ActivityChooserView.this.isShown()) {
          break label31;
        }
        ActivityChooserView.b(ActivityChooserView.this).i();
      }
      label31:
      do
      {
        return;
        ActivityChooserView.b(ActivityChooserView.this).c();
      } while (ActivityChooserView.this.a == null);
      ActivityChooserView.this.a.a(true);
    }
  };
  private ListPopupWindow m;
  private PopupWindow.OnDismissListener n;
  private boolean o;
  private int p = 4;
  private boolean q;
  private int r;
  
  public ActivityChooserView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public ActivityChooserView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public ActivityChooserView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    Object localObject = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ActivityChooserView, paramInt, 0);
    this.p = ((TypedArray)localObject).getInt(R.styleable.ActivityChooserView_initialActivityCount, 4);
    paramAttributeSet = ((TypedArray)localObject).getDrawable(R.styleable.ActivityChooserView_expandActivityOverflowButtonDrawable);
    ((TypedArray)localObject).recycle();
    LayoutInflater.from(getContext()).inflate(R.layout.abc_activity_chooser_view, this, true);
    this.c = new Callbacks(null);
    this.d = ((LinearLayoutCompat)findViewById(R.id.activity_chooser_view_content));
    this.e = this.d.getBackground();
    this.h = ((FrameLayout)findViewById(R.id.default_activity_button));
    this.h.setOnClickListener(this.c);
    this.h.setOnLongClickListener(this.c);
    this.i = ((ImageView)this.h.findViewById(R.id.image));
    localObject = (FrameLayout)findViewById(R.id.expand_activities_button);
    ((FrameLayout)localObject).setOnClickListener(this.c);
    ((FrameLayout)localObject).setOnTouchListener(new ListPopupWindow.ForwardingListener((View)localObject)
    {
      public ListPopupWindow a()
      {
        return ActivityChooserView.b(ActivityChooserView.this);
      }
      
      protected boolean b()
      {
        ActivityChooserView.this.a();
        return true;
      }
      
      protected boolean c()
      {
        ActivityChooserView.this.b();
        return true;
      }
    });
    this.f = ((FrameLayout)localObject);
    this.g = ((ImageView)((FrameLayout)localObject).findViewById(R.id.image));
    this.g.setImageDrawable(paramAttributeSet);
    this.b = new ActivityChooserViewAdapter(null);
    this.b.registerDataSetObserver(new DataSetObserver()
    {
      public void onChanged()
      {
        super.onChanged();
        ActivityChooserView.c(ActivityChooserView.this);
      }
    });
    paramContext = paramContext.getResources();
    this.j = Math.max(paramContext.getDisplayMetrics().widthPixels / 2, paramContext.getDimensionPixelSize(R.dimen.abc_config_prefDialogWidth));
  }
  
  private void a(int paramInt)
  {
    if (this.b.e() == null) {
      throw new IllegalStateException("No data model. Did you call #setDataModel?");
    }
    getViewTreeObserver().addOnGlobalLayoutListener(this.l);
    boolean bool;
    int i1;
    label59:
    label91:
    ListPopupWindow localListPopupWindow;
    if (this.h.getVisibility() == 0)
    {
      bool = true;
      int i2 = this.b.c();
      if (!bool) {
        break label191;
      }
      i1 = 1;
      if ((paramInt == Integer.MAX_VALUE) || (i2 <= paramInt + i1)) {
        break label196;
      }
      this.b.a(true);
      this.b.a(paramInt - 1);
      localListPopupWindow = getListPopupWindow();
      if (!localListPopupWindow.k())
      {
        if ((!this.o) && (bool)) {
          break label215;
        }
        this.b.a(true, bool);
      }
    }
    for (;;)
    {
      localListPopupWindow.f(Math.min(this.b.a(), this.j));
      localListPopupWindow.c();
      if (this.a != null) {
        this.a.a(true);
      }
      localListPopupWindow.m().setContentDescription(getContext().getString(R.string.abc_activitychooserview_choose_application));
      return;
      bool = false;
      break;
      label191:
      i1 = 0;
      break label59;
      label196:
      this.b.a(false);
      this.b.a(paramInt);
      break label91;
      label215:
      this.b.a(false, false);
    }
  }
  
  private void d()
  {
    if (this.b.getCount() > 0)
    {
      this.f.setEnabled(true);
      int i1 = this.b.c();
      int i2 = this.b.d();
      if ((i1 != 1) && ((i1 <= 1) || (i2 <= 0))) {
        break label161;
      }
      this.h.setVisibility(0);
      Object localObject = this.b.b();
      PackageManager localPackageManager = getContext().getPackageManager();
      this.i.setImageDrawable(((ResolveInfo)localObject).loadIcon(localPackageManager));
      if (this.r != 0)
      {
        localObject = ((ResolveInfo)localObject).loadLabel(localPackageManager);
        localObject = getContext().getString(this.r, new Object[] { localObject });
        this.h.setContentDescription((CharSequence)localObject);
      }
    }
    for (;;)
    {
      if (this.h.getVisibility() != 0) {
        break label173;
      }
      this.d.setBackgroundDrawable(this.e);
      return;
      this.f.setEnabled(false);
      break;
      label161:
      this.h.setVisibility(8);
    }
    label173:
    this.d.setBackgroundDrawable(null);
  }
  
  private ListPopupWindow getListPopupWindow()
  {
    if (this.m == null)
    {
      this.m = new ListPopupWindow(getContext());
      this.m.a(this.b);
      this.m.a(this);
      this.m.a(true);
      this.m.a(this.c);
      this.m.a(this.c);
    }
    return this.m;
  }
  
  public boolean a()
  {
    if ((c()) || (!this.q)) {
      return false;
    }
    this.o = false;
    a(this.p);
    return true;
  }
  
  public boolean b()
  {
    if (c())
    {
      getListPopupWindow().i();
      ViewTreeObserver localViewTreeObserver = getViewTreeObserver();
      if (localViewTreeObserver.isAlive()) {
        localViewTreeObserver.removeGlobalOnLayoutListener(this.l);
      }
    }
    return true;
  }
  
  public boolean c()
  {
    return getListPopupWindow().k();
  }
  
  public ActivityChooserModel getDataModel()
  {
    return this.b.e();
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    ActivityChooserModel localActivityChooserModel = this.b.e();
    if (localActivityChooserModel != null) {
      localActivityChooserModel.registerObserver(this.k);
    }
    this.q = true;
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    Object localObject = this.b.e();
    if (localObject != null) {
      ((ActivityChooserModel)localObject).unregisterObserver(this.k);
    }
    localObject = getViewTreeObserver();
    if (((ViewTreeObserver)localObject).isAlive()) {
      ((ViewTreeObserver)localObject).removeGlobalOnLayoutListener(this.l);
    }
    if (c()) {
      b();
    }
    this.q = false;
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.d.layout(0, 0, paramInt3 - paramInt1, paramInt4 - paramInt2);
    if (!c()) {
      b();
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    LinearLayoutCompat localLinearLayoutCompat = this.d;
    int i1 = paramInt2;
    if (this.h.getVisibility() != 0) {
      i1 = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(paramInt2), 1073741824);
    }
    measureChild(localLinearLayoutCompat, paramInt1, i1);
    setMeasuredDimension(localLinearLayoutCompat.getMeasuredWidth(), localLinearLayoutCompat.getMeasuredHeight());
  }
  
  public void setActivityChooserModel(ActivityChooserModel paramActivityChooserModel)
  {
    this.b.a(paramActivityChooserModel);
    if (c())
    {
      b();
      a();
    }
  }
  
  public void setDefaultActionButtonContentDescription(int paramInt)
  {
    this.r = paramInt;
  }
  
  public void setExpandActivityOverflowButtonContentDescription(int paramInt)
  {
    String str = getContext().getString(paramInt);
    this.g.setContentDescription(str);
  }
  
  public void setExpandActivityOverflowButtonDrawable(Drawable paramDrawable)
  {
    this.g.setImageDrawable(paramDrawable);
  }
  
  public void setInitialActivityCount(int paramInt)
  {
    this.p = paramInt;
  }
  
  public void setOnDismissListener(PopupWindow.OnDismissListener paramOnDismissListener)
  {
    this.n = paramOnDismissListener;
  }
  
  public void setProvider(ActionProvider paramActionProvider)
  {
    this.a = paramActionProvider;
  }
  
  private class ActivityChooserViewAdapter
    extends BaseAdapter
  {
    private ActivityChooserModel b;
    private int c = 4;
    private boolean d;
    private boolean e;
    private boolean f;
    
    private ActivityChooserViewAdapter() {}
    
    public int a()
    {
      int k = this.c;
      this.c = Integer.MAX_VALUE;
      int j = 0;
      View localView = null;
      int m = View.MeasureSpec.makeMeasureSpec(0, 0);
      int n = View.MeasureSpec.makeMeasureSpec(0, 0);
      int i1 = getCount();
      int i = 0;
      while (i < i1)
      {
        localView = getView(i, localView, null);
        localView.measure(m, n);
        j = Math.max(j, localView.getMeasuredWidth());
        i += 1;
      }
      this.c = k;
      return j;
    }
    
    public void a(int paramInt)
    {
      if (this.c != paramInt)
      {
        this.c = paramInt;
        notifyDataSetChanged();
      }
    }
    
    public void a(ActivityChooserModel paramActivityChooserModel)
    {
      ActivityChooserModel localActivityChooserModel = ActivityChooserView.a(ActivityChooserView.this).e();
      if ((localActivityChooserModel != null) && (ActivityChooserView.this.isShown())) {
        localActivityChooserModel.unregisterObserver(ActivityChooserView.i(ActivityChooserView.this));
      }
      this.b = paramActivityChooserModel;
      if ((paramActivityChooserModel != null) && (ActivityChooserView.this.isShown())) {
        paramActivityChooserModel.registerObserver(ActivityChooserView.i(ActivityChooserView.this));
      }
      notifyDataSetChanged();
    }
    
    public void a(boolean paramBoolean)
    {
      if (this.f != paramBoolean)
      {
        this.f = paramBoolean;
        notifyDataSetChanged();
      }
    }
    
    public void a(boolean paramBoolean1, boolean paramBoolean2)
    {
      if ((this.d != paramBoolean1) || (this.e != paramBoolean2))
      {
        this.d = paramBoolean1;
        this.e = paramBoolean2;
        notifyDataSetChanged();
      }
    }
    
    public ResolveInfo b()
    {
      return this.b.b();
    }
    
    public int c()
    {
      return this.b.a();
    }
    
    public int d()
    {
      return this.b.c();
    }
    
    public ActivityChooserModel e()
    {
      return this.b;
    }
    
    public boolean f()
    {
      return this.d;
    }
    
    public int getCount()
    {
      int j = this.b.a();
      int i = j;
      if (!this.d)
      {
        i = j;
        if (this.b.b() != null) {
          i = j - 1;
        }
      }
      j = Math.min(i, this.c);
      i = j;
      if (this.f) {
        i = j + 1;
      }
      return i;
    }
    
    public Object getItem(int paramInt)
    {
      switch (getItemViewType(paramInt))
      {
      default: 
        throw new IllegalArgumentException();
      case 1: 
        return null;
      }
      int i = paramInt;
      if (!this.d)
      {
        i = paramInt;
        if (this.b.b() != null) {
          i = paramInt + 1;
        }
      }
      return this.b.a(i);
    }
    
    public long getItemId(int paramInt)
    {
      return paramInt;
    }
    
    public int getItemViewType(int paramInt)
    {
      if ((this.f) && (paramInt == getCount() - 1)) {
        return 1;
      }
      return 0;
    }
    
    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      View localView;
      switch (getItemViewType(paramInt))
      {
      default: 
        throw new IllegalArgumentException();
      case 1: 
        if (paramView != null)
        {
          localView = paramView;
          if (paramView.getId() == 1) {}
        }
        else
        {
          localView = LayoutInflater.from(ActivityChooserView.this.getContext()).inflate(R.layout.abc_activity_chooser_view_list_item, paramViewGroup, false);
          localView.setId(1);
          ((TextView)localView.findViewById(R.id.title)).setText(ActivityChooserView.this.getContext().getString(R.string.abc_activity_chooser_view_see_all));
        }
        return localView;
      }
      if (paramView != null)
      {
        localView = paramView;
        if (paramView.getId() == R.id.list_item) {}
      }
      else
      {
        localView = LayoutInflater.from(ActivityChooserView.this.getContext()).inflate(R.layout.abc_activity_chooser_view_list_item, paramViewGroup, false);
      }
      paramView = ActivityChooserView.this.getContext().getPackageManager();
      paramViewGroup = (ImageView)localView.findViewById(R.id.icon);
      ResolveInfo localResolveInfo = (ResolveInfo)getItem(paramInt);
      paramViewGroup.setImageDrawable(localResolveInfo.loadIcon(paramView));
      ((TextView)localView.findViewById(R.id.title)).setText(localResolveInfo.loadLabel(paramView));
      if ((this.d) && (paramInt == 0) && (this.e)) {
        ViewCompat.c(localView, true);
      }
      for (;;)
      {
        return localView;
        ViewCompat.c(localView, false);
      }
    }
    
    public int getViewTypeCount()
    {
      return 3;
    }
  }
  
  private class Callbacks
    implements View.OnClickListener, View.OnLongClickListener, AdapterView.OnItemClickListener, PopupWindow.OnDismissListener
  {
    private Callbacks() {}
    
    private void a()
    {
      if (ActivityChooserView.h(ActivityChooserView.this) != null) {
        ActivityChooserView.h(ActivityChooserView.this).onDismiss();
      }
    }
    
    public void onClick(View paramView)
    {
      if (paramView == ActivityChooserView.e(ActivityChooserView.this))
      {
        ActivityChooserView.this.b();
        paramView = ActivityChooserView.a(ActivityChooserView.this).b();
        int i = ActivityChooserView.a(ActivityChooserView.this).e().a(paramView);
        paramView = ActivityChooserView.a(ActivityChooserView.this).e().b(i);
        if (paramView != null)
        {
          paramView.addFlags(524288);
          ActivityChooserView.this.getContext().startActivity(paramView);
        }
        return;
      }
      if (paramView == ActivityChooserView.f(ActivityChooserView.this))
      {
        ActivityChooserView.a(ActivityChooserView.this, false);
        ActivityChooserView.a(ActivityChooserView.this, ActivityChooserView.g(ActivityChooserView.this));
        return;
      }
      throw new IllegalArgumentException();
    }
    
    public void onDismiss()
    {
      a();
      if (ActivityChooserView.this.a != null) {
        ActivityChooserView.this.a.a(false);
      }
    }
    
    public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
    {
      switch (((ActivityChooserView.ActivityChooserViewAdapter)paramAdapterView.getAdapter()).getItemViewType(paramInt))
      {
      default: 
        throw new IllegalArgumentException();
      case 1: 
        ActivityChooserView.a(ActivityChooserView.this, Integer.MAX_VALUE);
      }
      do
      {
        return;
        ActivityChooserView.this.b();
        if (!ActivityChooserView.d(ActivityChooserView.this)) {
          break;
        }
      } while (paramInt <= 0);
      ActivityChooserView.a(ActivityChooserView.this).e().c(paramInt);
      return;
      if (ActivityChooserView.a(ActivityChooserView.this).f()) {}
      for (;;)
      {
        paramAdapterView = ActivityChooserView.a(ActivityChooserView.this).e().b(paramInt);
        if (paramAdapterView == null) {
          break;
        }
        paramAdapterView.addFlags(524288);
        ActivityChooserView.this.getContext().startActivity(paramAdapterView);
        return;
        paramInt += 1;
      }
    }
    
    public boolean onLongClick(View paramView)
    {
      if (paramView == ActivityChooserView.e(ActivityChooserView.this))
      {
        if (ActivityChooserView.a(ActivityChooserView.this).getCount() > 0)
        {
          ActivityChooserView.a(ActivityChooserView.this, true);
          ActivityChooserView.a(ActivityChooserView.this, ActivityChooserView.g(ActivityChooserView.this));
        }
        return true;
      }
      throw new IllegalArgumentException();
    }
  }
  
  public static class InnerLayout
    extends LinearLayoutCompat
  {
    private static final int[] a = { 16842964 };
    
    public InnerLayout(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
      paramContext = TintTypedArray.a(paramContext, paramAttributeSet, a);
      setBackgroundDrawable(paramContext.a(0));
      paramContext.a();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/ActivityChooserView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */