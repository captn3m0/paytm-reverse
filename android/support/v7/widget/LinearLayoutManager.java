package android.support.v7.widget;

import android.content.Context;
import android.graphics.PointF;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.support.v4.view.accessibility.AccessibilityRecordCompat;
import android.support.v7.widget.helper.ItemTouchHelper.ViewDropHandler;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import java.util.List;

public class LinearLayoutManager
  extends RecyclerView.LayoutManager
  implements ItemTouchHelper.ViewDropHandler
{
  private LayoutState a;
  private boolean b;
  private boolean c = false;
  private boolean d = false;
  private boolean e = true;
  private boolean f;
  int i;
  OrientationHelper j;
  boolean k = false;
  int l = -1;
  int m = Integer.MIN_VALUE;
  SavedState n = null;
  final AnchorInfo o = new AnchorInfo();
  
  public LinearLayoutManager(Context paramContext)
  {
    this(paramContext, 1, false);
  }
  
  public LinearLayoutManager(Context paramContext, int paramInt, boolean paramBoolean)
  {
    b(paramInt);
    b(paramBoolean);
    c(true);
  }
  
  public LinearLayoutManager(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
  {
    paramContext = a(paramContext, paramAttributeSet, paramInt1, paramInt2);
    b(paramContext.a);
    b(paramContext.c);
    a(paramContext.d);
    c(true);
  }
  
  private View K()
  {
    if (this.k) {}
    for (int i1 = u() - 1;; i1 = 0) {
      return i(i1);
    }
  }
  
  private View L()
  {
    if (this.k) {}
    for (int i1 = 0;; i1 = u() - 1) {
      return i(i1);
    }
  }
  
  private int a(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, boolean paramBoolean)
  {
    int i1 = this.j.d() - paramInt;
    if (i1 > 0)
    {
      i1 = -c(-i1, paramRecycler, paramState);
      if (paramBoolean)
      {
        paramInt = this.j.d() - (paramInt + i1);
        if (paramInt > 0)
        {
          this.j.a(paramInt);
          return paramInt + i1;
        }
      }
    }
    else
    {
      return 0;
    }
    return i1;
  }
  
  private View a(boolean paramBoolean1, boolean paramBoolean2)
  {
    if (this.k) {
      return a(u() - 1, -1, paramBoolean1, paramBoolean2);
    }
    return a(0, u(), paramBoolean1, paramBoolean2);
  }
  
  private void a(int paramInt1, int paramInt2, boolean paramBoolean, RecyclerView.State paramState)
  {
    int i1 = -1;
    int i2 = 1;
    this.a.l = k();
    this.a.h = a(paramState);
    this.a.f = paramInt1;
    if (paramInt1 == 1)
    {
      paramState = this.a;
      paramState.h += this.j.g();
      paramState = L();
      localLayoutState = this.a;
      if (this.k) {}
      for (paramInt1 = i1;; paramInt1 = 1)
      {
        localLayoutState.e = paramInt1;
        this.a.d = (d(paramState) + this.a.e);
        this.a.b = this.j.b(paramState);
        paramInt1 = this.j.b(paramState) - this.j.d();
        this.a.c = paramInt2;
        if (paramBoolean)
        {
          paramState = this.a;
          paramState.c -= paramInt1;
        }
        this.a.g = paramInt1;
        return;
      }
    }
    paramState = K();
    LayoutState localLayoutState = this.a;
    localLayoutState.h += this.j.c();
    localLayoutState = this.a;
    if (this.k) {}
    for (paramInt1 = i2;; paramInt1 = -1)
    {
      localLayoutState.e = paramInt1;
      this.a.d = (d(paramState) + this.a.e);
      this.a.b = this.j.a(paramState);
      paramInt1 = -this.j.a(paramState) + this.j.c();
      break;
    }
  }
  
  private void a(AnchorInfo paramAnchorInfo)
  {
    f(paramAnchorInfo.a, paramAnchorInfo.b);
  }
  
  private void a(RecyclerView.Recycler paramRecycler, int paramInt)
  {
    if (paramInt < 0) {}
    for (;;)
    {
      return;
      int i2 = u();
      int i1;
      View localView;
      if (this.k)
      {
        i1 = i2 - 1;
        while (i1 >= 0)
        {
          localView = i(i1);
          if (this.j.b(localView) > paramInt)
          {
            a(paramRecycler, i2 - 1, i1);
            return;
          }
          i1 -= 1;
        }
      }
      else
      {
        i1 = 0;
        while (i1 < i2)
        {
          localView = i(i1);
          if (this.j.b(localView) > paramInt)
          {
            a(paramRecycler, 0, i1);
            return;
          }
          i1 += 1;
        }
      }
    }
  }
  
  private void a(RecyclerView.Recycler paramRecycler, int paramInt1, int paramInt2)
  {
    if (paramInt1 == paramInt2) {}
    for (;;)
    {
      return;
      if (paramInt2 > paramInt1)
      {
        paramInt2 -= 1;
        while (paramInt2 >= paramInt1)
        {
          a(paramInt2, paramRecycler);
          paramInt2 -= 1;
        }
      }
      else
      {
        while (paramInt1 > paramInt2)
        {
          a(paramInt1, paramRecycler);
          paramInt1 -= 1;
        }
      }
    }
  }
  
  private void a(RecyclerView.Recycler paramRecycler, LayoutState paramLayoutState)
  {
    if ((!paramLayoutState.a) || (paramLayoutState.l)) {
      return;
    }
    if (paramLayoutState.f == -1)
    {
      b(paramRecycler, paramLayoutState.g);
      return;
    }
    a(paramRecycler, paramLayoutState.g);
  }
  
  private void a(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, AnchorInfo paramAnchorInfo)
  {
    if (a(paramState, paramAnchorInfo)) {}
    while (b(paramRecycler, paramState, paramAnchorInfo)) {
      return;
    }
    paramAnchorInfo.b();
    if (this.d) {}
    for (int i1 = paramState.e() - 1;; i1 = 0)
    {
      paramAnchorInfo.a = i1;
      return;
    }
  }
  
  private boolean a(RecyclerView.State paramState, AnchorInfo paramAnchorInfo)
  {
    boolean bool = false;
    if ((paramState.a()) || (this.l == -1)) {
      return false;
    }
    if ((this.l < 0) || (this.l >= paramState.e()))
    {
      this.l = -1;
      this.m = Integer.MIN_VALUE;
      return false;
    }
    paramAnchorInfo.a = this.l;
    if ((this.n != null) && (this.n.a()))
    {
      paramAnchorInfo.c = this.n.c;
      if (paramAnchorInfo.c)
      {
        paramAnchorInfo.b = (this.j.d() - this.n.b);
        return true;
      }
      paramAnchorInfo.b = (this.j.c() + this.n.b);
      return true;
    }
    if (this.m == Integer.MIN_VALUE)
    {
      paramState = c(this.l);
      int i1;
      if (paramState != null)
      {
        if (this.j.c(paramState) > this.j.f())
        {
          paramAnchorInfo.b();
          return true;
        }
        if (this.j.a(paramState) - this.j.c() < 0)
        {
          paramAnchorInfo.b = this.j.c();
          paramAnchorInfo.c = false;
          return true;
        }
        if (this.j.d() - this.j.b(paramState) < 0)
        {
          paramAnchorInfo.b = this.j.d();
          paramAnchorInfo.c = true;
          return true;
        }
        if (paramAnchorInfo.c) {}
        for (i1 = this.j.b(paramState) + this.j.b();; i1 = this.j.a(paramState))
        {
          paramAnchorInfo.b = i1;
          return true;
        }
      }
      if (u() > 0)
      {
        i1 = d(i(0));
        if (this.l >= i1) {
          break label351;
        }
      }
      label351:
      for (int i2 = 1;; i2 = 0)
      {
        if (i2 == this.k) {
          bool = true;
        }
        paramAnchorInfo.c = bool;
        paramAnchorInfo.b();
        return true;
      }
    }
    paramAnchorInfo.c = this.k;
    if (this.k)
    {
      paramAnchorInfo.b = (this.j.d() - this.m);
      return true;
    }
    paramAnchorInfo.b = (this.j.c() + this.m);
    return true;
  }
  
  private int b(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, boolean paramBoolean)
  {
    int i1 = paramInt - this.j.c();
    if (i1 > 0)
    {
      i1 = -c(i1, paramRecycler, paramState);
      if (paramBoolean)
      {
        paramInt = paramInt + i1 - this.j.c();
        if (paramInt > 0)
        {
          this.j.a(-paramInt);
          return i1 - paramInt;
        }
      }
    }
    else
    {
      return 0;
    }
    return i1;
  }
  
  private View b(boolean paramBoolean1, boolean paramBoolean2)
  {
    if (this.k) {
      return a(0, u(), paramBoolean1, paramBoolean2);
    }
    return a(u() - 1, -1, paramBoolean1, paramBoolean2);
  }
  
  private void b()
  {
    boolean bool = true;
    if ((this.i == 1) || (!h()))
    {
      this.k = this.c;
      return;
    }
    if (!this.c) {}
    for (;;)
    {
      this.k = bool;
      return;
      bool = false;
    }
  }
  
  private void b(AnchorInfo paramAnchorInfo)
  {
    g(paramAnchorInfo.a, paramAnchorInfo.b);
  }
  
  private void b(RecyclerView.Recycler paramRecycler, int paramInt)
  {
    int i1 = u();
    if (paramInt < 0) {}
    for (;;)
    {
      return;
      int i2 = this.j.e() - paramInt;
      View localView;
      if (this.k)
      {
        paramInt = 0;
        while (paramInt < i1)
        {
          localView = i(paramInt);
          if (this.j.a(localView) < i2)
          {
            a(paramRecycler, 0, paramInt);
            return;
          }
          paramInt += 1;
        }
      }
      else
      {
        paramInt = i1 - 1;
        while (paramInt >= 0)
        {
          localView = i(paramInt);
          if (this.j.a(localView) < i2)
          {
            a(paramRecycler, i1 - 1, paramInt);
            return;
          }
          paramInt -= 1;
        }
      }
    }
  }
  
  private void b(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, int paramInt1, int paramInt2)
  {
    if ((!paramState.b()) || (u() == 0) || (paramState.a()) || (!c())) {
      return;
    }
    int i2 = 0;
    int i3 = 0;
    List localList = paramRecycler.b();
    int i5 = localList.size();
    int i6 = d(i(0));
    int i1 = 0;
    if (i1 < i5)
    {
      RecyclerView.ViewHolder localViewHolder = (RecyclerView.ViewHolder)localList.get(i1);
      if (localViewHolder.isRemoved()) {}
      for (;;)
      {
        i1 += 1;
        break;
        int i7;
        if (localViewHolder.getLayoutPosition() < i6)
        {
          i7 = 1;
          label115:
          if (i7 == this.k) {
            break label159;
          }
        }
        label159:
        for (int i4 = -1;; i4 = 1)
        {
          if (i4 != -1) {
            break label165;
          }
          i2 += this.j.c(localViewHolder.itemView);
          break;
          i7 = 0;
          break label115;
        }
        label165:
        i3 += this.j.c(localViewHolder.itemView);
      }
    }
    this.a.k = localList;
    if (i2 > 0)
    {
      g(d(K()), paramInt1);
      this.a.h = i2;
      this.a.c = 0;
      this.a.a();
      a(paramRecycler, this.a, paramState, false);
    }
    if (i3 > 0)
    {
      f(d(L()), paramInt2);
      this.a.h = i3;
      this.a.c = 0;
      this.a.a();
      a(paramRecycler, this.a, paramState, false);
    }
    this.a.k = null;
  }
  
  private boolean b(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, AnchorInfo paramAnchorInfo)
  {
    if (u() == 0) {}
    do
    {
      return false;
      View localView = D();
      if ((localView != null) && (AnchorInfo.a(paramAnchorInfo, localView, paramState)))
      {
        paramAnchorInfo.a(localView);
        return true;
      }
    } while (this.b != this.d);
    if (paramAnchorInfo.c)
    {
      paramRecycler = f(paramRecycler, paramState);
      label63:
      if (paramRecycler == null) {
        break label162;
      }
      paramAnchorInfo.b(paramRecycler);
      if ((!paramState.a()) && (c()))
      {
        if ((this.j.a(paramRecycler) < this.j.d()) && (this.j.b(paramRecycler) >= this.j.c())) {
          break label164;
        }
        i1 = 1;
        label125:
        if (i1 != 0) {
          if (!paramAnchorInfo.c) {
            break label170;
          }
        }
      }
    }
    label162:
    label164:
    label170:
    for (int i1 = this.j.d();; i1 = this.j.c())
    {
      paramAnchorInfo.b = i1;
      return true;
      paramRecycler = g(paramRecycler, paramState);
      break label63;
      break;
      i1 = 0;
      break label125;
    }
  }
  
  private View f(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    if (this.k) {
      return h(paramRecycler, paramState);
    }
    return i(paramRecycler, paramState);
  }
  
  private void f(int paramInt1, int paramInt2)
  {
    this.a.c = (this.j.d() - paramInt2);
    LayoutState localLayoutState = this.a;
    if (this.k) {}
    for (int i1 = -1;; i1 = 1)
    {
      localLayoutState.e = i1;
      this.a.d = paramInt1;
      this.a.f = 1;
      this.a.b = paramInt2;
      this.a.g = Integer.MIN_VALUE;
      return;
    }
  }
  
  private View g(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    if (this.k) {
      return i(paramRecycler, paramState);
    }
    return h(paramRecycler, paramState);
  }
  
  private void g(int paramInt1, int paramInt2)
  {
    this.a.c = (paramInt2 - this.j.c());
    this.a.d = paramInt1;
    LayoutState localLayoutState = this.a;
    if (this.k) {}
    for (paramInt1 = 1;; paramInt1 = -1)
    {
      localLayoutState.e = paramInt1;
      this.a.f = -1;
      this.a.b = paramInt2;
      this.a.g = Integer.MIN_VALUE;
      return;
    }
  }
  
  private int h(RecyclerView.State paramState)
  {
    boolean bool2 = false;
    if (u() == 0) {
      return 0;
    }
    i();
    OrientationHelper localOrientationHelper = this.j;
    if (!this.e) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      View localView = a(bool1, true);
      bool1 = bool2;
      if (!this.e) {
        bool1 = true;
      }
      return ScrollbarHelper.a(paramState, localOrientationHelper, localView, b(bool1, true), this, this.e, this.k);
    }
  }
  
  private View h(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    return a(paramRecycler, paramState, 0, u(), paramState.e());
  }
  
  private int i(RecyclerView.State paramState)
  {
    boolean bool2 = false;
    if (u() == 0) {
      return 0;
    }
    i();
    OrientationHelper localOrientationHelper = this.j;
    if (!this.e) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      View localView = a(bool1, true);
      bool1 = bool2;
      if (!this.e) {
        bool1 = true;
      }
      return ScrollbarHelper.a(paramState, localOrientationHelper, localView, b(bool1, true), this, this.e);
    }
  }
  
  private View i(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    return a(paramRecycler, paramState, u() - 1, -1, paramState.e());
  }
  
  private int j(RecyclerView.State paramState)
  {
    boolean bool2 = false;
    if (u() == 0) {
      return 0;
    }
    i();
    OrientationHelper localOrientationHelper = this.j;
    if (!this.e) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      View localView = a(bool1, true);
      bool1 = bool2;
      if (!this.e) {
        bool1 = true;
      }
      return ScrollbarHelper.b(paramState, localOrientationHelper, localView, b(bool1, true), this, this.e);
    }
  }
  
  public int a(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    if (this.i == 1) {
      return 0;
    }
    return c(paramInt, paramRecycler, paramState);
  }
  
  int a(RecyclerView.Recycler paramRecycler, LayoutState paramLayoutState, RecyclerView.State paramState, boolean paramBoolean)
  {
    int i3 = paramLayoutState.c;
    if (paramLayoutState.g != Integer.MIN_VALUE)
    {
      if (paramLayoutState.c < 0) {
        paramLayoutState.g += paramLayoutState.c;
      }
      a(paramRecycler, paramLayoutState);
    }
    int i1 = paramLayoutState.c + paramLayoutState.h;
    LayoutChunkResult localLayoutChunkResult = new LayoutChunkResult();
    if (((paramLayoutState.l) || (i1 > 0)) && (paramLayoutState.a(paramState)))
    {
      localLayoutChunkResult.a();
      a(paramRecycler, paramState, paramLayoutState, localLayoutChunkResult);
      if (!localLayoutChunkResult.b) {
        break label111;
      }
    }
    for (;;)
    {
      return i3 - paramLayoutState.c;
      label111:
      paramLayoutState.b += localLayoutChunkResult.a * paramLayoutState.f;
      int i2;
      if ((localLayoutChunkResult.c) && (this.a.k == null))
      {
        i2 = i1;
        if (paramState.a()) {}
      }
      else
      {
        paramLayoutState.c -= localLayoutChunkResult.a;
        i2 = i1 - localLayoutChunkResult.a;
      }
      if (paramLayoutState.g != Integer.MIN_VALUE)
      {
        paramLayoutState.g += localLayoutChunkResult.a;
        if (paramLayoutState.c < 0) {
          paramLayoutState.g += paramLayoutState.c;
        }
        a(paramRecycler, paramLayoutState);
      }
      i1 = i2;
      if (!paramBoolean) {
        break;
      }
      i1 = i2;
      if (!localLayoutChunkResult.d) {
        break;
      }
    }
  }
  
  protected int a(RecyclerView.State paramState)
  {
    if (paramState.d()) {
      return this.j.f();
    }
    return 0;
  }
  
  public RecyclerView.LayoutParams a()
  {
    return new RecyclerView.LayoutParams(-2, -2);
  }
  
  View a(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
  {
    i();
    int i2 = this.j.c();
    int i3 = this.j.d();
    int i1;
    if (paramInt2 > paramInt1) {
      i1 = 1;
    }
    Object localObject2;
    for (Object localObject1 = null;; localObject1 = localObject2)
    {
      if (paramInt1 == paramInt2) {
        break label150;
      }
      View localView = i(paramInt1);
      int i4 = this.j.a(localView);
      int i5 = this.j.b(localView);
      localObject2 = localObject1;
      if (i4 < i3)
      {
        localObject2 = localObject1;
        if (i5 > i2)
        {
          if ((!paramBoolean1) || ((i4 >= i2) && (i5 <= i3)))
          {
            return localView;
            i1 = -1;
            break;
          }
          localObject2 = localObject1;
          if (paramBoolean2)
          {
            localObject2 = localObject1;
            if (localObject1 == null) {
              localObject2 = localView;
            }
          }
        }
      }
      paramInt1 += i1;
    }
    label150:
    return (View)localObject1;
  }
  
  View a(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, int paramInt1, int paramInt2, int paramInt3)
  {
    i();
    paramState = null;
    paramRecycler = null;
    int i2 = this.j.c();
    int i3 = this.j.d();
    int i1;
    View localView;
    Object localObject1;
    Object localObject2;
    if (paramInt2 > paramInt1)
    {
      i1 = 1;
      if (paramInt1 == paramInt2) {
        break label183;
      }
      localView = i(paramInt1);
      int i4 = d(localView);
      localObject1 = paramState;
      localObject2 = paramRecycler;
      if (i4 >= 0)
      {
        localObject1 = paramState;
        localObject2 = paramRecycler;
        if (i4 < paramInt3)
        {
          if (!((RecyclerView.LayoutParams)localView.getLayoutParams()).c()) {
            break label131;
          }
          localObject1 = paramState;
          localObject2 = paramRecycler;
          if (paramState == null)
          {
            localObject2 = paramRecycler;
            localObject1 = localView;
          }
        }
      }
    }
    for (;;)
    {
      paramInt1 += i1;
      paramState = (RecyclerView.State)localObject1;
      paramRecycler = (RecyclerView.Recycler)localObject2;
      break;
      i1 = -1;
      break;
      label131:
      if (this.j.a(localView) < i3)
      {
        localObject1 = localView;
        if (this.j.b(localView) >= i2) {
          break label190;
        }
      }
      localObject1 = paramState;
      localObject2 = paramRecycler;
      if (paramRecycler == null)
      {
        localObject1 = paramState;
        localObject2 = localView;
      }
    }
    label183:
    if (paramRecycler != null) {}
    for (;;)
    {
      localObject1 = paramRecycler;
      label190:
      return (View)localObject1;
      paramRecycler = paramState;
    }
  }
  
  public View a(View paramView, int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    b();
    if (u() == 0)
    {
      paramRecycler = null;
      return paramRecycler;
    }
    paramInt = f(paramInt);
    if (paramInt == Integer.MIN_VALUE) {
      return null;
    }
    i();
    if (paramInt == -1) {}
    for (View localView = g(paramRecycler, paramState); localView == null; localView = f(paramRecycler, paramState)) {
      return null;
    }
    i();
    a(paramInt, (int)(0.33333334F * this.j.f()), false, paramState);
    this.a.g = Integer.MIN_VALUE;
    this.a.a = false;
    a(paramRecycler, this.a, paramState, true);
    if (paramInt == -1) {}
    for (paramView = K();; paramView = L())
    {
      if (paramView != localView)
      {
        paramRecycler = paramView;
        if (paramView.isFocusable()) {
          break;
        }
      }
      return null;
    }
  }
  
  public void a(int paramInt1, int paramInt2)
  {
    this.l = paramInt1;
    this.m = paramInt2;
    if (this.n != null) {
      this.n.b();
    }
    o();
  }
  
  public void a(Parcelable paramParcelable)
  {
    if ((paramParcelable instanceof SavedState))
    {
      this.n = ((SavedState)paramParcelable);
      o();
    }
  }
  
  void a(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, AnchorInfo paramAnchorInfo, int paramInt) {}
  
  void a(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, LayoutState paramLayoutState, LayoutChunkResult paramLayoutChunkResult)
  {
    paramRecycler = paramLayoutState.a(paramRecycler);
    if (paramRecycler == null)
    {
      paramLayoutChunkResult.b = true;
      return;
    }
    paramState = (RecyclerView.LayoutParams)paramRecycler.getLayoutParams();
    boolean bool2;
    boolean bool1;
    label61:
    int i3;
    int i2;
    label120:
    int i1;
    int i4;
    if (paramLayoutState.k == null)
    {
      bool2 = this.k;
      if (paramLayoutState.f == -1)
      {
        bool1 = true;
        if (bool2 != bool1) {
          break label215;
        }
        b(paramRecycler);
        a(paramRecycler, 0, 0);
        paramLayoutChunkResult.a = this.j.c(paramRecycler);
        if (this.i != 1) {
          break label314;
        }
        if (!h()) {
          break label271;
        }
        i3 = x() - B();
        i2 = i3 - this.j.d(paramRecycler);
        if (paramLayoutState.f != -1) {
          break label293;
        }
        i1 = paramLayoutState.b;
        i4 = paramLayoutState.b - paramLayoutChunkResult.a;
      }
    }
    for (;;)
    {
      a(paramRecycler, i2 + paramState.leftMargin, i4 + paramState.topMargin, i3 - paramState.rightMargin, i1 - paramState.bottomMargin);
      if ((paramState.c()) || (paramState.d())) {
        paramLayoutChunkResult.c = true;
      }
      paramLayoutChunkResult.d = paramRecycler.isFocusable();
      return;
      bool1 = false;
      break;
      label215:
      b(paramRecycler, 0);
      break label61;
      bool2 = this.k;
      if (paramLayoutState.f == -1) {}
      for (bool1 = true;; bool1 = false)
      {
        if (bool2 != bool1) {
          break label262;
        }
        a(paramRecycler);
        break;
      }
      label262:
      a(paramRecycler, 0);
      break label61;
      label271:
      i2 = z();
      i3 = i2 + this.j.d(paramRecycler);
      break label120;
      label293:
      i4 = paramLayoutState.b;
      i1 = paramLayoutState.b + paramLayoutChunkResult.a;
      continue;
      label314:
      i4 = A();
      i1 = i4 + this.j.d(paramRecycler);
      if (paramLayoutState.f == -1)
      {
        i3 = paramLayoutState.b;
        i2 = paramLayoutState.b - paramLayoutChunkResult.a;
      }
      else
      {
        i2 = paramLayoutState.b;
        i3 = paramLayoutState.b + paramLayoutChunkResult.a;
      }
    }
  }
  
  public void a(RecyclerView paramRecyclerView, RecyclerView.Recycler paramRecycler)
  {
    super.a(paramRecyclerView, paramRecycler);
    if (this.f)
    {
      c(paramRecycler);
      paramRecycler.a();
    }
  }
  
  public void a(RecyclerView paramRecyclerView, RecyclerView.State paramState, int paramInt)
  {
    paramRecyclerView = new LinearSmoothScroller(paramRecyclerView.getContext())
    {
      public PointF a(int paramAnonymousInt)
      {
        return LinearLayoutManager.this.d(paramAnonymousInt);
      }
    };
    paramRecyclerView.d(paramInt);
    a(paramRecyclerView);
  }
  
  public void a(View paramView1, View paramView2, int paramInt1, int paramInt2)
  {
    a("Cannot drop a view during a scroll or layout calculation");
    i();
    b();
    paramInt1 = d(paramView1);
    paramInt2 = d(paramView2);
    if (paramInt1 < paramInt2) {
      paramInt1 = 1;
    }
    while (this.k) {
      if (paramInt1 == 1)
      {
        a(paramInt2, this.j.d() - (this.j.a(paramView2) + this.j.c(paramView1)));
        return;
        paramInt1 = -1;
      }
      else
      {
        a(paramInt2, this.j.d() - this.j.b(paramView2));
        return;
      }
    }
    if (paramInt1 == -1)
    {
      a(paramInt2, this.j.a(paramView2));
      return;
    }
    a(paramInt2, this.j.b(paramView2) - this.j.c(paramView1));
  }
  
  public void a(AccessibilityEvent paramAccessibilityEvent)
  {
    super.a(paramAccessibilityEvent);
    if (u() > 0)
    {
      paramAccessibilityEvent = AccessibilityEventCompat.a(paramAccessibilityEvent);
      paramAccessibilityEvent.b(m());
      paramAccessibilityEvent.c(n());
    }
  }
  
  public void a(String paramString)
  {
    if (this.n == null) {
      super.a(paramString);
    }
  }
  
  public void a(boolean paramBoolean)
  {
    a(null);
    if (this.d == paramBoolean) {
      return;
    }
    this.d = paramBoolean;
    o();
  }
  
  public int b(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    if (this.i == 0) {
      return 0;
    }
    return c(paramInt, paramRecycler, paramState);
  }
  
  public int b(RecyclerView.State paramState)
  {
    return h(paramState);
  }
  
  public void b(int paramInt)
  {
    if ((paramInt != 0) && (paramInt != 1)) {
      throw new IllegalArgumentException("invalid orientation:" + paramInt);
    }
    a(null);
    if (paramInt == this.i) {
      return;
    }
    this.i = paramInt;
    this.j = null;
    o();
  }
  
  public void b(boolean paramBoolean)
  {
    a(null);
    if (paramBoolean == this.c) {
      return;
    }
    this.c = paramBoolean;
    o();
  }
  
  int c(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    if ((u() == 0) || (paramInt == 0)) {}
    int i1;
    int i2;
    int i3;
    do
    {
      return 0;
      this.a.a = true;
      i();
      if (paramInt <= 0) {
        break;
      }
      i1 = 1;
      i2 = Math.abs(paramInt);
      a(i1, i2, true, paramState);
      i3 = this.a.g + a(paramRecycler, this.a, paramState, false);
    } while (i3 < 0);
    if (i2 > i3) {
      paramInt = i1 * i3;
    }
    for (;;)
    {
      this.j.a(-paramInt);
      this.a.j = paramInt;
      return paramInt;
      i1 = -1;
      break;
    }
  }
  
  public int c(RecyclerView.State paramState)
  {
    return h(paramState);
  }
  
  public View c(int paramInt)
  {
    int i1 = u();
    Object localObject;
    if (i1 == 0) {
      localObject = null;
    }
    View localView;
    do
    {
      return (View)localObject;
      int i2 = paramInt - d(i(0));
      if ((i2 < 0) || (i2 >= i1)) {
        break;
      }
      localView = i(i2);
      localObject = localView;
    } while (d(localView) == paramInt);
    return super.c(paramInt);
  }
  
  public void c(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    if (((this.n != null) || (this.l != -1)) && (paramState.e() == 0))
    {
      c(paramRecycler);
      return;
    }
    if ((this.n != null) && (this.n.a())) {
      this.l = this.n.a;
    }
    i();
    this.a.a = false;
    b();
    this.o.a();
    this.o.c = (this.k ^ this.d);
    a(paramRecycler, paramState, this.o);
    int i1 = a(paramState);
    int i2;
    int i4;
    Object localObject;
    label244:
    label257:
    label277:
    int i5;
    if (this.a.j >= 0)
    {
      i2 = 0;
      i3 = i2 + this.j.c();
      i4 = i1 + this.j.g();
      i1 = i4;
      i2 = i3;
      if (paramState.a())
      {
        i1 = i4;
        i2 = i3;
        if (this.l != -1)
        {
          i1 = i4;
          i2 = i3;
          if (this.m != Integer.MIN_VALUE)
          {
            localObject = c(this.l);
            i1 = i4;
            i2 = i3;
            if (localObject != null)
            {
              if (!this.k) {
                break label651;
              }
              i1 = this.j.d() - this.j.b((View)localObject) - this.m;
              if (i1 <= 0) {
                break label683;
              }
              i2 = i3 + i1;
              i1 = i4;
            }
          }
        }
      }
      if (!this.o.c) {
        break label701;
      }
      if (!this.k) {
        break label695;
      }
      i3 = 1;
      a(paramRecycler, paramState, this.o, i3);
      a(paramRecycler);
      this.a.l = k();
      this.a.i = paramState.a();
      if (!this.o.c) {
        break label720;
      }
      b(this.o);
      this.a.h = i2;
      a(paramRecycler, this.a, paramState, false);
      i3 = this.a.b;
      i5 = this.a.d;
      i2 = i1;
      if (this.a.c > 0) {
        i2 = i1 + this.a.c;
      }
      a(this.o);
      this.a.h = i2;
      localObject = this.a;
      ((LayoutState)localObject).d += this.a.e;
      a(paramRecycler, this.a, paramState, false);
      i4 = this.a.b;
      i1 = i4;
      i2 = i3;
      if (this.a.c > 0)
      {
        i1 = this.a.c;
        g(i5, i3);
        this.a.h = i1;
        a(paramRecycler, this.a, paramState, false);
        i2 = this.a.b;
        i1 = i4;
      }
      label524:
      i3 = i1;
      i4 = i2;
      if (u() > 0)
      {
        if (!(this.k ^ this.d)) {
          break label919;
        }
        i3 = a(i1, paramRecycler, paramState, true);
        i4 = i2 + i3;
        i2 = b(i4, paramRecycler, paramState, false);
        i4 += i2;
      }
    }
    for (int i3 = i1 + i3 + i2;; i3 = i1 + i5)
    {
      b(paramRecycler, paramState, i4, i3);
      if (!paramState.a())
      {
        this.l = -1;
        this.m = Integer.MIN_VALUE;
        this.j.a();
      }
      this.b = this.d;
      this.n = null;
      return;
      i2 = i1;
      i1 = 0;
      break;
      label651:
      i1 = this.j.a((View)localObject);
      i2 = this.j.c();
      i1 = this.m - (i1 - i2);
      break label244;
      label683:
      i1 = i4 - i1;
      i2 = i3;
      break label257;
      label695:
      i3 = -1;
      break label277;
      label701:
      if (this.k) {}
      for (i3 = -1;; i3 = 1) {
        break;
      }
      label720:
      a(this.o);
      this.a.h = i1;
      a(paramRecycler, this.a, paramState, false);
      i3 = this.a.b;
      i5 = this.a.d;
      i1 = i2;
      if (this.a.c > 0) {
        i1 = i2 + this.a.c;
      }
      b(this.o);
      this.a.h = i1;
      localObject = this.a;
      ((LayoutState)localObject).d += this.a.e;
      a(paramRecycler, this.a, paramState, false);
      i4 = this.a.b;
      i1 = i3;
      i2 = i4;
      if (this.a.c <= 0) {
        break label524;
      }
      i1 = this.a.c;
      f(i5, i3);
      this.a.h = i1;
      a(paramRecycler, this.a, paramState, false);
      i1 = this.a.b;
      i2 = i4;
      break label524;
      label919:
      i3 = b(i2, paramRecycler, paramState, true);
      i1 += i3;
      i5 = a(i1, paramRecycler, paramState, false);
      i4 = i2 + i3 + i5;
    }
  }
  
  public boolean c()
  {
    return (this.n == null) && (this.b == this.d);
  }
  
  public int d(RecyclerView.State paramState)
  {
    return i(paramState);
  }
  
  public PointF d(int paramInt)
  {
    int i1 = 0;
    if (u() == 0) {
      return null;
    }
    if (paramInt < d(i(0))) {
      i1 = 1;
    }
    if (i1 != this.k) {}
    for (paramInt = -1; this.i == 0; paramInt = 1) {
      return new PointF(paramInt, 0.0F);
    }
    return new PointF(0.0F, paramInt);
  }
  
  public Parcelable d()
  {
    if (this.n != null) {
      return new SavedState(this.n);
    }
    SavedState localSavedState = new SavedState();
    if (u() > 0)
    {
      i();
      boolean bool = this.b ^ this.k;
      localSavedState.c = bool;
      if (bool)
      {
        localView = L();
        localSavedState.b = (this.j.d() - this.j.b(localView));
        localSavedState.a = d(localView);
        return localSavedState;
      }
      View localView = K();
      localSavedState.a = d(localView);
      localSavedState.b = (this.j.a(localView) - this.j.c());
      return localSavedState;
    }
    localSavedState.b();
    return localSavedState;
  }
  
  public int e(RecyclerView.State paramState)
  {
    return i(paramState);
  }
  
  public void e(int paramInt)
  {
    this.l = paramInt;
    this.m = Integer.MIN_VALUE;
    if (this.n != null) {
      this.n.b();
    }
    o();
  }
  
  public boolean e()
  {
    return this.i == 0;
  }
  
  int f(int paramInt)
  {
    int i2 = -1;
    int i3 = 1;
    int i4 = Integer.MIN_VALUE;
    int i1 = i2;
    switch (paramInt)
    {
    default: 
      i1 = Integer.MIN_VALUE;
    case 1: 
    case 2: 
    case 33: 
    case 130: 
    case 17: 
      do
      {
        do
        {
          return i1;
          return 1;
          i1 = i2;
        } while (this.i == 1);
        return Integer.MIN_VALUE;
        paramInt = i4;
        if (this.i == 1) {
          paramInt = 1;
        }
        return paramInt;
        i1 = i2;
      } while (this.i == 0);
      return Integer.MIN_VALUE;
    }
    if (this.i == 0) {}
    for (paramInt = i3;; paramInt = Integer.MIN_VALUE) {
      return paramInt;
    }
  }
  
  public int f(RecyclerView.State paramState)
  {
    return j(paramState);
  }
  
  public boolean f()
  {
    return this.i == 1;
  }
  
  public int g()
  {
    return this.i;
  }
  
  public int g(RecyclerView.State paramState)
  {
    return j(paramState);
  }
  
  protected boolean h()
  {
    return s() == 1;
  }
  
  void i()
  {
    if (this.a == null) {
      this.a = j();
    }
    if (this.j == null) {
      this.j = OrientationHelper.a(this, this.i);
    }
  }
  
  LayoutState j()
  {
    return new LayoutState();
  }
  
  boolean k()
  {
    return (this.j.h() == 0) && (this.j.e() == 0);
  }
  
  boolean l()
  {
    return (w() != 1073741824) && (v() != 1073741824) && (J());
  }
  
  public int m()
  {
    View localView = a(0, u(), false, true);
    if (localView == null) {
      return -1;
    }
    return d(localView);
  }
  
  public int n()
  {
    View localView = a(u() - 1, -1, false, true);
    if (localView == null) {
      return -1;
    }
    return d(localView);
  }
  
  class AnchorInfo
  {
    int a;
    int b;
    boolean c;
    
    AnchorInfo() {}
    
    private boolean a(View paramView, RecyclerView.State paramState)
    {
      paramView = (RecyclerView.LayoutParams)paramView.getLayoutParams();
      return (!paramView.c()) && (paramView.e() >= 0) && (paramView.e() < paramState.e());
    }
    
    void a()
    {
      this.a = -1;
      this.b = Integer.MIN_VALUE;
      this.c = false;
    }
    
    public void a(View paramView)
    {
      int j = LinearLayoutManager.this.j.b();
      if (j >= 0) {
        b(paramView);
      }
      int i;
      do
      {
        int k;
        do
        {
          do
          {
            do
            {
              return;
              this.a = LinearLayoutManager.this.d(paramView);
              if (!this.c) {
                break;
              }
              i = LinearLayoutManager.this.j.d() - j - LinearLayoutManager.this.j.b(paramView);
              this.b = (LinearLayoutManager.this.j.d() - i);
            } while (i <= 0);
            j = LinearLayoutManager.this.j.c(paramView);
            k = this.b;
            m = LinearLayoutManager.this.j.c();
            j = k - j - (m + Math.min(LinearLayoutManager.this.j.a(paramView) - m, 0));
          } while (j >= 0);
          this.b += Math.min(i, -j);
          return;
          k = LinearLayoutManager.this.j.a(paramView);
          i = k - LinearLayoutManager.this.j.c();
          this.b = k;
        } while (i <= 0);
        int m = LinearLayoutManager.this.j.c(paramView);
        int n = LinearLayoutManager.this.j.d();
        int i1 = LinearLayoutManager.this.j.b(paramView);
        j = LinearLayoutManager.this.j.d() - Math.min(0, n - j - i1) - (k + m);
      } while (j >= 0);
      this.b -= Math.min(i, -j);
    }
    
    void b()
    {
      if (this.c) {}
      for (int i = LinearLayoutManager.this.j.d();; i = LinearLayoutManager.this.j.c())
      {
        this.b = i;
        return;
      }
    }
    
    public void b(View paramView)
    {
      if (this.c) {}
      for (this.b = (LinearLayoutManager.this.j.b(paramView) + LinearLayoutManager.this.j.b());; this.b = LinearLayoutManager.this.j.a(paramView))
      {
        this.a = LinearLayoutManager.this.d(paramView);
        return;
      }
    }
    
    public String toString()
    {
      return "AnchorInfo{mPosition=" + this.a + ", mCoordinate=" + this.b + ", mLayoutFromEnd=" + this.c + '}';
    }
  }
  
  protected static class LayoutChunkResult
  {
    public int a;
    public boolean b;
    public boolean c;
    public boolean d;
    
    void a()
    {
      this.a = 0;
      this.b = false;
      this.c = false;
      this.d = false;
    }
  }
  
  static class LayoutState
  {
    boolean a = true;
    int b;
    int c;
    int d;
    int e;
    int f;
    int g;
    int h = 0;
    boolean i = false;
    int j;
    List<RecyclerView.ViewHolder> k = null;
    boolean l;
    
    private View b()
    {
      int n = this.k.size();
      int m = 0;
      if (m < n)
      {
        View localView = ((RecyclerView.ViewHolder)this.k.get(m)).itemView;
        RecyclerView.LayoutParams localLayoutParams = (RecyclerView.LayoutParams)localView.getLayoutParams();
        if (localLayoutParams.c()) {}
        while (this.d != localLayoutParams.e())
        {
          m += 1;
          break;
        }
        a(localView);
        return localView;
      }
      return null;
    }
    
    View a(RecyclerView.Recycler paramRecycler)
    {
      if (this.k != null) {
        return b();
      }
      paramRecycler = paramRecycler.c(this.d);
      this.d += this.e;
      return paramRecycler;
    }
    
    public void a()
    {
      a(null);
    }
    
    public void a(View paramView)
    {
      paramView = b(paramView);
      if (paramView == null)
      {
        this.d = -1;
        return;
      }
      this.d = ((RecyclerView.LayoutParams)paramView.getLayoutParams()).e();
    }
    
    boolean a(RecyclerView.State paramState)
    {
      return (this.d >= 0) && (this.d < paramState.e());
    }
    
    public View b(View paramView)
    {
      int i3 = this.k.size();
      Object localObject1 = null;
      int n = Integer.MAX_VALUE;
      int m = 0;
      Object localObject2 = localObject1;
      if (m < i3)
      {
        View localView = ((RecyclerView.ViewHolder)this.k.get(m)).itemView;
        RecyclerView.LayoutParams localLayoutParams = (RecyclerView.LayoutParams)localView.getLayoutParams();
        localObject2 = localObject1;
        int i1 = n;
        if (localView != paramView)
        {
          if (!localLayoutParams.c()) {
            break label99;
          }
          i1 = n;
          localObject2 = localObject1;
        }
        label99:
        int i2;
        do
        {
          do
          {
            do
            {
              m += 1;
              localObject1 = localObject2;
              n = i1;
              break;
              i2 = (localLayoutParams.e() - this.d) * this.e;
              localObject2 = localObject1;
              i1 = n;
            } while (i2 < 0);
            localObject2 = localObject1;
            i1 = n;
          } while (i2 >= n);
          localObject1 = localView;
          i1 = i2;
          localObject2 = localObject1;
        } while (i2 != 0);
        localObject2 = localObject1;
      }
      return (View)localObject2;
    }
  }
  
  public static class SavedState
    implements Parcelable
  {
    public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
    {
      public LinearLayoutManager.SavedState a(Parcel paramAnonymousParcel)
      {
        return new LinearLayoutManager.SavedState(paramAnonymousParcel);
      }
      
      public LinearLayoutManager.SavedState[] a(int paramAnonymousInt)
      {
        return new LinearLayoutManager.SavedState[paramAnonymousInt];
      }
    };
    int a;
    int b;
    boolean c;
    
    public SavedState() {}
    
    SavedState(Parcel paramParcel)
    {
      this.a = paramParcel.readInt();
      this.b = paramParcel.readInt();
      if (paramParcel.readInt() == 1) {}
      for (;;)
      {
        this.c = bool;
        return;
        bool = false;
      }
    }
    
    public SavedState(SavedState paramSavedState)
    {
      this.a = paramSavedState.a;
      this.b = paramSavedState.b;
      this.c = paramSavedState.c;
    }
    
    boolean a()
    {
      return this.a >= 0;
    }
    
    void b()
    {
      this.a = -1;
    }
    
    public int describeContents()
    {
      return 0;
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      paramParcel.writeInt(this.a);
      paramParcel.writeInt(this.b);
      if (this.c) {}
      for (paramInt = 1;; paramInt = 0)
      {
        paramParcel.writeInt(paramInt);
        return;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/LinearLayoutManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */