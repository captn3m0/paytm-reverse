package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R.styleable;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class LinearLayoutCompat
  extends ViewGroup
{
  private boolean a = true;
  private int b = -1;
  private int c = 0;
  private int d;
  private int e = 8388659;
  private int f;
  private float g;
  private boolean h;
  private int[] i;
  private int[] j;
  private Drawable k;
  private int l;
  private int m;
  private int n;
  private int o;
  
  public LinearLayoutCompat(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public LinearLayoutCompat(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public LinearLayoutCompat(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    paramContext = TintTypedArray.a(paramContext, paramAttributeSet, R.styleable.LinearLayoutCompat, paramInt, 0);
    paramInt = paramContext.a(R.styleable.LinearLayoutCompat_android_orientation, -1);
    if (paramInt >= 0) {
      setOrientation(paramInt);
    }
    paramInt = paramContext.a(R.styleable.LinearLayoutCompat_android_gravity, -1);
    if (paramInt >= 0) {
      setGravity(paramInt);
    }
    boolean bool = paramContext.a(R.styleable.LinearLayoutCompat_android_baselineAligned, true);
    if (!bool) {
      setBaselineAligned(bool);
    }
    this.g = paramContext.a(R.styleable.LinearLayoutCompat_android_weightSum, -1.0F);
    this.b = paramContext.a(R.styleable.LinearLayoutCompat_android_baselineAlignedChildIndex, -1);
    this.h = paramContext.a(R.styleable.LinearLayoutCompat_measureWithLargestChild, false);
    setDividerDrawable(paramContext.a(R.styleable.LinearLayoutCompat_divider));
    this.n = paramContext.a(R.styleable.LinearLayoutCompat_showDividers, 0);
    this.o = paramContext.e(R.styleable.LinearLayoutCompat_dividerPadding, 0);
    paramContext.a();
  }
  
  private void a(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    paramView.layout(paramInt1, paramInt2, paramInt1 + paramInt3, paramInt2 + paramInt4);
  }
  
  private void c(int paramInt1, int paramInt2)
  {
    int i2 = View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824);
    int i1 = 0;
    while (i1 < paramInt1)
    {
      View localView = b(i1);
      if (localView.getVisibility() != 8)
      {
        LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
        if (localLayoutParams.width == -1)
        {
          int i3 = localLayoutParams.height;
          localLayoutParams.height = localView.getMeasuredHeight();
          measureChildWithMargins(localView, i2, 0, paramInt2, 0);
          localLayoutParams.height = i3;
        }
      }
      i1 += 1;
    }
  }
  
  private void d(int paramInt1, int paramInt2)
  {
    int i2 = View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 1073741824);
    int i1 = 0;
    while (i1 < paramInt1)
    {
      View localView = b(i1);
      if (localView.getVisibility() != 8)
      {
        LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
        if (localLayoutParams.height == -1)
        {
          int i3 = localLayoutParams.width;
          localLayoutParams.width = localView.getMeasuredWidth();
          measureChildWithMargins(localView, paramInt2, 0, i2, 0);
          localLayoutParams.width = i3;
        }
      }
      i1 += 1;
    }
  }
  
  int a(View paramView)
  {
    return 0;
  }
  
  int a(View paramView, int paramInt)
  {
    return 0;
  }
  
  void a(int paramInt1, int paramInt2)
  {
    this.f = 0;
    int i4 = 0;
    int i3 = 0;
    int i1 = 0;
    int i6 = 0;
    int i2 = 1;
    float f1 = 0.0F;
    int i13 = getVirtualChildCount();
    int i14 = View.MeasureSpec.getMode(paramInt1);
    int i15 = View.MeasureSpec.getMode(paramInt2);
    int i5 = 0;
    int i9 = 0;
    int i16 = this.b;
    boolean bool = this.h;
    int i8 = Integer.MIN_VALUE;
    int i7 = 0;
    int i11;
    label147:
    LayoutParams localLayoutParams;
    int i10;
    if (i7 < i13)
    {
      localView = b(i7);
      if (localView == null) {
        this.f += d(i7);
      }
      for (i11 = i8;; i11 = i8)
      {
        i7 += 1;
        i8 = i11;
        break;
        if (localView.getVisibility() != 8) {
          break label147;
        }
        i7 += a(localView, i7);
      }
      if (c(i7)) {
        this.f += this.m;
      }
      localLayoutParams = (LayoutParams)localView.getLayoutParams();
      f1 += localLayoutParams.g;
      if ((i15 == 1073741824) && (localLayoutParams.height == 0) && (localLayoutParams.g > 0.0F))
      {
        i9 = this.f;
        this.f = Math.max(i9, localLayoutParams.topMargin + i9 + localLayoutParams.bottomMargin);
        i10 = 1;
        i11 = i8;
        if ((i16 >= 0) && (i16 == i7 + 1)) {
          this.c = this.f;
        }
        if ((i7 < i16) && (localLayoutParams.g > 0.0F)) {
          throw new RuntimeException("A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won't work.  Either remove the weight, or don't set mBaselineAlignedChildIndex.");
        }
      }
      else
      {
        i11 = Integer.MIN_VALUE;
        i10 = i11;
        if (localLayoutParams.height == 0)
        {
          i10 = i11;
          if (localLayoutParams.g > 0.0F)
          {
            i10 = 0;
            localLayoutParams.height = -2;
          }
        }
        if (f1 == 0.0F) {}
        for (i11 = this.f;; i11 = 0)
        {
          a(localView, i7, paramInt1, 0, paramInt2, i11);
          if (i10 != Integer.MIN_VALUE) {
            localLayoutParams.height = i10;
          }
          i12 = localView.getMeasuredHeight();
          i10 = this.f;
          this.f = Math.max(i10, i10 + i12 + localLayoutParams.topMargin + localLayoutParams.bottomMargin + b(localView));
          i11 = i8;
          i10 = i9;
          if (!bool) {
            break;
          }
          i11 = Math.max(i12, i8);
          i10 = i9;
          break;
        }
      }
      i12 = 0;
      i8 = i5;
      i9 = i12;
      if (i14 != 1073741824)
      {
        i8 = i5;
        i9 = i12;
        if (localLayoutParams.width == -1)
        {
          i8 = 1;
          i9 = 1;
        }
      }
      i5 = localLayoutParams.leftMargin + localLayoutParams.rightMargin;
      i12 = localView.getMeasuredWidth() + i5;
      i4 = Math.max(i4, i12);
      i3 = ViewUtils.a(i3, ViewCompat.l(localView));
      if ((i2 != 0) && (localLayoutParams.width == -1))
      {
        i2 = 1;
        label559:
        if (localLayoutParams.g <= 0.0F) {
          break label620;
        }
        if (i9 == 0) {
          break label613;
        }
      }
      for (;;)
      {
        i6 = Math.max(i6, i5);
        i7 += a(localView, i7);
        i5 = i8;
        i9 = i10;
        break;
        i2 = 0;
        break label559;
        label613:
        i5 = i12;
      }
      label620:
      if (i9 != 0) {}
      for (;;)
      {
        i1 = Math.max(i1, i5);
        break;
        i5 = i12;
      }
    }
    if ((this.f > 0) && (c(i13))) {
      this.f += this.m;
    }
    if ((bool) && ((i15 == Integer.MIN_VALUE) || (i15 == 0)))
    {
      this.f = 0;
      i7 = 0;
      if (i7 < i13)
      {
        localView = b(i7);
        if (localView == null) {
          this.f += d(i7);
        }
        for (;;)
        {
          i7 += 1;
          break;
          if (localView.getVisibility() == 8)
          {
            i7 += a(localView, i7);
          }
          else
          {
            localLayoutParams = (LayoutParams)localView.getLayoutParams();
            i10 = this.f;
            this.f = Math.max(i10, i10 + i8 + localLayoutParams.topMargin + localLayoutParams.bottomMargin + b(localView));
          }
        }
      }
    }
    this.f += getPaddingTop() + getPaddingBottom();
    int i12 = ViewCompat.a(Math.max(this.f, getSuggestedMinimumHeight()), paramInt2, 0);
    i7 = (i12 & 0xFFFFFF) - this.f;
    if ((i9 != 0) || ((i7 != 0) && (f1 > 0.0F)))
    {
      if (this.g > 0.0F) {
        f1 = this.g;
      }
      for (;;)
      {
        this.f = 0;
        i9 = 0;
        i6 = i4;
        for (i8 = i7;; i8 = i4)
        {
          if (i9 >= i13) {
            break label1330;
          }
          localView = b(i9);
          if (localView.getVisibility() != 8) {
            break;
          }
          i4 = i8;
          i7 = i3;
          i3 = i1;
          i1 = i2;
          i9 += 1;
          i2 = i1;
          i1 = i3;
          i3 = i7;
        }
      }
      localLayoutParams = (LayoutParams)localView.getLayoutParams();
      float f3 = localLayoutParams.g;
      i7 = i3;
      i4 = i8;
      float f2 = f1;
      if (f3 > 0.0F)
      {
        i4 = (int)(i8 * f3 / f1);
        f2 = f1 - f3;
        i7 = i8 - i4;
        i10 = getChildMeasureSpec(paramInt1, getPaddingLeft() + getPaddingRight() + localLayoutParams.leftMargin + localLayoutParams.rightMargin, localLayoutParams.width);
        if ((localLayoutParams.height != 0) || (i15 != 1073741824))
        {
          i8 = localView.getMeasuredHeight() + i4;
          i4 = i8;
          if (i8 < 0) {
            i4 = 0;
          }
          localView.measure(i10, View.MeasureSpec.makeMeasureSpec(i4, 1073741824));
          i3 = ViewUtils.a(i3, ViewCompat.l(localView) & 0xFF00);
          i4 = i7;
          i7 = i3;
        }
      }
      else
      {
        i8 = localLayoutParams.leftMargin + localLayoutParams.rightMargin;
        i10 = localView.getMeasuredWidth() + i8;
        i6 = Math.max(i6, i10);
        if ((i14 == 1073741824) || (localLayoutParams.width != -1)) {
          break label1311;
        }
        i3 = 1;
        label1200:
        if (i3 == 0) {
          break label1317;
        }
        i3 = i8;
        label1209:
        i3 = Math.max(i1, i3);
        if ((i2 == 0) || (localLayoutParams.width != -1)) {
          break label1324;
        }
      }
      label1311:
      label1317:
      label1324:
      for (i1 = 1;; i1 = 0)
      {
        i2 = this.f;
        this.f = Math.max(i2, localView.getMeasuredHeight() + i2 + localLayoutParams.topMargin + localLayoutParams.bottomMargin + b(localView));
        f1 = f2;
        break;
        if (i4 > 0) {}
        for (;;)
        {
          localView.measure(i10, View.MeasureSpec.makeMeasureSpec(i4, 1073741824));
          break;
          i4 = 0;
        }
        i3 = 0;
        break label1200;
        i3 = i10;
        break label1209;
      }
      label1330:
      this.f += getPaddingTop() + getPaddingBottom();
      i7 = i3;
      i9 = i2;
    }
    do
    {
      do
      {
        do
        {
          i2 = i6;
          if (i9 == 0)
          {
            i2 = i6;
            if (i14 != 1073741824) {
              i2 = i1;
            }
          }
          setMeasuredDimension(ViewCompat.a(Math.max(i2 + (getPaddingLeft() + getPaddingRight()), getSuggestedMinimumWidth()), paramInt1, i7), i12);
          if (i5 != 0) {
            c(i13, paramInt2);
          }
          return;
          i11 = Math.max(i1, i6);
          i9 = i2;
          i1 = i11;
          i7 = i3;
          i6 = i4;
        } while (!bool);
        i9 = i2;
        i1 = i11;
        i7 = i3;
        i6 = i4;
      } while (i15 == 1073741824);
      i10 = 0;
      i9 = i2;
      i1 = i11;
      i7 = i3;
      i6 = i4;
    } while (i10 >= i13);
    View localView = b(i10);
    if ((localView == null) || (localView.getVisibility() == 8)) {}
    for (;;)
    {
      i10 += 1;
      break;
      if (((LayoutParams)localView.getLayoutParams()).g > 0.0F) {
        localView.measure(View.MeasureSpec.makeMeasureSpec(localView.getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(i8, 1073741824));
      }
    }
  }
  
  void a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i1 = getPaddingLeft();
    int i2 = paramInt3 - paramInt1;
    int i3 = getPaddingRight();
    int i4 = getPaddingRight();
    int i5 = getVirtualChildCount();
    paramInt1 = this.e;
    int i6 = this.e;
    label79:
    View localView;
    switch (paramInt1 & 0x70)
    {
    default: 
      paramInt1 = getPaddingTop();
      paramInt2 = 0;
      if (paramInt2 >= i5) {
        return;
      }
      localView = b(paramInt2);
      if (localView == null)
      {
        paramInt3 = paramInt1 + d(paramInt2);
        paramInt4 = paramInt2;
      }
      break;
    }
    do
    {
      paramInt2 = paramInt4 + 1;
      paramInt1 = paramInt3;
      break label79;
      paramInt1 = getPaddingTop() + paramInt4 - paramInt2 - this.f;
      break;
      paramInt1 = getPaddingTop() + (paramInt4 - paramInt2 - this.f) / 2;
      break;
      paramInt3 = paramInt1;
      paramInt4 = paramInt2;
    } while (localView.getVisibility() == 8);
    int i7 = localView.getMeasuredWidth();
    int i8 = localView.getMeasuredHeight();
    LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
    paramInt4 = localLayoutParams.h;
    paramInt3 = paramInt4;
    if (paramInt4 < 0) {
      paramInt3 = i6 & 0x800007;
    }
    switch (GravityCompat.a(paramInt3, ViewCompat.h(this)) & 0x7)
    {
    default: 
      paramInt3 = i1 + localLayoutParams.leftMargin;
    }
    for (;;)
    {
      paramInt4 = paramInt1;
      if (c(paramInt2)) {
        paramInt4 = paramInt1 + this.m;
      }
      paramInt1 = paramInt4 + localLayoutParams.topMargin;
      a(localView, paramInt3, paramInt1 + a(localView), i7, i8);
      paramInt3 = paramInt1 + (localLayoutParams.bottomMargin + i8 + b(localView));
      paramInt4 = paramInt2 + a(localView, paramInt2);
      break;
      paramInt3 = (i2 - i1 - i4 - i7) / 2 + i1 + localLayoutParams.leftMargin - localLayoutParams.rightMargin;
      continue;
      paramInt3 = i2 - i3 - i7 - localLayoutParams.rightMargin;
    }
  }
  
  void a(Canvas paramCanvas)
  {
    int i2 = getVirtualChildCount();
    int i1 = 0;
    View localView;
    LayoutParams localLayoutParams;
    while (i1 < i2)
    {
      localView = b(i1);
      if ((localView != null) && (localView.getVisibility() != 8) && (c(i1)))
      {
        localLayoutParams = (LayoutParams)localView.getLayoutParams();
        a(paramCanvas, localView.getTop() - localLayoutParams.topMargin - this.m);
      }
      i1 += 1;
    }
    if (c(i2))
    {
      localView = b(i2 - 1);
      if (localView != null) {
        break label124;
      }
    }
    for (i1 = getHeight() - getPaddingBottom() - this.m;; i1 = localView.getBottom() + localLayoutParams.bottomMargin)
    {
      a(paramCanvas, i1);
      return;
      label124:
      localLayoutParams = (LayoutParams)localView.getLayoutParams();
    }
  }
  
  void a(Canvas paramCanvas, int paramInt)
  {
    this.k.setBounds(getPaddingLeft() + this.o, paramInt, getWidth() - getPaddingRight() - this.o, this.m + paramInt);
    this.k.draw(paramCanvas);
  }
  
  void a(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    measureChildWithMargins(paramView, paramInt2, paramInt3, paramInt4, paramInt5);
  }
  
  int b(View paramView)
  {
    return 0;
  }
  
  public LayoutParams b(AttributeSet paramAttributeSet)
  {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected LayoutParams b(ViewGroup.LayoutParams paramLayoutParams)
  {
    return new LayoutParams(paramLayoutParams);
  }
  
  View b(int paramInt)
  {
    return getChildAt(paramInt);
  }
  
  void b(int paramInt1, int paramInt2)
  {
    this.f = 0;
    int i8 = 0;
    int i3 = 0;
    int i1 = 0;
    int i5 = 0;
    int i2 = 1;
    float f1 = 0.0F;
    int i16 = getVirtualChildCount();
    int i18 = View.MeasureSpec.getMode(paramInt1);
    int i17 = View.MeasureSpec.getMode(paramInt2);
    int i4 = 0;
    int i9 = 0;
    if ((this.i == null) || (this.j == null))
    {
      this.i = new int[4];
      this.j = new int[4];
    }
    Object localObject = this.i;
    int[] arrayOfInt = this.j;
    localObject[3] = -1;
    localObject[2] = -1;
    localObject[1] = -1;
    localObject[0] = -1;
    arrayOfInt[3] = -1;
    arrayOfInt[2] = -1;
    arrayOfInt[1] = -1;
    arrayOfInt[0] = -1;
    boolean bool1 = this.a;
    boolean bool2 = this.h;
    int i12;
    int i7;
    int i6;
    label155:
    View localView;
    if (i18 == 1073741824)
    {
      i12 = 1;
      i7 = Integer.MIN_VALUE;
      i6 = 0;
      if (i6 >= i16) {
        break label882;
      }
      localView = b(i6);
      if (localView != null) {
        break label213;
      }
      this.f += d(i6);
    }
    for (int i11 = i7;; i11 = i7)
    {
      i6 += 1;
      i7 = i11;
      break label155;
      i12 = 0;
      break;
      label213:
      if (localView.getVisibility() != 8) {
        break label243;
      }
      i6 += a(localView, i6);
    }
    label243:
    if (c(i6)) {
      this.f += this.l;
    }
    LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
    f1 += localLayoutParams.g;
    label333:
    int i10;
    label362:
    int i13;
    int i14;
    int i15;
    if ((i18 == 1073741824) && (localLayoutParams.width == 0) && (localLayoutParams.g > 0.0F)) {
      if (i12 != 0)
      {
        this.f += localLayoutParams.leftMargin + localLayoutParams.rightMargin;
        if (!bool1) {
          break label627;
        }
        i10 = View.MeasureSpec.makeMeasureSpec(0, 0);
        localView.measure(i10, i10);
        i10 = i9;
        i11 = i7;
        i13 = 0;
        i7 = i4;
        i9 = i13;
        if (i17 != 1073741824)
        {
          i7 = i4;
          i9 = i13;
          if (localLayoutParams.height == -1)
          {
            i7 = 1;
            i9 = 1;
          }
        }
        i4 = localLayoutParams.topMargin + localLayoutParams.bottomMargin;
        i13 = localView.getMeasuredHeight() + i4;
        i14 = ViewUtils.a(i3, ViewCompat.l(localView));
        if (bool1)
        {
          i15 = localView.getBaseline();
          if (i15 != -1)
          {
            if (localLayoutParams.h >= 0) {
              break label835;
            }
            i3 = this.e;
            label470:
            i3 = ((i3 & 0x70) >> 4 & 0xFFFFFFFE) >> 1;
            localObject[i3] = Math.max(localObject[i3], i15);
            arrayOfInt[i3] = Math.max(arrayOfInt[i3], i13 - i15);
          }
        }
        i8 = Math.max(i8, i13);
        if ((i2 == 0) || (localLayoutParams.height != -1)) {
          break label845;
        }
        i2 = 1;
        label543:
        if (localLayoutParams.g <= 0.0F) {
          break label858;
        }
        if (i9 == 0) {
          break label851;
        }
      }
    }
    for (;;)
    {
      i5 = Math.max(i5, i4);
      i6 += a(localView, i6);
      i3 = i14;
      i4 = i7;
      i9 = i10;
      break;
      i10 = this.f;
      this.f = Math.max(i10, localLayoutParams.leftMargin + i10 + localLayoutParams.rightMargin);
      break label333;
      label627:
      i10 = 1;
      i11 = i7;
      break label362;
      i11 = Integer.MIN_VALUE;
      i10 = i11;
      if (localLayoutParams.width == 0)
      {
        i10 = i11;
        if (localLayoutParams.g > 0.0F)
        {
          i10 = 0;
          localLayoutParams.width = -2;
        }
      }
      if (f1 == 0.0F)
      {
        i11 = this.f;
        label689:
        a(localView, i6, paramInt1, i11, paramInt2, 0);
        if (i10 != Integer.MIN_VALUE) {
          localLayoutParams.width = i10;
        }
        i13 = localView.getMeasuredWidth();
        if (i12 == 0) {
          break label793;
        }
      }
      for (this.f += localLayoutParams.leftMargin + i13 + localLayoutParams.rightMargin + b(localView);; this.f = Math.max(i10, i10 + i13 + localLayoutParams.leftMargin + localLayoutParams.rightMargin + b(localView)))
      {
        i11 = i7;
        i10 = i9;
        if (!bool2) {
          break;
        }
        i11 = Math.max(i13, i7);
        i10 = i9;
        break;
        i11 = 0;
        break label689;
        label793:
        i10 = this.f;
      }
      label835:
      i3 = localLayoutParams.h;
      break label470;
      label845:
      i2 = 0;
      break label543;
      label851:
      i4 = i13;
    }
    label858:
    if (i9 != 0) {}
    for (;;)
    {
      i1 = Math.max(i1, i4);
      break;
      i4 = i13;
    }
    label882:
    if ((this.f > 0) && (c(i16))) {
      this.f += this.l;
    }
    if ((localObject[1] == -1) && (localObject[0] == -1) && (localObject[2] == -1))
    {
      i6 = i8;
      if (localObject[3] == -1) {}
    }
    else
    {
      i6 = Math.max(i8, Math.max(localObject[3], Math.max(localObject[0], Math.max(localObject[1], localObject[2]))) + Math.max(arrayOfInt[3], Math.max(arrayOfInt[0], Math.max(arrayOfInt[1], arrayOfInt[2]))));
    }
    if ((bool2) && ((i18 == Integer.MIN_VALUE) || (i18 == 0)))
    {
      this.f = 0;
      i8 = 0;
      if (i8 < i16)
      {
        localView = b(i8);
        if (localView == null) {
          this.f += d(i8);
        }
        for (;;)
        {
          i8 += 1;
          break;
          if (localView.getVisibility() == 8)
          {
            i8 += a(localView, i8);
          }
          else
          {
            localLayoutParams = (LayoutParams)localView.getLayoutParams();
            if (i12 != 0)
            {
              this.f += localLayoutParams.leftMargin + i7 + localLayoutParams.rightMargin + b(localView);
            }
            else
            {
              i10 = this.f;
              this.f = Math.max(i10, i10 + i7 + localLayoutParams.leftMargin + localLayoutParams.rightMargin + b(localView));
            }
          }
        }
      }
    }
    this.f += getPaddingLeft() + getPaddingRight();
    int i19 = ViewCompat.a(Math.max(this.f, getSuggestedMinimumWidth()), paramInt1, 0);
    i8 = (i19 & 0xFFFFFF) - this.f;
    if ((i9 != 0) || ((i8 != 0) && (f1 > 0.0F)))
    {
      label1323:
      float f3;
      if (this.g > 0.0F)
      {
        f1 = this.g;
        localObject[3] = -1;
        localObject[2] = -1;
        localObject[1] = -1;
        localObject[0] = -1;
        arrayOfInt[3] = -1;
        arrayOfInt[2] = -1;
        arrayOfInt[1] = -1;
        arrayOfInt[0] = -1;
        i6 = -1;
        this.f = 0;
        i9 = 0;
        i5 = i1;
        if (i9 >= i16) {
          break label1980;
        }
        localView = b(i9);
        i10 = i2;
        i11 = i5;
        i13 = i3;
        i14 = i8;
        i15 = i6;
        f3 = f1;
        if (localView != null)
        {
          if (localView.getVisibility() != 8) {
            break label1434;
          }
          f3 = f1;
          i15 = i6;
          i14 = i8;
          i13 = i3;
          i11 = i5;
          i10 = i2;
        }
      }
      label1434:
      float f2;
      label1638:
      label1657:
      label1698:
      label1724:
      int i20;
      do
      {
        do
        {
          i9 += 1;
          i2 = i10;
          i5 = i11;
          i3 = i13;
          i8 = i14;
          i6 = i15;
          f1 = f3;
          break label1323;
          break;
          localLayoutParams = (LayoutParams)localView.getLayoutParams();
          f3 = localLayoutParams.g;
          i7 = i3;
          i1 = i8;
          f2 = f1;
          if (f3 > 0.0F)
          {
            i1 = (int)(i8 * f3 / f1);
            f2 = f1 - f3;
            i7 = i8 - i1;
            i10 = getChildMeasureSpec(paramInt2, getPaddingTop() + getPaddingBottom() + localLayoutParams.topMargin + localLayoutParams.bottomMargin, localLayoutParams.height);
            if ((localLayoutParams.width == 0) && (i18 == 1073741824)) {
              break label1878;
            }
            i8 = localView.getMeasuredWidth() + i1;
            i1 = i8;
            if (i8 < 0) {
              i1 = 0;
            }
            localView.measure(View.MeasureSpec.makeMeasureSpec(i1, 1073741824), i10);
            i3 = ViewUtils.a(i3, ViewCompat.l(localView) & 0xFF000000);
            i1 = i7;
            i7 = i3;
          }
          if (i12 == 0) {
            break label1906;
          }
          this.f += localView.getMeasuredWidth() + localLayoutParams.leftMargin + localLayoutParams.rightMargin + b(localView);
          if ((i17 == 1073741824) || (localLayoutParams.height != -1)) {
            break label1951;
          }
          i3 = 1;
          i10 = localLayoutParams.topMargin + localLayoutParams.bottomMargin;
          i8 = localView.getMeasuredHeight() + i10;
          i6 = Math.max(i6, i8);
          if (i3 == 0) {
            break label1957;
          }
          i3 = i10;
          i5 = Math.max(i5, i3);
          if ((i2 == 0) || (localLayoutParams.height != -1)) {
            break label1964;
          }
          i2 = 1;
          i10 = i2;
          i11 = i5;
          i13 = i7;
          i14 = i1;
          i15 = i6;
          f3 = f2;
        } while (!bool1);
        i20 = localView.getBaseline();
        i10 = i2;
        i11 = i5;
        i13 = i7;
        i14 = i1;
        i15 = i6;
        f3 = f2;
      } while (i20 == -1);
      if (localLayoutParams.h < 0) {}
      for (i3 = this.e;; i3 = localLayoutParams.h)
      {
        i3 = ((i3 & 0x70) >> 4 & 0xFFFFFFFE) >> 1;
        localObject[i3] = Math.max(localObject[i3], i20);
        arrayOfInt[i3] = Math.max(arrayOfInt[i3], i8 - i20);
        i10 = i2;
        i11 = i5;
        i13 = i7;
        i14 = i1;
        i15 = i6;
        f3 = f2;
        break;
        label1878:
        if (i1 > 0) {}
        for (;;)
        {
          localView.measure(View.MeasureSpec.makeMeasureSpec(i1, 1073741824), i10);
          break;
          i1 = 0;
        }
        label1906:
        i3 = this.f;
        this.f = Math.max(i3, localView.getMeasuredWidth() + i3 + localLayoutParams.leftMargin + localLayoutParams.rightMargin + b(localView));
        break label1638;
        label1951:
        i3 = 0;
        break label1657;
        label1957:
        i3 = i8;
        break label1698;
        label1964:
        i2 = 0;
        break label1724;
      }
      label1980:
      this.f += getPaddingLeft() + getPaddingRight();
      if ((localObject[1] == -1) && (localObject[0] == -1) && (localObject[2] == -1))
      {
        i10 = i2;
        i8 = i5;
        i9 = i3;
        i1 = i6;
        if (localObject[3] == -1) {}
      }
      else
      {
        i1 = Math.max(i6, Math.max(localObject[3], Math.max(localObject[0], Math.max(localObject[1], localObject[2]))) + Math.max(arrayOfInt[3], Math.max(arrayOfInt[0], Math.max(arrayOfInt[1], arrayOfInt[2]))));
        i9 = i3;
        i8 = i5;
        i10 = i2;
      }
    }
    do
    {
      do
      {
        do
        {
          i2 = i1;
          if (i10 == 0)
          {
            i2 = i1;
            if (i17 != 1073741824) {
              i2 = i8;
            }
          }
          setMeasuredDimension(0xFF000000 & i9 | i19, ViewCompat.a(Math.max(i2 + (getPaddingTop() + getPaddingBottom()), getSuggestedMinimumHeight()), paramInt2, i9 << 16));
          if (i4 != 0) {
            d(i16, paramInt1);
          }
          return;
          i11 = Math.max(i1, i5);
          i10 = i2;
          i8 = i11;
          i9 = i3;
          i1 = i6;
        } while (!bool2);
        i10 = i2;
        i8 = i11;
        i9 = i3;
        i1 = i6;
      } while (i18 == 1073741824);
      i5 = 0;
      i10 = i2;
      i8 = i11;
      i9 = i3;
      i1 = i6;
    } while (i5 >= i16);
    localObject = b(i5);
    if ((localObject == null) || (((View)localObject).getVisibility() == 8)) {}
    for (;;)
    {
      i5 += 1;
      break;
      if (((LayoutParams)((View)localObject).getLayoutParams()).g > 0.0F) {
        ((View)localObject).measure(View.MeasureSpec.makeMeasureSpec(i7, 1073741824), View.MeasureSpec.makeMeasureSpec(((View)localObject).getMeasuredHeight(), 1073741824));
      }
    }
  }
  
  void b(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    boolean bool1 = ViewUtils.a(this);
    int i3 = getPaddingTop();
    int i5 = paramInt4 - paramInt2;
    int i6 = getPaddingBottom();
    int i7 = getPaddingBottom();
    int i8 = getVirtualChildCount();
    paramInt2 = this.e;
    int i9 = this.e;
    boolean bool2 = this.a;
    int[] arrayOfInt1 = this.i;
    int[] arrayOfInt2 = this.j;
    label133:
    int i10;
    View localView;
    switch (GravityCompat.a(paramInt2 & 0x800007, ViewCompat.h(this)))
    {
    default: 
      paramInt1 = getPaddingLeft();
      int i1 = 0;
      paramInt4 = 1;
      if (bool1)
      {
        i1 = i8 - 1;
        paramInt4 = -1;
      }
      paramInt2 = 0;
      paramInt3 = paramInt1;
      if (paramInt2 >= i8) {
        return;
      }
      i10 = i1 + paramInt4 * paramInt2;
      localView = b(i10);
      if (localView == null)
      {
        paramInt1 = paramInt3 + d(i10);
        i2 = paramInt2;
      }
      break;
    }
    do
    {
      paramInt2 = i2 + 1;
      paramInt3 = paramInt1;
      break label133;
      paramInt1 = getPaddingLeft() + paramInt3 - paramInt1 - this.f;
      break;
      paramInt1 = getPaddingLeft() + (paramInt3 - paramInt1 - this.f) / 2;
      break;
      paramInt1 = paramInt3;
      i2 = paramInt2;
    } while (localView.getVisibility() == 8);
    int i11 = localView.getMeasuredWidth();
    int i12 = localView.getMeasuredHeight();
    paramInt1 = -1;
    LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
    int i2 = paramInt1;
    if (bool2)
    {
      i2 = paramInt1;
      if (localLayoutParams.height != -1) {
        i2 = localView.getBaseline();
      }
    }
    int i4 = localLayoutParams.h;
    paramInt1 = i4;
    if (i4 < 0) {
      paramInt1 = i9 & 0x70;
    }
    switch (paramInt1 & 0x70)
    {
    default: 
      paramInt1 = i3;
    }
    for (;;)
    {
      i2 = paramInt3;
      if (c(i10)) {
        i2 = paramInt3 + this.l;
      }
      paramInt3 = i2 + localLayoutParams.leftMargin;
      a(localView, paramInt3 + a(localView), paramInt1, i11, i12);
      paramInt1 = paramInt3 + (localLayoutParams.rightMargin + i11 + b(localView));
      i2 = paramInt2 + a(localView, i10);
      break;
      i4 = i3 + localLayoutParams.topMargin;
      paramInt1 = i4;
      if (i2 != -1)
      {
        paramInt1 = i4 + (arrayOfInt1[1] - i2);
        continue;
        paramInt1 = (i5 - i3 - i7 - i12) / 2 + i3 + localLayoutParams.topMargin - localLayoutParams.bottomMargin;
        continue;
        i4 = i5 - i6 - i12 - localLayoutParams.bottomMargin;
        paramInt1 = i4;
        if (i2 != -1)
        {
          paramInt1 = localView.getMeasuredHeight();
          paramInt1 = i4 - (arrayOfInt2[2] - (paramInt1 - i2));
        }
      }
    }
  }
  
  void b(Canvas paramCanvas)
  {
    int i3 = getVirtualChildCount();
    boolean bool = ViewUtils.a(this);
    int i1 = 0;
    View localView;
    LayoutParams localLayoutParams;
    if (i1 < i3)
    {
      localView = b(i1);
      if ((localView != null) && (localView.getVisibility() != 8) && (c(i1)))
      {
        localLayoutParams = (LayoutParams)localView.getLayoutParams();
        if (!bool) {
          break label90;
        }
      }
      label90:
      for (int i2 = localView.getRight() + localLayoutParams.rightMargin;; i2 = localView.getLeft() - localLayoutParams.leftMargin - this.l)
      {
        b(paramCanvas, i2);
        i1 += 1;
        break;
      }
    }
    if (c(i3))
    {
      localView = b(i3 - 1);
      if (localView != null) {
        break label169;
      }
      if (!bool) {
        break label151;
      }
      i1 = getPaddingLeft();
    }
    for (;;)
    {
      b(paramCanvas, i1);
      return;
      label151:
      i1 = getWidth() - getPaddingRight() - this.l;
      continue;
      label169:
      localLayoutParams = (LayoutParams)localView.getLayoutParams();
      if (bool) {
        i1 = localView.getLeft() - localLayoutParams.leftMargin - this.l;
      } else {
        i1 = localView.getRight() + localLayoutParams.rightMargin;
      }
    }
  }
  
  void b(Canvas paramCanvas, int paramInt)
  {
    this.k.setBounds(paramInt, getPaddingTop() + this.o, this.l + paramInt, getHeight() - getPaddingBottom() - this.o);
    this.k.draw(paramCanvas);
  }
  
  protected boolean c(int paramInt)
  {
    if (paramInt == 0) {
      if ((this.n & 0x1) == 0) {}
    }
    do
    {
      return true;
      return false;
      if (paramInt != getChildCount()) {
        break;
      }
    } while ((this.n & 0x4) != 0);
    return false;
    if ((this.n & 0x2) != 0)
    {
      boolean bool2 = false;
      paramInt -= 1;
      for (;;)
      {
        boolean bool1 = bool2;
        if (paramInt >= 0)
        {
          if (getChildAt(paramInt).getVisibility() != 8) {
            bool1 = true;
          }
        }
        else {
          return bool1;
        }
        paramInt -= 1;
      }
    }
    return false;
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  int d(int paramInt)
  {
    return 0;
  }
  
  public int getBaseline()
  {
    int i1 = -1;
    if (this.b < 0) {
      i1 = super.getBaseline();
    }
    View localView;
    int i3;
    do
    {
      return i1;
      if (getChildCount() <= this.b) {
        throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds.");
      }
      localView = getChildAt(this.b);
      i3 = localView.getBaseline();
      if (i3 != -1) {
        break;
      }
    } while (this.b == 0);
    throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout points to a View that doesn't know how to get its baseline.");
    int i2 = this.c;
    i1 = i2;
    if (this.d == 1)
    {
      int i4 = this.e & 0x70;
      i1 = i2;
      if (i4 != 48) {
        switch (i4)
        {
        default: 
          i1 = i2;
        }
      }
    }
    for (;;)
    {
      return ((LayoutParams)localView.getLayoutParams()).topMargin + i1 + i3;
      i1 = getBottom() - getTop() - getPaddingBottom() - this.f;
      continue;
      i1 = i2 + (getBottom() - getTop() - getPaddingTop() - getPaddingBottom() - this.f) / 2;
    }
  }
  
  public int getBaselineAlignedChildIndex()
  {
    return this.b;
  }
  
  public Drawable getDividerDrawable()
  {
    return this.k;
  }
  
  public int getDividerPadding()
  {
    return this.o;
  }
  
  public int getDividerWidth()
  {
    return this.l;
  }
  
  public int getOrientation()
  {
    return this.d;
  }
  
  public int getShowDividers()
  {
    return this.n;
  }
  
  int getVirtualChildCount()
  {
    return getChildCount();
  }
  
  public float getWeightSum()
  {
    return this.g;
  }
  
  protected LayoutParams j()
  {
    if (this.d == 0) {
      return new LayoutParams(-2, -2);
    }
    if (this.d == 1) {
      return new LayoutParams(-1, -2);
    }
    return null;
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    if (this.k == null) {
      return;
    }
    if (this.d == 1)
    {
      a(paramCanvas);
      return;
    }
    b(paramCanvas);
  }
  
  public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
  {
    if (Build.VERSION.SDK_INT >= 14)
    {
      super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
      paramAccessibilityEvent.setClassName(LinearLayoutCompat.class.getName());
    }
  }
  
  public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
  {
    if (Build.VERSION.SDK_INT >= 14)
    {
      super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
      paramAccessibilityNodeInfo.setClassName(LinearLayoutCompat.class.getName());
    }
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (this.d == 1)
    {
      a(paramInt1, paramInt2, paramInt3, paramInt4);
      return;
    }
    b(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    if (this.d == 1)
    {
      a(paramInt1, paramInt2);
      return;
    }
    b(paramInt1, paramInt2);
  }
  
  public void setBaselineAligned(boolean paramBoolean)
  {
    this.a = paramBoolean;
  }
  
  public void setBaselineAlignedChildIndex(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= getChildCount())) {
      throw new IllegalArgumentException("base aligned child index out of range (0, " + getChildCount() + ")");
    }
    this.b = paramInt;
  }
  
  public void setDividerDrawable(Drawable paramDrawable)
  {
    boolean bool = false;
    if (paramDrawable == this.k) {
      return;
    }
    this.k = paramDrawable;
    if (paramDrawable != null) {
      this.l = paramDrawable.getIntrinsicWidth();
    }
    for (this.m = paramDrawable.getIntrinsicHeight();; this.m = 0)
    {
      if (paramDrawable == null) {
        bool = true;
      }
      setWillNotDraw(bool);
      requestLayout();
      return;
      this.l = 0;
    }
  }
  
  public void setDividerPadding(int paramInt)
  {
    this.o = paramInt;
  }
  
  public void setGravity(int paramInt)
  {
    if (this.e != paramInt)
    {
      int i1 = paramInt;
      if ((0x800007 & paramInt) == 0) {
        i1 = paramInt | 0x800003;
      }
      paramInt = i1;
      if ((i1 & 0x70) == 0) {
        paramInt = i1 | 0x30;
      }
      this.e = paramInt;
      requestLayout();
    }
  }
  
  public void setHorizontalGravity(int paramInt)
  {
    paramInt &= 0x800007;
    if ((this.e & 0x800007) != paramInt)
    {
      this.e = (this.e & 0xFF7FFFF8 | paramInt);
      requestLayout();
    }
  }
  
  public void setMeasureWithLargestChildEnabled(boolean paramBoolean)
  {
    this.h = paramBoolean;
  }
  
  public void setOrientation(int paramInt)
  {
    if (this.d != paramInt)
    {
      this.d = paramInt;
      requestLayout();
    }
  }
  
  public void setShowDividers(int paramInt)
  {
    if (paramInt != this.n) {
      requestLayout();
    }
    this.n = paramInt;
  }
  
  public void setVerticalGravity(int paramInt)
  {
    paramInt &= 0x70;
    if ((this.e & 0x70) != paramInt)
    {
      this.e = (this.e & 0xFFFFFF8F | paramInt);
      requestLayout();
    }
  }
  
  public void setWeightSum(float paramFloat)
  {
    this.g = Math.max(0.0F, paramFloat);
  }
  
  public boolean shouldDelayChildPressedState()
  {
    return false;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface DividerMode {}
  
  public static class LayoutParams
    extends ViewGroup.MarginLayoutParams
  {
    public float g;
    public int h = -1;
    
    public LayoutParams(int paramInt1, int paramInt2)
    {
      super(paramInt2);
      this.g = 0.0F;
    }
    
    public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
      paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.LinearLayoutCompat_Layout);
      this.g = paramContext.getFloat(R.styleable.LinearLayoutCompat_Layout_android_layout_weight, 0.0F);
      this.h = paramContext.getInt(R.styleable.LinearLayoutCompat_Layout_android_layout_gravity, -1);
      paramContext.recycle();
    }
    
    public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface OrientationMode {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/LinearLayoutCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */