package android.support.v7.widget;

import android.content.Context;

abstract interface CardViewImpl
{
  public abstract float a(CardViewDelegate paramCardViewDelegate);
  
  public abstract void a();
  
  public abstract void a(CardViewDelegate paramCardViewDelegate, float paramFloat);
  
  public abstract void a(CardViewDelegate paramCardViewDelegate, int paramInt);
  
  public abstract void a(CardViewDelegate paramCardViewDelegate, Context paramContext, int paramInt, float paramFloat1, float paramFloat2, float paramFloat3);
  
  public abstract float b(CardViewDelegate paramCardViewDelegate);
  
  public abstract void b(CardViewDelegate paramCardViewDelegate, float paramFloat);
  
  public abstract float c(CardViewDelegate paramCardViewDelegate);
  
  public abstract void c(CardViewDelegate paramCardViewDelegate, float paramFloat);
  
  public abstract float d(CardViewDelegate paramCardViewDelegate);
  
  public abstract float e(CardViewDelegate paramCardViewDelegate);
  
  public abstract void f(CardViewDelegate paramCardViewDelegate);
  
  public abstract void g(CardViewDelegate paramCardViewDelegate);
  
  public abstract void h(CardViewDelegate paramCardViewDelegate);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/CardViewImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */