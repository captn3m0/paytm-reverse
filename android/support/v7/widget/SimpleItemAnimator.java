package android.support.v7.widget;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

public abstract class SimpleItemAnimator
  extends RecyclerView.ItemAnimator
{
  boolean a = true;
  
  public final void a(RecyclerView.ViewHolder paramViewHolder, boolean paramBoolean)
  {
    d(paramViewHolder, paramBoolean);
    e(paramViewHolder);
  }
  
  public abstract boolean a(RecyclerView.ViewHolder paramViewHolder);
  
  public abstract boolean a(RecyclerView.ViewHolder paramViewHolder, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  public boolean a(@NonNull RecyclerView.ViewHolder paramViewHolder, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo1, @Nullable RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo2)
  {
    int k = paramItemHolderInfo1.a;
    int m = paramItemHolderInfo1.b;
    paramItemHolderInfo1 = paramViewHolder.itemView;
    int i;
    if (paramItemHolderInfo2 == null)
    {
      i = paramItemHolderInfo1.getLeft();
      if (paramItemHolderInfo2 != null) {
        break label103;
      }
    }
    label103:
    for (int j = paramItemHolderInfo1.getTop();; j = paramItemHolderInfo2.b)
    {
      if ((paramViewHolder.isRemoved()) || ((k == i) && (m == j))) {
        break label112;
      }
      paramItemHolderInfo1.layout(i, j, paramItemHolderInfo1.getWidth() + i, paramItemHolderInfo1.getHeight() + j);
      return a(paramViewHolder, k, m, i, j);
      i = paramItemHolderInfo2.a;
      break;
    }
    label112:
    return a(paramViewHolder);
  }
  
  public abstract boolean a(RecyclerView.ViewHolder paramViewHolder1, RecyclerView.ViewHolder paramViewHolder2, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  public boolean a(@NonNull RecyclerView.ViewHolder paramViewHolder1, @NonNull RecyclerView.ViewHolder paramViewHolder2, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo1, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo2)
  {
    int k = paramItemHolderInfo1.a;
    int m = paramItemHolderInfo1.b;
    int i;
    if (paramViewHolder2.shouldIgnore()) {
      i = paramItemHolderInfo1.a;
    }
    for (int j = paramItemHolderInfo1.b;; j = paramItemHolderInfo2.b)
    {
      return a(paramViewHolder1, paramViewHolder2, k, m, i, j);
      i = paramItemHolderInfo2.a;
    }
  }
  
  public final void b(RecyclerView.ViewHolder paramViewHolder, boolean paramBoolean)
  {
    c(paramViewHolder, paramBoolean);
  }
  
  public abstract boolean b(RecyclerView.ViewHolder paramViewHolder);
  
  public boolean b(@NonNull RecyclerView.ViewHolder paramViewHolder, @Nullable RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo1, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo2)
  {
    if ((paramItemHolderInfo1 != null) && ((paramItemHolderInfo1.a != paramItemHolderInfo2.a) || (paramItemHolderInfo1.b != paramItemHolderInfo2.b))) {
      return a(paramViewHolder, paramItemHolderInfo1.a, paramItemHolderInfo1.b, paramItemHolderInfo2.a, paramItemHolderInfo2.b);
    }
    return b(paramViewHolder);
  }
  
  public void c(RecyclerView.ViewHolder paramViewHolder, boolean paramBoolean) {}
  
  public boolean c(@NonNull RecyclerView.ViewHolder paramViewHolder, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo1, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo2)
  {
    if ((paramItemHolderInfo1.a != paramItemHolderInfo2.a) || (paramItemHolderInfo1.b != paramItemHolderInfo2.b)) {
      return a(paramViewHolder, paramItemHolderInfo1.a, paramItemHolderInfo1.b, paramItemHolderInfo2.a, paramItemHolderInfo2.b);
    }
    i(paramViewHolder);
    return false;
  }
  
  public void d(RecyclerView.ViewHolder paramViewHolder, boolean paramBoolean) {}
  
  public boolean g(@NonNull RecyclerView.ViewHolder paramViewHolder)
  {
    return (!this.a) || (paramViewHolder.isInvalid());
  }
  
  public final void h(RecyclerView.ViewHolder paramViewHolder)
  {
    o(paramViewHolder);
    e(paramViewHolder);
  }
  
  public final void i(RecyclerView.ViewHolder paramViewHolder)
  {
    s(paramViewHolder);
    e(paramViewHolder);
  }
  
  public final void j(RecyclerView.ViewHolder paramViewHolder)
  {
    q(paramViewHolder);
    e(paramViewHolder);
  }
  
  public final void k(RecyclerView.ViewHolder paramViewHolder)
  {
    n(paramViewHolder);
  }
  
  public final void l(RecyclerView.ViewHolder paramViewHolder)
  {
    r(paramViewHolder);
  }
  
  public final void m(RecyclerView.ViewHolder paramViewHolder)
  {
    p(paramViewHolder);
  }
  
  public void n(RecyclerView.ViewHolder paramViewHolder) {}
  
  public void o(RecyclerView.ViewHolder paramViewHolder) {}
  
  public void p(RecyclerView.ViewHolder paramViewHolder) {}
  
  public void q(RecyclerView.ViewHolder paramViewHolder) {}
  
  public void r(RecyclerView.ViewHolder paramViewHolder) {}
  
  public void s(RecyclerView.ViewHolder paramViewHolder) {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/SimpleItemAnimator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */