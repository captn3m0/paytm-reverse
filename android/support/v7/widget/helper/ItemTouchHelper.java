package android.support.v7.widget.helper;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.support.v4.animation.AnimatorCompatHelper;
import android.support.v4.animation.AnimatorListenerCompat;
import android.support.v4.animation.AnimatorUpdateListenerCompat;
import android.support.v4.animation.ValueAnimatorCompat;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.VelocityTrackerCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.recyclerview.R.dimen;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ChildDrawingOrderCallback;
import android.support.v7.widget.RecyclerView.ItemAnimator;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView.OnChildAttachStateChangeListener;
import android.support.v7.widget.RecyclerView.OnItemTouchListener;
import android.support.v7.widget.RecyclerView.State;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewParent;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.List;

public class ItemTouchHelper
  extends RecyclerView.ItemDecoration
  implements RecyclerView.OnChildAttachStateChangeListener
{
  private Rect A;
  private long B;
  final List<View> a;
  RecyclerView.ViewHolder b;
  float c;
  float d;
  float e;
  float f;
  float g;
  float h;
  float i;
  float j;
  int k;
  Callback l;
  int m;
  int n;
  List<RecoverAnimation> o;
  private final float[] p;
  private int q;
  private RecyclerView r;
  private final Runnable s;
  private VelocityTracker t;
  private List<RecyclerView.ViewHolder> u;
  private List<Integer> v;
  private RecyclerView.ChildDrawingOrderCallback w;
  private View x;
  private int y;
  private GestureDetectorCompat z;
  
  private int a(RecyclerView.ViewHolder paramViewHolder, boolean paramBoolean)
  {
    int i1 = this.o.size() - 1;
    while (i1 >= 0)
    {
      RecoverAnimation localRecoverAnimation = (RecoverAnimation)this.o.get(i1);
      if (localRecoverAnimation.h == paramViewHolder)
      {
        localRecoverAnimation.m |= paramBoolean;
        if (!RecoverAnimation.a(localRecoverAnimation)) {
          localRecoverAnimation.cancel();
        }
        this.o.remove(i1);
        return RecoverAnimation.b(localRecoverAnimation);
      }
      i1 -= 1;
    }
    return 0;
  }
  
  private RecyclerView.ViewHolder a(MotionEvent paramMotionEvent)
  {
    RecyclerView.LayoutManager localLayoutManager = this.r.getLayoutManager();
    if (this.k == -1) {}
    do
    {
      float f3;
      float f1;
      do
      {
        return null;
        int i1 = MotionEventCompat.a(paramMotionEvent, this.k);
        f3 = MotionEventCompat.c(paramMotionEvent, i1);
        float f4 = this.c;
        f1 = MotionEventCompat.d(paramMotionEvent, i1);
        float f2 = this.d;
        f3 = Math.abs(f3 - f4);
        f1 = Math.abs(f1 - f2);
      } while (((f3 < this.q) && (f1 < this.q)) || ((f3 > f1) && (localLayoutManager.e())) || ((f1 > f3) && (localLayoutManager.f())));
      paramMotionEvent = b(paramMotionEvent);
    } while (paramMotionEvent == null);
    return this.r.a(paramMotionEvent);
  }
  
  private List<RecyclerView.ViewHolder> a(RecyclerView.ViewHolder paramViewHolder)
  {
    int i1;
    int i4;
    int i5;
    int i6;
    int i7;
    int i8;
    int i9;
    label137:
    View localView;
    if (this.u == null)
    {
      this.u = new ArrayList();
      this.v = new ArrayList();
      i1 = this.l.c();
      i4 = Math.round(this.i + this.g) - i1;
      i5 = Math.round(this.j + this.h) - i1;
      i6 = paramViewHolder.itemView.getWidth() + i4 + i1 * 2;
      i7 = paramViewHolder.itemView.getHeight() + i5 + i1 * 2;
      i8 = (i4 + i6) / 2;
      i9 = (i5 + i7) / 2;
      RecyclerView.LayoutManager localLayoutManager = this.r.getLayoutManager();
      int i10 = localLayoutManager.u();
      i1 = 0;
      if (i1 >= i10) {
        break label399;
      }
      localView = localLayoutManager.i(i1);
      if (localView != paramViewHolder.itemView) {
        break label188;
      }
    }
    for (;;)
    {
      i1 += 1;
      break label137;
      this.u.clear();
      this.v.clear();
      break;
      label188:
      if ((localView.getBottom() >= i5) && (localView.getTop() <= i7) && (localView.getRight() >= i4) && (localView.getLeft() <= i6))
      {
        RecyclerView.ViewHolder localViewHolder = this.r.a(localView);
        if (this.l.a(this.r, this.b, localViewHolder))
        {
          int i2 = Math.abs(i8 - (localView.getLeft() + localView.getRight()) / 2);
          int i3 = Math.abs(i9 - (localView.getTop() + localView.getBottom()) / 2);
          int i11 = i2 * i2 + i3 * i3;
          i3 = 0;
          int i12 = this.u.size();
          i2 = 0;
          while ((i2 < i12) && (i11 > ((Integer)this.v.get(i2)).intValue()))
          {
            i3 += 1;
            i2 += 1;
          }
          this.u.add(i3, localViewHolder);
          this.v.add(i3, Integer.valueOf(i11));
        }
      }
    }
    label399:
    return this.u;
  }
  
  private void a(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    if ((paramViewHolder == this.b) && (paramInt == this.m)) {
      return;
    }
    this.B = Long.MIN_VALUE;
    int i3 = this.m;
    a(paramViewHolder, true);
    this.m = paramInt;
    if (paramInt == 2)
    {
      this.x = paramViewHolder.itemView;
      e();
    }
    int i1 = 0;
    final int i2 = 0;
    final Object localObject;
    float f1;
    float f2;
    if (this.b != null)
    {
      localObject = this.b;
      if (((RecyclerView.ViewHolder)localObject).itemView.getParent() == null) {
        break label510;
      }
      if (i3 == 2)
      {
        i2 = 0;
        d();
      }
    }
    else
    {
      switch (i2)
      {
      default: 
        f1 = 0.0F;
        f2 = 0.0F;
        label169:
        if (i3 == 2)
        {
          i1 = 8;
          label179:
          a(this.p);
          float f3 = this.p[0];
          float f4 = this.p[1];
          localObject = new RecoverAnimation((RecyclerView.ViewHolder)localObject, i1, i3, f3, f4, f1, f2, i2)
          {
            public void b(ValueAnimatorCompat paramAnonymousValueAnimatorCompat)
            {
              super.b(paramAnonymousValueAnimatorCompat);
              if (this.m) {}
              for (;;)
              {
                return;
                if (i2 <= 0) {
                  ItemTouchHelper.this.l.c(ItemTouchHelper.c(ItemTouchHelper.this), localObject);
                }
                while (ItemTouchHelper.g(ItemTouchHelper.this) == localObject.itemView)
                {
                  ItemTouchHelper.a(ItemTouchHelper.this, localObject.itemView);
                  return;
                  ItemTouchHelper.this.a.add(localObject.itemView);
                  this.j = true;
                  if (i2 > 0) {
                    ItemTouchHelper.a(ItemTouchHelper.this, this, i2);
                  }
                }
              }
            }
          };
          ((RecoverAnimation)localObject).a(this.l.a(this.r, i1, f1 - f3, f2 - f4));
          this.o.add(localObject);
          ((RecoverAnimation)localObject).a();
          i1 = 1;
          label277:
          this.b = null;
          if (paramViewHolder != null)
          {
            this.n = ((this.l.b(this.r, paramViewHolder) & (1 << paramInt * 8 + 8) - 1) >> this.m * 8);
            this.i = paramViewHolder.itemView.getLeft();
            this.j = paramViewHolder.itemView.getTop();
            this.b = paramViewHolder;
            if (paramInt == 2) {
              this.b.itemView.performHapticFeedback(0);
            }
          }
          paramViewHolder = this.r.getParent();
          if (paramViewHolder != null) {
            if (this.b == null) {
              break label539;
            }
          }
        }
        break;
      }
    }
    label510:
    label539:
    for (boolean bool = true;; bool = false)
    {
      paramViewHolder.requestDisallowInterceptTouchEvent(bool);
      if (i1 == 0) {
        this.r.getLayoutManager().I();
      }
      this.l.b(this.b, this.m);
      this.r.invalidate();
      return;
      i2 = c((RecyclerView.ViewHolder)localObject);
      break;
      f2 = 0.0F;
      f1 = Math.signum(this.g) * this.r.getWidth();
      break label169;
      f1 = 0.0F;
      f2 = Math.signum(this.h) * this.r.getHeight();
      break label169;
      if (i2 > 0)
      {
        i1 = 2;
        break label179;
      }
      i1 = 4;
      break label179;
      c(((RecyclerView.ViewHolder)localObject).itemView);
      this.l.c(this.r, (RecyclerView.ViewHolder)localObject);
      i1 = i2;
      break label277;
    }
  }
  
  private void a(final RecoverAnimation paramRecoverAnimation, final int paramInt)
  {
    this.r.post(new Runnable()
    {
      public void run()
      {
        if ((ItemTouchHelper.c(ItemTouchHelper.this) != null) && (ItemTouchHelper.c(ItemTouchHelper.this).isAttachedToWindow()) && (!paramRecoverAnimation.m) && (paramRecoverAnimation.h.getAdapterPosition() != -1))
        {
          RecyclerView.ItemAnimator localItemAnimator = ItemTouchHelper.c(ItemTouchHelper.this).getItemAnimator();
          if (((localItemAnimator == null) || (!localItemAnimator.a(null))) && (!ItemTouchHelper.h(ItemTouchHelper.this))) {
            ItemTouchHelper.this.l.a(paramRecoverAnimation.h, paramInt);
          }
        }
        else
        {
          return;
        }
        ItemTouchHelper.c(ItemTouchHelper.this).post(this);
      }
    });
  }
  
  private void a(MotionEvent paramMotionEvent, int paramInt1, int paramInt2)
  {
    float f1 = MotionEventCompat.c(paramMotionEvent, paramInt2);
    float f2 = MotionEventCompat.d(paramMotionEvent, paramInt2);
    this.g = (f1 - this.c);
    this.h = (f2 - this.d);
    if ((paramInt1 & 0x4) == 0) {
      this.g = Math.max(0.0F, this.g);
    }
    if ((paramInt1 & 0x8) == 0) {
      this.g = Math.min(0.0F, this.g);
    }
    if ((paramInt1 & 0x1) == 0) {
      this.h = Math.max(0.0F, this.h);
    }
    if ((paramInt1 & 0x2) == 0) {
      this.h = Math.min(0.0F, this.h);
    }
  }
  
  private void a(float[] paramArrayOfFloat)
  {
    if ((this.n & 0xC) != 0) {
      paramArrayOfFloat[0] = (this.i + this.g - this.b.itemView.getLeft());
    }
    while ((this.n & 0x3) != 0)
    {
      paramArrayOfFloat[1] = (this.j + this.h - this.b.itemView.getTop());
      return;
      paramArrayOfFloat[0] = ViewCompat.o(this.b.itemView);
    }
    paramArrayOfFloat[1] = ViewCompat.p(this.b.itemView);
  }
  
  private boolean a()
  {
    int i2 = this.o.size();
    int i1 = 0;
    while (i1 < i2)
    {
      if (!RecoverAnimation.a((RecoverAnimation)this.o.get(i1))) {
        return true;
      }
      i1 += 1;
    }
    return false;
  }
  
  private boolean a(int paramInt1, MotionEvent paramMotionEvent, int paramInt2)
  {
    if ((this.b != null) || (paramInt1 != 2) || (this.m == 2) || (!this.l.b())) {
      return false;
    }
    if (this.r.getScrollState() == 1) {
      return false;
    }
    RecyclerView.ViewHolder localViewHolder = a(paramMotionEvent);
    if (localViewHolder == null) {
      return false;
    }
    paramInt1 = (0xFF00 & this.l.b(this.r, localViewHolder)) >> 8;
    if (paramInt1 == 0) {
      return false;
    }
    float f1 = MotionEventCompat.c(paramMotionEvent, paramInt2);
    float f2 = MotionEventCompat.d(paramMotionEvent, paramInt2);
    f1 -= this.c;
    f2 -= this.d;
    float f3 = Math.abs(f1);
    float f4 = Math.abs(f2);
    if ((f3 < this.q) && (f4 < this.q)) {
      return false;
    }
    if (f3 > f4)
    {
      if ((f1 < 0.0F) && ((paramInt1 & 0x4) == 0)) {
        return false;
      }
      if ((f1 > 0.0F) && ((paramInt1 & 0x8) == 0)) {
        return false;
      }
    }
    else
    {
      if ((f2 < 0.0F) && ((paramInt1 & 0x1) == 0)) {
        return false;
      }
      if ((f2 > 0.0F) && ((paramInt1 & 0x2) == 0)) {
        return false;
      }
    }
    this.h = 0.0F;
    this.g = 0.0F;
    this.k = MotionEventCompat.b(paramMotionEvent, 0);
    a(localViewHolder, 1);
    return true;
  }
  
  private static boolean a(View paramView, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    return (paramFloat1 >= paramFloat3) && (paramFloat1 <= paramView.getWidth() + paramFloat3) && (paramFloat2 >= paramFloat4) && (paramFloat2 <= paramView.getHeight() + paramFloat4);
  }
  
  private int b(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    if ((paramInt & 0xC) != 0)
    {
      int i1;
      if (this.g > 0.0F)
      {
        i1 = 8;
        if ((this.t == null) || (this.k <= -1)) {
          break label155;
        }
        this.t.computeCurrentVelocity(1000, this.l.b(this.f));
        f2 = VelocityTrackerCompat.a(this.t, this.k);
        f1 = VelocityTrackerCompat.b(this.t, this.k);
        if (f2 <= 0.0F) {
          break label149;
        }
      }
      label149:
      for (int i2 = 8;; i2 = 4)
      {
        f2 = Math.abs(f2);
        if (((i2 & paramInt) == 0) || (i1 != i2) || (f2 < this.l.a(this.e)) || (f2 <= Math.abs(f1))) {
          break label155;
        }
        return i2;
        i1 = 4;
        break;
      }
      label155:
      float f1 = this.r.getWidth();
      float f2 = this.l.a(paramViewHolder);
      if (((paramInt & i1) != 0) && (Math.abs(this.g) > f1 * f2)) {
        return i1;
      }
    }
    return 0;
  }
  
  private View b(MotionEvent paramMotionEvent)
  {
    float f1 = paramMotionEvent.getX();
    float f2 = paramMotionEvent.getY();
    if (this.b != null)
    {
      paramMotionEvent = this.b.itemView;
      if (a(paramMotionEvent, f1, f2, this.i + this.g, this.j + this.h)) {
        return paramMotionEvent;
      }
    }
    int i1 = this.o.size() - 1;
    while (i1 >= 0)
    {
      paramMotionEvent = (RecoverAnimation)this.o.get(i1);
      View localView = paramMotionEvent.h.itemView;
      if (a(localView, f1, f2, paramMotionEvent.k, paramMotionEvent.l)) {
        return localView;
      }
      i1 -= 1;
    }
    return this.r.a(f1, f2);
  }
  
  private void b(RecyclerView.ViewHolder paramViewHolder)
  {
    if (this.r.isLayoutRequested()) {}
    label10:
    int i1;
    int i2;
    Object localObject;
    int i3;
    int i4;
    do
    {
      do
      {
        float f1;
        do
        {
          break label10;
          do
          {
            return;
          } while (this.m != 2);
          f1 = this.l.b(paramViewHolder);
          i1 = (int)(this.i + this.g);
          i2 = (int)(this.j + this.h);
        } while ((Math.abs(i2 - paramViewHolder.itemView.getTop()) < paramViewHolder.itemView.getHeight() * f1) && (Math.abs(i1 - paramViewHolder.itemView.getLeft()) < paramViewHolder.itemView.getWidth() * f1));
        localObject = a(paramViewHolder);
      } while (((List)localObject).size() == 0);
      localObject = this.l.a(paramViewHolder, (List)localObject, i1, i2);
      if (localObject == null)
      {
        this.u.clear();
        this.v.clear();
        return;
      }
      i3 = ((RecyclerView.ViewHolder)localObject).getAdapterPosition();
      i4 = paramViewHolder.getAdapterPosition();
    } while (!this.l.b(this.r, paramViewHolder, (RecyclerView.ViewHolder)localObject));
    this.l.a(this.r, paramViewHolder, i4, (RecyclerView.ViewHolder)localObject, i3, i1, i2);
  }
  
  private boolean b()
  {
    if (this.b == null)
    {
      this.B = Long.MIN_VALUE;
      return false;
    }
    long l2 = System.currentTimeMillis();
    long l1;
    int i2;
    int i3;
    int i1;
    int i4;
    if (this.B == Long.MIN_VALUE)
    {
      l1 = 0L;
      RecyclerView.LayoutManager localLayoutManager = this.r.getLayoutManager();
      if (this.A == null) {
        this.A = new Rect();
      }
      i2 = 0;
      i3 = 0;
      localLayoutManager.a(this.b.itemView, this.A);
      i1 = i2;
      if (localLayoutManager.e())
      {
        i4 = (int)(this.i + this.g);
        i1 = i4 - this.A.left - this.r.getPaddingLeft();
        if ((this.g >= 0.0F) || (i1 >= 0)) {
          break label314;
        }
      }
      label136:
      i2 = i3;
      if (localLayoutManager.f())
      {
        i4 = (int)(this.j + this.h);
        i2 = i4 - this.A.top - this.r.getPaddingTop();
        if ((this.h >= 0.0F) || (i2 >= 0)) {
          break label377;
        }
      }
    }
    for (;;)
    {
      i3 = i1;
      if (i1 != 0) {
        i3 = this.l.a(this.r, this.b.itemView.getWidth(), i1, this.r.getWidth(), l1);
      }
      i1 = i2;
      if (i2 != 0) {
        i1 = this.l.a(this.r, this.b.itemView.getHeight(), i2, this.r.getHeight(), l1);
      }
      if ((i3 == 0) && (i1 == 0)) {
        break label440;
      }
      if (this.B == Long.MIN_VALUE) {
        this.B = l2;
      }
      this.r.scrollBy(i3, i1);
      return true;
      l1 = l2 - this.B;
      break;
      label314:
      i1 = i2;
      if (this.g <= 0.0F) {
        break label136;
      }
      i4 = this.b.itemView.getWidth() + i4 + this.A.right - (this.r.getWidth() - this.r.getPaddingRight());
      i1 = i2;
      if (i4 <= 0) {
        break label136;
      }
      i1 = i4;
      break label136;
      label377:
      i2 = i3;
      if (this.h > 0.0F)
      {
        i4 = this.b.itemView.getHeight() + i4 + this.A.bottom - (this.r.getHeight() - this.r.getPaddingBottom());
        i2 = i3;
        if (i4 > 0) {
          i2 = i4;
        }
      }
    }
    label440:
    this.B = Long.MIN_VALUE;
    return false;
  }
  
  private int c(RecyclerView.ViewHolder paramViewHolder)
  {
    int i1;
    if (this.m == 2) {
      i1 = 0;
    }
    int i4;
    int i2;
    label133:
    do
    {
      int i3;
      do
      {
        do
        {
          return i1;
          i1 = this.l.a(this.r, paramViewHolder);
          i3 = (this.l.b(i1, ViewCompat.h(this.r)) & 0xFF00) >> 8;
          if (i3 == 0) {
            return 0;
          }
          i4 = (i1 & 0xFF00) >> 8;
          if (Math.abs(this.g) <= Math.abs(this.h)) {
            break label133;
          }
          i2 = b(paramViewHolder, i3);
          if (i2 <= 0) {
            break;
          }
          i1 = i2;
        } while ((i4 & i2) != 0);
        return Callback.a(i2, ViewCompat.h(this.r));
        i2 = c(paramViewHolder, i3);
        i1 = i2;
      } while (i2 > 0);
      do
      {
        return 0;
        i2 = c(paramViewHolder, i3);
        i1 = i2;
        if (i2 > 0) {
          break;
        }
        i2 = b(paramViewHolder, i3);
      } while (i2 <= 0);
      i1 = i2;
    } while ((i4 & i2) != 0);
    return Callback.a(i2, ViewCompat.h(this.r));
  }
  
  private int c(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    if ((paramInt & 0x3) != 0)
    {
      int i1;
      if (this.h > 0.0F)
      {
        i1 = 2;
        if ((this.t == null) || (this.k <= -1)) {
          break label152;
        }
        this.t.computeCurrentVelocity(1000, this.l.b(this.f));
        f1 = VelocityTrackerCompat.a(this.t, this.k);
        f2 = VelocityTrackerCompat.b(this.t, this.k);
        if (f2 <= 0.0F) {
          break label146;
        }
      }
      label146:
      for (int i2 = 2;; i2 = 1)
      {
        f2 = Math.abs(f2);
        if (((i2 & paramInt) == 0) || (i2 != i1) || (f2 < this.l.a(this.e)) || (f2 <= Math.abs(f1))) {
          break label152;
        }
        return i2;
        i1 = 1;
        break;
      }
      label152:
      float f1 = this.r.getHeight();
      float f2 = this.l.a(paramViewHolder);
      if (((paramInt & i1) != 0) && (Math.abs(this.h) > f1 * f2)) {
        return i1;
      }
    }
    return 0;
  }
  
  private RecoverAnimation c(MotionEvent paramMotionEvent)
  {
    if (this.o.isEmpty())
    {
      paramMotionEvent = null;
      return paramMotionEvent;
    }
    View localView = b(paramMotionEvent);
    int i1 = this.o.size() - 1;
    for (;;)
    {
      if (i1 < 0) {
        break label74;
      }
      RecoverAnimation localRecoverAnimation = (RecoverAnimation)this.o.get(i1);
      paramMotionEvent = localRecoverAnimation;
      if (localRecoverAnimation.h.itemView == localView) {
        break;
      }
      i1 -= 1;
    }
    label74:
    return null;
  }
  
  private void c()
  {
    if (this.t != null) {
      this.t.recycle();
    }
    this.t = VelocityTracker.obtain();
  }
  
  private void c(View paramView)
  {
    if (paramView == this.x)
    {
      this.x = null;
      if (this.w != null) {
        this.r.setChildDrawingOrderCallback(null);
      }
    }
  }
  
  private void d()
  {
    if (this.t != null)
    {
      this.t.recycle();
      this.t = null;
    }
  }
  
  private void e()
  {
    if (Build.VERSION.SDK_INT >= 21) {
      return;
    }
    if (this.w == null) {
      this.w = new RecyclerView.ChildDrawingOrderCallback()
      {
        public int a(int paramAnonymousInt1, int paramAnonymousInt2)
        {
          if (ItemTouchHelper.g(ItemTouchHelper.this) == null) {}
          int i;
          do
          {
            return paramAnonymousInt2;
            int j = ItemTouchHelper.i(ItemTouchHelper.this);
            i = j;
            if (j == -1)
            {
              i = ItemTouchHelper.c(ItemTouchHelper.this).indexOfChild(ItemTouchHelper.g(ItemTouchHelper.this));
              ItemTouchHelper.a(ItemTouchHelper.this, i);
            }
            if (paramAnonymousInt2 == paramAnonymousInt1 - 1) {
              return i;
            }
          } while (paramAnonymousInt2 < i);
          return paramAnonymousInt2 + 1;
        }
      };
    }
    this.r.setChildDrawingOrderCallback(this.w);
  }
  
  public void a(Canvas paramCanvas, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    this.y = -1;
    float f1 = 0.0F;
    float f2 = 0.0F;
    if (this.b != null)
    {
      a(this.p);
      f1 = this.p[0];
      f2 = this.p[1];
    }
    Callback.b(this.l, paramCanvas, paramRecyclerView, this.b, this.o, this.m, f1, f2);
  }
  
  public void a(Rect paramRect, View paramView, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    paramRect.setEmpty();
  }
  
  public void a(View paramView) {}
  
  public void b(Canvas paramCanvas, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    float f1 = 0.0F;
    float f2 = 0.0F;
    if (this.b != null)
    {
      a(this.p);
      f1 = this.p[0];
      f2 = this.p[1];
    }
    Callback.a(this.l, paramCanvas, paramRecyclerView, this.b, this.o, this.m, f1, f2);
  }
  
  public void b(View paramView)
  {
    c(paramView);
    paramView = this.r.a(paramView);
    if (paramView == null) {}
    do
    {
      return;
      if ((this.b != null) && (paramView == this.b))
      {
        a(null, 0);
        return;
      }
      a(paramView, false);
    } while (!this.a.remove(paramView.itemView));
    this.l.c(this.r, paramView);
  }
  
  public static abstract class Callback
  {
    private static final ItemTouchUIUtil a = new ItemTouchUIUtilImpl.Gingerbread();
    private static final Interpolator b = new Interpolator()
    {
      public float getInterpolation(float paramAnonymousFloat)
      {
        return paramAnonymousFloat * paramAnonymousFloat * paramAnonymousFloat * paramAnonymousFloat * paramAnonymousFloat;
      }
    };
    private static final Interpolator c = new Interpolator()
    {
      public float getInterpolation(float paramAnonymousFloat)
      {
        paramAnonymousFloat -= 1.0F;
        return paramAnonymousFloat * paramAnonymousFloat * paramAnonymousFloat * paramAnonymousFloat * paramAnonymousFloat + 1.0F;
      }
    };
    private int d = -1;
    
    static
    {
      if (Build.VERSION.SDK_INT >= 21)
      {
        a = new ItemTouchUIUtilImpl.Lollipop();
        return;
      }
      if (Build.VERSION.SDK_INT >= 11)
      {
        a = new ItemTouchUIUtilImpl.Honeycomb();
        return;
      }
    }
    
    public static int a(int paramInt1, int paramInt2)
    {
      int i = paramInt1 & 0xC0C0C;
      if (i == 0) {
        return paramInt1;
      }
      paramInt1 &= (i ^ 0xFFFFFFFF);
      if (paramInt2 == 0) {
        return paramInt1 | i << 2;
      }
      return paramInt1 | i << 1 & 0xFFF3F3F3 | (i << 1 & 0xC0C0C) << 2;
    }
    
    private int a(RecyclerView paramRecyclerView)
    {
      if (this.d == -1) {
        this.d = paramRecyclerView.getResources().getDimensionPixelSize(R.dimen.item_touch_helper_max_drag_scroll_per_frame);
      }
      return this.d;
    }
    
    private void a(Canvas paramCanvas, RecyclerView paramRecyclerView, RecyclerView.ViewHolder paramViewHolder, List<ItemTouchHelper.RecoverAnimation> paramList, int paramInt, float paramFloat1, float paramFloat2)
    {
      int j = paramList.size();
      int i = 0;
      while (i < j)
      {
        ItemTouchHelper.RecoverAnimation localRecoverAnimation = (ItemTouchHelper.RecoverAnimation)paramList.get(i);
        localRecoverAnimation.b();
        int k = paramCanvas.save();
        a(paramCanvas, paramRecyclerView, localRecoverAnimation.h, localRecoverAnimation.k, localRecoverAnimation.l, localRecoverAnimation.i, false);
        paramCanvas.restoreToCount(k);
        i += 1;
      }
      if (paramViewHolder != null)
      {
        i = paramCanvas.save();
        a(paramCanvas, paramRecyclerView, paramViewHolder, paramFloat1, paramFloat2, paramInt, true);
        paramCanvas.restoreToCount(i);
      }
    }
    
    private void b(Canvas paramCanvas, RecyclerView paramRecyclerView, RecyclerView.ViewHolder paramViewHolder, List<ItemTouchHelper.RecoverAnimation> paramList, int paramInt, float paramFloat1, float paramFloat2)
    {
      int j = paramList.size();
      int i = 0;
      while (i < j)
      {
        ItemTouchHelper.RecoverAnimation localRecoverAnimation = (ItemTouchHelper.RecoverAnimation)paramList.get(i);
        int k = paramCanvas.save();
        b(paramCanvas, paramRecyclerView, localRecoverAnimation.h, localRecoverAnimation.k, localRecoverAnimation.l, localRecoverAnimation.i, false);
        paramCanvas.restoreToCount(k);
        i += 1;
      }
      if (paramViewHolder != null)
      {
        i = paramCanvas.save();
        b(paramCanvas, paramRecyclerView, paramViewHolder, paramFloat1, paramFloat2, paramInt, true);
        paramCanvas.restoreToCount(i);
      }
      i = 0;
      paramInt = j - 1;
      if (paramInt >= 0)
      {
        paramCanvas = (ItemTouchHelper.RecoverAnimation)paramList.get(paramInt);
        if ((ItemTouchHelper.RecoverAnimation.a(paramCanvas)) && (!paramCanvas.j)) {
          paramList.remove(paramInt);
        }
        for (;;)
        {
          paramInt -= 1;
          break;
          if (!ItemTouchHelper.RecoverAnimation.a(paramCanvas)) {
            i = 1;
          }
        }
      }
      if (i != 0) {
        paramRecyclerView.invalidate();
      }
    }
    
    private boolean d(RecyclerView paramRecyclerView, RecyclerView.ViewHolder paramViewHolder)
    {
      return (0xFF0000 & b(paramRecyclerView, paramViewHolder)) != 0;
    }
    
    public float a(float paramFloat)
    {
      return paramFloat;
    }
    
    public float a(RecyclerView.ViewHolder paramViewHolder)
    {
      return 0.5F;
    }
    
    public int a(RecyclerView paramRecyclerView, int paramInt1, int paramInt2, int paramInt3, long paramLong)
    {
      paramInt3 = a(paramRecyclerView);
      int i = Math.abs(paramInt2);
      int j = (int)Math.signum(paramInt2);
      float f = Math.min(1.0F, 1.0F * i / paramInt1);
      paramInt1 = (int)(j * paramInt3 * c.getInterpolation(f));
      if (paramLong > 2000L) {}
      for (f = 1.0F;; f = (float)paramLong / 2000.0F)
      {
        paramInt1 = (int)(paramInt1 * b.getInterpolation(f));
        if (paramInt1 != 0) {
          return paramInt1;
        }
        if (paramInt2 <= 0) {
          break;
        }
        return 1;
      }
      return -1;
      return paramInt1;
    }
    
    public abstract int a(RecyclerView paramRecyclerView, RecyclerView.ViewHolder paramViewHolder);
    
    public long a(RecyclerView paramRecyclerView, int paramInt, float paramFloat1, float paramFloat2)
    {
      paramRecyclerView = paramRecyclerView.getItemAnimator();
      if (paramRecyclerView == null)
      {
        if (paramInt == 8) {
          return 200L;
        }
        return 250L;
      }
      if (paramInt == 8) {
        return paramRecyclerView.d();
      }
      return paramRecyclerView.f();
    }
    
    public RecyclerView.ViewHolder a(RecyclerView.ViewHolder paramViewHolder, List<RecyclerView.ViewHolder> paramList, int paramInt1, int paramInt2)
    {
      int n = paramViewHolder.itemView.getWidth();
      int i1 = paramViewHolder.itemView.getHeight();
      Object localObject2 = null;
      int j = -1;
      int i2 = paramInt1 - paramViewHolder.itemView.getLeft();
      int i3 = paramInt2 - paramViewHolder.itemView.getTop();
      int i4 = paramList.size();
      int k = 0;
      while (k < i4)
      {
        RecyclerView.ViewHolder localViewHolder = (RecyclerView.ViewHolder)paramList.get(k);
        Object localObject1 = localObject2;
        int i = j;
        int m;
        if (i2 > 0)
        {
          m = localViewHolder.itemView.getRight() - (paramInt1 + n);
          localObject1 = localObject2;
          i = j;
          if (m < 0)
          {
            localObject1 = localObject2;
            i = j;
            if (localViewHolder.itemView.getRight() > paramViewHolder.itemView.getRight())
            {
              m = Math.abs(m);
              localObject1 = localObject2;
              i = j;
              if (m > j)
              {
                i = m;
                localObject1 = localViewHolder;
              }
            }
          }
        }
        localObject2 = localObject1;
        j = i;
        if (i2 < 0)
        {
          m = localViewHolder.itemView.getLeft() - paramInt1;
          localObject2 = localObject1;
          j = i;
          if (m > 0)
          {
            localObject2 = localObject1;
            j = i;
            if (localViewHolder.itemView.getLeft() < paramViewHolder.itemView.getLeft())
            {
              m = Math.abs(m);
              localObject2 = localObject1;
              j = i;
              if (m > i)
              {
                j = m;
                localObject2 = localViewHolder;
              }
            }
          }
        }
        localObject1 = localObject2;
        i = j;
        if (i3 < 0)
        {
          m = localViewHolder.itemView.getTop() - paramInt2;
          localObject1 = localObject2;
          i = j;
          if (m > 0)
          {
            localObject1 = localObject2;
            i = j;
            if (localViewHolder.itemView.getTop() < paramViewHolder.itemView.getTop())
            {
              m = Math.abs(m);
              localObject1 = localObject2;
              i = j;
              if (m > j)
              {
                i = m;
                localObject1 = localViewHolder;
              }
            }
          }
        }
        localObject2 = localObject1;
        j = i;
        if (i3 > 0)
        {
          m = localViewHolder.itemView.getBottom() - (paramInt2 + i1);
          localObject2 = localObject1;
          j = i;
          if (m < 0)
          {
            localObject2 = localObject1;
            j = i;
            if (localViewHolder.itemView.getBottom() > paramViewHolder.itemView.getBottom())
            {
              m = Math.abs(m);
              localObject2 = localObject1;
              j = i;
              if (m > i)
              {
                j = m;
                localObject2 = localViewHolder;
              }
            }
          }
        }
        k += 1;
      }
      return (RecyclerView.ViewHolder)localObject2;
    }
    
    public void a(Canvas paramCanvas, RecyclerView paramRecyclerView, RecyclerView.ViewHolder paramViewHolder, float paramFloat1, float paramFloat2, int paramInt, boolean paramBoolean)
    {
      a.a(paramCanvas, paramRecyclerView, paramViewHolder.itemView, paramFloat1, paramFloat2, paramInt, paramBoolean);
    }
    
    public abstract void a(RecyclerView.ViewHolder paramViewHolder, int paramInt);
    
    public void a(RecyclerView paramRecyclerView, RecyclerView.ViewHolder paramViewHolder1, int paramInt1, RecyclerView.ViewHolder paramViewHolder2, int paramInt2, int paramInt3, int paramInt4)
    {
      RecyclerView.LayoutManager localLayoutManager = paramRecyclerView.getLayoutManager();
      if ((localLayoutManager instanceof ItemTouchHelper.ViewDropHandler)) {
        ((ItemTouchHelper.ViewDropHandler)localLayoutManager).a(paramViewHolder1.itemView, paramViewHolder2.itemView, paramInt3, paramInt4);
      }
      do
      {
        do
        {
          return;
          if (localLayoutManager.e())
          {
            if (localLayoutManager.h(paramViewHolder2.itemView) <= paramRecyclerView.getPaddingLeft()) {
              paramRecyclerView.a(paramInt2);
            }
            if (localLayoutManager.j(paramViewHolder2.itemView) >= paramRecyclerView.getWidth() - paramRecyclerView.getPaddingRight()) {
              paramRecyclerView.a(paramInt2);
            }
          }
        } while (!localLayoutManager.f());
        if (localLayoutManager.i(paramViewHolder2.itemView) <= paramRecyclerView.getPaddingTop()) {
          paramRecyclerView.a(paramInt2);
        }
      } while (localLayoutManager.k(paramViewHolder2.itemView) < paramRecyclerView.getHeight() - paramRecyclerView.getPaddingBottom());
      paramRecyclerView.a(paramInt2);
    }
    
    public boolean a()
    {
      return true;
    }
    
    public boolean a(RecyclerView paramRecyclerView, RecyclerView.ViewHolder paramViewHolder1, RecyclerView.ViewHolder paramViewHolder2)
    {
      return true;
    }
    
    public float b(float paramFloat)
    {
      return paramFloat;
    }
    
    public float b(RecyclerView.ViewHolder paramViewHolder)
    {
      return 0.5F;
    }
    
    public int b(int paramInt1, int paramInt2)
    {
      int i = paramInt1 & 0x303030;
      if (i == 0) {
        return paramInt1;
      }
      paramInt1 &= (i ^ 0xFFFFFFFF);
      if (paramInt2 == 0) {
        return paramInt1 | i >> 2;
      }
      return paramInt1 | i >> 1 & 0xFFCFCFCF | (i >> 1 & 0x303030) >> 2;
    }
    
    final int b(RecyclerView paramRecyclerView, RecyclerView.ViewHolder paramViewHolder)
    {
      return b(a(paramRecyclerView, paramViewHolder), ViewCompat.h(paramRecyclerView));
    }
    
    public void b(Canvas paramCanvas, RecyclerView paramRecyclerView, RecyclerView.ViewHolder paramViewHolder, float paramFloat1, float paramFloat2, int paramInt, boolean paramBoolean)
    {
      a.b(paramCanvas, paramRecyclerView, paramViewHolder.itemView, paramFloat1, paramFloat2, paramInt, paramBoolean);
    }
    
    public void b(RecyclerView.ViewHolder paramViewHolder, int paramInt)
    {
      if (paramViewHolder != null) {
        a.b(paramViewHolder.itemView);
      }
    }
    
    public boolean b()
    {
      return true;
    }
    
    public abstract boolean b(RecyclerView paramRecyclerView, RecyclerView.ViewHolder paramViewHolder1, RecyclerView.ViewHolder paramViewHolder2);
    
    public int c()
    {
      return 0;
    }
    
    public void c(RecyclerView paramRecyclerView, RecyclerView.ViewHolder paramViewHolder)
    {
      a.a(paramViewHolder.itemView);
    }
  }
  
  private class ItemTouchHelperGestureListener
    extends GestureDetector.SimpleOnGestureListener
  {
    public boolean onDown(MotionEvent paramMotionEvent)
    {
      return true;
    }
    
    public void onLongPress(MotionEvent paramMotionEvent)
    {
      Object localObject = ItemTouchHelper.b(this.a, paramMotionEvent);
      if (localObject != null)
      {
        localObject = ItemTouchHelper.c(this.a).a((View)localObject);
        if ((localObject != null) && (ItemTouchHelper.Callback.a(this.a.l, ItemTouchHelper.c(this.a), (RecyclerView.ViewHolder)localObject))) {
          break label57;
        }
      }
      label57:
      do
      {
        do
        {
          return;
        } while (MotionEventCompat.b(paramMotionEvent, 0) != this.a.k);
        int i = MotionEventCompat.a(paramMotionEvent, this.a.k);
        float f1 = MotionEventCompat.c(paramMotionEvent, i);
        float f2 = MotionEventCompat.d(paramMotionEvent, i);
        this.a.c = f1;
        this.a.d = f2;
        paramMotionEvent = this.a;
        this.a.h = 0.0F;
        paramMotionEvent.g = 0.0F;
      } while (!this.a.l.a());
      ItemTouchHelper.a(this.a, (RecyclerView.ViewHolder)localObject, 2);
    }
  }
  
  private class RecoverAnimation
    implements AnimatorListenerCompat
  {
    private final ValueAnimatorCompat a;
    private final int b;
    private boolean c = false;
    final float d;
    final float e;
    final float f;
    final float g;
    final RecyclerView.ViewHolder h;
    final int i;
    public boolean j;
    float k;
    float l;
    boolean m = false;
    private float o;
    
    public RecoverAnimation(RecyclerView.ViewHolder paramViewHolder, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
      this.i = paramInt2;
      this.b = paramInt1;
      this.h = paramViewHolder;
      this.d = paramFloat1;
      this.e = paramFloat2;
      this.f = paramFloat3;
      this.g = paramFloat4;
      this.a = AnimatorCompatHelper.a();
      this.a.a(new AnimatorUpdateListenerCompat()
      {
        public void a(ValueAnimatorCompat paramAnonymousValueAnimatorCompat)
        {
          ItemTouchHelper.RecoverAnimation.this.a(paramAnonymousValueAnimatorCompat.b());
        }
      });
      this.a.a(paramViewHolder.itemView);
      this.a.a(this);
      a(0.0F);
    }
    
    public void a()
    {
      this.h.setIsRecyclable(false);
      this.a.a();
    }
    
    public void a(float paramFloat)
    {
      this.o = paramFloat;
    }
    
    public void a(long paramLong)
    {
      this.a.a(paramLong);
    }
    
    public void a(ValueAnimatorCompat paramValueAnimatorCompat) {}
    
    public void b()
    {
      if (this.d == this.f) {}
      for (this.k = ViewCompat.o(this.h.itemView); this.e == this.g; this.k = (this.d + this.o * (this.f - this.d)))
      {
        this.l = ViewCompat.p(this.h.itemView);
        return;
      }
      this.l = (this.e + this.o * (this.g - this.e));
    }
    
    public void b(ValueAnimatorCompat paramValueAnimatorCompat)
    {
      if (!this.c) {
        this.h.setIsRecyclable(true);
      }
      this.c = true;
    }
    
    public void c(ValueAnimatorCompat paramValueAnimatorCompat)
    {
      a(1.0F);
    }
    
    public void cancel()
    {
      this.a.cancel();
    }
    
    public void d(ValueAnimatorCompat paramValueAnimatorCompat) {}
  }
  
  public static abstract class SimpleCallback
    extends ItemTouchHelper.Callback
  {}
  
  public static abstract interface ViewDropHandler
  {
    public abstract void a(View paramView1, View paramView2, int paramInt1, int paramInt2);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/helper/ItemTouchHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */