package android.support.v7.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

class CardViewEclairMr1
  implements CardViewImpl
{
  final RectF a = new RectF();
  
  private RoundRectDrawableWithShadow a(Context paramContext, int paramInt, float paramFloat1, float paramFloat2, float paramFloat3)
  {
    return new RoundRectDrawableWithShadow(paramContext.getResources(), paramInt, paramFloat1, paramFloat2, paramFloat3);
  }
  
  private RoundRectDrawableWithShadow i(CardViewDelegate paramCardViewDelegate)
  {
    return (RoundRectDrawableWithShadow)paramCardViewDelegate.c();
  }
  
  public float a(CardViewDelegate paramCardViewDelegate)
  {
    return i(paramCardViewDelegate).c();
  }
  
  public void a()
  {
    RoundRectDrawableWithShadow.c = new RoundRectDrawableWithShadow.RoundRectHelper()
    {
      public void a(Canvas paramAnonymousCanvas, RectF paramAnonymousRectF, float paramAnonymousFloat, Paint paramAnonymousPaint)
      {
        float f1 = paramAnonymousFloat * 2.0F;
        float f2 = paramAnonymousRectF.width() - f1 - 1.0F;
        float f3 = paramAnonymousRectF.height();
        if (paramAnonymousFloat >= 1.0F)
        {
          float f4 = paramAnonymousFloat + 0.5F;
          CardViewEclairMr1.this.a.set(-f4, -f4, f4, f4);
          int i = paramAnonymousCanvas.save();
          paramAnonymousCanvas.translate(paramAnonymousRectF.left + f4, paramAnonymousRectF.top + f4);
          paramAnonymousCanvas.drawArc(CardViewEclairMr1.this.a, 180.0F, 90.0F, true, paramAnonymousPaint);
          paramAnonymousCanvas.translate(f2, 0.0F);
          paramAnonymousCanvas.rotate(90.0F);
          paramAnonymousCanvas.drawArc(CardViewEclairMr1.this.a, 180.0F, 90.0F, true, paramAnonymousPaint);
          paramAnonymousCanvas.translate(f3 - f1 - 1.0F, 0.0F);
          paramAnonymousCanvas.rotate(90.0F);
          paramAnonymousCanvas.drawArc(CardViewEclairMr1.this.a, 180.0F, 90.0F, true, paramAnonymousPaint);
          paramAnonymousCanvas.translate(f2, 0.0F);
          paramAnonymousCanvas.rotate(90.0F);
          paramAnonymousCanvas.drawArc(CardViewEclairMr1.this.a, 180.0F, 90.0F, true, paramAnonymousPaint);
          paramAnonymousCanvas.restoreToCount(i);
          paramAnonymousCanvas.drawRect(paramAnonymousRectF.left + f4 - 1.0F, paramAnonymousRectF.top, 1.0F + (paramAnonymousRectF.right - f4), paramAnonymousRectF.top + f4, paramAnonymousPaint);
          paramAnonymousCanvas.drawRect(paramAnonymousRectF.left + f4 - 1.0F, 1.0F + (paramAnonymousRectF.bottom - f4), 1.0F + (paramAnonymousRectF.right - f4), paramAnonymousRectF.bottom, paramAnonymousPaint);
        }
        f1 = paramAnonymousRectF.left;
        f2 = paramAnonymousRectF.top;
        paramAnonymousCanvas.drawRect(f1, Math.max(0.0F, paramAnonymousFloat - 1.0F) + f2, paramAnonymousRectF.right, 1.0F + (paramAnonymousRectF.bottom - paramAnonymousFloat), paramAnonymousPaint);
      }
    };
  }
  
  public void a(CardViewDelegate paramCardViewDelegate, float paramFloat)
  {
    i(paramCardViewDelegate).a(paramFloat);
    f(paramCardViewDelegate);
  }
  
  public void a(CardViewDelegate paramCardViewDelegate, int paramInt)
  {
    i(paramCardViewDelegate).a(paramInt);
  }
  
  public void a(CardViewDelegate paramCardViewDelegate, Context paramContext, int paramInt, float paramFloat1, float paramFloat2, float paramFloat3)
  {
    paramContext = a(paramContext, paramInt, paramFloat1, paramFloat2, paramFloat3);
    paramContext.a(paramCardViewDelegate.b());
    paramCardViewDelegate.a(paramContext);
    f(paramCardViewDelegate);
  }
  
  public float b(CardViewDelegate paramCardViewDelegate)
  {
    return i(paramCardViewDelegate).d();
  }
  
  public void b(CardViewDelegate paramCardViewDelegate, float paramFloat)
  {
    i(paramCardViewDelegate).c(paramFloat);
    f(paramCardViewDelegate);
  }
  
  public float c(CardViewDelegate paramCardViewDelegate)
  {
    return i(paramCardViewDelegate).e();
  }
  
  public void c(CardViewDelegate paramCardViewDelegate, float paramFloat)
  {
    i(paramCardViewDelegate).b(paramFloat);
  }
  
  public float d(CardViewDelegate paramCardViewDelegate)
  {
    return i(paramCardViewDelegate).a();
  }
  
  public float e(CardViewDelegate paramCardViewDelegate)
  {
    return i(paramCardViewDelegate).b();
  }
  
  public void f(CardViewDelegate paramCardViewDelegate)
  {
    Rect localRect = new Rect();
    i(paramCardViewDelegate).a(localRect);
    paramCardViewDelegate.a((int)Math.ceil(b(paramCardViewDelegate)), (int)Math.ceil(c(paramCardViewDelegate)));
    paramCardViewDelegate.a(localRect.left, localRect.top, localRect.right, localRect.bottom);
  }
  
  public void g(CardViewDelegate paramCardViewDelegate) {}
  
  public void h(CardViewDelegate paramCardViewDelegate)
  {
    i(paramCardViewDelegate).a(paramCardViewDelegate.b());
    f(paramCardViewDelegate);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/CardViewEclairMr1.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */