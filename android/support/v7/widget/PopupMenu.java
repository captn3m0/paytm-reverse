package android.support.v7.widget;

import android.content.Context;
import android.support.v7.appcompat.R.attr;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuBuilder.Callback;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.view.menu.MenuPresenter.Callback;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class PopupMenu
  implements MenuBuilder.Callback, MenuPresenter.Callback
{
  private Context a;
  private MenuBuilder b;
  private View c;
  private MenuPopupHelper d;
  private OnMenuItemClickListener e;
  private OnDismissListener f;
  
  public PopupMenu(Context paramContext, View paramView)
  {
    this(paramContext, paramView, 0);
  }
  
  public PopupMenu(Context paramContext, View paramView, int paramInt)
  {
    this(paramContext, paramView, paramInt, R.attr.popupMenuStyle, 0);
  }
  
  public PopupMenu(Context paramContext, View paramView, int paramInt1, int paramInt2, int paramInt3)
  {
    this.a = paramContext;
    this.b = new MenuBuilder(paramContext);
    this.b.a(this);
    this.c = paramView;
    this.d = new MenuPopupHelper(paramContext, this.b, paramView, false, paramInt2, paramInt3);
    this.d.a(paramInt1);
    this.d.a(this);
  }
  
  public Menu a()
  {
    return this.b;
  }
  
  public void a(MenuBuilder paramMenuBuilder) {}
  
  public void a(MenuBuilder paramMenuBuilder, boolean paramBoolean)
  {
    if (this.f != null) {
      this.f.a(this);
    }
  }
  
  public void a(OnMenuItemClickListener paramOnMenuItemClickListener)
  {
    this.e = paramOnMenuItemClickListener;
  }
  
  public boolean a(MenuBuilder paramMenuBuilder, MenuItem paramMenuItem)
  {
    if (this.e != null) {
      return this.e.a(paramMenuItem);
    }
    return false;
  }
  
  public boolean a_(MenuBuilder paramMenuBuilder)
  {
    boolean bool = true;
    if (paramMenuBuilder == null) {
      bool = false;
    }
    while (!paramMenuBuilder.hasVisibleItems()) {
      return bool;
    }
    new MenuPopupHelper(this.a, paramMenuBuilder, this.c).d();
    return true;
  }
  
  public void b()
  {
    this.d.d();
  }
  
  public void c()
  {
    this.d.g();
  }
  
  public static abstract interface OnDismissListener
  {
    public abstract void a(PopupMenu paramPopupMenu);
  }
  
  public static abstract interface OnMenuItemClickListener
  {
    public abstract boolean a(MenuItem paramMenuItem);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/PopupMenu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */