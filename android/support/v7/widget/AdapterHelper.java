package android.support.v7.widget;

import android.support.v4.util.Pools.Pool;
import android.support.v4.util.Pools.SimplePool;
import java.util.ArrayList;
import java.util.List;

class AdapterHelper
  implements OpReorderer.Callback
{
  final ArrayList<UpdateOp> a = new ArrayList();
  final ArrayList<UpdateOp> b = new ArrayList();
  final Callback c;
  Runnable d;
  final boolean e;
  final OpReorderer f;
  private Pools.Pool<UpdateOp> g = new Pools.SimplePool(30);
  private int h = 0;
  
  AdapterHelper(Callback paramCallback)
  {
    this(paramCallback, false);
  }
  
  AdapterHelper(Callback paramCallback, boolean paramBoolean)
  {
    this.c = paramCallback;
    this.e = paramBoolean;
    this.f = new OpReorderer(this);
  }
  
  private void b(UpdateOp paramUpdateOp)
  {
    g(paramUpdateOp);
  }
  
  private void c(UpdateOp paramUpdateOp)
  {
    int i2 = paramUpdateOp.b;
    int n = 0;
    int m = paramUpdateOp.b + paramUpdateOp.d;
    int i1 = -1;
    int i = paramUpdateOp.b;
    if (i < m)
    {
      int k = 0;
      int j = 0;
      if ((this.c.a(i) != null) || (d(i)))
      {
        if (i1 == 0)
        {
          e(a(2, i2, n, null));
          j = 1;
        }
        i1 = 1;
        k = j;
        j = i1;
        label90:
        if (k == 0) {
          break label152;
        }
        i -= n;
        m -= n;
      }
      label152:
      for (k = 1;; k = n + 1)
      {
        i += 1;
        n = k;
        i1 = j;
        break;
        if (i1 == 1)
        {
          g(a(2, i2, n, null));
          k = 1;
        }
        j = 0;
        break label90;
      }
    }
    UpdateOp localUpdateOp = paramUpdateOp;
    if (n != paramUpdateOp.d)
    {
      a(paramUpdateOp);
      localUpdateOp = a(2, i2, n, null);
    }
    if (i1 == 0)
    {
      e(localUpdateOp);
      return;
    }
    g(localUpdateOp);
  }
  
  private int d(int paramInt1, int paramInt2)
  {
    int i = this.b.size() - 1;
    int j = paramInt1;
    UpdateOp localUpdateOp;
    if (i >= 0)
    {
      localUpdateOp = (UpdateOp)this.b.get(i);
      int k;
      if (localUpdateOp.a == 8) {
        if (localUpdateOp.b < localUpdateOp.d)
        {
          k = localUpdateOp.b;
          paramInt1 = localUpdateOp.d;
          label66:
          if ((j < k) || (j > paramInt1)) {
            break label202;
          }
          if (k != localUpdateOp.b) {
            break label157;
          }
          if (paramInt2 != 1) {
            break label137;
          }
          localUpdateOp.d += 1;
          label106:
          paramInt1 = j + 1;
        }
      }
      for (;;)
      {
        i -= 1;
        j = paramInt1;
        break;
        k = localUpdateOp.d;
        paramInt1 = localUpdateOp.b;
        break label66;
        label137:
        if (paramInt2 != 2) {
          break label106;
        }
        localUpdateOp.d -= 1;
        break label106;
        label157:
        if (paramInt2 == 1) {
          localUpdateOp.b += 1;
        }
        for (;;)
        {
          paramInt1 = j - 1;
          break;
          if (paramInt2 == 2) {
            localUpdateOp.b -= 1;
          }
        }
        label202:
        paramInt1 = j;
        if (j < localUpdateOp.b) {
          if (paramInt2 == 1)
          {
            localUpdateOp.b += 1;
            localUpdateOp.d += 1;
            paramInt1 = j;
          }
          else
          {
            paramInt1 = j;
            if (paramInt2 == 2)
            {
              localUpdateOp.b -= 1;
              localUpdateOp.d -= 1;
              paramInt1 = j;
              continue;
              if (localUpdateOp.b <= j)
              {
                if (localUpdateOp.a == 1)
                {
                  paramInt1 = j - localUpdateOp.d;
                }
                else
                {
                  paramInt1 = j;
                  if (localUpdateOp.a == 2) {
                    paramInt1 = j + localUpdateOp.d;
                  }
                }
              }
              else if (paramInt2 == 1)
              {
                localUpdateOp.b += 1;
                paramInt1 = j;
              }
              else
              {
                paramInt1 = j;
                if (paramInt2 == 2)
                {
                  localUpdateOp.b -= 1;
                  paramInt1 = j;
                }
              }
            }
          }
        }
      }
    }
    paramInt1 = this.b.size() - 1;
    if (paramInt1 >= 0)
    {
      localUpdateOp = (UpdateOp)this.b.get(paramInt1);
      if (localUpdateOp.a == 8) {
        if ((localUpdateOp.d == localUpdateOp.b) || (localUpdateOp.d < 0))
        {
          this.b.remove(paramInt1);
          a(localUpdateOp);
        }
      }
      for (;;)
      {
        paramInt1 -= 1;
        break;
        if (localUpdateOp.d <= 0)
        {
          this.b.remove(paramInt1);
          a(localUpdateOp);
        }
      }
    }
    return j;
  }
  
  private void d(UpdateOp paramUpdateOp)
  {
    int k = paramUpdateOp.b;
    int j = 0;
    int i3 = paramUpdateOp.b;
    int i4 = paramUpdateOp.d;
    int i2 = -1;
    int i = paramUpdateOp.b;
    if (i < i3 + i4)
    {
      int n;
      int m;
      if ((this.c.a(i) != null) || (d(i)))
      {
        n = j;
        int i1 = k;
        if (i2 == 0)
        {
          e(a(4, k, j, paramUpdateOp.c));
          n = 0;
          i1 = i;
        }
        m = 1;
        k = i1;
      }
      for (;;)
      {
        j = n + 1;
        i += 1;
        i2 = m;
        break;
        n = j;
        m = k;
        if (i2 == 1)
        {
          g(a(4, k, j, paramUpdateOp.c));
          n = 0;
          m = i;
        }
        j = 0;
        k = m;
        m = j;
      }
    }
    Object localObject = paramUpdateOp;
    if (j != paramUpdateOp.d)
    {
      localObject = paramUpdateOp.c;
      a(paramUpdateOp);
      localObject = a(4, k, j, localObject);
    }
    if (i2 == 0)
    {
      e((UpdateOp)localObject);
      return;
    }
    g((UpdateOp)localObject);
  }
  
  private boolean d(int paramInt)
  {
    int k = this.b.size();
    int i = 0;
    while (i < k)
    {
      UpdateOp localUpdateOp = (UpdateOp)this.b.get(i);
      if (localUpdateOp.a == 8)
      {
        if (a(localUpdateOp.d, i + 1) == paramInt) {
          return true;
        }
      }
      else if (localUpdateOp.a == 1)
      {
        int m = localUpdateOp.b;
        int n = localUpdateOp.d;
        int j = localUpdateOp.b;
        for (;;)
        {
          if (j >= m + n) {
            break label115;
          }
          if (a(j, i + 1) == paramInt) {
            break;
          }
          j += 1;
        }
      }
      label115:
      i += 1;
    }
    return false;
  }
  
  private void e(UpdateOp paramUpdateOp)
  {
    if ((paramUpdateOp.a == 1) || (paramUpdateOp.a == 8)) {
      throw new IllegalArgumentException("should not dispatch add or move for pre layout");
    }
    int i1 = d(paramUpdateOp.b, paramUpdateOp.a);
    int n = 1;
    int i = paramUpdateOp.b;
    int k;
    int m;
    label113:
    int i2;
    switch (paramUpdateOp.a)
    {
    case 3: 
    default: 
      throw new IllegalArgumentException("op should be remove or update." + paramUpdateOp);
    case 4: 
      k = 1;
      m = 1;
      if (m >= paramUpdateOp.d) {
        break label307;
      }
      i2 = d(paramUpdateOp.b + k * m, paramUpdateOp.a);
      int i3 = 0;
      j = i3;
      switch (paramUpdateOp.a)
      {
      default: 
        j = i3;
      case 3: 
        if (j == 0) {}
        break;
      }
      break;
    }
    for (int j = n + 1;; j = n)
    {
      m += 1;
      n = j;
      break label113;
      k = 0;
      break;
      if (i2 == i1 + 1) {}
      for (j = 1;; j = 0) {
        break;
      }
      if (i2 == i1) {}
      for (j = 1;; j = 0) {
        break;
      }
      localObject = a(paramUpdateOp.a, i1, n, paramUpdateOp.c);
      a((UpdateOp)localObject, i);
      a((UpdateOp)localObject);
      j = i;
      if (paramUpdateOp.a == 4) {
        j = i + n;
      }
      i1 = i2;
      n = 1;
      i = j;
    }
    label307:
    Object localObject = paramUpdateOp.c;
    a(paramUpdateOp);
    if (n > 0)
    {
      paramUpdateOp = a(paramUpdateOp.a, i1, n, localObject);
      a(paramUpdateOp, i);
      a(paramUpdateOp);
    }
  }
  
  private void f(UpdateOp paramUpdateOp)
  {
    g(paramUpdateOp);
  }
  
  private void g(UpdateOp paramUpdateOp)
  {
    this.b.add(paramUpdateOp);
    switch (paramUpdateOp.a)
    {
    case 3: 
    case 5: 
    case 6: 
    case 7: 
    default: 
      throw new IllegalArgumentException("Unknown update op type for " + paramUpdateOp);
    case 1: 
      this.c.c(paramUpdateOp.b, paramUpdateOp.d);
      return;
    case 8: 
      this.c.d(paramUpdateOp.b, paramUpdateOp.d);
      return;
    case 2: 
      this.c.b(paramUpdateOp.b, paramUpdateOp.d);
      return;
    }
    this.c.a(paramUpdateOp.b, paramUpdateOp.d, paramUpdateOp.c);
  }
  
  int a(int paramInt1, int paramInt2)
  {
    int k = this.b.size();
    int j = paramInt2;
    paramInt2 = paramInt1;
    paramInt1 = paramInt2;
    UpdateOp localUpdateOp;
    if (j < k)
    {
      localUpdateOp = (UpdateOp)this.b.get(j);
      if (localUpdateOp.a == 8) {
        if (localUpdateOp.b == paramInt2) {
          paramInt1 = localUpdateOp.d;
        }
      }
    }
    for (;;)
    {
      j += 1;
      paramInt2 = paramInt1;
      break;
      int i = paramInt2;
      if (localUpdateOp.b < paramInt2) {
        i = paramInt2 - 1;
      }
      paramInt1 = i;
      if (localUpdateOp.d <= i)
      {
        paramInt1 = i + 1;
        continue;
        paramInt1 = paramInt2;
        if (localUpdateOp.b <= paramInt2) {
          if (localUpdateOp.a == 2)
          {
            if (paramInt2 < localUpdateOp.b + localUpdateOp.d)
            {
              paramInt1 = -1;
              return paramInt1;
            }
            paramInt1 = paramInt2 - localUpdateOp.d;
          }
          else
          {
            paramInt1 = paramInt2;
            if (localUpdateOp.a == 1) {
              paramInt1 = paramInt2 + localUpdateOp.d;
            }
          }
        }
      }
    }
  }
  
  public UpdateOp a(int paramInt1, int paramInt2, int paramInt3, Object paramObject)
  {
    UpdateOp localUpdateOp = (UpdateOp)this.g.a();
    if (localUpdateOp == null) {
      return new UpdateOp(paramInt1, paramInt2, paramInt3, paramObject);
    }
    localUpdateOp.a = paramInt1;
    localUpdateOp.b = paramInt2;
    localUpdateOp.d = paramInt3;
    localUpdateOp.c = paramObject;
    return localUpdateOp;
  }
  
  void a()
  {
    a(this.a);
    a(this.b);
    this.h = 0;
  }
  
  public void a(UpdateOp paramUpdateOp)
  {
    if (!this.e)
    {
      paramUpdateOp.c = null;
      this.g.a(paramUpdateOp);
    }
  }
  
  void a(UpdateOp paramUpdateOp, int paramInt)
  {
    this.c.a(paramUpdateOp);
    switch (paramUpdateOp.a)
    {
    case 3: 
    default: 
      throw new IllegalArgumentException("only remove and update ops can be dispatched in first pass");
    case 2: 
      this.c.a(paramInt, paramUpdateOp.d);
      return;
    }
    this.c.a(paramInt, paramUpdateOp.d, paramUpdateOp.c);
  }
  
  void a(List<UpdateOp> paramList)
  {
    int j = paramList.size();
    int i = 0;
    while (i < j)
    {
      a((UpdateOp)paramList.get(i));
      i += 1;
    }
    paramList.clear();
  }
  
  boolean a(int paramInt)
  {
    return (this.h & paramInt) != 0;
  }
  
  boolean a(int paramInt1, int paramInt2, int paramInt3)
  {
    boolean bool = true;
    if (paramInt1 == paramInt2) {
      return false;
    }
    if (paramInt3 != 1) {
      throw new IllegalArgumentException("Moving more than 1 item is not supported yet");
    }
    this.a.add(a(8, paramInt1, paramInt2, null));
    this.h |= 0x8;
    if (this.a.size() == 1) {}
    for (;;)
    {
      return bool;
      bool = false;
    }
  }
  
  boolean a(int paramInt1, int paramInt2, Object paramObject)
  {
    this.a.add(a(4, paramInt1, paramInt2, paramObject));
    this.h |= 0x4;
    return this.a.size() == 1;
  }
  
  int b(int paramInt)
  {
    return a(paramInt, 0);
  }
  
  void b()
  {
    this.f.a(this.a);
    int j = this.a.size();
    int i = 0;
    if (i < j)
    {
      UpdateOp localUpdateOp = (UpdateOp)this.a.get(i);
      switch (localUpdateOp.a)
      {
      }
      for (;;)
      {
        if (this.d != null) {
          this.d.run();
        }
        i += 1;
        break;
        f(localUpdateOp);
        continue;
        c(localUpdateOp);
        continue;
        d(localUpdateOp);
        continue;
        b(localUpdateOp);
      }
    }
    this.a.clear();
  }
  
  boolean b(int paramInt1, int paramInt2)
  {
    this.a.add(a(1, paramInt1, paramInt2, null));
    this.h |= 0x1;
    return this.a.size() == 1;
  }
  
  public int c(int paramInt)
  {
    int m = this.a.size();
    int k = 0;
    int i = paramInt;
    paramInt = i;
    UpdateOp localUpdateOp;
    if (k < m)
    {
      localUpdateOp = (UpdateOp)this.a.get(k);
      switch (localUpdateOp.a)
      {
      default: 
        paramInt = i;
      }
    }
    for (;;)
    {
      k += 1;
      i = paramInt;
      break;
      paramInt = i;
      if (localUpdateOp.b <= i)
      {
        paramInt = i + localUpdateOp.d;
        continue;
        paramInt = i;
        if (localUpdateOp.b <= i)
        {
          if (localUpdateOp.b + localUpdateOp.d > i)
          {
            paramInt = -1;
            return paramInt;
          }
          paramInt = i - localUpdateOp.d;
          continue;
          if (localUpdateOp.b == i)
          {
            paramInt = localUpdateOp.d;
          }
          else
          {
            int j = i;
            if (localUpdateOp.b < i) {
              j = i - 1;
            }
            paramInt = j;
            if (localUpdateOp.d <= j) {
              paramInt = j + 1;
            }
          }
        }
      }
    }
  }
  
  void c()
  {
    int j = this.b.size();
    int i = 0;
    while (i < j)
    {
      this.c.b((UpdateOp)this.b.get(i));
      i += 1;
    }
    a(this.b);
    this.h = 0;
  }
  
  boolean c(int paramInt1, int paramInt2)
  {
    this.a.add(a(2, paramInt1, paramInt2, null));
    this.h |= 0x2;
    return this.a.size() == 1;
  }
  
  boolean d()
  {
    return this.a.size() > 0;
  }
  
  void e()
  {
    c();
    int j = this.a.size();
    int i = 0;
    if (i < j)
    {
      UpdateOp localUpdateOp = (UpdateOp)this.a.get(i);
      switch (localUpdateOp.a)
      {
      }
      for (;;)
      {
        if (this.d != null) {
          this.d.run();
        }
        i += 1;
        break;
        this.c.b(localUpdateOp);
        this.c.c(localUpdateOp.b, localUpdateOp.d);
        continue;
        this.c.b(localUpdateOp);
        this.c.a(localUpdateOp.b, localUpdateOp.d);
        continue;
        this.c.b(localUpdateOp);
        this.c.a(localUpdateOp.b, localUpdateOp.d, localUpdateOp.c);
        continue;
        this.c.b(localUpdateOp);
        this.c.d(localUpdateOp.b, localUpdateOp.d);
      }
    }
    a(this.a);
    this.h = 0;
  }
  
  boolean f()
  {
    return (!this.b.isEmpty()) && (!this.a.isEmpty());
  }
  
  static abstract interface Callback
  {
    public abstract RecyclerView.ViewHolder a(int paramInt);
    
    public abstract void a(int paramInt1, int paramInt2);
    
    public abstract void a(int paramInt1, int paramInt2, Object paramObject);
    
    public abstract void a(AdapterHelper.UpdateOp paramUpdateOp);
    
    public abstract void b(int paramInt1, int paramInt2);
    
    public abstract void b(AdapterHelper.UpdateOp paramUpdateOp);
    
    public abstract void c(int paramInt1, int paramInt2);
    
    public abstract void d(int paramInt1, int paramInt2);
  }
  
  static class UpdateOp
  {
    int a;
    int b;
    Object c;
    int d;
    
    UpdateOp(int paramInt1, int paramInt2, int paramInt3, Object paramObject)
    {
      this.a = paramInt1;
      this.b = paramInt2;
      this.d = paramInt3;
      this.c = paramObject;
    }
    
    String a()
    {
      switch (this.a)
      {
      case 3: 
      case 5: 
      case 6: 
      case 7: 
      default: 
        return "??";
      case 1: 
        return "add";
      case 2: 
        return "rm";
      case 4: 
        return "up";
      }
      return "mv";
    }
    
    public boolean equals(Object paramObject)
    {
      if (this == paramObject) {}
      do
      {
        do
        {
          do
          {
            return true;
            if ((paramObject == null) || (getClass() != paramObject.getClass())) {
              return false;
            }
            paramObject = (UpdateOp)paramObject;
            if (this.a != ((UpdateOp)paramObject).a) {
              return false;
            }
          } while ((this.a == 8) && (Math.abs(this.d - this.b) == 1) && (this.d == ((UpdateOp)paramObject).b) && (this.b == ((UpdateOp)paramObject).d));
          if (this.d != ((UpdateOp)paramObject).d) {
            return false;
          }
          if (this.b != ((UpdateOp)paramObject).b) {
            return false;
          }
          if (this.c == null) {
            break;
          }
        } while (this.c.equals(((UpdateOp)paramObject).c));
        return false;
      } while (((UpdateOp)paramObject).c == null);
      return false;
    }
    
    public int hashCode()
    {
      return (this.a * 31 + this.b) * 31 + this.d;
    }
    
    public String toString()
    {
      return Integer.toHexString(System.identityHashCode(this)) + "[" + a() + ",s:" + this.b + "c:" + this.d + ",p:" + this.c + "]";
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/AdapterHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */