package android.support.v7.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.database.Observable;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.SystemClock;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.os.TraceCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.NestedScrollingChild;
import android.support.v4.view.NestedScrollingChildHelper;
import android.support.v4.view.ScrollingView;
import android.support.v4.view.VelocityTrackerCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewConfigurationCompat;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.CollectionInfoCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.CollectionItemInfoCompat;
import android.support.v4.view.accessibility.AccessibilityRecordCompat;
import android.support.v4.widget.EdgeEffectCompat;
import android.support.v4.widget.ScrollerCompat;
import android.support.v7.recyclerview.R.styleable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.util.TypedValue;
import android.view.FocusFinder;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Interpolator;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RecyclerView
  extends ViewGroup
  implements NestedScrollingChild, ScrollingView
{
  static final boolean a;
  private static final Interpolator aq;
  private static final int[] k = { 16843830 };
  private static final boolean l;
  private static final Class<?>[] m;
  private int A = 0;
  private boolean B;
  private boolean C;
  private boolean D;
  private int E;
  private boolean F;
  private final boolean G;
  private final AccessibilityManager H;
  private List<OnChildAttachStateChangeListener> I;
  private boolean J = false;
  private int K = 0;
  private EdgeEffectCompat L;
  private EdgeEffectCompat M;
  private EdgeEffectCompat N;
  private EdgeEffectCompat O;
  private int P = 0;
  private int Q = -1;
  private VelocityTracker R;
  private int S;
  private int T;
  private int U;
  private int V;
  private int W;
  private final int aa;
  private final int ab;
  private float ac = Float.MIN_VALUE;
  private final ViewFlinger ad = new ViewFlinger();
  private OnScrollListener ae;
  private List<OnScrollListener> af;
  private RecyclerView.ItemAnimator.ItemAnimatorListener ag = new ItemAnimatorRestoreListener(null);
  private boolean ah = false;
  private RecyclerViewAccessibilityDelegate ai;
  private ChildDrawingOrderCallback aj;
  private final int[] ak = new int[2];
  private NestedScrollingChildHelper al;
  private final int[] am = new int[2];
  private final int[] an = new int[2];
  private final int[] ao = new int[2];
  private Runnable ap = new Runnable()
  {
    public void run()
    {
      if (RecyclerView.this.g != null) {
        RecyclerView.this.g.a();
      }
      RecyclerView.b(RecyclerView.this, false);
    }
  };
  private final ViewInfoStore.ProcessCallback ar = new ViewInfoStore.ProcessCallback()
  {
    public void a(RecyclerView.ViewHolder paramAnonymousViewHolder)
    {
      RecyclerView.this.f.a(paramAnonymousViewHolder.itemView, RecyclerView.this.b);
    }
    
    public void a(RecyclerView.ViewHolder paramAnonymousViewHolder, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo paramAnonymousItemHolderInfo1, @Nullable RecyclerView.ItemAnimator.ItemHolderInfo paramAnonymousItemHolderInfo2)
    {
      RecyclerView.this.b.d(paramAnonymousViewHolder);
      RecyclerView.a(RecyclerView.this, paramAnonymousViewHolder, paramAnonymousItemHolderInfo1, paramAnonymousItemHolderInfo2);
    }
    
    public void b(RecyclerView.ViewHolder paramAnonymousViewHolder, RecyclerView.ItemAnimator.ItemHolderInfo paramAnonymousItemHolderInfo1, RecyclerView.ItemAnimator.ItemHolderInfo paramAnonymousItemHolderInfo2)
    {
      RecyclerView.b(RecyclerView.this, paramAnonymousViewHolder, paramAnonymousItemHolderInfo1, paramAnonymousItemHolderInfo2);
    }
    
    public void c(RecyclerView.ViewHolder paramAnonymousViewHolder, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo paramAnonymousItemHolderInfo1, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo paramAnonymousItemHolderInfo2)
    {
      paramAnonymousViewHolder.setIsRecyclable(false);
      if (RecyclerView.d(RecyclerView.this)) {
        if (RecyclerView.this.g.a(paramAnonymousViewHolder, paramAnonymousViewHolder, paramAnonymousItemHolderInfo1, paramAnonymousItemHolderInfo2)) {
          RecyclerView.e(RecyclerView.this);
        }
      }
      while (!RecyclerView.this.g.c(paramAnonymousViewHolder, paramAnonymousItemHolderInfo1, paramAnonymousItemHolderInfo2)) {
        return;
      }
      RecyclerView.e(RecyclerView.this);
    }
  };
  final Recycler b = new Recycler();
  AdapterHelper c;
  ChildHelper d;
  final ViewInfoStore e = new ViewInfoStore();
  @VisibleForTesting
  LayoutManager f;
  ItemAnimator g = new DefaultItemAnimator();
  final State h = new State();
  boolean i = false;
  boolean j = false;
  private final RecyclerViewDataObserver n = new RecyclerViewDataObserver(null);
  private SavedState o;
  private boolean p;
  private final Runnable q = new Runnable()
  {
    public void run()
    {
      if ((!RecyclerView.a(RecyclerView.this)) || (RecyclerView.this.isLayoutRequested())) {
        return;
      }
      if (RecyclerView.b(RecyclerView.this))
      {
        RecyclerView.a(RecyclerView.this, true);
        return;
      }
      RecyclerView.c(RecyclerView.this);
    }
  };
  private final Rect r = new Rect();
  private Adapter s;
  private RecyclerListener t;
  private final ArrayList<ItemDecoration> u = new ArrayList();
  private final ArrayList<OnItemTouchListener> v = new ArrayList();
  private OnItemTouchListener w;
  private boolean x;
  private boolean y;
  private boolean z;
  
  static
  {
    if ((Build.VERSION.SDK_INT == 18) || (Build.VERSION.SDK_INT == 19) || (Build.VERSION.SDK_INT == 20))
    {
      bool = true;
      l = bool;
      if (Build.VERSION.SDK_INT < 23) {
        break label100;
      }
    }
    label100:
    for (boolean bool = true;; bool = false)
    {
      a = bool;
      m = new Class[] { Context.class, AttributeSet.class, Integer.TYPE, Integer.TYPE };
      aq = new Interpolator()
      {
        public float getInterpolation(float paramAnonymousFloat)
        {
          paramAnonymousFloat -= 1.0F;
          return paramAnonymousFloat * paramAnonymousFloat * paramAnonymousFloat * paramAnonymousFloat * paramAnonymousFloat + 1.0F;
        }
      };
      return;
      bool = false;
      break;
    }
  }
  
  public RecyclerView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public RecyclerView(Context paramContext, @Nullable AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public RecyclerView(Context paramContext, @Nullable AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    setScrollContainer(true);
    setFocusableInTouchMode(true);
    Object localObject;
    if (Build.VERSION.SDK_INT >= 16)
    {
      bool1 = true;
      this.G = bool1;
      localObject = ViewConfiguration.get(paramContext);
      this.W = ((ViewConfiguration)localObject).getScaledTouchSlop();
      this.aa = ((ViewConfiguration)localObject).getScaledMinimumFlingVelocity();
      this.ab = ((ViewConfiguration)localObject).getScaledMaximumFlingVelocity();
      if (ViewCompat.a(this) != 2) {
        break label467;
      }
    }
    label467:
    for (boolean bool1 = true;; bool1 = false)
    {
      setWillNotDraw(bool1);
      this.g.a(this.ag);
      a();
      s();
      if (ViewCompat.e(this) == 0) {
        ViewCompat.c(this, 1);
      }
      this.H = ((AccessibilityManager)getContext().getSystemService("accessibility"));
      setAccessibilityDelegateCompat(new RecyclerViewAccessibilityDelegate(this));
      boolean bool2 = true;
      bool1 = bool2;
      if (paramAttributeSet != null)
      {
        localObject = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.RecyclerView, paramInt, 0);
        String str = ((TypedArray)localObject).getString(R.styleable.RecyclerView_layoutManager);
        ((TypedArray)localObject).recycle();
        a(paramContext, str, paramAttributeSet, paramInt, 0);
        bool1 = bool2;
        if (Build.VERSION.SDK_INT >= 21)
        {
          paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, k, paramInt, 0);
          bool1 = paramContext.getBoolean(0, true);
          paramContext.recycle();
        }
      }
      setNestedScrollingEnabled(bool1);
      return;
      bool1 = false;
      break;
    }
  }
  
  private void A()
  {
    this.K -= 1;
    if (this.K < 1)
    {
      this.K = 0;
      B();
    }
  }
  
  private void B()
  {
    int i1 = this.E;
    this.E = 0;
    if ((i1 != 0) && (i()))
    {
      AccessibilityEvent localAccessibilityEvent = AccessibilityEvent.obtain();
      localAccessibilityEvent.setEventType(2048);
      AccessibilityEventCompat.a(localAccessibilityEvent, i1);
      sendAccessibilityEventUnchecked(localAccessibilityEvent);
    }
  }
  
  private void C()
  {
    if ((!this.ah) && (this.x))
    {
      ViewCompat.a(this, this.ap);
      this.ah = true;
    }
  }
  
  private boolean D()
  {
    return (this.g != null) && (this.f.c());
  }
  
  private void E()
  {
    boolean bool2 = true;
    if (this.J)
    {
      this.c.a();
      o();
      this.f.a(this);
    }
    int i1;
    label58:
    State localState;
    if (D())
    {
      this.c.b();
      if ((!this.i) && (!this.j)) {
        break label179;
      }
      i1 = 1;
      localState = this.h;
      if ((!this.z) || (this.g == null) || ((!this.J) && (i1 == 0) && (!LayoutManager.b(this.f))) || ((this.J) && (!this.s.hasStableIds()))) {
        break label184;
      }
      bool1 = true;
      label118:
      State.d(localState, bool1);
      localState = this.h;
      if ((!State.c(this.h)) || (i1 == 0) || (this.J) || (!D())) {
        break label189;
      }
    }
    label179:
    label184:
    label189:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      State.e(localState, bool1);
      return;
      this.c.e();
      break;
      i1 = 0;
      break label58;
      bool1 = false;
      break label118;
    }
  }
  
  private void F()
  {
    this.h.a(1);
    State.b(this.h, false);
    b();
    this.e.a();
    z();
    E();
    Object localObject = this.h;
    boolean bool;
    int i2;
    int i1;
    if ((State.c(this.h)) && (this.j))
    {
      bool = true;
      State.f((State)localObject, bool);
      this.j = false;
      this.i = false;
      State.c(this.h, State.b(this.h));
      this.h.a = this.s.getItemCount();
      a(this.ak);
      if (!State.c(this.h)) {
        break label294;
      }
      i2 = this.d.b();
      i1 = 0;
      label137:
      if (i1 >= i2) {
        break label294;
      }
      localObject = c(this.d.b(i1));
      if ((!((ViewHolder)localObject).shouldIgnore()) && ((!((ViewHolder)localObject).isInvalid()) || (this.s.hasStableIds()))) {
        break label194;
      }
    }
    label194:
    RecyclerView.ItemAnimator.ItemHolderInfo localItemHolderInfo;
    for (;;)
    {
      i1 += 1;
      break label137;
      bool = false;
      break;
      localItemHolderInfo = this.g.a(this.h, (ViewHolder)localObject, ItemAnimator.d((ViewHolder)localObject), ((ViewHolder)localObject).getUnmodifiedPayloads());
      this.e.a((ViewHolder)localObject, localItemHolderInfo);
      if ((State.d(this.h)) && (((ViewHolder)localObject).isUpdated()) && (!((ViewHolder)localObject).isRemoved()) && (!((ViewHolder)localObject).shouldIgnore()) && (!((ViewHolder)localObject).isInvalid()))
      {
        long l1 = a((ViewHolder)localObject);
        this.e.a(l1, (ViewHolder)localObject);
      }
    }
    label294:
    if (State.b(this.h))
    {
      m();
      bool = State.e(this.h);
      State.a(this.h, false);
      this.f.c(this.b, this.h);
      State.a(this.h, bool);
      i1 = 0;
      if (i1 < this.d.b())
      {
        localObject = c(this.d.b(i1));
        if (((ViewHolder)localObject).shouldIgnore()) {}
        for (;;)
        {
          i1 += 1;
          break;
          if (!this.e.d((ViewHolder)localObject))
          {
            int i3 = ItemAnimator.d((ViewHolder)localObject);
            bool = ((ViewHolder)localObject).hasAnyOfTheFlags(8192);
            i2 = i3;
            if (!bool) {
              i2 = i3 | 0x1000;
            }
            localItemHolderInfo = this.g.a(this.h, (ViewHolder)localObject, i2, ((ViewHolder)localObject).getUnmodifiedPayloads());
            if (bool) {
              a((ViewHolder)localObject, localItemHolderInfo);
            } else {
              this.e.b((ViewHolder)localObject, localItemHolderInfo);
            }
          }
        }
      }
      n();
    }
    for (;;)
    {
      A();
      a(false);
      State.b(this.h, 2);
      return;
      n();
    }
  }
  
  private void G()
  {
    b();
    z();
    this.h.a(6);
    this.c.e();
    this.h.a = this.s.getItemCount();
    State.c(this.h, 0);
    State.c(this.h, false);
    this.f.c(this.b, this.h);
    State.a(this.h, false);
    this.o = null;
    State localState = this.h;
    if ((State.c(this.h)) && (this.g != null)) {}
    for (boolean bool = true;; bool = false)
    {
      State.d(localState, bool);
      State.b(this.h, 4);
      A();
      a(false);
      return;
    }
  }
  
  private void H()
  {
    this.h.a(4);
    b();
    z();
    State.b(this.h, 1);
    if (State.c(this.h))
    {
      int i1 = this.d.b() - 1;
      if (i1 >= 0)
      {
        ViewHolder localViewHolder1 = c(this.d.b(i1));
        if (localViewHolder1.shouldIgnore()) {}
        for (;;)
        {
          i1 -= 1;
          break;
          long l1 = a(localViewHolder1);
          RecyclerView.ItemAnimator.ItemHolderInfo localItemHolderInfo2 = this.g.a(this.h, localViewHolder1);
          ViewHolder localViewHolder2 = this.e.a(l1);
          if ((localViewHolder2 != null) && (!localViewHolder2.shouldIgnore()))
          {
            boolean bool1 = this.e.a(localViewHolder2);
            boolean bool2 = this.e.a(localViewHolder1);
            if ((bool1) && (localViewHolder2 == localViewHolder1))
            {
              this.e.c(localViewHolder1, localItemHolderInfo2);
            }
            else
            {
              RecyclerView.ItemAnimator.ItemHolderInfo localItemHolderInfo1 = this.e.b(localViewHolder2);
              this.e.c(localViewHolder1, localItemHolderInfo2);
              localItemHolderInfo2 = this.e.c(localViewHolder1);
              if (localItemHolderInfo1 == null) {
                a(l1, localViewHolder1, localViewHolder2);
              } else {
                a(localViewHolder2, localViewHolder1, localItemHolderInfo1, localItemHolderInfo2, bool1, bool2);
              }
            }
          }
          else
          {
            this.e.c(localViewHolder1, localItemHolderInfo2);
          }
        }
      }
      this.e.a(this.ar);
    }
    this.f.b(this.b);
    State.d(this.h, this.h.a);
    this.J = false;
    State.d(this.h, false);
    State.e(this.h, false);
    LayoutManager.a(this.f, false);
    if (Recycler.a(this.b) != null) {
      Recycler.a(this.b).clear();
    }
    A();
    a(false);
    this.e.a();
    if (j(this.ak[0], this.ak[1])) {
      h(0, 0);
    }
  }
  
  private void I()
  {
    if (this.J) {
      return;
    }
    this.J = true;
    int i2 = this.d.c();
    int i1 = 0;
    while (i1 < i2)
    {
      ViewHolder localViewHolder = c(this.d.c(i1));
      if ((localViewHolder != null) && (!localViewHolder.shouldIgnore())) {
        localViewHolder.addFlags(512);
      }
      i1 += 1;
    }
    this.b.g();
  }
  
  private void J()
  {
    int i2 = this.d.b();
    int i1 = 0;
    while (i1 < i2)
    {
      View localView = this.d.b(i1);
      Object localObject = a(localView);
      if ((localObject != null) && (((ViewHolder)localObject).mShadowingHolder != null))
      {
        localObject = ((ViewHolder)localObject).mShadowingHolder.itemView;
        int i3 = localView.getLeft();
        int i4 = localView.getTop();
        if ((i3 != ((View)localObject).getLeft()) || (i4 != ((View)localObject).getTop())) {
          ((View)localObject).layout(i3, i4, ((View)localObject).getWidth() + i3, ((View)localObject).getHeight() + i4);
        }
      }
      i1 += 1;
    }
  }
  
  private String a(Context paramContext, String paramString)
  {
    if (paramString.charAt(0) == '.') {
      paramContext = paramContext.getPackageName() + paramString;
    }
    do
    {
      return paramContext;
      paramContext = paramString;
    } while (paramString.contains("."));
    return RecyclerView.class.getPackage().getName() + '.' + paramString;
  }
  
  private void a(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    int i2 = 0;
    int i1;
    if (paramFloat2 < 0.0F)
    {
      d();
      i1 = i2;
      if (this.L.a(-paramFloat2 / getWidth(), 1.0F - paramFloat3 / getHeight())) {
        i1 = 1;
      }
      if (paramFloat4 >= 0.0F) {
        break label162;
      }
      f();
      i2 = i1;
      if (this.M.a(-paramFloat4 / getHeight(), paramFloat1 / getWidth())) {
        i2 = 1;
      }
    }
    for (;;)
    {
      if ((i2 != 0) || (paramFloat2 != 0.0F) || (paramFloat4 != 0.0F)) {
        ViewCompat.d(this);
      }
      return;
      i1 = i2;
      if (paramFloat2 <= 0.0F) {
        break;
      }
      e();
      i1 = i2;
      if (!this.N.a(paramFloat2 / getWidth(), paramFloat3 / getHeight())) {
        break;
      }
      i1 = 1;
      break;
      label162:
      i2 = i1;
      if (paramFloat4 > 0.0F)
      {
        g();
        i2 = i1;
        if (this.O.a(paramFloat4 / getHeight(), 1.0F - paramFloat1 / getWidth())) {
          i2 = 1;
        }
      }
    }
  }
  
  private void a(long paramLong, ViewHolder paramViewHolder1, ViewHolder paramViewHolder2)
  {
    int i2 = this.d.b();
    int i1 = 0;
    if (i1 < i2)
    {
      ViewHolder localViewHolder = c(this.d.b(i1));
      if (localViewHolder == paramViewHolder1) {}
      while (a(localViewHolder) != paramLong)
      {
        i1 += 1;
        break;
      }
      if ((this.s != null) && (this.s.hasStableIds())) {
        throw new IllegalStateException("Two different ViewHolders have the same stable ID. Stable IDs in your adapter MUST BE unique and SHOULD NOT change.\n ViewHolder 1:" + localViewHolder + " \n View Holder 2:" + paramViewHolder1);
      }
      throw new IllegalStateException("Two different ViewHolders have the same change ID. This might happen due to inconsistent Adapter update events or if the LayoutManager lays out the same View multiple times.\n ViewHolder 1:" + localViewHolder + " \n View Holder 2:" + paramViewHolder1);
    }
    Log.e("RecyclerView", "Problem while matching changed view holders with the newones. The pre-layout information for the change holder " + paramViewHolder2 + " cannot be found but it is necessary for " + paramViewHolder1);
  }
  
  private void a(Context paramContext, String paramString, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
  {
    if (paramString != null)
    {
      paramString = paramString.trim();
      if (paramString.length() != 0)
      {
        String str = a(paramContext, paramString);
        try
        {
          if (isInEditMode()) {}
          Class localClass;
          for (paramString = getClass().getClassLoader();; paramString = paramContext.getClassLoader())
          {
            localClass = paramString.loadClass(str).asSubclass(LayoutManager.class);
            paramString = null;
            try
            {
              Constructor localConstructor = localClass.getConstructor(m);
              paramString = new Object[] { paramContext, paramAttributeSet, Integer.valueOf(paramInt1), Integer.valueOf(paramInt2) };
              paramContext = localConstructor;
            }
            catch (NoSuchMethodException localNoSuchMethodException)
            {
              try
              {
                paramContext = localClass.getConstructor(new Class[0]);
              }
              catch (NoSuchMethodException paramContext)
              {
                paramContext.initCause(localNoSuchMethodException);
                throw new IllegalStateException(paramAttributeSet.getPositionDescription() + ": Error creating LayoutManager " + str, paramContext);
              }
            }
            paramContext.setAccessible(true);
            setLayoutManager((LayoutManager)paramContext.newInstance(paramString));
            return;
          }
          return;
        }
        catch (ClassNotFoundException paramContext)
        {
          throw new IllegalStateException(paramAttributeSet.getPositionDescription() + ": Unable to find LayoutManager " + str, paramContext);
        }
        catch (InvocationTargetException paramContext)
        {
          throw new IllegalStateException(paramAttributeSet.getPositionDescription() + ": Could not instantiate the LayoutManager: " + str, paramContext);
        }
        catch (InstantiationException paramContext)
        {
          throw new IllegalStateException(paramAttributeSet.getPositionDescription() + ": Could not instantiate the LayoutManager: " + str, paramContext);
        }
        catch (IllegalAccessException paramContext)
        {
          throw new IllegalStateException(paramAttributeSet.getPositionDescription() + ": Cannot access non-public constructor " + str, paramContext);
        }
        catch (ClassCastException paramContext)
        {
          throw new IllegalStateException(paramAttributeSet.getPositionDescription() + ": Class is not a LayoutManager " + str, paramContext);
        }
      }
    }
  }
  
  private void a(Adapter paramAdapter, boolean paramBoolean1, boolean paramBoolean2)
  {
    if (this.s != null)
    {
      this.s.unregisterAdapterDataObserver(this.n);
      this.s.onDetachedFromRecyclerView(this);
    }
    if ((!paramBoolean1) || (paramBoolean2))
    {
      if (this.g != null) {
        this.g.c();
      }
      if (this.f != null)
      {
        this.f.c(this.b);
        this.f.b(this.b);
      }
      this.b.a();
    }
    this.c.a();
    Adapter localAdapter = this.s;
    this.s = paramAdapter;
    if (paramAdapter != null)
    {
      paramAdapter.registerAdapterDataObserver(this.n);
      paramAdapter.onAttachedToRecyclerView(this);
    }
    if (this.f != null) {
      this.f.a(localAdapter, this.s);
    }
    this.b.a(localAdapter, this.s, paramBoolean1);
    State.a(this.h, true);
    o();
  }
  
  private void a(ViewHolder paramViewHolder, RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo)
  {
    paramViewHolder.setFlags(0, 8192);
    if ((State.d(this.h)) && (paramViewHolder.isUpdated()) && (!paramViewHolder.isRemoved()) && (!paramViewHolder.shouldIgnore()))
    {
      long l1 = a(paramViewHolder);
      this.e.a(l1, paramViewHolder);
    }
    this.e.a(paramViewHolder, paramItemHolderInfo);
  }
  
  private void a(@NonNull ViewHolder paramViewHolder, @Nullable RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo1, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo2)
  {
    paramViewHolder.setIsRecyclable(false);
    if (this.g.b(paramViewHolder, paramItemHolderInfo1, paramItemHolderInfo2)) {
      C();
    }
  }
  
  private void a(@NonNull ViewHolder paramViewHolder1, @NonNull ViewHolder paramViewHolder2, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo1, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo2, boolean paramBoolean1, boolean paramBoolean2)
  {
    paramViewHolder1.setIsRecyclable(false);
    if (paramBoolean1) {
      b(paramViewHolder1);
    }
    if (paramViewHolder1 != paramViewHolder2)
    {
      if (paramBoolean2) {
        b(paramViewHolder2);
      }
      paramViewHolder1.mShadowedHolder = paramViewHolder2;
      b(paramViewHolder1);
      this.b.d(paramViewHolder1);
      paramViewHolder2.setIsRecyclable(false);
      paramViewHolder2.mShadowingHolder = paramViewHolder1;
    }
    if (this.g.a(paramViewHolder1, paramViewHolder2, paramItemHolderInfo1, paramItemHolderInfo2)) {
      C();
    }
  }
  
  private void a(int[] paramArrayOfInt)
  {
    int i7 = this.d.b();
    if (i7 == 0)
    {
      paramArrayOfInt[0] = 0;
      paramArrayOfInt[1] = 0;
      return;
    }
    int i2 = Integer.MAX_VALUE;
    int i4 = Integer.MIN_VALUE;
    int i3 = 0;
    if (i3 < i7)
    {
      ViewHolder localViewHolder = c(this.d.b(i3));
      int i6;
      if (localViewHolder.shouldIgnore())
      {
        i6 = i2;
        i2 = i4;
      }
      for (;;)
      {
        i3 += 1;
        i4 = i2;
        i2 = i6;
        break;
        int i5 = localViewHolder.getLayoutPosition();
        int i1 = i2;
        if (i5 < i2) {
          i1 = i5;
        }
        i2 = i4;
        i6 = i1;
        if (i5 > i4)
        {
          i2 = i5;
          i6 = i1;
        }
      }
    }
    paramArrayOfInt[0] = i2;
    paramArrayOfInt[1] = i4;
  }
  
  private boolean a(MotionEvent paramMotionEvent)
  {
    int i2 = paramMotionEvent.getAction();
    if ((i2 == 3) || (i2 == 0)) {
      this.w = null;
    }
    int i3 = this.v.size();
    int i1 = 0;
    while (i1 < i3)
    {
      OnItemTouchListener localOnItemTouchListener = (OnItemTouchListener)this.v.get(i1);
      if ((localOnItemTouchListener.a(this, paramMotionEvent)) && (i2 != 3))
      {
        this.w = localOnItemTouchListener;
        return true;
      }
      i1 += 1;
    }
    return false;
  }
  
  private void b(ViewHolder paramViewHolder)
  {
    View localView = paramViewHolder.itemView;
    if (localView.getParent() == this) {}
    for (int i1 = 1;; i1 = 0)
    {
      this.b.d(a(localView));
      if (!paramViewHolder.isTmpDetached()) {
        break;
      }
      this.d.a(localView, -1, localView.getLayoutParams(), true);
      return;
    }
    if (i1 == 0)
    {
      this.d.a(localView, true);
      return;
    }
    this.d.d(localView);
  }
  
  private void b(@NonNull ViewHolder paramViewHolder, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo1, @Nullable RecyclerView.ItemAnimator.ItemHolderInfo paramItemHolderInfo2)
  {
    b(paramViewHolder);
    paramViewHolder.setIsRecyclable(false);
    if (this.g.a(paramViewHolder, paramItemHolderInfo1, paramItemHolderInfo2)) {
      C();
    }
  }
  
  private boolean b(MotionEvent paramMotionEvent)
  {
    int i1 = paramMotionEvent.getAction();
    int i2;
    if (this.w != null)
    {
      if (i1 == 0) {
        this.w = null;
      }
    }
    else
    {
      if (i1 == 0) {
        break label108;
      }
      i2 = this.v.size();
      i1 = 0;
    }
    while (i1 < i2)
    {
      OnItemTouchListener localOnItemTouchListener = (OnItemTouchListener)this.v.get(i1);
      if (localOnItemTouchListener.a(this, paramMotionEvent))
      {
        this.w = localOnItemTouchListener;
        do
        {
          return true;
          this.w.b(this, paramMotionEvent);
        } while ((i1 != 3) && (i1 != 1));
        this.w = null;
        return true;
      }
      i1 += 1;
    }
    label108:
    return false;
  }
  
  static ViewHolder c(View paramView)
  {
    if (paramView == null) {
      return null;
    }
    return ((LayoutParams)paramView.getLayoutParams()).a;
  }
  
  private void c(MotionEvent paramMotionEvent)
  {
    int i1 = MotionEventCompat.b(paramMotionEvent);
    if (MotionEventCompat.b(paramMotionEvent, i1) == this.Q) {
      if (i1 != 0) {
        break label75;
      }
    }
    label75:
    for (i1 = 1;; i1 = 0)
    {
      this.Q = MotionEventCompat.b(paramMotionEvent, i1);
      int i2 = (int)(MotionEventCompat.c(paramMotionEvent, i1) + 0.5F);
      this.U = i2;
      this.S = i2;
      i1 = (int)(MotionEventCompat.d(paramMotionEvent, i1) + 0.5F);
      this.V = i1;
      this.T = i1;
      return;
    }
  }
  
  private boolean c(ViewHolder paramViewHolder)
  {
    return (this.g == null) || (this.g.a(paramViewHolder, paramViewHolder.getUnmodifiedPayloads()));
  }
  
  private int d(ViewHolder paramViewHolder)
  {
    if ((paramViewHolder.hasAnyOfTheFlags(524)) || (!paramViewHolder.isBound())) {
      return -1;
    }
    return this.c.c(paramViewHolder.mPosition);
  }
  
  private float getScrollFactor()
  {
    if (this.ac == Float.MIN_VALUE)
    {
      TypedValue localTypedValue = new TypedValue();
      if (getContext().getTheme().resolveAttribute(16842829, localTypedValue, true)) {
        this.ac = localTypedValue.getDimension(getContext().getResources().getDisplayMetrics());
      }
    }
    else
    {
      return this.ac;
    }
    return 0.0F;
  }
  
  private NestedScrollingChildHelper getScrollingChildHelper()
  {
    if (this.al == null) {
      this.al = new NestedScrollingChildHelper(this);
    }
    return this.al;
  }
  
  private void h(int paramInt)
  {
    if (this.f == null) {
      return;
    }
    this.f.e(paramInt);
    awakenScrollBars();
  }
  
  private void i(int paramInt1, int paramInt2)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (this.L != null)
    {
      bool1 = bool2;
      if (!this.L.a())
      {
        bool1 = bool2;
        if (paramInt1 > 0) {
          bool1 = this.L.c();
        }
      }
    }
    bool2 = bool1;
    if (this.N != null)
    {
      bool2 = bool1;
      if (!this.N.a())
      {
        bool2 = bool1;
        if (paramInt1 < 0) {
          bool2 = bool1 | this.N.c();
        }
      }
    }
    bool1 = bool2;
    if (this.M != null)
    {
      bool1 = bool2;
      if (!this.M.a())
      {
        bool1 = bool2;
        if (paramInt2 > 0) {
          bool1 = bool2 | this.M.c();
        }
      }
    }
    bool2 = bool1;
    if (this.O != null)
    {
      bool2 = bool1;
      if (!this.O.a())
      {
        bool2 = bool1;
        if (paramInt2 < 0) {
          bool2 = bool1 | this.O.c();
        }
      }
    }
    if (bool2) {
      ViewCompat.d(this);
    }
  }
  
  private boolean j(int paramInt1, int paramInt2)
  {
    boolean bool = false;
    if (this.d.b() == 0) {
      if ((paramInt1 != 0) || (paramInt2 != 0)) {
        bool = true;
      }
    }
    do
    {
      return bool;
      a(this.ak);
    } while ((this.ak[0] == paramInt1) && (this.ak[1] == paramInt2));
    return true;
  }
  
  private boolean j(View paramView)
  {
    b();
    boolean bool2 = this.d.f(paramView);
    if (bool2)
    {
      paramView = c(paramView);
      this.b.d(paramView);
      this.b.b(paramView);
    }
    if (!bool2) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      a(bool1);
      return bool2;
    }
  }
  
  private void k(View paramView)
  {
    ViewHolder localViewHolder = c(paramView);
    h(paramView);
    if ((this.s != null) && (localViewHolder != null)) {
      this.s.onViewDetachedFromWindow(localViewHolder);
    }
    if (this.I != null)
    {
      int i1 = this.I.size() - 1;
      while (i1 >= 0)
      {
        ((OnChildAttachStateChangeListener)this.I.get(i1)).b(paramView);
        i1 -= 1;
      }
    }
  }
  
  private void l(View paramView)
  {
    ViewHolder localViewHolder = c(paramView);
    g(paramView);
    if ((this.s != null) && (localViewHolder != null)) {
      this.s.onViewAttachedToWindow(localViewHolder);
    }
    if (this.I != null)
    {
      int i1 = this.I.size() - 1;
      while (i1 >= 0)
      {
        ((OnChildAttachStateChangeListener)this.I.get(i1)).a(paramView);
        i1 -= 1;
      }
    }
  }
  
  private void s()
  {
    this.d = new ChildHelper(new ChildHelper.Callback()
    {
      public int a()
      {
        return RecyclerView.this.getChildCount();
      }
      
      public int a(View paramAnonymousView)
      {
        return RecyclerView.this.indexOfChild(paramAnonymousView);
      }
      
      public void a(int paramAnonymousInt)
      {
        View localView = RecyclerView.this.getChildAt(paramAnonymousInt);
        if (localView != null) {
          RecyclerView.b(RecyclerView.this, localView);
        }
        RecyclerView.this.removeViewAt(paramAnonymousInt);
      }
      
      public void a(View paramAnonymousView, int paramAnonymousInt)
      {
        RecyclerView.this.addView(paramAnonymousView, paramAnonymousInt);
        RecyclerView.a(RecyclerView.this, paramAnonymousView);
      }
      
      public void a(View paramAnonymousView, int paramAnonymousInt, ViewGroup.LayoutParams paramAnonymousLayoutParams)
      {
        RecyclerView.ViewHolder localViewHolder = RecyclerView.c(paramAnonymousView);
        if (localViewHolder != null)
        {
          if ((!localViewHolder.isTmpDetached()) && (!localViewHolder.shouldIgnore())) {
            throw new IllegalArgumentException("Called attach on a child which is not detached: " + localViewHolder);
          }
          localViewHolder.clearTmpDetachFlag();
        }
        RecyclerView.a(RecyclerView.this, paramAnonymousView, paramAnonymousInt, paramAnonymousLayoutParams);
      }
      
      public RecyclerView.ViewHolder b(View paramAnonymousView)
      {
        return RecyclerView.c(paramAnonymousView);
      }
      
      public View b(int paramAnonymousInt)
      {
        return RecyclerView.this.getChildAt(paramAnonymousInt);
      }
      
      public void b()
      {
        int j = a();
        int i = 0;
        while (i < j)
        {
          RecyclerView.b(RecyclerView.this, b(i));
          i += 1;
        }
        RecyclerView.this.removeAllViews();
      }
      
      public void c(int paramAnonymousInt)
      {
        Object localObject = b(paramAnonymousInt);
        if (localObject != null)
        {
          localObject = RecyclerView.c((View)localObject);
          if (localObject != null)
          {
            if ((((RecyclerView.ViewHolder)localObject).isTmpDetached()) && (!((RecyclerView.ViewHolder)localObject).shouldIgnore())) {
              throw new IllegalArgumentException("called detach on an already detached child " + localObject);
            }
            ((RecyclerView.ViewHolder)localObject).addFlags(256);
          }
        }
        RecyclerView.a(RecyclerView.this, paramAnonymousInt);
      }
      
      public void c(View paramAnonymousView)
      {
        paramAnonymousView = RecyclerView.c(paramAnonymousView);
        if (paramAnonymousView != null) {
          RecyclerView.ViewHolder.access$1500(paramAnonymousView);
        }
      }
      
      public void d(View paramAnonymousView)
      {
        paramAnonymousView = RecyclerView.c(paramAnonymousView);
        if (paramAnonymousView != null) {
          RecyclerView.ViewHolder.access$1600(paramAnonymousView);
        }
      }
    });
  }
  
  private void setScrollState(int paramInt)
  {
    if (paramInt == this.P) {
      return;
    }
    this.P = paramInt;
    if (paramInt != 2) {
      v();
    }
    g(paramInt);
  }
  
  private void t()
  {
    if (!this.z) {}
    label106:
    do
    {
      do
      {
        return;
        if (this.J)
        {
          TraceCompat.a("RV FullInvalidate");
          k();
          TraceCompat.a();
          return;
        }
      } while (!this.c.d());
      if ((this.c.a(4)) && (!this.c.a(11)))
      {
        TraceCompat.a("RV PartialInvalidate");
        b();
        this.c.b();
        if (!this.B)
        {
          if (!u()) {
            break label106;
          }
          k();
        }
        for (;;)
        {
          a(true);
          TraceCompat.a();
          return;
          this.c.c();
        }
      }
    } while (!this.c.d());
    TraceCompat.a("RV FullInvalidate");
    k();
    TraceCompat.a();
  }
  
  private boolean u()
  {
    int i2 = this.d.b();
    int i1 = 0;
    if (i1 < i2)
    {
      ViewHolder localViewHolder = c(this.d.b(i1));
      if ((localViewHolder == null) || (localViewHolder.shouldIgnore())) {}
      while (!localViewHolder.isUpdated())
      {
        i1 += 1;
        break;
      }
      return true;
    }
    return false;
  }
  
  private void v()
  {
    this.ad.b();
    if (this.f != null) {
      this.f.H();
    }
  }
  
  private void w()
  {
    boolean bool2 = false;
    if (this.L != null) {
      bool2 = this.L.c();
    }
    boolean bool1 = bool2;
    if (this.M != null) {
      bool1 = bool2 | this.M.c();
    }
    bool2 = bool1;
    if (this.N != null) {
      bool2 = bool1 | this.N.c();
    }
    bool1 = bool2;
    if (this.O != null) {
      bool1 = bool2 | this.O.c();
    }
    if (bool1) {
      ViewCompat.d(this);
    }
  }
  
  private void x()
  {
    if (this.R != null) {
      this.R.clear();
    }
    stopNestedScroll();
    w();
  }
  
  private void y()
  {
    x();
    setScrollState(0);
  }
  
  private void z()
  {
    this.K += 1;
  }
  
  long a(ViewHolder paramViewHolder)
  {
    if (this.s.hasStableIds()) {
      return paramViewHolder.getItemId();
    }
    return paramViewHolder.mPosition;
  }
  
  ViewHolder a(int paramInt, boolean paramBoolean)
  {
    int i2 = this.d.c();
    int i1 = 0;
    while (i1 < i2)
    {
      ViewHolder localViewHolder = c(this.d.c(i1));
      if ((localViewHolder != null) && (!localViewHolder.isRemoved())) {
        if (paramBoolean)
        {
          if (localViewHolder.mPosition != paramInt) {}
        }
        else {
          while (localViewHolder.getLayoutPosition() == paramInt) {
            return localViewHolder;
          }
        }
      }
      i1 += 1;
    }
    return null;
  }
  
  public ViewHolder a(View paramView)
  {
    ViewParent localViewParent = paramView.getParent();
    if ((localViewParent != null) && (localViewParent != this)) {
      throw new IllegalArgumentException("View " + paramView + " is not a direct child of " + this);
    }
    return c(paramView);
  }
  
  public View a(float paramFloat1, float paramFloat2)
  {
    int i1 = this.d.b() - 1;
    while (i1 >= 0)
    {
      View localView = this.d.b(i1);
      float f1 = ViewCompat.o(localView);
      float f2 = ViewCompat.p(localView);
      if ((paramFloat1 >= localView.getLeft() + f1) && (paramFloat1 <= localView.getRight() + f1) && (paramFloat2 >= localView.getTop() + f2) && (paramFloat2 <= localView.getBottom() + f2)) {
        return localView;
      }
      i1 -= 1;
    }
    return null;
  }
  
  void a()
  {
    this.c = new AdapterHelper(new AdapterHelper.Callback()
    {
      public RecyclerView.ViewHolder a(int paramAnonymousInt)
      {
        RecyclerView.ViewHolder localViewHolder2 = RecyclerView.this.a(paramAnonymousInt, true);
        RecyclerView.ViewHolder localViewHolder1;
        if (localViewHolder2 == null) {
          localViewHolder1 = null;
        }
        do
        {
          return localViewHolder1;
          localViewHolder1 = localViewHolder2;
        } while (!RecyclerView.this.d.c(localViewHolder2.itemView));
        return null;
      }
      
      public void a(int paramAnonymousInt1, int paramAnonymousInt2)
      {
        RecyclerView.this.a(paramAnonymousInt1, paramAnonymousInt2, true);
        RecyclerView.this.i = true;
        RecyclerView.State.a(RecyclerView.this.h, paramAnonymousInt2);
      }
      
      public void a(int paramAnonymousInt1, int paramAnonymousInt2, Object paramAnonymousObject)
      {
        RecyclerView.this.a(paramAnonymousInt1, paramAnonymousInt2, paramAnonymousObject);
        RecyclerView.this.j = true;
      }
      
      public void a(AdapterHelper.UpdateOp paramAnonymousUpdateOp)
      {
        c(paramAnonymousUpdateOp);
      }
      
      public void b(int paramAnonymousInt1, int paramAnonymousInt2)
      {
        RecyclerView.this.a(paramAnonymousInt1, paramAnonymousInt2, false);
        RecyclerView.this.i = true;
      }
      
      public void b(AdapterHelper.UpdateOp paramAnonymousUpdateOp)
      {
        c(paramAnonymousUpdateOp);
      }
      
      public void c(int paramAnonymousInt1, int paramAnonymousInt2)
      {
        RecyclerView.this.f(paramAnonymousInt1, paramAnonymousInt2);
        RecyclerView.this.i = true;
      }
      
      void c(AdapterHelper.UpdateOp paramAnonymousUpdateOp)
      {
        switch (paramAnonymousUpdateOp.a)
        {
        case 3: 
        case 5: 
        case 6: 
        case 7: 
        default: 
          return;
        case 1: 
          RecyclerView.this.f.a(RecyclerView.this, paramAnonymousUpdateOp.b, paramAnonymousUpdateOp.d);
          return;
        case 2: 
          RecyclerView.this.f.b(RecyclerView.this, paramAnonymousUpdateOp.b, paramAnonymousUpdateOp.d);
          return;
        case 4: 
          RecyclerView.this.f.a(RecyclerView.this, paramAnonymousUpdateOp.b, paramAnonymousUpdateOp.d, paramAnonymousUpdateOp.c);
          return;
        }
        RecyclerView.this.f.a(RecyclerView.this, paramAnonymousUpdateOp.b, paramAnonymousUpdateOp.d, 1);
      }
      
      public void d(int paramAnonymousInt1, int paramAnonymousInt2)
      {
        RecyclerView.this.e(paramAnonymousInt1, paramAnonymousInt2);
        RecyclerView.this.i = true;
      }
    });
  }
  
  public void a(int paramInt)
  {
    if (this.C) {
      return;
    }
    c();
    if (this.f == null)
    {
      Log.e("RecyclerView", "Cannot scroll to position a LayoutManager set. Call setLayoutManager with a non-null argument.");
      return;
    }
    this.f.e(paramInt);
    awakenScrollBars();
  }
  
  public void a(int paramInt1, int paramInt2)
  {
    if (this.f == null) {}
    do
    {
      Log.e("RecyclerView", "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
      do
      {
        return;
      } while (this.C);
      if (!this.f.e()) {
        paramInt1 = 0;
      }
      if (!this.f.f()) {
        paramInt2 = 0;
      }
    } while ((paramInt1 == 0) && (paramInt2 == 0));
    this.ad.b(paramInt1, paramInt2);
  }
  
  void a(int paramInt1, int paramInt2, Object paramObject)
  {
    int i2 = this.d.c();
    int i1 = 0;
    if (i1 < i2)
    {
      View localView = this.d.c(i1);
      ViewHolder localViewHolder = c(localView);
      if ((localViewHolder == null) || (localViewHolder.shouldIgnore())) {}
      for (;;)
      {
        i1 += 1;
        break;
        if ((localViewHolder.mPosition >= paramInt1) && (localViewHolder.mPosition < paramInt1 + paramInt2))
        {
          localViewHolder.addFlags(2);
          localViewHolder.addChangePayload(paramObject);
          ((LayoutParams)localView.getLayoutParams()).c = true;
        }
      }
    }
    this.b.c(paramInt1, paramInt2);
  }
  
  void a(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    int i2 = this.d.c();
    int i1 = 0;
    if (i1 < i2)
    {
      ViewHolder localViewHolder = c(this.d.c(i1));
      if ((localViewHolder != null) && (!localViewHolder.shouldIgnore()))
      {
        if (localViewHolder.mPosition < paramInt1 + paramInt2) {
          break label83;
        }
        localViewHolder.offsetPosition(-paramInt2, paramBoolean);
        State.a(this.h, true);
      }
      for (;;)
      {
        i1 += 1;
        break;
        label83:
        if (localViewHolder.mPosition >= paramInt1)
        {
          localViewHolder.flagRemovedAndOffsetPosition(paramInt1 - 1, -paramInt2, paramBoolean);
          State.a(this.h, true);
        }
      }
    }
    this.b.b(paramInt1, paramInt2, paramBoolean);
    requestLayout();
  }
  
  public void a(ItemDecoration paramItemDecoration)
  {
    a(paramItemDecoration, -1);
  }
  
  public void a(ItemDecoration paramItemDecoration, int paramInt)
  {
    if (this.f != null) {
      this.f.a("Cannot add item decoration during a scroll  or layout");
    }
    if (this.u.isEmpty()) {
      setWillNotDraw(false);
    }
    if (paramInt < 0) {
      this.u.add(paramItemDecoration);
    }
    for (;;)
    {
      l();
      requestLayout();
      return;
      this.u.add(paramInt, paramItemDecoration);
    }
  }
  
  public void a(OnItemTouchListener paramOnItemTouchListener)
  {
    this.v.add(paramOnItemTouchListener);
  }
  
  public void a(OnScrollListener paramOnScrollListener)
  {
    if (this.af == null) {
      this.af = new ArrayList();
    }
    this.af.add(paramOnScrollListener);
  }
  
  void a(String paramString)
  {
    if (j())
    {
      if (paramString == null) {
        throw new IllegalStateException("Cannot call this method while RecyclerView is computing a layout or scrolling");
      }
      throw new IllegalStateException(paramString);
    }
  }
  
  void a(boolean paramBoolean)
  {
    if (this.A < 1) {
      this.A = 1;
    }
    if (!paramBoolean) {
      this.B = false;
    }
    if (this.A == 1)
    {
      if ((paramBoolean) && (this.B) && (!this.C) && (this.f != null) && (this.s != null)) {
        k();
      }
      if (!this.C) {
        this.B = false;
      }
    }
    this.A -= 1;
  }
  
  boolean a(int paramInt1, int paramInt2, MotionEvent paramMotionEvent)
  {
    int i2 = 0;
    int i7 = 0;
    int i4 = 0;
    int i5 = 0;
    int i1 = 0;
    int i8 = 0;
    int i3 = 0;
    int i6 = 0;
    t();
    if (this.s != null)
    {
      b();
      z();
      TraceCompat.a("RV Scroll");
      i1 = i8;
      i2 = i7;
      if (paramInt1 != 0)
      {
        i1 = this.f.a(paramInt1, this.b, this.h);
        i2 = paramInt1 - i1;
      }
      i3 = i6;
      i4 = i5;
      if (paramInt2 != 0)
      {
        i3 = this.f.b(paramInt2, this.b, this.h);
        i4 = paramInt2 - i3;
      }
      TraceCompat.a();
      J();
      A();
      a(false);
    }
    if (!this.u.isEmpty()) {
      invalidate();
    }
    if (dispatchNestedScroll(i1, i3, i2, i4, this.am))
    {
      this.U -= this.am[0];
      this.V -= this.am[1];
      if (paramMotionEvent != null) {
        paramMotionEvent.offsetLocation(this.am[0], this.am[1]);
      }
      paramMotionEvent = this.ao;
      paramMotionEvent[0] += this.am[0];
      paramMotionEvent = this.ao;
      paramMotionEvent[1] += this.am[1];
    }
    for (;;)
    {
      if ((i1 != 0) || (i3 != 0)) {
        h(i1, i3);
      }
      if (!awakenScrollBars()) {
        invalidate();
      }
      if ((i1 == 0) && (i3 == 0)) {
        break;
      }
      return true;
      if (ViewCompat.a(this) != 2)
      {
        if (paramMotionEvent != null) {
          a(paramMotionEvent.getX(), i2, paramMotionEvent.getY(), i4);
        }
        i(paramInt1, paramInt2);
      }
    }
    return false;
  }
  
  boolean a(AccessibilityEvent paramAccessibilityEvent)
  {
    if (j())
    {
      int i1 = 0;
      if (paramAccessibilityEvent != null) {
        i1 = AccessibilityEventCompat.b(paramAccessibilityEvent);
      }
      int i2 = i1;
      if (i1 == 0) {
        i2 = 0;
      }
      this.E |= i2;
      return true;
    }
    return false;
  }
  
  public void addFocusables(ArrayList<View> paramArrayList, int paramInt1, int paramInt2)
  {
    if ((this.f == null) || (!this.f.a(this, paramArrayList, paramInt1, paramInt2))) {
      super.addFocusables(paramArrayList, paramInt1, paramInt2);
    }
  }
  
  @Nullable
  public View b(View paramView)
  {
    ViewParent localViewParent = paramView.getParent();
    View localView = paramView;
    for (paramView = localViewParent; (paramView != null) && (paramView != this) && ((paramView instanceof View)); paramView = localView.getParent()) {
      localView = (View)paramView;
    }
    if (paramView == this) {
      return localView;
    }
    return null;
  }
  
  void b()
  {
    this.A += 1;
    if ((this.A == 1) && (!this.C)) {
      this.B = false;
    }
  }
  
  public void b(int paramInt)
  {
    if (this.C) {
      return;
    }
    if (this.f == null)
    {
      Log.e("RecyclerView", "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
      return;
    }
    this.f.a(this, this.h, paramInt);
  }
  
  public boolean b(int paramInt1, int paramInt2)
  {
    if (this.f == null) {
      Log.e("RecyclerView", "Cannot fling without a LayoutManager set. Call setLayoutManager with a non-null argument.");
    }
    boolean bool2;
    int i1;
    do
    {
      do
      {
        return false;
      } while (this.C);
      bool1 = this.f.e();
      bool2 = this.f.f();
      if (bool1)
      {
        i1 = paramInt1;
        if (Math.abs(paramInt1) >= this.aa) {}
      }
      else
      {
        i1 = 0;
      }
      if (bool2)
      {
        paramInt1 = paramInt2;
        if (Math.abs(paramInt2) >= this.aa) {}
      }
      else
      {
        paramInt1 = 0;
      }
    } while (((i1 == 0) && (paramInt1 == 0)) || (dispatchNestedPreFling(i1, paramInt1)));
    if ((bool1) || (bool2)) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      dispatchNestedFling(i1, paramInt1, bool1);
      if (!bool1) {
        break;
      }
      paramInt2 = Math.max(-this.ab, Math.min(i1, this.ab));
      paramInt1 = Math.max(-this.ab, Math.min(paramInt1, this.ab));
      this.ad.a(paramInt2, paramInt1);
      return true;
    }
  }
  
  public ViewHolder c(int paramInt)
  {
    Object localObject;
    if (this.J)
    {
      localObject = null;
      return (ViewHolder)localObject;
    }
    int i2 = this.d.c();
    int i1 = 0;
    for (;;)
    {
      if (i1 >= i2) {
        break label75;
      }
      ViewHolder localViewHolder = c(this.d.c(i1));
      if ((localViewHolder != null) && (!localViewHolder.isRemoved()))
      {
        localObject = localViewHolder;
        if (d(localViewHolder) == paramInt) {
          break;
        }
      }
      i1 += 1;
    }
    label75:
    return null;
  }
  
  public void c()
  {
    setScrollState(0);
    v();
  }
  
  void c(int paramInt1, int paramInt2)
  {
    if (paramInt1 < 0)
    {
      d();
      this.L.a(-paramInt1);
      if (paramInt2 >= 0) {
        break label69;
      }
      f();
      this.M.a(-paramInt2);
    }
    for (;;)
    {
      if ((paramInt1 != 0) || (paramInt2 != 0)) {
        ViewCompat.d(this);
      }
      return;
      if (paramInt1 <= 0) {
        break;
      }
      e();
      this.N.a(paramInt1);
      break;
      label69:
      if (paramInt2 > 0)
      {
        g();
        this.O.a(paramInt2);
      }
    }
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return ((paramLayoutParams instanceof LayoutParams)) && (this.f.a((LayoutParams)paramLayoutParams));
  }
  
  public int computeHorizontalScrollExtent()
  {
    if (this.f == null) {}
    while (!this.f.e()) {
      return 0;
    }
    return this.f.d(this.h);
  }
  
  public int computeHorizontalScrollOffset()
  {
    if (this.f == null) {}
    while (!this.f.e()) {
      return 0;
    }
    return this.f.b(this.h);
  }
  
  public int computeHorizontalScrollRange()
  {
    if (this.f == null) {}
    while (!this.f.e()) {
      return 0;
    }
    return this.f.f(this.h);
  }
  
  public int computeVerticalScrollExtent()
  {
    if (this.f == null) {}
    while (!this.f.f()) {
      return 0;
    }
    return this.f.e(this.h);
  }
  
  public int computeVerticalScrollOffset()
  {
    if (this.f == null) {}
    while (!this.f.f()) {
      return 0;
    }
    return this.f.c(this.h);
  }
  
  public int computeVerticalScrollRange()
  {
    if (this.f == null) {}
    while (!this.f.f()) {
      return 0;
    }
    return this.f.g(this.h);
  }
  
  @Deprecated
  public int d(View paramView)
  {
    return e(paramView);
  }
  
  void d()
  {
    if (this.L != null) {
      return;
    }
    this.L = new EdgeEffectCompat(getContext());
    if (this.p)
    {
      this.L.a(getMeasuredHeight() - getPaddingTop() - getPaddingBottom(), getMeasuredWidth() - getPaddingLeft() - getPaddingRight());
      return;
    }
    this.L.a(getMeasuredHeight(), getMeasuredWidth());
  }
  
  public void d(int paramInt)
  {
    int i2 = this.d.b();
    int i1 = 0;
    while (i1 < i2)
    {
      this.d.b(i1).offsetTopAndBottom(paramInt);
      i1 += 1;
    }
  }
  
  void d(int paramInt1, int paramInt2)
  {
    setMeasuredDimension(LayoutManager.a(paramInt1, getPaddingLeft() + getPaddingRight(), ViewCompat.q(this)), LayoutManager.a(paramInt2, getPaddingTop() + getPaddingBottom(), ViewCompat.r(this)));
  }
  
  public boolean dispatchNestedFling(float paramFloat1, float paramFloat2, boolean paramBoolean)
  {
    return getScrollingChildHelper().a(paramFloat1, paramFloat2, paramBoolean);
  }
  
  public boolean dispatchNestedPreFling(float paramFloat1, float paramFloat2)
  {
    return getScrollingChildHelper().a(paramFloat1, paramFloat2);
  }
  
  public boolean dispatchNestedPreScroll(int paramInt1, int paramInt2, int[] paramArrayOfInt1, int[] paramArrayOfInt2)
  {
    return getScrollingChildHelper().a(paramInt1, paramInt2, paramArrayOfInt1, paramArrayOfInt2);
  }
  
  public boolean dispatchNestedScroll(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[] paramArrayOfInt)
  {
    return getScrollingChildHelper().a(paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfInt);
  }
  
  protected void dispatchRestoreInstanceState(SparseArray<Parcelable> paramSparseArray)
  {
    dispatchThawSelfOnly(paramSparseArray);
  }
  
  protected void dispatchSaveInstanceState(SparseArray<Parcelable> paramSparseArray)
  {
    dispatchFreezeSelfOnly(paramSparseArray);
  }
  
  public void draw(Canvas paramCanvas)
  {
    int i3 = 1;
    super.draw(paramCanvas);
    int i2 = this.u.size();
    int i1 = 0;
    while (i1 < i2)
    {
      ((ItemDecoration)this.u.get(i1)).b(paramCanvas, this, this.h);
      i1 += 1;
    }
    i1 = 0;
    i2 = i1;
    int i4;
    if (this.L != null)
    {
      i2 = i1;
      if (!this.L.a())
      {
        i4 = paramCanvas.save();
        if (!this.p) {
          break label456;
        }
        i1 = getPaddingBottom();
        paramCanvas.rotate(270.0F);
        paramCanvas.translate(-getHeight() + i1, 0.0F);
        if ((this.L == null) || (!this.L.a(paramCanvas))) {
          break label461;
        }
        i2 = 1;
        label131:
        paramCanvas.restoreToCount(i4);
      }
    }
    i1 = i2;
    if (this.M != null)
    {
      i1 = i2;
      if (!this.M.a())
      {
        i4 = paramCanvas.save();
        if (this.p) {
          paramCanvas.translate(getPaddingLeft(), getPaddingTop());
        }
        if ((this.M == null) || (!this.M.a(paramCanvas))) {
          break label466;
        }
        i1 = 1;
        label205:
        i1 = i2 | i1;
        paramCanvas.restoreToCount(i4);
      }
    }
    i2 = i1;
    if (this.N != null)
    {
      i2 = i1;
      if (!this.N.a())
      {
        i4 = paramCanvas.save();
        int i5 = getWidth();
        if (!this.p) {
          break label471;
        }
        i2 = getPaddingTop();
        label260:
        paramCanvas.rotate(90.0F);
        paramCanvas.translate(-i2, -i5);
        if ((this.N == null) || (!this.N.a(paramCanvas))) {
          break label476;
        }
        i2 = 1;
        label298:
        i2 = i1 | i2;
        paramCanvas.restoreToCount(i4);
      }
    }
    i1 = i2;
    if (this.O != null)
    {
      i1 = i2;
      if (!this.O.a())
      {
        i4 = paramCanvas.save();
        paramCanvas.rotate(180.0F);
        if (!this.p) {
          break label481;
        }
        paramCanvas.translate(-getWidth() + getPaddingRight(), -getHeight() + getPaddingBottom());
        label375:
        if ((this.O == null) || (!this.O.a(paramCanvas))) {
          break label500;
        }
      }
    }
    label456:
    label461:
    label466:
    label471:
    label476:
    label481:
    label500:
    for (i1 = i3;; i1 = 0)
    {
      i1 = i2 | i1;
      paramCanvas.restoreToCount(i4);
      i2 = i1;
      if (i1 == 0)
      {
        i2 = i1;
        if (this.g != null)
        {
          i2 = i1;
          if (this.u.size() > 0)
          {
            i2 = i1;
            if (this.g.b()) {
              i2 = 1;
            }
          }
        }
      }
      if (i2 != 0) {
        ViewCompat.d(this);
      }
      return;
      i1 = 0;
      break;
      i2 = 0;
      break label131;
      i1 = 0;
      break label205;
      i2 = 0;
      break label260;
      i2 = 0;
      break label298;
      paramCanvas.translate(-getWidth(), -getHeight());
      break label375;
    }
  }
  
  public boolean drawChild(Canvas paramCanvas, View paramView, long paramLong)
  {
    return super.drawChild(paramCanvas, paramView, paramLong);
  }
  
  public int e(View paramView)
  {
    paramView = c(paramView);
    if (paramView != null) {
      return paramView.getAdapterPosition();
    }
    return -1;
  }
  
  void e()
  {
    if (this.N != null) {
      return;
    }
    this.N = new EdgeEffectCompat(getContext());
    if (this.p)
    {
      this.N.a(getMeasuredHeight() - getPaddingTop() - getPaddingBottom(), getMeasuredWidth() - getPaddingLeft() - getPaddingRight());
      return;
    }
    this.N.a(getMeasuredHeight(), getMeasuredWidth());
  }
  
  public void e(int paramInt)
  {
    int i2 = this.d.b();
    int i1 = 0;
    while (i1 < i2)
    {
      this.d.b(i1).offsetLeftAndRight(paramInt);
      i1 += 1;
    }
  }
  
  void e(int paramInt1, int paramInt2)
  {
    int i5 = this.d.c();
    int i3;
    int i1;
    if (paramInt1 < paramInt2)
    {
      i3 = paramInt1;
      i1 = paramInt2;
    }
    ViewHolder localViewHolder;
    for (int i2 = -1;; i2 = 1)
    {
      int i4 = 0;
      for (;;)
      {
        if (i4 >= i5) {
          break label131;
        }
        localViewHolder = c(this.d.c(i4));
        if ((localViewHolder != null) && (localViewHolder.mPosition >= i3) && (localViewHolder.mPosition <= i1)) {
          break;
        }
        i4 += 1;
      }
      i3 = paramInt2;
      i1 = paramInt1;
    }
    if (localViewHolder.mPosition == paramInt1) {
      localViewHolder.offsetPosition(paramInt2 - paramInt1, false);
    }
    for (;;)
    {
      State.a(this.h, true);
      break;
      localViewHolder.offsetPosition(i2, false);
    }
    label131:
    this.b.a(paramInt1, paramInt2);
    requestLayout();
  }
  
  public int f(View paramView)
  {
    paramView = c(paramView);
    if (paramView != null) {
      return paramView.getLayoutPosition();
    }
    return -1;
  }
  
  void f()
  {
    if (this.M != null) {
      return;
    }
    this.M = new EdgeEffectCompat(getContext());
    if (this.p)
    {
      this.M.a(getMeasuredWidth() - getPaddingLeft() - getPaddingRight(), getMeasuredHeight() - getPaddingTop() - getPaddingBottom());
      return;
    }
    this.M.a(getMeasuredWidth(), getMeasuredHeight());
  }
  
  public void f(int paramInt) {}
  
  void f(int paramInt1, int paramInt2)
  {
    int i2 = this.d.c();
    int i1 = 0;
    while (i1 < i2)
    {
      ViewHolder localViewHolder = c(this.d.c(i1));
      if ((localViewHolder != null) && (!localViewHolder.shouldIgnore()) && (localViewHolder.mPosition >= paramInt1))
      {
        localViewHolder.offsetPosition(paramInt2, false);
        State.a(this.h, true);
      }
      i1 += 1;
    }
    this.b.b(paramInt1, paramInt2);
    requestLayout();
  }
  
  public View focusSearch(View paramView, int paramInt)
  {
    Object localObject = this.f.d(paramView, paramInt);
    if (localObject != null) {
      return (View)localObject;
    }
    View localView = FocusFinder.getInstance().findNextFocus(this, paramView, paramInt);
    localObject = localView;
    if (localView == null)
    {
      localObject = localView;
      if (this.s != null)
      {
        localObject = localView;
        if (this.f != null)
        {
          localObject = localView;
          if (!j())
          {
            localObject = localView;
            if (!this.C)
            {
              b();
              localObject = this.f.a(paramView, paramInt, this.b, this.h);
              a(false);
            }
          }
        }
      }
    }
    if (localObject != null) {
      return (View)localObject;
    }
    return super.focusSearch(paramView, paramInt);
  }
  
  void g()
  {
    if (this.O != null) {
      return;
    }
    this.O = new EdgeEffectCompat(getContext());
    if (this.p)
    {
      this.O.a(getMeasuredWidth() - getPaddingLeft() - getPaddingRight(), getMeasuredHeight() - getPaddingTop() - getPaddingBottom());
      return;
    }
    this.O.a(getMeasuredWidth(), getMeasuredHeight());
  }
  
  void g(int paramInt)
  {
    if (this.f != null) {
      this.f.l(paramInt);
    }
    f(paramInt);
    if (this.ae != null) {
      this.ae.onScrollStateChanged(this, paramInt);
    }
    if (this.af != null)
    {
      int i1 = this.af.size() - 1;
      while (i1 >= 0)
      {
        ((OnScrollListener)this.af.get(i1)).onScrollStateChanged(this, paramInt);
        i1 -= 1;
      }
    }
  }
  
  public void g(int paramInt1, int paramInt2) {}
  
  public void g(View paramView) {}
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams()
  {
    if (this.f == null) {
      throw new IllegalStateException("RecyclerView has no LayoutManager");
    }
    return this.f.a();
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
  {
    if (this.f == null) {
      throw new IllegalStateException("RecyclerView has no LayoutManager");
    }
    return this.f.a(getContext(), paramAttributeSet);
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    if (this.f == null) {
      throw new IllegalStateException("RecyclerView has no LayoutManager");
    }
    return this.f.a(paramLayoutParams);
  }
  
  public Adapter getAdapter()
  {
    return this.s;
  }
  
  public int getBaseline()
  {
    if (this.f != null) {
      return this.f.t();
    }
    return super.getBaseline();
  }
  
  protected int getChildDrawingOrder(int paramInt1, int paramInt2)
  {
    if (this.aj == null) {
      return super.getChildDrawingOrder(paramInt1, paramInt2);
    }
    return this.aj.a(paramInt1, paramInt2);
  }
  
  public RecyclerViewAccessibilityDelegate getCompatAccessibilityDelegate()
  {
    return this.ai;
  }
  
  public ItemAnimator getItemAnimator()
  {
    return this.g;
  }
  
  public LayoutManager getLayoutManager()
  {
    return this.f;
  }
  
  public int getMaxFlingVelocity()
  {
    return this.ab;
  }
  
  public int getMinFlingVelocity()
  {
    return this.aa;
  }
  
  public RecycledViewPool getRecycledViewPool()
  {
    return this.b.f();
  }
  
  public int getScrollState()
  {
    return this.P;
  }
  
  void h()
  {
    this.O = null;
    this.M = null;
    this.N = null;
    this.L = null;
  }
  
  void h(int paramInt1, int paramInt2)
  {
    int i1 = getScrollX();
    int i2 = getScrollY();
    onScrollChanged(i1, i2, i1, i2);
    g(paramInt1, paramInt2);
    if (this.ae != null) {
      this.ae.onScrolled(this, paramInt1, paramInt2);
    }
    if (this.af != null)
    {
      i1 = this.af.size() - 1;
      while (i1 >= 0)
      {
        ((OnScrollListener)this.af.get(i1)).onScrolled(this, paramInt1, paramInt2);
        i1 -= 1;
      }
    }
  }
  
  public void h(View paramView) {}
  
  public boolean hasNestedScrollingParent()
  {
    return getScrollingChildHelper().b();
  }
  
  Rect i(View paramView)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    if (!localLayoutParams.c) {
      return localLayoutParams.b;
    }
    Rect localRect = localLayoutParams.b;
    localRect.set(0, 0, 0, 0);
    int i2 = this.u.size();
    int i1 = 0;
    while (i1 < i2)
    {
      this.r.set(0, 0, 0, 0);
      ((ItemDecoration)this.u.get(i1)).a(this.r, paramView, this, this.h);
      localRect.left += this.r.left;
      localRect.top += this.r.top;
      localRect.right += this.r.right;
      localRect.bottom += this.r.bottom;
      i1 += 1;
    }
    localLayoutParams.c = false;
    return localRect;
  }
  
  boolean i()
  {
    return (this.H != null) && (this.H.isEnabled());
  }
  
  public boolean isAttachedToWindow()
  {
    return this.x;
  }
  
  public boolean isNestedScrollingEnabled()
  {
    return getScrollingChildHelper().a();
  }
  
  public boolean j()
  {
    return this.K > 0;
  }
  
  void k()
  {
    if (this.s == null)
    {
      Log.e("RecyclerView", "No adapter attached; skipping layout");
      return;
    }
    if (this.f == null)
    {
      Log.e("RecyclerView", "No layout manager attached; skipping layout");
      return;
    }
    State.b(this.h, false);
    if (State.a(this.h) == 1)
    {
      F();
      this.f.f(this);
      G();
    }
    for (;;)
    {
      H();
      return;
      if ((this.c.f()) || (this.f.x() != getWidth()) || (this.f.y() != getHeight()))
      {
        this.f.f(this);
        G();
      }
      else
      {
        this.f.f(this);
      }
    }
  }
  
  void l()
  {
    int i2 = this.d.c();
    int i1 = 0;
    while (i1 < i2)
    {
      ((LayoutParams)this.d.c(i1).getLayoutParams()).c = true;
      i1 += 1;
    }
    this.b.j();
  }
  
  void m()
  {
    int i2 = this.d.c();
    int i1 = 0;
    while (i1 < i2)
    {
      ViewHolder localViewHolder = c(this.d.c(i1));
      if (!localViewHolder.shouldIgnore()) {
        localViewHolder.saveOldPosition();
      }
      i1 += 1;
    }
  }
  
  void n()
  {
    int i2 = this.d.c();
    int i1 = 0;
    while (i1 < i2)
    {
      ViewHolder localViewHolder = c(this.d.c(i1));
      if (!localViewHolder.shouldIgnore()) {
        localViewHolder.clearOldPosition();
      }
      i1 += 1;
    }
    this.b.i();
  }
  
  void o()
  {
    int i2 = this.d.c();
    int i1 = 0;
    while (i1 < i2)
    {
      ViewHolder localViewHolder = c(this.d.c(i1));
      if ((localViewHolder != null) && (!localViewHolder.shouldIgnore())) {
        localViewHolder.addFlags(6);
      }
      i1 += 1;
    }
    l();
    this.b.h();
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    this.K = 0;
    this.x = true;
    this.z = false;
    if (this.f != null) {
      this.f.c(this);
    }
    this.ah = false;
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    if (this.g != null) {
      this.g.c();
    }
    this.z = false;
    c();
    this.x = false;
    if (this.f != null) {
      this.f.b(this, this.b);
    }
    removeCallbacks(this.ap);
    this.e.b();
  }
  
  public void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    int i2 = this.u.size();
    int i1 = 0;
    while (i1 < i2)
    {
      ((ItemDecoration)this.u.get(i1)).a(paramCanvas, this, this.h);
      i1 += 1;
    }
  }
  
  public boolean onGenericMotionEvent(MotionEvent paramMotionEvent)
  {
    if (this.f == null) {}
    label110:
    label113:
    for (;;)
    {
      return false;
      if ((!this.C) && ((MotionEventCompat.d(paramMotionEvent) & 0x2) != 0) && (paramMotionEvent.getAction() == 8))
      {
        float f1;
        if (this.f.f())
        {
          f1 = -MotionEventCompat.e(paramMotionEvent, 9);
          if (!this.f.e()) {
            break label110;
          }
        }
        for (float f2 = MotionEventCompat.e(paramMotionEvent, 10);; f2 = 0.0F)
        {
          if ((f1 == 0.0F) && (f2 == 0.0F)) {
            break label113;
          }
          float f3 = getScrollFactor();
          a((int)(f2 * f3), (int)(f1 * f3), paramMotionEvent);
          return false;
          f1 = 0.0F;
          break;
        }
      }
    }
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    if (this.C) {
      return false;
    }
    if (a(paramMotionEvent))
    {
      y();
      return true;
    }
    if (this.f == null) {
      return false;
    }
    boolean bool1 = this.f.e();
    boolean bool2 = this.f.f();
    if (this.R == null) {
      this.R = VelocityTracker.obtain();
    }
    this.R.addMovement(paramMotionEvent);
    int i2 = MotionEventCompat.a(paramMotionEvent);
    int i1 = MotionEventCompat.b(paramMotionEvent);
    switch (i2)
    {
    }
    while (this.P == 1)
    {
      return true;
      if (this.D) {
        this.D = false;
      }
      this.Q = MotionEventCompat.b(paramMotionEvent, 0);
      i1 = (int)(paramMotionEvent.getX() + 0.5F);
      this.U = i1;
      this.S = i1;
      i1 = (int)(paramMotionEvent.getY() + 0.5F);
      this.V = i1;
      this.T = i1;
      if (this.P == 2)
      {
        getParent().requestDisallowInterceptTouchEvent(true);
        setScrollState(1);
      }
      paramMotionEvent = this.ao;
      this.ao[1] = 0;
      paramMotionEvent[0] = 0;
      i1 = 0;
      if (bool1) {
        i1 = 0x0 | 0x1;
      }
      i2 = i1;
      if (bool2) {
        i2 = i1 | 0x2;
      }
      startNestedScroll(i2);
      continue;
      this.Q = MotionEventCompat.b(paramMotionEvent, i1);
      i2 = (int)(MotionEventCompat.c(paramMotionEvent, i1) + 0.5F);
      this.U = i2;
      this.S = i2;
      i1 = (int)(MotionEventCompat.d(paramMotionEvent, i1) + 0.5F);
      this.V = i1;
      this.T = i1;
      continue;
      i2 = MotionEventCompat.a(paramMotionEvent, this.Q);
      if (i2 < 0)
      {
        Log.e("RecyclerView", "Error processing scroll; pointer index for id " + this.Q + " not found. Did any MotionEvents get skipped?");
        return false;
      }
      i1 = (int)(MotionEventCompat.c(paramMotionEvent, i2) + 0.5F);
      i2 = (int)(MotionEventCompat.d(paramMotionEvent, i2) + 0.5F);
      if (this.P != 1)
      {
        int i4 = i1 - this.S;
        int i3 = i2 - this.T;
        i2 = 0;
        i1 = i2;
        if (bool1)
        {
          i1 = i2;
          if (Math.abs(i4) > this.W)
          {
            i2 = this.S;
            int i5 = this.W;
            if (i4 >= 0) {
              break label532;
            }
            i1 = -1;
            label457:
            this.U = (i1 * i5 + i2);
            i1 = 1;
          }
        }
        i2 = i1;
        if (bool2)
        {
          i2 = i1;
          if (Math.abs(i3) > this.W)
          {
            i2 = this.T;
            i4 = this.W;
            if (i3 >= 0) {
              break label537;
            }
          }
        }
        label532:
        label537:
        for (i1 = -1;; i1 = 1)
        {
          this.V = (i1 * i4 + i2);
          i2 = 1;
          if (i2 == 0) {
            break;
          }
          setScrollState(1);
          break;
          i1 = 1;
          break label457;
        }
        c(paramMotionEvent);
        continue;
        this.R.clear();
        stopNestedScroll();
        continue;
        y();
      }
    }
    return false;
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    TraceCompat.a("RV OnLayout");
    k();
    TraceCompat.a();
    this.z = true;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i2 = 0;
    if (this.f == null) {
      d(paramInt1, paramInt2);
    }
    do
    {
      int i1;
      do
      {
        return;
        if (!LayoutManager.a(this.f)) {
          break;
        }
        int i3 = View.MeasureSpec.getMode(paramInt1);
        int i4 = View.MeasureSpec.getMode(paramInt2);
        i1 = i2;
        if (i3 == 1073741824)
        {
          i1 = i2;
          if (i4 == 1073741824) {
            i1 = 1;
          }
        }
        this.f.a(this.b, this.h, paramInt1, paramInt2);
      } while ((i1 != 0) || (this.s == null));
      if (State.a(this.h) == 1) {
        F();
      }
      this.f.b(paramInt1, paramInt2);
      State.b(this.h, true);
      G();
      this.f.c(paramInt1, paramInt2);
    } while (!this.f.l());
    this.f.b(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 1073741824));
    State.b(this.h, true);
    G();
    this.f.c(paramInt1, paramInt2);
    return;
    if (this.y)
    {
      this.f.a(this.b, this.h, paramInt1, paramInt2);
      return;
    }
    if (this.F)
    {
      b();
      E();
      if (State.b(this.h))
      {
        State.c(this.h, true);
        this.F = false;
        a(false);
      }
    }
    else
    {
      if (this.s == null) {
        break label342;
      }
    }
    label342:
    for (this.h.a = this.s.getItemCount();; this.h.a = 0)
    {
      b();
      this.f.a(this.b, this.h, paramInt1, paramInt2);
      a(false);
      State.c(this.h, false);
      return;
      this.c.e();
      State.c(this.h, false);
      break;
    }
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    if (!(paramParcelable instanceof SavedState)) {
      super.onRestoreInstanceState(paramParcelable);
    }
    do
    {
      return;
      this.o = ((SavedState)paramParcelable);
      super.onRestoreInstanceState(this.o.getSuperState());
    } while ((this.f == null) || (this.o.a == null));
    this.f.a(this.o.a);
  }
  
  protected Parcelable onSaveInstanceState()
  {
    SavedState localSavedState = new SavedState(super.onSaveInstanceState());
    if (this.o != null)
    {
      SavedState.a(localSavedState, this.o);
      return localSavedState;
    }
    if (this.f != null)
    {
      localSavedState.a = this.f.d();
      return localSavedState;
    }
    localSavedState.a = null;
    return localSavedState;
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    if ((paramInt1 != paramInt3) || (paramInt2 != paramInt4)) {
      h();
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if ((this.C) || (this.D)) {
      return false;
    }
    if (b(paramMotionEvent))
    {
      y();
      return true;
    }
    if (this.f == null) {
      return false;
    }
    boolean bool1 = this.f.e();
    boolean bool2 = this.f.f();
    if (this.R == null) {
      this.R = VelocityTracker.obtain();
    }
    int i7 = 0;
    MotionEvent localMotionEvent = MotionEvent.obtain(paramMotionEvent);
    int i3 = MotionEventCompat.a(paramMotionEvent);
    int i2 = MotionEventCompat.b(paramMotionEvent);
    if (i3 == 0)
    {
      int[] arrayOfInt = this.ao;
      this.ao[1] = 0;
      arrayOfInt[0] = 0;
    }
    localMotionEvent.offsetLocation(this.ao[0], this.ao[1]);
    int i1 = i7;
    switch (i3)
    {
    default: 
      i1 = i7;
    }
    for (;;)
    {
      if (i1 == 0) {
        this.R.addMovement(localMotionEvent);
      }
      localMotionEvent.recycle();
      return true;
      this.Q = MotionEventCompat.b(paramMotionEvent, 0);
      i1 = (int)(paramMotionEvent.getX() + 0.5F);
      this.U = i1;
      this.S = i1;
      i1 = (int)(paramMotionEvent.getY() + 0.5F);
      this.V = i1;
      this.T = i1;
      i1 = 0;
      if (bool1) {
        i1 = 0x0 | 0x1;
      }
      i2 = i1;
      if (bool2) {
        i2 = i1 | 0x2;
      }
      startNestedScroll(i2);
      i1 = i7;
      continue;
      this.Q = MotionEventCompat.b(paramMotionEvent, i2);
      i1 = (int)(MotionEventCompat.c(paramMotionEvent, i2) + 0.5F);
      this.U = i1;
      this.S = i1;
      i1 = (int)(MotionEventCompat.d(paramMotionEvent, i2) + 0.5F);
      this.V = i1;
      this.T = i1;
      i1 = i7;
      continue;
      i1 = MotionEventCompat.a(paramMotionEvent, this.Q);
      if (i1 < 0)
      {
        Log.e("RecyclerView", "Error processing scroll; pointer index for id " + this.Q + " not found. Did any MotionEvents get skipped?");
        return false;
      }
      int i8 = (int)(MotionEventCompat.c(paramMotionEvent, i1) + 0.5F);
      int i9 = (int)(MotionEventCompat.d(paramMotionEvent, i1) + 0.5F);
      int i4 = this.U - i8;
      i3 = this.V - i9;
      i2 = i4;
      i1 = i3;
      if (dispatchNestedPreScroll(i4, i3, this.an, this.am))
      {
        i2 = i4 - this.an[0];
        i1 = i3 - this.an[1];
        localMotionEvent.offsetLocation(this.am[0], this.am[1]);
        paramMotionEvent = this.ao;
        paramMotionEvent[0] += this.am[0];
        paramMotionEvent = this.ao;
        paramMotionEvent[1] += this.am[1];
      }
      int i5 = i2;
      i4 = i1;
      if (this.P != 1)
      {
        i5 = 0;
        i3 = i2;
        i4 = i5;
        if (bool1)
        {
          i3 = i2;
          i4 = i5;
          if (Math.abs(i2) > this.W)
          {
            if (i2 <= 0) {
              break label801;
            }
            i3 = i2 - this.W;
            label640:
            i4 = 1;
          }
        }
        i2 = i1;
        int i6 = i4;
        if (bool2)
        {
          i2 = i1;
          i6 = i4;
          if (Math.abs(i1) > this.W)
          {
            if (i1 <= 0) {
              break label813;
            }
            i2 = i1 - this.W;
            label690:
            i6 = 1;
          }
        }
        i5 = i3;
        i4 = i2;
        if (i6 != 0)
        {
          setScrollState(1);
          i4 = i2;
          i5 = i3;
        }
      }
      i1 = i7;
      if (this.P == 1)
      {
        this.U = (i8 - this.am[0]);
        this.V = (i9 - this.am[1]);
        if (bool1) {
          label762:
          if (!bool2) {
            break label831;
          }
        }
        for (;;)
        {
          i1 = i7;
          if (!a(i5, i4, localMotionEvent)) {
            break;
          }
          getParent().requestDisallowInterceptTouchEvent(true);
          i1 = i7;
          break;
          label801:
          i3 = i2 + this.W;
          break label640;
          label813:
          i2 = i1 + this.W;
          break label690;
          i5 = 0;
          break label762;
          label831:
          i4 = 0;
        }
        c(paramMotionEvent);
        i1 = i7;
        continue;
        this.R.addMovement(localMotionEvent);
        i1 = 1;
        this.R.computeCurrentVelocity(1000, this.ab);
        float f1;
        if (bool1)
        {
          f1 = -VelocityTrackerCompat.a(this.R, this.Q);
          label894:
          if (!bool2) {
            break label952;
          }
        }
        label952:
        for (float f2 = -VelocityTrackerCompat.b(this.R, this.Q);; f2 = 0.0F)
        {
          if (((f1 == 0.0F) && (f2 == 0.0F)) || (!b((int)f1, (int)f2))) {
            setScrollState(0);
          }
          x();
          break;
          f1 = 0.0F;
          break label894;
        }
        y();
        i1 = i7;
      }
    }
  }
  
  public boolean p()
  {
    return (!this.z) || (this.J) || (this.c.d());
  }
  
  protected void removeDetachedView(View paramView, boolean paramBoolean)
  {
    ViewHolder localViewHolder = c(paramView);
    if (localViewHolder != null)
    {
      if (!localViewHolder.isTmpDetached()) {
        break label32;
      }
      localViewHolder.clearTmpDetachFlag();
    }
    label32:
    while (localViewHolder.shouldIgnore())
    {
      k(paramView);
      super.removeDetachedView(paramView, paramBoolean);
      return;
    }
    throw new IllegalArgumentException("Called removeDetachedView with a view which is not flagged as tmp detached." + localViewHolder);
  }
  
  public void requestChildFocus(View paramView1, View paramView2)
  {
    boolean bool = false;
    if ((!this.f.a(this, this.h, paramView1, paramView2)) && (paramView2 != null))
    {
      this.r.set(0, 0, paramView2.getWidth(), paramView2.getHeight());
      Object localObject = paramView2.getLayoutParams();
      if ((localObject instanceof LayoutParams))
      {
        localObject = (LayoutParams)localObject;
        if (!((LayoutParams)localObject).c)
        {
          localObject = ((LayoutParams)localObject).b;
          Rect localRect = this.r;
          localRect.left -= ((Rect)localObject).left;
          localRect = this.r;
          localRect.right += ((Rect)localObject).right;
          localRect = this.r;
          localRect.top -= ((Rect)localObject).top;
          localRect = this.r;
          localRect.bottom += ((Rect)localObject).bottom;
        }
      }
      offsetDescendantRectToMyCoords(paramView2, this.r);
      offsetRectIntoDescendantCoords(paramView1, this.r);
      localObject = this.r;
      if (!this.z) {
        bool = true;
      }
      requestChildRectangleOnScreen(paramView1, (Rect)localObject, bool);
    }
    super.requestChildFocus(paramView1, paramView2);
  }
  
  public boolean requestChildRectangleOnScreen(View paramView, Rect paramRect, boolean paramBoolean)
  {
    return this.f.a(this, paramView, paramRect, paramBoolean);
  }
  
  public void requestDisallowInterceptTouchEvent(boolean paramBoolean)
  {
    int i2 = this.v.size();
    int i1 = 0;
    while (i1 < i2)
    {
      ((OnItemTouchListener)this.v.get(i1)).a(paramBoolean);
      i1 += 1;
    }
    super.requestDisallowInterceptTouchEvent(paramBoolean);
  }
  
  public void requestLayout()
  {
    if ((this.A == 0) && (!this.C))
    {
      super.requestLayout();
      return;
    }
    this.B = true;
  }
  
  public void scrollBy(int paramInt1, int paramInt2)
  {
    if (this.f == null) {}
    boolean bool1;
    boolean bool2;
    do
    {
      Log.e("RecyclerView", "Cannot scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
      do
      {
        return;
      } while (this.C);
      bool1 = this.f.e();
      bool2 = this.f.f();
    } while ((!bool1) && (!bool2));
    if (bool1) {
      if (!bool2) {
        break label74;
      }
    }
    for (;;)
    {
      a(paramInt1, paramInt2, null);
      return;
      paramInt1 = 0;
      break;
      label74:
      paramInt2 = 0;
    }
  }
  
  public void scrollTo(int paramInt1, int paramInt2)
  {
    Log.w("RecyclerView", "RecyclerView does not support scrolling to an absolute position. Use scrollToPosition instead");
  }
  
  public void sendAccessibilityEventUnchecked(AccessibilityEvent paramAccessibilityEvent)
  {
    if (a(paramAccessibilityEvent)) {
      return;
    }
    super.sendAccessibilityEventUnchecked(paramAccessibilityEvent);
  }
  
  public void setAccessibilityDelegateCompat(RecyclerViewAccessibilityDelegate paramRecyclerViewAccessibilityDelegate)
  {
    this.ai = paramRecyclerViewAccessibilityDelegate;
    ViewCompat.a(this, this.ai);
  }
  
  public void setAdapter(Adapter paramAdapter)
  {
    setLayoutFrozen(false);
    a(paramAdapter, false, true);
    requestLayout();
  }
  
  public void setChildDrawingOrderCallback(ChildDrawingOrderCallback paramChildDrawingOrderCallback)
  {
    if (paramChildDrawingOrderCallback == this.aj) {
      return;
    }
    this.aj = paramChildDrawingOrderCallback;
    if (this.aj != null) {}
    for (boolean bool = true;; bool = false)
    {
      setChildrenDrawingOrderEnabled(bool);
      return;
    }
  }
  
  public void setClipToPadding(boolean paramBoolean)
  {
    if (paramBoolean != this.p) {
      h();
    }
    this.p = paramBoolean;
    super.setClipToPadding(paramBoolean);
    if (this.z) {
      requestLayout();
    }
  }
  
  public void setHasFixedSize(boolean paramBoolean)
  {
    this.y = paramBoolean;
  }
  
  public void setItemAnimator(ItemAnimator paramItemAnimator)
  {
    if (this.g != null)
    {
      this.g.c();
      this.g.a(null);
    }
    this.g = paramItemAnimator;
    if (this.g != null) {
      this.g.a(this.ag);
    }
  }
  
  public void setItemViewCacheSize(int paramInt)
  {
    this.b.a(paramInt);
  }
  
  public void setLayoutFrozen(boolean paramBoolean)
  {
    if (paramBoolean != this.C)
    {
      a("Do not setLayoutFrozen in layout or scroll");
      if (!paramBoolean)
      {
        this.C = false;
        if ((this.B) && (this.f != null) && (this.s != null)) {
          requestLayout();
        }
        this.B = false;
      }
    }
    else
    {
      return;
    }
    long l1 = SystemClock.uptimeMillis();
    onTouchEvent(MotionEvent.obtain(l1, l1, 3, 0.0F, 0.0F, 0));
    this.C = true;
    this.D = true;
    c();
  }
  
  public void setLayoutManager(LayoutManager paramLayoutManager)
  {
    if (paramLayoutManager == this.f) {
      return;
    }
    c();
    if (this.f != null)
    {
      if (this.x) {
        this.f.b(this, this.b);
      }
      this.f.b(null);
    }
    this.b.a();
    this.d.a();
    this.f = paramLayoutManager;
    if (paramLayoutManager != null)
    {
      if (paramLayoutManager.q != null) {
        throw new IllegalArgumentException("LayoutManager " + paramLayoutManager + " is already attached to a RecyclerView: " + paramLayoutManager.q);
      }
      this.f.b(this);
      if (this.x) {
        this.f.c(this);
      }
    }
    requestLayout();
  }
  
  public void setNestedScrollingEnabled(boolean paramBoolean)
  {
    getScrollingChildHelper().a(paramBoolean);
  }
  
  @Deprecated
  public void setOnScrollListener(OnScrollListener paramOnScrollListener)
  {
    this.ae = paramOnScrollListener;
  }
  
  public void setRecycledViewPool(RecycledViewPool paramRecycledViewPool)
  {
    this.b.a(paramRecycledViewPool);
  }
  
  public void setRecyclerListener(RecyclerListener paramRecyclerListener)
  {
    this.t = paramRecyclerListener;
  }
  
  public void setScrollingTouchSlop(int paramInt)
  {
    ViewConfiguration localViewConfiguration = ViewConfiguration.get(getContext());
    switch (paramInt)
    {
    default: 
      Log.w("RecyclerView", "setScrollingTouchSlop(): bad argument constant " + paramInt + "; using default value");
    case 0: 
      this.W = localViewConfiguration.getScaledTouchSlop();
      return;
    }
    this.W = ViewConfigurationCompat.a(localViewConfiguration);
  }
  
  public void setViewCacheExtension(ViewCacheExtension paramViewCacheExtension)
  {
    this.b.a(paramViewCacheExtension);
  }
  
  public boolean startNestedScroll(int paramInt)
  {
    return getScrollingChildHelper().a(paramInt);
  }
  
  public void stopNestedScroll()
  {
    getScrollingChildHelper().c();
  }
  
  public static abstract class Adapter<VH extends RecyclerView.ViewHolder>
  {
    private boolean mHasStableIds = false;
    private final RecyclerView.AdapterDataObservable mObservable = new RecyclerView.AdapterDataObservable();
    
    public final void bindViewHolder(VH paramVH, int paramInt)
    {
      paramVH.mPosition = paramInt;
      if (hasStableIds()) {
        paramVH.mItemId = getItemId(paramInt);
      }
      paramVH.setFlags(1, 519);
      TraceCompat.a("RV OnBindView");
      onBindViewHolder(paramVH, paramInt, paramVH.getUnmodifiedPayloads());
      paramVH.clearPayload();
      TraceCompat.a();
    }
    
    public final VH createViewHolder(ViewGroup paramViewGroup, int paramInt)
    {
      TraceCompat.a("RV CreateView");
      paramViewGroup = onCreateViewHolder(paramViewGroup, paramInt);
      paramViewGroup.mItemViewType = paramInt;
      TraceCompat.a();
      return paramViewGroup;
    }
    
    public abstract int getItemCount();
    
    public long getItemId(int paramInt)
    {
      return -1L;
    }
    
    public int getItemViewType(int paramInt)
    {
      return 0;
    }
    
    public final boolean hasObservers()
    {
      return this.mObservable.a();
    }
    
    public final boolean hasStableIds()
    {
      return this.mHasStableIds;
    }
    
    public final void notifyDataSetChanged()
    {
      this.mObservable.b();
    }
    
    public final void notifyItemChanged(int paramInt)
    {
      this.mObservable.a(paramInt, 1);
    }
    
    public final void notifyItemChanged(int paramInt, Object paramObject)
    {
      this.mObservable.a(paramInt, 1, paramObject);
    }
    
    public final void notifyItemInserted(int paramInt)
    {
      this.mObservable.b(paramInt, 1);
    }
    
    public final void notifyItemMoved(int paramInt1, int paramInt2)
    {
      this.mObservable.d(paramInt1, paramInt2);
    }
    
    public final void notifyItemRangeChanged(int paramInt1, int paramInt2)
    {
      this.mObservable.a(paramInt1, paramInt2);
    }
    
    public final void notifyItemRangeChanged(int paramInt1, int paramInt2, Object paramObject)
    {
      this.mObservable.a(paramInt1, paramInt2, paramObject);
    }
    
    public final void notifyItemRangeInserted(int paramInt1, int paramInt2)
    {
      this.mObservable.b(paramInt1, paramInt2);
    }
    
    public final void notifyItemRangeRemoved(int paramInt1, int paramInt2)
    {
      this.mObservable.c(paramInt1, paramInt2);
    }
    
    public final void notifyItemRemoved(int paramInt)
    {
      this.mObservable.c(paramInt, 1);
    }
    
    public void onAttachedToRecyclerView(RecyclerView paramRecyclerView) {}
    
    public abstract void onBindViewHolder(VH paramVH, int paramInt);
    
    public void onBindViewHolder(VH paramVH, int paramInt, List<Object> paramList)
    {
      onBindViewHolder(paramVH, paramInt);
    }
    
    public abstract VH onCreateViewHolder(ViewGroup paramViewGroup, int paramInt);
    
    public void onDetachedFromRecyclerView(RecyclerView paramRecyclerView) {}
    
    public boolean onFailedToRecycleView(VH paramVH)
    {
      return false;
    }
    
    public void onViewAttachedToWindow(VH paramVH) {}
    
    public void onViewDetachedFromWindow(VH paramVH) {}
    
    public void onViewRecycled(VH paramVH) {}
    
    public void registerAdapterDataObserver(RecyclerView.AdapterDataObserver paramAdapterDataObserver)
    {
      this.mObservable.registerObserver(paramAdapterDataObserver);
    }
    
    public void setHasStableIds(boolean paramBoolean)
    {
      if (hasObservers()) {
        throw new IllegalStateException("Cannot change whether this adapter has stable IDs while the adapter has registered observers.");
      }
      this.mHasStableIds = paramBoolean;
    }
    
    public void unregisterAdapterDataObserver(RecyclerView.AdapterDataObserver paramAdapterDataObserver)
    {
      this.mObservable.unregisterObserver(paramAdapterDataObserver);
    }
  }
  
  static class AdapterDataObservable
    extends Observable<RecyclerView.AdapterDataObserver>
  {
    public void a(int paramInt1, int paramInt2)
    {
      a(paramInt1, paramInt2, null);
    }
    
    public void a(int paramInt1, int paramInt2, Object paramObject)
    {
      int i = this.mObservers.size() - 1;
      while (i >= 0)
      {
        ((RecyclerView.AdapterDataObserver)this.mObservers.get(i)).a(paramInt1, paramInt2, paramObject);
        i -= 1;
      }
    }
    
    public boolean a()
    {
      return !this.mObservers.isEmpty();
    }
    
    public void b()
    {
      int i = this.mObservers.size() - 1;
      while (i >= 0)
      {
        ((RecyclerView.AdapterDataObserver)this.mObservers.get(i)).a();
        i -= 1;
      }
    }
    
    public void b(int paramInt1, int paramInt2)
    {
      int i = this.mObservers.size() - 1;
      while (i >= 0)
      {
        ((RecyclerView.AdapterDataObserver)this.mObservers.get(i)).b(paramInt1, paramInt2);
        i -= 1;
      }
    }
    
    public void c(int paramInt1, int paramInt2)
    {
      int i = this.mObservers.size() - 1;
      while (i >= 0)
      {
        ((RecyclerView.AdapterDataObserver)this.mObservers.get(i)).c(paramInt1, paramInt2);
        i -= 1;
      }
    }
    
    public void d(int paramInt1, int paramInt2)
    {
      int i = this.mObservers.size() - 1;
      while (i >= 0)
      {
        ((RecyclerView.AdapterDataObserver)this.mObservers.get(i)).a(paramInt1, paramInt2, 1);
        i -= 1;
      }
    }
  }
  
  public static abstract class AdapterDataObserver
  {
    public void a() {}
    
    public void a(int paramInt1, int paramInt2) {}
    
    public void a(int paramInt1, int paramInt2, int paramInt3) {}
    
    public void a(int paramInt1, int paramInt2, Object paramObject)
    {
      a(paramInt1, paramInt2);
    }
    
    public void b(int paramInt1, int paramInt2) {}
    
    public void c(int paramInt1, int paramInt2) {}
  }
  
  public static abstract interface ChildDrawingOrderCallback
  {
    public abstract int a(int paramInt1, int paramInt2);
  }
  
  public static abstract class ItemAnimator
  {
    private ItemAnimatorListener a = null;
    private ArrayList<ItemAnimatorFinishedListener> b = new ArrayList();
    private long c = 120L;
    private long d = 120L;
    private long e = 250L;
    private long f = 250L;
    
    static int d(RecyclerView.ViewHolder paramViewHolder)
    {
      int j = RecyclerView.ViewHolder.access$6500(paramViewHolder) & 0xE;
      if (paramViewHolder.isInvalid()) {
        return 4;
      }
      int i = j;
      if ((j & 0x4) == 0)
      {
        int k = paramViewHolder.getOldPosition();
        int m = paramViewHolder.getAdapterPosition();
        i = j;
        if (k != -1)
        {
          i = j;
          if (m != -1)
          {
            i = j;
            if (k != m) {
              i = j | 0x800;
            }
          }
        }
      }
      return i;
    }
    
    @NonNull
    public ItemHolderInfo a(@NonNull RecyclerView.State paramState, @NonNull RecyclerView.ViewHolder paramViewHolder)
    {
      return i().a(paramViewHolder);
    }
    
    @NonNull
    public ItemHolderInfo a(@NonNull RecyclerView.State paramState, @NonNull RecyclerView.ViewHolder paramViewHolder, int paramInt, @NonNull List<Object> paramList)
    {
      return i().a(paramViewHolder);
    }
    
    public abstract void a();
    
    void a(ItemAnimatorListener paramItemAnimatorListener)
    {
      this.a = paramItemAnimatorListener;
    }
    
    public final boolean a(ItemAnimatorFinishedListener paramItemAnimatorFinishedListener)
    {
      boolean bool = b();
      if (paramItemAnimatorFinishedListener != null)
      {
        if (!bool) {
          paramItemAnimatorFinishedListener.a();
        }
      }
      else {
        return bool;
      }
      this.b.add(paramItemAnimatorFinishedListener);
      return bool;
    }
    
    public abstract boolean a(@NonNull RecyclerView.ViewHolder paramViewHolder, @NonNull ItemHolderInfo paramItemHolderInfo1, @Nullable ItemHolderInfo paramItemHolderInfo2);
    
    public abstract boolean a(@NonNull RecyclerView.ViewHolder paramViewHolder1, @NonNull RecyclerView.ViewHolder paramViewHolder2, @NonNull ItemHolderInfo paramItemHolderInfo1, @NonNull ItemHolderInfo paramItemHolderInfo2);
    
    public boolean a(@NonNull RecyclerView.ViewHolder paramViewHolder, @NonNull List<Object> paramList)
    {
      return g(paramViewHolder);
    }
    
    public abstract boolean b();
    
    public abstract boolean b(@NonNull RecyclerView.ViewHolder paramViewHolder, @Nullable ItemHolderInfo paramItemHolderInfo1, @NonNull ItemHolderInfo paramItemHolderInfo2);
    
    public abstract void c();
    
    public abstract void c(RecyclerView.ViewHolder paramViewHolder);
    
    public abstract boolean c(@NonNull RecyclerView.ViewHolder paramViewHolder, @NonNull ItemHolderInfo paramItemHolderInfo1, @NonNull ItemHolderInfo paramItemHolderInfo2);
    
    public long d()
    {
      return this.e;
    }
    
    public long e()
    {
      return this.c;
    }
    
    public final void e(RecyclerView.ViewHolder paramViewHolder)
    {
      f(paramViewHolder);
      if (this.a != null) {
        this.a.a(paramViewHolder);
      }
    }
    
    public long f()
    {
      return this.d;
    }
    
    public void f(RecyclerView.ViewHolder paramViewHolder) {}
    
    public long g()
    {
      return this.f;
    }
    
    public boolean g(@NonNull RecyclerView.ViewHolder paramViewHolder)
    {
      return true;
    }
    
    public final void h()
    {
      int j = this.b.size();
      int i = 0;
      while (i < j)
      {
        ((ItemAnimatorFinishedListener)this.b.get(i)).a();
        i += 1;
      }
      this.b.clear();
    }
    
    public ItemHolderInfo i()
    {
      return new ItemHolderInfo();
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface AdapterChanges {}
    
    public static abstract interface ItemAnimatorFinishedListener
    {
      public abstract void a();
    }
    
    static abstract interface ItemAnimatorListener
    {
      public abstract void a(RecyclerView.ViewHolder paramViewHolder);
    }
    
    public static class ItemHolderInfo
    {
      public int a;
      public int b;
      public int c;
      public int d;
      
      public ItemHolderInfo a(RecyclerView.ViewHolder paramViewHolder)
      {
        return a(paramViewHolder, 0);
      }
      
      public ItemHolderInfo a(RecyclerView.ViewHolder paramViewHolder, int paramInt)
      {
        paramViewHolder = paramViewHolder.itemView;
        this.a = paramViewHolder.getLeft();
        this.b = paramViewHolder.getTop();
        this.c = paramViewHolder.getRight();
        this.d = paramViewHolder.getBottom();
        return this;
      }
    }
  }
  
  private class ItemAnimatorRestoreListener
    implements RecyclerView.ItemAnimator.ItemAnimatorListener
  {
    private ItemAnimatorRestoreListener() {}
    
    public void a(RecyclerView.ViewHolder paramViewHolder)
    {
      paramViewHolder.setIsRecyclable(true);
      if ((paramViewHolder.mShadowedHolder != null) && (paramViewHolder.mShadowingHolder == null)) {
        paramViewHolder.mShadowedHolder = null;
      }
      paramViewHolder.mShadowingHolder = null;
      if ((!RecyclerView.ViewHolder.access$6300(paramViewHolder)) && (!RecyclerView.c(RecyclerView.this, paramViewHolder.itemView)) && (paramViewHolder.isTmpDetached())) {
        RecyclerView.this.removeDetachedView(paramViewHolder.itemView, false);
      }
    }
  }
  
  public static abstract class ItemDecoration
  {
    @Deprecated
    public void a(Canvas paramCanvas, RecyclerView paramRecyclerView) {}
    
    public void a(Canvas paramCanvas, RecyclerView paramRecyclerView, RecyclerView.State paramState)
    {
      a(paramCanvas, paramRecyclerView);
    }
    
    @Deprecated
    public void a(Rect paramRect, int paramInt, RecyclerView paramRecyclerView)
    {
      paramRect.set(0, 0, 0, 0);
    }
    
    public void a(Rect paramRect, View paramView, RecyclerView paramRecyclerView, RecyclerView.State paramState)
    {
      a(paramRect, ((RecyclerView.LayoutParams)paramView.getLayoutParams()).e(), paramRecyclerView);
    }
    
    @Deprecated
    public void b(Canvas paramCanvas, RecyclerView paramRecyclerView) {}
    
    public void b(Canvas paramCanvas, RecyclerView paramRecyclerView, RecyclerView.State paramState)
    {
      b(paramCanvas, paramRecyclerView);
    }
  }
  
  public static abstract class LayoutManager
  {
    private boolean a = false;
    private boolean b = false;
    private boolean c = true;
    private int d;
    private int e;
    private int f;
    private int g;
    ChildHelper p;
    RecyclerView q;
    @Nullable
    RecyclerView.SmoothScroller r;
    boolean s = false;
    
    public static int a(int paramInt1, int paramInt2, int paramInt3)
    {
      int j = View.MeasureSpec.getMode(paramInt1);
      int i = View.MeasureSpec.getSize(paramInt1);
      paramInt1 = i;
      switch (j)
      {
      default: 
        paramInt1 = Math.max(paramInt2, paramInt3);
      case 1073741824: 
        return paramInt1;
      }
      return Math.min(i, Math.max(paramInt2, paramInt3));
    }
    
    public static int a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean)
    {
      int i = Math.max(0, paramInt1 - paramInt3);
      paramInt3 = 0;
      paramInt1 = 0;
      if (paramBoolean) {
        if (paramInt4 >= 0)
        {
          paramInt3 = paramInt4;
          paramInt1 = 1073741824;
        }
      }
      for (;;)
      {
        return View.MeasureSpec.makeMeasureSpec(paramInt3, paramInt1);
        if (paramInt4 == -1)
        {
          switch (paramInt2)
          {
          default: 
            break;
          case 1073741824: 
          case -2147483648: 
            paramInt3 = i;
            paramInt1 = paramInt2;
            break;
          case 0: 
            paramInt3 = 0;
            paramInt1 = 0;
            break;
          }
        }
        else if (paramInt4 == -2)
        {
          paramInt3 = 0;
          paramInt1 = 0;
          continue;
          if (paramInt4 >= 0)
          {
            paramInt3 = paramInt4;
            paramInt1 = 1073741824;
          }
          else if (paramInt4 == -1)
          {
            paramInt3 = i;
            paramInt1 = paramInt2;
          }
          else if (paramInt4 == -2)
          {
            paramInt3 = i;
            if ((paramInt2 == Integer.MIN_VALUE) || (paramInt2 == 1073741824)) {
              paramInt1 = Integer.MIN_VALUE;
            } else {
              paramInt1 = 0;
            }
          }
        }
      }
    }
    
    public static Properties a(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
    {
      Properties localProperties = new Properties();
      paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.RecyclerView, paramInt1, paramInt2);
      localProperties.a = paramContext.getInt(R.styleable.RecyclerView_android_orientation, 1);
      localProperties.b = paramContext.getInt(R.styleable.RecyclerView_spanCount, 1);
      localProperties.c = paramContext.getBoolean(R.styleable.RecyclerView_reverseLayout, false);
      localProperties.d = paramContext.getBoolean(R.styleable.RecyclerView_stackFromEnd, false);
      paramContext.recycle();
      return localProperties;
    }
    
    private void a(int paramInt, View paramView)
    {
      this.p.d(paramInt);
    }
    
    private void a(RecyclerView.Recycler paramRecycler, int paramInt, View paramView)
    {
      RecyclerView.ViewHolder localViewHolder = RecyclerView.c(paramView);
      if (localViewHolder.shouldIgnore()) {
        return;
      }
      if ((localViewHolder.isInvalid()) && (!localViewHolder.isRemoved()) && (!RecyclerView.f(this.q).hasStableIds()))
      {
        g(paramInt);
        paramRecycler.b(localViewHolder);
        return;
      }
      h(paramInt);
      paramRecycler.c(paramView);
      this.q.e.h(localViewHolder);
    }
    
    private void a(View paramView, int paramInt, boolean paramBoolean)
    {
      RecyclerView.ViewHolder localViewHolder = RecyclerView.c(paramView);
      RecyclerView.LayoutParams localLayoutParams;
      if ((paramBoolean) || (localViewHolder.isRemoved()))
      {
        this.q.e.e(localViewHolder);
        localLayoutParams = (RecyclerView.LayoutParams)paramView.getLayoutParams();
        if ((!localViewHolder.wasReturnedFromScrap()) && (!localViewHolder.isScrap())) {
          break label128;
        }
        if (!localViewHolder.isScrap()) {
          break label120;
        }
        localViewHolder.unScrap();
        label68:
        this.p.a(paramView, paramInt, paramView.getLayoutParams(), false);
      }
      for (;;)
      {
        if (localLayoutParams.d)
        {
          localViewHolder.itemView.invalidate();
          localLayoutParams.d = false;
        }
        return;
        this.q.e.f(localViewHolder);
        break;
        label120:
        localViewHolder.clearReturnedFromScrapFlag();
        break label68;
        label128:
        if (paramView.getParent() == this.q)
        {
          int j = this.p.b(paramView);
          int i = paramInt;
          if (paramInt == -1) {
            i = this.p.b();
          }
          if (j == -1) {
            throw new IllegalStateException("Added View has RecyclerView as parent but view is not a real child. Unfiltered index:" + this.q.indexOfChild(paramView));
          }
          if (j != i) {
            this.q.f.d(j, i);
          }
        }
        else
        {
          this.p.a(paramView, paramInt, false);
          localLayoutParams.c = true;
          if ((this.r != null) && (this.r.h())) {
            this.r.b(paramView);
          }
        }
      }
    }
    
    private void b(RecyclerView.SmoothScroller paramSmoothScroller)
    {
      if (this.r == paramSmoothScroller) {
        this.r = null;
      }
    }
    
    private static boolean b(int paramInt1, int paramInt2, int paramInt3)
    {
      boolean bool2 = true;
      int i = View.MeasureSpec.getMode(paramInt2);
      paramInt2 = View.MeasureSpec.getSize(paramInt2);
      boolean bool1;
      if ((paramInt3 > 0) && (paramInt1 != paramInt3)) {
        bool1 = false;
      }
      do
      {
        do
        {
          return bool1;
          bool1 = bool2;
          switch (i)
          {
          case 0: 
          default: 
            return false;
          case -2147483648: 
            bool1 = bool2;
          }
        } while (paramInt2 >= paramInt1);
        return false;
        bool1 = bool2;
      } while (paramInt2 == paramInt1);
      return false;
    }
    
    public int A()
    {
      if (this.q != null) {
        return this.q.getPaddingTop();
      }
      return 0;
    }
    
    public int B()
    {
      if (this.q != null) {
        return this.q.getPaddingRight();
      }
      return 0;
    }
    
    public int C()
    {
      if (this.q != null) {
        return this.q.getPaddingBottom();
      }
      return 0;
    }
    
    public View D()
    {
      Object localObject;
      if (this.q == null) {
        localObject = null;
      }
      View localView;
      do
      {
        return (View)localObject;
        localView = this.q.getFocusedChild();
        if (localView == null) {
          break;
        }
        localObject = localView;
      } while (!this.p.c(localView));
      return null;
    }
    
    public int E()
    {
      if (this.q != null) {}
      for (RecyclerView.Adapter localAdapter = this.q.getAdapter(); localAdapter != null; localAdapter = null) {
        return localAdapter.getItemCount();
      }
      return 0;
    }
    
    public int F()
    {
      return ViewCompat.q(this.q);
    }
    
    public int G()
    {
      return ViewCompat.r(this.q);
    }
    
    void H()
    {
      if (this.r != null) {
        this.r.f();
      }
    }
    
    public void I()
    {
      this.a = true;
    }
    
    boolean J()
    {
      int j = u();
      int i = 0;
      while (i < j)
      {
        ViewGroup.LayoutParams localLayoutParams = i(i).getLayoutParams();
        if ((localLayoutParams.width < 0) && (localLayoutParams.height < 0)) {
          return true;
        }
        i += 1;
      }
      return false;
    }
    
    public int a(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
    {
      return 0;
    }
    
    public int a(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
    {
      if ((this.q == null) || (RecyclerView.f(this.q) == null)) {}
      while (!f()) {
        return 1;
      }
      return RecyclerView.f(this.q).getItemCount();
    }
    
    public abstract RecyclerView.LayoutParams a();
    
    public RecyclerView.LayoutParams a(Context paramContext, AttributeSet paramAttributeSet)
    {
      return new RecyclerView.LayoutParams(paramContext, paramAttributeSet);
    }
    
    public RecyclerView.LayoutParams a(ViewGroup.LayoutParams paramLayoutParams)
    {
      if ((paramLayoutParams instanceof RecyclerView.LayoutParams)) {
        return new RecyclerView.LayoutParams((RecyclerView.LayoutParams)paramLayoutParams);
      }
      if ((paramLayoutParams instanceof ViewGroup.MarginLayoutParams)) {
        return new RecyclerView.LayoutParams((ViewGroup.MarginLayoutParams)paramLayoutParams);
      }
      return new RecyclerView.LayoutParams(paramLayoutParams);
    }
    
    @Nullable
    public View a(View paramView, int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
    {
      return null;
    }
    
    public void a(int paramInt, RecyclerView.Recycler paramRecycler)
    {
      View localView = i(paramInt);
      g(paramInt);
      paramRecycler.a(localView);
    }
    
    public void a(Rect paramRect, int paramInt1, int paramInt2)
    {
      int i = paramRect.width();
      int j = z();
      int k = B();
      int m = paramRect.height();
      int n = A();
      int i1 = C();
      e(a(paramInt1, i + j + k, F()), a(paramInt2, m + n + i1, G()));
    }
    
    public void a(Parcelable paramParcelable) {}
    
    void a(AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat)
    {
      a(this.q.b, this.q.h, paramAccessibilityNodeInfoCompat);
    }
    
    public void a(RecyclerView.Adapter paramAdapter1, RecyclerView.Adapter paramAdapter2) {}
    
    public void a(RecyclerView.Recycler paramRecycler)
    {
      int i = u() - 1;
      while (i >= 0)
      {
        a(paramRecycler, i, i(i));
        i -= 1;
      }
    }
    
    public void a(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, int paramInt1, int paramInt2)
    {
      this.q.d(paramInt1, paramInt2);
    }
    
    public void a(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat)
    {
      if ((ViewCompat.b(this.q, -1)) || (ViewCompat.a(this.q, -1)))
      {
        paramAccessibilityNodeInfoCompat.a(8192);
        paramAccessibilityNodeInfoCompat.i(true);
      }
      if ((ViewCompat.b(this.q, 1)) || (ViewCompat.a(this.q, 1)))
      {
        paramAccessibilityNodeInfoCompat.a(4096);
        paramAccessibilityNodeInfoCompat.i(true);
      }
      paramAccessibilityNodeInfoCompat.b(AccessibilityNodeInfoCompat.CollectionInfoCompat.a(a(paramRecycler, paramState), b(paramRecycler, paramState), e(paramRecycler, paramState), d(paramRecycler, paramState)));
    }
    
    public void a(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, View paramView, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat)
    {
      int i;
      if (f())
      {
        i = d(paramView);
        if (!e()) {
          break label51;
        }
      }
      label51:
      for (int j = d(paramView);; j = 0)
      {
        paramAccessibilityNodeInfoCompat.c(AccessibilityNodeInfoCompat.CollectionItemInfoCompat.a(i, 1, j, 1, false, false));
        return;
        i = 0;
        break;
      }
    }
    
    public void a(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, AccessibilityEvent paramAccessibilityEvent)
    {
      boolean bool2 = true;
      paramRecycler = AccessibilityEventCompat.a(paramAccessibilityEvent);
      if ((this.q == null) || (paramRecycler == null)) {
        return;
      }
      boolean bool1 = bool2;
      if (!ViewCompat.b(this.q, 1))
      {
        bool1 = bool2;
        if (!ViewCompat.b(this.q, -1))
        {
          bool1 = bool2;
          if (!ViewCompat.a(this.q, -1)) {
            if (!ViewCompat.a(this.q, 1)) {
              break label111;
            }
          }
        }
      }
      label111:
      for (bool1 = bool2;; bool1 = false)
      {
        paramRecycler.a(bool1);
        if (RecyclerView.f(this.q) == null) {
          break;
        }
        paramRecycler.a(RecyclerView.f(this.q).getItemCount());
        return;
      }
    }
    
    public void a(RecyclerView.SmoothScroller paramSmoothScroller)
    {
      if ((this.r != null) && (paramSmoothScroller != this.r) && (this.r.h())) {
        this.r.f();
      }
      this.r = paramSmoothScroller;
      this.r.a(this.q, this);
    }
    
    public void a(RecyclerView paramRecyclerView) {}
    
    public void a(RecyclerView paramRecyclerView, int paramInt1, int paramInt2) {}
    
    public void a(RecyclerView paramRecyclerView, int paramInt1, int paramInt2, int paramInt3) {}
    
    public void a(RecyclerView paramRecyclerView, int paramInt1, int paramInt2, Object paramObject)
    {
      c(paramRecyclerView, paramInt1, paramInt2);
    }
    
    @CallSuper
    public void a(RecyclerView paramRecyclerView, RecyclerView.Recycler paramRecycler)
    {
      e(paramRecyclerView);
    }
    
    public void a(RecyclerView paramRecyclerView, RecyclerView.State paramState, int paramInt)
    {
      Log.e("RecyclerView", "You must override smoothScrollToPosition to support smooth scrolling");
    }
    
    public void a(View paramView)
    {
      a(paramView, -1);
    }
    
    public void a(View paramView, int paramInt)
    {
      a(paramView, paramInt, true);
    }
    
    public void a(View paramView, int paramInt1, int paramInt2)
    {
      RecyclerView.LayoutParams localLayoutParams = (RecyclerView.LayoutParams)paramView.getLayoutParams();
      Rect localRect = this.q.i(paramView);
      int k = localRect.left;
      int m = localRect.right;
      int i = localRect.top;
      int j = localRect.bottom;
      paramInt1 = a(x(), v(), z() + B() + localLayoutParams.leftMargin + localLayoutParams.rightMargin + (paramInt1 + (k + m)), localLayoutParams.width, e());
      paramInt2 = a(y(), w(), A() + C() + localLayoutParams.topMargin + localLayoutParams.bottomMargin + (paramInt2 + (i + j)), localLayoutParams.height, f());
      if (b(paramView, paramInt1, paramInt2, localLayoutParams)) {
        paramView.measure(paramInt1, paramInt2);
      }
    }
    
    public void a(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      Rect localRect = ((RecyclerView.LayoutParams)paramView.getLayoutParams()).b;
      paramView.layout(localRect.left + paramInt1, localRect.top + paramInt2, paramInt3 - localRect.right, paramInt4 - localRect.bottom);
    }
    
    public void a(View paramView, int paramInt, RecyclerView.LayoutParams paramLayoutParams)
    {
      RecyclerView.ViewHolder localViewHolder = RecyclerView.c(paramView);
      if (localViewHolder.isRemoved()) {
        this.q.e.e(localViewHolder);
      }
      for (;;)
      {
        this.p.a(paramView, paramInt, paramLayoutParams, localViewHolder.isRemoved());
        return;
        this.q.e.f(localViewHolder);
      }
    }
    
    public void a(View paramView, Rect paramRect)
    {
      if (this.q == null)
      {
        paramRect.set(0, 0, 0, 0);
        return;
      }
      paramRect.set(this.q.i(paramView));
    }
    
    void a(View paramView, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat)
    {
      RecyclerView.ViewHolder localViewHolder = RecyclerView.c(paramView);
      if ((localViewHolder != null) && (!localViewHolder.isRemoved()) && (!this.p.c(localViewHolder.itemView))) {
        a(this.q.b, this.q.h, paramView, paramAccessibilityNodeInfoCompat);
      }
    }
    
    public void a(View paramView, RecyclerView.Recycler paramRecycler)
    {
      c(paramView);
      paramRecycler.a(paramView);
    }
    
    public void a(AccessibilityEvent paramAccessibilityEvent)
    {
      a(this.q.b, this.q.h, paramAccessibilityEvent);
    }
    
    public void a(String paramString)
    {
      if (this.q != null) {
        this.q.a(paramString);
      }
    }
    
    boolean a(int paramInt, Bundle paramBundle)
    {
      return a(this.q.b, this.q.h, paramInt, paramBundle);
    }
    
    public boolean a(RecyclerView.LayoutParams paramLayoutParams)
    {
      return paramLayoutParams != null;
    }
    
    public boolean a(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, int paramInt, Bundle paramBundle)
    {
      if (this.q == null) {
        return false;
      }
      int k = 0;
      int m = 0;
      int i = 0;
      int j = 0;
      switch (paramInt)
      {
      default: 
        paramInt = i;
      }
      while ((paramInt != 0) || (j != 0))
      {
        this.q.scrollBy(j, paramInt);
        return true;
        i = k;
        if (ViewCompat.b(this.q, -1)) {
          i = -(y() - A() - C());
        }
        paramInt = i;
        if (ViewCompat.a(this.q, -1))
        {
          j = -(x() - z() - B());
          paramInt = i;
          continue;
          i = m;
          if (ViewCompat.b(this.q, 1)) {
            i = y() - A() - C();
          }
          paramInt = i;
          if (ViewCompat.a(this.q, 1))
          {
            j = x() - z() - B();
            paramInt = i;
          }
        }
      }
    }
    
    public boolean a(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, View paramView, int paramInt, Bundle paramBundle)
    {
      return false;
    }
    
    public boolean a(RecyclerView paramRecyclerView, RecyclerView.State paramState, View paramView1, View paramView2)
    {
      return a(paramRecyclerView, paramView1, paramView2);
    }
    
    public boolean a(RecyclerView paramRecyclerView, View paramView, Rect paramRect, boolean paramBoolean)
    {
      int i2 = z();
      int m = A();
      int i3 = x() - B();
      int i1 = y();
      int i6 = C();
      int i4 = paramView.getLeft() + paramRect.left - paramView.getScrollX();
      int n = paramView.getTop() + paramRect.top - paramView.getScrollY();
      int i5 = i4 + paramRect.width();
      int i7 = paramRect.height();
      int i = Math.min(0, i4 - i2);
      int j = Math.min(0, n - m);
      int k = Math.max(0, i5 - i3);
      i1 = Math.max(0, n + i7 - (i1 - i6));
      if (s() == 1) {
        if (k != 0)
        {
          i = k;
          if (j == 0) {
            break label217;
          }
          label154:
          if ((i == 0) && (j == 0)) {
            break label243;
          }
          if (!paramBoolean) {
            break label232;
          }
          paramRecyclerView.scrollBy(i, j);
        }
      }
      for (;;)
      {
        return true;
        i = Math.max(i, i5 - i3);
        break;
        if (i != 0) {
          break;
        }
        for (;;)
        {
          i = Math.min(i4 - i2, k);
        }
        label217:
        j = Math.min(n - m, i1);
        break label154;
        label232:
        paramRecyclerView.a(i, j);
      }
      label243:
      return false;
    }
    
    @Deprecated
    public boolean a(RecyclerView paramRecyclerView, View paramView1, View paramView2)
    {
      return (r()) || (paramRecyclerView.j());
    }
    
    public boolean a(RecyclerView paramRecyclerView, ArrayList<View> paramArrayList, int paramInt1, int paramInt2)
    {
      return false;
    }
    
    boolean a(View paramView, int paramInt1, int paramInt2, RecyclerView.LayoutParams paramLayoutParams)
    {
      return (!this.c) || (!b(paramView.getMeasuredWidth(), paramInt1, paramLayoutParams.width)) || (!b(paramView.getMeasuredHeight(), paramInt2, paramLayoutParams.height));
    }
    
    boolean a(View paramView, int paramInt, Bundle paramBundle)
    {
      return a(this.q.b, this.q.h, paramView, paramInt, paramBundle);
    }
    
    public boolean a(Runnable paramRunnable)
    {
      if (this.q != null) {
        return this.q.removeCallbacks(paramRunnable);
      }
      return false;
    }
    
    public int b(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
    {
      return 0;
    }
    
    public int b(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
    {
      if ((this.q == null) || (RecyclerView.f(this.q) == null)) {}
      while (!e()) {
        return 1;
      }
      return RecyclerView.f(this.q).getItemCount();
    }
    
    public int b(RecyclerView.State paramState)
    {
      return 0;
    }
    
    void b(int paramInt1, int paramInt2)
    {
      this.f = View.MeasureSpec.getSize(paramInt1);
      this.d = View.MeasureSpec.getMode(paramInt1);
      if ((this.d == 0) && (!RecyclerView.a)) {
        this.f = 0;
      }
      this.g = View.MeasureSpec.getSize(paramInt2);
      this.e = View.MeasureSpec.getMode(paramInt2);
      if ((this.e == 0) && (!RecyclerView.a)) {
        this.g = 0;
      }
    }
    
    void b(RecyclerView.Recycler paramRecycler)
    {
      int j = paramRecycler.d();
      int i = j - 1;
      if (i >= 0)
      {
        View localView = paramRecycler.e(i);
        RecyclerView.ViewHolder localViewHolder = RecyclerView.c(localView);
        if (localViewHolder.shouldIgnore()) {}
        for (;;)
        {
          i -= 1;
          break;
          localViewHolder.setIsRecyclable(false);
          if (localViewHolder.isTmpDetached()) {
            this.q.removeDetachedView(localView, false);
          }
          if (this.q.g != null) {
            this.q.g.c(localViewHolder);
          }
          localViewHolder.setIsRecyclable(true);
          paramRecycler.b(localView);
        }
      }
      paramRecycler.e();
      if (j > 0) {
        this.q.invalidate();
      }
    }
    
    void b(RecyclerView paramRecyclerView)
    {
      if (paramRecyclerView == null)
      {
        this.q = null;
        this.p = null;
        this.f = 0;
      }
      for (this.g = 0;; this.g = paramRecyclerView.getHeight())
      {
        this.d = 1073741824;
        this.e = 1073741824;
        return;
        this.q = paramRecyclerView;
        this.p = paramRecyclerView.d;
        this.f = paramRecyclerView.getWidth();
      }
    }
    
    public void b(RecyclerView paramRecyclerView, int paramInt1, int paramInt2) {}
    
    void b(RecyclerView paramRecyclerView, RecyclerView.Recycler paramRecycler)
    {
      this.s = false;
      a(paramRecyclerView, paramRecycler);
    }
    
    public void b(View paramView)
    {
      b(paramView, -1);
    }
    
    public void b(View paramView, int paramInt)
    {
      a(paramView, paramInt, false);
    }
    
    boolean b(View paramView, int paramInt1, int paramInt2, RecyclerView.LayoutParams paramLayoutParams)
    {
      return (paramView.isLayoutRequested()) || (!this.c) || (!b(paramView.getWidth(), paramInt1, paramLayoutParams.width)) || (!b(paramView.getHeight(), paramInt2, paramLayoutParams.height));
    }
    
    public int c(RecyclerView.State paramState)
    {
      return 0;
    }
    
    public View c(int paramInt)
    {
      int j = u();
      int i = 0;
      if (i < j)
      {
        View localView = i(i);
        RecyclerView.ViewHolder localViewHolder = RecyclerView.c(localView);
        if (localViewHolder == null) {}
        while ((localViewHolder.getLayoutPosition() != paramInt) || (localViewHolder.shouldIgnore()) || ((!this.q.h.a()) && (localViewHolder.isRemoved())))
        {
          i += 1;
          break;
        }
        return localView;
      }
      return null;
    }
    
    void c(int paramInt1, int paramInt2)
    {
      int i6 = u();
      if (i6 == 0)
      {
        this.q.d(paramInt1, paramInt2);
        return;
      }
      int i1 = Integer.MAX_VALUE;
      int j = Integer.MAX_VALUE;
      int n = Integer.MIN_VALUE;
      int i = Integer.MIN_VALUE;
      int k = 0;
      while (k < i6)
      {
        View localView = i(k);
        RecyclerView.LayoutParams localLayoutParams = (RecyclerView.LayoutParams)localView.getLayoutParams();
        int i5 = h(localView) - localLayoutParams.leftMargin;
        int i2 = j(localView) + localLayoutParams.rightMargin;
        int i4 = i(localView) - localLayoutParams.topMargin;
        int i3 = k(localView) + localLayoutParams.bottomMargin;
        int m = i1;
        if (i5 < i1) {
          m = i5;
        }
        i1 = n;
        if (i2 > n) {
          i1 = i2;
        }
        i2 = j;
        if (i4 < j) {
          i2 = i4;
        }
        j = i;
        if (i3 > i) {
          j = i3;
        }
        k += 1;
        n = i1;
        i = j;
        i1 = m;
        j = i2;
      }
      RecyclerView.s(this.q).set(i1, j, n, i);
      a(RecyclerView.s(this.q), paramInt1, paramInt2);
    }
    
    public void c(RecyclerView.Recycler paramRecycler)
    {
      int i = u() - 1;
      while (i >= 0)
      {
        if (!RecyclerView.c(i(i)).shouldIgnore()) {
          a(i, paramRecycler);
        }
        i -= 1;
      }
    }
    
    public void c(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
    {
      Log.e("RecyclerView", "You must override onLayoutChildren(Recycler recycler, State state) ");
    }
    
    void c(RecyclerView paramRecyclerView)
    {
      this.s = true;
      d(paramRecyclerView);
    }
    
    public void c(RecyclerView paramRecyclerView, int paramInt1, int paramInt2) {}
    
    public void c(View paramView)
    {
      this.p.a(paramView);
    }
    
    public void c(View paramView, int paramInt)
    {
      a(paramView, paramInt, (RecyclerView.LayoutParams)paramView.getLayoutParams());
    }
    
    public void c(boolean paramBoolean)
    {
      this.b = paramBoolean;
    }
    
    public boolean c()
    {
      return false;
    }
    
    public int d(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
    {
      return 0;
    }
    
    public int d(RecyclerView.State paramState)
    {
      return 0;
    }
    
    public int d(View paramView)
    {
      return ((RecyclerView.LayoutParams)paramView.getLayoutParams()).e();
    }
    
    public Parcelable d()
    {
      return null;
    }
    
    public View d(View paramView, int paramInt)
    {
      return null;
    }
    
    public void d(int paramInt1, int paramInt2)
    {
      View localView = i(paramInt1);
      if (localView == null) {
        throw new IllegalArgumentException("Cannot move a child from non-existing index:" + paramInt1);
      }
      h(paramInt1);
      c(localView, paramInt2);
    }
    
    @CallSuper
    public void d(RecyclerView paramRecyclerView) {}
    
    public int e(RecyclerView.State paramState)
    {
      return 0;
    }
    
    @Nullable
    public View e(View paramView)
    {
      if (this.q == null) {
        paramView = null;
      }
      View localView;
      do
      {
        return paramView;
        localView = this.q.b(paramView);
        if (localView == null) {
          return null;
        }
        paramView = localView;
      } while (!this.p.c(localView));
      return null;
    }
    
    public void e(int paramInt) {}
    
    public void e(int paramInt1, int paramInt2)
    {
      RecyclerView.b(this.q, paramInt1, paramInt2);
    }
    
    @Deprecated
    public void e(RecyclerView paramRecyclerView) {}
    
    public boolean e()
    {
      return false;
    }
    
    public boolean e(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
    {
      return false;
    }
    
    public int f(RecyclerView.State paramState)
    {
      return 0;
    }
    
    public int f(View paramView)
    {
      Rect localRect = ((RecyclerView.LayoutParams)paramView.getLayoutParams()).b;
      return paramView.getMeasuredWidth() + localRect.left + localRect.right;
    }
    
    void f(RecyclerView paramRecyclerView)
    {
      b(View.MeasureSpec.makeMeasureSpec(paramRecyclerView.getWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(paramRecyclerView.getHeight(), 1073741824));
    }
    
    public boolean f()
    {
      return false;
    }
    
    public int g(RecyclerView.State paramState)
    {
      return 0;
    }
    
    public int g(View paramView)
    {
      Rect localRect = ((RecyclerView.LayoutParams)paramView.getLayoutParams()).b;
      return paramView.getMeasuredHeight() + localRect.top + localRect.bottom;
    }
    
    public void g(int paramInt)
    {
      if (i(paramInt) != null) {
        this.p.a(paramInt);
      }
    }
    
    public int h(View paramView)
    {
      return paramView.getLeft() - n(paramView);
    }
    
    public void h(int paramInt)
    {
      a(paramInt, i(paramInt));
    }
    
    public int i(View paramView)
    {
      return paramView.getTop() - l(paramView);
    }
    
    public View i(int paramInt)
    {
      if (this.p != null) {
        return this.p.b(paramInt);
      }
      return null;
    }
    
    public int j(View paramView)
    {
      return paramView.getRight() + o(paramView);
    }
    
    public void j(int paramInt)
    {
      if (this.q != null) {
        this.q.e(paramInt);
      }
    }
    
    public int k(View paramView)
    {
      return paramView.getBottom() + m(paramView);
    }
    
    public void k(int paramInt)
    {
      if (this.q != null) {
        this.q.d(paramInt);
      }
    }
    
    public int l(View paramView)
    {
      return ((RecyclerView.LayoutParams)paramView.getLayoutParams()).b.top;
    }
    
    public void l(int paramInt) {}
    
    boolean l()
    {
      return false;
    }
    
    public int m(View paramView)
    {
      return ((RecyclerView.LayoutParams)paramView.getLayoutParams()).b.bottom;
    }
    
    public int n(View paramView)
    {
      return ((RecyclerView.LayoutParams)paramView.getLayoutParams()).b.left;
    }
    
    public int o(View paramView)
    {
      return ((RecyclerView.LayoutParams)paramView.getLayoutParams()).b.right;
    }
    
    public void o()
    {
      if (this.q != null) {
        this.q.requestLayout();
      }
    }
    
    public boolean p()
    {
      return this.s;
    }
    
    public boolean q()
    {
      return (this.q != null) && (RecyclerView.t(this.q));
    }
    
    public boolean r()
    {
      return (this.r != null) && (this.r.h());
    }
    
    public int s()
    {
      return ViewCompat.h(this.q);
    }
    
    public int t()
    {
      return -1;
    }
    
    public int u()
    {
      if (this.p != null) {
        return this.p.b();
      }
      return 0;
    }
    
    public int v()
    {
      return this.d;
    }
    
    public int w()
    {
      return this.e;
    }
    
    public int x()
    {
      return this.f;
    }
    
    public int y()
    {
      return this.g;
    }
    
    public int z()
    {
      if (this.q != null) {
        return this.q.getPaddingLeft();
      }
      return 0;
    }
    
    public static class Properties
    {
      public int a;
      public int b;
      public boolean c;
      public boolean d;
    }
  }
  
  public static class LayoutParams
    extends ViewGroup.MarginLayoutParams
  {
    RecyclerView.ViewHolder a;
    final Rect b = new Rect();
    boolean c = true;
    boolean d = false;
    
    public LayoutParams(int paramInt1, int paramInt2)
    {
      super(paramInt2);
    }
    
    public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
    }
    
    public LayoutParams(LayoutParams paramLayoutParams)
    {
      super();
    }
    
    public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
    }
    
    public LayoutParams(ViewGroup.MarginLayoutParams paramMarginLayoutParams)
    {
      super();
    }
    
    public boolean c()
    {
      return this.a.isRemoved();
    }
    
    public boolean d()
    {
      return this.a.isUpdated();
    }
    
    public int e()
    {
      return this.a.getLayoutPosition();
    }
  }
  
  public static abstract interface OnChildAttachStateChangeListener
  {
    public abstract void a(View paramView);
    
    public abstract void b(View paramView);
  }
  
  public static abstract interface OnItemTouchListener
  {
    public abstract void a(boolean paramBoolean);
    
    public abstract boolean a(RecyclerView paramRecyclerView, MotionEvent paramMotionEvent);
    
    public abstract void b(RecyclerView paramRecyclerView, MotionEvent paramMotionEvent);
  }
  
  public static abstract class OnScrollListener
  {
    public void onScrollStateChanged(RecyclerView paramRecyclerView, int paramInt) {}
    
    public void onScrolled(RecyclerView paramRecyclerView, int paramInt1, int paramInt2) {}
  }
  
  public static class RecycledViewPool
  {
    private SparseArray<ArrayList<RecyclerView.ViewHolder>> a = new SparseArray();
    private SparseIntArray b = new SparseIntArray();
    private int c = 0;
    
    private ArrayList<RecyclerView.ViewHolder> b(int paramInt)
    {
      ArrayList localArrayList2 = (ArrayList)this.a.get(paramInt);
      ArrayList localArrayList1 = localArrayList2;
      if (localArrayList2 == null)
      {
        localArrayList2 = new ArrayList();
        this.a.put(paramInt, localArrayList2);
        localArrayList1 = localArrayList2;
        if (this.b.indexOfKey(paramInt) < 0)
        {
          this.b.put(paramInt, 5);
          localArrayList1 = localArrayList2;
        }
      }
      return localArrayList1;
    }
    
    public RecyclerView.ViewHolder a(int paramInt)
    {
      ArrayList localArrayList = (ArrayList)this.a.get(paramInt);
      if ((localArrayList != null) && (!localArrayList.isEmpty()))
      {
        paramInt = localArrayList.size() - 1;
        RecyclerView.ViewHolder localViewHolder = (RecyclerView.ViewHolder)localArrayList.get(paramInt);
        localArrayList.remove(paramInt);
        return localViewHolder;
      }
      return null;
    }
    
    public void a()
    {
      this.a.clear();
    }
    
    void a(RecyclerView.Adapter paramAdapter)
    {
      this.c += 1;
    }
    
    void a(RecyclerView.Adapter paramAdapter1, RecyclerView.Adapter paramAdapter2, boolean paramBoolean)
    {
      if (paramAdapter1 != null) {
        b();
      }
      if ((!paramBoolean) && (this.c == 0)) {
        a();
      }
      if (paramAdapter2 != null) {
        a(paramAdapter2);
      }
    }
    
    public void a(RecyclerView.ViewHolder paramViewHolder)
    {
      int i = paramViewHolder.getItemViewType();
      ArrayList localArrayList = b(i);
      if (this.b.get(i) <= localArrayList.size()) {
        return;
      }
      paramViewHolder.resetInternal();
      localArrayList.add(paramViewHolder);
    }
    
    void b()
    {
      this.c -= 1;
    }
  }
  
  public final class Recycler
  {
    final ArrayList<RecyclerView.ViewHolder> a = new ArrayList();
    final ArrayList<RecyclerView.ViewHolder> b = new ArrayList();
    private ArrayList<RecyclerView.ViewHolder> d = null;
    private final List<RecyclerView.ViewHolder> e = Collections.unmodifiableList(this.a);
    private int f = 2;
    private RecyclerView.RecycledViewPool g;
    private RecyclerView.ViewCacheExtension h;
    
    public Recycler() {}
    
    private void a(ViewGroup paramViewGroup, boolean paramBoolean)
    {
      int i = paramViewGroup.getChildCount() - 1;
      while (i >= 0)
      {
        View localView = paramViewGroup.getChildAt(i);
        if ((localView instanceof ViewGroup)) {
          a((ViewGroup)localView, true);
        }
        i -= 1;
      }
      if (!paramBoolean) {
        return;
      }
      if (paramViewGroup.getVisibility() == 4)
      {
        paramViewGroup.setVisibility(0);
        paramViewGroup.setVisibility(4);
        return;
      }
      i = paramViewGroup.getVisibility();
      paramViewGroup.setVisibility(4);
      paramViewGroup.setVisibility(i);
    }
    
    private void d(View paramView)
    {
      if (RecyclerView.this.i())
      {
        if (ViewCompat.e(paramView) == 0) {
          ViewCompat.c(paramView, 1);
        }
        if (!ViewCompat.b(paramView)) {
          ViewCompat.a(paramView, RecyclerView.q(RecyclerView.this).b());
        }
      }
    }
    
    private void f(RecyclerView.ViewHolder paramViewHolder)
    {
      if ((paramViewHolder.itemView instanceof ViewGroup)) {
        a((ViewGroup)paramViewHolder.itemView, false);
      }
    }
    
    RecyclerView.ViewHolder a(int paramInt1, int paramInt2, boolean paramBoolean)
    {
      int j = this.a.size();
      int i = 0;
      Object localObject;
      RecyclerView.ViewHolder localViewHolder;
      for (;;)
      {
        if (i < j)
        {
          localObject = (RecyclerView.ViewHolder)this.a.get(i);
          if ((((RecyclerView.ViewHolder)localObject).wasReturnedFromScrap()) || (((RecyclerView.ViewHolder)localObject).getLayoutPosition() != paramInt1) || (((RecyclerView.ViewHolder)localObject).isInvalid()) || ((!RecyclerView.State.f(RecyclerView.this.h)) && (((RecyclerView.ViewHolder)localObject).isRemoved()))) {
            break label248;
          }
          if ((paramInt2 != -1) && (((RecyclerView.ViewHolder)localObject).getItemViewType() != paramInt2)) {
            Log.e("RecyclerView", "Scrap view for position " + paramInt1 + " isn't dirty but has" + " wrong view type! (found " + ((RecyclerView.ViewHolder)localObject).getItemViewType() + " but expected " + paramInt2 + ")");
          }
        }
        else
        {
          if (paramBoolean) {
            break label285;
          }
          localObject = RecyclerView.this.d.a(paramInt1, paramInt2);
          if (localObject == null) {
            break label285;
          }
          localViewHolder = RecyclerView.c((View)localObject);
          RecyclerView.this.d.e((View)localObject);
          paramInt1 = RecyclerView.this.d.b((View)localObject);
          if (paramInt1 != -1) {
            break;
          }
          throw new IllegalStateException("layout index should not be -1 after unhiding a view:" + localViewHolder);
        }
        ((RecyclerView.ViewHolder)localObject).addFlags(32);
        return (RecyclerView.ViewHolder)localObject;
        label248:
        i += 1;
      }
      RecyclerView.this.d.d(paramInt1);
      c((View)localObject);
      localViewHolder.addFlags(8224);
      return localViewHolder;
      label285:
      i = this.b.size();
      paramInt2 = 0;
      for (;;)
      {
        if (paramInt2 >= i) {
          break label359;
        }
        localViewHolder = (RecyclerView.ViewHolder)this.b.get(paramInt2);
        if ((!localViewHolder.isInvalid()) && (localViewHolder.getLayoutPosition() == paramInt1))
        {
          localObject = localViewHolder;
          if (paramBoolean) {
            break;
          }
          this.b.remove(paramInt2);
          return localViewHolder;
        }
        paramInt2 += 1;
      }
      label359:
      return null;
    }
    
    RecyclerView.ViewHolder a(long paramLong, int paramInt, boolean paramBoolean)
    {
      int i = this.a.size() - 1;
      RecyclerView.ViewHolder localViewHolder2;
      RecyclerView.ViewHolder localViewHolder1;
      while (i >= 0)
      {
        localViewHolder2 = (RecyclerView.ViewHolder)this.a.get(i);
        if ((localViewHolder2.getItemId() == paramLong) && (!localViewHolder2.wasReturnedFromScrap()))
        {
          if (paramInt == localViewHolder2.getItemViewType())
          {
            localViewHolder2.addFlags(32);
            localViewHolder1 = localViewHolder2;
            if (localViewHolder2.isRemoved())
            {
              localViewHolder1 = localViewHolder2;
              if (!RecyclerView.this.h.a())
              {
                localViewHolder2.setFlags(2, 14);
                localViewHolder1 = localViewHolder2;
              }
            }
            return localViewHolder1;
          }
          if (!paramBoolean)
          {
            this.a.remove(i);
            RecyclerView.this.removeDetachedView(localViewHolder2.itemView, false);
            b(localViewHolder2.itemView);
          }
        }
        i -= 1;
      }
      i = this.b.size() - 1;
      for (;;)
      {
        if (i < 0) {
          break label245;
        }
        localViewHolder2 = (RecyclerView.ViewHolder)this.b.get(i);
        if (localViewHolder2.getItemId() == paramLong)
        {
          if (paramInt == localViewHolder2.getItemViewType())
          {
            localViewHolder1 = localViewHolder2;
            if (paramBoolean) {
              break;
            }
            this.b.remove(i);
            return localViewHolder2;
          }
          if (!paramBoolean) {
            d(i);
          }
        }
        i -= 1;
      }
      label245:
      return null;
    }
    
    View a(int paramInt, boolean paramBoolean)
    {
      if ((paramInt < 0) || (paramInt >= RecyclerView.this.h.e())) {
        throw new IndexOutOfBoundsException("Invalid item position " + paramInt + "(" + paramInt + "). Item count:" + RecyclerView.this.h.e());
      }
      int j = 0;
      Object localObject2 = null;
      if (RecyclerView.this.h.a())
      {
        localObject2 = f(paramInt);
        if (localObject2 != null) {
          j = 1;
        }
      }
      else
      {
        i = j;
        localObject1 = localObject2;
        if (localObject2 == null)
        {
          localObject2 = a(paramInt, -1, paramBoolean);
          i = j;
          localObject1 = localObject2;
          if (localObject2 != null)
          {
            if (a((RecyclerView.ViewHolder)localObject2)) {
              break label327;
            }
            if (!paramBoolean)
            {
              ((RecyclerView.ViewHolder)localObject2).addFlags(4);
              if (!((RecyclerView.ViewHolder)localObject2).isScrap()) {
                break label311;
              }
              RecyclerView.this.removeDetachedView(((RecyclerView.ViewHolder)localObject2).itemView, false);
              ((RecyclerView.ViewHolder)localObject2).unScrap();
              label184:
              b((RecyclerView.ViewHolder)localObject2);
            }
            localObject1 = null;
            i = j;
          }
        }
      }
      for (;;)
      {
        k = i;
        localObject2 = localObject1;
        if (localObject1 != null) {
          break label598;
        }
        k = RecyclerView.this.c.b(paramInt);
        if ((k >= 0) && (k < RecyclerView.f(RecyclerView.this).getItemCount())) {
          break label336;
        }
        throw new IndexOutOfBoundsException("Inconsistency detected. Invalid item position " + paramInt + "(offset:" + k + ")." + "state:" + RecyclerView.this.h.e());
        j = 0;
        break;
        label311:
        if (!((RecyclerView.ViewHolder)localObject2).wasReturnedFromScrap()) {
          break label184;
        }
        ((RecyclerView.ViewHolder)localObject2).clearReturnedFromScrapFlag();
        break label184;
        label327:
        i = 1;
        localObject1 = localObject2;
      }
      label336:
      int m = RecyclerView.f(RecyclerView.this).getItemViewType(k);
      j = i;
      localObject2 = localObject1;
      if (RecyclerView.f(RecyclerView.this).hasStableIds())
      {
        localObject1 = a(RecyclerView.f(RecyclerView.this).getItemId(k), m, paramBoolean);
        j = i;
        localObject2 = localObject1;
        if (localObject1 != null)
        {
          ((RecyclerView.ViewHolder)localObject1).mPosition = k;
          j = 1;
          localObject2 = localObject1;
        }
      }
      Object localObject1 = localObject2;
      if (localObject2 == null)
      {
        localObject1 = localObject2;
        if (this.h != null)
        {
          localObject3 = this.h.a(this, paramInt, m);
          localObject1 = localObject2;
          if (localObject3 != null)
          {
            localObject2 = RecyclerView.this.a((View)localObject3);
            if (localObject2 == null) {
              throw new IllegalArgumentException("getViewForPositionAndType returned a view which does not have a ViewHolder");
            }
            localObject1 = localObject2;
            if (((RecyclerView.ViewHolder)localObject2).shouldIgnore()) {
              throw new IllegalArgumentException("getViewForPositionAndType returned a view that is ignored. You must call stopIgnoring before returning this view.");
            }
          }
        }
      }
      Object localObject3 = localObject1;
      if (localObject1 == null)
      {
        localObject1 = f().a(m);
        localObject3 = localObject1;
        if (localObject1 != null)
        {
          ((RecyclerView.ViewHolder)localObject1).resetInternal();
          localObject3 = localObject1;
          if (RecyclerView.r())
          {
            f((RecyclerView.ViewHolder)localObject1);
            localObject3 = localObject1;
          }
        }
      }
      int k = j;
      localObject2 = localObject3;
      if (localObject3 == null)
      {
        localObject2 = RecyclerView.f(RecyclerView.this).createViewHolder(RecyclerView.this, m);
        k = j;
      }
      label598:
      if ((k != 0) && (!RecyclerView.this.h.a()) && (((RecyclerView.ViewHolder)localObject2).hasAnyOfTheFlags(8192)))
      {
        ((RecyclerView.ViewHolder)localObject2).setFlags(0, 8192);
        if (RecyclerView.State.c(RecyclerView.this.h))
        {
          i = RecyclerView.ItemAnimator.d((RecyclerView.ViewHolder)localObject2);
          localObject1 = RecyclerView.this.g.a(RecyclerView.this.h, (RecyclerView.ViewHolder)localObject2, i | 0x1000, ((RecyclerView.ViewHolder)localObject2).getUnmodifiedPayloads());
          RecyclerView.a(RecyclerView.this, (RecyclerView.ViewHolder)localObject2, (RecyclerView.ItemAnimator.ItemHolderInfo)localObject1);
        }
      }
      int i = 0;
      if ((RecyclerView.this.h.a()) && (((RecyclerView.ViewHolder)localObject2).isBound()))
      {
        ((RecyclerView.ViewHolder)localObject2).mPreLayoutPosition = paramInt;
        localObject1 = ((RecyclerView.ViewHolder)localObject2).itemView.getLayoutParams();
        if (localObject1 != null) {
          break label891;
        }
        localObject1 = (RecyclerView.LayoutParams)RecyclerView.this.generateDefaultLayoutParams();
        ((RecyclerView.ViewHolder)localObject2).itemView.setLayoutParams((ViewGroup.LayoutParams)localObject1);
        label763:
        ((RecyclerView.LayoutParams)localObject1).a = ((RecyclerView.ViewHolder)localObject2);
        if ((k == 0) || (i == 0)) {
          break label940;
        }
      }
      label891:
      label940:
      for (paramBoolean = true;; paramBoolean = false)
      {
        ((RecyclerView.LayoutParams)localObject1).d = paramBoolean;
        return ((RecyclerView.ViewHolder)localObject2).itemView;
        if ((((RecyclerView.ViewHolder)localObject2).isBound()) && (!((RecyclerView.ViewHolder)localObject2).needsUpdate()) && (!((RecyclerView.ViewHolder)localObject2).isInvalid())) {
          break;
        }
        i = RecyclerView.this.c.b(paramInt);
        ((RecyclerView.ViewHolder)localObject2).mOwnerRecyclerView = RecyclerView.this;
        RecyclerView.f(RecyclerView.this).bindViewHolder((RecyclerView.ViewHolder)localObject2, i);
        d(((RecyclerView.ViewHolder)localObject2).itemView);
        j = 1;
        i = j;
        if (!RecyclerView.this.h.a()) {
          break;
        }
        ((RecyclerView.ViewHolder)localObject2).mPreLayoutPosition = paramInt;
        i = j;
        break;
        if (!RecyclerView.this.checkLayoutParams((ViewGroup.LayoutParams)localObject1))
        {
          localObject1 = (RecyclerView.LayoutParams)RecyclerView.this.generateLayoutParams((ViewGroup.LayoutParams)localObject1);
          ((RecyclerView.ViewHolder)localObject2).itemView.setLayoutParams((ViewGroup.LayoutParams)localObject1);
          break label763;
        }
        localObject1 = (RecyclerView.LayoutParams)localObject1;
        break label763;
      }
    }
    
    public void a()
    {
      this.a.clear();
      c();
    }
    
    public void a(int paramInt)
    {
      this.f = paramInt;
      int i = this.b.size() - 1;
      while ((i >= 0) && (this.b.size() > paramInt))
      {
        d(i);
        i -= 1;
      }
    }
    
    void a(int paramInt1, int paramInt2)
    {
      int k;
      int i;
      int j;
      int m;
      label25:
      RecyclerView.ViewHolder localViewHolder;
      if (paramInt1 < paramInt2)
      {
        k = paramInt1;
        i = paramInt2;
        j = -1;
        int n = this.b.size();
        m = 0;
        if (m >= n) {
          return;
        }
        localViewHolder = (RecyclerView.ViewHolder)this.b.get(m);
        if ((localViewHolder != null) && (localViewHolder.mPosition >= k) && (localViewHolder.mPosition <= i)) {
          break label90;
        }
      }
      for (;;)
      {
        m += 1;
        break label25;
        k = paramInt2;
        i = paramInt1;
        j = 1;
        break;
        label90:
        if (localViewHolder.mPosition == paramInt1) {
          localViewHolder.offsetPosition(paramInt2 - paramInt1, false);
        } else {
          localViewHolder.offsetPosition(j, false);
        }
      }
    }
    
    void a(RecyclerView.Adapter paramAdapter1, RecyclerView.Adapter paramAdapter2, boolean paramBoolean)
    {
      a();
      f().a(paramAdapter1, paramAdapter2, paramBoolean);
    }
    
    void a(RecyclerView.RecycledViewPool paramRecycledViewPool)
    {
      if (this.g != null) {
        this.g.b();
      }
      this.g = paramRecycledViewPool;
      if (paramRecycledViewPool != null) {
        this.g.a(RecyclerView.this.getAdapter());
      }
    }
    
    void a(RecyclerView.ViewCacheExtension paramViewCacheExtension)
    {
      this.h = paramViewCacheExtension;
    }
    
    public void a(View paramView)
    {
      RecyclerView.ViewHolder localViewHolder = RecyclerView.c(paramView);
      if (localViewHolder.isTmpDetached()) {
        RecyclerView.this.removeDetachedView(paramView, false);
      }
      if (localViewHolder.isScrap()) {
        localViewHolder.unScrap();
      }
      for (;;)
      {
        b(localViewHolder);
        return;
        if (localViewHolder.wasReturnedFromScrap()) {
          localViewHolder.clearReturnedFromScrapFlag();
        }
      }
    }
    
    boolean a(RecyclerView.ViewHolder paramViewHolder)
    {
      boolean bool2 = true;
      boolean bool1;
      if (paramViewHolder.isRemoved()) {
        bool1 = RecyclerView.this.h.a();
      }
      do
      {
        do
        {
          return bool1;
          if ((paramViewHolder.mPosition < 0) || (paramViewHolder.mPosition >= RecyclerView.f(RecyclerView.this).getItemCount())) {
            throw new IndexOutOfBoundsException("Inconsistency detected. Invalid view holder adapter position" + paramViewHolder);
          }
          if ((!RecyclerView.this.h.a()) && (RecyclerView.f(RecyclerView.this).getItemViewType(paramViewHolder.mPosition) != paramViewHolder.getItemViewType())) {
            return false;
          }
          bool1 = bool2;
        } while (!RecyclerView.f(RecyclerView.this).hasStableIds());
        bool1 = bool2;
      } while (paramViewHolder.getItemId() == RecyclerView.f(RecyclerView.this).getItemId(paramViewHolder.mPosition));
      return false;
    }
    
    public int b(int paramInt)
    {
      if ((paramInt < 0) || (paramInt >= RecyclerView.this.h.e())) {
        throw new IndexOutOfBoundsException("invalid position " + paramInt + ". State " + "item count is " + RecyclerView.this.h.e());
      }
      if (!RecyclerView.this.h.a()) {
        return paramInt;
      }
      return RecyclerView.this.c.b(paramInt);
    }
    
    public List<RecyclerView.ViewHolder> b()
    {
      return this.e;
    }
    
    void b(int paramInt1, int paramInt2)
    {
      int j = this.b.size();
      int i = 0;
      while (i < j)
      {
        RecyclerView.ViewHolder localViewHolder = (RecyclerView.ViewHolder)this.b.get(i);
        if ((localViewHolder != null) && (localViewHolder.mPosition >= paramInt1)) {
          localViewHolder.offsetPosition(paramInt2, true);
        }
        i += 1;
      }
    }
    
    void b(int paramInt1, int paramInt2, boolean paramBoolean)
    {
      int i = this.b.size() - 1;
      if (i >= 0)
      {
        RecyclerView.ViewHolder localViewHolder = (RecyclerView.ViewHolder)this.b.get(i);
        if (localViewHolder != null)
        {
          if (localViewHolder.mPosition < paramInt1 + paramInt2) {
            break label63;
          }
          localViewHolder.offsetPosition(-paramInt2, paramBoolean);
        }
        for (;;)
        {
          i -= 1;
          break;
          label63:
          if (localViewHolder.mPosition >= paramInt1)
          {
            localViewHolder.addFlags(8);
            d(i);
          }
        }
      }
    }
    
    void b(RecyclerView.ViewHolder paramViewHolder)
    {
      boolean bool = true;
      if ((paramViewHolder.isScrap()) || (paramViewHolder.itemView.getParent() != null))
      {
        StringBuilder localStringBuilder = new StringBuilder().append("Scrapped or attached views may not be recycled. isScrap:").append(paramViewHolder.isScrap()).append(" isAttached:");
        if (paramViewHolder.itemView.getParent() != null) {}
        for (;;)
        {
          throw new IllegalArgumentException(bool);
          bool = false;
        }
      }
      if (paramViewHolder.isTmpDetached()) {
        throw new IllegalArgumentException("Tmp detached view should be removed from RecyclerView before it can be recycled: " + paramViewHolder);
      }
      if (paramViewHolder.shouldIgnore()) {
        throw new IllegalArgumentException("Trying to recycle an ignored view holder. You should first call stopIgnoringView(view) before calling recycle.");
      }
      bool = RecyclerView.ViewHolder.access$4900(paramViewHolder);
      if ((RecyclerView.f(RecyclerView.this) != null) && (bool) && (RecyclerView.f(RecyclerView.this).onFailedToRecycleView(paramViewHolder))) {}
      for (int i = 1;; i = 0)
      {
        int j = 0;
        int n = 0;
        int m = 0;
        int k;
        if (i == 0)
        {
          k = m;
          if (!paramViewHolder.isRecyclable()) {}
        }
        else
        {
          i = n;
          if (!paramViewHolder.hasAnyOfTheFlags(14))
          {
            j = this.b.size();
            if ((j == this.f) && (j > 0)) {
              d(0);
            }
            i = n;
            if (j < this.f)
            {
              this.b.add(paramViewHolder);
              i = 1;
            }
          }
          j = i;
          k = m;
          if (i == 0)
          {
            c(paramViewHolder);
            k = 1;
            j = i;
          }
        }
        RecyclerView.this.e.g(paramViewHolder);
        if ((j == 0) && (k == 0) && (bool)) {
          paramViewHolder.mOwnerRecyclerView = null;
        }
        return;
      }
    }
    
    void b(View paramView)
    {
      paramView = RecyclerView.c(paramView);
      RecyclerView.ViewHolder.access$5002(paramView, null);
      RecyclerView.ViewHolder.access$5102(paramView, false);
      paramView.clearReturnedFromScrapFlag();
      b(paramView);
    }
    
    public View c(int paramInt)
    {
      return a(paramInt, false);
    }
    
    void c()
    {
      int i = this.b.size() - 1;
      while (i >= 0)
      {
        d(i);
        i -= 1;
      }
      this.b.clear();
    }
    
    void c(int paramInt1, int paramInt2)
    {
      int i = this.b.size() - 1;
      if (i >= 0)
      {
        RecyclerView.ViewHolder localViewHolder = (RecyclerView.ViewHolder)this.b.get(i);
        if (localViewHolder == null) {}
        for (;;)
        {
          i -= 1;
          break;
          int j = localViewHolder.getLayoutPosition();
          if ((j >= paramInt1) && (j < paramInt1 + paramInt2))
          {
            localViewHolder.addFlags(2);
            d(i);
          }
        }
      }
    }
    
    void c(RecyclerView.ViewHolder paramViewHolder)
    {
      ViewCompat.a(paramViewHolder.itemView, null);
      e(paramViewHolder);
      paramViewHolder.mOwnerRecyclerView = null;
      f().a(paramViewHolder);
    }
    
    void c(View paramView)
    {
      paramView = RecyclerView.c(paramView);
      if ((paramView.hasAnyOfTheFlags(12)) || (!paramView.isUpdated()) || (RecyclerView.a(RecyclerView.this, paramView)))
      {
        if ((paramView.isInvalid()) && (!paramView.isRemoved()) && (!RecyclerView.f(RecyclerView.this).hasStableIds())) {
          throw new IllegalArgumentException("Called scrap view with an invalid view. Invalid views cannot be reused from scrap, they should rebound from recycler pool.");
        }
        paramView.setScrapContainer(this, false);
        this.a.add(paramView);
        return;
      }
      if (this.d == null) {
        this.d = new ArrayList();
      }
      paramView.setScrapContainer(this, true);
      this.d.add(paramView);
    }
    
    int d()
    {
      return this.a.size();
    }
    
    void d(int paramInt)
    {
      c((RecyclerView.ViewHolder)this.b.get(paramInt));
      this.b.remove(paramInt);
    }
    
    void d(RecyclerView.ViewHolder paramViewHolder)
    {
      if (RecyclerView.ViewHolder.access$5100(paramViewHolder)) {
        this.d.remove(paramViewHolder);
      }
      for (;;)
      {
        RecyclerView.ViewHolder.access$5002(paramViewHolder, null);
        RecyclerView.ViewHolder.access$5102(paramViewHolder, false);
        paramViewHolder.clearReturnedFromScrapFlag();
        return;
        this.a.remove(paramViewHolder);
      }
    }
    
    View e(int paramInt)
    {
      return ((RecyclerView.ViewHolder)this.a.get(paramInt)).itemView;
    }
    
    void e()
    {
      this.a.clear();
      if (this.d != null) {
        this.d.clear();
      }
    }
    
    void e(RecyclerView.ViewHolder paramViewHolder)
    {
      if (RecyclerView.r(RecyclerView.this) != null) {
        RecyclerView.r(RecyclerView.this).a(paramViewHolder);
      }
      if (RecyclerView.f(RecyclerView.this) != null) {
        RecyclerView.f(RecyclerView.this).onViewRecycled(paramViewHolder);
      }
      if (RecyclerView.this.h != null) {
        RecyclerView.this.e.g(paramViewHolder);
      }
    }
    
    RecyclerView.RecycledViewPool f()
    {
      if (this.g == null) {
        this.g = new RecyclerView.RecycledViewPool();
      }
      return this.g;
    }
    
    RecyclerView.ViewHolder f(int paramInt)
    {
      int j;
      if (this.d != null)
      {
        j = this.d.size();
        if (j != 0) {}
      }
      else
      {
        return null;
      }
      int i = 0;
      RecyclerView.ViewHolder localViewHolder;
      while (i < j)
      {
        localViewHolder = (RecyclerView.ViewHolder)this.d.get(i);
        if ((!localViewHolder.wasReturnedFromScrap()) && (localViewHolder.getLayoutPosition() == paramInt))
        {
          localViewHolder.addFlags(32);
          return localViewHolder;
        }
        i += 1;
      }
      if (RecyclerView.f(RecyclerView.this).hasStableIds())
      {
        paramInt = RecyclerView.this.c.b(paramInt);
        if ((paramInt > 0) && (paramInt < RecyclerView.f(RecyclerView.this).getItemCount()))
        {
          long l = RecyclerView.f(RecyclerView.this).getItemId(paramInt);
          paramInt = 0;
          while (paramInt < j)
          {
            localViewHolder = (RecyclerView.ViewHolder)this.d.get(paramInt);
            if ((!localViewHolder.wasReturnedFromScrap()) && (localViewHolder.getItemId() == l))
            {
              localViewHolder.addFlags(32);
              return localViewHolder;
            }
            paramInt += 1;
          }
        }
      }
      return null;
    }
    
    void g()
    {
      int j = this.b.size();
      int i = 0;
      while (i < j)
      {
        RecyclerView.ViewHolder localViewHolder = (RecyclerView.ViewHolder)this.b.get(i);
        if (localViewHolder != null) {
          localViewHolder.addFlags(512);
        }
        i += 1;
      }
    }
    
    void h()
    {
      int j;
      int i;
      if ((RecyclerView.f(RecyclerView.this) != null) && (RecyclerView.f(RecyclerView.this).hasStableIds()))
      {
        j = this.b.size();
        i = 0;
      }
      while (i < j)
      {
        RecyclerView.ViewHolder localViewHolder = (RecyclerView.ViewHolder)this.b.get(i);
        if (localViewHolder != null)
        {
          localViewHolder.addFlags(6);
          localViewHolder.addChangePayload(null);
        }
        i += 1;
        continue;
        c();
      }
    }
    
    void i()
    {
      int j = this.b.size();
      int i = 0;
      while (i < j)
      {
        ((RecyclerView.ViewHolder)this.b.get(i)).clearOldPosition();
        i += 1;
      }
      j = this.a.size();
      i = 0;
      while (i < j)
      {
        ((RecyclerView.ViewHolder)this.a.get(i)).clearOldPosition();
        i += 1;
      }
      if (this.d != null)
      {
        j = this.d.size();
        i = 0;
        while (i < j)
        {
          ((RecyclerView.ViewHolder)this.d.get(i)).clearOldPosition();
          i += 1;
        }
      }
    }
    
    void j()
    {
      int j = this.b.size();
      int i = 0;
      while (i < j)
      {
        RecyclerView.LayoutParams localLayoutParams = (RecyclerView.LayoutParams)((RecyclerView.ViewHolder)this.b.get(i)).itemView.getLayoutParams();
        if (localLayoutParams != null) {
          localLayoutParams.c = true;
        }
        i += 1;
      }
    }
  }
  
  public static abstract interface RecyclerListener
  {
    public abstract void a(RecyclerView.ViewHolder paramViewHolder);
  }
  
  private class RecyclerViewDataObserver
    extends RecyclerView.AdapterDataObserver
  {
    private RecyclerViewDataObserver() {}
    
    public void a()
    {
      RecyclerView.this.a(null);
      if (RecyclerView.f(RecyclerView.this).hasStableIds())
      {
        RecyclerView.State.a(RecyclerView.this.h, true);
        RecyclerView.l(RecyclerView.this);
      }
      for (;;)
      {
        if (!RecyclerView.this.c.d()) {
          RecyclerView.this.requestLayout();
        }
        return;
        RecyclerView.State.a(RecyclerView.this.h, true);
        RecyclerView.l(RecyclerView.this);
      }
    }
    
    public void a(int paramInt1, int paramInt2, int paramInt3)
    {
      RecyclerView.this.a(null);
      if (RecyclerView.this.c.a(paramInt1, paramInt2, paramInt3)) {
        b();
      }
    }
    
    public void a(int paramInt1, int paramInt2, Object paramObject)
    {
      RecyclerView.this.a(null);
      if (RecyclerView.this.c.a(paramInt1, paramInt2, paramObject)) {
        b();
      }
    }
    
    void b()
    {
      if ((RecyclerView.m(RecyclerView.this)) && (RecyclerView.n(RecyclerView.this)) && (RecyclerView.o(RecyclerView.this)))
      {
        ViewCompat.a(RecyclerView.this, RecyclerView.p(RecyclerView.this));
        return;
      }
      RecyclerView.c(RecyclerView.this, true);
      RecyclerView.this.requestLayout();
    }
    
    public void b(int paramInt1, int paramInt2)
    {
      RecyclerView.this.a(null);
      if (RecyclerView.this.c.b(paramInt1, paramInt2)) {
        b();
      }
    }
    
    public void c(int paramInt1, int paramInt2)
    {
      RecyclerView.this.a(null);
      if (RecyclerView.this.c.c(paramInt1, paramInt2)) {
        b();
      }
    }
  }
  
  public static class SavedState
    extends View.BaseSavedState
  {
    public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
    {
      public RecyclerView.SavedState a(Parcel paramAnonymousParcel)
      {
        return new RecyclerView.SavedState(paramAnonymousParcel);
      }
      
      public RecyclerView.SavedState[] a(int paramAnonymousInt)
      {
        return new RecyclerView.SavedState[paramAnonymousInt];
      }
    };
    Parcelable a;
    
    SavedState(Parcel paramParcel)
    {
      super();
      this.a = paramParcel.readParcelable(RecyclerView.LayoutManager.class.getClassLoader());
    }
    
    SavedState(Parcelable paramParcelable)
    {
      super();
    }
    
    private void a(SavedState paramSavedState)
    {
      this.a = paramSavedState.a;
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      super.writeToParcel(paramParcel, paramInt);
      paramParcel.writeParcelable(this.a, 0);
    }
  }
  
  public static class SimpleOnItemTouchListener
    implements RecyclerView.OnItemTouchListener
  {
    public void a(boolean paramBoolean) {}
    
    public boolean a(RecyclerView paramRecyclerView, MotionEvent paramMotionEvent)
    {
      return false;
    }
    
    public void b(RecyclerView paramRecyclerView, MotionEvent paramMotionEvent) {}
  }
  
  public static abstract class SmoothScroller
  {
    private int a = -1;
    private RecyclerView b;
    private RecyclerView.LayoutManager c;
    private boolean d;
    private boolean e;
    private View f;
    private final Action g = new Action(0, 0);
    
    private void a(int paramInt1, int paramInt2)
    {
      RecyclerView localRecyclerView = this.b;
      if ((!this.e) || (this.a == -1) || (localRecyclerView == null)) {
        f();
      }
      this.d = false;
      if (this.f != null)
      {
        if (a(this.f) != this.a) {
          break label151;
        }
        a(this.f, localRecyclerView.h, this.g);
        Action.a(this.g, localRecyclerView);
        f();
      }
      for (;;)
      {
        if (this.e)
        {
          a(paramInt1, paramInt2, localRecyclerView.h, this.g);
          boolean bool = this.g.a();
          Action.a(this.g, localRecyclerView);
          if (bool)
          {
            if (!this.e) {
              break;
            }
            this.d = true;
            RecyclerView.u(localRecyclerView).a();
          }
        }
        return;
        label151:
        Log.e("RecyclerView", "Passed over target position while smooth scrolling.");
        this.f = null;
      }
      f();
    }
    
    public int a(View paramView)
    {
      return this.b.f(paramView);
    }
    
    protected abstract void a();
    
    protected abstract void a(int paramInt1, int paramInt2, RecyclerView.State paramState, Action paramAction);
    
    protected void a(PointF paramPointF)
    {
      double d1 = Math.sqrt(paramPointF.x * paramPointF.x + paramPointF.y * paramPointF.y);
      paramPointF.x = ((float)(paramPointF.x / d1));
      paramPointF.y = ((float)(paramPointF.y / d1));
    }
    
    void a(RecyclerView paramRecyclerView, RecyclerView.LayoutManager paramLayoutManager)
    {
      this.b = paramRecyclerView;
      this.c = paramLayoutManager;
      if (this.a == -1) {
        throw new IllegalArgumentException("Invalid target position");
      }
      RecyclerView.State.e(this.b.h, this.a);
      this.e = true;
      this.d = true;
      this.f = e(i());
      a();
      RecyclerView.u(this.b).a();
    }
    
    protected abstract void a(View paramView, RecyclerView.State paramState, Action paramAction);
    
    protected abstract void b();
    
    protected void b(View paramView)
    {
      if (a(paramView) == i()) {
        this.f = paramView;
      }
    }
    
    public void d(int paramInt)
    {
      this.a = paramInt;
    }
    
    @Nullable
    public RecyclerView.LayoutManager e()
    {
      return this.c;
    }
    
    public View e(int paramInt)
    {
      return this.b.f.c(paramInt);
    }
    
    protected final void f()
    {
      if (!this.e) {
        return;
      }
      b();
      RecyclerView.State.e(this.b.h, -1);
      this.f = null;
      this.a = -1;
      this.d = false;
      this.e = false;
      RecyclerView.LayoutManager.a(this.c, this);
      this.c = null;
      this.b = null;
    }
    
    public boolean g()
    {
      return this.d;
    }
    
    public boolean h()
    {
      return this.e;
    }
    
    public int i()
    {
      return this.a;
    }
    
    public int j()
    {
      return this.b.f.u();
    }
    
    public static class Action
    {
      private int a;
      private int b;
      private int c;
      private int d = -1;
      private Interpolator e;
      private boolean f = false;
      private int g = 0;
      
      public Action(int paramInt1, int paramInt2)
      {
        this(paramInt1, paramInt2, Integer.MIN_VALUE, null);
      }
      
      public Action(int paramInt1, int paramInt2, int paramInt3, Interpolator paramInterpolator)
      {
        this.a = paramInt1;
        this.b = paramInt2;
        this.c = paramInt3;
        this.e = paramInterpolator;
      }
      
      private void a(RecyclerView paramRecyclerView)
      {
        if (this.d >= 0)
        {
          int i = this.d;
          this.d = -1;
          RecyclerView.c(paramRecyclerView, i);
          this.f = false;
          return;
        }
        if (this.f)
        {
          b();
          if (this.e == null) {
            if (this.c == Integer.MIN_VALUE) {
              RecyclerView.u(paramRecyclerView).b(this.a, this.b);
            }
          }
          for (;;)
          {
            this.g += 1;
            if (this.g > 10) {
              Log.e("RecyclerView", "Smooth Scroll action is being updated too frequently. Make sure you are not changing it unless necessary");
            }
            this.f = false;
            return;
            RecyclerView.u(paramRecyclerView).a(this.a, this.b, this.c);
            continue;
            RecyclerView.u(paramRecyclerView).a(this.a, this.b, this.c, this.e);
          }
        }
        this.g = 0;
      }
      
      private void b()
      {
        if ((this.e != null) && (this.c < 1)) {
          throw new IllegalStateException("If you provide an interpolator, you must set a positive duration");
        }
        if (this.c < 1) {
          throw new IllegalStateException("Scroll duration must be a positive number");
        }
      }
      
      public void a(int paramInt)
      {
        this.d = paramInt;
      }
      
      public void a(int paramInt1, int paramInt2, int paramInt3, Interpolator paramInterpolator)
      {
        this.a = paramInt1;
        this.b = paramInt2;
        this.c = paramInt3;
        this.e = paramInterpolator;
        this.f = true;
      }
      
      boolean a()
      {
        return this.d >= 0;
      }
    }
  }
  
  public static class State
  {
    int a = 0;
    private int b = -1;
    private int c = 1;
    private SparseArray<Object> d;
    private int e = 0;
    private int f = 0;
    private boolean g = false;
    private boolean h = false;
    private boolean i = false;
    private boolean j = false;
    private boolean k = false;
    private boolean l = false;
    
    void a(int paramInt)
    {
      if ((this.c & paramInt) == 0) {
        throw new IllegalStateException("Layout state should be one of " + Integer.toBinaryString(paramInt) + " but it is " + Integer.toBinaryString(this.c));
      }
    }
    
    public boolean a()
    {
      return this.h;
    }
    
    public boolean b()
    {
      return this.j;
    }
    
    public int c()
    {
      return this.b;
    }
    
    public boolean d()
    {
      return this.b != -1;
    }
    
    public int e()
    {
      if (this.h) {
        return this.e - this.f;
      }
      return this.a;
    }
    
    public String toString()
    {
      return "State{mTargetPosition=" + this.b + ", mData=" + this.d + ", mItemCount=" + this.a + ", mPreviousLayoutItemCount=" + this.e + ", mDeletedInvisibleItemCountSincePreviousLayout=" + this.f + ", mStructureChanged=" + this.g + ", mInPreLayout=" + this.h + ", mRunSimpleAnimations=" + this.i + ", mRunPredictiveAnimations=" + this.j + '}';
    }
  }
  
  public static abstract class ViewCacheExtension
  {
    public abstract View a(RecyclerView.Recycler paramRecycler, int paramInt1, int paramInt2);
  }
  
  private class ViewFlinger
    implements Runnable
  {
    private int b;
    private int c;
    private ScrollerCompat d = ScrollerCompat.a(RecyclerView.this.getContext(), RecyclerView.q());
    private Interpolator e = RecyclerView.q();
    private boolean f = false;
    private boolean g = false;
    
    public ViewFlinger() {}
    
    private float a(float paramFloat)
    {
      return (float)Math.sin((float)((paramFloat - 0.5F) * 0.4712389167638204D));
    }
    
    private int b(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      int j = Math.abs(paramInt1);
      int k = Math.abs(paramInt2);
      int i;
      if (j > k)
      {
        i = 1;
        paramInt3 = (int)Math.sqrt(paramInt3 * paramInt3 + paramInt4 * paramInt4);
        paramInt2 = (int)Math.sqrt(paramInt1 * paramInt1 + paramInt2 * paramInt2);
        if (i == 0) {
          break label140;
        }
      }
      label140:
      for (paramInt1 = RecyclerView.this.getWidth();; paramInt1 = RecyclerView.this.getHeight())
      {
        paramInt4 = paramInt1 / 2;
        float f3 = Math.min(1.0F, 1.0F * paramInt2 / paramInt1);
        float f1 = paramInt4;
        float f2 = paramInt4;
        f3 = a(f3);
        if (paramInt3 <= 0) {
          break label151;
        }
        paramInt1 = Math.round(1000.0F * Math.abs((f1 + f2 * f3) / paramInt3)) * 4;
        return Math.min(paramInt1, 2000);
        i = 0;
        break;
      }
      label151:
      if (i != 0) {}
      for (paramInt2 = j;; paramInt2 = k)
      {
        paramInt1 = (int)((paramInt2 / paramInt1 + 1.0F) * 300.0F);
        break;
      }
    }
    
    private void c()
    {
      this.g = false;
      this.f = true;
    }
    
    private void d()
    {
      this.f = false;
      if (this.g) {
        a();
      }
    }
    
    void a()
    {
      if (this.f)
      {
        this.g = true;
        return;
      }
      RecyclerView.this.removeCallbacks(this);
      ViewCompat.a(RecyclerView.this, this);
    }
    
    public void a(int paramInt1, int paramInt2)
    {
      RecyclerView.b(RecyclerView.this, 2);
      this.c = 0;
      this.b = 0;
      this.d.a(0, 0, paramInt1, paramInt2, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE);
      a();
    }
    
    public void a(int paramInt1, int paramInt2, int paramInt3)
    {
      a(paramInt1, paramInt2, paramInt3, RecyclerView.q());
    }
    
    public void a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      a(paramInt1, paramInt2, b(paramInt1, paramInt2, paramInt3, paramInt4));
    }
    
    public void a(int paramInt1, int paramInt2, int paramInt3, Interpolator paramInterpolator)
    {
      if (this.e != paramInterpolator)
      {
        this.e = paramInterpolator;
        this.d = ScrollerCompat.a(RecyclerView.this.getContext(), paramInterpolator);
      }
      RecyclerView.b(RecyclerView.this, 2);
      this.c = 0;
      this.b = 0;
      this.d.a(0, 0, paramInt1, paramInt2, paramInt3);
      a();
    }
    
    public void b()
    {
      RecyclerView.this.removeCallbacks(this);
      this.d.h();
    }
    
    public void b(int paramInt1, int paramInt2)
    {
      a(paramInt1, paramInt2, 0, 0);
    }
    
    public void run()
    {
      if (RecyclerView.this.f == null)
      {
        b();
        return;
      }
      c();
      RecyclerView.c(RecyclerView.this);
      ScrollerCompat localScrollerCompat = this.d;
      RecyclerView.SmoothScroller localSmoothScroller = RecyclerView.this.f.r;
      int i4;
      int i5;
      int n;
      int i;
      int i3;
      int m;
      int i1;
      int j;
      int i2;
      int k;
      if (localScrollerCompat.g())
      {
        int i6 = localScrollerCompat.b();
        int i7 = localScrollerCompat.c();
        i4 = i6 - this.b;
        i5 = i7 - this.c;
        n = 0;
        i = 0;
        i3 = 0;
        m = 0;
        this.b = i6;
        this.c = i7;
        i1 = 0;
        j = 0;
        i2 = 0;
        k = 0;
        if (RecyclerView.f(RecyclerView.this) != null)
        {
          RecyclerView.this.b();
          RecyclerView.g(RecyclerView.this);
          TraceCompat.a("RV Scroll");
          if (i4 != 0)
          {
            i = RecyclerView.this.f.a(i4, RecyclerView.this.b, RecyclerView.this.h);
            j = i4 - i;
          }
          if (i5 != 0)
          {
            m = RecyclerView.this.f.b(i5, RecyclerView.this.b, RecyclerView.this.h);
            k = i5 - m;
          }
          TraceCompat.a();
          RecyclerView.h(RecyclerView.this);
          RecyclerView.i(RecyclerView.this);
          RecyclerView.this.a(false);
          n = i;
          i1 = j;
          i2 = k;
          i3 = m;
          if (localSmoothScroller != null)
          {
            n = i;
            i1 = j;
            i2 = k;
            i3 = m;
            if (!localSmoothScroller.g())
            {
              n = i;
              i1 = j;
              i2 = k;
              i3 = m;
              if (localSmoothScroller.h())
              {
                n = RecyclerView.this.h.e();
                if (n != 0) {
                  break label667;
                }
                localSmoothScroller.f();
                i3 = m;
                i2 = k;
                i1 = j;
                n = i;
              }
            }
          }
        }
        if (!RecyclerView.j(RecyclerView.this).isEmpty()) {
          RecyclerView.this.invalidate();
        }
        if (ViewCompat.a(RecyclerView.this) != 2) {
          RecyclerView.a(RecyclerView.this, i4, i5);
        }
        if ((i1 != 0) || (i2 != 0))
        {
          k = (int)localScrollerCompat.f();
          i = 0;
          if (i1 != i6)
          {
            if (i1 >= 0) {
              break label744;
            }
            i = -k;
          }
          label418:
          j = 0;
          if (i2 != i7)
          {
            if (i2 >= 0) {
              break label759;
            }
            j = -k;
          }
          label435:
          if (ViewCompat.a(RecyclerView.this) != 2) {
            RecyclerView.this.c(i, j);
          }
          if (((i != 0) || (i1 == i6) || (localScrollerCompat.d() == 0)) && ((j != 0) || (i2 == i7) || (localScrollerCompat.e() == 0))) {
            localScrollerCompat.h();
          }
        }
        if ((n != 0) || (i3 != 0)) {
          RecyclerView.this.h(n, i3);
        }
        if (!RecyclerView.k(RecyclerView.this)) {
          RecyclerView.this.invalidate();
        }
        if ((i5 == 0) || (!RecyclerView.this.f.f()) || (i3 != i5)) {
          break label774;
        }
        i = 1;
        label563:
        if ((i4 == 0) || (!RecyclerView.this.f.e()) || (n != i4)) {
          break label779;
        }
        j = 1;
        label590:
        if (((i4 != 0) || (i5 != 0)) && (j == 0) && (i == 0)) {
          break label784;
        }
        i = 1;
        label610:
        if ((!localScrollerCompat.a()) && (i != 0)) {
          break label789;
        }
        RecyclerView.b(RecyclerView.this, 0);
      }
      for (;;)
      {
        if (localSmoothScroller != null)
        {
          if (localSmoothScroller.g()) {
            RecyclerView.SmoothScroller.a(localSmoothScroller, 0, 0);
          }
          if (!this.g) {
            localSmoothScroller.f();
          }
        }
        d();
        return;
        label667:
        if (localSmoothScroller.i() >= n)
        {
          localSmoothScroller.d(n - 1);
          RecyclerView.SmoothScroller.a(localSmoothScroller, i4 - j, i5 - k);
          n = i;
          i1 = j;
          i2 = k;
          i3 = m;
          break;
        }
        RecyclerView.SmoothScroller.a(localSmoothScroller, i4 - j, i5 - k);
        n = i;
        i1 = j;
        i2 = k;
        i3 = m;
        break;
        label744:
        if (i1 > 0)
        {
          i = k;
          break label418;
        }
        i = 0;
        break label418;
        label759:
        if (i2 > 0)
        {
          j = k;
          break label435;
        }
        j = 0;
        break label435;
        label774:
        i = 0;
        break label563;
        label779:
        j = 0;
        break label590;
        label784:
        i = 0;
        break label610;
        label789:
        a();
      }
    }
  }
  
  public static abstract class ViewHolder
  {
    static final int FLAG_ADAPTER_FULLUPDATE = 1024;
    static final int FLAG_ADAPTER_POSITION_UNKNOWN = 512;
    static final int FLAG_APPEARED_IN_PRE_LAYOUT = 4096;
    static final int FLAG_BOUNCED_FROM_HIDDEN_LIST = 8192;
    static final int FLAG_BOUND = 1;
    static final int FLAG_IGNORE = 128;
    static final int FLAG_INVALID = 4;
    static final int FLAG_MOVED = 2048;
    static final int FLAG_NOT_RECYCLABLE = 16;
    static final int FLAG_REMOVED = 8;
    static final int FLAG_RETURNED_FROM_SCRAP = 32;
    static final int FLAG_TMP_DETACHED = 256;
    static final int FLAG_UPDATE = 2;
    private static final List<Object> FULLUPDATE_PAYLOADS = Collections.EMPTY_LIST;
    public final View itemView;
    private int mFlags;
    private boolean mInChangeScrap = false;
    private int mIsRecyclableCount = 0;
    long mItemId = -1L;
    int mItemViewType = -1;
    int mOldPosition = -1;
    RecyclerView mOwnerRecyclerView;
    List<Object> mPayloads = null;
    int mPosition = -1;
    int mPreLayoutPosition = -1;
    private RecyclerView.Recycler mScrapContainer = null;
    ViewHolder mShadowedHolder = null;
    ViewHolder mShadowingHolder = null;
    List<Object> mUnmodifiedPayloads = null;
    private int mWasImportantForAccessibilityBeforeHidden = 0;
    
    public ViewHolder(View paramView)
    {
      if (paramView == null) {
        throw new IllegalArgumentException("itemView may not be null");
      }
      this.itemView = paramView;
    }
    
    private void createPayloadsIfNeeded()
    {
      if (this.mPayloads == null)
      {
        this.mPayloads = new ArrayList();
        this.mUnmodifiedPayloads = Collections.unmodifiableList(this.mPayloads);
      }
    }
    
    private boolean doesTransientStatePreventRecycling()
    {
      return ((this.mFlags & 0x10) == 0) && (ViewCompat.c(this.itemView));
    }
    
    private void onEnteredHiddenState()
    {
      this.mWasImportantForAccessibilityBeforeHidden = ViewCompat.e(this.itemView);
      ViewCompat.c(this.itemView, 4);
    }
    
    private void onLeftHiddenState()
    {
      ViewCompat.c(this.itemView, this.mWasImportantForAccessibilityBeforeHidden);
      this.mWasImportantForAccessibilityBeforeHidden = 0;
    }
    
    private boolean shouldBeKeptAsChild()
    {
      return (this.mFlags & 0x10) != 0;
    }
    
    void addChangePayload(Object paramObject)
    {
      if (paramObject == null) {
        addFlags(1024);
      }
      while ((this.mFlags & 0x400) != 0) {
        return;
      }
      createPayloadsIfNeeded();
      this.mPayloads.add(paramObject);
    }
    
    void addFlags(int paramInt)
    {
      this.mFlags |= paramInt;
    }
    
    void clearOldPosition()
    {
      this.mOldPosition = -1;
      this.mPreLayoutPosition = -1;
    }
    
    void clearPayload()
    {
      if (this.mPayloads != null) {
        this.mPayloads.clear();
      }
      this.mFlags &= 0xFBFF;
    }
    
    void clearReturnedFromScrapFlag()
    {
      this.mFlags &= 0xFFFFFFDF;
    }
    
    void clearTmpDetachFlag()
    {
      this.mFlags &= 0xFEFF;
    }
    
    void flagRemovedAndOffsetPosition(int paramInt1, int paramInt2, boolean paramBoolean)
    {
      addFlags(8);
      offsetPosition(paramInt2, paramBoolean);
      this.mPosition = paramInt1;
    }
    
    public final int getAdapterPosition()
    {
      if (this.mOwnerRecyclerView == null) {
        return -1;
      }
      return RecyclerView.b(this.mOwnerRecyclerView, this);
    }
    
    public final long getItemId()
    {
      return this.mItemId;
    }
    
    public final int getItemViewType()
    {
      return this.mItemViewType;
    }
    
    public final int getLayoutPosition()
    {
      if (this.mPreLayoutPosition == -1) {
        return this.mPosition;
      }
      return this.mPreLayoutPosition;
    }
    
    public final int getOldPosition()
    {
      return this.mOldPosition;
    }
    
    @Deprecated
    public final int getPosition()
    {
      if (this.mPreLayoutPosition == -1) {
        return this.mPosition;
      }
      return this.mPreLayoutPosition;
    }
    
    List<Object> getUnmodifiedPayloads()
    {
      if ((this.mFlags & 0x400) == 0)
      {
        if ((this.mPayloads == null) || (this.mPayloads.size() == 0)) {
          return FULLUPDATE_PAYLOADS;
        }
        return this.mUnmodifiedPayloads;
      }
      return FULLUPDATE_PAYLOADS;
    }
    
    boolean hasAnyOfTheFlags(int paramInt)
    {
      return (this.mFlags & paramInt) != 0;
    }
    
    boolean isAdapterPositionUnknown()
    {
      return ((this.mFlags & 0x200) != 0) || (isInvalid());
    }
    
    boolean isBound()
    {
      return (this.mFlags & 0x1) != 0;
    }
    
    boolean isInvalid()
    {
      return (this.mFlags & 0x4) != 0;
    }
    
    public final boolean isRecyclable()
    {
      return ((this.mFlags & 0x10) == 0) && (!ViewCompat.c(this.itemView));
    }
    
    boolean isRemoved()
    {
      return (this.mFlags & 0x8) != 0;
    }
    
    boolean isScrap()
    {
      return this.mScrapContainer != null;
    }
    
    boolean isTmpDetached()
    {
      return (this.mFlags & 0x100) != 0;
    }
    
    boolean isUpdated()
    {
      return (this.mFlags & 0x2) != 0;
    }
    
    boolean needsUpdate()
    {
      return (this.mFlags & 0x2) != 0;
    }
    
    void offsetPosition(int paramInt, boolean paramBoolean)
    {
      if (this.mOldPosition == -1) {
        this.mOldPosition = this.mPosition;
      }
      if (this.mPreLayoutPosition == -1) {
        this.mPreLayoutPosition = this.mPosition;
      }
      if (paramBoolean) {
        this.mPreLayoutPosition += paramInt;
      }
      this.mPosition += paramInt;
      if (this.itemView.getLayoutParams() != null) {
        ((RecyclerView.LayoutParams)this.itemView.getLayoutParams()).c = true;
      }
    }
    
    void resetInternal()
    {
      this.mFlags = 0;
      this.mPosition = -1;
      this.mOldPosition = -1;
      this.mItemId = -1L;
      this.mPreLayoutPosition = -1;
      this.mIsRecyclableCount = 0;
      this.mShadowedHolder = null;
      this.mShadowingHolder = null;
      clearPayload();
      this.mWasImportantForAccessibilityBeforeHidden = 0;
    }
    
    void saveOldPosition()
    {
      if (this.mOldPosition == -1) {
        this.mOldPosition = this.mPosition;
      }
    }
    
    void setFlags(int paramInt1, int paramInt2)
    {
      this.mFlags = (this.mFlags & (paramInt2 ^ 0xFFFFFFFF) | paramInt1 & paramInt2);
    }
    
    public final void setIsRecyclable(boolean paramBoolean)
    {
      int i;
      if (paramBoolean)
      {
        i = this.mIsRecyclableCount - 1;
        this.mIsRecyclableCount = i;
        if (this.mIsRecyclableCount >= 0) {
          break label64;
        }
        this.mIsRecyclableCount = 0;
        Log.e("View", "isRecyclable decremented below 0: unmatched pair of setIsRecyable() calls for " + this);
      }
      label64:
      do
      {
        return;
        i = this.mIsRecyclableCount + 1;
        break;
        if ((!paramBoolean) && (this.mIsRecyclableCount == 1))
        {
          this.mFlags |= 0x10;
          return;
        }
      } while ((!paramBoolean) || (this.mIsRecyclableCount != 0));
      this.mFlags &= 0xFFFFFFEF;
    }
    
    void setScrapContainer(RecyclerView.Recycler paramRecycler, boolean paramBoolean)
    {
      this.mScrapContainer = paramRecycler;
      this.mInChangeScrap = paramBoolean;
    }
    
    boolean shouldIgnore()
    {
      return (this.mFlags & 0x80) != 0;
    }
    
    void stopIgnoring()
    {
      this.mFlags &= 0xFF7F;
    }
    
    public String toString()
    {
      StringBuilder localStringBuilder1 = new StringBuilder("ViewHolder{" + Integer.toHexString(hashCode()) + " position=" + this.mPosition + " id=" + this.mItemId + ", oldPos=" + this.mOldPosition + ", pLpos:" + this.mPreLayoutPosition);
      StringBuilder localStringBuilder2;
      if (isScrap())
      {
        localStringBuilder2 = localStringBuilder1.append(" scrap ");
        if (!this.mInChangeScrap) {
          break label295;
        }
      }
      label295:
      for (String str = "[changeScrap]";; str = "[attachedScrap]")
      {
        localStringBuilder2.append(str);
        if (isInvalid()) {
          localStringBuilder1.append(" invalid");
        }
        if (!isBound()) {
          localStringBuilder1.append(" unbound");
        }
        if (needsUpdate()) {
          localStringBuilder1.append(" update");
        }
        if (isRemoved()) {
          localStringBuilder1.append(" removed");
        }
        if (shouldIgnore()) {
          localStringBuilder1.append(" ignored");
        }
        if (isTmpDetached()) {
          localStringBuilder1.append(" tmpDetached");
        }
        if (!isRecyclable()) {
          localStringBuilder1.append(" not recyclable(" + this.mIsRecyclableCount + ")");
        }
        if (isAdapterPositionUnknown()) {
          localStringBuilder1.append(" undefined adapter position");
        }
        if (this.itemView.getParent() == null) {
          localStringBuilder1.append(" no parent");
        }
        localStringBuilder1.append("}");
        return localStringBuilder1.toString();
      }
    }
    
    void unScrap()
    {
      this.mScrapContainer.d(this);
    }
    
    boolean wasReturnedFromScrap()
    {
      return (this.mFlags & 0x20) != 0;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/RecyclerView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */