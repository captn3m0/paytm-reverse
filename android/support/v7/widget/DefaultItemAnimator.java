package android.support.v7.widget;

import android.support.annotation.NonNull;
import android.support.v4.animation.AnimatorCompatHelper;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.view.View;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DefaultItemAnimator
  extends SimpleItemAnimator
{
  private ArrayList<RecyclerView.ViewHolder> b = new ArrayList();
  private ArrayList<RecyclerView.ViewHolder> c = new ArrayList();
  private ArrayList<MoveInfo> d = new ArrayList();
  private ArrayList<ChangeInfo> e = new ArrayList();
  private ArrayList<ArrayList<RecyclerView.ViewHolder>> f = new ArrayList();
  private ArrayList<ArrayList<MoveInfo>> g = new ArrayList();
  private ArrayList<ArrayList<ChangeInfo>> h = new ArrayList();
  private ArrayList<RecyclerView.ViewHolder> i = new ArrayList();
  private ArrayList<RecyclerView.ViewHolder> j = new ArrayList();
  private ArrayList<RecyclerView.ViewHolder> k = new ArrayList();
  private ArrayList<RecyclerView.ViewHolder> l = new ArrayList();
  
  private void a(final ChangeInfo paramChangeInfo)
  {
    final Object localObject1 = paramChangeInfo.a;
    if (localObject1 == null)
    {
      localObject1 = null;
      localObject2 = paramChangeInfo.b;
      if (localObject2 == null) {
        break label171;
      }
    }
    label171:
    for (final Object localObject2 = ((RecyclerView.ViewHolder)localObject2).itemView;; localObject2 = null)
    {
      if (localObject1 != null)
      {
        localObject1 = ViewCompat.s((View)localObject1).a(g());
        this.l.add(paramChangeInfo.a);
        ((ViewPropertyAnimatorCompat)localObject1).b(paramChangeInfo.e - paramChangeInfo.c);
        ((ViewPropertyAnimatorCompat)localObject1).c(paramChangeInfo.f - paramChangeInfo.d);
        ((ViewPropertyAnimatorCompat)localObject1).a(0.0F).a(new VpaListenerAdapter(paramChangeInfo)
        {
          public void a(View paramAnonymousView)
          {
            DefaultItemAnimator.this.b(paramChangeInfo.a, true);
          }
          
          public void b(View paramAnonymousView)
          {
            localObject1.a(null);
            ViewCompat.c(paramAnonymousView, 1.0F);
            ViewCompat.a(paramAnonymousView, 0.0F);
            ViewCompat.b(paramAnonymousView, 0.0F);
            DefaultItemAnimator.this.a(paramChangeInfo.a, true);
            DefaultItemAnimator.h(DefaultItemAnimator.this).remove(paramChangeInfo.a);
            DefaultItemAnimator.e(DefaultItemAnimator.this);
          }
        }).b();
      }
      if (localObject2 != null)
      {
        localObject1 = ViewCompat.s((View)localObject2);
        this.l.add(paramChangeInfo.b);
        ((ViewPropertyAnimatorCompat)localObject1).b(0.0F).c(0.0F).a(g()).a(1.0F).a(new VpaListenerAdapter(paramChangeInfo)
        {
          public void a(View paramAnonymousView)
          {
            DefaultItemAnimator.this.b(paramChangeInfo.b, false);
          }
          
          public void b(View paramAnonymousView)
          {
            localObject1.a(null);
            ViewCompat.c(localObject2, 1.0F);
            ViewCompat.a(localObject2, 0.0F);
            ViewCompat.b(localObject2, 0.0F);
            DefaultItemAnimator.this.a(paramChangeInfo.b, false);
            DefaultItemAnimator.h(DefaultItemAnimator.this).remove(paramChangeInfo.b);
            DefaultItemAnimator.e(DefaultItemAnimator.this);
          }
        }).b();
      }
      return;
      localObject1 = ((RecyclerView.ViewHolder)localObject1).itemView;
      break;
    }
  }
  
  private void a(List<ChangeInfo> paramList, RecyclerView.ViewHolder paramViewHolder)
  {
    int m = paramList.size() - 1;
    while (m >= 0)
    {
      ChangeInfo localChangeInfo = (ChangeInfo)paramList.get(m);
      if ((a(localChangeInfo, paramViewHolder)) && (localChangeInfo.a == null) && (localChangeInfo.b == null)) {
        paramList.remove(localChangeInfo);
      }
      m -= 1;
    }
  }
  
  private boolean a(ChangeInfo paramChangeInfo, RecyclerView.ViewHolder paramViewHolder)
  {
    boolean bool = false;
    if (paramChangeInfo.b == paramViewHolder) {
      paramChangeInfo.b = null;
    }
    for (;;)
    {
      ViewCompat.c(paramViewHolder.itemView, 1.0F);
      ViewCompat.a(paramViewHolder.itemView, 0.0F);
      ViewCompat.b(paramViewHolder.itemView, 0.0F);
      a(paramViewHolder, bool);
      return true;
      if (paramChangeInfo.a != paramViewHolder) {
        break;
      }
      paramChangeInfo.a = null;
      bool = true;
    }
    return false;
  }
  
  private void b(ChangeInfo paramChangeInfo)
  {
    if (paramChangeInfo.a != null) {
      a(paramChangeInfo, paramChangeInfo.a);
    }
    if (paramChangeInfo.b != null) {
      a(paramChangeInfo, paramChangeInfo.b);
    }
  }
  
  private void b(final RecyclerView.ViewHolder paramViewHolder, final int paramInt1, final int paramInt2, int paramInt3, int paramInt4)
  {
    final Object localObject = paramViewHolder.itemView;
    paramInt1 = paramInt3 - paramInt1;
    paramInt2 = paramInt4 - paramInt2;
    if (paramInt1 != 0) {
      ViewCompat.s((View)localObject).b(0.0F);
    }
    if (paramInt2 != 0) {
      ViewCompat.s((View)localObject).c(0.0F);
    }
    localObject = ViewCompat.s((View)localObject);
    this.j.add(paramViewHolder);
    ((ViewPropertyAnimatorCompat)localObject).a(d()).a(new VpaListenerAdapter(paramViewHolder)
    {
      public void a(View paramAnonymousView)
      {
        DefaultItemAnimator.this.l(paramViewHolder);
      }
      
      public void b(View paramAnonymousView)
      {
        localObject.a(null);
        DefaultItemAnimator.this.i(paramViewHolder);
        DefaultItemAnimator.g(DefaultItemAnimator.this).remove(paramViewHolder);
        DefaultItemAnimator.e(DefaultItemAnimator.this);
      }
      
      public void c(View paramAnonymousView)
      {
        if (paramInt1 != 0) {
          ViewCompat.a(paramAnonymousView, 0.0F);
        }
        if (paramInt2 != 0) {
          ViewCompat.b(paramAnonymousView, 0.0F);
        }
      }
    }).b();
  }
  
  private void j()
  {
    if (!b()) {
      h();
    }
  }
  
  private void t(final RecyclerView.ViewHolder paramViewHolder)
  {
    final ViewPropertyAnimatorCompat localViewPropertyAnimatorCompat = ViewCompat.s(paramViewHolder.itemView);
    this.k.add(paramViewHolder);
    localViewPropertyAnimatorCompat.a(f()).a(0.0F).a(new VpaListenerAdapter(paramViewHolder)
    {
      public void a(View paramAnonymousView)
      {
        DefaultItemAnimator.this.k(paramViewHolder);
      }
      
      public void b(View paramAnonymousView)
      {
        localViewPropertyAnimatorCompat.a(null);
        ViewCompat.c(paramAnonymousView, 1.0F);
        DefaultItemAnimator.this.h(paramViewHolder);
        DefaultItemAnimator.d(DefaultItemAnimator.this).remove(paramViewHolder);
        DefaultItemAnimator.e(DefaultItemAnimator.this);
      }
    }).b();
  }
  
  private void u(final RecyclerView.ViewHolder paramViewHolder)
  {
    final ViewPropertyAnimatorCompat localViewPropertyAnimatorCompat = ViewCompat.s(paramViewHolder.itemView);
    this.i.add(paramViewHolder);
    localViewPropertyAnimatorCompat.a(1.0F).a(e()).a(new VpaListenerAdapter(paramViewHolder)
    {
      public void a(View paramAnonymousView)
      {
        DefaultItemAnimator.this.m(paramViewHolder);
      }
      
      public void b(View paramAnonymousView)
      {
        localViewPropertyAnimatorCompat.a(null);
        DefaultItemAnimator.this.j(paramViewHolder);
        DefaultItemAnimator.f(DefaultItemAnimator.this).remove(paramViewHolder);
        DefaultItemAnimator.e(DefaultItemAnimator.this);
      }
      
      public void c(View paramAnonymousView)
      {
        ViewCompat.c(paramAnonymousView, 1.0F);
      }
    }).b();
  }
  
  private void v(RecyclerView.ViewHolder paramViewHolder)
  {
    AnimatorCompatHelper.a(paramViewHolder.itemView);
    c(paramViewHolder);
  }
  
  public void a()
  {
    int m;
    int n;
    label24:
    int i1;
    if (!this.b.isEmpty())
    {
      m = 1;
      if (this.d.isEmpty()) {
        break label72;
      }
      n = 1;
      if (this.e.isEmpty()) {
        break label77;
      }
      i1 = 1;
      label36:
      if (this.c.isEmpty()) {
        break label82;
      }
    }
    label72:
    label77:
    label82:
    for (int i2 = 1;; i2 = 0)
    {
      if ((m != 0) || (n != 0) || (i2 != 0) || (i1 != 0)) {
        break label88;
      }
      return;
      m = 0;
      break;
      n = 0;
      break label24;
      i1 = 0;
      break label36;
    }
    label88:
    final Object localObject1 = this.b.iterator();
    while (((Iterator)localObject1).hasNext()) {
      t((RecyclerView.ViewHolder)((Iterator)localObject1).next());
    }
    this.b.clear();
    Object localObject2;
    label211:
    label291:
    long l1;
    label366:
    long l2;
    if (n != 0)
    {
      localObject1 = new ArrayList();
      ((ArrayList)localObject1).addAll(this.d);
      this.g.add(localObject1);
      this.d.clear();
      localObject2 = new Runnable()
      {
        public void run()
        {
          Iterator localIterator = localObject1.iterator();
          while (localIterator.hasNext())
          {
            DefaultItemAnimator.MoveInfo localMoveInfo = (DefaultItemAnimator.MoveInfo)localIterator.next();
            DefaultItemAnimator.a(DefaultItemAnimator.this, localMoveInfo.a, localMoveInfo.b, localMoveInfo.c, localMoveInfo.d, localMoveInfo.e);
          }
          localObject1.clear();
          DefaultItemAnimator.a(DefaultItemAnimator.this).remove(localObject1);
        }
      };
      if (m != 0) {
        ViewCompat.a(((MoveInfo)((ArrayList)localObject1).get(0)).a.itemView, (Runnable)localObject2, f());
      }
    }
    else
    {
      if (i1 != 0)
      {
        localObject1 = new ArrayList();
        ((ArrayList)localObject1).addAll(this.e);
        this.h.add(localObject1);
        this.e.clear();
        localObject2 = new Runnable()
        {
          public void run()
          {
            Iterator localIterator = localObject1.iterator();
            while (localIterator.hasNext())
            {
              DefaultItemAnimator.ChangeInfo localChangeInfo = (DefaultItemAnimator.ChangeInfo)localIterator.next();
              DefaultItemAnimator.a(DefaultItemAnimator.this, localChangeInfo);
            }
            localObject1.clear();
            DefaultItemAnimator.b(DefaultItemAnimator.this).remove(localObject1);
          }
        };
        if (m == 0) {
          break label428;
        }
        ViewCompat.a(((ChangeInfo)((ArrayList)localObject1).get(0)).a.itemView, (Runnable)localObject2, f());
      }
      if (i2 == 0) {
        break label436;
      }
      localObject1 = new ArrayList();
      ((ArrayList)localObject1).addAll(this.c);
      this.f.add(localObject1);
      this.c.clear();
      localObject2 = new Runnable()
      {
        public void run()
        {
          Iterator localIterator = localObject1.iterator();
          while (localIterator.hasNext())
          {
            RecyclerView.ViewHolder localViewHolder = (RecyclerView.ViewHolder)localIterator.next();
            DefaultItemAnimator.a(DefaultItemAnimator.this, localViewHolder);
          }
          localObject1.clear();
          DefaultItemAnimator.c(DefaultItemAnimator.this).remove(localObject1);
        }
      };
      if ((m == 0) && (n == 0) && (i1 == 0)) {
        break label456;
      }
      if (m == 0) {
        break label438;
      }
      l1 = f();
      if (n == 0) {
        break label444;
      }
      l2 = d();
      label376:
      if (i1 == 0) {
        break label450;
      }
    }
    label428:
    label436:
    label438:
    label444:
    label450:
    for (long l3 = g();; l3 = 0L)
    {
      l2 = Math.max(l2, l3);
      ViewCompat.a(((RecyclerView.ViewHolder)((ArrayList)localObject1).get(0)).itemView, (Runnable)localObject2, l1 + l2);
      return;
      ((Runnable)localObject2).run();
      break label211;
      ((Runnable)localObject2).run();
      break label291;
      break;
      l1 = 0L;
      break label366;
      l2 = 0L;
      break label376;
    }
    label456:
    ((Runnable)localObject2).run();
  }
  
  void a(List<RecyclerView.ViewHolder> paramList)
  {
    int m = paramList.size() - 1;
    while (m >= 0)
    {
      ViewCompat.s(((RecyclerView.ViewHolder)paramList.get(m)).itemView).cancel();
      m -= 1;
    }
  }
  
  public boolean a(RecyclerView.ViewHolder paramViewHolder)
  {
    v(paramViewHolder);
    this.b.add(paramViewHolder);
    return true;
  }
  
  public boolean a(RecyclerView.ViewHolder paramViewHolder, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    View localView = paramViewHolder.itemView;
    paramInt1 = (int)(paramInt1 + ViewCompat.o(paramViewHolder.itemView));
    paramInt2 = (int)(paramInt2 + ViewCompat.p(paramViewHolder.itemView));
    v(paramViewHolder);
    int m = paramInt3 - paramInt1;
    int n = paramInt4 - paramInt2;
    if ((m == 0) && (n == 0))
    {
      i(paramViewHolder);
      return false;
    }
    if (m != 0) {
      ViewCompat.a(localView, -m);
    }
    if (n != 0) {
      ViewCompat.b(localView, -n);
    }
    this.d.add(new MoveInfo(paramViewHolder, paramInt1, paramInt2, paramInt3, paramInt4, null));
    return true;
  }
  
  public boolean a(RecyclerView.ViewHolder paramViewHolder1, RecyclerView.ViewHolder paramViewHolder2, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (paramViewHolder1 == paramViewHolder2) {
      return a(paramViewHolder1, paramInt1, paramInt2, paramInt3, paramInt4);
    }
    float f1 = ViewCompat.o(paramViewHolder1.itemView);
    float f2 = ViewCompat.p(paramViewHolder1.itemView);
    float f3 = ViewCompat.f(paramViewHolder1.itemView);
    v(paramViewHolder1);
    int m = (int)(paramInt3 - paramInt1 - f1);
    int n = (int)(paramInt4 - paramInt2 - f2);
    ViewCompat.a(paramViewHolder1.itemView, f1);
    ViewCompat.b(paramViewHolder1.itemView, f2);
    ViewCompat.c(paramViewHolder1.itemView, f3);
    if (paramViewHolder2 != null)
    {
      v(paramViewHolder2);
      ViewCompat.a(paramViewHolder2.itemView, -m);
      ViewCompat.b(paramViewHolder2.itemView, -n);
      ViewCompat.c(paramViewHolder2.itemView, 0.0F);
    }
    this.e.add(new ChangeInfo(paramViewHolder1, paramViewHolder2, paramInt1, paramInt2, paramInt3, paramInt4, null));
    return true;
  }
  
  public boolean a(@NonNull RecyclerView.ViewHolder paramViewHolder, @NonNull List<Object> paramList)
  {
    return (!paramList.isEmpty()) || (super.a(paramViewHolder, paramList));
  }
  
  public boolean b()
  {
    return (!this.c.isEmpty()) || (!this.e.isEmpty()) || (!this.d.isEmpty()) || (!this.b.isEmpty()) || (!this.j.isEmpty()) || (!this.k.isEmpty()) || (!this.i.isEmpty()) || (!this.l.isEmpty()) || (!this.g.isEmpty()) || (!this.f.isEmpty()) || (!this.h.isEmpty());
  }
  
  public boolean b(RecyclerView.ViewHolder paramViewHolder)
  {
    v(paramViewHolder);
    ViewCompat.c(paramViewHolder.itemView, 0.0F);
    this.c.add(paramViewHolder);
    return true;
  }
  
  public void c()
  {
    int m = this.d.size() - 1;
    Object localObject1;
    Object localObject2;
    while (m >= 0)
    {
      localObject1 = (MoveInfo)this.d.get(m);
      localObject2 = ((MoveInfo)localObject1).a.itemView;
      ViewCompat.b((View)localObject2, 0.0F);
      ViewCompat.a((View)localObject2, 0.0F);
      i(((MoveInfo)localObject1).a);
      this.d.remove(m);
      m -= 1;
    }
    m = this.b.size() - 1;
    while (m >= 0)
    {
      h((RecyclerView.ViewHolder)this.b.get(m));
      this.b.remove(m);
      m -= 1;
    }
    m = this.c.size() - 1;
    while (m >= 0)
    {
      localObject1 = (RecyclerView.ViewHolder)this.c.get(m);
      ViewCompat.c(((RecyclerView.ViewHolder)localObject1).itemView, 1.0F);
      j((RecyclerView.ViewHolder)localObject1);
      this.c.remove(m);
      m -= 1;
    }
    m = this.e.size() - 1;
    while (m >= 0)
    {
      b((ChangeInfo)this.e.get(m));
      m -= 1;
    }
    this.e.clear();
    if (!b()) {
      return;
    }
    m = this.g.size() - 1;
    int n;
    while (m >= 0)
    {
      localObject1 = (ArrayList)this.g.get(m);
      n = ((ArrayList)localObject1).size() - 1;
      while (n >= 0)
      {
        localObject2 = (MoveInfo)((ArrayList)localObject1).get(n);
        View localView = ((MoveInfo)localObject2).a.itemView;
        ViewCompat.b(localView, 0.0F);
        ViewCompat.a(localView, 0.0F);
        i(((MoveInfo)localObject2).a);
        ((ArrayList)localObject1).remove(n);
        if (((ArrayList)localObject1).isEmpty()) {
          this.g.remove(localObject1);
        }
        n -= 1;
      }
      m -= 1;
    }
    m = this.f.size() - 1;
    while (m >= 0)
    {
      localObject1 = (ArrayList)this.f.get(m);
      n = ((ArrayList)localObject1).size() - 1;
      while (n >= 0)
      {
        localObject2 = (RecyclerView.ViewHolder)((ArrayList)localObject1).get(n);
        ViewCompat.c(((RecyclerView.ViewHolder)localObject2).itemView, 1.0F);
        j((RecyclerView.ViewHolder)localObject2);
        ((ArrayList)localObject1).remove(n);
        if (((ArrayList)localObject1).isEmpty()) {
          this.f.remove(localObject1);
        }
        n -= 1;
      }
      m -= 1;
    }
    m = this.h.size() - 1;
    while (m >= 0)
    {
      localObject1 = (ArrayList)this.h.get(m);
      n = ((ArrayList)localObject1).size() - 1;
      while (n >= 0)
      {
        b((ChangeInfo)((ArrayList)localObject1).get(n));
        if (((ArrayList)localObject1).isEmpty()) {
          this.h.remove(localObject1);
        }
        n -= 1;
      }
      m -= 1;
    }
    a(this.k);
    a(this.j);
    a(this.i);
    a(this.l);
    h();
  }
  
  public void c(RecyclerView.ViewHolder paramViewHolder)
  {
    View localView = paramViewHolder.itemView;
    ViewCompat.s(localView).cancel();
    int m = this.d.size() - 1;
    while (m >= 0)
    {
      if (((MoveInfo)this.d.get(m)).a == paramViewHolder)
      {
        ViewCompat.b(localView, 0.0F);
        ViewCompat.a(localView, 0.0F);
        i(paramViewHolder);
        this.d.remove(m);
      }
      m -= 1;
    }
    a(this.e, paramViewHolder);
    if (this.b.remove(paramViewHolder))
    {
      ViewCompat.c(localView, 1.0F);
      h(paramViewHolder);
    }
    if (this.c.remove(paramViewHolder))
    {
      ViewCompat.c(localView, 1.0F);
      j(paramViewHolder);
    }
    m = this.h.size() - 1;
    ArrayList localArrayList;
    while (m >= 0)
    {
      localArrayList = (ArrayList)this.h.get(m);
      a(localArrayList, paramViewHolder);
      if (localArrayList.isEmpty()) {
        this.h.remove(m);
      }
      m -= 1;
    }
    m = this.g.size() - 1;
    if (m >= 0)
    {
      localArrayList = (ArrayList)this.g.get(m);
      int n = localArrayList.size() - 1;
      for (;;)
      {
        if (n >= 0)
        {
          if (((MoveInfo)localArrayList.get(n)).a != paramViewHolder) {
            break label293;
          }
          ViewCompat.b(localView, 0.0F);
          ViewCompat.a(localView, 0.0F);
          i(paramViewHolder);
          localArrayList.remove(n);
          if (localArrayList.isEmpty()) {
            this.g.remove(m);
          }
        }
        m -= 1;
        break;
        label293:
        n -= 1;
      }
    }
    m = this.f.size() - 1;
    while (m >= 0)
    {
      localArrayList = (ArrayList)this.f.get(m);
      if (localArrayList.remove(paramViewHolder))
      {
        ViewCompat.c(localView, 1.0F);
        j(paramViewHolder);
        if (localArrayList.isEmpty()) {
          this.f.remove(m);
        }
      }
      m -= 1;
    }
    if ((!this.k.remove(paramViewHolder)) || ((!this.i.remove(paramViewHolder)) || ((!this.l.remove(paramViewHolder)) || (this.j.remove(paramViewHolder))))) {}
    j();
  }
  
  private static class ChangeInfo
  {
    public RecyclerView.ViewHolder a;
    public RecyclerView.ViewHolder b;
    public int c;
    public int d;
    public int e;
    public int f;
    
    private ChangeInfo(RecyclerView.ViewHolder paramViewHolder1, RecyclerView.ViewHolder paramViewHolder2)
    {
      this.a = paramViewHolder1;
      this.b = paramViewHolder2;
    }
    
    private ChangeInfo(RecyclerView.ViewHolder paramViewHolder1, RecyclerView.ViewHolder paramViewHolder2, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      this(paramViewHolder1, paramViewHolder2);
      this.c = paramInt1;
      this.d = paramInt2;
      this.e = paramInt3;
      this.f = paramInt4;
    }
    
    public String toString()
    {
      return "ChangeInfo{oldHolder=" + this.a + ", newHolder=" + this.b + ", fromX=" + this.c + ", fromY=" + this.d + ", toX=" + this.e + ", toY=" + this.f + '}';
    }
  }
  
  private static class MoveInfo
  {
    public RecyclerView.ViewHolder a;
    public int b;
    public int c;
    public int d;
    public int e;
    
    private MoveInfo(RecyclerView.ViewHolder paramViewHolder, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      this.a = paramViewHolder;
      this.b = paramInt1;
      this.c = paramInt2;
      this.d = paramInt3;
      this.e = paramInt4;
    }
  }
  
  private static class VpaListenerAdapter
    implements ViewPropertyAnimatorListener
  {
    public void a(View paramView) {}
    
    public void b(View paramView) {}
    
    public void c(View paramView) {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/DefaultItemAnimator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */