package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.graphics.drawable.LayerDrawable;
import android.os.Build.VERSION;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.ColorUtils;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.LongSparseArray;
import android.support.v4.util.LruCache;
import android.support.v7.appcompat.R.attr;
import android.support.v7.appcompat.R.drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import android.util.Xml;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public final class AppCompatDrawableManager
{
  private static final PorterDuff.Mode a = PorterDuff.Mode.SRC_IN;
  private static AppCompatDrawableManager b;
  private static final ColorFilterLruCache c = new ColorFilterLruCache(6);
  private static final int[] d = { R.drawable.abc_textfield_search_default_mtrl_alpha, R.drawable.abc_textfield_default_mtrl_alpha, R.drawable.abc_ab_share_pack_mtrl_alpha };
  private static final int[] e = { R.drawable.abc_ic_ab_back_mtrl_am_alpha, R.drawable.abc_ic_go_search_api_mtrl_alpha, R.drawable.abc_ic_search_api_mtrl_alpha, R.drawable.abc_ic_commit_search_api_mtrl_alpha, R.drawable.abc_ic_clear_mtrl_alpha, R.drawable.abc_ic_menu_share_mtrl_alpha, R.drawable.abc_ic_menu_copy_mtrl_am_alpha, R.drawable.abc_ic_menu_cut_mtrl_alpha, R.drawable.abc_ic_menu_selectall_mtrl_alpha, R.drawable.abc_ic_menu_paste_mtrl_am_alpha, R.drawable.abc_ic_menu_moreoverflow_mtrl_alpha, R.drawable.abc_ic_voice_search_api_mtrl_alpha };
  private static final int[] f = { R.drawable.abc_textfield_activated_mtrl_alpha, R.drawable.abc_textfield_search_activated_mtrl_alpha, R.drawable.abc_cab_background_top_mtrl_alpha, R.drawable.abc_text_cursor_material };
  private static final int[] g = { R.drawable.abc_popup_background_mtrl_mult, R.drawable.abc_cab_background_internal_bg, R.drawable.abc_menu_hardkey_panel_mtrl_mult };
  private static final int[] h = { R.drawable.abc_edit_text_material, R.drawable.abc_tab_indicator_material, R.drawable.abc_textfield_search_material, R.drawable.abc_spinner_mtrl_am_alpha, R.drawable.abc_spinner_textfield_background_material, R.drawable.abc_ratingbar_full_material, R.drawable.abc_switch_track_mtrl_alpha, R.drawable.abc_switch_thumb_material, R.drawable.abc_btn_default_mtrl_shape, R.drawable.abc_btn_borderless_material };
  private static final int[] i = { R.drawable.abc_btn_check_material, R.drawable.abc_btn_radio_material };
  private WeakHashMap<Context, SparseArray<ColorStateList>> j;
  private ArrayMap<String, InflateDelegate> k;
  private SparseArray<String> l;
  private final Object m = new Object();
  private final WeakHashMap<Context, LongSparseArray<WeakReference<Drawable.ConstantState>>> n = new WeakHashMap(0);
  private TypedValue o;
  
  private static long a(TypedValue paramTypedValue)
  {
    return paramTypedValue.assetCookie << 32 | paramTypedValue.data;
  }
  
  private ColorStateList a(Context paramContext)
  {
    int i1 = ThemeUtils.a(paramContext, R.attr.colorControlNormal);
    int i2 = ThemeUtils.a(paramContext, R.attr.colorControlActivated);
    int[][] arrayOfInt = new int[7][];
    int[] arrayOfInt1 = new int[7];
    arrayOfInt[0] = ThemeUtils.a;
    arrayOfInt1[0] = ThemeUtils.c(paramContext, R.attr.colorControlNormal);
    int i3 = 0 + 1;
    arrayOfInt[i3] = ThemeUtils.b;
    arrayOfInt1[i3] = i2;
    i3 += 1;
    arrayOfInt[i3] = ThemeUtils.c;
    arrayOfInt1[i3] = i2;
    i3 += 1;
    arrayOfInt[i3] = ThemeUtils.d;
    arrayOfInt1[i3] = i2;
    i3 += 1;
    arrayOfInt[i3] = ThemeUtils.e;
    arrayOfInt1[i3] = i2;
    i3 += 1;
    arrayOfInt[i3] = ThemeUtils.f;
    arrayOfInt1[i3] = i2;
    i2 = i3 + 1;
    arrayOfInt[i2] = ThemeUtils.h;
    arrayOfInt1[i2] = i1;
    return new ColorStateList(arrayOfInt, arrayOfInt1);
  }
  
  public static PorterDuffColorFilter a(int paramInt, PorterDuff.Mode paramMode)
  {
    PorterDuffColorFilter localPorterDuffColorFilter2 = c.a(paramInt, paramMode);
    PorterDuffColorFilter localPorterDuffColorFilter1 = localPorterDuffColorFilter2;
    if (localPorterDuffColorFilter2 == null)
    {
      localPorterDuffColorFilter1 = new PorterDuffColorFilter(paramInt, paramMode);
      c.a(paramInt, paramMode, localPorterDuffColorFilter1);
    }
    return localPorterDuffColorFilter1;
  }
  
  private static PorterDuffColorFilter a(ColorStateList paramColorStateList, PorterDuff.Mode paramMode, int[] paramArrayOfInt)
  {
    if ((paramColorStateList == null) || (paramMode == null)) {
      return null;
    }
    return a(paramColorStateList.getColorForState(paramArrayOfInt, 0), paramMode);
  }
  
  private Drawable a(@NonNull Context paramContext, @DrawableRes int paramInt, boolean paramBoolean, @NonNull Drawable paramDrawable)
  {
    Object localObject = b(paramContext, paramInt);
    if (localObject != null)
    {
      paramContext = paramDrawable;
      if (DrawableUtils.c(paramDrawable)) {
        paramContext = paramDrawable.mutate();
      }
      paramContext = DrawableCompat.f(paramContext);
      DrawableCompat.a(paramContext, (ColorStateList)localObject);
      paramDrawable = a(paramInt);
      localObject = paramContext;
      if (paramDrawable != null)
      {
        DrawableCompat.a(paramContext, paramDrawable);
        localObject = paramContext;
      }
    }
    do
    {
      do
      {
        return (Drawable)localObject;
        if (paramInt == R.drawable.abc_seekbar_track_material)
        {
          localObject = (LayerDrawable)paramDrawable;
          a(((LayerDrawable)localObject).findDrawableByLayerId(16908288), ThemeUtils.a(paramContext, R.attr.colorControlNormal), a);
          a(((LayerDrawable)localObject).findDrawableByLayerId(16908303), ThemeUtils.a(paramContext, R.attr.colorControlNormal), a);
          a(((LayerDrawable)localObject).findDrawableByLayerId(16908301), ThemeUtils.a(paramContext, R.attr.colorControlActivated), a);
          return paramDrawable;
        }
        if ((paramInt == R.drawable.abc_ratingbar_indicator_material) || (paramInt == R.drawable.abc_ratingbar_small_material))
        {
          localObject = (LayerDrawable)paramDrawable;
          a(((LayerDrawable)localObject).findDrawableByLayerId(16908288), ThemeUtils.c(paramContext, R.attr.colorControlNormal), a);
          a(((LayerDrawable)localObject).findDrawableByLayerId(16908303), ThemeUtils.a(paramContext, R.attr.colorControlActivated), a);
          a(((LayerDrawable)localObject).findDrawableByLayerId(16908301), ThemeUtils.a(paramContext, R.attr.colorControlActivated), a);
          return paramDrawable;
        }
        localObject = paramDrawable;
      } while (a(paramContext, paramInt, paramDrawable));
      localObject = paramDrawable;
    } while (!paramBoolean);
    return null;
  }
  
  private Drawable a(@NonNull Context paramContext, long paramLong)
  {
    LongSparseArray localLongSparseArray;
    synchronized (this.m)
    {
      localLongSparseArray = (LongSparseArray)this.n.get(paramContext);
      if (localLongSparseArray == null) {
        return null;
      }
      Object localObject2 = (WeakReference)localLongSparseArray.a(paramLong);
      if (localObject2 == null) {
        break label90;
      }
      localObject2 = (Drawable.ConstantState)((WeakReference)localObject2).get();
      if (localObject2 != null)
      {
        paramContext = ((Drawable.ConstantState)localObject2).newDrawable(paramContext.getResources());
        return paramContext;
      }
    }
    localLongSparseArray.b(paramLong);
    label90:
    return null;
  }
  
  public static AppCompatDrawableManager a()
  {
    if (b == null)
    {
      b = new AppCompatDrawableManager();
      a(b);
    }
    return b;
  }
  
  private void a(@NonNull Context paramContext, @DrawableRes int paramInt, @NonNull ColorStateList paramColorStateList)
  {
    if (this.j == null) {
      this.j = new WeakHashMap();
    }
    SparseArray localSparseArray2 = (SparseArray)this.j.get(paramContext);
    SparseArray localSparseArray1 = localSparseArray2;
    if (localSparseArray2 == null)
    {
      localSparseArray1 = new SparseArray();
      this.j.put(paramContext, localSparseArray1);
    }
    localSparseArray1.append(paramInt, paramColorStateList);
  }
  
  private static void a(Drawable paramDrawable, int paramInt, PorterDuff.Mode paramMode)
  {
    Drawable localDrawable = paramDrawable;
    if (DrawableUtils.c(paramDrawable)) {
      localDrawable = paramDrawable.mutate();
    }
    paramDrawable = paramMode;
    if (paramMode == null) {
      paramDrawable = a;
    }
    localDrawable.setColorFilter(a(paramInt, paramDrawable));
  }
  
  public static void a(Drawable paramDrawable, TintInfo paramTintInfo, int[] paramArrayOfInt)
  {
    if ((DrawableUtils.c(paramDrawable)) && (paramDrawable.mutate() != paramDrawable)) {
      Log.d("AppCompatDrawableManager", "Mutated drawable is not the same instance as the input.");
    }
    label64:
    label92:
    label104:
    for (;;)
    {
      return;
      ColorStateList localColorStateList;
      if ((paramTintInfo.d) || (paramTintInfo.c)) {
        if (paramTintInfo.d)
        {
          localColorStateList = paramTintInfo.a;
          if (!paramTintInfo.c) {
            break label92;
          }
          paramTintInfo = paramTintInfo.b;
          paramDrawable.setColorFilter(a(localColorStateList, paramTintInfo, paramArrayOfInt));
        }
      }
      for (;;)
      {
        if (Build.VERSION.SDK_INT > 23) {
          break label104;
        }
        paramDrawable.invalidateSelf();
        return;
        localColorStateList = null;
        break;
        paramTintInfo = a;
        break label64;
        paramDrawable.clearColorFilter();
      }
    }
  }
  
  private static void a(@NonNull AppCompatDrawableManager paramAppCompatDrawableManager)
  {
    int i1 = Build.VERSION.SDK_INT;
    if (i1 < 23)
    {
      paramAppCompatDrawableManager.a("vector", new VdcInflateDelegate(null));
      if (i1 >= 11) {
        paramAppCompatDrawableManager.a("animated-vector", new AvdcInflateDelegate(null));
      }
    }
  }
  
  private void a(@NonNull String paramString, @NonNull InflateDelegate paramInflateDelegate)
  {
    if (this.k == null) {
      this.k = new ArrayMap();
    }
    this.k.put(paramString, paramInflateDelegate);
  }
  
  static boolean a(@NonNull Context paramContext, @DrawableRes int paramInt, @NonNull Drawable paramDrawable)
  {
    Object localObject2 = a;
    int i2 = 0;
    int i1 = 0;
    int i3 = -1;
    Object localObject1;
    if (a(d, paramInt))
    {
      i1 = R.attr.colorControlNormal;
      i2 = 1;
      localObject1 = localObject2;
    }
    while (i2 != 0)
    {
      localObject2 = paramDrawable;
      if (DrawableUtils.c(paramDrawable)) {
        localObject2 = paramDrawable.mutate();
      }
      ((Drawable)localObject2).setColorFilter(a(ThemeUtils.a(paramContext, i1), (PorterDuff.Mode)localObject1));
      if (i3 != -1) {
        ((Drawable)localObject2).setAlpha(i3);
      }
      return true;
      if (a(f, paramInt))
      {
        i1 = R.attr.colorControlActivated;
        i2 = 1;
        localObject1 = localObject2;
      }
      else if (a(g, paramInt))
      {
        i1 = 16842801;
        i2 = 1;
        localObject1 = PorterDuff.Mode.MULTIPLY;
      }
      else
      {
        localObject1 = localObject2;
        if (paramInt == R.drawable.abc_list_divider_mtrl_alpha)
        {
          i1 = 16842800;
          i2 = 1;
          i3 = Math.round(40.8F);
          localObject1 = localObject2;
        }
      }
    }
    return false;
  }
  
  private boolean a(@NonNull Context paramContext, long paramLong, @NonNull Drawable paramDrawable)
  {
    Drawable.ConstantState localConstantState = paramDrawable.getConstantState();
    if (localConstantState != null) {
      synchronized (this.m)
      {
        LongSparseArray localLongSparseArray = (LongSparseArray)this.n.get(paramContext);
        paramDrawable = localLongSparseArray;
        if (localLongSparseArray == null)
        {
          paramDrawable = new LongSparseArray();
          this.n.put(paramContext, paramDrawable);
        }
        paramDrawable.b(paramLong, new WeakReference(localConstantState));
        return true;
      }
    }
    return false;
  }
  
  private static boolean a(int[] paramArrayOfInt, int paramInt)
  {
    int i2 = paramArrayOfInt.length;
    int i1 = 0;
    while (i1 < i2)
    {
      if (paramArrayOfInt[i1] == paramInt) {
        return true;
      }
      i1 += 1;
    }
    return false;
  }
  
  private ColorStateList b(Context paramContext)
  {
    int[][] arrayOfInt = new int[3][];
    int[] arrayOfInt1 = new int[3];
    arrayOfInt[0] = ThemeUtils.a;
    arrayOfInt1[0] = ThemeUtils.c(paramContext, R.attr.colorControlNormal);
    int i1 = 0 + 1;
    arrayOfInt[i1] = ThemeUtils.e;
    arrayOfInt1[i1] = ThemeUtils.a(paramContext, R.attr.colorControlActivated);
    i1 += 1;
    arrayOfInt[i1] = ThemeUtils.h;
    arrayOfInt1[i1] = ThemeUtils.a(paramContext, R.attr.colorControlNormal);
    return new ColorStateList(arrayOfInt, arrayOfInt1);
  }
  
  private ColorStateList c(Context paramContext)
  {
    int[][] arrayOfInt = new int[3][];
    int[] arrayOfInt1 = new int[3];
    arrayOfInt[0] = ThemeUtils.a;
    arrayOfInt1[0] = ThemeUtils.a(paramContext, 16842800, 0.1F);
    int i1 = 0 + 1;
    arrayOfInt[i1] = ThemeUtils.e;
    arrayOfInt1[i1] = ThemeUtils.a(paramContext, R.attr.colorControlActivated, 0.3F);
    i1 += 1;
    arrayOfInt[i1] = ThemeUtils.h;
    arrayOfInt1[i1] = ThemeUtils.a(paramContext, 16842800, 0.3F);
    return new ColorStateList(arrayOfInt, arrayOfInt1);
  }
  
  private Drawable c(@NonNull Context paramContext, @DrawableRes int paramInt)
  {
    if (this.o == null) {
      this.o = new TypedValue();
    }
    TypedValue localTypedValue = this.o;
    paramContext.getResources().getValue(paramInt, localTypedValue, true);
    long l1 = a(localTypedValue);
    Object localObject = a(paramContext, l1);
    if (localObject != null) {
      return (Drawable)localObject;
    }
    if (paramInt == R.drawable.abc_cab_background_top_material) {
      localObject = new LayerDrawable(new Drawable[] { a(paramContext, R.drawable.abc_cab_background_internal_bg), a(paramContext, R.drawable.abc_cab_background_top_mtrl_alpha) });
    }
    if (localObject != null)
    {
      ((Drawable)localObject).setChangingConfigurations(localTypedValue.changingConfigurations);
      a(paramContext, l1, (Drawable)localObject);
    }
    return (Drawable)localObject;
  }
  
  private ColorStateList d(Context paramContext)
  {
    int[][] arrayOfInt = new int[3][];
    int[] arrayOfInt1 = new int[3];
    ColorStateList localColorStateList = ThemeUtils.b(paramContext, R.attr.colorSwitchThumbNormal);
    int i1;
    if ((localColorStateList != null) && (localColorStateList.isStateful()))
    {
      arrayOfInt[0] = ThemeUtils.a;
      arrayOfInt1[0] = localColorStateList.getColorForState(arrayOfInt[0], 0);
      i1 = 0 + 1;
      arrayOfInt[i1] = ThemeUtils.e;
      arrayOfInt1[i1] = ThemeUtils.a(paramContext, R.attr.colorControlActivated);
      i1 += 1;
      arrayOfInt[i1] = ThemeUtils.h;
      arrayOfInt1[i1] = localColorStateList.getDefaultColor();
    }
    for (;;)
    {
      return new ColorStateList(arrayOfInt, arrayOfInt1);
      arrayOfInt[0] = ThemeUtils.a;
      arrayOfInt1[0] = ThemeUtils.c(paramContext, R.attr.colorSwitchThumbNormal);
      i1 = 0 + 1;
      arrayOfInt[i1] = ThemeUtils.e;
      arrayOfInt1[i1] = ThemeUtils.a(paramContext, R.attr.colorControlActivated);
      i1 += 1;
      arrayOfInt[i1] = ThemeUtils.h;
      arrayOfInt1[i1] = ThemeUtils.a(paramContext, R.attr.colorSwitchThumbNormal);
    }
  }
  
  private Drawable d(@NonNull Context paramContext, @DrawableRes int paramInt)
  {
    if ((this.k != null) && (!this.k.isEmpty()))
    {
      Object localObject1;
      Object localObject2;
      if (this.l != null)
      {
        localObject1 = (String)this.l.get(paramInt);
        if ((!"appcompat_skip_skip".equals(localObject1)) && ((localObject1 == null) || (this.k.get(localObject1) != null))) {
          break label82;
        }
        localObject2 = null;
      }
      label82:
      TypedValue localTypedValue;
      Object localObject4;
      long l1;
      do
      {
        return (Drawable)localObject2;
        this.l = new SparseArray();
        if (this.o == null) {
          this.o = new TypedValue();
        }
        localTypedValue = this.o;
        localObject4 = paramContext.getResources();
        ((Resources)localObject4).getValue(paramInt, localTypedValue, true);
        l1 = a(localTypedValue);
        localObject1 = a(paramContext, l1);
        localObject2 = localObject1;
      } while (localObject1 != null);
      Object localObject3 = localObject1;
      AttributeSet localAttributeSet;
      if (localTypedValue.string != null)
      {
        localObject3 = localObject1;
        if (localTypedValue.string.toString().endsWith(".xml"))
        {
          localObject3 = localObject1;
          try
          {
            localObject4 = ((Resources)localObject4).getXml(paramInt);
            localObject3 = localObject1;
            localAttributeSet = Xml.asAttributeSet((XmlPullParser)localObject4);
            int i1;
            do
            {
              localObject3 = localObject1;
              i1 = ((XmlPullParser)localObject4).next();
            } while ((i1 != 2) && (i1 != 1));
            if (i1 != 2)
            {
              localObject3 = localObject1;
              throw new XmlPullParserException("No start tag found");
            }
          }
          catch (Exception paramContext)
          {
            Log.e("AppCompatDrawableManager", "Exception while inflating drawable", paramContext);
          }
        }
      }
      for (;;)
      {
        localObject2 = localObject3;
        if (localObject3 != null) {
          break;
        }
        this.l.append(paramInt, "appcompat_skip_skip");
        return (Drawable)localObject3;
        localObject3 = localObject1;
        localObject2 = ((XmlPullParser)localObject4).getName();
        localObject3 = localObject1;
        this.l.append(paramInt, localObject2);
        localObject3 = localObject1;
        InflateDelegate localInflateDelegate = (InflateDelegate)this.k.get(localObject2);
        localObject2 = localObject1;
        if (localInflateDelegate != null)
        {
          localObject3 = localObject1;
          localObject2 = localInflateDelegate.a(paramContext, (XmlPullParser)localObject4, localAttributeSet, paramContext.getTheme());
        }
        localObject3 = localObject2;
        if (localObject2 != null)
        {
          localObject3 = localObject2;
          ((Drawable)localObject2).setChangingConfigurations(localTypedValue.changingConfigurations);
          localObject3 = localObject2;
          boolean bool = a(paramContext, l1, (Drawable)localObject2);
          localObject3 = localObject2;
          if (bool) {
            localObject3 = localObject2;
          }
        }
      }
    }
    return null;
  }
  
  private ColorStateList e(Context paramContext)
  {
    int[][] arrayOfInt = new int[3][];
    int[] arrayOfInt1 = new int[3];
    arrayOfInt[0] = ThemeUtils.a;
    arrayOfInt1[0] = ThemeUtils.c(paramContext, R.attr.colorControlNormal);
    int i1 = 0 + 1;
    arrayOfInt[i1] = ThemeUtils.g;
    arrayOfInt1[i1] = ThemeUtils.a(paramContext, R.attr.colorControlNormal);
    i1 += 1;
    arrayOfInt[i1] = ThemeUtils.h;
    arrayOfInt1[i1] = ThemeUtils.a(paramContext, R.attr.colorControlActivated);
    return new ColorStateList(arrayOfInt, arrayOfInt1);
  }
  
  private ColorStateList e(@NonNull Context paramContext, @DrawableRes int paramInt)
  {
    Object localObject2 = null;
    Object localObject1 = localObject2;
    if (this.j != null)
    {
      paramContext = (SparseArray)this.j.get(paramContext);
      localObject1 = localObject2;
      if (paramContext != null) {
        localObject1 = (ColorStateList)paramContext.get(paramInt);
      }
    }
    return (ColorStateList)localObject1;
  }
  
  private ColorStateList f(Context paramContext)
  {
    return f(paramContext, ThemeUtils.a(paramContext, R.attr.colorButtonNormal));
  }
  
  private ColorStateList f(Context paramContext, @ColorInt int paramInt)
  {
    int[][] arrayOfInt = new int[4][];
    int[] arrayOfInt1 = new int[4];
    int i1 = ThemeUtils.a(paramContext, R.attr.colorControlHighlight);
    arrayOfInt[0] = ThemeUtils.a;
    arrayOfInt1[0] = ThemeUtils.c(paramContext, R.attr.colorButtonNormal);
    int i2 = 0 + 1;
    arrayOfInt[i2] = ThemeUtils.d;
    arrayOfInt1[i2] = ColorUtils.a(i1, paramInt);
    i2 += 1;
    arrayOfInt[i2] = ThemeUtils.b;
    arrayOfInt1[i2] = ColorUtils.a(i1, paramInt);
    i1 = i2 + 1;
    arrayOfInt[i1] = ThemeUtils.h;
    arrayOfInt1[i1] = paramInt;
    return new ColorStateList(arrayOfInt, arrayOfInt1);
  }
  
  private ColorStateList g(Context paramContext)
  {
    return f(paramContext, 0);
  }
  
  private ColorStateList h(Context paramContext)
  {
    return f(paramContext, ThemeUtils.a(paramContext, R.attr.colorAccent));
  }
  
  private ColorStateList i(Context paramContext)
  {
    int[][] arrayOfInt = new int[3][];
    int[] arrayOfInt1 = new int[3];
    arrayOfInt[0] = ThemeUtils.a;
    arrayOfInt1[0] = ThemeUtils.c(paramContext, R.attr.colorControlNormal);
    int i1 = 0 + 1;
    arrayOfInt[i1] = ThemeUtils.g;
    arrayOfInt1[i1] = ThemeUtils.a(paramContext, R.attr.colorControlNormal);
    i1 += 1;
    arrayOfInt[i1] = ThemeUtils.h;
    arrayOfInt1[i1] = ThemeUtils.a(paramContext, R.attr.colorControlActivated);
    return new ColorStateList(arrayOfInt, arrayOfInt1);
  }
  
  private ColorStateList j(Context paramContext)
  {
    int[][] arrayOfInt = new int[2][];
    int[] arrayOfInt1 = new int[2];
    arrayOfInt[0] = ThemeUtils.a;
    arrayOfInt1[0] = ThemeUtils.c(paramContext, R.attr.colorControlActivated);
    int i1 = 0 + 1;
    arrayOfInt[i1] = ThemeUtils.h;
    arrayOfInt1[i1] = ThemeUtils.a(paramContext, R.attr.colorControlActivated);
    return new ColorStateList(arrayOfInt, arrayOfInt1);
  }
  
  final PorterDuff.Mode a(int paramInt)
  {
    PorterDuff.Mode localMode = null;
    if (paramInt == R.drawable.abc_switch_thumb_material) {
      localMode = PorterDuff.Mode.MULTIPLY;
    }
    return localMode;
  }
  
  public Drawable a(@NonNull Context paramContext, @DrawableRes int paramInt)
  {
    return a(paramContext, paramInt, false);
  }
  
  public Drawable a(@NonNull Context paramContext, @DrawableRes int paramInt, boolean paramBoolean)
  {
    Object localObject2 = d(paramContext, paramInt);
    Object localObject1 = localObject2;
    if (localObject2 == null) {
      localObject1 = c(paramContext, paramInt);
    }
    localObject2 = localObject1;
    if (localObject1 == null) {
      localObject2 = ContextCompat.getDrawable(paramContext, paramInt);
    }
    localObject1 = localObject2;
    if (localObject2 != null) {
      localObject1 = a(paramContext, paramInt, paramBoolean, (Drawable)localObject2);
    }
    if (localObject1 != null) {
      DrawableUtils.b((Drawable)localObject1);
    }
    return (Drawable)localObject1;
  }
  
  public final Drawable a(@NonNull Context paramContext, @NonNull VectorEnabledTintResources paramVectorEnabledTintResources, @DrawableRes int paramInt)
  {
    Drawable localDrawable2 = d(paramContext, paramInt);
    Drawable localDrawable1 = localDrawable2;
    if (localDrawable2 == null) {
      localDrawable1 = paramVectorEnabledTintResources.a(paramInt);
    }
    if (localDrawable1 != null) {
      return a(paramContext, paramInt, false, localDrawable1);
    }
    return null;
  }
  
  public final ColorStateList b(@NonNull Context paramContext, @DrawableRes int paramInt)
  {
    ColorStateList localColorStateList1 = e(paramContext, paramInt);
    ColorStateList localColorStateList2 = localColorStateList1;
    if (localColorStateList1 == null)
    {
      if (paramInt != R.drawable.abc_edit_text_material) {
        break label47;
      }
      localColorStateList1 = e(paramContext);
    }
    for (;;)
    {
      localColorStateList2 = localColorStateList1;
      if (localColorStateList1 != null)
      {
        a(paramContext, paramInt, localColorStateList1);
        localColorStateList2 = localColorStateList1;
      }
      return localColorStateList2;
      label47:
      if (paramInt == R.drawable.abc_switch_track_mtrl_alpha) {
        localColorStateList1 = c(paramContext);
      } else if (paramInt == R.drawable.abc_switch_thumb_material) {
        localColorStateList1 = d(paramContext);
      } else if (paramInt == R.drawable.abc_btn_default_mtrl_shape) {
        localColorStateList1 = f(paramContext);
      } else if (paramInt == R.drawable.abc_btn_borderless_material) {
        localColorStateList1 = g(paramContext);
      } else if (paramInt == R.drawable.abc_btn_colored_material) {
        localColorStateList1 = h(paramContext);
      } else if ((paramInt == R.drawable.abc_spinner_mtrl_am_alpha) || (paramInt == R.drawable.abc_spinner_textfield_background_material)) {
        localColorStateList1 = i(paramContext);
      } else if (a(e, paramInt)) {
        localColorStateList1 = ThemeUtils.b(paramContext, R.attr.colorControlNormal);
      } else if (a(h, paramInt)) {
        localColorStateList1 = a(paramContext);
      } else if (a(i, paramInt)) {
        localColorStateList1 = b(paramContext);
      } else if (paramInt == R.drawable.abc_seekbar_thumb_material) {
        localColorStateList1 = j(paramContext);
      }
    }
  }
  
  private static class AvdcInflateDelegate
    implements AppCompatDrawableManager.InflateDelegate
  {
    public Drawable a(@NonNull Context paramContext, @NonNull XmlPullParser paramXmlPullParser, @NonNull AttributeSet paramAttributeSet, @Nullable Resources.Theme paramTheme)
    {
      try
      {
        paramContext = AnimatedVectorDrawableCompat.a(paramContext, paramContext.getResources(), paramXmlPullParser, paramAttributeSet, paramTheme);
        return paramContext;
      }
      catch (Exception paramContext)
      {
        Log.e("AvdcInflateDelegate", "Exception while inflating <animated-vector>", paramContext);
      }
      return null;
    }
  }
  
  private static class ColorFilterLruCache
    extends LruCache<Integer, PorterDuffColorFilter>
  {
    public ColorFilterLruCache(int paramInt)
    {
      super();
    }
    
    private static int b(int paramInt, PorterDuff.Mode paramMode)
    {
      return (paramInt + 31) * 31 + paramMode.hashCode();
    }
    
    PorterDuffColorFilter a(int paramInt, PorterDuff.Mode paramMode)
    {
      return (PorterDuffColorFilter)a(Integer.valueOf(b(paramInt, paramMode)));
    }
    
    PorterDuffColorFilter a(int paramInt, PorterDuff.Mode paramMode, PorterDuffColorFilter paramPorterDuffColorFilter)
    {
      return (PorterDuffColorFilter)a(Integer.valueOf(b(paramInt, paramMode)), paramPorterDuffColorFilter);
    }
  }
  
  private static abstract interface InflateDelegate
  {
    public abstract Drawable a(@NonNull Context paramContext, @NonNull XmlPullParser paramXmlPullParser, @NonNull AttributeSet paramAttributeSet, @Nullable Resources.Theme paramTheme);
  }
  
  private static class VdcInflateDelegate
    implements AppCompatDrawableManager.InflateDelegate
  {
    public Drawable a(@NonNull Context paramContext, @NonNull XmlPullParser paramXmlPullParser, @NonNull AttributeSet paramAttributeSet, @Nullable Resources.Theme paramTheme)
    {
      try
      {
        paramContext = VectorDrawableCompat.a(paramContext.getResources(), paramXmlPullParser, paramAttributeSet, paramTheme);
        return paramContext;
      }
      catch (Exception paramContext)
      {
        Log.e("VdcInflateDelegate", "Exception while inflating <vector>", paramContext);
      }
      return null;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/AppCompatDrawableManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */