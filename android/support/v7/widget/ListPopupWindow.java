package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.text.TextUtilsCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.widget.ListViewAutoScrollHelper;
import android.support.v4.widget.PopupWindowCompat;
import android.support.v7.appcompat.R.attr;
import android.support.v7.appcompat.R.styleable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import java.lang.reflect.Method;

public class ListPopupWindow
{
  private static Method a;
  private static Method c;
  private final ListSelectorHider A = new ListSelectorHider(null);
  private Runnable B;
  private final Handler C;
  private Rect D = new Rect();
  private boolean E;
  private int F;
  int b = Integer.MAX_VALUE;
  private Context d;
  private PopupWindow e;
  private ListAdapter f;
  private DropDownListView g;
  private int h = -2;
  private int i = -2;
  private int j;
  private int k;
  private int l = 1002;
  private boolean m;
  private int n = 0;
  private boolean o = false;
  private boolean p = false;
  private View q;
  private int r = 0;
  private DataSetObserver s;
  private View t;
  private Drawable u;
  private AdapterView.OnItemClickListener v;
  private AdapterView.OnItemSelectedListener w;
  private final ResizePopupRunnable x = new ResizePopupRunnable(null);
  private final PopupTouchInterceptor y = new PopupTouchInterceptor(null);
  private final PopupScrollListener z = new PopupScrollListener(null);
  
  static
  {
    try
    {
      a = PopupWindow.class.getDeclaredMethod("setClipToScreenEnabled", new Class[] { Boolean.TYPE });
    }
    catch (NoSuchMethodException localNoSuchMethodException1)
    {
      for (;;)
      {
        try
        {
          c = PopupWindow.class.getDeclaredMethod("getMaxAvailableHeight", new Class[] { View.class, Integer.TYPE, Boolean.TYPE });
          return;
        }
        catch (NoSuchMethodException localNoSuchMethodException2)
        {
          Log.i("ListPopupWindow", "Could not find method getMaxAvailableHeight(View, int, boolean) on PopupWindow. Oh well.");
        }
        localNoSuchMethodException1 = localNoSuchMethodException1;
        Log.i("ListPopupWindow", "Could not find method setClipToScreenEnabled() on PopupWindow. Oh well.");
      }
    }
  }
  
  public ListPopupWindow(Context paramContext)
  {
    this(paramContext, null, R.attr.listPopupWindowStyle);
  }
  
  public ListPopupWindow(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, R.attr.listPopupWindowStyle);
  }
  
  public ListPopupWindow(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public ListPopupWindow(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
  {
    this.d = paramContext;
    this.C = new Handler(paramContext.getMainLooper());
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ListPopupWindow, paramInt1, paramInt2);
    this.j = localTypedArray.getDimensionPixelOffset(R.styleable.ListPopupWindow_android_dropDownHorizontalOffset, 0);
    this.k = localTypedArray.getDimensionPixelOffset(R.styleable.ListPopupWindow_android_dropDownVerticalOffset, 0);
    if (this.k != 0) {
      this.m = true;
    }
    localTypedArray.recycle();
    this.e = new AppCompatPopupWindow(paramContext, paramAttributeSet, paramInt1);
    this.e.setInputMethodMode(1);
    this.F = TextUtilsCompat.a(this.d.getResources().getConfiguration().locale);
  }
  
  private int a(View paramView, int paramInt, boolean paramBoolean)
  {
    if (c != null) {
      try
      {
        int i1 = ((Integer)c.invoke(this.e, new Object[] { paramView, Integer.valueOf(paramInt), Boolean.valueOf(paramBoolean) })).intValue();
        return i1;
      }
      catch (Exception localException)
      {
        Log.i("ListPopupWindow", "Could not call getMaxAvailableHeightMethod(View, int, boolean) on PopupWindow. Using the public version.");
      }
    }
    return this.e.getMaxAvailableHeight(paramView, paramInt);
  }
  
  private void a()
  {
    if (this.q != null)
    {
      ViewParent localViewParent = this.q.getParent();
      if ((localViewParent instanceof ViewGroup)) {
        ((ViewGroup)localViewParent).removeView(this.q);
      }
    }
  }
  
  private int b()
  {
    int i2 = 0;
    int i1 = 0;
    Object localObject3;
    Object localObject2;
    View localView;
    Object localObject1;
    label261:
    label277:
    label325:
    int i3;
    if (this.g == null)
    {
      localObject3 = this.d;
      this.B = new Runnable()
      {
        public void run()
        {
          View localView = ListPopupWindow.this.e();
          if ((localView != null) && (localView.getWindowToken() != null)) {
            ListPopupWindow.this.c();
          }
        }
      };
      if (!this.E)
      {
        bool = true;
        this.g = new DropDownListView((Context)localObject3, bool);
        if (this.u != null) {
          this.g.setSelector(this.u);
        }
        this.g.setAdapter(this.f);
        this.g.setOnItemClickListener(this.v);
        this.g.setFocusable(true);
        this.g.setFocusableInTouchMode(true);
        this.g.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
          public void onItemSelected(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
          {
            if (paramAnonymousInt != -1)
            {
              paramAnonymousAdapterView = ListPopupWindow.a(ListPopupWindow.this);
              if (paramAnonymousAdapterView != null) {
                ListPopupWindow.DropDownListView.a(paramAnonymousAdapterView, false);
              }
            }
          }
          
          public void onNothingSelected(AdapterView<?> paramAnonymousAdapterView) {}
        });
        this.g.setOnScrollListener(this.z);
        if (this.w != null) {
          this.g.setOnItemSelectedListener(this.w);
        }
        localObject2 = this.g;
        localView = this.q;
        localObject1 = localObject2;
        if (localView != null)
        {
          localObject1 = new LinearLayout((Context)localObject3);
          ((LinearLayout)localObject1).setOrientation(1);
          localObject3 = new LinearLayout.LayoutParams(-1, 0, 1.0F);
        }
        switch (this.r)
        {
        default: 
          Log.e("ListPopupWindow", "Invalid hint position " + this.r);
          if (this.i >= 0)
          {
            i1 = Integer.MIN_VALUE;
            i2 = this.i;
            localView.measure(View.MeasureSpec.makeMeasureSpec(i2, i1), 0);
            localObject2 = (LinearLayout.LayoutParams)localView.getLayoutParams();
            i1 = localView.getMeasuredHeight() + ((LinearLayout.LayoutParams)localObject2).topMargin + ((LinearLayout.LayoutParams)localObject2).bottomMargin;
            this.e.setContentView((View)localObject1);
            i3 = 0;
            localObject1 = this.e.getBackground();
            if (localObject1 == null) {
              break label547;
            }
            ((Drawable)localObject1).getPadding(this.D);
            i2 = this.D.top + this.D.bottom;
            i3 = i2;
            if (!this.m)
            {
              this.k = (-this.D.top);
              i3 = i2;
            }
            label390:
            if (this.e.getInputMethodMode() != 2) {
              break label557;
            }
          }
          break;
        }
      }
    }
    int i4;
    label547:
    label557:
    for (boolean bool = true;; bool = false)
    {
      i4 = a(e(), this.k, bool);
      if ((!this.o) && (this.h != -1)) {
        break label563;
      }
      return i4 + i3;
      bool = false;
      break;
      ((LinearLayout)localObject1).addView((View)localObject2, (ViewGroup.LayoutParams)localObject3);
      ((LinearLayout)localObject1).addView(localView);
      break label261;
      ((LinearLayout)localObject1).addView(localView);
      ((LinearLayout)localObject1).addView((View)localObject2, (ViewGroup.LayoutParams)localObject3);
      break label261;
      i1 = 0;
      i2 = 0;
      break label277;
      localObject1 = (ViewGroup)this.e.getContentView();
      localObject1 = this.q;
      i1 = i2;
      if (localObject1 == null) {
        break label325;
      }
      localObject2 = (LinearLayout.LayoutParams)((View)localObject1).getLayoutParams();
      i1 = ((View)localObject1).getMeasuredHeight() + ((LinearLayout.LayoutParams)localObject2).topMargin + ((LinearLayout.LayoutParams)localObject2).bottomMargin;
      break label325;
      this.D.setEmpty();
      break label390;
    }
    label563:
    switch (this.i)
    {
    default: 
      i2 = View.MeasureSpec.makeMeasureSpec(this.i, 1073741824);
    }
    for (;;)
    {
      i4 = this.g.a(i2, 0, -1, i4 - i1, -1);
      i2 = i1;
      if (i4 > 0) {
        i2 = i1 + i3;
      }
      return i4 + i2;
      i2 = View.MeasureSpec.makeMeasureSpec(this.d.getResources().getDisplayMetrics().widthPixels - (this.D.left + this.D.right), Integer.MIN_VALUE);
      continue;
      i2 = View.MeasureSpec.makeMeasureSpec(this.d.getResources().getDisplayMetrics().widthPixels - (this.D.left + this.D.right), 1073741824);
    }
  }
  
  private void b(boolean paramBoolean)
  {
    if (a != null) {}
    try
    {
      a.invoke(this.e, new Object[] { Boolean.valueOf(paramBoolean) });
      return;
    }
    catch (Exception localException)
    {
      Log.i("ListPopupWindow", "Could not call setClipToScreenEnabled() on PopupWindow. Oh well.");
    }
  }
  
  public void a(int paramInt)
  {
    this.r = paramInt;
  }
  
  public void a(Drawable paramDrawable)
  {
    this.e.setBackgroundDrawable(paramDrawable);
  }
  
  public void a(View paramView)
  {
    this.t = paramView;
  }
  
  public void a(AdapterView.OnItemClickListener paramOnItemClickListener)
  {
    this.v = paramOnItemClickListener;
  }
  
  public void a(ListAdapter paramListAdapter)
  {
    if (this.s == null) {
      this.s = new PopupDataSetObserver(null);
    }
    for (;;)
    {
      this.f = paramListAdapter;
      if (this.f != null) {
        paramListAdapter.registerDataSetObserver(this.s);
      }
      if (this.g != null) {
        this.g.setAdapter(this.f);
      }
      return;
      if (this.f != null) {
        this.f.unregisterDataSetObserver(this.s);
      }
    }
  }
  
  public void a(PopupWindow.OnDismissListener paramOnDismissListener)
  {
    this.e.setOnDismissListener(paramOnDismissListener);
  }
  
  public void a(boolean paramBoolean)
  {
    this.E = paramBoolean;
    this.e.setFocusable(paramBoolean);
  }
  
  public void b(int paramInt)
  {
    this.j = paramInt;
  }
  
  public void c()
  {
    boolean bool1 = true;
    boolean bool2 = false;
    int i4 = -1;
    int i2 = b();
    boolean bool3 = l();
    PopupWindowCompat.a(this.e, this.l);
    int i1;
    label64:
    PopupWindow localPopupWindow;
    if (this.e.isShowing())
    {
      int i3;
      label85:
      label99:
      View localView;
      int i5;
      if (this.i == -1)
      {
        i1 = -1;
        if (this.h != -1) {
          break label262;
        }
        if (!bool3) {
          break label214;
        }
        if (!bool3) {
          break label224;
        }
        localPopupWindow = this.e;
        if (this.i != -1) {
          break label219;
        }
        i3 = -1;
        localPopupWindow.setWidth(i3);
        this.e.setHeight(0);
        localPopupWindow = this.e;
        bool1 = bool2;
        if (!this.p)
        {
          bool1 = bool2;
          if (!this.o) {
            bool1 = true;
          }
        }
        localPopupWindow.setOutsideTouchable(bool1);
        localPopupWindow = this.e;
        localView = e();
        i3 = this.j;
        i5 = this.k;
        if (i1 >= 0) {
          break label282;
        }
        i1 = -1;
        label166:
        if (i2 >= 0) {
          break label285;
        }
        i2 = i4;
      }
      label214:
      label219:
      label224:
      label262:
      label282:
      label285:
      for (;;)
      {
        localPopupWindow.update(localView, i3, i5, i1, i2);
        return;
        if (this.i == -2)
        {
          i1 = e().getWidth();
          break;
        }
        i1 = this.i;
        break;
        i2 = -1;
        break label64;
        i3 = 0;
        break label85;
        localPopupWindow = this.e;
        if (this.i == -1) {}
        for (i3 = -1;; i3 = 0)
        {
          localPopupWindow.setWidth(i3);
          this.e.setHeight(-1);
          break;
        }
        if (this.h == -2) {
          break label99;
        }
        i2 = this.h;
        break label99;
        break label166;
      }
    }
    if (this.i == -1)
    {
      i1 = -1;
      label298:
      if (this.h != -1) {
        break label467;
      }
      i2 = -1;
      label308:
      this.e.setWidth(i1);
      this.e.setHeight(i2);
      b(true);
      localPopupWindow = this.e;
      if ((this.p) || (this.o)) {
        break label487;
      }
    }
    for (;;)
    {
      localPopupWindow.setOutsideTouchable(bool1);
      this.e.setTouchInterceptor(this.y);
      PopupWindowCompat.a(this.e, e(), this.j, this.k, this.n);
      this.g.setSelection(-1);
      if ((!this.E) || (this.g.isInTouchMode())) {
        j();
      }
      if (this.E) {
        break;
      }
      this.C.post(this.A);
      return;
      if (this.i == -2)
      {
        i1 = e().getWidth();
        break label298;
      }
      i1 = this.i;
      break label298;
      label467:
      if (this.h == -2) {
        break label308;
      }
      i2 = this.h;
      break label308;
      label487:
      bool1 = false;
    }
  }
  
  public void c(int paramInt)
  {
    this.k = paramInt;
    this.m = true;
  }
  
  public Drawable d()
  {
    return this.e.getBackground();
  }
  
  public void d(int paramInt)
  {
    this.n = paramInt;
  }
  
  public View e()
  {
    return this.t;
  }
  
  public void e(int paramInt)
  {
    this.i = paramInt;
  }
  
  public int f()
  {
    return this.j;
  }
  
  public void f(int paramInt)
  {
    Drawable localDrawable = this.e.getBackground();
    if (localDrawable != null)
    {
      localDrawable.getPadding(this.D);
      this.i = (this.D.left + this.D.right + paramInt);
      return;
    }
    e(paramInt);
  }
  
  public int g()
  {
    if (!this.m) {
      return 0;
    }
    return this.k;
  }
  
  public void g(int paramInt)
  {
    this.h = paramInt;
  }
  
  public int h()
  {
    return this.i;
  }
  
  public void h(int paramInt)
  {
    this.e.setInputMethodMode(paramInt);
  }
  
  public void i()
  {
    this.e.dismiss();
    a();
    this.e.setContentView(null);
    this.g = null;
    this.C.removeCallbacks(this.x);
  }
  
  public void i(int paramInt)
  {
    DropDownListView localDropDownListView = this.g;
    if ((k()) && (localDropDownListView != null))
    {
      DropDownListView.a(localDropDownListView, false);
      localDropDownListView.setSelection(paramInt);
      if ((Build.VERSION.SDK_INT >= 11) && (localDropDownListView.getChoiceMode() != 0)) {
        localDropDownListView.setItemChecked(paramInt, true);
      }
    }
  }
  
  public void j()
  {
    DropDownListView localDropDownListView = this.g;
    if (localDropDownListView != null)
    {
      DropDownListView.a(localDropDownListView, true);
      localDropDownListView.requestLayout();
    }
  }
  
  public boolean k()
  {
    return this.e.isShowing();
  }
  
  public boolean l()
  {
    return this.e.getInputMethodMode() == 2;
  }
  
  public ListView m()
  {
    return this.g;
  }
  
  private static class DropDownListView
    extends ListViewCompat
  {
    private boolean g;
    private boolean h;
    private boolean i;
    private ViewPropertyAnimatorCompat j;
    private ListViewAutoScrollHelper k;
    
    public DropDownListView(Context paramContext, boolean paramBoolean)
    {
      super(null, R.attr.dropDownListViewStyle);
      this.h = paramBoolean;
      setCacheColorHint(0);
    }
    
    private void a(View paramView, int paramInt)
    {
      performItemClick(paramView, paramInt, getItemIdAtPosition(paramInt));
    }
    
    private void a(View paramView, int paramInt, float paramFloat1, float paramFloat2)
    {
      this.i = true;
      if (Build.VERSION.SDK_INT >= 21) {
        drawableHotspotChanged(paramFloat1, paramFloat2);
      }
      if (!isPressed()) {
        setPressed(true);
      }
      layoutChildren();
      if (this.f != -1)
      {
        View localView = getChildAt(this.f - getFirstVisiblePosition());
        if ((localView != null) && (localView != paramView) && (localView.isPressed())) {
          localView.setPressed(false);
        }
      }
      this.f = paramInt;
      float f1 = paramView.getLeft();
      float f2 = paramView.getTop();
      if (Build.VERSION.SDK_INT >= 21) {
        paramView.drawableHotspotChanged(paramFloat1 - f1, paramFloat2 - f2);
      }
      if (!paramView.isPressed()) {
        paramView.setPressed(true);
      }
      a(paramInt, paramView, paramFloat1, paramFloat2);
      setSelectorEnabled(false);
      refreshDrawableState();
    }
    
    private void d()
    {
      this.i = false;
      setPressed(false);
      drawableStateChanged();
      View localView = getChildAt(this.f - getFirstVisiblePosition());
      if (localView != null) {
        localView.setPressed(false);
      }
      if (this.j != null)
      {
        this.j.cancel();
        this.j = null;
      }
    }
    
    protected boolean a()
    {
      return (this.i) || (super.a());
    }
    
    public boolean a(MotionEvent paramMotionEvent, int paramInt)
    {
      boolean bool1 = true;
      boolean bool2 = true;
      int m = 0;
      int n = MotionEventCompat.a(paramMotionEvent);
      switch (n)
      {
      default: 
        bool1 = bool2;
        paramInt = m;
        if ((!bool1) || (paramInt != 0)) {
          d();
        }
        if (bool1)
        {
          if (this.k == null) {
            this.k = new ListViewAutoScrollHelper(this);
          }
          this.k.a(true);
          this.k.onTouch(this, paramMotionEvent);
        }
        break;
      }
      while (this.k == null)
      {
        return bool1;
        bool1 = false;
        paramInt = m;
        break;
        bool1 = false;
        int i1 = paramMotionEvent.findPointerIndex(paramInt);
        if (i1 < 0)
        {
          bool1 = false;
          paramInt = m;
          break;
        }
        paramInt = (int)paramMotionEvent.getX(i1);
        int i2 = (int)paramMotionEvent.getY(i1);
        i1 = pointToPosition(paramInt, i2);
        if (i1 == -1)
        {
          paramInt = 1;
          break;
        }
        View localView = getChildAt(i1 - getFirstVisiblePosition());
        a(localView, i1, paramInt, i2);
        bool2 = true;
        paramInt = m;
        bool1 = bool2;
        if (n != 1) {
          break;
        }
        a(localView, i1);
        paramInt = m;
        bool1 = bool2;
        break;
      }
      this.k.a(false);
      return bool1;
    }
    
    public boolean hasFocus()
    {
      return (this.h) || (super.hasFocus());
    }
    
    public boolean hasWindowFocus()
    {
      return (this.h) || (super.hasWindowFocus());
    }
    
    public boolean isFocused()
    {
      return (this.h) || (super.isFocused());
    }
    
    public boolean isInTouchMode()
    {
      return ((this.h) && (this.g)) || (super.isInTouchMode());
    }
  }
  
  public static abstract class ForwardingListener
    implements View.OnTouchListener
  {
    private final float a;
    private final int b;
    private final int c;
    private final View d;
    private Runnable e;
    private Runnable f;
    private boolean g;
    private boolean h;
    private int i;
    private final int[] j = new int[2];
    
    public ForwardingListener(View paramView)
    {
      this.d = paramView;
      this.a = ViewConfiguration.get(paramView.getContext()).getScaledTouchSlop();
      this.b = ViewConfiguration.getTapTimeout();
      this.c = ((this.b + ViewConfiguration.getLongPressTimeout()) / 2);
    }
    
    private boolean a(MotionEvent paramMotionEvent)
    {
      View localView = this.d;
      if (!localView.isEnabled()) {}
      int k;
      do
      {
        return false;
        switch (MotionEventCompat.a(paramMotionEvent))
        {
        default: 
          return false;
        case 0: 
          this.i = paramMotionEvent.getPointerId(0);
          this.h = false;
          if (this.e == null) {
            this.e = new DisallowIntercept(null);
          }
          localView.postDelayed(this.e, this.b);
          if (this.f == null) {
            this.f = new TriggerLongPress(null);
          }
          localView.postDelayed(this.f, this.c);
          return false;
        case 2: 
          k = paramMotionEvent.findPointerIndex(this.i);
        }
      } while ((k < 0) || (a(localView, paramMotionEvent.getX(k), paramMotionEvent.getY(k), this.a)));
      d();
      localView.getParent().requestDisallowInterceptTouchEvent(true);
      return true;
      d();
      return false;
    }
    
    private static boolean a(View paramView, float paramFloat1, float paramFloat2, float paramFloat3)
    {
      return (paramFloat1 >= -paramFloat3) && (paramFloat2 >= -paramFloat3) && (paramFloat1 < paramView.getRight() - paramView.getLeft() + paramFloat3) && (paramFloat2 < paramView.getBottom() - paramView.getTop() + paramFloat3);
    }
    
    private boolean a(View paramView, MotionEvent paramMotionEvent)
    {
      int[] arrayOfInt = this.j;
      paramView.getLocationOnScreen(arrayOfInt);
      paramMotionEvent.offsetLocation(-arrayOfInt[0], -arrayOfInt[1]);
      return true;
    }
    
    private boolean b(MotionEvent paramMotionEvent)
    {
      boolean bool1 = true;
      View localView = this.d;
      Object localObject = a();
      if ((localObject == null) || (!((ListPopupWindow)localObject).k())) {}
      do
      {
        return false;
        localObject = ListPopupWindow.a((ListPopupWindow)localObject);
      } while ((localObject == null) || (!((ListPopupWindow.DropDownListView)localObject).isShown()));
      MotionEvent localMotionEvent = MotionEvent.obtainNoHistory(paramMotionEvent);
      b(localView, localMotionEvent);
      a((View)localObject, localMotionEvent);
      boolean bool2 = ((ListPopupWindow.DropDownListView)localObject).a(localMotionEvent, this.i);
      localMotionEvent.recycle();
      int k = MotionEventCompat.a(paramMotionEvent);
      if ((k != 1) && (k != 3))
      {
        k = 1;
        if ((!bool2) || (k == 0)) {
          break label124;
        }
      }
      for (;;)
      {
        return bool1;
        k = 0;
        break;
        label124:
        bool1 = false;
      }
    }
    
    private boolean b(View paramView, MotionEvent paramMotionEvent)
    {
      int[] arrayOfInt = this.j;
      paramView.getLocationOnScreen(arrayOfInt);
      paramMotionEvent.offsetLocation(arrayOfInt[0], arrayOfInt[1]);
      return true;
    }
    
    private void d()
    {
      if (this.f != null) {
        this.d.removeCallbacks(this.f);
      }
      if (this.e != null) {
        this.d.removeCallbacks(this.e);
      }
    }
    
    private void e()
    {
      d();
      View localView = this.d;
      if ((!localView.isEnabled()) || (localView.isLongClickable())) {}
      while (!b()) {
        return;
      }
      localView.getParent().requestDisallowInterceptTouchEvent(true);
      long l = SystemClock.uptimeMillis();
      MotionEvent localMotionEvent = MotionEvent.obtain(l, l, 3, 0.0F, 0.0F, 0);
      localView.onTouchEvent(localMotionEvent);
      localMotionEvent.recycle();
      this.g = true;
      this.h = true;
    }
    
    public abstract ListPopupWindow a();
    
    protected boolean b()
    {
      ListPopupWindow localListPopupWindow = a();
      if ((localListPopupWindow != null) && (!localListPopupWindow.k())) {
        localListPopupWindow.c();
      }
      return true;
    }
    
    protected boolean c()
    {
      ListPopupWindow localListPopupWindow = a();
      if ((localListPopupWindow != null) && (localListPopupWindow.k())) {
        localListPopupWindow.i();
      }
      return true;
    }
    
    public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
    {
      boolean bool3 = false;
      boolean bool4 = this.g;
      boolean bool1;
      if (bool4)
      {
        if (this.h)
        {
          bool1 = b(paramMotionEvent);
          this.g = bool1;
          if (!bool1)
          {
            bool1 = bool3;
            if (!bool4) {}
          }
          else
          {
            bool1 = true;
          }
          return bool1;
        }
        if ((b(paramMotionEvent)) || (!c())) {}
        for (bool1 = true;; bool1 = false) {
          break;
        }
      }
      if ((a(paramMotionEvent)) && (b())) {}
      for (boolean bool2 = true;; bool2 = false)
      {
        bool1 = bool2;
        if (!bool2) {
          break;
        }
        long l = SystemClock.uptimeMillis();
        paramView = MotionEvent.obtain(l, l, 3, 0.0F, 0.0F, 0);
        this.d.onTouchEvent(paramView);
        paramView.recycle();
        bool1 = bool2;
        break;
      }
    }
    
    private class DisallowIntercept
      implements Runnable
    {
      private DisallowIntercept() {}
      
      public void run()
      {
        ListPopupWindow.ForwardingListener.a(ListPopupWindow.ForwardingListener.this).getParent().requestDisallowInterceptTouchEvent(true);
      }
    }
    
    private class TriggerLongPress
      implements Runnable
    {
      private TriggerLongPress() {}
      
      public void run()
      {
        ListPopupWindow.ForwardingListener.b(ListPopupWindow.ForwardingListener.this);
      }
    }
  }
  
  private class ListSelectorHider
    implements Runnable
  {
    private ListSelectorHider() {}
    
    public void run()
    {
      ListPopupWindow.this.j();
    }
  }
  
  private class PopupDataSetObserver
    extends DataSetObserver
  {
    private PopupDataSetObserver() {}
    
    public void onChanged()
    {
      if (ListPopupWindow.this.k()) {
        ListPopupWindow.this.c();
      }
    }
    
    public void onInvalidated()
    {
      ListPopupWindow.this.i();
    }
  }
  
  private class PopupScrollListener
    implements AbsListView.OnScrollListener
  {
    private PopupScrollListener() {}
    
    public void onScroll(AbsListView paramAbsListView, int paramInt1, int paramInt2, int paramInt3) {}
    
    public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt)
    {
      if ((paramInt == 1) && (!ListPopupWindow.this.l()) && (ListPopupWindow.b(ListPopupWindow.this).getContentView() != null))
      {
        ListPopupWindow.d(ListPopupWindow.this).removeCallbacks(ListPopupWindow.c(ListPopupWindow.this));
        ListPopupWindow.c(ListPopupWindow.this).run();
      }
    }
  }
  
  private class PopupTouchInterceptor
    implements View.OnTouchListener
  {
    private PopupTouchInterceptor() {}
    
    public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
    {
      int i = paramMotionEvent.getAction();
      int j = (int)paramMotionEvent.getX();
      int k = (int)paramMotionEvent.getY();
      if ((i == 0) && (ListPopupWindow.b(ListPopupWindow.this) != null) && (ListPopupWindow.b(ListPopupWindow.this).isShowing()) && (j >= 0) && (j < ListPopupWindow.b(ListPopupWindow.this).getWidth()) && (k >= 0) && (k < ListPopupWindow.b(ListPopupWindow.this).getHeight())) {
        ListPopupWindow.d(ListPopupWindow.this).postDelayed(ListPopupWindow.c(ListPopupWindow.this), 250L);
      }
      for (;;)
      {
        return false;
        if (i == 1) {
          ListPopupWindow.d(ListPopupWindow.this).removeCallbacks(ListPopupWindow.c(ListPopupWindow.this));
        }
      }
    }
  }
  
  private class ResizePopupRunnable
    implements Runnable
  {
    private ResizePopupRunnable() {}
    
    public void run()
    {
      if ((ListPopupWindow.a(ListPopupWindow.this) != null) && (ViewCompat.H(ListPopupWindow.a(ListPopupWindow.this))) && (ListPopupWindow.a(ListPopupWindow.this).getCount() > ListPopupWindow.a(ListPopupWindow.this).getChildCount()) && (ListPopupWindow.a(ListPopupWindow.this).getChildCount() <= ListPopupWindow.this.b))
      {
        ListPopupWindow.b(ListPopupWindow.this).setInputMethodMode(2);
        ListPopupWindow.this.c();
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/ListPopupWindow.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */