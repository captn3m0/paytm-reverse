package android.support.v7.widget;

import android.view.View;

class ScrollbarHelper
{
  static int a(RecyclerView.State paramState, OrientationHelper paramOrientationHelper, View paramView1, View paramView2, RecyclerView.LayoutManager paramLayoutManager, boolean paramBoolean)
  {
    if ((paramLayoutManager.u() == 0) || (paramState.e() == 0) || (paramView1 == null) || (paramView2 == null)) {
      return 0;
    }
    if (!paramBoolean) {
      return Math.abs(paramLayoutManager.d(paramView1) - paramLayoutManager.d(paramView2)) + 1;
    }
    int i = paramOrientationHelper.b(paramView2);
    int j = paramOrientationHelper.a(paramView1);
    return Math.min(paramOrientationHelper.f(), i - j);
  }
  
  static int a(RecyclerView.State paramState, OrientationHelper paramOrientationHelper, View paramView1, View paramView2, RecyclerView.LayoutManager paramLayoutManager, boolean paramBoolean1, boolean paramBoolean2)
  {
    int i = 0;
    int j = i;
    if (paramLayoutManager.u() != 0)
    {
      j = i;
      if (paramState.e() != 0)
      {
        j = i;
        if (paramView1 != null)
        {
          if (paramView2 != null) {
            break label45;
          }
          j = i;
        }
      }
    }
    return j;
    label45:
    i = Math.min(paramLayoutManager.d(paramView1), paramLayoutManager.d(paramView2));
    j = Math.max(paramLayoutManager.d(paramView1), paramLayoutManager.d(paramView2));
    if (paramBoolean2) {}
    for (i = Math.max(0, paramState.e() - j - 1);; i = Math.max(0, i))
    {
      j = i;
      if (!paramBoolean1) {
        break;
      }
      j = Math.abs(paramOrientationHelper.b(paramView2) - paramOrientationHelper.a(paramView1));
      int k = Math.abs(paramLayoutManager.d(paramView1) - paramLayoutManager.d(paramView2));
      float f = j / (k + 1);
      return Math.round(i * f + (paramOrientationHelper.c() - paramOrientationHelper.a(paramView1)));
    }
  }
  
  static int b(RecyclerView.State paramState, OrientationHelper paramOrientationHelper, View paramView1, View paramView2, RecyclerView.LayoutManager paramLayoutManager, boolean paramBoolean)
  {
    if ((paramLayoutManager.u() == 0) || (paramState.e() == 0) || (paramView1 == null) || (paramView2 == null)) {
      return 0;
    }
    if (!paramBoolean) {
      return paramState.e();
    }
    int i = paramOrientationHelper.b(paramView2);
    int j = paramOrientationHelper.a(paramView1);
    int k = Math.abs(paramLayoutManager.d(paramView1) - paramLayoutManager.d(paramView2));
    return (int)((i - j) / (k + 1) * paramState.e());
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/ScrollbarHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */