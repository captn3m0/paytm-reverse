package android.support.v7.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View.MeasureSpec;
import android.widget.FrameLayout;

public class ContentFrameLayout
  extends FrameLayout
{
  private TypedValue a;
  private TypedValue b;
  private TypedValue c;
  private TypedValue d;
  private TypedValue e;
  private TypedValue f;
  private final Rect g = new Rect();
  private OnAttachListener h;
  
  public ContentFrameLayout(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public ContentFrameLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public ContentFrameLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public void a(Rect paramRect)
  {
    fitSystemWindows(paramRect);
  }
  
  public TypedValue getFixedHeightMajor()
  {
    if (this.e == null) {
      this.e = new TypedValue();
    }
    return this.e;
  }
  
  public TypedValue getFixedHeightMinor()
  {
    if (this.f == null) {
      this.f = new TypedValue();
    }
    return this.f;
  }
  
  public TypedValue getFixedWidthMajor()
  {
    if (this.c == null) {
      this.c = new TypedValue();
    }
    return this.c;
  }
  
  public TypedValue getFixedWidthMinor()
  {
    if (this.d == null) {
      this.d = new TypedValue();
    }
    return this.d;
  }
  
  public TypedValue getMinWidthMajor()
  {
    if (this.a == null) {
      this.a = new TypedValue();
    }
    return this.a;
  }
  
  public TypedValue getMinWidthMinor()
  {
    if (this.b == null) {
      this.b = new TypedValue();
    }
    return this.b;
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    if (this.h != null) {
      this.h.a();
    }
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    if (this.h != null) {
      this.h.b();
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    DisplayMetrics localDisplayMetrics = getContext().getResources().getDisplayMetrics();
    int j;
    int i2;
    int n;
    int m;
    TypedValue localTypedValue;
    label68:
    int i;
    if (localDisplayMetrics.widthPixels < localDisplayMetrics.heightPixels)
    {
      j = 1;
      int i1 = View.MeasureSpec.getMode(paramInt1);
      i2 = View.MeasureSpec.getMode(paramInt2);
      n = 0;
      int k = n;
      m = paramInt1;
      if (i1 == Integer.MIN_VALUE)
      {
        if (j == 0) {
          break label424;
        }
        localTypedValue = this.d;
        k = n;
        m = paramInt1;
        if (localTypedValue != null)
        {
          k = n;
          m = paramInt1;
          if (localTypedValue.type != 0)
          {
            i = 0;
            if (localTypedValue.type != 5) {
              break label433;
            }
            i = (int)localTypedValue.getDimension(localDisplayMetrics);
            label115:
            k = n;
            m = paramInt1;
            if (i > 0)
            {
              m = View.MeasureSpec.makeMeasureSpec(Math.min(i - (this.g.left + this.g.right), View.MeasureSpec.getSize(paramInt1)), 1073741824);
              k = 1;
            }
          }
        }
      }
      i = paramInt2;
      if (i2 == Integer.MIN_VALUE)
      {
        if (j == 0) {
          break label465;
        }
        localTypedValue = this.e;
        label180:
        i = paramInt2;
        if (localTypedValue != null)
        {
          i = paramInt2;
          if (localTypedValue.type != 0)
          {
            paramInt1 = 0;
            if (localTypedValue.type != 5) {
              break label474;
            }
            paramInt1 = (int)localTypedValue.getDimension(localDisplayMetrics);
            label217:
            i = paramInt2;
            if (paramInt1 > 0) {
              i = View.MeasureSpec.makeMeasureSpec(Math.min(paramInt1 - (this.g.top + this.g.bottom), View.MeasureSpec.getSize(paramInt2)), 1073741824);
            }
          }
        }
      }
      super.onMeasure(m, i);
      i2 = getMeasuredWidth();
      m = 0;
      n = View.MeasureSpec.makeMeasureSpec(i2, 1073741824);
      paramInt2 = m;
      paramInt1 = n;
      if (k == 0)
      {
        paramInt2 = m;
        paramInt1 = n;
        if (i1 == Integer.MIN_VALUE)
        {
          if (j == 0) {
            break label506;
          }
          localTypedValue = this.b;
          label313:
          paramInt2 = m;
          paramInt1 = n;
          if (localTypedValue != null)
          {
            paramInt2 = m;
            paramInt1 = n;
            if (localTypedValue.type != 0)
            {
              paramInt1 = 0;
              if (localTypedValue.type != 5) {
                break label515;
              }
              paramInt1 = (int)localTypedValue.getDimension(localDisplayMetrics);
            }
          }
        }
      }
    }
    for (;;)
    {
      j = paramInt1;
      if (paramInt1 > 0) {
        j = paramInt1 - (this.g.left + this.g.right);
      }
      paramInt2 = m;
      paramInt1 = n;
      if (i2 < j)
      {
        paramInt1 = View.MeasureSpec.makeMeasureSpec(j, 1073741824);
        paramInt2 = 1;
      }
      if (paramInt2 != 0) {
        super.onMeasure(paramInt1, i);
      }
      return;
      j = 0;
      break;
      label424:
      localTypedValue = this.c;
      break label68;
      label433:
      if (localTypedValue.type != 6) {
        break label115;
      }
      i = (int)localTypedValue.getFraction(localDisplayMetrics.widthPixels, localDisplayMetrics.widthPixels);
      break label115;
      label465:
      localTypedValue = this.f;
      break label180;
      label474:
      if (localTypedValue.type != 6) {
        break label217;
      }
      paramInt1 = (int)localTypedValue.getFraction(localDisplayMetrics.heightPixels, localDisplayMetrics.heightPixels);
      break label217;
      label506:
      localTypedValue = this.a;
      break label313;
      label515:
      if (localTypedValue.type == 6) {
        paramInt1 = (int)localTypedValue.getFraction(localDisplayMetrics.widthPixels, localDisplayMetrics.widthPixels);
      }
    }
  }
  
  public void setAttachListener(OnAttachListener paramOnAttachListener)
  {
    this.h = paramOnAttachListener;
  }
  
  public void setDecorPadding(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.g.set(paramInt1, paramInt2, paramInt3, paramInt4);
    if (ViewCompat.F(this)) {
      requestLayout();
    }
  }
  
  public static abstract interface OnAttachListener
  {
    public abstract void a();
    
    public abstract void b();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/widget/ContentFrameLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */