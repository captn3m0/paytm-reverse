package android.support.v7.cardview;

public final class R
{
  public static final class attr
  {
    public static final int cardBackgroundColor = 2130772184;
    public static final int cardCornerRadius = 2130772185;
    public static final int cardElevation = 2130772186;
    public static final int cardMaxElevation = 2130772187;
    public static final int cardPreventCornerOverlap = 2130772189;
    public static final int cardUseCompatPadding = 2130772188;
    public static final int contentPadding = 2130772190;
    public static final int contentPaddingBottom = 2130772194;
    public static final int contentPaddingLeft = 2130772191;
    public static final int contentPaddingRight = 2130772192;
    public static final int contentPaddingTop = 2130772193;
  }
  
  public static final class color
  {
    public static final int cardview_dark_background = 2131755109;
    public static final int cardview_light_background = 2131755110;
    public static final int cardview_shadow_end_color = 2131755111;
    public static final int cardview_shadow_start_color = 2131755112;
  }
  
  public static final class dimen
  {
    public static final int cardview_compat_inset_shadow = 2131493226;
    public static final int cardview_default_elevation = 2131493227;
    public static final int cardview_default_radius = 2131493228;
  }
  
  public static final class style
  {
    public static final int Base_CardView = 2131624123;
    public static final int CardView = 2131624111;
    public static final int CardView_Dark = 2131624177;
    public static final int CardView_Light = 2131624178;
  }
  
  public static final class styleable
  {
    public static final int[] CardView = { 16843071, 16843072, 2130771975, 2130771976, 2130771977, 2130772184, 2130772185, 2130772186, 2130772187, 2130772188, 2130772189, 2130772190, 2130772191, 2130772192, 2130772193, 2130772194 };
    public static final int CardView_android_minHeight = 1;
    public static final int CardView_android_minWidth = 0;
    public static final int CardView_cardBackgroundColor = 5;
    public static final int CardView_cardCornerRadius = 6;
    public static final int CardView_cardElevation = 7;
    public static final int CardView_cardMaxElevation = 8;
    public static final int CardView_cardPreventCornerOverlap = 10;
    public static final int CardView_cardUseCompatPadding = 9;
    public static final int CardView_contentPadding = 11;
    public static final int CardView_contentPaddingBottom = 15;
    public static final int CardView_contentPaddingLeft = 12;
    public static final int CardView_contentPaddingRight = 13;
    public static final int CardView_contentPaddingTop = 14;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/cardview/R.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */