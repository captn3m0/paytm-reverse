package android.support.v7.util;

import android.os.Handler;
import android.util.Log;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

class MessageThreadUtil<T>
  implements ThreadUtil<T>
{
  static class MessageQueue
  {
    private MessageThreadUtil.SyncQueueItem a;
    
    /* Error */
    MessageThreadUtil.SyncQueueItem a()
    {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield 17	android/support/v7/util/MessageThreadUtil$MessageQueue:a	Landroid/support/v7/util/MessageThreadUtil$SyncQueueItem;
      //   6: astore_1
      //   7: aload_1
      //   8: ifnonnull +9 -> 17
      //   11: aconst_null
      //   12: astore_1
      //   13: aload_0
      //   14: monitorexit
      //   15: aload_1
      //   16: areturn
      //   17: aload_0
      //   18: getfield 17	android/support/v7/util/MessageThreadUtil$MessageQueue:a	Landroid/support/v7/util/MessageThreadUtil$SyncQueueItem;
      //   21: astore_1
      //   22: aload_0
      //   23: aload_0
      //   24: getfield 17	android/support/v7/util/MessageThreadUtil$MessageQueue:a	Landroid/support/v7/util/MessageThreadUtil$SyncQueueItem;
      //   27: invokestatic 22	android/support/v7/util/MessageThreadUtil$SyncQueueItem:a	(Landroid/support/v7/util/MessageThreadUtil$SyncQueueItem;)Landroid/support/v7/util/MessageThreadUtil$SyncQueueItem;
      //   30: putfield 17	android/support/v7/util/MessageThreadUtil$MessageQueue:a	Landroid/support/v7/util/MessageThreadUtil$SyncQueueItem;
      //   33: goto -20 -> 13
      //   36: astore_1
      //   37: aload_0
      //   38: monitorexit
      //   39: aload_1
      //   40: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	41	0	this	MessageQueue
      //   6	16	1	localSyncQueueItem	MessageThreadUtil.SyncQueueItem
      //   36	4	1	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   2	7	36	finally
      //   17	33	36	finally
    }
    
    void a(int paramInt)
    {
      try
      {
        while ((this.a != null) && (this.a.a == paramInt))
        {
          MessageThreadUtil.SyncQueueItem localSyncQueueItem1 = this.a;
          this.a = MessageThreadUtil.SyncQueueItem.a(this.a);
          localSyncQueueItem1.a();
        }
        if (this.a == null) {
          break label105;
        }
      }
      finally {}
      Object localObject3 = this.a;
      Object localObject2 = MessageThreadUtil.SyncQueueItem.a((MessageThreadUtil.SyncQueueItem)localObject3);
      if (localObject2 != null)
      {
        MessageThreadUtil.SyncQueueItem localSyncQueueItem2 = MessageThreadUtil.SyncQueueItem.a((MessageThreadUtil.SyncQueueItem)localObject2);
        if (((MessageThreadUtil.SyncQueueItem)localObject2).a == paramInt)
        {
          MessageThreadUtil.SyncQueueItem.a((MessageThreadUtil.SyncQueueItem)localObject3, localSyncQueueItem2);
          ((MessageThreadUtil.SyncQueueItem)localObject2).a();
        }
        for (;;)
        {
          localObject2 = localSyncQueueItem2;
          break;
          localObject3 = localObject2;
        }
      }
      label105:
    }
    
    void a(MessageThreadUtil.SyncQueueItem paramSyncQueueItem)
    {
      try
      {
        MessageThreadUtil.SyncQueueItem.a(paramSyncQueueItem, this.a);
        this.a = paramSyncQueueItem;
        return;
      }
      finally
      {
        paramSyncQueueItem = finally;
        throw paramSyncQueueItem;
      }
    }
    
    /* Error */
    void b(MessageThreadUtil.SyncQueueItem paramSyncQueueItem)
    {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield 17	android/support/v7/util/MessageThreadUtil$MessageQueue:a	Landroid/support/v7/util/MessageThreadUtil$SyncQueueItem;
      //   6: ifnonnull +11 -> 17
      //   9: aload_0
      //   10: aload_1
      //   11: putfield 17	android/support/v7/util/MessageThreadUtil$MessageQueue:a	Landroid/support/v7/util/MessageThreadUtil$SyncQueueItem;
      //   14: aload_0
      //   15: monitorexit
      //   16: return
      //   17: aload_0
      //   18: getfield 17	android/support/v7/util/MessageThreadUtil$MessageQueue:a	Landroid/support/v7/util/MessageThreadUtil$SyncQueueItem;
      //   21: astore_2
      //   22: aload_2
      //   23: invokestatic 22	android/support/v7/util/MessageThreadUtil$SyncQueueItem:a	(Landroid/support/v7/util/MessageThreadUtil$SyncQueueItem;)Landroid/support/v7/util/MessageThreadUtil$SyncQueueItem;
      //   26: ifnull +11 -> 37
      //   29: aload_2
      //   30: invokestatic 22	android/support/v7/util/MessageThreadUtil$SyncQueueItem:a	(Landroid/support/v7/util/MessageThreadUtil$SyncQueueItem;)Landroid/support/v7/util/MessageThreadUtil$SyncQueueItem;
      //   33: astore_2
      //   34: goto -12 -> 22
      //   37: aload_2
      //   38: aload_1
      //   39: invokestatic 31	android/support/v7/util/MessageThreadUtil$SyncQueueItem:a	(Landroid/support/v7/util/MessageThreadUtil$SyncQueueItem;Landroid/support/v7/util/MessageThreadUtil$SyncQueueItem;)Landroid/support/v7/util/MessageThreadUtil$SyncQueueItem;
      //   42: pop
      //   43: goto -29 -> 14
      //   46: astore_1
      //   47: aload_0
      //   48: monitorexit
      //   49: aload_1
      //   50: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	51	0	this	MessageQueue
      //   0	51	1	paramSyncQueueItem	MessageThreadUtil.SyncQueueItem
      //   21	17	2	localSyncQueueItem	MessageThreadUtil.SyncQueueItem
      // Exception table:
      //   from	to	target	type
      //   2	14	46	finally
      //   17	22	46	finally
      //   22	34	46	finally
      //   37	43	46	finally
    }
  }
  
  static class SyncQueueItem
  {
    private static SyncQueueItem h;
    private static final Object i = new Object();
    public int a;
    public int b;
    public int c;
    public int d;
    public int e;
    public int f;
    public Object g;
    private SyncQueueItem j;
    
    static SyncQueueItem a(int paramInt1, int paramInt2, int paramInt3)
    {
      return a(paramInt1, paramInt2, paramInt3, 0, 0, 0, null);
    }
    
    static SyncQueueItem a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, Object paramObject)
    {
      synchronized (i)
      {
        if (h == null)
        {
          localSyncQueueItem = new SyncQueueItem();
          localSyncQueueItem.a = paramInt1;
          localSyncQueueItem.b = paramInt2;
          localSyncQueueItem.c = paramInt3;
          localSyncQueueItem.d = paramInt4;
          localSyncQueueItem.e = paramInt5;
          localSyncQueueItem.f = paramInt6;
          localSyncQueueItem.g = paramObject;
          return localSyncQueueItem;
        }
        SyncQueueItem localSyncQueueItem = h;
        h = h.j;
        localSyncQueueItem.j = null;
      }
    }
    
    static SyncQueueItem a(int paramInt1, int paramInt2, Object paramObject)
    {
      return a(paramInt1, paramInt2, 0, 0, 0, 0, paramObject);
    }
    
    void a()
    {
      this.j = null;
      this.f = 0;
      this.e = 0;
      this.d = 0;
      this.c = 0;
      this.b = 0;
      this.a = 0;
      this.g = null;
      synchronized (i)
      {
        if (h != null) {
          this.j = h;
        }
        h = this;
        return;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/util/MessageThreadUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */