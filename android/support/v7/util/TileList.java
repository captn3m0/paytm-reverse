package android.support.v7.util;

import android.util.SparseArray;
import java.lang.reflect.Array;

class TileList<T>
{
  Tile<T> a;
  private final SparseArray<Tile<T>> b;
  
  public int a()
  {
    return this.b.size();
  }
  
  public Tile<T> a(int paramInt)
  {
    return (Tile)this.b.valueAt(paramInt);
  }
  
  public Tile<T> a(Tile<T> paramTile)
  {
    int i = this.b.indexOfKey(paramTile.b);
    Object localObject;
    if (i < 0)
    {
      this.b.put(paramTile.b, paramTile);
      localObject = null;
    }
    Tile localTile;
    do
    {
      return (Tile<T>)localObject;
      localTile = (Tile)this.b.valueAt(i);
      this.b.setValueAt(i, paramTile);
      localObject = localTile;
    } while (this.a != localTile);
    this.a = paramTile;
    return localTile;
  }
  
  public Tile<T> b(int paramInt)
  {
    Tile localTile = (Tile)this.b.get(paramInt);
    if (this.a == localTile) {
      this.a = null;
    }
    this.b.delete(paramInt);
    return localTile;
  }
  
  public void b()
  {
    this.b.clear();
  }
  
  public static class Tile<T>
  {
    public final T[] a;
    public int b;
    public int c;
    Tile<T> d;
    
    public Tile(Class<T> paramClass, int paramInt)
    {
      this.a = ((Object[])Array.newInstance(paramClass, paramInt));
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/util/TileList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */