package android.support.v7.util;

import android.support.annotation.UiThread;
import android.support.annotation.WorkerThread;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;

public class AsyncListUtil<T>
{
  final Class<T> a;
  final int b;
  final DataCallback<T> c;
  final ViewCallback d;
  final TileList<T> e;
  final ThreadUtil.MainThreadCallback<T> f;
  final ThreadUtil.BackgroundCallback<T> g;
  final int[] h;
  final int[] i;
  final int[] j;
  int k;
  int l;
  private boolean m;
  private int n;
  private int o;
  private final SparseIntArray p;
  
  private void a()
  {
    this.d.a(this.h);
    if ((this.h[0] > this.h[1]) || (this.h[0] < 0)) {}
    while (this.h[1] >= this.o) {
      return;
    }
    if (!this.m) {
      this.n = 0;
    }
    for (;;)
    {
      this.i[0] = this.h[0];
      this.i[1] = this.h[1];
      this.d.a(this.h, this.j, this.n);
      this.j[0] = Math.min(this.h[0], Math.max(this.j[0], 0));
      this.j[1] = Math.max(this.h[1], Math.min(this.j[1], this.o - 1));
      this.g.a(this.h[0], this.h[1], this.j[0], this.j[1], this.n);
      return;
      if ((this.h[0] > this.i[1]) || (this.i[0] > this.h[1])) {
        this.n = 0;
      } else if (this.h[0] < this.i[0]) {
        this.n = 1;
      } else if (this.h[0] > this.i[0]) {
        this.n = 2;
      }
    }
  }
  
  public static abstract class DataCallback<T>
  {
    @WorkerThread
    public abstract int a();
    
    @WorkerThread
    public void a(T[] paramArrayOfT, int paramInt) {}
    
    @WorkerThread
    public abstract void a(T[] paramArrayOfT, int paramInt1, int paramInt2);
    
    @WorkerThread
    public int b()
    {
      return 10;
    }
  }
  
  public static abstract class ViewCallback
  {
    @UiThread
    public abstract void a();
    
    @UiThread
    public abstract void a(int paramInt);
    
    @UiThread
    public abstract void a(int[] paramArrayOfInt);
    
    @UiThread
    public void a(int[] paramArrayOfInt1, int[] paramArrayOfInt2, int paramInt)
    {
      int i = paramArrayOfInt1[1] - paramArrayOfInt1[0] + 1;
      int j = i / 2;
      int m = paramArrayOfInt1[0];
      int k;
      if (paramInt == 1)
      {
        k = i;
        paramArrayOfInt2[0] = (m - k);
        k = paramArrayOfInt1[1];
        if (paramInt != 2) {
          break label65;
        }
      }
      for (;;)
      {
        paramArrayOfInt2[1] = (k + i);
        return;
        k = j;
        break;
        label65:
        i = j;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/util/AsyncListUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */