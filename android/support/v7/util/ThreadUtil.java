package android.support.v7.util;

abstract interface ThreadUtil<T>
{
  public static abstract interface BackgroundCallback<T>
  {
    public abstract void a(int paramInt);
    
    public abstract void a(int paramInt1, int paramInt2);
    
    public abstract void a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5);
    
    public abstract void a(TileList.Tile<T> paramTile);
  }
  
  public static abstract interface MainThreadCallback<T>
  {
    public abstract void a(int paramInt1, int paramInt2);
    
    public abstract void a(int paramInt, TileList.Tile<T> paramTile);
    
    public abstract void b(int paramInt1, int paramInt2);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/util/ThreadUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */