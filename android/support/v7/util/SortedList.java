package android.support.v7.util;

import java.util.Comparator;

public class SortedList<T>
{
  public static class BatchedCallback<T2>
    extends SortedList.Callback<T2>
  {
    private final SortedList.Callback<T2> a;
    
    public int compare(T2 paramT21, T2 paramT22)
    {
      return this.a.compare(paramT21, paramT22);
    }
  }
  
  public static abstract class Callback<T2>
    implements Comparator<T2>
  {
    public abstract int compare(T2 paramT21, T2 paramT22);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/util/SortedList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */