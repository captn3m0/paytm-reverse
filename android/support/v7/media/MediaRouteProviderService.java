package android.support.v7.media;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public abstract class MediaRouteProviderService
  extends Service
{
  private static final boolean a = Log.isLoggable("MediaRouteProviderSrv", 3);
  private final ArrayList<ClientRecord> b = new ArrayList();
  private final ReceiveHandler c = new ReceiveHandler(this);
  private final Messenger d = new Messenger(this.c);
  private final PrivateHandler e = new PrivateHandler(null);
  private final ProviderCallback f = new ProviderCallback(null);
  private MediaRouteProvider g;
  private MediaRouteDiscoveryRequest h;
  
  private void a(MediaRouteProviderDescriptor paramMediaRouteProviderDescriptor)
  {
    if (paramMediaRouteProviderDescriptor != null) {}
    for (Bundle localBundle = paramMediaRouteProviderDescriptor.c();; localBundle = null)
    {
      int j = this.b.size();
      int i = 0;
      while (i < j)
      {
        ClientRecord localClientRecord = (ClientRecord)this.b.get(i);
        b(localClientRecord.a, 5, 0, 0, localBundle, null);
        if (a) {
          Log.d("MediaRouteProviderSrv", localClientRecord + ": Sent descriptor change event, descriptor=" + paramMediaRouteProviderDescriptor);
        }
        i += 1;
      }
    }
  }
  
  private boolean a(Messenger paramMessenger, int paramInt1, int paramInt2)
  {
    if ((paramInt2 >= 1) && (d(paramMessenger) < 0))
    {
      Object localObject = new ClientRecord(paramMessenger, paramInt2);
      if (((ClientRecord)localObject).a())
      {
        this.b.add(localObject);
        if (a) {
          Log.d("MediaRouteProviderSrv", localObject + ": Registered, version=" + paramInt2);
        }
        if (paramInt1 != 0)
        {
          localObject = this.g.e();
          if (localObject == null) {
            break label116;
          }
        }
        label116:
        for (localObject = ((MediaRouteProviderDescriptor)localObject).c();; localObject = null)
        {
          b(paramMessenger, 2, paramInt1, 1, localObject, null);
          return true;
        }
      }
    }
    return false;
  }
  
  private boolean a(Messenger paramMessenger, int paramInt1, int paramInt2, int paramInt3)
  {
    ClientRecord localClientRecord = c(paramMessenger);
    if (localClientRecord != null)
    {
      MediaRouteProvider.RouteController localRouteController = localClientRecord.b(paramInt2);
      if (localRouteController != null)
      {
        localRouteController.a(paramInt3);
        if (a) {
          Log.d("MediaRouteProviderSrv", localClientRecord + ": Route unselected" + ", controllerId=" + paramInt2);
        }
        d(paramMessenger, paramInt1);
        return true;
      }
    }
    return false;
  }
  
  private boolean a(final Messenger paramMessenger, final int paramInt1, final int paramInt2, final Intent paramIntent)
  {
    final ClientRecord localClientRecord = c(paramMessenger);
    if (localClientRecord != null)
    {
      MediaRouteProvider.RouteController localRouteController = localClientRecord.b(paramInt2);
      if (localRouteController != null)
      {
        MediaRouter.ControlRequestCallback local1 = null;
        if (paramInt1 != 0) {
          local1 = new MediaRouter.ControlRequestCallback()
          {
            public void a(Bundle paramAnonymousBundle)
            {
              if (MediaRouteProviderService.a()) {
                Log.d("MediaRouteProviderSrv", localClientRecord + ": Route control request succeeded" + ", controllerId=" + paramInt2 + ", intent=" + paramIntent + ", data=" + paramAnonymousBundle);
              }
              if (MediaRouteProviderService.a(MediaRouteProviderService.this, paramMessenger) >= 0) {
                MediaRouteProviderService.a(paramMessenger, 3, paramInt1, 0, paramAnonymousBundle, null);
              }
            }
            
            public void a(String paramAnonymousString, Bundle paramAnonymousBundle)
            {
              if (MediaRouteProviderService.a()) {
                Log.d("MediaRouteProviderSrv", localClientRecord + ": Route control request failed" + ", controllerId=" + paramInt2 + ", intent=" + paramIntent + ", error=" + paramAnonymousString + ", data=" + paramAnonymousBundle);
              }
              if (MediaRouteProviderService.a(MediaRouteProviderService.this, paramMessenger) >= 0)
              {
                if (paramAnonymousString != null)
                {
                  Bundle localBundle = new Bundle();
                  localBundle.putString("error", paramAnonymousString);
                  MediaRouteProviderService.a(paramMessenger, 4, paramInt1, 0, paramAnonymousBundle, localBundle);
                }
              }
              else {
                return;
              }
              MediaRouteProviderService.a(paramMessenger, 4, paramInt1, 0, paramAnonymousBundle, null);
            }
          };
        }
        if (localRouteController.a(paramIntent, local1))
        {
          if (a) {
            Log.d("MediaRouteProviderSrv", localClientRecord + ": Route control request delivered" + ", controllerId=" + paramInt2 + ", intent=" + paramIntent);
          }
          return true;
        }
      }
    }
    return false;
  }
  
  private boolean a(Messenger paramMessenger, int paramInt1, int paramInt2, String paramString)
  {
    ClientRecord localClientRecord = c(paramMessenger);
    if ((localClientRecord != null) && (localClientRecord.a(paramString, paramInt2)))
    {
      if (a) {
        Log.d("MediaRouteProviderSrv", localClientRecord + ": Route controller created" + ", controllerId=" + paramInt2 + ", routeId=" + paramString);
      }
      d(paramMessenger, paramInt1);
      return true;
    }
    return false;
  }
  
  private boolean a(Messenger paramMessenger, int paramInt, MediaRouteDiscoveryRequest paramMediaRouteDiscoveryRequest)
  {
    ClientRecord localClientRecord = c(paramMessenger);
    if (localClientRecord != null)
    {
      boolean bool = localClientRecord.a(paramMediaRouteDiscoveryRequest);
      if (a) {
        Log.d("MediaRouteProviderSrv", localClientRecord + ": Set discovery request, request=" + paramMediaRouteDiscoveryRequest + ", actuallyChanged=" + bool + ", compositeDiscoveryRequest=" + this.h);
      }
      d(paramMessenger, paramInt);
      return true;
    }
    return false;
  }
  
  private void b(Messenger paramMessenger)
  {
    int i = d(paramMessenger);
    if (i >= 0)
    {
      paramMessenger = (ClientRecord)this.b.remove(i);
      if (a) {
        Log.d("MediaRouteProviderSrv", paramMessenger + ": Binder died");
      }
      paramMessenger.b();
    }
  }
  
  private static void b(Messenger paramMessenger, int paramInt1, int paramInt2, int paramInt3, Object paramObject, Bundle paramBundle)
  {
    Message localMessage = Message.obtain();
    localMessage.what = paramInt1;
    localMessage.arg1 = paramInt2;
    localMessage.arg2 = paramInt3;
    localMessage.obj = paramObject;
    localMessage.setData(paramBundle);
    try
    {
      paramMessenger.send(localMessage);
      return;
    }
    catch (RemoteException paramObject)
    {
      Log.e("MediaRouteProviderSrv", "Could not send message to " + e(paramMessenger), (Throwable)paramObject);
      return;
    }
    catch (DeadObjectException paramMessenger) {}
  }
  
  private boolean b()
  {
    Object localObject2 = null;
    Object localObject1 = null;
    boolean bool1 = false;
    int j = this.b.size();
    int i = 0;
    if (i < j)
    {
      MediaRouteDiscoveryRequest localMediaRouteDiscoveryRequest = ((ClientRecord)this.b.get(i)).c;
      boolean bool2 = bool1;
      Object localObject4 = localObject2;
      Object localObject3 = localObject1;
      if (localMediaRouteDiscoveryRequest != null) {
        if (localMediaRouteDiscoveryRequest.a().b())
        {
          bool2 = bool1;
          localObject4 = localObject2;
          localObject3 = localObject1;
          if (!localMediaRouteDiscoveryRequest.b()) {}
        }
        else
        {
          bool2 = bool1 | localMediaRouteDiscoveryRequest.b();
          if (localObject2 != null) {
            break label125;
          }
          localObject4 = localMediaRouteDiscoveryRequest;
          localObject3 = localObject1;
        }
      }
      for (;;)
      {
        i += 1;
        bool1 = bool2;
        localObject2 = localObject4;
        localObject1 = localObject3;
        break;
        label125:
        localObject3 = localObject1;
        if (localObject1 == null) {
          localObject3 = new MediaRouteSelector.Builder(((MediaRouteDiscoveryRequest)localObject2).a());
        }
        ((MediaRouteSelector.Builder)localObject3).a(localMediaRouteDiscoveryRequest.a());
        localObject4 = localObject2;
      }
    }
    if (localObject1 != null) {
      localObject2 = new MediaRouteDiscoveryRequest(((MediaRouteSelector.Builder)localObject1).a(), bool1);
    }
    if ((this.h != localObject2) && ((this.h == null) || (!this.h.equals(localObject2))))
    {
      this.h = ((MediaRouteDiscoveryRequest)localObject2);
      this.g.a((MediaRouteDiscoveryRequest)localObject2);
      return true;
    }
    return false;
  }
  
  private boolean b(Messenger paramMessenger, int paramInt)
  {
    int i = d(paramMessenger);
    if (i >= 0)
    {
      ClientRecord localClientRecord = (ClientRecord)this.b.remove(i);
      if (a) {
        Log.d("MediaRouteProviderSrv", localClientRecord + ": Unregistered");
      }
      localClientRecord.b();
      d(paramMessenger, paramInt);
      return true;
    }
    return false;
  }
  
  private boolean b(Messenger paramMessenger, int paramInt1, int paramInt2)
  {
    ClientRecord localClientRecord = c(paramMessenger);
    if ((localClientRecord != null) && (localClientRecord.a(paramInt2)))
    {
      if (a) {
        Log.d("MediaRouteProviderSrv", localClientRecord + ": Route controller released" + ", controllerId=" + paramInt2);
      }
      d(paramMessenger, paramInt1);
      return true;
    }
    return false;
  }
  
  private boolean b(Messenger paramMessenger, int paramInt1, int paramInt2, int paramInt3)
  {
    ClientRecord localClientRecord = c(paramMessenger);
    if (localClientRecord != null)
    {
      MediaRouteProvider.RouteController localRouteController = localClientRecord.b(paramInt2);
      if (localRouteController != null)
      {
        localRouteController.b(paramInt3);
        if (a) {
          Log.d("MediaRouteProviderSrv", localClientRecord + ": Route volume changed" + ", controllerId=" + paramInt2 + ", volume=" + paramInt3);
        }
        d(paramMessenger, paramInt1);
        return true;
      }
    }
    return false;
  }
  
  private ClientRecord c(Messenger paramMessenger)
  {
    int i = d(paramMessenger);
    if (i >= 0) {
      return (ClientRecord)this.b.get(i);
    }
    return null;
  }
  
  private static void c(Messenger paramMessenger, int paramInt)
  {
    if (paramInt != 0) {
      b(paramMessenger, 0, paramInt, 0, null, null);
    }
  }
  
  private boolean c(Messenger paramMessenger, int paramInt1, int paramInt2)
  {
    ClientRecord localClientRecord = c(paramMessenger);
    if (localClientRecord != null)
    {
      MediaRouteProvider.RouteController localRouteController = localClientRecord.b(paramInt2);
      if (localRouteController != null)
      {
        localRouteController.b();
        if (a) {
          Log.d("MediaRouteProviderSrv", localClientRecord + ": Route selected" + ", controllerId=" + paramInt2);
        }
        d(paramMessenger, paramInt1);
        return true;
      }
    }
    return false;
  }
  
  private boolean c(Messenger paramMessenger, int paramInt1, int paramInt2, int paramInt3)
  {
    ClientRecord localClientRecord = c(paramMessenger);
    if (localClientRecord != null)
    {
      MediaRouteProvider.RouteController localRouteController = localClientRecord.b(paramInt2);
      if (localRouteController != null)
      {
        localRouteController.c(paramInt3);
        if (a) {
          Log.d("MediaRouteProviderSrv", localClientRecord + ": Route volume updated" + ", controllerId=" + paramInt2 + ", delta=" + paramInt3);
        }
        d(paramMessenger, paramInt1);
        return true;
      }
    }
    return false;
  }
  
  private int d(Messenger paramMessenger)
  {
    int j = this.b.size();
    int i = 0;
    while (i < j)
    {
      if (((ClientRecord)this.b.get(i)).a(paramMessenger)) {
        return i;
      }
      i += 1;
    }
    return -1;
  }
  
  private static void d(Messenger paramMessenger, int paramInt)
  {
    if (paramInt != 0) {
      b(paramMessenger, 1, paramInt, 0, null, null);
    }
  }
  
  private static String e(Messenger paramMessenger)
  {
    return "Client connection " + paramMessenger.getBinder().toString();
  }
  
  private final class ClientRecord
    implements IBinder.DeathRecipient
  {
    public final Messenger a;
    public final int b;
    public MediaRouteDiscoveryRequest c;
    private final SparseArray<MediaRouteProvider.RouteController> e = new SparseArray();
    
    public ClientRecord(Messenger paramMessenger, int paramInt)
    {
      this.a = paramMessenger;
      this.b = paramInt;
    }
    
    public boolean a()
    {
      try
      {
        this.a.getBinder().linkToDeath(this, 0);
        return true;
      }
      catch (RemoteException localRemoteException)
      {
        binderDied();
      }
      return false;
    }
    
    public boolean a(int paramInt)
    {
      MediaRouteProvider.RouteController localRouteController = (MediaRouteProvider.RouteController)this.e.get(paramInt);
      if (localRouteController != null)
      {
        this.e.remove(paramInt);
        localRouteController.a();
        return true;
      }
      return false;
    }
    
    public boolean a(Messenger paramMessenger)
    {
      return this.a.getBinder() == paramMessenger.getBinder();
    }
    
    public boolean a(MediaRouteDiscoveryRequest paramMediaRouteDiscoveryRequest)
    {
      if ((this.c != paramMediaRouteDiscoveryRequest) && ((this.c == null) || (!this.c.equals(paramMediaRouteDiscoveryRequest))))
      {
        this.c = paramMediaRouteDiscoveryRequest;
        return MediaRouteProviderService.b(MediaRouteProviderService.this);
      }
      return false;
    }
    
    public boolean a(String paramString, int paramInt)
    {
      if (this.e.indexOfKey(paramInt) < 0)
      {
        paramString = MediaRouteProviderService.a(MediaRouteProviderService.this).a(paramString);
        if (paramString != null)
        {
          this.e.put(paramInt, paramString);
          return true;
        }
      }
      return false;
    }
    
    public MediaRouteProvider.RouteController b(int paramInt)
    {
      return (MediaRouteProvider.RouteController)this.e.get(paramInt);
    }
    
    public void b()
    {
      int j = this.e.size();
      int i = 0;
      while (i < j)
      {
        ((MediaRouteProvider.RouteController)this.e.valueAt(i)).a();
        i += 1;
      }
      this.e.clear();
      this.a.getBinder().unlinkToDeath(this, 0);
      a(null);
    }
    
    public void binderDied()
    {
      MediaRouteProviderService.c(MediaRouteProviderService.this).obtainMessage(1, this.a).sendToTarget();
    }
    
    public String toString()
    {
      return MediaRouteProviderService.a(this.a);
    }
  }
  
  private final class PrivateHandler
    extends Handler
  {
    private PrivateHandler() {}
    
    public void handleMessage(Message paramMessage)
    {
      switch (paramMessage.what)
      {
      default: 
        return;
      }
      MediaRouteProviderService.b(MediaRouteProviderService.this, (Messenger)paramMessage.obj);
    }
  }
  
  private final class ProviderCallback
    extends MediaRouteProvider.Callback
  {
    private ProviderCallback() {}
    
    public void a(MediaRouteProvider paramMediaRouteProvider, MediaRouteProviderDescriptor paramMediaRouteProviderDescriptor)
    {
      MediaRouteProviderService.a(MediaRouteProviderService.this, paramMediaRouteProviderDescriptor);
    }
  }
  
  private static final class ReceiveHandler
    extends Handler
  {
    private final WeakReference<MediaRouteProviderService> a;
    
    public ReceiveHandler(MediaRouteProviderService paramMediaRouteProviderService)
    {
      this.a = new WeakReference(paramMediaRouteProviderService);
    }
    
    private boolean a(int paramInt1, Messenger paramMessenger, int paramInt2, int paramInt3, Object paramObject, Bundle paramBundle)
    {
      int i = 0;
      MediaRouteProviderService localMediaRouteProviderService = (MediaRouteProviderService)this.a.get();
      if (localMediaRouteProviderService != null) {
        switch (paramInt1)
        {
        }
      }
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                return false;
                return MediaRouteProviderService.a(localMediaRouteProviderService, paramMessenger, paramInt2, paramInt3);
                return MediaRouteProviderService.a(localMediaRouteProviderService, paramMessenger, paramInt2);
                paramObject = paramBundle.getString("routeId");
              } while (paramObject == null);
              return MediaRouteProviderService.a(localMediaRouteProviderService, paramMessenger, paramInt2, paramInt3, (String)paramObject);
              return MediaRouteProviderService.b(localMediaRouteProviderService, paramMessenger, paramInt2, paramInt3);
              return MediaRouteProviderService.c(localMediaRouteProviderService, paramMessenger, paramInt2, paramInt3);
              if (paramBundle == null) {}
              for (paramInt1 = i;; paramInt1 = paramBundle.getInt("unselectReason", 0)) {
                return MediaRouteProviderService.a(localMediaRouteProviderService, paramMessenger, paramInt2, paramInt3, paramInt1);
              }
              paramInt1 = paramBundle.getInt("volume", -1);
            } while (paramInt1 < 0);
            return MediaRouteProviderService.b(localMediaRouteProviderService, paramMessenger, paramInt2, paramInt3, paramInt1);
            paramInt1 = paramBundle.getInt("volume", 0);
          } while (paramInt1 == 0);
          return MediaRouteProviderService.c(localMediaRouteProviderService, paramMessenger, paramInt2, paramInt3, paramInt1);
        } while (!(paramObject instanceof Intent));
        return MediaRouteProviderService.a(localMediaRouteProviderService, paramMessenger, paramInt2, paramInt3, (Intent)paramObject);
      } while ((paramObject != null) && (!(paramObject instanceof Bundle)));
      paramObject = MediaRouteDiscoveryRequest.a((Bundle)paramObject);
      if ((paramObject != null) && (((MediaRouteDiscoveryRequest)paramObject).c())) {}
      for (;;)
      {
        return MediaRouteProviderService.a(localMediaRouteProviderService, paramMessenger, paramInt2, (MediaRouteDiscoveryRequest)paramObject);
        paramObject = null;
      }
    }
    
    public void handleMessage(Message paramMessage)
    {
      Messenger localMessenger = paramMessage.replyTo;
      if (MediaRouteProviderProtocol.a(localMessenger))
      {
        i = paramMessage.what;
        j = paramMessage.arg1;
        k = paramMessage.arg2;
        localObject = paramMessage.obj;
        paramMessage = paramMessage.peekData();
        if (!a(i, localMessenger, j, k, localObject, paramMessage))
        {
          if (MediaRouteProviderService.a()) {
            Log.d("MediaRouteProviderSrv", MediaRouteProviderService.a(localMessenger) + ": Message failed, what=" + i + ", requestId=" + j + ", arg=" + k + ", obj=" + localObject + ", data=" + paramMessage);
          }
          MediaRouteProviderService.a(localMessenger, j);
        }
      }
      while (!MediaRouteProviderService.a())
      {
        int i;
        int j;
        int k;
        Object localObject;
        return;
      }
      Log.d("MediaRouteProviderSrv", "Ignoring message without valid reply messenger.");
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/media/MediaRouteProviderService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */