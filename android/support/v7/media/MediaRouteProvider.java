package android.support.v7.media;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public abstract class MediaRouteProvider
{
  private final Context a;
  private final ProviderMetadata b;
  private final ProviderHandler c = new ProviderHandler(null);
  private Callback d;
  private MediaRouteDiscoveryRequest e;
  private boolean f;
  private MediaRouteProviderDescriptor g;
  private boolean h;
  
  MediaRouteProvider(Context paramContext, ProviderMetadata paramProviderMetadata)
  {
    if (paramContext == null) {
      throw new IllegalArgumentException("context must not be null");
    }
    this.a = paramContext;
    if (paramProviderMetadata == null)
    {
      this.b = new ProviderMetadata(new ComponentName(paramContext, getClass()));
      return;
    }
    this.b = paramProviderMetadata;
  }
  
  private void f()
  {
    this.f = false;
    b(this.e);
  }
  
  private void g()
  {
    this.h = false;
    if (this.d != null) {
      this.d.a(this, this.g);
    }
  }
  
  public final Context a()
  {
    return this.a;
  }
  
  @Nullable
  public RouteController a(String paramString)
  {
    return null;
  }
  
  public final void a(MediaRouteDiscoveryRequest paramMediaRouteDiscoveryRequest)
  {
    
    if ((this.e == paramMediaRouteDiscoveryRequest) || ((this.e != null) && (this.e.equals(paramMediaRouteDiscoveryRequest)))) {}
    do
    {
      return;
      this.e = paramMediaRouteDiscoveryRequest;
    } while (this.f);
    this.f = true;
    this.c.sendEmptyMessage(2);
  }
  
  public final void a(@Nullable Callback paramCallback)
  {
    MediaRouter.e();
    this.d = paramCallback;
  }
  
  public final void a(@Nullable MediaRouteProviderDescriptor paramMediaRouteProviderDescriptor)
  {
    
    if (this.g != paramMediaRouteProviderDescriptor)
    {
      this.g = paramMediaRouteProviderDescriptor;
      if (!this.h)
      {
        this.h = true;
        this.c.sendEmptyMessage(1);
      }
    }
  }
  
  public final Handler b()
  {
    return this.c;
  }
  
  public void b(@Nullable MediaRouteDiscoveryRequest paramMediaRouteDiscoveryRequest) {}
  
  public final ProviderMetadata c()
  {
    return this.b;
  }
  
  @Nullable
  public final MediaRouteDiscoveryRequest d()
  {
    return this.e;
  }
  
  @Nullable
  public final MediaRouteProviderDescriptor e()
  {
    return this.g;
  }
  
  public static abstract class Callback
  {
    public void a(@NonNull MediaRouteProvider paramMediaRouteProvider, @Nullable MediaRouteProviderDescriptor paramMediaRouteProviderDescriptor) {}
  }
  
  private final class ProviderHandler
    extends Handler
  {
    private ProviderHandler() {}
    
    public void handleMessage(Message paramMessage)
    {
      switch (paramMessage.what)
      {
      default: 
        return;
      case 1: 
        MediaRouteProvider.a(MediaRouteProvider.this);
        return;
      }
      MediaRouteProvider.b(MediaRouteProvider.this);
    }
  }
  
  public static final class ProviderMetadata
  {
    private final ComponentName a;
    
    ProviderMetadata(ComponentName paramComponentName)
    {
      if (paramComponentName == null) {
        throw new IllegalArgumentException("componentName must not be null");
      }
      this.a = paramComponentName;
    }
    
    public String a()
    {
      return this.a.getPackageName();
    }
    
    public ComponentName b()
    {
      return this.a;
    }
    
    public String toString()
    {
      return "ProviderMetadata{ componentName=" + this.a.flattenToShortString() + " }";
    }
  }
  
  public static abstract class RouteController
  {
    public void a() {}
    
    public void a(int paramInt)
    {
      c();
    }
    
    public boolean a(Intent paramIntent, @Nullable MediaRouter.ControlRequestCallback paramControlRequestCallback)
    {
      return false;
    }
    
    public void b() {}
    
    public void b(int paramInt) {}
    
    public void c() {}
    
    public void c(int paramInt) {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/media/MediaRouteProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */