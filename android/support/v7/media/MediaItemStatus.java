package android.support.v7.media;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.util.TimeUtils;

public final class MediaItemStatus
{
  private final Bundle a;
  
  private MediaItemStatus(Bundle paramBundle)
  {
    this.a = paramBundle;
  }
  
  public static MediaItemStatus a(Bundle paramBundle)
  {
    if (paramBundle != null) {
      return new MediaItemStatus(paramBundle);
    }
    return null;
  }
  
  private static String a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return Integer.toString(paramInt);
    case 0: 
      return "pending";
    case 3: 
      return "buffering";
    case 1: 
      return "playing";
    case 2: 
      return "paused";
    case 4: 
      return "finished";
    case 5: 
      return "canceled";
    case 6: 
      return "invalidated";
    }
    return "error";
  }
  
  public long a()
  {
    return this.a.getLong("timestamp");
  }
  
  public int b()
  {
    return this.a.getInt("playbackState", 7);
  }
  
  public long c()
  {
    return this.a.getLong("contentPosition", -1L);
  }
  
  public long d()
  {
    return this.a.getLong("contentDuration", -1L);
  }
  
  public Bundle e()
  {
    return this.a.getBundle("extras");
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("MediaItemStatus{ ");
    localStringBuilder.append("timestamp=");
    TimeUtils.a(SystemClock.elapsedRealtime() - a(), localStringBuilder);
    localStringBuilder.append(" ms ago");
    localStringBuilder.append(", playbackState=").append(a(b()));
    localStringBuilder.append(", contentPosition=").append(c());
    localStringBuilder.append(", contentDuration=").append(d());
    localStringBuilder.append(", extras=").append(e());
    localStringBuilder.append(" }");
    return localStringBuilder.toString();
  }
  
  public static final class Builder {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/media/MediaItemStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */