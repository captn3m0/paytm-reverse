package android.support.v7.media;

import android.content.Context;
import android.hardware.display.DisplayManager;
import android.media.MediaRouter;
import android.media.MediaRouter.RouteInfo;
import android.os.Build.VERSION;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

final class MediaRouterJellybeanMr1
{
  public static Object a(Callback paramCallback)
  {
    return new CallbackProxy(paramCallback);
  }
  
  public static final class ActiveScanWorkaround
    implements Runnable
  {
    private final DisplayManager a;
    private final Handler b;
    private Method c;
    private boolean d;
    
    public ActiveScanWorkaround(Context paramContext, Handler paramHandler)
    {
      if (Build.VERSION.SDK_INT != 17) {
        throw new UnsupportedOperationException();
      }
      this.a = ((DisplayManager)paramContext.getSystemService("display"));
      this.b = paramHandler;
      try
      {
        this.c = DisplayManager.class.getMethod("scanWifiDisplays", new Class[0]);
        return;
      }
      catch (NoSuchMethodException paramContext) {}
    }
    
    public void a(int paramInt)
    {
      if ((paramInt & 0x2) != 0) {
        if (!this.d)
        {
          if (this.c == null) {
            break label35;
          }
          this.d = true;
          this.b.post(this);
        }
      }
      label35:
      while (!this.d)
      {
        return;
        Log.w("MediaRouterJellybeanMr1", "Cannot scan for wifi displays because the DisplayManager.scanWifiDisplays() method is not available on this device.");
        return;
      }
      this.d = false;
      this.b.removeCallbacks(this);
    }
    
    public void run()
    {
      if (this.d) {}
      try
      {
        this.c.invoke(this.a, new Object[0]);
        this.b.postDelayed(this, 15000L);
        return;
      }
      catch (IllegalAccessException localIllegalAccessException)
      {
        for (;;)
        {
          Log.w("MediaRouterJellybeanMr1", "Cannot scan for wifi displays.", localIllegalAccessException);
        }
      }
      catch (InvocationTargetException localInvocationTargetException)
      {
        for (;;)
        {
          Log.w("MediaRouterJellybeanMr1", "Cannot scan for wifi displays.", localInvocationTargetException);
        }
      }
    }
  }
  
  public static abstract interface Callback
    extends MediaRouterJellybean.Callback
  {
    public abstract void e(Object paramObject);
  }
  
  static class CallbackProxy<T extends MediaRouterJellybeanMr1.Callback>
    extends MediaRouterJellybean.CallbackProxy<T>
  {
    public CallbackProxy(T paramT)
    {
      super();
    }
    
    public void onRoutePresentationDisplayChanged(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      ((MediaRouterJellybeanMr1.Callback)this.a).e(paramRouteInfo);
    }
  }
  
  public static final class IsConnectingWorkaround
  {
    private Method a;
    private int b;
    
    public IsConnectingWorkaround()
    {
      if (Build.VERSION.SDK_INT != 17) {
        throw new UnsupportedOperationException();
      }
      try
      {
        this.b = MediaRouter.RouteInfo.class.getField("STATUS_CONNECTING").getInt(null);
        this.a = MediaRouter.RouteInfo.class.getMethod("getStatusCode", new Class[0]);
        return;
      }
      catch (IllegalAccessException localIllegalAccessException) {}catch (NoSuchMethodException localNoSuchMethodException) {}catch (NoSuchFieldException localNoSuchFieldException) {}
    }
    
    public boolean a(Object paramObject)
    {
      paramObject = (MediaRouter.RouteInfo)paramObject;
      if (this.a != null) {}
      try
      {
        int i = ((Integer)this.a.invoke(paramObject, new Object[0])).intValue();
        int j = this.b;
        return i == j;
      }
      catch (InvocationTargetException paramObject)
      {
        return false;
      }
      catch (IllegalAccessException paramObject)
      {
        for (;;) {}
      }
    }
  }
  
  public static final class RouteInfo
  {
    public static boolean a(Object paramObject)
    {
      return ((MediaRouter.RouteInfo)paramObject).isEnabled();
    }
    
    public static Display b(Object paramObject)
    {
      return ((MediaRouter.RouteInfo)paramObject).getPresentationDisplay();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/media/MediaRouterJellybeanMr1.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */