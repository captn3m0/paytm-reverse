package android.support.v7.media;

import android.content.IntentFilter;
import android.content.IntentSender;
import android.os.Bundle;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class MediaRouteDescriptor
{
  private final Bundle a;
  private List<IntentFilter> b;
  
  private MediaRouteDescriptor(Bundle paramBundle, List<IntentFilter> paramList)
  {
    this.a = paramBundle;
    this.b = paramList;
  }
  
  public static MediaRouteDescriptor a(Bundle paramBundle)
  {
    if (paramBundle != null) {
      return new MediaRouteDescriptor(paramBundle, null);
    }
    return null;
  }
  
  private void r()
  {
    if (this.b == null)
    {
      this.b = this.a.getParcelableArrayList("controlFilters");
      if (this.b == null) {
        this.b = Collections.emptyList();
      }
    }
  }
  
  public String a()
  {
    return this.a.getString("id");
  }
  
  public String b()
  {
    return this.a.getString("name");
  }
  
  public String c()
  {
    return this.a.getString("status");
  }
  
  public boolean d()
  {
    return this.a.getBoolean("enabled", true);
  }
  
  public boolean e()
  {
    return this.a.getBoolean("connecting", false);
  }
  
  public boolean f()
  {
    return this.a.getBoolean("canDisconnect", false);
  }
  
  public IntentSender g()
  {
    return (IntentSender)this.a.getParcelable("settingsIntent");
  }
  
  public List<IntentFilter> h()
  {
    r();
    return this.b;
  }
  
  public int i()
  {
    return this.a.getInt("playbackType", 1);
  }
  
  public int j()
  {
    return this.a.getInt("playbackStream", -1);
  }
  
  public int k()
  {
    return this.a.getInt("volume");
  }
  
  public int l()
  {
    return this.a.getInt("volumeMax");
  }
  
  public int m()
  {
    return this.a.getInt("volumeHandling", 0);
  }
  
  public int n()
  {
    return this.a.getInt("presentationDisplayId", -1);
  }
  
  public Bundle o()
  {
    return this.a.getBundle("extras");
  }
  
  public boolean p()
  {
    r();
    return (!TextUtils.isEmpty(a())) && (!TextUtils.isEmpty(b())) && (!this.b.contains(null));
  }
  
  public Bundle q()
  {
    return this.a;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("MediaRouteDescriptor{ ");
    localStringBuilder.append("id=").append(a());
    localStringBuilder.append(", name=").append(b());
    localStringBuilder.append(", description=").append(c());
    localStringBuilder.append(", isEnabled=").append(d());
    localStringBuilder.append(", isConnecting=").append(e());
    localStringBuilder.append(", controlFilters=").append(Arrays.toString(h().toArray()));
    localStringBuilder.append(", playbackType=").append(i());
    localStringBuilder.append(", playbackStream=").append(j());
    localStringBuilder.append(", volume=").append(k());
    localStringBuilder.append(", volumeMax=").append(l());
    localStringBuilder.append(", volumeHandling=").append(m());
    localStringBuilder.append(", presentationDisplayId=").append(n());
    localStringBuilder.append(", extras=").append(o());
    localStringBuilder.append(", isValid=").append(p());
    localStringBuilder.append(" }");
    return localStringBuilder.toString();
  }
  
  public static final class Builder
  {
    private final Bundle a;
    private ArrayList<IntentFilter> b;
    
    public Builder(MediaRouteDescriptor paramMediaRouteDescriptor)
    {
      if (paramMediaRouteDescriptor == null) {
        throw new IllegalArgumentException("descriptor must not be null");
      }
      this.a = new Bundle(MediaRouteDescriptor.a(paramMediaRouteDescriptor));
      MediaRouteDescriptor.b(paramMediaRouteDescriptor);
      if (!MediaRouteDescriptor.c(paramMediaRouteDescriptor).isEmpty()) {
        this.b = new ArrayList(MediaRouteDescriptor.c(paramMediaRouteDescriptor));
      }
    }
    
    public Builder(String paramString1, String paramString2)
    {
      this.a = new Bundle();
      a(paramString1);
      b(paramString2);
    }
    
    public Builder a(int paramInt)
    {
      this.a.putInt("playbackType", paramInt);
      return this;
    }
    
    public Builder a(IntentFilter paramIntentFilter)
    {
      if (paramIntentFilter == null) {
        throw new IllegalArgumentException("filter must not be null");
      }
      if (this.b == null) {
        this.b = new ArrayList();
      }
      if (!this.b.contains(paramIntentFilter)) {
        this.b.add(paramIntentFilter);
      }
      return this;
    }
    
    public Builder a(String paramString)
    {
      this.a.putString("id", paramString);
      return this;
    }
    
    public Builder a(Collection<IntentFilter> paramCollection)
    {
      if (paramCollection == null) {
        throw new IllegalArgumentException("filters must not be null");
      }
      if (!paramCollection.isEmpty())
      {
        paramCollection = paramCollection.iterator();
        while (paramCollection.hasNext()) {
          a((IntentFilter)paramCollection.next());
        }
      }
      return this;
    }
    
    public Builder a(boolean paramBoolean)
    {
      this.a.putBoolean("enabled", paramBoolean);
      return this;
    }
    
    public MediaRouteDescriptor a()
    {
      if (this.b != null) {
        this.a.putParcelableArrayList("controlFilters", this.b);
      }
      return new MediaRouteDescriptor(this.a, this.b, null);
    }
    
    public Builder b(int paramInt)
    {
      this.a.putInt("playbackStream", paramInt);
      return this;
    }
    
    public Builder b(String paramString)
    {
      this.a.putString("name", paramString);
      return this;
    }
    
    public Builder b(boolean paramBoolean)
    {
      this.a.putBoolean("connecting", paramBoolean);
      return this;
    }
    
    public Builder c(int paramInt)
    {
      this.a.putInt("volume", paramInt);
      return this;
    }
    
    public Builder c(String paramString)
    {
      this.a.putString("status", paramString);
      return this;
    }
    
    public Builder d(int paramInt)
    {
      this.a.putInt("volumeMax", paramInt);
      return this;
    }
    
    public Builder e(int paramInt)
    {
      this.a.putInt("volumeHandling", paramInt);
      return this;
    }
    
    public Builder f(int paramInt)
    {
      this.a.putInt("presentationDisplayId", paramInt);
      return this;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/media/MediaRouteDescriptor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */