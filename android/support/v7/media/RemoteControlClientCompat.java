package android.support.v7.media;

import java.lang.ref.WeakReference;

abstract class RemoteControlClientCompat
{
  protected final Object a;
  protected VolumeCallback b;
  
  public void a(PlaybackInfo paramPlaybackInfo) {}
  
  static class JellybeanImpl
    extends RemoteControlClientCompat
  {
    private final Object c;
    private boolean d;
    
    public void a(RemoteControlClientCompat.PlaybackInfo paramPlaybackInfo)
    {
      MediaRouterJellybean.UserRouteInfo.c(this.c, paramPlaybackInfo.a);
      MediaRouterJellybean.UserRouteInfo.d(this.c, paramPlaybackInfo.b);
      MediaRouterJellybean.UserRouteInfo.e(this.c, paramPlaybackInfo.c);
      MediaRouterJellybean.UserRouteInfo.b(this.c, paramPlaybackInfo.d);
      MediaRouterJellybean.UserRouteInfo.a(this.c, paramPlaybackInfo.e);
      if (!this.d)
      {
        this.d = true;
        MediaRouterJellybean.UserRouteInfo.a(this.c, MediaRouterJellybean.a(new VolumeCallbackWrapper(this)));
        MediaRouterJellybean.UserRouteInfo.b(this.c, this.a);
      }
    }
    
    private static final class VolumeCallbackWrapper
      implements MediaRouterJellybean.VolumeCallback
    {
      private final WeakReference<RemoteControlClientCompat.JellybeanImpl> a;
      
      public VolumeCallbackWrapper(RemoteControlClientCompat.JellybeanImpl paramJellybeanImpl)
      {
        this.a = new WeakReference(paramJellybeanImpl);
      }
      
      public void a(Object paramObject, int paramInt)
      {
        paramObject = (RemoteControlClientCompat.JellybeanImpl)this.a.get();
        if ((paramObject != null) && (((RemoteControlClientCompat.JellybeanImpl)paramObject).b != null)) {
          ((RemoteControlClientCompat.JellybeanImpl)paramObject).b.a(paramInt);
        }
      }
      
      public void b(Object paramObject, int paramInt)
      {
        paramObject = (RemoteControlClientCompat.JellybeanImpl)this.a.get();
        if ((paramObject != null) && (((RemoteControlClientCompat.JellybeanImpl)paramObject).b != null)) {
          ((RemoteControlClientCompat.JellybeanImpl)paramObject).b.b(paramInt);
        }
      }
    }
  }
  
  static class LegacyImpl
    extends RemoteControlClientCompat
  {}
  
  public static final class PlaybackInfo
  {
    public int a;
    public int b;
    public int c = 0;
    public int d = 3;
    public int e = 1;
  }
  
  public static abstract interface VolumeCallback
  {
    public abstract void a(int paramInt);
    
    public abstract void b(int paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/media/RemoteControlClientCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */