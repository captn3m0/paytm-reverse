package android.support.v7.media;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

final class RegisteredMediaRouteProvider
  extends MediaRouteProvider
  implements ServiceConnection
{
  private static final boolean a = Log.isLoggable("MediaRouteProviderProxy", 3);
  private final ComponentName b;
  private final PrivateHandler c;
  private final ArrayList<Controller> d = new ArrayList();
  private boolean e;
  private boolean f;
  private Connection g;
  private boolean h;
  
  public RegisteredMediaRouteProvider(Context paramContext, ComponentName paramComponentName)
  {
    super(paramContext, new MediaRouteProvider.ProviderMetadata(paramComponentName));
    this.b = paramComponentName;
    this.c = new PrivateHandler(null);
  }
  
  private void a(Connection paramConnection)
  {
    if (this.g == paramConnection)
    {
      this.h = true;
      o();
      paramConnection = d();
      if (paramConnection != null) {
        this.g.a(paramConnection);
      }
    }
  }
  
  private void a(Connection paramConnection, MediaRouteProviderDescriptor paramMediaRouteProviderDescriptor)
  {
    if (this.g == paramConnection)
    {
      if (a) {
        Log.d("MediaRouteProviderProxy", this + ": Descriptor changed, descriptor=" + paramMediaRouteProviderDescriptor);
      }
      a(paramMediaRouteProviderDescriptor);
    }
  }
  
  private void a(Connection paramConnection, String paramString)
  {
    if (this.g == paramConnection)
    {
      if (a) {
        Log.d("MediaRouteProviderProxy", this + ": Service connection error - " + paramString);
      }
      m();
    }
  }
  
  private void a(Controller paramController)
  {
    this.d.remove(paramController);
    paramController.d();
    j();
  }
  
  private void b(Connection paramConnection)
  {
    if (this.g == paramConnection)
    {
      if (a) {
        Log.d("MediaRouteProviderProxy", this + ": Service connection died");
      }
      n();
    }
  }
  
  private void j()
  {
    if (k())
    {
      l();
      return;
    }
    m();
  }
  
  private boolean k()
  {
    if (this.e)
    {
      if (d() != null) {}
      while (!this.d.isEmpty()) {
        return true;
      }
    }
    return false;
  }
  
  private void l()
  {
    Intent localIntent;
    if (!this.f)
    {
      if (a) {
        Log.d("MediaRouteProviderProxy", this + ": Binding");
      }
      localIntent = new Intent("android.media.MediaRouteProviderService");
      localIntent.setComponent(this.b);
    }
    try
    {
      this.f = a().bindService(localIntent, this, 1);
      if ((!this.f) && (a)) {
        Log.d("MediaRouteProviderProxy", this + ": Bind failed");
      }
      return;
    }
    catch (SecurityException localSecurityException)
    {
      while (!a) {}
      Log.d("MediaRouteProviderProxy", this + ": Bind failed", localSecurityException);
    }
  }
  
  private void m()
  {
    if (this.f)
    {
      if (a) {
        Log.d("MediaRouteProviderProxy", this + ": Unbinding");
      }
      this.f = false;
      n();
      a().unbindService(this);
    }
  }
  
  private void n()
  {
    if (this.g != null)
    {
      a(null);
      this.h = false;
      p();
      this.g.b();
      this.g = null;
    }
  }
  
  private void o()
  {
    int j = this.d.size();
    int i = 0;
    while (i < j)
    {
      ((Controller)this.d.get(i)).a(this.g);
      i += 1;
    }
  }
  
  private void p()
  {
    int j = this.d.size();
    int i = 0;
    while (i < j)
    {
      ((Controller)this.d.get(i)).d();
      i += 1;
    }
  }
  
  public MediaRouteProvider.RouteController a(String paramString)
  {
    Object localObject = e();
    if (localObject != null)
    {
      localObject = ((MediaRouteProviderDescriptor)localObject).a();
      int j = ((List)localObject).size();
      int i = 0;
      while (i < j)
      {
        if (((MediaRouteDescriptor)((List)localObject).get(i)).a().equals(paramString))
        {
          paramString = new Controller(paramString);
          this.d.add(paramString);
          if (this.h) {
            paramString.a(this.g);
          }
          j();
          return paramString;
        }
        i += 1;
      }
    }
    return null;
  }
  
  public boolean a(String paramString1, String paramString2)
  {
    return (this.b.getPackageName().equals(paramString1)) && (this.b.getClassName().equals(paramString2));
  }
  
  public void b(MediaRouteDiscoveryRequest paramMediaRouteDiscoveryRequest)
  {
    if (this.h) {
      this.g.a(paramMediaRouteDiscoveryRequest);
    }
    j();
  }
  
  public void f()
  {
    if (!this.e)
    {
      if (a) {
        Log.d("MediaRouteProviderProxy", this + ": Starting");
      }
      this.e = true;
      j();
    }
  }
  
  public void g()
  {
    if (this.e)
    {
      if (a) {
        Log.d("MediaRouteProviderProxy", this + ": Stopping");
      }
      this.e = false;
      j();
    }
  }
  
  public void h()
  {
    if ((this.g == null) && (k()))
    {
      m();
      l();
    }
  }
  
  public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    if (a) {
      Log.d("MediaRouteProviderProxy", this + ": Connected");
    }
    if (this.f)
    {
      n();
      if (paramIBinder == null) {
        break label86;
      }
      paramComponentName = new Messenger(paramIBinder);
      if (!MediaRouteProviderProtocol.a(paramComponentName)) {
        break label124;
      }
      paramComponentName = new Connection(paramComponentName);
      if (!paramComponentName.a()) {
        break label91;
      }
      this.g = paramComponentName;
    }
    label86:
    label91:
    while (!a)
    {
      return;
      paramComponentName = null;
      break;
    }
    Log.d("MediaRouteProviderProxy", this + ": Registration failed");
    return;
    label124:
    Log.e("MediaRouteProviderProxy", this + ": Service returned invalid messenger binder");
  }
  
  public void onServiceDisconnected(ComponentName paramComponentName)
  {
    if (a) {
      Log.d("MediaRouteProviderProxy", this + ": Service disconnected");
    }
    n();
  }
  
  public String toString()
  {
    return "Service connection " + this.b.flattenToShortString();
  }
  
  private final class Connection
    implements IBinder.DeathRecipient
  {
    private final Messenger b;
    private final RegisteredMediaRouteProvider.ReceiveHandler c;
    private final Messenger d;
    private int e = 1;
    private int f = 1;
    private int g;
    private int h;
    private final SparseArray<MediaRouter.ControlRequestCallback> i = new SparseArray();
    
    public Connection(Messenger paramMessenger)
    {
      this.b = paramMessenger;
      this.c = new RegisteredMediaRouteProvider.ReceiveHandler(this);
      this.d = new Messenger(this.c);
    }
    
    private boolean a(int paramInt1, int paramInt2, int paramInt3, Object paramObject, Bundle paramBundle)
    {
      Message localMessage = Message.obtain();
      localMessage.what = paramInt1;
      localMessage.arg1 = paramInt2;
      localMessage.arg2 = paramInt3;
      localMessage.obj = paramObject;
      localMessage.setData(paramBundle);
      localMessage.replyTo = this.d;
      try
      {
        this.b.send(localMessage);
        return true;
      }
      catch (RemoteException paramObject)
      {
        if (paramInt1 != 2) {
          Log.e("MediaRouteProviderProxy", "Could not send message to service.", (Throwable)paramObject);
        }
        return false;
      }
      catch (DeadObjectException paramObject)
      {
        for (;;) {}
      }
    }
    
    private void c()
    {
      int j = 0;
      while (j < this.i.size())
      {
        ((MediaRouter.ControlRequestCallback)this.i.valueAt(j)).a(null, null);
        j += 1;
      }
      this.i.clear();
    }
    
    public int a(String paramString)
    {
      int j = this.f;
      this.f = (j + 1);
      Bundle localBundle = new Bundle();
      localBundle.putString("routeId", paramString);
      int k = this.e;
      this.e = (k + 1);
      a(3, k, j, null, localBundle);
      return j;
    }
    
    public void a(int paramInt1, int paramInt2)
    {
      Bundle localBundle = new Bundle();
      localBundle.putInt("unselectReason", paramInt2);
      paramInt2 = this.e;
      this.e = (paramInt2 + 1);
      a(6, paramInt2, paramInt1, null, localBundle);
    }
    
    public void a(MediaRouteDiscoveryRequest paramMediaRouteDiscoveryRequest)
    {
      int j = this.e;
      this.e = (j + 1);
      if (paramMediaRouteDiscoveryRequest != null) {}
      for (paramMediaRouteDiscoveryRequest = paramMediaRouteDiscoveryRequest.d();; paramMediaRouteDiscoveryRequest = null)
      {
        a(10, j, 0, paramMediaRouteDiscoveryRequest, null);
        return;
      }
    }
    
    public boolean a()
    {
      int j = this.e;
      this.e = (j + 1);
      this.h = j;
      if (!a(1, this.h, 1, null, null)) {
        return false;
      }
      try
      {
        this.b.getBinder().linkToDeath(this, 0);
        return true;
      }
      catch (RemoteException localRemoteException)
      {
        binderDied();
      }
      return false;
    }
    
    public boolean a(int paramInt)
    {
      if (paramInt == this.h)
      {
        this.h = 0;
        RegisteredMediaRouteProvider.a(RegisteredMediaRouteProvider.this, this, "Registation failed");
      }
      MediaRouter.ControlRequestCallback localControlRequestCallback = (MediaRouter.ControlRequestCallback)this.i.get(paramInt);
      if (localControlRequestCallback != null)
      {
        this.i.remove(paramInt);
        localControlRequestCallback.a(null, null);
      }
      return true;
    }
    
    public boolean a(int paramInt1, int paramInt2, Bundle paramBundle)
    {
      if ((this.g == 0) && (paramInt1 == this.h) && (paramInt2 >= 1))
      {
        this.h = 0;
        this.g = paramInt2;
        RegisteredMediaRouteProvider.a(RegisteredMediaRouteProvider.this, this, MediaRouteProviderDescriptor.a(paramBundle));
        RegisteredMediaRouteProvider.a(RegisteredMediaRouteProvider.this, this);
        return true;
      }
      return false;
    }
    
    public boolean a(int paramInt, Intent paramIntent, MediaRouter.ControlRequestCallback paramControlRequestCallback)
    {
      int j = this.e;
      this.e = (j + 1);
      if (a(9, j, paramInt, paramIntent, null))
      {
        if (paramControlRequestCallback != null) {
          this.i.put(j, paramControlRequestCallback);
        }
        return true;
      }
      return false;
    }
    
    public boolean a(int paramInt, Bundle paramBundle)
    {
      MediaRouter.ControlRequestCallback localControlRequestCallback = (MediaRouter.ControlRequestCallback)this.i.get(paramInt);
      if (localControlRequestCallback != null)
      {
        this.i.remove(paramInt);
        localControlRequestCallback.a(paramBundle);
        return true;
      }
      return false;
    }
    
    public boolean a(int paramInt, String paramString, Bundle paramBundle)
    {
      MediaRouter.ControlRequestCallback localControlRequestCallback = (MediaRouter.ControlRequestCallback)this.i.get(paramInt);
      if (localControlRequestCallback != null)
      {
        this.i.remove(paramInt);
        localControlRequestCallback.a(paramString, paramBundle);
        return true;
      }
      return false;
    }
    
    public boolean a(Bundle paramBundle)
    {
      if (this.g != 0)
      {
        RegisteredMediaRouteProvider.a(RegisteredMediaRouteProvider.this, this, MediaRouteProviderDescriptor.a(paramBundle));
        return true;
      }
      return false;
    }
    
    public void b()
    {
      a(2, 0, 0, null, null);
      this.c.a();
      this.b.getBinder().unlinkToDeath(this, 0);
      RegisteredMediaRouteProvider.a(RegisteredMediaRouteProvider.this).post(new Runnable()
      {
        public void run()
        {
          RegisteredMediaRouteProvider.Connection.a(RegisteredMediaRouteProvider.Connection.this);
        }
      });
    }
    
    public void b(int paramInt1, int paramInt2)
    {
      Bundle localBundle = new Bundle();
      localBundle.putInt("volume", paramInt2);
      paramInt2 = this.e;
      this.e = (paramInt2 + 1);
      a(7, paramInt2, paramInt1, null, localBundle);
    }
    
    public boolean b(int paramInt)
    {
      return true;
    }
    
    public void binderDied()
    {
      RegisteredMediaRouteProvider.a(RegisteredMediaRouteProvider.this).post(new Runnable()
      {
        public void run()
        {
          RegisteredMediaRouteProvider.b(RegisteredMediaRouteProvider.this, RegisteredMediaRouteProvider.Connection.this);
        }
      });
    }
    
    public void c(int paramInt)
    {
      int j = this.e;
      this.e = (j + 1);
      a(4, j, paramInt, null, null);
    }
    
    public void c(int paramInt1, int paramInt2)
    {
      Bundle localBundle = new Bundle();
      localBundle.putInt("volume", paramInt2);
      paramInt2 = this.e;
      this.e = (paramInt2 + 1);
      a(8, paramInt2, paramInt1, null, localBundle);
    }
    
    public void d(int paramInt)
    {
      int j = this.e;
      this.e = (j + 1);
      a(5, j, paramInt, null, null);
    }
  }
  
  private final class Controller
    extends MediaRouteProvider.RouteController
  {
    private final String b;
    private boolean c;
    private int d = -1;
    private int e;
    private RegisteredMediaRouteProvider.Connection f;
    private int g;
    
    public Controller(String paramString)
    {
      this.b = paramString;
    }
    
    public void a()
    {
      RegisteredMediaRouteProvider.a(RegisteredMediaRouteProvider.this, this);
    }
    
    public void a(int paramInt)
    {
      this.c = false;
      if (this.f != null) {
        this.f.a(this.g, paramInt);
      }
    }
    
    public void a(RegisteredMediaRouteProvider.Connection paramConnection)
    {
      this.f = paramConnection;
      this.g = paramConnection.a(this.b);
      if (this.c)
      {
        paramConnection.d(this.g);
        if (this.d >= 0)
        {
          paramConnection.b(this.g, this.d);
          this.d = -1;
        }
        if (this.e != 0)
        {
          paramConnection.c(this.g, this.e);
          this.e = 0;
        }
      }
    }
    
    public boolean a(Intent paramIntent, MediaRouter.ControlRequestCallback paramControlRequestCallback)
    {
      if (this.f != null) {
        return this.f.a(this.g, paramIntent, paramControlRequestCallback);
      }
      return false;
    }
    
    public void b()
    {
      this.c = true;
      if (this.f != null) {
        this.f.d(this.g);
      }
    }
    
    public void b(int paramInt)
    {
      if (this.f != null)
      {
        this.f.b(this.g, paramInt);
        return;
      }
      this.d = paramInt;
      this.e = 0;
    }
    
    public void c()
    {
      a(0);
    }
    
    public void c(int paramInt)
    {
      if (this.f != null)
      {
        this.f.c(this.g, paramInt);
        return;
      }
      this.e += paramInt;
    }
    
    public void d()
    {
      if (this.f != null)
      {
        this.f.c(this.g);
        this.f = null;
        this.g = 0;
      }
    }
  }
  
  private final class PrivateHandler
    extends Handler
  {
    private PrivateHandler() {}
  }
  
  private static final class ReceiveHandler
    extends Handler
  {
    private final WeakReference<RegisteredMediaRouteProvider.Connection> a;
    
    public ReceiveHandler(RegisteredMediaRouteProvider.Connection paramConnection)
    {
      this.a = new WeakReference(paramConnection);
    }
    
    private boolean a(RegisteredMediaRouteProvider.Connection paramConnection, int paramInt1, int paramInt2, int paramInt3, Object paramObject, Bundle paramBundle)
    {
      switch (paramInt1)
      {
      }
      do
      {
        do
        {
          do
          {
            do
            {
              return false;
              paramConnection.a(paramInt2);
              return true;
              paramConnection.b(paramInt2);
              return true;
            } while ((paramObject != null) && (!(paramObject instanceof Bundle)));
            return paramConnection.a(paramInt2, paramInt3, (Bundle)paramObject);
          } while ((paramObject != null) && (!(paramObject instanceof Bundle)));
          return paramConnection.a((Bundle)paramObject);
        } while ((paramObject != null) && (!(paramObject instanceof Bundle)));
        return paramConnection.a(paramInt2, (Bundle)paramObject);
      } while ((paramObject != null) && (!(paramObject instanceof Bundle)));
      if (paramBundle == null) {}
      for (paramBundle = null;; paramBundle = paramBundle.getString("error")) {
        return paramConnection.a(paramInt2, paramBundle, (Bundle)paramObject);
      }
    }
    
    public void a()
    {
      this.a.clear();
    }
    
    public void handleMessage(Message paramMessage)
    {
      RegisteredMediaRouteProvider.Connection localConnection = (RegisteredMediaRouteProvider.Connection)this.a.get();
      if ((localConnection != null) && (!a(localConnection, paramMessage.what, paramMessage.arg1, paramMessage.arg2, paramMessage.obj, paramMessage.peekData())) && (RegisteredMediaRouteProvider.i())) {
        Log.d("MediaRouteProviderProxy", "Unhandled message from server: " + paramMessage);
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/media/RegisteredMediaRouteProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */