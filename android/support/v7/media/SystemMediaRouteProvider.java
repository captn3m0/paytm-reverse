package android.support.v7.media;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.media.AudioManager;
import android.os.Build.VERSION;
import android.support.v7.mediarouter.R.string;
import android.view.Display;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

abstract class SystemMediaRouteProvider
  extends MediaRouteProvider
{
  protected SystemMediaRouteProvider(Context paramContext)
  {
    super(paramContext, new MediaRouteProvider.ProviderMetadata(new ComponentName("android", SystemMediaRouteProvider.class.getName())));
  }
  
  public static SystemMediaRouteProvider a(Context paramContext, SyncCallback paramSyncCallback)
  {
    if (Build.VERSION.SDK_INT >= 18) {
      return new JellybeanMr2Impl(paramContext, paramSyncCallback);
    }
    if (Build.VERSION.SDK_INT >= 17) {
      return new JellybeanMr1Impl(paramContext, paramSyncCallback);
    }
    if (Build.VERSION.SDK_INT >= 16) {
      return new JellybeanImpl(paramContext, paramSyncCallback);
    }
    return new LegacyImpl(paramContext);
  }
  
  public void a(MediaRouter.RouteInfo paramRouteInfo) {}
  
  public void b(MediaRouter.RouteInfo paramRouteInfo) {}
  
  public void c(MediaRouter.RouteInfo paramRouteInfo) {}
  
  public void d(MediaRouter.RouteInfo paramRouteInfo) {}
  
  static class JellybeanImpl
    extends SystemMediaRouteProvider
    implements MediaRouterJellybean.Callback, MediaRouterJellybean.VolumeCallback
  {
    private static final ArrayList<IntentFilter> j;
    private static final ArrayList<IntentFilter> k;
    protected final Object a;
    protected final Object b;
    protected final Object c;
    protected final Object d;
    protected int e;
    protected boolean f;
    protected boolean g;
    protected final ArrayList<SystemRouteRecord> h = new ArrayList();
    protected final ArrayList<UserRouteRecord> i = new ArrayList();
    private final SystemMediaRouteProvider.SyncCallback l;
    private MediaRouterJellybean.SelectRouteWorkaround m;
    private MediaRouterJellybean.GetDefaultRouteWorkaround n;
    
    static
    {
      IntentFilter localIntentFilter = new IntentFilter();
      localIntentFilter.addCategory("android.media.intent.category.LIVE_AUDIO");
      j = new ArrayList();
      j.add(localIntentFilter);
      localIntentFilter = new IntentFilter();
      localIntentFilter.addCategory("android.media.intent.category.LIVE_VIDEO");
      k = new ArrayList();
      k.add(localIntentFilter);
    }
    
    public JellybeanImpl(Context paramContext, SystemMediaRouteProvider.SyncCallback paramSyncCallback)
    {
      super();
      this.l = paramSyncCallback;
      this.a = MediaRouterJellybean.a(paramContext);
      this.b = h();
      this.c = i();
      paramContext = paramContext.getResources();
      this.d = MediaRouterJellybean.a(this.a, paramContext.getString(R.string.mr_user_route_category_name), false);
      k();
    }
    
    private boolean e(Object paramObject)
    {
      if ((g(paramObject) == null) && (f(paramObject) < 0))
      {
        paramObject = new SystemRouteRecord(paramObject, j(paramObject));
        a((SystemRouteRecord)paramObject);
        this.h.add(paramObject);
        return true;
      }
      return false;
    }
    
    private String j(Object paramObject)
    {
      if (j() == paramObject)
      {
        i1 = 1;
        if (i1 == 0) {
          break label32;
        }
      }
      label32:
      for (paramObject = "DEFAULT_ROUTE";; paramObject = String.format(Locale.US, "ROUTE_%08x", new Object[] { Integer.valueOf(h(paramObject).hashCode()) }))
      {
        if (b((String)paramObject) >= 0) {
          break label62;
        }
        return (String)paramObject;
        i1 = 0;
        break;
      }
      label62:
      int i1 = 2;
      for (;;)
      {
        String str = String.format(Locale.US, "%s_%d", new Object[] { paramObject, Integer.valueOf(i1) });
        if (b(str) < 0) {
          return str;
        }
        i1 += 1;
      }
    }
    
    private void k()
    {
      boolean bool = false;
      Iterator localIterator = MediaRouterJellybean.a(this.a).iterator();
      while (localIterator.hasNext()) {
        bool |= e(localIterator.next());
      }
      if (bool) {
        f();
      }
    }
    
    public MediaRouteProvider.RouteController a(String paramString)
    {
      int i1 = b(paramString);
      if (i1 >= 0) {
        return new SystemRouteController(((SystemRouteRecord)this.h.get(i1)).a);
      }
      return null;
    }
    
    public void a(int paramInt, Object paramObject)
    {
      if (paramObject != MediaRouterJellybean.a(this.a, 8388611)) {}
      do
      {
        do
        {
          return;
          UserRouteRecord localUserRouteRecord = g(paramObject);
          if (localUserRouteRecord != null)
          {
            localUserRouteRecord.a.o();
            return;
          }
          paramInt = f(paramObject);
        } while (paramInt < 0);
        paramObject = (SystemRouteRecord)this.h.get(paramInt);
        paramObject = this.l.a(((SystemRouteRecord)paramObject).b);
      } while (paramObject == null);
      ((MediaRouter.RouteInfo)paramObject).o();
    }
    
    public void a(MediaRouter.RouteInfo paramRouteInfo)
    {
      if (paramRouteInfo.q() != this)
      {
        Object localObject = MediaRouterJellybean.b(this.a, this.d);
        paramRouteInfo = new UserRouteRecord(paramRouteInfo, localObject);
        MediaRouterJellybean.RouteInfo.a(localObject, paramRouteInfo);
        MediaRouterJellybean.UserRouteInfo.a(localObject, this.c);
        a(paramRouteInfo);
        this.i.add(paramRouteInfo);
        MediaRouterJellybean.c(this.a, localObject);
      }
      int i1;
      do
      {
        return;
        i1 = f(MediaRouterJellybean.a(this.a, 8388611));
      } while ((i1 < 0) || (!((SystemRouteRecord)this.h.get(i1)).b.equals(paramRouteInfo.p())));
      paramRouteInfo.o();
    }
    
    protected void a(SystemRouteRecord paramSystemRouteRecord)
    {
      MediaRouteDescriptor.Builder localBuilder = new MediaRouteDescriptor.Builder(paramSystemRouteRecord.b, h(paramSystemRouteRecord.a));
      a(paramSystemRouteRecord, localBuilder);
      paramSystemRouteRecord.c = localBuilder.a();
    }
    
    protected void a(SystemRouteRecord paramSystemRouteRecord, MediaRouteDescriptor.Builder paramBuilder)
    {
      int i1 = MediaRouterJellybean.RouteInfo.a(paramSystemRouteRecord.a);
      if ((i1 & 0x1) != 0) {
        paramBuilder.a(j);
      }
      if ((i1 & 0x2) != 0) {
        paramBuilder.a(k);
      }
      paramBuilder.a(MediaRouterJellybean.RouteInfo.b(paramSystemRouteRecord.a));
      paramBuilder.b(MediaRouterJellybean.RouteInfo.c(paramSystemRouteRecord.a));
      paramBuilder.c(MediaRouterJellybean.RouteInfo.d(paramSystemRouteRecord.a));
      paramBuilder.d(MediaRouterJellybean.RouteInfo.e(paramSystemRouteRecord.a));
      paramBuilder.e(MediaRouterJellybean.RouteInfo.f(paramSystemRouteRecord.a));
    }
    
    protected void a(UserRouteRecord paramUserRouteRecord)
    {
      MediaRouterJellybean.UserRouteInfo.a(paramUserRouteRecord.b, paramUserRouteRecord.a.a());
      MediaRouterJellybean.UserRouteInfo.a(paramUserRouteRecord.b, paramUserRouteRecord.a.g());
      MediaRouterJellybean.UserRouteInfo.b(paramUserRouteRecord.b, paramUserRouteRecord.a.h());
      MediaRouterJellybean.UserRouteInfo.c(paramUserRouteRecord.b, paramUserRouteRecord.a.j());
      MediaRouterJellybean.UserRouteInfo.d(paramUserRouteRecord.b, paramUserRouteRecord.a.k());
      MediaRouterJellybean.UserRouteInfo.e(paramUserRouteRecord.b, paramUserRouteRecord.a.i());
    }
    
    public void a(Object paramObject)
    {
      if (e(paramObject)) {
        f();
      }
    }
    
    public void a(Object paramObject, int paramInt)
    {
      paramObject = g(paramObject);
      if (paramObject != null) {
        ((UserRouteRecord)paramObject).a.a(paramInt);
      }
    }
    
    public void a(Object paramObject1, Object paramObject2) {}
    
    public void a(Object paramObject1, Object paramObject2, int paramInt) {}
    
    protected int b(String paramString)
    {
      int i2 = this.h.size();
      int i1 = 0;
      while (i1 < i2)
      {
        if (((SystemRouteRecord)this.h.get(i1)).b.equals(paramString)) {
          return i1;
        }
        i1 += 1;
      }
      return -1;
    }
    
    public void b(int paramInt, Object paramObject) {}
    
    public void b(MediaRouteDiscoveryRequest paramMediaRouteDiscoveryRequest)
    {
      int i1 = 0;
      int i2 = 0;
      boolean bool = false;
      if (paramMediaRouteDiscoveryRequest != null)
      {
        List localList = paramMediaRouteDiscoveryRequest.a().a();
        int i4 = localList.size();
        int i3 = 0;
        i1 = i2;
        i2 = i3;
        if (i2 < i4)
        {
          String str = (String)localList.get(i2);
          if (str.equals("android.media.intent.category.LIVE_AUDIO")) {
            i1 |= 0x1;
          }
          for (;;)
          {
            i2 += 1;
            break;
            if (str.equals("android.media.intent.category.LIVE_VIDEO")) {
              i1 |= 0x2;
            } else {
              i1 |= 0x800000;
            }
          }
        }
        bool = paramMediaRouteDiscoveryRequest.b();
      }
      if ((this.e != i1) || (this.f != bool))
      {
        this.e = i1;
        this.f = bool;
        g();
        k();
      }
    }
    
    public void b(MediaRouter.RouteInfo paramRouteInfo)
    {
      if (paramRouteInfo.q() != this)
      {
        int i1 = e(paramRouteInfo);
        if (i1 >= 0)
        {
          paramRouteInfo = (UserRouteRecord)this.i.remove(i1);
          MediaRouterJellybean.RouteInfo.a(paramRouteInfo.b, null);
          MediaRouterJellybean.UserRouteInfo.a(paramRouteInfo.b, null);
          MediaRouterJellybean.d(this.a, paramRouteInfo.b);
        }
      }
    }
    
    public void b(Object paramObject)
    {
      if (g(paramObject) == null)
      {
        int i1 = f(paramObject);
        if (i1 >= 0)
        {
          this.h.remove(i1);
          f();
        }
      }
    }
    
    public void b(Object paramObject, int paramInt)
    {
      paramObject = g(paramObject);
      if (paramObject != null) {
        ((UserRouteRecord)paramObject).a.b(paramInt);
      }
    }
    
    public void c(MediaRouter.RouteInfo paramRouteInfo)
    {
      if (paramRouteInfo.q() != this)
      {
        int i1 = e(paramRouteInfo);
        if (i1 >= 0) {
          a((UserRouteRecord)this.i.get(i1));
        }
      }
    }
    
    public void c(Object paramObject)
    {
      if (g(paramObject) == null)
      {
        int i1 = f(paramObject);
        if (i1 >= 0)
        {
          a((SystemRouteRecord)this.h.get(i1));
          f();
        }
      }
    }
    
    public void d(MediaRouter.RouteInfo paramRouteInfo)
    {
      if (!paramRouteInfo.e()) {}
      int i1;
      do
      {
        do
        {
          return;
          if (paramRouteInfo.q() == this) {
            break;
          }
          i1 = e(paramRouteInfo);
        } while (i1 < 0);
        i(((UserRouteRecord)this.i.get(i1)).b);
        return;
        i1 = b(paramRouteInfo.p());
      } while (i1 < 0);
      i(((SystemRouteRecord)this.h.get(i1)).a);
    }
    
    public void d(Object paramObject)
    {
      if (g(paramObject) == null)
      {
        int i1 = f(paramObject);
        if (i1 >= 0)
        {
          SystemRouteRecord localSystemRouteRecord = (SystemRouteRecord)this.h.get(i1);
          i1 = MediaRouterJellybean.RouteInfo.d(paramObject);
          if (i1 != localSystemRouteRecord.c.k())
          {
            localSystemRouteRecord.c = new MediaRouteDescriptor.Builder(localSystemRouteRecord.c).c(i1).a();
            f();
          }
        }
      }
    }
    
    protected int e(MediaRouter.RouteInfo paramRouteInfo)
    {
      int i2 = this.i.size();
      int i1 = 0;
      while (i1 < i2)
      {
        if (((UserRouteRecord)this.i.get(i1)).a == paramRouteInfo) {
          return i1;
        }
        i1 += 1;
      }
      return -1;
    }
    
    protected int f(Object paramObject)
    {
      int i2 = this.h.size();
      int i1 = 0;
      while (i1 < i2)
      {
        if (((SystemRouteRecord)this.h.get(i1)).a == paramObject) {
          return i1;
        }
        i1 += 1;
      }
      return -1;
    }
    
    protected void f()
    {
      MediaRouteProviderDescriptor.Builder localBuilder = new MediaRouteProviderDescriptor.Builder();
      int i2 = this.h.size();
      int i1 = 0;
      while (i1 < i2)
      {
        localBuilder.a(((SystemRouteRecord)this.h.get(i1)).c);
        i1 += 1;
      }
      a(localBuilder.a());
    }
    
    protected UserRouteRecord g(Object paramObject)
    {
      paramObject = MediaRouterJellybean.RouteInfo.g(paramObject);
      if ((paramObject instanceof UserRouteRecord)) {
        return (UserRouteRecord)paramObject;
      }
      return null;
    }
    
    protected void g()
    {
      if (this.g)
      {
        this.g = false;
        MediaRouterJellybean.a(this.a, this.b);
      }
      if (this.e != 0)
      {
        this.g = true;
        MediaRouterJellybean.b(this.a, this.e, this.b);
      }
    }
    
    protected Object h()
    {
      return MediaRouterJellybean.a(this);
    }
    
    protected String h(Object paramObject)
    {
      paramObject = MediaRouterJellybean.RouteInfo.a(paramObject, a());
      if (paramObject != null) {
        return ((CharSequence)paramObject).toString();
      }
      return "";
    }
    
    protected Object i()
    {
      return MediaRouterJellybean.a(this);
    }
    
    protected void i(Object paramObject)
    {
      if (this.m == null) {
        this.m = new MediaRouterJellybean.SelectRouteWorkaround();
      }
      this.m.a(this.a, 8388611, paramObject);
    }
    
    protected Object j()
    {
      if (this.n == null) {
        this.n = new MediaRouterJellybean.GetDefaultRouteWorkaround();
      }
      return this.n.a(this.a);
    }
    
    protected final class SystemRouteController
      extends MediaRouteProvider.RouteController
    {
      private final Object b;
      
      public SystemRouteController(Object paramObject)
      {
        this.b = paramObject;
      }
      
      public void b(int paramInt)
      {
        MediaRouterJellybean.RouteInfo.a(this.b, paramInt);
      }
      
      public void c(int paramInt)
      {
        MediaRouterJellybean.RouteInfo.b(this.b, paramInt);
      }
    }
    
    protected static final class SystemRouteRecord
    {
      public final Object a;
      public final String b;
      public MediaRouteDescriptor c;
      
      public SystemRouteRecord(Object paramObject, String paramString)
      {
        this.a = paramObject;
        this.b = paramString;
      }
    }
    
    protected static final class UserRouteRecord
    {
      public final MediaRouter.RouteInfo a;
      public final Object b;
      
      public UserRouteRecord(MediaRouter.RouteInfo paramRouteInfo, Object paramObject)
      {
        this.a = paramRouteInfo;
        this.b = paramObject;
      }
    }
  }
  
  private static class JellybeanMr1Impl
    extends SystemMediaRouteProvider.JellybeanImpl
    implements MediaRouterJellybeanMr1.Callback
  {
    private MediaRouterJellybeanMr1.ActiveScanWorkaround j;
    private MediaRouterJellybeanMr1.IsConnectingWorkaround k;
    
    public JellybeanMr1Impl(Context paramContext, SystemMediaRouteProvider.SyncCallback paramSyncCallback)
    {
      super(paramSyncCallback);
    }
    
    protected void a(SystemMediaRouteProvider.JellybeanImpl.SystemRouteRecord paramSystemRouteRecord, MediaRouteDescriptor.Builder paramBuilder)
    {
      super.a(paramSystemRouteRecord, paramBuilder);
      if (!MediaRouterJellybeanMr1.RouteInfo.a(paramSystemRouteRecord.a)) {
        paramBuilder.a(false);
      }
      if (b(paramSystemRouteRecord)) {
        paramBuilder.b(true);
      }
      paramSystemRouteRecord = MediaRouterJellybeanMr1.RouteInfo.b(paramSystemRouteRecord.a);
      if (paramSystemRouteRecord != null) {
        paramBuilder.f(paramSystemRouteRecord.getDisplayId());
      }
    }
    
    protected boolean b(SystemMediaRouteProvider.JellybeanImpl.SystemRouteRecord paramSystemRouteRecord)
    {
      if (this.k == null) {
        this.k = new MediaRouterJellybeanMr1.IsConnectingWorkaround();
      }
      return this.k.a(paramSystemRouteRecord.a);
    }
    
    public void e(Object paramObject)
    {
      int i = f(paramObject);
      SystemMediaRouteProvider.JellybeanImpl.SystemRouteRecord localSystemRouteRecord;
      if (i >= 0)
      {
        localSystemRouteRecord = (SystemMediaRouteProvider.JellybeanImpl.SystemRouteRecord)this.h.get(i);
        paramObject = MediaRouterJellybeanMr1.RouteInfo.b(paramObject);
        if (paramObject == null) {
          break label74;
        }
      }
      label74:
      for (i = ((Display)paramObject).getDisplayId();; i = -1)
      {
        if (i != localSystemRouteRecord.c.n())
        {
          localSystemRouteRecord.c = new MediaRouteDescriptor.Builder(localSystemRouteRecord.c).f(i).a();
          f();
        }
        return;
      }
    }
    
    protected void g()
    {
      super.g();
      if (this.j == null) {
        this.j = new MediaRouterJellybeanMr1.ActiveScanWorkaround(a(), b());
      }
      MediaRouterJellybeanMr1.ActiveScanWorkaround localActiveScanWorkaround = this.j;
      if (this.f) {}
      for (int i = this.e;; i = 0)
      {
        localActiveScanWorkaround.a(i);
        return;
      }
    }
    
    protected Object h()
    {
      return MediaRouterJellybeanMr1.a(this);
    }
  }
  
  private static class JellybeanMr2Impl
    extends SystemMediaRouteProvider.JellybeanMr1Impl
  {
    public JellybeanMr2Impl(Context paramContext, SystemMediaRouteProvider.SyncCallback paramSyncCallback)
    {
      super(paramSyncCallback);
    }
    
    protected void a(SystemMediaRouteProvider.JellybeanImpl.SystemRouteRecord paramSystemRouteRecord, MediaRouteDescriptor.Builder paramBuilder)
    {
      super.a(paramSystemRouteRecord, paramBuilder);
      paramSystemRouteRecord = MediaRouterJellybeanMr2.RouteInfo.a(paramSystemRouteRecord.a);
      if (paramSystemRouteRecord != null) {
        paramBuilder.c(paramSystemRouteRecord.toString());
      }
    }
    
    protected void a(SystemMediaRouteProvider.JellybeanImpl.UserRouteRecord paramUserRouteRecord)
    {
      super.a(paramUserRouteRecord);
      MediaRouterJellybeanMr2.UserRouteInfo.a(paramUserRouteRecord.b, paramUserRouteRecord.a.b());
    }
    
    protected boolean b(SystemMediaRouteProvider.JellybeanImpl.SystemRouteRecord paramSystemRouteRecord)
    {
      return MediaRouterJellybeanMr2.RouteInfo.b(paramSystemRouteRecord.a);
    }
    
    protected void g()
    {
      int i = 1;
      if (this.g) {
        MediaRouterJellybean.a(this.a, this.b);
      }
      this.g = true;
      Object localObject1 = this.a;
      int j = this.e;
      Object localObject2 = this.b;
      if (this.f) {}
      for (;;)
      {
        MediaRouterJellybeanMr2.a(localObject1, j, localObject2, i | 0x2);
        return;
        i = 0;
      }
    }
    
    protected void i(Object paramObject)
    {
      MediaRouterJellybean.a(this.a, 8388611, paramObject);
    }
    
    protected Object j()
    {
      return MediaRouterJellybeanMr2.a(this.a);
    }
  }
  
  static class LegacyImpl
    extends SystemMediaRouteProvider
  {
    private static final ArrayList<IntentFilter> a;
    private final AudioManager b;
    private final VolumeChangeReceiver c;
    private int d = -1;
    
    static
    {
      IntentFilter localIntentFilter = new IntentFilter();
      localIntentFilter.addCategory("android.media.intent.category.LIVE_AUDIO");
      localIntentFilter.addCategory("android.media.intent.category.LIVE_VIDEO");
      a = new ArrayList();
      a.add(localIntentFilter);
    }
    
    public LegacyImpl(Context paramContext)
    {
      super();
      this.b = ((AudioManager)paramContext.getSystemService("audio"));
      this.c = new VolumeChangeReceiver();
      paramContext.registerReceiver(this.c, new IntentFilter("android.media.VOLUME_CHANGED_ACTION"));
      f();
    }
    
    private void f()
    {
      Object localObject = a().getResources();
      int i = this.b.getStreamMaxVolume(3);
      this.d = this.b.getStreamVolume(3);
      localObject = new MediaRouteDescriptor.Builder("DEFAULT_ROUTE", ((Resources)localObject).getString(R.string.mr_system_route_name)).a(a).b(3).a(0).e(1).d(i).c(this.d).a();
      a(new MediaRouteProviderDescriptor.Builder().a((MediaRouteDescriptor)localObject).a());
    }
    
    public MediaRouteProvider.RouteController a(String paramString)
    {
      if (paramString.equals("DEFAULT_ROUTE")) {
        return new DefaultRouteController();
      }
      return null;
    }
    
    final class DefaultRouteController
      extends MediaRouteProvider.RouteController
    {
      DefaultRouteController() {}
      
      public void b(int paramInt)
      {
        SystemMediaRouteProvider.LegacyImpl.a(SystemMediaRouteProvider.LegacyImpl.this).setStreamVolume(3, paramInt, 0);
        SystemMediaRouteProvider.LegacyImpl.b(SystemMediaRouteProvider.LegacyImpl.this);
      }
      
      public void c(int paramInt)
      {
        int i = SystemMediaRouteProvider.LegacyImpl.a(SystemMediaRouteProvider.LegacyImpl.this).getStreamVolume(3);
        if (Math.min(SystemMediaRouteProvider.LegacyImpl.a(SystemMediaRouteProvider.LegacyImpl.this).getStreamMaxVolume(3), Math.max(0, i + paramInt)) != i) {
          SystemMediaRouteProvider.LegacyImpl.a(SystemMediaRouteProvider.LegacyImpl.this).setStreamVolume(3, i, 0);
        }
        SystemMediaRouteProvider.LegacyImpl.b(SystemMediaRouteProvider.LegacyImpl.this);
      }
    }
    
    final class VolumeChangeReceiver
      extends BroadcastReceiver
    {
      VolumeChangeReceiver() {}
      
      public void onReceive(Context paramContext, Intent paramIntent)
      {
        if ((paramIntent.getAction().equals("android.media.VOLUME_CHANGED_ACTION")) && (paramIntent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_TYPE", -1) == 3))
        {
          int i = paramIntent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_VALUE", -1);
          if ((i >= 0) && (i != SystemMediaRouteProvider.LegacyImpl.c(SystemMediaRouteProvider.LegacyImpl.this))) {
            SystemMediaRouteProvider.LegacyImpl.b(SystemMediaRouteProvider.LegacyImpl.this);
          }
        }
      }
    }
  }
  
  public static abstract interface SyncCallback
  {
    public abstract MediaRouter.RouteInfo a(String paramString);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/media/SystemMediaRouteProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */