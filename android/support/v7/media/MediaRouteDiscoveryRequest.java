package android.support.v7.media;

import android.os.Bundle;

public final class MediaRouteDiscoveryRequest
{
  private final Bundle a;
  private MediaRouteSelector b;
  
  private MediaRouteDiscoveryRequest(Bundle paramBundle)
  {
    this.a = paramBundle;
  }
  
  public MediaRouteDiscoveryRequest(MediaRouteSelector paramMediaRouteSelector, boolean paramBoolean)
  {
    if (paramMediaRouteSelector == null) {
      throw new IllegalArgumentException("selector must not be null");
    }
    this.a = new Bundle();
    this.b = paramMediaRouteSelector;
    this.a.putBundle("selector", paramMediaRouteSelector.d());
    this.a.putBoolean("activeScan", paramBoolean);
  }
  
  public static MediaRouteDiscoveryRequest a(Bundle paramBundle)
  {
    if (paramBundle != null) {
      return new MediaRouteDiscoveryRequest(paramBundle);
    }
    return null;
  }
  
  private void e()
  {
    if (this.b == null)
    {
      this.b = MediaRouteSelector.a(this.a.getBundle("selector"));
      if (this.b == null) {
        this.b = MediaRouteSelector.a;
      }
    }
  }
  
  public MediaRouteSelector a()
  {
    e();
    return this.b;
  }
  
  public boolean b()
  {
    return this.a.getBoolean("activeScan");
  }
  
  public boolean c()
  {
    e();
    return this.b.c();
  }
  
  public Bundle d()
  {
    return this.a;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if ((paramObject instanceof MediaRouteDiscoveryRequest))
    {
      paramObject = (MediaRouteDiscoveryRequest)paramObject;
      bool1 = bool2;
      if (a().equals(((MediaRouteDiscoveryRequest)paramObject).a()))
      {
        bool1 = bool2;
        if (b() == ((MediaRouteDiscoveryRequest)paramObject).b()) {
          bool1 = true;
        }
      }
    }
    return bool1;
  }
  
  public int hashCode()
  {
    int j = a().hashCode();
    if (b()) {}
    for (int i = 1;; i = 0) {
      return i ^ j;
    }
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("DiscoveryRequest{ selector=").append(a());
    localStringBuilder.append(", activeScan=").append(b());
    localStringBuilder.append(", isValid=").append(c());
    localStringBuilder.append(" }");
    return localStringBuilder.toString();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/media/MediaRouteDiscoveryRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */