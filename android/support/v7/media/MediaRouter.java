package android.support.v7.media;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityManagerCompat;
import android.support.v4.hardware.display.DisplayManagerCompat;
import android.support.v4.media.VolumeProviderCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.MediaSessionCompat.OnActiveChangeListener;
import android.support.v4.media.session.MediaSessionCompat.Token;
import android.util.Log;
import android.view.Display;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public final class MediaRouter
{
  static GlobalMediaRouter a;
  private static final boolean d = Log.isLoggable("MediaRouter", 3);
  final Context b;
  final ArrayList<CallbackRecord> c = new ArrayList();
  
  MediaRouter(Context paramContext)
  {
    this.b = paramContext;
  }
  
  public static MediaRouter a(@NonNull Context paramContext)
  {
    if (paramContext == null) {
      throw new IllegalArgumentException("context must not be null");
    }
    e();
    if (a == null)
    {
      a = new GlobalMediaRouter(paramContext.getApplicationContext());
      a.a();
    }
    return a.a(paramContext);
  }
  
  static <T> boolean a(T paramT1, T paramT2)
  {
    return (paramT1 == paramT2) || ((paramT1 != null) && (paramT2 != null) && (paramT1.equals(paramT2)));
  }
  
  private int b(Callback paramCallback)
  {
    int j = this.c.size();
    int i = 0;
    while (i < j)
    {
      if (((CallbackRecord)this.c.get(i)).b == paramCallback) {
        return i;
      }
      i += 1;
    }
    return -1;
  }
  
  static void e()
  {
    if (Looper.myLooper() != Looper.getMainLooper()) {
      throw new IllegalStateException("The media router service must only be accessed on the application's main thread.");
    }
  }
  
  public List<RouteInfo> a()
  {
    e();
    return a.b();
  }
  
  public void a(int paramInt)
  {
    if ((paramInt < 0) || (paramInt > 3)) {
      throw new IllegalArgumentException("Unsupported reason to unselect route");
    }
    e();
    a.c(b(), paramInt);
  }
  
  public void a(MediaRouteSelector paramMediaRouteSelector, Callback paramCallback)
  {
    a(paramMediaRouteSelector, paramCallback, 0);
  }
  
  public void a(@NonNull MediaRouteSelector paramMediaRouteSelector, @NonNull Callback paramCallback, int paramInt)
  {
    if (paramMediaRouteSelector == null) {
      throw new IllegalArgumentException("selector must not be null");
    }
    if (paramCallback == null) {
      throw new IllegalArgumentException("callback must not be null");
    }
    e();
    if (d) {
      Log.d("MediaRouter", "addCallback: selector=" + paramMediaRouteSelector + ", callback=" + paramCallback + ", flags=" + Integer.toHexString(paramInt));
    }
    int i = b(paramCallback);
    if (i < 0)
    {
      paramCallback = new CallbackRecord(this, paramCallback);
      this.c.add(paramCallback);
    }
    for (;;)
    {
      i = 0;
      if (((paramCallback.d ^ 0xFFFFFFFF) & paramInt) != 0)
      {
        paramCallback.d |= paramInt;
        i = 1;
      }
      if (!paramCallback.c.a(paramMediaRouteSelector))
      {
        paramCallback.c = new MediaRouteSelector.Builder(paramCallback.c).a(paramMediaRouteSelector).a();
        i = 1;
      }
      if (i != 0) {
        a.e();
      }
      return;
      paramCallback = (CallbackRecord)this.c.get(i);
    }
  }
  
  public void a(@NonNull Callback paramCallback)
  {
    if (paramCallback == null) {
      throw new IllegalArgumentException("callback must not be null");
    }
    e();
    if (d) {
      Log.d("MediaRouter", "removeCallback: callback=" + paramCallback);
    }
    int i = b(paramCallback);
    if (i >= 0)
    {
      this.c.remove(i);
      a.e();
    }
  }
  
  public void a(@NonNull RouteInfo paramRouteInfo)
  {
    if (paramRouteInfo == null) {
      throw new IllegalArgumentException("route must not be null");
    }
    e();
    if (d) {
      Log.d("MediaRouter", "selectRoute: " + paramRouteInfo);
    }
    a.a(paramRouteInfo);
  }
  
  public boolean a(@NonNull MediaRouteSelector paramMediaRouteSelector, int paramInt)
  {
    if (paramMediaRouteSelector == null) {
      throw new IllegalArgumentException("selector must not be null");
    }
    e();
    return a.a(paramMediaRouteSelector, paramInt);
  }
  
  @NonNull
  public RouteInfo b()
  {
    e();
    return a.c();
  }
  
  @NonNull
  public RouteInfo c()
  {
    e();
    return a.d();
  }
  
  public MediaSessionCompat.Token d()
  {
    return a.f();
  }
  
  public static abstract class Callback
  {
    public void a(MediaRouter paramMediaRouter, MediaRouter.ProviderInfo paramProviderInfo) {}
    
    public void a(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo) {}
    
    public void b(MediaRouter paramMediaRouter, MediaRouter.ProviderInfo paramProviderInfo) {}
    
    public void b(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo) {}
    
    public void c(MediaRouter paramMediaRouter, MediaRouter.ProviderInfo paramProviderInfo) {}
    
    public void c(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo) {}
    
    public void d(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo) {}
    
    public void e(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo) {}
    
    public void f(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo) {}
    
    public void g(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo) {}
  }
  
  private static final class CallbackRecord
  {
    public final MediaRouter a;
    public final MediaRouter.Callback b;
    public MediaRouteSelector c;
    public int d;
    
    public CallbackRecord(MediaRouter paramMediaRouter, MediaRouter.Callback paramCallback)
    {
      this.a = paramMediaRouter;
      this.b = paramCallback;
      this.c = MediaRouteSelector.a;
    }
    
    public boolean a(MediaRouter.RouteInfo paramRouteInfo)
    {
      return ((this.d & 0x2) != 0) || (paramRouteInfo.a(this.c));
    }
  }
  
  public static abstract class ControlRequestCallback
  {
    public void a(Bundle paramBundle) {}
    
    public void a(String paramString, Bundle paramBundle) {}
  }
  
  private static final class GlobalMediaRouter
    implements RegisteredMediaRouteProviderWatcher.Callback, SystemMediaRouteProvider.SyncCallback
  {
    private final Context a;
    private final ArrayList<WeakReference<MediaRouter>> b = new ArrayList();
    private final ArrayList<MediaRouter.RouteInfo> c = new ArrayList();
    private final ArrayList<MediaRouter.ProviderInfo> d = new ArrayList();
    private final ArrayList<RemoteControlClientRecord> e = new ArrayList();
    private final RemoteControlClientCompat.PlaybackInfo f = new RemoteControlClientCompat.PlaybackInfo();
    private final ProviderCallback g = new ProviderCallback(null);
    private final CallbackHandler h = new CallbackHandler(null);
    private final DisplayManagerCompat i;
    private final SystemMediaRouteProvider j;
    private final boolean k;
    private RegisteredMediaRouteProviderWatcher l;
    private MediaRouter.RouteInfo m;
    private MediaRouter.RouteInfo n;
    private MediaRouteProvider.RouteController o;
    private MediaRouteDiscoveryRequest p;
    private MediaSessionRecord q;
    private MediaSessionCompat r;
    private MediaSessionCompat.OnActiveChangeListener s = new MediaSessionCompat.OnActiveChangeListener() {};
    
    GlobalMediaRouter(Context paramContext)
    {
      this.a = paramContext;
      this.i = DisplayManagerCompat.a(paramContext);
      this.k = ActivityManagerCompat.isLowRamDevice((ActivityManager)paramContext.getSystemService("activity"));
      this.j = SystemMediaRouteProvider.a(paramContext, this);
      a(this.j);
    }
    
    private String a(MediaRouter.ProviderInfo paramProviderInfo, String paramString)
    {
      paramProviderInfo = paramProviderInfo.c().flattenToShortString() + ":" + paramString;
      if (b(paramProviderInfo) < 0) {
        return paramProviderInfo;
      }
      int i1 = 2;
      for (;;)
      {
        paramString = String.format(Locale.US, "%s_%d", new Object[] { paramProviderInfo, Integer.valueOf(i1) });
        if (b(paramString) < 0) {
          return paramString;
        }
        i1 += 1;
      }
    }
    
    private void a(MediaRouteProvider paramMediaRouteProvider, MediaRouteProviderDescriptor paramMediaRouteProviderDescriptor)
    {
      int i1 = c(paramMediaRouteProvider);
      if (i1 >= 0) {
        a((MediaRouter.ProviderInfo)this.d.get(i1), paramMediaRouteProviderDescriptor);
      }
    }
    
    private void a(MediaRouter.ProviderInfo paramProviderInfo, MediaRouteProviderDescriptor paramMediaRouteProviderDescriptor)
    {
      if (paramProviderInfo.a(paramMediaRouteProviderDescriptor))
      {
        int i2 = 0;
        boolean bool3 = false;
        boolean bool2 = false;
        boolean bool1 = bool3;
        int i1 = i2;
        if (paramMediaRouteProviderDescriptor != null)
        {
          if (!paramMediaRouteProviderDescriptor.b()) {
            break label546;
          }
          paramMediaRouteProviderDescriptor = paramMediaRouteProviderDescriptor.a();
          int i4 = paramMediaRouteProviderDescriptor.size();
          i2 = 0;
          i1 = 0;
          bool1 = bool2;
          if (i2 < i4)
          {
            MediaRouteDescriptor localMediaRouteDescriptor = (MediaRouteDescriptor)paramMediaRouteProviderDescriptor.get(i2);
            Object localObject = localMediaRouteDescriptor.a();
            int i5 = paramProviderInfo.a((String)localObject);
            ArrayList localArrayList;
            int i3;
            if (i5 < 0)
            {
              localObject = new MediaRouter.RouteInfo(paramProviderInfo, (String)localObject, a(paramProviderInfo, (String)localObject));
              localArrayList = MediaRouter.ProviderInfo.b(paramProviderInfo);
              i3 = i1 + 1;
              localArrayList.add(i1, localObject);
              this.c.add(localObject);
              ((MediaRouter.RouteInfo)localObject).a(localMediaRouteDescriptor);
              if (MediaRouter.f()) {
                Log.d("MediaRouter", "Route added: " + localObject);
              }
              this.h.a(257, localObject);
              i1 = i3;
              bool2 = bool1;
            }
            for (;;)
            {
              i2 += 1;
              bool1 = bool2;
              break;
              if (i5 < i1)
              {
                Log.w("MediaRouter", "Ignoring route descriptor with duplicate id: " + localMediaRouteDescriptor);
                bool2 = bool1;
              }
              else
              {
                localObject = (MediaRouter.RouteInfo)MediaRouter.ProviderInfo.b(paramProviderInfo).get(i5);
                localArrayList = MediaRouter.ProviderInfo.b(paramProviderInfo);
                i3 = i1 + 1;
                Collections.swap(localArrayList, i5, i1);
                i5 = ((MediaRouter.RouteInfo)localObject).a(localMediaRouteDescriptor);
                bool2 = bool1;
                i1 = i3;
                if (i5 != 0)
                {
                  if ((i5 & 0x1) != 0)
                  {
                    if (MediaRouter.f()) {
                      Log.d("MediaRouter", "Route changed: " + localObject);
                    }
                    this.h.a(259, localObject);
                  }
                  if ((i5 & 0x2) != 0)
                  {
                    if (MediaRouter.f()) {
                      Log.d("MediaRouter", "Route volume changed: " + localObject);
                    }
                    this.h.a(260, localObject);
                  }
                  if ((i5 & 0x4) != 0)
                  {
                    if (MediaRouter.f()) {
                      Log.d("MediaRouter", "Route presentation display changed: " + localObject);
                    }
                    this.h.a(261, localObject);
                  }
                  bool2 = bool1;
                  i1 = i3;
                  if (localObject == this.n)
                  {
                    bool2 = true;
                    i1 = i3;
                  }
                }
              }
            }
          }
        }
        for (;;)
        {
          i2 = MediaRouter.ProviderInfo.b(paramProviderInfo).size() - 1;
          while (i2 >= i1)
          {
            paramMediaRouteProviderDescriptor = (MediaRouter.RouteInfo)MediaRouter.ProviderInfo.b(paramProviderInfo).get(i2);
            paramMediaRouteProviderDescriptor.a(null);
            this.c.remove(paramMediaRouteProviderDescriptor);
            i2 -= 1;
          }
          label546:
          Log.w("MediaRouter", "Ignoring invalid provider descriptor: " + paramMediaRouteProviderDescriptor);
          bool1 = bool3;
          i1 = i2;
        }
        a(bool1);
        i2 = MediaRouter.ProviderInfo.b(paramProviderInfo).size() - 1;
        while (i2 >= i1)
        {
          paramMediaRouteProviderDescriptor = (MediaRouter.RouteInfo)MediaRouter.ProviderInfo.b(paramProviderInfo).remove(i2);
          if (MediaRouter.f()) {
            Log.d("MediaRouter", "Route removed: " + paramMediaRouteProviderDescriptor);
          }
          this.h.a(258, paramMediaRouteProviderDescriptor);
          i2 -= 1;
        }
        if (MediaRouter.f()) {
          Log.d("MediaRouter", "Provider changed: " + paramProviderInfo);
        }
        this.h.a(515, paramProviderInfo);
      }
    }
    
    private void a(boolean paramBoolean)
    {
      if ((this.m != null) && (!c(this.m)))
      {
        Log.i("MediaRouter", "Clearing the default route because it is no longer selectable: " + this.m);
        this.m = null;
      }
      if ((this.m == null) && (!this.c.isEmpty()))
      {
        Iterator localIterator = this.c.iterator();
        while (localIterator.hasNext())
        {
          MediaRouter.RouteInfo localRouteInfo = (MediaRouter.RouteInfo)localIterator.next();
          if ((d(localRouteInfo)) && (c(localRouteInfo)))
          {
            this.m = localRouteInfo;
            Log.i("MediaRouter", "Found default route: " + this.m);
          }
        }
      }
      if ((this.n != null) && (!c(this.n)))
      {
        Log.i("MediaRouter", "Unselecting the current route because it is no longer selectable: " + this.n);
        d(null, 0);
      }
      if (this.n == null) {
        d(g(), 0);
      }
      while (!paramBoolean) {
        return;
      }
      h();
    }
    
    private int b(String paramString)
    {
      int i2 = this.c.size();
      int i1 = 0;
      while (i1 < i2)
      {
        if (MediaRouter.RouteInfo.c((MediaRouter.RouteInfo)this.c.get(i1)).equals(paramString)) {
          return i1;
        }
        i1 += 1;
      }
      return -1;
    }
    
    private boolean b(MediaRouter.RouteInfo paramRouteInfo)
    {
      return (paramRouteInfo.q() == this.j) && (paramRouteInfo.a("android.media.intent.category.LIVE_AUDIO")) && (!paramRouteInfo.a("android.media.intent.category.LIVE_VIDEO"));
    }
    
    private int c(MediaRouteProvider paramMediaRouteProvider)
    {
      int i2 = this.d.size();
      int i1 = 0;
      while (i1 < i2)
      {
        if (MediaRouter.ProviderInfo.a((MediaRouter.ProviderInfo)this.d.get(i1)) == paramMediaRouteProvider) {
          return i1;
        }
        i1 += 1;
      }
      return -1;
    }
    
    private boolean c(MediaRouter.RouteInfo paramRouteInfo)
    {
      return (MediaRouter.RouteInfo.d(paramRouteInfo) != null) && (MediaRouter.RouteInfo.b(paramRouteInfo));
    }
    
    private void d(MediaRouter.RouteInfo paramRouteInfo, int paramInt)
    {
      if (this.n != paramRouteInfo)
      {
        if (this.n != null)
        {
          if (MediaRouter.f()) {
            Log.d("MediaRouter", "Route unselected: " + this.n + " reason: " + paramInt);
          }
          this.h.a(263, this.n);
          if (this.o != null)
          {
            this.o.a(paramInt);
            this.o.a();
            this.o = null;
          }
        }
        this.n = paramRouteInfo;
        if (this.n != null)
        {
          this.o = paramRouteInfo.q().a(MediaRouter.RouteInfo.a(paramRouteInfo));
          if (this.o != null) {
            this.o.b();
          }
          if (MediaRouter.f()) {
            Log.d("MediaRouter", "Route selected: " + this.n);
          }
          this.h.a(262, this.n);
        }
        h();
      }
    }
    
    private boolean d(MediaRouter.RouteInfo paramRouteInfo)
    {
      return (paramRouteInfo.q() == this.j) && (MediaRouter.RouteInfo.a(paramRouteInfo).equals("DEFAULT_ROUTE"));
    }
    
    private MediaRouter.RouteInfo g()
    {
      Iterator localIterator = this.c.iterator();
      while (localIterator.hasNext())
      {
        MediaRouter.RouteInfo localRouteInfo = (MediaRouter.RouteInfo)localIterator.next();
        if ((localRouteInfo != this.m) && (b(localRouteInfo)) && (c(localRouteInfo))) {
          return localRouteInfo;
        }
      }
      return this.m;
    }
    
    private void h()
    {
      if (this.n != null)
      {
        this.f.a = this.n.j();
        this.f.b = this.n.k();
        this.f.c = this.n.i();
        this.f.d = this.n.h();
        this.f.e = this.n.g();
        i2 = this.e.size();
        i1 = 0;
        while (i1 < i2)
        {
          ((RemoteControlClientRecord)this.e.get(i1)).a();
          i1 += 1;
        }
        if (this.q != null)
        {
          if (this.n != c()) {
            break label139;
          }
          this.q.a();
        }
      }
      label139:
      while (this.q == null)
      {
        int i2;
        return;
        int i1 = 0;
        if (this.f.c == 1) {
          i1 = 2;
        }
        this.q.a(i1, this.f.b, this.f.a);
        return;
      }
      this.q.a();
    }
    
    public MediaRouter.RouteInfo a(String paramString)
    {
      int i1 = c(this.j);
      if (i1 >= 0)
      {
        MediaRouter.ProviderInfo localProviderInfo = (MediaRouter.ProviderInfo)this.d.get(i1);
        i1 = localProviderInfo.a(paramString);
        if (i1 >= 0) {
          return (MediaRouter.RouteInfo)MediaRouter.ProviderInfo.b(localProviderInfo).get(i1);
        }
      }
      return null;
    }
    
    public MediaRouter a(Context paramContext)
    {
      int i1 = this.b.size();
      MediaRouter localMediaRouter;
      do
      {
        for (;;)
        {
          i1 -= 1;
          if (i1 < 0) {
            break label60;
          }
          localMediaRouter = (MediaRouter)((WeakReference)this.b.get(i1)).get();
          if (localMediaRouter != null) {
            break;
          }
          this.b.remove(i1);
        }
      } while (localMediaRouter.b != paramContext);
      return localMediaRouter;
      label60:
      paramContext = new MediaRouter(paramContext);
      this.b.add(new WeakReference(paramContext));
      return paramContext;
    }
    
    public void a()
    {
      this.l = new RegisteredMediaRouteProviderWatcher(this.a, this);
      this.l.a();
    }
    
    public void a(MediaRouteProvider paramMediaRouteProvider)
    {
      if (c(paramMediaRouteProvider) < 0)
      {
        MediaRouter.ProviderInfo localProviderInfo = new MediaRouter.ProviderInfo(paramMediaRouteProvider);
        this.d.add(localProviderInfo);
        if (MediaRouter.f()) {
          Log.d("MediaRouter", "Provider added: " + localProviderInfo);
        }
        this.h.a(513, localProviderInfo);
        a(localProviderInfo, paramMediaRouteProvider.e());
        paramMediaRouteProvider.a(this.g);
        paramMediaRouteProvider.a(this.p);
      }
    }
    
    public void a(MediaRouter.RouteInfo paramRouteInfo)
    {
      c(paramRouteInfo, 3);
    }
    
    public void a(MediaRouter.RouteInfo paramRouteInfo, int paramInt)
    {
      if ((paramRouteInfo == this.n) && (this.o != null)) {
        this.o.b(paramInt);
      }
    }
    
    public boolean a(MediaRouteSelector paramMediaRouteSelector, int paramInt)
    {
      if (paramMediaRouteSelector.b()) {
        return false;
      }
      if (((paramInt & 0x2) == 0) && (this.k)) {
        return true;
      }
      int i2 = this.c.size();
      int i1 = 0;
      label35:
      MediaRouter.RouteInfo localRouteInfo;
      if (i1 < i2)
      {
        localRouteInfo = (MediaRouter.RouteInfo)this.c.get(i1);
        if (((paramInt & 0x1) == 0) || (!localRouteInfo.f())) {
          break label75;
        }
      }
      label75:
      while (!localRouteInfo.a(paramMediaRouteSelector))
      {
        i1 += 1;
        break label35;
        break;
      }
      return true;
    }
    
    public List<MediaRouter.RouteInfo> b()
    {
      return this.c;
    }
    
    public void b(MediaRouteProvider paramMediaRouteProvider)
    {
      int i1 = c(paramMediaRouteProvider);
      if (i1 >= 0)
      {
        paramMediaRouteProvider.a(null);
        paramMediaRouteProvider.a(null);
        paramMediaRouteProvider = (MediaRouter.ProviderInfo)this.d.get(i1);
        a(paramMediaRouteProvider, null);
        if (MediaRouter.f()) {
          Log.d("MediaRouter", "Provider removed: " + paramMediaRouteProvider);
        }
        this.h.a(514, paramMediaRouteProvider);
        this.d.remove(i1);
      }
    }
    
    public void b(MediaRouter.RouteInfo paramRouteInfo, int paramInt)
    {
      if ((paramRouteInfo == this.n) && (this.o != null)) {
        this.o.c(paramInt);
      }
    }
    
    public MediaRouter.RouteInfo c()
    {
      if (this.m == null) {
        throw new IllegalStateException("There is no default route.  The media router has not yet been fully initialized.");
      }
      return this.m;
    }
    
    public void c(MediaRouter.RouteInfo paramRouteInfo, int paramInt)
    {
      if (!this.c.contains(paramRouteInfo))
      {
        Log.w("MediaRouter", "Ignoring attempt to select removed route: " + paramRouteInfo);
        return;
      }
      if (!MediaRouter.RouteInfo.b(paramRouteInfo))
      {
        Log.w("MediaRouter", "Ignoring attempt to select disabled route: " + paramRouteInfo);
        return;
      }
      d(paramRouteInfo, paramInt);
    }
    
    public MediaRouter.RouteInfo d()
    {
      if (this.n == null) {
        throw new IllegalStateException("There is no currently selected route.  The media router has not yet been fully initialized.");
      }
      return this.n;
    }
    
    public void e()
    {
      int i2 = 0;
      boolean bool1 = false;
      Object localObject = new MediaRouteSelector.Builder();
      int i5;
      MediaRouter localMediaRouter;
      for (int i4 = this.b.size();; i4 = i5)
      {
        i5 = i4 - 1;
        if (i5 < 0) {
          break label195;
        }
        localMediaRouter = (MediaRouter)((WeakReference)this.b.get(i5)).get();
        if (localMediaRouter != null) {
          break;
        }
        this.b.remove(i5);
      }
      int i6 = localMediaRouter.c.size();
      int i3 = 0;
      int i1 = i2;
      boolean bool2 = bool1;
      for (;;)
      {
        bool1 = bool2;
        i2 = i1;
        i4 = i5;
        if (i3 >= i6) {
          break;
        }
        MediaRouter.CallbackRecord localCallbackRecord = (MediaRouter.CallbackRecord)localMediaRouter.c.get(i3);
        ((MediaRouteSelector.Builder)localObject).a(localCallbackRecord.c);
        if ((localCallbackRecord.d & 0x1) != 0)
        {
          bool2 = true;
          i1 = 1;
        }
        i2 = i1;
        if ((localCallbackRecord.d & 0x4) != 0)
        {
          i2 = i1;
          if (!this.k) {
            i2 = 1;
          }
        }
        i1 = i2;
        if ((localCallbackRecord.d & 0x8) != 0) {
          i1 = 1;
        }
        i3 += 1;
      }
      label195:
      if (i2 != 0)
      {
        localObject = ((MediaRouteSelector.Builder)localObject).a();
        if ((this.p == null) || (!this.p.a().equals(localObject)) || (this.p.b() != bool1)) {
          break label249;
        }
      }
      label249:
      do
      {
        return;
        localObject = MediaRouteSelector.a;
        break;
        if ((!((MediaRouteSelector)localObject).b()) || (bool1)) {
          break label377;
        }
      } while (this.p == null);
      label377:
      for (this.p = null;; this.p = new MediaRouteDiscoveryRequest((MediaRouteSelector)localObject, bool1))
      {
        if (MediaRouter.f()) {
          Log.d("MediaRouter", "Updated discovery request: " + this.p);
        }
        if ((i2 != 0) && (!bool1) && (this.k)) {
          Log.i("MediaRouter", "Forcing passive route discovery on a low-RAM device, system performance may be affected.  Please consider using CALLBACK_FLAG_REQUEST_DISCOVERY instead of CALLBACK_FLAG_FORCE_DISCOVERY.");
        }
        i2 = this.d.size();
        i1 = 0;
        while (i1 < i2)
        {
          MediaRouter.ProviderInfo.a((MediaRouter.ProviderInfo)this.d.get(i1)).a(this.p);
          i1 += 1;
        }
        break;
      }
    }
    
    public MediaSessionCompat.Token f()
    {
      if (this.q != null) {
        return this.q.b();
      }
      if (this.r != null) {
        return this.r.a();
      }
      return null;
    }
    
    private final class CallbackHandler
      extends Handler
    {
      private final ArrayList<MediaRouter.CallbackRecord> b = new ArrayList();
      
      private CallbackHandler() {}
      
      private void a(MediaRouter.CallbackRecord paramCallbackRecord, int paramInt, Object paramObject)
      {
        MediaRouter localMediaRouter = paramCallbackRecord.a;
        MediaRouter.Callback localCallback = paramCallbackRecord.b;
        switch (0xFF00 & paramInt)
        {
        default: 
        case 256: 
          do
          {
            return;
            paramObject = (MediaRouter.RouteInfo)paramObject;
          } while (!paramCallbackRecord.a((MediaRouter.RouteInfo)paramObject));
          switch (paramInt)
          {
          default: 
            return;
          case 257: 
            localCallback.a(localMediaRouter, (MediaRouter.RouteInfo)paramObject);
            return;
          case 258: 
            localCallback.b(localMediaRouter, (MediaRouter.RouteInfo)paramObject);
            return;
          case 259: 
            localCallback.c(localMediaRouter, (MediaRouter.RouteInfo)paramObject);
            return;
          case 260: 
            localCallback.f(localMediaRouter, (MediaRouter.RouteInfo)paramObject);
            return;
          case 261: 
            localCallback.g(localMediaRouter, (MediaRouter.RouteInfo)paramObject);
            return;
          case 262: 
            localCallback.d(localMediaRouter, (MediaRouter.RouteInfo)paramObject);
            return;
          }
          localCallback.e(localMediaRouter, (MediaRouter.RouteInfo)paramObject);
          return;
        }
        paramCallbackRecord = (MediaRouter.ProviderInfo)paramObject;
        switch (paramInt)
        {
        default: 
          return;
        case 513: 
          localCallback.a(localMediaRouter, paramCallbackRecord);
          return;
        case 514: 
          localCallback.b(localMediaRouter, paramCallbackRecord);
          return;
        }
        localCallback.c(localMediaRouter, paramCallbackRecord);
      }
      
      private void b(int paramInt, Object paramObject)
      {
        switch (paramInt)
        {
        case 260: 
        case 261: 
        default: 
          return;
        case 257: 
          MediaRouter.GlobalMediaRouter.e(MediaRouter.GlobalMediaRouter.this).a((MediaRouter.RouteInfo)paramObject);
          return;
        case 258: 
          MediaRouter.GlobalMediaRouter.e(MediaRouter.GlobalMediaRouter.this).b((MediaRouter.RouteInfo)paramObject);
          return;
        case 259: 
          MediaRouter.GlobalMediaRouter.e(MediaRouter.GlobalMediaRouter.this).c((MediaRouter.RouteInfo)paramObject);
          return;
        }
        MediaRouter.GlobalMediaRouter.e(MediaRouter.GlobalMediaRouter.this).d((MediaRouter.RouteInfo)paramObject);
      }
      
      public void a(int paramInt, Object paramObject)
      {
        obtainMessage(paramInt, paramObject).sendToTarget();
      }
      
      public void handleMessage(Message paramMessage)
      {
        int j = paramMessage.what;
        paramMessage = paramMessage.obj;
        b(j, paramMessage);
        for (;;)
        {
          try
          {
            i = MediaRouter.GlobalMediaRouter.d(MediaRouter.GlobalMediaRouter.this).size();
            i -= 1;
            if (i < 0) {
              break;
            }
            MediaRouter localMediaRouter = (MediaRouter)((WeakReference)MediaRouter.GlobalMediaRouter.d(MediaRouter.GlobalMediaRouter.this).get(i)).get();
            if (localMediaRouter == null) {
              MediaRouter.GlobalMediaRouter.d(MediaRouter.GlobalMediaRouter.this).remove(i);
            } else {
              this.b.addAll(localMediaRouter.c);
            }
          }
          finally
          {
            this.b.clear();
          }
        }
        int k = this.b.size();
        int i = 0;
        while (i < k)
        {
          a((MediaRouter.CallbackRecord)this.b.get(i), j, paramMessage);
          i += 1;
        }
        this.b.clear();
      }
    }
    
    private final class MediaSessionRecord
    {
      private final MediaSessionCompat b;
      private int c;
      private int d;
      private VolumeProviderCompat e;
      
      public void a()
      {
        this.b.a(MediaRouter.GlobalMediaRouter.c(this.a).d);
        this.e = null;
      }
      
      public void a(int paramInt1, int paramInt2, int paramInt3)
      {
        if ((this.e != null) && (paramInt1 == this.c) && (paramInt2 == this.d))
        {
          this.e.a(paramInt3);
          return;
        }
        this.e = new VolumeProviderCompat(paramInt1, paramInt2, paramInt3)
        {
          public void b(final int paramAnonymousInt)
          {
            MediaRouter.GlobalMediaRouter.b(MediaRouter.GlobalMediaRouter.MediaSessionRecord.this.a).post(new Runnable()
            {
              public void run()
              {
                if (MediaRouter.GlobalMediaRouter.a(MediaRouter.GlobalMediaRouter.MediaSessionRecord.this.a) != null) {
                  MediaRouter.GlobalMediaRouter.a(MediaRouter.GlobalMediaRouter.MediaSessionRecord.this.a).a(paramAnonymousInt);
                }
              }
            });
          }
          
          public void c(final int paramAnonymousInt)
          {
            MediaRouter.GlobalMediaRouter.b(MediaRouter.GlobalMediaRouter.MediaSessionRecord.this.a).post(new Runnable()
            {
              public void run()
              {
                if (MediaRouter.GlobalMediaRouter.a(MediaRouter.GlobalMediaRouter.MediaSessionRecord.this.a) != null) {
                  MediaRouter.GlobalMediaRouter.a(MediaRouter.GlobalMediaRouter.MediaSessionRecord.this.a).b(paramAnonymousInt);
                }
              }
            });
          }
        };
        this.b.a(this.e);
      }
      
      public MediaSessionCompat.Token b()
      {
        return this.b.a();
      }
    }
    
    private final class ProviderCallback
      extends MediaRouteProvider.Callback
    {
      private ProviderCallback() {}
      
      public void a(MediaRouteProvider paramMediaRouteProvider, MediaRouteProviderDescriptor paramMediaRouteProviderDescriptor)
      {
        MediaRouter.GlobalMediaRouter.a(MediaRouter.GlobalMediaRouter.this, paramMediaRouteProvider, paramMediaRouteProviderDescriptor);
      }
    }
    
    private final class RemoteControlClientRecord
      implements RemoteControlClientCompat.VolumeCallback
    {
      private final RemoteControlClientCompat b;
      private boolean c;
      
      public void a()
      {
        this.b.a(MediaRouter.GlobalMediaRouter.c(this.a));
      }
      
      public void a(int paramInt)
      {
        if ((!this.c) && (MediaRouter.GlobalMediaRouter.a(this.a) != null)) {
          MediaRouter.GlobalMediaRouter.a(this.a).a(paramInt);
        }
      }
      
      public void b(int paramInt)
      {
        if ((!this.c) && (MediaRouter.GlobalMediaRouter.a(this.a) != null)) {
          MediaRouter.GlobalMediaRouter.a(this.a).b(paramInt);
        }
      }
    }
  }
  
  public static final class ProviderInfo
  {
    private final MediaRouteProvider a;
    private final ArrayList<MediaRouter.RouteInfo> b = new ArrayList();
    private final MediaRouteProvider.ProviderMetadata c;
    private MediaRouteProviderDescriptor d;
    
    ProviderInfo(MediaRouteProvider paramMediaRouteProvider)
    {
      this.a = paramMediaRouteProvider;
      this.c = paramMediaRouteProvider.c();
    }
    
    int a(String paramString)
    {
      int j = this.b.size();
      int i = 0;
      while (i < j)
      {
        if (MediaRouter.RouteInfo.a((MediaRouter.RouteInfo)this.b.get(i)).equals(paramString)) {
          return i;
        }
        i += 1;
      }
      return -1;
    }
    
    public MediaRouteProvider a()
    {
      MediaRouter.e();
      return this.a;
    }
    
    boolean a(MediaRouteProviderDescriptor paramMediaRouteProviderDescriptor)
    {
      if (this.d != paramMediaRouteProviderDescriptor)
      {
        this.d = paramMediaRouteProviderDescriptor;
        return true;
      }
      return false;
    }
    
    public String b()
    {
      return this.c.a();
    }
    
    public ComponentName c()
    {
      return this.c.b();
    }
    
    public String toString()
    {
      return "MediaRouter.RouteProviderInfo{ packageName=" + b() + " }";
    }
  }
  
  public static final class RouteInfo
  {
    private final MediaRouter.ProviderInfo a;
    private final String b;
    private final String c;
    private String d;
    private String e;
    private boolean f;
    private boolean g;
    private boolean h;
    private final ArrayList<IntentFilter> i = new ArrayList();
    private int j;
    private int k;
    private int l;
    private int m;
    private int n;
    private Display o;
    private int p = -1;
    private Bundle q;
    private IntentSender r;
    private MediaRouteDescriptor s;
    
    RouteInfo(MediaRouter.ProviderInfo paramProviderInfo, String paramString1, String paramString2)
    {
      this.a = paramProviderInfo;
      this.b = paramString1;
      this.c = paramString2;
    }
    
    int a(MediaRouteDescriptor paramMediaRouteDescriptor)
    {
      int i3 = 0;
      int i2 = 0;
      int i1 = i3;
      if (this.s != paramMediaRouteDescriptor)
      {
        this.s = paramMediaRouteDescriptor;
        i1 = i3;
        if (paramMediaRouteDescriptor != null)
        {
          if (!MediaRouter.a(this.d, paramMediaRouteDescriptor.b()))
          {
            this.d = paramMediaRouteDescriptor.b();
            i2 = 0x0 | 0x1;
          }
          i1 = i2;
          if (!MediaRouter.a(this.e, paramMediaRouteDescriptor.c()))
          {
            this.e = paramMediaRouteDescriptor.c();
            i1 = i2 | 0x1;
          }
          i2 = i1;
          if (this.f != paramMediaRouteDescriptor.d())
          {
            this.f = paramMediaRouteDescriptor.d();
            i2 = i1 | 0x1;
          }
          i1 = i2;
          if (this.g != paramMediaRouteDescriptor.e())
          {
            this.g = paramMediaRouteDescriptor.e();
            i1 = i2 | 0x1;
          }
          i2 = i1;
          if (!this.i.equals(paramMediaRouteDescriptor.h()))
          {
            this.i.clear();
            this.i.addAll(paramMediaRouteDescriptor.h());
            i2 = i1 | 0x1;
          }
          i1 = i2;
          if (this.j != paramMediaRouteDescriptor.i())
          {
            this.j = paramMediaRouteDescriptor.i();
            i1 = i2 | 0x1;
          }
          i2 = i1;
          if (this.k != paramMediaRouteDescriptor.j())
          {
            this.k = paramMediaRouteDescriptor.j();
            i2 = i1 | 0x1;
          }
          i1 = i2;
          if (this.l != paramMediaRouteDescriptor.m())
          {
            this.l = paramMediaRouteDescriptor.m();
            i1 = i2 | 0x3;
          }
          i2 = i1;
          if (this.m != paramMediaRouteDescriptor.k())
          {
            this.m = paramMediaRouteDescriptor.k();
            i2 = i1 | 0x3;
          }
          i1 = i2;
          if (this.n != paramMediaRouteDescriptor.l())
          {
            this.n = paramMediaRouteDescriptor.l();
            i1 = i2 | 0x3;
          }
          i2 = i1;
          if (this.p != paramMediaRouteDescriptor.n())
          {
            this.p = paramMediaRouteDescriptor.n();
            this.o = null;
            i2 = i1 | 0x5;
          }
          i1 = i2;
          if (!MediaRouter.a(this.q, paramMediaRouteDescriptor.o()))
          {
            this.q = paramMediaRouteDescriptor.o();
            i1 = i2 | 0x1;
          }
          i2 = i1;
          if (!MediaRouter.a(this.r, paramMediaRouteDescriptor.g()))
          {
            this.r = paramMediaRouteDescriptor.g();
            i2 = i1 | 0x1;
          }
          i1 = i2;
          if (this.h != paramMediaRouteDescriptor.f())
          {
            this.h = paramMediaRouteDescriptor.f();
            i1 = i2 | 0x5;
          }
        }
      }
      return i1;
    }
    
    public String a()
    {
      return this.d;
    }
    
    public void a(int paramInt)
    {
      MediaRouter.e();
      MediaRouter.a.a(this, Math.min(this.n, Math.max(0, paramInt)));
    }
    
    public boolean a(@NonNull MediaRouteSelector paramMediaRouteSelector)
    {
      if (paramMediaRouteSelector == null) {
        throw new IllegalArgumentException("selector must not be null");
      }
      MediaRouter.e();
      return paramMediaRouteSelector.a(this.i);
    }
    
    public boolean a(@NonNull String paramString)
    {
      if (paramString == null) {
        throw new IllegalArgumentException("category must not be null");
      }
      MediaRouter.e();
      int i2 = this.i.size();
      int i1 = 0;
      while (i1 < i2)
      {
        if (((IntentFilter)this.i.get(i1)).hasCategory(paramString)) {
          return true;
        }
        i1 += 1;
      }
      return false;
    }
    
    @Nullable
    public String b()
    {
      return this.e;
    }
    
    public void b(int paramInt)
    {
      
      if (paramInt != 0) {
        MediaRouter.a.b(this, paramInt);
      }
    }
    
    public boolean c()
    {
      return this.f;
    }
    
    public boolean d()
    {
      return this.g;
    }
    
    public boolean e()
    {
      MediaRouter.e();
      return MediaRouter.a.d() == this;
    }
    
    public boolean f()
    {
      MediaRouter.e();
      return MediaRouter.a.c() == this;
    }
    
    public int g()
    {
      return this.j;
    }
    
    public int h()
    {
      return this.k;
    }
    
    public int i()
    {
      return this.l;
    }
    
    public int j()
    {
      return this.m;
    }
    
    public int k()
    {
      return this.n;
    }
    
    public boolean l()
    {
      return this.h;
    }
    
    @Nullable
    public Bundle m()
    {
      return this.q;
    }
    
    @Nullable
    public IntentSender n()
    {
      return this.r;
    }
    
    public void o()
    {
      MediaRouter.e();
      MediaRouter.a.a(this);
    }
    
    String p()
    {
      return this.b;
    }
    
    MediaRouteProvider q()
    {
      return this.a.a();
    }
    
    public String toString()
    {
      return "MediaRouter.RouteInfo{ uniqueId=" + this.c + ", name=" + this.d + ", description=" + this.e + ", enabled=" + this.f + ", connecting=" + this.g + ", canDisconnect=" + this.h + ", playbackType=" + this.j + ", playbackStream=" + this.k + ", volumeHandling=" + this.l + ", volume=" + this.m + ", volumeMax=" + this.n + ", presentationDisplayId=" + this.p + ", extras=" + this.q + ", settingsIntent=" + this.r + ", providerPackageName=" + this.a.b() + " }";
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/media/MediaRouter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */