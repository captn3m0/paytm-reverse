package android.support.v7.media;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Handler;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

final class RegisteredMediaRouteProviderWatcher
{
  private final Context a;
  private final Callback b;
  private final Handler c;
  private final PackageManager d;
  private final ArrayList<RegisteredMediaRouteProvider> e = new ArrayList();
  private boolean f;
  private final BroadcastReceiver g = new BroadcastReceiver()
  {
    public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
    {
      RegisteredMediaRouteProviderWatcher.a(RegisteredMediaRouteProviderWatcher.this);
    }
  };
  private final Runnable h = new Runnable()
  {
    public void run()
    {
      RegisteredMediaRouteProviderWatcher.a(RegisteredMediaRouteProviderWatcher.this);
    }
  };
  
  public RegisteredMediaRouteProviderWatcher(Context paramContext, Callback paramCallback)
  {
    this.a = paramContext;
    this.b = paramCallback;
    this.c = new Handler();
    this.d = paramContext.getPackageManager();
  }
  
  private int a(String paramString1, String paramString2)
  {
    int j = this.e.size();
    int i = 0;
    while (i < j)
    {
      if (((RegisteredMediaRouteProvider)this.e.get(i)).a(paramString1, paramString2)) {
        return i;
      }
      i += 1;
    }
    return -1;
  }
  
  private void b()
  {
    if (!this.f) {}
    for (;;)
    {
      return;
      int i = 0;
      Object localObject1 = new Intent("android.media.MediaRouteProviderService");
      localObject1 = this.d.queryIntentServices((Intent)localObject1, 0).iterator();
      int j;
      while (((Iterator)localObject1).hasNext())
      {
        Object localObject2 = ((ResolveInfo)((Iterator)localObject1).next()).serviceInfo;
        if (localObject2 != null)
        {
          j = a(((ServiceInfo)localObject2).packageName, ((ServiceInfo)localObject2).name);
          if (j < 0)
          {
            localObject2 = new RegisteredMediaRouteProvider(this.a, new ComponentName(((ServiceInfo)localObject2).packageName, ((ServiceInfo)localObject2).name));
            ((RegisteredMediaRouteProvider)localObject2).f();
            this.e.add(i, localObject2);
            this.b.a((MediaRouteProvider)localObject2);
            i += 1;
          }
          else if (j >= i)
          {
            localObject2 = (RegisteredMediaRouteProvider)this.e.get(j);
            ((RegisteredMediaRouteProvider)localObject2).f();
            ((RegisteredMediaRouteProvider)localObject2).h();
            Collections.swap(this.e, j, i);
            i += 1;
          }
        }
      }
      if (i < this.e.size())
      {
        j = this.e.size() - 1;
        while (j >= i)
        {
          localObject1 = (RegisteredMediaRouteProvider)this.e.get(j);
          this.b.b((MediaRouteProvider)localObject1);
          this.e.remove(localObject1);
          ((RegisteredMediaRouteProvider)localObject1).g();
          j -= 1;
        }
      }
    }
  }
  
  public void a()
  {
    if (!this.f)
    {
      this.f = true;
      IntentFilter localIntentFilter = new IntentFilter();
      localIntentFilter.addAction("android.intent.action.PACKAGE_ADDED");
      localIntentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
      localIntentFilter.addAction("android.intent.action.PACKAGE_CHANGED");
      localIntentFilter.addAction("android.intent.action.PACKAGE_REPLACED");
      localIntentFilter.addAction("android.intent.action.PACKAGE_RESTARTED");
      localIntentFilter.addDataScheme("package");
      this.a.registerReceiver(this.g, localIntentFilter, null, this.c);
      this.c.post(this.h);
    }
  }
  
  public static abstract interface Callback
  {
    public abstract void a(MediaRouteProvider paramMediaRouteProvider);
    
    public abstract void b(MediaRouteProvider paramMediaRouteProvider);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/media/RegisteredMediaRouteProviderWatcher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */