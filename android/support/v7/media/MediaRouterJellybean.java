package android.support.v7.media;

import android.content.Context;
import android.media.MediaRouter;
import android.media.MediaRouter.Callback;
import android.media.MediaRouter.RouteCategory;
import android.media.MediaRouter.RouteGroup;
import android.media.MediaRouter.RouteInfo;
import android.media.MediaRouter.UserRouteInfo;
import android.media.MediaRouter.VolumeCallback;
import android.media.RemoteControlClient;
import android.os.Build.VERSION;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

final class MediaRouterJellybean
{
  public static Object a(Context paramContext)
  {
    return paramContext.getSystemService("media_router");
  }
  
  public static Object a(Callback paramCallback)
  {
    return new CallbackProxy(paramCallback);
  }
  
  public static Object a(VolumeCallback paramVolumeCallback)
  {
    return new VolumeCallbackProxy(paramVolumeCallback);
  }
  
  public static Object a(Object paramObject, int paramInt)
  {
    return ((MediaRouter)paramObject).getSelectedRoute(paramInt);
  }
  
  public static Object a(Object paramObject, String paramString, boolean paramBoolean)
  {
    return ((MediaRouter)paramObject).createRouteCategory(paramString, paramBoolean);
  }
  
  public static List a(Object paramObject)
  {
    paramObject = (MediaRouter)paramObject;
    int j = ((MediaRouter)paramObject).getRouteCount();
    ArrayList localArrayList = new ArrayList(j);
    int i = 0;
    while (i < j)
    {
      localArrayList.add(((MediaRouter)paramObject).getRouteAt(i));
      i += 1;
    }
    return localArrayList;
  }
  
  public static void a(Object paramObject1, int paramInt, Object paramObject2)
  {
    ((MediaRouter)paramObject1).selectRoute(paramInt, (MediaRouter.RouteInfo)paramObject2);
  }
  
  public static void a(Object paramObject1, Object paramObject2)
  {
    ((MediaRouter)paramObject1).removeCallback((MediaRouter.Callback)paramObject2);
  }
  
  public static Object b(Object paramObject1, Object paramObject2)
  {
    return ((MediaRouter)paramObject1).createUserRoute((MediaRouter.RouteCategory)paramObject2);
  }
  
  public static void b(Object paramObject1, int paramInt, Object paramObject2)
  {
    ((MediaRouter)paramObject1).addCallback(paramInt, (MediaRouter.Callback)paramObject2);
  }
  
  public static void c(Object paramObject1, Object paramObject2)
  {
    ((MediaRouter)paramObject1).addUserRoute((MediaRouter.UserRouteInfo)paramObject2);
  }
  
  public static void d(Object paramObject1, Object paramObject2)
  {
    ((MediaRouter)paramObject1).removeUserRoute((MediaRouter.UserRouteInfo)paramObject2);
  }
  
  public static abstract interface Callback
  {
    public abstract void a(int paramInt, Object paramObject);
    
    public abstract void a(Object paramObject);
    
    public abstract void a(Object paramObject1, Object paramObject2);
    
    public abstract void a(Object paramObject1, Object paramObject2, int paramInt);
    
    public abstract void b(int paramInt, Object paramObject);
    
    public abstract void b(Object paramObject);
    
    public abstract void c(Object paramObject);
    
    public abstract void d(Object paramObject);
  }
  
  static class CallbackProxy<T extends MediaRouterJellybean.Callback>
    extends MediaRouter.Callback
  {
    protected final T a;
    
    public CallbackProxy(T paramT)
    {
      this.a = paramT;
    }
    
    public void onRouteAdded(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      this.a.a(paramRouteInfo);
    }
    
    public void onRouteChanged(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      this.a.c(paramRouteInfo);
    }
    
    public void onRouteGrouped(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo, MediaRouter.RouteGroup paramRouteGroup, int paramInt)
    {
      this.a.a(paramRouteInfo, paramRouteGroup, paramInt);
    }
    
    public void onRouteRemoved(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      this.a.b(paramRouteInfo);
    }
    
    public void onRouteSelected(MediaRouter paramMediaRouter, int paramInt, MediaRouter.RouteInfo paramRouteInfo)
    {
      this.a.a(paramInt, paramRouteInfo);
    }
    
    public void onRouteUngrouped(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo, MediaRouter.RouteGroup paramRouteGroup)
    {
      this.a.a(paramRouteInfo, paramRouteGroup);
    }
    
    public void onRouteUnselected(MediaRouter paramMediaRouter, int paramInt, MediaRouter.RouteInfo paramRouteInfo)
    {
      this.a.b(paramInt, paramRouteInfo);
    }
    
    public void onRouteVolumeChanged(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      this.a.d(paramRouteInfo);
    }
  }
  
  public static final class GetDefaultRouteWorkaround
  {
    private Method a;
    
    public GetDefaultRouteWorkaround()
    {
      if ((Build.VERSION.SDK_INT < 16) || (Build.VERSION.SDK_INT > 17)) {
        throw new UnsupportedOperationException();
      }
      try
      {
        this.a = MediaRouter.class.getMethod("getSystemAudioRoute", new Class[0]);
        return;
      }
      catch (NoSuchMethodException localNoSuchMethodException) {}
    }
    
    public Object a(Object paramObject)
    {
      paramObject = (MediaRouter)paramObject;
      if (this.a != null) {}
      try
      {
        Object localObject = this.a.invoke(paramObject, new Object[0]);
        return localObject;
      }
      catch (InvocationTargetException localInvocationTargetException)
      {
        return ((MediaRouter)paramObject).getRouteAt(0);
      }
      catch (IllegalAccessException localIllegalAccessException)
      {
        for (;;) {}
      }
    }
  }
  
  public static final class RouteCategory {}
  
  public static final class RouteGroup {}
  
  public static final class RouteInfo
  {
    public static int a(Object paramObject)
    {
      return ((MediaRouter.RouteInfo)paramObject).getSupportedTypes();
    }
    
    public static CharSequence a(Object paramObject, Context paramContext)
    {
      return ((MediaRouter.RouteInfo)paramObject).getName(paramContext);
    }
    
    public static void a(Object paramObject, int paramInt)
    {
      ((MediaRouter.RouteInfo)paramObject).requestSetVolume(paramInt);
    }
    
    public static void a(Object paramObject1, Object paramObject2)
    {
      ((MediaRouter.RouteInfo)paramObject1).setTag(paramObject2);
    }
    
    public static int b(Object paramObject)
    {
      return ((MediaRouter.RouteInfo)paramObject).getPlaybackType();
    }
    
    public static void b(Object paramObject, int paramInt)
    {
      ((MediaRouter.RouteInfo)paramObject).requestUpdateVolume(paramInt);
    }
    
    public static int c(Object paramObject)
    {
      return ((MediaRouter.RouteInfo)paramObject).getPlaybackStream();
    }
    
    public static int d(Object paramObject)
    {
      return ((MediaRouter.RouteInfo)paramObject).getVolume();
    }
    
    public static int e(Object paramObject)
    {
      return ((MediaRouter.RouteInfo)paramObject).getVolumeMax();
    }
    
    public static int f(Object paramObject)
    {
      return ((MediaRouter.RouteInfo)paramObject).getVolumeHandling();
    }
    
    public static Object g(Object paramObject)
    {
      return ((MediaRouter.RouteInfo)paramObject).getTag();
    }
  }
  
  public static final class SelectRouteWorkaround
  {
    private Method a;
    
    public SelectRouteWorkaround()
    {
      if ((Build.VERSION.SDK_INT < 16) || (Build.VERSION.SDK_INT > 17)) {
        throw new UnsupportedOperationException();
      }
      try
      {
        this.a = MediaRouter.class.getMethod("selectRouteInt", new Class[] { Integer.TYPE, MediaRouter.RouteInfo.class });
        return;
      }
      catch (NoSuchMethodException localNoSuchMethodException) {}
    }
    
    public void a(Object paramObject1, int paramInt, Object paramObject2)
    {
      paramObject1 = (MediaRouter)paramObject1;
      paramObject2 = (MediaRouter.RouteInfo)paramObject2;
      if (((0x800000 & ((MediaRouter.RouteInfo)paramObject2).getSupportedTypes()) != 0) || (this.a != null)) {}
      for (;;)
      {
        try
        {
          this.a.invoke(paramObject1, new Object[] { Integer.valueOf(paramInt), paramObject2 });
          return;
        }
        catch (IllegalAccessException localIllegalAccessException)
        {
          Log.w("MediaRouterJellybean", "Cannot programmatically select non-user route.  Media routing may not work.", localIllegalAccessException);
          ((MediaRouter)paramObject1).selectRoute(paramInt, (MediaRouter.RouteInfo)paramObject2);
          return;
        }
        catch (InvocationTargetException localInvocationTargetException)
        {
          Log.w("MediaRouterJellybean", "Cannot programmatically select non-user route.  Media routing may not work.", localInvocationTargetException);
          continue;
        }
        Log.w("MediaRouterJellybean", "Cannot programmatically select non-user route because the platform is missing the selectRouteInt() method.  Media routing may not work.");
      }
    }
  }
  
  public static final class UserRouteInfo
  {
    public static void a(Object paramObject, int paramInt)
    {
      ((MediaRouter.UserRouteInfo)paramObject).setPlaybackType(paramInt);
    }
    
    public static void a(Object paramObject, CharSequence paramCharSequence)
    {
      ((MediaRouter.UserRouteInfo)paramObject).setName(paramCharSequence);
    }
    
    public static void a(Object paramObject1, Object paramObject2)
    {
      ((MediaRouter.UserRouteInfo)paramObject1).setVolumeCallback((MediaRouter.VolumeCallback)paramObject2);
    }
    
    public static void b(Object paramObject, int paramInt)
    {
      ((MediaRouter.UserRouteInfo)paramObject).setPlaybackStream(paramInt);
    }
    
    public static void b(Object paramObject1, Object paramObject2)
    {
      ((MediaRouter.UserRouteInfo)paramObject1).setRemoteControlClient((RemoteControlClient)paramObject2);
    }
    
    public static void c(Object paramObject, int paramInt)
    {
      ((MediaRouter.UserRouteInfo)paramObject).setVolume(paramInt);
    }
    
    public static void d(Object paramObject, int paramInt)
    {
      ((MediaRouter.UserRouteInfo)paramObject).setVolumeMax(paramInt);
    }
    
    public static void e(Object paramObject, int paramInt)
    {
      ((MediaRouter.UserRouteInfo)paramObject).setVolumeHandling(paramInt);
    }
  }
  
  public static abstract interface VolumeCallback
  {
    public abstract void a(Object paramObject, int paramInt);
    
    public abstract void b(Object paramObject, int paramInt);
  }
  
  static class VolumeCallbackProxy<T extends MediaRouterJellybean.VolumeCallback>
    extends MediaRouter.VolumeCallback
  {
    protected final T a;
    
    public VolumeCallbackProxy(T paramT)
    {
      this.a = paramT;
    }
    
    public void onVolumeSetRequest(MediaRouter.RouteInfo paramRouteInfo, int paramInt)
    {
      this.a.a(paramRouteInfo, paramInt);
    }
    
    public void onVolumeUpdateRequest(MediaRouter.RouteInfo paramRouteInfo, int paramInt)
    {
      this.a.b(paramRouteInfo, paramInt);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/media/MediaRouterJellybean.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */