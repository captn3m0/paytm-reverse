package android.support.v7.media;

import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class MediaRouteSelector
{
  public static final MediaRouteSelector a = new MediaRouteSelector(new Bundle(), null);
  private final Bundle b;
  private List<String> c;
  
  private MediaRouteSelector(Bundle paramBundle, List<String> paramList)
  {
    this.b = paramBundle;
    this.c = paramList;
  }
  
  public static MediaRouteSelector a(@Nullable Bundle paramBundle)
  {
    if (paramBundle != null) {
      return new MediaRouteSelector(paramBundle, null);
    }
    return null;
  }
  
  private void e()
  {
    if (this.c == null)
    {
      this.c = this.b.getStringArrayList("controlCategories");
      if ((this.c == null) || (this.c.isEmpty())) {
        this.c = Collections.emptyList();
      }
    }
  }
  
  public List<String> a()
  {
    e();
    return this.c;
  }
  
  public boolean a(MediaRouteSelector paramMediaRouteSelector)
  {
    if (paramMediaRouteSelector != null)
    {
      e();
      paramMediaRouteSelector.e();
      return this.c.containsAll(paramMediaRouteSelector.c);
    }
    return false;
  }
  
  public boolean a(List<IntentFilter> paramList)
  {
    if (paramList != null)
    {
      e();
      int k = this.c.size();
      if (k != 0)
      {
        int m = paramList.size();
        int i = 0;
        while (i < m)
        {
          IntentFilter localIntentFilter = (IntentFilter)paramList.get(i);
          if (localIntentFilter != null)
          {
            int j = 0;
            while (j < k)
            {
              if (localIntentFilter.hasCategory((String)this.c.get(j))) {
                return true;
              }
              j += 1;
            }
          }
          i += 1;
        }
      }
    }
    return false;
  }
  
  public boolean b()
  {
    e();
    return this.c.isEmpty();
  }
  
  public boolean c()
  {
    e();
    return !this.c.contains(null);
  }
  
  public Bundle d()
  {
    return this.b;
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject instanceof MediaRouteSelector))
    {
      paramObject = (MediaRouteSelector)paramObject;
      e();
      ((MediaRouteSelector)paramObject).e();
      return this.c.equals(((MediaRouteSelector)paramObject).c);
    }
    return false;
  }
  
  public int hashCode()
  {
    e();
    return this.c.hashCode();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("MediaRouteSelector{ ");
    localStringBuilder.append("controlCategories=").append(Arrays.toString(a().toArray()));
    localStringBuilder.append(" }");
    return localStringBuilder.toString();
  }
  
  public static final class Builder
  {
    private ArrayList<String> a;
    
    public Builder() {}
    
    public Builder(@NonNull MediaRouteSelector paramMediaRouteSelector)
    {
      if (paramMediaRouteSelector == null) {
        throw new IllegalArgumentException("selector must not be null");
      }
      MediaRouteSelector.b(paramMediaRouteSelector);
      if (!MediaRouteSelector.c(paramMediaRouteSelector).isEmpty()) {
        this.a = new ArrayList(MediaRouteSelector.c(paramMediaRouteSelector));
      }
    }
    
    @NonNull
    public Builder a(@NonNull MediaRouteSelector paramMediaRouteSelector)
    {
      if (paramMediaRouteSelector == null) {
        throw new IllegalArgumentException("selector must not be null");
      }
      a(paramMediaRouteSelector.a());
      return this;
    }
    
    @NonNull
    public Builder a(@NonNull String paramString)
    {
      if (paramString == null) {
        throw new IllegalArgumentException("category must not be null");
      }
      if (this.a == null) {
        this.a = new ArrayList();
      }
      if (!this.a.contains(paramString)) {
        this.a.add(paramString);
      }
      return this;
    }
    
    @NonNull
    public Builder a(@NonNull Collection<String> paramCollection)
    {
      if (paramCollection == null) {
        throw new IllegalArgumentException("categories must not be null");
      }
      if (!paramCollection.isEmpty())
      {
        paramCollection = paramCollection.iterator();
        while (paramCollection.hasNext()) {
          a((String)paramCollection.next());
        }
      }
      return this;
    }
    
    @NonNull
    public MediaRouteSelector a()
    {
      if (this.a == null) {
        return MediaRouteSelector.a;
      }
      Bundle localBundle = new Bundle();
      localBundle.putStringArrayList("controlCategories", this.a);
      return new MediaRouteSelector(localBundle, this.a, null);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/media/MediaRouteSelector.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */