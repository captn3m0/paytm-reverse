package android.support.v7.media;

import android.os.Bundle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class MediaRouteProviderDescriptor
{
  private final Bundle a;
  private List<MediaRouteDescriptor> b;
  
  private MediaRouteProviderDescriptor(Bundle paramBundle, List<MediaRouteDescriptor> paramList)
  {
    this.a = paramBundle;
    this.b = paramList;
  }
  
  public static MediaRouteProviderDescriptor a(Bundle paramBundle)
  {
    if (paramBundle != null) {
      return new MediaRouteProviderDescriptor(paramBundle, null);
    }
    return null;
  }
  
  private void d()
  {
    ArrayList localArrayList;
    if (this.b == null)
    {
      localArrayList = this.a.getParcelableArrayList("routes");
      if ((localArrayList != null) && (!localArrayList.isEmpty())) {
        break label36;
      }
      this.b = Collections.emptyList();
    }
    for (;;)
    {
      return;
      label36:
      int j = localArrayList.size();
      this.b = new ArrayList(j);
      int i = 0;
      while (i < j)
      {
        this.b.add(MediaRouteDescriptor.a((Bundle)localArrayList.get(i)));
        i += 1;
      }
    }
  }
  
  public List<MediaRouteDescriptor> a()
  {
    d();
    return this.b;
  }
  
  public boolean b()
  {
    d();
    int j = this.b.size();
    int i = 0;
    while (i < j)
    {
      MediaRouteDescriptor localMediaRouteDescriptor = (MediaRouteDescriptor)this.b.get(i);
      if ((localMediaRouteDescriptor == null) || (!localMediaRouteDescriptor.p())) {
        return false;
      }
      i += 1;
    }
    return true;
  }
  
  public Bundle c()
  {
    return this.a;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("MediaRouteProviderDescriptor{ ");
    localStringBuilder.append("routes=").append(Arrays.toString(a().toArray()));
    localStringBuilder.append(", isValid=").append(b());
    localStringBuilder.append(" }");
    return localStringBuilder.toString();
  }
  
  public static final class Builder
  {
    private final Bundle a = new Bundle();
    private ArrayList<MediaRouteDescriptor> b;
    
    public Builder a(MediaRouteDescriptor paramMediaRouteDescriptor)
    {
      if (paramMediaRouteDescriptor == null) {
        throw new IllegalArgumentException("route must not be null");
      }
      if (this.b == null) {
        this.b = new ArrayList();
      }
      while (!this.b.contains(paramMediaRouteDescriptor))
      {
        this.b.add(paramMediaRouteDescriptor);
        return this;
      }
      throw new IllegalArgumentException("route descriptor already added");
    }
    
    public MediaRouteProviderDescriptor a()
    {
      if (this.b != null)
      {
        int j = this.b.size();
        ArrayList localArrayList = new ArrayList(j);
        int i = 0;
        while (i < j)
        {
          localArrayList.add(((MediaRouteDescriptor)this.b.get(i)).q());
          i += 1;
        }
        this.a.putParcelableArrayList("routes", localArrayList);
      }
      return new MediaRouteProviderDescriptor(this.a, this.b, null);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/media/MediaRouteProviderDescriptor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */