package android.support.v7.media;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class RemotePlaybackClient
{
  private static final boolean a = Log.isLoggable("RemotePlaybackClient", 3);
  private String b;
  private StatusCallback c;
  
  private void a(Intent paramIntent, ActionCallback paramActionCallback, Bundle paramBundle)
  {
    Log.w("RemotePlaybackClient", "Received invalid result data from " + paramIntent.getAction() + ": data=" + b(paramBundle));
    paramActionCallback.a(null, 0, paramBundle);
  }
  
  private void a(Intent paramIntent, ActionCallback paramActionCallback, String paramString, Bundle paramBundle)
  {
    if (paramBundle != null) {}
    for (int i = paramBundle.getInt("android.media.intent.extra.ERROR_CODE", 0);; i = 0)
    {
      if (a) {
        Log.w("RemotePlaybackClient", "Received error from " + paramIntent.getAction() + ": error=" + paramString + ", code=" + i + ", data=" + b(paramBundle));
      }
      paramActionCallback.a(paramString, i, paramBundle);
      return;
    }
  }
  
  private static String b(Bundle paramBundle)
  {
    if (paramBundle != null)
    {
      paramBundle.size();
      return paramBundle.toString();
    }
    return "null";
  }
  
  private static String b(String paramString1, String paramString2)
  {
    if (paramString2 == null) {
      return paramString1;
    }
    if ((paramString1 == null) || (paramString1.equals(paramString2))) {
      return paramString2;
    }
    return null;
  }
  
  private void b(String paramString)
  {
    if (paramString != null) {
      a(paramString);
    }
  }
  
  public void a(String paramString)
  {
    if ((this.b != paramString) && ((this.b == null) || (!this.b.equals(paramString))))
    {
      if (a) {
        Log.d("RemotePlaybackClient", "Session id is now: " + paramString);
      }
      this.b = paramString;
      if (this.c != null) {
        this.c.a(paramString);
      }
    }
  }
  
  public static abstract class ActionCallback
  {
    public void a(String paramString, int paramInt, Bundle paramBundle) {}
  }
  
  public static abstract class ItemActionCallback
    extends RemotePlaybackClient.ActionCallback
  {
    public void a(Bundle paramBundle, String paramString1, MediaSessionStatus paramMediaSessionStatus, String paramString2, MediaItemStatus paramMediaItemStatus) {}
  }
  
  public static abstract class SessionActionCallback
    extends RemotePlaybackClient.ActionCallback
  {
    public void a(Bundle paramBundle, String paramString, MediaSessionStatus paramMediaSessionStatus) {}
  }
  
  public static abstract class StatusCallback
  {
    public void a(Bundle paramBundle, String paramString, MediaSessionStatus paramMediaSessionStatus) {}
    
    public void a(Bundle paramBundle, String paramString1, MediaSessionStatus paramMediaSessionStatus, String paramString2, MediaItemStatus paramMediaItemStatus) {}
    
    public void a(String paramString) {}
  }
  
  private final class StatusReceiver
    extends BroadcastReceiver
  {
    public void onReceive(Context paramContext, Intent paramIntent)
    {
      paramContext = paramIntent.getStringExtra("android.media.intent.extra.SESSION_ID");
      if ((paramContext == null) || (!paramContext.equals(RemotePlaybackClient.a(this.a)))) {
        Log.w("RemotePlaybackClient", "Discarding spurious status callback with missing or invalid session id: sessionId=" + paramContext);
      }
      MediaSessionStatus localMediaSessionStatus;
      do
      {
        String str;
        do
        {
          MediaItemStatus localMediaItemStatus;
          do
          {
            return;
            localMediaSessionStatus = MediaSessionStatus.a(paramIntent.getBundleExtra("android.media.intent.extra.SESSION_STATUS"));
            str = paramIntent.getAction();
            if (!str.equals("android.support.v7.media.actions.ACTION_ITEM_STATUS_CHANGED")) {
              break;
            }
            str = paramIntent.getStringExtra("android.media.intent.extra.ITEM_ID");
            if (str == null)
            {
              Log.w("RemotePlaybackClient", "Discarding spurious status callback with missing item id.");
              return;
            }
            localMediaItemStatus = MediaItemStatus.a(paramIntent.getBundleExtra("android.media.intent.extra.ITEM_STATUS"));
            if (localMediaItemStatus == null)
            {
              Log.w("RemotePlaybackClient", "Discarding spurious status callback with missing item status.");
              return;
            }
            if (RemotePlaybackClient.a()) {
              Log.d("RemotePlaybackClient", "Received item status callback: sessionId=" + paramContext + ", sessionStatus=" + localMediaSessionStatus + ", itemId=" + str + ", itemStatus=" + localMediaItemStatus);
            }
          } while (RemotePlaybackClient.b(this.a) == null);
          RemotePlaybackClient.b(this.a).a(paramIntent.getExtras(), paramContext, localMediaSessionStatus, str, localMediaItemStatus);
          return;
        } while (!str.equals("android.support.v7.media.actions.ACTION_SESSION_STATUS_CHANGED"));
        if (localMediaSessionStatus == null)
        {
          Log.w("RemotePlaybackClient", "Discarding spurious media status callback with missing session status.");
          return;
        }
        if (RemotePlaybackClient.a()) {
          Log.d("RemotePlaybackClient", "Received session status callback: sessionId=" + paramContext + ", sessionStatus=" + localMediaSessionStatus);
        }
      } while (RemotePlaybackClient.b(this.a) == null);
      RemotePlaybackClient.b(this.a).a(paramIntent.getExtras(), paramContext, localMediaSessionStatus);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/media/RemotePlaybackClient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */