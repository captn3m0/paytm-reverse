package android.support.v7.media;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.util.TimeUtils;

public final class MediaSessionStatus
{
  private final Bundle a;
  
  private MediaSessionStatus(Bundle paramBundle)
  {
    this.a = paramBundle;
  }
  
  public static MediaSessionStatus a(Bundle paramBundle)
  {
    if (paramBundle != null) {
      return new MediaSessionStatus(paramBundle);
    }
    return null;
  }
  
  private static String a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return Integer.toString(paramInt);
    case 0: 
      return "active";
    case 1: 
      return "ended";
    }
    return "invalidated";
  }
  
  public long a()
  {
    return this.a.getLong("timestamp");
  }
  
  public int b()
  {
    return this.a.getInt("sessionState", 2);
  }
  
  public boolean c()
  {
    return this.a.getBoolean("queuePaused");
  }
  
  public Bundle d()
  {
    return this.a.getBundle("extras");
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("MediaSessionStatus{ ");
    localStringBuilder.append("timestamp=");
    TimeUtils.a(SystemClock.elapsedRealtime() - a(), localStringBuilder);
    localStringBuilder.append(" ms ago");
    localStringBuilder.append(", sessionState=").append(a(b()));
    localStringBuilder.append(", queuePaused=").append(c());
    localStringBuilder.append(", extras=").append(d());
    localStringBuilder.append(" }");
    return localStringBuilder.toString();
  }
  
  public static final class Builder {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/media/MediaSessionStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */