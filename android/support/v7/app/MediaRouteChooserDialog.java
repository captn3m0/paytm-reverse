package android.support.v7.app;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.support.v7.media.MediaRouter.Callback;
import android.support.v7.media.MediaRouter.RouteInfo;
import android.support.v7.mediarouter.R.attr;
import android.support.v7.mediarouter.R.id;
import android.support.v7.mediarouter.R.layout;
import android.support.v7.mediarouter.R.string;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MediaRouteChooserDialog
  extends Dialog
{
  private final MediaRouter a = MediaRouter.a(getContext());
  private final MediaRouterCallback b = new MediaRouterCallback(null);
  private MediaRouteSelector c = MediaRouteSelector.a;
  private ArrayList<MediaRouter.RouteInfo> d;
  private RouteAdapter e;
  private ListView f;
  private boolean g;
  
  public MediaRouteChooserDialog(Context paramContext)
  {
    this(paramContext, 0);
  }
  
  public MediaRouteChooserDialog(Context paramContext, int paramInt)
  {
    super(MediaRouterThemeHelper.a(paramContext), paramInt);
  }
  
  public void a()
  {
    if (this.g)
    {
      this.d.clear();
      this.d.addAll(this.a.a());
      a(this.d);
      Collections.sort(this.d, RouteComparator.a);
      this.e.notifyDataSetChanged();
    }
  }
  
  public void a(@NonNull MediaRouteSelector paramMediaRouteSelector)
  {
    if (paramMediaRouteSelector == null) {
      throw new IllegalArgumentException("selector must not be null");
    }
    if (!this.c.equals(paramMediaRouteSelector))
    {
      this.c = paramMediaRouteSelector;
      if (this.g)
      {
        this.a.a(this.b);
        this.a.a(paramMediaRouteSelector, this.b, 1);
      }
      a();
    }
  }
  
  public void a(@NonNull List<MediaRouter.RouteInfo> paramList)
  {
    int i = paramList.size();
    for (;;)
    {
      int j = i - 1;
      if (i > 0)
      {
        if (!a((MediaRouter.RouteInfo)paramList.get(j)))
        {
          paramList.remove(j);
          i = j;
        }
      }
      else {
        return;
      }
      i = j;
    }
  }
  
  public boolean a(@NonNull MediaRouter.RouteInfo paramRouteInfo)
  {
    return (!paramRouteInfo.f()) && (paramRouteInfo.c()) && (paramRouteInfo.a(this.c));
  }
  
  public void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    this.g = true;
    this.a.a(this.c, this.b, 1);
    a();
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getWindow().requestFeature(3);
    setContentView(R.layout.mr_media_route_chooser_dialog);
    setTitle(R.string.mr_media_route_chooser_title);
    getWindow().setFeatureDrawableResource(3, MediaRouterThemeHelper.a(getContext(), R.attr.mediaRouteOffDrawable));
    this.d = new ArrayList();
    this.e = new RouteAdapter(getContext(), this.d);
    this.f = ((ListView)findViewById(R.id.media_route_list));
    this.f.setAdapter(this.e);
    this.f.setOnItemClickListener(this.e);
    this.f.setEmptyView(findViewById(16908292));
  }
  
  public void onDetachedFromWindow()
  {
    this.g = false;
    this.a.a(this.b);
    super.onDetachedFromWindow();
  }
  
  private final class MediaRouterCallback
    extends MediaRouter.Callback
  {
    private MediaRouterCallback() {}
    
    public void a(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      MediaRouteChooserDialog.this.a();
    }
    
    public void b(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      MediaRouteChooserDialog.this.a();
    }
    
    public void c(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      MediaRouteChooserDialog.this.a();
    }
    
    public void d(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      MediaRouteChooserDialog.this.dismiss();
    }
  }
  
  private final class RouteAdapter
    extends ArrayAdapter<MediaRouter.RouteInfo>
    implements AdapterView.OnItemClickListener
  {
    private final LayoutInflater b;
    
    public RouteAdapter(List<MediaRouter.RouteInfo> paramList)
    {
      super(0, localList);
      this.b = LayoutInflater.from(paramList);
    }
    
    public boolean areAllItemsEnabled()
    {
      return false;
    }
    
    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      Object localObject1 = paramView;
      paramView = (View)localObject1;
      if (localObject1 == null) {
        paramView = this.b.inflate(R.layout.mr_media_route_list_item, paramViewGroup, false);
      }
      paramViewGroup = (MediaRouter.RouteInfo)getItem(paramInt);
      Object localObject2 = (TextView)paramView.findViewById(16908308);
      localObject1 = (TextView)paramView.findViewById(16908309);
      ((TextView)localObject2).setText(paramViewGroup.a());
      localObject2 = paramViewGroup.b();
      if (TextUtils.isEmpty((CharSequence)localObject2))
      {
        ((TextView)localObject1).setVisibility(8);
        ((TextView)localObject1).setText("");
      }
      for (;;)
      {
        paramView.setEnabled(paramViewGroup.c());
        return paramView;
        ((TextView)localObject1).setVisibility(0);
        ((TextView)localObject1).setText((CharSequence)localObject2);
      }
    }
    
    public boolean isEnabled(int paramInt)
    {
      return ((MediaRouter.RouteInfo)getItem(paramInt)).c();
    }
    
    public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
    {
      paramAdapterView = (MediaRouter.RouteInfo)getItem(paramInt);
      if (paramAdapterView.c())
      {
        paramAdapterView.o();
        MediaRouteChooserDialog.this.dismiss();
      }
    }
  }
  
  private static final class RouteComparator
    implements Comparator<MediaRouter.RouteInfo>
  {
    public static final RouteComparator a = new RouteComparator();
    
    public int a(MediaRouter.RouteInfo paramRouteInfo1, MediaRouter.RouteInfo paramRouteInfo2)
    {
      return paramRouteInfo1.a().compareTo(paramRouteInfo2.a());
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/app/MediaRouteChooserDialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */