package android.support.v7.app;

import android.support.annotation.NonNull;

public class MediaRouteDialogFactory
{
  private static final MediaRouteDialogFactory a = new MediaRouteDialogFactory();
  
  @NonNull
  public static MediaRouteDialogFactory a()
  {
    return a;
  }
  
  @NonNull
  public MediaRouteChooserDialogFragment b()
  {
    return new MediaRouteChooserDialogFragment();
  }
  
  @NonNull
  public MediaRouteControllerDialogFragment c()
  {
    return new MediaRouteControllerDialogFragment();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/app/MediaRouteDialogFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */