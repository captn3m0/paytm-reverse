package android.support.v7.app;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.appcompat.R.styleable;
import android.support.v7.view.ActionMode;
import android.support.v7.view.ActionMode.Callback;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract class ActionBar
{
  public abstract int a();
  
  public ActionMode a(ActionMode.Callback paramCallback)
  {
    return null;
  }
  
  public void a(float paramFloat)
  {
    if (paramFloat != 0.0F) {
      throw new UnsupportedOperationException("Setting a non-zero elevation is not supported in this action bar configuration.");
    }
  }
  
  public abstract void a(int paramInt);
  
  public void a(Configuration paramConfiguration) {}
  
  public abstract void a(@Nullable Drawable paramDrawable);
  
  public abstract void a(View paramView);
  
  public abstract void a(CharSequence paramCharSequence);
  
  public abstract void a(boolean paramBoolean);
  
  public boolean a(int paramInt, KeyEvent paramKeyEvent)
  {
    return false;
  }
  
  public abstract int b();
  
  public abstract void b(@DrawableRes int paramInt);
  
  public void b(@Nullable Drawable paramDrawable) {}
  
  public void b(CharSequence paramCharSequence) {}
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c();
  
  public abstract void c(@DrawableRes int paramInt);
  
  public abstract void c(boolean paramBoolean);
  
  public abstract void d();
  
  public abstract void d(@StringRes int paramInt);
  
  public abstract void d(boolean paramBoolean);
  
  public void e(@StringRes int paramInt) {}
  
  public void e(boolean paramBoolean) {}
  
  public abstract boolean e();
  
  public Context f()
  {
    return null;
  }
  
  public void f(boolean paramBoolean)
  {
    if (paramBoolean) {
      throw new UnsupportedOperationException("Hide on content scroll is not supported in this action bar configuration.");
    }
  }
  
  public int g()
  {
    return 0;
  }
  
  public void g(boolean paramBoolean) {}
  
  public void h(boolean paramBoolean) {}
  
  public boolean h()
  {
    return false;
  }
  
  public void i(boolean paramBoolean) {}
  
  public boolean i()
  {
    return false;
  }
  
  boolean j()
  {
    return false;
  }
  
  void k() {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface DisplayOptions {}
  
  public static class LayoutParams
    extends ViewGroup.MarginLayoutParams
  {
    public int a = 0;
    
    public LayoutParams(int paramInt1, int paramInt2)
    {
      super(paramInt2);
      this.a = 8388627;
    }
    
    public LayoutParams(@NonNull Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
      paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ActionBarLayout);
      this.a = paramContext.getInt(R.styleable.ActionBarLayout_android_layout_gravity, 0);
      paramContext.recycle();
    }
    
    public LayoutParams(LayoutParams paramLayoutParams)
    {
      super();
      this.a = paramLayoutParams.a;
    }
    
    public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface NavigationMode {}
  
  public static abstract interface OnMenuVisibilityListener
  {
    public abstract void a(boolean paramBoolean);
  }
  
  public static abstract interface OnNavigationListener
  {
    public abstract boolean a(int paramInt, long paramLong);
  }
  
  public static abstract class Tab
  {
    public abstract int a();
    
    public abstract Drawable b();
    
    public abstract CharSequence c();
    
    public abstract View d();
    
    public abstract void e();
    
    public abstract CharSequence f();
  }
  
  public static abstract interface TabListener
  {
    public abstract void a(ActionBar.Tab paramTab, FragmentTransaction paramFragmentTransaction);
    
    public abstract void b(ActionBar.Tab paramTab, FragmentTransaction paramFragmentTransaction);
    
    public abstract void c(ActionBar.Tab paramTab, FragmentTransaction paramFragmentTransaction);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/app/ActionBar.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */