package android.support.v7.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.res.Resources.Theme;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.appcompat.R.attr;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListAdapter;

public class AlertDialog
  extends AppCompatDialog
  implements DialogInterface
{
  private AlertController a = new AlertController(getContext(), this, getWindow());
  
  protected AlertDialog(Context paramContext, int paramInt)
  {
    this(paramContext, paramInt, true);
  }
  
  AlertDialog(Context paramContext, int paramInt, boolean paramBoolean)
  {
    super(paramContext, a(paramContext, paramInt));
  }
  
  static int a(Context paramContext, int paramInt)
  {
    if (paramInt >= 16777216) {
      return paramInt;
    }
    TypedValue localTypedValue = new TypedValue();
    paramContext.getTheme().resolveAttribute(R.attr.alertDialogTheme, localTypedValue, true);
    return localTypedValue.resourceId;
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.a.a();
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (this.a.a(paramInt, paramKeyEvent)) {
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    if (this.a.b(paramInt, paramKeyEvent)) {
      return true;
    }
    return super.onKeyUp(paramInt, paramKeyEvent);
  }
  
  public void setTitle(CharSequence paramCharSequence)
  {
    super.setTitle(paramCharSequence);
    this.a.a(paramCharSequence);
  }
  
  public static class Builder
  {
    private final AlertController.AlertParams a;
    private int b;
    
    public Builder(Context paramContext)
    {
      this(paramContext, AlertDialog.a(paramContext, 0));
    }
    
    public Builder(Context paramContext, int paramInt)
    {
      this.a = new AlertController.AlertParams(new ContextThemeWrapper(paramContext, AlertDialog.a(paramContext, paramInt)));
      this.b = paramInt;
    }
    
    public Context a()
    {
      return this.a.a;
    }
    
    public Builder a(int paramInt, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.a.i = this.a.a.getText(paramInt);
      this.a.j = paramOnClickListener;
      return this;
    }
    
    public Builder a(DialogInterface.OnKeyListener paramOnKeyListener)
    {
      this.a.r = paramOnKeyListener;
      return this;
    }
    
    public Builder a(Drawable paramDrawable)
    {
      this.a.d = paramDrawable;
      return this;
    }
    
    public Builder a(View paramView)
    {
      this.a.g = paramView;
      return this;
    }
    
    public Builder a(ListAdapter paramListAdapter, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.a.t = paramListAdapter;
      this.a.u = paramOnClickListener;
      return this;
    }
    
    public Builder a(CharSequence paramCharSequence)
    {
      this.a.f = paramCharSequence;
      return this;
    }
    
    public Builder a(boolean paramBoolean)
    {
      this.a.o = paramBoolean;
      return this;
    }
    
    public Builder b(int paramInt, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.a.k = this.a.a.getText(paramInt);
      this.a.l = paramOnClickListener;
      return this;
    }
    
    public Builder b(CharSequence paramCharSequence)
    {
      this.a.h = paramCharSequence;
      return this;
    }
    
    public AlertDialog b()
    {
      AlertDialog localAlertDialog = new AlertDialog(this.a.a, this.b, false);
      this.a.a(AlertDialog.a(localAlertDialog));
      localAlertDialog.setCancelable(this.a.o);
      if (this.a.o) {
        localAlertDialog.setCanceledOnTouchOutside(true);
      }
      localAlertDialog.setOnCancelListener(this.a.p);
      localAlertDialog.setOnDismissListener(this.a.q);
      if (this.a.r != null) {
        localAlertDialog.setOnKeyListener(this.a.r);
      }
      return localAlertDialog;
    }
    
    public AlertDialog c()
    {
      AlertDialog localAlertDialog = b();
      localAlertDialog.show();
      return localAlertDialog;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/app/AlertDialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */