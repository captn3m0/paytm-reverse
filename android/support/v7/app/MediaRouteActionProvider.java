package android.support.v7.app;

import android.support.v4.view.ActionProvider;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.support.v7.media.MediaRouter.Callback;
import android.support.v7.media.MediaRouter.ProviderInfo;
import android.support.v7.media.MediaRouter.RouteInfo;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import java.lang.ref.WeakReference;

public class MediaRouteActionProvider
  extends ActionProvider
{
  private final MediaRouter a;
  private MediaRouteSelector b;
  private MediaRouteDialogFactory c;
  private MediaRouteButton d;
  
  private void j()
  {
    e();
  }
  
  public View b()
  {
    if (this.d != null) {
      Log.e("MediaRouteActionProvider", "onCreateActionView: this ActionProvider is already associated with a menu item. Don't reuse MediaRouteActionProvider instances! Abandoning the old menu item...");
    }
    this.d = i();
    this.d.setCheatSheetEnabled(true);
    this.d.setRouteSelector(this.b);
    this.d.setDialogFactory(this.c);
    this.d.setLayoutParams(new ViewGroup.LayoutParams(-2, -1));
    return this.d;
  }
  
  public boolean c()
  {
    return true;
  }
  
  public boolean d()
  {
    return this.a.a(this.b, 1);
  }
  
  public boolean f()
  {
    if (this.d != null) {
      return this.d.a();
    }
    return false;
  }
  
  public MediaRouteButton i()
  {
    return new MediaRouteButton(a());
  }
  
  private static final class MediaRouterCallback
    extends MediaRouter.Callback
  {
    private final WeakReference<MediaRouteActionProvider> a;
    
    private void a(MediaRouter paramMediaRouter)
    {
      MediaRouteActionProvider localMediaRouteActionProvider = (MediaRouteActionProvider)this.a.get();
      if (localMediaRouteActionProvider != null)
      {
        MediaRouteActionProvider.a(localMediaRouteActionProvider);
        return;
      }
      paramMediaRouter.a(this);
    }
    
    public void a(MediaRouter paramMediaRouter, MediaRouter.ProviderInfo paramProviderInfo)
    {
      a(paramMediaRouter);
    }
    
    public void a(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      a(paramMediaRouter);
    }
    
    public void b(MediaRouter paramMediaRouter, MediaRouter.ProviderInfo paramProviderInfo)
    {
      a(paramMediaRouter);
    }
    
    public void b(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      a(paramMediaRouter);
    }
    
    public void c(MediaRouter paramMediaRouter, MediaRouter.ProviderInfo paramProviderInfo)
    {
      a(paramMediaRouter);
    }
    
    public void c(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      a(paramMediaRouter);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/app/MediaRouteActionProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */