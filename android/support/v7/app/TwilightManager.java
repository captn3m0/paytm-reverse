package android.support.v7.app;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.content.PermissionChecker;
import android.util.Log;
import java.util.Calendar;

class TwilightManager
{
  private static final TwilightState a = new TwilightState(null);
  private final Context b;
  private final LocationManager c;
  
  TwilightManager(Context paramContext)
  {
    this.b = paramContext;
    this.c = ((LocationManager)paramContext.getSystemService("location"));
  }
  
  private Location a(String paramString)
  {
    if (this.c != null) {
      try
      {
        if (this.c.isProviderEnabled(paramString))
        {
          paramString = this.c.getLastKnownLocation(paramString);
          return paramString;
        }
      }
      catch (Exception paramString)
      {
        Log.d("TwilightManager", "Failed to get last known location", paramString);
      }
    }
    return null;
  }
  
  private void a(@NonNull Location paramLocation)
  {
    TwilightState localTwilightState = a;
    long l1 = System.currentTimeMillis();
    TwilightCalculator localTwilightCalculator = TwilightCalculator.a();
    localTwilightCalculator.a(l1 - 86400000L, paramLocation.getLatitude(), paramLocation.getLongitude());
    long l2 = localTwilightCalculator.a;
    localTwilightCalculator.a(l1, paramLocation.getLatitude(), paramLocation.getLongitude());
    if (localTwilightCalculator.c == 1) {}
    long l3;
    long l4;
    long l5;
    for (boolean bool = true;; bool = false)
    {
      l3 = localTwilightCalculator.b;
      l4 = localTwilightCalculator.a;
      localTwilightCalculator.a(86400000L + l1, paramLocation.getLatitude(), paramLocation.getLongitude());
      l5 = localTwilightCalculator.b;
      if ((l3 != -1L) && (l4 != -1L)) {
        break;
      }
      l1 += 43200000L;
      localTwilightState.a = bool;
      localTwilightState.b = l2;
      localTwilightState.c = l3;
      localTwilightState.d = l4;
      localTwilightState.e = l5;
      localTwilightState.f = l1;
      return;
    }
    if (l1 > l4) {
      l1 = 0L + l5;
    }
    for (;;)
    {
      l1 += 60000L;
      break;
      if (l1 > l3) {
        l1 = 0L + l4;
      } else {
        l1 = 0L + l3;
      }
    }
  }
  
  private boolean a(TwilightState paramTwilightState)
  {
    return (paramTwilightState != null) && (paramTwilightState.f > System.currentTimeMillis());
  }
  
  private Location b()
  {
    Location localLocation1 = null;
    Location localLocation2 = null;
    if (PermissionChecker.a(this.b, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
      localLocation1 = a("network");
    }
    if (PermissionChecker.a(this.b, "android.permission.ACCESS_FINE_LOCATION") == 0) {
      localLocation2 = a("gps");
    }
    if ((localLocation2 != null) && (localLocation1 != null)) {
      if (localLocation2.getTime() <= localLocation1.getTime()) {}
    }
    while (localLocation2 != null)
    {
      return localLocation2;
      return localLocation1;
    }
    return localLocation1;
  }
  
  boolean a()
  {
    TwilightState localTwilightState = a;
    if (a(localTwilightState)) {
      return localTwilightState.a;
    }
    Location localLocation = b();
    if (localLocation != null)
    {
      a(localLocation);
      return localTwilightState.a;
    }
    Log.i("TwilightManager", "Could not get last known location. This is probably because the app does not have any location permissions. Falling back to hardcoded sunrise/sunset values.");
    int i = Calendar.getInstance().get(11);
    return (i < 6) || (i >= 22);
  }
  
  private static class TwilightState
  {
    boolean a;
    long b;
    long c;
    long d;
    long e;
    long f;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/app/TwilightManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */