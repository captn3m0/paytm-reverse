package android.support.v7.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.support.v7.media.MediaRouter.Callback;

public class MediaRouteDiscoveryFragment
  extends Fragment
{
  private final String a = "selector";
  private MediaRouter b;
  private MediaRouteSelector c;
  private MediaRouter.Callback d;
  
  private void c()
  {
    if (this.b == null) {
      this.b = MediaRouter.a(getActivity());
    }
  }
  
  private void d()
  {
    if (this.c == null)
    {
      Bundle localBundle = getArguments();
      if (localBundle != null) {
        this.c = MediaRouteSelector.a(localBundle.getBundle("selector"));
      }
      if (this.c == null) {
        this.c = MediaRouteSelector.a;
      }
    }
  }
  
  public MediaRouter.Callback a()
  {
    new MediaRouter.Callback() {};
  }
  
  public int b()
  {
    return 4;
  }
  
  public void onStart()
  {
    super.onStart();
    d();
    c();
    this.d = a();
    if (this.d != null) {
      this.b.a(this.c, this.d, b());
    }
  }
  
  public void onStop()
  {
    if (this.d != null)
    {
      this.b.a(this.d);
      this.d = null;
    }
    super.onStop();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/app/MediaRouteDiscoveryFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */