package android.support.v7.app;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.support.v7.media.MediaRouter.Callback;
import android.support.v7.media.MediaRouter.ProviderInfo;
import android.support.v7.media.MediaRouter.RouteInfo;
import android.support.v7.mediarouter.R.attr;
import android.support.v7.mediarouter.R.styleable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.Toast;

public class MediaRouteButton
  extends View
{
  private static final int[] l = { 16842912 };
  private static final int[] m = { 16842911 };
  private final MediaRouter a;
  private final MediaRouterCallback b;
  private MediaRouteSelector c = MediaRouteSelector.a;
  private MediaRouteDialogFactory d = MediaRouteDialogFactory.a();
  private boolean e;
  private Drawable f;
  private boolean g;
  private boolean h;
  private boolean i;
  private int j;
  private int k;
  
  public MediaRouteButton(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public MediaRouteButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, R.attr.mediaRouteButtonStyle);
  }
  
  public MediaRouteButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(MediaRouterThemeHelper.a(paramContext), paramAttributeSet, paramInt);
    paramContext = getContext();
    this.a = MediaRouter.a(paramContext);
    this.b = new MediaRouterCallback(null);
    paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.MediaRouteButton, paramInt, 0);
    setRemoteIndicatorDrawable(paramContext.getDrawable(R.styleable.MediaRouteButton_externalRouteEnabledDrawable));
    this.j = paramContext.getDimensionPixelSize(R.styleable.MediaRouteButton_android_minWidth, 0);
    this.k = paramContext.getDimensionPixelSize(R.styleable.MediaRouteButton_android_minHeight, 0);
    paramContext.recycle();
    setClickable(true);
    setLongClickable(true);
  }
  
  private void b()
  {
    boolean bool3 = false;
    Object localObject;
    if (this.e)
    {
      localObject = this.a.c();
      if ((((MediaRouter.RouteInfo)localObject).f()) || (!((MediaRouter.RouteInfo)localObject).a(this.c))) {
        break label163;
      }
    }
    label163:
    for (boolean bool1 = true;; bool1 = false)
    {
      boolean bool2 = bool3;
      if (bool1)
      {
        bool2 = bool3;
        if (((MediaRouter.RouteInfo)localObject).d()) {
          bool2 = true;
        }
      }
      int n = 0;
      if (this.g != bool1)
      {
        this.g = bool1;
        n = 1;
      }
      if (this.i != bool2)
      {
        this.i = bool2;
        n = 1;
      }
      if (n != 0)
      {
        refreshDrawableState();
        if ((this.i) && ((this.f.getCurrent() instanceof AnimationDrawable)))
        {
          localObject = (AnimationDrawable)this.f.getCurrent();
          if (!((AnimationDrawable)localObject).isRunning()) {
            ((AnimationDrawable)localObject).start();
          }
        }
      }
      setEnabled(this.a.a(this.c, 1));
      return;
    }
  }
  
  private Activity getActivity()
  {
    for (Context localContext = getContext(); (localContext instanceof ContextWrapper); localContext = ((ContextWrapper)localContext).getBaseContext()) {
      if ((localContext instanceof Activity)) {
        return (Activity)localContext;
      }
    }
    return null;
  }
  
  private FragmentManager getFragmentManager()
  {
    Activity localActivity = getActivity();
    if ((localActivity instanceof FragmentActivity)) {
      return ((FragmentActivity)localActivity).getSupportFragmentManager();
    }
    return null;
  }
  
  public boolean a()
  {
    if (!this.e) {
      return false;
    }
    FragmentManager localFragmentManager = getFragmentManager();
    if (localFragmentManager == null) {
      throw new IllegalStateException("The activity must be a subclass of FragmentActivity");
    }
    Object localObject = this.a.c();
    if ((((MediaRouter.RouteInfo)localObject).f()) || (!((MediaRouter.RouteInfo)localObject).a(this.c)))
    {
      if (localFragmentManager.findFragmentByTag("android.support.v7.mediarouter:MediaRouteChooserDialogFragment") != null)
      {
        Log.w("MediaRouteButton", "showDialog(): Route chooser dialog already showing!");
        return false;
      }
      localObject = this.d.b();
      ((MediaRouteChooserDialogFragment)localObject).a(this.c);
      ((MediaRouteChooserDialogFragment)localObject).show(localFragmentManager, "android.support.v7.mediarouter:MediaRouteChooserDialogFragment");
    }
    for (;;)
    {
      return true;
      if (localFragmentManager.findFragmentByTag("android.support.v7.mediarouter:MediaRouteControllerDialogFragment") != null)
      {
        Log.w("MediaRouteButton", "showDialog(): Route controller dialog already showing!");
        return false;
      }
      this.d.c().show(localFragmentManager, "android.support.v7.mediarouter:MediaRouteControllerDialogFragment");
    }
  }
  
  protected void drawableStateChanged()
  {
    super.drawableStateChanged();
    if (this.f != null)
    {
      int[] arrayOfInt = getDrawableState();
      this.f.setState(arrayOfInt);
      invalidate();
    }
  }
  
  @NonNull
  public MediaRouteDialogFactory getDialogFactory()
  {
    return this.d;
  }
  
  @NonNull
  public MediaRouteSelector getRouteSelector()
  {
    return this.c;
  }
  
  public void jumpDrawablesToCurrentState()
  {
    if (getBackground() != null) {
      DrawableCompat.a(getBackground());
    }
    if (this.f != null) {
      DrawableCompat.a(this.f);
    }
  }
  
  public void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    this.e = true;
    if (!this.c.b()) {
      this.a.a(this.c, this.b);
    }
    b();
  }
  
  protected int[] onCreateDrawableState(int paramInt)
  {
    int[] arrayOfInt = super.onCreateDrawableState(paramInt + 1);
    if (this.i) {
      mergeDrawableStates(arrayOfInt, m);
    }
    while (!this.g) {
      return arrayOfInt;
    }
    mergeDrawableStates(arrayOfInt, l);
    return arrayOfInt;
  }
  
  public void onDetachedFromWindow()
  {
    this.e = false;
    if (!this.c.b()) {
      this.a.a(this.b);
    }
    super.onDetachedFromWindow();
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    if (this.f != null)
    {
      int i5 = getPaddingLeft();
      int i6 = getWidth();
      int i7 = getPaddingRight();
      int i2 = getPaddingTop();
      int i3 = getHeight();
      int i4 = getPaddingBottom();
      int n = this.f.getIntrinsicWidth();
      int i1 = this.f.getIntrinsicHeight();
      i5 += (i6 - i7 - i5 - n) / 2;
      i2 += (i3 - i4 - i2 - i1) / 2;
      this.f.setBounds(i5, i2, i5 + n, i2 + i1);
      this.f.draw(paramCanvas);
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i2 = 0;
    int i1 = View.MeasureSpec.getSize(paramInt1);
    int n = View.MeasureSpec.getSize(paramInt2);
    int i3 = View.MeasureSpec.getMode(paramInt1);
    paramInt2 = View.MeasureSpec.getMode(paramInt2);
    int i4 = this.j;
    if (this.f != null)
    {
      paramInt1 = this.f.getIntrinsicWidth();
      i4 = Math.max(i4, paramInt1);
      int i5 = this.k;
      paramInt1 = i2;
      if (this.f != null) {
        paramInt1 = this.f.getIntrinsicHeight();
      }
      i2 = Math.max(i5, paramInt1);
      switch (i3)
      {
      default: 
        paramInt1 = getPaddingLeft() + i4 + getPaddingRight();
        switch (paramInt2)
        {
        default: 
          label129:
          paramInt2 = getPaddingTop() + i2 + getPaddingBottom();
        }
        break;
      }
    }
    for (;;)
    {
      setMeasuredDimension(paramInt1, paramInt2);
      return;
      paramInt1 = 0;
      break;
      paramInt1 = i1;
      break label129;
      paramInt1 = Math.min(i1, getPaddingLeft() + i4 + getPaddingRight());
      break label129;
      paramInt2 = n;
      continue;
      paramInt2 = Math.min(n, getPaddingTop() + i2 + getPaddingBottom());
    }
  }
  
  public boolean performClick()
  {
    boolean bool1 = false;
    boolean bool2 = super.performClick();
    if (!bool2) {
      playSoundEffect(0);
    }
    if ((a()) || (bool2)) {
      bool1 = true;
    }
    return bool1;
  }
  
  public boolean performLongClick()
  {
    if (super.performLongClick()) {
      return true;
    }
    if (!this.h) {
      return false;
    }
    Object localObject = getContentDescription();
    if (TextUtils.isEmpty((CharSequence)localObject)) {
      return false;
    }
    int[] arrayOfInt = new int[2];
    Rect localRect = new Rect();
    getLocationOnScreen(arrayOfInt);
    getWindowVisibleDisplayFrame(localRect);
    Context localContext = getContext();
    int n = getWidth();
    int i1 = getHeight();
    int i2 = arrayOfInt[1];
    int i3 = i1 / 2;
    int i4 = localContext.getResources().getDisplayMetrics().widthPixels;
    localObject = Toast.makeText(localContext, (CharSequence)localObject, 0);
    if (i2 + i3 < localRect.height()) {
      ((Toast)localObject).setGravity(8388661, i4 - arrayOfInt[0] - n / 2, i1);
    }
    for (;;)
    {
      ((Toast)localObject).show();
      performHapticFeedback(0);
      return true;
      ((Toast)localObject).setGravity(81, 0, i1);
    }
  }
  
  void setCheatSheetEnabled(boolean paramBoolean)
  {
    this.h = paramBoolean;
  }
  
  public void setDialogFactory(@NonNull MediaRouteDialogFactory paramMediaRouteDialogFactory)
  {
    if (paramMediaRouteDialogFactory == null) {
      throw new IllegalArgumentException("factory must not be null");
    }
    this.d = paramMediaRouteDialogFactory;
  }
  
  public void setRemoteIndicatorDrawable(Drawable paramDrawable)
  {
    if (this.f != null)
    {
      this.f.setCallback(null);
      unscheduleDrawable(this.f);
    }
    this.f = paramDrawable;
    if (paramDrawable != null)
    {
      paramDrawable.setCallback(this);
      paramDrawable.setState(getDrawableState());
      if (getVisibility() != 0) {
        break label67;
      }
    }
    label67:
    for (boolean bool = true;; bool = false)
    {
      paramDrawable.setVisible(bool, false);
      refreshDrawableState();
      return;
    }
  }
  
  public void setRouteSelector(MediaRouteSelector paramMediaRouteSelector)
  {
    if (paramMediaRouteSelector == null) {
      throw new IllegalArgumentException("selector must not be null");
    }
    if (!this.c.equals(paramMediaRouteSelector))
    {
      if (this.e)
      {
        if (!this.c.b()) {
          this.a.a(this.b);
        }
        if (!paramMediaRouteSelector.b()) {
          this.a.a(paramMediaRouteSelector, this.b);
        }
      }
      this.c = paramMediaRouteSelector;
      b();
    }
  }
  
  public void setVisibility(int paramInt)
  {
    super.setVisibility(paramInt);
    Drawable localDrawable;
    if (this.f != null)
    {
      localDrawable = this.f;
      if (getVisibility() != 0) {
        break label34;
      }
    }
    label34:
    for (boolean bool = true;; bool = false)
    {
      localDrawable.setVisible(bool, false);
      return;
    }
  }
  
  protected boolean verifyDrawable(Drawable paramDrawable)
  {
    return (super.verifyDrawable(paramDrawable)) || (paramDrawable == this.f);
  }
  
  private final class MediaRouterCallback
    extends MediaRouter.Callback
  {
    private MediaRouterCallback() {}
    
    public void a(MediaRouter paramMediaRouter, MediaRouter.ProviderInfo paramProviderInfo)
    {
      MediaRouteButton.a(MediaRouteButton.this);
    }
    
    public void a(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      MediaRouteButton.a(MediaRouteButton.this);
    }
    
    public void b(MediaRouter paramMediaRouter, MediaRouter.ProviderInfo paramProviderInfo)
    {
      MediaRouteButton.a(MediaRouteButton.this);
    }
    
    public void b(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      MediaRouteButton.a(MediaRouteButton.this);
    }
    
    public void c(MediaRouter paramMediaRouter, MediaRouter.ProviderInfo paramProviderInfo)
    {
      MediaRouteButton.a(MediaRouteButton.this);
    }
    
    public void c(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      MediaRouteButton.a(MediaRouteButton.this);
    }
    
    public void d(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      MediaRouteButton.a(MediaRouteButton.this);
    }
    
    public void e(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      MediaRouteButton.a(MediaRouteButton.this);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/app/MediaRouteButton.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */