package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.support.v4.view.ViewPropertyAnimatorUpdateListener;
import android.support.v7.appcompat.R.attr;
import android.support.v7.appcompat.R.id;
import android.support.v7.appcompat.R.styleable;
import android.support.v7.view.ActionBarPolicy;
import android.support.v7.view.ActionMode;
import android.support.v7.view.ActionMode.Callback;
import android.support.v7.view.SupportMenuInflater;
import android.support.v7.view.ViewPropertyAnimatorCompatSet;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuBuilder.Callback;
import android.support.v7.widget.ActionBarContainer;
import android.support.v7.widget.ActionBarContextView;
import android.support.v7.widget.ActionBarOverlayLayout;
import android.support.v7.widget.ActionBarOverlayLayout.ActionBarVisibilityCallback;
import android.support.v7.widget.DecorToolbar;
import android.support.v7.widget.ScrollingTabContainerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class WindowDecorActionBar
  extends ActionBar
  implements ActionBarOverlayLayout.ActionBarVisibilityCallback
{
  private static final Interpolator i;
  private static final Interpolator j;
  private static final boolean k;
  private ArrayList<ActionBar.OnMenuVisibilityListener> A = new ArrayList();
  private boolean B;
  private int C = 0;
  private boolean D = true;
  private boolean E;
  private boolean F;
  private boolean G;
  private boolean H = true;
  private ViewPropertyAnimatorCompatSet I;
  private boolean J;
  ActionModeImpl a;
  ActionMode b;
  ActionMode.Callback c;
  boolean d;
  final ViewPropertyAnimatorListener e = new ViewPropertyAnimatorListenerAdapter()
  {
    public void b(View paramAnonymousView)
    {
      if ((WindowDecorActionBar.a(WindowDecorActionBar.this)) && (WindowDecorActionBar.b(WindowDecorActionBar.this) != null))
      {
        ViewCompat.b(WindowDecorActionBar.b(WindowDecorActionBar.this), 0.0F);
        ViewCompat.b(WindowDecorActionBar.c(WindowDecorActionBar.this), 0.0F);
      }
      WindowDecorActionBar.c(WindowDecorActionBar.this).setVisibility(8);
      WindowDecorActionBar.c(WindowDecorActionBar.this).setTransitioning(false);
      WindowDecorActionBar.a(WindowDecorActionBar.this, null);
      WindowDecorActionBar.this.l();
      if (WindowDecorActionBar.d(WindowDecorActionBar.this) != null) {
        ViewCompat.w(WindowDecorActionBar.d(WindowDecorActionBar.this));
      }
    }
  };
  final ViewPropertyAnimatorListener f = new ViewPropertyAnimatorListenerAdapter()
  {
    public void b(View paramAnonymousView)
    {
      WindowDecorActionBar.a(WindowDecorActionBar.this, null);
      WindowDecorActionBar.c(WindowDecorActionBar.this).requestLayout();
    }
  };
  final ViewPropertyAnimatorUpdateListener g = new ViewPropertyAnimatorUpdateListener()
  {
    public void a(View paramAnonymousView)
    {
      ((View)WindowDecorActionBar.c(WindowDecorActionBar.this).getParent()).invalidate();
    }
  };
  private Context l;
  private Context m;
  private Activity n;
  private Dialog o;
  private ActionBarOverlayLayout p;
  private ActionBarContainer q;
  private DecorToolbar r;
  private ActionBarContextView s;
  private View t;
  private ScrollingTabContainerView u;
  private ArrayList<TabImpl> v = new ArrayList();
  private TabImpl w;
  private int x = -1;
  private boolean y;
  private boolean z;
  
  static
  {
    boolean bool2 = true;
    if (!WindowDecorActionBar.class.desiredAssertionStatus())
    {
      bool1 = true;
      h = bool1;
      i = new AccelerateInterpolator();
      j = new DecelerateInterpolator();
      if (Build.VERSION.SDK_INT < 14) {
        break label56;
      }
    }
    label56:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      k = bool1;
      return;
      bool1 = false;
      break;
    }
  }
  
  public WindowDecorActionBar(Activity paramActivity, boolean paramBoolean)
  {
    this.n = paramActivity;
    paramActivity = paramActivity.getWindow().getDecorView();
    b(paramActivity);
    if (!paramBoolean) {
      this.t = paramActivity.findViewById(16908290);
    }
  }
  
  public WindowDecorActionBar(Dialog paramDialog)
  {
    this.o = paramDialog;
    b(paramDialog.getWindow().getDecorView());
  }
  
  private void b(View paramView)
  {
    this.p = ((ActionBarOverlayLayout)paramView.findViewById(R.id.decor_content_parent));
    if (this.p != null) {
      this.p.setActionBarVisibilityCallback(this);
    }
    this.r = c(paramView.findViewById(R.id.action_bar));
    this.s = ((ActionBarContextView)paramView.findViewById(R.id.action_context_bar));
    this.q = ((ActionBarContainer)paramView.findViewById(R.id.action_bar_container));
    if ((this.r == null) || (this.s == null) || (this.q == null)) {
      throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with a compatible window decor layout");
    }
    this.l = this.r.b();
    int i1;
    if ((this.r.o() & 0x4) != 0)
    {
      i1 = 1;
      if (i1 != 0) {
        this.y = true;
      }
      paramView = ActionBarPolicy.a(this.l);
      if ((!paramView.f()) && (i1 == 0)) {
        break label264;
      }
    }
    label264:
    for (boolean bool = true;; bool = false)
    {
      e(bool);
      n(paramView.d());
      paramView = this.l.obtainStyledAttributes(null, R.styleable.ActionBar, R.attr.actionBarStyle, 0);
      if (paramView.getBoolean(R.styleable.ActionBar_hideOnContentScroll, false)) {
        f(true);
      }
      i1 = paramView.getDimensionPixelSize(R.styleable.ActionBar_elevation, 0);
      if (i1 != 0) {
        a(i1);
      }
      paramView.recycle();
      return;
      i1 = 0;
      break;
    }
  }
  
  private static boolean b(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    if (paramBoolean3) {}
    while ((!paramBoolean1) && (!paramBoolean2)) {
      return true;
    }
    return false;
  }
  
  private DecorToolbar c(View paramView)
  {
    if ((paramView instanceof DecorToolbar)) {
      return (DecorToolbar)paramView;
    }
    if ((paramView instanceof Toolbar)) {
      return ((Toolbar)paramView).getWrapper();
    }
    if ("Can't make a decor toolbar out of " + paramView != null) {}
    for (paramView = paramView.getClass().getSimpleName();; paramView = "null") {
      throw new IllegalStateException(paramView);
    }
  }
  
  private void n(boolean paramBoolean)
  {
    boolean bool = true;
    this.B = paramBoolean;
    int i1;
    label45:
    label78:
    Object localObject;
    if (!this.B)
    {
      this.r.a(null);
      this.q.setTabContainer(this.u);
      if (m() != 2) {
        break label155;
      }
      i1 = 1;
      if (this.u != null)
      {
        if (i1 == 0) {
          break label160;
        }
        this.u.setVisibility(0);
        if (this.p != null) {
          ViewCompat.w(this.p);
        }
      }
      localObject = this.r;
      if ((this.B) || (i1 == 0)) {
        break label172;
      }
      paramBoolean = true;
      label97:
      ((DecorToolbar)localObject).a(paramBoolean);
      localObject = this.p;
      if ((this.B) || (i1 == 0)) {
        break label177;
      }
    }
    label155:
    label160:
    label172:
    label177:
    for (paramBoolean = bool;; paramBoolean = false)
    {
      ((ActionBarOverlayLayout)localObject).setHasNonEmbeddedTabs(paramBoolean);
      return;
      this.q.setTabContainer(null);
      this.r.a(this.u);
      break;
      i1 = 0;
      break label45;
      this.u.setVisibility(8);
      break label78;
      paramBoolean = false;
      break label97;
    }
  }
  
  private void o(boolean paramBoolean)
  {
    if (b(this.E, this.F, this.G)) {
      if (!this.H)
      {
        this.H = true;
        k(paramBoolean);
      }
    }
    while (!this.H) {
      return;
    }
    this.H = false;
    l(paramBoolean);
  }
  
  private void r()
  {
    if (!this.G)
    {
      this.G = true;
      if (this.p != null) {
        this.p.setShowingForActionMode(true);
      }
      o(false);
    }
  }
  
  private void s()
  {
    if (this.G)
    {
      this.G = false;
      if (this.p != null) {
        this.p.setShowingForActionMode(false);
      }
      o(false);
    }
  }
  
  public int a()
  {
    return this.r.o();
  }
  
  public ActionMode a(ActionMode.Callback paramCallback)
  {
    if (this.a != null) {
      this.a.c();
    }
    this.p.setHideOnContentScrollEnabled(false);
    this.s.c();
    paramCallback = new ActionModeImpl(this.s.getContext(), paramCallback);
    if (paramCallback.e())
    {
      paramCallback.d();
      this.s.a(paramCallback);
      m(true);
      this.s.sendAccessibilityEvent(32);
      this.a = paramCallback;
      return paramCallback;
    }
    return null;
  }
  
  public void a(float paramFloat)
  {
    ViewCompat.f(this.q, paramFloat);
  }
  
  public void a(int paramInt)
  {
    a(LayoutInflater.from(f()).inflate(paramInt, this.r.a(), false));
  }
  
  public void a(int paramInt1, int paramInt2)
  {
    int i1 = this.r.o();
    if ((paramInt2 & 0x4) != 0) {
      this.y = true;
    }
    this.r.c(paramInt1 & paramInt2 | (paramInt2 ^ 0xFFFFFFFF) & i1);
  }
  
  public void a(Configuration paramConfiguration)
  {
    n(ActionBarPolicy.a(this.l).d());
  }
  
  public void a(Drawable paramDrawable)
  {
    this.q.setPrimaryBackground(paramDrawable);
  }
  
  public void a(ActionBar.Tab paramTab)
  {
    int i1 = -1;
    if (m() != 2) {
      if (paramTab != null)
      {
        i1 = paramTab.a();
        this.x = i1;
      }
    }
    label137:
    label215:
    for (;;)
    {
      return;
      i1 = -1;
      break;
      FragmentTransaction localFragmentTransaction;
      if (((this.n instanceof FragmentActivity)) && (!this.r.a().isInEditMode()))
      {
        localFragmentTransaction = ((FragmentActivity)this.n).getSupportFragmentManager().beginTransaction().disallowAddToBackStack();
        if (this.w != paramTab) {
          break label137;
        }
        if (this.w != null)
        {
          this.w.g().c(this.w, localFragmentTransaction);
          this.u.a(paramTab.a());
        }
      }
      for (;;)
      {
        if ((localFragmentTransaction == null) || (localFragmentTransaction.isEmpty())) {
          break label215;
        }
        localFragmentTransaction.commit();
        return;
        localFragmentTransaction = null;
        break;
        ScrollingTabContainerView localScrollingTabContainerView = this.u;
        if (paramTab != null) {
          i1 = paramTab.a();
        }
        localScrollingTabContainerView.setTabSelected(i1);
        if (this.w != null) {
          this.w.g().b(this.w, localFragmentTransaction);
        }
        this.w = ((TabImpl)paramTab);
        if (this.w != null) {
          this.w.g().a(this.w, localFragmentTransaction);
        }
      }
    }
  }
  
  public void a(View paramView)
  {
    this.r.a(paramView);
  }
  
  public void a(CharSequence paramCharSequence)
  {
    this.r.b(paramCharSequence);
  }
  
  public void a(boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (int i1 = 2;; i1 = 0)
    {
      a(i1, 2);
      return;
    }
  }
  
  public int b()
  {
    return this.q.getHeight();
  }
  
  public void b(int paramInt)
  {
    this.r.a(paramInt);
  }
  
  public void b(Drawable paramDrawable)
  {
    this.r.b(paramDrawable);
  }
  
  public void b(CharSequence paramCharSequence)
  {
    this.r.a(paramCharSequence);
  }
  
  public void b(boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (int i1 = 4;; i1 = 0)
    {
      a(i1, 4);
      return;
    }
  }
  
  public void c()
  {
    if (this.E)
    {
      this.E = false;
      o(false);
    }
  }
  
  public void c(int paramInt)
  {
    this.r.b(paramInt);
  }
  
  public void c(boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (int i1 = 8;; i1 = 0)
    {
      a(i1, 8);
      return;
    }
  }
  
  public void d()
  {
    if (!this.E)
    {
      this.E = true;
      o(false);
    }
  }
  
  public void d(int paramInt)
  {
    a(this.l.getString(paramInt));
  }
  
  public void d(boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (int i1 = 16;; i1 = 0)
    {
      a(i1, 16);
      return;
    }
  }
  
  public void e(int paramInt)
  {
    this.r.d(paramInt);
  }
  
  public void e(boolean paramBoolean)
  {
    this.r.b(paramBoolean);
  }
  
  public boolean e()
  {
    int i1 = b();
    return (this.H) && ((i1 == 0) || (g() < i1));
  }
  
  public Context f()
  {
    int i1;
    if (this.m == null)
    {
      TypedValue localTypedValue = new TypedValue();
      this.l.getTheme().resolveAttribute(R.attr.actionBarWidgetTheme, localTypedValue, true);
      i1 = localTypedValue.resourceId;
      if (i1 == 0) {
        break label61;
      }
    }
    label61:
    for (this.m = new ContextThemeWrapper(this.l, i1);; this.m = this.l) {
      return this.m;
    }
  }
  
  public void f(int paramInt)
  {
    this.C = paramInt;
  }
  
  public void f(boolean paramBoolean)
  {
    if ((paramBoolean) && (!this.p.a())) {
      throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
    }
    this.d = paramBoolean;
    this.p.setHideOnContentScrollEnabled(paramBoolean);
  }
  
  public int g()
  {
    return this.p.getActionBarHideOffset();
  }
  
  public void g(boolean paramBoolean)
  {
    if (!this.y) {
      b(paramBoolean);
    }
  }
  
  public void h(boolean paramBoolean)
  {
    this.J = paramBoolean;
    if ((!paramBoolean) && (this.I != null)) {
      this.I.cancel();
    }
  }
  
  public void i(boolean paramBoolean)
  {
    if (paramBoolean == this.z) {}
    for (;;)
    {
      return;
      this.z = paramBoolean;
      int i2 = this.A.size();
      int i1 = 0;
      while (i1 < i2)
      {
        ((ActionBar.OnMenuVisibilityListener)this.A.get(i1)).a(paramBoolean);
        i1 += 1;
      }
    }
  }
  
  public boolean i()
  {
    if ((this.r != null) && (this.r.c()))
    {
      this.r.d();
      return true;
    }
    return false;
  }
  
  public void j(boolean paramBoolean)
  {
    this.D = paramBoolean;
  }
  
  public boolean j()
  {
    ViewGroup localViewGroup = this.r.a();
    if ((localViewGroup != null) && (!localViewGroup.hasFocus()))
    {
      localViewGroup.requestFocus();
      return true;
    }
    return false;
  }
  
  public void k(boolean paramBoolean)
  {
    if (this.I != null) {
      this.I.cancel();
    }
    this.q.setVisibility(0);
    if ((this.C == 0) && (k) && ((this.J) || (paramBoolean)))
    {
      ViewCompat.b(this.q, 0.0F);
      float f2 = -this.q.getHeight();
      float f1 = f2;
      if (paramBoolean)
      {
        localObject = new int[2];
        Object tmp77_75 = localObject;
        tmp77_75[0] = 0;
        Object tmp81_77 = tmp77_75;
        tmp81_77[1] = 0;
        tmp81_77;
        this.q.getLocationInWindow((int[])localObject);
        f1 = f2 - localObject[1];
      }
      ViewCompat.b(this.q, f1);
      Object localObject = new ViewPropertyAnimatorCompatSet();
      ViewPropertyAnimatorCompat localViewPropertyAnimatorCompat = ViewCompat.s(this.q).c(0.0F);
      localViewPropertyAnimatorCompat.a(this.g);
      ((ViewPropertyAnimatorCompatSet)localObject).a(localViewPropertyAnimatorCompat);
      if ((this.D) && (this.t != null))
      {
        ViewCompat.b(this.t, f1);
        ((ViewPropertyAnimatorCompatSet)localObject).a(ViewCompat.s(this.t).c(0.0F));
      }
      ((ViewPropertyAnimatorCompatSet)localObject).a(j);
      ((ViewPropertyAnimatorCompatSet)localObject).a(250L);
      ((ViewPropertyAnimatorCompatSet)localObject).a(this.f);
      this.I = ((ViewPropertyAnimatorCompatSet)localObject);
      ((ViewPropertyAnimatorCompatSet)localObject).a();
    }
    for (;;)
    {
      if (this.p != null) {
        ViewCompat.w(this.p);
      }
      return;
      ViewCompat.c(this.q, 1.0F);
      ViewCompat.b(this.q, 0.0F);
      if ((this.D) && (this.t != null)) {
        ViewCompat.b(this.t, 0.0F);
      }
      this.f.b(null);
    }
  }
  
  void l()
  {
    if (this.c != null)
    {
      this.c.a(this.b);
      this.b = null;
      this.c = null;
    }
  }
  
  public void l(boolean paramBoolean)
  {
    if (this.I != null) {
      this.I.cancel();
    }
    if ((this.C == 0) && (k) && ((this.J) || (paramBoolean)))
    {
      ViewCompat.c(this.q, 1.0F);
      this.q.setTransitioning(true);
      ViewPropertyAnimatorCompatSet localViewPropertyAnimatorCompatSet = new ViewPropertyAnimatorCompatSet();
      float f2 = -this.q.getHeight();
      float f1 = f2;
      if (paramBoolean)
      {
        localObject = new int[2];
        Object tmp86_84 = localObject;
        tmp86_84[0] = 0;
        Object tmp90_86 = tmp86_84;
        tmp90_86[1] = 0;
        tmp90_86;
        this.q.getLocationInWindow((int[])localObject);
        f1 = f2 - localObject[1];
      }
      Object localObject = ViewCompat.s(this.q).c(f1);
      ((ViewPropertyAnimatorCompat)localObject).a(this.g);
      localViewPropertyAnimatorCompatSet.a((ViewPropertyAnimatorCompat)localObject);
      if ((this.D) && (this.t != null)) {
        localViewPropertyAnimatorCompatSet.a(ViewCompat.s(this.t).c(f1));
      }
      localViewPropertyAnimatorCompatSet.a(i);
      localViewPropertyAnimatorCompatSet.a(250L);
      localViewPropertyAnimatorCompatSet.a(this.e);
      this.I = localViewPropertyAnimatorCompatSet;
      localViewPropertyAnimatorCompatSet.a();
      return;
    }
    this.e.b(null);
  }
  
  public int m()
  {
    return this.r.p();
  }
  
  public void m(boolean paramBoolean)
  {
    ViewPropertyAnimatorCompat localViewPropertyAnimatorCompat2;
    ViewPropertyAnimatorCompat localViewPropertyAnimatorCompat1;
    if (paramBoolean)
    {
      r();
      if (!paramBoolean) {
        break label68;
      }
      localViewPropertyAnimatorCompat2 = this.r.a(4, 100L);
      localViewPropertyAnimatorCompat1 = this.s.a(0, 200L);
    }
    for (;;)
    {
      ViewPropertyAnimatorCompatSet localViewPropertyAnimatorCompatSet = new ViewPropertyAnimatorCompatSet();
      localViewPropertyAnimatorCompatSet.a(localViewPropertyAnimatorCompat2, localViewPropertyAnimatorCompat1);
      localViewPropertyAnimatorCompatSet.a();
      return;
      s();
      break;
      label68:
      localViewPropertyAnimatorCompat1 = this.r.a(0, 200L);
      localViewPropertyAnimatorCompat2 = this.s.a(8, 100L);
    }
  }
  
  public void n()
  {
    if (this.F)
    {
      this.F = false;
      o(true);
    }
  }
  
  public void o()
  {
    if (!this.F)
    {
      this.F = true;
      o(true);
    }
  }
  
  public void p()
  {
    if (this.I != null)
    {
      this.I.cancel();
      this.I = null;
    }
  }
  
  public void q() {}
  
  public class ActionModeImpl
    extends ActionMode
    implements MenuBuilder.Callback
  {
    private final Context b;
    private final MenuBuilder c;
    private ActionMode.Callback d;
    private WeakReference<View> e;
    
    public ActionModeImpl(Context paramContext, ActionMode.Callback paramCallback)
    {
      this.b = paramContext;
      this.d = paramCallback;
      this.c = new MenuBuilder(paramContext).a(1);
      this.c.a(this);
    }
    
    public MenuInflater a()
    {
      return new SupportMenuInflater(this.b);
    }
    
    public void a(int paramInt)
    {
      b(WindowDecorActionBar.i(WindowDecorActionBar.this).getResources().getString(paramInt));
    }
    
    public void a(MenuBuilder paramMenuBuilder)
    {
      if (this.d == null) {
        return;
      }
      d();
      WindowDecorActionBar.g(WindowDecorActionBar.this).a();
    }
    
    public void a(View paramView)
    {
      WindowDecorActionBar.g(WindowDecorActionBar.this).setCustomView(paramView);
      this.e = new WeakReference(paramView);
    }
    
    public void a(CharSequence paramCharSequence)
    {
      WindowDecorActionBar.g(WindowDecorActionBar.this).setSubtitle(paramCharSequence);
    }
    
    public void a(boolean paramBoolean)
    {
      super.a(paramBoolean);
      WindowDecorActionBar.g(WindowDecorActionBar.this).setTitleOptional(paramBoolean);
    }
    
    public boolean a(MenuBuilder paramMenuBuilder, MenuItem paramMenuItem)
    {
      if (this.d != null) {
        return this.d.a(this, paramMenuItem);
      }
      return false;
    }
    
    public Menu b()
    {
      return this.c;
    }
    
    public void b(int paramInt)
    {
      a(WindowDecorActionBar.i(WindowDecorActionBar.this).getResources().getString(paramInt));
    }
    
    public void b(CharSequence paramCharSequence)
    {
      WindowDecorActionBar.g(WindowDecorActionBar.this).setTitle(paramCharSequence);
    }
    
    public void c()
    {
      if (WindowDecorActionBar.this.a != this) {
        return;
      }
      if (!WindowDecorActionBar.a(WindowDecorActionBar.e(WindowDecorActionBar.this), WindowDecorActionBar.f(WindowDecorActionBar.this), false))
      {
        WindowDecorActionBar.this.b = this;
        WindowDecorActionBar.this.c = this.d;
      }
      for (;;)
      {
        this.d = null;
        WindowDecorActionBar.this.m(false);
        WindowDecorActionBar.g(WindowDecorActionBar.this).b();
        WindowDecorActionBar.h(WindowDecorActionBar.this).a().sendAccessibilityEvent(32);
        WindowDecorActionBar.d(WindowDecorActionBar.this).setHideOnContentScrollEnabled(WindowDecorActionBar.this.d);
        WindowDecorActionBar.this.a = null;
        return;
        this.d.a(this);
      }
    }
    
    public void d()
    {
      if (WindowDecorActionBar.this.a != this) {
        return;
      }
      this.c.g();
      try
      {
        this.d.b(this, this.c);
        return;
      }
      finally
      {
        this.c.h();
      }
    }
    
    public boolean e()
    {
      this.c.g();
      try
      {
        boolean bool = this.d.a(this, this.c);
        return bool;
      }
      finally
      {
        this.c.h();
      }
    }
    
    public CharSequence f()
    {
      return WindowDecorActionBar.g(WindowDecorActionBar.this).getTitle();
    }
    
    public CharSequence g()
    {
      return WindowDecorActionBar.g(WindowDecorActionBar.this).getSubtitle();
    }
    
    public boolean h()
    {
      return WindowDecorActionBar.g(WindowDecorActionBar.this).d();
    }
    
    public View i()
    {
      if (this.e != null) {
        return (View)this.e.get();
      }
      return null;
    }
  }
  
  public class TabImpl
    extends ActionBar.Tab
  {
    private ActionBar.TabListener b;
    private Drawable c;
    private CharSequence d;
    private CharSequence e;
    private int f;
    private View g;
    
    public int a()
    {
      return this.f;
    }
    
    public Drawable b()
    {
      return this.c;
    }
    
    public CharSequence c()
    {
      return this.d;
    }
    
    public View d()
    {
      return this.g;
    }
    
    public void e()
    {
      this.a.a(this);
    }
    
    public CharSequence f()
    {
      return this.e;
    }
    
    public ActionBar.TabListener g()
    {
      return this.b;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/app/WindowDecorActionBar.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */