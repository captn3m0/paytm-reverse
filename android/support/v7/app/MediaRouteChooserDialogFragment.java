package android.support.v7.app;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.media.MediaRouteSelector;

public class MediaRouteChooserDialogFragment
  extends DialogFragment
{
  private final String a = "selector";
  private MediaRouteSelector b;
  
  public MediaRouteChooserDialogFragment()
  {
    setCancelable(true);
  }
  
  private void b()
  {
    if (this.b == null)
    {
      Bundle localBundle = getArguments();
      if (localBundle != null) {
        this.b = MediaRouteSelector.a(localBundle.getBundle("selector"));
      }
      if (this.b == null) {
        this.b = MediaRouteSelector.a;
      }
    }
  }
  
  public MediaRouteChooserDialog a(Context paramContext, Bundle paramBundle)
  {
    return new MediaRouteChooserDialog(paramContext);
  }
  
  public MediaRouteSelector a()
  {
    b();
    return this.b;
  }
  
  public void a(MediaRouteSelector paramMediaRouteSelector)
  {
    if (paramMediaRouteSelector == null) {
      throw new IllegalArgumentException("selector must not be null");
    }
    b();
    if (!this.b.equals(paramMediaRouteSelector))
    {
      this.b = paramMediaRouteSelector;
      Bundle localBundle = getArguments();
      Object localObject = localBundle;
      if (localBundle == null) {
        localObject = new Bundle();
      }
      ((Bundle)localObject).putBundle("selector", paramMediaRouteSelector.d());
      setArguments((Bundle)localObject);
      localObject = (MediaRouteChooserDialog)getDialog();
      if (localObject != null) {
        ((MediaRouteChooserDialog)localObject).a(paramMediaRouteSelector);
      }
    }
  }
  
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = a(getActivity(), paramBundle);
    paramBundle.a(a());
    return paramBundle;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/app/MediaRouteChooserDialogFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */