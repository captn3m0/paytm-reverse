package android.support.v7.app;

class TwilightCalculator
{
  private static TwilightCalculator d;
  public long a;
  public long b;
  public int c;
  
  static TwilightCalculator a()
  {
    if (d == null) {
      d = new TwilightCalculator();
    }
    return d;
  }
  
  public void a(long paramLong, double paramDouble1, double paramDouble2)
  {
    float f1 = (float)(paramLong - 946728000000L) / 8.64E7F;
    float f2 = 6.24006F + 0.01720197F * f1;
    double d1 = 1.796593063D + (f2 + 0.03341960161924362D * Math.sin(f2) + 3.4906598739326E-4D * Math.sin(2.0F * f2) + 5.236000106378924E-6D * Math.sin(3.0F * f2)) + 3.141592653589793D;
    paramDouble2 = -paramDouble2 / 360.0D;
    paramDouble2 = 9.0E-4F + (float)Math.round(f1 - 9.0E-4F - paramDouble2) + paramDouble2 + 0.0053D * Math.sin(f2) + -0.0069D * Math.sin(2.0D * d1);
    d1 = Math.asin(Math.sin(d1) * Math.sin(0.4092797040939331D));
    paramDouble1 *= 0.01745329238474369D;
    paramDouble1 = (Math.sin(-0.10471975803375244D) - Math.sin(paramDouble1) * Math.sin(d1)) / (Math.cos(paramDouble1) * Math.cos(d1));
    if (paramDouble1 >= 1.0D)
    {
      this.c = 1;
      this.a = -1L;
      this.b = -1L;
      return;
    }
    if (paramDouble1 <= -1.0D)
    {
      this.c = 0;
      this.a = -1L;
      this.b = -1L;
      return;
    }
    f1 = (float)(Math.acos(paramDouble1) / 6.283185307179586D);
    this.a = (Math.round((f1 + paramDouble2) * 8.64E7D) + 946728000000L);
    this.b = (Math.round((paramDouble2 - f1) * 8.64E7D) + 946728000000L);
    if ((this.b < paramLong) && (this.a > paramLong))
    {
      this.c = 0;
      return;
    }
    this.c = 1;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/app/TwilightCalculator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */