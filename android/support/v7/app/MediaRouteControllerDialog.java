package android.support.v7.app;

import android.content.Context;
import android.content.IntentSender;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaControllerCompat.Callback;
import android.support.v4.media.session.MediaControllerCompat.TransportControls;
import android.support.v4.media.session.MediaSessionCompat.Token;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.support.v7.media.MediaRouter.Callback;
import android.support.v7.media.MediaRouter.RouteInfo;
import android.support.v7.mediarouter.R.attr;
import android.support.v7.mediarouter.R.id;
import android.support.v7.mediarouter.R.layout;
import android.support.v7.mediarouter.R.string;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class MediaRouteControllerDialog
  extends AlertDialog
{
  private final MediaRouter a;
  private final MediaRouterCallback b;
  private final MediaRouter.RouteInfo c;
  private boolean d;
  private boolean e;
  private View f;
  private Button g;
  private Button h;
  private ImageButton i;
  private ImageButton j;
  private ImageView k;
  private TextView l;
  private TextView m;
  private TextView n;
  private boolean o = true;
  private LinearLayout p;
  private SeekBar q;
  private boolean r;
  private MediaControllerCompat s;
  private MediaControllerCallback t;
  private PlaybackStateCompat u;
  private MediaDescriptionCompat v;
  
  public MediaRouteControllerDialog(Context paramContext)
  {
    this(paramContext, 0);
  }
  
  public MediaRouteControllerDialog(Context paramContext, int paramInt)
  {
    super(MediaRouterThemeHelper.a(paramContext), paramInt);
    paramContext = getContext();
    this.t = new MediaControllerCallback(null);
    this.a = MediaRouter.a(paramContext);
    this.b = new MediaRouterCallback(null);
    this.c = this.a.c();
    a(this.a.d());
  }
  
  private void a(MediaSessionCompat.Token paramToken)
  {
    Object localObject = null;
    if (this.s != null)
    {
      this.s.b(this.t);
      this.s = null;
    }
    if (paramToken == null) {}
    while (!this.e) {
      return;
    }
    try
    {
      this.s = new MediaControllerCompat(getContext(), paramToken);
      if (this.s != null) {
        this.s.a(this.t);
      }
      if (this.s == null)
      {
        paramToken = null;
        if (paramToken != null) {
          break label135;
        }
        paramToken = null;
        this.v = paramToken;
        if (this.s != null) {
          break label143;
        }
        paramToken = (MediaSessionCompat.Token)localObject;
        this.u = paramToken;
        b();
      }
    }
    catch (RemoteException paramToken)
    {
      for (;;)
      {
        Log.e("MediaRouteControllerDialog", "Error creating media controller in setMediaSession.", paramToken);
        continue;
        paramToken = this.s.c();
        continue;
        label135:
        paramToken = paramToken.a();
        continue;
        label143:
        paramToken = this.s.b();
      }
    }
  }
  
  private boolean b()
  {
    if ((!this.c.e()) || (this.c.f()))
    {
      dismiss();
      return false;
    }
    if (!this.d) {
      return false;
    }
    c();
    this.n.setText(this.c.a());
    label89:
    label135:
    Object localObject;
    label145:
    boolean bool;
    label156:
    CharSequence localCharSequence;
    label166:
    int i1;
    label176:
    label252:
    int i2;
    label270:
    int i3;
    if (this.c.l())
    {
      this.g.setVisibility(0);
      if (this.c.n() == null) {
        break label355;
      }
      this.j.setVisibility(0);
      if (this.f == null)
      {
        if ((this.v == null) || (this.v.c() == null)) {
          break label367;
        }
        this.k.setImageBitmap(this.v.c());
        this.k.setVisibility(0);
        if (this.v != null) {
          break label429;
        }
        localObject = null;
        if (TextUtils.isEmpty((CharSequence)localObject)) {
          break label441;
        }
        bool = true;
        if (this.v != null) {
          break label447;
        }
        localCharSequence = null;
        if (TextUtils.isEmpty(localCharSequence)) {
          break label459;
        }
        i1 = 1;
        if ((bool) || (i1 != 0)) {
          break label464;
        }
        this.l.setText(R.string.mr_media_route_controller_no_info_available);
        this.l.setEnabled(false);
        this.l.setVisibility(0);
        this.m.setVisibility(8);
        if (this.u == null) {
          break label626;
        }
        if ((this.u.a() != 6) && (this.u.a() != 3)) {
          break label543;
        }
        i1 = 1;
        if ((this.u.d() & 0x204) == 0L) {
          break label548;
        }
        i2 = 1;
        if ((this.u.d() & 0x202) == 0L) {
          break label553;
        }
        i3 = 1;
        label288:
        if ((i1 == 0) || (i3 == 0)) {
          break label558;
        }
        this.i.setVisibility(0);
        this.i.setImageResource(MediaRouterThemeHelper.a(getContext(), R.attr.mediaRoutePauseDrawable));
        this.i.setContentDescription(getContext().getResources().getText(R.string.mr_media_route_controller_pause));
      }
    }
    for (;;)
    {
      return true;
      this.g.setVisibility(8);
      break;
      label355:
      this.j.setVisibility(8);
      break label89;
      label367:
      if ((this.v != null) && (this.v.d() != null))
      {
        this.k.setImageURI(this.v.d());
        this.k.setVisibility(0);
        break label135;
      }
      this.k.setImageDrawable(null);
      this.k.setVisibility(8);
      break label135;
      label429:
      localObject = this.v.a();
      break label145;
      label441:
      bool = false;
      break label156;
      label447:
      localCharSequence = this.v.b();
      break label166;
      label459:
      i1 = 0;
      break label176;
      label464:
      this.l.setText((CharSequence)localObject);
      this.l.setEnabled(bool);
      localObject = this.l;
      if (bool)
      {
        i2 = 0;
        label495:
        ((TextView)localObject).setVisibility(i2);
        this.m.setText(localCharSequence);
        localObject = this.m;
        if (i1 == 0) {
          break label537;
        }
      }
      label537:
      for (i1 = 0;; i1 = 8)
      {
        ((TextView)localObject).setVisibility(i1);
        break;
        i2 = 8;
        break label495;
      }
      label543:
      i1 = 0;
      break label252;
      label548:
      i2 = 0;
      break label270;
      label553:
      i3 = 0;
      break label288;
      label558:
      if ((i1 == 0) && (i2 != 0))
      {
        this.i.setVisibility(0);
        this.i.setImageResource(MediaRouterThemeHelper.a(getContext(), R.attr.mediaRoutePlayDrawable));
        this.i.setContentDescription(getContext().getResources().getText(R.string.mr_media_route_controller_play));
      }
      else
      {
        this.i.setVisibility(8);
        continue;
        label626:
        this.i.setVisibility(8);
      }
    }
  }
  
  private void c()
  {
    if (!this.r)
    {
      if (d())
      {
        this.p.setVisibility(0);
        this.q.setMax(this.c.k());
        this.q.setProgress(this.c.j());
      }
    }
    else {
      return;
    }
    this.p.setVisibility(8);
  }
  
  private boolean d()
  {
    return (this.o) && (this.c.i() == 1);
  }
  
  public View a(Bundle paramBundle)
  {
    return null;
  }
  
  public void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    this.e = true;
    this.a.a(MediaRouteSelector.a, this.b, 2);
    a(this.a.d());
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.mr_media_route_controller_material_dialog_b);
    ClickListener localClickListener = new ClickListener(null);
    this.g = ((Button)findViewById(R.id.disconnect));
    this.g.setOnClickListener(localClickListener);
    this.h = ((Button)findViewById(R.id.stop));
    this.h.setOnClickListener(localClickListener);
    this.j = ((ImageButton)findViewById(R.id.settings));
    this.j.setOnClickListener(localClickListener);
    this.k = ((ImageView)findViewById(R.id.art));
    this.l = ((TextView)findViewById(R.id.title));
    this.m = ((TextView)findViewById(R.id.subtitle));
    this.i = ((ImageButton)findViewById(R.id.play_pause));
    this.i.setOnClickListener(localClickListener);
    this.n = ((TextView)findViewById(R.id.route_name));
    this.p = ((LinearLayout)findViewById(R.id.media_route_volume_layout));
    this.q = ((SeekBar)findViewById(R.id.media_route_volume_slider));
    this.q.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
    {
      private final Runnable b = new Runnable()
      {
        public void run()
        {
          if (MediaRouteControllerDialog.a(MediaRouteControllerDialog.this))
          {
            MediaRouteControllerDialog.a(MediaRouteControllerDialog.this, false);
            MediaRouteControllerDialog.b(MediaRouteControllerDialog.this);
          }
        }
      };
      
      public void onProgressChanged(SeekBar paramAnonymousSeekBar, int paramAnonymousInt, boolean paramAnonymousBoolean)
      {
        if (paramAnonymousBoolean) {
          MediaRouteControllerDialog.d(MediaRouteControllerDialog.this).a(paramAnonymousInt);
        }
      }
      
      public void onStartTrackingTouch(SeekBar paramAnonymousSeekBar)
      {
        if (MediaRouteControllerDialog.a(MediaRouteControllerDialog.this))
        {
          MediaRouteControllerDialog.c(MediaRouteControllerDialog.this).removeCallbacks(this.b);
          return;
        }
        MediaRouteControllerDialog.a(MediaRouteControllerDialog.this, true);
      }
      
      public void onStopTrackingTouch(SeekBar paramAnonymousSeekBar)
      {
        MediaRouteControllerDialog.c(MediaRouteControllerDialog.this).postDelayed(this.b, 250L);
      }
    });
    this.d = true;
    if (b())
    {
      this.f = a(paramBundle);
      paramBundle = (FrameLayout)findViewById(R.id.media_route_control_frame);
      if (this.f != null)
      {
        paramBundle.findViewById(R.id.default_control_frame).setVisibility(8);
        paramBundle.addView(this.f);
      }
    }
  }
  
  public void onDetachedFromWindow()
  {
    this.a.a(this.b);
    a(null);
    this.e = false;
    super.onDetachedFromWindow();
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 25) || (paramInt == 24))
    {
      paramKeyEvent = this.c;
      if (paramInt == 25) {}
      for (paramInt = -1;; paramInt = 1)
      {
        paramKeyEvent.b(paramInt);
        return true;
      }
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 25) || (paramInt == 24)) {
      return true;
    }
    return super.onKeyUp(paramInt, paramKeyEvent);
  }
  
  private final class ClickListener
    implements View.OnClickListener
  {
    private ClickListener() {}
    
    public void onClick(View paramView)
    {
      int i = paramView.getId();
      if ((i == R.id.stop) || (i == R.id.disconnect)) {
        if (MediaRouteControllerDialog.d(MediaRouteControllerDialog.this).e())
        {
          paramView = MediaRouteControllerDialog.h(MediaRouteControllerDialog.this);
          if (i == R.id.stop)
          {
            i = 2;
            paramView.a(i);
          }
        }
        else
        {
          MediaRouteControllerDialog.this.dismiss();
        }
      }
      label136:
      do
      {
        do
        {
          do
          {
            return;
            i = 1;
            break;
            if (i != R.id.play_pause) {
              break label136;
            }
          } while ((MediaRouteControllerDialog.f(MediaRouteControllerDialog.this) == null) || (MediaRouteControllerDialog.i(MediaRouteControllerDialog.this) == null));
          if (MediaRouteControllerDialog.i(MediaRouteControllerDialog.this).a() == 3)
          {
            MediaRouteControllerDialog.f(MediaRouteControllerDialog.this).a().b();
            return;
          }
          MediaRouteControllerDialog.f(MediaRouteControllerDialog.this).a().a();
          return;
        } while (i != R.id.settings);
        paramView = MediaRouteControllerDialog.d(MediaRouteControllerDialog.this).n();
      } while (paramView == null);
      try
      {
        paramView.sendIntent(null, 0, null, null, null);
        MediaRouteControllerDialog.this.dismiss();
        return;
      }
      catch (Exception paramView)
      {
        Log.e("MediaRouteControllerDialog", "Error opening route settings.", paramView);
      }
    }
  }
  
  private final class MediaControllerCallback
    extends MediaControllerCompat.Callback
  {
    private MediaControllerCallback() {}
    
    public void a()
    {
      if (MediaRouteControllerDialog.f(MediaRouteControllerDialog.this) != null)
      {
        MediaRouteControllerDialog.f(MediaRouteControllerDialog.this).b(MediaRouteControllerDialog.g(MediaRouteControllerDialog.this));
        MediaRouteControllerDialog.a(MediaRouteControllerDialog.this, null);
      }
    }
    
    public void a(MediaMetadataCompat paramMediaMetadataCompat)
    {
      MediaRouteControllerDialog localMediaRouteControllerDialog = MediaRouteControllerDialog.this;
      if (paramMediaMetadataCompat == null) {}
      for (paramMediaMetadataCompat = null;; paramMediaMetadataCompat = paramMediaMetadataCompat.a())
      {
        MediaRouteControllerDialog.a(localMediaRouteControllerDialog, paramMediaMetadataCompat);
        MediaRouteControllerDialog.e(MediaRouteControllerDialog.this);
        return;
      }
    }
    
    public void a(PlaybackStateCompat paramPlaybackStateCompat)
    {
      MediaRouteControllerDialog.a(MediaRouteControllerDialog.this, paramPlaybackStateCompat);
      MediaRouteControllerDialog.e(MediaRouteControllerDialog.this);
    }
  }
  
  private final class MediaRouterCallback
    extends MediaRouter.Callback
  {
    private MediaRouterCallback() {}
    
    public void c(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      MediaRouteControllerDialog.e(MediaRouteControllerDialog.this);
    }
    
    public void e(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      MediaRouteControllerDialog.e(MediaRouteControllerDialog.this);
    }
    
    public void f(MediaRouter paramMediaRouter, MediaRouter.RouteInfo paramRouteInfo)
    {
      if (paramRouteInfo == MediaRouteControllerDialog.d(MediaRouteControllerDialog.this)) {
        MediaRouteControllerDialog.b(MediaRouteControllerDialog.this);
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/app/MediaRouteControllerDialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */