package android.support.v7.app;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R.attr;
import android.support.v7.appcompat.R.layout;
import android.support.v7.appcompat.R.style;
import android.support.v7.view.WindowCallbackWrapper;
import android.support.v7.view.menu.ListMenuPresenter;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuBuilder.Callback;
import android.support.v7.view.menu.MenuPresenter.Callback;
import android.support.v7.widget.DecorToolbar;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.Toolbar.OnMenuItemClickListener;
import android.support.v7.widget.ToolbarWidgetWrapper;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window.Callback;
import android.widget.ListAdapter;
import java.util.ArrayList;

class ToolbarActionBar
  extends ActionBar
{
  private DecorToolbar a;
  private boolean b;
  private Window.Callback c;
  private boolean d;
  private boolean e;
  private ArrayList<ActionBar.OnMenuVisibilityListener> f = new ArrayList();
  private ListMenuPresenter g;
  private final Runnable h = new Runnable()
  {
    public void run()
    {
      ToolbarActionBar.this.m();
    }
  };
  private final Toolbar.OnMenuItemClickListener i = new Toolbar.OnMenuItemClickListener()
  {
    public boolean a(MenuItem paramAnonymousMenuItem)
    {
      return ToolbarActionBar.a(ToolbarActionBar.this).onMenuItemSelected(0, paramAnonymousMenuItem);
    }
  };
  
  public ToolbarActionBar(Toolbar paramToolbar, CharSequence paramCharSequence, Window.Callback paramCallback)
  {
    this.a = new ToolbarWidgetWrapper(paramToolbar, false);
    this.c = new ToolbarCallbackWrapper(paramCallback);
    this.a.a(this.c);
    paramToolbar.setOnMenuItemClickListener(this.i);
    this.a.a(paramCharSequence);
  }
  
  private View a(Menu paramMenu)
  {
    b(paramMenu);
    if ((paramMenu == null) || (this.g == null)) {}
    while (this.g.d().getCount() <= 0) {
      return null;
    }
    return (View)this.g.a(this.a.a());
  }
  
  private void b(Menu paramMenu)
  {
    Object localObject;
    Resources.Theme localTheme;
    if ((this.g == null) && ((paramMenu instanceof MenuBuilder)))
    {
      paramMenu = (MenuBuilder)paramMenu;
      localObject = this.a.b();
      TypedValue localTypedValue = new TypedValue();
      localTheme = ((Context)localObject).getResources().newTheme();
      localTheme.setTo(((Context)localObject).getTheme());
      localTheme.resolveAttribute(R.attr.actionBarPopupTheme, localTypedValue, true);
      if (localTypedValue.resourceId != 0) {
        localTheme.applyStyle(localTypedValue.resourceId, true);
      }
      localTheme.resolveAttribute(R.attr.panelMenuListTheme, localTypedValue, true);
      if (localTypedValue.resourceId == 0) {
        break label170;
      }
      localTheme.applyStyle(localTypedValue.resourceId, true);
    }
    for (;;)
    {
      localObject = new ContextThemeWrapper((Context)localObject, 0);
      ((Context)localObject).getTheme().setTo(localTheme);
      this.g = new ListMenuPresenter((Context)localObject, R.layout.abc_list_menu_item_layout);
      this.g.a(new PanelMenuPresenterCallback(null));
      paramMenu.a(this.g);
      return;
      label170:
      localTheme.applyStyle(R.style.Theme_AppCompat_CompactMenu, true);
    }
  }
  
  private Menu n()
  {
    if (!this.d)
    {
      this.a.a(new ActionMenuPresenterCallback(null), new MenuBuilderCallback(null));
      this.d = true;
    }
    return this.a.s();
  }
  
  public int a()
  {
    return this.a.o();
  }
  
  public void a(float paramFloat)
  {
    ViewCompat.f(this.a.a(), paramFloat);
  }
  
  public void a(int paramInt)
  {
    a(LayoutInflater.from(this.a.b()).inflate(paramInt, this.a.a(), false));
  }
  
  public void a(int paramInt1, int paramInt2)
  {
    int j = this.a.o();
    this.a.c(paramInt1 & paramInt2 | (paramInt2 ^ 0xFFFFFFFF) & j);
  }
  
  public void a(Configuration paramConfiguration)
  {
    super.a(paramConfiguration);
  }
  
  public void a(@Nullable Drawable paramDrawable)
  {
    this.a.c(paramDrawable);
  }
  
  public void a(View paramView)
  {
    a(paramView, new ActionBar.LayoutParams(-2, -2));
  }
  
  public void a(View paramView, ActionBar.LayoutParams paramLayoutParams)
  {
    if (paramView != null) {
      paramView.setLayoutParams(paramLayoutParams);
    }
    this.a.a(paramView);
  }
  
  public void a(CharSequence paramCharSequence)
  {
    this.a.b(paramCharSequence);
  }
  
  public void a(boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (int j = 2;; j = 0)
    {
      a(j, 2);
      return;
    }
  }
  
  public boolean a(int paramInt, KeyEvent paramKeyEvent)
  {
    Menu localMenu = n();
    int j;
    if (localMenu != null)
    {
      if (paramKeyEvent == null) {
        break label56;
      }
      j = paramKeyEvent.getDeviceId();
      if (KeyCharacterMap.load(j).getKeyboardType() == 1) {
        break label61;
      }
    }
    label56:
    label61:
    for (boolean bool = true;; bool = false)
    {
      localMenu.setQwertyMode(bool);
      localMenu.performShortcut(paramInt, paramKeyEvent, 0);
      return true;
      j = -1;
      break;
    }
  }
  
  public int b()
  {
    return this.a.q();
  }
  
  public void b(int paramInt)
  {
    this.a.a(paramInt);
  }
  
  public void b(Drawable paramDrawable)
  {
    this.a.b(paramDrawable);
  }
  
  public void b(CharSequence paramCharSequence)
  {
    this.a.a(paramCharSequence);
  }
  
  public void b(boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (int j = 4;; j = 0)
    {
      a(j, 4);
      return;
    }
  }
  
  public void c()
  {
    this.a.e(0);
  }
  
  public void c(int paramInt)
  {
    this.a.b(paramInt);
  }
  
  public void c(boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (int j = 8;; j = 0)
    {
      a(j, 8);
      return;
    }
  }
  
  public void d()
  {
    this.a.e(8);
  }
  
  public void d(int paramInt)
  {
    DecorToolbar localDecorToolbar = this.a;
    if (paramInt != 0) {}
    for (CharSequence localCharSequence = this.a.b().getText(paramInt);; localCharSequence = null)
    {
      localDecorToolbar.b(localCharSequence);
      return;
    }
  }
  
  public void d(boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (int j = 16;; j = 0)
    {
      a(j, 16);
      return;
    }
  }
  
  public void e(int paramInt)
  {
    this.a.d(paramInt);
  }
  
  public void e(boolean paramBoolean) {}
  
  public boolean e()
  {
    return this.a.r() == 0;
  }
  
  public Context f()
  {
    return this.a.b();
  }
  
  public void g(boolean paramBoolean) {}
  
  public void h(boolean paramBoolean) {}
  
  public boolean h()
  {
    this.a.a().removeCallbacks(this.h);
    ViewCompat.a(this.a.a(), this.h);
    return true;
  }
  
  public void i(boolean paramBoolean)
  {
    if (paramBoolean == this.e) {}
    for (;;)
    {
      return;
      this.e = paramBoolean;
      int k = this.f.size();
      int j = 0;
      while (j < k)
      {
        ((ActionBar.OnMenuVisibilityListener)this.f.get(j)).a(paramBoolean);
        j += 1;
      }
    }
  }
  
  public boolean i()
  {
    if (this.a.c())
    {
      this.a.d();
      return true;
    }
    return false;
  }
  
  public boolean j()
  {
    ViewGroup localViewGroup = this.a.a();
    if ((localViewGroup != null) && (!localViewGroup.hasFocus()))
    {
      localViewGroup.requestFocus();
      return true;
    }
    return false;
  }
  
  void k()
  {
    this.a.a().removeCallbacks(this.h);
  }
  
  public Window.Callback l()
  {
    return this.c;
  }
  
  void m()
  {
    MenuBuilder localMenuBuilder = null;
    Menu localMenu = n();
    if ((localMenu instanceof MenuBuilder)) {
      localMenuBuilder = (MenuBuilder)localMenu;
    }
    if (localMenuBuilder != null) {
      localMenuBuilder.g();
    }
    try
    {
      localMenu.clear();
      if ((!this.c.onCreatePanelMenu(0, localMenu)) || (!this.c.onPreparePanel(0, null, localMenu))) {
        localMenu.clear();
      }
      return;
    }
    finally
    {
      if (localMenuBuilder != null) {
        localMenuBuilder.h();
      }
    }
  }
  
  private final class ActionMenuPresenterCallback
    implements MenuPresenter.Callback
  {
    private boolean b;
    
    private ActionMenuPresenterCallback() {}
    
    public void a(MenuBuilder paramMenuBuilder, boolean paramBoolean)
    {
      if (this.b) {
        return;
      }
      this.b = true;
      ToolbarActionBar.c(ToolbarActionBar.this).n();
      if (ToolbarActionBar.a(ToolbarActionBar.this) != null) {
        ToolbarActionBar.a(ToolbarActionBar.this).onPanelClosed(108, paramMenuBuilder);
      }
      this.b = false;
    }
    
    public boolean a_(MenuBuilder paramMenuBuilder)
    {
      if (ToolbarActionBar.a(ToolbarActionBar.this) != null)
      {
        ToolbarActionBar.a(ToolbarActionBar.this).onMenuOpened(108, paramMenuBuilder);
        return true;
      }
      return false;
    }
  }
  
  private final class MenuBuilderCallback
    implements MenuBuilder.Callback
  {
    private MenuBuilderCallback() {}
    
    public void a(MenuBuilder paramMenuBuilder)
    {
      if (ToolbarActionBar.a(ToolbarActionBar.this) != null)
      {
        if (!ToolbarActionBar.c(ToolbarActionBar.this).i()) {
          break label41;
        }
        ToolbarActionBar.a(ToolbarActionBar.this).onPanelClosed(108, paramMenuBuilder);
      }
      label41:
      while (!ToolbarActionBar.a(ToolbarActionBar.this).onPreparePanel(0, null, paramMenuBuilder)) {
        return;
      }
      ToolbarActionBar.a(ToolbarActionBar.this).onMenuOpened(108, paramMenuBuilder);
    }
    
    public boolean a(MenuBuilder paramMenuBuilder, MenuItem paramMenuItem)
    {
      return false;
    }
  }
  
  private final class PanelMenuPresenterCallback
    implements MenuPresenter.Callback
  {
    private PanelMenuPresenterCallback() {}
    
    public void a(MenuBuilder paramMenuBuilder, boolean paramBoolean)
    {
      if (ToolbarActionBar.a(ToolbarActionBar.this) != null) {
        ToolbarActionBar.a(ToolbarActionBar.this).onPanelClosed(0, paramMenuBuilder);
      }
    }
    
    public boolean a_(MenuBuilder paramMenuBuilder)
    {
      if ((paramMenuBuilder == null) && (ToolbarActionBar.a(ToolbarActionBar.this) != null)) {
        ToolbarActionBar.a(ToolbarActionBar.this).onMenuOpened(0, paramMenuBuilder);
      }
      return true;
    }
  }
  
  private class ToolbarCallbackWrapper
    extends WindowCallbackWrapper
  {
    public ToolbarCallbackWrapper(Window.Callback paramCallback)
    {
      super();
    }
    
    public View onCreatePanelView(int paramInt)
    {
      switch (paramInt)
      {
      }
      Menu localMenu;
      do
      {
        return super.onCreatePanelView(paramInt);
        localMenu = ToolbarActionBar.c(ToolbarActionBar.this).s();
      } while ((!onPreparePanel(paramInt, null, localMenu)) || (!onMenuOpened(paramInt, localMenu)));
      return ToolbarActionBar.a(ToolbarActionBar.this, localMenu);
    }
    
    public boolean onPreparePanel(int paramInt, View paramView, Menu paramMenu)
    {
      boolean bool = super.onPreparePanel(paramInt, paramView, paramMenu);
      if ((bool) && (!ToolbarActionBar.b(ToolbarActionBar.this)))
      {
        ToolbarActionBar.c(ToolbarActionBar.this).m();
        ToolbarActionBar.a(ToolbarActionBar.this, true);
      }
      return bool;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/app/ToolbarActionBar.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */