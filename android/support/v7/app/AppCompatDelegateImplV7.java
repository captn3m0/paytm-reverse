package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.media.AudioManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v4.view.LayoutInflaterFactory;
import android.support.v4.view.OnApplyWindowInsetsListener;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewConfigurationCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.support.v4.view.WindowInsetsCompat;
import android.support.v4.widget.PopupWindowCompat;
import android.support.v7.appcompat.R.attr;
import android.support.v7.appcompat.R.color;
import android.support.v7.appcompat.R.id;
import android.support.v7.appcompat.R.layout;
import android.support.v7.appcompat.R.style;
import android.support.v7.appcompat.R.styleable;
import android.support.v7.view.ActionMode;
import android.support.v7.view.ActionMode.Callback;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.view.StandaloneActionMode;
import android.support.v7.view.menu.ListMenuPresenter;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuBuilder.Callback;
import android.support.v7.view.menu.MenuPresenter.Callback;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.ActionBarContextView;
import android.support.v7.widget.AppCompatDrawableManager;
import android.support.v7.widget.ContentFrameLayout;
import android.support.v7.widget.ContentFrameLayout.OnAttachListener;
import android.support.v7.widget.DecorContentParent;
import android.support.v7.widget.FitWindowsViewGroup;
import android.support.v7.widget.FitWindowsViewGroup.OnFitSystemWindowsListener;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.VectorEnabledTintResources;
import android.support.v7.widget.ViewStubCompat;
import android.support.v7.widget.ViewUtils;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.LayoutInflater.Factory;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.Window;
import android.view.Window.Callback;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.PopupWindow;
import android.widget.TextView;

class AppCompatDelegateImplV7
  extends AppCompatDelegateImplBase
  implements LayoutInflaterFactory, MenuBuilder.Callback
{
  private boolean A;
  private PanelFeatureState[] B;
  private PanelFeatureState C;
  private boolean D;
  private boolean E;
  private int F;
  private final Runnable G = new Runnable()
  {
    public void run()
    {
      if ((AppCompatDelegateImplV7.a(AppCompatDelegateImplV7.this) & 0x1) != 0) {
        AppCompatDelegateImplV7.a(AppCompatDelegateImplV7.this, 0);
      }
      if ((AppCompatDelegateImplV7.a(AppCompatDelegateImplV7.this) & 0x1000) != 0) {
        AppCompatDelegateImplV7.a(AppCompatDelegateImplV7.this, 108);
      }
      AppCompatDelegateImplV7.a(AppCompatDelegateImplV7.this, false);
      AppCompatDelegateImplV7.b(AppCompatDelegateImplV7.this, 0);
    }
  };
  private boolean H;
  private Rect I;
  private Rect J;
  private AppCompatViewInflater K;
  ActionMode m;
  ActionBarContextView n;
  PopupWindow o;
  Runnable p;
  ViewPropertyAnimatorCompat q = null;
  private DecorContentParent r;
  private ActionMenuPresenterCallback s;
  private PanelMenuPresenterCallback t;
  private boolean u;
  private ViewGroup v;
  private TextView w;
  private View x;
  private boolean y;
  private boolean z;
  
  AppCompatDelegateImplV7(Context paramContext, Window paramWindow, AppCompatCallback paramAppCompatCallback)
  {
    super(paramContext, paramWindow, paramAppCompatCallback);
  }
  
  private PanelFeatureState a(int paramInt, boolean paramBoolean)
  {
    Object localObject2 = this.B;
    Object localObject1;
    if (localObject2 != null)
    {
      localObject1 = localObject2;
      if (localObject2.length > paramInt) {}
    }
    else
    {
      arrayOfPanelFeatureState = new PanelFeatureState[paramInt + 1];
      if (localObject2 != null) {
        System.arraycopy(localObject2, 0, arrayOfPanelFeatureState, 0, localObject2.length);
      }
      localObject1 = arrayOfPanelFeatureState;
      this.B = arrayOfPanelFeatureState;
    }
    PanelFeatureState[] arrayOfPanelFeatureState = localObject1[paramInt];
    localObject2 = arrayOfPanelFeatureState;
    if (arrayOfPanelFeatureState == null)
    {
      localObject2 = new PanelFeatureState(paramInt);
      localObject1[paramInt] = localObject2;
    }
    return (PanelFeatureState)localObject2;
  }
  
  private PanelFeatureState a(Menu paramMenu)
  {
    PanelFeatureState[] arrayOfPanelFeatureState = this.B;
    int i;
    int j;
    if (arrayOfPanelFeatureState != null)
    {
      i = arrayOfPanelFeatureState.length;
      j = 0;
    }
    for (;;)
    {
      if (j >= i) {
        break label57;
      }
      PanelFeatureState localPanelFeatureState = arrayOfPanelFeatureState[j];
      if ((localPanelFeatureState != null) && (localPanelFeatureState.j == paramMenu))
      {
        return localPanelFeatureState;
        i = 0;
        break;
      }
      j += 1;
    }
    label57:
    return null;
  }
  
  private void a(int paramInt, PanelFeatureState paramPanelFeatureState, Menu paramMenu)
  {
    Object localObject1 = paramPanelFeatureState;
    Object localObject2 = paramMenu;
    if (paramMenu == null)
    {
      PanelFeatureState localPanelFeatureState = paramPanelFeatureState;
      if (paramPanelFeatureState == null)
      {
        localPanelFeatureState = paramPanelFeatureState;
        if (paramInt >= 0)
        {
          localPanelFeatureState = paramPanelFeatureState;
          if (paramInt < this.B.length) {
            localPanelFeatureState = this.B[paramInt];
          }
        }
      }
      localObject1 = localPanelFeatureState;
      localObject2 = paramMenu;
      if (localPanelFeatureState != null)
      {
        localObject2 = localPanelFeatureState.j;
        localObject1 = localPanelFeatureState;
      }
    }
    if ((localObject1 != null) && (!((PanelFeatureState)localObject1).o)) {}
    while (p()) {
      return;
    }
    this.c.onPanelClosed(paramInt, (Menu)localObject2);
  }
  
  private void a(PanelFeatureState paramPanelFeatureState, KeyEvent paramKeyEvent)
  {
    if ((paramPanelFeatureState.o) || (p())) {}
    Object localObject;
    int i;
    int j;
    label109:
    label114:
    label118:
    label120:
    WindowManager localWindowManager;
    do
    {
      do
      {
        for (;;)
        {
          return;
          if (paramPanelFeatureState.a == 0)
          {
            localObject = this.a;
            if ((((Context)localObject).getResources().getConfiguration().screenLayout & 0xF) != 4) {
              break label109;
            }
            i = 1;
            if (((Context)localObject).getApplicationInfo().targetSdkVersion < 11) {
              break label114;
            }
          }
          for (j = 1;; j = 0)
          {
            if ((i != 0) && (j != 0)) {
              break label118;
            }
            localObject = q();
            if ((localObject == null) || (((Window.Callback)localObject).onMenuOpened(paramPanelFeatureState.a, paramPanelFeatureState.j))) {
              break label120;
            }
            a(paramPanelFeatureState, true);
            return;
            i = 0;
            break;
          }
        }
        localWindowManager = (WindowManager)this.a.getSystemService("window");
      } while ((localWindowManager == null) || (!b(paramPanelFeatureState, paramKeyEvent)));
      j = -2;
      if ((paramPanelFeatureState.g != null) && (!paramPanelFeatureState.q)) {
        break label407;
      }
      if (paramPanelFeatureState.g != null) {
        break;
      }
    } while ((!a(paramPanelFeatureState)) || (paramPanelFeatureState.g == null));
    label188:
    if ((c(paramPanelFeatureState)) && (paramPanelFeatureState.a()))
    {
      localObject = paramPanelFeatureState.h.getLayoutParams();
      paramKeyEvent = (KeyEvent)localObject;
      if (localObject == null) {
        paramKeyEvent = new ViewGroup.LayoutParams(-2, -2);
      }
      i = paramPanelFeatureState.b;
      paramPanelFeatureState.g.setBackgroundResource(i);
      localObject = paramPanelFeatureState.h.getParent();
      if ((localObject != null) && ((localObject instanceof ViewGroup))) {
        ((ViewGroup)localObject).removeView(paramPanelFeatureState.h);
      }
      paramPanelFeatureState.g.addView(paramPanelFeatureState.h, paramKeyEvent);
      i = j;
      if (!paramPanelFeatureState.h.hasFocus())
      {
        paramPanelFeatureState.h.requestFocus();
        i = j;
      }
    }
    for (;;)
    {
      paramPanelFeatureState.n = false;
      paramKeyEvent = new WindowManager.LayoutParams(i, -2, paramPanelFeatureState.d, paramPanelFeatureState.e, 1002, 8519680, -3);
      paramKeyEvent.gravity = paramPanelFeatureState.c;
      paramKeyEvent.windowAnimations = paramPanelFeatureState.f;
      localWindowManager.addView(paramPanelFeatureState.g, paramKeyEvent);
      paramPanelFeatureState.o = true;
      return;
      if ((!paramPanelFeatureState.q) || (paramPanelFeatureState.g.getChildCount() <= 0)) {
        break label188;
      }
      paramPanelFeatureState.g.removeAllViews();
      break label188;
      break;
      label407:
      i = j;
      if (paramPanelFeatureState.i != null)
      {
        paramKeyEvent = paramPanelFeatureState.i.getLayoutParams();
        i = j;
        if (paramKeyEvent != null)
        {
          i = j;
          if (paramKeyEvent.width == -1) {
            i = -1;
          }
        }
      }
    }
  }
  
  private void a(PanelFeatureState paramPanelFeatureState, boolean paramBoolean)
  {
    if ((paramBoolean) && (paramPanelFeatureState.a == 0) && (this.r != null) && (this.r.e())) {
      b(paramPanelFeatureState.j);
    }
    do
    {
      return;
      WindowManager localWindowManager = (WindowManager)this.a.getSystemService("window");
      if ((localWindowManager != null) && (paramPanelFeatureState.o) && (paramPanelFeatureState.g != null))
      {
        localWindowManager.removeView(paramPanelFeatureState.g);
        if (paramBoolean) {
          a(paramPanelFeatureState.a, paramPanelFeatureState, null);
        }
      }
      paramPanelFeatureState.m = false;
      paramPanelFeatureState.n = false;
      paramPanelFeatureState.o = false;
      paramPanelFeatureState.h = null;
      paramPanelFeatureState.q = true;
    } while (this.C != paramPanelFeatureState);
    this.C = null;
  }
  
  private void a(MenuBuilder paramMenuBuilder, boolean paramBoolean)
  {
    if ((this.r != null) && (this.r.d()) && ((!ViewConfigurationCompat.b(ViewConfiguration.get(this.a))) || (this.r.f())))
    {
      paramMenuBuilder = q();
      if ((!this.r.e()) || (!paramBoolean)) {
        if ((paramMenuBuilder != null) && (!p()))
        {
          if ((this.E) && ((this.F & 0x1) != 0))
          {
            this.b.getDecorView().removeCallbacks(this.G);
            this.G.run();
          }
          PanelFeatureState localPanelFeatureState = a(0, true);
          if ((localPanelFeatureState.j != null) && (!localPanelFeatureState.r) && (paramMenuBuilder.onPreparePanel(0, localPanelFeatureState.i, localPanelFeatureState.j)))
          {
            paramMenuBuilder.onMenuOpened(108, localPanelFeatureState.j);
            this.r.g();
          }
        }
      }
      do
      {
        return;
        this.r.h();
      } while (p());
      paramMenuBuilder.onPanelClosed(108, a(0, true).j);
      return;
    }
    paramMenuBuilder = a(0, true);
    paramMenuBuilder.q = true;
    a(paramMenuBuilder, false);
    a(paramMenuBuilder, null);
  }
  
  private boolean a(PanelFeatureState paramPanelFeatureState)
  {
    paramPanelFeatureState.a(n());
    paramPanelFeatureState.g = new ListMenuDecorView(paramPanelFeatureState.l);
    paramPanelFeatureState.c = 81;
    return true;
  }
  
  private boolean a(PanelFeatureState paramPanelFeatureState, int paramInt1, KeyEvent paramKeyEvent, int paramInt2)
  {
    boolean bool2;
    if (paramKeyEvent.isSystem()) {
      bool2 = false;
    }
    boolean bool1;
    do
    {
      do
      {
        do
        {
          return bool2;
          bool2 = false;
          if (!paramPanelFeatureState.m)
          {
            bool1 = bool2;
            if (!b(paramPanelFeatureState, paramKeyEvent)) {}
          }
          else
          {
            bool1 = bool2;
            if (paramPanelFeatureState.j != null) {
              bool1 = paramPanelFeatureState.j.performShortcut(paramInt1, paramKeyEvent, paramInt2);
            }
          }
          bool2 = bool1;
        } while (!bool1);
        bool2 = bool1;
      } while ((paramInt2 & 0x1) != 0);
      bool2 = bool1;
    } while (this.r != null);
    a(paramPanelFeatureState, true);
    return bool1;
  }
  
  private boolean a(ViewParent paramViewParent)
  {
    if (paramViewParent == null) {
      return false;
    }
    View localView = this.b.getDecorView();
    for (;;)
    {
      if (paramViewParent == null) {
        return true;
      }
      if ((paramViewParent == localView) || (!(paramViewParent instanceof View)) || (ViewCompat.H((View)paramViewParent))) {
        return false;
      }
      paramViewParent = paramViewParent.getParent();
    }
  }
  
  private void b(MenuBuilder paramMenuBuilder)
  {
    if (this.A) {
      return;
    }
    this.A = true;
    this.r.i();
    Window.Callback localCallback = q();
    if ((localCallback != null) && (!p())) {
      localCallback.onPanelClosed(108, paramMenuBuilder);
    }
    this.A = false;
  }
  
  private boolean b(PanelFeatureState paramPanelFeatureState)
  {
    Context localContext = this.a;
    Object localObject1;
    TypedValue localTypedValue;
    Resources.Theme localTheme;
    if (paramPanelFeatureState.a != 0)
    {
      localObject1 = localContext;
      if (paramPanelFeatureState.a != 108) {}
    }
    else
    {
      localObject1 = localContext;
      if (this.r != null)
      {
        localTypedValue = new TypedValue();
        localTheme = localContext.getTheme();
        localTheme.resolveAttribute(R.attr.actionBarTheme, localTypedValue, true);
        localObject1 = null;
        if (localTypedValue.resourceId == 0) {
          break label197;
        }
        localObject1 = localContext.getResources().newTheme();
        ((Resources.Theme)localObject1).setTo(localTheme);
        ((Resources.Theme)localObject1).applyStyle(localTypedValue.resourceId, true);
        ((Resources.Theme)localObject1).resolveAttribute(R.attr.actionBarWidgetTheme, localTypedValue, true);
      }
    }
    for (;;)
    {
      Object localObject2 = localObject1;
      if (localTypedValue.resourceId != 0)
      {
        localObject2 = localObject1;
        if (localObject1 == null)
        {
          localObject2 = localContext.getResources().newTheme();
          ((Resources.Theme)localObject2).setTo(localTheme);
        }
        ((Resources.Theme)localObject2).applyStyle(localTypedValue.resourceId, true);
      }
      localObject1 = localContext;
      if (localObject2 != null)
      {
        localObject1 = new ContextThemeWrapper(localContext, 0);
        ((Context)localObject1).getTheme().setTo((Resources.Theme)localObject2);
      }
      localObject1 = new MenuBuilder((Context)localObject1);
      ((MenuBuilder)localObject1).a(this);
      paramPanelFeatureState.a((MenuBuilder)localObject1);
      return true;
      label197:
      localTheme.resolveAttribute(R.attr.actionBarWidgetTheme, localTypedValue, true);
    }
  }
  
  private boolean b(PanelFeatureState paramPanelFeatureState, KeyEvent paramKeyEvent)
  {
    if (p()) {
      return false;
    }
    if (paramPanelFeatureState.m) {
      return true;
    }
    if ((this.C != null) && (this.C != paramPanelFeatureState)) {
      a(this.C, false);
    }
    Window.Callback localCallback = q();
    if (localCallback != null) {
      paramPanelFeatureState.i = localCallback.onCreatePanelView(paramPanelFeatureState.a);
    }
    if ((paramPanelFeatureState.a == 0) || (paramPanelFeatureState.a == 108)) {}
    for (int i = 1;; i = 0)
    {
      if ((i != 0) && (this.r != null)) {
        this.r.setMenuPrepared();
      }
      if ((paramPanelFeatureState.i != null) || ((i != 0) && ((m() instanceof ToolbarActionBar)))) {
        break label408;
      }
      if ((paramPanelFeatureState.j != null) && (!paramPanelFeatureState.r)) {
        break label278;
      }
      if ((paramPanelFeatureState.j == null) && ((!b(paramPanelFeatureState)) || (paramPanelFeatureState.j == null))) {
        break;
      }
      if ((i != 0) && (this.r != null))
      {
        if (this.s == null) {
          this.s = new ActionMenuPresenterCallback(null);
        }
        this.r.setMenu(paramPanelFeatureState.j, this.s);
      }
      paramPanelFeatureState.j.g();
      if (localCallback.onCreatePanelMenu(paramPanelFeatureState.a, paramPanelFeatureState.j)) {
        break label273;
      }
      paramPanelFeatureState.a(null);
      if ((i == 0) || (this.r == null)) {
        break;
      }
      this.r.setMenu(null, this.s);
      return false;
    }
    label273:
    paramPanelFeatureState.r = false;
    label278:
    paramPanelFeatureState.j.g();
    if (paramPanelFeatureState.s != null)
    {
      paramPanelFeatureState.j.d(paramPanelFeatureState.s);
      paramPanelFeatureState.s = null;
    }
    if (!localCallback.onPreparePanel(0, paramPanelFeatureState.i, paramPanelFeatureState.j))
    {
      if ((i != 0) && (this.r != null)) {
        this.r.setMenu(null, this.s);
      }
      paramPanelFeatureState.j.h();
      return false;
    }
    if (paramKeyEvent != null)
    {
      i = paramKeyEvent.getDeviceId();
      if (KeyCharacterMap.load(i).getKeyboardType() == 1) {
        break label430;
      }
    }
    label408:
    label430:
    for (boolean bool = true;; bool = false)
    {
      paramPanelFeatureState.p = bool;
      paramPanelFeatureState.j.setQwertyMode(paramPanelFeatureState.p);
      paramPanelFeatureState.j.h();
      paramPanelFeatureState.m = true;
      paramPanelFeatureState.n = false;
      this.C = paramPanelFeatureState;
      return true;
      i = -1;
      break;
    }
  }
  
  private boolean c(PanelFeatureState paramPanelFeatureState)
  {
    if (paramPanelFeatureState.i != null) {
      paramPanelFeatureState.h = paramPanelFeatureState.i;
    }
    do
    {
      return true;
      if (paramPanelFeatureState.j == null) {
        return false;
      }
      if (this.t == null) {
        this.t = new PanelMenuPresenterCallback(null);
      }
      paramPanelFeatureState.h = ((View)paramPanelFeatureState.a(this.t));
    } while (paramPanelFeatureState.h != null);
    return false;
  }
  
  private void d(int paramInt)
  {
    a(a(paramInt, true), true);
  }
  
  private boolean d(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramKeyEvent.getRepeatCount() == 0)
    {
      PanelFeatureState localPanelFeatureState = a(paramInt, true);
      if (!localPanelFeatureState.o) {
        return b(localPanelFeatureState, paramKeyEvent);
      }
    }
    return false;
  }
  
  private void e(int paramInt)
  {
    this.F |= 1 << paramInt;
    if (!this.E)
    {
      ViewCompat.a(this.b.getDecorView(), this.G);
      this.E = true;
    }
  }
  
  private boolean e(int paramInt, KeyEvent paramKeyEvent)
  {
    boolean bool2;
    if (this.m != null)
    {
      bool2 = false;
      return bool2;
    }
    boolean bool3 = false;
    PanelFeatureState localPanelFeatureState = a(paramInt, true);
    boolean bool1;
    if ((paramInt == 0) && (this.r != null) && (this.r.d()) && (!ViewConfigurationCompat.b(ViewConfiguration.get(this.a)))) {
      if (!this.r.e())
      {
        bool1 = bool3;
        if (!p())
        {
          bool1 = bool3;
          if (b(localPanelFeatureState, paramKeyEvent)) {
            bool1 = this.r.g();
          }
        }
      }
    }
    for (;;)
    {
      bool2 = bool1;
      if (!bool1) {
        break;
      }
      paramKeyEvent = (AudioManager)this.a.getSystemService("audio");
      if (paramKeyEvent == null) {
        break label239;
      }
      paramKeyEvent.playSoundEffect(0);
      return bool1;
      bool1 = this.r.h();
      continue;
      if ((localPanelFeatureState.o) || (localPanelFeatureState.n))
      {
        bool1 = localPanelFeatureState.o;
        a(localPanelFeatureState, true);
      }
      else
      {
        bool1 = bool3;
        if (localPanelFeatureState.m)
        {
          bool2 = true;
          if (localPanelFeatureState.r)
          {
            localPanelFeatureState.m = false;
            bool2 = b(localPanelFeatureState, paramKeyEvent);
          }
          bool1 = bool3;
          if (bool2)
          {
            a(localPanelFeatureState, paramKeyEvent);
            bool1 = true;
          }
        }
      }
    }
    label239:
    Log.w("AppCompatDelegate", "Couldn't get audio manager");
    return bool1;
  }
  
  private void f(int paramInt)
  {
    PanelFeatureState localPanelFeatureState = a(paramInt, true);
    if (localPanelFeatureState.j != null)
    {
      Bundle localBundle = new Bundle();
      localPanelFeatureState.j.c(localBundle);
      if (localBundle.size() > 0) {
        localPanelFeatureState.s = localBundle;
      }
      localPanelFeatureState.j.g();
      localPanelFeatureState.j.clear();
    }
    localPanelFeatureState.r = true;
    localPanelFeatureState.q = true;
    if (((paramInt == 108) || (paramInt == 0)) && (this.r != null))
    {
      localPanelFeatureState = a(0, false);
      if (localPanelFeatureState != null)
      {
        localPanelFeatureState.m = false;
        b(localPanelFeatureState, null);
      }
    }
  }
  
  private int g(int paramInt)
  {
    int i3 = 0;
    int j = 0;
    int i4 = 0;
    int i2 = j;
    int i = paramInt;
    Object localObject1;
    Object localObject2;
    label217:
    label227:
    int k;
    int i1;
    if (this.n != null)
    {
      i2 = j;
      i = paramInt;
      if ((this.n.getLayoutParams() instanceof ViewGroup.MarginLayoutParams))
      {
        localObject1 = (ViewGroup.MarginLayoutParams)this.n.getLayoutParams();
        j = 0;
        i = 0;
        if (!this.n.isShown()) {
          break label373;
        }
        if (this.I == null)
        {
          this.I = new Rect();
          this.J = new Rect();
        }
        localObject2 = this.I;
        Rect localRect = this.J;
        ((Rect)localObject2).set(0, paramInt, 0, 0);
        ViewUtils.a(this.v, (Rect)localObject2, localRect);
        if (localRect.top != 0) {
          break label322;
        }
        j = paramInt;
        if (((ViewGroup.MarginLayoutParams)localObject1).topMargin != j)
        {
          j = 1;
          ((ViewGroup.MarginLayoutParams)localObject1).topMargin = paramInt;
          if (this.x != null) {
            break label327;
          }
          this.x = new View(this.a);
          this.x.setBackgroundColor(this.a.getResources().getColor(R.color.abc_input_method_navigation_guard));
          this.v.addView(this.x, -1, new ViewGroup.LayoutParams(-1, paramInt));
          i = j;
        }
        if (this.x == null) {
          break label367;
        }
        i2 = 1;
        j = i;
        k = i2;
        i1 = paramInt;
        if (!this.j)
        {
          j = i;
          k = i2;
          i1 = paramInt;
          if (i2 != 0)
          {
            i1 = 0;
            k = i2;
            j = i;
          }
        }
        label266:
        i2 = k;
        i = i1;
        if (j != 0)
        {
          this.n.setLayoutParams((ViewGroup.LayoutParams)localObject1);
          i = i1;
          i2 = k;
        }
      }
    }
    if (this.x != null)
    {
      localObject1 = this.x;
      if (i2 == 0) {
        break label406;
      }
    }
    label322:
    label327:
    label367:
    label373:
    label406:
    for (paramInt = i3;; paramInt = 8)
    {
      ((View)localObject1).setVisibility(paramInt);
      return i;
      j = 0;
      break;
      localObject2 = this.x.getLayoutParams();
      i = j;
      if (((ViewGroup.LayoutParams)localObject2).height == paramInt) {
        break label217;
      }
      ((ViewGroup.LayoutParams)localObject2).height = paramInt;
      this.x.setLayoutParams((ViewGroup.LayoutParams)localObject2);
      i = j;
      break label217;
      i2 = 0;
      break label227;
      k = i4;
      i1 = paramInt;
      if (((ViewGroup.MarginLayoutParams)localObject1).topMargin == 0) {
        break label266;
      }
      j = 1;
      ((ViewGroup.MarginLayoutParams)localObject1).topMargin = 0;
      k = i4;
      i1 = paramInt;
      break label266;
    }
  }
  
  private int h(int paramInt)
  {
    int i;
    if (paramInt == 8)
    {
      Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature.");
      i = 108;
    }
    do
    {
      return i;
      i = paramInt;
    } while (paramInt != 9);
    Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature.");
    return 109;
  }
  
  private void t()
  {
    if (!this.u)
    {
      this.v = u();
      Object localObject = r();
      if (!TextUtils.isEmpty((CharSequence)localObject)) {
        b((CharSequence)localObject);
      }
      v();
      a(this.v);
      this.u = true;
      localObject = a(0, false);
      if ((!p()) && ((localObject == null) || (((PanelFeatureState)localObject).j == null))) {
        e(108);
      }
    }
  }
  
  private ViewGroup u()
  {
    Object localObject1 = this.a.obtainStyledAttributes(R.styleable.AppCompatTheme);
    if (!((TypedArray)localObject1).hasValue(R.styleable.AppCompatTheme_windowActionBar))
    {
      ((TypedArray)localObject1).recycle();
      throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
    }
    if (((TypedArray)localObject1).getBoolean(R.styleable.AppCompatTheme_windowNoTitle, false))
    {
      c(1);
      if (((TypedArray)localObject1).getBoolean(R.styleable.AppCompatTheme_windowActionBarOverlay, false)) {
        c(109);
      }
      if (((TypedArray)localObject1).getBoolean(R.styleable.AppCompatTheme_windowActionModeOverlay, false)) {
        c(10);
      }
      this.k = ((TypedArray)localObject1).getBoolean(R.styleable.AppCompatTheme_android_windowIsFloating, false);
      ((TypedArray)localObject1).recycle();
      this.b.getDecorView();
      localObject2 = LayoutInflater.from(this.a);
      localObject1 = null;
      if (this.l) {
        break label436;
      }
      if (!this.k) {
        break label273;
      }
      localObject1 = (ViewGroup)((LayoutInflater)localObject2).inflate(R.layout.abc_dialog_title_material, null);
      this.i = false;
      this.h = false;
    }
    for (;;)
    {
      if (localObject1 != null) {
        break label513;
      }
      throw new IllegalArgumentException("AppCompat does not support the current theme features: { windowActionBar: " + this.h + ", windowActionBarOverlay: " + this.i + ", android:windowIsFloating: " + this.k + ", windowActionModeOverlay: " + this.j + ", windowNoTitle: " + this.l + " }");
      if (!((TypedArray)localObject1).getBoolean(R.styleable.AppCompatTheme_windowActionBar, false)) {
        break;
      }
      c(108);
      break;
      label273:
      if (this.h)
      {
        localObject1 = new TypedValue();
        this.a.getTheme().resolveAttribute(R.attr.actionBarTheme, (TypedValue)localObject1, true);
        if (((TypedValue)localObject1).resourceId != 0) {}
        for (localObject1 = new ContextThemeWrapper(this.a, ((TypedValue)localObject1).resourceId);; localObject1 = this.a)
        {
          localObject2 = (ViewGroup)LayoutInflater.from((Context)localObject1).inflate(R.layout.abc_screen_toolbar, null);
          this.r = ((DecorContentParent)((ViewGroup)localObject2).findViewById(R.id.decor_content_parent));
          this.r.setWindowCallback(q());
          if (this.i) {
            this.r.a(109);
          }
          if (this.y) {
            this.r.a(2);
          }
          localObject1 = localObject2;
          if (!this.z) {
            break;
          }
          this.r.a(5);
          localObject1 = localObject2;
          break;
        }
        label436:
        if (this.j) {}
        for (localObject1 = (ViewGroup)((LayoutInflater)localObject2).inflate(R.layout.abc_screen_simple_overlay_action_mode, null);; localObject1 = (ViewGroup)((LayoutInflater)localObject2).inflate(R.layout.abc_screen_simple, null))
        {
          if (Build.VERSION.SDK_INT < 21) {
            break label493;
          }
          ViewCompat.a((View)localObject1, new OnApplyWindowInsetsListener()
          {
            public WindowInsetsCompat a(View paramAnonymousView, WindowInsetsCompat paramAnonymousWindowInsetsCompat)
            {
              int i = paramAnonymousWindowInsetsCompat.b();
              int j = AppCompatDelegateImplV7.c(AppCompatDelegateImplV7.this, i);
              WindowInsetsCompat localWindowInsetsCompat = paramAnonymousWindowInsetsCompat;
              if (i != j) {
                localWindowInsetsCompat = paramAnonymousWindowInsetsCompat.a(paramAnonymousWindowInsetsCompat.a(), j, paramAnonymousWindowInsetsCompat.c(), paramAnonymousWindowInsetsCompat.d());
              }
              return ViewCompat.a(paramAnonymousView, localWindowInsetsCompat);
            }
          });
          break;
        }
        label493:
        ((FitWindowsViewGroup)localObject1).setOnFitSystemWindowsListener(new FitWindowsViewGroup.OnFitSystemWindowsListener()
        {
          public void a(Rect paramAnonymousRect)
          {
            paramAnonymousRect.top = AppCompatDelegateImplV7.c(AppCompatDelegateImplV7.this, paramAnonymousRect.top);
          }
        });
      }
    }
    label513:
    if (this.r == null) {
      this.w = ((TextView)((ViewGroup)localObject1).findViewById(R.id.title));
    }
    ViewUtils.b((View)localObject1);
    Object localObject2 = (ContentFrameLayout)((ViewGroup)localObject1).findViewById(R.id.action_bar_activity_content);
    ViewGroup localViewGroup = (ViewGroup)this.b.findViewById(16908290);
    if (localViewGroup != null)
    {
      while (localViewGroup.getChildCount() > 0)
      {
        View localView = localViewGroup.getChildAt(0);
        localViewGroup.removeViewAt(0);
        ((ContentFrameLayout)localObject2).addView(localView);
      }
      localViewGroup.setId(-1);
      ((ContentFrameLayout)localObject2).setId(16908290);
      if ((localViewGroup instanceof FrameLayout)) {
        ((FrameLayout)localViewGroup).setForeground(null);
      }
    }
    this.b.setContentView((View)localObject1);
    ((ContentFrameLayout)localObject2).setAttachListener(new ContentFrameLayout.OnAttachListener()
    {
      public void a() {}
      
      public void b()
      {
        AppCompatDelegateImplV7.b(AppCompatDelegateImplV7.this);
      }
    });
    return (ViewGroup)localObject1;
  }
  
  private void v()
  {
    ContentFrameLayout localContentFrameLayout = (ContentFrameLayout)this.v.findViewById(16908290);
    Object localObject = this.b.getDecorView();
    localContentFrameLayout.setDecorPadding(((View)localObject).getPaddingLeft(), ((View)localObject).getPaddingTop(), ((View)localObject).getPaddingRight(), ((View)localObject).getPaddingBottom());
    localObject = this.a.obtainStyledAttributes(R.styleable.AppCompatTheme);
    ((TypedArray)localObject).getValue(R.styleable.AppCompatTheme_windowMinWidthMajor, localContentFrameLayout.getMinWidthMajor());
    ((TypedArray)localObject).getValue(R.styleable.AppCompatTheme_windowMinWidthMinor, localContentFrameLayout.getMinWidthMinor());
    if (((TypedArray)localObject).hasValue(R.styleable.AppCompatTheme_windowFixedWidthMajor)) {
      ((TypedArray)localObject).getValue(R.styleable.AppCompatTheme_windowFixedWidthMajor, localContentFrameLayout.getFixedWidthMajor());
    }
    if (((TypedArray)localObject).hasValue(R.styleable.AppCompatTheme_windowFixedWidthMinor)) {
      ((TypedArray)localObject).getValue(R.styleable.AppCompatTheme_windowFixedWidthMinor, localContentFrameLayout.getFixedWidthMinor());
    }
    if (((TypedArray)localObject).hasValue(R.styleable.AppCompatTheme_windowFixedHeightMajor)) {
      ((TypedArray)localObject).getValue(R.styleable.AppCompatTheme_windowFixedHeightMajor, localContentFrameLayout.getFixedHeightMajor());
    }
    if (((TypedArray)localObject).hasValue(R.styleable.AppCompatTheme_windowFixedHeightMinor)) {
      ((TypedArray)localObject).getValue(R.styleable.AppCompatTheme_windowFixedHeightMinor, localContentFrameLayout.getFixedHeightMinor());
    }
    ((TypedArray)localObject).recycle();
    localContentFrameLayout.requestLayout();
  }
  
  private void w()
  {
    if (this.q != null) {
      this.q.cancel();
    }
  }
  
  private void x()
  {
    if (this.u) {
      throw new AndroidRuntimeException("Window feature must be requested before adding content");
    }
  }
  
  private void y()
  {
    if (this.r != null) {
      this.r.i();
    }
    if (this.o != null)
    {
      this.b.getDecorView().removeCallbacks(this.p);
      if (!this.o.isShowing()) {}
    }
    try
    {
      this.o.dismiss();
      this.o = null;
      w();
      PanelFeatureState localPanelFeatureState = a(0, false);
      if ((localPanelFeatureState != null) && (localPanelFeatureState.j != null)) {
        localPanelFeatureState.j.close();
      }
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      for (;;) {}
    }
  }
  
  public ActionMode a(ActionMode.Callback paramCallback)
  {
    if (paramCallback == null) {
      throw new IllegalArgumentException("ActionMode callback can not be null.");
    }
    if (this.m != null) {
      this.m.c();
    }
    paramCallback = new ActionModeCallbackWrapperV7(paramCallback);
    ActionBar localActionBar = a();
    if (localActionBar != null)
    {
      this.m = localActionBar.a(paramCallback);
      if ((this.m != null) && (this.e != null)) {
        this.e.onSupportActionModeStarted(this.m);
      }
    }
    if (this.m == null) {
      this.m = b(paramCallback);
    }
    return this.m;
  }
  
  @Nullable
  public View a(@IdRes int paramInt)
  {
    t();
    return this.b.findViewById(paramInt);
  }
  
  View a(View paramView, String paramString, Context paramContext, AttributeSet paramAttributeSet)
  {
    if ((this.c instanceof LayoutInflater.Factory))
    {
      paramView = ((LayoutInflater.Factory)this.c).onCreateView(paramString, paramContext, paramAttributeSet);
      if (paramView != null) {
        return paramView;
      }
    }
    return null;
  }
  
  void a(int paramInt, Menu paramMenu)
  {
    if (paramInt == 108)
    {
      paramMenu = a();
      if (paramMenu != null) {
        paramMenu.i(false);
      }
    }
    do
    {
      do
      {
        return;
      } while (paramInt != 0);
      paramMenu = a(paramInt, true);
    } while (!paramMenu.o);
    a(paramMenu, false);
  }
  
  public void a(Configuration paramConfiguration)
  {
    if ((this.h) && (this.u))
    {
      ActionBar localActionBar = a();
      if (localActionBar != null) {
        localActionBar.a(paramConfiguration);
      }
    }
    i();
  }
  
  public void a(Bundle paramBundle)
  {
    if (((this.c instanceof Activity)) && (NavUtils.getParentActivityName((Activity)this.c) != null))
    {
      paramBundle = m();
      if (paramBundle == null) {
        this.H = true;
      }
    }
    else
    {
      return;
    }
    paramBundle.g(true);
  }
  
  public void a(MenuBuilder paramMenuBuilder)
  {
    a(paramMenuBuilder, true);
  }
  
  public void a(Toolbar paramToolbar)
  {
    if (!(this.c instanceof Activity)) {
      return;
    }
    ActionBar localActionBar = a();
    if ((localActionBar instanceof WindowDecorActionBar)) {
      throw new IllegalStateException("This Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_SUPPORT_ACTION_BAR and set windowActionBar to false in your theme to use a Toolbar instead.");
    }
    this.g = null;
    if (localActionBar != null) {
      localActionBar.k();
    }
    if (paramToolbar != null)
    {
      paramToolbar = new ToolbarActionBar(paramToolbar, ((Activity)this.a).getTitle(), this.d);
      this.f = paramToolbar;
      this.b.setCallback(paramToolbar.l());
    }
    for (;;)
    {
      e();
      return;
      this.f = null;
      this.b.setCallback(this.d);
    }
  }
  
  public void a(View paramView)
  {
    t();
    ViewGroup localViewGroup = (ViewGroup)this.v.findViewById(16908290);
    localViewGroup.removeAllViews();
    localViewGroup.addView(paramView);
    this.c.onContentChanged();
  }
  
  public void a(View paramView, ViewGroup.LayoutParams paramLayoutParams)
  {
    t();
    ViewGroup localViewGroup = (ViewGroup)this.v.findViewById(16908290);
    localViewGroup.removeAllViews();
    localViewGroup.addView(paramView, paramLayoutParams);
    this.c.onContentChanged();
  }
  
  void a(ViewGroup paramViewGroup) {}
  
  boolean a(int paramInt, KeyEvent paramKeyEvent)
  {
    Object localObject = a();
    if ((localObject != null) && (((ActionBar)localObject).a(paramInt, paramKeyEvent))) {}
    boolean bool;
    do
    {
      do
      {
        return true;
        if ((this.C == null) || (!a(this.C, paramKeyEvent.getKeyCode(), paramKeyEvent, 1))) {
          break;
        }
      } while (this.C == null);
      this.C.n = true;
      return true;
      if (this.C != null) {
        break;
      }
      localObject = a(0, true);
      b((PanelFeatureState)localObject, paramKeyEvent);
      bool = a((PanelFeatureState)localObject, paramKeyEvent.getKeyCode(), paramKeyEvent, 1);
      ((PanelFeatureState)localObject).m = false;
    } while (bool);
    return false;
  }
  
  public boolean a(MenuBuilder paramMenuBuilder, MenuItem paramMenuItem)
  {
    Window.Callback localCallback = q();
    if ((localCallback != null) && (!p()))
    {
      paramMenuBuilder = a(paramMenuBuilder.p());
      if (paramMenuBuilder != null) {
        return localCallback.onMenuItemSelected(paramMenuBuilder.a, paramMenuItem);
      }
    }
    return false;
  }
  
  boolean a(KeyEvent paramKeyEvent)
  {
    if ((paramKeyEvent.getKeyCode() == 82) && (this.c.dispatchKeyEvent(paramKeyEvent))) {
      return true;
    }
    int j = paramKeyEvent.getKeyCode();
    if (paramKeyEvent.getAction() == 0) {}
    for (int i = 1; i != 0; i = 0) {
      return c(j, paramKeyEvent);
    }
    return b(j, paramKeyEvent);
  }
  
  ActionMode b(ActionMode.Callback paramCallback)
  {
    w();
    if (this.m != null) {
      this.m.c();
    }
    ActionModeCallbackWrapperV7 localActionModeCallbackWrapperV7 = new ActionModeCallbackWrapperV7(paramCallback);
    Object localObject3 = null;
    Object localObject1 = localObject3;
    if (this.e != null)
    {
      localObject1 = localObject3;
      if (p()) {}
    }
    try
    {
      localObject1 = this.e.onWindowStartingSupportActionMode(localActionModeCallbackWrapperV7);
      if (localObject1 != null) {
        this.m = ((ActionMode)localObject1);
      }
      for (;;)
      {
        if ((this.m != null) && (this.e != null)) {
          this.e.onSupportActionModeStarted(this.m);
        }
        return this.m;
        if (this.n == null)
        {
          if (!this.k) {
            break label501;
          }
          localObject3 = new TypedValue();
          localObject1 = this.a.getTheme();
          ((Resources.Theme)localObject1).resolveAttribute(R.attr.actionBarTheme, (TypedValue)localObject3, true);
          if (((TypedValue)localObject3).resourceId != 0)
          {
            Resources.Theme localTheme = this.a.getResources().newTheme();
            localTheme.setTo((Resources.Theme)localObject1);
            localTheme.applyStyle(((TypedValue)localObject3).resourceId, true);
            localObject1 = new ContextThemeWrapper(this.a, 0);
            ((Context)localObject1).getTheme().setTo(localTheme);
            label216:
            this.n = new ActionBarContextView((Context)localObject1);
            this.o = new PopupWindow((Context)localObject1, null, R.attr.actionModePopupWindowStyle);
            PopupWindowCompat.a(this.o, 2);
            this.o.setContentView(this.n);
            this.o.setWidth(-1);
            ((Context)localObject1).getTheme().resolveAttribute(R.attr.actionBarSize, (TypedValue)localObject3, true);
            int i = TypedValue.complexToDimensionPixelSize(((TypedValue)localObject3).data, ((Context)localObject1).getResources().getDisplayMetrics());
            this.n.setContentHeight(i);
            this.o.setHeight(-2);
            this.p = new Runnable()
            {
              public void run()
              {
                AppCompatDelegateImplV7.this.o.showAtLocation(AppCompatDelegateImplV7.this.n, 55, 0, 0);
                AppCompatDelegateImplV7.c(AppCompatDelegateImplV7.this);
                ViewCompat.c(AppCompatDelegateImplV7.this.n, 0.0F);
                AppCompatDelegateImplV7.this.q = ViewCompat.s(AppCompatDelegateImplV7.this.n).a(1.0F);
                AppCompatDelegateImplV7.this.q.a(new ViewPropertyAnimatorListenerAdapter()
                {
                  public void a(View paramAnonymous2View)
                  {
                    AppCompatDelegateImplV7.this.n.setVisibility(0);
                  }
                  
                  public void b(View paramAnonymous2View)
                  {
                    ViewCompat.c(AppCompatDelegateImplV7.this.n, 1.0F);
                    AppCompatDelegateImplV7.this.q.a(null);
                    AppCompatDelegateImplV7.this.q = null;
                  }
                });
              }
            };
          }
        }
        else
        {
          label334:
          if (this.n == null) {
            break label546;
          }
          w();
          this.n.c();
          localObject1 = this.n.getContext();
          localObject3 = this.n;
          if (this.o != null) {
            break label548;
          }
        }
        label501:
        label546:
        label548:
        for (boolean bool = true;; bool = false)
        {
          localObject1 = new StandaloneActionMode((Context)localObject1, (ActionBarContextView)localObject3, localActionModeCallbackWrapperV7, bool);
          if (!paramCallback.a((ActionMode)localObject1, ((ActionMode)localObject1).b())) {
            break label553;
          }
          ((ActionMode)localObject1).d();
          this.n.a((ActionMode)localObject1);
          this.m = ((ActionMode)localObject1);
          ViewCompat.c(this.n, 0.0F);
          this.q = ViewCompat.s(this.n).a(1.0F);
          this.q.a(new ViewPropertyAnimatorListenerAdapter()
          {
            public void a(View paramAnonymousView)
            {
              AppCompatDelegateImplV7.this.n.setVisibility(0);
              AppCompatDelegateImplV7.this.n.sendAccessibilityEvent(32);
              if (AppCompatDelegateImplV7.this.n.getParent() != null) {
                ViewCompat.w((View)AppCompatDelegateImplV7.this.n.getParent());
              }
            }
            
            public void b(View paramAnonymousView)
            {
              ViewCompat.c(AppCompatDelegateImplV7.this.n, 1.0F);
              AppCompatDelegateImplV7.this.q.a(null);
              AppCompatDelegateImplV7.this.q = null;
            }
          });
          if (this.o == null) {
            break;
          }
          this.b.getDecorView().post(this.p);
          break;
          localObject1 = this.a;
          break label216;
          localObject1 = (ViewStubCompat)this.v.findViewById(R.id.action_mode_bar_stub);
          if (localObject1 == null) {
            break label334;
          }
          ((ViewStubCompat)localObject1).setLayoutInflater(LayoutInflater.from(n()));
          this.n = ((ActionBarContextView)((ViewStubCompat)localObject1).a());
          break label334;
          break;
        }
        label553:
        this.m = null;
      }
    }
    catch (AbstractMethodError localAbstractMethodError)
    {
      for (;;)
      {
        Object localObject2 = localObject3;
      }
    }
  }
  
  public View b(View paramView, String paramString, @NonNull Context paramContext, @NonNull AttributeSet paramAttributeSet)
  {
    boolean bool1;
    if (Build.VERSION.SDK_INT < 21)
    {
      bool1 = true;
      if (this.K == null) {
        this.K = new AppCompatViewInflater();
      }
      if ((!bool1) || (!a((ViewParent)paramView))) {
        break label75;
      }
    }
    label75:
    for (boolean bool2 = true;; bool2 = false)
    {
      return this.K.a(paramView, paramString, paramContext, paramAttributeSet, bool2, bool1, true, VectorEnabledTintResources.a());
      bool1 = false;
      break;
    }
  }
  
  public void b(int paramInt)
  {
    t();
    ViewGroup localViewGroup = (ViewGroup)this.v.findViewById(16908290);
    localViewGroup.removeAllViews();
    LayoutInflater.from(this.a).inflate(paramInt, localViewGroup);
    this.c.onContentChanged();
  }
  
  public void b(Bundle paramBundle)
  {
    t();
  }
  
  public void b(View paramView, ViewGroup.LayoutParams paramLayoutParams)
  {
    t();
    ((ViewGroup)this.v.findViewById(16908290)).addView(paramView, paramLayoutParams);
    this.c.onContentChanged();
  }
  
  void b(CharSequence paramCharSequence)
  {
    if (this.r != null) {
      this.r.setWindowTitle(paramCharSequence);
    }
    do
    {
      return;
      if (m() != null)
      {
        m().b(paramCharSequence);
        return;
      }
    } while (this.w == null);
    this.w.setText(paramCharSequence);
  }
  
  boolean b(int paramInt, KeyEvent paramKeyEvent)
  {
    boolean bool1 = true;
    switch (paramInt)
    {
    }
    do
    {
      bool1 = false;
      boolean bool2;
      do
      {
        return bool1;
        e(0, paramKeyEvent);
        return true;
        bool2 = this.D;
        this.D = false;
        paramKeyEvent = a(0, false);
        if ((paramKeyEvent == null) || (!paramKeyEvent.o)) {
          break;
        }
      } while (bool2);
      a(paramKeyEvent, true);
      return true;
    } while (!s());
    return true;
  }
  
  boolean b(int paramInt, Menu paramMenu)
  {
    if (paramInt == 108)
    {
      paramMenu = a();
      if (paramMenu != null) {
        paramMenu.i(true);
      }
      return true;
    }
    return false;
  }
  
  public void c()
  {
    ActionBar localActionBar = a();
    if (localActionBar != null) {
      localActionBar.h(false);
    }
  }
  
  public boolean c(int paramInt)
  {
    paramInt = h(paramInt);
    if ((this.l) && (paramInt == 108)) {
      return false;
    }
    if ((this.h) && (paramInt == 1)) {
      this.h = false;
    }
    switch (paramInt)
    {
    default: 
      return this.b.requestFeature(paramInt);
    case 108: 
      x();
      this.h = true;
      return true;
    case 109: 
      x();
      this.i = true;
      return true;
    case 10: 
      x();
      this.j = true;
      return true;
    case 2: 
      x();
      this.y = true;
      return true;
    case 5: 
      x();
      this.z = true;
      return true;
    }
    x();
    this.l = true;
    return true;
  }
  
  boolean c(int paramInt, KeyEvent paramKeyEvent)
  {
    boolean bool = true;
    switch (paramInt)
    {
    default: 
      if (Build.VERSION.SDK_INT < 11) {
        a(paramInt, paramKeyEvent);
      }
      return false;
    case 82: 
      d(0, paramKeyEvent);
      return true;
    }
    if ((paramKeyEvent.getFlags() & 0x80) != 0) {}
    for (;;)
    {
      this.D = bool;
      break;
      bool = false;
    }
  }
  
  public void d()
  {
    ActionBar localActionBar = a();
    if (localActionBar != null) {
      localActionBar.h(true);
    }
  }
  
  public void e()
  {
    ActionBar localActionBar = a();
    if ((localActionBar != null) && (localActionBar.h())) {
      return;
    }
    e(0);
  }
  
  public void f()
  {
    super.f();
    if (this.f != null) {
      this.f.k();
    }
  }
  
  public void h()
  {
    LayoutInflater localLayoutInflater = LayoutInflater.from(this.a);
    if (localLayoutInflater.getFactory() == null) {
      LayoutInflaterCompat.a(localLayoutInflater, this);
    }
    while ((LayoutInflaterCompat.a(localLayoutInflater) instanceof AppCompatDelegateImplV7)) {
      return;
    }
    Log.i("AppCompatDelegate", "The Activity's LayoutInflater already has a Factory installed so we can not install AppCompat's");
  }
  
  public void l()
  {
    t();
    if ((!this.h) || (this.f != null)) {}
    for (;;)
    {
      return;
      if ((this.c instanceof Activity)) {
        this.f = new WindowDecorActionBar((Activity)this.c, this.i);
      }
      while (this.f != null)
      {
        this.f.g(this.H);
        return;
        if ((this.c instanceof Dialog)) {
          this.f = new WindowDecorActionBar((Dialog)this.c);
        }
      }
    }
  }
  
  public final View onCreateView(View paramView, String paramString, Context paramContext, AttributeSet paramAttributeSet)
  {
    View localView = a(paramView, paramString, paramContext, paramAttributeSet);
    if (localView != null) {
      return localView;
    }
    return b(paramView, paramString, paramContext, paramAttributeSet);
  }
  
  boolean s()
  {
    if (this.m != null) {
      this.m.c();
    }
    ActionBar localActionBar;
    do
    {
      return true;
      localActionBar = a();
    } while ((localActionBar != null) && (localActionBar.i()));
    return false;
  }
  
  private final class ActionMenuPresenterCallback
    implements MenuPresenter.Callback
  {
    private ActionMenuPresenterCallback() {}
    
    public void a(MenuBuilder paramMenuBuilder, boolean paramBoolean)
    {
      AppCompatDelegateImplV7.a(AppCompatDelegateImplV7.this, paramMenuBuilder);
    }
    
    public boolean a_(MenuBuilder paramMenuBuilder)
    {
      Window.Callback localCallback = AppCompatDelegateImplV7.this.q();
      if (localCallback != null) {
        localCallback.onMenuOpened(108, paramMenuBuilder);
      }
      return true;
    }
  }
  
  class ActionModeCallbackWrapperV7
    implements ActionMode.Callback
  {
    private ActionMode.Callback b;
    
    public ActionModeCallbackWrapperV7(ActionMode.Callback paramCallback)
    {
      this.b = paramCallback;
    }
    
    public void a(ActionMode paramActionMode)
    {
      this.b.a(paramActionMode);
      if (AppCompatDelegateImplV7.this.o != null) {
        AppCompatDelegateImplV7.this.b.getDecorView().removeCallbacks(AppCompatDelegateImplV7.this.p);
      }
      if (AppCompatDelegateImplV7.this.n != null)
      {
        AppCompatDelegateImplV7.c(AppCompatDelegateImplV7.this);
        AppCompatDelegateImplV7.this.q = ViewCompat.s(AppCompatDelegateImplV7.this.n).a(0.0F);
        AppCompatDelegateImplV7.this.q.a(new ViewPropertyAnimatorListenerAdapter()
        {
          public void b(View paramAnonymousView)
          {
            AppCompatDelegateImplV7.this.n.setVisibility(8);
            if (AppCompatDelegateImplV7.this.o != null) {
              AppCompatDelegateImplV7.this.o.dismiss();
            }
            for (;;)
            {
              AppCompatDelegateImplV7.this.n.removeAllViews();
              AppCompatDelegateImplV7.this.q.a(null);
              AppCompatDelegateImplV7.this.q = null;
              return;
              if ((AppCompatDelegateImplV7.this.n.getParent() instanceof View)) {
                ViewCompat.w((View)AppCompatDelegateImplV7.this.n.getParent());
              }
            }
          }
        });
      }
      if (AppCompatDelegateImplV7.this.e != null) {
        AppCompatDelegateImplV7.this.e.onSupportActionModeFinished(AppCompatDelegateImplV7.this.m);
      }
      AppCompatDelegateImplV7.this.m = null;
    }
    
    public boolean a(ActionMode paramActionMode, Menu paramMenu)
    {
      return this.b.a(paramActionMode, paramMenu);
    }
    
    public boolean a(ActionMode paramActionMode, MenuItem paramMenuItem)
    {
      return this.b.a(paramActionMode, paramMenuItem);
    }
    
    public boolean b(ActionMode paramActionMode, Menu paramMenu)
    {
      return this.b.b(paramActionMode, paramMenu);
    }
  }
  
  private class ListMenuDecorView
    extends ContentFrameLayout
  {
    public ListMenuDecorView(Context paramContext)
    {
      super();
    }
    
    private boolean a(int paramInt1, int paramInt2)
    {
      return (paramInt1 < -5) || (paramInt2 < -5) || (paramInt1 > getWidth() + 5) || (paramInt2 > getHeight() + 5);
    }
    
    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
      return (AppCompatDelegateImplV7.this.a(paramKeyEvent)) || (super.dispatchKeyEvent(paramKeyEvent));
    }
    
    public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
    {
      if ((paramMotionEvent.getAction() == 0) && (a((int)paramMotionEvent.getX(), (int)paramMotionEvent.getY())))
      {
        AppCompatDelegateImplV7.d(AppCompatDelegateImplV7.this, 0);
        return true;
      }
      return super.onInterceptTouchEvent(paramMotionEvent);
    }
    
    public void setBackgroundResource(int paramInt)
    {
      setBackgroundDrawable(AppCompatDrawableManager.a().a(getContext(), paramInt));
    }
  }
  
  private static final class PanelFeatureState
  {
    int a;
    int b;
    int c;
    int d;
    int e;
    int f;
    ViewGroup g;
    View h;
    View i;
    MenuBuilder j;
    ListMenuPresenter k;
    Context l;
    boolean m;
    boolean n;
    boolean o;
    public boolean p;
    boolean q;
    boolean r;
    Bundle s;
    
    PanelFeatureState(int paramInt)
    {
      this.a = paramInt;
      this.q = false;
    }
    
    MenuView a(MenuPresenter.Callback paramCallback)
    {
      if (this.j == null) {
        return null;
      }
      if (this.k == null)
      {
        this.k = new ListMenuPresenter(this.l, R.layout.abc_list_menu_item_layout);
        this.k.a(paramCallback);
        this.j.a(this.k);
      }
      return this.k.a(this.g);
    }
    
    void a(Context paramContext)
    {
      TypedValue localTypedValue = new TypedValue();
      Resources.Theme localTheme = paramContext.getResources().newTheme();
      localTheme.setTo(paramContext.getTheme());
      localTheme.resolveAttribute(R.attr.actionBarPopupTheme, localTypedValue, true);
      if (localTypedValue.resourceId != 0) {
        localTheme.applyStyle(localTypedValue.resourceId, true);
      }
      localTheme.resolveAttribute(R.attr.panelMenuListTheme, localTypedValue, true);
      if (localTypedValue.resourceId != 0) {
        localTheme.applyStyle(localTypedValue.resourceId, true);
      }
      for (;;)
      {
        paramContext = new ContextThemeWrapper(paramContext, 0);
        paramContext.getTheme().setTo(localTheme);
        this.l = paramContext;
        paramContext = paramContext.obtainStyledAttributes(R.styleable.AppCompatTheme);
        this.b = paramContext.getResourceId(R.styleable.AppCompatTheme_panelBackground, 0);
        this.f = paramContext.getResourceId(R.styleable.AppCompatTheme_android_windowAnimationStyle, 0);
        paramContext.recycle();
        return;
        localTheme.applyStyle(R.style.Theme_AppCompat_CompactMenu, true);
      }
    }
    
    void a(MenuBuilder paramMenuBuilder)
    {
      if (paramMenuBuilder == this.j) {}
      do
      {
        return;
        if (this.j != null) {
          this.j.b(this.k);
        }
        this.j = paramMenuBuilder;
      } while ((paramMenuBuilder == null) || (this.k == null));
      paramMenuBuilder.a(this.k);
    }
    
    public boolean a()
    {
      boolean bool2 = true;
      boolean bool1;
      if (this.h == null) {
        bool1 = false;
      }
      do
      {
        do
        {
          return bool1;
          bool1 = bool2;
        } while (this.i != null);
        bool1 = bool2;
      } while (this.k.d().getCount() > 0);
      return false;
    }
    
    private static class SavedState
      implements Parcelable
    {
      public static final Parcelable.Creator<SavedState> CREATOR = ParcelableCompat.a(new ParcelableCompatCreatorCallbacks()
      {
        public AppCompatDelegateImplV7.PanelFeatureState.SavedState a(Parcel paramAnonymousParcel, ClassLoader paramAnonymousClassLoader)
        {
          return AppCompatDelegateImplV7.PanelFeatureState.SavedState.a(paramAnonymousParcel, paramAnonymousClassLoader);
        }
        
        public AppCompatDelegateImplV7.PanelFeatureState.SavedState[] a(int paramAnonymousInt)
        {
          return new AppCompatDelegateImplV7.PanelFeatureState.SavedState[paramAnonymousInt];
        }
      });
      int a;
      boolean b;
      Bundle c;
      
      private static SavedState b(Parcel paramParcel, ClassLoader paramClassLoader)
      {
        boolean bool = true;
        SavedState localSavedState = new SavedState();
        localSavedState.a = paramParcel.readInt();
        if (paramParcel.readInt() == 1) {}
        for (;;)
        {
          localSavedState.b = bool;
          if (localSavedState.b) {
            localSavedState.c = paramParcel.readBundle(paramClassLoader);
          }
          return localSavedState;
          bool = false;
        }
      }
      
      public int describeContents()
      {
        return 0;
      }
      
      public void writeToParcel(Parcel paramParcel, int paramInt)
      {
        paramParcel.writeInt(this.a);
        if (this.b) {}
        for (paramInt = 1;; paramInt = 0)
        {
          paramParcel.writeInt(paramInt);
          if (this.b) {
            paramParcel.writeBundle(this.c);
          }
          return;
        }
      }
    }
  }
  
  private final class PanelMenuPresenterCallback
    implements MenuPresenter.Callback
  {
    private PanelMenuPresenterCallback() {}
    
    public void a(MenuBuilder paramMenuBuilder, boolean paramBoolean)
    {
      MenuBuilder localMenuBuilder = paramMenuBuilder.p();
      if (localMenuBuilder != paramMenuBuilder) {}
      for (int i = 1;; i = 0)
      {
        AppCompatDelegateImplV7 localAppCompatDelegateImplV7 = AppCompatDelegateImplV7.this;
        if (i != 0) {
          paramMenuBuilder = localMenuBuilder;
        }
        paramMenuBuilder = AppCompatDelegateImplV7.a(localAppCompatDelegateImplV7, paramMenuBuilder);
        if (paramMenuBuilder != null)
        {
          if (i == 0) {
            break;
          }
          AppCompatDelegateImplV7.a(AppCompatDelegateImplV7.this, paramMenuBuilder.a, paramMenuBuilder, localMenuBuilder);
          AppCompatDelegateImplV7.a(AppCompatDelegateImplV7.this, paramMenuBuilder, true);
        }
        return;
      }
      AppCompatDelegateImplV7.a(AppCompatDelegateImplV7.this, paramMenuBuilder, paramBoolean);
    }
    
    public boolean a_(MenuBuilder paramMenuBuilder)
    {
      if ((paramMenuBuilder == null) && (AppCompatDelegateImplV7.this.h))
      {
        Window.Callback localCallback = AppCompatDelegateImplV7.this.q();
        if ((localCallback != null) && (!AppCompatDelegateImplV7.this.p())) {
          localCallback.onMenuOpened(108, paramMenuBuilder);
        }
      }
      return true;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/app/AppCompatDelegateImplV7.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */