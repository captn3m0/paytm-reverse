package android.support.v7.app;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.support.v7.graphics.drawable.DrawerArrowDrawable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

public class ActionBarDrawerToggle
  implements DrawerLayout.DrawerListener
{
  private final Delegate a;
  private final DrawerLayout b;
  private DrawerToggle c;
  private Drawable d;
  private boolean e = true;
  private boolean f;
  private final int g;
  private final int h;
  private View.OnClickListener i;
  private boolean j = false;
  
  public ActionBarDrawerToggle(Activity paramActivity, DrawerLayout paramDrawerLayout, @StringRes int paramInt1, @StringRes int paramInt2)
  {
    this(paramActivity, null, paramDrawerLayout, null, paramInt1, paramInt2);
  }
  
  <T extends Drawable,  extends DrawerToggle> ActionBarDrawerToggle(Activity paramActivity, Toolbar paramToolbar, DrawerLayout paramDrawerLayout, T paramT, @StringRes int paramInt1, @StringRes int paramInt2)
  {
    if (paramToolbar != null)
    {
      this.a = new ToolbarCompatDelegate(paramToolbar);
      paramToolbar.setNavigationOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          if (ActionBarDrawerToggle.a(ActionBarDrawerToggle.this)) {
            ActionBarDrawerToggle.b(ActionBarDrawerToggle.this);
          }
          while (ActionBarDrawerToggle.c(ActionBarDrawerToggle.this) == null) {
            return;
          }
          ActionBarDrawerToggle.c(ActionBarDrawerToggle.this).onClick(paramAnonymousView);
        }
      });
      this.b = paramDrawerLayout;
      this.g = paramInt1;
      this.h = paramInt2;
      if (paramT != null) {
        break label180;
      }
    }
    label180:
    for (this.c = new DrawerArrowDrawableToggle(paramActivity, this.a.b());; this.c = ((DrawerToggle)paramT))
    {
      this.d = c();
      return;
      if ((paramActivity instanceof DelegateProvider))
      {
        this.a = ((DelegateProvider)paramActivity).getDrawerToggleDelegate();
        break;
      }
      if (Build.VERSION.SDK_INT >= 18)
      {
        this.a = new JellybeanMr2Delegate(paramActivity, null);
        break;
      }
      if (Build.VERSION.SDK_INT >= 11)
      {
        this.a = new HoneycombDelegate(paramActivity, null);
        break;
      }
      this.a = new DummyDelegate(paramActivity);
      break;
    }
  }
  
  private void d()
  {
    int k = this.b.a(8388611);
    if ((this.b.h(8388611)) && (k != 2)) {
      this.b.f(8388611);
    }
    while (k == 1) {
      return;
    }
    this.b.e(8388611);
  }
  
  public void a()
  {
    Drawable localDrawable;
    if (this.b.g(8388611))
    {
      this.c.a(1.0F);
      if (this.e)
      {
        localDrawable = (Drawable)this.c;
        if (!this.b.g(8388611)) {
          break label74;
        }
      }
    }
    label74:
    for (int k = this.h;; k = this.g)
    {
      a(localDrawable, k);
      return;
      this.c.a(0.0F);
      break;
    }
  }
  
  public void a(int paramInt)
  {
    Drawable localDrawable = null;
    if (paramInt != 0) {
      localDrawable = this.b.getResources().getDrawable(paramInt);
    }
    a(localDrawable);
  }
  
  public void a(Configuration paramConfiguration)
  {
    if (!this.f) {
      this.d = c();
    }
    a();
  }
  
  public void a(Drawable paramDrawable)
  {
    if (paramDrawable == null) {
      this.d = c();
    }
    for (this.f = false;; this.f = true)
    {
      if (!this.e) {
        a(this.d, 0);
      }
      return;
      this.d = paramDrawable;
    }
  }
  
  void a(Drawable paramDrawable, int paramInt)
  {
    if ((!this.j) && (!this.a.c()))
    {
      Log.w("ActionBarDrawerToggle", "DrawerToggle may not show up because NavigationIcon is not visible. You may need to call actionbar.setDisplayHomeAsUpEnabled(true);");
      this.j = true;
    }
    this.a.a(paramDrawable, paramInt);
  }
  
  public void a(boolean paramBoolean)
  {
    int k;
    if (paramBoolean != this.e)
    {
      if (!paramBoolean) {
        break label57;
      }
      Drawable localDrawable = (Drawable)this.c;
      if (!this.b.g(8388611)) {
        break label49;
      }
      k = this.h;
      a(localDrawable, k);
    }
    for (;;)
    {
      this.e = paramBoolean;
      return;
      label49:
      k = this.g;
      break;
      label57:
      a(this.d, 0);
    }
  }
  
  public boolean a(MenuItem paramMenuItem)
  {
    if ((paramMenuItem != null) && (paramMenuItem.getItemId() == 16908332) && (this.e))
    {
      d();
      return true;
    }
    return false;
  }
  
  void b(int paramInt)
  {
    this.a.a(paramInt);
  }
  
  public boolean b()
  {
    return this.e;
  }
  
  Drawable c()
  {
    return this.a.a();
  }
  
  public void onDrawerClosed(View paramView)
  {
    this.c.a(0.0F);
    if (this.e) {
      b(this.g);
    }
  }
  
  public void onDrawerOpened(View paramView)
  {
    this.c.a(1.0F);
    if (this.e) {
      b(this.h);
    }
  }
  
  public void onDrawerSlide(View paramView, float paramFloat)
  {
    this.c.a(Math.min(1.0F, Math.max(0.0F, paramFloat)));
  }
  
  public void onDrawerStateChanged(int paramInt) {}
  
  public static abstract interface Delegate
  {
    public abstract Drawable a();
    
    public abstract void a(@StringRes int paramInt);
    
    public abstract void a(Drawable paramDrawable, @StringRes int paramInt);
    
    public abstract Context b();
    
    public abstract boolean c();
  }
  
  public static abstract interface DelegateProvider
  {
    @Nullable
    public abstract ActionBarDrawerToggle.Delegate getDrawerToggleDelegate();
  }
  
  static class DrawerArrowDrawableToggle
    extends DrawerArrowDrawable
    implements ActionBarDrawerToggle.DrawerToggle
  {
    private final Activity a;
    
    public DrawerArrowDrawableToggle(Activity paramActivity, Context paramContext)
    {
      super();
      this.a = paramActivity;
    }
    
    public void a(float paramFloat)
    {
      if (paramFloat == 1.0F) {
        b(true);
      }
      for (;;)
      {
        d(paramFloat);
        return;
        if (paramFloat == 0.0F) {
          b(false);
        }
      }
    }
  }
  
  static abstract interface DrawerToggle
  {
    public abstract void a(float paramFloat);
  }
  
  static class DummyDelegate
    implements ActionBarDrawerToggle.Delegate
  {
    final Activity a;
    
    DummyDelegate(Activity paramActivity)
    {
      this.a = paramActivity;
    }
    
    public Drawable a()
    {
      return null;
    }
    
    public void a(@StringRes int paramInt) {}
    
    public void a(Drawable paramDrawable, @StringRes int paramInt) {}
    
    public Context b()
    {
      return this.a;
    }
    
    public boolean c()
    {
      return true;
    }
  }
  
  private static class HoneycombDelegate
    implements ActionBarDrawerToggle.Delegate
  {
    final Activity a;
    ActionBarDrawerToggleHoneycomb.SetIndicatorInfo b;
    
    private HoneycombDelegate(Activity paramActivity)
    {
      this.a = paramActivity;
    }
    
    public Drawable a()
    {
      return ActionBarDrawerToggleHoneycomb.a(this.a);
    }
    
    public void a(int paramInt)
    {
      this.b = ActionBarDrawerToggleHoneycomb.a(this.b, this.a, paramInt);
    }
    
    public void a(Drawable paramDrawable, int paramInt)
    {
      this.a.getActionBar().setDisplayShowHomeEnabled(true);
      this.b = ActionBarDrawerToggleHoneycomb.a(this.b, this.a, paramDrawable, paramInt);
      this.a.getActionBar().setDisplayShowHomeEnabled(false);
    }
    
    public Context b()
    {
      ActionBar localActionBar = this.a.getActionBar();
      if (localActionBar != null) {
        return localActionBar.getThemedContext();
      }
      return this.a;
    }
    
    public boolean c()
    {
      ActionBar localActionBar = this.a.getActionBar();
      return (localActionBar != null) && ((localActionBar.getDisplayOptions() & 0x4) != 0);
    }
  }
  
  private static class JellybeanMr2Delegate
    implements ActionBarDrawerToggle.Delegate
  {
    final Activity a;
    
    private JellybeanMr2Delegate(Activity paramActivity)
    {
      this.a = paramActivity;
    }
    
    public Drawable a()
    {
      TypedArray localTypedArray = b().obtainStyledAttributes(null, new int[] { 16843531 }, 16843470, 0);
      Drawable localDrawable = localTypedArray.getDrawable(0);
      localTypedArray.recycle();
      return localDrawable;
    }
    
    public void a(int paramInt)
    {
      ActionBar localActionBar = this.a.getActionBar();
      if (localActionBar != null) {
        localActionBar.setHomeActionContentDescription(paramInt);
      }
    }
    
    public void a(Drawable paramDrawable, int paramInt)
    {
      ActionBar localActionBar = this.a.getActionBar();
      if (localActionBar != null)
      {
        localActionBar.setHomeAsUpIndicator(paramDrawable);
        localActionBar.setHomeActionContentDescription(paramInt);
      }
    }
    
    public Context b()
    {
      ActionBar localActionBar = this.a.getActionBar();
      if (localActionBar != null) {
        return localActionBar.getThemedContext();
      }
      return this.a;
    }
    
    public boolean c()
    {
      ActionBar localActionBar = this.a.getActionBar();
      return (localActionBar != null) && ((localActionBar.getDisplayOptions() & 0x4) != 0);
    }
  }
  
  static class ToolbarCompatDelegate
    implements ActionBarDrawerToggle.Delegate
  {
    final Toolbar a;
    final Drawable b;
    final CharSequence c;
    
    ToolbarCompatDelegate(Toolbar paramToolbar)
    {
      this.a = paramToolbar;
      this.b = paramToolbar.getNavigationIcon();
      this.c = paramToolbar.getNavigationContentDescription();
    }
    
    public Drawable a()
    {
      return this.b;
    }
    
    public void a(@StringRes int paramInt)
    {
      if (paramInt == 0)
      {
        this.a.setNavigationContentDescription(this.c);
        return;
      }
      this.a.setNavigationContentDescription(paramInt);
    }
    
    public void a(Drawable paramDrawable, @StringRes int paramInt)
    {
      this.a.setNavigationIcon(paramDrawable);
      a(paramInt);
    }
    
    public Context b()
    {
      return this.a.getContext();
    }
    
    public boolean c()
    {
      return true;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/app/ActionBarDrawerToggle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */