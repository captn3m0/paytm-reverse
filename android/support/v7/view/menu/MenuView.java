package android.support.v7.view.menu;

public abstract interface MenuView
{
  public abstract void a(MenuBuilder paramMenuBuilder);
  
  public static abstract interface ItemView
  {
    public abstract void a(MenuItemImpl paramMenuItemImpl, int paramInt);
    
    public abstract boolean b();
    
    public abstract MenuItemImpl getItemData();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/view/menu/MenuView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */