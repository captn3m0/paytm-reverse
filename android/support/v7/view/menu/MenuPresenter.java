package android.support.v7.view.menu;

import android.content.Context;
import android.os.Parcelable;

public abstract interface MenuPresenter
{
  public abstract void a(Context paramContext, MenuBuilder paramMenuBuilder);
  
  public abstract void a(Parcelable paramParcelable);
  
  public abstract void a(MenuBuilder paramMenuBuilder, boolean paramBoolean);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract boolean a();
  
  public abstract boolean a(MenuBuilder paramMenuBuilder, MenuItemImpl paramMenuItemImpl);
  
  public abstract boolean a(SubMenuBuilder paramSubMenuBuilder);
  
  public abstract int b();
  
  public abstract boolean b(MenuBuilder paramMenuBuilder, MenuItemImpl paramMenuItemImpl);
  
  public abstract Parcelable c();
  
  public static abstract interface Callback
  {
    public abstract void a(MenuBuilder paramMenuBuilder, boolean paramBoolean);
    
    public abstract boolean a_(MenuBuilder paramMenuBuilder);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/view/menu/MenuPresenter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */