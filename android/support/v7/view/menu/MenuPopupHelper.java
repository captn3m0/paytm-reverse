package android.support.v7.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcelable;
import android.support.v7.appcompat.R.attr;
import android.support.v7.appcompat.R.dimen;
import android.support.v7.appcompat.R.layout;
import android.support.v7.widget.ListPopupWindow;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow.OnDismissListener;
import java.util.ArrayList;

public class MenuPopupHelper
  implements MenuPresenter, View.OnKeyListener, ViewTreeObserver.OnGlobalLayoutListener, AdapterView.OnItemClickListener, PopupWindow.OnDismissListener
{
  static final int a = R.layout.abc_popup_menu_item_layout;
  boolean b;
  private final Context c;
  private final LayoutInflater d;
  private final MenuBuilder e;
  private final MenuAdapter f;
  private final boolean g;
  private final int h;
  private final int i;
  private final int j;
  private View k;
  private ListPopupWindow l;
  private ViewTreeObserver m;
  private MenuPresenter.Callback n;
  private ViewGroup o;
  private boolean p;
  private int q;
  private int r = 0;
  
  public MenuPopupHelper(Context paramContext, MenuBuilder paramMenuBuilder, View paramView)
  {
    this(paramContext, paramMenuBuilder, paramView, false, R.attr.popupMenuStyle);
  }
  
  public MenuPopupHelper(Context paramContext, MenuBuilder paramMenuBuilder, View paramView, boolean paramBoolean, int paramInt)
  {
    this(paramContext, paramMenuBuilder, paramView, paramBoolean, paramInt, 0);
  }
  
  public MenuPopupHelper(Context paramContext, MenuBuilder paramMenuBuilder, View paramView, boolean paramBoolean, int paramInt1, int paramInt2)
  {
    this.c = paramContext;
    this.d = LayoutInflater.from(paramContext);
    this.e = paramMenuBuilder;
    this.f = new MenuAdapter(this.e);
    this.g = paramBoolean;
    this.i = paramInt1;
    this.j = paramInt2;
    Resources localResources = paramContext.getResources();
    this.h = Math.max(localResources.getDisplayMetrics().widthPixels / 2, localResources.getDimensionPixelSize(R.dimen.abc_config_prefDialogWidth));
    this.k = paramView;
    paramMenuBuilder.a(this, paramContext);
  }
  
  private int i()
  {
    int i1 = 0;
    View localView = null;
    int i4 = 0;
    MenuAdapter localMenuAdapter = this.f;
    int i6 = View.MeasureSpec.makeMeasureSpec(0, 0);
    int i7 = View.MeasureSpec.makeMeasureSpec(0, 0);
    int i8 = localMenuAdapter.getCount();
    int i2 = 0;
    for (;;)
    {
      int i3 = i1;
      if (i2 < i8)
      {
        i5 = localMenuAdapter.getItemViewType(i2);
        i3 = i4;
        if (i5 != i4)
        {
          i3 = i5;
          localView = null;
        }
        if (this.o == null) {
          this.o = new FrameLayout(this.c);
        }
        localView = localMenuAdapter.getView(i2, localView, this.o);
        localView.measure(i6, i7);
        i4 = localView.getMeasuredWidth();
        if (i4 >= this.h) {
          i3 = this.h;
        }
      }
      else
      {
        return i3;
      }
      int i5 = i1;
      if (i4 > i1) {
        i5 = i4;
      }
      i2 += 1;
      i4 = i3;
      i1 = i5;
    }
  }
  
  public void a(int paramInt)
  {
    this.r = paramInt;
  }
  
  public void a(Context paramContext, MenuBuilder paramMenuBuilder) {}
  
  public void a(Parcelable paramParcelable) {}
  
  public void a(MenuBuilder paramMenuBuilder, boolean paramBoolean)
  {
    if (paramMenuBuilder != this.e) {}
    do
    {
      return;
      g();
    } while (this.n == null);
    this.n.a(paramMenuBuilder, paramBoolean);
  }
  
  public void a(MenuPresenter.Callback paramCallback)
  {
    this.n = paramCallback;
  }
  
  public void a(View paramView)
  {
    this.k = paramView;
  }
  
  public void a(boolean paramBoolean)
  {
    this.p = false;
    if (this.f != null) {
      this.f.notifyDataSetChanged();
    }
  }
  
  public boolean a()
  {
    return false;
  }
  
  public boolean a(MenuBuilder paramMenuBuilder, MenuItemImpl paramMenuItemImpl)
  {
    return false;
  }
  
  public boolean a(SubMenuBuilder paramSubMenuBuilder)
  {
    if (paramSubMenuBuilder.hasVisibleItems())
    {
      MenuPopupHelper localMenuPopupHelper = new MenuPopupHelper(this.c, paramSubMenuBuilder, this.k);
      localMenuPopupHelper.a(this.n);
      boolean bool2 = false;
      int i2 = paramSubMenuBuilder.size();
      int i1 = 0;
      for (;;)
      {
        boolean bool1 = bool2;
        if (i1 < i2)
        {
          MenuItem localMenuItem = paramSubMenuBuilder.getItem(i1);
          if ((localMenuItem.isVisible()) && (localMenuItem.getIcon() != null)) {
            bool1 = true;
          }
        }
        else
        {
          localMenuPopupHelper.b(bool1);
          if (!localMenuPopupHelper.f()) {
            break;
          }
          if (this.n != null) {
            this.n.a_(paramSubMenuBuilder);
          }
          return true;
        }
        i1 += 1;
      }
    }
    return false;
  }
  
  public int b()
  {
    return 0;
  }
  
  public void b(boolean paramBoolean)
  {
    this.b = paramBoolean;
  }
  
  public boolean b(MenuBuilder paramMenuBuilder, MenuItemImpl paramMenuItemImpl)
  {
    return false;
  }
  
  public Parcelable c()
  {
    return null;
  }
  
  public void d()
  {
    if (!f()) {
      throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
    }
  }
  
  public ListPopupWindow e()
  {
    return this.l;
  }
  
  public boolean f()
  {
    int i1 = 0;
    this.l = new ListPopupWindow(this.c, null, this.i, this.j);
    this.l.a(this);
    this.l.a(this);
    this.l.a(this.f);
    this.l.a(true);
    View localView = this.k;
    if (localView != null)
    {
      if (this.m == null) {
        i1 = 1;
      }
      this.m = localView.getViewTreeObserver();
      if (i1 != 0) {
        this.m.addOnGlobalLayoutListener(this);
      }
      this.l.a(localView);
      this.l.d(this.r);
      if (!this.p)
      {
        this.q = i();
        this.p = true;
      }
      this.l.f(this.q);
      this.l.h(2);
      this.l.c();
      this.l.m().setOnKeyListener(this);
      return true;
    }
    return false;
  }
  
  public void g()
  {
    if (h()) {
      this.l.i();
    }
  }
  
  public boolean h()
  {
    return (this.l != null) && (this.l.k());
  }
  
  public void onDismiss()
  {
    this.l = null;
    this.e.close();
    if (this.m != null)
    {
      if (!this.m.isAlive()) {
        this.m = this.k.getViewTreeObserver();
      }
      this.m.removeGlobalOnLayoutListener(this);
      this.m = null;
    }
  }
  
  public void onGlobalLayout()
  {
    if (h())
    {
      View localView = this.k;
      if ((localView != null) && (localView.isShown())) {
        break label28;
      }
      g();
    }
    label28:
    while (!h()) {
      return;
    }
    this.l.c();
  }
  
  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    paramAdapterView = this.f;
    MenuAdapter.a(paramAdapterView).a(paramAdapterView.a(paramInt), 0);
  }
  
  public boolean onKey(View paramView, int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramKeyEvent.getAction() == 1) && (paramInt == 82))
    {
      g();
      return true;
    }
    return false;
  }
  
  private class MenuAdapter
    extends BaseAdapter
  {
    private MenuBuilder b;
    private int c = -1;
    
    public MenuAdapter(MenuBuilder paramMenuBuilder)
    {
      this.b = paramMenuBuilder;
      a();
    }
    
    public MenuItemImpl a(int paramInt)
    {
      if (MenuPopupHelper.a(MenuPopupHelper.this)) {}
      for (ArrayList localArrayList = this.b.l();; localArrayList = this.b.i())
      {
        int i = paramInt;
        if (this.c >= 0)
        {
          i = paramInt;
          if (paramInt >= this.c) {
            i = paramInt + 1;
          }
        }
        return (MenuItemImpl)localArrayList.get(i);
      }
    }
    
    void a()
    {
      MenuItemImpl localMenuItemImpl = MenuPopupHelper.c(MenuPopupHelper.this).r();
      if (localMenuItemImpl != null)
      {
        ArrayList localArrayList = MenuPopupHelper.c(MenuPopupHelper.this).l();
        int j = localArrayList.size();
        int i = 0;
        while (i < j)
        {
          if ((MenuItemImpl)localArrayList.get(i) == localMenuItemImpl)
          {
            this.c = i;
            return;
          }
          i += 1;
        }
      }
      this.c = -1;
    }
    
    public int getCount()
    {
      if (MenuPopupHelper.a(MenuPopupHelper.this)) {}
      for (ArrayList localArrayList = this.b.l(); this.c < 0; localArrayList = this.b.i()) {
        return localArrayList.size();
      }
      return localArrayList.size() - 1;
    }
    
    public long getItemId(int paramInt)
    {
      return paramInt;
    }
    
    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      View localView = paramView;
      if (paramView == null) {
        localView = MenuPopupHelper.b(MenuPopupHelper.this).inflate(MenuPopupHelper.a, paramViewGroup, false);
      }
      paramView = (MenuView.ItemView)localView;
      if (MenuPopupHelper.this.b) {
        ((ListMenuItemView)localView).setForceShowIcon(true);
      }
      paramView.a(a(paramInt), 0);
      return localView;
    }
    
    public void notifyDataSetChanged()
    {
      a();
      super.notifyDataSetChanged();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/view/menu/MenuPopupHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */