package android.support.v7.view;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.TypedArray;
import android.support.v4.view.ActionProvider;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.appcompat.R.styleable;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.MenuItemWrapperICS;
import android.util.AttributeSet;
import android.util.Log;
import android.view.InflateException;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.SubMenu;
import android.view.View;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class SupportMenuInflater
  extends MenuInflater
{
  private static final Class<?>[] a = { Context.class };
  private static final Class<?>[] b = a;
  private final Object[] c;
  private final Object[] d;
  private Context e;
  private Object f;
  
  public SupportMenuInflater(Context paramContext)
  {
    super(paramContext);
    this.e = paramContext;
    this.c = new Object[] { paramContext };
    this.d = this.c;
  }
  
  private Object a(Object paramObject)
  {
    if ((paramObject instanceof Activity)) {}
    while (!(paramObject instanceof ContextWrapper)) {
      return paramObject;
    }
    return a(((ContextWrapper)paramObject).getBaseContext());
  }
  
  private void a(XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Menu paramMenu)
    throws XmlPullParserException, IOException
  {
    MenuState localMenuState = new MenuState(paramMenu);
    int i = paramXmlPullParser.getEventType();
    int k = 0;
    Menu localMenu = null;
    label55:
    int j;
    int n;
    if (i == 2)
    {
      paramMenu = paramXmlPullParser.getName();
      if (paramMenu.equals("menu"))
      {
        i = paramXmlPullParser.next();
        j = 0;
        n = i;
        label62:
        if (j != 0) {
          return;
        }
      }
    }
    int m;
    switch (n)
    {
    default: 
      paramMenu = localMenu;
      m = j;
      i = k;
    case 2: 
    case 3: 
      for (;;)
      {
        n = paramXmlPullParser.next();
        k = i;
        j = m;
        localMenu = paramMenu;
        break label62;
        throw new RuntimeException("Expecting menu, got " + paramMenu);
        j = paramXmlPullParser.next();
        i = j;
        if (j != 1) {
          break;
        }
        i = j;
        break label55;
        i = k;
        m = j;
        paramMenu = localMenu;
        if (k == 0)
        {
          paramMenu = paramXmlPullParser.getName();
          if (paramMenu.equals("group"))
          {
            localMenuState.a(paramAttributeSet);
            i = k;
            m = j;
            paramMenu = localMenu;
          }
          else if (paramMenu.equals("item"))
          {
            localMenuState.b(paramAttributeSet);
            i = k;
            m = j;
            paramMenu = localMenu;
          }
          else if (paramMenu.equals("menu"))
          {
            a(paramXmlPullParser, paramAttributeSet, localMenuState.c());
            i = k;
            m = j;
            paramMenu = localMenu;
          }
          else
          {
            i = 1;
            m = j;
            continue;
            String str = paramXmlPullParser.getName();
            if ((k != 0) && (str.equals(localMenu)))
            {
              i = 0;
              paramMenu = null;
              m = j;
            }
            else if (str.equals("group"))
            {
              localMenuState.a();
              i = k;
              m = j;
              paramMenu = localMenu;
            }
            else if (str.equals("item"))
            {
              i = k;
              m = j;
              paramMenu = localMenu;
              if (!localMenuState.d()) {
                if ((MenuState.a(localMenuState) != null) && (MenuState.a(localMenuState).g()))
                {
                  localMenuState.c();
                  i = k;
                  m = j;
                  paramMenu = localMenu;
                }
                else
                {
                  localMenuState.b();
                  i = k;
                  m = j;
                  paramMenu = localMenu;
                }
              }
            }
            else
            {
              i = k;
              m = j;
              paramMenu = localMenu;
              if (str.equals("menu"))
              {
                m = 1;
                i = k;
                paramMenu = localMenu;
              }
            }
          }
        }
      }
    }
    throw new RuntimeException("Unexpected end of document");
  }
  
  private Object c()
  {
    if (this.f == null) {
      this.f = a(this.e);
    }
    return this.f;
  }
  
  /* Error */
  public void inflate(int paramInt, Menu paramMenu)
  {
    // Byte code:
    //   0: aload_2
    //   1: instanceof 147
    //   4: ifne +10 -> 14
    //   7: aload_0
    //   8: iload_1
    //   9: aload_2
    //   10: invokespecial 149	android/view/MenuInflater:inflate	(ILandroid/view/Menu;)V
    //   13: return
    //   14: aconst_null
    //   15: astore_3
    //   16: aconst_null
    //   17: astore 5
    //   19: aconst_null
    //   20: astore 4
    //   22: aload_0
    //   23: getfield 38	android/support/v7/view/SupportMenuInflater:e	Landroid/content/Context;
    //   26: invokevirtual 153	android/content/Context:getResources	()Landroid/content/res/Resources;
    //   29: iload_1
    //   30: invokevirtual 159	android/content/res/Resources:getLayout	(I)Landroid/content/res/XmlResourceParser;
    //   33: astore 6
    //   35: aload 6
    //   37: astore 4
    //   39: aload 6
    //   41: astore_3
    //   42: aload 6
    //   44: astore 5
    //   46: aload_0
    //   47: aload 6
    //   49: aload 6
    //   51: invokestatic 165	android/util/Xml:asAttributeSet	(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
    //   54: aload_2
    //   55: invokespecial 117	android/support/v7/view/SupportMenuInflater:a	(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/view/Menu;)V
    //   58: aload 6
    //   60: ifnull -47 -> 13
    //   63: aload 6
    //   65: invokeinterface 170 1 0
    //   70: return
    //   71: astore_2
    //   72: aload 4
    //   74: astore_3
    //   75: new 172	android/view/InflateException
    //   78: dup
    //   79: ldc -82
    //   81: aload_2
    //   82: invokespecial 177	android/view/InflateException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   85: athrow
    //   86: astore_2
    //   87: aload_3
    //   88: ifnull +9 -> 97
    //   91: aload_3
    //   92: invokeinterface 170 1 0
    //   97: aload_2
    //   98: athrow
    //   99: astore_2
    //   100: aload 5
    //   102: astore_3
    //   103: new 172	android/view/InflateException
    //   106: dup
    //   107: ldc -82
    //   109: aload_2
    //   110: invokespecial 177	android/view/InflateException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   113: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	114	0	this	SupportMenuInflater
    //   0	114	1	paramInt	int
    //   0	114	2	paramMenu	Menu
    //   15	88	3	localObject1	Object
    //   20	53	4	localObject2	Object
    //   17	84	5	localObject3	Object
    //   33	31	6	localXmlResourceParser	android.content.res.XmlResourceParser
    // Exception table:
    //   from	to	target	type
    //   22	35	71	org/xmlpull/v1/XmlPullParserException
    //   46	58	71	org/xmlpull/v1/XmlPullParserException
    //   22	35	86	finally
    //   46	58	86	finally
    //   75	86	86	finally
    //   103	114	86	finally
    //   22	35	99	java/io/IOException
    //   46	58	99	java/io/IOException
  }
  
  private static class InflatedOnMenuItemClickListener
    implements MenuItem.OnMenuItemClickListener
  {
    private static final Class<?>[] a = { MenuItem.class };
    private Object b;
    private Method c;
    
    public InflatedOnMenuItemClickListener(Object paramObject, String paramString)
    {
      this.b = paramObject;
      Class localClass = paramObject.getClass();
      try
      {
        this.c = localClass.getMethod(paramString, a);
        return;
      }
      catch (Exception paramObject)
      {
        paramString = new InflateException("Couldn't resolve menu item onClick handler " + paramString + " in class " + localClass.getName());
        paramString.initCause((Throwable)paramObject);
        throw paramString;
      }
    }
    
    public boolean onMenuItemClick(MenuItem paramMenuItem)
    {
      try
      {
        if (this.c.getReturnType() == Boolean.TYPE) {
          return ((Boolean)this.c.invoke(this.b, new Object[] { paramMenuItem })).booleanValue();
        }
        this.c.invoke(this.b, new Object[] { paramMenuItem });
        return true;
      }
      catch (Exception paramMenuItem)
      {
        throw new RuntimeException(paramMenuItem);
      }
    }
  }
  
  private class MenuState
  {
    private Menu b;
    private int c;
    private int d;
    private int e;
    private int f;
    private boolean g;
    private boolean h;
    private boolean i;
    private int j;
    private int k;
    private CharSequence l;
    private CharSequence m;
    private int n;
    private char o;
    private char p;
    private int q;
    private boolean r;
    private boolean s;
    private boolean t;
    private int u;
    private int v;
    private String w;
    private String x;
    private String y;
    private ActionProvider z;
    
    public MenuState(Menu paramMenu)
    {
      this.b = paramMenu;
      a();
    }
    
    private char a(String paramString)
    {
      if (paramString == null) {
        return '\000';
      }
      return paramString.charAt(0);
    }
    
    private <T> T a(String paramString, Class<?>[] paramArrayOfClass, Object[] paramArrayOfObject)
    {
      try
      {
        paramArrayOfClass = SupportMenuInflater.a(SupportMenuInflater.this).getClassLoader().loadClass(paramString).getConstructor(paramArrayOfClass);
        paramArrayOfClass.setAccessible(true);
        paramArrayOfClass = paramArrayOfClass.newInstance(paramArrayOfObject);
        return paramArrayOfClass;
      }
      catch (Exception paramArrayOfClass)
      {
        Log.w("SupportMenuInflater", "Cannot instantiate class: " + paramString, paramArrayOfClass);
      }
      return null;
    }
    
    private void a(MenuItem paramMenuItem)
    {
      Object localObject = paramMenuItem.setChecked(this.r).setVisible(this.s).setEnabled(this.t);
      if (this.q >= 1) {}
      for (boolean bool = true;; bool = false)
      {
        ((MenuItem)localObject).setCheckable(bool).setTitleCondensed(this.m).setIcon(this.n).setAlphabeticShortcut(this.o).setNumericShortcut(this.p);
        if (this.u >= 0) {
          MenuItemCompat.a(paramMenuItem, this.u);
        }
        if (this.y == null) {
          break label160;
        }
        if (!SupportMenuInflater.a(SupportMenuInflater.this).isRestricted()) {
          break;
        }
        throw new IllegalStateException("The android:onClick attribute cannot be used within a restricted context");
      }
      paramMenuItem.setOnMenuItemClickListener(new SupportMenuInflater.InflatedOnMenuItemClickListener(SupportMenuInflater.c(SupportMenuInflater.this), this.y));
      label160:
      if ((paramMenuItem instanceof MenuItemImpl))
      {
        localObject = (MenuItemImpl)paramMenuItem;
        if (this.q >= 2)
        {
          if (!(paramMenuItem instanceof MenuItemImpl)) {
            break label273;
          }
          ((MenuItemImpl)paramMenuItem).a(true);
        }
        label196:
        int i1 = 0;
        if (this.w != null)
        {
          MenuItemCompat.a(paramMenuItem, (View)a(this.w, SupportMenuInflater.b(), SupportMenuInflater.d(SupportMenuInflater.this)));
          i1 = 1;
        }
        if (this.v > 0)
        {
          if (i1 != 0) {
            break label291;
          }
          MenuItemCompat.b(paramMenuItem, this.v);
        }
      }
      for (;;)
      {
        if (this.z != null) {
          MenuItemCompat.a(paramMenuItem, this.z);
        }
        return;
        break;
        label273:
        if (!(paramMenuItem instanceof MenuItemWrapperICS)) {
          break label196;
        }
        ((MenuItemWrapperICS)paramMenuItem).a(true);
        break label196;
        label291:
        Log.w("SupportMenuInflater", "Ignoring attribute 'itemActionViewLayout'. Action view already specified.");
      }
    }
    
    public void a()
    {
      this.c = 0;
      this.d = 0;
      this.e = 0;
      this.f = 0;
      this.g = true;
      this.h = true;
    }
    
    public void a(AttributeSet paramAttributeSet)
    {
      paramAttributeSet = SupportMenuInflater.a(SupportMenuInflater.this).obtainStyledAttributes(paramAttributeSet, R.styleable.MenuGroup);
      this.c = paramAttributeSet.getResourceId(R.styleable.MenuGroup_android_id, 0);
      this.d = paramAttributeSet.getInt(R.styleable.MenuGroup_android_menuCategory, 0);
      this.e = paramAttributeSet.getInt(R.styleable.MenuGroup_android_orderInCategory, 0);
      this.f = paramAttributeSet.getInt(R.styleable.MenuGroup_android_checkableBehavior, 0);
      this.g = paramAttributeSet.getBoolean(R.styleable.MenuGroup_android_visible, true);
      this.h = paramAttributeSet.getBoolean(R.styleable.MenuGroup_android_enabled, true);
      paramAttributeSet.recycle();
    }
    
    public void b()
    {
      this.i = true;
      a(this.b.add(this.c, this.j, this.k, this.l));
    }
    
    public void b(AttributeSet paramAttributeSet)
    {
      paramAttributeSet = SupportMenuInflater.a(SupportMenuInflater.this).obtainStyledAttributes(paramAttributeSet, R.styleable.MenuItem);
      this.j = paramAttributeSet.getResourceId(R.styleable.MenuItem_android_id, 0);
      this.k = (0xFFFF0000 & paramAttributeSet.getInt(R.styleable.MenuItem_android_menuCategory, this.d) | 0xFFFF & paramAttributeSet.getInt(R.styleable.MenuItem_android_orderInCategory, this.e));
      this.l = paramAttributeSet.getText(R.styleable.MenuItem_android_title);
      this.m = paramAttributeSet.getText(R.styleable.MenuItem_android_titleCondensed);
      this.n = paramAttributeSet.getResourceId(R.styleable.MenuItem_android_icon, 0);
      this.o = a(paramAttributeSet.getString(R.styleable.MenuItem_android_alphabeticShortcut));
      this.p = a(paramAttributeSet.getString(R.styleable.MenuItem_android_numericShortcut));
      int i1;
      if (paramAttributeSet.hasValue(R.styleable.MenuItem_android_checkable)) {
        if (paramAttributeSet.getBoolean(R.styleable.MenuItem_android_checkable, false))
        {
          i1 = 1;
          this.q = i1;
          label154:
          this.r = paramAttributeSet.getBoolean(R.styleable.MenuItem_android_checked, false);
          this.s = paramAttributeSet.getBoolean(R.styleable.MenuItem_android_visible, this.g);
          this.t = paramAttributeSet.getBoolean(R.styleable.MenuItem_android_enabled, this.h);
          this.u = paramAttributeSet.getInt(R.styleable.MenuItem_showAsAction, -1);
          this.y = paramAttributeSet.getString(R.styleable.MenuItem_android_onClick);
          this.v = paramAttributeSet.getResourceId(R.styleable.MenuItem_actionLayout, 0);
          this.w = paramAttributeSet.getString(R.styleable.MenuItem_actionViewClass);
          this.x = paramAttributeSet.getString(R.styleable.MenuItem_actionProviderClass);
          if (this.x == null) {
            break label331;
          }
          i1 = 1;
          label262:
          if ((i1 == 0) || (this.v != 0) || (this.w != null)) {
            break label336;
          }
        }
      }
      for (this.z = ((ActionProvider)a(this.x, SupportMenuInflater.a(), SupportMenuInflater.b(SupportMenuInflater.this)));; this.z = null)
      {
        paramAttributeSet.recycle();
        this.i = false;
        return;
        i1 = 0;
        break;
        this.q = this.f;
        break label154;
        label331:
        i1 = 0;
        break label262;
        label336:
        if (i1 != 0) {
          Log.w("SupportMenuInflater", "Ignoring attribute 'actionProviderClass'. Action view already specified.");
        }
      }
    }
    
    public SubMenu c()
    {
      this.i = true;
      SubMenu localSubMenu = this.b.addSubMenu(this.c, this.j, this.k, this.l);
      a(localSubMenu.getItem());
      return localSubMenu;
    }
    
    public boolean d()
    {
      return this.i;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/view/SupportMenuInflater.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */