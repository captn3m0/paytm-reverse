package android.support.v7.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v4.util.SimpleArrayMap;
import android.support.v7.view.menu.MenuWrapperFactory;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.util.ArrayList;

@TargetApi(11)
public class SupportActionModeWrapper
  extends android.view.ActionMode
{
  final Context a;
  final ActionMode b;
  
  public SupportActionModeWrapper(Context paramContext, ActionMode paramActionMode)
  {
    this.a = paramContext;
    this.b = paramActionMode;
  }
  
  public void finish()
  {
    this.b.c();
  }
  
  public View getCustomView()
  {
    return this.b.i();
  }
  
  public Menu getMenu()
  {
    return MenuWrapperFactory.a(this.a, (SupportMenu)this.b.b());
  }
  
  public MenuInflater getMenuInflater()
  {
    return this.b.a();
  }
  
  public CharSequence getSubtitle()
  {
    return this.b.g();
  }
  
  public Object getTag()
  {
    return this.b.j();
  }
  
  public CharSequence getTitle()
  {
    return this.b.f();
  }
  
  public boolean getTitleOptionalHint()
  {
    return this.b.k();
  }
  
  public void invalidate()
  {
    this.b.d();
  }
  
  public boolean isTitleOptional()
  {
    return this.b.h();
  }
  
  public void setCustomView(View paramView)
  {
    this.b.a(paramView);
  }
  
  public void setSubtitle(int paramInt)
  {
    this.b.b(paramInt);
  }
  
  public void setSubtitle(CharSequence paramCharSequence)
  {
    this.b.a(paramCharSequence);
  }
  
  public void setTag(Object paramObject)
  {
    this.b.a(paramObject);
  }
  
  public void setTitle(int paramInt)
  {
    this.b.a(paramInt);
  }
  
  public void setTitle(CharSequence paramCharSequence)
  {
    this.b.b(paramCharSequence);
  }
  
  public void setTitleOptionalHint(boolean paramBoolean)
  {
    this.b.a(paramBoolean);
  }
  
  public static class CallbackWrapper
    implements ActionMode.Callback
  {
    final android.view.ActionMode.Callback a;
    final Context b;
    final ArrayList<SupportActionModeWrapper> c;
    final SimpleArrayMap<Menu, Menu> d;
    
    public CallbackWrapper(Context paramContext, android.view.ActionMode.Callback paramCallback)
    {
      this.b = paramContext;
      this.a = paramCallback;
      this.c = new ArrayList();
      this.d = new SimpleArrayMap();
    }
    
    private Menu a(Menu paramMenu)
    {
      Menu localMenu2 = (Menu)this.d.get(paramMenu);
      Menu localMenu1 = localMenu2;
      if (localMenu2 == null)
      {
        localMenu1 = MenuWrapperFactory.a(this.b, (SupportMenu)paramMenu);
        this.d.put(paramMenu, localMenu1);
      }
      return localMenu1;
    }
    
    public void a(ActionMode paramActionMode)
    {
      this.a.onDestroyActionMode(b(paramActionMode));
    }
    
    public boolean a(ActionMode paramActionMode, Menu paramMenu)
    {
      return this.a.onCreateActionMode(b(paramActionMode), a(paramMenu));
    }
    
    public boolean a(ActionMode paramActionMode, MenuItem paramMenuItem)
    {
      return this.a.onActionItemClicked(b(paramActionMode), MenuWrapperFactory.a(this.b, (SupportMenuItem)paramMenuItem));
    }
    
    public android.view.ActionMode b(ActionMode paramActionMode)
    {
      int i = 0;
      int j = this.c.size();
      while (i < j)
      {
        SupportActionModeWrapper localSupportActionModeWrapper = (SupportActionModeWrapper)this.c.get(i);
        if ((localSupportActionModeWrapper != null) && (localSupportActionModeWrapper.b == paramActionMode)) {
          return localSupportActionModeWrapper;
        }
        i += 1;
      }
      paramActionMode = new SupportActionModeWrapper(this.b, paramActionMode);
      this.c.add(paramActionMode);
      return paramActionMode;
    }
    
    public boolean b(ActionMode paramActionMode, Menu paramMenu)
    {
      return this.a.onPrepareActionMode(b(paramActionMode), a(paramMenu));
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/view/SupportActionModeWrapper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */