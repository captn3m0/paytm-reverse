package android.support.v7.view;

import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.view.View;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Iterator;

public class ViewPropertyAnimatorCompatSet
{
  private final ArrayList<ViewPropertyAnimatorCompat> a = new ArrayList();
  private long b = -1L;
  private Interpolator c;
  private ViewPropertyAnimatorListener d;
  private boolean e;
  private final ViewPropertyAnimatorListenerAdapter f = new ViewPropertyAnimatorListenerAdapter()
  {
    private boolean b = false;
    private int c = 0;
    
    void a()
    {
      this.c = 0;
      this.b = false;
      ViewPropertyAnimatorCompatSet.b(ViewPropertyAnimatorCompatSet.this);
    }
    
    public void a(View paramAnonymousView)
    {
      if (this.b) {}
      do
      {
        return;
        this.b = true;
      } while (ViewPropertyAnimatorCompatSet.a(ViewPropertyAnimatorCompatSet.this) == null);
      ViewPropertyAnimatorCompatSet.a(ViewPropertyAnimatorCompatSet.this).a(null);
    }
    
    public void b(View paramAnonymousView)
    {
      int i = this.c + 1;
      this.c = i;
      if (i == ViewPropertyAnimatorCompatSet.c(ViewPropertyAnimatorCompatSet.this).size())
      {
        if (ViewPropertyAnimatorCompatSet.a(ViewPropertyAnimatorCompatSet.this) != null) {
          ViewPropertyAnimatorCompatSet.a(ViewPropertyAnimatorCompatSet.this).b(null);
        }
        a();
      }
    }
  };
  
  private void b()
  {
    this.e = false;
  }
  
  public ViewPropertyAnimatorCompatSet a(long paramLong)
  {
    if (!this.e) {
      this.b = paramLong;
    }
    return this;
  }
  
  public ViewPropertyAnimatorCompatSet a(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat)
  {
    if (!this.e) {
      this.a.add(paramViewPropertyAnimatorCompat);
    }
    return this;
  }
  
  public ViewPropertyAnimatorCompatSet a(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat1, ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat2)
  {
    this.a.add(paramViewPropertyAnimatorCompat1);
    paramViewPropertyAnimatorCompat2.b(paramViewPropertyAnimatorCompat1.a());
    this.a.add(paramViewPropertyAnimatorCompat2);
    return this;
  }
  
  public ViewPropertyAnimatorCompatSet a(ViewPropertyAnimatorListener paramViewPropertyAnimatorListener)
  {
    if (!this.e) {
      this.d = paramViewPropertyAnimatorListener;
    }
    return this;
  }
  
  public ViewPropertyAnimatorCompatSet a(Interpolator paramInterpolator)
  {
    if (!this.e) {
      this.c = paramInterpolator;
    }
    return this;
  }
  
  public void a()
  {
    if (this.e) {
      return;
    }
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext())
    {
      ViewPropertyAnimatorCompat localViewPropertyAnimatorCompat = (ViewPropertyAnimatorCompat)localIterator.next();
      if (this.b >= 0L) {
        localViewPropertyAnimatorCompat.a(this.b);
      }
      if (this.c != null) {
        localViewPropertyAnimatorCompat.a(this.c);
      }
      if (this.d != null) {
        localViewPropertyAnimatorCompat.a(this.f);
      }
      localViewPropertyAnimatorCompat.b();
    }
    this.e = true;
  }
  
  public void cancel()
  {
    if (!this.e) {
      return;
    }
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext()) {
      ((ViewPropertyAnimatorCompat)localIterator.next()).cancel();
    }
    this.e = false;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v7/view/ViewPropertyAnimatorCompatSet.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */