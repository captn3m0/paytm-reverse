package android.support.design.internal;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.design.R.dimen;
import android.support.design.R.layout;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.MenuPresenter;
import android.support.v7.view.menu.MenuPresenter.Callback;
import android.support.v7.view.menu.MenuView;
import android.support.v7.view.menu.SubMenuBuilder;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Iterator;

public class NavigationMenuPresenter
  implements MenuPresenter
{
  private NavigationMenuView a;
  private LinearLayout b;
  private MenuPresenter.Callback c;
  private MenuBuilder d;
  private int e;
  private NavigationMenuAdapter f;
  private LayoutInflater g;
  private int h;
  private boolean i;
  private ColorStateList j;
  private ColorStateList k;
  private Drawable l;
  private int m;
  private int n;
  private final View.OnClickListener o = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      paramAnonymousView = (NavigationMenuItemView)paramAnonymousView;
      NavigationMenuPresenter.this.b(true);
      paramAnonymousView = paramAnonymousView.getItemData();
      boolean bool = NavigationMenuPresenter.a(NavigationMenuPresenter.this).a(paramAnonymousView, NavigationMenuPresenter.this, 0);
      if ((paramAnonymousView != null) && (paramAnonymousView.isCheckable()) && (bool)) {
        NavigationMenuPresenter.b(NavigationMenuPresenter.this).a(paramAnonymousView);
      }
      NavigationMenuPresenter.this.b(false);
      NavigationMenuPresenter.this.a(false);
    }
  };
  
  public MenuView a(ViewGroup paramViewGroup)
  {
    if (this.a == null)
    {
      this.a = ((NavigationMenuView)this.g.inflate(R.layout.design_navigation_menu, paramViewGroup, false));
      if (this.f == null) {
        this.f = new NavigationMenuAdapter();
      }
      this.b = ((LinearLayout)this.g.inflate(R.layout.design_navigation_item_header, this.a, false));
      this.a.setAdapter(this.f);
    }
    return this.a;
  }
  
  public void a(int paramInt)
  {
    this.e = paramInt;
  }
  
  public void a(Context paramContext, MenuBuilder paramMenuBuilder)
  {
    this.g = LayoutInflater.from(paramContext);
    this.d = paramMenuBuilder;
    this.n = paramContext.getResources().getDimensionPixelOffset(R.dimen.design_navigation_separator_vertical_padding);
  }
  
  public void a(@Nullable ColorStateList paramColorStateList)
  {
    this.k = paramColorStateList;
    a(false);
  }
  
  public void a(@Nullable Drawable paramDrawable)
  {
    this.l = paramDrawable;
    a(false);
  }
  
  public void a(Parcelable paramParcelable)
  {
    paramParcelable = (Bundle)paramParcelable;
    SparseArray localSparseArray = paramParcelable.getSparseParcelableArray("android:menu:list");
    if (localSparseArray != null) {
      this.a.restoreHierarchyState(localSparseArray);
    }
    paramParcelable = paramParcelable.getBundle("android:menu:adapter");
    if (paramParcelable != null) {
      this.f.a(paramParcelable);
    }
  }
  
  public void a(MenuBuilder paramMenuBuilder, boolean paramBoolean)
  {
    if (this.c != null) {
      this.c.a(paramMenuBuilder, paramBoolean);
    }
  }
  
  public void a(MenuItemImpl paramMenuItemImpl)
  {
    this.f.a(paramMenuItemImpl);
  }
  
  public void a(@NonNull View paramView)
  {
    this.b.addView(paramView);
    this.a.setPadding(0, 0, 0, this.a.getPaddingBottom());
  }
  
  public void a(boolean paramBoolean)
  {
    if (this.f != null) {
      this.f.a();
    }
  }
  
  public boolean a()
  {
    return false;
  }
  
  public boolean a(MenuBuilder paramMenuBuilder, MenuItemImpl paramMenuItemImpl)
  {
    return false;
  }
  
  public boolean a(SubMenuBuilder paramSubMenuBuilder)
  {
    return false;
  }
  
  public int b()
  {
    return this.e;
  }
  
  public View b(@LayoutRes int paramInt)
  {
    View localView = this.g.inflate(paramInt, this.b, false);
    a(localView);
    return localView;
  }
  
  public void b(@Nullable ColorStateList paramColorStateList)
  {
    this.j = paramColorStateList;
    a(false);
  }
  
  public void b(boolean paramBoolean)
  {
    if (this.f != null) {
      this.f.a(paramBoolean);
    }
  }
  
  public boolean b(MenuBuilder paramMenuBuilder, MenuItemImpl paramMenuItemImpl)
  {
    return false;
  }
  
  public Parcelable c()
  {
    Bundle localBundle = new Bundle();
    if (this.a != null)
    {
      SparseArray localSparseArray = new SparseArray();
      this.a.saveHierarchyState(localSparseArray);
      localBundle.putSparseParcelableArray("android:menu:list", localSparseArray);
    }
    if (this.f != null) {
      localBundle.putBundle("android:menu:adapter", this.f.b());
    }
    return localBundle;
  }
  
  public void c(@StyleRes int paramInt)
  {
    this.h = paramInt;
    this.i = true;
    a(false);
  }
  
  public int d()
  {
    return this.b.getChildCount();
  }
  
  public void d(int paramInt)
  {
    if (this.m != paramInt)
    {
      this.m = paramInt;
      if (this.b.getChildCount() == 0) {
        this.a.setPadding(0, this.m, 0, this.a.getPaddingBottom());
      }
    }
  }
  
  @Nullable
  public ColorStateList e()
  {
    return this.k;
  }
  
  @Nullable
  public ColorStateList f()
  {
    return this.j;
  }
  
  @Nullable
  public Drawable g()
  {
    return this.l;
  }
  
  private static class HeaderViewHolder
    extends NavigationMenuPresenter.ViewHolder
  {
    public HeaderViewHolder(View paramView)
    {
      super();
    }
  }
  
  private class NavigationMenuAdapter
    extends RecyclerView.Adapter<NavigationMenuPresenter.ViewHolder>
  {
    private final ArrayList<NavigationMenuPresenter.NavigationMenuItem> b = new ArrayList();
    private MenuItemImpl c;
    private ColorDrawable d;
    private boolean e;
    
    NavigationMenuAdapter()
    {
      c();
    }
    
    private void a(int paramInt1, int paramInt2)
    {
      while (paramInt1 < paramInt2)
      {
        MenuItemImpl localMenuItemImpl = ((NavigationMenuPresenter.NavigationMenuTextItem)this.b.get(paramInt1)).a();
        if (localMenuItemImpl.getIcon() == null)
        {
          if (this.d == null) {
            this.d = new ColorDrawable(0);
          }
          localMenuItemImpl.setIcon(this.d);
        }
        paramInt1 += 1;
      }
    }
    
    private void c()
    {
      if (this.e) {
        return;
      }
      this.e = true;
      this.b.clear();
      this.b.add(new NavigationMenuPresenter.NavigationMenuHeaderItem(null));
      int i1 = -1;
      int k = 0;
      int m = 0;
      int n = 0;
      int i4 = NavigationMenuPresenter.a(NavigationMenuPresenter.this).i().size();
      while (n < i4)
      {
        MenuItemImpl localMenuItemImpl1 = (MenuItemImpl)NavigationMenuPresenter.a(NavigationMenuPresenter.this).i().get(n);
        if (localMenuItemImpl1.isChecked()) {
          a(localMenuItemImpl1);
        }
        if (localMenuItemImpl1.isCheckable()) {
          localMenuItemImpl1.a(false);
        }
        int j;
        int i2;
        int i3;
        int i;
        if (localMenuItemImpl1.hasSubMenu())
        {
          SubMenu localSubMenu = localMenuItemImpl1.getSubMenu();
          j = m;
          i2 = i1;
          i3 = k;
          if (localSubMenu.hasVisibleItems())
          {
            if (n != 0) {
              this.b.add(new NavigationMenuPresenter.NavigationMenuSeparatorItem(NavigationMenuPresenter.k(NavigationMenuPresenter.this), 0));
            }
            this.b.add(new NavigationMenuPresenter.NavigationMenuTextItem(localMenuItemImpl1, null));
            i = 0;
            int i5 = this.b.size();
            i2 = 0;
            i3 = localSubMenu.size();
            while (i2 < i3)
            {
              MenuItemImpl localMenuItemImpl2 = (MenuItemImpl)localSubMenu.getItem(i2);
              j = i;
              if (localMenuItemImpl2.isVisible())
              {
                j = i;
                if (i == 0)
                {
                  j = i;
                  if (localMenuItemImpl2.getIcon() != null) {
                    j = 1;
                  }
                }
                if (localMenuItemImpl2.isCheckable()) {
                  localMenuItemImpl2.a(false);
                }
                if (localMenuItemImpl1.isChecked()) {
                  a(localMenuItemImpl1);
                }
                this.b.add(new NavigationMenuPresenter.NavigationMenuTextItem(localMenuItemImpl2, null));
              }
              i2 += 1;
              i = j;
            }
            j = m;
            i2 = i1;
            i3 = k;
            if (i != 0)
            {
              a(i5, this.b.size());
              i3 = k;
              i2 = i1;
              j = m;
            }
          }
          n += 1;
          m = j;
          i1 = i2;
          k = i3;
        }
        else
        {
          i2 = localMenuItemImpl1.getGroupId();
          if (i2 != i1)
          {
            m = this.b.size();
            if (localMenuItemImpl1.getIcon() != null)
            {
              k = 1;
              label416:
              j = k;
              i = m;
              if (n != 0)
              {
                i = m + 1;
                this.b.add(new NavigationMenuPresenter.NavigationMenuSeparatorItem(NavigationMenuPresenter.k(NavigationMenuPresenter.this), NavigationMenuPresenter.k(NavigationMenuPresenter.this)));
                j = k;
              }
            }
          }
          for (;;)
          {
            if ((j != 0) && (localMenuItemImpl1.getIcon() == null)) {
              localMenuItemImpl1.setIcon(17170445);
            }
            this.b.add(new NavigationMenuPresenter.NavigationMenuTextItem(localMenuItemImpl1, null));
            i3 = i;
            break;
            k = 0;
            break label416;
            j = m;
            i = k;
            if (m == 0)
            {
              j = m;
              i = k;
              if (localMenuItemImpl1.getIcon() != null)
              {
                j = 1;
                a(k, this.b.size());
                i = k;
              }
            }
          }
        }
      }
      this.e = false;
    }
    
    public NavigationMenuPresenter.ViewHolder a(ViewGroup paramViewGroup, int paramInt)
    {
      switch (paramInt)
      {
      default: 
        return null;
      case 0: 
        return new NavigationMenuPresenter.NormalViewHolder(NavigationMenuPresenter.c(NavigationMenuPresenter.this), paramViewGroup, NavigationMenuPresenter.d(NavigationMenuPresenter.this));
      case 1: 
        return new NavigationMenuPresenter.SubheaderViewHolder(NavigationMenuPresenter.c(NavigationMenuPresenter.this), paramViewGroup);
      case 2: 
        return new NavigationMenuPresenter.SeparatorViewHolder(NavigationMenuPresenter.c(NavigationMenuPresenter.this), paramViewGroup);
      }
      return new NavigationMenuPresenter.HeaderViewHolder(NavigationMenuPresenter.e(NavigationMenuPresenter.this));
    }
    
    public void a()
    {
      c();
      notifyDataSetChanged();
    }
    
    public void a(Bundle paramBundle)
    {
      int i = paramBundle.getInt("android:menu:checked", 0);
      if (i != 0)
      {
        this.e = true;
        localObject1 = this.b.iterator();
        while (((Iterator)localObject1).hasNext())
        {
          localObject2 = (NavigationMenuPresenter.NavigationMenuItem)((Iterator)localObject1).next();
          if ((localObject2 instanceof NavigationMenuPresenter.NavigationMenuTextItem))
          {
            localObject2 = ((NavigationMenuPresenter.NavigationMenuTextItem)localObject2).a();
            if ((localObject2 != null) && (((MenuItemImpl)localObject2).getItemId() == i)) {
              a((MenuItemImpl)localObject2);
            }
          }
        }
        this.e = false;
        c();
      }
      Object localObject1 = paramBundle.getSparseParcelableArray("android:menu:action_views");
      Object localObject2 = this.b.iterator();
      label182:
      while (((Iterator)localObject2).hasNext())
      {
        paramBundle = (NavigationMenuPresenter.NavigationMenuItem)((Iterator)localObject2).next();
        if ((paramBundle instanceof NavigationMenuPresenter.NavigationMenuTextItem))
        {
          MenuItemImpl localMenuItemImpl = ((NavigationMenuPresenter.NavigationMenuTextItem)paramBundle).a();
          if (localMenuItemImpl != null) {}
          for (paramBundle = localMenuItemImpl.getActionView();; paramBundle = null)
          {
            if (paramBundle == null) {
              break label182;
            }
            paramBundle.restoreHierarchyState((SparseArray)((SparseArray)localObject1).get(localMenuItemImpl.getItemId()));
            break;
          }
        }
      }
    }
    
    public void a(NavigationMenuPresenter.ViewHolder paramViewHolder)
    {
      if ((paramViewHolder instanceof NavigationMenuPresenter.NormalViewHolder)) {
        ((NavigationMenuItemView)paramViewHolder.itemView).a();
      }
    }
    
    public void a(NavigationMenuPresenter.ViewHolder paramViewHolder, int paramInt)
    {
      switch (getItemViewType(paramInt))
      {
      default: 
        return;
      case 0: 
        localObject = (NavigationMenuItemView)paramViewHolder.itemView;
        ((NavigationMenuItemView)localObject).setIconTintList(NavigationMenuPresenter.f(NavigationMenuPresenter.this));
        if (NavigationMenuPresenter.g(NavigationMenuPresenter.this)) {
          ((NavigationMenuItemView)localObject).setTextAppearance(((NavigationMenuItemView)localObject).getContext(), NavigationMenuPresenter.h(NavigationMenuPresenter.this));
        }
        if (NavigationMenuPresenter.i(NavigationMenuPresenter.this) != null) {
          ((NavigationMenuItemView)localObject).setTextColor(NavigationMenuPresenter.i(NavigationMenuPresenter.this));
        }
        if (NavigationMenuPresenter.j(NavigationMenuPresenter.this) != null) {}
        for (paramViewHolder = NavigationMenuPresenter.j(NavigationMenuPresenter.this).getConstantState().newDrawable();; paramViewHolder = null)
        {
          ((NavigationMenuItemView)localObject).setBackgroundDrawable(paramViewHolder);
          ((NavigationMenuItemView)localObject).a(((NavigationMenuPresenter.NavigationMenuTextItem)this.b.get(paramInt)).a(), 0);
          return;
        }
      case 1: 
        ((TextView)paramViewHolder.itemView).setText(((NavigationMenuPresenter.NavigationMenuTextItem)this.b.get(paramInt)).a().getTitle());
        return;
      }
      Object localObject = (NavigationMenuPresenter.NavigationMenuSeparatorItem)this.b.get(paramInt);
      paramViewHolder.itemView.setPadding(0, ((NavigationMenuPresenter.NavigationMenuSeparatorItem)localObject).a(), 0, ((NavigationMenuPresenter.NavigationMenuSeparatorItem)localObject).b());
    }
    
    public void a(MenuItemImpl paramMenuItemImpl)
    {
      if ((this.c == paramMenuItemImpl) || (!paramMenuItemImpl.isCheckable())) {
        return;
      }
      if (this.c != null) {
        this.c.setChecked(false);
      }
      this.c = paramMenuItemImpl;
      paramMenuItemImpl.setChecked(true);
    }
    
    public void a(boolean paramBoolean)
    {
      this.e = paramBoolean;
    }
    
    public Bundle b()
    {
      Bundle localBundle = new Bundle();
      if (this.c != null) {
        localBundle.putInt("android:menu:checked", this.c.getItemId());
      }
      SparseArray localSparseArray = new SparseArray();
      Iterator localIterator = this.b.iterator();
      label129:
      while (localIterator.hasNext())
      {
        Object localObject = (NavigationMenuPresenter.NavigationMenuItem)localIterator.next();
        if ((localObject instanceof NavigationMenuPresenter.NavigationMenuTextItem))
        {
          MenuItemImpl localMenuItemImpl = ((NavigationMenuPresenter.NavigationMenuTextItem)localObject).a();
          if (localMenuItemImpl != null) {}
          for (localObject = localMenuItemImpl.getActionView();; localObject = null)
          {
            if (localObject == null) {
              break label129;
            }
            ParcelableSparseArray localParcelableSparseArray = new ParcelableSparseArray();
            ((View)localObject).saveHierarchyState(localParcelableSparseArray);
            localSparseArray.put(localMenuItemImpl.getItemId(), localParcelableSparseArray);
            break;
          }
        }
      }
      localBundle.putSparseParcelableArray("android:menu:action_views", localSparseArray);
      return localBundle;
    }
    
    public int getItemCount()
    {
      return this.b.size();
    }
    
    public long getItemId(int paramInt)
    {
      return paramInt;
    }
    
    public int getItemViewType(int paramInt)
    {
      NavigationMenuPresenter.NavigationMenuItem localNavigationMenuItem = (NavigationMenuPresenter.NavigationMenuItem)this.b.get(paramInt);
      if ((localNavigationMenuItem instanceof NavigationMenuPresenter.NavigationMenuSeparatorItem)) {
        return 2;
      }
      if ((localNavigationMenuItem instanceof NavigationMenuPresenter.NavigationMenuHeaderItem)) {
        return 3;
      }
      if ((localNavigationMenuItem instanceof NavigationMenuPresenter.NavigationMenuTextItem))
      {
        if (((NavigationMenuPresenter.NavigationMenuTextItem)localNavigationMenuItem).a().hasSubMenu()) {
          return 1;
        }
        return 0;
      }
      throw new RuntimeException("Unknown item type.");
    }
  }
  
  private static class NavigationMenuHeaderItem
    implements NavigationMenuPresenter.NavigationMenuItem
  {}
  
  private static abstract interface NavigationMenuItem {}
  
  private static class NavigationMenuSeparatorItem
    implements NavigationMenuPresenter.NavigationMenuItem
  {
    private final int a;
    private final int b;
    
    public NavigationMenuSeparatorItem(int paramInt1, int paramInt2)
    {
      this.a = paramInt1;
      this.b = paramInt2;
    }
    
    public int a()
    {
      return this.a;
    }
    
    public int b()
    {
      return this.b;
    }
  }
  
  private static class NavigationMenuTextItem
    implements NavigationMenuPresenter.NavigationMenuItem
  {
    private final MenuItemImpl a;
    
    private NavigationMenuTextItem(MenuItemImpl paramMenuItemImpl)
    {
      this.a = paramMenuItemImpl;
    }
    
    public MenuItemImpl a()
    {
      return this.a;
    }
  }
  
  private static class NormalViewHolder
    extends NavigationMenuPresenter.ViewHolder
  {
    public NormalViewHolder(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, View.OnClickListener paramOnClickListener)
    {
      super();
      this.itemView.setOnClickListener(paramOnClickListener);
    }
  }
  
  private static class SeparatorViewHolder
    extends NavigationMenuPresenter.ViewHolder
  {
    public SeparatorViewHolder(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup)
    {
      super();
    }
  }
  
  private static class SubheaderViewHolder
    extends NavigationMenuPresenter.ViewHolder
  {
    public SubheaderViewHolder(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup)
    {
      super();
    }
  }
  
  private static abstract class ViewHolder
    extends RecyclerView.ViewHolder
  {
    public ViewHolder(View paramView)
    {
      super();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/design/internal/NavigationMenuPresenter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */