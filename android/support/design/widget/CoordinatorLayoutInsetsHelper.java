package android.support.design.widget;

import android.support.v4.view.OnApplyWindowInsetsListener;
import android.view.View;

abstract interface CoordinatorLayoutInsetsHelper
{
  public abstract void a(View paramView, OnApplyWindowInsetsListener paramOnApplyWindowInsetsListener);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/design/widget/CoordinatorLayoutInsetsHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */