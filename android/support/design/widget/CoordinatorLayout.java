package android.support.design.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.SystemClock;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.design.R.style;
import android.support.design.R.styleable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.NestedScrollingParent;
import android.support.v4.view.NestedScrollingParentHelper;
import android.support.v4.view.OnApplyWindowInsetsListener;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.WindowInsetsCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewGroup.OnHierarchyChangeListener;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoordinatorLayout
  extends ViewGroup
  implements NestedScrollingParent
{
  static final String a;
  static final Class<?>[] b;
  static final ThreadLocal<Map<String, Constructor<Behavior>>> c;
  static final Comparator<View> e;
  static final CoordinatorLayoutInsetsHelper f;
  private final NestedScrollingParentHelper A = new NestedScrollingParentHelper(this);
  final Comparator<View> d = new Comparator()
  {
    public int a(View paramAnonymousView1, View paramAnonymousView2)
    {
      if (paramAnonymousView1 == paramAnonymousView2) {
        return 0;
      }
      if (((CoordinatorLayout.LayoutParams)paramAnonymousView1.getLayoutParams()).a(CoordinatorLayout.this, paramAnonymousView1, paramAnonymousView2)) {
        return 1;
      }
      if (((CoordinatorLayout.LayoutParams)paramAnonymousView2.getLayoutParams()).a(CoordinatorLayout.this, paramAnonymousView2, paramAnonymousView1)) {
        return -1;
      }
      return 0;
    }
  };
  private final List<View> g = new ArrayList();
  private final List<View> h = new ArrayList();
  private final List<View> i = new ArrayList();
  private final Rect j = new Rect();
  private final Rect k = new Rect();
  private final Rect l = new Rect();
  private final int[] m = new int[2];
  private Paint n;
  private boolean o;
  private boolean p;
  private int[] q;
  private View r;
  private View s;
  private View t;
  private OnPreDrawListener u;
  private boolean v;
  private WindowInsetsCompat w;
  private boolean x;
  private Drawable y;
  private ViewGroup.OnHierarchyChangeListener z;
  
  static
  {
    Object localObject = CoordinatorLayout.class.getPackage();
    if (localObject != null)
    {
      localObject = ((Package)localObject).getName();
      a = (String)localObject;
      if (Build.VERSION.SDK_INT < 21) {
        break label80;
      }
      e = new ViewElevationComparator();
    }
    for (f = new CoordinatorLayoutInsetsHelperLollipop();; f = null)
    {
      b = new Class[] { Context.class, AttributeSet.class };
      c = new ThreadLocal();
      return;
      localObject = null;
      break;
      label80:
      e = null;
    }
  }
  
  public CoordinatorLayout(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public CoordinatorLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public CoordinatorLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    ThemeUtils.a(paramContext);
    paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CoordinatorLayout, paramInt, R.style.Widget_Design_CoordinatorLayout);
    paramInt = paramAttributeSet.getResourceId(R.styleable.CoordinatorLayout_keylines, 0);
    if (paramInt != 0)
    {
      paramContext = paramContext.getResources();
      this.q = paramContext.getIntArray(paramInt);
      float f1 = paramContext.getDisplayMetrics().density;
      int i1 = this.q.length;
      paramInt = 0;
      while (paramInt < i1)
      {
        paramContext = this.q;
        paramContext[paramInt] = ((int)(paramContext[paramInt] * f1));
        paramInt += 1;
      }
    }
    this.y = paramAttributeSet.getDrawable(R.styleable.CoordinatorLayout_statusBarBackground);
    paramAttributeSet.recycle();
    if (f != null) {
      f.a(this, new ApplyInsetsListener(null));
    }
    super.setOnHierarchyChangeListener(new HierarchyChangeListener(null));
  }
  
  private int a(int paramInt)
  {
    if (this.q == null)
    {
      Log.e("CoordinatorLayout", "No keylines defined for " + this + " - attempted index lookup " + paramInt);
      return 0;
    }
    if ((paramInt < 0) || (paramInt >= this.q.length))
    {
      Log.e("CoordinatorLayout", "Keyline index " + paramInt + " out of range for " + this);
      return 0;
    }
    return this.q[paramInt];
  }
  
  static Behavior a(Context paramContext, AttributeSet paramAttributeSet, String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return null;
    }
    if (paramString.startsWith(".")) {
      paramString = paramContext.getPackageName() + paramString;
    }
    do
    {
      try
      {
        Object localObject2 = (Map)c.get();
        Object localObject1 = localObject2;
        if (localObject2 == null)
        {
          localObject1 = new HashMap();
          c.set(localObject1);
        }
        Constructor localConstructor = (Constructor)((Map)localObject1).get(paramString);
        localObject2 = localConstructor;
        if (localConstructor == null)
        {
          localObject2 = Class.forName(paramString, true, paramContext.getClassLoader()).getConstructor(b);
          ((Constructor)localObject2).setAccessible(true);
          ((Map)localObject1).put(paramString, localObject2);
        }
        paramContext = (Behavior)((Constructor)localObject2).newInstance(new Object[] { paramContext, paramAttributeSet });
        return paramContext;
      }
      catch (Exception paramContext)
      {
        throw new RuntimeException("Could not inflate Behavior subclass " + paramString, paramContext);
      }
    } while (paramString.indexOf('.') >= 0);
    if (!TextUtils.isEmpty(a)) {
      paramString = a + '.' + paramString;
    }
    for (;;)
    {
      break;
    }
  }
  
  private WindowInsetsCompat a(WindowInsetsCompat paramWindowInsetsCompat)
  {
    boolean bool2 = true;
    WindowInsetsCompat localWindowInsetsCompat = paramWindowInsetsCompat;
    if (this.w != paramWindowInsetsCompat)
    {
      this.w = paramWindowInsetsCompat;
      if ((paramWindowInsetsCompat == null) || (paramWindowInsetsCompat.b() <= 0)) {
        break label71;
      }
      bool1 = true;
      this.x = bool1;
      if ((this.x) || (getBackground() != null)) {
        break label76;
      }
    }
    label71:
    label76:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      setWillNotDraw(bool1);
      localWindowInsetsCompat = b(paramWindowInsetsCompat);
      requestLayout();
      return localWindowInsetsCompat;
      bool1 = false;
      break;
    }
  }
  
  private void a(View paramView1, View paramView2, int paramInt)
  {
    Object localObject = (LayoutParams)paramView1.getLayoutParams();
    localObject = this.j;
    Rect localRect = this.k;
    a(paramView2, (Rect)localObject);
    a(paramView1, paramInt, (Rect)localObject, localRect);
    paramView1.layout(localRect.left, localRect.top, localRect.right, localRect.bottom);
  }
  
  private void a(List<View> paramList)
  {
    paramList.clear();
    boolean bool = isChildrenDrawingOrderEnabled();
    int i3 = getChildCount();
    int i1 = i3 - 1;
    if (i1 >= 0)
    {
      if (bool) {}
      for (int i2 = getChildDrawingOrder(i3, i1);; i2 = i1)
      {
        paramList.add(getChildAt(i2));
        i1 -= 1;
        break;
      }
    }
    if (e != null) {
      Collections.sort(paramList, e);
    }
  }
  
  private static void a(List<View> paramList, Comparator<View> paramComparator)
  {
    if ((paramList == null) || (paramList.size() < 2)) {}
    for (;;)
    {
      return;
      View[] arrayOfView = new View[paramList.size()];
      paramList.toArray(arrayOfView);
      int i5 = arrayOfView.length;
      int i1 = 0;
      while (i1 < i5)
      {
        int i3 = i1;
        int i2 = i1 + 1;
        while (i2 < i5)
        {
          int i4 = i3;
          if (paramComparator.compare(arrayOfView[i2], arrayOfView[i3]) < 0) {
            i4 = i2;
          }
          i2 += 1;
          i3 = i4;
        }
        if (i1 != i3)
        {
          View localView = arrayOfView[i3];
          arrayOfView[i3] = arrayOfView[i1];
          arrayOfView[i1] = localView;
        }
        i1 += 1;
      }
      paramList.clear();
      i1 = 0;
      while (i1 < i5)
      {
        paramList.add(arrayOfView[i1]);
        i1 += 1;
      }
    }
  }
  
  private boolean a(MotionEvent paramMotionEvent, int paramInt)
  {
    boolean bool1 = false;
    int i1 = 0;
    Object localObject1 = null;
    int i4 = MotionEventCompat.a(paramMotionEvent);
    List localList = this.h;
    a(localList);
    int i5 = localList.size();
    int i2 = 0;
    boolean bool2 = bool1;
    View localView;
    Object localObject2;
    Behavior localBehavior;
    boolean bool3;
    int i3;
    if (i2 < i5)
    {
      localView = (View)localList.get(i2);
      localObject2 = (LayoutParams)localView.getLayoutParams();
      localBehavior = ((LayoutParams)localObject2).b();
      if (((bool1) || (i1 != 0)) && (i4 != 0))
      {
        localObject2 = localObject1;
        bool3 = bool1;
        i3 = i1;
        if (localBehavior != null)
        {
          localObject2 = localObject1;
          if (localObject1 == null)
          {
            long l1 = SystemClock.uptimeMillis();
            localObject2 = MotionEvent.obtain(l1, l1, 3, 0.0F, 0.0F, 0);
          }
          switch (paramInt)
          {
          default: 
            i3 = i1;
            bool3 = bool1;
          }
        }
        for (;;)
        {
          i2 += 1;
          localObject1 = localObject2;
          bool1 = bool3;
          i1 = i3;
          break;
          localBehavior.a(this, localView, (MotionEvent)localObject2);
          bool3 = bool1;
          i3 = i1;
          continue;
          localBehavior.b(this, localView, (MotionEvent)localObject2);
          bool3 = bool1;
          i3 = i1;
        }
      }
      bool2 = bool1;
      if (!bool1)
      {
        bool2 = bool1;
        if (localBehavior == null) {}
      }
      switch (paramInt)
      {
      default: 
        label272:
        bool2 = bool1;
        if (bool1)
        {
          this.r = localView;
          bool2 = bool1;
        }
        bool3 = ((LayoutParams)localObject2).e();
        bool1 = ((LayoutParams)localObject2).a(this, localView);
        if ((!bool1) || (bool3)) {
          break;
        }
      }
    }
    for (i1 = 1;; i1 = 0)
    {
      localObject2 = localObject1;
      bool3 = bool2;
      i3 = i1;
      if (!bool1) {
        break;
      }
      localObject2 = localObject1;
      bool3 = bool2;
      i3 = i1;
      if (i1 != 0) {
        break;
      }
      localList.clear();
      return bool2;
      bool1 = localBehavior.a(this, localView, paramMotionEvent);
      break label272;
      bool1 = localBehavior.b(this, localView, paramMotionEvent);
      break label272;
    }
  }
  
  private static int b(int paramInt)
  {
    int i1 = paramInt;
    if (paramInt == 0) {
      i1 = 8388659;
    }
    return i1;
  }
  
  private WindowInsetsCompat b(WindowInsetsCompat paramWindowInsetsCompat)
  {
    if (paramWindowInsetsCompat.e()) {
      return paramWindowInsetsCompat;
    }
    int i1 = 0;
    int i2 = getChildCount();
    for (;;)
    {
      WindowInsetsCompat localWindowInsetsCompat = paramWindowInsetsCompat;
      if (i1 < i2)
      {
        View localView = getChildAt(i1);
        localWindowInsetsCompat = paramWindowInsetsCompat;
        if (ViewCompat.x(localView))
        {
          Behavior localBehavior = ((LayoutParams)localView.getLayoutParams()).b();
          localWindowInsetsCompat = paramWindowInsetsCompat;
          if (localBehavior != null)
          {
            paramWindowInsetsCompat = localBehavior.a(this, localView, paramWindowInsetsCompat);
            localWindowInsetsCompat = paramWindowInsetsCompat;
            if (paramWindowInsetsCompat.e()) {
              localWindowInsetsCompat = paramWindowInsetsCompat;
            }
          }
        }
      }
      else
      {
        return localWindowInsetsCompat;
      }
      i1 += 1;
      paramWindowInsetsCompat = localWindowInsetsCompat;
    }
  }
  
  private void b(View paramView, int paramInt1, int paramInt2)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    int i6 = GravityCompat.a(c(localLayoutParams.c), paramInt2);
    int i5 = getWidth();
    int i4 = getHeight();
    int i2 = paramView.getMeasuredWidth();
    int i3 = paramView.getMeasuredHeight();
    int i1 = paramInt1;
    if (paramInt2 == 1) {
      i1 = i5 - paramInt1;
    }
    paramInt1 = a(i1) - i2;
    paramInt2 = 0;
    switch (i6 & 0x7)
    {
    default: 
      switch (i6 & 0x70)
      {
      }
      break;
    }
    for (;;)
    {
      paramInt1 = Math.max(getPaddingLeft() + localLayoutParams.leftMargin, Math.min(paramInt1, i5 - getPaddingRight() - i2 - localLayoutParams.rightMargin));
      paramInt2 = Math.max(getPaddingTop() + localLayoutParams.topMargin, Math.min(paramInt2, i4 - getPaddingBottom() - i3 - localLayoutParams.bottomMargin));
      paramView.layout(paramInt1, paramInt2, paramInt1 + i2, paramInt2 + i3);
      return;
      paramInt1 += i2;
      break;
      paramInt1 += i2 / 2;
      break;
      paramInt2 = 0 + i3;
      continue;
      paramInt2 = 0 + i3 / 2;
    }
  }
  
  private static int c(int paramInt)
  {
    int i1 = paramInt;
    if (paramInt == 0) {
      i1 = 8388661;
    }
    return i1;
  }
  
  private void c(View paramView, int paramInt)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    Rect localRect1 = this.j;
    localRect1.set(getPaddingLeft() + localLayoutParams.leftMargin, getPaddingTop() + localLayoutParams.topMargin, getWidth() - getPaddingRight() - localLayoutParams.rightMargin, getHeight() - getPaddingBottom() - localLayoutParams.bottomMargin);
    if ((this.w != null) && (ViewCompat.x(this)) && (!ViewCompat.x(paramView)))
    {
      localRect1.left += this.w.a();
      localRect1.top += this.w.b();
      localRect1.right -= this.w.c();
      localRect1.bottom -= this.w.d();
    }
    Rect localRect2 = this.k;
    GravityCompat.a(b(localLayoutParams.c), paramView.getMeasuredWidth(), paramView.getMeasuredHeight(), localRect1, localRect2, paramInt);
    paramView.layout(localRect2.left, localRect2.top, localRect2.right, localRect2.bottom);
  }
  
  private static int d(int paramInt)
  {
    int i1 = paramInt;
    if (paramInt == 0) {
      i1 = 17;
    }
    return i1;
  }
  
  private void e()
  {
    if (this.r != null)
    {
      Behavior localBehavior = ((LayoutParams)this.r.getLayoutParams()).b();
      if (localBehavior != null)
      {
        long l1 = SystemClock.uptimeMillis();
        MotionEvent localMotionEvent = MotionEvent.obtain(l1, l1, 3, 0.0F, 0.0F, 0);
        localBehavior.b(this, this.r, localMotionEvent);
        localMotionEvent.recycle();
      }
      this.r = null;
    }
    int i2 = getChildCount();
    int i1 = 0;
    while (i1 < i2)
    {
      ((LayoutParams)getChildAt(i1).getLayoutParams()).f();
      i1 += 1;
    }
    this.o = false;
  }
  
  private void f()
  {
    this.g.clear();
    int i1 = 0;
    int i2 = getChildCount();
    while (i1 < i2)
    {
      View localView = getChildAt(i1);
      a(localView).b(this, localView);
      this.g.add(localView);
      i1 += 1;
    }
    a(this.g, this.d);
  }
  
  public LayoutParams a(AttributeSet paramAttributeSet)
  {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  LayoutParams a(View paramView)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    View localView;
    if (!localLayoutParams.b)
    {
      Class localClass = paramView.getClass();
      paramView = null;
      for (;;)
      {
        localView = paramView;
        if (localClass == null) {
          break;
        }
        paramView = (DefaultBehavior)localClass.getAnnotation(DefaultBehavior.class);
        localView = paramView;
        if (paramView != null) {
          break;
        }
        localClass = localClass.getSuperclass();
      }
      if (localView == null) {}
    }
    try
    {
      localLayoutParams.a((Behavior)localView.a().newInstance());
      localLayoutParams.b = true;
      return localLayoutParams;
    }
    catch (Exception paramView)
    {
      for (;;)
      {
        Log.e("CoordinatorLayout", "Default behavior class " + localView.a().getName() + " could not be instantiated. Did you forget a default constructor?", paramView);
      }
    }
  }
  
  protected LayoutParams a(ViewGroup.LayoutParams paramLayoutParams)
  {
    if ((paramLayoutParams instanceof LayoutParams)) {
      return new LayoutParams((LayoutParams)paramLayoutParams);
    }
    if ((paramLayoutParams instanceof ViewGroup.MarginLayoutParams)) {
      return new LayoutParams((ViewGroup.MarginLayoutParams)paramLayoutParams);
    }
    return new LayoutParams(paramLayoutParams);
  }
  
  void a()
  {
    int i4 = 0;
    int i2 = getChildCount();
    int i1 = 0;
    for (;;)
    {
      int i3 = i4;
      if (i1 < i2)
      {
        if (e(getChildAt(i1))) {
          i3 = 1;
        }
      }
      else
      {
        if (i3 != this.v)
        {
          if (i3 == 0) {
            break;
          }
          b();
        }
        return;
      }
      i1 += 1;
    }
    c();
  }
  
  public void a(View paramView, int paramInt)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    if (localLayoutParams.d()) {
      throw new IllegalStateException("An anchor may not be changed after CoordinatorLayout measurement begins before layout is complete.");
    }
    if (localLayoutParams.g != null)
    {
      a(paramView, localLayoutParams.g, paramInt);
      return;
    }
    if (localLayoutParams.e >= 0)
    {
      b(paramView, localLayoutParams.e, paramInt);
      return;
    }
    c(paramView, paramInt);
  }
  
  public void a(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    measureChildWithMargins(paramView, paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  void a(View paramView, int paramInt, Rect paramRect1, Rect paramRect2)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    int i5 = GravityCompat.a(d(localLayoutParams.c), paramInt);
    int i1 = GravityCompat.a(b(localLayoutParams.d), paramInt);
    int i3 = paramView.getMeasuredWidth();
    int i4 = paramView.getMeasuredHeight();
    label122:
    int i2;
    switch (i1 & 0x7)
    {
    default: 
      paramInt = paramRect1.left;
      switch (i1 & 0x70)
      {
      default: 
        i1 = paramRect1.top;
        i2 = paramInt;
        switch (i5 & 0x7)
        {
        default: 
          i2 = paramInt - i3;
        case 5: 
          label162:
          paramInt = i1;
          switch (i5 & 0x70)
          {
          }
          break;
        }
        break;
      }
      break;
    }
    for (paramInt = i1 - i4;; paramInt = i1 - i4 / 2)
    {
      i5 = getWidth();
      i1 = getHeight();
      i2 = Math.max(getPaddingLeft() + localLayoutParams.leftMargin, Math.min(i2, i5 - getPaddingRight() - i3 - localLayoutParams.rightMargin));
      paramInt = Math.max(getPaddingTop() + localLayoutParams.topMargin, Math.min(paramInt, i1 - getPaddingBottom() - i4 - localLayoutParams.bottomMargin));
      paramRect2.set(i2, paramInt, i2 + i3, paramInt + i4);
      return;
      paramInt = paramRect1.right;
      break;
      paramInt = paramRect1.left + paramRect1.width() / 2;
      break;
      i1 = paramRect1.bottom;
      break label122;
      i1 = paramRect1.top + paramRect1.height() / 2;
      break label122;
      i2 = paramInt - i3 / 2;
      break label162;
    }
  }
  
  void a(View paramView, Rect paramRect)
  {
    ViewGroupUtils.b(this, paramView, paramRect);
  }
  
  void a(View paramView, boolean paramBoolean, Rect paramRect)
  {
    if ((paramView.isLayoutRequested()) || (paramView.getVisibility() == 8))
    {
      paramRect.set(0, 0, 0, 0);
      return;
    }
    if (paramBoolean)
    {
      a(paramView, paramRect);
      return;
    }
    paramRect.set(paramView.getLeft(), paramView.getTop(), paramView.getRight(), paramView.getBottom());
  }
  
  void a(boolean paramBoolean)
  {
    int i3 = ViewCompat.h(this);
    int i4 = this.g.size();
    int i1 = 0;
    while (i1 < i4)
    {
      View localView = (View)this.g.get(i1);
      Object localObject1 = (LayoutParams)localView.getLayoutParams();
      int i2 = 0;
      while (i2 < i1)
      {
        localObject2 = (View)this.g.get(i2);
        if (((LayoutParams)localObject1).h == localObject2) {
          b(localView, i3);
        }
        i2 += 1;
      }
      localObject1 = this.j;
      Object localObject2 = this.k;
      c(localView, (Rect)localObject1);
      a(localView, true, (Rect)localObject2);
      if (((Rect)localObject1).equals(localObject2))
      {
        i1 += 1;
      }
      else
      {
        b(localView, (Rect)localObject2);
        i2 = i1 + 1;
        label155:
        Behavior localBehavior;
        if (i2 < i4)
        {
          localObject1 = (View)this.g.get(i2);
          localObject2 = (LayoutParams)((View)localObject1).getLayoutParams();
          localBehavior = ((LayoutParams)localObject2).b();
          if ((localBehavior != null) && (localBehavior.b(this, (View)localObject1, localView)))
          {
            if ((paramBoolean) || (!((LayoutParams)localObject2).i())) {
              break label235;
            }
            ((LayoutParams)localObject2).j();
          }
        }
        for (;;)
        {
          i2 += 1;
          break label155;
          break;
          label235:
          boolean bool = localBehavior.c(this, (View)localObject1, localView);
          if (paramBoolean) {
            ((LayoutParams)localObject2).b(bool);
          }
        }
      }
    }
  }
  
  public boolean a(View paramView, int paramInt1, int paramInt2)
  {
    Rect localRect = this.j;
    a(paramView, localRect);
    return localRect.contains(paramInt1, paramInt2);
  }
  
  public boolean a(View paramView1, View paramView2)
  {
    if ((paramView1.getVisibility() == 0) && (paramView2.getVisibility() == 0))
    {
      Rect localRect = this.j;
      if (paramView1.getParent() != this)
      {
        bool = true;
        a(paramView1, bool, localRect);
        paramView1 = this.k;
        if (paramView2.getParent() == this) {
          break label115;
        }
      }
      label115:
      for (boolean bool = true;; bool = false)
      {
        a(paramView2, bool, paramView1);
        if ((localRect.left > paramView1.right) || (localRect.top > paramView1.bottom) || (localRect.right < paramView1.left) || (localRect.bottom < paramView1.top)) {
          break label120;
        }
        return true;
        bool = false;
        break;
      }
      label120:
      return false;
    }
    return false;
  }
  
  void b()
  {
    if (this.p)
    {
      if (this.u == null) {
        this.u = new OnPreDrawListener();
      }
      getViewTreeObserver().addOnPreDrawListener(this.u);
    }
    this.v = true;
  }
  
  void b(View paramView)
  {
    int i4 = this.g.size();
    int i2 = 0;
    int i1 = 0;
    if (i1 < i4)
    {
      View localView = (View)this.g.get(i1);
      int i3;
      if (localView == paramView) {
        i3 = 1;
      }
      for (;;)
      {
        i1 += 1;
        i2 = i3;
        break;
        i3 = i2;
        if (i2 != 0)
        {
          LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
          Behavior localBehavior = localLayoutParams.b();
          i3 = i2;
          if (localBehavior != null)
          {
            i3 = i2;
            if (localLayoutParams.a(this, localView, paramView))
            {
              localBehavior.d(this, localView, paramView);
              i3 = i2;
            }
          }
        }
      }
    }
  }
  
  void b(View paramView, int paramInt)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    if (localLayoutParams.g != null)
    {
      Object localObject = this.j;
      Rect localRect1 = this.k;
      Rect localRect2 = this.l;
      a(localLayoutParams.g, (Rect)localObject);
      a(paramView, false, localRect1);
      a(paramView, paramInt, (Rect)localObject, localRect2);
      paramInt = localRect2.left - localRect1.left;
      int i1 = localRect2.top - localRect1.top;
      if (paramInt != 0) {
        paramView.offsetLeftAndRight(paramInt);
      }
      if (i1 != 0) {
        paramView.offsetTopAndBottom(i1);
      }
      if ((paramInt != 0) || (i1 != 0))
      {
        localObject = localLayoutParams.b();
        if (localObject != null) {
          ((Behavior)localObject).c(this, paramView, localLayoutParams.g);
        }
      }
    }
  }
  
  void b(View paramView, Rect paramRect)
  {
    ((LayoutParams)paramView.getLayoutParams()).a(paramRect);
  }
  
  void c()
  {
    if ((this.p) && (this.u != null)) {
      getViewTreeObserver().removeOnPreDrawListener(this.u);
    }
    this.v = false;
  }
  
  public void c(View paramView)
  {
    int i4 = this.g.size();
    int i2 = 0;
    int i1 = 0;
    if (i1 < i4)
    {
      View localView = (View)this.g.get(i1);
      int i3;
      if (localView == paramView) {
        i3 = 1;
      }
      for (;;)
      {
        i1 += 1;
        i2 = i3;
        break;
        i3 = i2;
        if (i2 != 0)
        {
          LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
          Behavior localBehavior = localLayoutParams.b();
          i3 = i2;
          if (localBehavior != null)
          {
            i3 = i2;
            if (localLayoutParams.a(this, localView, paramView))
            {
              localBehavior.c(this, localView, paramView);
              i3 = i2;
            }
          }
        }
      }
    }
  }
  
  void c(View paramView, Rect paramRect)
  {
    paramRect.set(((LayoutParams)paramView.getLayoutParams()).c());
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return ((paramLayoutParams instanceof LayoutParams)) && (super.checkLayoutParams(paramLayoutParams));
  }
  
  protected LayoutParams d()
  {
    return new LayoutParams(-2, -2);
  }
  
  public List<View> d(View paramView)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    List localList = this.i;
    localList.clear();
    int i2 = getChildCount();
    int i1 = 0;
    if (i1 < i2)
    {
      View localView = getChildAt(i1);
      if (localView == paramView) {}
      for (;;)
      {
        i1 += 1;
        break;
        if (localLayoutParams.a(this, paramView, localView)) {
          localList.add(localView);
        }
      }
    }
    return localList;
  }
  
  protected boolean drawChild(Canvas paramCanvas, View paramView, long paramLong)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    if ((localLayoutParams.a != null) && (localLayoutParams.a.d(this, paramView) > 0.0F))
    {
      if (this.n == null) {
        this.n = new Paint();
      }
      this.n.setColor(localLayoutParams.a.c(this, paramView));
      paramCanvas.drawRect(getPaddingLeft(), getPaddingTop(), getWidth() - getPaddingRight(), getHeight() - getPaddingBottom(), this.n);
    }
    return super.drawChild(paramCanvas, paramView, paramLong);
  }
  
  protected void drawableStateChanged()
  {
    super.drawableStateChanged();
    int[] arrayOfInt = getDrawableState();
    boolean bool2 = false;
    Drawable localDrawable = this.y;
    boolean bool1 = bool2;
    if (localDrawable != null)
    {
      bool1 = bool2;
      if (localDrawable.isStateful()) {
        bool1 = false | localDrawable.setState(arrayOfInt);
      }
    }
    if (bool1) {
      invalidate();
    }
  }
  
  boolean e(View paramView)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    if (localLayoutParams.g != null) {
      return true;
    }
    int i2 = getChildCount();
    int i1 = 0;
    if (i1 < i2)
    {
      View localView = getChildAt(i1);
      if (localView == paramView) {}
      while (!localLayoutParams.a(this, paramView, localView))
      {
        i1 += 1;
        break;
      }
      return true;
    }
    return false;
  }
  
  public int getNestedScrollAxes()
  {
    return this.A.a();
  }
  
  @Nullable
  public Drawable getStatusBarBackground()
  {
    return this.y;
  }
  
  protected int getSuggestedMinimumHeight()
  {
    return Math.max(super.getSuggestedMinimumHeight(), getPaddingTop() + getPaddingBottom());
  }
  
  protected int getSuggestedMinimumWidth()
  {
    return Math.max(super.getSuggestedMinimumWidth(), getPaddingLeft() + getPaddingRight());
  }
  
  public void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    e();
    if (this.v)
    {
      if (this.u == null) {
        this.u = new OnPreDrawListener();
      }
      getViewTreeObserver().addOnPreDrawListener(this.u);
    }
    if ((this.w == null) && (ViewCompat.x(this))) {
      ViewCompat.w(this);
    }
    this.p = true;
  }
  
  public void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    e();
    if ((this.v) && (this.u != null)) {
      getViewTreeObserver().removeOnPreDrawListener(this.u);
    }
    if (this.t != null) {
      onStopNestedScroll(this.t);
    }
    this.p = false;
  }
  
  public void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    if ((this.x) && (this.y != null)) {
      if (this.w == null) {
        break label61;
      }
    }
    label61:
    for (int i1 = this.w.b();; i1 = 0)
    {
      if (i1 > 0)
      {
        this.y.setBounds(0, 0, getWidth(), i1);
        this.y.draw(paramCanvas);
      }
      return;
    }
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    int i1 = MotionEventCompat.a(paramMotionEvent);
    if (i1 == 0) {
      e();
    }
    boolean bool = a(paramMotionEvent, 0);
    if (0 != 0) {
      throw new NullPointerException();
    }
    if ((i1 == 1) || (i1 == 3)) {
      e();
    }
    return bool;
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    paramInt2 = ViewCompat.h(this);
    paramInt3 = this.g.size();
    paramInt1 = 0;
    while (paramInt1 < paramInt3)
    {
      View localView = (View)this.g.get(paramInt1);
      Behavior localBehavior = ((LayoutParams)localView.getLayoutParams()).b();
      if ((localBehavior == null) || (!localBehavior.a(this, localView, paramInt2))) {
        a(localView, paramInt2);
      }
      paramInt1 += 1;
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    f();
    a();
    int i12 = getPaddingLeft();
    int i13 = getPaddingTop();
    int i14 = getPaddingRight();
    int i15 = getPaddingBottom();
    int i16 = ViewCompat.h(this);
    int i2;
    int i17;
    int i18;
    int i19;
    int i20;
    int i7;
    int i6;
    int i5;
    int i3;
    label103:
    int i4;
    label117:
    View localView;
    LayoutParams localLayoutParams;
    int i8;
    int i1;
    int i9;
    int i10;
    if (i16 == 1)
    {
      i2 = 1;
      i17 = View.MeasureSpec.getMode(paramInt1);
      i18 = View.MeasureSpec.getSize(paramInt1);
      i19 = View.MeasureSpec.getMode(paramInt2);
      i20 = View.MeasureSpec.getSize(paramInt2);
      i7 = getSuggestedMinimumWidth();
      i6 = getSuggestedMinimumHeight();
      i5 = 0;
      if ((this.w == null) || (!ViewCompat.x(this))) {
        break label465;
      }
      i3 = 1;
      int i21 = this.g.size();
      i4 = 0;
      if (i4 >= i21) {
        break label512;
      }
      localView = (View)this.g.get(i4);
      localLayoutParams = (LayoutParams)localView.getLayoutParams();
      i8 = 0;
      i1 = i8;
      if (localLayoutParams.e >= 0)
      {
        i1 = i8;
        if (i17 != 0)
        {
          i9 = a(localLayoutParams.e);
          i10 = GravityCompat.a(c(localLayoutParams.c), i16) & 0x7;
          if (((i10 != 3) || (i2 != 0)) && ((i10 != 5) || (i2 == 0))) {
            break label471;
          }
          i1 = Math.max(0, i18 - i14 - i9);
        }
      }
    }
    for (;;)
    {
      i9 = paramInt1;
      i10 = paramInt2;
      int i11 = i9;
      i8 = i10;
      if (i3 != 0)
      {
        i11 = i9;
        i8 = i10;
        if (!ViewCompat.x(localView))
        {
          i10 = this.w.a();
          i11 = this.w.c();
          i8 = this.w.b();
          i9 = this.w.d();
          i11 = View.MeasureSpec.makeMeasureSpec(i18 - (i10 + i11), i17);
          i8 = View.MeasureSpec.makeMeasureSpec(i20 - (i8 + i9), i19);
        }
      }
      Behavior localBehavior = localLayoutParams.b();
      if ((localBehavior == null) || (!localBehavior.a(this, localView, i11, i1, i8, 0))) {
        a(localView, i11, i1, i8, 0);
      }
      i7 = Math.max(i7, localView.getMeasuredWidth() + (i12 + i14) + localLayoutParams.leftMargin + localLayoutParams.rightMargin);
      i6 = Math.max(i6, localView.getMeasuredHeight() + (i13 + i15) + localLayoutParams.topMargin + localLayoutParams.bottomMargin);
      i5 = ViewCompat.a(i5, ViewCompat.l(localView));
      i4 += 1;
      break label117;
      i2 = 0;
      break;
      label465:
      i3 = 0;
      break label103;
      label471:
      if ((i10 != 5) || (i2 != 0))
      {
        i1 = i8;
        if (i10 == 3)
        {
          i1 = i8;
          if (i2 == 0) {}
        }
      }
      else
      {
        i1 = Math.max(0, i9 - i12);
      }
    }
    label512:
    setMeasuredDimension(ViewCompat.a(i7, paramInt1, 0xFF000000 & i5), ViewCompat.a(i6, paramInt2, i5 << 16));
  }
  
  public boolean onNestedFling(View paramView, float paramFloat1, float paramFloat2, boolean paramBoolean)
  {
    boolean bool1 = false;
    int i2 = getChildCount();
    int i1 = 0;
    if (i1 < i2)
    {
      View localView = getChildAt(i1);
      Object localObject = (LayoutParams)localView.getLayoutParams();
      boolean bool2;
      if (!((LayoutParams)localObject).h()) {
        bool2 = bool1;
      }
      for (;;)
      {
        i1 += 1;
        bool1 = bool2;
        break;
        localObject = ((LayoutParams)localObject).b();
        bool2 = bool1;
        if (localObject != null) {
          bool2 = bool1 | ((Behavior)localObject).a(this, localView, paramView, paramFloat1, paramFloat2, paramBoolean);
        }
      }
    }
    if (bool1) {
      a(true);
    }
    return bool1;
  }
  
  public boolean onNestedPreFling(View paramView, float paramFloat1, float paramFloat2)
  {
    boolean bool1 = false;
    int i2 = getChildCount();
    int i1 = 0;
    if (i1 < i2)
    {
      View localView = getChildAt(i1);
      Object localObject = (LayoutParams)localView.getLayoutParams();
      boolean bool2;
      if (!((LayoutParams)localObject).h()) {
        bool2 = bool1;
      }
      for (;;)
      {
        i1 += 1;
        bool1 = bool2;
        break;
        localObject = ((LayoutParams)localObject).b();
        bool2 = bool1;
        if (localObject != null) {
          bool2 = bool1 | ((Behavior)localObject).a(this, localView, paramView, paramFloat1, paramFloat2);
        }
      }
    }
    return bool1;
  }
  
  public void onNestedPreScroll(View paramView, int paramInt1, int paramInt2, int[] paramArrayOfInt)
  {
    int i1 = 0;
    int i2 = 0;
    int i4 = 0;
    int i7 = getChildCount();
    int i3 = 0;
    if (i3 < i7)
    {
      View localView = getChildAt(i3);
      Object localObject = (LayoutParams)localView.getLayoutParams();
      int i6;
      int i5;
      if (!((LayoutParams)localObject).h())
      {
        i6 = i2;
        i5 = i1;
      }
      do
      {
        i3 += 1;
        i1 = i5;
        i2 = i6;
        break;
        localObject = ((LayoutParams)localObject).b();
        i5 = i1;
        i6 = i2;
      } while (localObject == null);
      int[] arrayOfInt = this.m;
      this.m[1] = 0;
      arrayOfInt[0] = 0;
      ((Behavior)localObject).a(this, localView, paramView, paramInt1, paramInt2, this.m);
      if (paramInt1 > 0)
      {
        i1 = Math.max(i1, this.m[0]);
        label146:
        if (paramInt2 <= 0) {
          break label193;
        }
      }
      label193:
      for (i2 = Math.max(i2, this.m[1]);; i2 = Math.min(i2, this.m[1]))
      {
        i4 = 1;
        i5 = i1;
        i6 = i2;
        break;
        i1 = Math.min(i1, this.m[0]);
        break label146;
      }
    }
    paramArrayOfInt[0] = i1;
    paramArrayOfInt[1] = i2;
    if (i4 != 0) {
      a(true);
    }
  }
  
  public void onNestedScroll(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i3 = getChildCount();
    int i2 = 0;
    int i1 = 0;
    if (i1 < i3)
    {
      View localView = getChildAt(i1);
      Object localObject = (LayoutParams)localView.getLayoutParams();
      if (!((LayoutParams)localObject).h()) {}
      for (;;)
      {
        i1 += 1;
        break;
        localObject = ((LayoutParams)localObject).b();
        if (localObject != null)
        {
          ((Behavior)localObject).a(this, localView, paramView, paramInt1, paramInt2, paramInt3, paramInt4);
          i2 = 1;
        }
      }
    }
    if (i2 != 0) {
      a(true);
    }
  }
  
  public void onNestedScrollAccepted(View paramView1, View paramView2, int paramInt)
  {
    this.A.a(paramView1, paramView2, paramInt);
    this.s = paramView1;
    this.t = paramView2;
    int i2 = getChildCount();
    int i1 = 0;
    if (i1 < i2)
    {
      View localView = getChildAt(i1);
      Object localObject = (LayoutParams)localView.getLayoutParams();
      if (!((LayoutParams)localObject).h()) {}
      for (;;)
      {
        i1 += 1;
        break;
        localObject = ((LayoutParams)localObject).b();
        if (localObject != null) {
          ((Behavior)localObject).b(this, localView, paramView1, paramView2, paramInt);
        }
      }
    }
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    if (!(paramParcelable instanceof SavedState)) {
      super.onRestoreInstanceState(paramParcelable);
    }
    for (;;)
    {
      return;
      paramParcelable = (SavedState)paramParcelable;
      super.onRestoreInstanceState(paramParcelable.getSuperState());
      paramParcelable = paramParcelable.a;
      int i1 = 0;
      int i2 = getChildCount();
      while (i1 < i2)
      {
        View localView = getChildAt(i1);
        int i3 = localView.getId();
        Behavior localBehavior = a(localView).b();
        if ((i3 != -1) && (localBehavior != null))
        {
          Parcelable localParcelable = (Parcelable)paramParcelable.get(i3);
          if (localParcelable != null) {
            localBehavior.a(this, localView, localParcelable);
          }
        }
        i1 += 1;
      }
    }
  }
  
  protected Parcelable onSaveInstanceState()
  {
    SavedState localSavedState = new SavedState(super.onSaveInstanceState());
    SparseArray localSparseArray = new SparseArray();
    int i1 = 0;
    int i2 = getChildCount();
    while (i1 < i2)
    {
      Object localObject = getChildAt(i1);
      int i3 = ((View)localObject).getId();
      Behavior localBehavior = ((LayoutParams)((View)localObject).getLayoutParams()).b();
      if ((i3 != -1) && (localBehavior != null))
      {
        localObject = localBehavior.b(this, (View)localObject);
        if (localObject != null) {
          localSparseArray.append(i3, localObject);
        }
      }
      i1 += 1;
    }
    localSavedState.a = localSparseArray;
    return localSavedState;
  }
  
  public boolean onStartNestedScroll(View paramView1, View paramView2, int paramInt)
  {
    boolean bool1 = false;
    int i2 = getChildCount();
    int i1 = 0;
    if (i1 < i2)
    {
      View localView = getChildAt(i1);
      LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
      Behavior localBehavior = localLayoutParams.b();
      if (localBehavior != null)
      {
        boolean bool2 = localBehavior.a(this, localView, paramView1, paramView2, paramInt);
        bool1 |= bool2;
        localLayoutParams.a(bool2);
      }
      for (;;)
      {
        i1 += 1;
        break;
        localLayoutParams.a(false);
      }
    }
    return bool1;
  }
  
  public void onStopNestedScroll(View paramView)
  {
    this.A.a(paramView);
    int i2 = getChildCount();
    int i1 = 0;
    if (i1 < i2)
    {
      View localView = getChildAt(i1);
      LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
      if (!localLayoutParams.h()) {}
      for (;;)
      {
        i1 += 1;
        break;
        Behavior localBehavior = localLayoutParams.b();
        if (localBehavior != null) {
          localBehavior.a(this, localView, paramView);
        }
        localLayoutParams.g();
        localLayoutParams.j();
      }
    }
    this.s = null;
    this.t = null;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    boolean bool4 = false;
    boolean bool2 = false;
    Object localObject1 = null;
    Object localObject2 = null;
    int i1 = MotionEventCompat.a(paramMotionEvent);
    boolean bool3;
    boolean bool1;
    if (this.r == null)
    {
      bool2 = a(paramMotionEvent, 1);
      bool3 = bool2;
      bool1 = bool4;
      if (!bool2) {}
    }
    else
    {
      Behavior localBehavior = ((LayoutParams)this.r.getLayoutParams()).b();
      bool3 = bool2;
      bool1 = bool4;
      if (localBehavior != null)
      {
        bool1 = localBehavior.b(this, this.r, paramMotionEvent);
        bool3 = bool2;
      }
    }
    if (this.r == null)
    {
      bool2 = bool1 | super.onTouchEvent(paramMotionEvent);
      paramMotionEvent = (MotionEvent)localObject2;
    }
    for (;;)
    {
      if (((bool2) || (i1 != 0)) || (paramMotionEvent != null)) {
        paramMotionEvent.recycle();
      }
      if ((i1 == 1) || (i1 == 3)) {
        e();
      }
      return bool2;
      paramMotionEvent = (MotionEvent)localObject2;
      bool2 = bool1;
      if (bool3)
      {
        paramMotionEvent = (MotionEvent)localObject1;
        if (0 == 0)
        {
          long l1 = SystemClock.uptimeMillis();
          paramMotionEvent = MotionEvent.obtain(l1, l1, 3, 0.0F, 0.0F, 0);
        }
        super.onTouchEvent(paramMotionEvent);
        bool2 = bool1;
      }
    }
  }
  
  public void requestDisallowInterceptTouchEvent(boolean paramBoolean)
  {
    super.requestDisallowInterceptTouchEvent(paramBoolean);
    if ((paramBoolean) && (!this.o))
    {
      e();
      this.o = true;
    }
  }
  
  public void setOnHierarchyChangeListener(ViewGroup.OnHierarchyChangeListener paramOnHierarchyChangeListener)
  {
    this.z = paramOnHierarchyChangeListener;
  }
  
  public void setStatusBarBackground(@Nullable Drawable paramDrawable)
  {
    Drawable localDrawable = null;
    if (this.y != paramDrawable)
    {
      if (this.y != null) {
        this.y.setCallback(null);
      }
      if (paramDrawable != null) {
        localDrawable = paramDrawable.mutate();
      }
      this.y = localDrawable;
      if (this.y != null)
      {
        if (this.y.isStateful()) {
          this.y.setState(getDrawableState());
        }
        DrawableCompat.b(this.y, ViewCompat.h(this));
        paramDrawable = this.y;
        if (getVisibility() != 0) {
          break label113;
        }
      }
    }
    label113:
    for (boolean bool = true;; bool = false)
    {
      paramDrawable.setVisible(bool, false);
      this.y.setCallback(this);
      ViewCompat.d(this);
      return;
    }
  }
  
  public void setStatusBarBackgroundColor(@ColorInt int paramInt)
  {
    setStatusBarBackground(new ColorDrawable(paramInt));
  }
  
  public void setStatusBarBackgroundResource(@DrawableRes int paramInt)
  {
    if (paramInt != 0) {}
    for (Drawable localDrawable = ContextCompat.getDrawable(getContext(), paramInt);; localDrawable = null)
    {
      setStatusBarBackground(localDrawable);
      return;
    }
  }
  
  public void setVisibility(int paramInt)
  {
    super.setVisibility(paramInt);
    if (paramInt == 0) {}
    for (boolean bool = true;; bool = false)
    {
      if ((this.y != null) && (this.y.isVisible() != bool)) {
        this.y.setVisible(bool, false);
      }
      return;
    }
  }
  
  protected boolean verifyDrawable(Drawable paramDrawable)
  {
    return (super.verifyDrawable(paramDrawable)) || (paramDrawable == this.y);
  }
  
  private class ApplyInsetsListener
    implements OnApplyWindowInsetsListener
  {
    private ApplyInsetsListener() {}
    
    public WindowInsetsCompat a(View paramView, WindowInsetsCompat paramWindowInsetsCompat)
    {
      return CoordinatorLayout.a(CoordinatorLayout.this, paramWindowInsetsCompat);
    }
  }
  
  public static abstract class Behavior<V extends View>
  {
    public Behavior() {}
    
    public Behavior(Context paramContext, AttributeSet paramAttributeSet) {}
    
    public WindowInsetsCompat a(CoordinatorLayout paramCoordinatorLayout, V paramV, WindowInsetsCompat paramWindowInsetsCompat)
    {
      return paramWindowInsetsCompat;
    }
    
    public void a(CoordinatorLayout paramCoordinatorLayout, V paramV, Parcelable paramParcelable) {}
    
    public void a(CoordinatorLayout paramCoordinatorLayout, V paramV, View paramView) {}
    
    public void a(CoordinatorLayout paramCoordinatorLayout, V paramV, View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {}
    
    public void a(CoordinatorLayout paramCoordinatorLayout, V paramV, View paramView, int paramInt1, int paramInt2, int[] paramArrayOfInt) {}
    
    public boolean a(CoordinatorLayout paramCoordinatorLayout, V paramV, int paramInt)
    {
      return false;
    }
    
    public boolean a(CoordinatorLayout paramCoordinatorLayout, V paramV, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      return false;
    }
    
    public boolean a(CoordinatorLayout paramCoordinatorLayout, V paramV, MotionEvent paramMotionEvent)
    {
      return false;
    }
    
    public boolean a(CoordinatorLayout paramCoordinatorLayout, V paramV, View paramView, float paramFloat1, float paramFloat2)
    {
      return false;
    }
    
    public boolean a(CoordinatorLayout paramCoordinatorLayout, V paramV, View paramView, float paramFloat1, float paramFloat2, boolean paramBoolean)
    {
      return false;
    }
    
    public boolean a(CoordinatorLayout paramCoordinatorLayout, V paramV, View paramView1, View paramView2, int paramInt)
    {
      return false;
    }
    
    public Parcelable b(CoordinatorLayout paramCoordinatorLayout, V paramV)
    {
      return View.BaseSavedState.EMPTY_STATE;
    }
    
    public void b(CoordinatorLayout paramCoordinatorLayout, V paramV, View paramView1, View paramView2, int paramInt) {}
    
    public boolean b(CoordinatorLayout paramCoordinatorLayout, V paramV, MotionEvent paramMotionEvent)
    {
      return false;
    }
    
    public boolean b(CoordinatorLayout paramCoordinatorLayout, V paramV, View paramView)
    {
      return false;
    }
    
    public int c(CoordinatorLayout paramCoordinatorLayout, V paramV)
    {
      return -16777216;
    }
    
    public boolean c(CoordinatorLayout paramCoordinatorLayout, V paramV, View paramView)
    {
      return false;
    }
    
    public float d(CoordinatorLayout paramCoordinatorLayout, V paramV)
    {
      return 0.0F;
    }
    
    public void d(CoordinatorLayout paramCoordinatorLayout, V paramV, View paramView) {}
    
    public boolean e(CoordinatorLayout paramCoordinatorLayout, V paramV)
    {
      return d(paramCoordinatorLayout, paramV) > 0.0F;
    }
  }
  
  @Retention(RetentionPolicy.RUNTIME)
  public static @interface DefaultBehavior
  {
    Class<? extends CoordinatorLayout.Behavior> a();
  }
  
  private class HierarchyChangeListener
    implements ViewGroup.OnHierarchyChangeListener
  {
    private HierarchyChangeListener() {}
    
    public void onChildViewAdded(View paramView1, View paramView2)
    {
      if (CoordinatorLayout.a(CoordinatorLayout.this) != null) {
        CoordinatorLayout.a(CoordinatorLayout.this).onChildViewAdded(paramView1, paramView2);
      }
    }
    
    public void onChildViewRemoved(View paramView1, View paramView2)
    {
      CoordinatorLayout.this.b(paramView2);
      if (CoordinatorLayout.a(CoordinatorLayout.this) != null) {
        CoordinatorLayout.a(CoordinatorLayout.this).onChildViewRemoved(paramView1, paramView2);
      }
    }
  }
  
  public static class LayoutParams
    extends ViewGroup.MarginLayoutParams
  {
    CoordinatorLayout.Behavior a;
    boolean b = false;
    public int c = 0;
    public int d = 0;
    public int e = -1;
    int f = -1;
    View g;
    View h;
    final Rect i = new Rect();
    Object j;
    private boolean k;
    private boolean l;
    private boolean m;
    
    public LayoutParams(int paramInt1, int paramInt2)
    {
      super(paramInt2);
    }
    
    LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
      TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CoordinatorLayout_LayoutParams);
      this.c = localTypedArray.getInteger(R.styleable.CoordinatorLayout_LayoutParams_android_layout_gravity, 0);
      this.f = localTypedArray.getResourceId(R.styleable.CoordinatorLayout_LayoutParams_layout_anchor, -1);
      this.d = localTypedArray.getInteger(R.styleable.CoordinatorLayout_LayoutParams_layout_anchorGravity, 0);
      this.e = localTypedArray.getInteger(R.styleable.CoordinatorLayout_LayoutParams_layout_keyline, -1);
      this.b = localTypedArray.hasValue(R.styleable.CoordinatorLayout_LayoutParams_layout_behavior);
      if (this.b) {
        this.a = CoordinatorLayout.a(paramContext, paramAttributeSet, localTypedArray.getString(R.styleable.CoordinatorLayout_LayoutParams_layout_behavior));
      }
      localTypedArray.recycle();
    }
    
    public LayoutParams(LayoutParams paramLayoutParams)
    {
      super();
    }
    
    public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
    }
    
    public LayoutParams(ViewGroup.MarginLayoutParams paramMarginLayoutParams)
    {
      super();
    }
    
    private void a(View paramView, CoordinatorLayout paramCoordinatorLayout)
    {
      this.g = paramCoordinatorLayout.findViewById(this.f);
      if (this.g != null)
      {
        if (this.g == paramCoordinatorLayout)
        {
          if (paramCoordinatorLayout.isInEditMode())
          {
            this.h = null;
            this.g = null;
            return;
          }
          throw new IllegalStateException("View can not be anchored to the the parent CoordinatorLayout");
        }
        View localView = this.g;
        for (ViewParent localViewParent = this.g.getParent(); (localViewParent != paramCoordinatorLayout) && (localViewParent != null); localViewParent = localViewParent.getParent())
        {
          if (localViewParent == paramView)
          {
            if (paramCoordinatorLayout.isInEditMode())
            {
              this.h = null;
              this.g = null;
              return;
            }
            throw new IllegalStateException("Anchor must not be a descendant of the anchored view");
          }
          if ((localViewParent instanceof View)) {
            localView = (View)localViewParent;
          }
        }
        this.h = localView;
        return;
      }
      if (paramCoordinatorLayout.isInEditMode())
      {
        this.h = null;
        this.g = null;
        return;
      }
      throw new IllegalStateException("Could not find CoordinatorLayout descendant view with id " + paramCoordinatorLayout.getResources().getResourceName(this.f) + " to anchor view " + paramView);
    }
    
    private boolean b(View paramView, CoordinatorLayout paramCoordinatorLayout)
    {
      if (this.g.getId() != this.f) {
        return false;
      }
      View localView = this.g;
      for (ViewParent localViewParent = this.g.getParent(); localViewParent != paramCoordinatorLayout; localViewParent = localViewParent.getParent())
      {
        if ((localViewParent == null) || (localViewParent == paramView))
        {
          this.h = null;
          this.g = null;
          return false;
        }
        if ((localViewParent instanceof View)) {
          localView = (View)localViewParent;
        }
      }
      this.h = localView;
      return true;
    }
    
    public int a()
    {
      return this.f;
    }
    
    void a(Rect paramRect)
    {
      this.i.set(paramRect);
    }
    
    public void a(CoordinatorLayout.Behavior paramBehavior)
    {
      if (this.a != paramBehavior)
      {
        this.a = paramBehavior;
        this.j = null;
        this.b = true;
      }
    }
    
    void a(boolean paramBoolean)
    {
      this.l = paramBoolean;
    }
    
    boolean a(CoordinatorLayout paramCoordinatorLayout, View paramView)
    {
      if (this.k) {
        return true;
      }
      boolean bool2 = this.k;
      if (this.a != null) {}
      for (boolean bool1 = this.a.e(paramCoordinatorLayout, paramView);; bool1 = false)
      {
        bool1 |= bool2;
        this.k = bool1;
        return bool1;
      }
    }
    
    boolean a(CoordinatorLayout paramCoordinatorLayout, View paramView1, View paramView2)
    {
      return (paramView2 == this.h) || ((this.a != null) && (this.a.b(paramCoordinatorLayout, paramView1, paramView2)));
    }
    
    public CoordinatorLayout.Behavior b()
    {
      return this.a;
    }
    
    View b(CoordinatorLayout paramCoordinatorLayout, View paramView)
    {
      if (this.f == -1)
      {
        this.h = null;
        this.g = null;
        return null;
      }
      if ((this.g == null) || (!b(paramView, paramCoordinatorLayout))) {
        a(paramView, paramCoordinatorLayout);
      }
      return this.g;
    }
    
    void b(boolean paramBoolean)
    {
      this.m = paramBoolean;
    }
    
    Rect c()
    {
      return this.i;
    }
    
    boolean d()
    {
      return (this.g == null) && (this.f != -1);
    }
    
    boolean e()
    {
      if (this.a == null) {
        this.k = false;
      }
      return this.k;
    }
    
    void f()
    {
      this.k = false;
    }
    
    void g()
    {
      this.l = false;
    }
    
    boolean h()
    {
      return this.l;
    }
    
    boolean i()
    {
      return this.m;
    }
    
    void j()
    {
      this.m = false;
    }
  }
  
  class OnPreDrawListener
    implements ViewTreeObserver.OnPreDrawListener
  {
    OnPreDrawListener() {}
    
    public boolean onPreDraw()
    {
      CoordinatorLayout.this.a(false);
      return true;
    }
  }
  
  protected static class SavedState
    extends View.BaseSavedState
  {
    public static final Parcelable.Creator<SavedState> CREATOR = ParcelableCompat.a(new ParcelableCompatCreatorCallbacks()
    {
      public CoordinatorLayout.SavedState a(Parcel paramAnonymousParcel, ClassLoader paramAnonymousClassLoader)
      {
        return new CoordinatorLayout.SavedState(paramAnonymousParcel, paramAnonymousClassLoader);
      }
      
      public CoordinatorLayout.SavedState[] a(int paramAnonymousInt)
      {
        return new CoordinatorLayout.SavedState[paramAnonymousInt];
      }
    });
    SparseArray<Parcelable> a;
    
    public SavedState(Parcel paramParcel, ClassLoader paramClassLoader)
    {
      super();
      int j = paramParcel.readInt();
      int[] arrayOfInt = new int[j];
      paramParcel.readIntArray(arrayOfInt);
      paramParcel = paramParcel.readParcelableArray(paramClassLoader);
      this.a = new SparseArray(j);
      int i = 0;
      while (i < j)
      {
        this.a.append(arrayOfInt[i], paramParcel[i]);
        i += 1;
      }
    }
    
    public SavedState(Parcelable paramParcelable)
    {
      super();
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      super.writeToParcel(paramParcel, paramInt);
      if (this.a != null) {}
      int[] arrayOfInt;
      Parcelable[] arrayOfParcelable;
      for (int i = this.a.size();; i = 0)
      {
        paramParcel.writeInt(i);
        arrayOfInt = new int[i];
        arrayOfParcelable = new Parcelable[i];
        int j = 0;
        while (j < i)
        {
          arrayOfInt[j] = this.a.keyAt(j);
          arrayOfParcelable[j] = ((Parcelable)this.a.valueAt(j));
          j += 1;
        }
      }
      paramParcel.writeIntArray(arrayOfInt);
      paramParcel.writeParcelableArray(arrayOfParcelable, paramInt);
    }
  }
  
  static class ViewElevationComparator
    implements Comparator<View>
  {
    public int a(View paramView1, View paramView2)
    {
      float f1 = ViewCompat.G(paramView1);
      float f2 = ViewCompat.G(paramView2);
      if (f1 > f2) {
        return -1;
      }
      if (f1 < f2) {
        return 1;
      }
      return 0;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/design/widget/CoordinatorLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */