package android.support.design.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.design.R.id;
import android.support.design.R.style;
import android.support.design.R.styleable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.OnApplyWindowInsetsListener;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.WindowInsetsCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;

public class CollapsingToolbarLayout
  extends FrameLayout
{
  private boolean a = true;
  private int b;
  private Toolbar c;
  private View d;
  private View e;
  private int f;
  private int g;
  private int h;
  private int i;
  private final Rect j = new Rect();
  private final CollapsingTextHelper k;
  private boolean l;
  private boolean m;
  private Drawable n;
  private Drawable o;
  private int p;
  private boolean q;
  private ValueAnimatorCompat r;
  private AppBarLayout.OnOffsetChangedListener s;
  private int t;
  private WindowInsetsCompat u;
  
  public CollapsingToolbarLayout(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public CollapsingToolbarLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public CollapsingToolbarLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    ThemeUtils.a(paramContext);
    this.k = new CollapsingTextHelper(this);
    this.k.a(AnimationUtils.e);
    paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CollapsingToolbarLayout, paramInt, R.style.Widget_Design_CollapsingToolbar);
    this.k.c(paramContext.getInt(R.styleable.CollapsingToolbarLayout_expandedTitleGravity, 8388691));
    this.k.d(paramContext.getInt(R.styleable.CollapsingToolbarLayout_collapsedTitleGravity, 8388627));
    paramInt = paramContext.getDimensionPixelSize(R.styleable.CollapsingToolbarLayout_expandedTitleMargin, 0);
    this.i = paramInt;
    this.h = paramInt;
    this.g = paramInt;
    this.f = paramInt;
    if (paramContext.hasValue(R.styleable.CollapsingToolbarLayout_expandedTitleMarginStart)) {
      this.f = paramContext.getDimensionPixelSize(R.styleable.CollapsingToolbarLayout_expandedTitleMarginStart, 0);
    }
    if (paramContext.hasValue(R.styleable.CollapsingToolbarLayout_expandedTitleMarginEnd)) {
      this.h = paramContext.getDimensionPixelSize(R.styleable.CollapsingToolbarLayout_expandedTitleMarginEnd, 0);
    }
    if (paramContext.hasValue(R.styleable.CollapsingToolbarLayout_expandedTitleMarginTop)) {
      this.g = paramContext.getDimensionPixelSize(R.styleable.CollapsingToolbarLayout_expandedTitleMarginTop, 0);
    }
    if (paramContext.hasValue(R.styleable.CollapsingToolbarLayout_expandedTitleMarginBottom)) {
      this.i = paramContext.getDimensionPixelSize(R.styleable.CollapsingToolbarLayout_expandedTitleMarginBottom, 0);
    }
    this.l = paramContext.getBoolean(R.styleable.CollapsingToolbarLayout_titleEnabled, true);
    setTitle(paramContext.getText(R.styleable.CollapsingToolbarLayout_title));
    this.k.f(R.style.TextAppearance_Design_CollapsingToolbar_Expanded);
    this.k.e(R.style.TextAppearance_AppCompat_Widget_ActionBar_Title);
    if (paramContext.hasValue(R.styleable.CollapsingToolbarLayout_expandedTitleTextAppearance)) {
      this.k.f(paramContext.getResourceId(R.styleable.CollapsingToolbarLayout_expandedTitleTextAppearance, 0));
    }
    if (paramContext.hasValue(R.styleable.CollapsingToolbarLayout_collapsedTitleTextAppearance)) {
      this.k.e(paramContext.getResourceId(R.styleable.CollapsingToolbarLayout_collapsedTitleTextAppearance, 0));
    }
    setContentScrim(paramContext.getDrawable(R.styleable.CollapsingToolbarLayout_contentScrim));
    setStatusBarScrim(paramContext.getDrawable(R.styleable.CollapsingToolbarLayout_statusBarScrim));
    this.b = paramContext.getResourceId(R.styleable.CollapsingToolbarLayout_toolbarId, -1);
    paramContext.recycle();
    setWillNotDraw(false);
    ViewCompat.a(this, new OnApplyWindowInsetsListener()
    {
      public WindowInsetsCompat a(View paramAnonymousView, WindowInsetsCompat paramAnonymousWindowInsetsCompat)
      {
        return CollapsingToolbarLayout.a(CollapsingToolbarLayout.this, paramAnonymousWindowInsetsCompat);
      }
    });
  }
  
  private WindowInsetsCompat a(WindowInsetsCompat paramWindowInsetsCompat)
  {
    if (this.u != paramWindowInsetsCompat)
    {
      this.u = paramWindowInsetsCompat;
      requestLayout();
    }
    return paramWindowInsetsCompat.f();
  }
  
  private void a(int paramInt)
  {
    b();
    Interpolator localInterpolator;
    if (this.r == null)
    {
      this.r = ViewUtils.a();
      this.r.a(600);
      ValueAnimatorCompat localValueAnimatorCompat = this.r;
      if (paramInt > this.p)
      {
        localInterpolator = AnimationUtils.c;
        localValueAnimatorCompat.a(localInterpolator);
        this.r.a(new ValueAnimatorCompat.AnimatorUpdateListener()
        {
          public void a(ValueAnimatorCompat paramAnonymousValueAnimatorCompat)
          {
            CollapsingToolbarLayout.a(CollapsingToolbarLayout.this, paramAnonymousValueAnimatorCompat.c());
          }
        });
      }
    }
    for (;;)
    {
      this.r.a(this.p, paramInt);
      this.r.a();
      return;
      localInterpolator = AnimationUtils.d;
      break;
      if (this.r.b()) {
        this.r.cancel();
      }
    }
  }
  
  private View b(View paramView)
  {
    View localView = paramView;
    for (paramView = paramView.getParent(); (paramView != this) && (paramView != null); paramView = paramView.getParent()) {
      if ((paramView instanceof View)) {
        localView = (View)paramView;
      }
    }
    return localView;
  }
  
  private void b()
  {
    if (!this.a) {
      return;
    }
    this.c = null;
    this.d = null;
    if (this.b != -1)
    {
      this.c = ((Toolbar)findViewById(this.b));
      if (this.c != null) {
        this.d = b(this.c);
      }
    }
    Object localObject2;
    int i1;
    int i2;
    if (this.c == null)
    {
      localObject2 = null;
      i1 = 0;
      i2 = getChildCount();
    }
    for (;;)
    {
      Object localObject1 = localObject2;
      if (i1 < i2)
      {
        localObject1 = getChildAt(i1);
        if ((localObject1 instanceof Toolbar)) {
          localObject1 = (Toolbar)localObject1;
        }
      }
      else
      {
        this.c = ((Toolbar)localObject1);
        c();
        this.a = false;
        return;
      }
      i1 += 1;
    }
  }
  
  private static int c(@NonNull View paramView)
  {
    Object localObject = paramView.getLayoutParams();
    if ((localObject instanceof ViewGroup.MarginLayoutParams))
    {
      localObject = (ViewGroup.MarginLayoutParams)localObject;
      return paramView.getHeight() + ((ViewGroup.MarginLayoutParams)localObject).topMargin + ((ViewGroup.MarginLayoutParams)localObject).bottomMargin;
    }
    return paramView.getHeight();
  }
  
  private void c()
  {
    if ((!this.l) && (this.e != null))
    {
      ViewParent localViewParent = this.e.getParent();
      if ((localViewParent instanceof ViewGroup)) {
        ((ViewGroup)localViewParent).removeView(this.e);
      }
    }
    if ((this.l) && (this.c != null))
    {
      if (this.e == null) {
        this.e = new View(getContext());
      }
      if (this.e.getParent() == null) {
        this.c.addView(this.e, -1, -1);
      }
    }
  }
  
  private static ViewOffsetHelper d(View paramView)
  {
    ViewOffsetHelper localViewOffsetHelper2 = (ViewOffsetHelper)paramView.getTag(R.id.view_offset_helper);
    ViewOffsetHelper localViewOffsetHelper1 = localViewOffsetHelper2;
    if (localViewOffsetHelper2 == null)
    {
      localViewOffsetHelper1 = new ViewOffsetHelper(paramView);
      paramView.setTag(R.id.view_offset_helper, localViewOffsetHelper1);
    }
    return localViewOffsetHelper1;
  }
  
  private void setScrimAlpha(int paramInt)
  {
    if (paramInt != this.p)
    {
      if ((this.n != null) && (this.c != null)) {
        ViewCompat.d(this.c);
      }
      this.p = paramInt;
      ViewCompat.d(this);
    }
  }
  
  protected LayoutParams a()
  {
    return new LayoutParams(super.generateDefaultLayoutParams());
  }
  
  protected FrameLayout.LayoutParams a(ViewGroup.LayoutParams paramLayoutParams)
  {
    return new LayoutParams(paramLayoutParams);
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  public void draw(Canvas paramCanvas)
  {
    super.draw(paramCanvas);
    b();
    if ((this.c == null) && (this.n != null) && (this.p > 0))
    {
      this.n.mutate().setAlpha(this.p);
      this.n.draw(paramCanvas);
    }
    if ((this.l) && (this.m)) {
      this.k.a(paramCanvas);
    }
    if ((this.o != null) && (this.p > 0)) {
      if (this.u == null) {
        break label153;
      }
    }
    label153:
    for (int i1 = this.u.b();; i1 = 0)
    {
      if (i1 > 0)
      {
        this.o.setBounds(0, -this.t, getWidth(), i1 - this.t);
        this.o.mutate().setAlpha(this.p);
        this.o.draw(paramCanvas);
      }
      return;
    }
  }
  
  protected boolean drawChild(Canvas paramCanvas, View paramView, long paramLong)
  {
    b();
    if ((paramView == this.c) && (this.n != null) && (this.p > 0))
    {
      this.n.mutate().setAlpha(this.p);
      this.n.draw(paramCanvas);
    }
    return super.drawChild(paramCanvas, paramView, paramLong);
  }
  
  protected void drawableStateChanged()
  {
    super.drawableStateChanged();
    int[] arrayOfInt = getDrawableState();
    boolean bool2 = false;
    Drawable localDrawable = this.o;
    boolean bool1 = bool2;
    if (localDrawable != null)
    {
      bool1 = bool2;
      if (localDrawable.isStateful()) {
        bool1 = false | localDrawable.setState(arrayOfInt);
      }
    }
    localDrawable = this.n;
    bool2 = bool1;
    if (localDrawable != null)
    {
      bool2 = bool1;
      if (localDrawable.isStateful()) {
        bool2 = bool1 | localDrawable.setState(arrayOfInt);
      }
    }
    if (bool2) {
      invalidate();
    }
  }
  
  public FrameLayout.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
  {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  public int getCollapsedTitleGravity()
  {
    return this.k.c();
  }
  
  @NonNull
  public Typeface getCollapsedTitleTypeface()
  {
    return this.k.d();
  }
  
  @Nullable
  public Drawable getContentScrim()
  {
    return this.n;
  }
  
  public int getExpandedTitleGravity()
  {
    return this.k.b();
  }
  
  public int getExpandedTitleMarginBottom()
  {
    return this.i;
  }
  
  public int getExpandedTitleMarginEnd()
  {
    return this.h;
  }
  
  public int getExpandedTitleMarginStart()
  {
    return this.f;
  }
  
  public int getExpandedTitleMarginTop()
  {
    return this.g;
  }
  
  @NonNull
  public Typeface getExpandedTitleTypeface()
  {
    return this.k.e();
  }
  
  final int getScrimTriggerOffset()
  {
    return ViewCompat.r(this) * 2;
  }
  
  @Nullable
  public Drawable getStatusBarScrim()
  {
    return this.o;
  }
  
  @Nullable
  public CharSequence getTitle()
  {
    if (this.l) {
      return this.k.i();
    }
    return null;
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    ViewParent localViewParent = getParent();
    if ((localViewParent instanceof AppBarLayout))
    {
      if (this.s == null) {
        this.s = new OffsetUpdateListener(null);
      }
      ((AppBarLayout)localViewParent).a(this.s);
    }
    ViewCompat.w(this);
  }
  
  protected void onDetachedFromWindow()
  {
    ViewParent localViewParent = getParent();
    if ((this.s != null) && ((localViewParent instanceof AppBarLayout))) {
      ((AppBarLayout)localViewParent).b(this.s);
    }
    super.onDetachedFromWindow();
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    int i1;
    label162:
    Object localObject;
    label179:
    int i3;
    int i4;
    if ((this.l) && (this.e != null))
    {
      if ((!ViewCompat.H(this.e)) || (this.e.getVisibility() != 0)) {
        break label315;
      }
      paramBoolean = true;
      this.m = paramBoolean;
      if (this.m)
      {
        i2 = 0;
        i1 = i2;
        if (this.d != null)
        {
          i1 = i2;
          if (this.d != this) {
            i1 = ((LayoutParams)this.d.getLayoutParams()).bottomMargin;
          }
        }
        ViewGroupUtils.b(this, this.e, this.j);
        this.k.b(this.j.left, paramInt4 - this.j.height() - i1, this.j.right, paramInt4 - i1);
        if (ViewCompat.h(this) != 1) {
          break label320;
        }
        i2 = 1;
        localObject = this.k;
        if (i2 == 0) {
          break label326;
        }
        i1 = this.h;
        i3 = this.j.bottom;
        i4 = this.g;
        if (i2 == 0) {
          break label335;
        }
      }
    }
    label315:
    label320:
    label326:
    label335:
    for (int i2 = this.f;; i2 = this.h)
    {
      ((CollapsingTextHelper)localObject).a(i1, i4 + i3, paramInt3 - paramInt1 - i2, paramInt4 - paramInt2 - this.i);
      this.k.h();
      paramInt1 = 0;
      paramInt2 = getChildCount();
      while (paramInt1 < paramInt2)
      {
        localObject = getChildAt(paramInt1);
        if ((this.u != null) && (!ViewCompat.x((View)localObject)))
        {
          paramInt3 = this.u.b();
          if (((View)localObject).getTop() < paramInt3) {
            ViewCompat.e((View)localObject, paramInt3);
          }
        }
        d((View)localObject).a();
        paramInt1 += 1;
      }
      paramBoolean = false;
      break;
      i2 = 0;
      break label162;
      i1 = this.f;
      break label179;
    }
    if (this.c != null)
    {
      if ((this.l) && (TextUtils.isEmpty(this.k.i()))) {
        this.k.a(this.c.getTitle());
      }
      if ((this.d == null) || (this.d == this)) {
        setMinimumHeight(c(this.c));
      }
    }
    else
    {
      return;
    }
    setMinimumHeight(c(this.d));
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    b();
    super.onMeasure(paramInt1, paramInt2);
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    if (this.n != null) {
      this.n.setBounds(0, 0, paramInt1, paramInt2);
    }
  }
  
  public void setCollapsedTitleGravity(int paramInt)
  {
    this.k.d(paramInt);
  }
  
  public void setCollapsedTitleTextAppearance(@StyleRes int paramInt)
  {
    this.k.e(paramInt);
  }
  
  public void setCollapsedTitleTextColor(@ColorInt int paramInt)
  {
    this.k.a(paramInt);
  }
  
  public void setCollapsedTitleTypeface(@Nullable Typeface paramTypeface)
  {
    this.k.a(paramTypeface);
  }
  
  public void setContentScrim(@Nullable Drawable paramDrawable)
  {
    Drawable localDrawable = null;
    if (this.n != paramDrawable)
    {
      if (this.n != null) {
        this.n.setCallback(null);
      }
      if (paramDrawable != null) {
        localDrawable = paramDrawable.mutate();
      }
      this.n = localDrawable;
      if (this.n != null)
      {
        this.n.setBounds(0, 0, getWidth(), getHeight());
        this.n.setCallback(this);
        this.n.setAlpha(this.p);
      }
      ViewCompat.d(this);
    }
  }
  
  public void setContentScrimColor(@ColorInt int paramInt)
  {
    setContentScrim(new ColorDrawable(paramInt));
  }
  
  public void setContentScrimResource(@DrawableRes int paramInt)
  {
    setContentScrim(ContextCompat.getDrawable(getContext(), paramInt));
  }
  
  public void setExpandedTitleColor(@ColorInt int paramInt)
  {
    this.k.b(paramInt);
  }
  
  public void setExpandedTitleGravity(int paramInt)
  {
    this.k.c(paramInt);
  }
  
  public void setExpandedTitleMargin(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.f = paramInt1;
    this.g = paramInt2;
    this.h = paramInt3;
    this.i = paramInt4;
    requestLayout();
  }
  
  public void setExpandedTitleMarginBottom(int paramInt)
  {
    this.i = paramInt;
    requestLayout();
  }
  
  public void setExpandedTitleMarginEnd(int paramInt)
  {
    this.h = paramInt;
    requestLayout();
  }
  
  public void setExpandedTitleMarginStart(int paramInt)
  {
    this.f = paramInt;
    requestLayout();
  }
  
  public void setExpandedTitleMarginTop(int paramInt)
  {
    this.g = paramInt;
    requestLayout();
  }
  
  public void setExpandedTitleTextAppearance(@StyleRes int paramInt)
  {
    this.k.f(paramInt);
  }
  
  public void setExpandedTitleTypeface(@Nullable Typeface paramTypeface)
  {
    this.k.b(paramTypeface);
  }
  
  public void setScrimsShown(boolean paramBoolean)
  {
    if ((ViewCompat.F(this)) && (!isInEditMode())) {}
    for (boolean bool = true;; bool = false)
    {
      setScrimsShown(paramBoolean, bool);
      return;
    }
  }
  
  public void setScrimsShown(boolean paramBoolean1, boolean paramBoolean2)
  {
    int i1 = 255;
    if (this.q != paramBoolean1)
    {
      if (!paramBoolean2) {
        break label36;
      }
      if (!paramBoolean1) {
        break label31;
      }
    }
    for (;;)
    {
      a(i1);
      this.q = paramBoolean1;
      return;
      label31:
      i1 = 0;
    }
    label36:
    if (paramBoolean1) {}
    for (;;)
    {
      setScrimAlpha(i1);
      break;
      i1 = 0;
    }
  }
  
  public void setStatusBarScrim(@Nullable Drawable paramDrawable)
  {
    Drawable localDrawable = null;
    if (this.o != paramDrawable)
    {
      if (this.o != null) {
        this.o.setCallback(null);
      }
      if (paramDrawable != null) {
        localDrawable = paramDrawable.mutate();
      }
      this.o = localDrawable;
      if (this.o != null)
      {
        if (this.o.isStateful()) {
          this.o.setState(getDrawableState());
        }
        DrawableCompat.b(this.o, ViewCompat.h(this));
        paramDrawable = this.o;
        if (getVisibility() != 0) {
          break label124;
        }
      }
    }
    label124:
    for (boolean bool = true;; bool = false)
    {
      paramDrawable.setVisible(bool, false);
      this.o.setCallback(this);
      this.o.setAlpha(this.p);
      ViewCompat.d(this);
      return;
    }
  }
  
  public void setStatusBarScrimColor(@ColorInt int paramInt)
  {
    setStatusBarScrim(new ColorDrawable(paramInt));
  }
  
  public void setStatusBarScrimResource(@DrawableRes int paramInt)
  {
    setStatusBarScrim(ContextCompat.getDrawable(getContext(), paramInt));
  }
  
  public void setTitle(@Nullable CharSequence paramCharSequence)
  {
    this.k.a(paramCharSequence);
  }
  
  public void setTitleEnabled(boolean paramBoolean)
  {
    if (paramBoolean != this.l)
    {
      this.l = paramBoolean;
      c();
      requestLayout();
    }
  }
  
  public void setVisibility(int paramInt)
  {
    super.setVisibility(paramInt);
    if (paramInt == 0) {}
    for (boolean bool = true;; bool = false)
    {
      if ((this.o != null) && (this.o.isVisible() != bool)) {
        this.o.setVisible(bool, false);
      }
      if ((this.n != null) && (this.n.isVisible() != bool)) {
        this.n.setVisible(bool, false);
      }
      return;
    }
  }
  
  protected boolean verifyDrawable(Drawable paramDrawable)
  {
    return (super.verifyDrawable(paramDrawable)) || (paramDrawable == this.n) || (paramDrawable == this.o);
  }
  
  public static class LayoutParams
    extends FrameLayout.LayoutParams
  {
    int a = 0;
    float b = 0.5F;
    
    public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
      paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CollapsingAppBarLayout_LayoutParams);
      this.a = paramContext.getInt(R.styleable.CollapsingAppBarLayout_LayoutParams_layout_collapseMode, 0);
      a(paramContext.getFloat(R.styleable.CollapsingAppBarLayout_LayoutParams_layout_collapseParallaxMultiplier, 0.5F));
      paramContext.recycle();
    }
    
    public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
    }
    
    public LayoutParams(FrameLayout.LayoutParams paramLayoutParams)
    {
      super();
    }
    
    public void a(float paramFloat)
    {
      this.b = paramFloat;
    }
  }
  
  private class OffsetUpdateListener
    implements AppBarLayout.OnOffsetChangedListener
  {
    private OffsetUpdateListener() {}
    
    public void a(AppBarLayout paramAppBarLayout, int paramInt)
    {
      boolean bool = false;
      CollapsingToolbarLayout.b(CollapsingToolbarLayout.this, paramInt);
      int i;
      int k;
      label51:
      Object localObject;
      CollapsingToolbarLayout.LayoutParams localLayoutParams;
      ViewOffsetHelper localViewOffsetHelper;
      if (CollapsingToolbarLayout.a(CollapsingToolbarLayout.this) != null)
      {
        i = CollapsingToolbarLayout.a(CollapsingToolbarLayout.this).b();
        k = paramAppBarLayout.getTotalScrollRange();
        j = 0;
        m = CollapsingToolbarLayout.this.getChildCount();
        if (j >= m) {
          break label177;
        }
        localObject = CollapsingToolbarLayout.this.getChildAt(j);
        localLayoutParams = (CollapsingToolbarLayout.LayoutParams)((View)localObject).getLayoutParams();
        localViewOffsetHelper = CollapsingToolbarLayout.a((View)localObject);
        switch (localLayoutParams.a)
        {
        }
      }
      for (;;)
      {
        j += 1;
        break label51;
        i = 0;
        break;
        if (CollapsingToolbarLayout.this.getHeight() - i + paramInt >= ((View)localObject).getHeight())
        {
          localViewOffsetHelper.a(-paramInt);
          continue;
          localViewOffsetHelper.a(Math.round(-paramInt * localLayoutParams.b));
        }
      }
      label177:
      if ((CollapsingToolbarLayout.b(CollapsingToolbarLayout.this) != null) || (CollapsingToolbarLayout.c(CollapsingToolbarLayout.this) != null))
      {
        localObject = CollapsingToolbarLayout.this;
        if (CollapsingToolbarLayout.this.getHeight() + paramInt < CollapsingToolbarLayout.this.getScrimTriggerOffset() + i) {
          bool = true;
        }
        ((CollapsingToolbarLayout)localObject).setScrimsShown(bool);
      }
      if ((CollapsingToolbarLayout.c(CollapsingToolbarLayout.this) != null) && (i > 0)) {
        ViewCompat.d(CollapsingToolbarLayout.this);
      }
      int j = CollapsingToolbarLayout.this.getHeight();
      int m = ViewCompat.r(CollapsingToolbarLayout.this);
      CollapsingToolbarLayout.d(CollapsingToolbarLayout.this).b(Math.abs(paramInt) / (j - m - i));
      if (Math.abs(paramInt) == k)
      {
        ViewCompat.f(paramAppBarLayout, paramAppBarLayout.getTargetElevation());
        return;
      }
      ViewCompat.f(paramAppBarLayout, 0.0F);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/design/widget/CollapsingToolbarLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */