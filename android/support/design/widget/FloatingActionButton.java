package android.support.design.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.R.dimen;
import android.support.design.R.style;
import android.support.design.R.styleable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatDrawableManager;
import android.support.v7.widget.AppCompatImageHelper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import java.util.List;

@CoordinatorLayout.DefaultBehavior(a=Behavior.class)
public class FloatingActionButton
  extends VisibilityAwareImageButton
{
  private ColorStateList a;
  private PorterDuff.Mode b;
  private int c;
  private int d;
  private int e;
  private int f;
  private boolean g;
  private final Rect h = new Rect();
  private AppCompatImageHelper i;
  private FloatingActionButtonImpl j;
  
  public FloatingActionButton(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public FloatingActionButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public FloatingActionButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    ThemeUtils.a(paramContext);
    paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.FloatingActionButton, paramInt, R.style.Widget_Design_FloatingActionButton);
    this.a = paramContext.getColorStateList(R.styleable.FloatingActionButton_backgroundTint);
    this.b = a(paramContext.getInt(R.styleable.FloatingActionButton_backgroundTintMode, -1), null);
    this.d = paramContext.getColor(R.styleable.FloatingActionButton_rippleColor, 0);
    this.e = paramContext.getInt(R.styleable.FloatingActionButton_fabSize, 0);
    this.c = paramContext.getDimensionPixelSize(R.styleable.FloatingActionButton_borderWidth, 0);
    float f1 = paramContext.getDimension(R.styleable.FloatingActionButton_elevation, 0.0F);
    float f2 = paramContext.getDimension(R.styleable.FloatingActionButton_pressedTranslationZ, 0.0F);
    this.g = paramContext.getBoolean(R.styleable.FloatingActionButton_useCompatPadding, false);
    paramContext.recycle();
    this.i = new AppCompatImageHelper(this, AppCompatDrawableManager.a());
    this.i.a(paramAttributeSet, paramInt);
    paramInt = (int)getResources().getDimension(R.dimen.design_fab_image_size);
    this.f = ((getSizeDimension() - paramInt) / 2);
    getImpl().a(this.a, this.b, this.d, this.c);
    getImpl().c(f1);
    getImpl().d(f2);
    getImpl().g();
  }
  
  private static int a(int paramInt1, int paramInt2)
  {
    int k = View.MeasureSpec.getMode(paramInt2);
    paramInt2 = View.MeasureSpec.getSize(paramInt2);
    switch (k)
    {
    default: 
      return paramInt1;
    case 0: 
      return paramInt1;
    case -2147483648: 
      return Math.min(paramInt1, paramInt2);
    }
    return paramInt2;
  }
  
  static PorterDuff.Mode a(int paramInt, PorterDuff.Mode paramMode)
  {
    switch (paramInt)
    {
    default: 
      return paramMode;
    case 3: 
      return PorterDuff.Mode.SRC_OVER;
    case 5: 
      return PorterDuff.Mode.SRC_IN;
    case 9: 
      return PorterDuff.Mode.SRC_ATOP;
    case 14: 
      return PorterDuff.Mode.MULTIPLY;
    }
    return PorterDuff.Mode.SCREEN;
  }
  
  @Nullable
  private FloatingActionButtonImpl.InternalVisibilityChangedListener a(@Nullable final OnVisibilityChangedListener paramOnVisibilityChangedListener)
  {
    if (paramOnVisibilityChangedListener == null) {
      return null;
    }
    new FloatingActionButtonImpl.InternalVisibilityChangedListener()
    {
      public void a()
      {
        paramOnVisibilityChangedListener.a(FloatingActionButton.this);
      }
      
      public void b()
      {
        paramOnVisibilityChangedListener.b(FloatingActionButton.this);
      }
    };
  }
  
  private FloatingActionButtonImpl a()
  {
    int k = Build.VERSION.SDK_INT;
    if (k >= 21) {
      return new FloatingActionButtonLollipop(this, new ShadowDelegateImpl(null));
    }
    if (k >= 14) {
      return new FloatingActionButtonIcs(this, new ShadowDelegateImpl(null));
    }
    return new FloatingActionButtonEclairMr1(this, new ShadowDelegateImpl(null));
  }
  
  private void a(OnVisibilityChangedListener paramOnVisibilityChangedListener, boolean paramBoolean)
  {
    getImpl().b(a(paramOnVisibilityChangedListener), paramBoolean);
  }
  
  private void b(@Nullable OnVisibilityChangedListener paramOnVisibilityChangedListener, boolean paramBoolean)
  {
    getImpl().a(a(paramOnVisibilityChangedListener), paramBoolean);
  }
  
  private FloatingActionButtonImpl getImpl()
  {
    if (this.j == null) {
      this.j = a();
    }
    return this.j;
  }
  
  protected void drawableStateChanged()
  {
    super.drawableStateChanged();
    getImpl().a(getDrawableState());
  }
  
  @Nullable
  public ColorStateList getBackgroundTintList()
  {
    return this.a;
  }
  
  @Nullable
  public PorterDuff.Mode getBackgroundTintMode()
  {
    return this.b;
  }
  
  public float getCompatElevation()
  {
    return getImpl().a();
  }
  
  @NonNull
  public Drawable getContentBackground()
  {
    return getImpl().f();
  }
  
  final int getSizeDimension()
  {
    switch (this.e)
    {
    default: 
      return getResources().getDimensionPixelSize(R.dimen.design_fab_size_normal);
    }
    return getResources().getDimensionPixelSize(R.dimen.design_fab_size_mini);
  }
  
  public boolean getUseCompatPadding()
  {
    return this.g;
  }
  
  @TargetApi(11)
  public void jumpDrawablesToCurrentState()
  {
    super.jumpDrawablesToCurrentState();
    getImpl().b();
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    getImpl().h();
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    getImpl().i();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int k = getSizeDimension();
    paramInt1 = Math.min(a(k, paramInt1), a(k, paramInt2));
    setMeasuredDimension(this.h.left + paramInt1 + this.h.right, this.h.top + paramInt1 + this.h.bottom);
  }
  
  public void setBackgroundColor(int paramInt)
  {
    Log.i("FloatingActionButton", "Setting a custom background is not supported.");
  }
  
  public void setBackgroundDrawable(Drawable paramDrawable)
  {
    Log.i("FloatingActionButton", "Setting a custom background is not supported.");
  }
  
  public void setBackgroundResource(int paramInt)
  {
    Log.i("FloatingActionButton", "Setting a custom background is not supported.");
  }
  
  public void setBackgroundTintList(@Nullable ColorStateList paramColorStateList)
  {
    if (this.a != paramColorStateList)
    {
      this.a = paramColorStateList;
      getImpl().a(paramColorStateList);
    }
  }
  
  public void setBackgroundTintMode(@Nullable PorterDuff.Mode paramMode)
  {
    if (this.b != paramMode)
    {
      this.b = paramMode;
      getImpl().a(paramMode);
    }
  }
  
  public void setCompatElevation(float paramFloat)
  {
    getImpl().c(paramFloat);
  }
  
  public void setImageResource(@DrawableRes int paramInt)
  {
    this.i.a(paramInt);
  }
  
  public void setRippleColor(@ColorInt int paramInt)
  {
    if (this.d != paramInt)
    {
      this.d = paramInt;
      getImpl().a(paramInt);
    }
  }
  
  public void setUseCompatPadding(boolean paramBoolean)
  {
    if (this.g != paramBoolean)
    {
      this.g = paramBoolean;
      getImpl().c();
    }
  }
  
  public static class Behavior
    extends CoordinatorLayout.Behavior<FloatingActionButton>
  {
    private static final boolean a;
    private ValueAnimatorCompat b;
    private float c;
    private Rect d;
    
    static
    {
      if (Build.VERSION.SDK_INT >= 11) {}
      for (boolean bool = true;; bool = false)
      {
        a = bool;
        return;
      }
    }
    
    private float a(CoordinatorLayout paramCoordinatorLayout, FloatingActionButton paramFloatingActionButton)
    {
      float f1 = 0.0F;
      List localList = paramCoordinatorLayout.d(paramFloatingActionButton);
      int i = 0;
      int j = localList.size();
      while (i < j)
      {
        View localView = (View)localList.get(i);
        float f2 = f1;
        if ((localView instanceof Snackbar.SnackbarLayout))
        {
          f2 = f1;
          if (paramCoordinatorLayout.a(paramFloatingActionButton, localView)) {
            f2 = Math.min(f1, ViewCompat.p(localView) - localView.getHeight());
          }
        }
        i += 1;
        f1 = f2;
      }
      return f1;
    }
    
    private boolean a(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, FloatingActionButton paramFloatingActionButton)
    {
      if (((CoordinatorLayout.LayoutParams)paramFloatingActionButton.getLayoutParams()).a() != paramAppBarLayout.getId()) {}
      while (paramFloatingActionButton.getUserSetVisibility() != 0) {
        return false;
      }
      if (this.d == null) {
        this.d = new Rect();
      }
      Rect localRect = this.d;
      ViewGroupUtils.b(paramCoordinatorLayout, paramAppBarLayout, localRect);
      if (localRect.bottom <= paramAppBarLayout.getMinimumHeightForVisibleOverlappingContent()) {
        FloatingActionButton.a(paramFloatingActionButton, null, false);
      }
      for (;;)
      {
        return true;
        FloatingActionButton.b(paramFloatingActionButton, null, false);
      }
    }
    
    private void b(CoordinatorLayout paramCoordinatorLayout, FloatingActionButton paramFloatingActionButton)
    {
      Rect localRect = FloatingActionButton.a(paramFloatingActionButton);
      CoordinatorLayout.LayoutParams localLayoutParams;
      int j;
      int i;
      if ((localRect != null) && (localRect.centerX() > 0) && (localRect.centerY() > 0))
      {
        localLayoutParams = (CoordinatorLayout.LayoutParams)paramFloatingActionButton.getLayoutParams();
        j = 0;
        i = 0;
        if (paramFloatingActionButton.getRight() < paramCoordinatorLayout.getWidth() - localLayoutParams.rightMargin) {
          break label100;
        }
        i = localRect.right;
        if (paramFloatingActionButton.getBottom() < paramCoordinatorLayout.getBottom() - localLayoutParams.bottomMargin) {
          break label122;
        }
        j = localRect.bottom;
      }
      for (;;)
      {
        paramFloatingActionButton.offsetTopAndBottom(j);
        paramFloatingActionButton.offsetLeftAndRight(i);
        return;
        label100:
        if (paramFloatingActionButton.getLeft() > localLayoutParams.leftMargin) {
          break;
        }
        i = -localRect.left;
        break;
        label122:
        if (paramFloatingActionButton.getTop() <= localLayoutParams.topMargin) {
          j = -localRect.top;
        }
      }
    }
    
    private void d(CoordinatorLayout paramCoordinatorLayout, final FloatingActionButton paramFloatingActionButton, View paramView)
    {
      float f1 = a(paramCoordinatorLayout, paramFloatingActionButton);
      if (this.c == f1) {
        return;
      }
      float f2 = ViewCompat.p(paramFloatingActionButton);
      if ((this.b != null) && (this.b.b())) {
        this.b.cancel();
      }
      if ((paramFloatingActionButton.isShown()) && (Math.abs(f2 - f1) > paramFloatingActionButton.getHeight() * 0.667F))
      {
        if (this.b == null)
        {
          this.b = ViewUtils.a();
          this.b.a(AnimationUtils.b);
          this.b.a(new ValueAnimatorCompat.AnimatorUpdateListener()
          {
            public void a(ValueAnimatorCompat paramAnonymousValueAnimatorCompat)
            {
              ViewCompat.b(paramFloatingActionButton, paramAnonymousValueAnimatorCompat.d());
            }
          });
        }
        this.b.a(f2, f1);
        this.b.a();
      }
      for (;;)
      {
        this.c = f1;
        return;
        ViewCompat.b(paramFloatingActionButton, f1);
      }
    }
    
    public boolean a(CoordinatorLayout paramCoordinatorLayout, FloatingActionButton paramFloatingActionButton, int paramInt)
    {
      List localList = paramCoordinatorLayout.d(paramFloatingActionButton);
      int i = 0;
      int j = localList.size();
      for (;;)
      {
        if (i < j)
        {
          View localView = (View)localList.get(i);
          if ((!(localView instanceof AppBarLayout)) || (!a(paramCoordinatorLayout, (AppBarLayout)localView, paramFloatingActionButton))) {}
        }
        else
        {
          paramCoordinatorLayout.a(paramFloatingActionButton, paramInt);
          b(paramCoordinatorLayout, paramFloatingActionButton);
          return true;
        }
        i += 1;
      }
    }
    
    public boolean a(CoordinatorLayout paramCoordinatorLayout, FloatingActionButton paramFloatingActionButton, View paramView)
    {
      return (a) && ((paramView instanceof Snackbar.SnackbarLayout));
    }
    
    public boolean b(CoordinatorLayout paramCoordinatorLayout, FloatingActionButton paramFloatingActionButton, View paramView)
    {
      if ((paramView instanceof Snackbar.SnackbarLayout)) {
        d(paramCoordinatorLayout, paramFloatingActionButton, paramView);
      }
      for (;;)
      {
        return false;
        if ((paramView instanceof AppBarLayout)) {
          a(paramCoordinatorLayout, (AppBarLayout)paramView, paramFloatingActionButton);
        }
      }
    }
    
    public void c(CoordinatorLayout paramCoordinatorLayout, FloatingActionButton paramFloatingActionButton, View paramView)
    {
      if ((paramView instanceof Snackbar.SnackbarLayout)) {
        d(paramCoordinatorLayout, paramFloatingActionButton, paramView);
      }
    }
  }
  
  public static abstract class OnVisibilityChangedListener
  {
    public void a(FloatingActionButton paramFloatingActionButton) {}
    
    public void b(FloatingActionButton paramFloatingActionButton) {}
  }
  
  private class ShadowDelegateImpl
    implements ShadowViewDelegate
  {
    private ShadowDelegateImpl() {}
    
    public float a()
    {
      return FloatingActionButton.this.getSizeDimension() / 2.0F;
    }
    
    public void a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      FloatingActionButton.a(FloatingActionButton.this).set(paramInt1, paramInt2, paramInt3, paramInt4);
      FloatingActionButton.this.setPadding(FloatingActionButton.b(FloatingActionButton.this) + paramInt1, FloatingActionButton.b(FloatingActionButton.this) + paramInt2, FloatingActionButton.b(FloatingActionButton.this) + paramInt3, FloatingActionButton.b(FloatingActionButton.this) + paramInt4);
    }
    
    public void a(Drawable paramDrawable)
    {
      FloatingActionButton.a(FloatingActionButton.this, paramDrawable);
    }
    
    public boolean b()
    {
      return FloatingActionButton.c(FloatingActionButton.this);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/design/widget/FloatingActionButton.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */