package android.support.design.widget;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.annotation.Nullable;
import android.support.design.R.anim;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.animation.Animation;
import android.view.animation.Transformation;

class FloatingActionButtonEclairMr1
  extends FloatingActionButtonImpl
{
  ShadowDrawableWrapper a;
  private int m;
  private StateListAnimator n;
  private boolean o;
  
  FloatingActionButtonEclairMr1(VisibilityAwareImageButton paramVisibilityAwareImageButton, ShadowViewDelegate paramShadowViewDelegate)
  {
    super(paramVisibilityAwareImageButton, paramShadowViewDelegate);
    this.m = paramVisibilityAwareImageButton.getResources().getInteger(17694720);
    this.n = new StateListAnimator();
    this.n.a(paramVisibilityAwareImageButton);
    this.n.a(h, a(new ElevateToTranslationZAnimation(null)));
    this.n.a(i, a(new ElevateToTranslationZAnimation(null)));
    this.n.a(j, a(new ResetElevationAnimation(null)));
  }
  
  private Animation a(Animation paramAnimation)
  {
    paramAnimation.setInterpolator(AnimationUtils.b);
    paramAnimation.setDuration(this.m);
    return paramAnimation;
  }
  
  private static ColorStateList b(int paramInt)
  {
    int[][] arrayOfInt = new int[3][];
    int[] arrayOfInt1 = new int[3];
    arrayOfInt[0] = i;
    arrayOfInt1[0] = paramInt;
    int i = 0 + 1;
    arrayOfInt[i] = h;
    arrayOfInt1[i] = paramInt;
    paramInt = i + 1;
    arrayOfInt[paramInt] = new int[0];
    arrayOfInt1[paramInt] = 0;
    return new ColorStateList(arrayOfInt, arrayOfInt1);
  }
  
  float a()
  {
    return this.f;
  }
  
  void a(float paramFloat)
  {
    if (this.a != null)
    {
      this.a.a(paramFloat, this.g + paramFloat);
      g();
    }
  }
  
  void a(int paramInt)
  {
    if (this.c != null) {
      DrawableCompat.a(this.c, b(paramInt));
    }
  }
  
  void a(ColorStateList paramColorStateList)
  {
    if (this.b != null) {
      DrawableCompat.a(this.b, paramColorStateList);
    }
    if (this.d != null) {
      this.d.a(paramColorStateList);
    }
  }
  
  void a(ColorStateList paramColorStateList, PorterDuff.Mode paramMode, int paramInt1, int paramInt2)
  {
    this.b = DrawableCompat.f(k());
    DrawableCompat.a(this.b, paramColorStateList);
    if (paramMode != null) {
      DrawableCompat.a(this.b, paramMode);
    }
    this.c = DrawableCompat.f(k());
    DrawableCompat.a(this.c, b(paramInt1));
    if (paramInt2 > 0)
    {
      this.d = a(paramInt2, paramColorStateList);
      paramColorStateList = new Drawable[3];
      paramColorStateList[0] = this.d;
      paramColorStateList[1] = this.b;
      paramColorStateList[2] = this.c;
    }
    for (;;)
    {
      this.e = new LayerDrawable(paramColorStateList);
      this.a = new ShadowDrawableWrapper(this.k.getResources(), this.e, this.l.a(), this.f, this.f + this.g);
      this.a.a(false);
      this.l.a(this.a);
      return;
      this.d = null;
      paramColorStateList = new Drawable[2];
      paramColorStateList[0] = this.b;
      paramColorStateList[1] = this.c;
    }
  }
  
  void a(PorterDuff.Mode paramMode)
  {
    if (this.b != null) {
      DrawableCompat.a(this.b, paramMode);
    }
  }
  
  void a(Rect paramRect)
  {
    this.a.getPadding(paramRect);
  }
  
  void a(@Nullable final FloatingActionButtonImpl.InternalVisibilityChangedListener paramInternalVisibilityChangedListener, final boolean paramBoolean)
  {
    if ((this.o) || (this.k.getVisibility() != 0))
    {
      if (paramInternalVisibilityChangedListener != null) {
        paramInternalVisibilityChangedListener.b();
      }
      return;
    }
    Animation localAnimation = android.view.animation.AnimationUtils.loadAnimation(this.k.getContext(), R.anim.design_fab_out);
    localAnimation.setInterpolator(AnimationUtils.c);
    localAnimation.setDuration(200L);
    localAnimation.setAnimationListener(new AnimationUtils.AnimationListenerAdapter()
    {
      public void onAnimationEnd(Animation paramAnonymousAnimation)
      {
        FloatingActionButtonEclairMr1.a(FloatingActionButtonEclairMr1.this, false);
        FloatingActionButtonEclairMr1.this.k.a(8, paramBoolean);
        if (paramInternalVisibilityChangedListener != null) {
          paramInternalVisibilityChangedListener.b();
        }
      }
      
      public void onAnimationStart(Animation paramAnonymousAnimation)
      {
        FloatingActionButtonEclairMr1.a(FloatingActionButtonEclairMr1.this, true);
      }
    });
    this.k.startAnimation(localAnimation);
  }
  
  void a(int[] paramArrayOfInt)
  {
    this.n.a(paramArrayOfInt);
  }
  
  void b()
  {
    this.n.b();
  }
  
  void b(float paramFloat)
  {
    if (this.a != null)
    {
      this.a.c(this.f + paramFloat);
      g();
    }
  }
  
  void b(@Nullable final FloatingActionButtonImpl.InternalVisibilityChangedListener paramInternalVisibilityChangedListener, boolean paramBoolean)
  {
    if ((this.k.getVisibility() != 0) || (this.o))
    {
      this.k.clearAnimation();
      this.k.a(0, paramBoolean);
      localAnimation = android.view.animation.AnimationUtils.loadAnimation(this.k.getContext(), R.anim.design_fab_in);
      localAnimation.setDuration(200L);
      localAnimation.setInterpolator(AnimationUtils.d);
      localAnimation.setAnimationListener(new AnimationUtils.AnimationListenerAdapter()
      {
        public void onAnimationEnd(Animation paramAnonymousAnimation)
        {
          if (paramInternalVisibilityChangedListener != null) {
            paramInternalVisibilityChangedListener.a();
          }
        }
      });
      this.k.startAnimation(localAnimation);
    }
    while (paramInternalVisibilityChangedListener == null)
    {
      Animation localAnimation;
      return;
    }
    paramInternalVisibilityChangedListener.a();
  }
  
  void c() {}
  
  private abstract class BaseShadowAnimation
    extends Animation
  {
    private float b;
    private float c;
    
    private BaseShadowAnimation() {}
    
    protected abstract float a();
    
    protected void applyTransformation(float paramFloat, Transformation paramTransformation)
    {
      FloatingActionButtonEclairMr1.this.a.b(this.b + this.c * paramFloat);
    }
    
    public void reset()
    {
      super.reset();
      this.b = FloatingActionButtonEclairMr1.this.a.a();
      this.c = (a() - this.b);
    }
  }
  
  private class ElevateToTranslationZAnimation
    extends FloatingActionButtonEclairMr1.BaseShadowAnimation
  {
    private ElevateToTranslationZAnimation()
    {
      super(null);
    }
    
    protected float a()
    {
      return FloatingActionButtonEclairMr1.this.f + FloatingActionButtonEclairMr1.this.g;
    }
  }
  
  private class ResetElevationAnimation
    extends FloatingActionButtonEclairMr1.BaseShadowAnimation
  {
    private ResetElevationAnimation()
    {
      super(null);
    }
    
    protected float a()
    {
      return FloatingActionButtonEclairMr1.this.f;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/design/widget/FloatingActionButtonEclairMr1.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */