package android.support.design.widget;

import android.graphics.drawable.Drawable;

abstract interface ShadowViewDelegate
{
  public abstract float a();
  
  public abstract void a(int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  public abstract void a(Drawable paramDrawable);
  
  public abstract boolean b();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/design/widget/ShadowViewDelegate.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */