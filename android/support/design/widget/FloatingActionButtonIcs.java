package android.support.design.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.view.ViewPropertyAnimator;

class FloatingActionButtonIcs
  extends FloatingActionButtonEclairMr1
{
  private boolean m;
  
  FloatingActionButtonIcs(VisibilityAwareImageButton paramVisibilityAwareImageButton, ShadowViewDelegate paramShadowViewDelegate)
  {
    super(paramVisibilityAwareImageButton, paramShadowViewDelegate);
  }
  
  private void e(float paramFloat)
  {
    if (this.a != null) {
      this.a.a(-paramFloat);
    }
    if (this.d != null) {
      this.d.b(-paramFloat);
    }
  }
  
  void a(@Nullable final FloatingActionButtonImpl.InternalVisibilityChangedListener paramInternalVisibilityChangedListener, final boolean paramBoolean)
  {
    if ((this.m) || (this.k.getVisibility() != 0)) {
      if (paramInternalVisibilityChangedListener != null) {
        paramInternalVisibilityChangedListener.b();
      }
    }
    do
    {
      return;
      if ((ViewCompat.F(this.k)) && (!this.k.isInEditMode())) {
        break;
      }
      this.k.a(8, paramBoolean);
    } while (paramInternalVisibilityChangedListener == null);
    paramInternalVisibilityChangedListener.b();
    return;
    this.k.animate().cancel();
    this.k.animate().scaleX(0.0F).scaleY(0.0F).alpha(0.0F).setDuration(200L).setInterpolator(AnimationUtils.c).setListener(new AnimatorListenerAdapter()
    {
      private boolean d;
      
      public void onAnimationCancel(Animator paramAnonymousAnimator)
      {
        FloatingActionButtonIcs.a(FloatingActionButtonIcs.this, false);
        this.d = true;
      }
      
      public void onAnimationEnd(Animator paramAnonymousAnimator)
      {
        FloatingActionButtonIcs.a(FloatingActionButtonIcs.this, false);
        if (!this.d)
        {
          FloatingActionButtonIcs.this.k.a(8, paramBoolean);
          if (paramInternalVisibilityChangedListener != null) {
            paramInternalVisibilityChangedListener.b();
          }
        }
      }
      
      public void onAnimationStart(Animator paramAnonymousAnimator)
      {
        FloatingActionButtonIcs.a(FloatingActionButtonIcs.this, true);
        this.d = false;
        FloatingActionButtonIcs.this.k.a(0, paramBoolean);
      }
    });
  }
  
  void b(@Nullable final FloatingActionButtonImpl.InternalVisibilityChangedListener paramInternalVisibilityChangedListener, final boolean paramBoolean)
  {
    if ((this.m) || (this.k.getVisibility() != 0))
    {
      if ((!ViewCompat.F(this.k)) || (this.k.isInEditMode())) {
        break label127;
      }
      this.k.animate().cancel();
      if (this.k.getVisibility() != 0)
      {
        this.k.setAlpha(0.0F);
        this.k.setScaleY(0.0F);
        this.k.setScaleX(0.0F);
      }
      this.k.animate().scaleX(1.0F).scaleY(1.0F).alpha(1.0F).setDuration(200L).setInterpolator(AnimationUtils.d).setListener(new AnimatorListenerAdapter()
      {
        public void onAnimationEnd(Animator paramAnonymousAnimator)
        {
          if (paramInternalVisibilityChangedListener != null) {
            paramInternalVisibilityChangedListener.a();
          }
        }
        
        public void onAnimationStart(Animator paramAnonymousAnimator)
        {
          FloatingActionButtonIcs.this.k.a(0, paramBoolean);
        }
      });
    }
    label127:
    do
    {
      return;
      this.k.a(0, paramBoolean);
      this.k.setAlpha(1.0F);
      this.k.setScaleY(1.0F);
      this.k.setScaleX(1.0F);
    } while (paramInternalVisibilityChangedListener == null);
    paramInternalVisibilityChangedListener.a();
  }
  
  boolean d()
  {
    return true;
  }
  
  void e()
  {
    e(this.k.getRotation());
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/design/widget/FloatingActionButtonIcs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */