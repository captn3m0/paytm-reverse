package android.support.design.widget;

import android.view.animation.Interpolator;

class ValueAnimatorCompat
{
  private final Impl a;
  
  ValueAnimatorCompat(Impl paramImpl)
  {
    this.a = paramImpl;
  }
  
  public void a()
  {
    this.a.a();
  }
  
  public void a(float paramFloat1, float paramFloat2)
  {
    this.a.a(paramFloat1, paramFloat2);
  }
  
  public void a(int paramInt)
  {
    this.a.a(paramInt);
  }
  
  public void a(int paramInt1, int paramInt2)
  {
    this.a.a(paramInt1, paramInt2);
  }
  
  public void a(final AnimatorListener paramAnimatorListener)
  {
    if (paramAnimatorListener != null)
    {
      this.a.a(new ValueAnimatorCompat.Impl.AnimatorListenerProxy()
      {
        public void a()
        {
          paramAnimatorListener.b(ValueAnimatorCompat.this);
        }
        
        public void b()
        {
          paramAnimatorListener.a(ValueAnimatorCompat.this);
        }
        
        public void c()
        {
          paramAnimatorListener.c(ValueAnimatorCompat.this);
        }
      });
      return;
    }
    this.a.a(null);
  }
  
  public void a(final AnimatorUpdateListener paramAnimatorUpdateListener)
  {
    if (paramAnimatorUpdateListener != null)
    {
      this.a.a(new ValueAnimatorCompat.Impl.AnimatorUpdateListenerProxy()
      {
        public void a()
        {
          paramAnimatorUpdateListener.a(ValueAnimatorCompat.this);
        }
      });
      return;
    }
    this.a.a(null);
  }
  
  public void a(Interpolator paramInterpolator)
  {
    this.a.a(paramInterpolator);
  }
  
  public boolean b()
  {
    return this.a.b();
  }
  
  public int c()
  {
    return this.a.c();
  }
  
  public void cancel()
  {
    this.a.cancel();
  }
  
  public float d()
  {
    return this.a.d();
  }
  
  public float e()
  {
    return this.a.e();
  }
  
  public long f()
  {
    return this.a.f();
  }
  
  static abstract interface AnimatorListener
  {
    public abstract void a(ValueAnimatorCompat paramValueAnimatorCompat);
    
    public abstract void b(ValueAnimatorCompat paramValueAnimatorCompat);
    
    public abstract void c(ValueAnimatorCompat paramValueAnimatorCompat);
  }
  
  static class AnimatorListenerAdapter
    implements ValueAnimatorCompat.AnimatorListener
  {
    public void a(ValueAnimatorCompat paramValueAnimatorCompat) {}
    
    public void b(ValueAnimatorCompat paramValueAnimatorCompat) {}
    
    public void c(ValueAnimatorCompat paramValueAnimatorCompat) {}
  }
  
  static abstract interface AnimatorUpdateListener
  {
    public abstract void a(ValueAnimatorCompat paramValueAnimatorCompat);
  }
  
  static abstract interface Creator
  {
    public abstract ValueAnimatorCompat a();
  }
  
  static abstract class Impl
  {
    abstract void a();
    
    abstract void a(float paramFloat1, float paramFloat2);
    
    abstract void a(int paramInt);
    
    abstract void a(int paramInt1, int paramInt2);
    
    abstract void a(AnimatorListenerProxy paramAnimatorListenerProxy);
    
    abstract void a(AnimatorUpdateListenerProxy paramAnimatorUpdateListenerProxy);
    
    abstract void a(Interpolator paramInterpolator);
    
    abstract boolean b();
    
    abstract int c();
    
    abstract void cancel();
    
    abstract float d();
    
    abstract float e();
    
    abstract long f();
    
    static abstract interface AnimatorListenerProxy
    {
      public abstract void a();
      
      public abstract void b();
      
      public abstract void c();
    }
    
    static abstract interface AnimatorUpdateListenerProxy
    {
      public abstract void a();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/design/widget/ValueAnimatorCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */