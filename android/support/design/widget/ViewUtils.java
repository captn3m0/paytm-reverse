package android.support.design.widget;

import android.os.Build.VERSION;
import android.view.View;

class ViewUtils
{
  static final ValueAnimatorCompat.Creator a = new ValueAnimatorCompat.Creator()
  {
    public ValueAnimatorCompat a()
    {
      if (Build.VERSION.SDK_INT >= 12) {}
      for (Object localObject = new ValueAnimatorCompatImplHoneycombMr1();; localObject = new ValueAnimatorCompatImplEclairMr1()) {
        return new ValueAnimatorCompat((ValueAnimatorCompat.Impl)localObject);
      }
    }
  };
  private static final ViewUtilsImpl b = new ViewUtilsImplBase(null);
  
  static
  {
    if (Build.VERSION.SDK_INT >= 21)
    {
      b = new ViewUtilsImplLollipop(null);
      return;
    }
  }
  
  static ValueAnimatorCompat a()
  {
    return a.a();
  }
  
  static void a(View paramView)
  {
    b.a(paramView);
  }
  
  private static abstract interface ViewUtilsImpl
  {
    public abstract void a(View paramView);
  }
  
  private static class ViewUtilsImplBase
    implements ViewUtils.ViewUtilsImpl
  {
    public void a(View paramView) {}
  }
  
  private static class ViewUtilsImplLollipop
    implements ViewUtils.ViewUtilsImpl
  {
    public void a(View paramView)
    {
      ViewUtilsLollipop.a(paramView);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/design/widget/ViewUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */