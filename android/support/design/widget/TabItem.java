package android.support.design.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.design.R.styleable;
import android.support.v7.widget.TintTypedArray;
import android.util.AttributeSet;
import android.view.View;

public final class TabItem
  extends View
{
  final CharSequence a;
  final Drawable b;
  final int c;
  
  public TabItem(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public TabItem(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    paramContext = TintTypedArray.a(paramContext, paramAttributeSet, R.styleable.TabItem);
    this.a = paramContext.c(R.styleable.TabItem_android_text);
    this.b = paramContext.a(R.styleable.TabItem_android_icon);
    this.c = paramContext.g(R.styleable.TabItem_android_layout, 0);
    paramContext.a();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/design/widget/TabItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */