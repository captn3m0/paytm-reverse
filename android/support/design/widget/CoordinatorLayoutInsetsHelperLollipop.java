package android.support.design.widget;

import android.support.v4.view.OnApplyWindowInsetsListener;
import android.support.v4.view.ViewCompat;
import android.view.View;

class CoordinatorLayoutInsetsHelperLollipop
  implements CoordinatorLayoutInsetsHelper
{
  public void a(View paramView, OnApplyWindowInsetsListener paramOnApplyWindowInsetsListener)
  {
    if (ViewCompat.x(paramView))
    {
      ViewCompat.a(paramView, paramOnApplyWindowInsetsListener);
      paramView.setSystemUiVisibility(1280);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/design/widget/CoordinatorLayoutInsetsHelperLollipop.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */