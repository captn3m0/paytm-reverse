package android.support.design.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.R.dimen;
import android.support.design.R.layout;
import android.support.design.R.style;
import android.support.design.R.styleable;
import android.support.v4.util.Pools.Pool;
import android.support.v4.util.Pools.SimplePool;
import android.support.v4.util.Pools.SynchronizedPool;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.widget.AppCompatDrawableManager;
import android.text.Layout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.FrameLayout.LayoutParams;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public class TabLayout
  extends HorizontalScrollView
{
  private static final Pools.Pool<Tab> a = new Pools.SynchronizedPool(16);
  private final Pools.Pool<TabView> A = new Pools.SimplePool(12);
  private final ArrayList<Tab> b = new ArrayList();
  private Tab c;
  private final SlidingTabStrip d;
  private int e;
  private int f;
  private int g;
  private int h;
  private int i;
  private ColorStateList j;
  private float k;
  private float l;
  private final int m;
  private int n = Integer.MAX_VALUE;
  private final int o;
  private final int p;
  private final int q;
  private int r;
  private int s;
  private int t;
  private OnTabSelectedListener u;
  private ValueAnimatorCompat v;
  private ViewPager w;
  private PagerAdapter x;
  private DataSetObserver y;
  private TabLayoutOnPageChangeListener z;
  
  public TabLayout(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public TabLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public TabLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    ThemeUtils.a(paramContext);
    setHorizontalScrollBarEnabled(false);
    this.d = new SlidingTabStrip(paramContext);
    super.addView(this.d, 0, new FrameLayout.LayoutParams(-2, -1));
    paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.TabLayout, paramInt, R.style.Widget_Design_TabLayout);
    this.d.b(paramAttributeSet.getDimensionPixelSize(R.styleable.TabLayout_tabIndicatorHeight, 0));
    this.d.a(paramAttributeSet.getColor(R.styleable.TabLayout_tabIndicatorColor, 0));
    paramInt = paramAttributeSet.getDimensionPixelSize(R.styleable.TabLayout_tabPadding, 0);
    this.h = paramInt;
    this.g = paramInt;
    this.f = paramInt;
    this.e = paramInt;
    this.e = paramAttributeSet.getDimensionPixelSize(R.styleable.TabLayout_tabPaddingStart, this.e);
    this.f = paramAttributeSet.getDimensionPixelSize(R.styleable.TabLayout_tabPaddingTop, this.f);
    this.g = paramAttributeSet.getDimensionPixelSize(R.styleable.TabLayout_tabPaddingEnd, this.g);
    this.h = paramAttributeSet.getDimensionPixelSize(R.styleable.TabLayout_tabPaddingBottom, this.h);
    this.i = paramAttributeSet.getResourceId(R.styleable.TabLayout_tabTextAppearance, R.style.TextAppearance_Design_Tab);
    paramContext = paramContext.obtainStyledAttributes(this.i, R.styleable.TextAppearance);
    try
    {
      this.k = paramContext.getDimensionPixelSize(R.styleable.TextAppearance_android_textSize, 0);
      this.j = paramContext.getColorStateList(R.styleable.TextAppearance_android_textColor);
      paramContext.recycle();
      if (paramAttributeSet.hasValue(R.styleable.TabLayout_tabTextColor)) {
        this.j = paramAttributeSet.getColorStateList(R.styleable.TabLayout_tabTextColor);
      }
      if (paramAttributeSet.hasValue(R.styleable.TabLayout_tabSelectedTextColor))
      {
        paramInt = paramAttributeSet.getColor(R.styleable.TabLayout_tabSelectedTextColor, 0);
        this.j = a(this.j.getDefaultColor(), paramInt);
      }
      this.o = paramAttributeSet.getDimensionPixelSize(R.styleable.TabLayout_tabMinWidth, -1);
      this.p = paramAttributeSet.getDimensionPixelSize(R.styleable.TabLayout_tabMaxWidth, -1);
      this.m = paramAttributeSet.getResourceId(R.styleable.TabLayout_tabBackground, 0);
      this.r = paramAttributeSet.getDimensionPixelSize(R.styleable.TabLayout_tabContentStart, 0);
      this.t = paramAttributeSet.getInt(R.styleable.TabLayout_tabMode, 1);
      this.s = paramAttributeSet.getInt(R.styleable.TabLayout_tabGravity, 0);
      paramAttributeSet.recycle();
      paramContext = getResources();
      this.l = paramContext.getDimensionPixelSize(R.dimen.design_tab_text_size_2line);
      this.q = paramContext.getDimensionPixelSize(R.dimen.design_tab_scrollable_min_width);
      f();
      return;
    }
    finally
    {
      paramContext.recycle();
    }
  }
  
  private int a(int paramInt, float paramFloat)
  {
    int i1 = 0;
    int i2 = 0;
    View localView2;
    View localView1;
    if (this.t == 0)
    {
      localView2 = this.d.getChildAt(paramInt);
      if (paramInt + 1 >= this.d.getChildCount()) {
        break label107;
      }
      localView1 = this.d.getChildAt(paramInt + 1);
      if (localView2 == null) {
        break label113;
      }
    }
    label107:
    label113:
    for (paramInt = localView2.getWidth();; paramInt = 0)
    {
      i1 = i2;
      if (localView1 != null) {
        i1 = localView1.getWidth();
      }
      i1 = localView2.getLeft() + (int)((paramInt + i1) * paramFloat * 0.5F) + localView2.getWidth() / 2 - getWidth() / 2;
      return i1;
      localView1 = null;
      break;
    }
  }
  
  private static ColorStateList a(int paramInt1, int paramInt2)
  {
    int[][] arrayOfInt = new int[2][];
    int[] arrayOfInt1 = new int[2];
    arrayOfInt[0] = SELECTED_STATE_SET;
    arrayOfInt1[0] = paramInt2;
    paramInt2 = 0 + 1;
    arrayOfInt[paramInt2] = EMPTY_STATE_SET;
    arrayOfInt1[paramInt2] = paramInt1;
    return new ColorStateList(arrayOfInt, arrayOfInt1);
  }
  
  private void a(int paramInt, float paramFloat, boolean paramBoolean1, boolean paramBoolean2)
  {
    int i1 = Math.round(paramInt + paramFloat);
    if ((i1 < 0) || (i1 >= this.d.getChildCount())) {}
    do
    {
      return;
      if (paramBoolean2) {
        this.d.a(paramInt, paramFloat);
      }
      if ((this.v != null) && (this.v.b())) {
        this.v.cancel();
      }
      scrollTo(a(paramInt, paramFloat), 0);
    } while (!paramBoolean1);
    setSelectedTabView(i1);
  }
  
  private void a(@NonNull TabItem paramTabItem)
  {
    Tab localTab = a();
    if (paramTabItem.a != null) {
      localTab.a(paramTabItem.a);
    }
    if (paramTabItem.b != null) {
      localTab.a(paramTabItem.b);
    }
    if (paramTabItem.c != 0) {
      localTab.a(paramTabItem.c);
    }
    a(localTab);
  }
  
  private void a(Tab paramTab, int paramInt)
  {
    paramTab.b(paramInt);
    this.b.add(paramInt, paramTab);
    int i1 = this.b.size();
    paramInt += 1;
    while (paramInt < i1)
    {
      ((Tab)this.b.get(paramInt)).b(paramInt);
      paramInt += 1;
    }
  }
  
  private void a(@Nullable PagerAdapter paramPagerAdapter, boolean paramBoolean)
  {
    if ((this.x != null) && (this.y != null)) {
      this.x.unregisterDataSetObserver(this.y);
    }
    this.x = paramPagerAdapter;
    if ((paramBoolean) && (paramPagerAdapter != null))
    {
      if (this.y == null) {
        this.y = new PagerAdapterObserver(null);
      }
      paramPagerAdapter.registerDataSetObserver(this.y);
    }
    c();
  }
  
  private void a(View paramView)
  {
    if ((paramView instanceof TabItem))
    {
      a((TabItem)paramView);
      return;
    }
    throw new IllegalArgumentException("Only TabItem instances can be added to TabLayout");
  }
  
  private void a(LinearLayout.LayoutParams paramLayoutParams)
  {
    if ((this.t == 1) && (this.s == 0))
    {
      paramLayoutParams.width = 0;
      paramLayoutParams.weight = 1.0F;
      return;
    }
    paramLayoutParams.width = -2;
    paramLayoutParams.weight = 0.0F;
  }
  
  private void a(boolean paramBoolean)
  {
    int i1 = 0;
    while (i1 < this.d.getChildCount())
    {
      View localView = this.d.getChildAt(i1);
      localView.setMinimumWidth(getTabMinWidth());
      a((LinearLayout.LayoutParams)localView.getLayoutParams());
      if (paramBoolean) {
        localView.requestLayout();
      }
      i1 += 1;
    }
  }
  
  private int b(int paramInt)
  {
    return Math.round(getResources().getDisplayMetrics().density * paramInt);
  }
  
  private TabView c(@NonNull Tab paramTab)
  {
    if (this.A != null) {}
    for (TabView localTabView1 = (TabView)this.A.a();; localTabView1 = null)
    {
      TabView localTabView2 = localTabView1;
      if (localTabView1 == null) {
        localTabView2 = new TabView(getContext());
      }
      TabView.a(localTabView2, paramTab);
      localTabView2.setFocusable(true);
      localTabView2.setMinimumWidth(getTabMinWidth());
      return localTabView2;
    }
  }
  
  private void c()
  {
    b();
    if (this.x != null)
    {
      int i2 = this.x.getCount();
      int i1 = 0;
      while (i1 < i2)
      {
        a(a().a(this.x.getPageTitle(i1)), false);
        i1 += 1;
      }
      if ((this.w != null) && (i2 > 0))
      {
        i1 = this.w.getCurrentItem();
        if ((i1 != getSelectedTabPosition()) && (i1 < getTabCount())) {
          b(a(i1));
        }
      }
      return;
    }
    b();
  }
  
  private void c(int paramInt)
  {
    TabView localTabView = (TabView)this.d.getChildAt(paramInt);
    this.d.removeViewAt(paramInt);
    if (localTabView != null)
    {
      TabView.a(localTabView);
      this.A.a(localTabView);
    }
    requestLayout();
  }
  
  private void c(Tab paramTab, boolean paramBoolean)
  {
    paramTab = Tab.d(paramTab);
    this.d.addView(paramTab, e());
    if (paramBoolean) {
      paramTab.setSelected(true);
    }
  }
  
  private void d()
  {
    int i1 = 0;
    int i2 = this.b.size();
    while (i1 < i2)
    {
      Tab.c((Tab)this.b.get(i1));
      i1 += 1;
    }
  }
  
  private void d(int paramInt)
  {
    if (paramInt == -1) {
      return;
    }
    if ((getWindowToken() == null) || (!ViewCompat.F(this)) || (this.d.a()))
    {
      setScrollPosition(paramInt, 0.0F, true);
      return;
    }
    int i1 = getScrollX();
    int i2 = a(paramInt, 0.0F);
    if (i1 != i2)
    {
      if (this.v == null)
      {
        this.v = ViewUtils.a();
        this.v.a(AnimationUtils.b);
        this.v.a(300);
        this.v.a(new ValueAnimatorCompat.AnimatorUpdateListener()
        {
          public void a(ValueAnimatorCompat paramAnonymousValueAnimatorCompat)
          {
            TabLayout.this.scrollTo(paramAnonymousValueAnimatorCompat.c(), 0);
          }
        });
      }
      this.v.a(i1, i2);
      this.v.a();
    }
    this.d.a(paramInt, 300);
  }
  
  private LinearLayout.LayoutParams e()
  {
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-2, -1);
    a(localLayoutParams);
    return localLayoutParams;
  }
  
  private void f()
  {
    int i1 = 0;
    if (this.t == 0) {
      i1 = Math.max(0, this.r - this.e);
    }
    ViewCompat.b(this.d, i1, 0, 0, 0);
    switch (this.t)
    {
    }
    for (;;)
    {
      a(true);
      return;
      this.d.setGravity(1);
      continue;
      this.d.setGravity(8388611);
    }
  }
  
  private int getDefaultHeight()
  {
    int i3 = 0;
    int i1 = 0;
    int i4 = this.b.size();
    for (;;)
    {
      int i2 = i3;
      if (i1 < i4)
      {
        Tab localTab = (Tab)this.b.get(i1);
        if ((localTab != null) && (localTab.b() != null) && (!TextUtils.isEmpty(localTab.d()))) {
          i2 = 1;
        }
      }
      else
      {
        if (i2 == 0) {
          break;
        }
        return 72;
      }
      i1 += 1;
    }
    return 48;
  }
  
  private float getScrollPosition()
  {
    return this.d.b();
  }
  
  private int getTabMaxWidth()
  {
    return this.n;
  }
  
  private int getTabMinWidth()
  {
    if (this.o != -1) {
      return this.o;
    }
    if (this.t == 0) {
      return this.q;
    }
    return 0;
  }
  
  private int getTabScrollRange()
  {
    return Math.max(0, this.d.getWidth() - getWidth() - getPaddingLeft() - getPaddingRight());
  }
  
  private void setSelectedTabView(int paramInt)
  {
    int i2 = this.d.getChildCount();
    if ((paramInt < i2) && (!this.d.getChildAt(paramInt).isSelected()))
    {
      int i1 = 0;
      if (i1 < i2)
      {
        View localView = this.d.getChildAt(i1);
        if (i1 == paramInt) {}
        for (boolean bool = true;; bool = false)
        {
          localView.setSelected(bool);
          i1 += 1;
          break;
        }
      }
    }
  }
  
  @NonNull
  public Tab a()
  {
    Tab localTab2 = (Tab)a.a();
    Tab localTab1 = localTab2;
    if (localTab2 == null) {
      localTab1 = new Tab(null);
    }
    Tab.a(localTab1, this);
    Tab.a(localTab1, c(localTab1));
    return localTab1;
  }
  
  @Nullable
  public Tab a(int paramInt)
  {
    return (Tab)this.b.get(paramInt);
  }
  
  public void a(@NonNull Tab paramTab)
  {
    a(paramTab, this.b.isEmpty());
  }
  
  public void a(@NonNull Tab paramTab, boolean paramBoolean)
  {
    if (Tab.a(paramTab) != this) {
      throw new IllegalArgumentException("Tab belongs to a different TabLayout.");
    }
    c(paramTab, paramBoolean);
    a(paramTab, this.b.size());
    if (paramBoolean) {
      paramTab.e();
    }
  }
  
  public void addView(View paramView)
  {
    a(paramView);
  }
  
  public void addView(View paramView, int paramInt)
  {
    a(paramView);
  }
  
  public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams)
  {
    a(paramView);
  }
  
  public void addView(View paramView, ViewGroup.LayoutParams paramLayoutParams)
  {
    a(paramView);
  }
  
  public void b()
  {
    int i1 = this.d.getChildCount() - 1;
    while (i1 >= 0)
    {
      c(i1);
      i1 -= 1;
    }
    Iterator localIterator = this.b.iterator();
    while (localIterator.hasNext())
    {
      Tab localTab = (Tab)localIterator.next();
      localIterator.remove();
      Tab.b(localTab);
      a.a(localTab);
    }
    this.c = null;
  }
  
  void b(Tab paramTab)
  {
    b(paramTab, true);
  }
  
  void b(Tab paramTab, boolean paramBoolean)
  {
    if (this.c == paramTab)
    {
      if (this.c != null)
      {
        if (this.u != null) {
          this.u.onTabReselected(this.c);
        }
        d(paramTab.c());
      }
      return;
    }
    int i1;
    if (paramBoolean)
    {
      if (paramTab == null) {
        break label157;
      }
      i1 = paramTab.c();
      label57:
      if (i1 != -1) {
        setSelectedTabView(i1);
      }
      if (((this.c != null) && (this.c.c() != -1)) || (i1 == -1)) {
        break label162;
      }
      setScrollPosition(i1, 0.0F, true);
    }
    for (;;)
    {
      if ((this.c != null) && (this.u != null)) {
        this.u.onTabUnselected(this.c);
      }
      this.c = paramTab;
      if ((this.c == null) || (this.u == null)) {
        break;
      }
      this.u.onTabSelected(this.c);
      return;
      label157:
      i1 = -1;
      break label57;
      label162:
      d(i1);
    }
  }
  
  public FrameLayout.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
  {
    return generateDefaultLayoutParams();
  }
  
  public int getSelectedTabPosition()
  {
    if (this.c != null) {
      return this.c.c();
    }
    return -1;
  }
  
  public int getTabCount()
  {
    return this.b.size();
  }
  
  public int getTabGravity()
  {
    return this.s;
  }
  
  public int getTabMode()
  {
    return this.t;
  }
  
  @Nullable
  public ColorStateList getTabTextColors()
  {
    return this.j;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i1 = b(getDefaultHeight()) + getPaddingTop() + getPaddingBottom();
    switch (View.MeasureSpec.getMode(paramInt2))
    {
    default: 
      label48:
      i1 = View.MeasureSpec.getSize(paramInt1);
      if (View.MeasureSpec.getMode(paramInt1) != 0)
      {
        if (this.p <= 0) {
          break label200;
        }
        i1 = this.p;
      }
      break;
    }
    View localView;
    for (;;)
    {
      this.n = i1;
      super.onMeasure(paramInt1, paramInt2);
      if (getChildCount() == 1)
      {
        localView = getChildAt(0);
        paramInt1 = 0;
      }
      switch (this.t)
      {
      default: 
        if (paramInt1 != 0)
        {
          paramInt1 = getChildMeasureSpec(paramInt2, getPaddingTop() + getPaddingBottom(), localView.getLayoutParams().height);
          localView.measure(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), paramInt1);
        }
        return;
        paramInt2 = View.MeasureSpec.makeMeasureSpec(Math.min(i1, View.MeasureSpec.getSize(paramInt2)), 1073741824);
        break label48;
        paramInt2 = View.MeasureSpec.makeMeasureSpec(i1, 1073741824);
        break label48;
        label200:
        i1 -= b(56);
      }
    }
    if (localView.getMeasuredWidth() < getMeasuredWidth()) {}
    for (paramInt1 = 1;; paramInt1 = 0) {
      break;
    }
    if (localView.getMeasuredWidth() != getMeasuredWidth()) {}
    for (paramInt1 = 1;; paramInt1 = 0) {
      break;
    }
  }
  
  public void setOnTabSelectedListener(OnTabSelectedListener paramOnTabSelectedListener)
  {
    this.u = paramOnTabSelectedListener;
  }
  
  public void setScrollPosition(int paramInt, float paramFloat, boolean paramBoolean)
  {
    a(paramInt, paramFloat, paramBoolean, true);
  }
  
  public void setSelectedTabIndicatorColor(@ColorInt int paramInt)
  {
    this.d.a(paramInt);
  }
  
  public void setSelectedTabIndicatorHeight(int paramInt)
  {
    this.d.b(paramInt);
  }
  
  public void setTabGravity(int paramInt)
  {
    if (this.s != paramInt)
    {
      this.s = paramInt;
      f();
    }
  }
  
  public void setTabMode(int paramInt)
  {
    if (paramInt != this.t)
    {
      this.t = paramInt;
      f();
    }
  }
  
  public void setTabTextColors(int paramInt1, int paramInt2)
  {
    setTabTextColors(a(paramInt1, paramInt2));
  }
  
  public void setTabTextColors(@Nullable ColorStateList paramColorStateList)
  {
    if (this.j != paramColorStateList)
    {
      this.j = paramColorStateList;
      d();
    }
  }
  
  @Deprecated
  public void setTabsFromPagerAdapter(@Nullable PagerAdapter paramPagerAdapter)
  {
    a(paramPagerAdapter, false);
  }
  
  public void setupWithViewPager(@Nullable ViewPager paramViewPager)
  {
    if ((this.w != null) && (this.z != null)) {
      this.w.b(this.z);
    }
    if (paramViewPager != null)
    {
      PagerAdapter localPagerAdapter = paramViewPager.getAdapter();
      if (localPagerAdapter == null) {
        throw new IllegalArgumentException("ViewPager does not have a PagerAdapter set");
      }
      this.w = paramViewPager;
      if (this.z == null) {
        this.z = new TabLayoutOnPageChangeListener(this);
      }
      TabLayoutOnPageChangeListener.a(this.z);
      paramViewPager.a(this.z);
      setOnTabSelectedListener(new ViewPagerOnTabSelectedListener(paramViewPager));
      a(localPagerAdapter, true);
      return;
    }
    this.w = null;
    setOnTabSelectedListener(null);
    a(null, true);
  }
  
  public boolean shouldDelayChildPressedState()
  {
    return getTabScrollRange() > 0;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Mode {}
  
  public static abstract interface OnTabSelectedListener
  {
    public abstract void onTabReselected(TabLayout.Tab paramTab);
    
    public abstract void onTabSelected(TabLayout.Tab paramTab);
    
    public abstract void onTabUnselected(TabLayout.Tab paramTab);
  }
  
  private class PagerAdapterObserver
    extends DataSetObserver
  {
    private PagerAdapterObserver() {}
    
    public void onChanged()
    {
      TabLayout.n(TabLayout.this);
    }
    
    public void onInvalidated()
    {
      TabLayout.n(TabLayout.this);
    }
  }
  
  private class SlidingTabStrip
    extends LinearLayout
  {
    private int b;
    private final Paint c;
    private int d = -1;
    private float e;
    private int f = -1;
    private int g = -1;
    private ValueAnimatorCompat h;
    
    SlidingTabStrip(Context paramContext)
    {
      super();
      setWillNotDraw(false);
      this.c = new Paint();
    }
    
    private void b(int paramInt1, int paramInt2)
    {
      if ((paramInt1 != this.f) || (paramInt2 != this.g))
      {
        this.f = paramInt1;
        this.g = paramInt2;
        ViewCompat.d(this);
      }
    }
    
    private void c()
    {
      View localView = getChildAt(this.d);
      int i;
      int j;
      if ((localView != null) && (localView.getWidth() > 0))
      {
        int m = localView.getLeft();
        int k = localView.getRight();
        i = m;
        j = k;
        if (this.e > 0.0F)
        {
          i = m;
          j = k;
          if (this.d < getChildCount() - 1)
          {
            localView = getChildAt(this.d + 1);
            i = (int)(this.e * localView.getLeft() + (1.0F - this.e) * m);
            j = (int)(this.e * localView.getRight() + (1.0F - this.e) * k);
          }
        }
      }
      for (;;)
      {
        b(i, j);
        return;
        j = -1;
        i = -1;
      }
    }
    
    void a(int paramInt)
    {
      if (this.c.getColor() != paramInt)
      {
        this.c.setColor(paramInt);
        ViewCompat.d(this);
      }
    }
    
    void a(int paramInt, float paramFloat)
    {
      if ((this.h != null) && (this.h.b())) {
        this.h.cancel();
      }
      this.d = paramInt;
      this.e = paramFloat;
      c();
    }
    
    void a(final int paramInt1, int paramInt2)
    {
      if ((this.h != null) && (this.h.b())) {
        this.h.cancel();
      }
      final int i;
      Object localObject;
      if (ViewCompat.h(this) == 1)
      {
        i = 1;
        localObject = getChildAt(paramInt1);
        if (localObject != null) {
          break label56;
        }
        c();
      }
      for (;;)
      {
        return;
        i = 0;
        break;
        label56:
        final int k = ((View)localObject).getLeft();
        final int m = ((View)localObject).getRight();
        final int j;
        if (Math.abs(paramInt1 - this.d) <= 1)
        {
          i = this.f;
          j = this.g;
        }
        while ((i != k) || (j != m))
        {
          localObject = ViewUtils.a();
          this.h = ((ValueAnimatorCompat)localObject);
          ((ValueAnimatorCompat)localObject).a(AnimationUtils.b);
          ((ValueAnimatorCompat)localObject).a(paramInt2);
          ((ValueAnimatorCompat)localObject).a(0.0F, 1.0F);
          ((ValueAnimatorCompat)localObject).a(new ValueAnimatorCompat.AnimatorUpdateListener()
          {
            public void a(ValueAnimatorCompat paramAnonymousValueAnimatorCompat)
            {
              float f = paramAnonymousValueAnimatorCompat.e();
              TabLayout.SlidingTabStrip.a(TabLayout.SlidingTabStrip.this, AnimationUtils.a(i, k, f), AnimationUtils.a(j, m, f));
            }
          });
          ((ValueAnimatorCompat)localObject).a(new ValueAnimatorCompat.AnimatorListenerAdapter()
          {
            public void a(ValueAnimatorCompat paramAnonymousValueAnimatorCompat)
            {
              TabLayout.SlidingTabStrip.a(TabLayout.SlidingTabStrip.this, paramInt1);
              TabLayout.SlidingTabStrip.a(TabLayout.SlidingTabStrip.this, 0.0F);
            }
          });
          ((ValueAnimatorCompat)localObject).a();
          return;
          j = TabLayout.a(TabLayout.this, 24);
          if (paramInt1 < this.d)
          {
            if (i != 0)
            {
              j = k - j;
              i = j;
            }
            else
            {
              j = m + j;
              i = j;
            }
          }
          else if (i != 0)
          {
            j = m + j;
            i = j;
          }
          else
          {
            j = k - j;
            i = j;
          }
        }
      }
    }
    
    boolean a()
    {
      int i = 0;
      int j = getChildCount();
      while (i < j)
      {
        if (getChildAt(i).getWidth() <= 0) {
          return true;
        }
        i += 1;
      }
      return false;
    }
    
    float b()
    {
      return this.d + this.e;
    }
    
    void b(int paramInt)
    {
      if (this.b != paramInt)
      {
        this.b = paramInt;
        ViewCompat.d(this);
      }
    }
    
    public void draw(Canvas paramCanvas)
    {
      super.draw(paramCanvas);
      if ((this.f >= 0) && (this.g > this.f)) {
        paramCanvas.drawRect(this.f, getHeight() - this.b, this.g, getHeight(), this.c);
      }
    }
    
    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
      if ((this.h != null) && (this.h.b()))
      {
        this.h.cancel();
        long l = this.h.f();
        a(this.d, Math.round((1.0F - this.h.e()) * (float)l));
        return;
      }
      c();
    }
    
    protected void onMeasure(int paramInt1, int paramInt2)
    {
      super.onMeasure(paramInt1, paramInt2);
      if (View.MeasureSpec.getMode(paramInt1) != 1073741824) {}
      int m;
      do
      {
        int n;
        int j;
        Object localObject;
        do
        {
          do
          {
            return;
          } while ((TabLayout.j(TabLayout.this) != 1) || (TabLayout.m(TabLayout.this) != 1));
          n = getChildCount();
          j = 0;
          i = 0;
          while (i < n)
          {
            localObject = getChildAt(i);
            k = j;
            if (((View)localObject).getVisibility() == 0) {
              k = Math.max(j, ((View)localObject).getMeasuredWidth());
            }
            i += 1;
            j = k;
          }
        } while (j <= 0);
        int k = TabLayout.a(TabLayout.this, 16);
        int i = 0;
        if (j * n <= getMeasuredWidth() - k * 2)
        {
          k = 0;
          for (;;)
          {
            m = i;
            if (k >= n) {
              break;
            }
            localObject = (LinearLayout.LayoutParams)getChildAt(k).getLayoutParams();
            if ((((LinearLayout.LayoutParams)localObject).width != j) || (((LinearLayout.LayoutParams)localObject).weight != 0.0F))
            {
              ((LinearLayout.LayoutParams)localObject).width = j;
              ((LinearLayout.LayoutParams)localObject).weight = 0.0F;
              i = 1;
            }
            k += 1;
          }
        }
        TabLayout.b(TabLayout.this, 0);
        TabLayout.a(TabLayout.this, false);
        m = 1;
      } while (m == 0);
      super.onMeasure(paramInt1, paramInt2);
    }
  }
  
  public static final class Tab
  {
    private Object a;
    private Drawable b;
    private CharSequence c;
    private CharSequence d;
    private int e = -1;
    private View f;
    private TabLayout g;
    private TabLayout.TabView h;
    
    private void g()
    {
      if (this.h != null) {
        this.h.a();
      }
    }
    
    private void h()
    {
      this.g = null;
      this.h = null;
      this.a = null;
      this.b = null;
      this.c = null;
      this.d = null;
      this.e = -1;
      this.f = null;
    }
    
    @NonNull
    public Tab a(@LayoutRes int paramInt)
    {
      return a(LayoutInflater.from(this.h.getContext()).inflate(paramInt, this.h, false));
    }
    
    @NonNull
    public Tab a(@Nullable Drawable paramDrawable)
    {
      this.b = paramDrawable;
      g();
      return this;
    }
    
    @NonNull
    public Tab a(@Nullable View paramView)
    {
      this.f = paramView;
      g();
      return this;
    }
    
    @NonNull
    public Tab a(@Nullable CharSequence paramCharSequence)
    {
      this.c = paramCharSequence;
      g();
      return this;
    }
    
    @Nullable
    public View a()
    {
      return this.f;
    }
    
    @Nullable
    public Drawable b()
    {
      return this.b;
    }
    
    void b(int paramInt)
    {
      this.e = paramInt;
    }
    
    public int c()
    {
      return this.e;
    }
    
    @Nullable
    public CharSequence d()
    {
      return this.c;
    }
    
    public void e()
    {
      if (this.g == null) {
        throw new IllegalArgumentException("Tab not attached to a TabLayout");
      }
      this.g.b(this);
    }
    
    @Nullable
    public CharSequence f()
    {
      return this.d;
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface TabGravity {}
  
  public static class TabLayoutOnPageChangeListener
    implements ViewPager.OnPageChangeListener
  {
    private final WeakReference<TabLayout> a;
    private int b;
    private int c;
    
    public TabLayoutOnPageChangeListener(TabLayout paramTabLayout)
    {
      this.a = new WeakReference(paramTabLayout);
    }
    
    private void a()
    {
      this.c = 0;
      this.b = 0;
    }
    
    public void onPageScrollStateChanged(int paramInt)
    {
      this.b = this.c;
      this.c = paramInt;
    }
    
    public void onPageScrolled(int paramInt1, float paramFloat, int paramInt2)
    {
      TabLayout localTabLayout = (TabLayout)this.a.get();
      boolean bool1;
      if (localTabLayout != null)
      {
        if ((this.c == 2) && (this.b != 1)) {
          break label66;
        }
        bool1 = true;
        if ((this.c == 2) && (this.b == 0)) {
          break label72;
        }
      }
      label66:
      label72:
      for (boolean bool2 = true;; bool2 = false)
      {
        TabLayout.a(localTabLayout, paramInt1, paramFloat, bool1, bool2);
        return;
        bool1 = false;
        break;
      }
    }
    
    public void onPageSelected(int paramInt)
    {
      TabLayout localTabLayout = (TabLayout)this.a.get();
      if ((localTabLayout != null) && (localTabLayout.getSelectedTabPosition() != paramInt)) {
        if ((this.c != 0) && ((this.c != 2) || (this.b != 0))) {
          break label58;
        }
      }
      label58:
      for (boolean bool = true;; bool = false)
      {
        localTabLayout.b(localTabLayout.a(paramInt), bool);
        return;
      }
    }
  }
  
  class TabView
    extends LinearLayout
    implements View.OnLongClickListener
  {
    private TabLayout.Tab b;
    private TextView c;
    private ImageView d;
    private View e;
    private TextView f;
    private ImageView g;
    private int h = 2;
    
    public TabView(Context paramContext)
    {
      super();
      if (TabLayout.a(TabLayout.this) != 0) {
        setBackgroundDrawable(AppCompatDrawableManager.a().a(paramContext, TabLayout.a(TabLayout.this)));
      }
      ViewCompat.b(this, TabLayout.b(TabLayout.this), TabLayout.c(TabLayout.this), TabLayout.d(TabLayout.this), TabLayout.e(TabLayout.this));
      setGravity(17);
      setOrientation(1);
      setClickable(true);
    }
    
    private float a(Layout paramLayout, int paramInt, float paramFloat)
    {
      return paramLayout.getLineWidth(paramInt) * (paramFloat / paramLayout.getPaint().getTextSize());
    }
    
    private void a(@Nullable TabLayout.Tab paramTab)
    {
      if (paramTab != this.b)
      {
        this.b = paramTab;
        a();
      }
    }
    
    private void a(@Nullable TextView paramTextView, @Nullable ImageView paramImageView)
    {
      Drawable localDrawable;
      CharSequence localCharSequence1;
      label32:
      CharSequence localCharSequence2;
      label48:
      label73:
      int i;
      if (this.b != null)
      {
        localDrawable = this.b.b();
        if (this.b == null) {
          break label207;
        }
        localCharSequence1 = this.b.d();
        if (this.b == null) {
          break label213;
        }
        localCharSequence2 = this.b.f();
        if (paramImageView != null)
        {
          if (localDrawable == null) {
            break label219;
          }
          paramImageView.setImageDrawable(localDrawable);
          paramImageView.setVisibility(0);
          setVisibility(0);
          paramImageView.setContentDescription(localCharSequence2);
        }
        if (TextUtils.isEmpty(localCharSequence1)) {
          break label233;
        }
        i = 1;
        label89:
        if (paramTextView != null)
        {
          if (i == 0) {
            break label238;
          }
          paramTextView.setText(localCharSequence1);
          paramTextView.setVisibility(0);
          setVisibility(0);
        }
      }
      for (;;)
      {
        paramTextView.setContentDescription(localCharSequence2);
        if (paramImageView != null)
        {
          paramTextView = (ViewGroup.MarginLayoutParams)paramImageView.getLayoutParams();
          int k = 0;
          int j = k;
          if (i != 0)
          {
            j = k;
            if (paramImageView.getVisibility() == 0) {
              j = TabLayout.a(TabLayout.this, 8);
            }
          }
          if (j != paramTextView.bottomMargin)
          {
            paramTextView.bottomMargin = j;
            paramImageView.requestLayout();
          }
        }
        if ((i != 0) || (TextUtils.isEmpty(localCharSequence2))) {
          break label252;
        }
        setOnLongClickListener(this);
        return;
        localDrawable = null;
        break;
        label207:
        localCharSequence1 = null;
        break label32;
        label213:
        localCharSequence2 = null;
        break label48;
        label219:
        paramImageView.setVisibility(8);
        paramImageView.setImageDrawable(null);
        break label73;
        label233:
        i = 0;
        break label89;
        label238:
        paramTextView.setVisibility(8);
        paramTextView.setText(null);
      }
      label252:
      setOnLongClickListener(null);
      setLongClickable(false);
    }
    
    private void b()
    {
      a(null);
      setSelected(false);
    }
    
    final void a()
    {
      Object localObject = this.b;
      if (localObject != null)
      {
        localObject = ((TabLayout.Tab)localObject).a();
        if (localObject == null) {
          break label285;
        }
        ViewParent localViewParent = ((View)localObject).getParent();
        if (localViewParent != this)
        {
          if (localViewParent != null) {
            ((ViewGroup)localViewParent).removeView((View)localObject);
          }
          addView((View)localObject);
        }
        this.e = ((View)localObject);
        if (this.c != null) {
          this.c.setVisibility(8);
        }
        if (this.d != null)
        {
          this.d.setVisibility(8);
          this.d.setImageDrawable(null);
        }
        this.f = ((TextView)((View)localObject).findViewById(16908308));
        if (this.f != null) {
          this.h = TextViewCompat.a(this.f);
        }
        this.g = ((ImageView)((View)localObject).findViewById(16908294));
        label134:
        if (this.e != null) {
          break label318;
        }
        if (this.d == null)
        {
          localObject = (ImageView)LayoutInflater.from(getContext()).inflate(R.layout.design_layout_tab_icon, this, false);
          addView((View)localObject, 0);
          this.d = ((ImageView)localObject);
        }
        if (this.c == null)
        {
          localObject = (TextView)LayoutInflater.from(getContext()).inflate(R.layout.design_layout_tab_text, this, false);
          addView((View)localObject);
          this.c = ((TextView)localObject);
          this.h = TextViewCompat.a(this.c);
        }
        this.c.setTextAppearance(getContext(), TabLayout.k(TabLayout.this));
        if (TabLayout.l(TabLayout.this) != null) {
          this.c.setTextColor(TabLayout.l(TabLayout.this));
        }
        a(this.c, this.d);
      }
      label285:
      label318:
      while ((this.f == null) && (this.g == null))
      {
        return;
        localObject = null;
        break;
        if (this.e != null)
        {
          removeView(this.e);
          this.e = null;
        }
        this.f = null;
        this.g = null;
        break label134;
      }
      a(this.f, this.g);
    }
    
    @TargetApi(14)
    public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
      super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
      paramAccessibilityEvent.setClassName(ActionBar.Tab.class.getName());
    }
    
    @TargetApi(14)
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
    {
      super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
      paramAccessibilityNodeInfo.setClassName(ActionBar.Tab.class.getName());
    }
    
    public boolean onLongClick(View paramView)
    {
      paramView = new int[2];
      getLocationOnScreen(paramView);
      Object localObject = getContext();
      int i = getWidth();
      int j = getHeight();
      int k = ((Context)localObject).getResources().getDisplayMetrics().widthPixels;
      localObject = Toast.makeText((Context)localObject, this.b.f(), 0);
      ((Toast)localObject).setGravity(49, paramView[0] + i / 2 - k / 2, j);
      ((Toast)localObject).show();
      return true;
    }
    
    public void onMeasure(int paramInt1, int paramInt2)
    {
      int i = View.MeasureSpec.getSize(paramInt1);
      int j = View.MeasureSpec.getMode(paramInt1);
      int k = TabLayout.f(TabLayout.this);
      float f2;
      float f1;
      if ((k > 0) && ((j == 0) || (i > k)))
      {
        paramInt1 = View.MeasureSpec.makeMeasureSpec(TabLayout.g(TabLayout.this), Integer.MIN_VALUE);
        super.onMeasure(paramInt1, paramInt2);
        if (this.c != null)
        {
          getResources();
          f2 = TabLayout.h(TabLayout.this);
          j = this.h;
          if ((this.d == null) || (this.d.getVisibility() != 0)) {
            break label265;
          }
          i = 1;
          f1 = f2;
        }
      }
      for (;;)
      {
        f2 = this.c.getTextSize();
        int m = this.c.getLineCount();
        j = TextViewCompat.a(this.c);
        if ((f1 != f2) || ((j >= 0) && (i != j)))
        {
          k = 1;
          j = k;
          if (TabLayout.j(TabLayout.this) == 1)
          {
            j = k;
            if (f1 > f2)
            {
              j = k;
              if (m == 1)
              {
                Layout localLayout = this.c.getLayout();
                if (localLayout != null)
                {
                  j = k;
                  if (a(localLayout, 0, f1) <= localLayout.getWidth()) {}
                }
                else
                {
                  j = 0;
                }
              }
            }
          }
          if (j != 0)
          {
            this.c.setTextSize(0, f1);
            this.c.setMaxLines(i);
            super.onMeasure(paramInt1, paramInt2);
          }
        }
        return;
        break;
        label265:
        i = j;
        f1 = f2;
        if (this.c != null)
        {
          i = j;
          f1 = f2;
          if (this.c.getLineCount() > 1)
          {
            f1 = TabLayout.i(TabLayout.this);
            i = j;
          }
        }
      }
    }
    
    public boolean performClick()
    {
      boolean bool = super.performClick();
      if (this.b != null)
      {
        this.b.e();
        bool = true;
      }
      return bool;
    }
    
    public void setSelected(boolean paramBoolean)
    {
      if (isSelected() != paramBoolean) {}
      for (int i = 1;; i = 0)
      {
        super.setSelected(paramBoolean);
        if ((i != 0) && (paramBoolean))
        {
          sendAccessibilityEvent(4);
          if (this.c != null) {
            this.c.setSelected(paramBoolean);
          }
          if (this.d != null) {
            this.d.setSelected(paramBoolean);
          }
        }
        return;
      }
    }
  }
  
  public static class ViewPagerOnTabSelectedListener
    implements TabLayout.OnTabSelectedListener
  {
    private final ViewPager a;
    
    public ViewPagerOnTabSelectedListener(ViewPager paramViewPager)
    {
      this.a = paramViewPager;
    }
    
    public void onTabReselected(TabLayout.Tab paramTab) {}
    
    public void onTabSelected(TabLayout.Tab paramTab)
    {
      this.a.setCurrentItem(paramTab.c());
    }
    
    public void onTabUnselected(TabLayout.Tab paramTab) {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/design/widget/TabLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */