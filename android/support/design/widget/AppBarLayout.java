package android.support.design.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.design.R.style;
import android.support.design.R.styleable;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.support.v4.view.OnApplyWindowInsetsListener;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.WindowInsetsCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

@CoordinatorLayout.DefaultBehavior(a=Behavior.class)
public class AppBarLayout
  extends LinearLayout
{
  boolean a;
  private int b = -1;
  private int c = -1;
  private int d = -1;
  private float e;
  private int f = 0;
  private WindowInsetsCompat g;
  private final List<OnOffsetChangedListener> h;
  
  public AppBarLayout(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public AppBarLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    setOrientation(1);
    ThemeUtils.a(paramContext);
    paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.AppBarLayout, 0, R.style.Widget_Design_AppBarLayout);
    this.e = paramContext.getDimensionPixelSize(R.styleable.AppBarLayout_elevation, 0);
    setBackgroundDrawable(paramContext.getDrawable(R.styleable.AppBarLayout_android_background));
    if (paramContext.hasValue(R.styleable.AppBarLayout_expanded)) {
      setExpanded(paramContext.getBoolean(R.styleable.AppBarLayout_expanded, false));
    }
    paramContext.recycle();
    ViewUtils.a(this);
    this.h = new ArrayList();
    ViewCompat.f(this, this.e);
    ViewCompat.a(this, new OnApplyWindowInsetsListener()
    {
      public WindowInsetsCompat a(View paramAnonymousView, WindowInsetsCompat paramAnonymousWindowInsetsCompat)
      {
        return AppBarLayout.a(AppBarLayout.this, paramAnonymousWindowInsetsCompat);
      }
    });
  }
  
  private WindowInsetsCompat a(WindowInsetsCompat paramWindowInsetsCompat)
  {
    WindowInsetsCompat localWindowInsetsCompat = null;
    if (ViewCompat.x(this)) {
      localWindowInsetsCompat = paramWindowInsetsCompat;
    }
    if (localWindowInsetsCompat != this.g)
    {
      this.g = localWindowInsetsCompat;
      b();
    }
    return paramWindowInsetsCompat;
  }
  
  private void b()
  {
    this.b = -1;
    this.c = -1;
    this.d = -1;
  }
  
  private boolean c()
  {
    return this.a;
  }
  
  private boolean d()
  {
    return getTotalScrollRange() != 0;
  }
  
  private void e()
  {
    this.f = 0;
  }
  
  private int getDownNestedPreScrollRange()
  {
    if (this.c != -1) {
      return this.c;
    }
    int k = 0;
    int j = getChildCount() - 1;
    if (j >= 0)
    {
      View localView = getChildAt(j);
      LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
      i = localView.getMeasuredHeight();
      int m = localLayoutParams.a;
      if ((m & 0x5) == 5)
      {
        k += localLayoutParams.topMargin + localLayoutParams.bottomMargin;
        if ((m & 0x8) != 0) {
          i = k + ViewCompat.r(localView);
        }
      }
      do
      {
        for (;;)
        {
          j -= 1;
          k = i;
          break;
          if ((m & 0x2) != 0) {
            i = k + (i - ViewCompat.r(localView));
          } else {
            i = k + i;
          }
        }
        i = k;
      } while (k <= 0);
    }
    int i = Math.max(0, k - getTopInset());
    this.c = i;
    return i;
  }
  
  private int getDownNestedScrollRange()
  {
    if (this.d != -1) {
      return this.d;
    }
    int i = 0;
    int j = 0;
    int m = getChildCount();
    for (;;)
    {
      int k = i;
      if (j < m)
      {
        View localView = getChildAt(j);
        LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
        int i1 = localView.getMeasuredHeight();
        int i2 = localLayoutParams.topMargin;
        int i3 = localLayoutParams.bottomMargin;
        int n = localLayoutParams.a;
        k = i;
        if ((n & 0x1) != 0)
        {
          i += i1 + (i2 + i3);
          if ((n & 0x2) == 0) {
            break label129;
          }
          k = i - (ViewCompat.r(localView) + getTopInset());
        }
      }
      i = Math.max(0, k);
      this.d = i;
      return i;
      label129:
      j += 1;
    }
  }
  
  private int getPendingAction()
  {
    return this.f;
  }
  
  private int getTopInset()
  {
    if (this.g != null) {
      return this.g.b();
    }
    return 0;
  }
  
  private int getUpNestedPreScrollRange()
  {
    return getTotalScrollRange();
  }
  
  protected LayoutParams a()
  {
    return new LayoutParams(-1, -2);
  }
  
  public LayoutParams a(AttributeSet paramAttributeSet)
  {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected LayoutParams a(ViewGroup.LayoutParams paramLayoutParams)
  {
    if ((paramLayoutParams instanceof LinearLayout.LayoutParams)) {
      return new LayoutParams((LinearLayout.LayoutParams)paramLayoutParams);
    }
    if ((paramLayoutParams instanceof ViewGroup.MarginLayoutParams)) {
      return new LayoutParams((ViewGroup.MarginLayoutParams)paramLayoutParams);
    }
    return new LayoutParams(paramLayoutParams);
  }
  
  public void a(OnOffsetChangedListener paramOnOffsetChangedListener)
  {
    if ((paramOnOffsetChangedListener != null) && (!this.h.contains(paramOnOffsetChangedListener))) {
      this.h.add(paramOnOffsetChangedListener);
    }
  }
  
  public void b(OnOffsetChangedListener paramOnOffsetChangedListener)
  {
    if (paramOnOffsetChangedListener != null) {
      this.h.remove(paramOnOffsetChangedListener);
    }
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  final int getMinimumHeightForVisibleOverlappingContent()
  {
    int i = getTopInset();
    int j = ViewCompat.r(this);
    if (j != 0) {
      return j * 2 + i;
    }
    j = getChildCount();
    if (j >= 1) {
      return ViewCompat.r(getChildAt(j - 1)) * 2 + i;
    }
    return 0;
  }
  
  public float getTargetElevation()
  {
    return this.e;
  }
  
  public final int getTotalScrollRange()
  {
    if (this.b != -1) {
      return this.b;
    }
    int i = 0;
    int j = 0;
    int m = getChildCount();
    for (;;)
    {
      int k = i;
      if (j < m)
      {
        View localView = getChildAt(j);
        LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
        int i1 = localView.getMeasuredHeight();
        int n = localLayoutParams.a;
        k = i;
        if ((n & 0x1) != 0)
        {
          i += localLayoutParams.topMargin + i1 + localLayoutParams.bottomMargin;
          if ((n & 0x2) == 0) {
            break label121;
          }
          k = i - ViewCompat.r(localView);
        }
      }
      i = Math.max(0, k - getTopInset());
      this.b = i;
      return i;
      label121:
      j += 1;
    }
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    b();
    this.a = false;
    paramInt1 = 0;
    paramInt2 = getChildCount();
    for (;;)
    {
      if (paramInt1 < paramInt2)
      {
        if (((LayoutParams)getChildAt(paramInt1).getLayoutParams()).b() != null) {
          this.a = true;
        }
      }
      else {
        return;
      }
      paramInt1 += 1;
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    b();
  }
  
  public void setExpanded(boolean paramBoolean)
  {
    setExpanded(paramBoolean, ViewCompat.F(this));
  }
  
  public void setExpanded(boolean paramBoolean1, boolean paramBoolean2)
  {
    int i;
    if (paramBoolean1)
    {
      i = 1;
      if (!paramBoolean2) {
        break label31;
      }
    }
    label31:
    for (int j = 4;; j = 0)
    {
      this.f = (j | i);
      requestLayout();
      return;
      i = 2;
      break;
    }
  }
  
  public void setOrientation(int paramInt)
  {
    if (paramInt != 1) {
      throw new IllegalArgumentException("AppBarLayout is always vertical and does not support horizontal orientation");
    }
    super.setOrientation(paramInt);
  }
  
  public void setTargetElevation(float paramFloat)
  {
    this.e = paramFloat;
  }
  
  public static class Behavior
    extends HeaderBehavior<AppBarLayout>
  {
    private int a;
    private boolean b;
    private boolean c;
    private ValueAnimatorCompat d;
    private int e = -1;
    private boolean f;
    private float g;
    private WeakReference<View> h;
    private DragCallback i;
    
    public Behavior() {}
    
    public Behavior(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
    }
    
    private int a(AppBarLayout paramAppBarLayout, int paramInt)
    {
      int j = 0;
      int k = paramAppBarLayout.getChildCount();
      while (j < k)
      {
        View localView = paramAppBarLayout.getChildAt(j);
        if ((localView.getTop() <= -paramInt) && (localView.getBottom() >= -paramInt)) {
          return j;
        }
        j += 1;
      }
      return -1;
    }
    
    private static boolean a(int paramInt1, int paramInt2)
    {
      return (paramInt1 & paramInt2) == paramInt2;
    }
    
    private int b(AppBarLayout paramAppBarLayout, int paramInt)
    {
      int m = Math.abs(paramInt);
      int k = 0;
      int n = paramAppBarLayout.getChildCount();
      for (;;)
      {
        int j = paramInt;
        if (k < n)
        {
          View localView = paramAppBarLayout.getChildAt(k);
          AppBarLayout.LayoutParams localLayoutParams = (AppBarLayout.LayoutParams)localView.getLayoutParams();
          Interpolator localInterpolator = localLayoutParams.b();
          if ((m < localView.getTop()) || (m > localView.getBottom())) {
            break label203;
          }
          j = paramInt;
          if (localInterpolator != null)
          {
            j = 0;
            n = localLayoutParams.a();
            if ((n & 0x1) != 0)
            {
              k = 0 + (localView.getHeight() + localLayoutParams.topMargin + localLayoutParams.bottomMargin);
              j = k;
              if ((n & 0x2) != 0) {
                j = k - ViewCompat.r(localView);
              }
            }
            k = j;
            if (ViewCompat.x(localView)) {
              k = j - AppBarLayout.e(paramAppBarLayout);
            }
            j = paramInt;
            if (k > 0)
            {
              j = localView.getTop();
              j = Math.round(k * localInterpolator.getInterpolation((m - j) / k));
              j = Integer.signum(paramInt) * (localView.getTop() + j);
            }
          }
        }
        return j;
        label203:
        k += 1;
      }
    }
    
    private void b(final CoordinatorLayout paramCoordinatorLayout, final AppBarLayout paramAppBarLayout, int paramInt)
    {
      int j = a();
      if (j == paramInt)
      {
        if ((this.d != null) && (this.d.b())) {
          this.d.cancel();
        }
        return;
      }
      if (this.d == null)
      {
        this.d = ViewUtils.a();
        this.d.a(AnimationUtils.e);
        this.d.a(new ValueAnimatorCompat.AnimatorUpdateListener()
        {
          public void a(ValueAnimatorCompat paramAnonymousValueAnimatorCompat)
          {
            AppBarLayout.Behavior.this.a_(paramCoordinatorLayout, paramAppBarLayout, paramAnonymousValueAnimatorCompat.c());
          }
        });
      }
      for (;;)
      {
        float f1 = Math.abs(j - paramInt) / paramCoordinatorLayout.getResources().getDisplayMetrics().density;
        this.d.a(Math.round(1000.0F * f1 / 300.0F));
        this.d.a(j, paramInt);
        this.d.a();
        return;
        this.d.cancel();
      }
    }
    
    private void c(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout)
    {
      int i1 = a();
      int m = a(paramAppBarLayout, i1);
      View localView;
      int i2;
      int n;
      int j;
      int k;
      if (m >= 0)
      {
        localView = paramAppBarLayout.getChildAt(m);
        i2 = ((AppBarLayout.LayoutParams)localView.getLayoutParams()).a();
        if ((i2 & 0x11) == 17)
        {
          n = -localView.getTop();
          j = -localView.getBottom();
          k = j;
          if (m == paramAppBarLayout.getChildCount() - 1) {
            k = j + AppBarLayout.e(paramAppBarLayout);
          }
          if (!a(i2, 2)) {
            break label138;
          }
          j = k + ViewCompat.r(localView);
          m = n;
          if (i1 >= (j + m) / 2) {
            break label185;
          }
        }
      }
      for (;;)
      {
        b(paramCoordinatorLayout, paramAppBarLayout, MathUtils.a(j, -paramAppBarLayout.getTotalScrollRange(), 0));
        return;
        label138:
        j = k;
        m = n;
        if (!a(i2, 5)) {
          break;
        }
        j = k + ViewCompat.r(localView);
        if (i1 < j)
        {
          m = j;
          j = k;
          break;
        }
        m = n;
        break;
        label185:
        j = m;
      }
    }
    
    private void d(AppBarLayout paramAppBarLayout)
    {
      List localList = AppBarLayout.i(paramAppBarLayout);
      int j = 0;
      int k = localList.size();
      while (j < k)
      {
        AppBarLayout.OnOffsetChangedListener localOnOffsetChangedListener = (AppBarLayout.OnOffsetChangedListener)localList.get(j);
        if (localOnOffsetChangedListener != null) {
          localOnOffsetChangedListener.a(paramAppBarLayout, b());
        }
        j += 1;
      }
    }
    
    int a()
    {
      return b() + this.a;
    }
    
    int a(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, int paramInt1, int paramInt2, int paramInt3)
    {
      int k = a();
      int j = 0;
      if ((paramInt2 != 0) && (k >= paramInt2) && (k <= paramInt3))
      {
        paramInt2 = MathUtils.a(paramInt1, paramInt2, paramInt3);
        paramInt1 = j;
        if (k != paramInt2) {
          if (!AppBarLayout.h(paramAppBarLayout)) {
            break label112;
          }
        }
        label112:
        for (paramInt1 = b(paramAppBarLayout, paramInt2);; paramInt1 = paramInt2)
        {
          boolean bool = a(paramInt1);
          paramInt3 = k - paramInt2;
          this.a = (paramInt2 - paramInt1);
          if ((!bool) && (AppBarLayout.h(paramAppBarLayout))) {
            paramCoordinatorLayout.c(paramAppBarLayout);
          }
          d(paramAppBarLayout);
          paramInt1 = paramInt3;
          return paramInt1;
        }
      }
      this.a = 0;
      return 0;
    }
    
    void a(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout)
    {
      c(paramCoordinatorLayout, paramAppBarLayout);
    }
    
    public void a(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, Parcelable paramParcelable)
    {
      if ((paramParcelable instanceof SavedState))
      {
        paramParcelable = (SavedState)paramParcelable;
        super.a(paramCoordinatorLayout, paramAppBarLayout, paramParcelable.getSuperState());
        this.e = paramParcelable.a;
        this.g = paramParcelable.b;
        this.f = paramParcelable.c;
        return;
      }
      super.a(paramCoordinatorLayout, paramAppBarLayout, paramParcelable);
      this.e = -1;
    }
    
    public void a(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, View paramView)
    {
      if (!this.c) {
        c(paramCoordinatorLayout, paramAppBarLayout);
      }
      this.b = false;
      this.c = false;
      this.h = new WeakReference(paramView);
    }
    
    public void a(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      if (paramInt4 < 0)
      {
        b(paramCoordinatorLayout, paramAppBarLayout, paramInt4, -AppBarLayout.d(paramAppBarLayout), 0);
        this.b = true;
        return;
      }
      this.b = false;
    }
    
    public void a(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, View paramView, int paramInt1, int paramInt2, int[] paramArrayOfInt)
    {
      if ((paramInt2 != 0) && (!this.b))
      {
        if (paramInt2 >= 0) {
          break label50;
        }
        paramInt1 = -paramAppBarLayout.getTotalScrollRange();
      }
      for (int j = paramInt1 + AppBarLayout.b(paramAppBarLayout);; j = 0)
      {
        paramArrayOfInt[1] = b(paramCoordinatorLayout, paramAppBarLayout, paramInt2, paramInt1, j);
        return;
        label50:
        paramInt1 = -AppBarLayout.c(paramAppBarLayout);
      }
    }
    
    boolean a(AppBarLayout paramAppBarLayout)
    {
      boolean bool2 = true;
      boolean bool1;
      if (this.i != null) {
        bool1 = this.i.a(paramAppBarLayout);
      }
      do
      {
        do
        {
          return bool1;
          bool1 = bool2;
        } while (this.h == null);
        paramAppBarLayout = (View)this.h.get();
        if ((paramAppBarLayout == null) || (!paramAppBarLayout.isShown())) {
          break;
        }
        bool1 = bool2;
      } while (!ViewCompat.b(paramAppBarLayout, -1));
      return false;
    }
    
    public boolean a(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, int paramInt)
    {
      boolean bool = super.a(paramCoordinatorLayout, paramAppBarLayout, paramInt);
      int j = AppBarLayout.f(paramAppBarLayout);
      if (j != 0) {
        if ((j & 0x4) != 0)
        {
          paramInt = 1;
          if ((j & 0x2) == 0) {
            break label107;
          }
          j = -AppBarLayout.c(paramAppBarLayout);
          if (paramInt == 0) {
            break label95;
          }
          b(paramCoordinatorLayout, paramAppBarLayout, j);
        }
      }
      label95:
      label107:
      while (this.e < 0) {
        for (;;)
        {
          AppBarLayout.g(paramAppBarLayout);
          this.e = -1;
          a(MathUtils.a(b(), -paramAppBarLayout.getTotalScrollRange(), 0));
          d(paramAppBarLayout);
          return bool;
          paramInt = 0;
          continue;
          a_(paramCoordinatorLayout, paramAppBarLayout, j);
          continue;
          if ((j & 0x1) != 0) {
            if (paramInt != 0) {
              b(paramCoordinatorLayout, paramAppBarLayout, 0);
            } else {
              a_(paramCoordinatorLayout, paramAppBarLayout, 0);
            }
          }
        }
      }
      paramCoordinatorLayout = paramAppBarLayout.getChildAt(this.e);
      paramInt = -paramCoordinatorLayout.getBottom();
      if (this.f) {
        paramInt += ViewCompat.r(paramCoordinatorLayout);
      }
      for (;;)
      {
        a(paramInt);
        break;
        paramInt += Math.round(paramCoordinatorLayout.getHeight() * this.g);
      }
    }
    
    public boolean a(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      if (((CoordinatorLayout.LayoutParams)paramAppBarLayout.getLayoutParams()).height == -2)
      {
        paramCoordinatorLayout.a(paramAppBarLayout, paramInt1, paramInt2, View.MeasureSpec.makeMeasureSpec(0, 0), paramInt4);
        return true;
      }
      return super.a(paramCoordinatorLayout, paramAppBarLayout, paramInt1, paramInt2, paramInt3, paramInt4);
    }
    
    public boolean a(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, View paramView, float paramFloat1, float paramFloat2, boolean paramBoolean)
    {
      boolean bool = false;
      if (!paramBoolean) {
        paramBoolean = a(paramCoordinatorLayout, paramAppBarLayout, -paramAppBarLayout.getTotalScrollRange(), 0, -paramFloat2);
      }
      for (;;)
      {
        this.c = paramBoolean;
        return paramBoolean;
        int j;
        if (paramFloat2 < 0.0F)
        {
          j = -paramAppBarLayout.getTotalScrollRange() + AppBarLayout.b(paramAppBarLayout);
          paramBoolean = bool;
          if (a() < j)
          {
            b(paramCoordinatorLayout, paramAppBarLayout, j);
            paramBoolean = true;
          }
        }
        else
        {
          j = -AppBarLayout.c(paramAppBarLayout);
          paramBoolean = bool;
          if (a() > j)
          {
            b(paramCoordinatorLayout, paramAppBarLayout, j);
            paramBoolean = true;
          }
        }
      }
    }
    
    public boolean a(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, View paramView1, View paramView2, int paramInt)
    {
      if (((paramInt & 0x2) != 0) && (AppBarLayout.a(paramAppBarLayout)) && (paramCoordinatorLayout.getHeight() - paramView1.getHeight() <= paramAppBarLayout.getHeight())) {}
      for (boolean bool = true;; bool = false)
      {
        if ((bool) && (this.d != null)) {
          this.d.cancel();
        }
        this.h = null;
        return bool;
      }
    }
    
    int b(AppBarLayout paramAppBarLayout)
    {
      return -AppBarLayout.d(paramAppBarLayout);
    }
    
    public Parcelable b(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout)
    {
      Parcelable localParcelable = super.b(paramCoordinatorLayout, paramAppBarLayout);
      int k = b();
      int j = 0;
      int m = paramAppBarLayout.getChildCount();
      while (j < m)
      {
        paramCoordinatorLayout = paramAppBarLayout.getChildAt(j);
        int n = paramCoordinatorLayout.getBottom() + k;
        if ((paramCoordinatorLayout.getTop() + k <= 0) && (n >= 0))
        {
          paramAppBarLayout = new SavedState(localParcelable);
          paramAppBarLayout.a = j;
          if (n == ViewCompat.r(paramCoordinatorLayout)) {}
          for (boolean bool = true;; bool = false)
          {
            paramAppBarLayout.c = bool;
            paramAppBarLayout.b = (n / paramCoordinatorLayout.getHeight());
            return paramAppBarLayout;
          }
        }
        j += 1;
      }
      return localParcelable;
    }
    
    int c(AppBarLayout paramAppBarLayout)
    {
      return paramAppBarLayout.getTotalScrollRange();
    }
    
    public static abstract class DragCallback
    {
      public abstract boolean a(@NonNull AppBarLayout paramAppBarLayout);
    }
    
    protected static class SavedState
      extends View.BaseSavedState
    {
      public static final Parcelable.Creator<SavedState> CREATOR = ParcelableCompat.a(new ParcelableCompatCreatorCallbacks()
      {
        public AppBarLayout.Behavior.SavedState a(Parcel paramAnonymousParcel, ClassLoader paramAnonymousClassLoader)
        {
          return new AppBarLayout.Behavior.SavedState(paramAnonymousParcel, paramAnonymousClassLoader);
        }
        
        public AppBarLayout.Behavior.SavedState[] a(int paramAnonymousInt)
        {
          return new AppBarLayout.Behavior.SavedState[paramAnonymousInt];
        }
      });
      int a;
      float b;
      boolean c;
      
      public SavedState(Parcel paramParcel, ClassLoader paramClassLoader)
      {
        super();
        this.a = paramParcel.readInt();
        this.b = paramParcel.readFloat();
        if (paramParcel.readByte() != 0) {}
        for (boolean bool = true;; bool = false)
        {
          this.c = bool;
          return;
        }
      }
      
      public SavedState(Parcelable paramParcelable)
      {
        super();
      }
      
      public void writeToParcel(Parcel paramParcel, int paramInt)
      {
        super.writeToParcel(paramParcel, paramInt);
        paramParcel.writeInt(this.a);
        paramParcel.writeFloat(this.b);
        if (this.c) {}
        for (paramInt = 1;; paramInt = 0)
        {
          paramParcel.writeByte((byte)paramInt);
          return;
        }
      }
    }
  }
  
  public static class LayoutParams
    extends LinearLayout.LayoutParams
  {
    int a = 1;
    Interpolator b;
    
    public LayoutParams(int paramInt1, int paramInt2)
    {
      super(paramInt2);
    }
    
    public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
      paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.AppBarLayout_LayoutParams);
      this.a = paramAttributeSet.getInt(R.styleable.AppBarLayout_LayoutParams_layout_scrollFlags, 0);
      if (paramAttributeSet.hasValue(R.styleable.AppBarLayout_LayoutParams_layout_scrollInterpolator)) {
        this.b = android.view.animation.AnimationUtils.loadInterpolator(paramContext, paramAttributeSet.getResourceId(R.styleable.AppBarLayout_LayoutParams_layout_scrollInterpolator, 0));
      }
      paramAttributeSet.recycle();
    }
    
    public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
    }
    
    public LayoutParams(ViewGroup.MarginLayoutParams paramMarginLayoutParams)
    {
      super();
    }
    
    public LayoutParams(LinearLayout.LayoutParams paramLayoutParams)
    {
      super();
    }
    
    public int a()
    {
      return this.a;
    }
    
    public Interpolator b()
    {
      return this.b;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface ScrollFlags {}
  }
  
  public static abstract interface OnOffsetChangedListener
  {
    public abstract void a(AppBarLayout paramAppBarLayout, int paramInt);
  }
  
  public static class ScrollingViewBehavior
    extends HeaderScrollingViewBehavior
  {
    public ScrollingViewBehavior() {}
    
    public ScrollingViewBehavior(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
      paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ScrollingViewBehavior_Params);
      b(paramContext.getDimensionPixelSize(R.styleable.ScrollingViewBehavior_Params_behavior_overlapTop, 0));
      paramContext.recycle();
    }
    
    private static int a(AppBarLayout paramAppBarLayout)
    {
      paramAppBarLayout = ((CoordinatorLayout.LayoutParams)paramAppBarLayout.getLayoutParams()).b();
      if ((paramAppBarLayout instanceof AppBarLayout.Behavior)) {
        return ((AppBarLayout.Behavior)paramAppBarLayout).a();
      }
      return 0;
    }
    
    private void e(CoordinatorLayout paramCoordinatorLayout, View paramView1, View paramView2)
    {
      paramCoordinatorLayout = ((CoordinatorLayout.LayoutParams)paramView2.getLayoutParams()).b();
      if ((paramCoordinatorLayout instanceof AppBarLayout.Behavior))
      {
        paramCoordinatorLayout = (AppBarLayout.Behavior)paramCoordinatorLayout;
        paramCoordinatorLayout.a();
        ViewCompat.e(paramView1, paramView2.getBottom() - paramView1.getTop() + AppBarLayout.Behavior.a(paramCoordinatorLayout) + a() - c(paramView2));
      }
    }
    
    float a(View paramView)
    {
      int j;
      int k;
      int i;
      if ((paramView instanceof AppBarLayout))
      {
        paramView = (AppBarLayout)paramView;
        j = paramView.getTotalScrollRange();
        k = AppBarLayout.b(paramView);
        i = a(paramView);
        if ((k == 0) || (j + i > k)) {
          break label43;
        }
      }
      label43:
      do
      {
        return 0.0F;
        j -= k;
      } while (j == 0);
      return 1.0F + i / j;
    }
    
    View a(List<View> paramList)
    {
      int i = 0;
      int j = paramList.size();
      while (i < j)
      {
        View localView = (View)paramList.get(i);
        if ((localView instanceof AppBarLayout)) {
          return localView;
        }
        i += 1;
      }
      return null;
    }
    
    int b(View paramView)
    {
      if ((paramView instanceof AppBarLayout)) {
        return ((AppBarLayout)paramView).getTotalScrollRange();
      }
      return super.b(paramView);
    }
    
    public boolean b(CoordinatorLayout paramCoordinatorLayout, View paramView1, View paramView2)
    {
      return paramView2 instanceof AppBarLayout;
    }
    
    public boolean c(CoordinatorLayout paramCoordinatorLayout, View paramView1, View paramView2)
    {
      e(paramCoordinatorLayout, paramView1, paramView2);
      return false;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/design/widget/AppBarLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */