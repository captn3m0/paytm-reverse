package android.support.design.widget;

import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.VelocityTrackerCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ScrollerCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;

abstract class HeaderBehavior<V extends View>
  extends ViewOffsetBehavior<V>
{
  private Runnable a;
  private ScrollerCompat b;
  private boolean c;
  private int d = -1;
  private int e;
  private int f = -1;
  private VelocityTracker g;
  
  public HeaderBehavior() {}
  
  public HeaderBehavior(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  private void c()
  {
    if (this.g == null) {
      this.g = VelocityTracker.obtain();
    }
  }
  
  int a()
  {
    return b();
  }
  
  int a(CoordinatorLayout paramCoordinatorLayout, V paramV, int paramInt1, int paramInt2, int paramInt3)
  {
    int k = b();
    int j = 0;
    int i = j;
    if (paramInt2 != 0)
    {
      i = j;
      if (k >= paramInt2)
      {
        i = j;
        if (k <= paramInt3)
        {
          paramInt1 = MathUtils.a(paramInt1, paramInt2, paramInt3);
          i = j;
          if (k != paramInt1)
          {
            a(paramInt1);
            i = k - paramInt1;
          }
        }
      }
    }
    return i;
  }
  
  int a(V paramV)
  {
    return paramV.getHeight();
  }
  
  void a(CoordinatorLayout paramCoordinatorLayout, V paramV) {}
  
  final boolean a(CoordinatorLayout paramCoordinatorLayout, V paramV, int paramInt1, int paramInt2, float paramFloat)
  {
    if (this.a != null)
    {
      paramV.removeCallbacks(this.a);
      this.a = null;
    }
    if (this.b == null) {
      this.b = ScrollerCompat.a(paramV.getContext());
    }
    this.b.a(0, b(), 0, Math.round(paramFloat), 0, 0, paramInt1, paramInt2);
    if (this.b.g())
    {
      this.a = new FlingRunnable(paramCoordinatorLayout, paramV);
      ViewCompat.a(paramV, this.a);
      return true;
    }
    a(paramCoordinatorLayout, paramV);
    return false;
  }
  
  public boolean a(CoordinatorLayout paramCoordinatorLayout, V paramV, MotionEvent paramMotionEvent)
  {
    if (this.f < 0) {
      this.f = ViewConfiguration.get(paramCoordinatorLayout.getContext()).getScaledTouchSlop();
    }
    if ((paramMotionEvent.getAction() == 2) && (this.c)) {
      return true;
    }
    switch (MotionEventCompat.a(paramMotionEvent))
    {
    }
    for (;;)
    {
      if (this.g != null) {
        this.g.addMovement(paramMotionEvent);
      }
      return this.c;
      this.c = false;
      int i = (int)paramMotionEvent.getX();
      int j = (int)paramMotionEvent.getY();
      if ((c(paramV)) && (paramCoordinatorLayout.a(paramV, i, j)))
      {
        this.e = j;
        this.d = MotionEventCompat.b(paramMotionEvent, 0);
        c();
        continue;
        i = this.d;
        if (i != -1)
        {
          i = MotionEventCompat.a(paramMotionEvent, i);
          if (i != -1)
          {
            i = (int)MotionEventCompat.d(paramMotionEvent, i);
            if (Math.abs(i - this.e) > this.f)
            {
              this.c = true;
              this.e = i;
              continue;
              this.c = false;
              this.d = -1;
              if (this.g != null)
              {
                this.g.recycle();
                this.g = null;
              }
            }
          }
        }
      }
    }
  }
  
  int a_(CoordinatorLayout paramCoordinatorLayout, V paramV, int paramInt)
  {
    return a(paramCoordinatorLayout, paramV, paramInt, Integer.MIN_VALUE, Integer.MAX_VALUE);
  }
  
  final int b(CoordinatorLayout paramCoordinatorLayout, V paramV, int paramInt1, int paramInt2, int paramInt3)
  {
    return a(paramCoordinatorLayout, paramV, a() - paramInt1, paramInt2, paramInt3);
  }
  
  int b(V paramV)
  {
    return -paramV.getHeight();
  }
  
  public boolean b(CoordinatorLayout paramCoordinatorLayout, V paramV, MotionEvent paramMotionEvent)
  {
    if (this.f < 0) {
      this.f = ViewConfiguration.get(paramCoordinatorLayout.getContext()).getScaledTouchSlop();
    }
    switch (MotionEventCompat.a(paramMotionEvent))
    {
    }
    for (;;)
    {
      if (this.g != null) {
        this.g.addMovement(paramMotionEvent);
      }
      return true;
      int i = (int)paramMotionEvent.getX();
      int j = (int)paramMotionEvent.getY();
      if ((paramCoordinatorLayout.a(paramV, i, j)) && (c(paramV)))
      {
        this.e = j;
        this.d = MotionEventCompat.b(paramMotionEvent, 0);
        c();
      }
      else
      {
        return false;
        i = MotionEventCompat.a(paramMotionEvent, this.d);
        if (i == -1) {
          return false;
        }
        int k = (int)MotionEventCompat.d(paramMotionEvent, i);
        j = this.e - k;
        i = j;
        if (!this.c)
        {
          i = j;
          if (Math.abs(j) > this.f)
          {
            this.c = true;
            if (j <= 0) {
              break label244;
            }
          }
        }
        label244:
        for (i = j - this.f; this.c; i = j + this.f)
        {
          this.e = k;
          b(paramCoordinatorLayout, paramV, i, b(paramV), 0);
          break;
        }
        if (this.g != null)
        {
          this.g.addMovement(paramMotionEvent);
          this.g.computeCurrentVelocity(1000);
          float f1 = VelocityTrackerCompat.b(this.g, this.d);
          a(paramCoordinatorLayout, paramV, -a(paramV), 0, f1);
        }
        this.c = false;
        this.d = -1;
        if (this.g != null)
        {
          this.g.recycle();
          this.g = null;
        }
      }
    }
  }
  
  boolean c(V paramV)
  {
    return false;
  }
  
  private class FlingRunnable
    implements Runnable
  {
    private final CoordinatorLayout b;
    private final V c;
    
    FlingRunnable(V paramV)
    {
      this.b = paramV;
      View localView;
      this.c = localView;
    }
    
    public void run()
    {
      if ((this.c != null) && (HeaderBehavior.a(HeaderBehavior.this) != null))
      {
        if (HeaderBehavior.a(HeaderBehavior.this).g())
        {
          HeaderBehavior.this.a_(this.b, this.c, HeaderBehavior.a(HeaderBehavior.this).c());
          ViewCompat.a(this.c, this);
        }
      }
      else {
        return;
      }
      HeaderBehavior.this.a(this.b, this.c);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/design/widget/HeaderBehavior.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */