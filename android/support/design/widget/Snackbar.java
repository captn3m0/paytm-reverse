package android.support.design.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.design.R.anim;
import android.support.design.R.dimen;
import android.support.design.R.id;
import android.support.design.R.layout;
import android.support.design.R.styleable;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.text.Layout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class Snackbar
{
  private static final Handler a = new Handler(Looper.getMainLooper(), new Handler.Callback()
  {
    public boolean handleMessage(Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.what)
      {
      default: 
        return false;
      case 0: 
        ((Snackbar)paramAnonymousMessage.obj).e();
        return true;
      }
      ((Snackbar)paramAnonymousMessage.obj).c(paramAnonymousMessage.arg1);
      return true;
    }
  });
  private final ViewGroup b;
  private final Context c;
  private final SnackbarLayout d;
  private int e;
  private Callback f;
  private final AccessibilityManager g;
  private final SnackbarManager.Callback h = new SnackbarManager.Callback()
  {
    public void a()
    {
      Snackbar.f().sendMessage(Snackbar.f().obtainMessage(0, Snackbar.this));
    }
    
    public void a(int paramAnonymousInt)
    {
      Snackbar.f().sendMessage(Snackbar.f().obtainMessage(1, paramAnonymousInt, 0, Snackbar.this));
    }
  };
  
  private Snackbar(ViewGroup paramViewGroup)
  {
    this.b = paramViewGroup;
    this.c = paramViewGroup.getContext();
    ThemeUtils.a(this.c);
    this.d = ((SnackbarLayout)LayoutInflater.from(this.c).inflate(R.layout.design_layout_snackbar, this.b, false));
    this.g = ((AccessibilityManager)this.c.getSystemService("accessibility"));
  }
  
  @NonNull
  public static Snackbar a(@NonNull View paramView, @NonNull CharSequence paramCharSequence, int paramInt)
  {
    paramView = new Snackbar(a(paramView));
    paramView.a(paramCharSequence);
    paramView.b(paramInt);
    return paramView;
  }
  
  private static ViewGroup a(View paramView)
  {
    Object localObject2 = null;
    View localView = paramView;
    if ((localView instanceof CoordinatorLayout)) {
      return (ViewGroup)localView;
    }
    Object localObject1 = localObject2;
    if ((localView instanceof FrameLayout))
    {
      if (localView.getId() == 16908290) {
        return (ViewGroup)localView;
      }
      localObject1 = (ViewGroup)localView;
    }
    paramView = localView;
    if (localView != null)
    {
      paramView = localView.getParent();
      if (!(paramView instanceof View)) {
        break label77;
      }
    }
    label77:
    for (paramView = (View)paramView;; paramView = null)
    {
      localObject2 = localObject1;
      localView = paramView;
      if (paramView != null) {
        break;
      }
      return (ViewGroup)localObject1;
    }
  }
  
  private void d(int paramInt)
  {
    SnackbarManager.a().a(this.h, paramInt);
  }
  
  private void e(final int paramInt)
  {
    if (Build.VERSION.SDK_INT >= 14)
    {
      ViewCompat.s(this.d).c(this.d.getHeight()).a(AnimationUtils.b).a(250L).a(new ViewPropertyAnimatorListenerAdapter()
      {
        public void a(View paramAnonymousView)
        {
          Snackbar.b(Snackbar.this).b(0, 180);
        }
        
        public void b(View paramAnonymousView)
        {
          Snackbar.b(Snackbar.this, paramInt);
        }
      }).b();
      return;
    }
    Animation localAnimation = android.view.animation.AnimationUtils.loadAnimation(this.d.getContext(), R.anim.design_snackbar_out);
    localAnimation.setInterpolator(AnimationUtils.b);
    localAnimation.setDuration(250L);
    localAnimation.setAnimationListener(new Animation.AnimationListener()
    {
      public void onAnimationEnd(Animation paramAnonymousAnimation)
      {
        Snackbar.b(Snackbar.this, paramInt);
      }
      
      public void onAnimationRepeat(Animation paramAnonymousAnimation) {}
      
      public void onAnimationStart(Animation paramAnonymousAnimation) {}
    });
    this.d.startAnimation(localAnimation);
  }
  
  private void f(int paramInt)
  {
    SnackbarManager.a().a(this.h);
    if (this.f != null) {
      this.f.a(this, paramInt);
    }
    ViewParent localViewParent = this.d.getParent();
    if ((localViewParent instanceof ViewGroup)) {
      ((ViewGroup)localViewParent).removeView(this.d);
    }
  }
  
  private void g()
  {
    if (Build.VERSION.SDK_INT >= 14)
    {
      ViewCompat.b(this.d, this.d.getHeight());
      ViewCompat.s(this.d).c(0.0F).a(AnimationUtils.b).a(250L).a(new ViewPropertyAnimatorListenerAdapter()
      {
        public void a(View paramAnonymousView)
        {
          Snackbar.b(Snackbar.this).a(70, 180);
        }
        
        public void b(View paramAnonymousView)
        {
          Snackbar.e(Snackbar.this);
        }
      }).b();
      return;
    }
    Animation localAnimation = android.view.animation.AnimationUtils.loadAnimation(this.d.getContext(), R.anim.design_snackbar_in);
    localAnimation.setInterpolator(AnimationUtils.b);
    localAnimation.setDuration(250L);
    localAnimation.setAnimationListener(new Animation.AnimationListener()
    {
      public void onAnimationEnd(Animation paramAnonymousAnimation)
      {
        Snackbar.e(Snackbar.this);
      }
      
      public void onAnimationRepeat(Animation paramAnonymousAnimation) {}
      
      public void onAnimationStart(Animation paramAnonymousAnimation) {}
    });
    this.d.startAnimation(localAnimation);
  }
  
  private void h()
  {
    SnackbarManager.a().b(this.h);
    if (this.f != null) {
      this.f.a(this);
    }
  }
  
  private boolean i()
  {
    return !this.g.isEnabled();
  }
  
  @NonNull
  public Snackbar a(@ColorInt int paramInt)
  {
    this.d.getActionView().setTextColor(paramInt);
    return this;
  }
  
  @NonNull
  public Snackbar a(@NonNull CharSequence paramCharSequence)
  {
    this.d.getMessageView().setText(paramCharSequence);
    return this;
  }
  
  @NonNull
  public Snackbar a(CharSequence paramCharSequence, final View.OnClickListener paramOnClickListener)
  {
    Button localButton = this.d.getActionView();
    if ((TextUtils.isEmpty(paramCharSequence)) || (paramOnClickListener == null))
    {
      localButton.setVisibility(8);
      localButton.setOnClickListener(null);
      return this;
    }
    localButton.setVisibility(0);
    localButton.setText(paramCharSequence);
    localButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        paramOnClickListener.onClick(paramAnonymousView);
        Snackbar.a(Snackbar.this, 1);
      }
    });
    return this;
  }
  
  @NonNull
  public View a()
  {
    return this.d;
  }
  
  @NonNull
  public Snackbar b(int paramInt)
  {
    this.e = paramInt;
    return this;
  }
  
  public void b()
  {
    SnackbarManager.a().a(this.e, this.h);
  }
  
  public void c()
  {
    d(3);
  }
  
  final void c(int paramInt)
  {
    if ((i()) && (this.d.getVisibility() == 0))
    {
      e(paramInt);
      return;
    }
    f(paramInt);
  }
  
  public boolean d()
  {
    return SnackbarManager.a().e(this.h);
  }
  
  final void e()
  {
    if (this.d.getParent() == null)
    {
      ViewGroup.LayoutParams localLayoutParams = this.d.getLayoutParams();
      if ((localLayoutParams instanceof CoordinatorLayout.LayoutParams))
      {
        Behavior localBehavior = new Behavior();
        localBehavior.a(0.1F);
        localBehavior.b(0.6F);
        localBehavior.a(0);
        localBehavior.a(new SwipeDismissBehavior.OnDismissListener()
        {
          public void a(int paramAnonymousInt)
          {
            switch (paramAnonymousInt)
            {
            default: 
              return;
            case 1: 
            case 2: 
              SnackbarManager.a().c(Snackbar.a(Snackbar.this));
              return;
            }
            SnackbarManager.a().d(Snackbar.a(Snackbar.this));
          }
          
          public void a(View paramAnonymousView)
          {
            paramAnonymousView.setVisibility(8);
            Snackbar.a(Snackbar.this, 0);
          }
        });
        ((CoordinatorLayout.LayoutParams)localLayoutParams).a(localBehavior);
      }
      this.b.addView(this.d);
    }
    this.d.setOnAttachStateChangeListener(new Snackbar.SnackbarLayout.OnAttachStateChangeListener()
    {
      public void a(View paramAnonymousView) {}
      
      public void b(View paramAnonymousView)
      {
        if (Snackbar.this.d()) {
          Snackbar.f().post(new Runnable()
          {
            public void run()
            {
              Snackbar.b(Snackbar.this, 3);
            }
          });
        }
      }
    });
    if (ViewCompat.F(this.d))
    {
      if (i())
      {
        g();
        return;
      }
      h();
      return;
    }
    this.d.setOnLayoutChangeListener(new Snackbar.SnackbarLayout.OnLayoutChangeListener()
    {
      public void a(View paramAnonymousView, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3, int paramAnonymousInt4)
      {
        Snackbar.b(Snackbar.this).setOnLayoutChangeListener(null);
        if (Snackbar.c(Snackbar.this))
        {
          Snackbar.d(Snackbar.this);
          return;
        }
        Snackbar.e(Snackbar.this);
      }
    });
  }
  
  final class Behavior
    extends SwipeDismissBehavior<Snackbar.SnackbarLayout>
  {
    Behavior() {}
    
    public boolean a(CoordinatorLayout paramCoordinatorLayout, Snackbar.SnackbarLayout paramSnackbarLayout, MotionEvent paramMotionEvent)
    {
      if (paramCoordinatorLayout.a(paramSnackbarLayout, (int)paramMotionEvent.getX(), (int)paramMotionEvent.getY())) {
        switch (paramMotionEvent.getActionMasked())
        {
        }
      }
      for (;;)
      {
        return super.a(paramCoordinatorLayout, paramSnackbarLayout, paramMotionEvent);
        SnackbarManager.a().c(Snackbar.a(Snackbar.this));
        continue;
        SnackbarManager.a().d(Snackbar.a(Snackbar.this));
      }
    }
    
    public boolean a(View paramView)
    {
      return paramView instanceof Snackbar.SnackbarLayout;
    }
  }
  
  public static abstract class Callback
  {
    public void a(Snackbar paramSnackbar) {}
    
    public void a(Snackbar paramSnackbar, int paramInt) {}
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface DismissEvent {}
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Duration {}
  
  public static class SnackbarLayout
    extends LinearLayout
  {
    private TextView a;
    private Button b;
    private int c;
    private int d;
    private OnLayoutChangeListener e;
    private OnAttachStateChangeListener f;
    
    public SnackbarLayout(Context paramContext)
    {
      this(paramContext, null);
    }
    
    public SnackbarLayout(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
      paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.SnackbarLayout);
      this.c = paramAttributeSet.getDimensionPixelSize(R.styleable.SnackbarLayout_android_maxWidth, -1);
      this.d = paramAttributeSet.getDimensionPixelSize(R.styleable.SnackbarLayout_maxActionInlineWidth, -1);
      if (paramAttributeSet.hasValue(R.styleable.SnackbarLayout_elevation)) {
        ViewCompat.f(this, paramAttributeSet.getDimensionPixelSize(R.styleable.SnackbarLayout_elevation, 0));
      }
      paramAttributeSet.recycle();
      setClickable(true);
      LayoutInflater.from(paramContext).inflate(R.layout.design_layout_snackbar_include, this);
      ViewCompat.d(this, 1);
      ViewCompat.c(this, 1);
    }
    
    private static void a(View paramView, int paramInt1, int paramInt2)
    {
      if (ViewCompat.A(paramView))
      {
        ViewCompat.b(paramView, ViewCompat.m(paramView), paramInt1, ViewCompat.n(paramView), paramInt2);
        return;
      }
      paramView.setPadding(paramView.getPaddingLeft(), paramInt1, paramView.getPaddingRight(), paramInt2);
    }
    
    private boolean a(int paramInt1, int paramInt2, int paramInt3)
    {
      boolean bool = false;
      if (paramInt1 != getOrientation())
      {
        setOrientation(paramInt1);
        bool = true;
      }
      if ((this.a.getPaddingTop() != paramInt2) || (this.a.getPaddingBottom() != paramInt3))
      {
        a(this.a, paramInt2, paramInt3);
        bool = true;
      }
      return bool;
    }
    
    void a(int paramInt1, int paramInt2)
    {
      ViewCompat.c(this.a, 0.0F);
      ViewCompat.s(this.a).a(1.0F).a(paramInt2).b(paramInt1).b();
      if (this.b.getVisibility() == 0)
      {
        ViewCompat.c(this.b, 0.0F);
        ViewCompat.s(this.b).a(1.0F).a(paramInt2).b(paramInt1).b();
      }
    }
    
    void b(int paramInt1, int paramInt2)
    {
      ViewCompat.c(this.a, 1.0F);
      ViewCompat.s(this.a).a(0.0F).a(paramInt2).b(paramInt1).b();
      if (this.b.getVisibility() == 0)
      {
        ViewCompat.c(this.b, 1.0F);
        ViewCompat.s(this.b).a(0.0F).a(paramInt2).b(paramInt1).b();
      }
    }
    
    Button getActionView()
    {
      return this.b;
    }
    
    TextView getMessageView()
    {
      return this.a;
    }
    
    protected void onAttachedToWindow()
    {
      super.onAttachedToWindow();
      if (this.f != null) {
        this.f.a(this);
      }
    }
    
    protected void onDetachedFromWindow()
    {
      super.onDetachedFromWindow();
      if (this.f != null) {
        this.f.b(this);
      }
    }
    
    protected void onFinishInflate()
    {
      super.onFinishInflate();
      this.a = ((TextView)findViewById(R.id.snackbar_text));
      this.b = ((Button)findViewById(R.id.snackbar_action));
    }
    
    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
      if (this.e != null) {
        this.e.a(this, paramInt1, paramInt2, paramInt3, paramInt4);
      }
    }
    
    protected void onMeasure(int paramInt1, int paramInt2)
    {
      super.onMeasure(paramInt1, paramInt2);
      int i = paramInt1;
      if (this.c > 0)
      {
        i = paramInt1;
        if (getMeasuredWidth() > this.c)
        {
          i = View.MeasureSpec.makeMeasureSpec(this.c, 1073741824);
          super.onMeasure(i, paramInt2);
        }
      }
      int j = getResources().getDimensionPixelSize(R.dimen.design_snackbar_padding_vertical_2lines);
      int k = getResources().getDimensionPixelSize(R.dimen.design_snackbar_padding_vertical);
      if (this.a.getLayout().getLineCount() > 1) {}
      int m;
      for (paramInt1 = 1;; paramInt1 = 0)
      {
        m = 0;
        if ((paramInt1 == 0) || (this.d <= 0) || (this.b.getMeasuredWidth() <= this.d)) {
          break;
        }
        paramInt1 = m;
        if (a(1, j, j - k)) {
          paramInt1 = 1;
        }
        if (paramInt1 != 0) {
          super.onMeasure(i, paramInt2);
        }
        return;
      }
      if (paramInt1 != 0) {}
      for (;;)
      {
        paramInt1 = m;
        if (!a(0, j, j)) {
          break;
        }
        paramInt1 = 1;
        break;
        j = k;
      }
    }
    
    void setOnAttachStateChangeListener(OnAttachStateChangeListener paramOnAttachStateChangeListener)
    {
      this.f = paramOnAttachStateChangeListener;
    }
    
    void setOnLayoutChangeListener(OnLayoutChangeListener paramOnLayoutChangeListener)
    {
      this.e = paramOnLayoutChangeListener;
    }
    
    static abstract interface OnAttachStateChangeListener
    {
      public abstract void a(View paramView);
      
      public abstract void b(View paramView);
    }
    
    static abstract interface OnLayoutChangeListener
    {
      public abstract void a(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/design/widget/Snackbar.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */