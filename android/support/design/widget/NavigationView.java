package android.support.design.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.design.R.attr;
import android.support.design.R.style;
import android.support.design.R.styleable;
import android.support.design.internal.NavigationMenu;
import android.support.design.internal.NavigationMenuPresenter;
import android.support.design.internal.ScrimInsetsFrameLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.support.v4.view.ViewCompat;
import android.support.v7.view.SupportMenuInflater;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuBuilder.Callback;
import android.support.v7.view.menu.MenuItemImpl;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;

public class NavigationView
  extends ScrimInsetsFrameLayout
{
  private static final int[] a = { 16842912 };
  private static final int[] b = { -16842910 };
  private final NavigationMenu c;
  private final NavigationMenuPresenter d = new NavigationMenuPresenter();
  private OnNavigationItemSelectedListener e;
  private int f;
  private MenuInflater g;
  
  public NavigationView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public NavigationView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public NavigationView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    ThemeUtils.a(paramContext);
    this.c = new NavigationMenu(paramContext);
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.NavigationView, paramInt, R.style.Widget_Design_NavigationView);
    setBackgroundDrawable(localTypedArray.getDrawable(R.styleable.NavigationView_android_background));
    if (localTypedArray.hasValue(R.styleable.NavigationView_elevation)) {
      ViewCompat.f(this, localTypedArray.getDimensionPixelSize(R.styleable.NavigationView_elevation, 0));
    }
    ViewCompat.a(this, localTypedArray.getBoolean(R.styleable.NavigationView_android_fitsSystemWindows, false));
    this.f = localTypedArray.getDimensionPixelSize(R.styleable.NavigationView_android_maxWidth, 0);
    if (localTypedArray.hasValue(R.styleable.NavigationView_itemIconTint)) {}
    for (ColorStateList localColorStateList = localTypedArray.getColorStateList(R.styleable.NavigationView_itemIconTint);; localColorStateList = c(16842808))
    {
      int i = 0;
      paramInt = 0;
      if (localTypedArray.hasValue(R.styleable.NavigationView_itemTextAppearance))
      {
        paramInt = localTypedArray.getResourceId(R.styleable.NavigationView_itemTextAppearance, 0);
        i = 1;
      }
      paramAttributeSet = null;
      if (localTypedArray.hasValue(R.styleable.NavigationView_itemTextColor)) {
        paramAttributeSet = localTypedArray.getColorStateList(R.styleable.NavigationView_itemTextColor);
      }
      Object localObject = paramAttributeSet;
      if (i == 0)
      {
        localObject = paramAttributeSet;
        if (paramAttributeSet == null) {
          localObject = c(16842806);
        }
      }
      paramAttributeSet = localTypedArray.getDrawable(R.styleable.NavigationView_itemBackground);
      this.c.a(new MenuBuilder.Callback()
      {
        public void a(MenuBuilder paramAnonymousMenuBuilder) {}
        
        public boolean a(MenuBuilder paramAnonymousMenuBuilder, MenuItem paramAnonymousMenuItem)
        {
          return (NavigationView.a(NavigationView.this) != null) && (NavigationView.a(NavigationView.this).a(paramAnonymousMenuItem));
        }
      });
      this.d.a(1);
      this.d.a(paramContext, this.c);
      this.d.a(localColorStateList);
      if (i != 0) {
        this.d.c(paramInt);
      }
      this.d.b((ColorStateList)localObject);
      this.d.a(paramAttributeSet);
      this.c.a(this.d);
      addView((View)this.d.a(this));
      if (localTypedArray.hasValue(R.styleable.NavigationView_menu)) {
        a(localTypedArray.getResourceId(R.styleable.NavigationView_menu, 0));
      }
      if (localTypedArray.hasValue(R.styleable.NavigationView_headerLayout)) {
        b(localTypedArray.getResourceId(R.styleable.NavigationView_headerLayout, 0));
      }
      localTypedArray.recycle();
      return;
    }
  }
  
  private ColorStateList c(int paramInt)
  {
    Object localObject = new TypedValue();
    if (!getContext().getTheme().resolveAttribute(paramInt, (TypedValue)localObject, true)) {}
    ColorStateList localColorStateList;
    do
    {
      return null;
      localColorStateList = getResources().getColorStateList(((TypedValue)localObject).resourceId);
    } while (!getContext().getTheme().resolveAttribute(R.attr.colorPrimary, (TypedValue)localObject, true));
    paramInt = ((TypedValue)localObject).data;
    int i = localColorStateList.getDefaultColor();
    localObject = b;
    int[] arrayOfInt1 = a;
    int[] arrayOfInt2 = EMPTY_STATE_SET;
    int j = localColorStateList.getColorForState(b, i);
    return new ColorStateList(new int[][] { localObject, arrayOfInt1, arrayOfInt2 }, new int[] { j, paramInt, i });
  }
  
  private MenuInflater getMenuInflater()
  {
    if (this.g == null) {
      this.g = new SupportMenuInflater(getContext());
    }
    return this.g;
  }
  
  public void a(int paramInt)
  {
    this.d.b(true);
    getMenuInflater().inflate(paramInt, this.c);
    this.d.b(false);
    this.d.a(false);
  }
  
  protected void a(Rect paramRect)
  {
    this.d.d(paramRect.top);
  }
  
  public View b(@LayoutRes int paramInt)
  {
    return this.d.b(paramInt);
  }
  
  public int getHeaderCount()
  {
    return this.d.d();
  }
  
  @Nullable
  public Drawable getItemBackground()
  {
    return this.d.g();
  }
  
  @Nullable
  public ColorStateList getItemIconTintList()
  {
    return this.d.e();
  }
  
  @Nullable
  public ColorStateList getItemTextColor()
  {
    return this.d.f();
  }
  
  public Menu getMenu()
  {
    return this.c;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i = paramInt1;
    switch (View.MeasureSpec.getMode(paramInt1))
    {
    default: 
      i = paramInt1;
    }
    for (;;)
    {
      super.onMeasure(i, paramInt2);
      return;
      i = View.MeasureSpec.makeMeasureSpec(Math.min(View.MeasureSpec.getSize(paramInt1), this.f), 1073741824);
      continue;
      i = View.MeasureSpec.makeMeasureSpec(this.f, 1073741824);
    }
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    if (!(paramParcelable instanceof SavedState))
    {
      super.onRestoreInstanceState(paramParcelable);
      return;
    }
    paramParcelable = (SavedState)paramParcelable;
    super.onRestoreInstanceState(paramParcelable.getSuperState());
    this.c.b(paramParcelable.a);
  }
  
  protected Parcelable onSaveInstanceState()
  {
    SavedState localSavedState = new SavedState(super.onSaveInstanceState());
    localSavedState.a = new Bundle();
    this.c.a(localSavedState.a);
    return localSavedState;
  }
  
  public void setCheckedItem(@IdRes int paramInt)
  {
    MenuItem localMenuItem = this.c.findItem(paramInt);
    if (localMenuItem != null) {
      this.d.a((MenuItemImpl)localMenuItem);
    }
  }
  
  public void setItemBackground(@Nullable Drawable paramDrawable)
  {
    this.d.a(paramDrawable);
  }
  
  public void setItemBackgroundResource(@DrawableRes int paramInt)
  {
    setItemBackground(ContextCompat.getDrawable(getContext(), paramInt));
  }
  
  public void setItemIconTintList(@Nullable ColorStateList paramColorStateList)
  {
    this.d.a(paramColorStateList);
  }
  
  public void setItemTextAppearance(@StyleRes int paramInt)
  {
    this.d.c(paramInt);
  }
  
  public void setItemTextColor(@Nullable ColorStateList paramColorStateList)
  {
    this.d.b(paramColorStateList);
  }
  
  public void setNavigationItemSelectedListener(OnNavigationItemSelectedListener paramOnNavigationItemSelectedListener)
  {
    this.e = paramOnNavigationItemSelectedListener;
  }
  
  public static abstract interface OnNavigationItemSelectedListener
  {
    public abstract boolean a(MenuItem paramMenuItem);
  }
  
  public static class SavedState
    extends View.BaseSavedState
  {
    public static final Parcelable.Creator<SavedState> CREATOR = ParcelableCompat.a(new ParcelableCompatCreatorCallbacks()
    {
      public NavigationView.SavedState a(Parcel paramAnonymousParcel, ClassLoader paramAnonymousClassLoader)
      {
        return new NavigationView.SavedState(paramAnonymousParcel, paramAnonymousClassLoader);
      }
      
      public NavigationView.SavedState[] a(int paramAnonymousInt)
      {
        return new NavigationView.SavedState[paramAnonymousInt];
      }
    });
    public Bundle a;
    
    public SavedState(Parcel paramParcel, ClassLoader paramClassLoader)
    {
      super();
      this.a = paramParcel.readBundle(paramClassLoader);
    }
    
    public SavedState(Parcelable paramParcelable)
    {
      super();
    }
    
    public void writeToParcel(@NonNull Parcel paramParcel, int paramInt)
    {
      super.writeToParcel(paramParcel, paramInt);
      paramParcel.writeBundle(this.a);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/design/widget/NavigationView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */