package android.support.design.widget;

import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import java.lang.ref.WeakReference;

class SnackbarManager
{
  private static SnackbarManager a;
  private final Object b = new Object();
  private final Handler c = new Handler(Looper.getMainLooper(), new Handler.Callback()
  {
    public boolean handleMessage(Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.what)
      {
      default: 
        return false;
      }
      SnackbarManager.a(SnackbarManager.this, (SnackbarManager.SnackbarRecord)paramAnonymousMessage.obj);
      return true;
    }
  });
  private SnackbarRecord d;
  private SnackbarRecord e;
  
  static SnackbarManager a()
  {
    if (a == null) {
      a = new SnackbarManager();
    }
    return a;
  }
  
  private void a(SnackbarRecord paramSnackbarRecord)
  {
    if (SnackbarRecord.b(paramSnackbarRecord) == -2) {
      return;
    }
    int i = 2750;
    if (SnackbarRecord.b(paramSnackbarRecord) > 0) {
      i = SnackbarRecord.b(paramSnackbarRecord);
    }
    for (;;)
    {
      this.c.removeCallbacksAndMessages(paramSnackbarRecord);
      this.c.sendMessageDelayed(Message.obtain(this.c, 0, paramSnackbarRecord), i);
      return;
      if (SnackbarRecord.b(paramSnackbarRecord) == -1) {
        i = 1500;
      }
    }
  }
  
  private boolean a(SnackbarRecord paramSnackbarRecord, int paramInt)
  {
    Callback localCallback = (Callback)SnackbarRecord.a(paramSnackbarRecord).get();
    if (localCallback != null)
    {
      this.c.removeCallbacksAndMessages(paramSnackbarRecord);
      localCallback.a(paramInt);
      return true;
    }
    return false;
  }
  
  private void b()
  {
    if (this.e != null)
    {
      this.d = this.e;
      this.e = null;
      Callback localCallback = (Callback)SnackbarRecord.a(this.d).get();
      if (localCallback != null) {
        localCallback.a();
      }
    }
    else
    {
      return;
    }
    this.d = null;
  }
  
  private void b(SnackbarRecord paramSnackbarRecord)
  {
    synchronized (this.b)
    {
      if ((this.d == paramSnackbarRecord) || (this.e == paramSnackbarRecord)) {
        a(paramSnackbarRecord, 2);
      }
      return;
    }
  }
  
  private boolean f(Callback paramCallback)
  {
    return (this.d != null) && (this.d.a(paramCallback));
  }
  
  private boolean g(Callback paramCallback)
  {
    return (this.e != null) && (this.e.a(paramCallback));
  }
  
  public void a(int paramInt, Callback paramCallback)
  {
    for (;;)
    {
      synchronized (this.b)
      {
        if (f(paramCallback))
        {
          SnackbarRecord.a(this.d, paramInt);
          this.c.removeCallbacksAndMessages(this.d);
          a(this.d);
          return;
        }
        if (g(paramCallback))
        {
          SnackbarRecord.a(this.e, paramInt);
          if ((this.d == null) || (!a(this.d, 4))) {
            break;
          }
          return;
        }
      }
      this.e = new SnackbarRecord(paramInt, paramCallback);
    }
    this.d = null;
    b();
  }
  
  public void a(Callback paramCallback)
  {
    synchronized (this.b)
    {
      if (f(paramCallback))
      {
        this.d = null;
        if (this.e != null) {
          b();
        }
      }
      return;
    }
  }
  
  public void a(Callback paramCallback, int paramInt)
  {
    synchronized (this.b)
    {
      if (f(paramCallback)) {
        a(this.d, paramInt);
      }
      while (!g(paramCallback)) {
        return;
      }
      a(this.e, paramInt);
    }
  }
  
  public void b(Callback paramCallback)
  {
    synchronized (this.b)
    {
      if (f(paramCallback)) {
        a(this.d);
      }
      return;
    }
  }
  
  public void c(Callback paramCallback)
  {
    synchronized (this.b)
    {
      if (f(paramCallback)) {
        this.c.removeCallbacksAndMessages(this.d);
      }
      return;
    }
  }
  
  public void d(Callback paramCallback)
  {
    synchronized (this.b)
    {
      if (f(paramCallback)) {
        a(this.d);
      }
      return;
    }
  }
  
  public boolean e(Callback paramCallback)
  {
    for (;;)
    {
      synchronized (this.b)
      {
        if (!f(paramCallback))
        {
          if (!g(paramCallback)) {
            break label40;
          }
          break label35;
          return bool;
        }
      }
      label35:
      boolean bool = true;
      continue;
      label40:
      bool = false;
    }
  }
  
  static abstract interface Callback
  {
    public abstract void a();
    
    public abstract void a(int paramInt);
  }
  
  private static class SnackbarRecord
  {
    private final WeakReference<SnackbarManager.Callback> a;
    private int b;
    
    SnackbarRecord(int paramInt, SnackbarManager.Callback paramCallback)
    {
      this.a = new WeakReference(paramCallback);
      this.b = paramInt;
    }
    
    boolean a(SnackbarManager.Callback paramCallback)
    {
      return (paramCallback != null) && (this.a.get() == paramCallback);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/design/widget/SnackbarManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */