package android.support.v4.hardware.fingerprint;

import android.os.Build.VERSION;
import java.security.Signature;
import javax.crypto.Cipher;
import javax.crypto.Mac;

public final class FingerprintManagerCompat
{
  static final FingerprintManagerCompatImpl a = new LegacyFingerprintManagerCompatImpl();
  
  static
  {
    if (Build.VERSION.SDK_INT >= 23)
    {
      a = new Api23FingerprintManagerCompatImpl();
      return;
    }
  }
  
  private static class Api23FingerprintManagerCompatImpl
    implements FingerprintManagerCompat.FingerprintManagerCompatImpl
  {
    private static FingerprintManagerCompat.CryptoObject b(FingerprintManagerCompatApi23.CryptoObject paramCryptoObject)
    {
      if (paramCryptoObject == null) {}
      do
      {
        return null;
        if (paramCryptoObject.b() != null) {
          return new FingerprintManagerCompat.CryptoObject(paramCryptoObject.b());
        }
        if (paramCryptoObject.a() != null) {
          return new FingerprintManagerCompat.CryptoObject(paramCryptoObject.a());
        }
      } while (paramCryptoObject.c() == null);
      return new FingerprintManagerCompat.CryptoObject(paramCryptoObject.c());
    }
  }
  
  public static abstract class AuthenticationCallback
  {
    public void a() {}
    
    public void a(int paramInt, CharSequence paramCharSequence) {}
    
    public void a(FingerprintManagerCompat.AuthenticationResult paramAuthenticationResult) {}
    
    public void b(int paramInt, CharSequence paramCharSequence) {}
  }
  
  public static final class AuthenticationResult
  {
    private FingerprintManagerCompat.CryptoObject a;
    
    public AuthenticationResult(FingerprintManagerCompat.CryptoObject paramCryptoObject)
    {
      this.a = paramCryptoObject;
    }
  }
  
  public static class CryptoObject
  {
    private final Signature a;
    private final Cipher b;
    private final Mac c;
    
    public CryptoObject(Signature paramSignature)
    {
      this.a = paramSignature;
      this.b = null;
      this.c = null;
    }
    
    public CryptoObject(Cipher paramCipher)
    {
      this.b = paramCipher;
      this.a = null;
      this.c = null;
    }
    
    public CryptoObject(Mac paramMac)
    {
      this.c = paramMac;
      this.b = null;
      this.a = null;
    }
  }
  
  private static abstract interface FingerprintManagerCompatImpl {}
  
  private static class LegacyFingerprintManagerCompatImpl
    implements FingerprintManagerCompat.FingerprintManagerCompatImpl
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/hardware/fingerprint/FingerprintManagerCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */