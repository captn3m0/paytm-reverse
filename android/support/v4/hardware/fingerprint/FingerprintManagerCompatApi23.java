package android.support.v4.hardware.fingerprint;

import android.hardware.fingerprint.FingerprintManager.AuthenticationCallback;
import android.hardware.fingerprint.FingerprintManager.AuthenticationResult;
import android.hardware.fingerprint.FingerprintManager.CryptoObject;
import java.security.Signature;
import javax.crypto.Cipher;
import javax.crypto.Mac;

public final class FingerprintManagerCompatApi23
{
  private static CryptoObject b(FingerprintManager.CryptoObject paramCryptoObject)
  {
    if (paramCryptoObject == null) {}
    do
    {
      return null;
      if (paramCryptoObject.getCipher() != null) {
        return new CryptoObject(paramCryptoObject.getCipher());
      }
      if (paramCryptoObject.getSignature() != null) {
        return new CryptoObject(paramCryptoObject.getSignature());
      }
    } while (paramCryptoObject.getMac() == null);
    return new CryptoObject(paramCryptoObject.getMac());
  }
  
  public static abstract class AuthenticationCallback
  {
    public void a() {}
    
    public void a(int paramInt, CharSequence paramCharSequence) {}
    
    public void a(FingerprintManagerCompatApi23.AuthenticationResultInternal paramAuthenticationResultInternal) {}
    
    public void b(int paramInt, CharSequence paramCharSequence) {}
  }
  
  public static final class AuthenticationResultInternal
  {
    private FingerprintManagerCompatApi23.CryptoObject a;
    
    public AuthenticationResultInternal(FingerprintManagerCompatApi23.CryptoObject paramCryptoObject)
    {
      this.a = paramCryptoObject;
    }
    
    public FingerprintManagerCompatApi23.CryptoObject a()
    {
      return this.a;
    }
  }
  
  public static class CryptoObject
  {
    private final Signature a;
    private final Cipher b;
    private final Mac c;
    
    public CryptoObject(Signature paramSignature)
    {
      this.a = paramSignature;
      this.b = null;
      this.c = null;
    }
    
    public CryptoObject(Cipher paramCipher)
    {
      this.b = paramCipher;
      this.a = null;
      this.c = null;
    }
    
    public CryptoObject(Mac paramMac)
    {
      this.c = paramMac;
      this.b = null;
      this.a = null;
    }
    
    public Signature a()
    {
      return this.a;
    }
    
    public Cipher b()
    {
      return this.b;
    }
    
    public Mac c()
    {
      return this.c;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/hardware/fingerprint/FingerprintManagerCompatApi23.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */