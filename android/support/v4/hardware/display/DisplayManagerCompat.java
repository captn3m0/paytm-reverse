package android.support.v4.hardware.display;

import android.content.Context;
import android.os.Build.VERSION;
import android.view.WindowManager;
import java.util.WeakHashMap;

public abstract class DisplayManagerCompat
{
  private static final WeakHashMap<Context, DisplayManagerCompat> a = new WeakHashMap();
  
  public static DisplayManagerCompat a(Context paramContext)
  {
    synchronized (a)
    {
      DisplayManagerCompat localDisplayManagerCompat = (DisplayManagerCompat)a.get(paramContext);
      Object localObject = localDisplayManagerCompat;
      if (localDisplayManagerCompat == null)
      {
        if (Build.VERSION.SDK_INT >= 17)
        {
          localObject = new JellybeanMr1Impl(paramContext);
          a.put(paramContext, localObject);
        }
      }
      else {
        return (DisplayManagerCompat)localObject;
      }
      localObject = new LegacyImpl(paramContext);
    }
  }
  
  private static class JellybeanMr1Impl
    extends DisplayManagerCompat
  {
    private final Object a;
    
    public JellybeanMr1Impl(Context paramContext)
    {
      this.a = DisplayManagerJellybeanMr1.a(paramContext);
    }
  }
  
  private static class LegacyImpl
    extends DisplayManagerCompat
  {
    private final WindowManager a;
    
    public LegacyImpl(Context paramContext)
    {
      this.a = ((WindowManager)paramContext.getSystemService("window"));
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/hardware/display/DisplayManagerCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */