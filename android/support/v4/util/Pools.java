package android.support.v4.util;

public final class Pools
{
  public static abstract interface Pool<T>
  {
    public abstract T a();
    
    public abstract boolean a(T paramT);
  }
  
  public static class SimplePool<T>
    implements Pools.Pool<T>
  {
    private final Object[] a;
    private int b;
    
    public SimplePool(int paramInt)
    {
      if (paramInt <= 0) {
        throw new IllegalArgumentException("The max pool size must be > 0");
      }
      this.a = new Object[paramInt];
    }
    
    private boolean b(T paramT)
    {
      int i = 0;
      while (i < this.b)
      {
        if (this.a[i] == paramT) {
          return true;
        }
        i += 1;
      }
      return false;
    }
    
    public T a()
    {
      if (this.b > 0)
      {
        int i = this.b - 1;
        Object localObject = this.a[i];
        this.a[i] = null;
        this.b -= 1;
        return (T)localObject;
      }
      return null;
    }
    
    public boolean a(T paramT)
    {
      if (b(paramT)) {
        throw new IllegalStateException("Already in the pool!");
      }
      if (this.b < this.a.length)
      {
        this.a[this.b] = paramT;
        this.b += 1;
        return true;
      }
      return false;
    }
  }
  
  public static class SynchronizedPool<T>
    extends Pools.SimplePool<T>
  {
    private final Object a = new Object();
    
    public SynchronizedPool(int paramInt)
    {
      super();
    }
    
    public T a()
    {
      synchronized (this.a)
      {
        Object localObject2 = super.a();
        return (T)localObject2;
      }
    }
    
    public boolean a(T paramT)
    {
      synchronized (this.a)
      {
        boolean bool = super.a(paramT);
        return bool;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/util/Pools.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */