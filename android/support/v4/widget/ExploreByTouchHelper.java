package android.support.v4.widget;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.view.AccessibilityDelegateCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewParentCompat;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.support.v4.view.accessibility.AccessibilityManagerCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v4.view.accessibility.AccessibilityNodeProviderCompat;
import android.support.v4.view.accessibility.AccessibilityRecordCompat;
import android.view.View;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public abstract class ExploreByTouchHelper
  extends AccessibilityDelegateCompat
{
  private static final String a = View.class.getName();
  private static final Rect c = new Rect(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
  private final Rect d;
  private final Rect e;
  private final Rect f;
  private final int[] g;
  private final AccessibilityManager h;
  private final View i;
  private ExploreByTouchNodeProvider j;
  private int k;
  
  private AccessibilityEvent a(int paramInt)
  {
    AccessibilityEvent localAccessibilityEvent = AccessibilityEvent.obtain(paramInt);
    ViewCompat.a(this.i, localAccessibilityEvent);
    return localAccessibilityEvent;
  }
  
  private boolean a(int paramInt, Bundle paramBundle)
  {
    return ViewCompat.a(this.i, paramInt, paramBundle);
  }
  
  private boolean a(Rect paramRect)
  {
    if ((paramRect == null) || (paramRect.isEmpty())) {}
    Object localObject;
    label67:
    do
    {
      do
      {
        return false;
      } while (this.i.getWindowVisibility() != 0);
      for (localObject = this.i.getParent();; localObject = ((View)localObject).getParent())
      {
        if (!(localObject instanceof View)) {
          break label67;
        }
        localObject = (View)localObject;
        if ((ViewCompat.f((View)localObject) <= 0.0F) || (((View)localObject).getVisibility() != 0)) {
          break;
        }
      }
    } while ((localObject == null) || (!this.i.getLocalVisibleRect(this.f)));
    return paramRect.intersect(this.f);
  }
  
  private AccessibilityNodeInfoCompat b()
  {
    AccessibilityNodeInfoCompat localAccessibilityNodeInfoCompat = AccessibilityNodeInfoCompat.a(this.i);
    ViewCompat.a(this.i, localAccessibilityNodeInfoCompat);
    a(localAccessibilityNodeInfoCompat);
    Object localObject = new LinkedList();
    a((List)localObject);
    localObject = ((LinkedList)localObject).iterator();
    while (((Iterator)localObject).hasNext())
    {
      Integer localInteger = (Integer)((Iterator)localObject).next();
      localAccessibilityNodeInfoCompat.b(this.i, localInteger.intValue());
    }
    return localAccessibilityNodeInfoCompat;
  }
  
  private AccessibilityNodeInfoCompat b(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return c(paramInt);
    }
    return b();
  }
  
  private AccessibilityEvent b(int paramInt1, int paramInt2)
  {
    switch (paramInt1)
    {
    default: 
      return c(paramInt1, paramInt2);
    }
    return a(paramInt2);
  }
  
  private boolean b(int paramInt1, int paramInt2, Bundle paramBundle)
  {
    switch (paramInt1)
    {
    default: 
      return c(paramInt1, paramInt2, paramBundle);
    }
    return a(paramInt2, paramBundle);
  }
  
  private AccessibilityNodeInfoCompat c(int paramInt)
  {
    AccessibilityNodeInfoCompat localAccessibilityNodeInfoCompat = AccessibilityNodeInfoCompat.b();
    localAccessibilityNodeInfoCompat.h(true);
    localAccessibilityNodeInfoCompat.b(a);
    localAccessibilityNodeInfoCompat.b(c);
    a(paramInt, localAccessibilityNodeInfoCompat);
    if ((localAccessibilityNodeInfoCompat.s() == null) && (localAccessibilityNodeInfoCompat.t() == null)) {
      throw new RuntimeException("Callbacks must add text or a content description in populateNodeForVirtualViewId()");
    }
    localAccessibilityNodeInfoCompat.a(this.e);
    if (this.e.equals(c)) {
      throw new RuntimeException("Callbacks must set parent bounds in populateNodeForVirtualViewId()");
    }
    int m = localAccessibilityNodeInfoCompat.c();
    if ((m & 0x40) != 0) {
      throw new RuntimeException("Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
    }
    if ((m & 0x80) != 0) {
      throw new RuntimeException("Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
    }
    localAccessibilityNodeInfoCompat.a(this.i.getContext().getPackageName());
    localAccessibilityNodeInfoCompat.a(this.i, paramInt);
    localAccessibilityNodeInfoCompat.d(this.i);
    if (this.k == paramInt)
    {
      localAccessibilityNodeInfoCompat.d(true);
      localAccessibilityNodeInfoCompat.a(128);
    }
    for (;;)
    {
      if (a(this.e))
      {
        localAccessibilityNodeInfoCompat.c(true);
        localAccessibilityNodeInfoCompat.b(this.e);
      }
      this.i.getLocationOnScreen(this.g);
      paramInt = this.g[0];
      m = this.g[1];
      this.d.set(this.e);
      this.d.offset(paramInt, m);
      localAccessibilityNodeInfoCompat.d(this.d);
      return localAccessibilityNodeInfoCompat;
      localAccessibilityNodeInfoCompat.d(false);
      localAccessibilityNodeInfoCompat.a(64);
    }
  }
  
  private AccessibilityEvent c(int paramInt1, int paramInt2)
  {
    AccessibilityEvent localAccessibilityEvent = AccessibilityEvent.obtain(paramInt2);
    localAccessibilityEvent.setEnabled(true);
    localAccessibilityEvent.setClassName(a);
    a(paramInt1, localAccessibilityEvent);
    if ((localAccessibilityEvent.getText().isEmpty()) && (localAccessibilityEvent.getContentDescription() == null)) {
      throw new RuntimeException("Callbacks must add text or a content description in populateEventForVirtualViewId()");
    }
    localAccessibilityEvent.setPackageName(this.i.getContext().getPackageName());
    AccessibilityEventCompat.a(localAccessibilityEvent).a(this.i, paramInt1);
    return localAccessibilityEvent;
  }
  
  private boolean c(int paramInt1, int paramInt2, Bundle paramBundle)
  {
    switch (paramInt2)
    {
    default: 
      return a(paramInt1, paramInt2, paramBundle);
    }
    return d(paramInt1, paramInt2, paramBundle);
  }
  
  private boolean d(int paramInt)
  {
    return this.k == paramInt;
  }
  
  private boolean d(int paramInt1, int paramInt2, Bundle paramBundle)
  {
    switch (paramInt2)
    {
    default: 
      return false;
    case 64: 
      return e(paramInt1);
    }
    return f(paramInt1);
  }
  
  private boolean e(int paramInt)
  {
    if ((!this.h.isEnabled()) || (!AccessibilityManagerCompat.a(this.h))) {}
    while (d(paramInt)) {
      return false;
    }
    if (this.k != Integer.MIN_VALUE) {
      a(this.k, 65536);
    }
    this.k = paramInt;
    this.i.invalidate();
    a(paramInt, 32768);
    return true;
  }
  
  private boolean f(int paramInt)
  {
    if (d(paramInt))
    {
      this.k = Integer.MIN_VALUE;
      this.i.invalidate();
      a(paramInt, 65536);
      return true;
    }
    return false;
  }
  
  public AccessibilityNodeProviderCompat a(View paramView)
  {
    if (this.j == null) {
      this.j = new ExploreByTouchNodeProvider(null);
    }
    return this.j;
  }
  
  protected abstract void a(int paramInt, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat);
  
  protected abstract void a(int paramInt, AccessibilityEvent paramAccessibilityEvent);
  
  public void a(AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat) {}
  
  protected abstract void a(List<Integer> paramList);
  
  public boolean a(int paramInt1, int paramInt2)
  {
    if ((paramInt1 == Integer.MIN_VALUE) || (!this.h.isEnabled())) {}
    ViewParent localViewParent;
    do
    {
      return false;
      localViewParent = this.i.getParent();
    } while (localViewParent == null);
    AccessibilityEvent localAccessibilityEvent = b(paramInt1, paramInt2);
    return ViewParentCompat.a(localViewParent, this.i, localAccessibilityEvent);
  }
  
  protected abstract boolean a(int paramInt1, int paramInt2, Bundle paramBundle);
  
  private class ExploreByTouchNodeProvider
    extends AccessibilityNodeProviderCompat
  {
    private ExploreByTouchNodeProvider() {}
    
    public AccessibilityNodeInfoCompat a(int paramInt)
    {
      return ExploreByTouchHelper.a(ExploreByTouchHelper.this, paramInt);
    }
    
    public boolean a(int paramInt1, int paramInt2, Bundle paramBundle)
    {
      return ExploreByTouchHelper.a(ExploreByTouchHelper.this, paramInt1, paramInt2, paramBundle);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/widget/ExploreByTouchHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */