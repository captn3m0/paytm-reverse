package android.support.v4.widget;

import android.widget.SearchView.OnCloseListener;
import android.widget.SearchView.OnQueryTextListener;

class SearchViewCompatHoneycomb
{
  public static Object a(OnCloseListenerCompatBridge paramOnCloseListenerCompatBridge)
  {
    new SearchView.OnCloseListener()
    {
      public boolean onClose()
      {
        return this.a.a();
      }
    };
  }
  
  public static Object a(OnQueryTextListenerCompatBridge paramOnQueryTextListenerCompatBridge)
  {
    new SearchView.OnQueryTextListener()
    {
      public boolean onQueryTextChange(String paramAnonymousString)
      {
        return this.a.b(paramAnonymousString);
      }
      
      public boolean onQueryTextSubmit(String paramAnonymousString)
      {
        return this.a.a(paramAnonymousString);
      }
    };
  }
  
  static abstract interface OnCloseListenerCompatBridge
  {
    public abstract boolean a();
  }
  
  static abstract interface OnQueryTextListenerCompatBridge
  {
    public abstract boolean a(String paramString);
    
    public abstract boolean b(String paramString);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/widget/SearchViewCompatHoneycomb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */