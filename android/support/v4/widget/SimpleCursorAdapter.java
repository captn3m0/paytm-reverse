package android.support.v4.widget;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class SimpleCursorAdapter
  extends ResourceCursorAdapter
{
  protected int[] j;
  protected int[] k;
  String[] l;
  private int m;
  private CursorToStringConverter n;
  private ViewBinder o;
  
  private void a(String[] paramArrayOfString)
  {
    if (this.c != null)
    {
      int i1 = paramArrayOfString.length;
      if ((this.j == null) || (this.j.length != i1)) {
        this.j = new int[i1];
      }
      int i = 0;
      while (i < i1)
      {
        this.j[i] = this.c.getColumnIndexOrThrow(paramArrayOfString[i]);
        i += 1;
      }
    }
    this.j = null;
  }
  
  public void a(View paramView, Context paramContext, Cursor paramCursor)
  {
    ViewBinder localViewBinder = this.o;
    int i1 = this.k.length;
    int[] arrayOfInt1 = this.j;
    int[] arrayOfInt2 = this.k;
    int i = 0;
    if (i < i1)
    {
      View localView = paramView.findViewById(arrayOfInt2[i]);
      if (localView != null)
      {
        boolean bool = false;
        if (localViewBinder != null) {
          bool = localViewBinder.a(localView, paramCursor, arrayOfInt1[i]);
        }
        if (!bool)
        {
          String str = paramCursor.getString(arrayOfInt1[i]);
          paramContext = str;
          if (str == null) {
            paramContext = "";
          }
          if (!(localView instanceof TextView)) {
            break label132;
          }
          a((TextView)localView, paramContext);
        }
      }
      for (;;)
      {
        i += 1;
        break;
        label132:
        if (!(localView instanceof ImageView)) {
          break label153;
        }
        a((ImageView)localView, paramContext);
      }
      label153:
      throw new IllegalStateException(localView.getClass().getName() + " is not a " + " view that can be bounds by this SimpleCursorAdapter");
    }
  }
  
  public void a(ImageView paramImageView, String paramString)
  {
    try
    {
      paramImageView.setImageResource(Integer.parseInt(paramString));
      return;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      paramImageView.setImageURI(Uri.parse(paramString));
    }
  }
  
  public void a(TextView paramTextView, String paramString)
  {
    paramTextView.setText(paramString);
  }
  
  public Cursor b(Cursor paramCursor)
  {
    paramCursor = super.b(paramCursor);
    a(this.l);
    return paramCursor;
  }
  
  public CharSequence c(Cursor paramCursor)
  {
    if (this.n != null) {
      return this.n.a(paramCursor);
    }
    if (this.m > -1) {
      return paramCursor.getString(this.m);
    }
    return super.c(paramCursor);
  }
  
  public static abstract interface CursorToStringConverter
  {
    public abstract CharSequence a(Cursor paramCursor);
  }
  
  public static abstract interface ViewBinder
  {
    public abstract boolean a(View paramView, Cursor paramCursor, int paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/widget/SimpleCursorAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */