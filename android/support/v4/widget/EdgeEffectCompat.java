package android.support.v4.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Build.VERSION;

public final class EdgeEffectCompat
{
  private static final EdgeEffectImpl b = new BaseEdgeEffectImpl();
  private Object a;
  
  static
  {
    if (Build.VERSION.SDK_INT >= 21)
    {
      b = new EdgeEffectLollipopImpl();
      return;
    }
    if (Build.VERSION.SDK_INT >= 14)
    {
      b = new EdgeEffectIcsImpl();
      return;
    }
  }
  
  public EdgeEffectCompat(Context paramContext)
  {
    this.a = b.a(paramContext);
  }
  
  public void a(int paramInt1, int paramInt2)
  {
    b.a(this.a, paramInt1, paramInt2);
  }
  
  public boolean a()
  {
    return b.a(this.a);
  }
  
  public boolean a(float paramFloat)
  {
    return b.a(this.a, paramFloat);
  }
  
  public boolean a(float paramFloat1, float paramFloat2)
  {
    return b.a(this.a, paramFloat1, paramFloat2);
  }
  
  public boolean a(int paramInt)
  {
    return b.a(this.a, paramInt);
  }
  
  public boolean a(Canvas paramCanvas)
  {
    return b.a(this.a, paramCanvas);
  }
  
  public void b()
  {
    b.b(this.a);
  }
  
  public boolean c()
  {
    return b.c(this.a);
  }
  
  static class BaseEdgeEffectImpl
    implements EdgeEffectCompat.EdgeEffectImpl
  {
    public Object a(Context paramContext)
    {
      return null;
    }
    
    public void a(Object paramObject, int paramInt1, int paramInt2) {}
    
    public boolean a(Object paramObject)
    {
      return true;
    }
    
    public boolean a(Object paramObject, float paramFloat)
    {
      return false;
    }
    
    public boolean a(Object paramObject, float paramFloat1, float paramFloat2)
    {
      return false;
    }
    
    public boolean a(Object paramObject, int paramInt)
    {
      return false;
    }
    
    public boolean a(Object paramObject, Canvas paramCanvas)
    {
      return false;
    }
    
    public void b(Object paramObject) {}
    
    public boolean c(Object paramObject)
    {
      return false;
    }
  }
  
  static class EdgeEffectIcsImpl
    implements EdgeEffectCompat.EdgeEffectImpl
  {
    public Object a(Context paramContext)
    {
      return EdgeEffectCompatIcs.a(paramContext);
    }
    
    public void a(Object paramObject, int paramInt1, int paramInt2)
    {
      EdgeEffectCompatIcs.a(paramObject, paramInt1, paramInt2);
    }
    
    public boolean a(Object paramObject)
    {
      return EdgeEffectCompatIcs.a(paramObject);
    }
    
    public boolean a(Object paramObject, float paramFloat)
    {
      return EdgeEffectCompatIcs.a(paramObject, paramFloat);
    }
    
    public boolean a(Object paramObject, float paramFloat1, float paramFloat2)
    {
      return EdgeEffectCompatIcs.a(paramObject, paramFloat1);
    }
    
    public boolean a(Object paramObject, int paramInt)
    {
      return EdgeEffectCompatIcs.a(paramObject, paramInt);
    }
    
    public boolean a(Object paramObject, Canvas paramCanvas)
    {
      return EdgeEffectCompatIcs.a(paramObject, paramCanvas);
    }
    
    public void b(Object paramObject)
    {
      EdgeEffectCompatIcs.b(paramObject);
    }
    
    public boolean c(Object paramObject)
    {
      return EdgeEffectCompatIcs.c(paramObject);
    }
  }
  
  static abstract interface EdgeEffectImpl
  {
    public abstract Object a(Context paramContext);
    
    public abstract void a(Object paramObject, int paramInt1, int paramInt2);
    
    public abstract boolean a(Object paramObject);
    
    public abstract boolean a(Object paramObject, float paramFloat);
    
    public abstract boolean a(Object paramObject, float paramFloat1, float paramFloat2);
    
    public abstract boolean a(Object paramObject, int paramInt);
    
    public abstract boolean a(Object paramObject, Canvas paramCanvas);
    
    public abstract void b(Object paramObject);
    
    public abstract boolean c(Object paramObject);
  }
  
  static class EdgeEffectLollipopImpl
    extends EdgeEffectCompat.EdgeEffectIcsImpl
  {
    public boolean a(Object paramObject, float paramFloat1, float paramFloat2)
    {
      return EdgeEffectCompatLollipop.a(paramObject, paramFloat1, paramFloat2);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/widget/EdgeEffectCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */