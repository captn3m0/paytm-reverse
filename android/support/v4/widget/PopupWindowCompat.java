package android.support.v4.widget;

import android.os.Build.VERSION;
import android.view.View;
import android.widget.PopupWindow;

public final class PopupWindowCompat
{
  static final PopupWindowImpl a = new BasePopupWindowImpl();
  
  static
  {
    int i = Build.VERSION.SDK_INT;
    if (i >= 23)
    {
      a = new Api23PopupWindowImpl();
      return;
    }
    if (i >= 21)
    {
      a = new Api21PopupWindowImpl();
      return;
    }
    if (i >= 19)
    {
      a = new KitKatPopupWindowImpl();
      return;
    }
    if (i >= 9)
    {
      a = new GingerbreadPopupWindowImpl();
      return;
    }
  }
  
  public static void a(PopupWindow paramPopupWindow, int paramInt)
  {
    a.a(paramPopupWindow, paramInt);
  }
  
  public static void a(PopupWindow paramPopupWindow, View paramView, int paramInt1, int paramInt2, int paramInt3)
  {
    a.a(paramPopupWindow, paramView, paramInt1, paramInt2, paramInt3);
  }
  
  public static void a(PopupWindow paramPopupWindow, boolean paramBoolean)
  {
    a.a(paramPopupWindow, paramBoolean);
  }
  
  static class Api21PopupWindowImpl
    extends PopupWindowCompat.KitKatPopupWindowImpl
  {
    public void a(PopupWindow paramPopupWindow, boolean paramBoolean)
    {
      PopupWindowCompatApi21.a(paramPopupWindow, paramBoolean);
    }
  }
  
  static class Api23PopupWindowImpl
    extends PopupWindowCompat.Api21PopupWindowImpl
  {
    public void a(PopupWindow paramPopupWindow, int paramInt)
    {
      PopupWindowCompatApi23.a(paramPopupWindow, paramInt);
    }
    
    public void a(PopupWindow paramPopupWindow, boolean paramBoolean)
    {
      PopupWindowCompatApi23.a(paramPopupWindow, paramBoolean);
    }
  }
  
  static class BasePopupWindowImpl
    implements PopupWindowCompat.PopupWindowImpl
  {
    public void a(PopupWindow paramPopupWindow, int paramInt) {}
    
    public void a(PopupWindow paramPopupWindow, View paramView, int paramInt1, int paramInt2, int paramInt3)
    {
      paramPopupWindow.showAsDropDown(paramView, paramInt1, paramInt2);
    }
    
    public void a(PopupWindow paramPopupWindow, boolean paramBoolean) {}
  }
  
  static class GingerbreadPopupWindowImpl
    extends PopupWindowCompat.BasePopupWindowImpl
  {
    public void a(PopupWindow paramPopupWindow, int paramInt)
    {
      PopupWindowCompatGingerbread.a(paramPopupWindow, paramInt);
    }
  }
  
  static class KitKatPopupWindowImpl
    extends PopupWindowCompat.GingerbreadPopupWindowImpl
  {
    public void a(PopupWindow paramPopupWindow, View paramView, int paramInt1, int paramInt2, int paramInt3)
    {
      PopupWindowCompatKitKat.a(paramPopupWindow, paramView, paramInt1, paramInt2, paramInt3);
    }
  }
  
  static abstract interface PopupWindowImpl
  {
    public abstract void a(PopupWindow paramPopupWindow, int paramInt);
    
    public abstract void a(PopupWindow paramPopupWindow, View paramView, int paramInt1, int paramInt2, int paramInt3);
    
    public abstract void a(PopupWindow paramPopupWindow, boolean paramBoolean);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/widget/PopupWindowCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */