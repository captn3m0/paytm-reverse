package android.support.v4.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.FillType;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.Callback;
import android.support.annotation.NonNull;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;

class MaterialProgressDrawable
  extends Drawable
  implements Animatable
{
  private static final Interpolator b = new LinearInterpolator();
  private static final Interpolator c = new FastOutSlowInInterpolator();
  boolean a;
  private final int[] d = { -16777216 };
  private final ArrayList<Animation> e = new ArrayList();
  private final Ring f;
  private float g;
  private Resources h;
  private View i;
  private Animation j;
  private float k;
  private double l;
  private double m;
  private final Drawable.Callback n = new Drawable.Callback()
  {
    public void invalidateDrawable(Drawable paramAnonymousDrawable)
    {
      MaterialProgressDrawable.this.invalidateSelf();
    }
    
    public void scheduleDrawable(Drawable paramAnonymousDrawable, Runnable paramAnonymousRunnable, long paramAnonymousLong)
    {
      MaterialProgressDrawable.this.scheduleSelf(paramAnonymousRunnable, paramAnonymousLong);
    }
    
    public void unscheduleDrawable(Drawable paramAnonymousDrawable, Runnable paramAnonymousRunnable)
    {
      MaterialProgressDrawable.this.unscheduleSelf(paramAnonymousRunnable);
    }
  };
  
  public MaterialProgressDrawable(Context paramContext, View paramView)
  {
    this.i = paramView;
    this.h = paramContext.getResources();
    this.f = new Ring(this.n);
    this.f.a(this.d);
    a(1);
    b();
  }
  
  private float a(Ring paramRing)
  {
    return (float)Math.toRadians(paramRing.d() / (6.283185307179586D * paramRing.j()));
  }
  
  private int a(float paramFloat, int paramInt1, int paramInt2)
  {
    int i3 = Integer.valueOf(paramInt1).intValue();
    paramInt1 = i3 >> 24 & 0xFF;
    int i1 = i3 >> 16 & 0xFF;
    int i2 = i3 >> 8 & 0xFF;
    i3 &= 0xFF;
    paramInt2 = Integer.valueOf(paramInt2).intValue();
    return (int)(((paramInt2 >> 24 & 0xFF) - paramInt1) * paramFloat) + paramInt1 << 24 | (int)(((paramInt2 >> 16 & 0xFF) - i1) * paramFloat) + i1 << 16 | (int)(((paramInt2 >> 8 & 0xFF) - i2) * paramFloat) + i2 << 8 | (int)(((paramInt2 & 0xFF) - i3) * paramFloat) + i3;
  }
  
  private void a(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, float paramFloat1, float paramFloat2)
  {
    Ring localRing = this.f;
    float f1 = this.h.getDisplayMetrics().density;
    this.l = (f1 * paramDouble1);
    this.m = (f1 * paramDouble2);
    localRing.a((float)paramDouble4 * f1);
    localRing.a(f1 * paramDouble3);
    localRing.c(0);
    localRing.a(paramFloat1 * f1, paramFloat2 * f1);
    localRing.a((int)this.l, (int)this.m);
  }
  
  private void a(float paramFloat, Ring paramRing)
  {
    if (paramFloat > 0.75F) {
      paramRing.b(a((paramFloat - 0.75F) / 0.25F, paramRing.h(), paramRing.a()));
    }
  }
  
  private void b()
  {
    final Ring localRing = this.f;
    Animation local1 = new Animation()
    {
      public void applyTransformation(float paramAnonymousFloat, Transformation paramAnonymousTransformation)
      {
        if (MaterialProgressDrawable.this.a)
        {
          MaterialProgressDrawable.a(MaterialProgressDrawable.this, paramAnonymousFloat, localRing);
          return;
        }
        float f1 = MaterialProgressDrawable.a(MaterialProgressDrawable.this, localRing);
        float f2 = localRing.g();
        float f4 = localRing.f();
        float f3 = localRing.k();
        MaterialProgressDrawable.b(MaterialProgressDrawable.this, paramAnonymousFloat, localRing);
        if (paramAnonymousFloat <= 0.5F)
        {
          float f5 = paramAnonymousFloat / 0.5F;
          f5 = MaterialProgressDrawable.a().getInterpolation(f5);
          localRing.b(f4 + (0.8F - f1) * f5);
        }
        if (paramAnonymousFloat > 0.5F)
        {
          f4 = (paramAnonymousFloat - 0.5F) / 0.5F;
          f4 = MaterialProgressDrawable.a().getInterpolation(f4);
          localRing.c(f2 + f4 * (0.8F - f1));
        }
        localRing.d(f3 + 0.25F * paramAnonymousFloat);
        f1 = MaterialProgressDrawable.a(MaterialProgressDrawable.this) / 5.0F;
        MaterialProgressDrawable.this.c(216.0F * paramAnonymousFloat + 1080.0F * f1);
      }
    };
    local1.setRepeatCount(-1);
    local1.setRepeatMode(1);
    local1.setInterpolator(b);
    local1.setAnimationListener(new Animation.AnimationListener()
    {
      public void onAnimationEnd(Animation paramAnonymousAnimation) {}
      
      public void onAnimationRepeat(Animation paramAnonymousAnimation)
      {
        localRing.l();
        localRing.b();
        localRing.b(localRing.i());
        if (MaterialProgressDrawable.this.a)
        {
          MaterialProgressDrawable.this.a = false;
          paramAnonymousAnimation.setDuration(1332L);
          localRing.a(false);
          return;
        }
        MaterialProgressDrawable.a(MaterialProgressDrawable.this, (MaterialProgressDrawable.a(MaterialProgressDrawable.this) + 1.0F) % 5.0F);
      }
      
      public void onAnimationStart(Animation paramAnonymousAnimation)
      {
        MaterialProgressDrawable.a(MaterialProgressDrawable.this, 0.0F);
      }
    });
    this.j = local1;
  }
  
  private void b(float paramFloat, Ring paramRing)
  {
    a(paramFloat, paramRing);
    float f1 = (float)(Math.floor(paramRing.k() / 0.8F) + 1.0D);
    float f2 = a(paramRing);
    paramRing.b(paramRing.f() + (paramRing.g() - f2 - paramRing.f()) * paramFloat);
    paramRing.c(paramRing.g());
    paramRing.d(paramRing.k() + (f1 - paramRing.k()) * paramFloat);
  }
  
  public void a(float paramFloat)
  {
    this.f.e(paramFloat);
  }
  
  public void a(float paramFloat1, float paramFloat2)
  {
    this.f.b(paramFloat1);
    this.f.c(paramFloat2);
  }
  
  public void a(@ProgressDrawableSize int paramInt)
  {
    if (paramInt == 0)
    {
      a(56.0D, 56.0D, 12.5D, 3.0D, 12.0F, 6.0F);
      return;
    }
    a(40.0D, 40.0D, 8.75D, 2.5D, 10.0F, 5.0F);
  }
  
  public void a(boolean paramBoolean)
  {
    this.f.a(paramBoolean);
  }
  
  public void a(int... paramVarArgs)
  {
    this.f.a(paramVarArgs);
    this.f.c(0);
  }
  
  public void b(float paramFloat)
  {
    this.f.d(paramFloat);
  }
  
  public void b(int paramInt)
  {
    this.f.a(paramInt);
  }
  
  void c(float paramFloat)
  {
    this.g = paramFloat;
    invalidateSelf();
  }
  
  public void draw(Canvas paramCanvas)
  {
    Rect localRect = getBounds();
    int i1 = paramCanvas.save();
    paramCanvas.rotate(this.g, localRect.exactCenterX(), localRect.exactCenterY());
    this.f.a(paramCanvas, localRect);
    paramCanvas.restoreToCount(i1);
  }
  
  public int getAlpha()
  {
    return this.f.c();
  }
  
  public int getIntrinsicHeight()
  {
    return (int)this.m;
  }
  
  public int getIntrinsicWidth()
  {
    return (int)this.l;
  }
  
  public int getOpacity()
  {
    return -3;
  }
  
  public boolean isRunning()
  {
    ArrayList localArrayList = this.e;
    int i2 = localArrayList.size();
    int i1 = 0;
    while (i1 < i2)
    {
      Animation localAnimation = (Animation)localArrayList.get(i1);
      if ((localAnimation.hasStarted()) && (!localAnimation.hasEnded())) {
        return true;
      }
      i1 += 1;
    }
    return false;
  }
  
  public void setAlpha(int paramInt)
  {
    this.f.d(paramInt);
  }
  
  public void setColorFilter(ColorFilter paramColorFilter)
  {
    this.f.a(paramColorFilter);
  }
  
  public void start()
  {
    this.j.reset();
    this.f.l();
    if (this.f.i() != this.f.e())
    {
      this.a = true;
      this.j.setDuration(666L);
      this.i.startAnimation(this.j);
      return;
    }
    this.f.c(0);
    this.f.m();
    this.j.setDuration(1332L);
    this.i.startAnimation(this.j);
  }
  
  public void stop()
  {
    this.i.clearAnimation();
    c(0.0F);
    this.f.a(false);
    this.f.c(0);
    this.f.m();
  }
  
  @Retention(RetentionPolicy.CLASS)
  public static @interface ProgressDrawableSize {}
  
  private static class Ring
  {
    private final RectF a = new RectF();
    private final Paint b = new Paint();
    private final Paint c = new Paint();
    private final Drawable.Callback d;
    private float e = 0.0F;
    private float f = 0.0F;
    private float g = 0.0F;
    private float h = 5.0F;
    private float i = 2.5F;
    private int[] j;
    private int k;
    private float l;
    private float m;
    private float n;
    private boolean o;
    private Path p;
    private float q;
    private double r;
    private int s;
    private int t;
    private int u;
    private final Paint v = new Paint(1);
    private int w;
    private int x;
    
    public Ring(Drawable.Callback paramCallback)
    {
      this.d = paramCallback;
      this.b.setStrokeCap(Paint.Cap.SQUARE);
      this.b.setAntiAlias(true);
      this.b.setStyle(Paint.Style.STROKE);
      this.c.setStyle(Paint.Style.FILL);
      this.c.setAntiAlias(true);
    }
    
    private void a(Canvas paramCanvas, float paramFloat1, float paramFloat2, Rect paramRect)
    {
      if (this.o)
      {
        if (this.p != null) {
          break label213;
        }
        this.p = new Path();
        this.p.setFillType(Path.FillType.EVEN_ODD);
      }
      for (;;)
      {
        float f1 = (int)this.i / 2;
        float f2 = this.q;
        float f3 = (float)(this.r * Math.cos(0.0D) + paramRect.exactCenterX());
        float f4 = (float)(this.r * Math.sin(0.0D) + paramRect.exactCenterY());
        this.p.moveTo(0.0F, 0.0F);
        this.p.lineTo(this.s * this.q, 0.0F);
        this.p.lineTo(this.s * this.q / 2.0F, this.t * this.q);
        this.p.offset(f3 - f1 * f2, f4);
        this.p.close();
        this.c.setColor(this.x);
        paramCanvas.rotate(paramFloat1 + paramFloat2 - 5.0F, paramRect.exactCenterX(), paramRect.exactCenterY());
        paramCanvas.drawPath(this.p, this.c);
        return;
        label213:
        this.p.reset();
      }
    }
    
    private int n()
    {
      return (this.k + 1) % this.j.length;
    }
    
    private void o()
    {
      this.d.invalidateDrawable(null);
    }
    
    public int a()
    {
      return this.j[n()];
    }
    
    public void a(double paramDouble)
    {
      this.r = paramDouble;
    }
    
    public void a(float paramFloat)
    {
      this.h = paramFloat;
      this.b.setStrokeWidth(paramFloat);
      o();
    }
    
    public void a(float paramFloat1, float paramFloat2)
    {
      this.s = ((int)paramFloat1);
      this.t = ((int)paramFloat2);
    }
    
    public void a(int paramInt)
    {
      this.w = paramInt;
    }
    
    public void a(int paramInt1, int paramInt2)
    {
      float f1 = Math.min(paramInt1, paramInt2);
      if ((this.r <= 0.0D) || (f1 < 0.0F)) {}
      for (f1 = (float)Math.ceil(this.h / 2.0F);; f1 = (float)(f1 / 2.0F - this.r))
      {
        this.i = f1;
        return;
      }
    }
    
    public void a(Canvas paramCanvas, Rect paramRect)
    {
      RectF localRectF = this.a;
      localRectF.set(paramRect);
      localRectF.inset(this.i, this.i);
      float f1 = (this.e + this.g) * 360.0F;
      float f2 = (this.f + this.g) * 360.0F - f1;
      this.b.setColor(this.x);
      paramCanvas.drawArc(localRectF, f1, f2, false, this.b);
      a(paramCanvas, f1, f2, paramRect);
      if (this.u < 255)
      {
        this.v.setColor(this.w);
        this.v.setAlpha(255 - this.u);
        paramCanvas.drawCircle(paramRect.exactCenterX(), paramRect.exactCenterY(), paramRect.width() / 2, this.v);
      }
    }
    
    public void a(ColorFilter paramColorFilter)
    {
      this.b.setColorFilter(paramColorFilter);
      o();
    }
    
    public void a(boolean paramBoolean)
    {
      if (this.o != paramBoolean)
      {
        this.o = paramBoolean;
        o();
      }
    }
    
    public void a(@NonNull int[] paramArrayOfInt)
    {
      this.j = paramArrayOfInt;
      c(0);
    }
    
    public void b()
    {
      c(n());
    }
    
    public void b(float paramFloat)
    {
      this.e = paramFloat;
      o();
    }
    
    public void b(int paramInt)
    {
      this.x = paramInt;
    }
    
    public int c()
    {
      return this.u;
    }
    
    public void c(float paramFloat)
    {
      this.f = paramFloat;
      o();
    }
    
    public void c(int paramInt)
    {
      this.k = paramInt;
      this.x = this.j[this.k];
    }
    
    public float d()
    {
      return this.h;
    }
    
    public void d(float paramFloat)
    {
      this.g = paramFloat;
      o();
    }
    
    public void d(int paramInt)
    {
      this.u = paramInt;
    }
    
    public float e()
    {
      return this.e;
    }
    
    public void e(float paramFloat)
    {
      if (paramFloat != this.q)
      {
        this.q = paramFloat;
        o();
      }
    }
    
    public float f()
    {
      return this.l;
    }
    
    public float g()
    {
      return this.m;
    }
    
    public int h()
    {
      return this.j[this.k];
    }
    
    public float i()
    {
      return this.f;
    }
    
    public double j()
    {
      return this.r;
    }
    
    public float k()
    {
      return this.n;
    }
    
    public void l()
    {
      this.l = this.e;
      this.m = this.f;
      this.n = this.g;
    }
    
    public void m()
    {
      this.l = 0.0F;
      this.m = 0.0F;
      this.n = 0.0F;
      b(0.0F);
      c(0.0F);
      d(0.0F);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/widget/MaterialProgressDrawable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */