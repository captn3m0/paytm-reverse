package android.support.v4.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.v4.view.AccessibilityDelegateCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.accessibility.AccessibilityEvent;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class SlidingPaneLayout
  extends ViewGroup
{
  static final SlidingPanelLayoutImpl a = new SlidingPanelLayoutImplBase();
  private int b = -858993460;
  private int c;
  private Drawable d;
  private Drawable e;
  private final int f;
  private boolean g;
  private View h;
  private float i;
  private float j;
  private int k;
  private boolean l;
  private int m;
  private float n;
  private float o;
  private PanelSlideListener p;
  private final ViewDragHelper q;
  private boolean r;
  private boolean s = true;
  private final Rect t = new Rect();
  private final ArrayList<DisableLayerRunnable> u = new ArrayList();
  
  static
  {
    int i1 = Build.VERSION.SDK_INT;
    if (i1 >= 17)
    {
      a = new SlidingPanelLayoutImplJBMR1();
      return;
    }
    if (i1 >= 16)
    {
      a = new SlidingPanelLayoutImplJB();
      return;
    }
  }
  
  public SlidingPaneLayout(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public SlidingPaneLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public SlidingPaneLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    float f1 = paramContext.getResources().getDisplayMetrics().density;
    this.f = ((int)(32.0F * f1 + 0.5F));
    ViewConfiguration.get(paramContext);
    setWillNotDraw(false);
    ViewCompat.a(this, new AccessibilityDelegate());
    ViewCompat.c(this, 1);
    this.q = ViewDragHelper.a(this, 0.5F, new DragHelperCallback(null));
    this.q.a(400.0F * f1);
  }
  
  private void a(float paramFloat)
  {
    boolean bool = f();
    Object localObject = (LayoutParams)this.h.getLayoutParams();
    int i1;
    label43:
    int i2;
    if (((LayoutParams)localObject).c) {
      if (bool)
      {
        i1 = ((LayoutParams)localObject).rightMargin;
        if (i1 > 0) {
          break label94;
        }
        i1 = 1;
        int i5 = getChildCount();
        i2 = 0;
        label52:
        if (i2 >= i5) {
          return;
        }
        localObject = getChildAt(i2);
        if (localObject != this.h) {
          break label99;
        }
      }
    }
    label94:
    label99:
    do
    {
      i2 += 1;
      break label52;
      i1 = ((LayoutParams)localObject).leftMargin;
      break;
      i1 = 0;
      break label43;
      int i3 = (int)((1.0F - this.j) * this.m);
      this.j = paramFloat;
      int i4 = i3 - (int)((1.0F - paramFloat) * this.m);
      i3 = i4;
      if (bool) {
        i3 = -i4;
      }
      ((View)localObject).offsetLeftAndRight(i3);
    } while (i1 == 0);
    if (bool) {}
    for (float f1 = this.j - 1.0F;; f1 = 1.0F - this.j)
    {
      a((View)localObject, f1, this.c);
      break;
    }
  }
  
  private void a(int paramInt)
  {
    if (this.h == null)
    {
      this.i = 0.0F;
      return;
    }
    boolean bool = f();
    LayoutParams localLayoutParams = (LayoutParams)this.h.getLayoutParams();
    int i1 = this.h.getWidth();
    if (bool)
    {
      paramInt = getWidth() - paramInt - i1;
      if (!bool) {
        break label141;
      }
      i1 = getPaddingRight();
      label63:
      if (!bool) {
        break label149;
      }
    }
    label141:
    label149:
    for (int i2 = localLayoutParams.rightMargin;; i2 = localLayoutParams.leftMargin)
    {
      this.i = ((paramInt - (i1 + i2)) / this.k);
      if (this.m != 0) {
        a(this.i);
      }
      if (localLayoutParams.c) {
        a(this.h, this.i, this.b);
      }
      a(this.h);
      return;
      break;
      i1 = getPaddingLeft();
      break label63;
    }
  }
  
  private void a(View paramView, float paramFloat, int paramInt)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    if ((paramFloat > 0.0F) && (paramInt != 0))
    {
      i1 = (int)(((0xFF000000 & paramInt) >>> 24) * paramFloat);
      if (localLayoutParams.d == null) {
        localLayoutParams.d = new Paint();
      }
      localLayoutParams.d.setColorFilter(new PorterDuffColorFilter(i1 << 24 | 0xFFFFFF & paramInt, PorterDuff.Mode.SRC_OVER));
      if (ViewCompat.g(paramView) != 2) {
        ViewCompat.a(paramView, 2, localLayoutParams.d);
      }
      g(paramView);
    }
    while (ViewCompat.g(paramView) == 0)
    {
      int i1;
      return;
    }
    if (localLayoutParams.d != null) {
      localLayoutParams.d.setColorFilter(null);
    }
    paramView = new DisableLayerRunnable(paramView);
    this.u.add(paramView);
    ViewCompat.a(this, paramView);
  }
  
  private boolean a(View paramView, int paramInt)
  {
    boolean bool = false;
    if ((this.s) || (a(0.0F, paramInt)))
    {
      this.r = false;
      bool = true;
    }
    return bool;
  }
  
  private boolean b(View paramView, int paramInt)
  {
    if ((this.s) || (a(1.0F, paramInt)))
    {
      this.r = true;
      return true;
    }
    return false;
  }
  
  private boolean f()
  {
    return ViewCompat.h(this) == 1;
  }
  
  private static boolean f(View paramView)
  {
    if (ViewCompat.j(paramView)) {}
    do
    {
      return true;
      if (Build.VERSION.SDK_INT >= 18) {
        return false;
      }
      paramView = paramView.getBackground();
      if (paramView == null) {
        break;
      }
    } while (paramView.getOpacity() == -1);
    return false;
    return false;
  }
  
  private void g(View paramView)
  {
    a.a(this, paramView);
  }
  
  void a()
  {
    int i1 = 0;
    int i2 = getChildCount();
    while (i1 < i2)
    {
      View localView = getChildAt(i1);
      if (localView.getVisibility() == 4) {
        localView.setVisibility(0);
      }
      i1 += 1;
    }
  }
  
  void a(View paramView)
  {
    if (this.p != null) {
      this.p.a(paramView, this.i);
    }
  }
  
  boolean a(float paramFloat, int paramInt)
  {
    if (!this.g) {}
    for (;;)
    {
      return false;
      boolean bool = f();
      LayoutParams localLayoutParams = (LayoutParams)this.h.getLayoutParams();
      int i1;
      int i2;
      if (bool)
      {
        paramInt = getPaddingRight();
        i1 = localLayoutParams.rightMargin;
        i2 = this.h.getWidth();
      }
      for (paramInt = (int)(getWidth() - (paramInt + i1 + this.k * paramFloat + i2)); this.q.a(this.h, paramInt, this.h.getTop()); paramInt = (int)(getPaddingLeft() + localLayoutParams.leftMargin + this.k * paramFloat))
      {
        a();
        ViewCompat.d(this);
        return true;
      }
    }
  }
  
  void b(View paramView)
  {
    if (this.p != null) {
      this.p.a(paramView);
    }
    sendAccessibilityEvent(32);
  }
  
  public boolean b()
  {
    return b(this.h, 0);
  }
  
  void c(View paramView)
  {
    if (this.p != null) {
      this.p.b(paramView);
    }
    sendAccessibilityEvent(32);
  }
  
  public boolean c()
  {
    return a(this.h, 0);
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return ((paramLayoutParams instanceof LayoutParams)) && (super.checkLayoutParams(paramLayoutParams));
  }
  
  public void computeScroll()
  {
    if (this.q.a(true))
    {
      if (!this.g) {
        this.q.e();
      }
    }
    else {
      return;
    }
    ViewCompat.d(this);
  }
  
  void d(View paramView)
  {
    boolean bool = f();
    int i1;
    int i2;
    label31:
    int i9;
    int i10;
    int i11;
    int i4;
    int i5;
    int i6;
    int i3;
    if (bool)
    {
      i1 = getWidth() - getPaddingRight();
      if (!bool) {
        break label123;
      }
      i2 = getPaddingLeft();
      i9 = getPaddingTop();
      i10 = getHeight();
      i11 = getPaddingBottom();
      if ((paramView == null) || (!f(paramView))) {
        break label136;
      }
      i4 = paramView.getLeft();
      i5 = paramView.getRight();
      i6 = paramView.getTop();
      i3 = paramView.getBottom();
    }
    int i7;
    View localView;
    for (;;)
    {
      i7 = 0;
      int i12 = getChildCount();
      if (i7 < i12)
      {
        localView = getChildAt(i7);
        if (localView != paramView) {
          break label151;
        }
      }
      return;
      i1 = getPaddingLeft();
      break;
      label123:
      i2 = getWidth() - getPaddingRight();
      break label31;
      label136:
      i3 = 0;
      i6 = 0;
      i5 = 0;
      i4 = 0;
    }
    label151:
    if (bool)
    {
      i8 = i2;
      label159:
      int i13 = Math.max(i8, localView.getLeft());
      int i14 = Math.max(i9, localView.getTop());
      if (!bool) {
        break label271;
      }
      i8 = i1;
      label191:
      i8 = Math.min(i8, localView.getRight());
      int i15 = Math.min(i10 - i11, localView.getBottom());
      if ((i13 < i4) || (i14 < i6) || (i8 > i5) || (i15 > i3)) {
        break label277;
      }
    }
    label271:
    label277:
    for (int i8 = 4;; i8 = 0)
    {
      localView.setVisibility(i8);
      i7 += 1;
      break;
      i8 = i1;
      break label159;
      i8 = i2;
      break label191;
    }
  }
  
  public boolean d()
  {
    return (!this.g) || (this.i == 1.0F);
  }
  
  public void draw(Canvas paramCanvas)
  {
    super.draw(paramCanvas);
    Drawable localDrawable;
    if (f())
    {
      localDrawable = this.e;
      if (getChildCount() <= 1) {
        break label53;
      }
    }
    label53:
    for (View localView = getChildAt(1);; localView = null)
    {
      if ((localView != null) && (localDrawable != null)) {
        break label59;
      }
      return;
      localDrawable = this.d;
      break;
    }
    label59:
    int i3 = localView.getTop();
    int i4 = localView.getBottom();
    int i5 = localDrawable.getIntrinsicWidth();
    int i1;
    int i2;
    if (f())
    {
      i1 = localView.getRight();
      i2 = i1 + i5;
    }
    for (;;)
    {
      localDrawable.setBounds(i1, i3, i2, i4);
      localDrawable.draw(paramCanvas);
      return;
      i2 = localView.getLeft();
      i1 = i2 - i5;
    }
  }
  
  protected boolean drawChild(Canvas paramCanvas, View paramView, long paramLong)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    int i1 = paramCanvas.save(2);
    boolean bool;
    if ((this.g) && (!localLayoutParams.b) && (this.h != null))
    {
      paramCanvas.getClipBounds(this.t);
      if (f())
      {
        this.t.left = Math.max(this.t.left, this.h.getRight());
        paramCanvas.clipRect(this.t);
      }
    }
    else
    {
      if (Build.VERSION.SDK_INT < 11) {
        break label140;
      }
      bool = super.drawChild(paramCanvas, paramView, paramLong);
    }
    for (;;)
    {
      paramCanvas.restoreToCount(i1);
      return bool;
      this.t.right = Math.min(this.t.right, this.h.getLeft());
      break;
      label140:
      if ((localLayoutParams.c) && (this.i > 0.0F))
      {
        if (!paramView.isDrawingCacheEnabled()) {
          paramView.setDrawingCacheEnabled(true);
        }
        Bitmap localBitmap = paramView.getDrawingCache();
        if (localBitmap != null)
        {
          paramCanvas.drawBitmap(localBitmap, paramView.getLeft(), paramView.getTop(), localLayoutParams.d);
          bool = false;
        }
        else
        {
          Log.e("SlidingPaneLayout", "drawChild: child view " + paramView + " returned null drawing cache");
          bool = super.drawChild(paramCanvas, paramView, paramLong);
        }
      }
      else
      {
        if (paramView.isDrawingCacheEnabled()) {
          paramView.setDrawingCacheEnabled(false);
        }
        bool = super.drawChild(paramCanvas, paramView, paramLong);
      }
    }
  }
  
  public boolean e()
  {
    return this.g;
  }
  
  boolean e(View paramView)
  {
    if (paramView == null) {}
    do
    {
      return false;
      paramView = (LayoutParams)paramView.getLayoutParams();
    } while ((!this.g) || (!paramView.c) || (this.i <= 0.0F));
    return true;
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams()
  {
    return new LayoutParams();
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
  {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    if ((paramLayoutParams instanceof ViewGroup.MarginLayoutParams)) {
      return new LayoutParams((ViewGroup.MarginLayoutParams)paramLayoutParams);
    }
    return new LayoutParams(paramLayoutParams);
  }
  
  @ColorInt
  public int getCoveredFadeColor()
  {
    return this.c;
  }
  
  public int getParallaxDistance()
  {
    return this.m;
  }
  
  @ColorInt
  public int getSliderFadeColor()
  {
    return this.b;
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    this.s = true;
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    this.s = true;
    int i1 = 0;
    int i2 = this.u.size();
    while (i1 < i2)
    {
      ((DisableLayerRunnable)this.u.get(i1)).run();
      i1 += 1;
    }
    this.u.clear();
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    int i3 = MotionEventCompat.a(paramMotionEvent);
    if ((!this.g) && (i3 == 0) && (getChildCount() > 1))
    {
      View localView = getChildAt(1);
      if (localView != null) {
        if (this.q.b(localView, (int)paramMotionEvent.getX(), (int)paramMotionEvent.getY())) {
          break label101;
        }
      }
    }
    label101:
    for (boolean bool = true;; bool = false)
    {
      this.r = bool;
      if ((this.g) && ((!this.l) || (i3 == 0))) {
        break;
      }
      this.q.cancel();
      return super.onInterceptTouchEvent(paramMotionEvent);
    }
    if ((i3 == 3) || (i3 == 1))
    {
      this.q.cancel();
      return false;
    }
    int i2 = 0;
    int i1 = i2;
    switch (i3)
    {
    default: 
      i1 = i2;
    }
    while ((this.q.a(paramMotionEvent)) || (i1 != 0))
    {
      return true;
      this.l = false;
      float f1 = paramMotionEvent.getX();
      float f2 = paramMotionEvent.getY();
      this.n = f1;
      this.o = f2;
      i1 = i2;
      if (this.q.b(this.h, (int)f1, (int)f2))
      {
        i1 = i2;
        if (e(this.h))
        {
          i1 = 1;
          continue;
          f2 = paramMotionEvent.getX();
          f1 = paramMotionEvent.getY();
          f2 = Math.abs(f2 - this.n);
          f1 = Math.abs(f1 - this.o);
          i1 = i2;
          if (f2 > this.q.d())
          {
            i1 = i2;
            if (f1 > f2)
            {
              this.q.cancel();
              this.l = true;
              return false;
            }
          }
        }
      }
    }
    return false;
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    boolean bool = f();
    int i3;
    label35:
    label46:
    int i5;
    int i4;
    if (bool)
    {
      this.q.a(2);
      i3 = paramInt3 - paramInt1;
      if (!bool) {
        break label148;
      }
      paramInt1 = getPaddingRight();
      if (!bool) {
        break label156;
      }
      paramInt3 = getPaddingLeft();
      i5 = getPaddingTop();
      i4 = getChildCount();
      paramInt4 = paramInt1;
      if (this.s) {
        if ((!this.g) || (!this.r)) {
          break label165;
        }
      }
    }
    int i1;
    View localView;
    label148:
    label156:
    label165:
    for (float f1 = 1.0F;; f1 = 0.0F)
    {
      this.i = f1;
      i1 = 0;
      paramInt2 = paramInt1;
      paramInt1 = paramInt4;
      paramInt4 = i1;
      for (;;)
      {
        if (paramInt4 >= i4) {
          break label444;
        }
        localView = getChildAt(paramInt4);
        if (localView.getVisibility() != 8) {
          break;
        }
        paramInt4 += 1;
      }
      this.q.a(1);
      break;
      paramInt1 = getPaddingLeft();
      break label35;
      paramInt3 = getPaddingRight();
      break label46;
    }
    LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
    int i6 = localView.getMeasuredWidth();
    int i2 = 0;
    if (localLayoutParams.b)
    {
      i1 = localLayoutParams.leftMargin;
      int i7 = localLayoutParams.rightMargin;
      i7 = Math.min(paramInt1, i3 - paramInt3 - this.f) - paramInt2 - (i1 + i7);
      this.k = i7;
      if (bool)
      {
        i1 = localLayoutParams.rightMargin;
        label255:
        if (paramInt2 + i1 + i7 + i6 / 2 <= i3 - paramInt3) {
          break label380;
        }
        paramBoolean = true;
        label277:
        localLayoutParams.c = paramBoolean;
        i7 = (int)(i7 * this.i);
        paramInt2 += i7 + i1;
        this.i = (i7 / this.k);
        i1 = i2;
        label319:
        if (!bool) {
          break label428;
        }
        i2 = i3 - paramInt2 + i1;
        i1 = i2 - i6;
      }
    }
    for (;;)
    {
      localView.layout(i1, i5, i2, i5 + localView.getMeasuredHeight());
      paramInt1 += localView.getWidth();
      break;
      i1 = localLayoutParams.leftMargin;
      break label255;
      label380:
      paramBoolean = false;
      break label277;
      if ((this.g) && (this.m != 0))
      {
        i1 = (int)((1.0F - this.i) * this.m);
        paramInt2 = paramInt1;
        break label319;
      }
      paramInt2 = paramInt1;
      i1 = i2;
      break label319;
      label428:
      i1 = paramInt2 - i1;
      i2 = i1 + i6;
    }
    label444:
    if (this.s)
    {
      if (!this.g) {
        break label519;
      }
      if (this.m != 0) {
        a(this.i);
      }
      if (((LayoutParams)this.h.getLayoutParams()).c) {
        a(this.h, this.i, this.b);
      }
    }
    for (;;)
    {
      d(this.h);
      this.s = false;
      return;
      label519:
      paramInt1 = 0;
      while (paramInt1 < i4)
      {
        a(getChildAt(paramInt1), 0.0F, this.b);
        paramInt1 += 1;
      }
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i4 = View.MeasureSpec.getMode(paramInt1);
    int i1 = View.MeasureSpec.getSize(paramInt1);
    int i2 = View.MeasureSpec.getMode(paramInt2);
    paramInt2 = View.MeasureSpec.getSize(paramInt2);
    int i3;
    int i5;
    label88:
    float f1;
    boolean bool1;
    int i8;
    int i9;
    int i6;
    label141:
    View localView;
    LayoutParams localLayoutParams;
    int i7;
    if (i4 != 1073741824) {
      if (isInEditMode()) {
        if (i4 == Integer.MIN_VALUE)
        {
          i3 = i1;
          paramInt1 = paramInt2;
          i5 = i2;
          i1 = 0;
          paramInt2 = -1;
          switch (i5)
          {
          default: 
            f1 = 0.0F;
            bool1 = false;
            i8 = i3 - getPaddingLeft() - getPaddingRight();
            i4 = i8;
            i9 = getChildCount();
            if (i9 > 2) {
              Log.e("SlidingPaneLayout", "onMeasure: More than two child views are not supported.");
            }
            this.h = null;
            i6 = 0;
            if (i6 >= i9) {
              break label654;
            }
            localView = getChildAt(i6);
            localLayoutParams = (LayoutParams)localView.getLayoutParams();
            if (localView.getVisibility() == 8)
            {
              localLayoutParams.c = false;
              i7 = i4;
              i2 = i1;
              bool2 = bool1;
            }
            break;
          }
        }
      }
    }
    float f2;
    do
    {
      i6 += 1;
      bool1 = bool2;
      i1 = i2;
      i4 = i7;
      break label141;
      i5 = i2;
      paramInt1 = paramInt2;
      i3 = i1;
      if (i4 != 0) {
        break;
      }
      i3 = 300;
      i5 = i2;
      paramInt1 = paramInt2;
      break;
      throw new IllegalStateException("Width must have an exact value or MATCH_PARENT");
      i5 = i2;
      paramInt1 = paramInt2;
      i3 = i1;
      if (i2 != 0) {
        break;
      }
      if (isInEditMode())
      {
        i5 = i2;
        paramInt1 = paramInt2;
        i3 = i1;
        if (i2 != 0) {
          break;
        }
        i5 = Integer.MIN_VALUE;
        paramInt1 = 300;
        i3 = i1;
        break;
      }
      throw new IllegalStateException("Height must not be UNSPECIFIED");
      paramInt2 = paramInt1 - getPaddingTop() - getPaddingBottom();
      i1 = paramInt2;
      break label88;
      paramInt2 = paramInt1 - getPaddingTop() - getPaddingBottom();
      break label88;
      f2 = f1;
      if (localLayoutParams.a <= 0.0F) {
        break label397;
      }
      f2 = f1 + localLayoutParams.a;
      bool2 = bool1;
      i2 = i1;
      f1 = f2;
      i7 = i4;
    } while (localLayoutParams.width == 0);
    label397:
    paramInt1 = localLayoutParams.leftMargin + localLayoutParams.rightMargin;
    if (localLayoutParams.width == -2)
    {
      paramInt1 = View.MeasureSpec.makeMeasureSpec(i8 - paramInt1, Integer.MIN_VALUE);
      label430:
      if (localLayoutParams.height != -2) {
        break label611;
      }
      i2 = View.MeasureSpec.makeMeasureSpec(paramInt2, Integer.MIN_VALUE);
      label449:
      localView.measure(paramInt1, i2);
      i2 = localView.getMeasuredWidth();
      i7 = localView.getMeasuredHeight();
      paramInt1 = i1;
      if (i5 == Integer.MIN_VALUE)
      {
        paramInt1 = i1;
        if (i7 > i1) {
          paramInt1 = Math.min(i7, paramInt2);
        }
      }
      i1 = i4 - i2;
      if (i1 >= 0) {
        break label648;
      }
    }
    label611:
    label648:
    for (boolean bool2 = true;; bool2 = false)
    {
      localLayoutParams.b = bool2;
      bool1 |= bool2;
      bool2 = bool1;
      i2 = paramInt1;
      f1 = f2;
      i7 = i1;
      if (!localLayoutParams.b) {
        break;
      }
      this.h = localView;
      bool2 = bool1;
      i2 = paramInt1;
      f1 = f2;
      i7 = i1;
      break;
      if (localLayoutParams.width == -1)
      {
        paramInt1 = View.MeasureSpec.makeMeasureSpec(i8 - paramInt1, 1073741824);
        break label430;
      }
      paramInt1 = View.MeasureSpec.makeMeasureSpec(localLayoutParams.width, 1073741824);
      break label430;
      if (localLayoutParams.height == -1)
      {
        i2 = View.MeasureSpec.makeMeasureSpec(paramInt2, 1073741824);
        break label449;
      }
      i2 = View.MeasureSpec.makeMeasureSpec(localLayoutParams.height, 1073741824);
      break label449;
    }
    label654:
    if ((bool1) || (f1 > 0.0F))
    {
      i6 = i8 - this.f;
      i2 = 0;
      if (i2 < i9)
      {
        localView = getChildAt(i2);
        if (localView.getVisibility() == 8) {}
        for (;;)
        {
          i2 += 1;
          break;
          localLayoutParams = (LayoutParams)localView.getLayoutParams();
          if (localView.getVisibility() != 8)
          {
            if ((localLayoutParams.width == 0) && (localLayoutParams.a > 0.0F))
            {
              paramInt1 = 1;
              label751:
              if (paramInt1 == 0) {
                break label841;
              }
              i5 = 0;
              label758:
              if ((!bool1) || (localView == this.h)) {
                break label901;
              }
              if ((localLayoutParams.width >= 0) || ((i5 <= i6) && (localLayoutParams.a <= 0.0F))) {
                continue;
              }
              if (paramInt1 == 0) {
                break label886;
              }
              if (localLayoutParams.height != -2) {
                break label851;
              }
              paramInt1 = View.MeasureSpec.makeMeasureSpec(paramInt2, Integer.MIN_VALUE);
            }
            for (;;)
            {
              localView.measure(View.MeasureSpec.makeMeasureSpec(i6, 1073741824), paramInt1);
              break;
              paramInt1 = 0;
              break label751;
              label841:
              i5 = localView.getMeasuredWidth();
              break label758;
              label851:
              if (localLayoutParams.height == -1)
              {
                paramInt1 = View.MeasureSpec.makeMeasureSpec(paramInt2, 1073741824);
              }
              else
              {
                paramInt1 = View.MeasureSpec.makeMeasureSpec(localLayoutParams.height, 1073741824);
                continue;
                label886:
                paramInt1 = View.MeasureSpec.makeMeasureSpec(localView.getMeasuredHeight(), 1073741824);
              }
            }
            label901:
            if (localLayoutParams.a > 0.0F)
            {
              if (localLayoutParams.width == 0) {
                if (localLayoutParams.height == -2) {
                  paramInt1 = View.MeasureSpec.makeMeasureSpec(paramInt2, Integer.MIN_VALUE);
                }
              }
              for (;;)
              {
                if (!bool1) {
                  break label1036;
                }
                i7 = i8 - (localLayoutParams.leftMargin + localLayoutParams.rightMargin);
                int i10 = View.MeasureSpec.makeMeasureSpec(i7, 1073741824);
                if (i5 == i7) {
                  break;
                }
                localView.measure(i10, paramInt1);
                break;
                if (localLayoutParams.height == -1)
                {
                  paramInt1 = View.MeasureSpec.makeMeasureSpec(paramInt2, 1073741824);
                }
                else
                {
                  paramInt1 = View.MeasureSpec.makeMeasureSpec(localLayoutParams.height, 1073741824);
                  continue;
                  paramInt1 = View.MeasureSpec.makeMeasureSpec(localView.getMeasuredHeight(), 1073741824);
                }
              }
              label1036:
              i7 = Math.max(0, i4);
              localView.measure(View.MeasureSpec.makeMeasureSpec(i5 + (int)(localLayoutParams.a * i7 / f1), 1073741824), paramInt1);
            }
          }
        }
      }
    }
    setMeasuredDimension(i3, getPaddingTop() + i1 + getPaddingBottom());
    this.g = bool1;
    if ((this.q.a() != 0) && (!bool1)) {
      this.q.e();
    }
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    if (!(paramParcelable instanceof SavedState))
    {
      super.onRestoreInstanceState(paramParcelable);
      return;
    }
    paramParcelable = (SavedState)paramParcelable;
    super.onRestoreInstanceState(paramParcelable.getSuperState());
    if (paramParcelable.a) {
      b();
    }
    for (;;)
    {
      this.r = paramParcelable.a;
      return;
      c();
    }
  }
  
  protected Parcelable onSaveInstanceState()
  {
    SavedState localSavedState = new SavedState(super.onSaveInstanceState());
    if (e()) {}
    for (boolean bool = d();; bool = this.r)
    {
      localSavedState.a = bool;
      return localSavedState;
    }
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    if (paramInt1 != paramInt3) {
      this.s = true;
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    boolean bool1;
    if (!this.g) {
      bool1 = super.onTouchEvent(paramMotionEvent);
    }
    float f1;
    float f2;
    do
    {
      int i1;
      boolean bool2;
      float f3;
      float f4;
      do
      {
        do
        {
          return bool1;
          this.q.b(paramMotionEvent);
          i1 = paramMotionEvent.getAction();
          bool2 = true;
          switch (i1 & 0xFF)
          {
          default: 
            return true;
          case 0: 
            f1 = paramMotionEvent.getX();
            f2 = paramMotionEvent.getY();
            this.n = f1;
            this.o = f2;
            return true;
          }
          bool1 = bool2;
        } while (!e(this.h));
        f1 = paramMotionEvent.getX();
        f2 = paramMotionEvent.getY();
        f3 = f1 - this.n;
        f4 = f2 - this.o;
        i1 = this.q.d();
        bool1 = bool2;
      } while (f3 * f3 + f4 * f4 >= i1 * i1);
      bool1 = bool2;
    } while (!this.q.b(this.h, (int)f1, (int)f2));
    a(this.h, 0);
    return true;
  }
  
  public void requestChildFocus(View paramView1, View paramView2)
  {
    super.requestChildFocus(paramView1, paramView2);
    if ((!isInTouchMode()) && (!this.g)) {
      if (paramView1 != this.h) {
        break label36;
      }
    }
    label36:
    for (boolean bool = true;; bool = false)
    {
      this.r = bool;
      return;
    }
  }
  
  public void setCoveredFadeColor(@ColorInt int paramInt)
  {
    this.c = paramInt;
  }
  
  public void setPanelSlideListener(PanelSlideListener paramPanelSlideListener)
  {
    this.p = paramPanelSlideListener;
  }
  
  public void setParallaxDistance(int paramInt)
  {
    this.m = paramInt;
    requestLayout();
  }
  
  @Deprecated
  public void setShadowDrawable(Drawable paramDrawable)
  {
    setShadowDrawableLeft(paramDrawable);
  }
  
  public void setShadowDrawableLeft(Drawable paramDrawable)
  {
    this.d = paramDrawable;
  }
  
  public void setShadowDrawableRight(Drawable paramDrawable)
  {
    this.e = paramDrawable;
  }
  
  @Deprecated
  public void setShadowResource(@DrawableRes int paramInt)
  {
    setShadowDrawable(getResources().getDrawable(paramInt));
  }
  
  public void setShadowResourceLeft(int paramInt)
  {
    setShadowDrawableLeft(getResources().getDrawable(paramInt));
  }
  
  public void setShadowResourceRight(int paramInt)
  {
    setShadowDrawableRight(getResources().getDrawable(paramInt));
  }
  
  public void setSliderFadeColor(@ColorInt int paramInt)
  {
    this.b = paramInt;
  }
  
  class AccessibilityDelegate
    extends AccessibilityDelegateCompat
  {
    private final Rect c = new Rect();
    
    AccessibilityDelegate() {}
    
    private void a(AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat1, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat2)
    {
      Rect localRect = this.c;
      paramAccessibilityNodeInfoCompat2.a(localRect);
      paramAccessibilityNodeInfoCompat1.b(localRect);
      paramAccessibilityNodeInfoCompat2.c(localRect);
      paramAccessibilityNodeInfoCompat1.d(localRect);
      paramAccessibilityNodeInfoCompat1.c(paramAccessibilityNodeInfoCompat2.i());
      paramAccessibilityNodeInfoCompat1.a(paramAccessibilityNodeInfoCompat2.q());
      paramAccessibilityNodeInfoCompat1.b(paramAccessibilityNodeInfoCompat2.r());
      paramAccessibilityNodeInfoCompat1.d(paramAccessibilityNodeInfoCompat2.t());
      paramAccessibilityNodeInfoCompat1.h(paramAccessibilityNodeInfoCompat2.n());
      paramAccessibilityNodeInfoCompat1.f(paramAccessibilityNodeInfoCompat2.l());
      paramAccessibilityNodeInfoCompat1.a(paramAccessibilityNodeInfoCompat2.g());
      paramAccessibilityNodeInfoCompat1.b(paramAccessibilityNodeInfoCompat2.h());
      paramAccessibilityNodeInfoCompat1.d(paramAccessibilityNodeInfoCompat2.j());
      paramAccessibilityNodeInfoCompat1.e(paramAccessibilityNodeInfoCompat2.k());
      paramAccessibilityNodeInfoCompat1.g(paramAccessibilityNodeInfoCompat2.m());
      paramAccessibilityNodeInfoCompat1.a(paramAccessibilityNodeInfoCompat2.c());
      paramAccessibilityNodeInfoCompat1.b(paramAccessibilityNodeInfoCompat2.d());
    }
    
    public void a(View paramView, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat)
    {
      AccessibilityNodeInfoCompat localAccessibilityNodeInfoCompat = AccessibilityNodeInfoCompat.a(paramAccessibilityNodeInfoCompat);
      super.a(paramView, localAccessibilityNodeInfoCompat);
      a(paramAccessibilityNodeInfoCompat, localAccessibilityNodeInfoCompat);
      localAccessibilityNodeInfoCompat.u();
      paramAccessibilityNodeInfoCompat.b(SlidingPaneLayout.class.getName());
      paramAccessibilityNodeInfoCompat.b(paramView);
      paramView = ViewCompat.i(paramView);
      if ((paramView instanceof View)) {
        paramAccessibilityNodeInfoCompat.d((View)paramView);
      }
      int j = SlidingPaneLayout.this.getChildCount();
      int i = 0;
      while (i < j)
      {
        paramView = SlidingPaneLayout.this.getChildAt(i);
        if ((!b(paramView)) && (paramView.getVisibility() == 0))
        {
          ViewCompat.c(paramView, 1);
          paramAccessibilityNodeInfoCompat.c(paramView);
        }
        i += 1;
      }
    }
    
    public void a(View paramView, AccessibilityEvent paramAccessibilityEvent)
    {
      super.a(paramView, paramAccessibilityEvent);
      paramAccessibilityEvent.setClassName(SlidingPaneLayout.class.getName());
    }
    
    public boolean a(ViewGroup paramViewGroup, View paramView, AccessibilityEvent paramAccessibilityEvent)
    {
      if (!b(paramView)) {
        return super.a(paramViewGroup, paramView, paramAccessibilityEvent);
      }
      return false;
    }
    
    public boolean b(View paramView)
    {
      return SlidingPaneLayout.this.e(paramView);
    }
  }
  
  private class DisableLayerRunnable
    implements Runnable
  {
    final View a;
    
    DisableLayerRunnable(View paramView)
    {
      this.a = paramView;
    }
    
    public void run()
    {
      if (this.a.getParent() == SlidingPaneLayout.this)
      {
        ViewCompat.a(this.a, 0, null);
        SlidingPaneLayout.a(SlidingPaneLayout.this, this.a);
      }
      SlidingPaneLayout.g(SlidingPaneLayout.this).remove(this);
    }
  }
  
  private class DragHelperCallback
    extends ViewDragHelper.Callback
  {
    private DragHelperCallback() {}
    
    public int a(View paramView, int paramInt1, int paramInt2)
    {
      return paramView.getTop();
    }
    
    public void a(int paramInt)
    {
      if (SlidingPaneLayout.b(SlidingPaneLayout.this).a() == 0)
      {
        if (SlidingPaneLayout.c(SlidingPaneLayout.this) == 0.0F)
        {
          SlidingPaneLayout.this.d(SlidingPaneLayout.d(SlidingPaneLayout.this));
          SlidingPaneLayout.this.c(SlidingPaneLayout.d(SlidingPaneLayout.this));
          SlidingPaneLayout.a(SlidingPaneLayout.this, false);
        }
      }
      else {
        return;
      }
      SlidingPaneLayout.this.b(SlidingPaneLayout.d(SlidingPaneLayout.this));
      SlidingPaneLayout.a(SlidingPaneLayout.this, true);
    }
    
    public void a(View paramView, float paramFloat1, float paramFloat2)
    {
      SlidingPaneLayout.LayoutParams localLayoutParams = (SlidingPaneLayout.LayoutParams)paramView.getLayoutParams();
      int j;
      int i;
      if (SlidingPaneLayout.e(SlidingPaneLayout.this))
      {
        j = SlidingPaneLayout.this.getPaddingRight() + localLayoutParams.rightMargin;
        if (paramFloat1 >= 0.0F)
        {
          i = j;
          if (paramFloat1 == 0.0F)
          {
            i = j;
            if (SlidingPaneLayout.c(SlidingPaneLayout.this) <= 0.5F) {}
          }
        }
        else
        {
          i = j + SlidingPaneLayout.f(SlidingPaneLayout.this);
        }
        j = SlidingPaneLayout.d(SlidingPaneLayout.this).getWidth();
        i = SlidingPaneLayout.this.getWidth() - i - j;
      }
      for (;;)
      {
        SlidingPaneLayout.b(SlidingPaneLayout.this).a(i, paramView.getTop());
        SlidingPaneLayout.this.invalidate();
        return;
        j = SlidingPaneLayout.this.getPaddingLeft() + localLayoutParams.leftMargin;
        if (paramFloat1 <= 0.0F)
        {
          i = j;
          if (paramFloat1 == 0.0F)
          {
            i = j;
            if (SlidingPaneLayout.c(SlidingPaneLayout.this) <= 0.5F) {}
          }
        }
        else
        {
          i = j + SlidingPaneLayout.f(SlidingPaneLayout.this);
        }
      }
    }
    
    public void a(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      SlidingPaneLayout.a(SlidingPaneLayout.this, paramInt1);
      SlidingPaneLayout.this.invalidate();
    }
    
    public boolean a(View paramView, int paramInt)
    {
      if (SlidingPaneLayout.a(SlidingPaneLayout.this)) {
        return false;
      }
      return ((SlidingPaneLayout.LayoutParams)paramView.getLayoutParams()).b;
    }
    
    public int b(View paramView)
    {
      return SlidingPaneLayout.f(SlidingPaneLayout.this);
    }
    
    public int b(View paramView, int paramInt1, int paramInt2)
    {
      paramView = (SlidingPaneLayout.LayoutParams)SlidingPaneLayout.d(SlidingPaneLayout.this).getLayoutParams();
      if (SlidingPaneLayout.e(SlidingPaneLayout.this))
      {
        paramInt2 = SlidingPaneLayout.this.getWidth() - (SlidingPaneLayout.this.getPaddingRight() + paramView.rightMargin + SlidingPaneLayout.d(SlidingPaneLayout.this).getWidth());
        i = SlidingPaneLayout.f(SlidingPaneLayout.this);
        return Math.max(Math.min(paramInt1, paramInt2), paramInt2 - i);
      }
      paramInt2 = SlidingPaneLayout.this.getPaddingLeft() + paramView.leftMargin;
      int i = SlidingPaneLayout.f(SlidingPaneLayout.this);
      return Math.min(Math.max(paramInt1, paramInt2), paramInt2 + i);
    }
    
    public void b(int paramInt1, int paramInt2)
    {
      SlidingPaneLayout.b(SlidingPaneLayout.this).a(SlidingPaneLayout.d(SlidingPaneLayout.this), paramInt2);
    }
    
    public void b(View paramView, int paramInt)
    {
      SlidingPaneLayout.this.a();
    }
  }
  
  public static class LayoutParams
    extends ViewGroup.MarginLayoutParams
  {
    private static final int[] e = { 16843137 };
    public float a = 0.0F;
    boolean b;
    boolean c;
    Paint d;
    
    public LayoutParams()
    {
      super(-1);
    }
    
    public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
      paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, e);
      this.a = paramContext.getFloat(0, 0.0F);
      paramContext.recycle();
    }
    
    public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
    }
    
    public LayoutParams(ViewGroup.MarginLayoutParams paramMarginLayoutParams)
    {
      super();
    }
  }
  
  public static abstract interface PanelSlideListener
  {
    public abstract void a(View paramView);
    
    public abstract void a(View paramView, float paramFloat);
    
    public abstract void b(View paramView);
  }
  
  static class SavedState
    extends View.BaseSavedState
  {
    public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
    {
      public SlidingPaneLayout.SavedState a(Parcel paramAnonymousParcel)
      {
        return new SlidingPaneLayout.SavedState(paramAnonymousParcel, null);
      }
      
      public SlidingPaneLayout.SavedState[] a(int paramAnonymousInt)
      {
        return new SlidingPaneLayout.SavedState[paramAnonymousInt];
      }
    };
    boolean a;
    
    private SavedState(Parcel paramParcel)
    {
      super();
      if (paramParcel.readInt() != 0) {}
      for (boolean bool = true;; bool = false)
      {
        this.a = bool;
        return;
      }
    }
    
    SavedState(Parcelable paramParcelable)
    {
      super();
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      super.writeToParcel(paramParcel, paramInt);
      if (this.a) {}
      for (paramInt = 1;; paramInt = 0)
      {
        paramParcel.writeInt(paramInt);
        return;
      }
    }
  }
  
  public static class SimplePanelSlideListener
    implements SlidingPaneLayout.PanelSlideListener
  {
    public void a(View paramView) {}
    
    public void a(View paramView, float paramFloat) {}
    
    public void b(View paramView) {}
  }
  
  static abstract interface SlidingPanelLayoutImpl
  {
    public abstract void a(SlidingPaneLayout paramSlidingPaneLayout, View paramView);
  }
  
  static class SlidingPanelLayoutImplBase
    implements SlidingPaneLayout.SlidingPanelLayoutImpl
  {
    public void a(SlidingPaneLayout paramSlidingPaneLayout, View paramView)
    {
      ViewCompat.a(paramSlidingPaneLayout, paramView.getLeft(), paramView.getTop(), paramView.getRight(), paramView.getBottom());
    }
  }
  
  static class SlidingPanelLayoutImplJB
    extends SlidingPaneLayout.SlidingPanelLayoutImplBase
  {
    private Method a;
    private Field b;
    
    SlidingPanelLayoutImplJB()
    {
      try
      {
        this.a = View.class.getDeclaredMethod("getDisplayList", (Class[])null);
      }
      catch (NoSuchMethodException localNoSuchMethodException)
      {
        for (;;)
        {
          try
          {
            this.b = View.class.getDeclaredField("mRecreateDisplayList");
            this.b.setAccessible(true);
            return;
          }
          catch (NoSuchFieldException localNoSuchFieldException)
          {
            Log.e("SlidingPaneLayout", "Couldn't fetch mRecreateDisplayList field; dimming will be slow.", localNoSuchFieldException);
          }
          localNoSuchMethodException = localNoSuchMethodException;
          Log.e("SlidingPaneLayout", "Couldn't fetch getDisplayList method; dimming won't work right.", localNoSuchMethodException);
        }
      }
    }
    
    public void a(SlidingPaneLayout paramSlidingPaneLayout, View paramView)
    {
      if ((this.a != null) && (this.b != null)) {
        try
        {
          this.b.setBoolean(paramView, true);
          this.a.invoke(paramView, (Object[])null);
          super.a(paramSlidingPaneLayout, paramView);
          return;
        }
        catch (Exception localException)
        {
          for (;;)
          {
            Log.e("SlidingPaneLayout", "Error refreshing display list state", localException);
          }
        }
      }
      paramView.invalidate();
    }
  }
  
  static class SlidingPanelLayoutImplJBMR1
    extends SlidingPaneLayout.SlidingPanelLayoutImplBase
  {
    public void a(SlidingPaneLayout paramSlidingPaneLayout, View paramView)
    {
      ViewCompat.a(paramView, ((SlidingPaneLayout.LayoutParams)paramView.getLayoutParams()).d);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/widget/SlidingPaneLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */