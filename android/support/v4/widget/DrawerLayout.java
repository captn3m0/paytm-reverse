package android.support.v4.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.SystemClock;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.AccessibilityDelegateCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.KeyEventCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewGroupCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import java.util.ArrayList;
import java.util.List;

public class DrawerLayout
  extends ViewGroup
  implements DrawerLayoutImpl
{
  static final DrawerLayoutCompatImpl a;
  private static final int[] b;
  private static final boolean c;
  private static final boolean d;
  private float A;
  private Drawable B;
  private Drawable C;
  private Drawable D;
  private CharSequence E;
  private CharSequence F;
  private Object G;
  private boolean H;
  private Drawable I = null;
  private Drawable J = null;
  private Drawable K = null;
  private Drawable L = null;
  private final ArrayList<View> M;
  private final ChildAccessibilityDelegate e = new ChildAccessibilityDelegate();
  private float f;
  private int g;
  private int h = -1728053248;
  private float i;
  private Paint j = new Paint();
  private final ViewDragHelper k;
  private final ViewDragHelper l;
  private final ViewDragCallback m;
  private final ViewDragCallback n;
  private int o;
  private boolean p;
  private boolean q = true;
  private int r = 3;
  private int s = 3;
  private int t = 3;
  private int u = 3;
  private boolean v;
  private boolean w;
  @Deprecated
  @Nullable
  private DrawerListener x;
  private List<DrawerListener> y;
  private float z;
  
  static
  {
    boolean bool2 = true;
    b = new int[] { 16842931 };
    if (Build.VERSION.SDK_INT >= 19)
    {
      bool1 = true;
      c = bool1;
      if (Build.VERSION.SDK_INT < 21) {
        break label65;
      }
    }
    label65:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      d = bool1;
      if (Build.VERSION.SDK_INT < 21) {
        break label70;
      }
      a = new DrawerLayoutCompatImplApi21();
      return;
      bool1 = false;
      break;
    }
    label70:
    a = new DrawerLayoutCompatImplBase();
  }
  
  public DrawerLayout(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public DrawerLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public DrawerLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    setDescendantFocusability(262144);
    float f1 = getResources().getDisplayMetrics().density;
    this.g = ((int)(64.0F * f1 + 0.5F));
    float f2 = 400.0F * f1;
    this.m = new ViewDragCallback(3);
    this.n = new ViewDragCallback(5);
    this.k = ViewDragHelper.a(this, 1.0F, this.m);
    this.k.a(1);
    this.k.a(f2);
    this.m.a(this.k);
    this.l = ViewDragHelper.a(this, 1.0F, this.n);
    this.l.a(2);
    this.l.a(f2);
    this.n.a(this.l);
    setFocusableInTouchMode(true);
    ViewCompat.c(this, 1);
    ViewCompat.a(this, new AccessibilityDelegate());
    ViewGroupCompat.a(this, false);
    if (ViewCompat.x(this))
    {
      a.a(this);
      this.B = a.a(paramContext);
    }
    this.f = (10.0F * f1);
    this.M = new ArrayList();
  }
  
  private void a(View paramView, boolean paramBoolean)
  {
    int i2 = getChildCount();
    int i1 = 0;
    if (i1 < i2)
    {
      View localView = getChildAt(i1);
      if (((!paramBoolean) && (!g(localView))) || ((paramBoolean) && (localView == paramView))) {
        ViewCompat.c(localView, 1);
      }
      for (;;)
      {
        i1 += 1;
        break;
        ViewCompat.c(localView, 4);
      }
    }
  }
  
  private boolean a(Drawable paramDrawable, int paramInt)
  {
    if ((paramDrawable == null) || (!DrawableCompat.b(paramDrawable))) {
      return false;
    }
    DrawableCompat.b(paramDrawable, paramInt);
    return true;
  }
  
  static String d(int paramInt)
  {
    if ((paramInt & 0x3) == 3) {
      return "LEFT";
    }
    if ((paramInt & 0x5) == 5) {
      return "RIGHT";
    }
    return Integer.toHexString(paramInt);
  }
  
  private void f()
  {
    if (d) {
      return;
    }
    this.C = g();
    this.D = h();
  }
  
  private Drawable g()
  {
    int i1 = ViewCompat.h(this);
    if (i1 == 0)
    {
      if (this.I != null)
      {
        a(this.I, i1);
        return this.I;
      }
    }
    else if (this.J != null)
    {
      a(this.J, i1);
      return this.J;
    }
    return this.K;
  }
  
  private Drawable h()
  {
    int i1 = ViewCompat.h(this);
    if (i1 == 0)
    {
      if (this.J != null)
      {
        a(this.J, i1);
        return this.J;
      }
    }
    else if (this.I != null)
    {
      a(this.I, i1);
      return this.I;
    }
    return this.L;
  }
  
  private boolean i()
  {
    int i2 = getChildCount();
    int i1 = 0;
    while (i1 < i2)
    {
      if (LayoutParams.c((LayoutParams)getChildAt(i1).getLayoutParams())) {
        return true;
      }
      i1 += 1;
    }
    return false;
  }
  
  private boolean j()
  {
    return k() != null;
  }
  
  private View k()
  {
    int i2 = getChildCount();
    int i1 = 0;
    while (i1 < i2)
    {
      View localView = getChildAt(i1);
      if ((g(localView)) && (k(localView))) {
        return localView;
      }
      i1 += 1;
    }
    return null;
  }
  
  private static boolean m(View paramView)
  {
    boolean bool2 = false;
    paramView = paramView.getBackground();
    boolean bool1 = bool2;
    if (paramView != null)
    {
      bool1 = bool2;
      if (paramView.getOpacity() == -1) {
        bool1 = true;
      }
    }
    return bool1;
  }
  
  private static boolean n(View paramView)
  {
    return (ViewCompat.e(paramView) != 4) && (ViewCompat.e(paramView) != 2);
  }
  
  public int a(int paramInt)
  {
    int i1 = ViewCompat.h(this);
    switch (paramInt)
    {
    }
    for (;;)
    {
      return 0;
      if (this.r != 3) {
        return this.r;
      }
      if (i1 == 0) {}
      for (paramInt = this.t; paramInt != 3; paramInt = this.u) {
        return paramInt;
      }
      if (this.s != 3) {
        return this.s;
      }
      if (i1 == 0) {}
      for (paramInt = this.u; paramInt != 3; paramInt = this.t) {
        return paramInt;
      }
      if (this.t != 3) {
        return this.t;
      }
      if (i1 == 0) {}
      for (paramInt = this.r; paramInt != 3; paramInt = this.s) {
        return paramInt;
      }
      if (this.u != 3) {
        return this.u;
      }
      if (i1 == 0) {}
      for (paramInt = this.s; paramInt != 3; paramInt = this.r) {
        return paramInt;
      }
    }
  }
  
  public int a(View paramView)
  {
    if (!g(paramView)) {
      throw new IllegalArgumentException("View " + paramView + " is not a drawer");
    }
    return a(((LayoutParams)paramView.getLayoutParams()).a);
  }
  
  View a()
  {
    int i2 = getChildCount();
    int i1 = 0;
    while (i1 < i2)
    {
      View localView = getChildAt(i1);
      if ((LayoutParams.b((LayoutParams)localView.getLayoutParams()) & 0x1) == 1) {
        return localView;
      }
      i1 += 1;
    }
    return null;
  }
  
  void a(int paramInt1, int paramInt2, View paramView)
  {
    paramInt1 = this.k.a();
    int i1 = this.l.a();
    LayoutParams localLayoutParams;
    if ((paramInt1 == 1) || (i1 == 1))
    {
      paramInt1 = 1;
      if ((paramView != null) && (paramInt2 == 0))
      {
        localLayoutParams = (LayoutParams)paramView.getLayoutParams();
        if (LayoutParams.a(localLayoutParams) != 0.0F) {
          break label145;
        }
        b(paramView);
      }
    }
    for (;;)
    {
      if (paramInt1 == this.o) {
        return;
      }
      this.o = paramInt1;
      if (this.y == null) {
        return;
      }
      paramInt2 = this.y.size() - 1;
      while (paramInt2 >= 0)
      {
        ((DrawerListener)this.y.get(paramInt2)).onDrawerStateChanged(paramInt1);
        paramInt2 -= 1;
      }
      if ((paramInt1 == 2) || (i1 == 2))
      {
        paramInt1 = 2;
        break;
      }
      paramInt1 = 0;
      break;
      label145:
      if (LayoutParams.a(localLayoutParams) == 1.0F) {
        c(paramView);
      }
    }
  }
  
  public void a(@NonNull DrawerListener paramDrawerListener)
  {
    if (paramDrawerListener == null) {
      return;
    }
    if (this.y == null) {
      this.y = new ArrayList();
    }
    this.y.add(paramDrawerListener);
  }
  
  void a(View paramView, float paramFloat)
  {
    if (this.y != null)
    {
      int i1 = this.y.size() - 1;
      while (i1 >= 0)
      {
        ((DrawerListener)this.y.get(i1)).onDrawerSlide(paramView, paramFloat);
        i1 -= 1;
      }
    }
  }
  
  void a(boolean paramBoolean)
  {
    int i1 = 0;
    int i5 = getChildCount();
    int i2 = 0;
    while (i2 < i5)
    {
      View localView = getChildAt(i2);
      LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
      int i3 = i1;
      if (g(localView))
      {
        if ((paramBoolean) && (!LayoutParams.c(localLayoutParams))) {
          i3 = i1;
        }
      }
      else
      {
        i2 += 1;
        i1 = i3;
        continue;
      }
      int i4 = localView.getWidth();
      if (a(localView, 3)) {
        i1 |= this.k.a(localView, -i4, localView.getTop());
      }
      for (;;)
      {
        LayoutParams.a(localLayoutParams, false);
        i4 = i1;
        break;
        i1 |= this.l.a(localView, getWidth(), localView.getTop());
      }
    }
    this.m.a();
    this.n.a();
    if (i1 != 0) {
      invalidate();
    }
  }
  
  boolean a(View paramView, int paramInt)
  {
    return (e(paramView) & paramInt) == paramInt;
  }
  
  public void addFocusables(ArrayList<View> paramArrayList, int paramInt1, int paramInt2)
  {
    if (getDescendantFocusability() == 393216) {
      return;
    }
    int i3 = getChildCount();
    int i2 = 0;
    int i1 = 0;
    View localView;
    if (i1 < i3)
    {
      localView = getChildAt(i1);
      if (g(localView)) {
        if (j(localView))
        {
          i2 = 1;
          localView.addFocusables(paramArrayList, paramInt1, paramInt2);
        }
      }
      for (;;)
      {
        i1 += 1;
        break;
        this.M.add(localView);
      }
    }
    if (i2 == 0)
    {
      i2 = this.M.size();
      i1 = 0;
      while (i1 < i2)
      {
        localView = (View)this.M.get(i1);
        if (localView.getVisibility() == 0) {
          localView.addFocusables(paramArrayList, paramInt1, paramInt2);
        }
        i1 += 1;
      }
    }
    this.M.clear();
  }
  
  public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams)
  {
    super.addView(paramView, paramInt, paramLayoutParams);
    if ((a() != null) || (g(paramView))) {
      ViewCompat.c(paramView, 4);
    }
    for (;;)
    {
      if (!c) {
        ViewCompat.a(paramView, this.e);
      }
      return;
      ViewCompat.c(paramView, 1);
    }
  }
  
  @Nullable
  public CharSequence b(int paramInt)
  {
    paramInt = GravityCompat.a(paramInt, ViewCompat.h(this));
    if (paramInt == 3) {
      return this.E;
    }
    if (paramInt == 5) {
      return this.F;
    }
    return null;
  }
  
  public void b()
  {
    a(false);
  }
  
  public void b(@NonNull DrawerListener paramDrawerListener)
  {
    if (paramDrawerListener == null) {}
    while (this.y == null) {
      return;
    }
    this.y.remove(paramDrawerListener);
  }
  
  void b(View paramView)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    if ((LayoutParams.b(localLayoutParams) & 0x1) == 1)
    {
      LayoutParams.a(localLayoutParams, 0);
      if (this.y != null)
      {
        int i1 = this.y.size() - 1;
        while (i1 >= 0)
        {
          ((DrawerListener)this.y.get(i1)).onDrawerClosed(paramView);
          i1 -= 1;
        }
      }
      a(paramView, false);
      if (hasWindowFocus())
      {
        paramView = getRootView();
        if (paramView != null) {
          paramView.sendAccessibilityEvent(32);
        }
      }
    }
  }
  
  void b(View paramView, float paramFloat)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    if (paramFloat == LayoutParams.a(localLayoutParams)) {
      return;
    }
    LayoutParams.a(localLayoutParams, paramFloat);
    a(paramView, paramFloat);
  }
  
  View c(int paramInt)
  {
    int i1 = GravityCompat.a(paramInt, ViewCompat.h(this));
    int i2 = getChildCount();
    paramInt = 0;
    while (paramInt < i2)
    {
      View localView = getChildAt(paramInt);
      if ((e(localView) & 0x7) == (i1 & 0x7)) {
        return localView;
      }
      paramInt += 1;
    }
    return null;
  }
  
  void c()
  {
    if (!this.w)
    {
      long l1 = SystemClock.uptimeMillis();
      MotionEvent localMotionEvent = MotionEvent.obtain(l1, l1, 3, 0.0F, 0.0F, 0);
      int i2 = getChildCount();
      int i1 = 0;
      while (i1 < i2)
      {
        getChildAt(i1).dispatchTouchEvent(localMotionEvent);
        i1 += 1;
      }
      localMotionEvent.recycle();
      this.w = true;
    }
  }
  
  void c(View paramView)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    if ((LayoutParams.b(localLayoutParams) & 0x1) == 0)
    {
      LayoutParams.a(localLayoutParams, 1);
      if (this.y != null)
      {
        int i1 = this.y.size() - 1;
        while (i1 >= 0)
        {
          ((DrawerListener)this.y.get(i1)).onDrawerOpened(paramView);
          i1 -= 1;
        }
      }
      a(paramView, true);
      if (hasWindowFocus()) {
        sendAccessibilityEvent(32);
      }
      paramView.requestFocus();
    }
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return ((paramLayoutParams instanceof LayoutParams)) && (super.checkLayoutParams(paramLayoutParams));
  }
  
  public void computeScroll()
  {
    int i2 = getChildCount();
    float f1 = 0.0F;
    int i1 = 0;
    while (i1 < i2)
    {
      f1 = Math.max(f1, LayoutParams.a((LayoutParams)getChildAt(i1).getLayoutParams()));
      i1 += 1;
    }
    this.i = f1;
    if ((this.k.a(true) | this.l.a(true))) {
      ViewCompat.d(this);
    }
  }
  
  float d(View paramView)
  {
    return LayoutParams.a((LayoutParams)paramView.getLayoutParams());
  }
  
  protected boolean drawChild(Canvas paramCanvas, View paramView, long paramLong)
  {
    int i7 = getHeight();
    boolean bool1 = f(paramView);
    int i1 = 0;
    int i4 = 0;
    int i2 = getWidth();
    int i8 = paramCanvas.save();
    int i3 = i2;
    if (bool1)
    {
      int i9 = getChildCount();
      i3 = 0;
      i1 = i4;
      if (i3 < i9)
      {
        View localView = getChildAt(i3);
        i4 = i1;
        int i5 = i2;
        if (localView != paramView)
        {
          i4 = i1;
          i5 = i2;
          if (localView.getVisibility() == 0)
          {
            i4 = i1;
            i5 = i2;
            if (m(localView))
            {
              i4 = i1;
              i5 = i2;
              if (g(localView))
              {
                if (localView.getHeight() >= i7) {
                  break label166;
                }
                i5 = i2;
                i4 = i1;
              }
            }
          }
        }
        for (;;)
        {
          i3 += 1;
          i1 = i4;
          i2 = i5;
          break;
          label166:
          int i6;
          if (a(localView, 3))
          {
            i6 = localView.getRight();
            i4 = i1;
            i5 = i2;
            if (i6 > i1)
            {
              i4 = i6;
              i5 = i2;
            }
          }
          else
          {
            i6 = localView.getLeft();
            i4 = i1;
            i5 = i2;
            if (i6 < i2)
            {
              i5 = i6;
              i4 = i1;
            }
          }
        }
      }
      paramCanvas.clipRect(i1, 0, i2, getHeight());
      i3 = i2;
    }
    boolean bool2 = super.drawChild(paramCanvas, paramView, paramLong);
    paramCanvas.restoreToCount(i8);
    if ((this.i > 0.0F) && (bool1))
    {
      i2 = (int)(((this.h & 0xFF000000) >>> 24) * this.i);
      i4 = this.h;
      this.j.setColor(i2 << 24 | i4 & 0xFFFFFF);
      paramCanvas.drawRect(i1, 0.0F, i3, getHeight(), this.j);
    }
    do
    {
      return bool2;
      if ((this.C != null) && (a(paramView, 3)))
      {
        i1 = this.C.getIntrinsicWidth();
        i2 = paramView.getRight();
        i3 = this.k.b();
        f1 = Math.max(0.0F, Math.min(i2 / i3, 1.0F));
        this.C.setBounds(i2, paramView.getTop(), i2 + i1, paramView.getBottom());
        this.C.setAlpha((int)(255.0F * f1));
        this.C.draw(paramCanvas);
        return bool2;
      }
    } while ((this.D == null) || (!a(paramView, 5)));
    i1 = this.D.getIntrinsicWidth();
    i2 = paramView.getLeft();
    i3 = getWidth();
    i4 = this.l.b();
    float f1 = Math.max(0.0F, Math.min((i3 - i2) / i4, 1.0F));
    this.D.setBounds(i2 - i1, paramView.getTop(), i2, paramView.getBottom());
    this.D.setAlpha((int)(255.0F * f1));
    this.D.draw(paramCanvas);
    return bool2;
  }
  
  int e(View paramView)
  {
    return GravityCompat.a(((LayoutParams)paramView.getLayoutParams()).a, ViewCompat.h(this));
  }
  
  public void e(int paramInt)
  {
    View localView = c(paramInt);
    if (localView == null) {
      throw new IllegalArgumentException("No drawer view found with gravity " + d(paramInt));
    }
    h(localView);
  }
  
  public void f(int paramInt)
  {
    View localView = c(paramInt);
    if (localView == null) {
      throw new IllegalArgumentException("No drawer view found with gravity " + d(paramInt));
    }
    i(localView);
  }
  
  boolean f(View paramView)
  {
    return ((LayoutParams)paramView.getLayoutParams()).a == 0;
  }
  
  public boolean g(int paramInt)
  {
    View localView = c(paramInt);
    if (localView != null) {
      return j(localView);
    }
    return false;
  }
  
  boolean g(View paramView)
  {
    int i1 = GravityCompat.a(((LayoutParams)paramView.getLayoutParams()).a, ViewCompat.h(paramView));
    if ((i1 & 0x3) != 0) {
      return true;
    }
    return (i1 & 0x5) != 0;
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams()
  {
    return new LayoutParams(-1, -1);
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
  {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    if ((paramLayoutParams instanceof LayoutParams)) {
      return new LayoutParams((LayoutParams)paramLayoutParams);
    }
    if ((paramLayoutParams instanceof ViewGroup.MarginLayoutParams)) {
      return new LayoutParams((ViewGroup.MarginLayoutParams)paramLayoutParams);
    }
    return new LayoutParams(paramLayoutParams);
  }
  
  public float getDrawerElevation()
  {
    if (d) {
      return this.f;
    }
    return 0.0F;
  }
  
  public Drawable getStatusBarBackgroundDrawable()
  {
    return this.B;
  }
  
  public void h(View paramView)
  {
    if (!g(paramView)) {
      throw new IllegalArgumentException("View " + paramView + " is not a sliding drawer");
    }
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    if (this.q)
    {
      LayoutParams.a(localLayoutParams, 1.0F);
      LayoutParams.a(localLayoutParams, 1);
      a(paramView, true);
    }
    for (;;)
    {
      invalidate();
      return;
      LayoutParams.b(localLayoutParams, 2);
      if (a(paramView, 3)) {
        this.k.a(paramView, 0, paramView.getTop());
      } else {
        this.l.a(paramView, getWidth() - paramView.getWidth(), paramView.getTop());
      }
    }
  }
  
  public boolean h(int paramInt)
  {
    View localView = c(paramInt);
    if (localView != null) {
      return k(localView);
    }
    return false;
  }
  
  public void i(View paramView)
  {
    if (!g(paramView)) {
      throw new IllegalArgumentException("View " + paramView + " is not a sliding drawer");
    }
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    if (this.q)
    {
      LayoutParams.a(localLayoutParams, 0.0F);
      LayoutParams.a(localLayoutParams, 0);
    }
    for (;;)
    {
      invalidate();
      return;
      LayoutParams.b(localLayoutParams, 4);
      if (a(paramView, 3)) {
        this.k.a(paramView, -paramView.getWidth(), paramView.getTop());
      } else {
        this.l.a(paramView, getWidth(), paramView.getTop());
      }
    }
  }
  
  public boolean j(View paramView)
  {
    if (!g(paramView)) {
      throw new IllegalArgumentException("View " + paramView + " is not a drawer");
    }
    return (LayoutParams.b((LayoutParams)paramView.getLayoutParams()) & 0x1) == 1;
  }
  
  public boolean k(View paramView)
  {
    if (!g(paramView)) {
      throw new IllegalArgumentException("View " + paramView + " is not a drawer");
    }
    return LayoutParams.a((LayoutParams)paramView.getLayoutParams()) > 0.0F;
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    this.q = true;
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    this.q = true;
  }
  
  public void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    if ((this.H) && (this.B != null))
    {
      int i1 = a.a(this.G);
      if (i1 > 0)
      {
        this.B.setBounds(0, 0, getWidth(), i1);
        this.B.draw(paramCanvas);
      }
    }
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    boolean bool1 = false;
    int i1 = MotionEventCompat.a(paramMotionEvent);
    boolean bool2 = this.k.a(paramMotionEvent);
    boolean bool3 = this.l.a(paramMotionEvent);
    int i3 = 0;
    int i2 = 0;
    switch (i1)
    {
    default: 
      i1 = i2;
    }
    for (;;)
    {
      if (((bool2 | bool3)) || (i1 != 0) || (i()) || (this.w)) {
        bool1 = true;
      }
      return bool1;
      float f1 = paramMotionEvent.getX();
      float f2 = paramMotionEvent.getY();
      this.z = f1;
      this.A = f2;
      i1 = i3;
      if (this.i > 0.0F)
      {
        paramMotionEvent = this.k.d((int)f1, (int)f2);
        i1 = i3;
        if (paramMotionEvent != null)
        {
          i1 = i3;
          if (f(paramMotionEvent)) {
            i1 = 1;
          }
        }
      }
      this.v = false;
      this.w = false;
      continue;
      i1 = i2;
      if (this.k.d(3))
      {
        this.m.a();
        this.n.a();
        i1 = i2;
        continue;
        a(true);
        this.v = false;
        this.w = false;
        i1 = i2;
      }
    }
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) && (j()))
    {
      KeyEventCompat.b(paramKeyEvent);
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      paramKeyEvent = k();
      if ((paramKeyEvent != null) && (a(paramKeyEvent) == 0)) {
        b();
      }
      return paramKeyEvent != null;
    }
    return super.onKeyUp(paramInt, paramKeyEvent);
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.p = true;
    int i4 = paramInt3 - paramInt1;
    int i5 = getChildCount();
    paramInt3 = 0;
    if (paramInt3 < i5)
    {
      View localView = getChildAt(paramInt3);
      if (localView.getVisibility() == 8) {}
      LayoutParams localLayoutParams;
      for (;;)
      {
        paramInt3 += 1;
        break;
        localLayoutParams = (LayoutParams)localView.getLayoutParams();
        if (!f(localView)) {
          break label113;
        }
        localView.layout(localLayoutParams.leftMargin, localLayoutParams.topMargin, localLayoutParams.leftMargin + localView.getMeasuredWidth(), localLayoutParams.topMargin + localView.getMeasuredHeight());
      }
      label113:
      int i6 = localView.getMeasuredWidth();
      int i7 = localView.getMeasuredHeight();
      int i1;
      float f1;
      label165:
      int i2;
      if (a(localView, 3))
      {
        i1 = -i6 + (int)(i6 * LayoutParams.a(localLayoutParams));
        f1 = (i6 + i1) / i6;
        if (f1 == LayoutParams.a(localLayoutParams)) {
          break label310;
        }
        i2 = 1;
        label179:
        switch (localLayoutParams.a & 0x70)
        {
        default: 
          localView.layout(i1, localLayoutParams.topMargin, i1 + i6, localLayoutParams.topMargin + i7);
          label237:
          if (i2 != 0) {
            b(localView, f1);
          }
          if (LayoutParams.a(localLayoutParams) <= 0.0F) {
            break;
          }
        }
      }
      for (paramInt1 = 0; localView.getVisibility() != paramInt1; paramInt1 = 4)
      {
        localView.setVisibility(paramInt1);
        break;
        i1 = i4 - (int)(i6 * LayoutParams.a(localLayoutParams));
        f1 = (i4 - i1) / i6;
        break label165;
        label310:
        i2 = 0;
        break label179;
        paramInt1 = paramInt4 - paramInt2;
        localView.layout(i1, paramInt1 - localLayoutParams.bottomMargin - localView.getMeasuredHeight(), i1 + i6, paramInt1 - localLayoutParams.bottomMargin);
        break label237;
        int i8 = paramInt4 - paramInt2;
        int i3 = (i8 - i7) / 2;
        if (i3 < localLayoutParams.topMargin) {
          paramInt1 = localLayoutParams.topMargin;
        }
        for (;;)
        {
          localView.layout(i1, paramInt1, i1 + i6, paramInt1 + i7);
          break;
          paramInt1 = i3;
          if (i3 + i7 > i8 - localLayoutParams.bottomMargin) {
            paramInt1 = i8 - localLayoutParams.bottomMargin - i7;
          }
        }
      }
    }
    this.p = false;
    this.q = false;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i6 = View.MeasureSpec.getMode(paramInt1);
    int i5 = View.MeasureSpec.getMode(paramInt2);
    int i1 = View.MeasureSpec.getSize(paramInt1);
    int i2 = View.MeasureSpec.getSize(paramInt2);
    int i3;
    int i4;
    if (i6 == 1073741824)
    {
      i3 = i2;
      i4 = i1;
      if (i5 == 1073741824) {}
    }
    else
    {
      if (!isInEditMode()) {
        break label190;
      }
      if (i6 != Integer.MIN_VALUE) {
        break label155;
      }
      if (i5 != Integer.MIN_VALUE) {
        break label167;
      }
      i4 = i1;
      i3 = i2;
    }
    label76:
    setMeasuredDimension(i4, i3);
    if ((this.G != null) && (ViewCompat.x(this))) {}
    int i8;
    View localView;
    for (i5 = 1;; i5 = 0)
    {
      i8 = ViewCompat.h(this);
      i2 = 0;
      i1 = 0;
      int i9 = getChildCount();
      i6 = 0;
      for (;;)
      {
        if (i6 >= i9) {
          return;
        }
        localView = getChildAt(i6);
        if (localView.getVisibility() != 8) {
          break;
        }
        i6 += 1;
      }
      label155:
      if (i6 != 0) {
        break;
      }
      i1 = 300;
      break;
      label167:
      i3 = i2;
      i4 = i1;
      if (i5 != 0) {
        break label76;
      }
      i3 = 300;
      i4 = i1;
      break label76;
      label190:
      throw new IllegalArgumentException("DrawerLayout must be measured with MeasureSpec.EXACTLY.");
    }
    LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
    int i7;
    if (i5 != 0)
    {
      i7 = GravityCompat.a(localLayoutParams.a, i8);
      if (!ViewCompat.x(localView)) {
        break label315;
      }
      a.a(localView, this.G, i7);
    }
    for (;;)
    {
      if (!f(localView)) {
        break label334;
      }
      localView.measure(View.MeasureSpec.makeMeasureSpec(i4 - localLayoutParams.leftMargin - localLayoutParams.rightMargin, 1073741824), View.MeasureSpec.makeMeasureSpec(i3 - localLayoutParams.topMargin - localLayoutParams.bottomMargin, 1073741824));
      break;
      label315:
      a.a(localLayoutParams, this.G, i7);
    }
    label334:
    if (g(localView))
    {
      if ((d) && (ViewCompat.u(localView) != this.f)) {
        ViewCompat.f(localView, this.f);
      }
      int i10 = e(localView) & 0x7;
      if (i10 == 3) {}
      for (i7 = 1; ((i7 != 0) && (i2 != 0)) || ((i7 == 0) && (i1 != 0)); i7 = 0) {
        throw new IllegalStateException("Child drawer has absolute gravity " + d(i10) + " but this " + "DrawerLayout" + " already has a " + "drawer view along that edge");
      }
      if (i7 != 0) {
        i2 = 1;
      }
      for (;;)
      {
        localView.measure(getChildMeasureSpec(paramInt1, this.g + localLayoutParams.leftMargin + localLayoutParams.rightMargin, localLayoutParams.width), getChildMeasureSpec(paramInt2, localLayoutParams.topMargin + localLayoutParams.bottomMargin, localLayoutParams.height));
        break;
        i1 = 1;
      }
    }
    throw new IllegalStateException("Child " + localView + " at index " + i6 + " does not have a valid layout_gravity - must be Gravity.LEFT, " + "Gravity.RIGHT or Gravity.NO_GRAVITY");
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    if (!(paramParcelable instanceof SavedState)) {
      super.onRestoreInstanceState(paramParcelable);
    }
    do
    {
      return;
      paramParcelable = (SavedState)paramParcelable;
      super.onRestoreInstanceState(paramParcelable.getSuperState());
      if (paramParcelable.a != 0)
      {
        View localView = c(paramParcelable.a);
        if (localView != null) {
          h(localView);
        }
      }
      if (paramParcelable.b != 3) {
        setDrawerLockMode(paramParcelable.b, 3);
      }
      if (paramParcelable.c != 3) {
        setDrawerLockMode(paramParcelable.c, 5);
      }
      if (paramParcelable.d != 3) {
        setDrawerLockMode(paramParcelable.d, 8388611);
      }
    } while (paramParcelable.e == 3);
    setDrawerLockMode(paramParcelable.e, 8388613);
  }
  
  public void onRtlPropertiesChanged(int paramInt)
  {
    f();
  }
  
  protected Parcelable onSaveInstanceState()
  {
    SavedState localSavedState = new SavedState(super.onSaveInstanceState());
    int i4 = getChildCount();
    int i1 = 0;
    for (;;)
    {
      LayoutParams localLayoutParams;
      int i2;
      if (i1 < i4)
      {
        localLayoutParams = (LayoutParams)getChildAt(i1).getLayoutParams();
        if (LayoutParams.b(localLayoutParams) != 1) {
          break label119;
        }
        i2 = 1;
        if (LayoutParams.b(localLayoutParams) != 2) {
          break label124;
        }
      }
      label119:
      label124:
      for (int i3 = 1;; i3 = 0)
      {
        if ((i2 == 0) && (i3 == 0)) {
          break label129;
        }
        localSavedState.a = localLayoutParams.a;
        localSavedState.b = this.r;
        localSavedState.c = this.s;
        localSavedState.d = this.t;
        localSavedState.e = this.u;
        return localSavedState;
        i2 = 0;
        break;
      }
      label129:
      i1 += 1;
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    this.k.b(paramMotionEvent);
    this.l.b(paramMotionEvent);
    float f1;
    float f2;
    switch (paramMotionEvent.getAction() & 0xFF)
    {
    case 2: 
    default: 
      return true;
    case 0: 
      f1 = paramMotionEvent.getX();
      f2 = paramMotionEvent.getY();
      this.z = f1;
      this.A = f2;
      this.v = false;
      this.w = false;
      return true;
    case 1: 
      f2 = paramMotionEvent.getX();
      f1 = paramMotionEvent.getY();
      boolean bool2 = true;
      paramMotionEvent = this.k.d((int)f2, (int)f1);
      boolean bool1 = bool2;
      if (paramMotionEvent != null)
      {
        bool1 = bool2;
        if (f(paramMotionEvent))
        {
          f2 -= this.z;
          f1 -= this.A;
          int i1 = this.k.d();
          bool1 = bool2;
          if (f2 * f2 + f1 * f1 < i1 * i1)
          {
            paramMotionEvent = a();
            bool1 = bool2;
            if (paramMotionEvent != null) {
              if (a(paramMotionEvent) != 2) {
                break label217;
              }
            }
          }
        }
      }
      label217:
      for (bool1 = true;; bool1 = false)
      {
        a(bool1);
        this.v = false;
        return true;
      }
    }
    a(true);
    this.v = false;
    this.w = false;
    return true;
  }
  
  public void requestDisallowInterceptTouchEvent(boolean paramBoolean)
  {
    super.requestDisallowInterceptTouchEvent(paramBoolean);
    this.v = paramBoolean;
    if (paramBoolean) {
      a(true);
    }
  }
  
  public void requestLayout()
  {
    if (!this.p) {
      super.requestLayout();
    }
  }
  
  public void setChildInsets(Object paramObject, boolean paramBoolean)
  {
    this.G = paramObject;
    this.H = paramBoolean;
    if ((!paramBoolean) && (getBackground() == null)) {}
    for (paramBoolean = true;; paramBoolean = false)
    {
      setWillNotDraw(paramBoolean);
      requestLayout();
      return;
    }
  }
  
  public void setDrawerElevation(float paramFloat)
  {
    this.f = paramFloat;
    int i1 = 0;
    while (i1 < getChildCount())
    {
      View localView = getChildAt(i1);
      if (g(localView)) {
        ViewCompat.f(localView, this.f);
      }
      i1 += 1;
    }
  }
  
  @Deprecated
  public void setDrawerListener(DrawerListener paramDrawerListener)
  {
    if (this.x != null) {
      b(this.x);
    }
    if (paramDrawerListener != null) {
      a(paramDrawerListener);
    }
    this.x = paramDrawerListener;
  }
  
  public void setDrawerLockMode(int paramInt)
  {
    setDrawerLockMode(paramInt, 3);
    setDrawerLockMode(paramInt, 5);
  }
  
  public void setDrawerLockMode(int paramInt1, int paramInt2)
  {
    int i1 = GravityCompat.a(paramInt2, ViewCompat.h(this));
    Object localObject;
    switch (paramInt2)
    {
    default: 
      if (paramInt1 != 0)
      {
        if (i1 == 3)
        {
          localObject = this.k;
          label67:
          ((ViewDragHelper)localObject).cancel();
        }
      }
      else {
        switch (paramInt1)
        {
        }
      }
      break;
    }
    do
    {
      do
      {
        return;
        this.r = paramInt1;
        break;
        this.s = paramInt1;
        break;
        this.t = paramInt1;
        break;
        this.u = paramInt1;
        break;
        localObject = this.l;
        break label67;
        localObject = c(i1);
      } while (localObject == null);
      h((View)localObject);
      return;
      localObject = c(i1);
    } while (localObject == null);
    i((View)localObject);
  }
  
  public void setDrawerLockMode(int paramInt, View paramView)
  {
    if (!g(paramView)) {
      throw new IllegalArgumentException("View " + paramView + " is not a " + "drawer with appropriate layout_gravity");
    }
    setDrawerLockMode(paramInt, ((LayoutParams)paramView.getLayoutParams()).a);
  }
  
  public void setDrawerShadow(@DrawableRes int paramInt1, int paramInt2)
  {
    setDrawerShadow(getResources().getDrawable(paramInt1), paramInt2);
  }
  
  public void setDrawerShadow(Drawable paramDrawable, int paramInt)
  {
    if (d) {
      return;
    }
    if ((paramInt & 0x800003) == 8388611) {
      this.I = paramDrawable;
    }
    for (;;)
    {
      f();
      invalidate();
      return;
      if ((paramInt & 0x800005) == 8388613)
      {
        this.J = paramDrawable;
      }
      else if ((paramInt & 0x3) == 3)
      {
        this.K = paramDrawable;
      }
      else
      {
        if ((paramInt & 0x5) != 5) {
          break;
        }
        this.L = paramDrawable;
      }
    }
  }
  
  public void setDrawerTitle(int paramInt, CharSequence paramCharSequence)
  {
    paramInt = GravityCompat.a(paramInt, ViewCompat.h(this));
    if (paramInt == 3) {
      this.E = paramCharSequence;
    }
    while (paramInt != 5) {
      return;
    }
    this.F = paramCharSequence;
  }
  
  public void setScrimColor(@ColorInt int paramInt)
  {
    this.h = paramInt;
    invalidate();
  }
  
  public void setStatusBarBackground(int paramInt)
  {
    if (paramInt != 0) {}
    for (Drawable localDrawable = ContextCompat.getDrawable(getContext(), paramInt);; localDrawable = null)
    {
      this.B = localDrawable;
      invalidate();
      return;
    }
  }
  
  public void setStatusBarBackground(Drawable paramDrawable)
  {
    this.B = paramDrawable;
    invalidate();
  }
  
  public void setStatusBarBackgroundColor(@ColorInt int paramInt)
  {
    this.B = new ColorDrawable(paramInt);
    invalidate();
  }
  
  class AccessibilityDelegate
    extends AccessibilityDelegateCompat
  {
    private final Rect c = new Rect();
    
    AccessibilityDelegate() {}
    
    private void a(AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat1, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat2)
    {
      Rect localRect = this.c;
      paramAccessibilityNodeInfoCompat2.a(localRect);
      paramAccessibilityNodeInfoCompat1.b(localRect);
      paramAccessibilityNodeInfoCompat2.c(localRect);
      paramAccessibilityNodeInfoCompat1.d(localRect);
      paramAccessibilityNodeInfoCompat1.c(paramAccessibilityNodeInfoCompat2.i());
      paramAccessibilityNodeInfoCompat1.a(paramAccessibilityNodeInfoCompat2.q());
      paramAccessibilityNodeInfoCompat1.b(paramAccessibilityNodeInfoCompat2.r());
      paramAccessibilityNodeInfoCompat1.d(paramAccessibilityNodeInfoCompat2.t());
      paramAccessibilityNodeInfoCompat1.h(paramAccessibilityNodeInfoCompat2.n());
      paramAccessibilityNodeInfoCompat1.f(paramAccessibilityNodeInfoCompat2.l());
      paramAccessibilityNodeInfoCompat1.a(paramAccessibilityNodeInfoCompat2.g());
      paramAccessibilityNodeInfoCompat1.b(paramAccessibilityNodeInfoCompat2.h());
      paramAccessibilityNodeInfoCompat1.d(paramAccessibilityNodeInfoCompat2.j());
      paramAccessibilityNodeInfoCompat1.e(paramAccessibilityNodeInfoCompat2.k());
      paramAccessibilityNodeInfoCompat1.g(paramAccessibilityNodeInfoCompat2.m());
      paramAccessibilityNodeInfoCompat1.a(paramAccessibilityNodeInfoCompat2.c());
    }
    
    private void a(AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat, ViewGroup paramViewGroup)
    {
      int j = paramViewGroup.getChildCount();
      int i = 0;
      while (i < j)
      {
        View localView = paramViewGroup.getChildAt(i);
        if (DrawerLayout.l(localView)) {
          paramAccessibilityNodeInfoCompat.c(localView);
        }
        i += 1;
      }
    }
    
    public void a(View paramView, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat)
    {
      if (DrawerLayout.e()) {
        super.a(paramView, paramAccessibilityNodeInfoCompat);
      }
      for (;;)
      {
        paramAccessibilityNodeInfoCompat.b(DrawerLayout.class.getName());
        paramAccessibilityNodeInfoCompat.a(false);
        paramAccessibilityNodeInfoCompat.b(false);
        paramAccessibilityNodeInfoCompat.a(AccessibilityNodeInfoCompat.AccessibilityActionCompat.a);
        paramAccessibilityNodeInfoCompat.a(AccessibilityNodeInfoCompat.AccessibilityActionCompat.b);
        return;
        AccessibilityNodeInfoCompat localAccessibilityNodeInfoCompat = AccessibilityNodeInfoCompat.a(paramAccessibilityNodeInfoCompat);
        super.a(paramView, localAccessibilityNodeInfoCompat);
        paramAccessibilityNodeInfoCompat.b(paramView);
        ViewParent localViewParent = ViewCompat.i(paramView);
        if ((localViewParent instanceof View)) {
          paramAccessibilityNodeInfoCompat.d((View)localViewParent);
        }
        a(paramAccessibilityNodeInfoCompat, localAccessibilityNodeInfoCompat);
        localAccessibilityNodeInfoCompat.u();
        a(paramAccessibilityNodeInfoCompat, (ViewGroup)paramView);
      }
    }
    
    public void a(View paramView, AccessibilityEvent paramAccessibilityEvent)
    {
      super.a(paramView, paramAccessibilityEvent);
      paramAccessibilityEvent.setClassName(DrawerLayout.class.getName());
    }
    
    public boolean a(ViewGroup paramViewGroup, View paramView, AccessibilityEvent paramAccessibilityEvent)
    {
      if ((DrawerLayout.e()) || (DrawerLayout.l(paramView))) {
        return super.a(paramViewGroup, paramView, paramAccessibilityEvent);
      }
      return false;
    }
    
    public boolean d(View paramView, AccessibilityEvent paramAccessibilityEvent)
    {
      if (paramAccessibilityEvent.getEventType() == 32)
      {
        paramView = paramAccessibilityEvent.getText();
        paramAccessibilityEvent = DrawerLayout.a(DrawerLayout.this);
        if (paramAccessibilityEvent != null)
        {
          int i = DrawerLayout.this.e(paramAccessibilityEvent);
          paramAccessibilityEvent = DrawerLayout.this.b(i);
          if (paramAccessibilityEvent != null) {
            paramView.add(paramAccessibilityEvent);
          }
        }
        return true;
      }
      return super.d(paramView, paramAccessibilityEvent);
    }
  }
  
  final class ChildAccessibilityDelegate
    extends AccessibilityDelegateCompat
  {
    ChildAccessibilityDelegate() {}
    
    public void a(View paramView, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat)
    {
      super.a(paramView, paramAccessibilityNodeInfoCompat);
      if (!DrawerLayout.l(paramView)) {
        paramAccessibilityNodeInfoCompat.d(null);
      }
    }
  }
  
  static abstract interface DrawerLayoutCompatImpl
  {
    public abstract int a(Object paramObject);
    
    public abstract Drawable a(Context paramContext);
    
    public abstract void a(View paramView);
    
    public abstract void a(View paramView, Object paramObject, int paramInt);
    
    public abstract void a(ViewGroup.MarginLayoutParams paramMarginLayoutParams, Object paramObject, int paramInt);
  }
  
  static class DrawerLayoutCompatImplApi21
    implements DrawerLayout.DrawerLayoutCompatImpl
  {
    public int a(Object paramObject)
    {
      return DrawerLayoutCompatApi21.a(paramObject);
    }
    
    public Drawable a(Context paramContext)
    {
      return DrawerLayoutCompatApi21.a(paramContext);
    }
    
    public void a(View paramView)
    {
      DrawerLayoutCompatApi21.a(paramView);
    }
    
    public void a(View paramView, Object paramObject, int paramInt)
    {
      DrawerLayoutCompatApi21.a(paramView, paramObject, paramInt);
    }
    
    public void a(ViewGroup.MarginLayoutParams paramMarginLayoutParams, Object paramObject, int paramInt)
    {
      DrawerLayoutCompatApi21.a(paramMarginLayoutParams, paramObject, paramInt);
    }
  }
  
  static class DrawerLayoutCompatImplBase
    implements DrawerLayout.DrawerLayoutCompatImpl
  {
    public int a(Object paramObject)
    {
      return 0;
    }
    
    public Drawable a(Context paramContext)
    {
      return null;
    }
    
    public void a(View paramView) {}
    
    public void a(View paramView, Object paramObject, int paramInt) {}
    
    public void a(ViewGroup.MarginLayoutParams paramMarginLayoutParams, Object paramObject, int paramInt) {}
  }
  
  public static abstract interface DrawerListener
  {
    public abstract void onDrawerClosed(View paramView);
    
    public abstract void onDrawerOpened(View paramView);
    
    public abstract void onDrawerSlide(View paramView, float paramFloat);
    
    public abstract void onDrawerStateChanged(int paramInt);
  }
  
  public static class LayoutParams
    extends ViewGroup.MarginLayoutParams
  {
    public int a = 0;
    private float b;
    private boolean c;
    private int d;
    
    public LayoutParams(int paramInt1, int paramInt2)
    {
      super(paramInt2);
    }
    
    public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
      paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, DrawerLayout.d());
      this.a = paramContext.getInt(0, 0);
      paramContext.recycle();
    }
    
    public LayoutParams(LayoutParams paramLayoutParams)
    {
      super();
      this.a = paramLayoutParams.a;
    }
    
    public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
    }
    
    public LayoutParams(ViewGroup.MarginLayoutParams paramMarginLayoutParams)
    {
      super();
    }
  }
  
  protected static class SavedState
    extends View.BaseSavedState
  {
    public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
    {
      public DrawerLayout.SavedState a(Parcel paramAnonymousParcel)
      {
        return new DrawerLayout.SavedState(paramAnonymousParcel);
      }
      
      public DrawerLayout.SavedState[] a(int paramAnonymousInt)
      {
        return new DrawerLayout.SavedState[paramAnonymousInt];
      }
    };
    int a = 0;
    int b;
    int c;
    int d;
    int e;
    
    public SavedState(Parcel paramParcel)
    {
      super();
      this.a = paramParcel.readInt();
      this.b = paramParcel.readInt();
      this.c = paramParcel.readInt();
      this.d = paramParcel.readInt();
      this.e = paramParcel.readInt();
    }
    
    public SavedState(Parcelable paramParcelable)
    {
      super();
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      super.writeToParcel(paramParcel, paramInt);
      paramParcel.writeInt(this.a);
      paramParcel.writeInt(this.b);
      paramParcel.writeInt(this.c);
      paramParcel.writeInt(this.d);
      paramParcel.writeInt(this.e);
    }
  }
  
  public static abstract class SimpleDrawerListener
    implements DrawerLayout.DrawerListener
  {}
  
  private class ViewDragCallback
    extends ViewDragHelper.Callback
  {
    private final int b;
    private ViewDragHelper c;
    private final Runnable d = new Runnable()
    {
      public void run()
      {
        DrawerLayout.ViewDragCallback.a(DrawerLayout.ViewDragCallback.this);
      }
    };
    
    public ViewDragCallback(int paramInt)
    {
      this.b = paramInt;
    }
    
    private void b()
    {
      int i = 3;
      if (this.b == 3) {
        i = 5;
      }
      View localView = DrawerLayout.this.c(i);
      if (localView != null) {
        DrawerLayout.this.i(localView);
      }
    }
    
    private void c()
    {
      int j = 0;
      int k = this.c.b();
      int i;
      View localView;
      if (this.b == 3)
      {
        i = 1;
        if (i == 0) {
          break label150;
        }
        localView = DrawerLayout.this.c(3);
        if (localView != null) {
          j = -localView.getWidth();
        }
        j += k;
      }
      for (;;)
      {
        if ((localView != null) && (((i != 0) && (localView.getLeft() < j)) || ((i == 0) && (localView.getLeft() > j) && (DrawerLayout.this.a(localView) == 0))))
        {
          DrawerLayout.LayoutParams localLayoutParams = (DrawerLayout.LayoutParams)localView.getLayoutParams();
          this.c.a(localView, j, localView.getTop());
          DrawerLayout.LayoutParams.a(localLayoutParams, true);
          DrawerLayout.this.invalidate();
          b();
          DrawerLayout.this.c();
        }
        return;
        i = 0;
        break;
        label150:
        localView = DrawerLayout.this.c(5);
        j = DrawerLayout.this.getWidth() - k;
      }
    }
    
    public int a(View paramView, int paramInt1, int paramInt2)
    {
      return paramView.getTop();
    }
    
    public void a()
    {
      DrawerLayout.this.removeCallbacks(this.d);
    }
    
    public void a(int paramInt)
    {
      DrawerLayout.this.a(this.b, paramInt, this.c.c());
    }
    
    public void a(int paramInt1, int paramInt2)
    {
      DrawerLayout.this.postDelayed(this.d, 160L);
    }
    
    public void a(ViewDragHelper paramViewDragHelper)
    {
      this.c = paramViewDragHelper;
    }
    
    public void a(View paramView, float paramFloat1, float paramFloat2)
    {
      paramFloat2 = DrawerLayout.this.d(paramView);
      int j = paramView.getWidth();
      if (DrawerLayout.this.a(paramView, 3))
      {
        if ((paramFloat1 > 0.0F) || ((paramFloat1 == 0.0F) && (paramFloat2 > 0.5F))) {}
        for (i = 0;; i = -j)
        {
          this.c.a(i, paramView.getTop());
          DrawerLayout.this.invalidate();
          return;
        }
      }
      int i = DrawerLayout.this.getWidth();
      if ((paramFloat1 < 0.0F) || ((paramFloat1 == 0.0F) && (paramFloat2 > 0.5F))) {
        i -= j;
      }
      for (;;)
      {
        break;
      }
    }
    
    public void a(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      paramInt2 = paramView.getWidth();
      float f;
      if (DrawerLayout.this.a(paramView, 3))
      {
        f = (paramInt2 + paramInt1) / paramInt2;
        DrawerLayout.this.b(paramView, f);
        if (f != 0.0F) {
          break label76;
        }
      }
      label76:
      for (paramInt1 = 4;; paramInt1 = 0)
      {
        paramView.setVisibility(paramInt1);
        DrawerLayout.this.invalidate();
        return;
        f = (DrawerLayout.this.getWidth() - paramInt1) / paramInt2;
        break;
      }
    }
    
    public boolean a(View paramView, int paramInt)
    {
      return (DrawerLayout.this.g(paramView)) && (DrawerLayout.this.a(paramView, this.b)) && (DrawerLayout.this.a(paramView) == 0);
    }
    
    public int b(View paramView)
    {
      if (DrawerLayout.this.g(paramView)) {
        return paramView.getWidth();
      }
      return 0;
    }
    
    public int b(View paramView, int paramInt1, int paramInt2)
    {
      if (DrawerLayout.this.a(paramView, 3)) {
        return Math.max(-paramView.getWidth(), Math.min(paramInt1, 0));
      }
      paramInt2 = DrawerLayout.this.getWidth();
      return Math.max(paramInt2 - paramView.getWidth(), Math.min(paramInt1, paramInt2));
    }
    
    public void b(int paramInt1, int paramInt2)
    {
      if ((paramInt1 & 0x1) == 1) {}
      for (View localView = DrawerLayout.this.c(3);; localView = DrawerLayout.this.c(5))
      {
        if ((localView != null) && (DrawerLayout.this.a(localView) == 0)) {
          this.c.a(localView, paramInt2);
        }
        return;
      }
    }
    
    public void b(View paramView, int paramInt)
    {
      DrawerLayout.LayoutParams.a((DrawerLayout.LayoutParams)paramView.getLayoutParams(), false);
      b();
    }
    
    public boolean b(int paramInt)
    {
      return false;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/widget/DrawerLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */