package android.support.v4.widget;

import android.os.Build.VERSION;

public final class SearchViewCompat
{
  private static final SearchViewCompatImpl a = new SearchViewCompatStubImpl();
  
  static
  {
    if (Build.VERSION.SDK_INT >= 14)
    {
      a = new SearchViewCompatIcsImpl();
      return;
    }
    if (Build.VERSION.SDK_INT >= 11)
    {
      a = new SearchViewCompatHoneycombImpl();
      return;
    }
  }
  
  public static abstract class OnCloseListenerCompat
  {
    final Object a = SearchViewCompat.a().a(this);
    
    public boolean a()
    {
      return false;
    }
  }
  
  public static abstract class OnQueryTextListenerCompat
  {
    final Object a = SearchViewCompat.a().a(this);
    
    public boolean a(String paramString)
    {
      return false;
    }
    
    public boolean b(String paramString)
    {
      return false;
    }
  }
  
  static class SearchViewCompatHoneycombImpl
    extends SearchViewCompat.SearchViewCompatStubImpl
  {
    public Object a(final SearchViewCompat.OnCloseListenerCompat paramOnCloseListenerCompat)
    {
      SearchViewCompatHoneycomb.a(new SearchViewCompatHoneycomb.OnCloseListenerCompatBridge()
      {
        public boolean a()
        {
          return paramOnCloseListenerCompat.a();
        }
      });
    }
    
    public Object a(final SearchViewCompat.OnQueryTextListenerCompat paramOnQueryTextListenerCompat)
    {
      SearchViewCompatHoneycomb.a(new SearchViewCompatHoneycomb.OnQueryTextListenerCompatBridge()
      {
        public boolean a(String paramAnonymousString)
        {
          return paramOnQueryTextListenerCompat.a(paramAnonymousString);
        }
        
        public boolean b(String paramAnonymousString)
        {
          return paramOnQueryTextListenerCompat.b(paramAnonymousString);
        }
      });
    }
  }
  
  static class SearchViewCompatIcsImpl
    extends SearchViewCompat.SearchViewCompatHoneycombImpl
  {}
  
  static abstract interface SearchViewCompatImpl
  {
    public abstract Object a(SearchViewCompat.OnCloseListenerCompat paramOnCloseListenerCompat);
    
    public abstract Object a(SearchViewCompat.OnQueryTextListenerCompat paramOnQueryTextListenerCompat);
  }
  
  static class SearchViewCompatStubImpl
    implements SearchViewCompat.SearchViewCompatImpl
  {
    public Object a(SearchViewCompat.OnCloseListenerCompat paramOnCloseListenerCompat)
    {
      return null;
    }
    
    public Object a(SearchViewCompat.OnQueryTextListenerCompat paramOnQueryTextListenerCompat)
    {
      return null;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/widget/SearchViewCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */