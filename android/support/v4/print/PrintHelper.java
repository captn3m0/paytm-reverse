package android.support.v4.print;

public final class PrintHelper
{
  public static abstract interface OnPrintFinishCallback
  {
    public abstract void a();
  }
  
  private static final class PrintHelperKitkatImpl
    implements PrintHelper.PrintHelperVersionImpl
  {}
  
  private static final class PrintHelperStubImpl
    implements PrintHelper.PrintHelperVersionImpl
  {
    int a = 2;
    int b = 2;
    int c = 1;
  }
  
  static abstract interface PrintHelperVersionImpl {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/print/PrintHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */