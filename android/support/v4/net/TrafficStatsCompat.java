package android.support.v4.net;

import android.os.Build.VERSION;

public final class TrafficStatsCompat
{
  private static final TrafficStatsCompatImpl a = new BaseTrafficStatsCompatImpl();
  
  static
  {
    if (Build.VERSION.SDK_INT >= 14)
    {
      a = new IcsTrafficStatsCompatImpl();
      return;
    }
  }
  
  static class BaseTrafficStatsCompatImpl
    implements TrafficStatsCompat.TrafficStatsCompatImpl
  {
    private ThreadLocal<SocketTags> a = new ThreadLocal()
    {
      protected TrafficStatsCompat.BaseTrafficStatsCompatImpl.SocketTags a()
      {
        return new TrafficStatsCompat.BaseTrafficStatsCompatImpl.SocketTags(null);
      }
    };
    
    private static class SocketTags
    {
      public int a = -1;
    }
  }
  
  static class IcsTrafficStatsCompatImpl
    implements TrafficStatsCompat.TrafficStatsCompatImpl
  {}
  
  static abstract interface TrafficStatsCompatImpl {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/net/TrafficStatsCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */