package android.support.v4.graphics.drawable;

import android.graphics.drawable.Drawable;

class DrawableCompatApi23
{
  public static int a(Drawable paramDrawable)
  {
    return paramDrawable.getLayoutDirection();
  }
  
  public static void a(Drawable paramDrawable, int paramInt)
  {
    paramDrawable.setLayoutDirection(paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/graphics/drawable/DrawableCompatApi23.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */