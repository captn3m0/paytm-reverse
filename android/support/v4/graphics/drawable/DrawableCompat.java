package android.support.v4.graphics.drawable;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public final class DrawableCompat
{
  static final DrawableImpl a = new BaseDrawableImpl();
  
  static
  {
    int i = Build.VERSION.SDK_INT;
    if (i >= 23)
    {
      a = new MDrawableImpl();
      return;
    }
    if (i >= 21)
    {
      a = new LollipopDrawableImpl();
      return;
    }
    if (i >= 19)
    {
      a = new KitKatDrawableImpl();
      return;
    }
    if (i >= 17)
    {
      a = new JellybeanMr1DrawableImpl();
      return;
    }
    if (i >= 11)
    {
      a = new HoneycombDrawableImpl();
      return;
    }
    if (i >= 5)
    {
      a = new EclairDrawableImpl();
      return;
    }
  }
  
  public static void a(@NonNull Drawable paramDrawable)
  {
    a.a(paramDrawable);
  }
  
  public static void a(@NonNull Drawable paramDrawable, float paramFloat1, float paramFloat2)
  {
    a.a(paramDrawable, paramFloat1, paramFloat2);
  }
  
  public static void a(@NonNull Drawable paramDrawable, @ColorInt int paramInt)
  {
    a.a(paramDrawable, paramInt);
  }
  
  public static void a(@NonNull Drawable paramDrawable, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    a.a(paramDrawable, paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public static void a(@NonNull Drawable paramDrawable, @Nullable ColorStateList paramColorStateList)
  {
    a.a(paramDrawable, paramColorStateList);
  }
  
  public static void a(Drawable paramDrawable, Resources.Theme paramTheme)
  {
    a.a(paramDrawable, paramTheme);
  }
  
  public static void a(Drawable paramDrawable, Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme)
    throws XmlPullParserException, IOException
  {
    a.a(paramDrawable, paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
  }
  
  public static void a(@NonNull Drawable paramDrawable, @Nullable PorterDuff.Mode paramMode)
  {
    a.a(paramDrawable, paramMode);
  }
  
  public static void a(@NonNull Drawable paramDrawable, boolean paramBoolean)
  {
    a.a(paramDrawable, paramBoolean);
  }
  
  public static void b(@NonNull Drawable paramDrawable, int paramInt)
  {
    a.b(paramDrawable, paramInt);
  }
  
  public static boolean b(@NonNull Drawable paramDrawable)
  {
    return a.b(paramDrawable);
  }
  
  public static int c(@NonNull Drawable paramDrawable)
  {
    return a.e(paramDrawable);
  }
  
  public static boolean d(Drawable paramDrawable)
  {
    return a.f(paramDrawable);
  }
  
  public static ColorFilter e(Drawable paramDrawable)
  {
    return a.g(paramDrawable);
  }
  
  public static Drawable f(@NonNull Drawable paramDrawable)
  {
    return a.c(paramDrawable);
  }
  
  public static <T extends Drawable> T g(@NonNull Drawable paramDrawable)
  {
    Drawable localDrawable = paramDrawable;
    if ((paramDrawable instanceof DrawableWrapper)) {
      localDrawable = ((DrawableWrapper)paramDrawable).a();
    }
    return localDrawable;
  }
  
  public static int h(@NonNull Drawable paramDrawable)
  {
    return a.d(paramDrawable);
  }
  
  static class BaseDrawableImpl
    implements DrawableCompat.DrawableImpl
  {
    public void a(Drawable paramDrawable) {}
    
    public void a(Drawable paramDrawable, float paramFloat1, float paramFloat2) {}
    
    public void a(Drawable paramDrawable, int paramInt)
    {
      DrawableCompatBase.a(paramDrawable, paramInt);
    }
    
    public void a(Drawable paramDrawable, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {}
    
    public void a(Drawable paramDrawable, ColorStateList paramColorStateList)
    {
      DrawableCompatBase.a(paramDrawable, paramColorStateList);
    }
    
    public void a(Drawable paramDrawable, Resources.Theme paramTheme) {}
    
    public void a(Drawable paramDrawable, Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme)
      throws IOException, XmlPullParserException
    {
      DrawableCompatBase.a(paramDrawable, paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
    }
    
    public void a(Drawable paramDrawable, PorterDuff.Mode paramMode)
    {
      DrawableCompatBase.a(paramDrawable, paramMode);
    }
    
    public void a(Drawable paramDrawable, boolean paramBoolean) {}
    
    public void b(Drawable paramDrawable, int paramInt) {}
    
    public boolean b(Drawable paramDrawable)
    {
      return false;
    }
    
    public Drawable c(Drawable paramDrawable)
    {
      return DrawableCompatBase.a(paramDrawable);
    }
    
    public int d(Drawable paramDrawable)
    {
      return 0;
    }
    
    public int e(Drawable paramDrawable)
    {
      return 0;
    }
    
    public boolean f(Drawable paramDrawable)
    {
      return false;
    }
    
    public ColorFilter g(Drawable paramDrawable)
    {
      return null;
    }
  }
  
  static abstract interface DrawableImpl
  {
    public abstract void a(Drawable paramDrawable);
    
    public abstract void a(Drawable paramDrawable, float paramFloat1, float paramFloat2);
    
    public abstract void a(Drawable paramDrawable, int paramInt);
    
    public abstract void a(Drawable paramDrawable, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
    
    public abstract void a(Drawable paramDrawable, ColorStateList paramColorStateList);
    
    public abstract void a(Drawable paramDrawable, Resources.Theme paramTheme);
    
    public abstract void a(Drawable paramDrawable, Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme)
      throws IOException, XmlPullParserException;
    
    public abstract void a(Drawable paramDrawable, PorterDuff.Mode paramMode);
    
    public abstract void a(Drawable paramDrawable, boolean paramBoolean);
    
    public abstract void b(Drawable paramDrawable, int paramInt);
    
    public abstract boolean b(Drawable paramDrawable);
    
    public abstract Drawable c(Drawable paramDrawable);
    
    public abstract int d(Drawable paramDrawable);
    
    public abstract int e(Drawable paramDrawable);
    
    public abstract boolean f(Drawable paramDrawable);
    
    public abstract ColorFilter g(Drawable paramDrawable);
  }
  
  static class EclairDrawableImpl
    extends DrawableCompat.BaseDrawableImpl
  {
    public Drawable c(Drawable paramDrawable)
    {
      return DrawableCompatEclair.a(paramDrawable);
    }
  }
  
  static class HoneycombDrawableImpl
    extends DrawableCompat.EclairDrawableImpl
  {
    public void a(Drawable paramDrawable)
    {
      DrawableCompatHoneycomb.a(paramDrawable);
    }
    
    public Drawable c(Drawable paramDrawable)
    {
      return DrawableCompatHoneycomb.b(paramDrawable);
    }
  }
  
  static class JellybeanMr1DrawableImpl
    extends DrawableCompat.HoneycombDrawableImpl
  {
    public void b(Drawable paramDrawable, int paramInt)
    {
      DrawableCompatJellybeanMr1.a(paramDrawable, paramInt);
    }
    
    public int d(Drawable paramDrawable)
    {
      int i = DrawableCompatJellybeanMr1.a(paramDrawable);
      if (i >= 0) {
        return i;
      }
      return 0;
    }
  }
  
  static class KitKatDrawableImpl
    extends DrawableCompat.JellybeanMr1DrawableImpl
  {
    public void a(Drawable paramDrawable, boolean paramBoolean)
    {
      DrawableCompatKitKat.a(paramDrawable, paramBoolean);
    }
    
    public boolean b(Drawable paramDrawable)
    {
      return DrawableCompatKitKat.a(paramDrawable);
    }
    
    public Drawable c(Drawable paramDrawable)
    {
      return DrawableCompatKitKat.b(paramDrawable);
    }
    
    public int e(Drawable paramDrawable)
    {
      return DrawableCompatKitKat.c(paramDrawable);
    }
  }
  
  static class LollipopDrawableImpl
    extends DrawableCompat.KitKatDrawableImpl
  {
    public void a(Drawable paramDrawable, float paramFloat1, float paramFloat2)
    {
      DrawableCompatLollipop.a(paramDrawable, paramFloat1, paramFloat2);
    }
    
    public void a(Drawable paramDrawable, int paramInt)
    {
      DrawableCompatLollipop.a(paramDrawable, paramInt);
    }
    
    public void a(Drawable paramDrawable, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      DrawableCompatLollipop.a(paramDrawable, paramInt1, paramInt2, paramInt3, paramInt4);
    }
    
    public void a(Drawable paramDrawable, ColorStateList paramColorStateList)
    {
      DrawableCompatLollipop.a(paramDrawable, paramColorStateList);
    }
    
    public void a(Drawable paramDrawable, Resources.Theme paramTheme)
    {
      DrawableCompatLollipop.a(paramDrawable, paramTheme);
    }
    
    public void a(Drawable paramDrawable, Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme)
      throws IOException, XmlPullParserException
    {
      DrawableCompatLollipop.a(paramDrawable, paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
    }
    
    public void a(Drawable paramDrawable, PorterDuff.Mode paramMode)
    {
      DrawableCompatLollipop.a(paramDrawable, paramMode);
    }
    
    public Drawable c(Drawable paramDrawable)
    {
      return DrawableCompatLollipop.a(paramDrawable);
    }
    
    public boolean f(Drawable paramDrawable)
    {
      return DrawableCompatLollipop.b(paramDrawable);
    }
    
    public ColorFilter g(Drawable paramDrawable)
    {
      return DrawableCompatLollipop.c(paramDrawable);
    }
  }
  
  static class MDrawableImpl
    extends DrawableCompat.LollipopDrawableImpl
  {
    public void b(Drawable paramDrawable, int paramInt)
    {
      DrawableCompatApi23.a(paramDrawable, paramInt);
    }
    
    public Drawable c(Drawable paramDrawable)
    {
      return paramDrawable;
    }
    
    public int d(Drawable paramDrawable)
    {
      return DrawableCompatApi23.a(paramDrawable);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/graphics/drawable/DrawableCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */