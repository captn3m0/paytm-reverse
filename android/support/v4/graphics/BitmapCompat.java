package android.support.v4.graphics;

import android.os.Build.VERSION;

public final class BitmapCompat
{
  static final BitmapImpl a = new BaseBitmapImpl();
  
  static
  {
    int i = Build.VERSION.SDK_INT;
    if (i >= 19)
    {
      a = new KitKatBitmapCompatImpl();
      return;
    }
    if (i >= 18)
    {
      a = new JbMr2BitmapCompatImpl();
      return;
    }
    if (i >= 12)
    {
      a = new HcMr1BitmapCompatImpl();
      return;
    }
  }
  
  static class BaseBitmapImpl
    implements BitmapCompat.BitmapImpl
  {}
  
  static abstract interface BitmapImpl {}
  
  static class HcMr1BitmapCompatImpl
    extends BitmapCompat.BaseBitmapImpl
  {}
  
  static class JbMr2BitmapCompatImpl
    extends BitmapCompat.HcMr1BitmapCompatImpl
  {}
  
  static class KitKatBitmapCompatImpl
    extends BitmapCompat.JbMr2BitmapCompatImpl
  {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/graphics/BitmapCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */