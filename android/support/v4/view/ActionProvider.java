package android.support.v4.view;

import android.content.Context;
import android.util.Log;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

public abstract class ActionProvider
{
  private final Context a;
  private SubUiVisibilityListener b;
  private VisibilityListener c;
  
  public ActionProvider(Context paramContext)
  {
    this.a = paramContext;
  }
  
  public Context a()
  {
    return this.a;
  }
  
  public View a(MenuItem paramMenuItem)
  {
    return b();
  }
  
  public void a(SubUiVisibilityListener paramSubUiVisibilityListener)
  {
    this.b = paramSubUiVisibilityListener;
  }
  
  public void a(VisibilityListener paramVisibilityListener)
  {
    if ((this.c != null) && (paramVisibilityListener != null)) {
      Log.w("ActionProvider(support)", "setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this " + getClass().getSimpleName() + " instance while it is still in use somewhere else?");
    }
    this.c = paramVisibilityListener;
  }
  
  public void a(SubMenu paramSubMenu) {}
  
  public void a(boolean paramBoolean)
  {
    if (this.b != null) {
      this.b.b(paramBoolean);
    }
  }
  
  public abstract View b();
  
  public boolean c()
  {
    return false;
  }
  
  public boolean d()
  {
    return true;
  }
  
  public void e()
  {
    if ((this.c != null) && (c())) {
      this.c.a(d());
    }
  }
  
  public boolean f()
  {
    return false;
  }
  
  public boolean g()
  {
    return false;
  }
  
  public void h()
  {
    this.c = null;
    this.b = null;
  }
  
  public static abstract interface SubUiVisibilityListener
  {
    public abstract void b(boolean paramBoolean);
  }
  
  public static abstract interface VisibilityListener
  {
    public abstract void a(boolean paramBoolean);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/view/ActionProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */