package android.support.v4.view;

import android.view.View;

class ViewCompatMarshmallow
{
  static void a(View paramView, int paramInt)
  {
    paramView.offsetTopAndBottom(paramInt);
  }
  
  public static void a(View paramView, int paramInt1, int paramInt2)
  {
    paramView.setScrollIndicators(paramInt1, paramInt2);
  }
  
  static void b(View paramView, int paramInt)
  {
    paramView.offsetLeftAndRight(paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/view/ViewCompatMarshmallow.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */