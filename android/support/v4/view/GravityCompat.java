package android.support.v4.view;

import android.graphics.Rect;
import android.os.Build.VERSION;
import android.view.Gravity;

public final class GravityCompat
{
  static final GravityCompatImpl a = new GravityCompatImplBase();
  
  static
  {
    if (Build.VERSION.SDK_INT >= 17)
    {
      a = new GravityCompatImplJellybeanMr1();
      return;
    }
  }
  
  public static int a(int paramInt1, int paramInt2)
  {
    return a.a(paramInt1, paramInt2);
  }
  
  public static void a(int paramInt1, int paramInt2, int paramInt3, Rect paramRect1, Rect paramRect2, int paramInt4)
  {
    a.a(paramInt1, paramInt2, paramInt3, paramRect1, paramRect2, paramInt4);
  }
  
  static abstract interface GravityCompatImpl
  {
    public abstract int a(int paramInt1, int paramInt2);
    
    public abstract void a(int paramInt1, int paramInt2, int paramInt3, Rect paramRect1, Rect paramRect2, int paramInt4);
  }
  
  static class GravityCompatImplBase
    implements GravityCompat.GravityCompatImpl
  {
    public int a(int paramInt1, int paramInt2)
    {
      return 0xFF7FFFFF & paramInt1;
    }
    
    public void a(int paramInt1, int paramInt2, int paramInt3, Rect paramRect1, Rect paramRect2, int paramInt4)
    {
      Gravity.apply(paramInt1, paramInt2, paramInt3, paramRect1, paramRect2);
    }
  }
  
  static class GravityCompatImplJellybeanMr1
    implements GravityCompat.GravityCompatImpl
  {
    public int a(int paramInt1, int paramInt2)
    {
      return GravityCompatJellybeanMr1.a(paramInt1, paramInt2);
    }
    
    public void a(int paramInt1, int paramInt2, int paramInt3, Rect paramRect1, Rect paramRect2, int paramInt4)
    {
      GravityCompatJellybeanMr1.a(paramInt1, paramInt2, paramInt3, paramRect1, paramRect2, paramInt4);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/view/GravityCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */