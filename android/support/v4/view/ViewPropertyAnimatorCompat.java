package android.support.v4.view;

import android.os.Build.VERSION;
import android.view.View;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;

public final class ViewPropertyAnimatorCompat
{
  static final ViewPropertyAnimatorCompatImpl a = new BaseViewPropertyAnimatorCompatImpl();
  private WeakReference<View> b;
  private Runnable c = null;
  private Runnable d = null;
  private int e = -1;
  
  static
  {
    int i = Build.VERSION.SDK_INT;
    if (i >= 21)
    {
      a = new LollipopViewPropertyAnimatorCompatImpl();
      return;
    }
    if (i >= 19)
    {
      a = new KitKatViewPropertyAnimatorCompatImpl();
      return;
    }
    if (i >= 18)
    {
      a = new JBMr2ViewPropertyAnimatorCompatImpl();
      return;
    }
    if (i >= 16)
    {
      a = new JBViewPropertyAnimatorCompatImpl();
      return;
    }
    if (i >= 14)
    {
      a = new ICSViewPropertyAnimatorCompatImpl();
      return;
    }
  }
  
  ViewPropertyAnimatorCompat(View paramView)
  {
    this.b = new WeakReference(paramView);
  }
  
  public long a()
  {
    View localView = (View)this.b.get();
    if (localView != null) {
      return a.a(this, localView);
    }
    return 0L;
  }
  
  public ViewPropertyAnimatorCompat a(float paramFloat)
  {
    View localView = (View)this.b.get();
    if (localView != null) {
      a.a(this, localView, paramFloat);
    }
    return this;
  }
  
  public ViewPropertyAnimatorCompat a(long paramLong)
  {
    View localView = (View)this.b.get();
    if (localView != null) {
      a.a(this, localView, paramLong);
    }
    return this;
  }
  
  public ViewPropertyAnimatorCompat a(ViewPropertyAnimatorListener paramViewPropertyAnimatorListener)
  {
    View localView = (View)this.b.get();
    if (localView != null) {
      a.a(this, localView, paramViewPropertyAnimatorListener);
    }
    return this;
  }
  
  public ViewPropertyAnimatorCompat a(ViewPropertyAnimatorUpdateListener paramViewPropertyAnimatorUpdateListener)
  {
    View localView = (View)this.b.get();
    if (localView != null) {
      a.a(this, localView, paramViewPropertyAnimatorUpdateListener);
    }
    return this;
  }
  
  public ViewPropertyAnimatorCompat a(Interpolator paramInterpolator)
  {
    View localView = (View)this.b.get();
    if (localView != null) {
      a.a(this, localView, paramInterpolator);
    }
    return this;
  }
  
  public ViewPropertyAnimatorCompat b(float paramFloat)
  {
    View localView = (View)this.b.get();
    if (localView != null) {
      a.b(this, localView, paramFloat);
    }
    return this;
  }
  
  public ViewPropertyAnimatorCompat b(long paramLong)
  {
    View localView = (View)this.b.get();
    if (localView != null) {
      a.b(this, localView, paramLong);
    }
    return this;
  }
  
  public void b()
  {
    View localView = (View)this.b.get();
    if (localView != null) {
      a.b(this, localView);
    }
  }
  
  public ViewPropertyAnimatorCompat c(float paramFloat)
  {
    View localView = (View)this.b.get();
    if (localView != null) {
      a.c(this, localView, paramFloat);
    }
    return this;
  }
  
  public void cancel()
  {
    View localView = (View)this.b.get();
    if (localView != null) {
      a.cancel(this, localView);
    }
  }
  
  static class BaseViewPropertyAnimatorCompatImpl
    implements ViewPropertyAnimatorCompat.ViewPropertyAnimatorCompatImpl
  {
    WeakHashMap<View, Runnable> a = null;
    
    private void a(View paramView)
    {
      if (this.a != null)
      {
        Runnable localRunnable = (Runnable)this.a.get(paramView);
        if (localRunnable != null) {
          paramView.removeCallbacks(localRunnable);
        }
      }
    }
    
    private void c(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView)
    {
      Object localObject = paramView.getTag(2113929216);
      ViewPropertyAnimatorListener localViewPropertyAnimatorListener = null;
      if ((localObject instanceof ViewPropertyAnimatorListener)) {
        localViewPropertyAnimatorListener = (ViewPropertyAnimatorListener)localObject;
      }
      localObject = ViewPropertyAnimatorCompat.a(paramViewPropertyAnimatorCompat);
      Runnable localRunnable = ViewPropertyAnimatorCompat.b(paramViewPropertyAnimatorCompat);
      ViewPropertyAnimatorCompat.b(paramViewPropertyAnimatorCompat, null);
      ViewPropertyAnimatorCompat.a(paramViewPropertyAnimatorCompat, null);
      if (localObject != null) {
        ((Runnable)localObject).run();
      }
      if (localViewPropertyAnimatorListener != null)
      {
        localViewPropertyAnimatorListener.a(paramView);
        localViewPropertyAnimatorListener.b(paramView);
      }
      if (localRunnable != null) {
        localRunnable.run();
      }
      if (this.a != null) {
        this.a.remove(paramView);
      }
    }
    
    private void d(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView)
    {
      Runnable localRunnable = null;
      if (this.a != null) {
        localRunnable = (Runnable)this.a.get(paramView);
      }
      Object localObject = localRunnable;
      if (localRunnable == null)
      {
        localObject = new Starter(paramViewPropertyAnimatorCompat, paramView, null);
        if (this.a == null) {
          this.a = new WeakHashMap();
        }
        this.a.put(paramView, localObject);
      }
      paramView.removeCallbacks((Runnable)localObject);
      paramView.post((Runnable)localObject);
    }
    
    public long a(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView)
    {
      return 0L;
    }
    
    public void a(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, float paramFloat)
    {
      d(paramViewPropertyAnimatorCompat, paramView);
    }
    
    public void a(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, long paramLong) {}
    
    public void a(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, ViewPropertyAnimatorListener paramViewPropertyAnimatorListener)
    {
      paramView.setTag(2113929216, paramViewPropertyAnimatorListener);
    }
    
    public void a(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, ViewPropertyAnimatorUpdateListener paramViewPropertyAnimatorUpdateListener) {}
    
    public void a(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, Interpolator paramInterpolator) {}
    
    public void b(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView)
    {
      a(paramView);
      c(paramViewPropertyAnimatorCompat, paramView);
    }
    
    public void b(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, float paramFloat)
    {
      d(paramViewPropertyAnimatorCompat, paramView);
    }
    
    public void b(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, long paramLong) {}
    
    public void c(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, float paramFloat)
    {
      d(paramViewPropertyAnimatorCompat, paramView);
    }
    
    public void cancel(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView)
    {
      d(paramViewPropertyAnimatorCompat, paramView);
    }
    
    class Starter
      implements Runnable
    {
      WeakReference<View> a;
      ViewPropertyAnimatorCompat b;
      
      private Starter(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView)
      {
        this.a = new WeakReference(paramView);
        this.b = paramViewPropertyAnimatorCompat;
      }
      
      public void run()
      {
        View localView = (View)this.a.get();
        if (localView != null) {
          ViewPropertyAnimatorCompat.BaseViewPropertyAnimatorCompatImpl.a(ViewPropertyAnimatorCompat.BaseViewPropertyAnimatorCompatImpl.this, this.b, localView);
        }
      }
    }
  }
  
  static class ICSViewPropertyAnimatorCompatImpl
    extends ViewPropertyAnimatorCompat.BaseViewPropertyAnimatorCompatImpl
  {
    WeakHashMap<View, Integer> b = null;
    
    public long a(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView)
    {
      return ViewPropertyAnimatorCompatICS.a(paramView);
    }
    
    public void a(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, float paramFloat)
    {
      ViewPropertyAnimatorCompatICS.a(paramView, paramFloat);
    }
    
    public void a(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, long paramLong)
    {
      ViewPropertyAnimatorCompatICS.a(paramView, paramLong);
    }
    
    public void a(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, ViewPropertyAnimatorListener paramViewPropertyAnimatorListener)
    {
      paramView.setTag(2113929216, paramViewPropertyAnimatorListener);
      ViewPropertyAnimatorCompatICS.a(paramView, new MyVpaListener(paramViewPropertyAnimatorCompat));
    }
    
    public void a(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, Interpolator paramInterpolator)
    {
      ViewPropertyAnimatorCompatICS.a(paramView, paramInterpolator);
    }
    
    public void b(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView)
    {
      ViewPropertyAnimatorCompatICS.b(paramView);
    }
    
    public void b(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, float paramFloat)
    {
      ViewPropertyAnimatorCompatICS.b(paramView, paramFloat);
    }
    
    public void b(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, long paramLong)
    {
      ViewPropertyAnimatorCompatICS.b(paramView, paramLong);
    }
    
    public void c(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, float paramFloat)
    {
      ViewPropertyAnimatorCompatICS.c(paramView, paramFloat);
    }
    
    public void cancel(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView)
    {
      ViewPropertyAnimatorCompatICS.cancel(paramView);
    }
    
    static class MyVpaListener
      implements ViewPropertyAnimatorListener
    {
      ViewPropertyAnimatorCompat a;
      boolean b;
      
      MyVpaListener(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat)
      {
        this.a = paramViewPropertyAnimatorCompat;
      }
      
      public void a(View paramView)
      {
        this.b = false;
        if (ViewPropertyAnimatorCompat.c(this.a) >= 0) {
          ViewCompat.a(paramView, 2, null);
        }
        if (ViewPropertyAnimatorCompat.a(this.a) != null)
        {
          localObject1 = ViewPropertyAnimatorCompat.a(this.a);
          ViewPropertyAnimatorCompat.b(this.a, null);
          ((Runnable)localObject1).run();
        }
        Object localObject2 = paramView.getTag(2113929216);
        Object localObject1 = null;
        if ((localObject2 instanceof ViewPropertyAnimatorListener)) {
          localObject1 = (ViewPropertyAnimatorListener)localObject2;
        }
        if (localObject1 != null) {
          ((ViewPropertyAnimatorListener)localObject1).a(paramView);
        }
      }
      
      public void b(View paramView)
      {
        if (ViewPropertyAnimatorCompat.c(this.a) >= 0)
        {
          ViewCompat.a(paramView, ViewPropertyAnimatorCompat.c(this.a), null);
          ViewPropertyAnimatorCompat.a(this.a, -1);
        }
        if ((Build.VERSION.SDK_INT >= 16) || (!this.b))
        {
          if (ViewPropertyAnimatorCompat.b(this.a) != null)
          {
            localObject1 = ViewPropertyAnimatorCompat.b(this.a);
            ViewPropertyAnimatorCompat.a(this.a, null);
            ((Runnable)localObject1).run();
          }
          Object localObject2 = paramView.getTag(2113929216);
          Object localObject1 = null;
          if ((localObject2 instanceof ViewPropertyAnimatorListener)) {
            localObject1 = (ViewPropertyAnimatorListener)localObject2;
          }
          if (localObject1 != null) {
            ((ViewPropertyAnimatorListener)localObject1).b(paramView);
          }
          this.b = true;
        }
      }
      
      public void c(View paramView)
      {
        Object localObject = paramView.getTag(2113929216);
        ViewPropertyAnimatorListener localViewPropertyAnimatorListener = null;
        if ((localObject instanceof ViewPropertyAnimatorListener)) {
          localViewPropertyAnimatorListener = (ViewPropertyAnimatorListener)localObject;
        }
        if (localViewPropertyAnimatorListener != null) {
          localViewPropertyAnimatorListener.c(paramView);
        }
      }
    }
  }
  
  static class JBMr2ViewPropertyAnimatorCompatImpl
    extends ViewPropertyAnimatorCompat.JBViewPropertyAnimatorCompatImpl
  {}
  
  static class JBViewPropertyAnimatorCompatImpl
    extends ViewPropertyAnimatorCompat.ICSViewPropertyAnimatorCompatImpl
  {
    public void a(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, ViewPropertyAnimatorListener paramViewPropertyAnimatorListener)
    {
      ViewPropertyAnimatorCompatJB.a(paramView, paramViewPropertyAnimatorListener);
    }
  }
  
  static class KitKatViewPropertyAnimatorCompatImpl
    extends ViewPropertyAnimatorCompat.JBMr2ViewPropertyAnimatorCompatImpl
  {
    public void a(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, ViewPropertyAnimatorUpdateListener paramViewPropertyAnimatorUpdateListener)
    {
      ViewPropertyAnimatorCompatKK.a(paramView, paramViewPropertyAnimatorUpdateListener);
    }
  }
  
  static class LollipopViewPropertyAnimatorCompatImpl
    extends ViewPropertyAnimatorCompat.KitKatViewPropertyAnimatorCompatImpl
  {}
  
  static abstract interface ViewPropertyAnimatorCompatImpl
  {
    public abstract long a(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView);
    
    public abstract void a(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, float paramFloat);
    
    public abstract void a(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, long paramLong);
    
    public abstract void a(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, ViewPropertyAnimatorListener paramViewPropertyAnimatorListener);
    
    public abstract void a(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, ViewPropertyAnimatorUpdateListener paramViewPropertyAnimatorUpdateListener);
    
    public abstract void a(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, Interpolator paramInterpolator);
    
    public abstract void b(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView);
    
    public abstract void b(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, float paramFloat);
    
    public abstract void b(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, long paramLong);
    
    public abstract void c(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView, float paramFloat);
    
    public abstract void cancel(ViewPropertyAnimatorCompat paramViewPropertyAnimatorCompat, View paramView);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/view/ViewPropertyAnimatorCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */