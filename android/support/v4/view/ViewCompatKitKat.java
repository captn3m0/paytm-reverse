package android.support.v4.view;

import android.view.View;

class ViewCompatKitKat
{
  public static void a(View paramView, int paramInt)
  {
    paramView.setAccessibilityLiveRegion(paramInt);
  }
  
  public static boolean a(View paramView)
  {
    return paramView.isLaidOut();
  }
  
  public static boolean b(View paramView)
  {
    return paramView.isAttachedToWindow();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/view/ViewCompatKitKat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */