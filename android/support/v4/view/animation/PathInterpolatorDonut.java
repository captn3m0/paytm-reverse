package android.support.v4.view.animation;

import android.view.animation.Interpolator;

class PathInterpolatorDonut
  implements Interpolator
{
  private final float[] a;
  private final float[] b;
  
  public float getInterpolation(float paramFloat)
  {
    if (paramFloat <= 0.0F) {
      return 0.0F;
    }
    if (paramFloat >= 1.0F) {
      return 1.0F;
    }
    int i = 0;
    int j = this.a.length - 1;
    while (j - i > 1)
    {
      int k = (i + j) / 2;
      if (paramFloat < this.a[k]) {
        j = k;
      } else {
        i = k;
      }
    }
    float f = this.a[j] - this.a[i];
    if (f == 0.0F) {
      return this.b[i];
    }
    paramFloat = (paramFloat - this.a[i]) / f;
    f = this.b[i];
    return (this.b[j] - f) * paramFloat + f;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/view/animation/PathInterpolatorDonut.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */