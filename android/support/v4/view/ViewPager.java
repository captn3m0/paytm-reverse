package android.support.v4.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.SystemClock;
import android.support.annotation.CallSuper;
import android.support.annotation.DrawableRes;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v4.view.accessibility.AccessibilityRecordCompat;
import android.support.v4.widget.EdgeEffectCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Interpolator;
import android.widget.Scroller;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ViewPager
  extends ViewGroup
{
  private static final int[] a = { 16842931 };
  private static final ViewPositionComparator aj = new ViewPositionComparator();
  private static final Comparator<ItemInfo> c = new Comparator()
  {
    public int a(ViewPager.ItemInfo paramAnonymousItemInfo1, ViewPager.ItemInfo paramAnonymousItemInfo2)
    {
      return paramAnonymousItemInfo1.b - paramAnonymousItemInfo2.b;
    }
  };
  private static final Interpolator d = new Interpolator()
  {
    public float getInterpolation(float paramAnonymousFloat)
    {
      paramAnonymousFloat -= 1.0F;
      return paramAnonymousFloat * paramAnonymousFloat * paramAnonymousFloat * paramAnonymousFloat * paramAnonymousFloat + 1.0F;
    }
  };
  private int A = 1;
  private boolean B;
  private boolean C;
  private int D;
  private int E;
  private int F;
  private float G;
  private float H;
  private float I;
  private float J;
  private int K = -1;
  private VelocityTracker L;
  private int M;
  private int N;
  private int O;
  private int P;
  private boolean Q;
  private long R;
  private EdgeEffectCompat S;
  private EdgeEffectCompat T;
  private boolean U = true;
  private boolean V = false;
  private boolean W;
  private int aa;
  private List<OnPageChangeListener> ab;
  private OnPageChangeListener ac;
  private OnPageChangeListener ad;
  private OnAdapterChangeListener ae;
  private PageTransformer af;
  private Method ag;
  private int ah;
  private ArrayList<View> ai;
  private final Runnable ak = new Runnable()
  {
    public void run()
    {
      ViewPager.a(ViewPager.this, 0);
      ViewPager.this.c();
    }
  };
  private int al = 0;
  private int b;
  private final ArrayList<ItemInfo> e = new ArrayList();
  private final ItemInfo f = new ItemInfo();
  private final Rect g = new Rect();
  private PagerAdapter h;
  private int i;
  private int j = -1;
  private Parcelable k = null;
  private ClassLoader l = null;
  private Scroller m;
  private boolean n;
  private PagerObserver o;
  private int p;
  private Drawable q;
  private int r;
  private int s;
  private float t = -3.4028235E38F;
  private float u = Float.MAX_VALUE;
  private int v;
  private int w;
  private boolean x;
  private boolean y;
  private boolean z;
  
  public ViewPager(Context paramContext)
  {
    super(paramContext);
    a();
  }
  
  public ViewPager(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a();
  }
  
  private int a(int paramInt1, float paramFloat, int paramInt2, int paramInt3)
  {
    if ((Math.abs(paramInt3) > this.O) && (Math.abs(paramInt2) > this.M))
    {
      if (paramInt2 > 0) {}
      for (;;)
      {
        paramInt2 = paramInt1;
        if (this.e.size() > 0)
        {
          ItemInfo localItemInfo1 = (ItemInfo)this.e.get(0);
          ItemInfo localItemInfo2 = (ItemInfo)this.e.get(this.e.size() - 1);
          paramInt2 = Math.max(localItemInfo1.b, Math.min(paramInt1, localItemInfo2.b));
        }
        return paramInt2;
        paramInt1 += 1;
      }
    }
    if (paramInt1 >= this.i) {}
    for (float f1 = 0.4F;; f1 = 0.6F)
    {
      paramInt1 = (int)(paramInt1 + paramFloat + f1);
      break;
    }
  }
  
  private Rect a(Rect paramRect, View paramView)
  {
    Rect localRect = paramRect;
    if (paramRect == null) {
      localRect = new Rect();
    }
    if (paramView == null) {
      localRect.set(0, 0, 0, 0);
    }
    for (;;)
    {
      return localRect;
      localRect.left = paramView.getLeft();
      localRect.right = paramView.getRight();
      localRect.top = paramView.getTop();
      localRect.bottom = paramView.getBottom();
      for (paramRect = paramView.getParent(); ((paramRect instanceof ViewGroup)) && (paramRect != this); paramRect = paramRect.getParent())
      {
        paramRect = (ViewGroup)paramRect;
        localRect.left += paramRect.getLeft();
        localRect.right += paramRect.getRight();
        localRect.top += paramRect.getTop();
        localRect.bottom += paramRect.getBottom();
      }
    }
  }
  
  private void a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if ((paramInt2 > 0) && (!this.e.isEmpty()))
    {
      if (!this.m.isFinished())
      {
        this.m.setFinalX(getCurrentItem() * getClientWidth());
        return;
      }
      int i1 = getPaddingLeft();
      int i2 = getPaddingRight();
      int i3 = getPaddingLeft();
      int i4 = getPaddingRight();
      f1 = getScrollX() / (paramInt2 - i3 - i4 + paramInt4);
      scrollTo((int)((paramInt1 - i1 - i2 + paramInt3) * f1), getScrollY());
      return;
    }
    ItemInfo localItemInfo = b(this.i);
    if (localItemInfo != null) {}
    for (float f1 = Math.min(localItemInfo.e, this.u);; f1 = 0.0F)
    {
      paramInt1 = (int)((paramInt1 - getPaddingLeft() - getPaddingRight()) * f1);
      if (paramInt1 == getScrollX()) {
        break;
      }
      a(false);
      scrollTo(paramInt1, getScrollY());
      return;
    }
  }
  
  private void a(int paramInt1, boolean paramBoolean1, int paramInt2, boolean paramBoolean2)
  {
    ItemInfo localItemInfo = b(paramInt1);
    int i1 = 0;
    if (localItemInfo != null) {
      i1 = (int)(getClientWidth() * Math.max(this.t, Math.min(localItemInfo.e, this.u)));
    }
    if (paramBoolean1)
    {
      a(i1, 0, paramInt2);
      if (paramBoolean2) {
        e(paramInt1);
      }
      return;
    }
    if (paramBoolean2) {
      e(paramInt1);
    }
    a(false);
    scrollTo(i1, 0);
    d(i1);
  }
  
  private void a(ItemInfo paramItemInfo1, int paramInt, ItemInfo paramItemInfo2)
  {
    int i4 = this.h.getCount();
    int i1 = getClientWidth();
    float f2;
    if (i1 > 0)
    {
      f2 = this.p / i1;
      if (paramItemInfo2 == null) {
        break label409;
      }
      i1 = paramItemInfo2.b;
      if (i1 < paramItemInfo1.b)
      {
        i2 = 0;
        f1 = paramItemInfo2.e + paramItemInfo2.d + f2;
        i1 += 1;
      }
    }
    else
    {
      for (;;)
      {
        if ((i1 > paramItemInfo1.b) || (i2 >= this.e.size())) {
          break label409;
        }
        for (paramItemInfo2 = (ItemInfo)this.e.get(i2);; paramItemInfo2 = (ItemInfo)this.e.get(i2))
        {
          f3 = f1;
          i3 = i1;
          if (i1 <= paramItemInfo2.b) {
            break;
          }
          f3 = f1;
          i3 = i1;
          if (i2 >= this.e.size() - 1) {
            break;
          }
          i2 += 1;
        }
        f2 = 0.0F;
        break;
        while (i3 < paramItemInfo2.b)
        {
          f3 += this.h.getPageWidth(i3) + f2;
          i3 += 1;
        }
        paramItemInfo2.e = f3;
        f1 = f3 + (paramItemInfo2.d + f2);
        i1 = i3 + 1;
      }
    }
    if (i1 > paramItemInfo1.b)
    {
      i2 = this.e.size() - 1;
      f1 = paramItemInfo2.e;
      i1 -= 1;
      while ((i1 >= paramItemInfo1.b) && (i2 >= 0))
      {
        for (paramItemInfo2 = (ItemInfo)this.e.get(i2);; paramItemInfo2 = (ItemInfo)this.e.get(i2))
        {
          f3 = f1;
          i3 = i1;
          if (i1 >= paramItemInfo2.b) {
            break;
          }
          f3 = f1;
          i3 = i1;
          if (i2 <= 0) {
            break;
          }
          i2 -= 1;
        }
        while (i3 > paramItemInfo2.b)
        {
          f3 -= this.h.getPageWidth(i3) + f2;
          i3 -= 1;
        }
        f1 = f3 - (paramItemInfo2.d + f2);
        paramItemInfo2.e = f1;
        i1 = i3 - 1;
      }
    }
    label409:
    int i3 = this.e.size();
    float f3 = paramItemInfo1.e;
    i1 = paramItemInfo1.b - 1;
    if (paramItemInfo1.b == 0)
    {
      f1 = paramItemInfo1.e;
      this.t = f1;
      if (paramItemInfo1.b != i4 - 1) {
        break label550;
      }
      f1 = paramItemInfo1.e + paramItemInfo1.d - 1.0F;
      label475:
      this.u = f1;
      i2 = paramInt - 1;
      f1 = f3;
    }
    for (;;)
    {
      if (i2 < 0) {
        break label603;
      }
      paramItemInfo2 = (ItemInfo)this.e.get(i2);
      for (;;)
      {
        if (i1 > paramItemInfo2.b)
        {
          f1 -= this.h.getPageWidth(i1) + f2;
          i1 -= 1;
          continue;
          f1 = -3.4028235E38F;
          break;
          label550:
          f1 = Float.MAX_VALUE;
          break label475;
        }
      }
      f1 -= paramItemInfo2.d + f2;
      paramItemInfo2.e = f1;
      if (paramItemInfo2.b == 0) {
        this.t = f1;
      }
      i2 -= 1;
      i1 -= 1;
    }
    label603:
    float f1 = paramItemInfo1.e + paramItemInfo1.d + f2;
    i1 = paramItemInfo1.b + 1;
    int i2 = paramInt + 1;
    paramInt = i1;
    i1 = i2;
    while (i1 < i3)
    {
      paramItemInfo1 = (ItemInfo)this.e.get(i1);
      while (paramInt < paramItemInfo1.b)
      {
        f1 += this.h.getPageWidth(paramInt) + f2;
        paramInt += 1;
      }
      if (paramItemInfo1.b == i4 - 1) {
        this.u = (paramItemInfo1.d + f1 - 1.0F);
      }
      paramItemInfo1.e = f1;
      f1 += paramItemInfo1.d + f2;
      i1 += 1;
      paramInt += 1;
    }
    this.V = false;
  }
  
  private void a(MotionEvent paramMotionEvent)
  {
    int i1 = MotionEventCompat.b(paramMotionEvent);
    if (MotionEventCompat.b(paramMotionEvent, i1) == this.K) {
      if (i1 != 0) {
        break label56;
      }
    }
    label56:
    for (i1 = 1;; i1 = 0)
    {
      this.G = MotionEventCompat.c(paramMotionEvent, i1);
      this.K = MotionEventCompat.b(paramMotionEvent, i1);
      if (this.L != null) {
        this.L.clear();
      }
      return;
    }
  }
  
  private void a(boolean paramBoolean)
  {
    int i2 = 1;
    int i1;
    if (this.al == 2)
    {
      i1 = 1;
      if (i1 != 0)
      {
        setScrollingCacheEnabled(false);
        if (this.m.isFinished()) {
          break label170;
        }
      }
    }
    for (;;)
    {
      if (i2 != 0)
      {
        this.m.abortAnimation();
        i2 = getScrollX();
        i3 = getScrollY();
        int i4 = this.m.getCurrX();
        int i5 = this.m.getCurrY();
        if ((i2 != i4) || (i3 != i5))
        {
          scrollTo(i4, i5);
          if (i4 != i2) {
            d(i4);
          }
        }
      }
      this.z = false;
      int i3 = 0;
      i2 = i1;
      i1 = i3;
      while (i1 < this.e.size())
      {
        ItemInfo localItemInfo = (ItemInfo)this.e.get(i1);
        if (localItemInfo.c)
        {
          i2 = 1;
          localItemInfo.c = false;
        }
        i1 += 1;
      }
      i1 = 0;
      break;
      label170:
      i2 = 0;
    }
    if (i2 != 0)
    {
      if (paramBoolean) {
        ViewCompat.a(this, this.ak);
      }
    }
    else {
      return;
    }
    this.ak.run();
  }
  
  private boolean a(float paramFloat1, float paramFloat2)
  {
    return ((paramFloat1 < this.E) && (paramFloat2 > 0.0F)) || ((paramFloat1 > getWidth() - this.E) && (paramFloat2 < 0.0F));
  }
  
  private void b(int paramInt1, float paramFloat, int paramInt2)
  {
    if (this.ac != null) {
      this.ac.onPageScrolled(paramInt1, paramFloat, paramInt2);
    }
    if (this.ab != null)
    {
      int i1 = 0;
      int i2 = this.ab.size();
      while (i1 < i2)
      {
        OnPageChangeListener localOnPageChangeListener = (OnPageChangeListener)this.ab.get(i1);
        if (localOnPageChangeListener != null) {
          localOnPageChangeListener.onPageScrolled(paramInt1, paramFloat, paramInt2);
        }
        i1 += 1;
      }
    }
    if (this.ad != null) {
      this.ad.onPageScrolled(paramInt1, paramFloat, paramInt2);
    }
  }
  
  private void b(boolean paramBoolean)
  {
    int i3 = getChildCount();
    int i1 = 0;
    if (i1 < i3)
    {
      if (paramBoolean) {}
      for (int i2 = 2;; i2 = 0)
      {
        ViewCompat.a(getChildAt(i1), i2, null);
        i1 += 1;
        break;
      }
    }
  }
  
  private void c(boolean paramBoolean)
  {
    ViewParent localViewParent = getParent();
    if (localViewParent != null) {
      localViewParent.requestDisallowInterceptTouchEvent(paramBoolean);
    }
  }
  
  private boolean c(float paramFloat)
  {
    boolean bool3 = false;
    boolean bool2 = false;
    boolean bool1 = false;
    float f1 = this.G;
    this.G = paramFloat;
    float f2 = getScrollX() + (f1 - paramFloat);
    int i3 = getClientWidth();
    paramFloat = i3 * this.t;
    f1 = i3 * this.u;
    int i1 = 1;
    int i2 = 1;
    ItemInfo localItemInfo1 = (ItemInfo)this.e.get(0);
    ItemInfo localItemInfo2 = (ItemInfo)this.e.get(this.e.size() - 1);
    if (localItemInfo1.b != 0)
    {
      i1 = 0;
      paramFloat = localItemInfo1.e * i3;
    }
    if (localItemInfo2.b != this.h.getCount() - 1)
    {
      i2 = 0;
      f1 = localItemInfo2.e * i3;
    }
    if (f2 < paramFloat) {
      if (i1 != 0) {
        bool1 = this.S.a(Math.abs(paramFloat - f2) / i3);
      }
    }
    for (;;)
    {
      this.G += paramFloat - (int)paramFloat;
      scrollTo((int)paramFloat, getScrollY());
      d((int)paramFloat);
      return bool1;
      bool1 = bool3;
      paramFloat = f2;
      if (f2 > f1)
      {
        bool1 = bool2;
        if (i2 != 0) {
          bool1 = this.T.a(Math.abs(f2 - f1) / i3);
        }
        paramFloat = f1;
      }
    }
  }
  
  private boolean d(int paramInt)
  {
    if (this.e.size() == 0)
    {
      if (this.U) {}
      do
      {
        return false;
        this.W = false;
        a(0, 0.0F, 0);
      } while (this.W);
      throw new IllegalStateException("onPageScrolled did not call superclass implementation");
    }
    ItemInfo localItemInfo = m();
    int i2 = getClientWidth();
    int i3 = this.p;
    float f1 = this.p / i2;
    int i1 = localItemInfo.b;
    f1 = (paramInt / i2 - localItemInfo.e) / (localItemInfo.d + f1);
    paramInt = (int)((i2 + i3) * f1);
    this.W = false;
    a(i1, f1, paramInt);
    if (!this.W) {
      throw new IllegalStateException("onPageScrolled did not call superclass implementation");
    }
    return true;
  }
  
  private void e(int paramInt)
  {
    if (this.ac != null) {
      this.ac.onPageSelected(paramInt);
    }
    if (this.ab != null)
    {
      int i1 = 0;
      int i2 = this.ab.size();
      while (i1 < i2)
      {
        OnPageChangeListener localOnPageChangeListener = (OnPageChangeListener)this.ab.get(i1);
        if (localOnPageChangeListener != null) {
          localOnPageChangeListener.onPageSelected(paramInt);
        }
        i1 += 1;
      }
    }
    if (this.ad != null) {
      this.ad.onPageSelected(paramInt);
    }
  }
  
  private void f(int paramInt)
  {
    if (this.ac != null) {
      this.ac.onPageScrollStateChanged(paramInt);
    }
    if (this.ab != null)
    {
      int i1 = 0;
      int i2 = this.ab.size();
      while (i1 < i2)
      {
        OnPageChangeListener localOnPageChangeListener = (OnPageChangeListener)this.ab.get(i1);
        if (localOnPageChangeListener != null) {
          localOnPageChangeListener.onPageScrollStateChanged(paramInt);
        }
        i1 += 1;
      }
    }
    if (this.ad != null) {
      this.ad.onPageScrollStateChanged(paramInt);
    }
  }
  
  private int getClientWidth()
  {
    return getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
  }
  
  private void j()
  {
    int i2;
    for (int i1 = 0; i1 < getChildCount(); i1 = i2 + 1)
    {
      i2 = i1;
      if (!((LayoutParams)getChildAt(i1).getLayoutParams()).a)
      {
        removeViewAt(i1);
        i2 = i1 - 1;
      }
    }
  }
  
  private void k()
  {
    if (this.ah != 0)
    {
      if (this.ai == null) {
        this.ai = new ArrayList();
      }
      for (;;)
      {
        int i2 = getChildCount();
        int i1 = 0;
        while (i1 < i2)
        {
          View localView = getChildAt(i1);
          this.ai.add(localView);
          i1 += 1;
        }
        this.ai.clear();
      }
      Collections.sort(this.ai, aj);
    }
  }
  
  private boolean l()
  {
    this.K = -1;
    n();
    return this.S.c() | this.T.c();
  }
  
  private ItemInfo m()
  {
    float f2 = 0.0F;
    int i1 = getClientWidth();
    float f1;
    int i3;
    float f3;
    float f4;
    int i2;
    Object localObject2;
    if (i1 > 0)
    {
      f1 = getScrollX() / i1;
      if (i1 > 0) {
        f2 = this.p / i1;
      }
      i3 = -1;
      f3 = 0.0F;
      f4 = 0.0F;
      i2 = 1;
      localObject2 = null;
      i1 = 0;
    }
    for (;;)
    {
      Object localObject3 = localObject2;
      int i4;
      Object localObject1;
      if (i1 < this.e.size())
      {
        localObject3 = (ItemInfo)this.e.get(i1);
        i4 = i1;
        localObject1 = localObject3;
        if (i2 == 0)
        {
          i4 = i1;
          localObject1 = localObject3;
          if (((ItemInfo)localObject3).b != i3 + 1)
          {
            localObject1 = this.f;
            ((ItemInfo)localObject1).e = (f3 + f4 + f2);
            ((ItemInfo)localObject1).b = (i3 + 1);
            ((ItemInfo)localObject1).d = this.h.getPageWidth(((ItemInfo)localObject1).b);
            i4 = i1 - 1;
          }
        }
        f3 = ((ItemInfo)localObject1).e;
        f4 = ((ItemInfo)localObject1).d;
        if (i2 == 0)
        {
          localObject3 = localObject2;
          if (f1 < f3) {}
        }
        else
        {
          if ((f1 >= f4 + f3 + f2) && (i4 != this.e.size() - 1)) {
            break label232;
          }
          localObject3 = localObject1;
        }
      }
      return (ItemInfo)localObject3;
      f1 = 0.0F;
      break;
      label232:
      i2 = 0;
      i3 = ((ItemInfo)localObject1).b;
      f4 = ((ItemInfo)localObject1).d;
      i1 = i4 + 1;
      localObject2 = localObject1;
    }
  }
  
  private void n()
  {
    this.B = false;
    this.C = false;
    if (this.L != null)
    {
      this.L.recycle();
      this.L = null;
    }
  }
  
  private void setScrollState(int paramInt)
  {
    if (this.al == paramInt) {
      return;
    }
    this.al = paramInt;
    if (this.af != null) {
      if (paramInt == 0) {
        break label38;
      }
    }
    label38:
    for (boolean bool = true;; bool = false)
    {
      b(bool);
      f(paramInt);
      return;
    }
  }
  
  private void setScrollingCacheEnabled(boolean paramBoolean)
  {
    if (this.y != paramBoolean) {
      this.y = paramBoolean;
    }
  }
  
  float a(float paramFloat)
  {
    return (float)Math.sin((float)((paramFloat - 0.5F) * 0.4712389167638204D));
  }
  
  ItemInfo a(int paramInt1, int paramInt2)
  {
    ItemInfo localItemInfo = new ItemInfo();
    localItemInfo.b = paramInt1;
    localItemInfo.a = this.h.instantiateItem(this, paramInt1);
    localItemInfo.d = this.h.getPageWidth(paramInt1);
    if ((paramInt2 < 0) || (paramInt2 >= this.e.size()))
    {
      this.e.add(localItemInfo);
      return localItemInfo;
    }
    this.e.add(paramInt2, localItemInfo);
    return localItemInfo;
  }
  
  ItemInfo a(View paramView)
  {
    int i1 = 0;
    while (i1 < this.e.size())
    {
      ItemInfo localItemInfo = (ItemInfo)this.e.get(i1);
      if (this.h.isViewFromObject(paramView, localItemInfo.a)) {
        return localItemInfo;
      }
      i1 += 1;
    }
    return null;
  }
  
  void a()
  {
    setWillNotDraw(false);
    setDescendantFocusability(262144);
    setFocusable(true);
    Context localContext = getContext();
    this.m = new Scroller(localContext, d);
    ViewConfiguration localViewConfiguration = ViewConfiguration.get(localContext);
    float f1 = localContext.getResources().getDisplayMetrics().density;
    this.F = ViewConfigurationCompat.a(localViewConfiguration);
    this.M = ((int)(400.0F * f1));
    this.N = localViewConfiguration.getScaledMaximumFlingVelocity();
    this.S = new EdgeEffectCompat(localContext);
    this.T = new EdgeEffectCompat(localContext);
    this.O = ((int)(25.0F * f1));
    this.P = ((int)(2.0F * f1));
    this.D = ((int)(16.0F * f1));
    ViewCompat.a(this, new MyAccessibilityDelegate());
    if (ViewCompat.e(this) == 0) {
      ViewCompat.c(this, 1);
    }
    ViewCompat.a(this, new OnApplyWindowInsetsListener()
    {
      private final Rect b = new Rect();
      
      public WindowInsetsCompat a(View paramAnonymousView, WindowInsetsCompat paramAnonymousWindowInsetsCompat)
      {
        paramAnonymousView = ViewCompat.a(paramAnonymousView, paramAnonymousWindowInsetsCompat);
        if (paramAnonymousView.e()) {
          return paramAnonymousView;
        }
        paramAnonymousWindowInsetsCompat = this.b;
        paramAnonymousWindowInsetsCompat.left = paramAnonymousView.a();
        paramAnonymousWindowInsetsCompat.top = paramAnonymousView.b();
        paramAnonymousWindowInsetsCompat.right = paramAnonymousView.c();
        paramAnonymousWindowInsetsCompat.bottom = paramAnonymousView.d();
        int i = 0;
        int j = ViewPager.this.getChildCount();
        while (i < j)
        {
          WindowInsetsCompat localWindowInsetsCompat = ViewCompat.b(ViewPager.this.getChildAt(i), paramAnonymousView);
          paramAnonymousWindowInsetsCompat.left = Math.min(localWindowInsetsCompat.a(), paramAnonymousWindowInsetsCompat.left);
          paramAnonymousWindowInsetsCompat.top = Math.min(localWindowInsetsCompat.b(), paramAnonymousWindowInsetsCompat.top);
          paramAnonymousWindowInsetsCompat.right = Math.min(localWindowInsetsCompat.c(), paramAnonymousWindowInsetsCompat.right);
          paramAnonymousWindowInsetsCompat.bottom = Math.min(localWindowInsetsCompat.d(), paramAnonymousWindowInsetsCompat.bottom);
          i += 1;
        }
        return paramAnonymousView.a(paramAnonymousWindowInsetsCompat.left, paramAnonymousWindowInsetsCompat.top, paramAnonymousWindowInsetsCompat.right, paramAnonymousWindowInsetsCompat.bottom);
      }
    });
  }
  
  void a(int paramInt)
  {
    Object localObject2 = null;
    if (this.i != paramInt)
    {
      localObject2 = b(this.i);
      this.i = paramInt;
    }
    if (this.h == null) {
      k();
    }
    label350:
    label363:
    label448:
    label455:
    label638:
    label651:
    label672:
    label799:
    label808:
    label923:
    label929:
    label944:
    label1058:
    label1070:
    label1189:
    label1298:
    label1304:
    for (;;)
    {
      return;
      if (this.z)
      {
        k();
        return;
      }
      if (getWindowToken() != null)
      {
        this.h.startUpdate(this);
        paramInt = this.A;
        int i7 = Math.max(0, this.i - paramInt);
        int i5 = this.h.getCount();
        int i6 = Math.min(i5 - 1, this.i + paramInt);
        if (i5 != this.b) {
          try
          {
            String str = getResources().getResourceName(getId());
            throw new IllegalStateException("The application's PagerAdapter changed the adapter's contents without calling PagerAdapter#notifyDataSetChanged! Expected adapter item count: " + this.b + ", found: " + i5 + " Pager id: " + str + " Pager class: " + getClass() + " Problematic adapter: " + this.h.getClass());
          }
          catch (Resources.NotFoundException localNotFoundException)
          {
            for (;;)
            {
              localObject1 = Integer.toHexString(getId());
            }
          }
        }
        Object localObject3 = null;
        paramInt = 0;
        Object localObject1 = localObject3;
        Object localObject4;
        if (paramInt < this.e.size())
        {
          localObject4 = (ItemInfo)this.e.get(paramInt);
          if (((ItemInfo)localObject4).b < this.i) {
            break label638;
          }
          localObject1 = localObject3;
          if (((ItemInfo)localObject4).b == this.i) {
            localObject1 = localObject4;
          }
        }
        localObject3 = localObject1;
        if (localObject1 == null)
        {
          localObject3 = localObject1;
          if (i5 > 0) {
            localObject3 = a(this.i, paramInt);
          }
        }
        float f3;
        int i4;
        int i8;
        float f2;
        int i3;
        int i2;
        int i1;
        if (localObject3 != null)
        {
          f3 = 0.0F;
          i4 = paramInt - 1;
          if (i4 >= 0)
          {
            localObject1 = (ItemInfo)this.e.get(i4);
            i8 = getClientWidth();
            if (i8 > 0) {
              break label651;
            }
            f2 = 0.0F;
            i3 = this.i - 1;
            localObject4 = localObject1;
            i2 = paramInt;
            if (i3 >= 0)
            {
              if ((f3 < f2) || (i3 >= i7)) {
                break label808;
              }
              if (localObject4 != null) {
                break label672;
              }
            }
            f3 = ((ItemInfo)localObject3).d;
            i3 = i2 + 1;
            if (f3 < 2.0F)
            {
              if (i3 >= this.e.size()) {
                break label923;
              }
              localObject1 = (ItemInfo)this.e.get(i3);
              if (i8 > 0) {
                break label929;
              }
              f2 = 0.0F;
              i1 = this.i + 1;
              localObject4 = localObject1;
              if (i1 < i5)
              {
                if ((f3 < f2) || (i1 <= i6)) {
                  break label1070;
                }
                if (localObject4 != null) {
                  break label944;
                }
              }
            }
            a((ItemInfo)localObject3, i2, (ItemInfo)localObject2);
          }
        }
        else
        {
          localObject2 = this.h;
          paramInt = this.i;
          if (localObject3 == null) {
            break label1189;
          }
        }
        for (localObject1 = ((ItemInfo)localObject3).a;; localObject1 = null)
        {
          ((PagerAdapter)localObject2).setPrimaryItem(this, paramInt, localObject1);
          this.h.finishUpdate(this);
          i1 = getChildCount();
          paramInt = 0;
          while (paramInt < i1)
          {
            localObject2 = getChildAt(paramInt);
            localObject1 = (LayoutParams)((View)localObject2).getLayoutParams();
            ((LayoutParams)localObject1).f = paramInt;
            if ((!((LayoutParams)localObject1).a) && (((LayoutParams)localObject1).c == 0.0F))
            {
              localObject2 = a((View)localObject2);
              if (localObject2 != null)
              {
                ((LayoutParams)localObject1).c = ((ItemInfo)localObject2).d;
                ((LayoutParams)localObject1).e = ((ItemInfo)localObject2).b;
              }
            }
            paramInt += 1;
          }
          paramInt += 1;
          break;
          localObject1 = null;
          break label350;
          f2 = 2.0F - ((ItemInfo)localObject3).d + getPaddingLeft() / i8;
          break label363;
          paramInt = i2;
          float f1 = f3;
          localObject1 = localObject4;
          i1 = i4;
          if (i3 == ((ItemInfo)localObject4).b)
          {
            paramInt = i2;
            f1 = f3;
            localObject1 = localObject4;
            i1 = i4;
            if (!((ItemInfo)localObject4).c)
            {
              this.e.remove(i4);
              this.h.destroyItem(this, i3, ((ItemInfo)localObject4).a);
              i1 = i4 - 1;
              paramInt = i2 - 1;
              if (i1 < 0) {
                break label799;
              }
              localObject1 = (ItemInfo)this.e.get(i1);
            }
          }
          for (f1 = f3;; f1 = f3)
          {
            i3 -= 1;
            i2 = paramInt;
            f3 = f1;
            localObject4 = localObject1;
            i4 = i1;
            break;
            localObject1 = null;
          }
          if ((localObject4 != null) && (i3 == ((ItemInfo)localObject4).b))
          {
            f1 = f3 + ((ItemInfo)localObject4).d;
            i1 = i4 - 1;
            if (i1 >= 0) {}
            for (localObject1 = (ItemInfo)this.e.get(i1);; localObject1 = null)
            {
              paramInt = i2;
              break;
            }
          }
          f1 = f3 + a(i3, i4 + 1).d;
          paramInt = i2 + 1;
          if (i4 >= 0) {}
          for (localObject1 = (ItemInfo)this.e.get(i4);; localObject1 = null)
          {
            i1 = i4;
            break;
          }
          localObject1 = null;
          break label448;
          f2 = getPaddingRight() / i8 + 2.0F;
          break label455;
          f1 = f3;
          localObject1 = localObject4;
          paramInt = i3;
          if (i1 == ((ItemInfo)localObject4).b)
          {
            f1 = f3;
            localObject1 = localObject4;
            paramInt = i3;
            if (!((ItemInfo)localObject4).c)
            {
              this.e.remove(i3);
              this.h.destroyItem(this, i1, ((ItemInfo)localObject4).a);
              if (i3 >= this.e.size()) {
                break label1058;
              }
              localObject1 = (ItemInfo)this.e.get(i3);
              paramInt = i3;
              f1 = f3;
            }
          }
          for (;;)
          {
            i1 += 1;
            f3 = f1;
            localObject4 = localObject1;
            i3 = paramInt;
            break;
            localObject1 = null;
            f1 = f3;
            paramInt = i3;
          }
          if ((localObject4 != null) && (i1 == ((ItemInfo)localObject4).b))
          {
            f1 = f3 + ((ItemInfo)localObject4).d;
            paramInt = i3 + 1;
            if (paramInt < this.e.size()) {}
            for (localObject1 = (ItemInfo)this.e.get(paramInt);; localObject1 = null) {
              break;
            }
          }
          localObject1 = a(i1, i3);
          paramInt = i3 + 1;
          f1 = f3 + ((ItemInfo)localObject1).d;
          if (paramInt < this.e.size()) {}
          for (localObject1 = (ItemInfo)this.e.get(paramInt);; localObject1 = null) {
            break;
          }
        }
        k();
        if (hasFocus())
        {
          localObject1 = findFocus();
          if (localObject1 != null) {}
          for (localObject1 = b((View)localObject1);; localObject1 = null)
          {
            if ((localObject1 != null) && (((ItemInfo)localObject1).b == this.i)) {
              break label1304;
            }
            paramInt = 0;
            for (;;)
            {
              if (paramInt >= getChildCount()) {
                break label1298;
              }
              localObject1 = getChildAt(paramInt);
              localObject2 = a((View)localObject1);
              if ((localObject2 != null) && (((ItemInfo)localObject2).b == this.i) && (((View)localObject1).requestFocus(2))) {
                break;
              }
              paramInt += 1;
            }
            break;
          }
        }
      }
    }
  }
  
  @CallSuper
  protected void a(int paramInt1, float paramFloat, int paramInt2)
  {
    int i1;
    View localView;
    if (this.aa > 0)
    {
      int i6 = getScrollX();
      i1 = getPaddingLeft();
      int i3 = getPaddingRight();
      int i7 = getWidth();
      int i8 = getChildCount();
      int i4 = 0;
      while (i4 < i8)
      {
        localView = getChildAt(i4);
        LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
        int i2;
        int i5;
        if (!localLayoutParams.a)
        {
          i2 = i3;
          i5 = i1;
          i4 += 1;
          i1 = i5;
          i3 = i2;
        }
        else
        {
          switch (localLayoutParams.b & 0x7)
          {
          case 2: 
          case 4: 
          default: 
            i2 = i1;
          }
          for (;;)
          {
            int i9 = i2 + i6 - localView.getLeft();
            i5 = i1;
            i2 = i3;
            if (i9 == 0) {
              break;
            }
            localView.offsetLeftAndRight(i9);
            i5 = i1;
            i2 = i3;
            break;
            i2 = i1;
            i1 += localView.getWidth();
            continue;
            i2 = Math.max((i7 - localView.getMeasuredWidth()) / 2, i1);
            continue;
            i2 = i7 - i3 - localView.getMeasuredWidth();
            i3 += localView.getMeasuredWidth();
          }
        }
      }
    }
    b(paramInt1, paramFloat, paramInt2);
    if (this.af != null)
    {
      paramInt2 = getScrollX();
      i1 = getChildCount();
      paramInt1 = 0;
      if (paramInt1 < i1)
      {
        localView = getChildAt(paramInt1);
        if (((LayoutParams)localView.getLayoutParams()).a) {}
        for (;;)
        {
          paramInt1 += 1;
          break;
          paramFloat = (localView.getLeft() - paramInt2) / getClientWidth();
          this.af.a(localView, paramFloat);
        }
      }
    }
    this.W = true;
  }
  
  void a(int paramInt1, int paramInt2, int paramInt3)
  {
    if (getChildCount() == 0)
    {
      setScrollingCacheEnabled(false);
      return;
    }
    int i1;
    if ((this.m != null) && (!this.m.isFinished()))
    {
      i1 = 1;
      if (i1 == 0) {
        break label125;
      }
      if (!this.n) {
        break label113;
      }
      i1 = this.m.getCurrX();
      label54:
      this.m.abortAnimation();
      setScrollingCacheEnabled(false);
    }
    int i2;
    int i3;
    for (;;)
    {
      i2 = getScrollY();
      i3 = paramInt1 - i1;
      paramInt2 -= i2;
      if ((i3 != 0) || (paramInt2 != 0)) {
        break label134;
      }
      a(false);
      c();
      setScrollState(0);
      return;
      i1 = 0;
      break;
      label113:
      i1 = this.m.getStartX();
      break label54;
      label125:
      i1 = getScrollX();
    }
    label134:
    setScrollingCacheEnabled(true);
    setScrollState(2);
    paramInt1 = getClientWidth();
    int i4 = paramInt1 / 2;
    float f3 = Math.min(1.0F, 1.0F * Math.abs(i3) / paramInt1);
    float f1 = i4;
    float f2 = i4;
    f3 = a(f3);
    paramInt3 = Math.abs(paramInt3);
    if (paramInt3 > 0) {}
    for (paramInt1 = Math.round(1000.0F * Math.abs((f1 + f2 * f3) / paramInt3)) * 4;; paramInt1 = (int)((1.0F + Math.abs(i3) / (this.p + f1 * f2)) * 100.0F))
    {
      paramInt1 = Math.min(paramInt1, 600);
      this.n = false;
      this.m.startScroll(i1, i2, i3, paramInt2, paramInt1);
      ViewCompat.d(this);
      return;
      f1 = paramInt1;
      f2 = this.h.getPageWidth(this.i);
    }
  }
  
  void a(int paramInt, boolean paramBoolean1, boolean paramBoolean2)
  {
    a(paramInt, paramBoolean1, paramBoolean2, 0);
  }
  
  void a(int paramInt1, boolean paramBoolean1, boolean paramBoolean2, int paramInt2)
  {
    boolean bool = true;
    if ((this.h == null) || (this.h.getCount() <= 0))
    {
      setScrollingCacheEnabled(false);
      return;
    }
    if ((!paramBoolean2) && (this.i == paramInt1) && (this.e.size() != 0))
    {
      setScrollingCacheEnabled(false);
      return;
    }
    int i1;
    if (paramInt1 < 0) {
      i1 = 0;
    }
    for (;;)
    {
      paramInt1 = this.A;
      if ((i1 <= this.i + paramInt1) && (i1 >= this.i - paramInt1)) {
        break;
      }
      paramInt1 = 0;
      while (paramInt1 < this.e.size())
      {
        ((ItemInfo)this.e.get(paramInt1)).c = true;
        paramInt1 += 1;
      }
      i1 = paramInt1;
      if (paramInt1 >= this.h.getCount()) {
        i1 = this.h.getCount() - 1;
      }
    }
    if (this.i != i1) {}
    for (paramBoolean2 = bool; this.U; paramBoolean2 = false)
    {
      this.i = i1;
      if (paramBoolean2) {
        e(i1);
      }
      requestLayout();
      return;
    }
    a(i1);
    a(i1, paramBoolean1, paramInt2, paramBoolean2);
  }
  
  public void a(OnPageChangeListener paramOnPageChangeListener)
  {
    if (this.ab == null) {
      this.ab = new ArrayList();
    }
    this.ab.add(paramOnPageChangeListener);
  }
  
  public boolean a(KeyEvent paramKeyEvent)
  {
    if (paramKeyEvent.getAction() == 0) {
      switch (paramKeyEvent.getKeyCode())
      {
      }
    }
    do
    {
      do
      {
        return false;
        return c(17);
        return c(66);
      } while (Build.VERSION.SDK_INT < 11);
      if (KeyEventCompat.a(paramKeyEvent)) {
        return c(2);
      }
    } while (!KeyEventCompat.a(paramKeyEvent, 1));
    return c(1);
  }
  
  protected boolean a(View paramView, boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3)
  {
    if ((paramView instanceof ViewGroup))
    {
      ViewGroup localViewGroup = (ViewGroup)paramView;
      int i2 = paramView.getScrollX();
      int i3 = paramView.getScrollY();
      int i1 = localViewGroup.getChildCount() - 1;
      while (i1 >= 0)
      {
        View localView = localViewGroup.getChildAt(i1);
        if ((paramInt2 + i2 >= localView.getLeft()) && (paramInt2 + i2 < localView.getRight()) && (paramInt3 + i3 >= localView.getTop()) && (paramInt3 + i3 < localView.getBottom()) && (a(localView, true, paramInt1, paramInt2 + i2 - localView.getLeft(), paramInt3 + i3 - localView.getTop()))) {
          return true;
        }
        i1 -= 1;
      }
    }
    return (paramBoolean) && (ViewCompat.a(paramView, -paramInt1));
  }
  
  public void addFocusables(ArrayList<View> paramArrayList, int paramInt1, int paramInt2)
  {
    int i2 = paramArrayList.size();
    int i3 = getDescendantFocusability();
    if (i3 != 393216)
    {
      int i1 = 0;
      while (i1 < getChildCount())
      {
        View localView = getChildAt(i1);
        if (localView.getVisibility() == 0)
        {
          ItemInfo localItemInfo = a(localView);
          if ((localItemInfo != null) && (localItemInfo.b == this.i)) {
            localView.addFocusables(paramArrayList, paramInt1, paramInt2);
          }
        }
        i1 += 1;
      }
    }
    if (((i3 == 262144) && (i2 != paramArrayList.size())) || (!isFocusable())) {}
    while ((((paramInt2 & 0x1) == 1) && (isInTouchMode()) && (!isFocusableInTouchMode())) || (paramArrayList == null)) {
      return;
    }
    paramArrayList.add(this);
  }
  
  public void addTouchables(ArrayList<View> paramArrayList)
  {
    int i1 = 0;
    while (i1 < getChildCount())
    {
      View localView = getChildAt(i1);
      if (localView.getVisibility() == 0)
      {
        ItemInfo localItemInfo = a(localView);
        if ((localItemInfo != null) && (localItemInfo.b == this.i)) {
          localView.addTouchables(paramArrayList);
        }
      }
      i1 += 1;
    }
  }
  
  public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams)
  {
    ViewGroup.LayoutParams localLayoutParams = paramLayoutParams;
    if (!checkLayoutParams(paramLayoutParams)) {
      localLayoutParams = generateLayoutParams(paramLayoutParams);
    }
    paramLayoutParams = (LayoutParams)localLayoutParams;
    paramLayoutParams.a |= paramView instanceof Decor;
    if (this.x)
    {
      if ((paramLayoutParams != null) && (paramLayoutParams.a)) {
        throw new IllegalStateException("Cannot add pager decor view during layout");
      }
      paramLayoutParams.d = true;
      addViewInLayout(paramView, paramInt, localLayoutParams);
      return;
    }
    super.addView(paramView, paramInt, localLayoutParams);
  }
  
  ItemInfo b(int paramInt)
  {
    int i1 = 0;
    while (i1 < this.e.size())
    {
      ItemInfo localItemInfo = (ItemInfo)this.e.get(i1);
      if (localItemInfo.b == paramInt) {
        return localItemInfo;
      }
      i1 += 1;
    }
    return null;
  }
  
  ItemInfo b(View paramView)
  {
    for (;;)
    {
      ViewParent localViewParent = paramView.getParent();
      if (localViewParent == this) {
        break;
      }
      if ((localViewParent == null) || (!(localViewParent instanceof View))) {
        return null;
      }
      paramView = (View)localViewParent;
    }
    return a(paramView);
  }
  
  void b()
  {
    int i9 = this.h.getCount();
    this.b = i9;
    int i1;
    int i2;
    int i3;
    int i4;
    label57:
    Object localObject;
    int i8;
    int i5;
    int i6;
    int i7;
    if ((this.e.size() < this.A * 2 + 1) && (this.e.size() < i9))
    {
      i1 = 1;
      i2 = this.i;
      i3 = 0;
      i4 = 0;
      if (i4 >= this.e.size()) {
        break label304;
      }
      localObject = (ItemInfo)this.e.get(i4);
      i8 = this.h.getItemPosition(((ItemInfo)localObject).a);
      if (i8 != -1) {
        break label133;
      }
      i5 = i2;
      i6 = i3;
      i7 = i4;
    }
    for (;;)
    {
      i4 = i7 + 1;
      i3 = i6;
      i2 = i5;
      break label57;
      i1 = 0;
      break;
      label133:
      if (i8 == -2)
      {
        this.e.remove(i4);
        i8 = i4 - 1;
        i4 = i3;
        if (i3 == 0)
        {
          this.h.startUpdate(this);
          i4 = 1;
        }
        this.h.destroyItem(this, ((ItemInfo)localObject).b, ((ItemInfo)localObject).a);
        i1 = 1;
        i7 = i8;
        i6 = i4;
        i5 = i2;
        if (this.i == ((ItemInfo)localObject).b)
        {
          i5 = Math.max(0, Math.min(this.i, i9 - 1));
          i1 = 1;
          i7 = i8;
          i6 = i4;
        }
      }
      else
      {
        i7 = i4;
        i6 = i3;
        i5 = i2;
        if (((ItemInfo)localObject).b != i8)
        {
          if (((ItemInfo)localObject).b == this.i) {
            i2 = i8;
          }
          ((ItemInfo)localObject).b = i8;
          i1 = 1;
          i7 = i4;
          i6 = i3;
          i5 = i2;
        }
      }
    }
    label304:
    if (i3 != 0) {
      this.h.finishUpdate(this);
    }
    Collections.sort(this.e, c);
    if (i1 != 0)
    {
      i3 = getChildCount();
      i1 = 0;
      while (i1 < i3)
      {
        localObject = (LayoutParams)getChildAt(i1).getLayoutParams();
        if (!((LayoutParams)localObject).a) {
          ((LayoutParams)localObject).c = 0.0F;
        }
        i1 += 1;
      }
      a(i2, false, true);
      requestLayout();
    }
  }
  
  public void b(float paramFloat)
  {
    if (!this.Q) {
      throw new IllegalStateException("No fake drag in progress. Call beginFakeDrag first.");
    }
    if (this.h == null) {
      return;
    }
    this.G += paramFloat;
    float f2 = getScrollX() - paramFloat;
    int i1 = getClientWidth();
    paramFloat = i1 * this.t;
    float f1 = i1 * this.u;
    Object localObject = (ItemInfo)this.e.get(0);
    ItemInfo localItemInfo = (ItemInfo)this.e.get(this.e.size() - 1);
    if (((ItemInfo)localObject).b != 0) {
      paramFloat = ((ItemInfo)localObject).e * i1;
    }
    if (localItemInfo.b != this.h.getCount() - 1) {
      f1 = localItemInfo.e * i1;
    }
    if (f2 < paramFloat) {}
    for (;;)
    {
      this.G += paramFloat - (int)paramFloat;
      scrollTo((int)paramFloat, getScrollY());
      d((int)paramFloat);
      long l1 = SystemClock.uptimeMillis();
      localObject = MotionEvent.obtain(this.R, l1, 2, this.G, 0.0F, 0);
      this.L.addMovement((MotionEvent)localObject);
      ((MotionEvent)localObject).recycle();
      return;
      paramFloat = f2;
      if (f2 > f1) {
        paramFloat = f1;
      }
    }
  }
  
  public void b(OnPageChangeListener paramOnPageChangeListener)
  {
    if (this.ab != null) {
      this.ab.remove(paramOnPageChangeListener);
    }
  }
  
  OnPageChangeListener c(OnPageChangeListener paramOnPageChangeListener)
  {
    OnPageChangeListener localOnPageChangeListener = this.ad;
    this.ad = paramOnPageChangeListener;
    return localOnPageChangeListener;
  }
  
  void c()
  {
    a(this.i);
  }
  
  public boolean c(int paramInt)
  {
    View localView = findFocus();
    Object localObject;
    boolean bool;
    int i1;
    int i2;
    if (localView == this)
    {
      localObject = null;
      bool = false;
      localView = FocusFinder.getInstance().findNextFocus(this, (View)localObject, paramInt);
      if ((localView == null) || (localView == localObject)) {
        break label344;
      }
      if (paramInt != 17) {
        break label281;
      }
      i1 = a(this.g, localView).left;
      i2 = a(this.g, (View)localObject).left;
      if ((localObject == null) || (i1 < i2)) {
        break label271;
      }
      bool = g();
    }
    for (;;)
    {
      if (bool) {
        playSoundEffect(SoundEffectConstants.getContantForFocusDirection(paramInt));
      }
      return bool;
      localObject = localView;
      if (localView == null) {
        break;
      }
      i2 = 0;
      StringBuilder localStringBuilder;
      for (localObject = localView.getParent();; localObject = ((ViewParent)localObject).getParent())
      {
        i1 = i2;
        if ((localObject instanceof ViewGroup))
        {
          if (localObject == this) {
            i1 = 1;
          }
        }
        else
        {
          localObject = localView;
          if (i1 != 0) {
            break;
          }
          localStringBuilder = new StringBuilder();
          localStringBuilder.append(localView.getClass().getSimpleName());
          for (localObject = localView.getParent(); (localObject instanceof ViewGroup); localObject = ((ViewParent)localObject).getParent()) {
            localStringBuilder.append(" => ").append(localObject.getClass().getSimpleName());
          }
        }
      }
      Log.e("ViewPager", "arrowScroll tried to find focus based on non-child current focused view " + localStringBuilder.toString());
      localObject = null;
      break;
      label271:
      bool = localView.requestFocus();
      continue;
      label281:
      if (paramInt == 66)
      {
        i1 = a(this.g, localView).left;
        i2 = a(this.g, (View)localObject).left;
        if ((localObject != null) && (i1 <= i2))
        {
          bool = h();
        }
        else
        {
          bool = localView.requestFocus();
          continue;
          label344:
          if ((paramInt == 17) || (paramInt == 1)) {
            bool = g();
          } else if ((paramInt == 66) || (paramInt == 2)) {
            bool = h();
          }
        }
      }
    }
  }
  
  public boolean canScrollHorizontally(int paramInt)
  {
    boolean bool2 = true;
    boolean bool1 = true;
    if (this.h == null) {}
    int i1;
    int i2;
    do
    {
      return false;
      i1 = getClientWidth();
      i2 = getScrollX();
      if (paramInt < 0)
      {
        if (i2 > (int)(i1 * this.t)) {}
        for (;;)
        {
          return bool1;
          bool1 = false;
        }
      }
    } while (paramInt <= 0);
    if (i2 < (int)(i1 * this.u)) {}
    for (bool1 = bool2;; bool1 = false) {
      return bool1;
    }
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return ((paramLayoutParams instanceof LayoutParams)) && (super.checkLayoutParams(paramLayoutParams));
  }
  
  public void computeScroll()
  {
    this.n = true;
    if ((!this.m.isFinished()) && (this.m.computeScrollOffset()))
    {
      int i1 = getScrollX();
      int i2 = getScrollY();
      int i3 = this.m.getCurrX();
      int i4 = this.m.getCurrY();
      if ((i1 != i3) || (i2 != i4))
      {
        scrollTo(i3, i4);
        if (!d(i3))
        {
          this.m.abortAnimation();
          scrollTo(0, i4);
        }
      }
      ViewCompat.d(this);
      return;
    }
    a(true);
  }
  
  public boolean d()
  {
    if (this.B) {
      return false;
    }
    this.Q = true;
    setScrollState(1);
    this.G = 0.0F;
    this.I = 0.0F;
    if (this.L == null) {
      this.L = VelocityTracker.obtain();
    }
    for (;;)
    {
      long l1 = SystemClock.uptimeMillis();
      MotionEvent localMotionEvent = MotionEvent.obtain(l1, l1, 0, 0.0F, 0.0F, 0);
      this.L.addMovement(localMotionEvent);
      localMotionEvent.recycle();
      this.R = l1;
      return true;
      this.L.clear();
    }
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
  {
    return (super.dispatchKeyEvent(paramKeyEvent)) || (a(paramKeyEvent));
  }
  
  public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
  {
    if (paramAccessibilityEvent.getEventType() == 4096) {
      return super.dispatchPopulateAccessibilityEvent(paramAccessibilityEvent);
    }
    int i2 = getChildCount();
    int i1 = 0;
    while (i1 < i2)
    {
      View localView = getChildAt(i1);
      if (localView.getVisibility() == 0)
      {
        ItemInfo localItemInfo = a(localView);
        if ((localItemInfo != null) && (localItemInfo.b == this.i) && (localView.dispatchPopulateAccessibilityEvent(paramAccessibilityEvent))) {
          return true;
        }
      }
      i1 += 1;
    }
    return false;
  }
  
  public void draw(Canvas paramCanvas)
  {
    super.draw(paramCanvas);
    int i3 = 0;
    int i1 = 0;
    int i4 = ViewCompat.a(this);
    boolean bool;
    if ((i4 == 0) || ((i4 == 1) && (this.h != null) && (this.h.getCount() > 1)))
    {
      int i2;
      if (!this.S.a())
      {
        i3 = paramCanvas.save();
        i1 = getHeight() - getPaddingTop() - getPaddingBottom();
        i4 = getWidth();
        paramCanvas.rotate(270.0F);
        paramCanvas.translate(-i1 + getPaddingTop(), this.t * i4);
        this.S.a(i1, i4);
        i2 = false | this.S.a(paramCanvas);
        paramCanvas.restoreToCount(i3);
      }
      i3 = i2;
      if (!this.T.a())
      {
        i4 = paramCanvas.save();
        i3 = getWidth();
        int i5 = getHeight();
        int i6 = getPaddingTop();
        int i7 = getPaddingBottom();
        paramCanvas.rotate(90.0F);
        paramCanvas.translate(-getPaddingTop(), -(this.u + 1.0F) * i3);
        this.T.a(i5 - i6 - i7, i3);
        bool = i2 | this.T.a(paramCanvas);
        paramCanvas.restoreToCount(i4);
      }
    }
    for (;;)
    {
      if (bool) {
        ViewCompat.d(this);
      }
      return;
      this.S.b();
      this.T.b();
    }
  }
  
  protected void drawableStateChanged()
  {
    super.drawableStateChanged();
    Drawable localDrawable = this.q;
    if ((localDrawable != null) && (localDrawable.isStateful())) {
      localDrawable.setState(getDrawableState());
    }
  }
  
  public void e()
  {
    if (!this.Q) {
      throw new IllegalStateException("No fake drag in progress. Call beginFakeDrag first.");
    }
    if (this.h != null)
    {
      Object localObject = this.L;
      ((VelocityTracker)localObject).computeCurrentVelocity(1000, this.N);
      int i1 = (int)VelocityTrackerCompat.a((VelocityTracker)localObject, this.K);
      this.z = true;
      int i2 = getClientWidth();
      int i3 = getScrollX();
      localObject = m();
      a(a(((ItemInfo)localObject).b, (i3 / i2 - ((ItemInfo)localObject).e) / ((ItemInfo)localObject).d, i1, (int)(this.G - this.I)), true, true, i1);
    }
    n();
    this.Q = false;
  }
  
  public boolean f()
  {
    return this.Q;
  }
  
  boolean g()
  {
    if (this.i > 0)
    {
      setCurrentItem(this.i - 1, true);
      return true;
    }
    return false;
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams()
  {
    return new LayoutParams();
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
  {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return generateDefaultLayoutParams();
  }
  
  public PagerAdapter getAdapter()
  {
    return this.h;
  }
  
  protected int getChildDrawingOrder(int paramInt1, int paramInt2)
  {
    if (this.ah == 2) {}
    for (paramInt1 = paramInt1 - 1 - paramInt2;; paramInt1 = paramInt2) {
      return ((LayoutParams)((View)this.ai.get(paramInt1)).getLayoutParams()).f;
    }
  }
  
  public int getCurrentItem()
  {
    return this.i;
  }
  
  public int getOffscreenPageLimit()
  {
    return this.A;
  }
  
  public int getPageMargin()
  {
    return this.p;
  }
  
  boolean h()
  {
    if ((this.h != null) && (this.i < this.h.getCount() - 1))
    {
      setCurrentItem(this.i + 1, true);
      return true;
    }
    return false;
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    this.U = true;
  }
  
  protected void onDetachedFromWindow()
  {
    removeCallbacks(this.ak);
    if ((this.m != null) && (!this.m.isFinished())) {
      this.m.abortAnimation();
    }
    super.onDetachedFromWindow();
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    int i3;
    int i4;
    float f3;
    int i2;
    Object localObject;
    float f1;
    int i5;
    int i1;
    int i6;
    if ((this.p > 0) && (this.q != null) && (this.e.size() > 0) && (this.h != null))
    {
      i3 = getScrollX();
      i4 = getWidth();
      f3 = this.p / i4;
      i2 = 0;
      localObject = (ItemInfo)this.e.get(0);
      f1 = ((ItemInfo)localObject).e;
      i5 = this.e.size();
      i1 = ((ItemInfo)localObject).b;
      i6 = ((ItemInfo)this.e.get(i5 - 1)).b;
    }
    for (;;)
    {
      float f2;
      if (i1 < i6)
      {
        while ((i1 > ((ItemInfo)localObject).b) && (i2 < i5))
        {
          localObject = this.e;
          i2 += 1;
          localObject = (ItemInfo)((ArrayList)localObject).get(i2);
        }
        if (i1 != ((ItemInfo)localObject).b) {
          break label271;
        }
        f2 = (((ItemInfo)localObject).e + ((ItemInfo)localObject).d) * i4;
      }
      label271:
      float f4;
      for (f1 = ((ItemInfo)localObject).e + ((ItemInfo)localObject).d + f3;; f1 += f4 + f3)
      {
        if (this.p + f2 > i3)
        {
          this.q.setBounds(Math.round(f2), this.r, Math.round(this.p + f2), this.s);
          this.q.draw(paramCanvas);
        }
        if (f2 <= i3 + i4) {
          break;
        }
        return;
        f4 = this.h.getPageWidth(i1);
        f2 = (f1 + f4) * i4;
      }
      i1 += 1;
    }
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    int i1 = paramMotionEvent.getAction() & 0xFF;
    if ((i1 == 3) || (i1 == 1))
    {
      l();
      return false;
    }
    if (i1 != 0)
    {
      if (this.B) {
        return true;
      }
      if (this.C) {
        return false;
      }
    }
    switch (i1)
    {
    }
    for (;;)
    {
      if (this.L == null) {
        this.L = VelocityTracker.obtain();
      }
      this.L.addMovement(paramMotionEvent);
      return this.B;
      i1 = this.K;
      if (i1 != -1)
      {
        i1 = MotionEventCompat.a(paramMotionEvent, i1);
        float f2 = MotionEventCompat.c(paramMotionEvent, i1);
        float f1 = f2 - this.G;
        float f4 = Math.abs(f1);
        float f3 = MotionEventCompat.d(paramMotionEvent, i1);
        float f5 = Math.abs(f3 - this.J);
        if ((f1 != 0.0F) && (!a(this.G, f1)) && (a(this, false, (int)f1, (int)f2, (int)f3)))
        {
          this.G = f2;
          this.H = f3;
          this.C = true;
          return false;
        }
        if ((f4 > this.F) && (0.5F * f4 > f5))
        {
          this.B = true;
          c(true);
          setScrollState(1);
          if (f1 > 0.0F)
          {
            f1 = this.I + this.F;
            label282:
            this.G = f1;
            this.H = f3;
            setScrollingCacheEnabled(true);
          }
        }
        while ((this.B) && (c(f2)))
        {
          ViewCompat.d(this);
          break;
          f1 = this.I - this.F;
          break label282;
          if (f5 > this.F) {
            this.C = true;
          }
        }
        f1 = paramMotionEvent.getX();
        this.I = f1;
        this.G = f1;
        f1 = paramMotionEvent.getY();
        this.J = f1;
        this.H = f1;
        this.K = MotionEventCompat.b(paramMotionEvent, 0);
        this.C = false;
        this.n = true;
        this.m.computeScrollOffset();
        if ((this.al == 2) && (Math.abs(this.m.getFinalX() - this.m.getCurrX()) > this.P))
        {
          this.m.abortAnimation();
          this.z = false;
          c();
          this.B = true;
          c(true);
          setScrollState(1);
        }
        else
        {
          a(false);
          this.B = false;
          continue;
          a(paramMotionEvent);
        }
      }
    }
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i8 = getChildCount();
    int i10 = paramInt3 - paramInt1;
    int i9 = paramInt4 - paramInt2;
    paramInt2 = getPaddingLeft();
    paramInt1 = getPaddingTop();
    int i1 = getPaddingRight();
    paramInt4 = getPaddingBottom();
    int i11 = getScrollX();
    int i3 = 0;
    int i4 = 0;
    View localView;
    int i2;
    LayoutParams localLayoutParams;
    if (i4 < i8)
    {
      localView = getChildAt(i4);
      int i7 = i3;
      int i6 = paramInt4;
      i2 = paramInt2;
      int i5 = i1;
      paramInt3 = paramInt1;
      if (localView.getVisibility() != 8)
      {
        localLayoutParams = (LayoutParams)localView.getLayoutParams();
        i7 = i3;
        i6 = paramInt4;
        i2 = paramInt2;
        i5 = i1;
        paramInt3 = paramInt1;
        if (localLayoutParams.a)
        {
          paramInt3 = localLayoutParams.b;
          i5 = localLayoutParams.b;
          switch (paramInt3 & 0x7)
          {
          case 2: 
          case 4: 
          default: 
            paramInt3 = paramInt2;
            i2 = paramInt2;
            label190:
            switch (i5 & 0x70)
            {
            default: 
              paramInt2 = paramInt1;
            }
            break;
          }
        }
      }
      for (;;)
      {
        paramInt3 += i11;
        localView.layout(paramInt3, paramInt2, localView.getMeasuredWidth() + paramInt3, localView.getMeasuredHeight() + paramInt2);
        i7 = i3 + 1;
        paramInt3 = paramInt1;
        i5 = i1;
        i6 = paramInt4;
        i4 += 1;
        i3 = i7;
        paramInt4 = i6;
        paramInt2 = i2;
        i1 = i5;
        paramInt1 = paramInt3;
        break;
        paramInt3 = paramInt2;
        i2 = paramInt2 + localView.getMeasuredWidth();
        break label190;
        paramInt3 = Math.max((i10 - localView.getMeasuredWidth()) / 2, paramInt2);
        i2 = paramInt2;
        break label190;
        paramInt3 = i10 - i1 - localView.getMeasuredWidth();
        i1 += localView.getMeasuredWidth();
        i2 = paramInt2;
        break label190;
        paramInt2 = paramInt1;
        paramInt1 += localView.getMeasuredHeight();
        continue;
        paramInt2 = Math.max((i9 - localView.getMeasuredHeight()) / 2, paramInt1);
        continue;
        paramInt2 = i9 - paramInt4 - localView.getMeasuredHeight();
        paramInt4 += localView.getMeasuredHeight();
      }
    }
    i1 = i10 - paramInt2 - i1;
    paramInt3 = 0;
    while (paramInt3 < i8)
    {
      localView = getChildAt(paramInt3);
      if (localView.getVisibility() != 8)
      {
        localLayoutParams = (LayoutParams)localView.getLayoutParams();
        if (!localLayoutParams.a)
        {
          ItemInfo localItemInfo = a(localView);
          if (localItemInfo != null)
          {
            i2 = paramInt2 + (int)(i1 * localItemInfo.e);
            if (localLayoutParams.d)
            {
              localLayoutParams.d = false;
              localView.measure(View.MeasureSpec.makeMeasureSpec((int)(i1 * localLayoutParams.c), 1073741824), View.MeasureSpec.makeMeasureSpec(i9 - paramInt1 - paramInt4, 1073741824));
            }
            localView.layout(i2, paramInt1, localView.getMeasuredWidth() + i2, localView.getMeasuredHeight() + paramInt1);
          }
        }
      }
      paramInt3 += 1;
    }
    this.r = paramInt1;
    this.s = (i9 - paramInt4);
    this.aa = i3;
    if (this.U) {
      a(this.i, false, 0, false);
    }
    this.U = false;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    setMeasuredDimension(getDefaultSize(0, paramInt1), getDefaultSize(0, paramInt2));
    paramInt1 = getMeasuredWidth();
    this.E = Math.min(paramInt1 / 10, this.D);
    paramInt1 = paramInt1 - getPaddingLeft() - getPaddingRight();
    paramInt2 = getMeasuredHeight() - getPaddingTop() - getPaddingBottom();
    int i10 = getChildCount();
    int i3 = 0;
    View localView;
    LayoutParams localLayoutParams;
    if (i3 < i10)
    {
      localView = getChildAt(i3);
      i1 = paramInt2;
      int i2 = paramInt1;
      int i4;
      int i6;
      label179:
      int i5;
      if (localView.getVisibility() != 8)
      {
        localLayoutParams = (LayoutParams)localView.getLayoutParams();
        i1 = paramInt2;
        i2 = paramInt1;
        if (localLayoutParams != null)
        {
          i1 = paramInt2;
          i2 = paramInt1;
          if (localLayoutParams.a)
          {
            i2 = localLayoutParams.b & 0x7;
            i4 = localLayoutParams.b & 0x70;
            i6 = Integer.MIN_VALUE;
            i1 = Integer.MIN_VALUE;
            if ((i4 != 48) && (i4 != 80)) {
              break label350;
            }
            i4 = 1;
            if ((i2 != 3) && (i2 != 5)) {
              break label356;
            }
            i5 = 1;
            label194:
            if (i4 == 0) {
              break label362;
            }
            i2 = 1073741824;
            label204:
            int i8 = paramInt1;
            i6 = paramInt2;
            int i7 = i8;
            int i9;
            if (localLayoutParams.width != -2)
            {
              i9 = 1073741824;
              i2 = i9;
              i7 = i8;
              if (localLayoutParams.width != -1)
              {
                i7 = localLayoutParams.width;
                i2 = i9;
              }
            }
            i8 = i6;
            if (localLayoutParams.height != -2)
            {
              i9 = 1073741824;
              i1 = i9;
              i8 = i6;
              if (localLayoutParams.height != -1)
              {
                i8 = localLayoutParams.height;
                i1 = i9;
              }
            }
            localView.measure(View.MeasureSpec.makeMeasureSpec(i7, i2), View.MeasureSpec.makeMeasureSpec(i8, i1));
            if (i4 == 0) {
              break label382;
            }
            i1 = paramInt2 - localView.getMeasuredHeight();
            i2 = paramInt1;
          }
        }
      }
      for (;;)
      {
        i3 += 1;
        paramInt2 = i1;
        paramInt1 = i2;
        break;
        label350:
        i4 = 0;
        break label179;
        label356:
        i5 = 0;
        break label194;
        label362:
        i2 = i6;
        if (i5 == 0) {
          break label204;
        }
        i1 = 1073741824;
        i2 = i6;
        break label204;
        label382:
        i1 = paramInt2;
        i2 = paramInt1;
        if (i5 != 0)
        {
          i2 = paramInt1 - localView.getMeasuredWidth();
          i1 = paramInt2;
        }
      }
    }
    this.v = View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824);
    this.w = View.MeasureSpec.makeMeasureSpec(paramInt2, 1073741824);
    this.x = true;
    c();
    this.x = false;
    int i1 = getChildCount();
    paramInt2 = 0;
    while (paramInt2 < i1)
    {
      localView = getChildAt(paramInt2);
      if (localView.getVisibility() != 8)
      {
        localLayoutParams = (LayoutParams)localView.getLayoutParams();
        if ((localLayoutParams == null) || (!localLayoutParams.a)) {
          localView.measure(View.MeasureSpec.makeMeasureSpec((int)(paramInt1 * localLayoutParams.c), 1073741824), this.w);
        }
      }
      paramInt2 += 1;
    }
  }
  
  protected boolean onRequestFocusInDescendants(int paramInt, Rect paramRect)
  {
    int i2 = getChildCount();
    int i1;
    int i3;
    if ((paramInt & 0x2) != 0)
    {
      i1 = 0;
      i3 = 1;
    }
    while (i1 != i2)
    {
      View localView = getChildAt(i1);
      if (localView.getVisibility() == 0)
      {
        ItemInfo localItemInfo = a(localView);
        if ((localItemInfo != null) && (localItemInfo.b == this.i) && (localView.requestFocus(paramInt, paramRect)))
        {
          return true;
          i1 = i2 - 1;
          i3 = -1;
          i2 = -1;
          continue;
        }
      }
      i1 += i3;
    }
    return false;
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable)
  {
    if (!(paramParcelable instanceof SavedState))
    {
      super.onRestoreInstanceState(paramParcelable);
      return;
    }
    paramParcelable = (SavedState)paramParcelable;
    super.onRestoreInstanceState(paramParcelable.getSuperState());
    if (this.h != null)
    {
      this.h.restoreState(paramParcelable.b, paramParcelable.c);
      a(paramParcelable.a, false, true);
      return;
    }
    this.j = paramParcelable.a;
    this.k = paramParcelable.b;
    this.l = paramParcelable.c;
  }
  
  public Parcelable onSaveInstanceState()
  {
    SavedState localSavedState = new SavedState(super.onSaveInstanceState());
    localSavedState.a = this.i;
    if (this.h != null) {
      localSavedState.b = this.h.saveState();
    }
    return localSavedState;
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    if (paramInt1 != paramInt3) {
      a(paramInt1, paramInt3, this.p, this.p);
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if (this.Q) {
      return true;
    }
    if ((paramMotionEvent.getAction() == 0) && (paramMotionEvent.getEdgeFlags() != 0)) {
      return false;
    }
    if ((this.h == null) || (this.h.getCount() == 0)) {
      return false;
    }
    if (this.L == null) {
      this.L = VelocityTracker.obtain();
    }
    this.L.addMovement(paramMotionEvent);
    int i1 = paramMotionEvent.getAction();
    boolean bool2 = false;
    boolean bool1 = bool2;
    switch (i1 & 0xFF)
    {
    default: 
      bool1 = bool2;
    }
    for (;;)
    {
      if (bool1) {
        ViewCompat.d(this);
      }
      return true;
      this.m.abortAnimation();
      this.z = false;
      c();
      float f1 = paramMotionEvent.getX();
      this.I = f1;
      this.G = f1;
      f1 = paramMotionEvent.getY();
      this.J = f1;
      this.H = f1;
      this.K = MotionEventCompat.b(paramMotionEvent, 0);
      bool1 = bool2;
      continue;
      float f2;
      if (!this.B)
      {
        i1 = MotionEventCompat.a(paramMotionEvent, this.K);
        if (i1 == -1)
        {
          bool1 = l();
          continue;
        }
        f1 = MotionEventCompat.c(paramMotionEvent, i1);
        float f3 = Math.abs(f1 - this.G);
        f2 = MotionEventCompat.d(paramMotionEvent, i1);
        float f4 = Math.abs(f2 - this.H);
        if ((f3 > this.F) && (f3 > f4))
        {
          this.B = true;
          c(true);
          if (f1 - this.I <= 0.0F) {
            break label397;
          }
        }
      }
      Object localObject;
      label397:
      for (f1 = this.I + this.F;; f1 = this.I - this.F)
      {
        this.G = f1;
        this.H = f2;
        setScrollState(1);
        setScrollingCacheEnabled(true);
        localObject = getParent();
        if (localObject != null) {
          ((ViewParent)localObject).requestDisallowInterceptTouchEvent(true);
        }
        bool1 = bool2;
        if (!this.B) {
          break;
        }
        bool1 = false | c(MotionEventCompat.c(paramMotionEvent, MotionEventCompat.a(paramMotionEvent, this.K)));
        break;
      }
      bool1 = bool2;
      if (this.B)
      {
        localObject = this.L;
        ((VelocityTracker)localObject).computeCurrentVelocity(1000, this.N);
        i1 = (int)VelocityTrackerCompat.a((VelocityTracker)localObject, this.K);
        this.z = true;
        int i2 = getClientWidth();
        int i3 = getScrollX();
        localObject = m();
        f1 = this.p / i2;
        a(a(((ItemInfo)localObject).b, (i3 / i2 - ((ItemInfo)localObject).e) / (((ItemInfo)localObject).d + f1), i1, (int)(MotionEventCompat.c(paramMotionEvent, MotionEventCompat.a(paramMotionEvent, this.K)) - this.I)), true, true, i1);
        bool1 = l();
        continue;
        bool1 = bool2;
        if (this.B)
        {
          a(this.i, true, 0, false);
          bool1 = l();
          continue;
          i1 = MotionEventCompat.b(paramMotionEvent);
          this.G = MotionEventCompat.c(paramMotionEvent, i1);
          this.K = MotionEventCompat.b(paramMotionEvent, i1);
          bool1 = bool2;
          continue;
          a(paramMotionEvent);
          this.G = MotionEventCompat.c(paramMotionEvent, MotionEventCompat.a(paramMotionEvent, this.K));
          bool1 = bool2;
        }
      }
    }
  }
  
  public void removeView(View paramView)
  {
    if (this.x)
    {
      removeViewInLayout(paramView);
      return;
    }
    super.removeView(paramView);
  }
  
  public void setAdapter(PagerAdapter paramPagerAdapter)
  {
    if (this.h != null)
    {
      this.h.setViewPagerObserver(null);
      this.h.startUpdate(this);
      int i1 = 0;
      while (i1 < this.e.size())
      {
        localObject = (ItemInfo)this.e.get(i1);
        this.h.destroyItem(this, ((ItemInfo)localObject).b, ((ItemInfo)localObject).a);
        i1 += 1;
      }
      this.h.finishUpdate(this);
      this.e.clear();
      j();
      this.i = 0;
      scrollTo(0, 0);
    }
    Object localObject = this.h;
    this.h = paramPagerAdapter;
    this.b = 0;
    boolean bool;
    if (this.h != null)
    {
      if (this.o == null) {
        this.o = new PagerObserver(null);
      }
      this.h.setViewPagerObserver(this.o);
      this.z = false;
      bool = this.U;
      this.U = true;
      this.b = this.h.getCount();
      if (this.j < 0) {
        break label257;
      }
      this.h.restoreState(this.k, this.l);
      a(this.j, false, true);
      this.j = -1;
      this.k = null;
      this.l = null;
    }
    for (;;)
    {
      if ((this.ae != null) && (localObject != paramPagerAdapter)) {
        this.ae.a((PagerAdapter)localObject, paramPagerAdapter);
      }
      return;
      label257:
      if (!bool) {
        c();
      } else {
        requestLayout();
      }
    }
  }
  
  void setChildrenDrawingOrderEnabledCompat(boolean paramBoolean)
  {
    if ((Build.VERSION.SDK_INT < 7) || (this.ag == null)) {}
    try
    {
      this.ag = ViewGroup.class.getDeclaredMethod("setChildrenDrawingOrderEnabled", new Class[] { Boolean.TYPE });
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      for (;;)
      {
        try
        {
          this.ag.invoke(this, new Object[] { Boolean.valueOf(paramBoolean) });
          return;
        }
        catch (Exception localException)
        {
          Log.e("ViewPager", "Error changing children drawing order", localException);
        }
        localNoSuchMethodException = localNoSuchMethodException;
        Log.e("ViewPager", "Can't find setChildrenDrawingOrderEnabled", localNoSuchMethodException);
      }
    }
  }
  
  public void setCurrentItem(int paramInt)
  {
    this.z = false;
    if (!this.U) {}
    for (boolean bool = true;; bool = false)
    {
      a(paramInt, bool, false);
      return;
    }
  }
  
  public void setCurrentItem(int paramInt, boolean paramBoolean)
  {
    this.z = false;
    a(paramInt, paramBoolean, false);
  }
  
  public void setOffscreenPageLimit(int paramInt)
  {
    int i1 = paramInt;
    if (paramInt < 1)
    {
      Log.w("ViewPager", "Requested offscreen page limit " + paramInt + " too small; defaulting to " + 1);
      i1 = 1;
    }
    if (i1 != this.A)
    {
      this.A = i1;
      c();
    }
  }
  
  void setOnAdapterChangeListener(OnAdapterChangeListener paramOnAdapterChangeListener)
  {
    this.ae = paramOnAdapterChangeListener;
  }
  
  @Deprecated
  public void setOnPageChangeListener(OnPageChangeListener paramOnPageChangeListener)
  {
    this.ac = paramOnPageChangeListener;
  }
  
  public void setPageMargin(int paramInt)
  {
    int i1 = this.p;
    this.p = paramInt;
    int i2 = getWidth();
    a(i2, i2, paramInt, i1);
    requestLayout();
  }
  
  public void setPageMarginDrawable(@DrawableRes int paramInt)
  {
    setPageMarginDrawable(getContext().getResources().getDrawable(paramInt));
  }
  
  public void setPageMarginDrawable(Drawable paramDrawable)
  {
    this.q = paramDrawable;
    if (paramDrawable != null) {
      refreshDrawableState();
    }
    if (paramDrawable == null) {}
    for (boolean bool = true;; bool = false)
    {
      setWillNotDraw(bool);
      invalidate();
      return;
    }
  }
  
  public void setPageTransformer(boolean paramBoolean, PageTransformer paramPageTransformer)
  {
    int i2 = 1;
    boolean bool1;
    boolean bool2;
    label28:
    int i1;
    if (Build.VERSION.SDK_INT >= 11)
    {
      if (paramPageTransformer == null) {
        break label75;
      }
      bool1 = true;
      if (this.af == null) {
        break label81;
      }
      bool2 = true;
      if (bool1 == bool2) {
        break label87;
      }
      i1 = 1;
      label37:
      this.af = paramPageTransformer;
      setChildrenDrawingOrderEnabledCompat(bool1);
      if (!bool1) {
        break label92;
      }
      if (paramBoolean) {
        i2 = 2;
      }
    }
    label75:
    label81:
    label87:
    label92:
    for (this.ah = i2;; this.ah = 0)
    {
      if (i1 != 0) {
        c();
      }
      return;
      bool1 = false;
      break;
      bool2 = false;
      break label28;
      i1 = 0;
      break label37;
    }
  }
  
  protected boolean verifyDrawable(Drawable paramDrawable)
  {
    return (super.verifyDrawable(paramDrawable)) || (paramDrawable == this.q);
  }
  
  static abstract interface Decor {}
  
  static class ItemInfo
  {
    Object a;
    int b;
    boolean c;
    float d;
    float e;
  }
  
  public static class LayoutParams
    extends ViewGroup.LayoutParams
  {
    public boolean a;
    public int b;
    float c = 0.0F;
    boolean d;
    int e;
    int f;
    
    public LayoutParams()
    {
      super(-1);
    }
    
    public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
      paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, ViewPager.i());
      this.b = paramContext.getInteger(0, 48);
      paramContext.recycle();
    }
  }
  
  class MyAccessibilityDelegate
    extends AccessibilityDelegateCompat
  {
    MyAccessibilityDelegate() {}
    
    private boolean b()
    {
      return (ViewPager.a(ViewPager.this) != null) && (ViewPager.a(ViewPager.this).getCount() > 1);
    }
    
    public void a(View paramView, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat)
    {
      super.a(paramView, paramAccessibilityNodeInfoCompat);
      paramAccessibilityNodeInfoCompat.b(ViewPager.class.getName());
      paramAccessibilityNodeInfoCompat.i(b());
      if (ViewPager.this.canScrollHorizontally(1)) {
        paramAccessibilityNodeInfoCompat.a(4096);
      }
      if (ViewPager.this.canScrollHorizontally(-1)) {
        paramAccessibilityNodeInfoCompat.a(8192);
      }
    }
    
    public void a(View paramView, AccessibilityEvent paramAccessibilityEvent)
    {
      super.a(paramView, paramAccessibilityEvent);
      paramAccessibilityEvent.setClassName(ViewPager.class.getName());
      paramView = AccessibilityEventCompat.a(paramAccessibilityEvent);
      paramView.a(b());
      if ((paramAccessibilityEvent.getEventType() == 4096) && (ViewPager.a(ViewPager.this) != null))
      {
        paramView.a(ViewPager.a(ViewPager.this).getCount());
        paramView.b(ViewPager.b(ViewPager.this));
        paramView.c(ViewPager.b(ViewPager.this));
      }
    }
    
    public boolean a(View paramView, int paramInt, Bundle paramBundle)
    {
      if (super.a(paramView, paramInt, paramBundle)) {
        return true;
      }
      switch (paramInt)
      {
      default: 
        return false;
      case 4096: 
        if (ViewPager.this.canScrollHorizontally(1))
        {
          ViewPager.this.setCurrentItem(ViewPager.b(ViewPager.this) + 1);
          return true;
        }
        return false;
      }
      if (ViewPager.this.canScrollHorizontally(-1))
      {
        ViewPager.this.setCurrentItem(ViewPager.b(ViewPager.this) - 1);
        return true;
      }
      return false;
    }
  }
  
  static abstract interface OnAdapterChangeListener
  {
    public abstract void a(PagerAdapter paramPagerAdapter1, PagerAdapter paramPagerAdapter2);
  }
  
  public static abstract interface OnPageChangeListener
  {
    public abstract void onPageScrollStateChanged(int paramInt);
    
    public abstract void onPageScrolled(int paramInt1, float paramFloat, int paramInt2);
    
    public abstract void onPageSelected(int paramInt);
  }
  
  public static abstract interface PageTransformer
  {
    public abstract void a(View paramView, float paramFloat);
  }
  
  private class PagerObserver
    extends DataSetObserver
  {
    private PagerObserver() {}
    
    public void onChanged()
    {
      ViewPager.this.b();
    }
    
    public void onInvalidated()
    {
      ViewPager.this.b();
    }
  }
  
  public static class SavedState
    extends View.BaseSavedState
  {
    public static final Parcelable.Creator<SavedState> CREATOR = ParcelableCompat.a(new ParcelableCompatCreatorCallbacks()
    {
      public ViewPager.SavedState a(Parcel paramAnonymousParcel, ClassLoader paramAnonymousClassLoader)
      {
        return new ViewPager.SavedState(paramAnonymousParcel, paramAnonymousClassLoader);
      }
      
      public ViewPager.SavedState[] a(int paramAnonymousInt)
      {
        return new ViewPager.SavedState[paramAnonymousInt];
      }
    });
    int a;
    Parcelable b;
    ClassLoader c;
    
    SavedState(Parcel paramParcel, ClassLoader paramClassLoader)
    {
      super();
      ClassLoader localClassLoader = paramClassLoader;
      if (paramClassLoader == null) {
        localClassLoader = getClass().getClassLoader();
      }
      this.a = paramParcel.readInt();
      this.b = paramParcel.readParcelable(localClassLoader);
      this.c = localClassLoader;
    }
    
    public SavedState(Parcelable paramParcelable)
    {
      super();
    }
    
    public String toString()
    {
      return "FragmentPager.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " position=" + this.a + "}";
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      super.writeToParcel(paramParcel, paramInt);
      paramParcel.writeInt(this.a);
      paramParcel.writeParcelable(this.b, paramInt);
    }
  }
  
  public static class SimpleOnPageChangeListener
    implements ViewPager.OnPageChangeListener
  {
    public void onPageScrollStateChanged(int paramInt) {}
    
    public void onPageScrolled(int paramInt1, float paramFloat, int paramInt2) {}
    
    public void onPageSelected(int paramInt) {}
  }
  
  static class ViewPositionComparator
    implements Comparator<View>
  {
    public int a(View paramView1, View paramView2)
    {
      paramView1 = (ViewPager.LayoutParams)paramView1.getLayoutParams();
      paramView2 = (ViewPager.LayoutParams)paramView2.getLayoutParams();
      if (paramView1.a != paramView2.a)
      {
        if (paramView1.a) {
          return 1;
        }
        return -1;
      }
      return paramView1.e - paramView2.e;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/view/ViewPager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */