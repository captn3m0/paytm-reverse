package android.support.v4.view.accessibility;

import android.os.Build.VERSION;
import android.view.accessibility.AccessibilityManager;

public final class AccessibilityManagerCompat
{
  private static final AccessibilityManagerVersionImpl a = new AccessibilityManagerStubImpl();
  
  static
  {
    if (Build.VERSION.SDK_INT >= 14)
    {
      a = new AccessibilityManagerIcsImpl();
      return;
    }
  }
  
  public static boolean a(AccessibilityManager paramAccessibilityManager)
  {
    return a.a(paramAccessibilityManager);
  }
  
  static class AccessibilityManagerIcsImpl
    extends AccessibilityManagerCompat.AccessibilityManagerStubImpl
  {
    public Object a(final AccessibilityManagerCompat.AccessibilityStateChangeListenerCompat paramAccessibilityStateChangeListenerCompat)
    {
      AccessibilityManagerCompatIcs.a(new AccessibilityManagerCompatIcs.AccessibilityStateChangeListenerBridge()
      {
        public void a(boolean paramAnonymousBoolean)
        {
          paramAccessibilityStateChangeListenerCompat.a(paramAnonymousBoolean);
        }
      });
    }
    
    public boolean a(AccessibilityManager paramAccessibilityManager)
    {
      return AccessibilityManagerCompatIcs.a(paramAccessibilityManager);
    }
  }
  
  static class AccessibilityManagerStubImpl
    implements AccessibilityManagerCompat.AccessibilityManagerVersionImpl
  {
    public Object a(AccessibilityManagerCompat.AccessibilityStateChangeListenerCompat paramAccessibilityStateChangeListenerCompat)
    {
      return null;
    }
    
    public boolean a(AccessibilityManager paramAccessibilityManager)
    {
      return false;
    }
  }
  
  static abstract interface AccessibilityManagerVersionImpl
  {
    public abstract Object a(AccessibilityManagerCompat.AccessibilityStateChangeListenerCompat paramAccessibilityStateChangeListenerCompat);
    
    public abstract boolean a(AccessibilityManager paramAccessibilityManager);
  }
  
  public static abstract class AccessibilityStateChangeListenerCompat
  {
    final Object a = AccessibilityManagerCompat.a().a(this);
    
    public abstract void a(boolean paramBoolean);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/view/accessibility/AccessibilityManagerCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */