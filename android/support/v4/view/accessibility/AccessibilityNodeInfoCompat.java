package android.support.v4.view.accessibility;

import android.graphics.Rect;
import android.os.Build.VERSION;
import android.view.View;

public class AccessibilityNodeInfoCompat
{
  private static final AccessibilityNodeInfoImpl a = new AccessibilityNodeInfoStubImpl();
  private final Object b;
  
  static
  {
    if (Build.VERSION.SDK_INT >= 22)
    {
      a = new AccessibilityNodeInfoApi22Impl();
      return;
    }
    if (Build.VERSION.SDK_INT >= 21)
    {
      a = new AccessibilityNodeInfoApi21Impl();
      return;
    }
    if (Build.VERSION.SDK_INT >= 19)
    {
      a = new AccessibilityNodeInfoKitKatImpl();
      return;
    }
    if (Build.VERSION.SDK_INT >= 18)
    {
      a = new AccessibilityNodeInfoJellybeanMr2Impl();
      return;
    }
    if (Build.VERSION.SDK_INT >= 17)
    {
      a = new AccessibilityNodeInfoJellybeanMr1Impl();
      return;
    }
    if (Build.VERSION.SDK_INT >= 16)
    {
      a = new AccessibilityNodeInfoJellybeanImpl();
      return;
    }
    if (Build.VERSION.SDK_INT >= 14)
    {
      a = new AccessibilityNodeInfoIcsImpl();
      return;
    }
  }
  
  public AccessibilityNodeInfoCompat(Object paramObject)
  {
    this.b = paramObject;
  }
  
  public static AccessibilityNodeInfoCompat a(AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat)
  {
    return a(a.a(paramAccessibilityNodeInfoCompat.b));
  }
  
  public static AccessibilityNodeInfoCompat a(View paramView)
  {
    return a(a.a(paramView));
  }
  
  static AccessibilityNodeInfoCompat a(Object paramObject)
  {
    if (paramObject != null) {
      return new AccessibilityNodeInfoCompat(paramObject);
    }
    return null;
  }
  
  public static AccessibilityNodeInfoCompat b()
  {
    return a(a.a());
  }
  
  private static String c(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return "ACTION_UNKNOWN";
    case 1: 
      return "ACTION_FOCUS";
    case 2: 
      return "ACTION_CLEAR_FOCUS";
    case 4: 
      return "ACTION_SELECT";
    case 8: 
      return "ACTION_CLEAR_SELECTION";
    case 16: 
      return "ACTION_CLICK";
    case 32: 
      return "ACTION_LONG_CLICK";
    case 64: 
      return "ACTION_ACCESSIBILITY_FOCUS";
    case 128: 
      return "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
    case 256: 
      return "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
    case 512: 
      return "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
    case 1024: 
      return "ACTION_NEXT_HTML_ELEMENT";
    case 2048: 
      return "ACTION_PREVIOUS_HTML_ELEMENT";
    case 4096: 
      return "ACTION_SCROLL_FORWARD";
    case 8192: 
      return "ACTION_SCROLL_BACKWARD";
    case 65536: 
      return "ACTION_CUT";
    case 16384: 
      return "ACTION_COPY";
    case 32768: 
      return "ACTION_PASTE";
    }
    return "ACTION_SET_SELECTION";
  }
  
  public Object a()
  {
    return this.b;
  }
  
  public void a(int paramInt)
  {
    a.a(this.b, paramInt);
  }
  
  public void a(Rect paramRect)
  {
    a.a(this.b, paramRect);
  }
  
  public void a(View paramView, int paramInt)
  {
    a.a(this.b, paramView, paramInt);
  }
  
  public void a(CharSequence paramCharSequence)
  {
    a.d(this.b, paramCharSequence);
  }
  
  public void a(boolean paramBoolean)
  {
    a.c(this.b, paramBoolean);
  }
  
  public boolean a(AccessibilityActionCompat paramAccessibilityActionCompat)
  {
    return a.a(this.b, AccessibilityActionCompat.a(paramAccessibilityActionCompat));
  }
  
  public void b(int paramInt)
  {
    a.b(this.b, paramInt);
  }
  
  public void b(Rect paramRect)
  {
    a.c(this.b, paramRect);
  }
  
  public void b(View paramView)
  {
    a.c(this.b, paramView);
  }
  
  public void b(View paramView, int paramInt)
  {
    a.b(this.b, paramView, paramInt);
  }
  
  public void b(CharSequence paramCharSequence)
  {
    a.b(this.b, paramCharSequence);
  }
  
  public void b(Object paramObject)
  {
    a.b(this.b, ((CollectionInfoCompat)paramObject).a);
  }
  
  public void b(boolean paramBoolean)
  {
    a.d(this.b, paramBoolean);
  }
  
  public int c()
  {
    return a.b(this.b);
  }
  
  public void c(Rect paramRect)
  {
    a.b(this.b, paramRect);
  }
  
  public void c(View paramView)
  {
    a.a(this.b, paramView);
  }
  
  public void c(CharSequence paramCharSequence)
  {
    a.e(this.b, paramCharSequence);
  }
  
  public void c(Object paramObject)
  {
    a.c(this.b, CollectionItemInfoCompat.a((CollectionItemInfoCompat)paramObject));
  }
  
  public void c(boolean paramBoolean)
  {
    a.h(this.b, paramBoolean);
  }
  
  public int d()
  {
    return a.r(this.b);
  }
  
  public void d(Rect paramRect)
  {
    a.d(this.b, paramRect);
  }
  
  public void d(View paramView)
  {
    a.b(this.b, paramView);
  }
  
  public void d(CharSequence paramCharSequence)
  {
    a.c(this.b, paramCharSequence);
  }
  
  public void d(boolean paramBoolean)
  {
    a.i(this.b, paramBoolean);
  }
  
  public void e(View paramView)
  {
    a.d(this.b, paramView);
  }
  
  public void e(CharSequence paramCharSequence)
  {
    a.a(this.b, paramCharSequence);
  }
  
  public void e(boolean paramBoolean)
  {
    a.g(this.b, paramBoolean);
  }
  
  public boolean e()
  {
    return a.g(this.b);
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      do
      {
        return true;
        if (paramObject == null) {
          return false;
        }
        if (getClass() != paramObject.getClass()) {
          return false;
        }
        paramObject = (AccessibilityNodeInfoCompat)paramObject;
        if (this.b != null) {
          break;
        }
      } while (((AccessibilityNodeInfoCompat)paramObject).b == null);
      return false;
    } while (this.b.equals(((AccessibilityNodeInfoCompat)paramObject).b));
    return false;
  }
  
  public void f(boolean paramBoolean)
  {
    a.a(this.b, paramBoolean);
  }
  
  public boolean f()
  {
    return a.h(this.b);
  }
  
  public void g(boolean paramBoolean)
  {
    a.e(this.b, paramBoolean);
  }
  
  public boolean g()
  {
    return a.k(this.b);
  }
  
  public void h(boolean paramBoolean)
  {
    a.b(this.b, paramBoolean);
  }
  
  public boolean h()
  {
    return a.l(this.b);
  }
  
  public int hashCode()
  {
    if (this.b == null) {
      return 0;
    }
    return this.b.hashCode();
  }
  
  public void i(boolean paramBoolean)
  {
    a.f(this.b, paramBoolean);
  }
  
  public boolean i()
  {
    return a.s(this.b);
  }
  
  public void j(boolean paramBoolean)
  {
    a.j(this.b, paramBoolean);
  }
  
  public boolean j()
  {
    return a.t(this.b);
  }
  
  public boolean k()
  {
    return a.p(this.b);
  }
  
  public boolean l()
  {
    return a.i(this.b);
  }
  
  public boolean m()
  {
    return a.m(this.b);
  }
  
  public boolean n()
  {
    return a.j(this.b);
  }
  
  public boolean o()
  {
    return a.n(this.b);
  }
  
  public boolean p()
  {
    return a.o(this.b);
  }
  
  public CharSequence q()
  {
    return a.e(this.b);
  }
  
  public CharSequence r()
  {
    return a.c(this.b);
  }
  
  public CharSequence s()
  {
    return a.f(this.b);
  }
  
  public CharSequence t()
  {
    return a.d(this.b);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(super.toString());
    Rect localRect = new Rect();
    a(localRect);
    localStringBuilder.append("; boundsInParent: " + localRect);
    c(localRect);
    localStringBuilder.append("; boundsInScreen: " + localRect);
    localStringBuilder.append("; packageName: ").append(q());
    localStringBuilder.append("; className: ").append(r());
    localStringBuilder.append("; text: ").append(s());
    localStringBuilder.append("; contentDescription: ").append(t());
    localStringBuilder.append("; viewId: ").append(v());
    localStringBuilder.append("; checkable: ").append(e());
    localStringBuilder.append("; checked: ").append(f());
    localStringBuilder.append("; focusable: ").append(g());
    localStringBuilder.append("; focused: ").append(h());
    localStringBuilder.append("; selected: ").append(k());
    localStringBuilder.append("; clickable: ").append(l());
    localStringBuilder.append("; longClickable: ").append(m());
    localStringBuilder.append("; enabled: ").append(n());
    localStringBuilder.append("; password: ").append(o());
    localStringBuilder.append("; scrollable: " + p());
    localStringBuilder.append("; [");
    int i = c();
    while (i != 0)
    {
      int k = 1 << Integer.numberOfTrailingZeros(i);
      int j = i & (k ^ 0xFFFFFFFF);
      localStringBuilder.append(c(k));
      i = j;
      if (j != 0)
      {
        localStringBuilder.append(", ");
        i = j;
      }
    }
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
  
  public void u()
  {
    a.q(this.b);
  }
  
  public String v()
  {
    return a.u(this.b);
  }
  
  public static class AccessibilityActionCompat
  {
    public static final AccessibilityActionCompat a = new AccessibilityActionCompat(1, null);
    public static final AccessibilityActionCompat b = new AccessibilityActionCompat(2, null);
    public static final AccessibilityActionCompat c = new AccessibilityActionCompat(4, null);
    public static final AccessibilityActionCompat d = new AccessibilityActionCompat(8, null);
    public static final AccessibilityActionCompat e = new AccessibilityActionCompat(16, null);
    public static final AccessibilityActionCompat f = new AccessibilityActionCompat(32, null);
    public static final AccessibilityActionCompat g = new AccessibilityActionCompat(64, null);
    public static final AccessibilityActionCompat h = new AccessibilityActionCompat(128, null);
    public static final AccessibilityActionCompat i = new AccessibilityActionCompat(256, null);
    public static final AccessibilityActionCompat j = new AccessibilityActionCompat(512, null);
    public static final AccessibilityActionCompat k = new AccessibilityActionCompat(1024, null);
    public static final AccessibilityActionCompat l = new AccessibilityActionCompat(2048, null);
    public static final AccessibilityActionCompat m = new AccessibilityActionCompat(4096, null);
    public static final AccessibilityActionCompat n = new AccessibilityActionCompat(8192, null);
    public static final AccessibilityActionCompat o = new AccessibilityActionCompat(16384, null);
    public static final AccessibilityActionCompat p = new AccessibilityActionCompat(32768, null);
    public static final AccessibilityActionCompat q = new AccessibilityActionCompat(65536, null);
    public static final AccessibilityActionCompat r = new AccessibilityActionCompat(131072, null);
    public static final AccessibilityActionCompat s = new AccessibilityActionCompat(262144, null);
    public static final AccessibilityActionCompat t = new AccessibilityActionCompat(524288, null);
    public static final AccessibilityActionCompat u = new AccessibilityActionCompat(1048576, null);
    public static final AccessibilityActionCompat v = new AccessibilityActionCompat(2097152, null);
    private final Object w;
    
    public AccessibilityActionCompat(int paramInt, CharSequence paramCharSequence)
    {
      this(AccessibilityNodeInfoCompat.w().a(paramInt, paramCharSequence));
    }
    
    private AccessibilityActionCompat(Object paramObject)
    {
      this.w = paramObject;
    }
  }
  
  static class AccessibilityNodeInfoApi21Impl
    extends AccessibilityNodeInfoCompat.AccessibilityNodeInfoKitKatImpl
  {
    public Object a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean1, boolean paramBoolean2)
    {
      return AccessibilityNodeInfoCompatApi21.a(paramInt1, paramInt2, paramInt3, paramInt4, paramBoolean1, paramBoolean2);
    }
    
    public Object a(int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3)
    {
      return AccessibilityNodeInfoCompatApi21.a(paramInt1, paramInt2, paramBoolean, paramInt3);
    }
    
    public Object a(int paramInt, CharSequence paramCharSequence)
    {
      return AccessibilityNodeInfoCompatApi21.a(paramInt, paramCharSequence);
    }
    
    public void a(Object paramObject, CharSequence paramCharSequence)
    {
      AccessibilityNodeInfoCompatApi21.a(paramObject, paramCharSequence);
    }
    
    public boolean a(Object paramObject1, Object paramObject2)
    {
      return AccessibilityNodeInfoCompatApi21.a(paramObject1, paramObject2);
    }
  }
  
  static class AccessibilityNodeInfoApi22Impl
    extends AccessibilityNodeInfoCompat.AccessibilityNodeInfoApi21Impl
  {}
  
  static class AccessibilityNodeInfoIcsImpl
    extends AccessibilityNodeInfoCompat.AccessibilityNodeInfoStubImpl
  {
    public Object a()
    {
      return AccessibilityNodeInfoCompatIcs.a();
    }
    
    public Object a(View paramView)
    {
      return AccessibilityNodeInfoCompatIcs.a(paramView);
    }
    
    public Object a(Object paramObject)
    {
      return AccessibilityNodeInfoCompatIcs.a(paramObject);
    }
    
    public void a(Object paramObject, int paramInt)
    {
      AccessibilityNodeInfoCompatIcs.a(paramObject, paramInt);
    }
    
    public void a(Object paramObject, Rect paramRect)
    {
      AccessibilityNodeInfoCompatIcs.a(paramObject, paramRect);
    }
    
    public void a(Object paramObject, View paramView)
    {
      AccessibilityNodeInfoCompatIcs.a(paramObject, paramView);
    }
    
    public void a(Object paramObject, boolean paramBoolean)
    {
      AccessibilityNodeInfoCompatIcs.a(paramObject, paramBoolean);
    }
    
    public int b(Object paramObject)
    {
      return AccessibilityNodeInfoCompatIcs.b(paramObject);
    }
    
    public void b(Object paramObject, Rect paramRect)
    {
      AccessibilityNodeInfoCompatIcs.b(paramObject, paramRect);
    }
    
    public void b(Object paramObject, View paramView)
    {
      AccessibilityNodeInfoCompatIcs.b(paramObject, paramView);
    }
    
    public void b(Object paramObject, CharSequence paramCharSequence)
    {
      AccessibilityNodeInfoCompatIcs.a(paramObject, paramCharSequence);
    }
    
    public void b(Object paramObject, boolean paramBoolean)
    {
      AccessibilityNodeInfoCompatIcs.b(paramObject, paramBoolean);
    }
    
    public CharSequence c(Object paramObject)
    {
      return AccessibilityNodeInfoCompatIcs.c(paramObject);
    }
    
    public void c(Object paramObject, Rect paramRect)
    {
      AccessibilityNodeInfoCompatIcs.c(paramObject, paramRect);
    }
    
    public void c(Object paramObject, View paramView)
    {
      AccessibilityNodeInfoCompatIcs.c(paramObject, paramView);
    }
    
    public void c(Object paramObject, CharSequence paramCharSequence)
    {
      AccessibilityNodeInfoCompatIcs.b(paramObject, paramCharSequence);
    }
    
    public void c(Object paramObject, boolean paramBoolean)
    {
      AccessibilityNodeInfoCompatIcs.c(paramObject, paramBoolean);
    }
    
    public CharSequence d(Object paramObject)
    {
      return AccessibilityNodeInfoCompatIcs.d(paramObject);
    }
    
    public void d(Object paramObject, Rect paramRect)
    {
      AccessibilityNodeInfoCompatIcs.d(paramObject, paramRect);
    }
    
    public void d(Object paramObject, CharSequence paramCharSequence)
    {
      AccessibilityNodeInfoCompatIcs.c(paramObject, paramCharSequence);
    }
    
    public void d(Object paramObject, boolean paramBoolean)
    {
      AccessibilityNodeInfoCompatIcs.d(paramObject, paramBoolean);
    }
    
    public CharSequence e(Object paramObject)
    {
      return AccessibilityNodeInfoCompatIcs.e(paramObject);
    }
    
    public void e(Object paramObject, CharSequence paramCharSequence)
    {
      AccessibilityNodeInfoCompatIcs.d(paramObject, paramCharSequence);
    }
    
    public void e(Object paramObject, boolean paramBoolean)
    {
      AccessibilityNodeInfoCompatIcs.e(paramObject, paramBoolean);
    }
    
    public CharSequence f(Object paramObject)
    {
      return AccessibilityNodeInfoCompatIcs.f(paramObject);
    }
    
    public void f(Object paramObject, boolean paramBoolean)
    {
      AccessibilityNodeInfoCompatIcs.f(paramObject, paramBoolean);
    }
    
    public void g(Object paramObject, boolean paramBoolean)
    {
      AccessibilityNodeInfoCompatIcs.g(paramObject, paramBoolean);
    }
    
    public boolean g(Object paramObject)
    {
      return AccessibilityNodeInfoCompatIcs.g(paramObject);
    }
    
    public boolean h(Object paramObject)
    {
      return AccessibilityNodeInfoCompatIcs.h(paramObject);
    }
    
    public boolean i(Object paramObject)
    {
      return AccessibilityNodeInfoCompatIcs.i(paramObject);
    }
    
    public boolean j(Object paramObject)
    {
      return AccessibilityNodeInfoCompatIcs.j(paramObject);
    }
    
    public boolean k(Object paramObject)
    {
      return AccessibilityNodeInfoCompatIcs.k(paramObject);
    }
    
    public boolean l(Object paramObject)
    {
      return AccessibilityNodeInfoCompatIcs.l(paramObject);
    }
    
    public boolean m(Object paramObject)
    {
      return AccessibilityNodeInfoCompatIcs.m(paramObject);
    }
    
    public boolean n(Object paramObject)
    {
      return AccessibilityNodeInfoCompatIcs.n(paramObject);
    }
    
    public boolean o(Object paramObject)
    {
      return AccessibilityNodeInfoCompatIcs.o(paramObject);
    }
    
    public boolean p(Object paramObject)
    {
      return AccessibilityNodeInfoCompatIcs.p(paramObject);
    }
    
    public void q(Object paramObject)
    {
      AccessibilityNodeInfoCompatIcs.q(paramObject);
    }
  }
  
  static abstract interface AccessibilityNodeInfoImpl
  {
    public abstract Object a();
    
    public abstract Object a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean1, boolean paramBoolean2);
    
    public abstract Object a(int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3);
    
    public abstract Object a(int paramInt, CharSequence paramCharSequence);
    
    public abstract Object a(View paramView);
    
    public abstract Object a(Object paramObject);
    
    public abstract void a(Object paramObject, int paramInt);
    
    public abstract void a(Object paramObject, Rect paramRect);
    
    public abstract void a(Object paramObject, View paramView);
    
    public abstract void a(Object paramObject, View paramView, int paramInt);
    
    public abstract void a(Object paramObject, CharSequence paramCharSequence);
    
    public abstract void a(Object paramObject, boolean paramBoolean);
    
    public abstract boolean a(Object paramObject1, Object paramObject2);
    
    public abstract int b(Object paramObject);
    
    public abstract void b(Object paramObject, int paramInt);
    
    public abstract void b(Object paramObject, Rect paramRect);
    
    public abstract void b(Object paramObject, View paramView);
    
    public abstract void b(Object paramObject, View paramView, int paramInt);
    
    public abstract void b(Object paramObject, CharSequence paramCharSequence);
    
    public abstract void b(Object paramObject1, Object paramObject2);
    
    public abstract void b(Object paramObject, boolean paramBoolean);
    
    public abstract CharSequence c(Object paramObject);
    
    public abstract void c(Object paramObject, Rect paramRect);
    
    public abstract void c(Object paramObject, View paramView);
    
    public abstract void c(Object paramObject, CharSequence paramCharSequence);
    
    public abstract void c(Object paramObject1, Object paramObject2);
    
    public abstract void c(Object paramObject, boolean paramBoolean);
    
    public abstract CharSequence d(Object paramObject);
    
    public abstract void d(Object paramObject, Rect paramRect);
    
    public abstract void d(Object paramObject, View paramView);
    
    public abstract void d(Object paramObject, CharSequence paramCharSequence);
    
    public abstract void d(Object paramObject, boolean paramBoolean);
    
    public abstract CharSequence e(Object paramObject);
    
    public abstract void e(Object paramObject, CharSequence paramCharSequence);
    
    public abstract void e(Object paramObject, boolean paramBoolean);
    
    public abstract CharSequence f(Object paramObject);
    
    public abstract void f(Object paramObject, boolean paramBoolean);
    
    public abstract void g(Object paramObject, boolean paramBoolean);
    
    public abstract boolean g(Object paramObject);
    
    public abstract void h(Object paramObject, boolean paramBoolean);
    
    public abstract boolean h(Object paramObject);
    
    public abstract void i(Object paramObject, boolean paramBoolean);
    
    public abstract boolean i(Object paramObject);
    
    public abstract void j(Object paramObject, boolean paramBoolean);
    
    public abstract boolean j(Object paramObject);
    
    public abstract boolean k(Object paramObject);
    
    public abstract boolean l(Object paramObject);
    
    public abstract boolean m(Object paramObject);
    
    public abstract boolean n(Object paramObject);
    
    public abstract boolean o(Object paramObject);
    
    public abstract boolean p(Object paramObject);
    
    public abstract void q(Object paramObject);
    
    public abstract int r(Object paramObject);
    
    public abstract boolean s(Object paramObject);
    
    public abstract boolean t(Object paramObject);
    
    public abstract String u(Object paramObject);
  }
  
  static class AccessibilityNodeInfoJellybeanImpl
    extends AccessibilityNodeInfoCompat.AccessibilityNodeInfoIcsImpl
  {
    public void a(Object paramObject, View paramView, int paramInt)
    {
      AccessibilityNodeInfoCompatJellyBean.b(paramObject, paramView, paramInt);
    }
    
    public void b(Object paramObject, int paramInt)
    {
      AccessibilityNodeInfoCompatJellyBean.a(paramObject, paramInt);
    }
    
    public void b(Object paramObject, View paramView, int paramInt)
    {
      AccessibilityNodeInfoCompatJellyBean.a(paramObject, paramView, paramInt);
    }
    
    public void h(Object paramObject, boolean paramBoolean)
    {
      AccessibilityNodeInfoCompatJellyBean.a(paramObject, paramBoolean);
    }
    
    public void i(Object paramObject, boolean paramBoolean)
    {
      AccessibilityNodeInfoCompatJellyBean.b(paramObject, paramBoolean);
    }
    
    public int r(Object paramObject)
    {
      return AccessibilityNodeInfoCompatJellyBean.b(paramObject);
    }
    
    public boolean s(Object paramObject)
    {
      return AccessibilityNodeInfoCompatJellyBean.a(paramObject);
    }
    
    public boolean t(Object paramObject)
    {
      return AccessibilityNodeInfoCompatJellyBean.c(paramObject);
    }
  }
  
  static class AccessibilityNodeInfoJellybeanMr1Impl
    extends AccessibilityNodeInfoCompat.AccessibilityNodeInfoJellybeanImpl
  {
    public void d(Object paramObject, View paramView)
    {
      AccessibilityNodeInfoCompatJellybeanMr1.a(paramObject, paramView);
    }
  }
  
  static class AccessibilityNodeInfoJellybeanMr2Impl
    extends AccessibilityNodeInfoCompat.AccessibilityNodeInfoJellybeanMr1Impl
  {
    public String u(Object paramObject)
    {
      return AccessibilityNodeInfoCompatJellybeanMr2.a(paramObject);
    }
  }
  
  static class AccessibilityNodeInfoKitKatImpl
    extends AccessibilityNodeInfoCompat.AccessibilityNodeInfoJellybeanMr2Impl
  {
    public Object a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean1, boolean paramBoolean2)
    {
      return AccessibilityNodeInfoCompatKitKat.a(paramInt1, paramInt2, paramInt3, paramInt4, paramBoolean1);
    }
    
    public Object a(int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3)
    {
      return AccessibilityNodeInfoCompatKitKat.a(paramInt1, paramInt2, paramBoolean, paramInt3);
    }
    
    public void b(Object paramObject1, Object paramObject2)
    {
      AccessibilityNodeInfoCompatKitKat.a(paramObject1, paramObject2);
    }
    
    public void c(Object paramObject1, Object paramObject2)
    {
      AccessibilityNodeInfoCompatKitKat.b(paramObject1, paramObject2);
    }
    
    public void j(Object paramObject, boolean paramBoolean)
    {
      AccessibilityNodeInfoCompatKitKat.a(paramObject, paramBoolean);
    }
  }
  
  static class AccessibilityNodeInfoStubImpl
    implements AccessibilityNodeInfoCompat.AccessibilityNodeInfoImpl
  {
    public Object a()
    {
      return null;
    }
    
    public Object a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean1, boolean paramBoolean2)
    {
      return null;
    }
    
    public Object a(int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3)
    {
      return null;
    }
    
    public Object a(int paramInt, CharSequence paramCharSequence)
    {
      return null;
    }
    
    public Object a(View paramView)
    {
      return null;
    }
    
    public Object a(Object paramObject)
    {
      return null;
    }
    
    public void a(Object paramObject, int paramInt) {}
    
    public void a(Object paramObject, Rect paramRect) {}
    
    public void a(Object paramObject, View paramView) {}
    
    public void a(Object paramObject, View paramView, int paramInt) {}
    
    public void a(Object paramObject, CharSequence paramCharSequence) {}
    
    public void a(Object paramObject, boolean paramBoolean) {}
    
    public boolean a(Object paramObject1, Object paramObject2)
    {
      return false;
    }
    
    public int b(Object paramObject)
    {
      return 0;
    }
    
    public void b(Object paramObject, int paramInt) {}
    
    public void b(Object paramObject, Rect paramRect) {}
    
    public void b(Object paramObject, View paramView) {}
    
    public void b(Object paramObject, View paramView, int paramInt) {}
    
    public void b(Object paramObject, CharSequence paramCharSequence) {}
    
    public void b(Object paramObject1, Object paramObject2) {}
    
    public void b(Object paramObject, boolean paramBoolean) {}
    
    public CharSequence c(Object paramObject)
    {
      return null;
    }
    
    public void c(Object paramObject, Rect paramRect) {}
    
    public void c(Object paramObject, View paramView) {}
    
    public void c(Object paramObject, CharSequence paramCharSequence) {}
    
    public void c(Object paramObject1, Object paramObject2) {}
    
    public void c(Object paramObject, boolean paramBoolean) {}
    
    public CharSequence d(Object paramObject)
    {
      return null;
    }
    
    public void d(Object paramObject, Rect paramRect) {}
    
    public void d(Object paramObject, View paramView) {}
    
    public void d(Object paramObject, CharSequence paramCharSequence) {}
    
    public void d(Object paramObject, boolean paramBoolean) {}
    
    public CharSequence e(Object paramObject)
    {
      return null;
    }
    
    public void e(Object paramObject, CharSequence paramCharSequence) {}
    
    public void e(Object paramObject, boolean paramBoolean) {}
    
    public CharSequence f(Object paramObject)
    {
      return null;
    }
    
    public void f(Object paramObject, boolean paramBoolean) {}
    
    public void g(Object paramObject, boolean paramBoolean) {}
    
    public boolean g(Object paramObject)
    {
      return false;
    }
    
    public void h(Object paramObject, boolean paramBoolean) {}
    
    public boolean h(Object paramObject)
    {
      return false;
    }
    
    public void i(Object paramObject, boolean paramBoolean) {}
    
    public boolean i(Object paramObject)
    {
      return false;
    }
    
    public void j(Object paramObject, boolean paramBoolean) {}
    
    public boolean j(Object paramObject)
    {
      return false;
    }
    
    public boolean k(Object paramObject)
    {
      return false;
    }
    
    public boolean l(Object paramObject)
    {
      return false;
    }
    
    public boolean m(Object paramObject)
    {
      return false;
    }
    
    public boolean n(Object paramObject)
    {
      return false;
    }
    
    public boolean o(Object paramObject)
    {
      return false;
    }
    
    public boolean p(Object paramObject)
    {
      return false;
    }
    
    public void q(Object paramObject) {}
    
    public int r(Object paramObject)
    {
      return 0;
    }
    
    public boolean s(Object paramObject)
    {
      return false;
    }
    
    public boolean t(Object paramObject)
    {
      return false;
    }
    
    public String u(Object paramObject)
    {
      return null;
    }
  }
  
  public static class CollectionInfoCompat
  {
    final Object a;
    
    private CollectionInfoCompat(Object paramObject)
    {
      this.a = paramObject;
    }
    
    public static CollectionInfoCompat a(int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3)
    {
      return new CollectionInfoCompat(AccessibilityNodeInfoCompat.w().a(paramInt1, paramInt2, paramBoolean, paramInt3));
    }
  }
  
  public static class CollectionItemInfoCompat
  {
    private final Object a;
    
    private CollectionItemInfoCompat(Object paramObject)
    {
      this.a = paramObject;
    }
    
    public static CollectionItemInfoCompat a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean1, boolean paramBoolean2)
    {
      return new CollectionItemInfoCompat(AccessibilityNodeInfoCompat.w().a(paramInt1, paramInt2, paramInt3, paramInt4, paramBoolean1, paramBoolean2));
    }
  }
  
  public static class RangeInfoCompat {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/view/accessibility/AccessibilityNodeInfoCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */