package android.support.v4.view.accessibility;

import android.graphics.Rect;
import android.os.Build.VERSION;

public class AccessibilityWindowInfoCompat
{
  private static final AccessibilityWindowInfoImpl a = new AccessibilityWindowInfoStubImpl(null);
  private Object b;
  
  static
  {
    if (Build.VERSION.SDK_INT >= 21)
    {
      a = new AccessibilityWindowInfoApi21Impl(null);
      return;
    }
  }
  
  private AccessibilityWindowInfoCompat(Object paramObject)
  {
    this.b = paramObject;
  }
  
  static AccessibilityWindowInfoCompat a(Object paramObject)
  {
    if (paramObject != null) {
      return new AccessibilityWindowInfoCompat(paramObject);
    }
    return null;
  }
  
  private static String a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return "<UNKNOWN>";
    case 1: 
      return "TYPE_APPLICATION";
    case 2: 
      return "TYPE_INPUT_METHOD";
    case 3: 
      return "TYPE_SYSTEM";
    }
    return "TYPE_ACCESSIBILITY_OVERLAY";
  }
  
  public int a()
  {
    return a.a(this.b);
  }
  
  public void a(Rect paramRect)
  {
    a.a(this.b, paramRect);
  }
  
  public int b()
  {
    return a.b(this.b);
  }
  
  public AccessibilityWindowInfoCompat c()
  {
    return a(a.c(this.b));
  }
  
  public int d()
  {
    return a.d(this.b);
  }
  
  public boolean e()
  {
    return a.e(this.b);
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      do
      {
        return true;
        if (paramObject == null) {
          return false;
        }
        if (getClass() != paramObject.getClass()) {
          return false;
        }
        paramObject = (AccessibilityWindowInfoCompat)paramObject;
        if (this.b != null) {
          break;
        }
      } while (((AccessibilityWindowInfoCompat)paramObject).b == null);
      return false;
    } while (this.b.equals(((AccessibilityWindowInfoCompat)paramObject).b));
    return false;
  }
  
  public boolean f()
  {
    return a.f(this.b);
  }
  
  public int g()
  {
    return a.g(this.b);
  }
  
  public int hashCode()
  {
    if (this.b == null) {
      return 0;
    }
    return this.b.hashCode();
  }
  
  public String toString()
  {
    boolean bool2 = true;
    StringBuilder localStringBuilder = new StringBuilder();
    Object localObject = new Rect();
    a((Rect)localObject);
    localStringBuilder.append("AccessibilityWindowInfo[");
    localStringBuilder.append("id=").append(d());
    localStringBuilder.append(", type=").append(a(a()));
    localStringBuilder.append(", layer=").append(b());
    localStringBuilder.append(", bounds=").append(localObject);
    localStringBuilder.append(", focused=").append(f());
    localStringBuilder.append(", active=").append(e());
    localObject = localStringBuilder.append(", hasParent=");
    if (c() != null)
    {
      bool1 = true;
      ((StringBuilder)localObject).append(bool1);
      localObject = localStringBuilder.append(", hasChildren=");
      if (g() <= 0) {
        break label182;
      }
    }
    label182:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      ((StringBuilder)localObject).append(bool1);
      localStringBuilder.append(']');
      return localStringBuilder.toString();
      bool1 = false;
      break;
    }
  }
  
  private static class AccessibilityWindowInfoApi21Impl
    extends AccessibilityWindowInfoCompat.AccessibilityWindowInfoStubImpl
  {
    private AccessibilityWindowInfoApi21Impl()
    {
      super();
    }
    
    public int a(Object paramObject)
    {
      return AccessibilityWindowInfoCompatApi21.a(paramObject);
    }
    
    public void a(Object paramObject, Rect paramRect)
    {
      AccessibilityWindowInfoCompatApi21.a(paramObject, paramRect);
    }
    
    public int b(Object paramObject)
    {
      return AccessibilityWindowInfoCompatApi21.b(paramObject);
    }
    
    public Object c(Object paramObject)
    {
      return AccessibilityWindowInfoCompatApi21.c(paramObject);
    }
    
    public int d(Object paramObject)
    {
      return AccessibilityWindowInfoCompatApi21.d(paramObject);
    }
    
    public boolean e(Object paramObject)
    {
      return AccessibilityWindowInfoCompatApi21.e(paramObject);
    }
    
    public boolean f(Object paramObject)
    {
      return AccessibilityWindowInfoCompatApi21.f(paramObject);
    }
    
    public int g(Object paramObject)
    {
      return AccessibilityWindowInfoCompatApi21.g(paramObject);
    }
  }
  
  private static abstract interface AccessibilityWindowInfoImpl
  {
    public abstract int a(Object paramObject);
    
    public abstract void a(Object paramObject, Rect paramRect);
    
    public abstract int b(Object paramObject);
    
    public abstract Object c(Object paramObject);
    
    public abstract int d(Object paramObject);
    
    public abstract boolean e(Object paramObject);
    
    public abstract boolean f(Object paramObject);
    
    public abstract int g(Object paramObject);
  }
  
  private static class AccessibilityWindowInfoStubImpl
    implements AccessibilityWindowInfoCompat.AccessibilityWindowInfoImpl
  {
    public int a(Object paramObject)
    {
      return -1;
    }
    
    public void a(Object paramObject, Rect paramRect) {}
    
    public int b(Object paramObject)
    {
      return -1;
    }
    
    public Object c(Object paramObject)
    {
      return null;
    }
    
    public int d(Object paramObject)
    {
      return -1;
    }
    
    public boolean e(Object paramObject)
    {
      return true;
    }
    
    public boolean f(Object paramObject)
    {
      return true;
    }
    
    public int g(Object paramObject)
    {
      return 0;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/view/accessibility/AccessibilityWindowInfoCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */