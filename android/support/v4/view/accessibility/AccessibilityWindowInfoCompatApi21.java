package android.support.v4.view.accessibility;

import android.graphics.Rect;
import android.view.accessibility.AccessibilityWindowInfo;

class AccessibilityWindowInfoCompatApi21
{
  public static int a(Object paramObject)
  {
    return ((AccessibilityWindowInfo)paramObject).getType();
  }
  
  public static void a(Object paramObject, Rect paramRect)
  {
    ((AccessibilityWindowInfo)paramObject).getBoundsInScreen(paramRect);
  }
  
  public static int b(Object paramObject)
  {
    return ((AccessibilityWindowInfo)paramObject).getLayer();
  }
  
  public static Object c(Object paramObject)
  {
    return ((AccessibilityWindowInfo)paramObject).getParent();
  }
  
  public static int d(Object paramObject)
  {
    return ((AccessibilityWindowInfo)paramObject).getId();
  }
  
  public static boolean e(Object paramObject)
  {
    return ((AccessibilityWindowInfo)paramObject).isActive();
  }
  
  public static boolean f(Object paramObject)
  {
    return ((AccessibilityWindowInfo)paramObject).isFocused();
  }
  
  public static int g(Object paramObject)
  {
    return ((AccessibilityWindowInfo)paramObject).getChildCount();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/view/accessibility/AccessibilityWindowInfoCompatApi21.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */