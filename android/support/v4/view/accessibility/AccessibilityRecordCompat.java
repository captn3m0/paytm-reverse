package android.support.v4.view.accessibility;

import android.os.Build.VERSION;
import android.view.View;

public class AccessibilityRecordCompat
{
  private static final AccessibilityRecordImpl a = new AccessibilityRecordStubImpl();
  private final Object b;
  
  static
  {
    if (Build.VERSION.SDK_INT >= 16)
    {
      a = new AccessibilityRecordJellyBeanImpl();
      return;
    }
    if (Build.VERSION.SDK_INT >= 15)
    {
      a = new AccessibilityRecordIcsMr1Impl();
      return;
    }
    if (Build.VERSION.SDK_INT >= 14)
    {
      a = new AccessibilityRecordIcsImpl();
      return;
    }
  }
  
  public AccessibilityRecordCompat(Object paramObject)
  {
    this.b = paramObject;
  }
  
  public void a(int paramInt)
  {
    a.b(this.b, paramInt);
  }
  
  public void a(View paramView, int paramInt)
  {
    a.a(this.b, paramView, paramInt);
  }
  
  public void a(boolean paramBoolean)
  {
    a.a(this.b, paramBoolean);
  }
  
  public void b(int paramInt)
  {
    a.a(this.b, paramInt);
  }
  
  public void c(int paramInt)
  {
    a.e(this.b, paramInt);
  }
  
  public void d(int paramInt)
  {
    a.c(this.b, paramInt);
  }
  
  public void e(int paramInt)
  {
    a.d(this.b, paramInt);
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      do
      {
        return true;
        if (paramObject == null) {
          return false;
        }
        if (getClass() != paramObject.getClass()) {
          return false;
        }
        paramObject = (AccessibilityRecordCompat)paramObject;
        if (this.b != null) {
          break;
        }
      } while (((AccessibilityRecordCompat)paramObject).b == null);
      return false;
    } while (this.b.equals(((AccessibilityRecordCompat)paramObject).b));
    return false;
  }
  
  public void f(int paramInt)
  {
    a.f(this.b, paramInt);
  }
  
  public void g(int paramInt)
  {
    a.g(this.b, paramInt);
  }
  
  public int hashCode()
  {
    if (this.b == null) {
      return 0;
    }
    return this.b.hashCode();
  }
  
  static class AccessibilityRecordIcsImpl
    extends AccessibilityRecordCompat.AccessibilityRecordStubImpl
  {
    public void a(Object paramObject, int paramInt)
    {
      AccessibilityRecordCompatIcs.a(paramObject, paramInt);
    }
    
    public void a(Object paramObject, boolean paramBoolean)
    {
      AccessibilityRecordCompatIcs.a(paramObject, paramBoolean);
    }
    
    public void b(Object paramObject, int paramInt)
    {
      AccessibilityRecordCompatIcs.b(paramObject, paramInt);
    }
    
    public void c(Object paramObject, int paramInt)
    {
      AccessibilityRecordCompatIcs.c(paramObject, paramInt);
    }
    
    public void d(Object paramObject, int paramInt)
    {
      AccessibilityRecordCompatIcs.d(paramObject, paramInt);
    }
    
    public void e(Object paramObject, int paramInt)
    {
      AccessibilityRecordCompatIcs.e(paramObject, paramInt);
    }
  }
  
  static class AccessibilityRecordIcsMr1Impl
    extends AccessibilityRecordCompat.AccessibilityRecordIcsImpl
  {
    public void f(Object paramObject, int paramInt)
    {
      AccessibilityRecordCompatIcsMr1.a(paramObject, paramInt);
    }
    
    public void g(Object paramObject, int paramInt)
    {
      AccessibilityRecordCompatIcsMr1.b(paramObject, paramInt);
    }
  }
  
  static abstract interface AccessibilityRecordImpl
  {
    public abstract void a(Object paramObject, int paramInt);
    
    public abstract void a(Object paramObject, View paramView, int paramInt);
    
    public abstract void a(Object paramObject, boolean paramBoolean);
    
    public abstract void b(Object paramObject, int paramInt);
    
    public abstract void c(Object paramObject, int paramInt);
    
    public abstract void d(Object paramObject, int paramInt);
    
    public abstract void e(Object paramObject, int paramInt);
    
    public abstract void f(Object paramObject, int paramInt);
    
    public abstract void g(Object paramObject, int paramInt);
  }
  
  static class AccessibilityRecordJellyBeanImpl
    extends AccessibilityRecordCompat.AccessibilityRecordIcsMr1Impl
  {
    public void a(Object paramObject, View paramView, int paramInt)
    {
      AccessibilityRecordCompatJellyBean.a(paramObject, paramView, paramInt);
    }
  }
  
  static class AccessibilityRecordStubImpl
    implements AccessibilityRecordCompat.AccessibilityRecordImpl
  {
    public void a(Object paramObject, int paramInt) {}
    
    public void a(Object paramObject, View paramView, int paramInt) {}
    
    public void a(Object paramObject, boolean paramBoolean) {}
    
    public void b(Object paramObject, int paramInt) {}
    
    public void c(Object paramObject, int paramInt) {}
    
    public void d(Object paramObject, int paramInt) {}
    
    public void e(Object paramObject, int paramInt) {}
    
    public void f(Object paramObject, int paramInt) {}
    
    public void g(Object paramObject, int paramInt) {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/view/accessibility/AccessibilityRecordCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */