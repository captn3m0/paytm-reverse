package android.support.v4.view.accessibility;

import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityManager.AccessibilityStateChangeListener;

class AccessibilityManagerCompatIcs
{
  public static Object a(AccessibilityStateChangeListenerBridge paramAccessibilityStateChangeListenerBridge)
  {
    new AccessibilityManager.AccessibilityStateChangeListener()
    {
      public void onAccessibilityStateChanged(boolean paramAnonymousBoolean)
      {
        this.a.a(paramAnonymousBoolean);
      }
    };
  }
  
  public static boolean a(AccessibilityManager paramAccessibilityManager)
  {
    return paramAccessibilityManager.isTouchExplorationEnabled();
  }
  
  static abstract interface AccessibilityStateChangeListenerBridge
  {
    public abstract void a(boolean paramBoolean);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/view/accessibility/AccessibilityManagerCompatIcs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */