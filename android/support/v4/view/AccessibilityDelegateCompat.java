package android.support.v4.view;

import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v4.view.accessibility.AccessibilityNodeProviderCompat;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

public class AccessibilityDelegateCompat
{
  private static final AccessibilityDelegateImpl a;
  private static final Object c;
  final Object b = a.a(this);
  
  static
  {
    if (Build.VERSION.SDK_INT >= 16) {
      a = new AccessibilityDelegateJellyBeanImpl();
    }
    for (;;)
    {
      c = a.a();
      return;
      if (Build.VERSION.SDK_INT >= 14) {
        a = new AccessibilityDelegateIcsImpl();
      } else {
        a = new AccessibilityDelegateStubImpl();
      }
    }
  }
  
  public AccessibilityNodeProviderCompat a(View paramView)
  {
    return a.a(c, paramView);
  }
  
  Object a()
  {
    return this.b;
  }
  
  public void a(View paramView, int paramInt)
  {
    a.a(c, paramView, paramInt);
  }
  
  public void a(View paramView, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat)
  {
    a.a(c, paramView, paramAccessibilityNodeInfoCompat);
  }
  
  public void a(View paramView, AccessibilityEvent paramAccessibilityEvent)
  {
    a.b(c, paramView, paramAccessibilityEvent);
  }
  
  public boolean a(View paramView, int paramInt, Bundle paramBundle)
  {
    return a.a(c, paramView, paramInt, paramBundle);
  }
  
  public boolean a(ViewGroup paramViewGroup, View paramView, AccessibilityEvent paramAccessibilityEvent)
  {
    return a.a(c, paramViewGroup, paramView, paramAccessibilityEvent);
  }
  
  public void b(View paramView, AccessibilityEvent paramAccessibilityEvent)
  {
    a.c(c, paramView, paramAccessibilityEvent);
  }
  
  public void c(View paramView, AccessibilityEvent paramAccessibilityEvent)
  {
    a.d(c, paramView, paramAccessibilityEvent);
  }
  
  public boolean d(View paramView, AccessibilityEvent paramAccessibilityEvent)
  {
    return a.a(c, paramView, paramAccessibilityEvent);
  }
  
  static class AccessibilityDelegateIcsImpl
    extends AccessibilityDelegateCompat.AccessibilityDelegateStubImpl
  {
    public Object a()
    {
      return AccessibilityDelegateCompatIcs.a();
    }
    
    public Object a(final AccessibilityDelegateCompat paramAccessibilityDelegateCompat)
    {
      AccessibilityDelegateCompatIcs.a(new AccessibilityDelegateCompatIcs.AccessibilityDelegateBridge()
      {
        public void a(View paramAnonymousView, int paramAnonymousInt)
        {
          paramAccessibilityDelegateCompat.a(paramAnonymousView, paramAnonymousInt);
        }
        
        public void a(View paramAnonymousView, Object paramAnonymousObject)
        {
          paramAccessibilityDelegateCompat.a(paramAnonymousView, new AccessibilityNodeInfoCompat(paramAnonymousObject));
        }
        
        public boolean a(View paramAnonymousView, AccessibilityEvent paramAnonymousAccessibilityEvent)
        {
          return paramAccessibilityDelegateCompat.d(paramAnonymousView, paramAnonymousAccessibilityEvent);
        }
        
        public boolean a(ViewGroup paramAnonymousViewGroup, View paramAnonymousView, AccessibilityEvent paramAnonymousAccessibilityEvent)
        {
          return paramAccessibilityDelegateCompat.a(paramAnonymousViewGroup, paramAnonymousView, paramAnonymousAccessibilityEvent);
        }
        
        public void b(View paramAnonymousView, AccessibilityEvent paramAnonymousAccessibilityEvent)
        {
          paramAccessibilityDelegateCompat.a(paramAnonymousView, paramAnonymousAccessibilityEvent);
        }
        
        public void c(View paramAnonymousView, AccessibilityEvent paramAnonymousAccessibilityEvent)
        {
          paramAccessibilityDelegateCompat.b(paramAnonymousView, paramAnonymousAccessibilityEvent);
        }
        
        public void d(View paramAnonymousView, AccessibilityEvent paramAnonymousAccessibilityEvent)
        {
          paramAccessibilityDelegateCompat.c(paramAnonymousView, paramAnonymousAccessibilityEvent);
        }
      });
    }
    
    public void a(Object paramObject, View paramView, int paramInt)
    {
      AccessibilityDelegateCompatIcs.a(paramObject, paramView, paramInt);
    }
    
    public void a(Object paramObject, View paramView, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat)
    {
      AccessibilityDelegateCompatIcs.a(paramObject, paramView, paramAccessibilityNodeInfoCompat.a());
    }
    
    public boolean a(Object paramObject, View paramView, AccessibilityEvent paramAccessibilityEvent)
    {
      return AccessibilityDelegateCompatIcs.a(paramObject, paramView, paramAccessibilityEvent);
    }
    
    public boolean a(Object paramObject, ViewGroup paramViewGroup, View paramView, AccessibilityEvent paramAccessibilityEvent)
    {
      return AccessibilityDelegateCompatIcs.a(paramObject, paramViewGroup, paramView, paramAccessibilityEvent);
    }
    
    public void b(Object paramObject, View paramView, AccessibilityEvent paramAccessibilityEvent)
    {
      AccessibilityDelegateCompatIcs.b(paramObject, paramView, paramAccessibilityEvent);
    }
    
    public void c(Object paramObject, View paramView, AccessibilityEvent paramAccessibilityEvent)
    {
      AccessibilityDelegateCompatIcs.c(paramObject, paramView, paramAccessibilityEvent);
    }
    
    public void d(Object paramObject, View paramView, AccessibilityEvent paramAccessibilityEvent)
    {
      AccessibilityDelegateCompatIcs.d(paramObject, paramView, paramAccessibilityEvent);
    }
  }
  
  static abstract interface AccessibilityDelegateImpl
  {
    public abstract AccessibilityNodeProviderCompat a(Object paramObject, View paramView);
    
    public abstract Object a();
    
    public abstract Object a(AccessibilityDelegateCompat paramAccessibilityDelegateCompat);
    
    public abstract void a(Object paramObject, View paramView, int paramInt);
    
    public abstract void a(Object paramObject, View paramView, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat);
    
    public abstract boolean a(Object paramObject, View paramView, int paramInt, Bundle paramBundle);
    
    public abstract boolean a(Object paramObject, View paramView, AccessibilityEvent paramAccessibilityEvent);
    
    public abstract boolean a(Object paramObject, ViewGroup paramViewGroup, View paramView, AccessibilityEvent paramAccessibilityEvent);
    
    public abstract void b(Object paramObject, View paramView, AccessibilityEvent paramAccessibilityEvent);
    
    public abstract void c(Object paramObject, View paramView, AccessibilityEvent paramAccessibilityEvent);
    
    public abstract void d(Object paramObject, View paramView, AccessibilityEvent paramAccessibilityEvent);
  }
  
  static class AccessibilityDelegateJellyBeanImpl
    extends AccessibilityDelegateCompat.AccessibilityDelegateIcsImpl
  {
    public AccessibilityNodeProviderCompat a(Object paramObject, View paramView)
    {
      paramObject = AccessibilityDelegateCompatJellyBean.a(paramObject, paramView);
      if (paramObject != null) {
        return new AccessibilityNodeProviderCompat(paramObject);
      }
      return null;
    }
    
    public Object a(final AccessibilityDelegateCompat paramAccessibilityDelegateCompat)
    {
      AccessibilityDelegateCompatJellyBean.a(new AccessibilityDelegateCompatJellyBean.AccessibilityDelegateBridgeJellyBean()
      {
        public Object a(View paramAnonymousView)
        {
          paramAnonymousView = paramAccessibilityDelegateCompat.a(paramAnonymousView);
          if (paramAnonymousView != null) {
            return paramAnonymousView.a();
          }
          return null;
        }
        
        public void a(View paramAnonymousView, int paramAnonymousInt)
        {
          paramAccessibilityDelegateCompat.a(paramAnonymousView, paramAnonymousInt);
        }
        
        public void a(View paramAnonymousView, Object paramAnonymousObject)
        {
          paramAccessibilityDelegateCompat.a(paramAnonymousView, new AccessibilityNodeInfoCompat(paramAnonymousObject));
        }
        
        public boolean a(View paramAnonymousView, int paramAnonymousInt, Bundle paramAnonymousBundle)
        {
          return paramAccessibilityDelegateCompat.a(paramAnonymousView, paramAnonymousInt, paramAnonymousBundle);
        }
        
        public boolean a(View paramAnonymousView, AccessibilityEvent paramAnonymousAccessibilityEvent)
        {
          return paramAccessibilityDelegateCompat.d(paramAnonymousView, paramAnonymousAccessibilityEvent);
        }
        
        public boolean a(ViewGroup paramAnonymousViewGroup, View paramAnonymousView, AccessibilityEvent paramAnonymousAccessibilityEvent)
        {
          return paramAccessibilityDelegateCompat.a(paramAnonymousViewGroup, paramAnonymousView, paramAnonymousAccessibilityEvent);
        }
        
        public void b(View paramAnonymousView, AccessibilityEvent paramAnonymousAccessibilityEvent)
        {
          paramAccessibilityDelegateCompat.a(paramAnonymousView, paramAnonymousAccessibilityEvent);
        }
        
        public void c(View paramAnonymousView, AccessibilityEvent paramAnonymousAccessibilityEvent)
        {
          paramAccessibilityDelegateCompat.b(paramAnonymousView, paramAnonymousAccessibilityEvent);
        }
        
        public void d(View paramAnonymousView, AccessibilityEvent paramAnonymousAccessibilityEvent)
        {
          paramAccessibilityDelegateCompat.c(paramAnonymousView, paramAnonymousAccessibilityEvent);
        }
      });
    }
    
    public boolean a(Object paramObject, View paramView, int paramInt, Bundle paramBundle)
    {
      return AccessibilityDelegateCompatJellyBean.a(paramObject, paramView, paramInt, paramBundle);
    }
  }
  
  static class AccessibilityDelegateStubImpl
    implements AccessibilityDelegateCompat.AccessibilityDelegateImpl
  {
    public AccessibilityNodeProviderCompat a(Object paramObject, View paramView)
    {
      return null;
    }
    
    public Object a()
    {
      return null;
    }
    
    public Object a(AccessibilityDelegateCompat paramAccessibilityDelegateCompat)
    {
      return null;
    }
    
    public void a(Object paramObject, View paramView, int paramInt) {}
    
    public void a(Object paramObject, View paramView, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat) {}
    
    public boolean a(Object paramObject, View paramView, int paramInt, Bundle paramBundle)
    {
      return false;
    }
    
    public boolean a(Object paramObject, View paramView, AccessibilityEvent paramAccessibilityEvent)
    {
      return false;
    }
    
    public boolean a(Object paramObject, ViewGroup paramViewGroup, View paramView, AccessibilityEvent paramAccessibilityEvent)
    {
      return true;
    }
    
    public void b(Object paramObject, View paramView, AccessibilityEvent paramAccessibilityEvent) {}
    
    public void c(Object paramObject, View paramView, AccessibilityEvent paramAccessibilityEvent) {}
    
    public void d(Object paramObject, View paramView, AccessibilityEvent paramAccessibilityEvent) {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/view/AccessibilityDelegateCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */