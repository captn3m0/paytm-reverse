package android.support.v4.view;

import android.os.Build.VERSION;

public final class ScaleGestureDetectorCompat
{
  static final ScaleGestureDetectorImpl a = new BaseScaleGestureDetectorImpl(null);
  
  static
  {
    if (Build.VERSION.SDK_INT >= 19)
    {
      a = new ScaleGestureDetectorCompatKitKatImpl(null);
      return;
    }
  }
  
  private static class BaseScaleGestureDetectorImpl
    implements ScaleGestureDetectorCompat.ScaleGestureDetectorImpl
  {}
  
  private static class ScaleGestureDetectorCompatKitKatImpl
    implements ScaleGestureDetectorCompat.ScaleGestureDetectorImpl
  {}
  
  static abstract interface ScaleGestureDetectorImpl {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/view/ScaleGestureDetectorCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */