package android.support.v4.view;

import android.os.Build.VERSION;
import android.support.v4.internal.view.SupportMenuItem;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

public final class MenuItemCompat
{
  static final MenuVersionImpl a = new BaseMenuVersionImpl();
  
  static
  {
    int i = Build.VERSION.SDK_INT;
    if (i >= 14)
    {
      a = new IcsMenuVersionImpl();
      return;
    }
    if (i >= 11)
    {
      a = new HoneycombMenuVersionImpl();
      return;
    }
  }
  
  public static MenuItem a(MenuItem paramMenuItem, ActionProvider paramActionProvider)
  {
    if ((paramMenuItem instanceof SupportMenuItem)) {
      return ((SupportMenuItem)paramMenuItem).a(paramActionProvider);
    }
    Log.w("MenuItemCompat", "setActionProvider: item does not implement SupportMenuItem; ignoring");
    return paramMenuItem;
  }
  
  public static MenuItem a(MenuItem paramMenuItem, View paramView)
  {
    if ((paramMenuItem instanceof SupportMenuItem)) {
      return ((SupportMenuItem)paramMenuItem).setActionView(paramView);
    }
    return a.a(paramMenuItem, paramView);
  }
  
  public static View a(MenuItem paramMenuItem)
  {
    if ((paramMenuItem instanceof SupportMenuItem)) {
      return ((SupportMenuItem)paramMenuItem).getActionView();
    }
    return a.a(paramMenuItem);
  }
  
  public static void a(MenuItem paramMenuItem, int paramInt)
  {
    if ((paramMenuItem instanceof SupportMenuItem))
    {
      ((SupportMenuItem)paramMenuItem).setShowAsAction(paramInt);
      return;
    }
    a.a(paramMenuItem, paramInt);
  }
  
  public static MenuItem b(MenuItem paramMenuItem, int paramInt)
  {
    if ((paramMenuItem instanceof SupportMenuItem)) {
      return ((SupportMenuItem)paramMenuItem).setActionView(paramInt);
    }
    return a.b(paramMenuItem, paramInt);
  }
  
  public static boolean b(MenuItem paramMenuItem)
  {
    if ((paramMenuItem instanceof SupportMenuItem)) {
      return ((SupportMenuItem)paramMenuItem).expandActionView();
    }
    return a.b(paramMenuItem);
  }
  
  public static boolean c(MenuItem paramMenuItem)
  {
    if ((paramMenuItem instanceof SupportMenuItem)) {
      return ((SupportMenuItem)paramMenuItem).collapseActionView();
    }
    return a.c(paramMenuItem);
  }
  
  public static boolean d(MenuItem paramMenuItem)
  {
    if ((paramMenuItem instanceof SupportMenuItem)) {
      return ((SupportMenuItem)paramMenuItem).isActionViewExpanded();
    }
    return a.d(paramMenuItem);
  }
  
  static class BaseMenuVersionImpl
    implements MenuItemCompat.MenuVersionImpl
  {
    public MenuItem a(MenuItem paramMenuItem, View paramView)
    {
      return paramMenuItem;
    }
    
    public View a(MenuItem paramMenuItem)
    {
      return null;
    }
    
    public void a(MenuItem paramMenuItem, int paramInt) {}
    
    public MenuItem b(MenuItem paramMenuItem, int paramInt)
    {
      return paramMenuItem;
    }
    
    public boolean b(MenuItem paramMenuItem)
    {
      return false;
    }
    
    public boolean c(MenuItem paramMenuItem)
    {
      return false;
    }
    
    public boolean d(MenuItem paramMenuItem)
    {
      return false;
    }
  }
  
  static class HoneycombMenuVersionImpl
    implements MenuItemCompat.MenuVersionImpl
  {
    public MenuItem a(MenuItem paramMenuItem, View paramView)
    {
      return MenuItemCompatHoneycomb.a(paramMenuItem, paramView);
    }
    
    public View a(MenuItem paramMenuItem)
    {
      return MenuItemCompatHoneycomb.a(paramMenuItem);
    }
    
    public void a(MenuItem paramMenuItem, int paramInt)
    {
      MenuItemCompatHoneycomb.a(paramMenuItem, paramInt);
    }
    
    public MenuItem b(MenuItem paramMenuItem, int paramInt)
    {
      return MenuItemCompatHoneycomb.b(paramMenuItem, paramInt);
    }
    
    public boolean b(MenuItem paramMenuItem)
    {
      return false;
    }
    
    public boolean c(MenuItem paramMenuItem)
    {
      return false;
    }
    
    public boolean d(MenuItem paramMenuItem)
    {
      return false;
    }
  }
  
  static class IcsMenuVersionImpl
    extends MenuItemCompat.HoneycombMenuVersionImpl
  {
    public boolean b(MenuItem paramMenuItem)
    {
      return MenuItemCompatIcs.a(paramMenuItem);
    }
    
    public boolean c(MenuItem paramMenuItem)
    {
      return MenuItemCompatIcs.b(paramMenuItem);
    }
    
    public boolean d(MenuItem paramMenuItem)
    {
      return MenuItemCompatIcs.c(paramMenuItem);
    }
  }
  
  static abstract interface MenuVersionImpl
  {
    public abstract MenuItem a(MenuItem paramMenuItem, View paramView);
    
    public abstract View a(MenuItem paramMenuItem);
    
    public abstract void a(MenuItem paramMenuItem, int paramInt);
    
    public abstract MenuItem b(MenuItem paramMenuItem, int paramInt);
    
    public abstract boolean b(MenuItem paramMenuItem);
    
    public abstract boolean c(MenuItem paramMenuItem);
    
    public abstract boolean d(MenuItem paramMenuItem);
  }
  
  public static abstract interface OnActionExpandListener
  {
    public abstract boolean a(MenuItem paramMenuItem);
    
    public abstract boolean b(MenuItem paramMenuItem);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/view/MenuItemCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */