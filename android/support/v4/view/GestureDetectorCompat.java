package android.support.v4.view;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;

public final class GestureDetectorCompat
{
  private final GestureDetectorCompatImpl a;
  
  public GestureDetectorCompat(Context paramContext, GestureDetector.OnGestureListener paramOnGestureListener)
  {
    this(paramContext, paramOnGestureListener, null);
  }
  
  public GestureDetectorCompat(Context paramContext, GestureDetector.OnGestureListener paramOnGestureListener, Handler paramHandler)
  {
    if (Build.VERSION.SDK_INT > 17)
    {
      this.a = new GestureDetectorCompatImplJellybeanMr2(paramContext, paramOnGestureListener, paramHandler);
      return;
    }
    this.a = new GestureDetectorCompatImplBase(paramContext, paramOnGestureListener, paramHandler);
  }
  
  public boolean a(MotionEvent paramMotionEvent)
  {
    return this.a.a(paramMotionEvent);
  }
  
  static abstract interface GestureDetectorCompatImpl
  {
    public abstract boolean a(MotionEvent paramMotionEvent);
  }
  
  static class GestureDetectorCompatImplBase
    implements GestureDetectorCompat.GestureDetectorCompatImpl
  {
    private static final int e = ;
    private static final int f = ViewConfiguration.getTapTimeout();
    private static final int g = ViewConfiguration.getDoubleTapTimeout();
    private int a;
    private int b;
    private int c;
    private int d;
    private final Handler h;
    private final GestureDetector.OnGestureListener i;
    private GestureDetector.OnDoubleTapListener j;
    private boolean k;
    private boolean l;
    private boolean m;
    private boolean n;
    private boolean o;
    private MotionEvent p;
    private MotionEvent q;
    private boolean r;
    private float s;
    private float t;
    private float u;
    private float v;
    private boolean w;
    private VelocityTracker x;
    
    public GestureDetectorCompatImplBase(Context paramContext, GestureDetector.OnGestureListener paramOnGestureListener, Handler paramHandler)
    {
      if (paramHandler != null) {}
      for (this.h = new GestureHandler(paramHandler);; this.h = new GestureHandler())
      {
        this.i = paramOnGestureListener;
        if ((paramOnGestureListener instanceof GestureDetector.OnDoubleTapListener)) {
          a((GestureDetector.OnDoubleTapListener)paramOnGestureListener);
        }
        a(paramContext);
        return;
      }
    }
    
    private void a()
    {
      this.h.removeMessages(1);
      this.h.removeMessages(2);
      this.h.removeMessages(3);
      this.r = false;
      this.n = false;
      this.o = false;
      this.l = false;
      if (this.m) {
        this.m = false;
      }
    }
    
    private void a(Context paramContext)
    {
      if (paramContext == null) {
        throw new IllegalArgumentException("Context must not be null");
      }
      if (this.i == null) {
        throw new IllegalArgumentException("OnGestureListener must not be null");
      }
      this.w = true;
      paramContext = ViewConfiguration.get(paramContext);
      int i1 = paramContext.getScaledTouchSlop();
      int i2 = paramContext.getScaledDoubleTapSlop();
      this.c = paramContext.getScaledMinimumFlingVelocity();
      this.d = paramContext.getScaledMaximumFlingVelocity();
      this.a = (i1 * i1);
      this.b = (i2 * i2);
    }
    
    private boolean a(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, MotionEvent paramMotionEvent3)
    {
      if (!this.o) {}
      int i1;
      int i2;
      do
      {
        do
        {
          return false;
        } while (paramMotionEvent3.getEventTime() - paramMotionEvent2.getEventTime() > g);
        i1 = (int)paramMotionEvent1.getX() - (int)paramMotionEvent3.getX();
        i2 = (int)paramMotionEvent1.getY() - (int)paramMotionEvent3.getY();
      } while (i1 * i1 + i2 * i2 >= this.b);
      return true;
    }
    
    private void b()
    {
      this.h.removeMessages(3);
      this.l = false;
      this.m = true;
      this.i.onLongPress(this.p);
    }
    
    private void cancel()
    {
      this.h.removeMessages(1);
      this.h.removeMessages(2);
      this.h.removeMessages(3);
      this.x.recycle();
      this.x = null;
      this.r = false;
      this.k = false;
      this.n = false;
      this.o = false;
      this.l = false;
      if (this.m) {
        this.m = false;
      }
    }
    
    public void a(GestureDetector.OnDoubleTapListener paramOnDoubleTapListener)
    {
      this.j = paramOnDoubleTapListener;
    }
    
    public boolean a(MotionEvent paramMotionEvent)
    {
      int i6 = paramMotionEvent.getAction();
      if (this.x == null) {
        this.x = VelocityTracker.obtain();
      }
      this.x.addMovement(paramMotionEvent);
      int i1;
      int i3;
      label53:
      float f2;
      float f1;
      int i5;
      int i4;
      if ((i6 & 0xFF) == 6)
      {
        i1 = 1;
        if (i1 == 0) {
          break label95;
        }
        i3 = MotionEventCompat.b(paramMotionEvent);
        f2 = 0.0F;
        f1 = 0.0F;
        i5 = MotionEventCompat.c(paramMotionEvent);
        i4 = 0;
        label66:
        if (i4 >= i5) {
          break label122;
        }
        if (i3 != i4) {
          break label101;
        }
      }
      for (;;)
      {
        i4 += 1;
        break label66;
        i1 = 0;
        break;
        label95:
        i3 = -1;
        break label53;
        label101:
        f2 += MotionEventCompat.c(paramMotionEvent, i4);
        f1 += MotionEventCompat.d(paramMotionEvent, i4);
      }
      label122:
      boolean bool4;
      boolean bool5;
      boolean bool2;
      boolean bool3;
      if (i1 != 0)
      {
        i1 = i5 - 1;
        f2 /= i1;
        f1 /= i1;
        i3 = 0;
        bool4 = false;
        bool5 = false;
        bool2 = false;
        bool3 = bool2;
      }
      label637:
      int i2;
      switch (i6 & 0xFF)
      {
      default: 
        bool3 = bool2;
      case 4: 
      case 5: 
      case 6: 
      case 0: 
      case 2: 
        float f3;
        float f4;
        do
        {
          do
          {
            do
            {
              do
              {
                return bool3;
                i1 = i5;
                break;
                this.s = f2;
                this.u = f2;
                this.t = f1;
                this.v = f1;
                a();
                return false;
                this.s = f2;
                this.u = f2;
                this.t = f1;
                this.v = f1;
                this.x.computeCurrentVelocity(1000, this.d);
                i3 = MotionEventCompat.b(paramMotionEvent);
                i1 = MotionEventCompat.b(paramMotionEvent, i3);
                f1 = VelocityTrackerCompat.a(this.x, i1);
                f2 = VelocityTrackerCompat.b(this.x, i1);
                i1 = 0;
                bool3 = bool2;
              } while (i1 >= i5);
              if (i1 == i3) {}
              do
              {
                i1 += 1;
                break;
                i4 = MotionEventCompat.b(paramMotionEvent, i1);
              } while (f1 * VelocityTrackerCompat.a(this.x, i4) + f2 * VelocityTrackerCompat.b(this.x, i4) >= 0.0F);
              this.x.clear();
              return false;
              i1 = i3;
              if (this.j != null)
              {
                bool2 = this.h.hasMessages(3);
                if (bool2) {
                  this.h.removeMessages(3);
                }
                if ((this.p == null) || (this.q == null) || (!bool2) || (!a(this.p, this.q, paramMotionEvent))) {
                  break label637;
                }
                this.r = true;
              }
              for (boolean bool1 = false | this.j.onDoubleTap(this.p) | this.j.onDoubleTapEvent(paramMotionEvent);; bool1 = i3)
              {
                this.s = f2;
                this.u = f2;
                this.t = f1;
                this.v = f1;
                if (this.p != null) {
                  this.p.recycle();
                }
                this.p = MotionEvent.obtain(paramMotionEvent);
                this.n = true;
                this.o = true;
                this.k = true;
                this.m = false;
                this.l = false;
                if (this.w)
                {
                  this.h.removeMessages(2);
                  this.h.sendEmptyMessageAtTime(2, this.p.getDownTime() + f + e);
                }
                this.h.sendEmptyMessageAtTime(1, this.p.getDownTime() + f);
                return bool1 | this.i.onDown(paramMotionEvent);
                this.h.sendEmptyMessageDelayed(3, g);
              }
              bool3 = bool2;
            } while (this.m);
            f3 = this.s - f2;
            f4 = this.t - f1;
            if (this.r) {
              return false | this.j.onDoubleTapEvent(paramMotionEvent);
            }
            if (!this.n) {
              break label835;
            }
            i2 = (int)(f2 - this.u);
            i3 = (int)(f1 - this.v);
            i2 = i2 * i2 + i3 * i3;
            bool2 = bool4;
            if (i2 > this.a)
            {
              bool2 = this.i.onScroll(this.p, paramMotionEvent, f3, f4);
              this.s = f2;
              this.t = f1;
              this.n = false;
              this.h.removeMessages(3);
              this.h.removeMessages(1);
              this.h.removeMessages(2);
            }
            bool3 = bool2;
          } while (i2 <= this.a);
          this.o = false;
          return bool2;
          if (Math.abs(f3) >= 1.0F) {
            break label859;
          }
          bool3 = bool2;
        } while (Math.abs(f4) < 1.0F);
        bool2 = this.i.onScroll(this.p, paramMotionEvent, f3, f4);
        this.s = f2;
        this.t = f1;
        return bool2;
      case 1: 
        label835:
        label859:
        this.k = false;
        MotionEvent localMotionEvent = MotionEvent.obtain(paramMotionEvent);
        if (this.r) {
          bool2 = false | this.j.onDoubleTapEvent(paramMotionEvent);
        }
        for (;;)
        {
          if (this.q != null) {
            this.q.recycle();
          }
          this.q = localMotionEvent;
          if (this.x != null)
          {
            this.x.recycle();
            this.x = null;
          }
          this.r = false;
          this.l = false;
          this.h.removeMessages(1);
          this.h.removeMessages(2);
          return bool2;
          if (this.m)
          {
            this.h.removeMessages(3);
            this.m = false;
            bool2 = bool5;
          }
          else if (this.n)
          {
            bool3 = this.i.onSingleTapUp(paramMotionEvent);
            bool2 = bool3;
            if (this.l)
            {
              bool2 = bool3;
              if (this.j != null)
              {
                this.j.onSingleTapConfirmed(paramMotionEvent);
                bool2 = bool3;
              }
            }
          }
          else
          {
            VelocityTracker localVelocityTracker = this.x;
            i2 = MotionEventCompat.b(paramMotionEvent, 0);
            localVelocityTracker.computeCurrentVelocity(1000, this.d);
            f1 = VelocityTrackerCompat.b(localVelocityTracker, i2);
            f2 = VelocityTrackerCompat.a(localVelocityTracker, i2);
            if (Math.abs(f1) <= this.c)
            {
              bool2 = bool5;
              if (Math.abs(f2) <= this.c) {}
            }
            else
            {
              bool2 = this.i.onFling(this.p, paramMotionEvent, f2, f1);
            }
          }
        }
      }
      cancel();
      return false;
    }
    
    private class GestureHandler
      extends Handler
    {
      GestureHandler() {}
      
      GestureHandler(Handler paramHandler)
      {
        super();
      }
      
      public void handleMessage(Message paramMessage)
      {
        switch (paramMessage.what)
        {
        default: 
          throw new RuntimeException("Unknown message " + paramMessage);
        case 1: 
          GestureDetectorCompat.GestureDetectorCompatImplBase.b(GestureDetectorCompat.GestureDetectorCompatImplBase.this).onShowPress(GestureDetectorCompat.GestureDetectorCompatImplBase.a(GestureDetectorCompat.GestureDetectorCompatImplBase.this));
        }
        do
        {
          return;
          GestureDetectorCompat.GestureDetectorCompatImplBase.c(GestureDetectorCompat.GestureDetectorCompatImplBase.this);
          return;
        } while (GestureDetectorCompat.GestureDetectorCompatImplBase.d(GestureDetectorCompat.GestureDetectorCompatImplBase.this) == null);
        if (!GestureDetectorCompat.GestureDetectorCompatImplBase.e(GestureDetectorCompat.GestureDetectorCompatImplBase.this))
        {
          GestureDetectorCompat.GestureDetectorCompatImplBase.d(GestureDetectorCompat.GestureDetectorCompatImplBase.this).onSingleTapConfirmed(GestureDetectorCompat.GestureDetectorCompatImplBase.a(GestureDetectorCompat.GestureDetectorCompatImplBase.this));
          return;
        }
        GestureDetectorCompat.GestureDetectorCompatImplBase.a(GestureDetectorCompat.GestureDetectorCompatImplBase.this, true);
      }
    }
  }
  
  static class GestureDetectorCompatImplJellybeanMr2
    implements GestureDetectorCompat.GestureDetectorCompatImpl
  {
    private final GestureDetector a;
    
    public GestureDetectorCompatImplJellybeanMr2(Context paramContext, GestureDetector.OnGestureListener paramOnGestureListener, Handler paramHandler)
    {
      this.a = new GestureDetector(paramContext, paramOnGestureListener, paramHandler);
    }
    
    public boolean a(MotionEvent paramMotionEvent)
    {
      return this.a.onTouchEvent(paramMotionEvent);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/view/GestureDetectorCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */