package android.support.v4.view;

public abstract interface NestedScrollingChild
{
  public abstract boolean isNestedScrollingEnabled();
  
  public abstract void stopNestedScroll();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/view/NestedScrollingChild.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */