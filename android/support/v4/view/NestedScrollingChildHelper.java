package android.support.v4.view;

import android.view.View;
import android.view.ViewParent;

public class NestedScrollingChildHelper
{
  private final View a;
  private ViewParent b;
  private boolean c;
  private int[] d;
  
  public NestedScrollingChildHelper(View paramView)
  {
    this.a = paramView;
  }
  
  public void a(boolean paramBoolean)
  {
    if (this.c) {
      ViewCompat.E(this.a);
    }
    this.c = paramBoolean;
  }
  
  public boolean a()
  {
    return this.c;
  }
  
  public boolean a(float paramFloat1, float paramFloat2)
  {
    if ((a()) && (this.b != null)) {
      return ViewParentCompat.a(this.b, this.a, paramFloat1, paramFloat2);
    }
    return false;
  }
  
  public boolean a(float paramFloat1, float paramFloat2, boolean paramBoolean)
  {
    if ((a()) && (this.b != null)) {
      return ViewParentCompat.a(this.b, this.a, paramFloat1, paramFloat2, paramBoolean);
    }
    return false;
  }
  
  public boolean a(int paramInt)
  {
    if (b()) {
      return true;
    }
    if (a())
    {
      ViewParent localViewParent = this.a.getParent();
      View localView = this.a;
      while (localViewParent != null)
      {
        if (ViewParentCompat.a(localViewParent, localView, this.a, paramInt))
        {
          this.b = localViewParent;
          ViewParentCompat.b(localViewParent, localView, this.a, paramInt);
          return true;
        }
        if ((localViewParent instanceof View)) {
          localView = (View)localViewParent;
        }
        localViewParent = localViewParent.getParent();
      }
    }
    return false;
  }
  
  public boolean a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[] paramArrayOfInt)
  {
    if ((a()) && (this.b != null))
    {
      if ((paramInt1 != 0) || (paramInt2 != 0) || (paramInt3 != 0) || (paramInt4 != 0))
      {
        int i = 0;
        int j = 0;
        if (paramArrayOfInt != null)
        {
          this.a.getLocationInWindow(paramArrayOfInt);
          i = paramArrayOfInt[0];
          j = paramArrayOfInt[1];
        }
        ViewParentCompat.a(this.b, this.a, paramInt1, paramInt2, paramInt3, paramInt4);
        if (paramArrayOfInt != null)
        {
          this.a.getLocationInWindow(paramArrayOfInt);
          paramArrayOfInt[0] -= i;
          paramArrayOfInt[1] -= j;
        }
        return true;
      }
      if (paramArrayOfInt != null)
      {
        paramArrayOfInt[0] = 0;
        paramArrayOfInt[1] = 0;
      }
    }
    return false;
  }
  
  public boolean a(int paramInt1, int paramInt2, int[] paramArrayOfInt1, int[] paramArrayOfInt2)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (a())
    {
      bool1 = bool2;
      if (this.b != null)
      {
        if ((paramInt1 == 0) && (paramInt2 == 0)) {
          break label177;
        }
        int i = 0;
        int j = 0;
        if (paramArrayOfInt2 != null)
        {
          this.a.getLocationInWindow(paramArrayOfInt2);
          i = paramArrayOfInt2[0];
          j = paramArrayOfInt2[1];
        }
        int[] arrayOfInt = paramArrayOfInt1;
        if (paramArrayOfInt1 == null)
        {
          if (this.d == null) {
            this.d = new int[2];
          }
          arrayOfInt = this.d;
        }
        arrayOfInt[0] = 0;
        arrayOfInt[1] = 0;
        ViewParentCompat.a(this.b, this.a, paramInt1, paramInt2, arrayOfInt);
        if (paramArrayOfInt2 != null)
        {
          this.a.getLocationInWindow(paramArrayOfInt2);
          paramArrayOfInt2[0] -= i;
          paramArrayOfInt2[1] -= j;
        }
        if (arrayOfInt[0] == 0)
        {
          bool1 = bool2;
          if (arrayOfInt[1] == 0) {}
        }
        else
        {
          bool1 = true;
        }
      }
    }
    label177:
    do
    {
      return bool1;
      bool1 = bool2;
    } while (paramArrayOfInt2 == null);
    paramArrayOfInt2[0] = 0;
    paramArrayOfInt2[1] = 0;
    return false;
  }
  
  public boolean b()
  {
    return this.b != null;
  }
  
  public void c()
  {
    if (this.b != null)
    {
      ViewParentCompat.a(this.b, this.a);
      this.b = null;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/view/NestedScrollingChildHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */