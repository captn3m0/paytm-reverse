package android.support.v4.view;

import android.content.res.ColorStateList;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.FloatRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Field;
import java.util.WeakHashMap;

public final class ViewCompat
{
  static final ViewCompatImpl a = new BaseViewCompatImpl();
  
  static
  {
    int i = Build.VERSION.SDK_INT;
    if (i >= 23)
    {
      a = new MarshmallowViewCompatImpl();
      return;
    }
    if (i >= 21)
    {
      a = new LollipopViewCompatImpl();
      return;
    }
    if (i >= 19)
    {
      a = new KitKatViewCompatImpl();
      return;
    }
    if (i >= 17)
    {
      a = new JbMr1ViewCompatImpl();
      return;
    }
    if (i >= 16)
    {
      a = new JBViewCompatImpl();
      return;
    }
    if (i >= 15)
    {
      a = new ICSMr1ViewCompatImpl();
      return;
    }
    if (i >= 14)
    {
      a = new ICSViewCompatImpl();
      return;
    }
    if (i >= 11)
    {
      a = new HCViewCompatImpl();
      return;
    }
    if (i >= 9)
    {
      a = new GBViewCompatImpl();
      return;
    }
    if (i >= 7)
    {
      a = new EclairMr1ViewCompatImpl();
      return;
    }
  }
  
  public static boolean A(View paramView)
  {
    return a.B(paramView);
  }
  
  public static ColorStateList B(View paramView)
  {
    return a.D(paramView);
  }
  
  public static PorterDuff.Mode C(View paramView)
  {
    return a.E(paramView);
  }
  
  public static boolean D(View paramView)
  {
    return a.C(paramView);
  }
  
  public static void E(View paramView)
  {
    a.F(paramView);
  }
  
  public static boolean F(View paramView)
  {
    return a.G(paramView);
  }
  
  public static float G(View paramView)
  {
    return a.H(paramView);
  }
  
  public static boolean H(View paramView)
  {
    return a.I(paramView);
  }
  
  public static boolean I(View paramView)
  {
    return a.J(paramView);
  }
  
  public static int a(int paramInt1, int paramInt2)
  {
    return a.a(paramInt1, paramInt2);
  }
  
  public static int a(int paramInt1, int paramInt2, int paramInt3)
  {
    return a.a(paramInt1, paramInt2, paramInt3);
  }
  
  public static int a(View paramView)
  {
    return a.a(paramView);
  }
  
  public static WindowInsetsCompat a(View paramView, WindowInsetsCompat paramWindowInsetsCompat)
  {
    return a.a(paramView, paramWindowInsetsCompat);
  }
  
  public static void a(View paramView, float paramFloat)
  {
    a.a(paramView, paramFloat);
  }
  
  public static void a(@NonNull View paramView, int paramInt1, int paramInt2)
  {
    a.a(paramView, paramInt1, paramInt2);
  }
  
  public static void a(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    a.a(paramView, paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public static void a(View paramView, int paramInt, Paint paramPaint)
  {
    a.a(paramView, paramInt, paramPaint);
  }
  
  public static void a(View paramView, ColorStateList paramColorStateList)
  {
    a.a(paramView, paramColorStateList);
  }
  
  public static void a(View paramView, Paint paramPaint)
  {
    a.a(paramView, paramPaint);
  }
  
  public static void a(View paramView, PorterDuff.Mode paramMode)
  {
    a.a(paramView, paramMode);
  }
  
  public static void a(View paramView, AccessibilityDelegateCompat paramAccessibilityDelegateCompat)
  {
    a.a(paramView, paramAccessibilityDelegateCompat);
  }
  
  public static void a(View paramView, OnApplyWindowInsetsListener paramOnApplyWindowInsetsListener)
  {
    a.a(paramView, paramOnApplyWindowInsetsListener);
  }
  
  public static void a(View paramView, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat)
  {
    a.a(paramView, paramAccessibilityNodeInfoCompat);
  }
  
  public static void a(View paramView, AccessibilityEvent paramAccessibilityEvent)
  {
    a.a(paramView, paramAccessibilityEvent);
  }
  
  public static void a(View paramView, Runnable paramRunnable)
  {
    a.a(paramView, paramRunnable);
  }
  
  public static void a(View paramView, Runnable paramRunnable, long paramLong)
  {
    a.a(paramView, paramRunnable, paramLong);
  }
  
  public static void a(View paramView, boolean paramBoolean)
  {
    a.a(paramView, paramBoolean);
  }
  
  public static void a(ViewGroup paramViewGroup, boolean paramBoolean)
  {
    a.a(paramViewGroup, paramBoolean);
  }
  
  public static boolean a(View paramView, int paramInt)
  {
    return a.a(paramView, paramInt);
  }
  
  public static boolean a(View paramView, int paramInt, Bundle paramBundle)
  {
    return a.a(paramView, paramInt, paramBundle);
  }
  
  public static WindowInsetsCompat b(View paramView, WindowInsetsCompat paramWindowInsetsCompat)
  {
    return a.b(paramView, paramWindowInsetsCompat);
  }
  
  public static void b(View paramView, float paramFloat)
  {
    a.b(paramView, paramFloat);
  }
  
  public static void b(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    a.b(paramView, paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public static void b(View paramView, boolean paramBoolean)
  {
    a.b(paramView, paramBoolean);
  }
  
  public static boolean b(View paramView)
  {
    return a.b(paramView);
  }
  
  public static boolean b(View paramView, int paramInt)
  {
    return a.b(paramView, paramInt);
  }
  
  public static void c(View paramView, @FloatRange float paramFloat)
  {
    a.c(paramView, paramFloat);
  }
  
  public static void c(View paramView, int paramInt)
  {
    a.c(paramView, paramInt);
  }
  
  public static void c(View paramView, boolean paramBoolean)
  {
    a.c(paramView, paramBoolean);
  }
  
  public static boolean c(View paramView)
  {
    return a.c(paramView);
  }
  
  public static void d(View paramView)
  {
    a.d(paramView);
  }
  
  public static void d(View paramView, float paramFloat)
  {
    a.d(paramView, paramFloat);
  }
  
  public static void d(View paramView, int paramInt)
  {
    a.d(paramView, paramInt);
  }
  
  public static int e(View paramView)
  {
    return a.e(paramView);
  }
  
  public static void e(View paramView, float paramFloat)
  {
    a.e(paramView, paramFloat);
  }
  
  public static void e(View paramView, int paramInt)
  {
    a.f(paramView, paramInt);
  }
  
  public static float f(View paramView)
  {
    return a.f(paramView);
  }
  
  public static void f(View paramView, float paramFloat)
  {
    a.f(paramView, paramFloat);
  }
  
  public static void f(View paramView, int paramInt)
  {
    a.e(paramView, paramInt);
  }
  
  public static int g(View paramView)
  {
    return a.g(paramView);
  }
  
  public static int h(View paramView)
  {
    return a.h(paramView);
  }
  
  public static ViewParent i(View paramView)
  {
    return a.i(paramView);
  }
  
  public static boolean j(View paramView)
  {
    return a.j(paramView);
  }
  
  public static int k(View paramView)
  {
    return a.k(paramView);
  }
  
  public static int l(View paramView)
  {
    return a.l(paramView);
  }
  
  public static int m(View paramView)
  {
    return a.m(paramView);
  }
  
  public static int n(View paramView)
  {
    return a.n(paramView);
  }
  
  public static float o(View paramView)
  {
    return a.p(paramView);
  }
  
  public static float p(View paramView)
  {
    return a.q(paramView);
  }
  
  public static int q(View paramView)
  {
    return a.s(paramView);
  }
  
  public static int r(View paramView)
  {
    return a.t(paramView);
  }
  
  public static ViewPropertyAnimatorCompat s(View paramView)
  {
    return a.u(paramView);
  }
  
  public static float t(View paramView)
  {
    return a.r(paramView);
  }
  
  public static float u(View paramView)
  {
    return a.x(paramView);
  }
  
  public static int v(View paramView)
  {
    return a.v(paramView);
  }
  
  public static void w(View paramView)
  {
    a.w(paramView);
  }
  
  public static boolean x(View paramView)
  {
    return a.z(paramView);
  }
  
  public static void y(View paramView)
  {
    a.A(paramView);
  }
  
  public static boolean z(View paramView)
  {
    return a.o(paramView);
  }
  
  static class BaseViewCompatImpl
    implements ViewCompat.ViewCompatImpl
  {
    WeakHashMap<View, ViewPropertyAnimatorCompat> a = null;
    
    private boolean a(ScrollingView paramScrollingView, int paramInt)
    {
      boolean bool = true;
      int i = paramScrollingView.computeHorizontalScrollOffset();
      int j = paramScrollingView.computeHorizontalScrollRange() - paramScrollingView.computeHorizontalScrollExtent();
      if (j == 0) {
        bool = false;
      }
      do
      {
        do
        {
          return bool;
          if (paramInt >= 0) {
            break;
          }
        } while (i > 0);
        return false;
      } while (i < j - 1);
      return false;
    }
    
    private boolean b(ScrollingView paramScrollingView, int paramInt)
    {
      boolean bool = true;
      int i = paramScrollingView.computeVerticalScrollOffset();
      int j = paramScrollingView.computeVerticalScrollRange() - paramScrollingView.computeVerticalScrollExtent();
      if (j == 0) {
        bool = false;
      }
      do
      {
        do
        {
          return bool;
          if (paramInt >= 0) {
            break;
          }
        } while (i > 0);
        return false;
      } while (i < j - 1);
      return false;
    }
    
    public void A(View paramView) {}
    
    public boolean B(View paramView)
    {
      return false;
    }
    
    public boolean C(View paramView)
    {
      if ((paramView instanceof NestedScrollingChild)) {
        return ((NestedScrollingChild)paramView).isNestedScrollingEnabled();
      }
      return false;
    }
    
    public ColorStateList D(View paramView)
    {
      return ViewCompatBase.a(paramView);
    }
    
    public PorterDuff.Mode E(View paramView)
    {
      return ViewCompatBase.b(paramView);
    }
    
    public void F(View paramView)
    {
      if ((paramView instanceof NestedScrollingChild)) {
        ((NestedScrollingChild)paramView).stopNestedScroll();
      }
    }
    
    public boolean G(View paramView)
    {
      return ViewCompatBase.c(paramView);
    }
    
    public float H(View paramView)
    {
      return y(paramView) + x(paramView);
    }
    
    public boolean I(View paramView)
    {
      return ViewCompatBase.f(paramView);
    }
    
    public boolean J(View paramView)
    {
      return false;
    }
    
    public int a(int paramInt1, int paramInt2)
    {
      return paramInt1 | paramInt2;
    }
    
    public int a(int paramInt1, int paramInt2, int paramInt3)
    {
      return View.resolveSize(paramInt1, paramInt2);
    }
    
    public int a(View paramView)
    {
      return 2;
    }
    
    long a()
    {
      return 10L;
    }
    
    public WindowInsetsCompat a(View paramView, WindowInsetsCompat paramWindowInsetsCompat)
    {
      return paramWindowInsetsCompat;
    }
    
    public void a(View paramView, float paramFloat) {}
    
    public void a(View paramView, int paramInt1, int paramInt2) {}
    
    public void a(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      paramView.invalidate(paramInt1, paramInt2, paramInt3, paramInt4);
    }
    
    public void a(View paramView, int paramInt, Paint paramPaint) {}
    
    public void a(View paramView, ColorStateList paramColorStateList)
    {
      ViewCompatBase.a(paramView, paramColorStateList);
    }
    
    public void a(View paramView, Paint paramPaint) {}
    
    public void a(View paramView, PorterDuff.Mode paramMode)
    {
      ViewCompatBase.a(paramView, paramMode);
    }
    
    public void a(View paramView, AccessibilityDelegateCompat paramAccessibilityDelegateCompat) {}
    
    public void a(View paramView, OnApplyWindowInsetsListener paramOnApplyWindowInsetsListener) {}
    
    public void a(View paramView, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat) {}
    
    public void a(View paramView, AccessibilityEvent paramAccessibilityEvent) {}
    
    public void a(View paramView, Runnable paramRunnable)
    {
      paramView.postDelayed(paramRunnable, a());
    }
    
    public void a(View paramView, Runnable paramRunnable, long paramLong)
    {
      paramView.postDelayed(paramRunnable, a() + paramLong);
    }
    
    public void a(View paramView, boolean paramBoolean) {}
    
    public void a(ViewGroup paramViewGroup, boolean paramBoolean) {}
    
    public boolean a(View paramView, int paramInt)
    {
      return ((paramView instanceof ScrollingView)) && (a((ScrollingView)paramView, paramInt));
    }
    
    public boolean a(View paramView, int paramInt, Bundle paramBundle)
    {
      return false;
    }
    
    public WindowInsetsCompat b(View paramView, WindowInsetsCompat paramWindowInsetsCompat)
    {
      return paramWindowInsetsCompat;
    }
    
    public void b(View paramView, float paramFloat) {}
    
    public void b(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      paramView.setPadding(paramInt1, paramInt2, paramInt3, paramInt4);
    }
    
    public void b(View paramView, boolean paramBoolean) {}
    
    public boolean b(View paramView)
    {
      return false;
    }
    
    public boolean b(View paramView, int paramInt)
    {
      return ((paramView instanceof ScrollingView)) && (b((ScrollingView)paramView, paramInt));
    }
    
    public void c(View paramView, float paramFloat) {}
    
    public void c(View paramView, int paramInt) {}
    
    public void c(View paramView, boolean paramBoolean) {}
    
    public boolean c(View paramView)
    {
      return false;
    }
    
    public void d(View paramView)
    {
      paramView.invalidate();
    }
    
    public void d(View paramView, float paramFloat) {}
    
    public void d(View paramView, int paramInt) {}
    
    public int e(View paramView)
    {
      return 0;
    }
    
    public void e(View paramView, float paramFloat) {}
    
    public void e(View paramView, int paramInt)
    {
      ViewCompatBase.b(paramView, paramInt);
    }
    
    public float f(View paramView)
    {
      return 1.0F;
    }
    
    public void f(View paramView, float paramFloat) {}
    
    public void f(View paramView, int paramInt)
    {
      ViewCompatBase.a(paramView, paramInt);
    }
    
    public int g(View paramView)
    {
      return 0;
    }
    
    public int h(View paramView)
    {
      return 0;
    }
    
    public ViewParent i(View paramView)
    {
      return paramView.getParent();
    }
    
    public boolean j(View paramView)
    {
      boolean bool2 = false;
      paramView = paramView.getBackground();
      boolean bool1 = bool2;
      if (paramView != null)
      {
        bool1 = bool2;
        if (paramView.getOpacity() == -1) {
          bool1 = true;
        }
      }
      return bool1;
    }
    
    public int k(View paramView)
    {
      return paramView.getMeasuredWidth();
    }
    
    public int l(View paramView)
    {
      return 0;
    }
    
    public int m(View paramView)
    {
      return paramView.getPaddingLeft();
    }
    
    public int n(View paramView)
    {
      return paramView.getPaddingRight();
    }
    
    public boolean o(View paramView)
    {
      return true;
    }
    
    public float p(View paramView)
    {
      return 0.0F;
    }
    
    public float q(View paramView)
    {
      return 0.0F;
    }
    
    public float r(View paramView)
    {
      return 0.0F;
    }
    
    public int s(View paramView)
    {
      return ViewCompatBase.d(paramView);
    }
    
    public int t(View paramView)
    {
      return ViewCompatBase.e(paramView);
    }
    
    public ViewPropertyAnimatorCompat u(View paramView)
    {
      return new ViewPropertyAnimatorCompat(paramView);
    }
    
    public int v(View paramView)
    {
      return 0;
    }
    
    public void w(View paramView) {}
    
    public float x(View paramView)
    {
      return 0.0F;
    }
    
    public float y(View paramView)
    {
      return 0.0F;
    }
    
    public boolean z(View paramView)
    {
      return false;
    }
  }
  
  static class EclairMr1ViewCompatImpl
    extends ViewCompat.BaseViewCompatImpl
  {
    public void a(ViewGroup paramViewGroup, boolean paramBoolean)
    {
      ViewCompatEclairMr1.a(paramViewGroup, paramBoolean);
    }
    
    public boolean j(View paramView)
    {
      return ViewCompatEclairMr1.a(paramView);
    }
  }
  
  static class GBViewCompatImpl
    extends ViewCompat.EclairMr1ViewCompatImpl
  {
    public int a(View paramView)
    {
      return ViewCompatGingerbread.a(paramView);
    }
  }
  
  static class HCViewCompatImpl
    extends ViewCompat.GBViewCompatImpl
  {
    public void A(View paramView)
    {
      ViewCompatHC.h(paramView);
    }
    
    public int a(int paramInt1, int paramInt2)
    {
      return ViewCompatHC.a(paramInt1, paramInt2);
    }
    
    public int a(int paramInt1, int paramInt2, int paramInt3)
    {
      return ViewCompatHC.a(paramInt1, paramInt2, paramInt3);
    }
    
    long a()
    {
      return ViewCompatHC.a();
    }
    
    public void a(View paramView, float paramFloat)
    {
      ViewCompatHC.a(paramView, paramFloat);
    }
    
    public void a(View paramView, int paramInt, Paint paramPaint)
    {
      ViewCompatHC.a(paramView, paramInt, paramPaint);
    }
    
    public void a(View paramView, Paint paramPaint)
    {
      a(paramView, g(paramView), paramPaint);
      paramView.invalidate();
    }
    
    public void b(View paramView, float paramFloat)
    {
      ViewCompatHC.b(paramView, paramFloat);
    }
    
    public void b(View paramView, boolean paramBoolean)
    {
      ViewCompatHC.a(paramView, paramBoolean);
    }
    
    public void c(View paramView, float paramFloat)
    {
      ViewCompatHC.c(paramView, paramFloat);
    }
    
    public void c(View paramView, boolean paramBoolean)
    {
      ViewCompatHC.b(paramView, paramBoolean);
    }
    
    public void d(View paramView, float paramFloat)
    {
      ViewCompatHC.d(paramView, paramFloat);
    }
    
    public void e(View paramView, float paramFloat)
    {
      ViewCompatHC.e(paramView, paramFloat);
    }
    
    public void e(View paramView, int paramInt)
    {
      ViewCompatHC.b(paramView, paramInt);
    }
    
    public float f(View paramView)
    {
      return ViewCompatHC.a(paramView);
    }
    
    public void f(View paramView, int paramInt)
    {
      ViewCompatHC.a(paramView, paramInt);
    }
    
    public int g(View paramView)
    {
      return ViewCompatHC.b(paramView);
    }
    
    public int k(View paramView)
    {
      return ViewCompatHC.c(paramView);
    }
    
    public int l(View paramView)
    {
      return ViewCompatHC.d(paramView);
    }
    
    public float p(View paramView)
    {
      return ViewCompatHC.e(paramView);
    }
    
    public float q(View paramView)
    {
      return ViewCompatHC.f(paramView);
    }
    
    public float r(View paramView)
    {
      return ViewCompatHC.g(paramView);
    }
  }
  
  static class ICSMr1ViewCompatImpl
    extends ViewCompat.ICSViewCompatImpl
  {
    public boolean J(View paramView)
    {
      return ViewCompatICSMr1.a(paramView);
    }
  }
  
  static class ICSViewCompatImpl
    extends ViewCompat.HCViewCompatImpl
  {
    static Field b;
    static boolean c = false;
    
    public void a(View paramView, @Nullable AccessibilityDelegateCompat paramAccessibilityDelegateCompat)
    {
      if (paramAccessibilityDelegateCompat == null) {}
      for (paramAccessibilityDelegateCompat = null;; paramAccessibilityDelegateCompat = paramAccessibilityDelegateCompat.a())
      {
        ViewCompatICS.a(paramView, paramAccessibilityDelegateCompat);
        return;
      }
    }
    
    public void a(View paramView, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat)
    {
      ViewCompatICS.b(paramView, paramAccessibilityNodeInfoCompat.a());
    }
    
    public void a(View paramView, AccessibilityEvent paramAccessibilityEvent)
    {
      ViewCompatICS.a(paramView, paramAccessibilityEvent);
    }
    
    public void a(View paramView, boolean paramBoolean)
    {
      ViewCompatICS.a(paramView, paramBoolean);
    }
    
    public boolean a(View paramView, int paramInt)
    {
      return ViewCompatICS.a(paramView, paramInt);
    }
    
    /* Error */
    public boolean b(View paramView)
    {
      // Byte code:
      //   0: iconst_1
      //   1: istore_2
      //   2: getstatic 15	android/support/v4/view/ViewCompat$ICSViewCompatImpl:c	Z
      //   5: ifeq +5 -> 10
      //   8: iconst_0
      //   9: ireturn
      //   10: getstatic 53	android/support/v4/view/ViewCompat$ICSViewCompatImpl:b	Ljava/lang/reflect/Field;
      //   13: ifnonnull +20 -> 33
      //   16: ldc 55
      //   18: ldc 57
      //   20: invokevirtual 63	java/lang/Class:getDeclaredField	(Ljava/lang/String;)Ljava/lang/reflect/Field;
      //   23: putstatic 53	android/support/v4/view/ViewCompat$ICSViewCompatImpl:b	Ljava/lang/reflect/Field;
      //   26: getstatic 53	android/support/v4/view/ViewCompat$ICSViewCompatImpl:b	Ljava/lang/reflect/Field;
      //   29: iconst_1
      //   30: invokevirtual 69	java/lang/reflect/Field:setAccessible	(Z)V
      //   33: getstatic 53	android/support/v4/view/ViewCompat$ICSViewCompatImpl:b	Ljava/lang/reflect/Field;
      //   36: aload_1
      //   37: invokevirtual 73	java/lang/reflect/Field:get	(Ljava/lang/Object;)Ljava/lang/Object;
      //   40: astore_1
      //   41: aload_1
      //   42: ifnull +12 -> 54
      //   45: iload_2
      //   46: ireturn
      //   47: astore_1
      //   48: iconst_1
      //   49: putstatic 15	android/support/v4/view/ViewCompat$ICSViewCompatImpl:c	Z
      //   52: iconst_0
      //   53: ireturn
      //   54: iconst_0
      //   55: istore_2
      //   56: goto -11 -> 45
      //   59: astore_1
      //   60: iconst_1
      //   61: putstatic 15	android/support/v4/view/ViewCompat$ICSViewCompatImpl:c	Z
      //   64: iconst_0
      //   65: ireturn
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	66	0	this	ICSViewCompatImpl
      //   0	66	1	paramView	View
      //   1	55	2	bool	boolean
      // Exception table:
      //   from	to	target	type
      //   16	33	47	java/lang/Throwable
      //   33	41	59	java/lang/Throwable
    }
    
    public boolean b(View paramView, int paramInt)
    {
      return ViewCompatICS.b(paramView, paramInt);
    }
    
    public ViewPropertyAnimatorCompat u(View paramView)
    {
      if (this.a == null) {
        this.a = new WeakHashMap();
      }
      ViewPropertyAnimatorCompat localViewPropertyAnimatorCompat2 = (ViewPropertyAnimatorCompat)this.a.get(paramView);
      ViewPropertyAnimatorCompat localViewPropertyAnimatorCompat1 = localViewPropertyAnimatorCompat2;
      if (localViewPropertyAnimatorCompat2 == null)
      {
        localViewPropertyAnimatorCompat1 = new ViewPropertyAnimatorCompat(paramView);
        this.a.put(paramView, localViewPropertyAnimatorCompat1);
      }
      return localViewPropertyAnimatorCompat1;
    }
  }
  
  static class JBViewCompatImpl
    extends ViewCompat.ICSMr1ViewCompatImpl
  {
    public void a(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      ViewCompatJB.a(paramView, paramInt1, paramInt2, paramInt3, paramInt4);
    }
    
    public void a(View paramView, Runnable paramRunnable)
    {
      ViewCompatJB.a(paramView, paramRunnable);
    }
    
    public void a(View paramView, Runnable paramRunnable, long paramLong)
    {
      ViewCompatJB.a(paramView, paramRunnable, paramLong);
    }
    
    public boolean a(View paramView, int paramInt, Bundle paramBundle)
    {
      return ViewCompatJB.a(paramView, paramInt, paramBundle);
    }
    
    public void c(View paramView, int paramInt)
    {
      int i = paramInt;
      if (paramInt == 4) {
        i = 2;
      }
      ViewCompatJB.a(paramView, i);
    }
    
    public boolean c(View paramView)
    {
      return ViewCompatJB.a(paramView);
    }
    
    public void d(View paramView)
    {
      ViewCompatJB.b(paramView);
    }
    
    public int e(View paramView)
    {
      return ViewCompatJB.c(paramView);
    }
    
    public ViewParent i(View paramView)
    {
      return ViewCompatJB.d(paramView);
    }
    
    public boolean o(View paramView)
    {
      return ViewCompatJB.i(paramView);
    }
    
    public int s(View paramView)
    {
      return ViewCompatJB.e(paramView);
    }
    
    public int t(View paramView)
    {
      return ViewCompatJB.f(paramView);
    }
    
    public void w(View paramView)
    {
      ViewCompatJB.g(paramView);
    }
    
    public boolean z(View paramView)
    {
      return ViewCompatJB.h(paramView);
    }
  }
  
  static class JbMr1ViewCompatImpl
    extends ViewCompat.JBViewCompatImpl
  {
    public boolean B(View paramView)
    {
      return ViewCompatJellybeanMr1.e(paramView);
    }
    
    public void a(View paramView, Paint paramPaint)
    {
      ViewCompatJellybeanMr1.a(paramView, paramPaint);
    }
    
    public void b(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      ViewCompatJellybeanMr1.a(paramView, paramInt1, paramInt2, paramInt3, paramInt4);
    }
    
    public int h(View paramView)
    {
      return ViewCompatJellybeanMr1.a(paramView);
    }
    
    public int m(View paramView)
    {
      return ViewCompatJellybeanMr1.b(paramView);
    }
    
    public int n(View paramView)
    {
      return ViewCompatJellybeanMr1.c(paramView);
    }
    
    public int v(View paramView)
    {
      return ViewCompatJellybeanMr1.d(paramView);
    }
  }
  
  static class JbMr2ViewCompatImpl
    extends ViewCompat.JbMr1ViewCompatImpl
  {}
  
  static class KitKatViewCompatImpl
    extends ViewCompat.JbMr2ViewCompatImpl
  {
    public boolean G(View paramView)
    {
      return ViewCompatKitKat.a(paramView);
    }
    
    public boolean I(View paramView)
    {
      return ViewCompatKitKat.b(paramView);
    }
    
    public void c(View paramView, int paramInt)
    {
      ViewCompatJB.a(paramView, paramInt);
    }
    
    public void d(View paramView, int paramInt)
    {
      ViewCompatKitKat.a(paramView, paramInt);
    }
  }
  
  static class LollipopViewCompatImpl
    extends ViewCompat.KitKatViewCompatImpl
  {
    public boolean C(View paramView)
    {
      return ViewCompatLollipop.f(paramView);
    }
    
    public ColorStateList D(View paramView)
    {
      return ViewCompatLollipop.d(paramView);
    }
    
    public PorterDuff.Mode E(View paramView)
    {
      return ViewCompatLollipop.e(paramView);
    }
    
    public void F(View paramView)
    {
      ViewCompatLollipop.g(paramView);
    }
    
    public float H(View paramView)
    {
      return ViewCompatLollipop.h(paramView);
    }
    
    public WindowInsetsCompat a(View paramView, WindowInsetsCompat paramWindowInsetsCompat)
    {
      return ViewCompatLollipop.a(paramView, paramWindowInsetsCompat);
    }
    
    public void a(View paramView, ColorStateList paramColorStateList)
    {
      ViewCompatLollipop.a(paramView, paramColorStateList);
    }
    
    public void a(View paramView, PorterDuff.Mode paramMode)
    {
      ViewCompatLollipop.a(paramView, paramMode);
    }
    
    public void a(View paramView, OnApplyWindowInsetsListener paramOnApplyWindowInsetsListener)
    {
      ViewCompatLollipop.a(paramView, paramOnApplyWindowInsetsListener);
    }
    
    public WindowInsetsCompat b(View paramView, WindowInsetsCompat paramWindowInsetsCompat)
    {
      return ViewCompatLollipop.b(paramView, paramWindowInsetsCompat);
    }
    
    public void e(View paramView, int paramInt)
    {
      ViewCompatLollipop.b(paramView, paramInt);
    }
    
    public void f(View paramView, float paramFloat)
    {
      ViewCompatLollipop.a(paramView, paramFloat);
    }
    
    public void f(View paramView, int paramInt)
    {
      ViewCompatLollipop.a(paramView, paramInt);
    }
    
    public void w(View paramView)
    {
      ViewCompatLollipop.a(paramView);
    }
    
    public float x(View paramView)
    {
      return ViewCompatLollipop.b(paramView);
    }
    
    public float y(View paramView)
    {
      return ViewCompatLollipop.c(paramView);
    }
  }
  
  static class MarshmallowViewCompatImpl
    extends ViewCompat.LollipopViewCompatImpl
  {
    public void a(View paramView, int paramInt1, int paramInt2)
    {
      ViewCompatMarshmallow.a(paramView, paramInt1, paramInt2);
    }
    
    public void e(View paramView, int paramInt)
    {
      ViewCompatMarshmallow.b(paramView, paramInt);
    }
    
    public void f(View paramView, int paramInt)
    {
      ViewCompatMarshmallow.a(paramView, paramInt);
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ScrollIndicators {}
  
  static abstract interface ViewCompatImpl
  {
    public abstract void A(View paramView);
    
    public abstract boolean B(View paramView);
    
    public abstract boolean C(View paramView);
    
    public abstract ColorStateList D(View paramView);
    
    public abstract PorterDuff.Mode E(View paramView);
    
    public abstract void F(View paramView);
    
    public abstract boolean G(View paramView);
    
    public abstract float H(View paramView);
    
    public abstract boolean I(View paramView);
    
    public abstract boolean J(View paramView);
    
    public abstract int a(int paramInt1, int paramInt2);
    
    public abstract int a(int paramInt1, int paramInt2, int paramInt3);
    
    public abstract int a(View paramView);
    
    public abstract WindowInsetsCompat a(View paramView, WindowInsetsCompat paramWindowInsetsCompat);
    
    public abstract void a(View paramView, float paramFloat);
    
    public abstract void a(View paramView, int paramInt1, int paramInt2);
    
    public abstract void a(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
    
    public abstract void a(View paramView, int paramInt, Paint paramPaint);
    
    public abstract void a(View paramView, ColorStateList paramColorStateList);
    
    public abstract void a(View paramView, Paint paramPaint);
    
    public abstract void a(View paramView, PorterDuff.Mode paramMode);
    
    public abstract void a(View paramView, @Nullable AccessibilityDelegateCompat paramAccessibilityDelegateCompat);
    
    public abstract void a(View paramView, OnApplyWindowInsetsListener paramOnApplyWindowInsetsListener);
    
    public abstract void a(View paramView, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat);
    
    public abstract void a(View paramView, AccessibilityEvent paramAccessibilityEvent);
    
    public abstract void a(View paramView, Runnable paramRunnable);
    
    public abstract void a(View paramView, Runnable paramRunnable, long paramLong);
    
    public abstract void a(View paramView, boolean paramBoolean);
    
    public abstract void a(ViewGroup paramViewGroup, boolean paramBoolean);
    
    public abstract boolean a(View paramView, int paramInt);
    
    public abstract boolean a(View paramView, int paramInt, Bundle paramBundle);
    
    public abstract WindowInsetsCompat b(View paramView, WindowInsetsCompat paramWindowInsetsCompat);
    
    public abstract void b(View paramView, float paramFloat);
    
    public abstract void b(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
    
    public abstract void b(View paramView, boolean paramBoolean);
    
    public abstract boolean b(View paramView);
    
    public abstract boolean b(View paramView, int paramInt);
    
    public abstract void c(View paramView, float paramFloat);
    
    public abstract void c(View paramView, int paramInt);
    
    public abstract void c(View paramView, boolean paramBoolean);
    
    public abstract boolean c(View paramView);
    
    public abstract void d(View paramView);
    
    public abstract void d(View paramView, float paramFloat);
    
    public abstract void d(View paramView, int paramInt);
    
    public abstract int e(View paramView);
    
    public abstract void e(View paramView, float paramFloat);
    
    public abstract void e(View paramView, int paramInt);
    
    public abstract float f(View paramView);
    
    public abstract void f(View paramView, float paramFloat);
    
    public abstract void f(View paramView, int paramInt);
    
    public abstract int g(View paramView);
    
    public abstract int h(View paramView);
    
    public abstract ViewParent i(View paramView);
    
    public abstract boolean j(View paramView);
    
    public abstract int k(View paramView);
    
    public abstract int l(View paramView);
    
    public abstract int m(View paramView);
    
    public abstract int n(View paramView);
    
    public abstract boolean o(View paramView);
    
    public abstract float p(View paramView);
    
    public abstract float q(View paramView);
    
    public abstract float r(View paramView);
    
    public abstract int s(View paramView);
    
    public abstract int t(View paramView);
    
    public abstract ViewPropertyAnimatorCompat u(View paramView);
    
    public abstract int v(View paramView);
    
    public abstract void w(View paramView);
    
    public abstract float x(View paramView);
    
    public abstract boolean z(View paramView);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/view/ViewCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */