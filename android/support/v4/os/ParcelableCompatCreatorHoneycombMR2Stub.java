package android.support.v4.os;

import android.os.Parcelable.Creator;

class ParcelableCompatCreatorHoneycombMR2Stub
{
  static <T> Parcelable.Creator<T> a(ParcelableCompatCreatorCallbacks<T> paramParcelableCompatCreatorCallbacks)
  {
    return new ParcelableCompatCreatorHoneycombMR2(paramParcelableCompatCreatorCallbacks);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/os/ParcelableCompatCreatorHoneycombMR2Stub.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */