package android.support.v4.os;

import android.os.Build.VERSION;

public final class TraceCompat
{
  public static void a()
  {
    if (Build.VERSION.SDK_INT >= 18) {
      TraceJellybeanMR2.a();
    }
  }
  
  public static void a(String paramString)
  {
    if (Build.VERSION.SDK_INT >= 18) {
      TraceJellybeanMR2.a(paramString);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/os/TraceCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */