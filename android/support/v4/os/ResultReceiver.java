package android.support.v4.os;

import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public class ResultReceiver
  implements Parcelable
{
  public static final Parcelable.Creator<ResultReceiver> CREATOR = new Parcelable.Creator()
  {
    public ResultReceiver a(Parcel paramAnonymousParcel)
    {
      return new ResultReceiver(paramAnonymousParcel);
    }
    
    public ResultReceiver[] a(int paramAnonymousInt)
    {
      return new ResultReceiver[paramAnonymousInt];
    }
  };
  final boolean c;
  final Handler d;
  IResultReceiver e;
  
  public ResultReceiver(Handler paramHandler)
  {
    this.c = true;
    this.d = paramHandler;
  }
  
  ResultReceiver(Parcel paramParcel)
  {
    this.c = false;
    this.d = null;
    this.e = IResultReceiver.Stub.a(paramParcel.readStrongBinder());
  }
  
  protected void a(int paramInt, Bundle paramBundle) {}
  
  public void b(int paramInt, Bundle paramBundle)
  {
    if (this.c) {
      if (this.d != null) {
        this.d.post(new MyRunnable(paramInt, paramBundle));
      }
    }
    while (this.e == null)
    {
      return;
      a(paramInt, paramBundle);
      return;
    }
    try
    {
      this.e.a(paramInt, paramBundle);
      return;
    }
    catch (RemoteException paramBundle) {}
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    try
    {
      if (this.e == null) {
        this.e = new MyResultReceiver();
      }
      paramParcel.writeStrongBinder(this.e.asBinder());
      return;
    }
    finally {}
  }
  
  class MyResultReceiver
    extends IResultReceiver.Stub
  {
    MyResultReceiver() {}
    
    public void a(int paramInt, Bundle paramBundle)
    {
      if (ResultReceiver.this.d != null)
      {
        ResultReceiver.this.d.post(new ResultReceiver.MyRunnable(ResultReceiver.this, paramInt, paramBundle));
        return;
      }
      ResultReceiver.this.a(paramInt, paramBundle);
    }
  }
  
  class MyRunnable
    implements Runnable
  {
    final int a;
    final Bundle b;
    
    MyRunnable(int paramInt, Bundle paramBundle)
    {
      this.a = paramInt;
      this.b = paramBundle;
    }
    
    public void run()
    {
      ResultReceiver.this.a(this.a, this.b);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/os/ResultReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */