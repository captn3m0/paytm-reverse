package android.support.v4.accessibilityservice;

import android.os.Build.VERSION;

public final class AccessibilityServiceInfoCompat
{
  private static final AccessibilityServiceInfoVersionImpl a = new AccessibilityServiceInfoStubImpl();
  
  static
  {
    if (Build.VERSION.SDK_INT >= 18)
    {
      a = new AccessibilityServiceInfoJellyBeanMr2();
      return;
    }
    if (Build.VERSION.SDK_INT >= 14)
    {
      a = new AccessibilityServiceInfoIcsImpl();
      return;
    }
  }
  
  static class AccessibilityServiceInfoIcsImpl
    extends AccessibilityServiceInfoCompat.AccessibilityServiceInfoStubImpl
  {}
  
  static class AccessibilityServiceInfoJellyBeanMr2
    extends AccessibilityServiceInfoCompat.AccessibilityServiceInfoIcsImpl
  {}
  
  static class AccessibilityServiceInfoStubImpl
    implements AccessibilityServiceInfoCompat.AccessibilityServiceInfoVersionImpl
  {}
  
  static abstract interface AccessibilityServiceInfoVersionImpl {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/accessibilityservice/AccessibilityServiceInfoCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */