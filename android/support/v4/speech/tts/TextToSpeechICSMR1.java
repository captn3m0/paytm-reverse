package android.support.v4.speech.tts;

import android.speech.tts.TextToSpeech.OnUtteranceCompletedListener;
import android.speech.tts.UtteranceProgressListener;

class TextToSpeechICSMR1
{
  static abstract interface UtteranceProgressListenerICSMR1
  {
    public abstract void a(String paramString);
    
    public abstract void b(String paramString);
    
    public abstract void c(String paramString);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/speech/tts/TextToSpeechICSMR1.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */