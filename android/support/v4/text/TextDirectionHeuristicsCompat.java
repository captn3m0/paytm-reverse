package android.support.v4.text;

import java.util.Locale;

public final class TextDirectionHeuristicsCompat
{
  public static final TextDirectionHeuristicCompat a = new TextDirectionHeuristicInternal(null, false, null);
  public static final TextDirectionHeuristicCompat b = new TextDirectionHeuristicInternal(null, true, null);
  public static final TextDirectionHeuristicCompat c = new TextDirectionHeuristicInternal(FirstStrong.a, false, null);
  public static final TextDirectionHeuristicCompat d = new TextDirectionHeuristicInternal(FirstStrong.a, true, null);
  public static final TextDirectionHeuristicCompat e = new TextDirectionHeuristicInternal(AnyStrong.a, false, null);
  public static final TextDirectionHeuristicCompat f = TextDirectionHeuristicLocale.a;
  
  private static int c(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return 2;
    case 0: 
      return 1;
    }
    return 0;
  }
  
  private static int d(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return 2;
    case 0: 
    case 14: 
    case 15: 
      return 1;
    }
    return 0;
  }
  
  private static class AnyStrong
    implements TextDirectionHeuristicsCompat.TextDirectionAlgorithm
  {
    public static final AnyStrong a = new AnyStrong(true);
    public static final AnyStrong b = new AnyStrong(false);
    private final boolean c;
    
    private AnyStrong(boolean paramBoolean)
    {
      this.c = paramBoolean;
    }
    
    public int a(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
      int k = 1;
      int i = 0;
      int j = paramInt1;
      for (;;)
      {
        if (j < paramInt1 + paramInt2) {
          switch (TextDirectionHeuristicsCompat.b(Character.getDirectionality(paramCharSequence.charAt(j))))
          {
          default: 
            j += 1;
            break;
          case 0: 
            if (this.c) {
              i = 0;
            }
            break;
          }
        }
      }
      do
      {
        do
        {
          return i;
          i = 1;
          break;
          i = k;
        } while (!this.c);
        i = 1;
        break;
        if (i == 0) {
          break label115;
        }
        i = k;
      } while (this.c);
      return 0;
      label115:
      return 2;
    }
  }
  
  private static class FirstStrong
    implements TextDirectionHeuristicsCompat.TextDirectionAlgorithm
  {
    public static final FirstStrong a = new FirstStrong();
    
    public int a(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
      int j = 2;
      int i = paramInt1;
      while ((i < paramInt1 + paramInt2) && (j == 2))
      {
        j = TextDirectionHeuristicsCompat.a(Character.getDirectionality(paramCharSequence.charAt(i)));
        i += 1;
      }
      return j;
    }
  }
  
  private static abstract interface TextDirectionAlgorithm
  {
    public abstract int a(CharSequence paramCharSequence, int paramInt1, int paramInt2);
  }
  
  private static abstract class TextDirectionHeuristicImpl
    implements TextDirectionHeuristicCompat
  {
    private final TextDirectionHeuristicsCompat.TextDirectionAlgorithm a;
    
    public TextDirectionHeuristicImpl(TextDirectionHeuristicsCompat.TextDirectionAlgorithm paramTextDirectionAlgorithm)
    {
      this.a = paramTextDirectionAlgorithm;
    }
    
    private boolean b(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
      switch (this.a.a(paramCharSequence, paramInt1, paramInt2))
      {
      default: 
        return a();
      case 0: 
        return true;
      }
      return false;
    }
    
    protected abstract boolean a();
    
    public boolean a(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
      if ((paramCharSequence == null) || (paramInt1 < 0) || (paramInt2 < 0) || (paramCharSequence.length() - paramInt2 < paramInt1)) {
        throw new IllegalArgumentException();
      }
      if (this.a == null) {
        return a();
      }
      return b(paramCharSequence, paramInt1, paramInt2);
    }
  }
  
  private static class TextDirectionHeuristicInternal
    extends TextDirectionHeuristicsCompat.TextDirectionHeuristicImpl
  {
    private final boolean a;
    
    private TextDirectionHeuristicInternal(TextDirectionHeuristicsCompat.TextDirectionAlgorithm paramTextDirectionAlgorithm, boolean paramBoolean)
    {
      super();
      this.a = paramBoolean;
    }
    
    protected boolean a()
    {
      return this.a;
    }
  }
  
  private static class TextDirectionHeuristicLocale
    extends TextDirectionHeuristicsCompat.TextDirectionHeuristicImpl
  {
    public static final TextDirectionHeuristicLocale a = new TextDirectionHeuristicLocale();
    
    public TextDirectionHeuristicLocale()
    {
      super();
    }
    
    protected boolean a()
    {
      return TextUtilsCompat.a(Locale.getDefault()) == 1;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/text/TextDirectionHeuristicsCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */