package android.support.v4.text;

import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Locale;

public final class TextUtilsCompat
{
  public static final Locale a;
  private static final TextUtilsCompatImpl b;
  private static String c;
  private static String d;
  
  static
  {
    if (Build.VERSION.SDK_INT >= 17) {}
    for (b = new TextUtilsCompatJellybeanMr1Impl(null);; b = new TextUtilsCompatImpl(null))
    {
      a = new Locale("", "");
      c = "Arab";
      d = "Hebr";
      return;
    }
  }
  
  public static int a(@Nullable Locale paramLocale)
  {
    return b.a(paramLocale);
  }
  
  private static class TextUtilsCompatImpl
  {
    private static int b(@NonNull Locale paramLocale)
    {
      switch (Character.getDirectionality(paramLocale.getDisplayName(paramLocale).charAt(0)))
      {
      default: 
        return 0;
      }
      return 1;
    }
    
    public int a(@Nullable Locale paramLocale)
    {
      if ((paramLocale != null) && (!paramLocale.equals(TextUtilsCompat.a)))
      {
        String str = ICUCompat.a(paramLocale);
        if (str == null) {
          return b(paramLocale);
        }
        if ((str.equalsIgnoreCase(TextUtilsCompat.a())) || (str.equalsIgnoreCase(TextUtilsCompat.b()))) {
          return 1;
        }
      }
      return 0;
    }
  }
  
  private static class TextUtilsCompatJellybeanMr1Impl
    extends TextUtilsCompat.TextUtilsCompatImpl
  {
    private TextUtilsCompatJellybeanMr1Impl()
    {
      super();
    }
    
    public int a(@Nullable Locale paramLocale)
    {
      return TextUtilsCompatJellybeanMr1.a(paramLocale);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/text/TextUtilsCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */