package android.support.v4.text;

import java.util.Locale;

public final class BidiFormatter
{
  private static TextDirectionHeuristicCompat a = TextDirectionHeuristicsCompat.c;
  private static final String b = Character.toString('‎');
  private static final String c = Character.toString('‏');
  private static final BidiFormatter d = new BidiFormatter(false, 2, a);
  private static final BidiFormatter e = new BidiFormatter(true, 2, a);
  private final boolean f;
  private final int g;
  private final TextDirectionHeuristicCompat h;
  
  private BidiFormatter(boolean paramBoolean, int paramInt, TextDirectionHeuristicCompat paramTextDirectionHeuristicCompat)
  {
    this.f = paramBoolean;
    this.g = paramInt;
    this.h = paramTextDirectionHeuristicCompat;
  }
  
  private static boolean b(Locale paramLocale)
  {
    return TextUtilsCompat.a(paramLocale) == 1;
  }
  
  public static final class Builder
  {
    private boolean a;
    private int b;
    private TextDirectionHeuristicCompat c;
    
    public Builder()
    {
      a(BidiFormatter.a(Locale.getDefault()));
    }
    
    private void a(boolean paramBoolean)
    {
      this.a = paramBoolean;
      this.c = BidiFormatter.a();
      this.b = 2;
    }
  }
  
  private static class DirectionalityEstimator
  {
    private static final byte[] a = new byte['܀'];
    
    static
    {
      int i = 0;
      while (i < 1792)
      {
        a[i] = Character.getDirectionality(i);
        i += 1;
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/text/BidiFormatter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */