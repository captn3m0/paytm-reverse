package android.support.v4.content;

import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;

public final class SharedPreferencesCompat
{
  public static final class EditorCompat
  {
    private static EditorCompat a;
    private final Helper b;
    
    private EditorCompat()
    {
      if (Build.VERSION.SDK_INT >= 9)
      {
        this.b = new EditorHelperApi9Impl(null);
        return;
      }
      this.b = new EditorHelperBaseImpl(null);
    }
    
    public static EditorCompat a()
    {
      if (a == null) {
        a = new EditorCompat();
      }
      return a;
    }
    
    public void a(@NonNull SharedPreferences.Editor paramEditor)
    {
      this.b.a(paramEditor);
    }
    
    private static class EditorHelperApi9Impl
      implements SharedPreferencesCompat.EditorCompat.Helper
    {
      public void a(@NonNull SharedPreferences.Editor paramEditor)
      {
        EditorCompatGingerbread.a(paramEditor);
      }
    }
    
    private static class EditorHelperBaseImpl
      implements SharedPreferencesCompat.EditorCompat.Helper
    {
      public void a(@NonNull SharedPreferences.Editor paramEditor)
      {
        paramEditor.commit();
      }
    }
    
    private static abstract interface Helper
    {
      public abstract void a(@NonNull SharedPreferences.Editor paramEditor);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/content/SharedPreferencesCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */