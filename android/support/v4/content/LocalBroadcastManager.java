package android.support.v4.content;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public final class LocalBroadcastManager
{
  private static final Object f = new Object();
  private static LocalBroadcastManager g;
  private final Context a;
  private final HashMap<BroadcastReceiver, ArrayList<IntentFilter>> b = new HashMap();
  private final HashMap<String, ArrayList<ReceiverRecord>> c = new HashMap();
  private final ArrayList<BroadcastRecord> d = new ArrayList();
  private final Handler e;
  
  private LocalBroadcastManager(Context paramContext)
  {
    this.a = paramContext;
    this.e = new Handler(paramContext.getMainLooper())
    {
      public void handleMessage(Message paramAnonymousMessage)
      {
        switch (paramAnonymousMessage.what)
        {
        default: 
          super.handleMessage(paramAnonymousMessage);
          return;
        }
        LocalBroadcastManager.a(LocalBroadcastManager.this);
      }
    };
  }
  
  public static LocalBroadcastManager a(Context paramContext)
  {
    synchronized (f)
    {
      if (g == null) {
        g = new LocalBroadcastManager(paramContext.getApplicationContext());
      }
      paramContext = g;
      return paramContext;
    }
  }
  
  private void a()
  {
    for (;;)
    {
      int i;
      synchronized (this.b)
      {
        i = this.d.size();
        if (i <= 0) {
          return;
        }
        BroadcastRecord[] arrayOfBroadcastRecord = new BroadcastRecord[i];
        this.d.toArray(arrayOfBroadcastRecord);
        this.d.clear();
        i = 0;
        if (i >= arrayOfBroadcastRecord.length) {
          continue;
        }
        ??? = arrayOfBroadcastRecord[i];
        int j = 0;
        if (j < ???.b.size())
        {
          ((ReceiverRecord)???.b.get(j)).b.onReceive(this.a, ???.a);
          j += 1;
        }
      }
      i += 1;
    }
  }
  
  public void a(BroadcastReceiver paramBroadcastReceiver)
  {
    for (;;)
    {
      int k;
      int m;
      synchronized (this.b)
      {
        ArrayList localArrayList1 = (ArrayList)this.b.remove(paramBroadcastReceiver);
        if (localArrayList1 == null)
        {
          return;
          if (j < localArrayList1.size())
          {
            IntentFilter localIntentFilter = (IntentFilter)localArrayList1.get(j);
            k = 0;
            if (k >= localIntentFilter.countActions()) {
              break label190;
            }
            String str = localIntentFilter.getAction(k);
            ArrayList localArrayList2 = (ArrayList)this.c.get(str);
            if (localArrayList2 == null) {
              break label181;
            }
            i = 0;
            if (i < localArrayList2.size())
            {
              m = i;
              if (((ReceiverRecord)localArrayList2.get(i)).b != paramBroadcastReceiver) {
                break label173;
              }
              localArrayList2.remove(i);
              m = i - 1;
              break label173;
            }
            if (localArrayList2.size() > 0) {
              break label181;
            }
            this.c.remove(str);
            break label181;
          }
          return;
        }
      }
      int j = 0;
      continue;
      label173:
      int i = m + 1;
      continue;
      label181:
      k += 1;
      continue;
      label190:
      j += 1;
    }
  }
  
  public void a(BroadcastReceiver paramBroadcastReceiver, IntentFilter paramIntentFilter)
  {
    synchronized (this.b)
    {
      ReceiverRecord localReceiverRecord = new ReceiverRecord(paramIntentFilter, paramBroadcastReceiver);
      Object localObject2 = (ArrayList)this.b.get(paramBroadcastReceiver);
      Object localObject1 = localObject2;
      if (localObject2 == null)
      {
        localObject1 = new ArrayList(1);
        this.b.put(paramBroadcastReceiver, localObject1);
      }
      ((ArrayList)localObject1).add(paramIntentFilter);
      int i = 0;
      while (i < paramIntentFilter.countActions())
      {
        localObject2 = paramIntentFilter.getAction(i);
        localObject1 = (ArrayList)this.c.get(localObject2);
        paramBroadcastReceiver = (BroadcastReceiver)localObject1;
        if (localObject1 == null)
        {
          paramBroadcastReceiver = new ArrayList(1);
          this.c.put(localObject2, paramBroadcastReceiver);
        }
        paramBroadcastReceiver.add(localReceiverRecord);
        i += 1;
      }
      return;
    }
  }
  
  public boolean a(Intent paramIntent)
  {
    int i;
    label161:
    int j;
    Object localObject2;
    int k;
    synchronized (this.b)
    {
      String str1 = paramIntent.getAction();
      String str2 = paramIntent.resolveTypeIfNeeded(this.a.getContentResolver());
      Uri localUri = paramIntent.getData();
      String str3 = paramIntent.getScheme();
      Set localSet = paramIntent.getCategories();
      if ((paramIntent.getFlags() & 0x8) == 0) {
        break label521;
      }
      i = 1;
      if (i != 0) {
        Log.v("LocalBroadcastManager", "Resolving type " + str2 + " scheme " + str3 + " of intent " + paramIntent);
      }
      ArrayList localArrayList = (ArrayList)this.c.get(paramIntent.getAction());
      if (localArrayList == null) {
        break label497;
      }
      if (i == 0) {
        break label502;
      }
      Log.v("LocalBroadcastManager", "Action list: " + localArrayList);
      break label502;
      if (j >= localArrayList.size()) {
        break label554;
      }
      ReceiverRecord localReceiverRecord = (ReceiverRecord)localArrayList.get(j);
      if (i != 0) {
        Log.v("LocalBroadcastManager", "Matching against filter " + localReceiverRecord.a);
      }
      if (localReceiverRecord.c)
      {
        localObject1 = localObject2;
        if (i != 0)
        {
          Log.v("LocalBroadcastManager", "  Filter's target already added");
          localObject1 = localObject2;
        }
      }
      else
      {
        k = localReceiverRecord.a.match(str1, str2, str3, localUri, localSet, "LocalBroadcastManager");
        if (k >= 0)
        {
          if (i != 0) {
            Log.v("LocalBroadcastManager", "  Filter matched!  match=0x" + Integer.toHexString(k));
          }
          localObject1 = localObject2;
          if (localObject2 == null) {
            localObject1 = new ArrayList();
          }
          ((ArrayList)localObject1).add(localReceiverRecord);
          localReceiverRecord.c = true;
        }
      }
    }
    Object localObject1 = localObject2;
    if (i != 0) {
      switch (k)
      {
      default: 
        localObject1 = "unknown reason";
        label392:
        Log.v("LocalBroadcastManager", "  Filter did not match: " + (String)localObject1);
        localObject1 = localObject2;
        break;
      }
    }
    for (;;)
    {
      if (i < ((ArrayList)localObject2).size())
      {
        ((ReceiverRecord)((ArrayList)localObject2).get(i)).c = false;
        i += 1;
      }
      else
      {
        this.d.add(new BroadcastRecord(paramIntent, (ArrayList)localObject2));
        if (!this.e.hasMessages(1)) {
          this.e.sendEmptyMessage(1);
        }
        return true;
        label497:
        label502:
        label521:
        label554:
        do
        {
          return false;
          localObject2 = null;
          j = 0;
          break label161;
          j += 1;
          localObject2 = localObject1;
          break label161;
          i = 0;
          break;
          localObject1 = "action";
          break label392;
          localObject1 = "category";
          break label392;
          localObject1 = "data";
          break label392;
          localObject1 = "type";
          break label392;
        } while (localObject2 == null);
        i = 0;
      }
    }
  }
  
  private static class BroadcastRecord
  {
    final Intent a;
    final ArrayList<LocalBroadcastManager.ReceiverRecord> b;
    
    BroadcastRecord(Intent paramIntent, ArrayList<LocalBroadcastManager.ReceiverRecord> paramArrayList)
    {
      this.a = paramIntent;
      this.b = paramArrayList;
    }
  }
  
  private static class ReceiverRecord
  {
    final IntentFilter a;
    final BroadcastReceiver b;
    boolean c;
    
    ReceiverRecord(IntentFilter paramIntentFilter, BroadcastReceiver paramBroadcastReceiver)
    {
      this.a = paramIntentFilter;
      this.b = paramBroadcastReceiver;
    }
    
    public String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(128);
      localStringBuilder.append("Receiver{");
      localStringBuilder.append(this.b);
      localStringBuilder.append(" filter=");
      localStringBuilder.append(this.a);
      localStringBuilder.append("}");
      return localStringBuilder.toString();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/content/LocalBroadcastManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */