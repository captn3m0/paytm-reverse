package android.support.v4.content;

import android.content.Context;
import android.content.res.ColorStateList;

class ContextCompatApi23
{
  public static ColorStateList a(Context paramContext, int paramInt)
  {
    return paramContext.getColorStateList(paramInt);
  }
  
  public static int b(Context paramContext, int paramInt)
  {
    return paramContext.getColor(paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/content/ContextCompatApi23.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */