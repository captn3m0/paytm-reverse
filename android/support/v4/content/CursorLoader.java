package android.support.v4.content;

import android.database.Cursor;
import android.net.Uri;
import android.support.v4.os.CancellationSignal;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Arrays;

public class CursorLoader
  extends AsyncTaskLoader<Cursor>
{
  final Loader<Cursor>.ForceLoadContentObserver f;
  Uri g;
  String[] h;
  String i;
  String[] j;
  String k;
  Cursor l;
  CancellationSignal m;
  
  public void a(Cursor paramCursor)
  {
    if (isReset()) {
      if (paramCursor != null) {
        paramCursor.close();
      }
    }
    Cursor localCursor;
    do
    {
      return;
      localCursor = this.l;
      this.l = paramCursor;
      if (isStarted()) {
        super.deliverResult(paramCursor);
      }
    } while ((localCursor == null) || (localCursor == paramCursor) || (localCursor.isClosed()));
    localCursor.close();
  }
  
  public void b(Cursor paramCursor)
  {
    if ((paramCursor != null) && (!paramCursor.isClosed())) {
      paramCursor.close();
    }
  }
  
  public void d()
  {
    super.d();
    try
    {
      if (this.m != null) {
        this.m.cancel();
      }
      return;
    }
    finally {}
  }
  
  public void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
  {
    super.dump(paramString, paramFileDescriptor, paramPrintWriter, paramArrayOfString);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mUri=");
    paramPrintWriter.println(this.g);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mProjection=");
    paramPrintWriter.println(Arrays.toString(this.h));
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mSelection=");
    paramPrintWriter.println(this.i);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mSelectionArgs=");
    paramPrintWriter.println(Arrays.toString(this.j));
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mSortOrder=");
    paramPrintWriter.println(this.k);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mCursor=");
    paramPrintWriter.println(this.l);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mContentChanged=");
    paramPrintWriter.println(this.mContentChanged);
  }
  
  /* Error */
  public Cursor f()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual 121	android/support/v4/content/CursorLoader:e	()Z
    //   6: ifeq +16 -> 22
    //   9: new 123	android/support/v4/os/OperationCanceledException
    //   12: dup
    //   13: invokespecial 126	android/support/v4/os/OperationCanceledException:<init>	()V
    //   16: athrow
    //   17: astore_1
    //   18: aload_0
    //   19: monitorexit
    //   20: aload_1
    //   21: athrow
    //   22: aload_0
    //   23: new 59	android/support/v4/os/CancellationSignal
    //   26: dup
    //   27: invokespecial 127	android/support/v4/os/CancellationSignal:<init>	()V
    //   30: putfield 57	android/support/v4/content/CursorLoader:m	Landroid/support/v4/os/CancellationSignal;
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_0
    //   36: invokevirtual 131	android/support/v4/content/CursorLoader:getContext	()Landroid/content/Context;
    //   39: invokevirtual 137	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   42: aload_0
    //   43: getfield 78	android/support/v4/content/CursorLoader:g	Landroid/net/Uri;
    //   46: aload_0
    //   47: getfield 85	android/support/v4/content/CursorLoader:h	[Ljava/lang/String;
    //   50: aload_0
    //   51: getfield 97	android/support/v4/content/CursorLoader:i	Ljava/lang/String;
    //   54: aload_0
    //   55: getfield 101	android/support/v4/content/CursorLoader:j	[Ljava/lang/String;
    //   58: aload_0
    //   59: getfield 105	android/support/v4/content/CursorLoader:k	Ljava/lang/String;
    //   62: aload_0
    //   63: getfield 57	android/support/v4/content/CursorLoader:m	Landroid/support/v4/os/CancellationSignal;
    //   66: invokestatic 142	android/support/v4/content/ContentResolverCompat:a	(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/os/CancellationSignal;)Landroid/database/Cursor;
    //   69: astore_1
    //   70: aload_1
    //   71: ifnull +20 -> 91
    //   74: aload_1
    //   75: invokeinterface 146 1 0
    //   80: pop
    //   81: aload_1
    //   82: aload_0
    //   83: getfield 148	android/support/v4/content/CursorLoader:f	Landroid/support/v4/content/Loader$ForceLoadContentObserver;
    //   86: invokeinterface 152 2 0
    //   91: aload_0
    //   92: monitorenter
    //   93: aload_0
    //   94: aconst_null
    //   95: putfield 57	android/support/v4/content/CursorLoader:m	Landroid/support/v4/os/CancellationSignal;
    //   98: aload_0
    //   99: monitorexit
    //   100: aload_1
    //   101: areturn
    //   102: astore_2
    //   103: aload_1
    //   104: invokeinterface 32 1 0
    //   109: aload_2
    //   110: athrow
    //   111: astore_1
    //   112: aload_0
    //   113: monitorenter
    //   114: aload_0
    //   115: aconst_null
    //   116: putfield 57	android/support/v4/content/CursorLoader:m	Landroid/support/v4/os/CancellationSignal;
    //   119: aload_0
    //   120: monitorexit
    //   121: aload_1
    //   122: athrow
    //   123: astore_1
    //   124: aload_0
    //   125: monitorexit
    //   126: aload_1
    //   127: athrow
    //   128: astore_1
    //   129: aload_0
    //   130: monitorexit
    //   131: aload_1
    //   132: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	133	0	this	CursorLoader
    //   17	4	1	localObject1	Object
    //   69	35	1	localCursor	Cursor
    //   111	11	1	localObject2	Object
    //   123	4	1	localObject3	Object
    //   128	4	1	localObject4	Object
    //   102	8	2	localRuntimeException	RuntimeException
    // Exception table:
    //   from	to	target	type
    //   2	17	17	finally
    //   18	20	17	finally
    //   22	35	17	finally
    //   74	91	102	java/lang/RuntimeException
    //   35	70	111	finally
    //   74	91	111	finally
    //   103	111	111	finally
    //   93	100	123	finally
    //   124	126	123	finally
    //   114	121	128	finally
    //   129	131	128	finally
  }
  
  protected void onReset()
  {
    super.onReset();
    onStopLoading();
    if ((this.l != null) && (!this.l.isClosed())) {
      this.l.close();
    }
    this.l = null;
  }
  
  protected void onStartLoading()
  {
    if (this.l != null) {
      a(this.l);
    }
    if ((takeContentChanged()) || (this.l == null)) {
      forceLoad();
    }
  }
  
  protected void onStopLoading()
  {
    cancelLoad();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/content/CursorLoader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */