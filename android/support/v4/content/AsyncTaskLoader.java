package android.support.v4.content;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.os.OperationCanceledException;
import android.support.v4.util.TimeUtils;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;

public abstract class AsyncTaskLoader<D>
  extends Loader<D>
{
  volatile AsyncTaskLoader<D>.LoadTask a;
  volatile AsyncTaskLoader<D>.LoadTask b;
  long c;
  long d = -10000L;
  Handler e;
  private final Executor f;
  
  public AsyncTaskLoader(Context paramContext)
  {
    this(paramContext, ModernAsyncTask.c);
  }
  
  private AsyncTaskLoader(Context paramContext, Executor paramExecutor)
  {
    super(paramContext);
    this.f = paramExecutor;
  }
  
  void a()
  {
    if ((this.b == null) && (this.a != null))
    {
      if (this.a.a)
      {
        this.a.a = false;
        this.e.removeCallbacks(this.a);
      }
      if ((this.c > 0L) && (SystemClock.uptimeMillis() < this.d + this.c))
      {
        this.a.a = true;
        this.e.postAtTime(this.a, this.d + this.c);
      }
    }
    else
    {
      return;
    }
    this.a.a(this.f, (Void[])null);
  }
  
  void a(AsyncTaskLoader<D>.LoadTask paramAsyncTaskLoader, D paramD)
  {
    a(paramD);
    if (this.b == paramAsyncTaskLoader)
    {
      rollbackContentChanged();
      this.d = SystemClock.uptimeMillis();
      this.b = null;
      deliverCancellation();
      a();
    }
  }
  
  public void a(D paramD) {}
  
  public abstract D b();
  
  void b(AsyncTaskLoader<D>.LoadTask paramAsyncTaskLoader, D paramD)
  {
    if (this.a != paramAsyncTaskLoader)
    {
      a(paramAsyncTaskLoader, paramD);
      return;
    }
    if (isAbandoned())
    {
      a(paramD);
      return;
    }
    commitContentChanged();
    this.d = SystemClock.uptimeMillis();
    this.a = null;
    deliverResult(paramD);
  }
  
  protected D c()
  {
    return (D)b();
  }
  
  public void d() {}
  
  public void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
  {
    super.dump(paramString, paramFileDescriptor, paramPrintWriter, paramArrayOfString);
    if (this.a != null)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mTask=");
      paramPrintWriter.print(this.a);
      paramPrintWriter.print(" waiting=");
      paramPrintWriter.println(this.a.a);
    }
    if (this.b != null)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mCancellingTask=");
      paramPrintWriter.print(this.b);
      paramPrintWriter.print(" waiting=");
      paramPrintWriter.println(this.b.a);
    }
    if (this.c != 0L)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mUpdateThrottle=");
      TimeUtils.a(this.c, paramPrintWriter);
      paramPrintWriter.print(" mLastLoadCompleteTime=");
      TimeUtils.a(this.d, SystemClock.uptimeMillis(), paramPrintWriter);
      paramPrintWriter.println();
    }
  }
  
  public boolean e()
  {
    return this.b != null;
  }
  
  protected boolean onCancelLoad()
  {
    if (this.a != null)
    {
      if (this.b != null)
      {
        if (this.a.a)
        {
          this.a.a = false;
          this.e.removeCallbacks(this.a);
        }
        this.a = null;
      }
    }
    else {
      return false;
    }
    if (this.a.a)
    {
      this.a.a = false;
      this.e.removeCallbacks(this.a);
      this.a = null;
      return false;
    }
    boolean bool = this.a.cancel(false);
    if (bool)
    {
      this.b = this.a;
      d();
    }
    this.a = null;
    return bool;
  }
  
  protected void onForceLoad()
  {
    super.onForceLoad();
    cancelLoad();
    this.a = new LoadTask();
    a();
  }
  
  final class LoadTask
    extends ModernAsyncTask<Void, Void, D>
    implements Runnable
  {
    boolean a;
    private final CountDownLatch d = new CountDownLatch(1);
    
    LoadTask() {}
    
    protected D a(Void... paramVarArgs)
    {
      try
      {
        paramVarArgs = AsyncTaskLoader.this.c();
        return paramVarArgs;
      }
      catch (OperationCanceledException paramVarArgs)
      {
        if (!c()) {
          throw paramVarArgs;
        }
      }
      return null;
    }
    
    protected void a(D paramD)
    {
      try
      {
        AsyncTaskLoader.this.b(this, paramD);
        return;
      }
      finally
      {
        this.d.countDown();
      }
    }
    
    protected void b(D paramD)
    {
      try
      {
        AsyncTaskLoader.this.a(this, paramD);
        return;
      }
      finally
      {
        this.d.countDown();
      }
    }
    
    public void run()
    {
      this.a = false;
      AsyncTaskLoader.this.a();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/content/AsyncTaskLoader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */