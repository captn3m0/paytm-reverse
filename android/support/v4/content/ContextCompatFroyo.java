package android.support.v4.content;

import android.content.Context;
import java.io.File;

class ContextCompatFroyo
{
  public static File a(Context paramContext)
  {
    return paramContext.getExternalCacheDir();
  }
  
  public static File a(Context paramContext, String paramString)
  {
    return paramContext.getExternalFilesDir(paramString);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/content/ContextCompatFroyo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */