package android.support.v4.animation;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.view.View;
import android.view.ViewPropertyAnimator;

class HoneycombMr1AnimatorCompatProvider
  implements AnimatorProvider
{
  private TimeInterpolator a;
  
  public ValueAnimatorCompat a()
  {
    return new HoneycombValueAnimatorCompat(ValueAnimator.ofFloat(new float[] { 0.0F, 1.0F }));
  }
  
  public void a(View paramView)
  {
    if (this.a == null) {
      this.a = new ValueAnimator().getInterpolator();
    }
    paramView.animate().setInterpolator(this.a);
  }
  
  static class AnimatorListenerCompatWrapper
    implements Animator.AnimatorListener
  {
    final AnimatorListenerCompat a;
    final ValueAnimatorCompat b;
    
    public AnimatorListenerCompatWrapper(AnimatorListenerCompat paramAnimatorListenerCompat, ValueAnimatorCompat paramValueAnimatorCompat)
    {
      this.a = paramAnimatorListenerCompat;
      this.b = paramValueAnimatorCompat;
    }
    
    public void onAnimationCancel(Animator paramAnimator)
    {
      this.a.c(this.b);
    }
    
    public void onAnimationEnd(Animator paramAnimator)
    {
      this.a.b(this.b);
    }
    
    public void onAnimationRepeat(Animator paramAnimator)
    {
      this.a.d(this.b);
    }
    
    public void onAnimationStart(Animator paramAnimator)
    {
      this.a.a(this.b);
    }
  }
  
  static class HoneycombValueAnimatorCompat
    implements ValueAnimatorCompat
  {
    final Animator a;
    
    public HoneycombValueAnimatorCompat(Animator paramAnimator)
    {
      this.a = paramAnimator;
    }
    
    public void a()
    {
      this.a.start();
    }
    
    public void a(long paramLong)
    {
      this.a.setDuration(paramLong);
    }
    
    public void a(AnimatorListenerCompat paramAnimatorListenerCompat)
    {
      this.a.addListener(new HoneycombMr1AnimatorCompatProvider.AnimatorListenerCompatWrapper(paramAnimatorListenerCompat, this));
    }
    
    public void a(final AnimatorUpdateListenerCompat paramAnimatorUpdateListenerCompat)
    {
      if ((this.a instanceof ValueAnimator)) {
        ((ValueAnimator)this.a).addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
        {
          public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
          {
            paramAnimatorUpdateListenerCompat.a(HoneycombMr1AnimatorCompatProvider.HoneycombValueAnimatorCompat.this);
          }
        });
      }
    }
    
    public void a(View paramView)
    {
      this.a.setTarget(paramView);
    }
    
    public float b()
    {
      return ((ValueAnimator)this.a).getAnimatedFraction();
    }
    
    public void cancel()
    {
      this.a.cancel();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/animation/HoneycombMr1AnimatorCompatProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */