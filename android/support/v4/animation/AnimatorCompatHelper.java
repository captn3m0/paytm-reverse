package android.support.v4.animation;

import android.os.Build.VERSION;
import android.view.View;

public final class AnimatorCompatHelper
{
  private static final AnimatorProvider a = new DonutAnimatorCompatProvider();
  
  static
  {
    if (Build.VERSION.SDK_INT >= 12)
    {
      a = new HoneycombMr1AnimatorCompatProvider();
      return;
    }
  }
  
  public static ValueAnimatorCompat a()
  {
    return a.a();
  }
  
  public static void a(View paramView)
  {
    a.a(paramView);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/animation/AnimatorCompatHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */