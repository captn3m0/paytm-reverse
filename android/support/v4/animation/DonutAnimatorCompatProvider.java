package android.support.v4.animation;

import android.view.View;
import java.util.ArrayList;
import java.util.List;

class DonutAnimatorCompatProvider
  implements AnimatorProvider
{
  public ValueAnimatorCompat a()
  {
    return new DonutFloatValueAnimator();
  }
  
  public void a(View paramView) {}
  
  private static class DonutFloatValueAnimator
    implements ValueAnimatorCompat
  {
    List<AnimatorListenerCompat> a = new ArrayList();
    List<AnimatorUpdateListenerCompat> b = new ArrayList();
    View c;
    private long d;
    private long e = 200L;
    private float f = 0.0F;
    private boolean g = false;
    private boolean h = false;
    private Runnable i = new Runnable()
    {
      public void run()
      {
        float f = (float)(DonutAnimatorCompatProvider.DonutFloatValueAnimator.a(DonutAnimatorCompatProvider.DonutFloatValueAnimator.this) - DonutAnimatorCompatProvider.DonutFloatValueAnimator.b(DonutAnimatorCompatProvider.DonutFloatValueAnimator.this)) * 1.0F / (float)DonutAnimatorCompatProvider.DonutFloatValueAnimator.c(DonutAnimatorCompatProvider.DonutFloatValueAnimator.this);
        if ((f > 1.0F) || (DonutAnimatorCompatProvider.DonutFloatValueAnimator.this.c.getParent() == null)) {
          f = 1.0F;
        }
        DonutAnimatorCompatProvider.DonutFloatValueAnimator.a(DonutAnimatorCompatProvider.DonutFloatValueAnimator.this, f);
        DonutAnimatorCompatProvider.DonutFloatValueAnimator.d(DonutAnimatorCompatProvider.DonutFloatValueAnimator.this);
        if (DonutAnimatorCompatProvider.DonutFloatValueAnimator.e(DonutAnimatorCompatProvider.DonutFloatValueAnimator.this) >= 1.0F)
        {
          DonutAnimatorCompatProvider.DonutFloatValueAnimator.f(DonutAnimatorCompatProvider.DonutFloatValueAnimator.this);
          return;
        }
        DonutAnimatorCompatProvider.DonutFloatValueAnimator.this.c.postDelayed(DonutAnimatorCompatProvider.DonutFloatValueAnimator.g(DonutAnimatorCompatProvider.DonutFloatValueAnimator.this), 16L);
      }
    };
    
    private void c()
    {
      int j = this.b.size() - 1;
      while (j >= 0)
      {
        ((AnimatorUpdateListenerCompat)this.b.get(j)).a(this);
        j -= 1;
      }
    }
    
    private long d()
    {
      return this.c.getDrawingTime();
    }
    
    private void e()
    {
      int j = this.a.size() - 1;
      while (j >= 0)
      {
        ((AnimatorListenerCompat)this.a.get(j)).a(this);
        j -= 1;
      }
    }
    
    private void f()
    {
      int j = this.a.size() - 1;
      while (j >= 0)
      {
        ((AnimatorListenerCompat)this.a.get(j)).b(this);
        j -= 1;
      }
    }
    
    private void g()
    {
      int j = this.a.size() - 1;
      while (j >= 0)
      {
        ((AnimatorListenerCompat)this.a.get(j)).c(this);
        j -= 1;
      }
    }
    
    public void a()
    {
      if (this.g) {
        return;
      }
      this.g = true;
      e();
      this.f = 0.0F;
      this.d = d();
      this.c.postDelayed(this.i, 16L);
    }
    
    public void a(long paramLong)
    {
      if (!this.g) {
        this.e = paramLong;
      }
    }
    
    public void a(AnimatorListenerCompat paramAnimatorListenerCompat)
    {
      this.a.add(paramAnimatorListenerCompat);
    }
    
    public void a(AnimatorUpdateListenerCompat paramAnimatorUpdateListenerCompat)
    {
      this.b.add(paramAnimatorUpdateListenerCompat);
    }
    
    public void a(View paramView)
    {
      this.c = paramView;
    }
    
    public float b()
    {
      return this.f;
    }
    
    public void cancel()
    {
      if (this.h) {
        return;
      }
      this.h = true;
      if (this.g) {
        g();
      }
      f();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/animation/DonutAnimatorCompatProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */