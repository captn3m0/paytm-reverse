package android.support.v4.animation;

import android.view.View;

public abstract interface ValueAnimatorCompat
{
  public abstract void a();
  
  public abstract void a(long paramLong);
  
  public abstract void a(AnimatorListenerCompat paramAnimatorListenerCompat);
  
  public abstract void a(AnimatorUpdateListenerCompat paramAnimatorUpdateListenerCompat);
  
  public abstract void a(View paramView);
  
  public abstract float b();
  
  public abstract void cancel();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/animation/ValueAnimatorCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */