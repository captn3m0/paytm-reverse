package android.support.v4.media;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Log;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class MediaMetadataCompat
  implements Parcelable
{
  public static final Parcelable.Creator<MediaMetadataCompat> CREATOR = new Parcelable.Creator()
  {
    public MediaMetadataCompat a(Parcel paramAnonymousParcel)
    {
      return new MediaMetadataCompat(paramAnonymousParcel, null);
    }
    
    public MediaMetadataCompat[] a(int paramAnonymousInt)
    {
      return new MediaMetadataCompat[paramAnonymousInt];
    }
  };
  private static final ArrayMap<String, Integer> a = new ArrayMap();
  private static final String[] b;
  private static final String[] c;
  private static final String[] d;
  private final Bundle e;
  private Object f;
  private MediaDescriptionCompat g;
  
  static
  {
    a.put("android.media.metadata.TITLE", Integer.valueOf(1));
    a.put("android.media.metadata.ARTIST", Integer.valueOf(1));
    a.put("android.media.metadata.DURATION", Integer.valueOf(0));
    a.put("android.media.metadata.ALBUM", Integer.valueOf(1));
    a.put("android.media.metadata.AUTHOR", Integer.valueOf(1));
    a.put("android.media.metadata.WRITER", Integer.valueOf(1));
    a.put("android.media.metadata.COMPOSER", Integer.valueOf(1));
    a.put("android.media.metadata.COMPILATION", Integer.valueOf(1));
    a.put("android.media.metadata.DATE", Integer.valueOf(1));
    a.put("android.media.metadata.YEAR", Integer.valueOf(0));
    a.put("android.media.metadata.GENRE", Integer.valueOf(1));
    a.put("android.media.metadata.TRACK_NUMBER", Integer.valueOf(0));
    a.put("android.media.metadata.NUM_TRACKS", Integer.valueOf(0));
    a.put("android.media.metadata.DISC_NUMBER", Integer.valueOf(0));
    a.put("android.media.metadata.ALBUM_ARTIST", Integer.valueOf(1));
    a.put("android.media.metadata.ART", Integer.valueOf(2));
    a.put("android.media.metadata.ART_URI", Integer.valueOf(1));
    a.put("android.media.metadata.ALBUM_ART", Integer.valueOf(2));
    a.put("android.media.metadata.ALBUM_ART_URI", Integer.valueOf(1));
    a.put("android.media.metadata.USER_RATING", Integer.valueOf(3));
    a.put("android.media.metadata.RATING", Integer.valueOf(3));
    a.put("android.media.metadata.DISPLAY_TITLE", Integer.valueOf(1));
    a.put("android.media.metadata.DISPLAY_SUBTITLE", Integer.valueOf(1));
    a.put("android.media.metadata.DISPLAY_DESCRIPTION", Integer.valueOf(1));
    a.put("android.media.metadata.DISPLAY_ICON", Integer.valueOf(2));
    a.put("android.media.metadata.DISPLAY_ICON_URI", Integer.valueOf(1));
    a.put("android.media.metadata.MEDIA_ID", Integer.valueOf(1));
    b = new String[] { "android.media.metadata.TITLE", "android.media.metadata.ARTIST", "android.media.metadata.ALBUM", "android.media.metadata.ALBUM_ARTIST", "android.media.metadata.WRITER", "android.media.metadata.AUTHOR", "android.media.metadata.COMPOSER" };
    c = new String[] { "android.media.metadata.DISPLAY_ICON", "android.media.metadata.ART", "android.media.metadata.ALBUM_ART" };
    d = new String[] { "android.media.metadata.DISPLAY_ICON_URI", "android.media.metadata.ART_URI", "android.media.metadata.ALBUM_ART_URI" };
  }
  
  private MediaMetadataCompat(Parcel paramParcel)
  {
    this.e = paramParcel.readBundle();
  }
  
  public static MediaMetadataCompat a(Object paramObject)
  {
    if ((paramObject == null) || (Build.VERSION.SDK_INT < 21)) {
      return null;
    }
    Parcel localParcel = Parcel.obtain();
    MediaMetadataCompatApi21.a(paramObject, localParcel, 0);
    localParcel.setDataPosition(0);
    MediaMetadataCompat localMediaMetadataCompat = (MediaMetadataCompat)CREATOR.createFromParcel(localParcel);
    localParcel.recycle();
    localMediaMetadataCompat.f = paramObject;
    return localMediaMetadataCompat;
  }
  
  public MediaDescriptionCompat a()
  {
    if (this.g != null) {
      return this.g;
    }
    String str = c("android.media.metadata.MEDIA_ID");
    CharSequence[] arrayOfCharSequence = new CharSequence[3];
    Object localObject2 = null;
    MediaDescriptionCompat.Builder localBuilder = null;
    Object localObject1 = b("android.media.metadata.DISPLAY_TITLE");
    int i;
    if (!TextUtils.isEmpty((CharSequence)localObject1))
    {
      arrayOfCharSequence[0] = localObject1;
      arrayOfCharSequence[1] = b("android.media.metadata.DISPLAY_SUBTITLE");
      arrayOfCharSequence[2] = b("android.media.metadata.DISPLAY_DESCRIPTION");
      i = 0;
      label76:
      localObject1 = localObject2;
      if (i < c.length)
      {
        localObject1 = e(c[i]);
        if (localObject1 == null) {
          break label280;
        }
      }
      i = 0;
    }
    for (;;)
    {
      localObject2 = localBuilder;
      if (i < d.length)
      {
        localObject2 = c(d[i]);
        if (!TextUtils.isEmpty((CharSequence)localObject2)) {
          localObject2 = Uri.parse((String)localObject2);
        }
      }
      else
      {
        localBuilder = new MediaDescriptionCompat.Builder();
        localBuilder.a(str);
        localBuilder.a(arrayOfCharSequence[0]);
        localBuilder.b(arrayOfCharSequence[1]);
        localBuilder.c(arrayOfCharSequence[2]);
        localBuilder.a((Bitmap)localObject1);
        localBuilder.a((Uri)localObject2);
        this.g = localBuilder.a();
        return this.g;
        int j = 0;
        i = 0;
        while ((j < arrayOfCharSequence.length) && (i < b.length))
        {
          localObject1 = b(b[i]);
          int k = j;
          if (!TextUtils.isEmpty((CharSequence)localObject1))
          {
            arrayOfCharSequence[j] = localObject1;
            k = j + 1;
          }
          i += 1;
          j = k;
        }
        break;
        label280:
        i += 1;
        break label76;
      }
      i += 1;
    }
  }
  
  public boolean a(String paramString)
  {
    return this.e.containsKey(paramString);
  }
  
  public CharSequence b(String paramString)
  {
    return this.e.getCharSequence(paramString);
  }
  
  public String c(String paramString)
  {
    paramString = this.e.getCharSequence(paramString);
    if (paramString != null) {
      return paramString.toString();
    }
    return null;
  }
  
  public long d(String paramString)
  {
    return this.e.getLong(paramString, 0L);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Bitmap e(String paramString)
  {
    try
    {
      paramString = (Bitmap)this.e.getParcelable(paramString);
      return paramString;
    }
    catch (Exception paramString)
    {
      Log.w("MediaMetadata", "Failed to retrieve a key as Bitmap.", paramString);
    }
    return null;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeBundle(this.e);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface BitmapKey {}
  
  public static final class Builder
  {
    private final Bundle a = new Bundle();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface LongKey {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface RatingKey {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface TextKey {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/media/MediaMetadataCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */