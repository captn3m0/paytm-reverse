package android.support.v4.media.session;

import android.app.PendingIntent;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.RatingCompat;
import android.support.v4.media.VolumeProviderCompat;
import android.support.v4.media.VolumeProviderCompat.Callback;
import android.view.KeyEvent;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

public class MediaSessionCompat
{
  private final MediaSessionImpl a;
  
  public Token a()
  {
    return this.a.a();
  }
  
  public void a(int paramInt)
  {
    this.a.a(paramInt);
  }
  
  public void a(VolumeProviderCompat paramVolumeProviderCompat)
  {
    if (paramVolumeProviderCompat == null) {
      throw new IllegalArgumentException("volumeProvider may not be null!");
    }
    this.a.a(paramVolumeProviderCompat);
  }
  
  public static abstract class Callback
  {
    final Object a;
    
    public Callback()
    {
      if (Build.VERSION.SDK_INT >= 23)
      {
        this.a = MediaSessionCompatApi23.a(new StubApi23(null));
        return;
      }
      if (Build.VERSION.SDK_INT >= 21)
      {
        this.a = MediaSessionCompatApi21.a(new StubApi21(null));
        return;
      }
      this.a = null;
    }
    
    public void a() {}
    
    public void a(long paramLong) {}
    
    public void a(Uri paramUri, Bundle paramBundle) {}
    
    public void a(RatingCompat paramRatingCompat) {}
    
    public void a(String paramString, Bundle paramBundle) {}
    
    public void a(String paramString, Bundle paramBundle, ResultReceiver paramResultReceiver) {}
    
    public boolean a(Intent paramIntent)
    {
      return false;
    }
    
    public void b() {}
    
    public void b(long paramLong) {}
    
    public void b(String paramString, Bundle paramBundle) {}
    
    public void c() {}
    
    public void c(String paramString, Bundle paramBundle) {}
    
    public void d() {}
    
    public void e() {}
    
    public void f() {}
    
    public void g() {}
    
    private class StubApi21
      implements MediaSessionCompatApi21.Callback
    {
      private StubApi21() {}
      
      public void a()
      {
        MediaSessionCompat.Callback.this.a();
      }
      
      public void a(long paramLong)
      {
        MediaSessionCompat.Callback.this.a(paramLong);
      }
      
      public void a(Object paramObject)
      {
        MediaSessionCompat.Callback.this.a(RatingCompat.a(paramObject));
      }
      
      public void a(String paramString, Bundle paramBundle)
      {
        MediaSessionCompat.Callback.this.a(paramString, paramBundle);
      }
      
      public void a(String paramString, Bundle paramBundle, ResultReceiver paramResultReceiver)
      {
        MediaSessionCompat.Callback.this.a(paramString, paramBundle, paramResultReceiver);
      }
      
      public boolean a(Intent paramIntent)
      {
        return MediaSessionCompat.Callback.this.a(paramIntent);
      }
      
      public void b()
      {
        MediaSessionCompat.Callback.this.b();
      }
      
      public void b(long paramLong)
      {
        MediaSessionCompat.Callback.this.b(paramLong);
      }
      
      public void b(String paramString, Bundle paramBundle)
      {
        MediaSessionCompat.Callback.this.b(paramString, paramBundle);
      }
      
      public void c()
      {
        MediaSessionCompat.Callback.this.c();
      }
      
      public void c(String paramString, Bundle paramBundle)
      {
        if (paramString.equals("android.support.v4.media.session.action.PLAY_FROM_URI"))
        {
          paramString = (Uri)paramBundle.getParcelable("android.support.v4.media.session.action.ARGUMENT_URI");
          paramBundle = (Bundle)paramBundle.getParcelable("android.support.v4.media.session.action.ARGUMENT_EXTRAS");
          MediaSessionCompat.Callback.this.a(paramString, paramBundle);
          return;
        }
        MediaSessionCompat.Callback.this.c(paramString, paramBundle);
      }
      
      public void d()
      {
        MediaSessionCompat.Callback.this.d();
      }
      
      public void e()
      {
        MediaSessionCompat.Callback.this.e();
      }
      
      public void f()
      {
        MediaSessionCompat.Callback.this.f();
      }
      
      public void g()
      {
        MediaSessionCompat.Callback.this.g();
      }
    }
    
    private class StubApi23
      extends MediaSessionCompat.Callback.StubApi21
      implements MediaSessionCompatApi23.Callback
    {
      private StubApi23()
      {
        super(null);
      }
      
      public void a(Uri paramUri, Bundle paramBundle)
      {
        MediaSessionCompat.Callback.this.a(paramUri, paramBundle);
      }
    }
  }
  
  static abstract interface MediaSessionImpl
  {
    public abstract MediaSessionCompat.Token a();
    
    public abstract void a(int paramInt);
    
    public abstract void a(VolumeProviderCompat paramVolumeProviderCompat);
  }
  
  static class MediaSessionImplApi21
    implements MediaSessionCompat.MediaSessionImpl
  {
    private final Object a;
    private final MediaSessionCompat.Token b;
    
    public MediaSessionCompat.Token a()
    {
      return this.b;
    }
    
    public void a(int paramInt)
    {
      MediaSessionCompatApi21.a(this.a, paramInt);
    }
    
    public void a(VolumeProviderCompat paramVolumeProviderCompat)
    {
      MediaSessionCompatApi21.a(this.a, paramVolumeProviderCompat.d());
    }
  }
  
  static class MediaSessionImplBase
    implements MediaSessionCompat.MediaSessionImpl
  {
    private final MediaSessionCompat.Token a;
    private final String b;
    private final String c;
    private final AudioManager d;
    private final Object e;
    private final RemoteCallbackList<IMediaControllerCallback> f;
    private MessageHandler g;
    private boolean h;
    private volatile MediaSessionCompat.Callback i;
    private int j;
    private MediaMetadataCompat k;
    private PlaybackStateCompat l;
    private PendingIntent m;
    private List<MediaSessionCompat.QueueItem> n;
    private CharSequence o;
    private int p;
    private Bundle q;
    private int r;
    private int s;
    private VolumeProviderCompat t;
    private VolumeProviderCompat.Callback u;
    
    private void a(int paramInt1, int paramInt2)
    {
      if (this.r == 2)
      {
        if (this.t != null) {
          this.t.c(paramInt1);
        }
        return;
      }
      this.d.adjustStreamVolume(this.s, paramInt1, paramInt2);
    }
    
    private void a(int paramInt, Object paramObject)
    {
      a(paramInt, paramObject, null);
    }
    
    private void a(int paramInt, Object paramObject, Bundle paramBundle)
    {
      synchronized (this.e)
      {
        if (this.g != null) {
          this.g.a(paramInt, paramObject, paramBundle);
        }
        return;
      }
    }
    
    private void a(ParcelableVolumeInfo paramParcelableVolumeInfo)
    {
      int i1 = this.f.beginBroadcast() - 1;
      for (;;)
      {
        IMediaControllerCallback localIMediaControllerCallback;
        if (i1 >= 0) {
          localIMediaControllerCallback = (IMediaControllerCallback)this.f.getBroadcastItem(i1);
        }
        try
        {
          localIMediaControllerCallback.a(paramParcelableVolumeInfo);
          i1 -= 1;
          continue;
          this.f.finishBroadcast();
          return;
        }
        catch (RemoteException localRemoteException)
        {
          for (;;) {}
        }
      }
    }
    
    private PlaybackStateCompat b()
    {
      long l2 = -1L;
      for (;;)
      {
        synchronized (this.e)
        {
          PlaybackStateCompat localPlaybackStateCompat = this.l;
          l1 = l2;
          if (this.k != null)
          {
            l1 = l2;
            if (this.k.a("android.media.metadata.DURATION")) {
              l1 = this.k.d("android.media.metadata.DURATION");
            }
          }
          Object localObject2 = null;
          ??? = localObject2;
          if (localPlaybackStateCompat != null) {
            if ((localPlaybackStateCompat.a() != 3) && (localPlaybackStateCompat.a() != 4))
            {
              ??? = localObject2;
              if (localPlaybackStateCompat.a() != 5) {}
            }
            else
            {
              l2 = localPlaybackStateCompat.e();
              long l3 = SystemClock.elapsedRealtime();
              ??? = localObject2;
              if (l2 > 0L)
              {
                l2 = (localPlaybackStateCompat.c() * (float)(l3 - l2)) + localPlaybackStateCompat.b();
                if ((l1 < 0L) || (l2 <= l1)) {
                  break label203;
                }
                ??? = new PlaybackStateCompat.Builder(localPlaybackStateCompat);
                ((PlaybackStateCompat.Builder)???).a(localPlaybackStateCompat.a(), l1, localPlaybackStateCompat.c(), l3);
                ??? = ((PlaybackStateCompat.Builder)???).a();
              }
            }
          }
          if (??? != null) {
            break;
          }
          return localPlaybackStateCompat;
        }
        label203:
        long l1 = l2;
        if (l2 < 0L) {
          l1 = 0L;
        }
      }
      return (PlaybackStateCompat)???;
    }
    
    private void b(int paramInt)
    {
      a(paramInt, null);
    }
    
    private void b(int paramInt1, int paramInt2)
    {
      if (this.r == 2)
      {
        if (this.t != null) {
          this.t.b(paramInt1);
        }
        return;
      }
      this.d.setStreamVolume(this.s, paramInt1, paramInt2);
    }
    
    public MediaSessionCompat.Token a()
    {
      return this.a;
    }
    
    public void a(int paramInt)
    {
      if (this.t != null) {
        this.t.a(null);
      }
      this.r = 1;
      a(new ParcelableVolumeInfo(this.r, this.s, 2, this.d.getStreamMaxVolume(this.s), this.d.getStreamVolume(this.s)));
    }
    
    public void a(VolumeProviderCompat paramVolumeProviderCompat)
    {
      if (paramVolumeProviderCompat == null) {
        throw new IllegalArgumentException("volumeProvider may not be null");
      }
      if (this.t != null) {
        this.t.a(null);
      }
      this.r = 2;
      this.t = paramVolumeProviderCompat;
      a(new ParcelableVolumeInfo(this.r, this.s, this.t.b(), this.t.c(), this.t.a()));
      paramVolumeProviderCompat.a(this.u);
    }
    
    private static final class Command
    {
      public final String a;
      public final Bundle b;
      public final ResultReceiver c;
      
      public Command(String paramString, Bundle paramBundle, ResultReceiver paramResultReceiver)
      {
        this.a = paramString;
        this.b = paramBundle;
        this.c = paramResultReceiver;
      }
    }
    
    class MediaSessionStub
      extends IMediaSession.Stub
    {
      public void a(int paramInt1, int paramInt2, String paramString)
      {
        MediaSessionCompat.MediaSessionImplBase.a(this.a, paramInt1, paramInt2);
      }
      
      public void a(long paramLong)
      {
        MediaSessionCompat.MediaSessionImplBase.a(this.a, 4, Long.valueOf(paramLong));
      }
      
      public void a(Uri paramUri, Bundle paramBundle)
        throws RemoteException
      {
        MediaSessionCompat.MediaSessionImplBase.a(this.a, 18, paramUri, paramBundle);
      }
      
      public void a(RatingCompat paramRatingCompat)
        throws RemoteException
      {
        MediaSessionCompat.MediaSessionImplBase.a(this.a, 12, paramRatingCompat);
      }
      
      public void a(IMediaControllerCallback paramIMediaControllerCallback)
      {
        if (MediaSessionCompat.MediaSessionImplBase.e(this.a)) {}
        try
        {
          paramIMediaControllerCallback.a();
          return;
        }
        catch (Exception paramIMediaControllerCallback) {}
        MediaSessionCompat.MediaSessionImplBase.f(this.a).register(paramIMediaControllerCallback);
        return;
      }
      
      public void a(String paramString, Bundle paramBundle)
        throws RemoteException
      {
        MediaSessionCompat.MediaSessionImplBase.a(this.a, 2, paramString, paramBundle);
      }
      
      public void a(String paramString, Bundle paramBundle, MediaSessionCompat.ResultReceiverWrapper paramResultReceiverWrapper)
      {
        MediaSessionCompat.MediaSessionImplBase.a(this.a, 15, new MediaSessionCompat.MediaSessionImplBase.Command(paramString, paramBundle, MediaSessionCompat.ResultReceiverWrapper.a(paramResultReceiverWrapper)));
      }
      
      public boolean a()
      {
        return (MediaSessionCompat.MediaSessionImplBase.d(this.a) & 0x2) != 0;
      }
      
      public boolean a(KeyEvent paramKeyEvent)
      {
        if ((MediaSessionCompat.MediaSessionImplBase.d(this.a) & 0x1) != 0) {}
        for (boolean bool = true;; bool = false)
        {
          if (bool) {
            MediaSessionCompat.MediaSessionImplBase.a(this.a, 14, paramKeyEvent);
          }
          return bool;
        }
      }
      
      public String b()
      {
        return MediaSessionCompat.MediaSessionImplBase.g(this.a);
      }
      
      public void b(int paramInt1, int paramInt2, String paramString)
      {
        MediaSessionCompat.MediaSessionImplBase.b(this.a, paramInt1, paramInt2);
      }
      
      public void b(long paramLong)
        throws RemoteException
      {
        MediaSessionCompat.MediaSessionImplBase.a(this.a, 11, Long.valueOf(paramLong));
      }
      
      public void b(IMediaControllerCallback paramIMediaControllerCallback)
      {
        MediaSessionCompat.MediaSessionImplBase.f(this.a).unregister(paramIMediaControllerCallback);
      }
      
      public void b(String paramString, Bundle paramBundle)
        throws RemoteException
      {
        MediaSessionCompat.MediaSessionImplBase.a(this.a, 3, paramString, paramBundle);
      }
      
      public String c()
      {
        return MediaSessionCompat.MediaSessionImplBase.h(this.a);
      }
      
      public void c(String paramString, Bundle paramBundle)
        throws RemoteException
      {
        MediaSessionCompat.MediaSessionImplBase.a(this.a, 13, paramString, paramBundle);
      }
      
      public PendingIntent d()
      {
        synchronized (MediaSessionCompat.MediaSessionImplBase.i(this.a))
        {
          PendingIntent localPendingIntent = MediaSessionCompat.MediaSessionImplBase.j(this.a);
          return localPendingIntent;
        }
      }
      
      public long e()
      {
        synchronized (MediaSessionCompat.MediaSessionImplBase.i(this.a))
        {
          long l = MediaSessionCompat.MediaSessionImplBase.d(this.a);
          return l;
        }
      }
      
      public ParcelableVolumeInfo f()
      {
        synchronized (MediaSessionCompat.MediaSessionImplBase.i(this.a))
        {
          int m = MediaSessionCompat.MediaSessionImplBase.b(this.a);
          int n = MediaSessionCompat.MediaSessionImplBase.c(this.a);
          VolumeProviderCompat localVolumeProviderCompat = MediaSessionCompat.MediaSessionImplBase.a(this.a);
          if (m == 2)
          {
            i = localVolumeProviderCompat.b();
            j = localVolumeProviderCompat.c();
            k = localVolumeProviderCompat.a();
            return new ParcelableVolumeInfo(m, n, i, j, k);
          }
          int i = 2;
          int j = MediaSessionCompat.MediaSessionImplBase.k(this.a).getStreamMaxVolume(n);
          int k = MediaSessionCompat.MediaSessionImplBase.k(this.a).getStreamVolume(n);
        }
      }
      
      public void g()
        throws RemoteException
      {
        MediaSessionCompat.MediaSessionImplBase.a(this.a, 1);
      }
      
      public void h()
        throws RemoteException
      {
        MediaSessionCompat.MediaSessionImplBase.a(this.a, 5);
      }
      
      public void i()
        throws RemoteException
      {
        MediaSessionCompat.MediaSessionImplBase.a(this.a, 6);
      }
      
      public void j()
        throws RemoteException
      {
        MediaSessionCompat.MediaSessionImplBase.a(this.a, 7);
      }
      
      public void k()
        throws RemoteException
      {
        MediaSessionCompat.MediaSessionImplBase.a(this.a, 8);
      }
      
      public void l()
        throws RemoteException
      {
        MediaSessionCompat.MediaSessionImplBase.a(this.a, 9);
      }
      
      public void m()
        throws RemoteException
      {
        MediaSessionCompat.MediaSessionImplBase.a(this.a, 10);
      }
      
      public MediaMetadataCompat n()
      {
        return MediaSessionCompat.MediaSessionImplBase.l(this.a);
      }
      
      public PlaybackStateCompat o()
      {
        return MediaSessionCompat.MediaSessionImplBase.m(this.a);
      }
      
      public List<MediaSessionCompat.QueueItem> p()
      {
        synchronized (MediaSessionCompat.MediaSessionImplBase.i(this.a))
        {
          List localList = MediaSessionCompat.MediaSessionImplBase.n(this.a);
          return localList;
        }
      }
      
      public CharSequence q()
      {
        return MediaSessionCompat.MediaSessionImplBase.o(this.a);
      }
      
      public Bundle r()
      {
        synchronized (MediaSessionCompat.MediaSessionImplBase.i(this.a))
        {
          Bundle localBundle = MediaSessionCompat.MediaSessionImplBase.p(this.a);
          return localBundle;
        }
      }
      
      public int s()
      {
        return MediaSessionCompat.MediaSessionImplBase.q(this.a);
      }
    }
    
    private class MessageHandler
      extends Handler
    {
      private void a(KeyEvent paramKeyEvent, MediaSessionCompat.Callback paramCallback)
      {
        int k = 1;
        if ((paramKeyEvent == null) || (paramKeyEvent.getAction() != 0)) {}
        label28:
        int i;
        label143:
        int j;
        label157:
        label312:
        label318:
        label324:
        do
        {
          return;
          long l;
          if (MediaSessionCompat.MediaSessionImplBase.s(this.a) == null)
          {
            l = 0L;
            switch (paramKeyEvent.getKeyCode())
            {
            default: 
              return;
            case 79: 
            case 85: 
              if ((MediaSessionCompat.MediaSessionImplBase.s(this.a) != null) && (MediaSessionCompat.MediaSessionImplBase.s(this.a).a() == 3))
              {
                i = 1;
                if ((0x204 & l) == 0L) {
                  break label312;
                }
                j = 1;
                if ((0x202 & l) == 0L) {
                  break label318;
                }
              }
              break;
            }
          }
          for (;;)
          {
            if ((i == 0) || (k == 0)) {
              break label324;
            }
            paramCallback.b();
            return;
            l = MediaSessionCompat.MediaSessionImplBase.s(this.a).d();
            break label28;
            if ((0x4 & l) == 0L) {
              break;
            }
            paramCallback.a();
            return;
            if ((0x2 & l) == 0L) {
              break;
            }
            paramCallback.b();
            return;
            if ((0x20 & l) == 0L) {
              break;
            }
            paramCallback.c();
            return;
            if ((0x10 & l) == 0L) {
              break;
            }
            paramCallback.d();
            return;
            if ((1L & l) == 0L) {
              break;
            }
            paramCallback.g();
            return;
            if ((0x40 & l) == 0L) {
              break;
            }
            paramCallback.e();
            return;
            if ((0x8 & l) == 0L) {
              break;
            }
            paramCallback.f();
            return;
            i = 0;
            break label143;
            j = 0;
            break label157;
            k = 0;
          }
        } while ((i != 0) || (j == 0));
        paramCallback.a();
      }
      
      public void a(int paramInt, Object paramObject, Bundle paramBundle)
      {
        paramObject = obtainMessage(paramInt, paramObject);
        ((Message)paramObject).setData(paramBundle);
        ((Message)paramObject).sendToTarget();
      }
      
      public void handleMessage(Message paramMessage)
      {
        MediaSessionCompat.Callback localCallback = MediaSessionCompat.MediaSessionImplBase.r(this.a);
        if (localCallback == null) {}
        Intent localIntent;
        do
        {
          return;
          switch (paramMessage.what)
          {
          default: 
            return;
          case 1: 
            localCallback.a();
            return;
          case 2: 
            localCallback.a((String)paramMessage.obj, paramMessage.getData());
            return;
          case 3: 
            localCallback.b((String)paramMessage.obj, paramMessage.getData());
            return;
          case 18: 
            localCallback.a((Uri)paramMessage.obj, paramMessage.getData());
            return;
          case 4: 
            localCallback.a(((Long)paramMessage.obj).longValue());
            return;
          case 5: 
            localCallback.b();
            return;
          case 6: 
            localCallback.g();
            return;
          case 7: 
            localCallback.c();
            return;
          case 8: 
            localCallback.d();
            return;
          case 9: 
            localCallback.e();
            return;
          case 10: 
            localCallback.f();
            return;
          case 11: 
            localCallback.b(((Long)paramMessage.obj).longValue());
            return;
          case 12: 
            localCallback.a((RatingCompat)paramMessage.obj);
            return;
          case 13: 
            localCallback.c((String)paramMessage.obj, paramMessage.getData());
            return;
          case 14: 
            paramMessage = (KeyEvent)paramMessage.obj;
            localIntent = new Intent("android.intent.action.MEDIA_BUTTON");
            localIntent.putExtra("android.intent.extra.KEY_EVENT", paramMessage);
          }
        } while (localCallback.a(localIntent));
        a(paramMessage, localCallback);
        return;
        paramMessage = (MediaSessionCompat.MediaSessionImplBase.Command)paramMessage.obj;
        localCallback.a(paramMessage.a, paramMessage.b, paramMessage.c);
        return;
        MediaSessionCompat.MediaSessionImplBase.a(this.a, ((Integer)paramMessage.obj).intValue(), 0);
        return;
        MediaSessionCompat.MediaSessionImplBase.b(this.a, ((Integer)paramMessage.obj).intValue(), 0);
      }
    }
  }
  
  public static abstract interface OnActiveChangeListener {}
  
  public static final class QueueItem
    implements Parcelable
  {
    public static final Parcelable.Creator<QueueItem> CREATOR = new Parcelable.Creator()
    {
      public MediaSessionCompat.QueueItem a(Parcel paramAnonymousParcel)
      {
        return new MediaSessionCompat.QueueItem(paramAnonymousParcel, null);
      }
      
      public MediaSessionCompat.QueueItem[] a(int paramAnonymousInt)
      {
        return new MediaSessionCompat.QueueItem[paramAnonymousInt];
      }
    };
    private final MediaDescriptionCompat a;
    private final long b;
    
    private QueueItem(Parcel paramParcel)
    {
      this.a = ((MediaDescriptionCompat)MediaDescriptionCompat.CREATOR.createFromParcel(paramParcel));
      this.b = paramParcel.readLong();
    }
    
    public int describeContents()
    {
      return 0;
    }
    
    public String toString()
    {
      return "MediaSession.QueueItem {Description=" + this.a + ", Id=" + this.b + " }";
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      this.a.writeToParcel(paramParcel, paramInt);
      paramParcel.writeLong(this.b);
    }
  }
  
  static final class ResultReceiverWrapper
    implements Parcelable
  {
    public static final Parcelable.Creator<ResultReceiverWrapper> CREATOR = new Parcelable.Creator()
    {
      public MediaSessionCompat.ResultReceiverWrapper a(Parcel paramAnonymousParcel)
      {
        return new MediaSessionCompat.ResultReceiverWrapper(paramAnonymousParcel);
      }
      
      public MediaSessionCompat.ResultReceiverWrapper[] a(int paramAnonymousInt)
      {
        return new MediaSessionCompat.ResultReceiverWrapper[paramAnonymousInt];
      }
    };
    private ResultReceiver a;
    
    ResultReceiverWrapper(Parcel paramParcel)
    {
      this.a = ((ResultReceiver)ResultReceiver.CREATOR.createFromParcel(paramParcel));
    }
    
    public int describeContents()
    {
      return 0;
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      this.a.writeToParcel(paramParcel, paramInt);
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SessionFlags {}
  
  public static final class Token
    implements Parcelable
  {
    public static final Parcelable.Creator<Token> CREATOR = new Parcelable.Creator()
    {
      public MediaSessionCompat.Token a(Parcel paramAnonymousParcel)
      {
        if (Build.VERSION.SDK_INT >= 21) {}
        for (paramAnonymousParcel = paramAnonymousParcel.readParcelable(null);; paramAnonymousParcel = paramAnonymousParcel.readStrongBinder()) {
          return new MediaSessionCompat.Token(paramAnonymousParcel);
        }
      }
      
      public MediaSessionCompat.Token[] a(int paramAnonymousInt)
      {
        return new MediaSessionCompat.Token[paramAnonymousInt];
      }
    };
    private final Object a;
    
    Token(Object paramObject)
    {
      this.a = paramObject;
    }
    
    public Object a()
    {
      return this.a;
    }
    
    public int describeContents()
    {
      return 0;
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      if (Build.VERSION.SDK_INT >= 21)
      {
        paramParcel.writeParcelable((Parcelable)this.a, paramInt);
        return;
      }
      paramParcel.writeStrongBinder((IBinder)this.a);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/media/session/MediaSessionCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */