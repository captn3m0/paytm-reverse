package android.support.v4.media.session;

import android.content.Intent;
import android.media.AudioAttributes.Builder;
import android.media.Rating;
import android.media.VolumeProvider;
import android.media.session.MediaSession;
import android.media.session.MediaSession.Callback;
import android.os.Bundle;
import android.os.ResultReceiver;

class MediaSessionCompatApi21
{
  public static Object a(Callback paramCallback)
  {
    return new CallbackProxy(paramCallback);
  }
  
  public static void a(Object paramObject, int paramInt)
  {
    AudioAttributes.Builder localBuilder = new AudioAttributes.Builder();
    localBuilder.setLegacyStreamType(paramInt);
    ((MediaSession)paramObject).setPlaybackToLocal(localBuilder.build());
  }
  
  public static void a(Object paramObject1, Object paramObject2)
  {
    ((MediaSession)paramObject1).setPlaybackToRemote((VolumeProvider)paramObject2);
  }
  
  static abstract interface Callback
    extends MediaSessionCompatApi19.Callback
  {
    public abstract void a();
    
    public abstract void a(long paramLong);
    
    public abstract void a(String paramString, Bundle paramBundle);
    
    public abstract void a(String paramString, Bundle paramBundle, ResultReceiver paramResultReceiver);
    
    public abstract boolean a(Intent paramIntent);
    
    public abstract void b();
    
    public abstract void b(String paramString, Bundle paramBundle);
    
    public abstract void c();
    
    public abstract void c(String paramString, Bundle paramBundle);
    
    public abstract void d();
    
    public abstract void e();
    
    public abstract void f();
    
    public abstract void g();
  }
  
  static class CallbackProxy<T extends MediaSessionCompatApi21.Callback>
    extends MediaSession.Callback
  {
    protected final T a;
    
    public CallbackProxy(T paramT)
    {
      this.a = paramT;
    }
    
    public void onCommand(String paramString, Bundle paramBundle, ResultReceiver paramResultReceiver)
    {
      this.a.a(paramString, paramBundle, paramResultReceiver);
    }
    
    public void onCustomAction(String paramString, Bundle paramBundle)
    {
      this.a.c(paramString, paramBundle);
    }
    
    public void onFastForward()
    {
      this.a.e();
    }
    
    public boolean onMediaButtonEvent(Intent paramIntent)
    {
      return (this.a.a(paramIntent)) || (super.onMediaButtonEvent(paramIntent));
    }
    
    public void onPause()
    {
      this.a.b();
    }
    
    public void onPlay()
    {
      this.a.a();
    }
    
    public void onPlayFromMediaId(String paramString, Bundle paramBundle)
    {
      this.a.a(paramString, paramBundle);
    }
    
    public void onPlayFromSearch(String paramString, Bundle paramBundle)
    {
      this.a.b(paramString, paramBundle);
    }
    
    public void onRewind()
    {
      this.a.f();
    }
    
    public void onSeekTo(long paramLong)
    {
      this.a.b(paramLong);
    }
    
    public void onSetRating(Rating paramRating)
    {
      this.a.a(paramRating);
    }
    
    public void onSkipToNext()
    {
      this.a.c();
    }
    
    public void onSkipToPrevious()
    {
      this.a.d();
    }
    
    public void onSkipToQueueItem(long paramLong)
    {
      this.a.a(paramLong);
    }
    
    public void onStop()
    {
      this.a.g();
    }
  }
  
  static class QueueItem {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/media/session/MediaSessionCompatApi21.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */