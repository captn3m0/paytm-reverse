package android.support.v4.media.session;

import android.media.RemoteControlClient.OnPlaybackPositionUpdateListener;

class MediaSessionCompatApi18
{
  private static boolean a = true;
  
  static abstract interface Callback
  {
    public abstract void b(long paramLong);
  }
  
  static class OnPlaybackPositionUpdateListener<T extends MediaSessionCompatApi18.Callback>
    implements RemoteControlClient.OnPlaybackPositionUpdateListener
  {
    protected final T a;
    
    public void onPlaybackPositionUpdate(long paramLong)
    {
      this.a.b(paramLong);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/media/session/MediaSessionCompatApi18.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */