package android.support.v4.media.session;

import android.media.Rating;
import android.media.RemoteControlClient.OnMetadataUpdateListener;

class MediaSessionCompatApi19
{
  static abstract interface Callback
    extends MediaSessionCompatApi18.Callback
  {
    public abstract void a(Object paramObject);
  }
  
  static class OnMetadataUpdateListener<T extends MediaSessionCompatApi19.Callback>
    implements RemoteControlClient.OnMetadataUpdateListener
  {
    protected final T a;
    
    public void onMetadataUpdate(int paramInt, Object paramObject)
    {
      if ((paramInt == 268435457) && ((paramObject instanceof Rating))) {
        this.a.a(paramObject);
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/media/session/MediaSessionCompatApi19.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */