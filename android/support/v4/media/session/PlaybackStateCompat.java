package android.support.v4.media.session;

import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class PlaybackStateCompat
  implements Parcelable
{
  public static final Parcelable.Creator<PlaybackStateCompat> CREATOR = new Parcelable.Creator()
  {
    public PlaybackStateCompat a(Parcel paramAnonymousParcel)
    {
      return new PlaybackStateCompat(paramAnonymousParcel, null);
    }
    
    public PlaybackStateCompat[] a(int paramAnonymousInt)
    {
      return new PlaybackStateCompat[paramAnonymousInt];
    }
  };
  private final int a;
  private final long b;
  private final long c;
  private final float d;
  private final long e;
  private final CharSequence f;
  private final long g;
  private List<CustomAction> h;
  private final long i;
  private final Bundle j;
  private Object k;
  
  private PlaybackStateCompat(int paramInt, long paramLong1, long paramLong2, float paramFloat, long paramLong3, CharSequence paramCharSequence, long paramLong4, List<CustomAction> paramList, long paramLong5, Bundle paramBundle)
  {
    this.a = paramInt;
    this.b = paramLong1;
    this.c = paramLong2;
    this.d = paramFloat;
    this.e = paramLong3;
    this.f = paramCharSequence;
    this.g = paramLong4;
    this.h = new ArrayList(paramList);
    this.i = paramLong5;
    this.j = paramBundle;
  }
  
  private PlaybackStateCompat(Parcel paramParcel)
  {
    this.a = paramParcel.readInt();
    this.b = paramParcel.readLong();
    this.d = paramParcel.readFloat();
    this.g = paramParcel.readLong();
    this.c = paramParcel.readLong();
    this.e = paramParcel.readLong();
    this.f = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
    this.h = paramParcel.createTypedArrayList(CustomAction.CREATOR);
    this.i = paramParcel.readLong();
    this.j = paramParcel.readBundle();
  }
  
  public static PlaybackStateCompat a(Object paramObject)
  {
    if ((paramObject == null) || (Build.VERSION.SDK_INT < 21)) {
      return null;
    }
    Object localObject3 = PlaybackStateCompatApi21.h(paramObject);
    Object localObject1 = null;
    if (localObject3 != null)
    {
      localObject2 = new ArrayList(((List)localObject3).size());
      localObject3 = ((List)localObject3).iterator();
      for (;;)
      {
        localObject1 = localObject2;
        if (!((Iterator)localObject3).hasNext()) {
          break;
        }
        ((List)localObject2).add(CustomAction.a(((Iterator)localObject3).next()));
      }
    }
    if (Build.VERSION.SDK_INT >= 22) {}
    for (Object localObject2 = PlaybackStateCompatApi22.a(paramObject);; localObject2 = null)
    {
      localObject1 = new PlaybackStateCompat(PlaybackStateCompatApi21.a(paramObject), PlaybackStateCompatApi21.b(paramObject), PlaybackStateCompatApi21.c(paramObject), PlaybackStateCompatApi21.d(paramObject), PlaybackStateCompatApi21.e(paramObject), PlaybackStateCompatApi21.f(paramObject), PlaybackStateCompatApi21.g(paramObject), (List)localObject1, PlaybackStateCompatApi21.i(paramObject), (Bundle)localObject2);
      ((PlaybackStateCompat)localObject1).k = paramObject;
      return (PlaybackStateCompat)localObject1;
    }
  }
  
  public int a()
  {
    return this.a;
  }
  
  public long b()
  {
    return this.b;
  }
  
  public float c()
  {
    return this.d;
  }
  
  public long d()
  {
    return this.e;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public long e()
  {
    return this.g;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("PlaybackState {");
    localStringBuilder.append("state=").append(this.a);
    localStringBuilder.append(", position=").append(this.b);
    localStringBuilder.append(", buffered position=").append(this.c);
    localStringBuilder.append(", speed=").append(this.d);
    localStringBuilder.append(", updated=").append(this.g);
    localStringBuilder.append(", actions=").append(this.e);
    localStringBuilder.append(", error=").append(this.f);
    localStringBuilder.append(", custom actions=").append(this.h);
    localStringBuilder.append(", active item id=").append(this.i);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeInt(this.a);
    paramParcel.writeLong(this.b);
    paramParcel.writeFloat(this.d);
    paramParcel.writeLong(this.g);
    paramParcel.writeLong(this.c);
    paramParcel.writeLong(this.e);
    TextUtils.writeToParcel(this.f, paramParcel, paramInt);
    paramParcel.writeTypedList(this.h);
    paramParcel.writeLong(this.i);
    paramParcel.writeBundle(this.j);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Actions {}
  
  public static final class Builder
  {
    private final List<PlaybackStateCompat.CustomAction> a = new ArrayList();
    private int b;
    private long c;
    private long d;
    private float e;
    private long f;
    private CharSequence g;
    private long h;
    private long i = -1L;
    private Bundle j;
    
    public Builder() {}
    
    public Builder(PlaybackStateCompat paramPlaybackStateCompat)
    {
      this.b = PlaybackStateCompat.a(paramPlaybackStateCompat);
      this.c = PlaybackStateCompat.b(paramPlaybackStateCompat);
      this.e = PlaybackStateCompat.c(paramPlaybackStateCompat);
      this.h = PlaybackStateCompat.d(paramPlaybackStateCompat);
      this.d = PlaybackStateCompat.e(paramPlaybackStateCompat);
      this.f = PlaybackStateCompat.f(paramPlaybackStateCompat);
      this.g = PlaybackStateCompat.g(paramPlaybackStateCompat);
      if (PlaybackStateCompat.h(paramPlaybackStateCompat) != null) {
        this.a.addAll(PlaybackStateCompat.h(paramPlaybackStateCompat));
      }
      this.i = PlaybackStateCompat.i(paramPlaybackStateCompat);
      this.j = PlaybackStateCompat.j(paramPlaybackStateCompat);
    }
    
    public Builder a(int paramInt, long paramLong1, float paramFloat, long paramLong2)
    {
      this.b = paramInt;
      this.c = paramLong1;
      this.h = paramLong2;
      this.e = paramFloat;
      return this;
    }
    
    public PlaybackStateCompat a()
    {
      return new PlaybackStateCompat(this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.a, this.i, this.j, null);
    }
  }
  
  public static final class CustomAction
    implements Parcelable
  {
    public static final Parcelable.Creator<CustomAction> CREATOR = new Parcelable.Creator()
    {
      public PlaybackStateCompat.CustomAction a(Parcel paramAnonymousParcel)
      {
        return new PlaybackStateCompat.CustomAction(paramAnonymousParcel, null);
      }
      
      public PlaybackStateCompat.CustomAction[] a(int paramAnonymousInt)
      {
        return new PlaybackStateCompat.CustomAction[paramAnonymousInt];
      }
    };
    private final String a;
    private final CharSequence b;
    private final int c;
    private final Bundle d;
    private Object e;
    
    private CustomAction(Parcel paramParcel)
    {
      this.a = paramParcel.readString();
      this.b = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
      this.c = paramParcel.readInt();
      this.d = paramParcel.readBundle();
    }
    
    private CustomAction(String paramString, CharSequence paramCharSequence, int paramInt, Bundle paramBundle)
    {
      this.a = paramString;
      this.b = paramCharSequence;
      this.c = paramInt;
      this.d = paramBundle;
    }
    
    public static CustomAction a(Object paramObject)
    {
      if ((paramObject == null) || (Build.VERSION.SDK_INT < 21)) {
        return null;
      }
      CustomAction localCustomAction = new CustomAction(PlaybackStateCompatApi21.CustomAction.a(paramObject), PlaybackStateCompatApi21.CustomAction.b(paramObject), PlaybackStateCompatApi21.CustomAction.c(paramObject), PlaybackStateCompatApi21.CustomAction.d(paramObject));
      localCustomAction.e = paramObject;
      return localCustomAction;
    }
    
    public int describeContents()
    {
      return 0;
    }
    
    public String toString()
    {
      return "Action:mName='" + this.b + ", mIcon=" + this.c + ", mExtras=" + this.d;
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      paramParcel.writeString(this.a);
      TextUtils.writeToParcel(this.b, paramParcel, paramInt);
      paramParcel.writeInt(this.c);
      paramParcel.writeBundle(this.d);
    }
    
    public static final class Builder {}
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface State {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/media/session/PlaybackStateCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */