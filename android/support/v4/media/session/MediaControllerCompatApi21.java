package android.support.v4.media.session;

import android.content.Context;
import android.media.MediaMetadata;
import android.media.session.MediaController;
import android.media.session.MediaController.Callback;
import android.media.session.MediaController.TransportControls;
import android.media.session.MediaSession.Token;
import android.media.session.PlaybackState;
import android.os.Bundle;
import android.os.Handler;

class MediaControllerCompatApi21
{
  public static Object a(Context paramContext, Object paramObject)
  {
    return new MediaController(paramContext, (MediaSession.Token)paramObject);
  }
  
  public static Object a(Callback paramCallback)
  {
    return new CallbackProxy(paramCallback);
  }
  
  public static Object a(Object paramObject)
  {
    return ((MediaController)paramObject).getTransportControls();
  }
  
  public static void a(Object paramObject1, Object paramObject2)
  {
    ((MediaController)paramObject1).unregisterCallback((MediaController.Callback)paramObject2);
  }
  
  public static void a(Object paramObject1, Object paramObject2, Handler paramHandler)
  {
    ((MediaController)paramObject1).registerCallback((MediaController.Callback)paramObject2, paramHandler);
  }
  
  public static Object b(Object paramObject)
  {
    return ((MediaController)paramObject).getPlaybackState();
  }
  
  public static Object c(Object paramObject)
  {
    return ((MediaController)paramObject).getMetadata();
  }
  
  public static abstract interface Callback
  {
    public abstract void a();
    
    public abstract void a(Object paramObject);
    
    public abstract void a(String paramString, Bundle paramBundle);
    
    public abstract void b(Object paramObject);
  }
  
  static class CallbackProxy<T extends MediaControllerCompatApi21.Callback>
    extends MediaController.Callback
  {
    protected final T a;
    
    public CallbackProxy(T paramT)
    {
      this.a = paramT;
    }
    
    public void onMetadataChanged(MediaMetadata paramMediaMetadata)
    {
      this.a.b(paramMediaMetadata);
    }
    
    public void onPlaybackStateChanged(PlaybackState paramPlaybackState)
    {
      this.a.a(paramPlaybackState);
    }
    
    public void onSessionDestroyed()
    {
      this.a.a();
    }
    
    public void onSessionEvent(String paramString, Bundle paramBundle)
    {
      this.a.a(paramString, paramBundle);
    }
  }
  
  public static class PlaybackInfo {}
  
  public static class TransportControls
  {
    public static void a(Object paramObject)
    {
      ((MediaController.TransportControls)paramObject).play();
    }
    
    public static void b(Object paramObject)
    {
      ((MediaController.TransportControls)paramObject).pause();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/media/session/MediaControllerCompatApi21.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */