package android.support.v4.media.session;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.support.v4.media.MediaMetadataCompat;
import android.util.Log;
import java.util.List;

public final class MediaControllerCompat
{
  private final MediaControllerImpl a;
  private final MediaSessionCompat.Token b;
  
  public MediaControllerCompat(Context paramContext, MediaSessionCompat.Token paramToken)
    throws RemoteException
  {
    if (paramToken == null) {
      throw new IllegalArgumentException("sessionToken must not be null");
    }
    this.b = paramToken;
    if (Build.VERSION.SDK_INT >= 23)
    {
      this.a = new MediaControllerImplApi23(paramContext, paramToken);
      return;
    }
    if (Build.VERSION.SDK_INT >= 21)
    {
      this.a = new MediaControllerImplApi21(paramContext, paramToken);
      return;
    }
    this.a = new MediaControllerImplBase(this.b);
  }
  
  public TransportControls a()
  {
    return this.a.a();
  }
  
  public void a(Callback paramCallback)
  {
    a(paramCallback, null);
  }
  
  public void a(Callback paramCallback, Handler paramHandler)
  {
    if (paramCallback == null) {
      throw new IllegalArgumentException("callback cannot be null");
    }
    Handler localHandler = paramHandler;
    if (paramHandler == null) {
      localHandler = new Handler();
    }
    this.a.a(paramCallback, localHandler);
  }
  
  public PlaybackStateCompat b()
  {
    return this.a.b();
  }
  
  public void b(Callback paramCallback)
  {
    if (paramCallback == null) {
      throw new IllegalArgumentException("callback cannot be null");
    }
    this.a.a(paramCallback);
  }
  
  public MediaMetadataCompat c()
  {
    return this.a.c();
  }
  
  public Object d()
  {
    return this.a.d();
  }
  
  public static abstract class Callback
    implements IBinder.DeathRecipient
  {
    private final Object a;
    private MessageHandler b;
    private boolean c = false;
    
    public Callback()
    {
      if (Build.VERSION.SDK_INT >= 21)
      {
        this.a = MediaControllerCompatApi21.a(new StubApi21(null));
        return;
      }
      this.a = new StubCompat(null);
    }
    
    private void a(Handler paramHandler)
    {
      this.b = new MessageHandler(paramHandler.getLooper());
    }
    
    public void a() {}
    
    public void a(Bundle paramBundle) {}
    
    public void a(MediaMetadataCompat paramMediaMetadataCompat) {}
    
    public void a(MediaControllerCompat.PlaybackInfo paramPlaybackInfo) {}
    
    public void a(PlaybackStateCompat paramPlaybackStateCompat) {}
    
    public void a(CharSequence paramCharSequence) {}
    
    public void a(String paramString, Bundle paramBundle) {}
    
    public void a(List<MediaSessionCompat.QueueItem> paramList) {}
    
    public void binderDied()
    {
      a();
    }
    
    private class MessageHandler
      extends Handler
    {
      public MessageHandler(Looper paramLooper)
      {
        super();
      }
      
      public void a(int paramInt, Object paramObject, Bundle paramBundle)
      {
        paramObject = obtainMessage(paramInt, paramObject);
        ((Message)paramObject).setData(paramBundle);
        ((Message)paramObject).sendToTarget();
      }
      
      public void handleMessage(Message paramMessage)
      {
        if (!MediaControllerCompat.Callback.b(MediaControllerCompat.Callback.this)) {
          return;
        }
        switch (paramMessage.what)
        {
        default: 
          return;
        case 1: 
          MediaControllerCompat.Callback.this.a((String)paramMessage.obj, paramMessage.getData());
          return;
        case 2: 
          MediaControllerCompat.Callback.this.a((PlaybackStateCompat)paramMessage.obj);
          return;
        case 3: 
          MediaControllerCompat.Callback.this.a((MediaMetadataCompat)paramMessage.obj);
          return;
        case 5: 
          MediaControllerCompat.Callback.this.a((List)paramMessage.obj);
          return;
        case 6: 
          MediaControllerCompat.Callback.this.a((CharSequence)paramMessage.obj);
          return;
        case 7: 
          MediaControllerCompat.Callback.this.a((Bundle)paramMessage.obj);
          return;
        case 4: 
          MediaControllerCompat.Callback.this.a((MediaControllerCompat.PlaybackInfo)paramMessage.obj);
          return;
        }
        MediaControllerCompat.Callback.this.a();
      }
    }
    
    private class StubApi21
      implements MediaControllerCompatApi21.Callback
    {
      private StubApi21() {}
      
      public void a()
      {
        MediaControllerCompat.Callback.this.a();
      }
      
      public void a(Object paramObject)
      {
        MediaControllerCompat.Callback.this.a(PlaybackStateCompat.a(paramObject));
      }
      
      public void a(String paramString, Bundle paramBundle)
      {
        MediaControllerCompat.Callback.this.a(paramString, paramBundle);
      }
      
      public void b(Object paramObject)
      {
        MediaControllerCompat.Callback.this.a(MediaMetadataCompat.a(paramObject));
      }
    }
    
    private class StubCompat
      extends IMediaControllerCallback.Stub
    {
      private StubCompat() {}
      
      public void a()
        throws RemoteException
      {
        MediaControllerCompat.Callback.a(MediaControllerCompat.Callback.this).a(8, null, null);
      }
      
      public void a(Bundle paramBundle)
        throws RemoteException
      {
        MediaControllerCompat.Callback.a(MediaControllerCompat.Callback.this).a(7, paramBundle, null);
      }
      
      public void a(MediaMetadataCompat paramMediaMetadataCompat)
        throws RemoteException
      {
        MediaControllerCompat.Callback.a(MediaControllerCompat.Callback.this).a(3, paramMediaMetadataCompat, null);
      }
      
      public void a(ParcelableVolumeInfo paramParcelableVolumeInfo)
        throws RemoteException
      {
        MediaControllerCompat.PlaybackInfo localPlaybackInfo = null;
        if (paramParcelableVolumeInfo != null) {
          localPlaybackInfo = new MediaControllerCompat.PlaybackInfo(paramParcelableVolumeInfo.a, paramParcelableVolumeInfo.b, paramParcelableVolumeInfo.c, paramParcelableVolumeInfo.d, paramParcelableVolumeInfo.e);
        }
        MediaControllerCompat.Callback.a(MediaControllerCompat.Callback.this).a(4, localPlaybackInfo, null);
      }
      
      public void a(PlaybackStateCompat paramPlaybackStateCompat)
        throws RemoteException
      {
        MediaControllerCompat.Callback.a(MediaControllerCompat.Callback.this).a(2, paramPlaybackStateCompat, null);
      }
      
      public void a(CharSequence paramCharSequence)
        throws RemoteException
      {
        MediaControllerCompat.Callback.a(MediaControllerCompat.Callback.this).a(6, paramCharSequence, null);
      }
      
      public void a(String paramString, Bundle paramBundle)
        throws RemoteException
      {
        MediaControllerCompat.Callback.a(MediaControllerCompat.Callback.this).a(1, paramString, paramBundle);
      }
      
      public void a(List<MediaSessionCompat.QueueItem> paramList)
        throws RemoteException
      {
        MediaControllerCompat.Callback.a(MediaControllerCompat.Callback.this).a(5, paramList, null);
      }
    }
  }
  
  static abstract interface MediaControllerImpl
  {
    public abstract MediaControllerCompat.TransportControls a();
    
    public abstract void a(MediaControllerCompat.Callback paramCallback);
    
    public abstract void a(MediaControllerCompat.Callback paramCallback, Handler paramHandler);
    
    public abstract PlaybackStateCompat b();
    
    public abstract MediaMetadataCompat c();
    
    public abstract Object d();
  }
  
  static class MediaControllerImplApi21
    implements MediaControllerCompat.MediaControllerImpl
  {
    protected final Object a;
    
    public MediaControllerImplApi21(Context paramContext, MediaSessionCompat.Token paramToken)
      throws RemoteException
    {
      this.a = MediaControllerCompatApi21.a(paramContext, paramToken.a());
      if (this.a == null) {
        throw new RemoteException();
      }
    }
    
    public MediaControllerCompat.TransportControls a()
    {
      Object localObject = MediaControllerCompatApi21.a(this.a);
      if (localObject != null) {
        return new MediaControllerCompat.TransportControlsApi21(localObject);
      }
      return null;
    }
    
    public void a(MediaControllerCompat.Callback paramCallback)
    {
      MediaControllerCompatApi21.a(this.a, MediaControllerCompat.Callback.c(paramCallback));
    }
    
    public void a(MediaControllerCompat.Callback paramCallback, Handler paramHandler)
    {
      MediaControllerCompatApi21.a(this.a, MediaControllerCompat.Callback.c(paramCallback), paramHandler);
    }
    
    public PlaybackStateCompat b()
    {
      Object localObject = MediaControllerCompatApi21.b(this.a);
      if (localObject != null) {
        return PlaybackStateCompat.a(localObject);
      }
      return null;
    }
    
    public MediaMetadataCompat c()
    {
      Object localObject = MediaControllerCompatApi21.c(this.a);
      if (localObject != null) {
        return MediaMetadataCompat.a(localObject);
      }
      return null;
    }
    
    public Object d()
    {
      return this.a;
    }
  }
  
  static class MediaControllerImplApi23
    extends MediaControllerCompat.MediaControllerImplApi21
  {
    public MediaControllerImplApi23(Context paramContext, MediaSessionCompat.Token paramToken)
      throws RemoteException
    {
      super(paramToken);
    }
    
    public MediaControllerCompat.TransportControls a()
    {
      Object localObject = MediaControllerCompatApi21.a(this.a);
      if (localObject != null) {
        return new MediaControllerCompat.TransportControlsApi23(localObject);
      }
      return null;
    }
  }
  
  static class MediaControllerImplBase
    implements MediaControllerCompat.MediaControllerImpl
  {
    private MediaSessionCompat.Token a;
    private IMediaSession b;
    private MediaControllerCompat.TransportControls c;
    
    public MediaControllerImplBase(MediaSessionCompat.Token paramToken)
    {
      this.a = paramToken;
      this.b = IMediaSession.Stub.a((IBinder)paramToken.a());
    }
    
    public MediaControllerCompat.TransportControls a()
    {
      if (this.c == null) {
        this.c = new MediaControllerCompat.TransportControlsBase(this.b);
      }
      return this.c;
    }
    
    public void a(MediaControllerCompat.Callback paramCallback)
    {
      if (paramCallback == null) {
        throw new IllegalArgumentException("callback may not be null.");
      }
      try
      {
        this.b.b((IMediaControllerCallback)MediaControllerCompat.Callback.c(paramCallback));
        this.b.asBinder().unlinkToDeath(paramCallback, 0);
        MediaControllerCompat.Callback.a(paramCallback, false);
        return;
      }
      catch (RemoteException paramCallback)
      {
        Log.e("MediaControllerCompat", "Dead object in unregisterCallback. " + paramCallback);
      }
    }
    
    public void a(MediaControllerCompat.Callback paramCallback, Handler paramHandler)
    {
      if (paramCallback == null) {
        throw new IllegalArgumentException("callback may not be null.");
      }
      try
      {
        this.b.asBinder().linkToDeath(paramCallback, 0);
        this.b.a((IMediaControllerCallback)MediaControllerCompat.Callback.c(paramCallback));
        MediaControllerCompat.Callback.a(paramCallback, paramHandler);
        MediaControllerCompat.Callback.a(paramCallback, true);
        return;
      }
      catch (RemoteException paramHandler)
      {
        Log.e("MediaControllerCompat", "Dead object in registerCallback. " + paramHandler);
        paramCallback.a();
      }
    }
    
    public PlaybackStateCompat b()
    {
      try
      {
        PlaybackStateCompat localPlaybackStateCompat = this.b.o();
        return localPlaybackStateCompat;
      }
      catch (RemoteException localRemoteException)
      {
        Log.e("MediaControllerCompat", "Dead object in getPlaybackState. " + localRemoteException);
      }
      return null;
    }
    
    public MediaMetadataCompat c()
    {
      try
      {
        MediaMetadataCompat localMediaMetadataCompat = this.b.n();
        return localMediaMetadataCompat;
      }
      catch (RemoteException localRemoteException)
      {
        Log.e("MediaControllerCompat", "Dead object in getMetadata. " + localRemoteException);
      }
      return null;
    }
    
    public Object d()
    {
      return null;
    }
  }
  
  public static final class PlaybackInfo
  {
    private final int a;
    private final int b;
    private final int c;
    private final int d;
    private final int e;
    
    PlaybackInfo(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
      this.a = paramInt1;
      this.b = paramInt2;
      this.c = paramInt3;
      this.d = paramInt4;
      this.e = paramInt5;
    }
  }
  
  public static abstract class TransportControls
  {
    public abstract void a();
    
    public abstract void b();
  }
  
  static class TransportControlsApi21
    extends MediaControllerCompat.TransportControls
  {
    protected final Object a;
    
    public TransportControlsApi21(Object paramObject)
    {
      this.a = paramObject;
    }
    
    public void a()
    {
      MediaControllerCompatApi21.TransportControls.a(this.a);
    }
    
    public void b()
    {
      MediaControllerCompatApi21.TransportControls.b(this.a);
    }
  }
  
  static class TransportControlsApi23
    extends MediaControllerCompat.TransportControlsApi21
  {
    public TransportControlsApi23(Object paramObject)
    {
      super();
    }
  }
  
  static class TransportControlsBase
    extends MediaControllerCompat.TransportControls
  {
    private IMediaSession a;
    
    public TransportControlsBase(IMediaSession paramIMediaSession)
    {
      this.a = paramIMediaSession;
    }
    
    public void a()
    {
      try
      {
        this.a.g();
        return;
      }
      catch (RemoteException localRemoteException)
      {
        Log.e("MediaControllerCompat", "Dead object in play. " + localRemoteException);
      }
    }
    
    public void b()
    {
      try
      {
        this.a.h();
        return;
      }
      catch (RemoteException localRemoteException)
      {
        Log.e("MediaControllerCompat", "Dead object in pause. " + localRemoteException);
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/media/session/MediaControllerCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */