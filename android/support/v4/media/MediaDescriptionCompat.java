package android.support.v4.media;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import android.text.TextUtils;

public final class MediaDescriptionCompat
  implements Parcelable
{
  public static final Parcelable.Creator<MediaDescriptionCompat> CREATOR = new Parcelable.Creator()
  {
    public MediaDescriptionCompat a(Parcel paramAnonymousParcel)
    {
      if (Build.VERSION.SDK_INT < 21) {
        return new MediaDescriptionCompat(paramAnonymousParcel, null);
      }
      return MediaDescriptionCompat.a(MediaDescriptionCompatApi21.a(paramAnonymousParcel));
    }
    
    public MediaDescriptionCompat[] a(int paramAnonymousInt)
    {
      return new MediaDescriptionCompat[paramAnonymousInt];
    }
  };
  private final String a;
  private final CharSequence b;
  private final CharSequence c;
  private final CharSequence d;
  private final Bitmap e;
  private final Uri f;
  private final Bundle g;
  private final Uri h;
  private Object i;
  
  private MediaDescriptionCompat(Parcel paramParcel)
  {
    this.a = paramParcel.readString();
    this.b = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
    this.c = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
    this.d = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
    this.e = ((Bitmap)paramParcel.readParcelable(null));
    this.f = ((Uri)paramParcel.readParcelable(null));
    this.g = paramParcel.readBundle();
    this.h = ((Uri)paramParcel.readParcelable(null));
  }
  
  private MediaDescriptionCompat(String paramString, CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3, Bitmap paramBitmap, Uri paramUri1, Bundle paramBundle, Uri paramUri2)
  {
    this.a = paramString;
    this.b = paramCharSequence1;
    this.c = paramCharSequence2;
    this.d = paramCharSequence3;
    this.e = paramBitmap;
    this.f = paramUri1;
    this.g = paramBundle;
    this.h = paramUri2;
  }
  
  public static MediaDescriptionCompat a(Object paramObject)
  {
    if ((paramObject == null) || (Build.VERSION.SDK_INT < 21)) {
      return null;
    }
    Builder localBuilder = new Builder();
    localBuilder.a(MediaDescriptionCompatApi21.a(paramObject));
    localBuilder.a(MediaDescriptionCompatApi21.b(paramObject));
    localBuilder.b(MediaDescriptionCompatApi21.c(paramObject));
    localBuilder.c(MediaDescriptionCompatApi21.d(paramObject));
    localBuilder.a(MediaDescriptionCompatApi21.e(paramObject));
    localBuilder.a(MediaDescriptionCompatApi21.f(paramObject));
    Bundle localBundle2 = MediaDescriptionCompatApi21.g(paramObject);
    Object localObject;
    Bundle localBundle1;
    if (localBundle2 == null)
    {
      localObject = null;
      localBundle1 = localBundle2;
      if (localObject != null)
      {
        if ((!localBundle2.containsKey("android.support.v4.media.description.NULL_BUNDLE_FLAG")) || (localBundle2.size() != 2)) {
          break label163;
        }
        localBundle1 = null;
      }
      label119:
      localBuilder.a(localBundle1);
      if (localObject == null) {
        break label180;
      }
      localBuilder.b((Uri)localObject);
    }
    for (;;)
    {
      localObject = localBuilder.a();
      ((MediaDescriptionCompat)localObject).i = paramObject;
      return (MediaDescriptionCompat)localObject;
      localObject = (Uri)localBundle2.getParcelable("android.support.v4.media.description.MEDIA_URI");
      break;
      label163:
      localBundle2.remove("android.support.v4.media.description.MEDIA_URI");
      localBundle2.remove("android.support.v4.media.description.NULL_BUNDLE_FLAG");
      localBundle1 = localBundle2;
      break label119;
      label180:
      if (Build.VERSION.SDK_INT >= 23) {
        localBuilder.b(MediaDescriptionCompatApi23.h(paramObject));
      }
    }
  }
  
  @Nullable
  public CharSequence a()
  {
    return this.b;
  }
  
  @Nullable
  public CharSequence b()
  {
    return this.c;
  }
  
  @Nullable
  public Bitmap c()
  {
    return this.e;
  }
  
  @Nullable
  public Uri d()
  {
    return this.f;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Object e()
  {
    if ((this.i != null) || (Build.VERSION.SDK_INT < 21)) {
      return this.i;
    }
    Object localObject = MediaDescriptionCompatApi21.Builder.a();
    MediaDescriptionCompatApi21.Builder.a(localObject, this.a);
    MediaDescriptionCompatApi21.Builder.a(localObject, this.b);
    MediaDescriptionCompatApi21.Builder.b(localObject, this.c);
    MediaDescriptionCompatApi21.Builder.c(localObject, this.d);
    MediaDescriptionCompatApi21.Builder.a(localObject, this.e);
    MediaDescriptionCompatApi21.Builder.a(localObject, this.f);
    Bundle localBundle2 = this.g;
    Bundle localBundle1 = localBundle2;
    if (Build.VERSION.SDK_INT < 23)
    {
      localBundle1 = localBundle2;
      if (this.h != null)
      {
        localBundle1 = localBundle2;
        if (localBundle2 == null)
        {
          localBundle1 = new Bundle();
          localBundle1.putBoolean("android.support.v4.media.description.NULL_BUNDLE_FLAG", true);
        }
        localBundle1.putParcelable("android.support.v4.media.description.MEDIA_URI", this.h);
      }
    }
    MediaDescriptionCompatApi21.Builder.a(localObject, localBundle1);
    if (Build.VERSION.SDK_INT >= 23) {
      MediaDescriptionCompatApi23.Builder.b(localObject, this.h);
    }
    this.i = MediaDescriptionCompatApi21.Builder.a(localObject);
    return this.i;
  }
  
  public String toString()
  {
    return this.b + ", " + this.c + ", " + this.d;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    if (Build.VERSION.SDK_INT < 21)
    {
      paramParcel.writeString(this.a);
      TextUtils.writeToParcel(this.b, paramParcel, paramInt);
      TextUtils.writeToParcel(this.c, paramParcel, paramInt);
      TextUtils.writeToParcel(this.d, paramParcel, paramInt);
      paramParcel.writeParcelable(this.e, paramInt);
      paramParcel.writeParcelable(this.f, paramInt);
      paramParcel.writeBundle(this.g);
      paramParcel.writeParcelable(this.h, paramInt);
      return;
    }
    MediaDescriptionCompatApi21.a(e(), paramParcel, paramInt);
  }
  
  public static final class Builder
  {
    private String a;
    private CharSequence b;
    private CharSequence c;
    private CharSequence d;
    private Bitmap e;
    private Uri f;
    private Bundle g;
    private Uri h;
    
    public Builder a(@Nullable Bitmap paramBitmap)
    {
      this.e = paramBitmap;
      return this;
    }
    
    public Builder a(@Nullable Uri paramUri)
    {
      this.f = paramUri;
      return this;
    }
    
    public Builder a(@Nullable Bundle paramBundle)
    {
      this.g = paramBundle;
      return this;
    }
    
    public Builder a(@Nullable CharSequence paramCharSequence)
    {
      this.b = paramCharSequence;
      return this;
    }
    
    public Builder a(@Nullable String paramString)
    {
      this.a = paramString;
      return this;
    }
    
    public MediaDescriptionCompat a()
    {
      return new MediaDescriptionCompat(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, null);
    }
    
    public Builder b(@Nullable Uri paramUri)
    {
      this.h = paramUri;
      return this;
    }
    
    public Builder b(@Nullable CharSequence paramCharSequence)
    {
      this.c = paramCharSequence;
      return this;
    }
    
    public Builder c(@Nullable CharSequence paramCharSequence)
    {
      this.d = paramCharSequence;
      return this;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/media/MediaDescriptionCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */