package android.support.v4.media;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.RemoteControlClient;
import android.media.RemoteControlClient.OnGetPlaybackPositionListener;
import android.media.RemoteControlClient.OnPlaybackPositionUpdateListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.ViewTreeObserver.OnWindowAttachListener;
import android.view.ViewTreeObserver.OnWindowFocusChangeListener;

class TransportMediatorJellybeanMR2
{
  final Context a;
  final AudioManager b;
  final TransportMediatorCallback c;
  final IntentFilter d;
  final Intent e;
  final BroadcastReceiver f;
  AudioManager.OnAudioFocusChangeListener g;
  final RemoteControlClient.OnGetPlaybackPositionListener h;
  final RemoteControlClient.OnPlaybackPositionUpdateListener i;
  PendingIntent j;
  RemoteControlClient k;
  boolean l;
  int m;
  boolean n;
  
  void a()
  {
    this.a.registerReceiver(this.f, this.d);
    this.j = PendingIntent.getBroadcast(this.a, 0, this.e, 268435456);
    this.k = new RemoteControlClient(this.j);
    this.k.setOnGetPlaybackPositionListener(this.h);
    this.k.setPlaybackPositionUpdateListener(this.i);
  }
  
  void b()
  {
    if (!this.l)
    {
      this.l = true;
      this.b.registerMediaButtonEventReceiver(this.j);
      this.b.registerRemoteControlClient(this.k);
      if (this.m == 3) {
        c();
      }
    }
  }
  
  void c()
  {
    if (!this.n)
    {
      this.n = true;
      this.b.requestAudioFocus(this.g, 3, 1);
    }
  }
  
  void d()
  {
    if (this.n)
    {
      this.n = false;
      this.b.abandonAudioFocus(this.g);
    }
  }
  
  void e()
  {
    d();
    if (this.l)
    {
      this.l = false;
      this.b.unregisterRemoteControlClient(this.k);
      this.b.unregisterMediaButtonEventReceiver(this.j);
    }
  }
  
  void f()
  {
    e();
    if (this.j != null)
    {
      this.a.unregisterReceiver(this.f);
      this.j.cancel();
      this.j = null;
      this.k = null;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/media/TransportMediatorJellybeanMR2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */