package android.support.v4.media;

import android.media.MediaDescription.Builder;
import android.media.browse.MediaBrowser.MediaItem;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.os.ResultReceiver;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class MediaBrowserServiceCompatApi21
{
  static class MediaBrowserServiceAdaptorApi21
  {
    static class ServiceBinderProxyApi21
      extends IMediaBrowserServiceAdapterApi21.Stub
    {
      final MediaBrowserServiceCompatApi21.ServiceImplApi21 a;
      
      public void a(Object paramObject)
      {
        this.a.a(new MediaBrowserServiceCompatApi21.ServiceCallbacksApi21(paramObject));
      }
      
      public void a(String paramString, Bundle paramBundle, Object paramObject)
      {
        this.a.a(paramString, paramBundle, new MediaBrowserServiceCompatApi21.ServiceCallbacksApi21(paramObject));
      }
      
      public void a(String paramString, ResultReceiver paramResultReceiver) {}
      
      public void a(String paramString, Object paramObject)
      {
        this.a.a(paramString, new MediaBrowserServiceCompatApi21.ServiceCallbacksApi21(paramObject));
      }
      
      public void b(String paramString, Object paramObject)
      {
        this.a.b(paramString, new MediaBrowserServiceCompatApi21.ServiceCallbacksApi21(paramObject));
      }
    }
  }
  
  public static abstract interface ServiceCallbacks
  {
    public abstract IBinder a();
    
    public abstract void a(String paramString, Object paramObject, Bundle paramBundle)
      throws RemoteException;
    
    public abstract void a(String paramString, List<Parcel> paramList)
      throws RemoteException;
    
    public abstract void b()
      throws RemoteException;
  }
  
  public static class ServiceCallbacksApi21
    implements MediaBrowserServiceCompatApi21.ServiceCallbacks
  {
    private static Object a;
    private final IMediaBrowserServiceCallbacksAdapterApi21 b;
    
    static
    {
      MediaBrowser.MediaItem localMediaItem = new MediaBrowser.MediaItem(new MediaDescription.Builder().setMediaId("android.support.v4.media.MediaBrowserCompat.NULL_MEDIA_ITEM").build(), 0);
      ArrayList localArrayList = new ArrayList();
      localArrayList.add(localMediaItem);
      a = ParceledListSliceAdapterApi21.a(localArrayList);
    }
    
    ServiceCallbacksApi21(Object paramObject)
    {
      this.b = new IMediaBrowserServiceCallbacksAdapterApi21(paramObject);
    }
    
    public IBinder a()
    {
      return this.b.a();
    }
    
    public void a(String paramString, Object paramObject, Bundle paramBundle)
      throws RemoteException
    {
      this.b.a(paramString, paramObject, paramBundle);
    }
    
    public void a(String paramString, List<Parcel> paramList)
      throws RemoteException
    {
      Object localObject = null;
      if (paramList != null)
      {
        ArrayList localArrayList = new ArrayList();
        paramList = paramList.iterator();
        for (;;)
        {
          localObject = localArrayList;
          if (!paramList.hasNext()) {
            break;
          }
          localObject = (Parcel)paramList.next();
          ((Parcel)localObject).setDataPosition(0);
          localArrayList.add(MediaBrowser.MediaItem.CREATOR.createFromParcel((Parcel)localObject));
          ((Parcel)localObject).recycle();
        }
      }
      if (Build.VERSION.SDK_INT > 23)
      {
        if (localObject == null) {}
        for (paramList = null;; paramList = ParceledListSliceAdapterApi21.a((List)localObject))
        {
          this.b.a(paramString, paramList);
          return;
        }
      }
      if (localObject == null) {}
      for (paramList = a;; paramList = ParceledListSliceAdapterApi21.a((List)localObject)) {
        break;
      }
    }
    
    public void b()
      throws RemoteException
    {
      this.b.b();
    }
  }
  
  public static abstract interface ServiceImplApi21
  {
    public abstract void a(MediaBrowserServiceCompatApi21.ServiceCallbacks paramServiceCallbacks);
    
    public abstract void a(String paramString, Bundle paramBundle, MediaBrowserServiceCompatApi21.ServiceCallbacks paramServiceCallbacks);
    
    public abstract void a(String paramString, MediaBrowserServiceCompatApi21.ServiceCallbacks paramServiceCallbacks);
    
    public abstract void b(String paramString, MediaBrowserServiceCompatApi21.ServiceCallbacks paramServiceCallbacks);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/media/MediaBrowserServiceCompatApi21.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */