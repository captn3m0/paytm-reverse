package android.support.v4.media;

import android.os.SystemClock;
import android.view.KeyEvent;

public abstract class TransportPerformer
{
  public abstract void a();
  
  public void a(int paramInt)
  {
    int i = 0;
    switch (paramInt)
    {
    }
    for (paramInt = i;; paramInt = 127)
    {
      if (paramInt != 0)
      {
        long l = SystemClock.uptimeMillis();
        a(paramInt, new KeyEvent(l, l, 0, paramInt, 0));
        b(paramInt, new KeyEvent(l, l, 1, paramInt, 0));
      }
      return;
    }
  }
  
  public abstract void a(long paramLong);
  
  public boolean a(int paramInt, KeyEvent paramKeyEvent)
  {
    switch (paramInt)
    {
    default: 
      return true;
    case 126: 
      a();
      return true;
    case 127: 
      b();
      return true;
    case 86: 
      c();
      return true;
    }
    if (e())
    {
      b();
      return true;
    }
    a();
    return true;
  }
  
  public abstract void b();
  
  public boolean b(int paramInt, KeyEvent paramKeyEvent)
  {
    return true;
  }
  
  public abstract void c();
  
  public abstract long d();
  
  public abstract boolean e();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/media/TransportPerformer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */