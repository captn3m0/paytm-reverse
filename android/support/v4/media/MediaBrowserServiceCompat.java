package android.support.v4.media;

import android.app.Service;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.BundleCompat;
import android.support.v4.media.session.MediaSessionCompat.Token;
import android.support.v4.os.ResultReceiver;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public abstract class MediaBrowserServiceCompat
  extends Service
{
  MediaSessionCompat.Token a;
  private final ArrayMap<IBinder, ConnectionRecord> b = new ArrayMap();
  private final ServiceHandler c = new ServiceHandler(null);
  
  private void a(String paramString, ConnectionRecord paramConnectionRecord, Bundle paramBundle)
  {
    Object localObject2 = (List)paramConnectionRecord.e.get(paramString);
    Object localObject1 = localObject2;
    if (localObject2 == null) {
      localObject1 = new ArrayList();
    }
    localObject2 = ((List)localObject1).iterator();
    while (((Iterator)localObject2).hasNext()) {
      if (MediaBrowserCompatUtils.a(paramBundle, (Bundle)((Iterator)localObject2).next())) {
        return;
      }
    }
    ((List)localObject1).add(paramBundle);
    paramConnectionRecord.e.put(paramString, localObject1);
    c(paramString, paramConnectionRecord, paramBundle);
  }
  
  private void a(String paramString, final ResultReceiver paramResultReceiver)
  {
    paramResultReceiver = new Result(paramString)
    {
      void a(MediaBrowserCompat.MediaItem paramAnonymousMediaItem, int paramAnonymousInt)
      {
        Bundle localBundle = new Bundle();
        localBundle.putParcelable("media_item", paramAnonymousMediaItem);
        paramResultReceiver.b(0, localBundle);
      }
    };
    b(paramString, paramResultReceiver);
    if (!paramResultReceiver.a()) {
      throw new IllegalStateException("onLoadItem must call detach() or sendResult() before returning for id=" + paramString);
    }
  }
  
  private boolean a(String paramString, int paramInt)
  {
    if (paramString == null) {}
    for (;;)
    {
      return false;
      String[] arrayOfString = getPackageManager().getPackagesForUid(paramInt);
      int i = arrayOfString.length;
      paramInt = 0;
      while (paramInt < i)
      {
        if (arrayOfString[paramInt].equals(paramString)) {
          return true;
        }
        paramInt += 1;
      }
    }
  }
  
  private boolean b(String paramString, ConnectionRecord paramConnectionRecord, Bundle paramBundle)
  {
    boolean bool2 = false;
    boolean bool3 = false;
    List localList = (List)paramConnectionRecord.e.get(paramString);
    if (localList != null)
    {
      Iterator localIterator = localList.iterator();
      Bundle localBundle;
      do
      {
        bool1 = bool3;
        if (!localIterator.hasNext()) {
          break;
        }
        localBundle = (Bundle)localIterator.next();
      } while (!MediaBrowserCompatUtils.a(paramBundle, localBundle));
      boolean bool1 = true;
      localList.remove(localBundle);
      bool2 = bool1;
      if (localList.size() == 0)
      {
        paramConnectionRecord.e.remove(paramString);
        bool2 = bool1;
      }
    }
    return bool2;
  }
  
  private void c(final String paramString, final ConnectionRecord paramConnectionRecord, final Bundle paramBundle)
  {
    Result local3 = new Result(paramString)
    {
      void a(List<MediaBrowserCompat.MediaItem> paramAnonymousList, int paramAnonymousInt)
      {
        if (MediaBrowserServiceCompat.b(MediaBrowserServiceCompat.this).get(paramConnectionRecord.c.a()) != paramConnectionRecord) {
          return;
        }
        if ((paramAnonymousInt & 0x1) != 0) {
          paramAnonymousList = MediaBrowserCompatUtils.a(paramAnonymousList, paramBundle);
        }
        for (;;)
        {
          try
          {
            paramConnectionRecord.c.a(paramString, paramAnonymousList, paramBundle);
            return;
          }
          catch (RemoteException paramAnonymousList)
          {
            Log.w("MediaBrowserServiceCompat", "Calling onLoadChildren() failed for id=" + paramString + " package=" + paramConnectionRecord.a);
            return;
          }
        }
      }
    };
    if (paramBundle == null) {
      a(paramString, local3);
    }
    while (!local3.a())
    {
      throw new IllegalStateException("onLoadChildren must call detach() or sendResult() before returning for package=" + paramConnectionRecord.a + " id=" + paramString);
      a(paramString, local3, paramBundle);
    }
  }
  
  @Nullable
  public abstract BrowserRoot a(@NonNull String paramString, int paramInt, @Nullable Bundle paramBundle);
  
  public abstract void a(@NonNull String paramString, @NonNull Result<List<MediaBrowserCompat.MediaItem>> paramResult);
  
  public void a(@NonNull String paramString, @NonNull Result<List<MediaBrowserCompat.MediaItem>> paramResult, @NonNull Bundle paramBundle)
  {
    paramResult.a(1);
    a(paramString, paramResult);
  }
  
  public void b(String paramString, Result<MediaBrowserCompat.MediaItem> paramResult)
  {
    paramResult.a(null);
  }
  
  public static final class BrowserRoot
  {
    private final String a;
    private final Bundle b;
    
    public String a()
    {
      return this.a;
    }
    
    public Bundle b()
    {
      return this.b;
    }
  }
  
  private class ConnectionRecord
  {
    String a;
    Bundle b;
    MediaBrowserServiceCompat.ServiceCallbacks c;
    MediaBrowserServiceCompat.BrowserRoot d;
    HashMap<String, List<Bundle>> e = new HashMap();
    
    private ConnectionRecord() {}
  }
  
  static abstract interface MediaBrowserServiceImpl {}
  
  class MediaBrowserServiceImplApi21
    implements MediaBrowserServiceCompat.MediaBrowserServiceImpl
  {}
  
  class MediaBrowserServiceImplApi23
    implements MediaBrowserServiceCompat.MediaBrowserServiceImpl
  {}
  
  class MediaBrowserServiceImplBase
    implements MediaBrowserServiceCompat.MediaBrowserServiceImpl
  {}
  
  public static class Result<T>
  {
    private Object a;
    private boolean b;
    private boolean c;
    private int d;
    
    Result(Object paramObject)
    {
      this.a = paramObject;
    }
    
    void a(int paramInt)
    {
      this.d = paramInt;
    }
    
    public void a(T paramT)
    {
      if (this.c) {
        throw new IllegalStateException("sendResult() called twice for: " + this.a);
      }
      this.c = true;
      a(paramT, this.d);
    }
    
    void a(T paramT, int paramInt) {}
    
    boolean a()
    {
      return (this.b) || (this.c);
    }
  }
  
  private static abstract interface ServiceCallbacks
  {
    public abstract IBinder a();
    
    public abstract void a(String paramString, MediaSessionCompat.Token paramToken, Bundle paramBundle)
      throws RemoteException;
    
    public abstract void a(String paramString, List<MediaBrowserCompat.MediaItem> paramList, Bundle paramBundle)
      throws RemoteException;
    
    public abstract void b()
      throws RemoteException;
  }
  
  private class ServiceCallbacksApi21
    implements MediaBrowserServiceCompat.ServiceCallbacks
  {
    final MediaBrowserServiceCompatApi21.ServiceCallbacks a;
    Messenger b;
    
    ServiceCallbacksApi21(MediaBrowserServiceCompatApi21.ServiceCallbacks paramServiceCallbacks)
    {
      this.a = paramServiceCallbacks;
    }
    
    public IBinder a()
    {
      return this.a.a();
    }
    
    public void a(String paramString, MediaSessionCompat.Token paramToken, Bundle paramBundle)
      throws RemoteException
    {
      Bundle localBundle = paramBundle;
      if (paramBundle == null) {
        localBundle = new Bundle();
      }
      this.b = new Messenger(MediaBrowserServiceCompat.a(MediaBrowserServiceCompat.this));
      BundleCompat.putBinder(localBundle, "extra_messenger", this.b.getBinder());
      localBundle.putInt("extra_service_version", 1);
      this.a.a(paramString, paramToken.a(), localBundle);
    }
    
    public void a(String paramString, List<MediaBrowserCompat.MediaItem> paramList, Bundle paramBundle)
      throws RemoteException
    {
      paramBundle = null;
      if (paramList != null)
      {
        ArrayList localArrayList = new ArrayList();
        paramList = paramList.iterator();
        for (;;)
        {
          paramBundle = localArrayList;
          if (!paramList.hasNext()) {
            break;
          }
          paramBundle = (MediaBrowserCompat.MediaItem)paramList.next();
          Parcel localParcel = Parcel.obtain();
          paramBundle.writeToParcel(localParcel, 0);
          localArrayList.add(localParcel);
        }
      }
      this.a.a(paramString, paramBundle);
    }
    
    public void b()
      throws RemoteException
    {
      this.a.b();
    }
  }
  
  private class ServiceCallbacksCompat
    implements MediaBrowserServiceCompat.ServiceCallbacks
  {
    final Messenger a;
    
    ServiceCallbacksCompat(Messenger paramMessenger)
    {
      this.a = paramMessenger;
    }
    
    private void a(int paramInt, Bundle paramBundle)
      throws RemoteException
    {
      Message localMessage = Message.obtain();
      localMessage.what = paramInt;
      localMessage.arg1 = 1;
      localMessage.setData(paramBundle);
      this.a.send(localMessage);
    }
    
    public IBinder a()
    {
      return this.a.getBinder();
    }
    
    public void a(String paramString, MediaSessionCompat.Token paramToken, Bundle paramBundle)
      throws RemoteException
    {
      Bundle localBundle = paramBundle;
      if (paramBundle == null) {
        localBundle = new Bundle();
      }
      localBundle.putInt("extra_service_version", 1);
      paramBundle = new Bundle();
      paramBundle.putString("data_media_item_id", paramString);
      paramBundle.putParcelable("data_media_session_token", paramToken);
      paramBundle.putBundle("data_root_hints", localBundle);
      a(1, paramBundle);
    }
    
    public void a(String paramString, List<MediaBrowserCompat.MediaItem> paramList, Bundle paramBundle)
      throws RemoteException
    {
      Bundle localBundle = new Bundle();
      localBundle.putString("data_media_item_id", paramString);
      localBundle.putBundle("data_options", paramBundle);
      if (paramList != null) {
        if (!(paramList instanceof ArrayList)) {
          break label57;
        }
      }
      label57:
      for (paramString = (ArrayList)paramList;; paramString = new ArrayList(paramList))
      {
        localBundle.putParcelableArrayList("data_media_item_list", paramString);
        a(3, localBundle);
        return;
      }
    }
    
    public void b()
      throws RemoteException
    {
      a(2, null);
    }
  }
  
  private final class ServiceHandler
    extends Handler
  {
    private final MediaBrowserServiceCompat.ServiceImpl b = new MediaBrowserServiceCompat.ServiceImpl(MediaBrowserServiceCompat.this, null);
    
    private ServiceHandler() {}
    
    public void a(Runnable paramRunnable)
    {
      if (Thread.currentThread() == getLooper().getThread())
      {
        paramRunnable.run();
        return;
      }
      post(paramRunnable);
    }
    
    public void handleMessage(Message paramMessage)
    {
      Bundle localBundle = paramMessage.getData();
      switch (paramMessage.what)
      {
      default: 
        Log.w("MediaBrowserServiceCompat", "Unhandled message: " + paramMessage + "\n  Service version: " + 1 + "\n  Client version: " + paramMessage.arg1);
        return;
      case 1: 
        this.b.a(localBundle.getString("data_package_name"), localBundle.getInt("data_calling_uid"), localBundle.getBundle("data_root_hints"), new MediaBrowserServiceCompat.ServiceCallbacksCompat(MediaBrowserServiceCompat.this, paramMessage.replyTo));
        return;
      case 2: 
        this.b.a(new MediaBrowserServiceCompat.ServiceCallbacksCompat(MediaBrowserServiceCompat.this, paramMessage.replyTo));
        return;
      case 3: 
        this.b.a(localBundle.getString("data_media_item_id"), localBundle.getBundle("data_options"), new MediaBrowserServiceCompat.ServiceCallbacksCompat(MediaBrowserServiceCompat.this, paramMessage.replyTo));
        return;
      case 4: 
        this.b.b(localBundle.getString("data_media_item_id"), localBundle.getBundle("data_options"), new MediaBrowserServiceCompat.ServiceCallbacksCompat(MediaBrowserServiceCompat.this, paramMessage.replyTo));
        return;
      case 5: 
        this.b.a(localBundle.getString("data_media_item_id"), (ResultReceiver)localBundle.getParcelable("data_result_receiver"));
        return;
      case 6: 
        this.b.b(new MediaBrowserServiceCompat.ServiceCallbacksCompat(MediaBrowserServiceCompat.this, paramMessage.replyTo));
        return;
      }
      this.b.c(new MediaBrowserServiceCompat.ServiceCallbacksCompat(MediaBrowserServiceCompat.this, paramMessage.replyTo));
    }
    
    public boolean sendMessageAtTime(Message paramMessage, long paramLong)
    {
      Bundle localBundle = paramMessage.getData();
      localBundle.setClassLoader(MediaBrowserCompat.class.getClassLoader());
      localBundle.putInt("data_calling_uid", Binder.getCallingUid());
      return super.sendMessageAtTime(paramMessage, paramLong);
    }
  }
  
  private class ServiceImpl
  {
    private ServiceImpl() {}
    
    public void a(final MediaBrowserServiceCompat.ServiceCallbacks paramServiceCallbacks)
    {
      MediaBrowserServiceCompat.a(MediaBrowserServiceCompat.this).a(new Runnable()
      {
        public void run()
        {
          IBinder localIBinder = paramServiceCallbacks.a();
          if ((MediaBrowserServiceCompat.ConnectionRecord)MediaBrowserServiceCompat.b(MediaBrowserServiceCompat.this).remove(localIBinder) != null) {}
        }
      });
    }
    
    public void a(final String paramString, final int paramInt, final Bundle paramBundle, final MediaBrowserServiceCompat.ServiceCallbacks paramServiceCallbacks)
    {
      if (!MediaBrowserServiceCompat.a(MediaBrowserServiceCompat.this, paramString, paramInt)) {
        throw new IllegalArgumentException("Package/uid mismatch: uid=" + paramInt + " package=" + paramString);
      }
      MediaBrowserServiceCompat.a(MediaBrowserServiceCompat.this).a(new Runnable()
      {
        public void run()
        {
          IBinder localIBinder = paramServiceCallbacks.a();
          MediaBrowserServiceCompat.b(MediaBrowserServiceCompat.this).remove(localIBinder);
          MediaBrowserServiceCompat.ConnectionRecord localConnectionRecord = new MediaBrowserServiceCompat.ConnectionRecord(MediaBrowserServiceCompat.this, null);
          localConnectionRecord.a = paramString;
          localConnectionRecord.b = paramBundle;
          localConnectionRecord.c = paramServiceCallbacks;
          localConnectionRecord.d = MediaBrowserServiceCompat.this.a(paramString, paramInt, paramBundle);
          if (localConnectionRecord.d == null) {
            Log.i("MediaBrowserServiceCompat", "No root for client " + paramString + " from service " + getClass().getName());
          }
          for (;;)
          {
            try
            {
              paramServiceCallbacks.b();
              return;
            }
            catch (RemoteException localRemoteException1)
            {
              Log.w("MediaBrowserServiceCompat", "Calling onConnectFailed() failed. Ignoring. pkg=" + paramString);
              return;
            }
            try
            {
              MediaBrowserServiceCompat.b(MediaBrowserServiceCompat.this).put(localRemoteException1, localConnectionRecord);
              if (MediaBrowserServiceCompat.this.a != null)
              {
                paramServiceCallbacks.a(localConnectionRecord.d.a(), MediaBrowserServiceCompat.this.a, localConnectionRecord.d.b());
                return;
              }
            }
            catch (RemoteException localRemoteException2)
            {
              Log.w("MediaBrowserServiceCompat", "Calling onConnect() failed. Dropping client. pkg=" + paramString);
              MediaBrowserServiceCompat.b(MediaBrowserServiceCompat.this).remove(localRemoteException1);
            }
          }
        }
      });
    }
    
    public void a(final String paramString, final Bundle paramBundle, final MediaBrowserServiceCompat.ServiceCallbacks paramServiceCallbacks)
    {
      MediaBrowserServiceCompat.a(MediaBrowserServiceCompat.this).a(new Runnable()
      {
        public void run()
        {
          Object localObject = paramServiceCallbacks.a();
          localObject = (MediaBrowserServiceCompat.ConnectionRecord)MediaBrowserServiceCompat.b(MediaBrowserServiceCompat.this).get(localObject);
          if (localObject == null)
          {
            Log.w("MediaBrowserServiceCompat", "addSubscription for callback that isn't registered id=" + paramString);
            return;
          }
          MediaBrowserServiceCompat.a(MediaBrowserServiceCompat.this, paramString, (MediaBrowserServiceCompat.ConnectionRecord)localObject, paramBundle);
        }
      });
    }
    
    public void a(final String paramString, final ResultReceiver paramResultReceiver)
    {
      if ((TextUtils.isEmpty(paramString)) || (paramResultReceiver == null)) {
        return;
      }
      MediaBrowserServiceCompat.a(MediaBrowserServiceCompat.this).a(new Runnable()
      {
        public void run()
        {
          MediaBrowserServiceCompat.a(MediaBrowserServiceCompat.this, paramString, paramResultReceiver);
        }
      });
    }
    
    public void b(final MediaBrowserServiceCompat.ServiceCallbacks paramServiceCallbacks)
    {
      MediaBrowserServiceCompat.a(MediaBrowserServiceCompat.this).a(new Runnable()
      {
        public void run()
        {
          IBinder localIBinder = paramServiceCallbacks.a();
          MediaBrowserServiceCompat.b(MediaBrowserServiceCompat.this).remove(localIBinder);
          MediaBrowserServiceCompat.ConnectionRecord localConnectionRecord = new MediaBrowserServiceCompat.ConnectionRecord(MediaBrowserServiceCompat.this, null);
          localConnectionRecord.c = paramServiceCallbacks;
          MediaBrowserServiceCompat.b(MediaBrowserServiceCompat.this).put(localIBinder, localConnectionRecord);
        }
      });
    }
    
    public void b(final String paramString, final Bundle paramBundle, final MediaBrowserServiceCompat.ServiceCallbacks paramServiceCallbacks)
    {
      MediaBrowserServiceCompat.a(MediaBrowserServiceCompat.this).a(new Runnable()
      {
        public void run()
        {
          Object localObject = paramServiceCallbacks.a();
          localObject = (MediaBrowserServiceCompat.ConnectionRecord)MediaBrowserServiceCompat.b(MediaBrowserServiceCompat.this).get(localObject);
          if (localObject == null) {
            Log.w("MediaBrowserServiceCompat", "removeSubscription for callback that isn't registered id=" + paramString);
          }
          while (MediaBrowserServiceCompat.b(MediaBrowserServiceCompat.this, paramString, (MediaBrowserServiceCompat.ConnectionRecord)localObject, paramBundle)) {
            return;
          }
          Log.w("MediaBrowserServiceCompat", "removeSubscription called for " + paramString + " which is not subscribed");
        }
      });
    }
    
    public void c(final MediaBrowserServiceCompat.ServiceCallbacks paramServiceCallbacks)
    {
      MediaBrowserServiceCompat.a(MediaBrowserServiceCompat.this).a(new Runnable()
      {
        public void run()
        {
          IBinder localIBinder = paramServiceCallbacks.a();
          MediaBrowserServiceCompat.b(MediaBrowserServiceCompat.this).remove(localIBinder);
        }
      });
    }
  }
  
  private class ServiceImplApi21
    implements MediaBrowserServiceCompatApi21.ServiceImplApi21
  {
    final MediaBrowserServiceCompat.ServiceImpl a;
    
    public void a(MediaBrowserServiceCompatApi21.ServiceCallbacks paramServiceCallbacks)
    {
      this.a.a(new MediaBrowserServiceCompat.ServiceCallbacksApi21(this.b, paramServiceCallbacks));
    }
    
    public void a(String paramString, Bundle paramBundle, MediaBrowserServiceCompatApi21.ServiceCallbacks paramServiceCallbacks)
    {
      this.a.a(paramString, Binder.getCallingUid(), paramBundle, new MediaBrowserServiceCompat.ServiceCallbacksApi21(this.b, paramServiceCallbacks));
    }
    
    public void a(String paramString, MediaBrowserServiceCompatApi21.ServiceCallbacks paramServiceCallbacks)
    {
      this.a.a(paramString, null, new MediaBrowserServiceCompat.ServiceCallbacksApi21(this.b, paramServiceCallbacks));
    }
    
    public void b(String paramString, MediaBrowserServiceCompatApi21.ServiceCallbacks paramServiceCallbacks)
    {
      this.a.b(paramString, null, new MediaBrowserServiceCompat.ServiceCallbacksApi21(this.b, paramServiceCallbacks));
    }
  }
  
  private class ServiceImplApi23
    extends MediaBrowserServiceCompat.ServiceImplApi21
    implements MediaBrowserServiceCompatApi23.ServiceImplApi23
  {
    public void a(String paramString, final MediaBrowserServiceCompatApi23.ItemCallback paramItemCallback)
    {
      paramItemCallback = new ResultReceiver(MediaBrowserServiceCompat.a(this.c))
      {
        protected void a(int paramAnonymousInt, Bundle paramAnonymousBundle)
        {
          MediaBrowserCompat.MediaItem localMediaItem = (MediaBrowserCompat.MediaItem)paramAnonymousBundle.getParcelable("media_item");
          Parcel localParcel = null;
          if (localMediaItem != null)
          {
            localParcel = Parcel.obtain();
            localMediaItem.writeToParcel(localParcel, 0);
          }
          paramItemCallback.a(paramAnonymousInt, paramAnonymousBundle, localParcel);
        }
      };
      this.a.a(paramString, paramItemCallback);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/media/MediaBrowserServiceCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */