package android.support.v4.media;

import android.media.browse.MediaBrowser;
import android.media.browse.MediaBrowser.ConnectionCallback;
import android.media.browse.MediaBrowser.MediaItem;
import android.media.browse.MediaBrowser.SubscriptionCallback;
import android.os.Bundle;
import android.os.Parcel;
import android.support.annotation.NonNull;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class MediaBrowserCompatApi21
{
  public static Bundle a(Object paramObject)
  {
    return ((MediaBrowser)paramObject).getExtras();
  }
  
  public static Object a(ConnectionCallback paramConnectionCallback)
  {
    return new ConnectionCallbackProxy(paramConnectionCallback);
  }
  
  public static void a(Object paramObject1, String paramString, Object paramObject2)
  {
    ((MediaBrowser)paramObject1).subscribe(paramString, (MediaBrowser.SubscriptionCallback)paramObject2);
  }
  
  static abstract interface ConnectionCallback
  {
    public abstract void a();
    
    public abstract void b();
    
    public abstract void c();
  }
  
  static class ConnectionCallbackProxy<T extends MediaBrowserCompatApi21.ConnectionCallback>
    extends MediaBrowser.ConnectionCallback
  {
    protected final T a;
    
    public ConnectionCallbackProxy(T paramT)
    {
      this.a = paramT;
    }
    
    public void onConnected()
    {
      this.a.a();
    }
    
    public void onConnectionFailed()
    {
      this.a.c();
    }
    
    public void onConnectionSuspended()
    {
      this.a.b();
    }
  }
  
  static abstract interface SubscriptionCallback
  {
    public abstract void a(@NonNull String paramString);
    
    public abstract void a(@NonNull String paramString, List<Parcel> paramList);
  }
  
  static class SubscriptionCallbackProxy<T extends MediaBrowserCompatApi21.SubscriptionCallback>
    extends MediaBrowser.SubscriptionCallback
  {
    protected final T a;
    
    public void onChildrenLoaded(@NonNull String paramString, List<MediaBrowser.MediaItem> paramList)
    {
      ArrayList localArrayList = null;
      Object localObject = paramList;
      if (paramList != null)
      {
        localObject = paramList;
        if (paramList.size() == 1)
        {
          localObject = paramList;
          if (((MediaBrowser.MediaItem)paramList.get(0)).getMediaId().equals("android.support.v4.media.MediaBrowserCompat.NULL_MEDIA_ITEM")) {
            localObject = null;
          }
        }
      }
      paramList = localArrayList;
      if (localObject != null)
      {
        localArrayList = new ArrayList();
        localObject = ((List)localObject).iterator();
        for (;;)
        {
          paramList = localArrayList;
          if (!((Iterator)localObject).hasNext()) {
            break;
          }
          paramList = (MediaBrowser.MediaItem)((Iterator)localObject).next();
          Parcel localParcel = Parcel.obtain();
          paramList.writeToParcel(localParcel, 0);
          localArrayList.add(localParcel);
        }
      }
      this.a.a(paramString, paramList);
    }
    
    public void onError(@NonNull String paramString)
    {
      this.a.a(paramString);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/media/MediaBrowserCompatApi21.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */