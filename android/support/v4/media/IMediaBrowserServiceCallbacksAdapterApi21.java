package android.support.v4.media;

import android.media.session.MediaSession.Token;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class IMediaBrowserServiceCallbacksAdapterApi21
{
  Object a;
  private Method b;
  private Method c;
  private Method d;
  private Method e;
  
  IMediaBrowserServiceCallbacksAdapterApi21(Object paramObject)
  {
    this.a = paramObject;
    try
    {
      paramObject = Class.forName("android.service.media.IMediaBrowserServiceCallbacks");
      Class localClass = Class.forName("android.content.pm.ParceledListSlice");
      this.b = ((Class)paramObject).getMethod("asBinder", new Class[0]);
      this.c = ((Class)paramObject).getMethod("onConnect", new Class[] { String.class, MediaSession.Token.class, Bundle.class });
      this.d = ((Class)paramObject).getMethod("onConnectFailed", new Class[0]);
      this.e = ((Class)paramObject).getMethod("onLoadChildren", new Class[] { String.class, localClass });
      return;
    }
    catch (ClassNotFoundException paramObject)
    {
      ((ReflectiveOperationException)paramObject).printStackTrace();
      return;
    }
    catch (NoSuchMethodException paramObject)
    {
      for (;;) {}
    }
  }
  
  IBinder a()
  {
    try
    {
      IBinder localIBinder = (IBinder)this.b.invoke(this.a, new Object[0]);
      return localIBinder;
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      localIllegalAccessException.printStackTrace();
      return null;
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      for (;;) {}
    }
  }
  
  void a(String paramString, Object paramObject)
    throws RemoteException
  {
    try
    {
      this.e.invoke(this.a, new Object[] { paramString, paramObject });
      return;
    }
    catch (IllegalAccessException paramString)
    {
      paramString.printStackTrace();
      return;
    }
    catch (InvocationTargetException paramString)
    {
      for (;;) {}
    }
  }
  
  void a(String paramString, Object paramObject, Bundle paramBundle)
    throws RemoteException
  {
    try
    {
      this.c.invoke(this.a, new Object[] { paramString, paramObject, paramBundle });
      return;
    }
    catch (IllegalAccessException paramString)
    {
      paramString.printStackTrace();
      return;
    }
    catch (InvocationTargetException paramString)
    {
      for (;;) {}
    }
  }
  
  void b()
    throws RemoteException
  {
    try
    {
      this.d.invoke(this.a, new Object[0]);
      return;
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      localIllegalAccessException.printStackTrace();
      return;
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      for (;;) {}
    }
  }
  
  static class Stub
  {
    static Method a;
    
    static
    {
      try
      {
        a = Class.forName("android.service.media.IMediaBrowserServiceCallbacks$Stub").getMethod("asInterface", new Class[] { IBinder.class });
        return;
      }
      catch (ClassNotFoundException localClassNotFoundException)
      {
        localClassNotFoundException.printStackTrace();
        return;
      }
      catch (NoSuchMethodException localNoSuchMethodException)
      {
        for (;;) {}
      }
    }
    
    static Object a(IBinder paramIBinder)
    {
      try
      {
        paramIBinder = a.invoke(null, new Object[] { paramIBinder });
        return paramIBinder;
      }
      catch (IllegalAccessException paramIBinder)
      {
        paramIBinder.printStackTrace();
        return null;
      }
      catch (InvocationTargetException paramIBinder)
      {
        for (;;) {}
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/media/IMediaBrowserServiceCallbacksAdapterApi21.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */