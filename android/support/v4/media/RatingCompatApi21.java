package android.support.v4.media;

import android.media.Rating;

class RatingCompatApi21
{
  public static boolean a(Object paramObject)
  {
    return ((Rating)paramObject).isRated();
  }
  
  public static int b(Object paramObject)
  {
    return ((Rating)paramObject).getRatingStyle();
  }
  
  public static boolean c(Object paramObject)
  {
    return ((Rating)paramObject).hasHeart();
  }
  
  public static boolean d(Object paramObject)
  {
    return ((Rating)paramObject).isThumbUp();
  }
  
  public static float e(Object paramObject)
  {
    return ((Rating)paramObject).getStarRating();
  }
  
  public static float f(Object paramObject)
  {
    return ((Rating)paramObject).getPercentRating();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/media/RatingCompatApi21.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */