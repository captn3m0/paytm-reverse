package android.support.v4.media;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.v4.app.BundleCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.MediaSessionCompat.Token;
import android.support.v4.os.ResultReceiver;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

public final class MediaBrowserCompat
{
  private static class CallbackHandler
    extends Handler
  {
    private final MediaBrowserCompat.MediaBrowserServiceCallbackImpl a;
    private WeakReference<Messenger> b;
    
    void a(Messenger paramMessenger)
    {
      this.b = new WeakReference(paramMessenger);
    }
    
    public void handleMessage(Message paramMessage)
    {
      if (this.b == null) {
        return;
      }
      Bundle localBundle = paramMessage.getData();
      localBundle.setClassLoader(MediaSessionCompat.class.getClassLoader());
      switch (paramMessage.what)
      {
      default: 
        Log.w("MediaBrowserCompat", "Unhandled message: " + paramMessage + "\n  Client version: " + 1 + "\n  Service version: " + paramMessage.arg1);
        return;
      case 1: 
        this.a.a((Messenger)this.b.get(), localBundle.getString("data_media_item_id"), (MediaSessionCompat.Token)localBundle.getParcelable("data_media_session_token"), localBundle.getBundle("data_root_hints"));
        return;
      case 2: 
        this.a.a((Messenger)this.b.get());
        return;
      }
      this.a.a((Messenger)this.b.get(), localBundle.getString("data_media_item_id"), localBundle.getParcelableArrayList("data_media_item_list"), localBundle.getBundle("data_options"));
    }
  }
  
  public static class ConnectionCallback
  {
    final Object a;
    private ConnectionCallbackInternal b;
    
    public ConnectionCallback()
    {
      if (Build.VERSION.SDK_INT >= 21)
      {
        this.a = MediaBrowserCompatApi21.a(new StubApi21(null));
        return;
      }
      this.a = null;
    }
    
    public void a() {}
    
    public void b() {}
    
    public void c() {}
    
    static abstract interface ConnectionCallbackInternal
    {
      public abstract void a();
      
      public abstract void b();
      
      public abstract void c();
    }
    
    private class StubApi21
      implements MediaBrowserCompatApi21.ConnectionCallback
    {
      private StubApi21() {}
      
      public void a()
      {
        if (MediaBrowserCompat.ConnectionCallback.a(MediaBrowserCompat.ConnectionCallback.this) != null) {
          MediaBrowserCompat.ConnectionCallback.a(MediaBrowserCompat.ConnectionCallback.this).a();
        }
        MediaBrowserCompat.ConnectionCallback.this.a();
      }
      
      public void b()
      {
        if (MediaBrowserCompat.ConnectionCallback.a(MediaBrowserCompat.ConnectionCallback.this) != null) {
          MediaBrowserCompat.ConnectionCallback.a(MediaBrowserCompat.ConnectionCallback.this).b();
        }
        MediaBrowserCompat.ConnectionCallback.this.b();
      }
      
      public void c()
      {
        if (MediaBrowserCompat.ConnectionCallback.a(MediaBrowserCompat.ConnectionCallback.this) != null) {
          MediaBrowserCompat.ConnectionCallback.a(MediaBrowserCompat.ConnectionCallback.this).c();
        }
        MediaBrowserCompat.ConnectionCallback.this.c();
      }
    }
  }
  
  public static abstract class ItemCallback
  {
    final Object a;
    
    public ItemCallback()
    {
      if (Build.VERSION.SDK_INT >= 23)
      {
        this.a = MediaBrowserCompatApi23.a(new StubApi23(null));
        return;
      }
      this.a = null;
    }
    
    public void a(MediaBrowserCompat.MediaItem paramMediaItem) {}
    
    public void a(@NonNull String paramString) {}
    
    private class StubApi23
      implements MediaBrowserCompatApi23.ItemCallback
    {
      private StubApi23() {}
      
      public void a(Parcel paramParcel)
      {
        paramParcel.setDataPosition(0);
        MediaBrowserCompat.MediaItem localMediaItem = (MediaBrowserCompat.MediaItem)MediaBrowserCompat.MediaItem.CREATOR.createFromParcel(paramParcel);
        paramParcel.recycle();
        MediaBrowserCompat.ItemCallback.this.a(localMediaItem);
      }
      
      public void a(@NonNull String paramString)
      {
        MediaBrowserCompat.ItemCallback.this.a(paramString);
      }
    }
  }
  
  private static class ItemReceiver
    extends ResultReceiver
  {
    private final String a;
    private final MediaBrowserCompat.ItemCallback b;
    
    protected void a(int paramInt, Bundle paramBundle)
    {
      paramBundle.setClassLoader(MediaBrowserCompat.class.getClassLoader());
      if ((paramInt != 0) || (paramBundle == null) || (!paramBundle.containsKey("media_item")))
      {
        this.b.a(this.a);
        return;
      }
      paramBundle = paramBundle.getParcelable("media_item");
      if ((paramBundle instanceof MediaBrowserCompat.MediaItem))
      {
        this.b.a((MediaBrowserCompat.MediaItem)paramBundle);
        return;
      }
      this.b.a(this.a);
    }
  }
  
  static abstract interface MediaBrowserImpl {}
  
  static class MediaBrowserImplApi21
    implements MediaBrowserCompat.ConnectionCallback.ConnectionCallbackInternal, MediaBrowserCompat.MediaBrowserImpl, MediaBrowserCompat.MediaBrowserServiceCallbackImpl
  {
    protected Object a;
    private final MediaBrowserCompat.CallbackHandler b;
    private final ArrayMap<String, MediaBrowserCompat.Subscription> c;
    private MediaBrowserCompat.ServiceBinderWrapper d;
    private Messenger e;
    
    public void a()
    {
      Object localObject = MediaBrowserCompatApi21.a(this.a);
      if (localObject == null) {}
      do
      {
        return;
        localObject = BundleCompat.getBinder((Bundle)localObject, "extra_messenger");
      } while (localObject == null);
      this.d = new MediaBrowserCompat.ServiceBinderWrapper((IBinder)localObject);
      this.e = new Messenger(this.b);
      this.b.a(this.e);
      try
      {
        this.d.a(this.e);
        a(this.e, null, null, null);
        return;
      }
      catch (RemoteException localRemoteException)
      {
        for (;;)
        {
          Log.i("MediaBrowserCompat", "Remote error registering client messenger.");
        }
      }
    }
    
    public void a(Messenger paramMessenger) {}
    
    public void a(Messenger paramMessenger, String paramString, MediaSessionCompat.Token paramToken, Bundle paramBundle)
    {
      paramMessenger = this.c.entrySet().iterator();
      if (paramMessenger.hasNext())
      {
        paramToken = (Map.Entry)paramMessenger.next();
        paramString = (String)paramToken.getKey();
        paramBundle = (MediaBrowserCompat.Subscription)paramToken.getValue();
        paramToken = paramBundle.a();
        paramBundle = paramBundle.b();
        int i = 0;
        label69:
        if (i < paramToken.size())
        {
          if (paramToken.get(i) != null) {
            break label123;
          }
          MediaBrowserCompatApi21.a(this.a, paramString, MediaBrowserCompat.SubscriptionCallbackApi21.b((MediaBrowserCompat.SubscriptionCallbackApi21)paramBundle.get(i)));
        }
        for (;;)
        {
          i += 1;
          break label69;
          break;
          try
          {
            label123:
            this.d.a(paramString, (Bundle)paramToken.get(i), this.e);
          }
          catch (RemoteException localRemoteException)
          {
            Log.d("MediaBrowserCompat", "addSubscription failed with RemoteException parentId=" + paramString);
          }
        }
      }
    }
    
    public void a(Messenger paramMessenger, String paramString, List paramList, @NonNull Bundle paramBundle)
    {
      if (this.e != paramMessenger) {}
      do
      {
        return;
        paramMessenger = (MediaBrowserCompat.Subscription)this.c.get(paramString);
      } while (paramMessenger == null);
      paramMessenger.a(paramBundle).a(paramString, paramList, paramBundle);
    }
    
    public void b()
    {
      this.d = null;
      this.e = null;
      this.b.a(null);
    }
    
    public void c() {}
  }
  
  static class MediaBrowserImplApi23
    extends MediaBrowserCompat.MediaBrowserImplApi21
  {}
  
  static class MediaBrowserImplBase
    implements MediaBrowserCompat.MediaBrowserImpl, MediaBrowserCompat.MediaBrowserServiceCallbackImpl
  {
    private final Context a;
    private final ComponentName b;
    private final MediaBrowserCompat.ConnectionCallback c;
    private final Bundle d;
    private final MediaBrowserCompat.CallbackHandler e;
    private final ArrayMap<String, MediaBrowserCompat.Subscription> f;
    private int g;
    private MediaServiceConnection h;
    private MediaBrowserCompat.ServiceBinderWrapper i;
    private Messenger j;
    private String k;
    private MediaSessionCompat.Token l;
    private Bundle m;
    
    private static String a(int paramInt)
    {
      switch (paramInt)
      {
      default: 
        return "UNKNOWN/" + paramInt;
      case 0: 
        return "CONNECT_STATE_DISCONNECTED";
      case 1: 
        return "CONNECT_STATE_CONNECTING";
      case 2: 
        return "CONNECT_STATE_CONNECTED";
      }
      return "CONNECT_STATE_SUSPENDED";
    }
    
    private void a()
    {
      if (this.h != null) {
        this.a.unbindService(this.h);
      }
      this.g = 0;
      this.h = null;
      this.i = null;
      this.j = null;
      this.e.a(null);
      this.k = null;
      this.l = null;
    }
    
    private boolean a(Messenger paramMessenger, String paramString)
    {
      if (this.j != paramMessenger)
      {
        if (this.g != 0) {
          Log.i("MediaBrowserCompat", paramString + " for " + this.b + " with mCallbacksMessenger=" + this.j + " this=" + this);
        }
        return false;
      }
      return true;
    }
    
    public void a(Messenger paramMessenger)
    {
      Log.e("MediaBrowserCompat", "onConnectFailed for " + this.b);
      if (!a(paramMessenger, "onConnectFailed")) {
        return;
      }
      if (this.g != 1)
      {
        Log.w("MediaBrowserCompat", "onConnect from service while mState=" + a(this.g) + "... ignoring");
        return;
      }
      a();
      this.c.c();
    }
    
    public void a(Messenger paramMessenger, String paramString, MediaSessionCompat.Token paramToken, Bundle paramBundle)
    {
      if (!a(paramMessenger, "onConnect")) {}
      for (;;)
      {
        return;
        if (this.g != 1)
        {
          Log.w("MediaBrowserCompat", "onConnect from service while mState=" + a(this.g) + "... ignoring");
          return;
        }
        this.k = paramString;
        this.l = paramToken;
        this.m = paramBundle;
        this.g = 2;
        this.c.a();
        try
        {
          paramMessenger = this.f.entrySet().iterator();
          while (paramMessenger.hasNext())
          {
            paramToken = (Map.Entry)paramMessenger.next();
            paramString = (String)paramToken.getKey();
            paramToken = ((MediaBrowserCompat.Subscription)paramToken.getValue()).a().iterator();
            while (paramToken.hasNext())
            {
              paramBundle = (Bundle)paramToken.next();
              this.i.a(paramString, paramBundle, this.j);
            }
          }
          return;
        }
        catch (RemoteException paramMessenger)
        {
          Log.d("MediaBrowserCompat", "addSubscription failed with RemoteException.");
        }
      }
    }
    
    public void a(Messenger paramMessenger, String paramString, List paramList, Bundle paramBundle)
    {
      if (!a(paramMessenger, "onLoadChildren")) {}
      do
      {
        do
        {
          return;
          paramMessenger = (MediaBrowserCompat.Subscription)this.f.get(paramString);
        } while (paramMessenger == null);
        paramMessenger = paramMessenger.a(paramBundle);
      } while (paramMessenger == null);
      if (paramBundle == null)
      {
        paramMessenger.a(paramString, paramList);
        return;
      }
      paramMessenger.a(paramString, paramList, paramBundle);
    }
    
    private class MediaServiceConnection
      implements ServiceConnection
    {
      private void a(Runnable paramRunnable)
      {
        if (Thread.currentThread() == MediaBrowserCompat.MediaBrowserImplBase.d(this.a).getLooper().getThread())
        {
          paramRunnable.run();
          return;
        }
        MediaBrowserCompat.MediaBrowserImplBase.d(this.a).post(paramRunnable);
      }
      
      private boolean a(String paramString)
      {
        if (MediaBrowserCompat.MediaBrowserImplBase.a(this.a) != this)
        {
          if (MediaBrowserCompat.MediaBrowserImplBase.j(this.a) != 0) {
            Log.i("MediaBrowserCompat", paramString + " for " + MediaBrowserCompat.MediaBrowserImplBase.i(this.a) + " with mServiceConnection=" + MediaBrowserCompat.MediaBrowserImplBase.a(this.a) + " this=" + this);
          }
          return false;
        }
        return true;
      }
      
      public void onServiceConnected(final ComponentName paramComponentName, final IBinder paramIBinder)
      {
        a(new Runnable()
        {
          public void run()
          {
            if (!MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.a(MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.this, "onServiceConnected")) {
              return;
            }
            MediaBrowserCompat.MediaBrowserImplBase.a(MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.this.a, new MediaBrowserCompat.ServiceBinderWrapper(paramIBinder));
            MediaBrowserCompat.MediaBrowserImplBase.a(MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.this.a, new Messenger(MediaBrowserCompat.MediaBrowserImplBase.d(MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.this.a)));
            MediaBrowserCompat.MediaBrowserImplBase.d(MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.this.a).a(MediaBrowserCompat.MediaBrowserImplBase.e(MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.this.a));
            MediaBrowserCompat.MediaBrowserImplBase.a(MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.this.a, 1);
            try
            {
              MediaBrowserCompat.MediaBrowserImplBase.h(MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.this.a).a(MediaBrowserCompat.MediaBrowserImplBase.f(MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.this.a), MediaBrowserCompat.MediaBrowserImplBase.g(MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.this.a), MediaBrowserCompat.MediaBrowserImplBase.e(MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.this.a));
              return;
            }
            catch (RemoteException localRemoteException)
            {
              Log.w("MediaBrowserCompat", "RemoteException during connect for " + MediaBrowserCompat.MediaBrowserImplBase.i(MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.this.a));
            }
          }
        });
      }
      
      public void onServiceDisconnected(final ComponentName paramComponentName)
      {
        a(new Runnable()
        {
          public void run()
          {
            if (!MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.a(MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.this, "onServiceDisconnected")) {
              return;
            }
            MediaBrowserCompat.MediaBrowserImplBase.a(MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.this.a, null);
            MediaBrowserCompat.MediaBrowserImplBase.a(MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.this.a, null);
            MediaBrowserCompat.MediaBrowserImplBase.d(MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.this.a).a(null);
            MediaBrowserCompat.MediaBrowserImplBase.a(MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.this.a, 3);
            MediaBrowserCompat.MediaBrowserImplBase.c(MediaBrowserCompat.MediaBrowserImplBase.MediaServiceConnection.this.a).b();
          }
        });
      }
    }
  }
  
  static abstract interface MediaBrowserServiceCallbackImpl
  {
    public abstract void a(Messenger paramMessenger);
    
    public abstract void a(Messenger paramMessenger, String paramString, MediaSessionCompat.Token paramToken, Bundle paramBundle);
    
    public abstract void a(Messenger paramMessenger, String paramString, List paramList, Bundle paramBundle);
  }
  
  public static class MediaItem
    implements Parcelable
  {
    public static final Parcelable.Creator<MediaItem> CREATOR = new Parcelable.Creator()
    {
      public MediaBrowserCompat.MediaItem a(Parcel paramAnonymousParcel)
      {
        return new MediaBrowserCompat.MediaItem(paramAnonymousParcel, null);
      }
      
      public MediaBrowserCompat.MediaItem[] a(int paramAnonymousInt)
      {
        return new MediaBrowserCompat.MediaItem[paramAnonymousInt];
      }
    };
    private final int a;
    private final MediaDescriptionCompat b;
    
    private MediaItem(Parcel paramParcel)
    {
      this.a = paramParcel.readInt();
      this.b = ((MediaDescriptionCompat)MediaDescriptionCompat.CREATOR.createFromParcel(paramParcel));
    }
    
    public int describeContents()
    {
      return 0;
    }
    
    public String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder("MediaItem{");
      localStringBuilder.append("mFlags=").append(this.a);
      localStringBuilder.append(", mDescription=").append(this.b);
      localStringBuilder.append('}');
      return localStringBuilder.toString();
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      paramParcel.writeInt(this.a);
      this.b.writeToParcel(paramParcel, paramInt);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface Flags {}
  }
  
  private static class ServiceBinderWrapper
  {
    private Messenger a;
    
    public ServiceBinderWrapper(IBinder paramIBinder)
    {
      this.a = new Messenger(paramIBinder);
    }
    
    private void a(int paramInt, Bundle paramBundle, Messenger paramMessenger)
      throws RemoteException
    {
      Message localMessage = Message.obtain();
      localMessage.what = paramInt;
      localMessage.arg1 = 1;
      localMessage.setData(paramBundle);
      localMessage.replyTo = paramMessenger;
      this.a.send(localMessage);
    }
    
    void a(Context paramContext, Bundle paramBundle, Messenger paramMessenger)
      throws RemoteException
    {
      Bundle localBundle = new Bundle();
      localBundle.putString("data_package_name", paramContext.getPackageName());
      localBundle.putBundle("data_root_hints", paramBundle);
      a(1, localBundle, paramMessenger);
    }
    
    void a(Messenger paramMessenger)
      throws RemoteException
    {
      a(6, null, paramMessenger);
    }
    
    void a(String paramString, Bundle paramBundle, Messenger paramMessenger)
      throws RemoteException
    {
      Bundle localBundle = new Bundle();
      localBundle.putString("data_media_item_id", paramString);
      localBundle.putBundle("data_options", paramBundle);
      a(3, localBundle, paramMessenger);
    }
  }
  
  private static class Subscription
  {
    private final List<MediaBrowserCompat.SubscriptionCallback> a = new ArrayList();
    private final List<Bundle> b = new ArrayList();
    
    public MediaBrowserCompat.SubscriptionCallback a(Bundle paramBundle)
    {
      int i = 0;
      while (i < this.b.size())
      {
        if (MediaBrowserCompatUtils.a((Bundle)this.b.get(i), paramBundle)) {
          return (MediaBrowserCompat.SubscriptionCallback)this.a.get(i);
        }
        i += 1;
      }
      return null;
    }
    
    public List<Bundle> a()
    {
      return this.b;
    }
    
    public List<MediaBrowserCompat.SubscriptionCallback> b()
    {
      return this.a;
    }
  }
  
  public static abstract class SubscriptionCallback
  {
    public void a(@NonNull String paramString) {}
    
    public void a(@NonNull String paramString, @NonNull Bundle paramBundle) {}
    
    public void a(@NonNull String paramString, List<MediaBrowserCompat.MediaItem> paramList) {}
    
    public void a(@NonNull String paramString, List<MediaBrowserCompat.MediaItem> paramList, @NonNull Bundle paramBundle) {}
  }
  
  static class SubscriptionCallbackApi21
    extends MediaBrowserCompat.SubscriptionCallback
  {
    MediaBrowserCompat.SubscriptionCallback a;
    private final Object b;
    private Bundle c;
    
    public void a(@NonNull String paramString)
    {
      this.a.a(paramString);
    }
    
    public void a(@NonNull String paramString, @NonNull Bundle paramBundle)
    {
      this.a.a(paramString, paramBundle);
    }
    
    public void a(@NonNull String paramString, List<MediaBrowserCompat.MediaItem> paramList)
    {
      this.a.a(paramString, paramList);
    }
    
    public void a(@NonNull String paramString, List<MediaBrowserCompat.MediaItem> paramList, @NonNull Bundle paramBundle)
    {
      this.a.a(paramString, paramList, paramBundle);
    }
    
    private class StubApi21
      implements MediaBrowserCompatApi21.SubscriptionCallback
    {
      public void a(@NonNull String paramString)
      {
        if (MediaBrowserCompat.SubscriptionCallbackApi21.a(this.a) != null)
        {
          this.a.a(paramString, MediaBrowserCompat.SubscriptionCallbackApi21.a(this.a));
          return;
        }
        this.a.a(paramString);
      }
      
      public void a(@NonNull String paramString, List<Parcel> paramList)
      {
        Object localObject = null;
        if (paramList != null)
        {
          ArrayList localArrayList = new ArrayList();
          paramList = paramList.iterator();
          for (;;)
          {
            localObject = localArrayList;
            if (!paramList.hasNext()) {
              break;
            }
            localObject = (Parcel)paramList.next();
            ((Parcel)localObject).setDataPosition(0);
            localArrayList.add(MediaBrowserCompat.MediaItem.CREATOR.createFromParcel((Parcel)localObject));
            ((Parcel)localObject).recycle();
          }
        }
        if (MediaBrowserCompat.SubscriptionCallbackApi21.a(this.a) != null)
        {
          this.a.a(paramString, MediaBrowserCompatUtils.a((List)localObject, MediaBrowserCompat.SubscriptionCallbackApi21.a(this.a)), MediaBrowserCompat.SubscriptionCallbackApi21.a(this.a));
          return;
        }
        this.a.a(paramString, (List)localObject);
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/media/MediaBrowserCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */