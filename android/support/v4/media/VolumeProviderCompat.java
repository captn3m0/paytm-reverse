package android.support.v4.media;

import android.os.Build.VERSION;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract class VolumeProviderCompat
{
  private final int a;
  private final int b;
  private int c;
  private Callback d;
  private Object e;
  
  public VolumeProviderCompat(int paramInt1, int paramInt2, int paramInt3)
  {
    this.a = paramInt1;
    this.b = paramInt2;
    this.c = paramInt3;
  }
  
  public final int a()
  {
    return this.c;
  }
  
  public final void a(int paramInt)
  {
    this.c = paramInt;
    Object localObject = d();
    if (localObject != null) {
      VolumeProviderCompatApi21.a(localObject, paramInt);
    }
    if (this.d != null) {
      this.d.a(this);
    }
  }
  
  public void a(Callback paramCallback)
  {
    this.d = paramCallback;
  }
  
  public final int b()
  {
    return this.a;
  }
  
  public void b(int paramInt) {}
  
  public final int c()
  {
    return this.b;
  }
  
  public void c(int paramInt) {}
  
  public Object d()
  {
    if ((this.e != null) || (Build.VERSION.SDK_INT < 21)) {
      return this.e;
    }
    this.e = VolumeProviderCompatApi21.a(this.a, this.b, this.c, new VolumeProviderCompatApi21.Delegate()
    {
      public void a(int paramAnonymousInt)
      {
        VolumeProviderCompat.this.b(paramAnonymousInt);
      }
      
      public void b(int paramAnonymousInt)
      {
        VolumeProviderCompat.this.c(paramAnonymousInt);
      }
    });
    return this.e;
  }
  
  public static abstract class Callback
  {
    public abstract void a(VolumeProviderCompat paramVolumeProviderCompat);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ControlType {}
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/media/VolumeProviderCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */