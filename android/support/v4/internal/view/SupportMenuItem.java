package android.support.v4.internal.view;

import android.support.v4.view.ActionProvider;
import android.support.v4.view.MenuItemCompat.OnActionExpandListener;
import android.view.MenuItem;
import android.view.View;

public abstract interface SupportMenuItem
  extends MenuItem
{
  public abstract SupportMenuItem a(ActionProvider paramActionProvider);
  
  public abstract SupportMenuItem a(MenuItemCompat.OnActionExpandListener paramOnActionExpandListener);
  
  public abstract ActionProvider a();
  
  public abstract boolean collapseActionView();
  
  public abstract boolean expandActionView();
  
  public abstract View getActionView();
  
  public abstract boolean isActionViewExpanded();
  
  public abstract MenuItem setActionView(int paramInt);
  
  public abstract MenuItem setActionView(View paramView);
  
  public abstract void setShowAsAction(int paramInt);
  
  public abstract MenuItem setShowAsActionFlags(int paramInt);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/android/support/v4/internal/view/SupportMenuItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */