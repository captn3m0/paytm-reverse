package io.a.a.a;

import android.content.Context;
import io.a.a.a.a.b.o;
import io.a.a.a.a.c.l;
import java.io.File;
import java.util.Collection;

public abstract class h<Result>
  implements Comparable<h>
{
  c a;
  g<Result> b = new g(this);
  Context c;
  e<Result> d;
  o e;
  
  public c A()
  {
    return this.a;
  }
  
  public String B()
  {
    return ".Fabric" + File.separator + c();
  }
  
  boolean C()
  {
    return (io.a.a.a.a.c.d)getClass().getAnnotation(io.a.a.a.a.c.d.class) != null;
  }
  
  protected Collection<l> D()
  {
    return this.b.c();
  }
  
  public int a(h paramh)
  {
    if (b(paramh)) {}
    do
    {
      return 1;
      if (paramh.b(this)) {
        return -1;
      }
    } while ((C()) && (!paramh.C()));
    if ((!C()) && (paramh.C())) {
      return -1;
    }
    return 0;
  }
  
  void a(Context paramContext, c paramc, e<Result> parame, o paramo)
  {
    this.a = paramc;
    this.c = new d(paramContext, c(), B());
    this.d = parame;
    this.e = paramo;
  }
  
  protected void a(Result paramResult) {}
  
  protected boolean a()
  {
    return true;
  }
  
  protected void b(Result paramResult) {}
  
  boolean b(h paramh)
  {
    Object localObject = (io.a.a.a.a.c.d)getClass().getAnnotation(io.a.a.a.a.c.d.class);
    if (localObject != null)
    {
      localObject = ((io.a.a.a.a.c.d)localObject).a();
      int j = localObject.length;
      int i = 0;
      while (i < j)
      {
        if (localObject[i].equals(paramh.getClass())) {
          return true;
        }
        i += 1;
      }
    }
    return false;
  }
  
  public abstract String c();
  
  public abstract String d();
  
  protected abstract Result w();
  
  final void x()
  {
    this.b.a(this.a.e(), new Void[] { (Void)null });
  }
  
  protected o y()
  {
    return this.e;
  }
  
  public Context z()
  {
    return this.c;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/io/a/a/a/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */