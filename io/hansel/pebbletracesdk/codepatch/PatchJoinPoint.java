package io.hansel.pebbletracesdk.codepatch;

import java.lang.reflect.Method;

public class PatchJoinPoint
{
  private Object[] arguments;
  private Class classOfMethod;
  private Method method;
  private Object target;
  
  public PatchJoinPoint(Class paramClass, Method paramMethod, Object paramObject, Object[] paramArrayOfObject)
  {
    this.classOfMethod = paramClass;
    this.method = paramMethod;
    this.target = paramObject;
    this.arguments = paramArrayOfObject;
  }
  
  public Object[] getArgs()
  {
    return this.arguments;
  }
  
  public Class getClassOfMethod()
  {
    return this.classOfMethod;
  }
  
  public Method getMethod()
  {
    return this.method;
  }
  
  public Object getTarget()
  {
    return this.target;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder().append("\n");
    localStringBuilder.append(this.classOfMethod).append("\n");
    localStringBuilder.append(this.method).append("\n");
    localStringBuilder.append("target: ").append(this.target).append("\n");
    if (this.arguments == null) {
      localStringBuilder.append("arguments: null").append("\n");
    }
    for (;;)
    {
      return localStringBuilder.toString();
      localStringBuilder.append("arguments:[");
      int i = 0;
      while (i < this.arguments.length)
      {
        localStringBuilder.append(" ").append(this.arguments[i]).append(",");
        i += 1;
      }
      localStringBuilder.deleteCharAt(localStringBuilder.length() - 1).append("]");
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/io/hansel/pebbletracesdk/codepatch/PatchJoinPoint.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */