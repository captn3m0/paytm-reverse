package io.hansel.pebbletracesdk.codepatch.patch;

import io.hansel.pebbletracesdk.codepatch.PatchJoinPoint;
import java.lang.reflect.Method;

public abstract class Patch
{
  protected Class classForPatch;
  protected Method methodForPatch;
  protected String methodName;
  protected Class[] methodParameterTypes;
  
  public Patch(Class paramClass, Method paramMethod)
  {
    this.classForPatch = paramClass;
    this.methodForPatch = paramMethod;
    initialize();
  }
  
  public abstract Object apply(PatchJoinPoint paramPatchJoinPoint);
  
  public Class getClassForPatch()
  {
    return this.classForPatch;
  }
  
  public Method getMethodForPatch()
  {
    return this.methodForPatch;
  }
  
  protected void initialize()
  {
    this.methodName = this.methodForPatch.getName();
    this.methodParameterTypes = this.methodForPatch.getParameterTypes();
  }
  
  public abstract boolean isApplicableForMethod(String paramString, Class[] paramArrayOfClass);
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/io/hansel/pebbletracesdk/codepatch/patch/Patch.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */