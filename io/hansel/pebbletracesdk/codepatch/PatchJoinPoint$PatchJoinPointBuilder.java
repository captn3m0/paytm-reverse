package io.hansel.pebbletracesdk.codepatch;

import java.lang.reflect.Method;

public class PatchJoinPoint$PatchJoinPointBuilder
{
  private Object[] arguments;
  private Class classOfMethod;
  private Method method;
  private Object target;
  
  public PatchJoinPointBuilder setArguments(Object[] paramArrayOfObject)
  {
    this.arguments = paramArrayOfObject;
    return this;
  }
  
  public PatchJoinPointBuilder setClassOfMethod(Class paramClass)
  {
    this.classOfMethod = paramClass;
    return this;
  }
  
  public PatchJoinPointBuilder setMethod(Method paramMethod)
  {
    this.method = paramMethod;
    return this;
  }
  
  public PatchJoinPointBuilder setTarget(Object paramObject)
  {
    this.target = paramObject;
    return this;
  }
  
  public PatchJoinPoint toPatchJoinPoint()
  {
    if ((this.classOfMethod != null) && (this.method != null)) {
      return new PatchJoinPoint(this.classOfMethod, this.method, this.target, this.arguments);
    }
    return null;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/io/hansel/pebbletracesdk/codepatch/PatchJoinPoint$PatchJoinPointBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */