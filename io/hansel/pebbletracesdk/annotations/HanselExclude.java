package io.hansel.pebbletracesdk.annotations;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.CLASS)
@Target({java.lang.annotation.ElementType.TYPE, java.lang.annotation.ElementType.METHOD, java.lang.annotation.ElementType.CONSTRUCTOR})
public @interface HanselExclude {}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/io/hansel/pebbletracesdk/annotations/HanselExclude.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */