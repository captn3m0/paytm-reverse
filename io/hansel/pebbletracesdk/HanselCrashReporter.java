package io.hansel.pebbletracesdk;

import android.app.Application;
import io.hansel.pebbletracesdk.codepatch.patch.Patch;

public class HanselCrashReporter
{
  public static int SYNC_DEFAULT = SYNC_ON_INIT | SYNC_ON_FOREGROUND;
  public static int SYNC_NO_AUTOSYNC = 0;
  public static int SYNC_ON_FOREGROUND;
  public static int SYNC_ON_INIT = 1;
  private static HanselCrashReporter hanselCrashReporter = new HanselCrashReporter();
  private a hanselInternal = new a();
  
  static
  {
    SYNC_ON_FOREGROUND = 2;
  }
  
  public static HanselCrashReporter getInstance()
  {
    return hanselCrashReporter;
  }
  
  public static Patch getPatch(Class paramClass, String paramString, Class... paramVarArgs)
  {
    return io.hansel.pebbletracesdk.codepatch.a.a(paramClass, paramString, paramVarArgs);
  }
  
  public static void setEndGame(EndGame paramEndGame)
  {
    d.a(paramEndGame);
  }
  
  public int getSyncFlag()
  {
    return this.hanselInternal.a();
  }
  
  public HanselCrashReporter initializeSDK(Application paramApplication, String paramString1, String paramString2)
  {
    this.hanselInternal.a(paramApplication, paramString1, paramString2);
    return this;
  }
  
  public void patchSync()
  {
    this.hanselInternal.a(true);
  }
  
  public void setSyncFlag(int paramInt)
  {
    this.hanselInternal.a(paramInt);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/io/hansel/pebbletracesdk/HanselCrashReporter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */