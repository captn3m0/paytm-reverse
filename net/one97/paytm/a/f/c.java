package net.one97.paytm.a.f;

import com.google.c.a.b;
import io.hansel.pebbletracesdk.HanselCrashReporter;
import io.hansel.pebbletracesdk.annotations.HanselInclude;
import io.hansel.pebbletracesdk.codepatch.PatchJoinPoint.PatchJoinPointBuilder;
import io.hansel.pebbletracesdk.codepatch.patch.Patch;
import net.one97.paytm.common.entity.IJRDataModel;

@HanselInclude
public class c
  implements IJRDataModel
{
  @b(a="response")
  private a a;
  
  public a a()
  {
    Patch localPatch = HanselCrashReporter.getPatch(c.class, "a", null);
    if (localPatch != null) {
      return (a)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.a;
  }
  
  @HanselInclude
  public class a
  {
    @b(a="qrCodeId")
    private String a;
    
    public String a()
    {
      Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", null);
      if (localPatch != null) {
        return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.a;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/a/f/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */