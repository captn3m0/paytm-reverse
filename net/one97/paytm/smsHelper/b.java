package net.one97.paytm.smsHelper;

import io.hansel.pebbletracesdk.HanselCrashReporter;
import io.hansel.pebbletracesdk.annotations.HanselInclude;
import io.hansel.pebbletracesdk.codepatch.Conversions;
import io.hansel.pebbletracesdk.codepatch.PatchJoinPoint.PatchJoinPointBuilder;
import io.hansel.pebbletracesdk.codepatch.patch.Patch;

@HanselInclude
public class b
{
  @com.google.c.a.b(a="sendMessage")
  private boolean a;
  @com.google.c.a.b(a="description")
  private String b;
  
  public String a()
  {
    Patch localPatch = HanselCrashReporter.getPatch(b.class, "a", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.b;
  }
  
  public boolean b()
  {
    Patch localPatch = HanselCrashReporter.getPatch(b.class, "b", null);
    if (localPatch != null) {
      return Conversions.booleanValue(localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint()));
    }
    return this.a;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/smsHelper/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */