package net.one97.paytm.common.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import java.util.Locale;

public class o
{
  public static String a()
  {
    String str = Locale.getDefault().getLanguage();
    if ((str.equalsIgnoreCase("en")) || (str.equalsIgnoreCase("hi")) || (str.equalsIgnoreCase("ta")) || (str.equalsIgnoreCase("te")) || (str.equalsIgnoreCase("kn")) || (str.equalsIgnoreCase("pa")) || (str.equalsIgnoreCase("mr")) || (str.equalsIgnoreCase("gu")) || (str.equalsIgnoreCase("bn")) || (str.equalsIgnoreCase("ml")) || (str.equalsIgnoreCase("or"))) {
      return str;
    }
    return "en";
  }
  
  public static String a(Context paramContext)
  {
    return a(paramContext, a());
  }
  
  public static String a(Context paramContext, String paramString)
  {
    try
    {
      paramContext = PreferenceManager.getDefaultSharedPreferences(paramContext).getString("Locale.Helper.Selected.Language", paramString);
      return paramContext;
    }
    catch (Exception paramContext) {}
    return paramString;
  }
  
  public static void b(Context paramContext, String paramString)
  {
    c(paramContext, paramString);
    d(paramContext, paramString);
  }
  
  public static void c(Context paramContext, String paramString)
  {
    try
    {
      paramContext = PreferenceManager.getDefaultSharedPreferences(paramContext).edit();
      paramContext.putString("Locale.Helper.Selected.Language", paramString);
      paramContext.apply();
      return;
    }
    catch (Exception paramContext)
    {
      paramContext.printStackTrace();
    }
  }
  
  private static void d(Context paramContext, String paramString)
  {
    try
    {
      paramString = new Locale(paramString);
      Locale.setDefault(paramString);
      paramContext = paramContext.getResources();
      Configuration localConfiguration = paramContext.getConfiguration();
      localConfiguration.locale = paramString;
      paramContext.updateConfiguration(localConfiguration, paramContext.getDisplayMetrics());
      return;
    }
    catch (Exception paramContext)
    {
      paramContext.printStackTrace();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/utility/o.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */