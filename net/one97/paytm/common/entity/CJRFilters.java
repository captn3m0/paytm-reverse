package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.Iterator;

public class CJRFilters
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="filters")
  private ArrayList<CJRFilterItem> mFilterList = new ArrayList();
  
  public CJRFilterItem getFilterItemByTitle(String paramString1, String paramString2)
  {
    Object localObject2 = null;
    Iterator localIterator = this.mFilterList.iterator();
    Object localObject1;
    do
    {
      localObject1 = localObject2;
      if (!localIterator.hasNext()) {
        break;
      }
      localObject1 = (CJRFilterItem)localIterator.next();
    } while ((!((CJRFilterItem)localObject1).getTitle().equalsIgnoreCase(paramString1)) || (!((CJRFilterItem)localObject1).getType().equalsIgnoreCase(paramString2)));
    return (CJRFilterItem)localObject1;
  }
  
  public ArrayList<CJRFilterItem> getFilterList()
  {
    return this.mFilterList;
  }
  
  public String getName()
  {
    return null;
  }
  
  public void setFilterList(ArrayList<CJRFilterItem> paramArrayList)
  {
    this.mFilterList = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRFilters.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */