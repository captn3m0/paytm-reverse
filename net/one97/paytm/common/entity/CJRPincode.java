package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRPincode
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="city")
  private String mCity;
  @b(a="pincode")
  private String mPincode;
  @b(a="state")
  private String mState;
  
  public String getCity()
  {
    return this.mCity;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getPincode()
  {
    return this.mPincode;
  }
  
  public String getState()
  {
    return this.mState;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRPincode.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */