package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRUserSocialInfoV2
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="siteLoginId")
  private String mSiteLoginId;
  @b(a="siteName")
  private String mSiteName;
  @b(a="info")
  private SocialSiteInfo mSocialSiteInfo;
  @b(a="userId")
  private String mUserId;
  
  public String getSiteLoginId()
  {
    return this.mSiteLoginId;
  }
  
  public String getSiteName()
  {
    return this.mSiteName;
  }
  
  public SocialSiteInfo getSocialSiteInfo()
  {
    return this.mSocialSiteInfo;
  }
  
  public String getUserId()
  {
    return this.mUserId;
  }
  
  public void setSiteLoginId(String paramString)
  {
    this.mSiteLoginId = paramString;
  }
  
  public void setSiteName(String paramString)
  {
    this.mSiteName = paramString;
  }
  
  public void setSocialSiteInfo(SocialSiteInfo paramSocialSiteInfo)
  {
    this.mSocialSiteInfo = paramSocialSiteInfo;
  }
  
  public void setUserId(String paramString)
  {
    this.mUserId = paramString;
  }
  
  public class SocialSiteInfo
  {
    @b(a="firstName")
    private String mFirstName;
    @b(a="gender")
    private String mGender;
    @b(a="lastName")
    private String mLastName;
    @b(a="locale")
    private String mLocale;
    @b(a="picture")
    private String mPicture;
    
    public SocialSiteInfo() {}
    
    public String getFirstName()
    {
      return this.mFirstName;
    }
    
    public String getGender()
    {
      return this.mGender;
    }
    
    public String getLastName()
    {
      return this.mLastName;
    }
    
    public String getLocale()
    {
      return this.mLocale;
    }
    
    public String getPicture()
    {
      return this.mPicture;
    }
    
    public void setFirstName(String paramString)
    {
      this.mFirstName = paramString;
    }
    
    public void setGender(String paramString)
    {
      this.mGender = paramString;
    }
    
    public void setLastName(String paramString)
    {
      this.mLastName = paramString;
    }
    
    public void setLocale(String paramString)
    {
      this.mLocale = paramString;
    }
    
    public void setPicture(String paramString)
    {
      this.mPicture = paramString;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRUserSocialInfoV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */