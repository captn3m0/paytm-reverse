package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRRequest
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="amount")
  private String mAmount;
  @b(a="currencyCode")
  private String mCurrencyCode;
  @b(a="merchantOrderId")
  private String mMerchantOrderId;
  @b(a="payeeCustId")
  private String mPayeeCustId;
  @b(a="payeeEmail")
  private String mPayeeEmail;
  @b(a="payeeMobile")
  private String mPayeeMobile;
  @b(a="payeePhoneNumber")
  private String mPayeePhoneNumber;
  @b(a="payeeSsoId")
  private String mPayeeSsoId;
  @b(a="pgTxnId")
  private String mPgTxnId;
  @b(a="txnGuid")
  private String mTxnGuid;
  @b(a="userGuid")
  private String mUserGuid;
  @b(a="walletTxnGuid")
  private String mWalletTxnGuid;
  
  public String getAmount()
  {
    return this.mAmount;
  }
  
  public String getCurrencyCode()
  {
    return this.mCurrencyCode;
  }
  
  public String getMerchantOrderId()
  {
    return this.mMerchantOrderId;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getPayeeCustId()
  {
    return this.mPayeeCustId;
  }
  
  public String getPayeeEmail()
  {
    return this.mPayeeEmail;
  }
  
  public String getPayeeMobile()
  {
    return this.mPayeeMobile;
  }
  
  public String getPayeePhoneNumber()
  {
    return this.mPayeePhoneNumber;
  }
  
  public String getPayeeSsoId()
  {
    return this.mPayeeSsoId;
  }
  
  public String getPgTxnId()
  {
    return this.mPgTxnId;
  }
  
  public String getTxnGuid()
  {
    return this.mTxnGuid;
  }
  
  public String getUserGuid()
  {
    return this.mUserGuid;
  }
  
  public String getWalletTxnGuid()
  {
    return this.mWalletTxnGuid;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */