package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.ArrayList;

public class CJRAds
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="ad")
  private ArrayList<CJRAd> mAd;
  @b(a="count")
  private String mCount;
  @b(a="version")
  private String mVersion;
  
  public ArrayList<CJRAd> getAd()
  {
    return this.mAd;
  }
  
  public String getCount()
  {
    return this.mCount;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getVersion()
  {
    return this.mVersion;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRAds.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */