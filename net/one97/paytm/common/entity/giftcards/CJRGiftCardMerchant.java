package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftCardMerchant
  implements IJRDataModel
{
  @b(a="address")
  private CJRGiftCardMerchantAddress address;
  @b(a="merchant_id")
  private int merchantId;
  @b(a="merchant_image")
  private String merchantImage;
  @b(a="merchant_name")
  private String merchantName;
  @b(a="paytm_verified")
  private int paytmVerified;
  @b(a="selling_since")
  private String sellingSince;
  @b(a="stores")
  private List<CJRGiftCardMerchantStore> stores = new ArrayList();
  
  public CJRGiftCardMerchantAddress getAddress()
  {
    return this.address;
  }
  
  public int getMerchantId()
  {
    return this.merchantId;
  }
  
  public String getMerchantImage()
  {
    return this.merchantImage;
  }
  
  public String getMerchantName()
  {
    return this.merchantName;
  }
  
  public int getPaytmVerified()
  {
    return this.paytmVerified;
  }
  
  public String getSellingSince()
  {
    return this.sellingSince;
  }
  
  public List<CJRGiftCardMerchantStore> getStores()
  {
    return this.stores;
  }
  
  public void setAddress(CJRGiftCardMerchantAddress paramCJRGiftCardMerchantAddress)
  {
    this.address = paramCJRGiftCardMerchantAddress;
  }
  
  public void setMerchantId(int paramInt)
  {
    this.merchantId = paramInt;
  }
  
  public void setMerchantImage(String paramString)
  {
    this.merchantImage = paramString;
  }
  
  public void setMerchantName(String paramString)
  {
    this.merchantName = paramString;
  }
  
  public void setPaytmVerified(int paramInt)
  {
    this.paytmVerified = paramInt;
  }
  
  public void setSellingSince(String paramString)
  {
    this.sellingSince = paramString;
  }
  
  public void setStores(List<CJRGiftCardMerchantStore> paramList)
  {
    this.stores = paramList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardMerchant.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */