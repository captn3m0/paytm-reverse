package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftCardFilters
  implements IJRDataModel
{
  @b(a="display_value")
  private String displayValue;
  @b(a="filter_param")
  private String filterParam;
  @b(a="values")
  private List<CJRGiftCardFilterValue> giftCardFilterValues = new ArrayList();
  @b(a="title")
  private String title;
  @b(a="type")
  private String type;
  
  public String getDisplayValue()
  {
    return this.displayValue;
  }
  
  public String getFilterParam()
  {
    return this.filterParam;
  }
  
  public List<CJRGiftCardFilterValue> getGiftCardFilterValues()
  {
    return this.giftCardFilterValues;
  }
  
  public String getTitle()
  {
    return this.title;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public void setDisplayValue(String paramString)
  {
    this.displayValue = paramString;
  }
  
  public void setFilterParam(String paramString)
  {
    this.filterParam = paramString;
  }
  
  public void setGiftCardFilterValues(List<CJRGiftCardFilterValue> paramList)
  {
    this.giftCardFilterValues = paramList;
  }
  
  public void setTitle(String paramString)
  {
    this.title = paramString;
  }
  
  public void setType(String paramString)
  {
    this.type = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardFilters.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */