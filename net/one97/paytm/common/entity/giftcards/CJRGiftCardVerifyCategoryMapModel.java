package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftCardVerifyCategoryMapModel
  implements IJRDataModel
{
  @b(a="id")
  private Integer id;
  @b(a="name")
  private String name;
  @b(a="parent_id")
  private Integer parent_id;
  @b(a="url_key")
  private String url_key;
  @b(a="vat_restrict")
  private Integer vat_restrict;
  
  public Integer getId()
  {
    return this.id;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public Integer getParent_id()
  {
    return this.parent_id;
  }
  
  public String getUrl_key()
  {
    return this.url_key;
  }
  
  public Integer getVat_restrict()
  {
    return this.vat_restrict;
  }
  
  public void setId(Integer paramInteger)
  {
    this.id = paramInteger;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setParent_id(Integer paramInteger)
  {
    this.parent_id = paramInteger;
  }
  
  public void setUrl_key(String paramString)
  {
    this.url_key = paramString;
  }
  
  public void setVat_restrict(Integer paramInteger)
  {
    this.vat_restrict = paramInteger;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardVerifyCategoryMapModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */