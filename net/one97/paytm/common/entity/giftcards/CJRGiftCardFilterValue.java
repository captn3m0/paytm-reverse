package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftCardFilterValue
  implements IJRDataModel
{
  @b(a="applied")
  private boolean applied;
  @b(a="attributes")
  private String attributes;
  @b(a="exist")
  private boolean exist;
  @b(a="id")
  private String id;
  @b(a="name")
  private String name;
  @b(a="product_id")
  private int productId;
  @b(a="seourl")
  private String seourl;
  @b(a="totalScore")
  private int totalScore;
  @b(a="url")
  private String url;
  
  public String getAttributes()
  {
    return this.attributes;
  }
  
  public String getId()
  {
    return this.id;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public int getProductId()
  {
    return this.productId;
  }
  
  public String getSeourl()
  {
    return this.seourl;
  }
  
  public int getTotalScore()
  {
    return this.totalScore;
  }
  
  public String getUrl()
  {
    return this.url;
  }
  
  public boolean isApplied()
  {
    return this.applied;
  }
  
  public boolean isExist()
  {
    return this.exist;
  }
  
  public void setApplied(boolean paramBoolean)
  {
    this.applied = paramBoolean;
  }
  
  public void setAttributes(String paramString)
  {
    this.attributes = paramString;
  }
  
  public void setExist(boolean paramBoolean)
  {
    this.exist = paramBoolean;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setProductId(int paramInt)
  {
    this.productId = paramInt;
  }
  
  public void setSeourl(String paramString)
  {
    this.seourl = paramString;
  }
  
  public void setTotalScore(int paramInt)
  {
    this.totalScore = paramInt;
  }
  
  public void setUrl(String paramString)
  {
    this.url = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardFilterValue.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */