package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftCardUserInputField
  implements IJRDataModel
{
  public static final String FIELD_TYPE_CALENDAR = "date";
  public static final String FIELD_TYPE_CHECKBOX = "checkbox";
  public static final String FIELD_TYPE_DROPDOWN = "select";
  public static final String FIELD_TYPE_RADIO = "radio";
  public static final String FIELD_TYPE_TEXTBOX = "text";
  public static final String FIELD_TYPE_TEXT_AREA = "textarea";
  private static final long serialVersionUID = 1L;
  private String appliedData;
  @b(a="config_key")
  private String configKey;
  @b(a="mandatory")
  private boolean mandatory;
  @b(a="regex")
  private String regex;
  @b(a="title")
  private String title;
  @b(a="type")
  private String type;
  
  public String getAppliedData()
  {
    return this.appliedData;
  }
  
  public String getConfigKey()
  {
    return this.configKey;
  }
  
  public String getRegex()
  {
    return this.regex;
  }
  
  public String getTitle()
  {
    return this.title;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public boolean isMandatory()
  {
    return this.mandatory;
  }
  
  public void setAppliedData(String paramString)
  {
    this.appliedData = paramString;
  }
  
  public void setConfigKey(String paramString)
  {
    this.configKey = paramString;
  }
  
  public void setMandatory(boolean paramBoolean)
  {
    this.mandatory = paramBoolean;
  }
  
  public void setRegex(String paramString)
  {
    this.regex = paramString;
  }
  
  public void setTitle(String paramString)
  {
    this.title = paramString;
  }
  
  public void setType(String paramString)
  {
    this.type = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardUserInputField.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */