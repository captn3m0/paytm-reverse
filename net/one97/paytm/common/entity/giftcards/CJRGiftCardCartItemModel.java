package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftCardCartItemModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="configuration")
  private CJRGiftCardPDPConfigurationModel configuration;
  @b(a="meta_data")
  private CJRGiftCardPDPMetaDataModel meta_data;
  @b(a="product_id")
  private Integer product_id;
  @b(a="qty")
  private Integer qty;
  
  public CJRGiftCardPDPConfigurationModel getConfiguration()
  {
    return this.configuration;
  }
  
  public CJRGiftCardPDPMetaDataModel getMeta_data()
  {
    return this.meta_data;
  }
  
  public Integer getProductId()
  {
    return this.product_id;
  }
  
  public Integer getQty()
  {
    return this.qty;
  }
  
  public void setConfiguration(CJRGiftCardPDPConfigurationModel paramCJRGiftCardPDPConfigurationModel)
  {
    this.configuration = paramCJRGiftCardPDPConfigurationModel;
  }
  
  public void setMeta_data(CJRGiftCardPDPMetaDataModel paramCJRGiftCardPDPMetaDataModel)
  {
    this.meta_data = paramCJRGiftCardPDPMetaDataModel;
  }
  
  public void setProductId(Integer paramInteger)
  {
    this.product_id = paramInteger;
  }
  
  public void setQty(Integer paramInteger)
  {
    this.qty = paramInteger;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardCartItemModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */