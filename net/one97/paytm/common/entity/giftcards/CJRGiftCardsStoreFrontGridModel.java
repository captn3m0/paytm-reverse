package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.CJRFilterItem;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftCardsStoreFrontGridModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="filters")
  private ArrayList<CJRFilterItem> mFilters;
  @b(a="grid_layout")
  private ArrayList<CJRGiftCardsStoreFrontItemDetailModel> mStoreFrontGridItems;
  @b(a="name")
  private String title;
  
  public ArrayList<CJRGiftCardsStoreFrontItemDetailModel> getStoreFrontGridItems()
  {
    return this.mStoreFrontGridItems;
  }
  
  public String getTitle()
  {
    return this.title;
  }
  
  public ArrayList<CJRFilterItem> getmFilters()
  {
    return this.mFilters;
  }
  
  public void setStoreFrontGridItems(ArrayList<CJRGiftCardsStoreFrontItemDetailModel> paramArrayList)
  {
    this.mStoreFrontGridItems = paramArrayList;
  }
  
  public void setTitle(String paramString)
  {
    this.title = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardsStoreFrontGridModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */