package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import java.io.Serializable;

public class CJRGiftCardLongRichDesc
  implements Serializable
{
  @b(a="attributes")
  private CJRGiftCardLongRichDescAttributes attributes;
  @b(a="description")
  private String description;
  @b(a="title")
  private String title;
  
  public CJRGiftCardLongRichDescAttributes getAttributes()
  {
    return this.attributes;
  }
  
  public String getDescription()
  {
    return this.description;
  }
  
  public String getTitle()
  {
    return this.title;
  }
  
  public void setAttributes(CJRGiftCardLongRichDescAttributes paramCJRGiftCardLongRichDescAttributes)
  {
    this.attributes = paramCJRGiftCardLongRichDescAttributes;
  }
  
  public void setDescription(String paramString)
  {
    this.description = paramString;
  }
  
  public void setTitle(String paramString)
  {
    this.title = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardLongRichDesc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */