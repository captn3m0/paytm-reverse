package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftCardTermsConditions
  implements IJRDataModel
{
  @b(a="message")
  private String message;
  @b(a="title")
  private String title;
  
  public String getMessage()
  {
    return this.message;
  }
  
  public String getTitle()
  {
    return this.title;
  }
  
  public void setMessage(String paramString)
  {
    this.message = paramString;
  }
  
  public void setTitle(String paramString)
  {
    this.title = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardTermsConditions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */