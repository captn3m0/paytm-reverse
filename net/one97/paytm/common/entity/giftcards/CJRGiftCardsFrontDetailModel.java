package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftCardsFrontDetailModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="items")
  private ArrayList<CJRGiftCardsStoreFrontItemDetailModel> mBannertems;
  @b(a="id")
  private int mId;
  @b(a="layout")
  private String mLayout;
  @b(a="name")
  private String mName;
  @b(a="priority")
  private int mPriority;
  @b(a="status")
  private int mStatus;
  
  public ArrayList<CJRGiftCardsStoreFrontItemDetailModel> getmBannertems()
  {
    return this.mBannertems;
  }
  
  public int getmId()
  {
    return this.mId;
  }
  
  public String getmLayout()
  {
    return this.mLayout;
  }
  
  public String getmName()
  {
    return this.mName;
  }
  
  public int getmPriority()
  {
    return this.mPriority;
  }
  
  public int getmStatus()
  {
    return this.mStatus;
  }
  
  public void setmBannertems(ArrayList<CJRGiftCardsStoreFrontItemDetailModel> paramArrayList)
  {
    this.mBannertems = paramArrayList;
  }
  
  public void setmId(int paramInt)
  {
    this.mId = paramInt;
  }
  
  public void setmLayout(String paramString)
  {
    this.mLayout = paramString;
  }
  
  public void setmName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setmPriority(int paramInt)
  {
    this.mPriority = paramInt;
  }
  
  public void setmStatus(int paramInt)
  {
    this.mStatus = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardsFrontDetailModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */