package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.a;
import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftCardMerchantAddress
  implements IJRDataModel
{
  @a
  @b(a="address")
  private String address;
  @a
  @b(a="city")
  private String city;
  @a
  @b(a="pin_code")
  private String pinCode;
  @a
  @b(a="state")
  private String state;
  
  public String getAddress()
  {
    return this.address;
  }
  
  public String getCity()
  {
    return this.city;
  }
  
  public String getPinCode()
  {
    return this.pinCode;
  }
  
  public String getState()
  {
    return this.state;
  }
  
  public void setAddress(String paramString)
  {
    this.address = paramString;
  }
  
  public void setCity(String paramString)
  {
    this.city = paramString;
  }
  
  public void setPinCode(String paramString)
  {
    this.pinCode = paramString;
  }
  
  public void setState(String paramString)
  {
    this.state = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardMerchantAddress.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */