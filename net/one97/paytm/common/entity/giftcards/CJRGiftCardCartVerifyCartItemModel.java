package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;
import net.one97.paytm.common.entity.amPark.CJRConvenienceFeeModel;

public class CJRGiftCardCartVerifyCartItemModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="aggregate_item_price")
  private Integer aggregate_item_price;
  @b(a="attributes_dim_values")
  private CJRGiftCardAttributesDimValues attributes_dim_values;
  @b(a="available_quantity")
  private Object available_quantity;
  @b(a="brand")
  private String brand;
  @b(a="brand_id")
  private Integer brand_id;
  @b(a="categoryMap")
  private List<CJRGiftCardVerifyCategoryMapModel> categoryMap = new ArrayList();
  @b(a="configuration")
  private CJRGiftCardPDPConfigurationModel configuration;
  @b(a="conv_fee")
  private Integer conv_fee;
  @b(a="conv_fee_map")
  private CJRConvenienceFeeModel conv_fee_map;
  @b(a="discounted_price")
  private Integer discounted_price;
  @b(a="display_sequence")
  private Integer display_sequence;
  @b(a="estimated_delivery")
  private Object estimated_delivery;
  @b(a="ga_key")
  private String ga_key;
  @b(a="image_url")
  private String image_url;
  @b(a="item_id")
  private String item_id;
  @b(a="merchant_id")
  private Integer merchant_id;
  @b(a="merchant_name")
  private String merchant_name;
  @b(a="meta_data")
  private CJRGiftCardPDPMetaDataModel meta_data;
  @b(a="mrp")
  private Integer mrp;
  @b(a="name")
  private String name;
  @b(a="need_shipping")
  private Boolean need_shipping;
  @b(a="offer_url")
  private String offer_url;
  @b(a="other_taxes")
  private List<Object> other_taxes = new ArrayList();
  @b(a="price")
  private Integer price;
  @b(a="price_incl_taxes")
  private List<Object> price_incl_taxes = new ArrayList();
  @b(a="product_id")
  private Integer product_id;
  @b(a="promocode")
  private String promocode;
  @b(a="promostatus")
  private String promostatus;
  @b(a="promotext")
  private String promotext;
  @b(a="quantity")
  private Integer quantity;
  @b(a="readonly")
  private Boolean readonly;
  @b(a="selling_price")
  private Integer selling_price;
  @b(a="seourl")
  private String seourl;
  @b(a="shipping_charges")
  private Integer shipping_charges;
  @b(a="short_description")
  private Object short_description;
  @b(a="title")
  private String title;
  @b(a="total_price")
  private Integer total_price;
  @b(a="url")
  private String url;
  @b(a="vertical_id")
  private Integer vertical_id;
  @b(a="vertical_label")
  private String vertical_label;
  
  public Integer getAggregate_item_price()
  {
    return this.aggregate_item_price;
  }
  
  public CJRGiftCardAttributesDimValues getAttributes_dim_values()
  {
    return this.attributes_dim_values;
  }
  
  public Object getAvailable_quantity()
  {
    return this.available_quantity;
  }
  
  public String getBrand()
  {
    return this.brand;
  }
  
  public Integer getBrand_id()
  {
    return this.brand_id;
  }
  
  public List<CJRGiftCardVerifyCategoryMapModel> getCategoryMap()
  {
    return this.categoryMap;
  }
  
  public CJRGiftCardPDPConfigurationModel getConfiguration()
  {
    return this.configuration;
  }
  
  public Integer getConv_fee()
  {
    return this.conv_fee;
  }
  
  public CJRConvenienceFeeModel getConv_fee_map()
  {
    return this.conv_fee_map;
  }
  
  public Integer getDiscounted_price()
  {
    return this.discounted_price;
  }
  
  public Integer getDisplay_sequence()
  {
    return this.display_sequence;
  }
  
  public Object getEstimated_delivery()
  {
    return this.estimated_delivery;
  }
  
  public String getGa_key()
  {
    return this.ga_key;
  }
  
  public String getImage_url()
  {
    return this.image_url;
  }
  
  public String getItem_id()
  {
    return this.item_id;
  }
  
  public Integer getMerchant_id()
  {
    return this.merchant_id;
  }
  
  public String getMerchant_name()
  {
    return this.merchant_name;
  }
  
  public CJRGiftCardPDPMetaDataModel getMeta_data()
  {
    return this.meta_data;
  }
  
  public Integer getMrp()
  {
    return this.mrp;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public Boolean getNeed_shipping()
  {
    return this.need_shipping;
  }
  
  public String getOffer_url()
  {
    return this.offer_url;
  }
  
  public List<Object> getOther_taxes()
  {
    return this.other_taxes;
  }
  
  public Integer getPrice()
  {
    return this.price;
  }
  
  public List<Object> getPrice_incl_taxes()
  {
    return this.price_incl_taxes;
  }
  
  public Integer getProduct_id()
  {
    return this.product_id;
  }
  
  public String getPromocode()
  {
    return this.promocode;
  }
  
  public String getPromostatus()
  {
    return this.promostatus;
  }
  
  public String getPromotext()
  {
    return this.promotext;
  }
  
  public Integer getQuantity()
  {
    return this.quantity;
  }
  
  public Boolean getReadonly()
  {
    return this.readonly;
  }
  
  public Integer getSelling_price()
  {
    return this.selling_price;
  }
  
  public String getSeourl()
  {
    return this.seourl;
  }
  
  public Integer getShipping_charges()
  {
    return this.shipping_charges;
  }
  
  public Object getShort_description()
  {
    return this.short_description;
  }
  
  public String getTitle()
  {
    return this.title;
  }
  
  public Integer getTotal_price()
  {
    return this.total_price;
  }
  
  public String getUrl()
  {
    return this.url;
  }
  
  public Integer getVertical_id()
  {
    return this.vertical_id;
  }
  
  public String getVertical_label()
  {
    return this.vertical_label;
  }
  
  public void setAggregate_item_price(Integer paramInteger)
  {
    this.aggregate_item_price = paramInteger;
  }
  
  public void setAttributes_dim_values(CJRGiftCardAttributesDimValues paramCJRGiftCardAttributesDimValues)
  {
    this.attributes_dim_values = paramCJRGiftCardAttributesDimValues;
  }
  
  public void setAvailable_quantity(Object paramObject)
  {
    this.available_quantity = paramObject;
  }
  
  public void setBrand(String paramString)
  {
    this.brand = paramString;
  }
  
  public void setBrand_id(Integer paramInteger)
  {
    this.brand_id = paramInteger;
  }
  
  public void setCategoryMap(List<CJRGiftCardVerifyCategoryMapModel> paramList)
  {
    this.categoryMap = paramList;
  }
  
  public void setConfiguration(CJRGiftCardPDPConfigurationModel paramCJRGiftCardPDPConfigurationModel)
  {
    this.configuration = paramCJRGiftCardPDPConfigurationModel;
  }
  
  public void setConv_fee(Integer paramInteger)
  {
    this.conv_fee = paramInteger;
  }
  
  public void setConv_fee_map(CJRConvenienceFeeModel paramCJRConvenienceFeeModel)
  {
    this.conv_fee_map = paramCJRConvenienceFeeModel;
  }
  
  public void setDiscounted_price(Integer paramInteger)
  {
    this.discounted_price = paramInteger;
  }
  
  public void setDisplay_sequence(Integer paramInteger)
  {
    this.display_sequence = paramInteger;
  }
  
  public void setEstimated_delivery(Object paramObject)
  {
    this.estimated_delivery = paramObject;
  }
  
  public void setGa_key(String paramString)
  {
    this.ga_key = paramString;
  }
  
  public void setImage_url(String paramString)
  {
    this.image_url = paramString;
  }
  
  public void setItem_id(String paramString)
  {
    this.item_id = paramString;
  }
  
  public void setMerchant_id(Integer paramInteger)
  {
    this.merchant_id = paramInteger;
  }
  
  public void setMerchant_name(String paramString)
  {
    this.merchant_name = paramString;
  }
  
  public void setMeta_data(CJRGiftCardPDPMetaDataModel paramCJRGiftCardPDPMetaDataModel)
  {
    this.meta_data = paramCJRGiftCardPDPMetaDataModel;
  }
  
  public void setMrp(Integer paramInteger)
  {
    this.mrp = paramInteger;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setNeed_shipping(Boolean paramBoolean)
  {
    this.need_shipping = paramBoolean;
  }
  
  public void setOffer_url(String paramString)
  {
    this.offer_url = paramString;
  }
  
  public void setOther_taxes(List<Object> paramList)
  {
    this.other_taxes = paramList;
  }
  
  public void setPrice(Integer paramInteger)
  {
    this.price = paramInteger;
  }
  
  public void setPrice_incl_taxes(List<Object> paramList)
  {
    this.price_incl_taxes = paramList;
  }
  
  public void setProduct_id(Integer paramInteger)
  {
    this.product_id = paramInteger;
  }
  
  public void setPromocode(String paramString)
  {
    this.promocode = paramString;
  }
  
  public void setPromostatus(String paramString)
  {
    this.promostatus = paramString;
  }
  
  public void setPromotext(String paramString)
  {
    this.promotext = paramString;
  }
  
  public void setQuantity(Integer paramInteger)
  {
    this.quantity = paramInteger;
  }
  
  public void setReadonly(Boolean paramBoolean)
  {
    this.readonly = paramBoolean;
  }
  
  public void setSelling_price(Integer paramInteger)
  {
    this.selling_price = paramInteger;
  }
  
  public void setSeourl(String paramString)
  {
    this.seourl = paramString;
  }
  
  public void setShipping_charges(Integer paramInteger)
  {
    this.shipping_charges = paramInteger;
  }
  
  public void setShort_description(Object paramObject)
  {
    this.short_description = paramObject;
  }
  
  public void setTitle(String paramString)
  {
    this.title = paramString;
  }
  
  public void setTotal_price(Integer paramInteger)
  {
    this.total_price = paramInteger;
  }
  
  public void setUrl(String paramString)
  {
    this.url = paramString;
  }
  
  public void setVertical_id(Integer paramInteger)
  {
    this.vertical_id = paramInteger;
  }
  
  public void setVertical_label(String paramString)
  {
    this.vertical_label = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardCartVerifyCartItemModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */