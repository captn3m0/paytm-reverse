package net.one97.paytm.common.entity.giftcards;

import android.text.TextUtils;
import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.CJRItem;
import net.one97.paytm.common.entity.shopping.CJRRelatedCategory;

public class CJRGiftCardsStoreFrontItemDetailModel
  extends CJRItem
{
  private static final long serialVersionUID = 1L;
  private boolean isVisited;
  @b(a="actual_price")
  private double mActualPrice;
  @b(a="alt_image_url")
  private String mAltImageUrl;
  @b(a="brand_id")
  private int mBrandId;
  @b(a="brand")
  private String mBrandName;
  @b(a="complex_product_id")
  private long mComplex_product_id;
  @b(a="discount")
  private double mDiscount;
  @b(a="id")
  private int mId;
  @b(a="image_url")
  private String mImageUrl;
  @b(a="img_height")
  private int mImgHeight;
  @b(a="img_width")
  private int mImgWidth;
  @b(a="name")
  private String mName;
  @b(a="offer_price")
  private double mOfferPrice;
  @b(a="parent_id")
  private long mParentId;
  @b(a="priority")
  private int mPriority;
  @b(a="status")
  private String mStatus;
  @b(a="stock")
  private boolean mStock;
  @b(a="subtitle")
  private String mSubTitle;
  @b(a="tag")
  private String mTag;
  @b(a="title")
  private String mTitle;
  @b(a="url")
  private String mUrl;
  @b(a="url_info")
  private String mUrlInfo;
  @b(a="url_type")
  private String mUrlType;
  @b(a="view_item_id")
  private int mViewItemId;
  @b(a="seourl")
  private String mseourl;
  
  public double getActualPrice()
  {
    return this.mActualPrice;
  }
  
  public String getAltImageUrl()
  {
    return this.mAltImageUrl;
  }
  
  public String getBrand()
  {
    return this.mBrandName;
  }
  
  public int getBrandId()
  {
    return this.mBrandId;
  }
  
  public String getCategoryId()
  {
    return "";
  }
  
  public long getComplex_product_id()
  {
    return this.mComplex_product_id;
  }
  
  public double getDiscount()
  {
    return this.mDiscount;
  }
  
  public int getId()
  {
    return this.mId;
  }
  
  public String getImageUrl()
  {
    return this.mImageUrl;
  }
  
  public int getImgHeight()
  {
    return this.mImgHeight;
  }
  
  public int getImgWidth()
  {
    return this.mImgWidth;
  }
  
  public String getItemID()
  {
    try
    {
      String str = Integer.toString(getId());
      boolean bool = TextUtils.isEmpty(str);
      if (!bool) {
        return str;
      }
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return "";
  }
  
  public String getLabel()
  {
    return "";
  }
  
  public String getListId()
  {
    return "";
  }
  
  public String getListName()
  {
    return "";
  }
  
  public int getListPosition()
  {
    return 0;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public double getOfferPrice()
  {
    return this.mOfferPrice;
  }
  
  public String getParentID()
  {
    return "";
  }
  
  public long getParentId()
  {
    return this.mParentId;
  }
  
  public int getPriority()
  {
    return this.mPriority;
  }
  
  public ArrayList<CJRRelatedCategory> getRelatedCategories()
  {
    return null;
  }
  
  public String getSearchABValue()
  {
    return null;
  }
  
  public String getSearchCategory()
  {
    return "";
  }
  
  public String getSearchResultType()
  {
    return "";
  }
  
  public String getSearchTerm()
  {
    return "";
  }
  
  public String getSearchType()
  {
    return "";
  }
  
  public String getSeourl()
  {
    return this.mseourl;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public String getSubTitle()
  {
    return this.mSubTitle;
  }
  
  public String getTag()
  {
    return this.mTag;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
  
  public String getURL()
  {
    return this.mUrl;
  }
  
  public String getURLType()
  {
    return this.mUrlType;
  }
  
  public String getUrlInfo()
  {
    return this.mUrlInfo;
  }
  
  public int getViewItemId()
  {
    return this.mViewItemId;
  }
  
  public String getmBrandName()
  {
    return this.mBrandName;
  }
  
  public String getmContainerInstanceID()
  {
    return "";
  }
  
  public boolean isStock()
  {
    return this.mStock;
  }
  
  public boolean isVisited()
  {
    return this.isVisited;
  }
  
  public void setActualPrice(double paramDouble)
  {
    this.mActualPrice = paramDouble;
  }
  
  public void setAltImageUrl(String paramString)
  {
    this.mAltImageUrl = paramString;
  }
  
  public void setBrandId(int paramInt)
  {
    this.mBrandId = paramInt;
  }
  
  public void setBrandName(String paramString)
  {
    this.mBrandName = paramString;
  }
  
  public void setComplex_product_id(long paramLong)
  {
    this.mComplex_product_id = paramLong;
  }
  
  public void setDiscount(double paramDouble)
  {
    this.mDiscount = paramDouble;
  }
  
  public void setId(int paramInt)
  {
    this.mId = paramInt;
  }
  
  public void setImageUrl(String paramString)
  {
    this.mImageUrl = paramString;
  }
  
  public void setImgHeight(int paramInt)
  {
    this.mImgHeight = paramInt;
  }
  
  public void setImgWidth(int paramInt)
  {
    this.mImgWidth = paramInt;
  }
  
  public void setName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setOfferPrice(double paramDouble)
  {
    this.mOfferPrice = paramDouble;
  }
  
  public void setParentId(long paramLong)
  {
    this.mParentId = paramLong;
  }
  
  public void setPriority(int paramInt)
  {
    this.mPriority = paramInt;
  }
  
  public void setSeourl(String paramString)
  {
    this.mseourl = paramString;
  }
  
  public void setStatus(String paramString)
  {
    this.mStatus = paramString;
  }
  
  public void setStock(boolean paramBoolean)
  {
    this.mStock = paramBoolean;
  }
  
  public void setSubTitle(String paramString)
  {
    this.mSubTitle = paramString;
  }
  
  public void setTag(String paramString)
  {
    this.mTag = paramString;
  }
  
  public void setTitle(String paramString)
  {
    this.mTitle = paramString;
  }
  
  public void setUrl(String paramString)
  {
    this.mUrl = paramString;
  }
  
  public void setUrlInfo(String paramString)
  {
    this.mUrlInfo = paramString;
  }
  
  public void setUrlType(String paramString)
  {
    this.mUrlType = paramString;
  }
  
  public void setViewItemId(int paramInt)
  {
    this.mViewItemId = paramInt;
  }
  
  public void setVisited(boolean paramBoolean)
  {
    this.isVisited = paramBoolean;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardsStoreFrontItemDetailModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */