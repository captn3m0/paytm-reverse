package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftCardPDPMetaDataModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="campaign")
  private String campaign;
  @b(a="contact_email")
  private String contact_email;
  @b(a="contact_phone")
  private String contact_phone;
  @b(a="dateOfDelivery")
  private String dateOfDelivery;
  @b(a="gift_amount")
  private String gift_amount;
  @b(a="message")
  private String message;
  @b(a="recipient_email")
  private String recipient_email;
  @b(a="recipient_name")
  private String recipient_name;
  @b(a="recipient_phonenumber")
  private String recipient_phonenumber;
  @b(a="scheduled_at")
  private String scheduled_at;
  
  public String getCampaign()
  {
    return this.campaign;
  }
  
  public String getContactEmail()
  {
    return this.contact_email;
  }
  
  public String getContact_phone()
  {
    return this.contact_phone;
  }
  
  public String getDateOfDelivery()
  {
    return this.dateOfDelivery;
  }
  
  public String getGift_amount()
  {
    return this.gift_amount;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public String getRecipientName()
  {
    return this.recipient_name;
  }
  
  public String getRecipient_email()
  {
    return this.recipient_email;
  }
  
  public String getRecipient_phonenumber()
  {
    return this.recipient_phonenumber;
  }
  
  public String getScheduled_at()
  {
    return this.scheduled_at;
  }
  
  public void setCampaign(String paramString)
  {
    this.campaign = paramString;
  }
  
  public void setContactEmail(String paramString)
  {
    this.contact_email = paramString;
  }
  
  public void setContact_phone(String paramString)
  {
    this.contact_phone = paramString;
  }
  
  public void setDateOfDelivery(String paramString)
  {
    this.dateOfDelivery = paramString;
  }
  
  public void setGift_amount(String paramString)
  {
    this.gift_amount = paramString;
  }
  
  public void setMessage(String paramString)
  {
    this.message = paramString;
  }
  
  public void setRecipientName(String paramString)
  {
    this.recipient_name = paramString;
  }
  
  public void setRecipient_email(String paramString)
  {
    this.recipient_email = paramString;
  }
  
  public void setRecipient_phonenumber(String paramString)
  {
    this.recipient_phonenumber = paramString;
  }
  
  public void setScheduled_at(String paramString)
  {
    this.scheduled_at = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardPDPMetaDataModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */