package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftFilterModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="filter_param")
  private String filterParam;
  @b(a="title")
  private String title;
  @b(a="type")
  private String type;
  @b(a="values")
  private ArrayList<CJRGiftFilterValueModel> values;
  
  public String getFilterParam()
  {
    return this.filterParam;
  }
  
  public String getTitle()
  {
    return this.title;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public ArrayList<CJRGiftFilterValueModel> getValues()
  {
    return this.values;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftFilterModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */