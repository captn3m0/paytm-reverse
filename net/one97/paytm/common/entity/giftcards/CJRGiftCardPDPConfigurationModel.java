package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftCardPDPConfigurationModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="price")
  private String price;
  
  public String getPrice()
  {
    return this.price;
  }
  
  public void setPrice(String paramString)
  {
    this.price = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardPDPConfigurationModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */