package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftFilterValueModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="count")
  private int count;
  @b(a="max")
  private int max;
  @b(a="min")
  private int min;
  @b(a="name")
  private String name;
  
  public int getCount()
  {
    return this.count;
  }
  
  public int getMax()
  {
    return this.max;
  }
  
  public int getMin()
  {
    return this.min;
  }
  
  public String getName()
  {
    return this.name;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftFilterValueModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */