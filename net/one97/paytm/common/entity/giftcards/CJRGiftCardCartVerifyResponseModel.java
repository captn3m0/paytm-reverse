package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;
import net.one97.paytm.common.entity.shopping.CJRCartStatus;

public class CJRGiftCardCartVerifyResponseModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="cart")
  private CJRGiftCardExpressCartModel cart;
  @b(a="status")
  private CJRCartStatus status;
  
  public CJRGiftCardExpressCartModel getCart()
  {
    return this.cart;
  }
  
  public CJRCartStatus getStatus()
  {
    return this.status;
  }
  
  public void setCart(CJRGiftCardExpressCartModel paramCJRGiftCardExpressCartModel)
  {
    this.cart = paramCJRGiftCardExpressCartModel;
  }
  
  public void setStatus(CJRCartStatus paramCJRCartStatus)
  {
    this.status = paramCJRCartStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardCartVerifyResponseModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */