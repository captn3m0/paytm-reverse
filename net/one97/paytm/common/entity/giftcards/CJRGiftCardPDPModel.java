package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftCardPDPModel
  implements IJRDataModel
{
  @b(a="action_text")
  private String actionText;
  @b(a="actual_price")
  private int actualPrice;
  @b(a="add_qty_using_price")
  private boolean addQtyUsingPrice;
  @b(a="ancestors")
  private List<CJRGiftCardAncestor> ancestors = new ArrayList();
  @b(a="attribute_chart")
  private Object attributeChart;
  @b(a="attributes")
  private CJRGiftCardAttributes attributes;
  @b(a="autocode")
  private Object autocode;
  @b(a="bargain_name")
  private String bargainName;
  @b(a="bargainable")
  private boolean bargainable;
  @b(a="brand")
  private String brand;
  @b(a="brand_id")
  private int brandId;
  @b(a="brand_seller_url")
  private String brandSellerUrl;
  @b(a="bulk_pricing")
  private boolean bulkPricing;
  @b(a="bullet_points")
  private Object bulletPoints;
  @b(a="button_text")
  private Object buttonText;
  @b(a="category_ids")
  private List<Integer> categoryIds = new ArrayList();
  @b(a="convenience_fee")
  private int convenienceFee;
  @b(a="dimensions")
  private String dimensions;
  @b(a="discount")
  private String discount;
  @b(a="emi_enabled")
  private boolean emiEnabled;
  @b(a="filters")
  private List<CJRGiftCardFilters> filters = new ArrayList();
  @b(a="fulfilled_by_paytm")
  private int fulfilledByPaytm;
  @b(a="ga_key")
  private String gaKey;
  @b(a="image_url")
  private String imageUrl;
  @b(a="input_fields")
  private List<CJRGiftCardUserInputField> inputFields = new ArrayList();
  @b(a="instock")
  private boolean instock;
  @b(a="listing_tag")
  private List<Object> listingTag = new ArrayList();
  @b(a="long_rich_desc")
  private List<CJRGiftCardLongRichDesc> longRichDesc = new ArrayList();
  @b(a="merchant")
  private CJRGiftCardMerchant merchant;
  @b(a="merchant_sku")
  private String merchantSku;
  @b(a="meta_description")
  private String metaDescription;
  @b(a="meta_keyword")
  private Object metaKeyword;
  @b(a="meta_title")
  private String metaTitle;
  @b(a="name")
  private String name;
  @b(a="need_shipping")
  private boolean needShipping;
  @b(a="no_follow")
  private boolean noFollow;
  @b(a="no_index")
  private boolean noIndex;
  @b(a="offer_price")
  private int offerPrice;
  @b(a="offer_url")
  private String offerUrl;
  @b(a="other_images")
  private List<Object> otherImages = new ArrayList();
  @b(a="other_rich_desc")
  private Object otherRichDesc;
  @b(a="other_sellers")
  private CJRGiftCardOtherSellers otherSellers;
  @b(a="parent_id")
  private int parentId;
  @b(a="pay_type_supported")
  private CJRGiftCardPayTypeSupported payTypeSupported;
  @b(a="pincode")
  private String pincode;
  @b(a="preorder")
  private boolean preorder;
  @b(a="product_created_date")
  private Object productCreatedDate;
  @b(a="product_end_date")
  private Object productEndDate;
  @b(a="product_id")
  private int productId;
  @b(a="productName")
  private String productName;
  @b(a="product_rating")
  private Object productRating;
  @b(a="product_type")
  private String productType;
  @b(a="promo_text")
  private Object promoText;
  @b(a="quantity")
  private int quantity;
  @b(a="sequential_dimension")
  private boolean sequentialDimension;
  @b(a="shareurl")
  private String shareurl;
  @b(a="short_desc")
  private String shortDesc;
  @b(a="status")
  private boolean status;
  @b(a="tag")
  private Object tag;
  @b(a="terms_conditions")
  private CJRGiftCardTermsConditions termsConditions;
  @b(a="thumbnail")
  private String thumbnail;
  @b(a="totalScore")
  private int totalScore;
  @b(a="upcfields")
  private List<Object> upcfields = new ArrayList();
  @b(a="url")
  private String url;
  @b(a="vertical_id")
  private int verticalId;
  @b(a="vertical_label")
  private String verticalLabel;
  @b(a="weight")
  private String weight;
  
  public String getActionText()
  {
    return this.actionText;
  }
  
  public int getActualPrice()
  {
    return this.actualPrice;
  }
  
  public List<CJRGiftCardAncestor> getAncestors()
  {
    return this.ancestors;
  }
  
  public Object getAttributeChart()
  {
    return this.attributeChart;
  }
  
  public CJRGiftCardAttributes getAttributes()
  {
    return this.attributes;
  }
  
  public Object getAutocode()
  {
    return this.autocode;
  }
  
  public String getBargainName()
  {
    return this.bargainName;
  }
  
  public String getBrand()
  {
    return this.brand;
  }
  
  public int getBrandId()
  {
    return this.brandId;
  }
  
  public String getBrandSellerUrl()
  {
    return this.brandSellerUrl;
  }
  
  public Object getBulletPoints()
  {
    return this.bulletPoints;
  }
  
  public Object getButtonText()
  {
    return this.buttonText;
  }
  
  public List<Integer> getCategoryIds()
  {
    return this.categoryIds;
  }
  
  public int getConvenienceFee()
  {
    return this.convenienceFee;
  }
  
  public String getDimensions()
  {
    return this.dimensions;
  }
  
  public String getDiscount()
  {
    return this.discount;
  }
  
  public List<CJRGiftCardFilters> getFilters()
  {
    return this.filters;
  }
  
  public int getFulfilledByPaytm()
  {
    return this.fulfilledByPaytm;
  }
  
  public String getGaKey()
  {
    return this.gaKey;
  }
  
  public String getImageUrl()
  {
    return this.imageUrl;
  }
  
  public List<CJRGiftCardUserInputField> getInputFields()
  {
    return this.inputFields;
  }
  
  public List<Object> getListingTag()
  {
    return this.listingTag;
  }
  
  public List<CJRGiftCardLongRichDesc> getLongRichDesc()
  {
    return this.longRichDesc;
  }
  
  public CJRGiftCardMerchant getMerchant()
  {
    return this.merchant;
  }
  
  public String getMerchantSku()
  {
    return this.merchantSku;
  }
  
  public String getMetaDescription()
  {
    return this.metaDescription;
  }
  
  public Object getMetaKeyword()
  {
    return this.metaKeyword;
  }
  
  public String getMetaTitle()
  {
    return this.metaTitle;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public int getOfferPrice()
  {
    return this.offerPrice;
  }
  
  public String getOfferUrl()
  {
    return this.offerUrl;
  }
  
  public List<Object> getOtherImages()
  {
    return this.otherImages;
  }
  
  public Object getOtherRichDesc()
  {
    return this.otherRichDesc;
  }
  
  public CJRGiftCardOtherSellers getOtherSellers()
  {
    return this.otherSellers;
  }
  
  public int getParentId()
  {
    return this.parentId;
  }
  
  public CJRGiftCardPayTypeSupported getPayTypeSupported()
  {
    return this.payTypeSupported;
  }
  
  public String getPincode()
  {
    return this.pincode;
  }
  
  public Object getProductCreatedDate()
  {
    return this.productCreatedDate;
  }
  
  public Object getProductEndDate()
  {
    return this.productEndDate;
  }
  
  public int getProductId()
  {
    return this.productId;
  }
  
  public String getProductName()
  {
    return this.productName;
  }
  
  public Object getProductRating()
  {
    return this.productRating;
  }
  
  public String getProductType()
  {
    return this.productType;
  }
  
  public Object getPromoText()
  {
    return this.promoText;
  }
  
  public int getQuantity()
  {
    return this.quantity;
  }
  
  public String getShareurl()
  {
    return this.shareurl;
  }
  
  public String getShortDesc()
  {
    return this.shortDesc;
  }
  
  public Object getTag()
  {
    return this.tag;
  }
  
  public CJRGiftCardTermsConditions getTermsConditions()
  {
    return this.termsConditions;
  }
  
  public String getThumbnail()
  {
    return this.thumbnail;
  }
  
  public int getTotalScore()
  {
    return this.totalScore;
  }
  
  public List<Object> getUpcfields()
  {
    return this.upcfields;
  }
  
  public String getUrl()
  {
    return this.url;
  }
  
  public int getVerticalId()
  {
    return this.verticalId;
  }
  
  public String getVerticalLabel()
  {
    return this.verticalLabel;
  }
  
  public String getWeight()
  {
    return this.weight;
  }
  
  public boolean isAddQtyUsingPrice()
  {
    return this.addQtyUsingPrice;
  }
  
  public boolean isBargainable()
  {
    return this.bargainable;
  }
  
  public boolean isBulkPricing()
  {
    return this.bulkPricing;
  }
  
  public boolean isEmiEnabled()
  {
    return this.emiEnabled;
  }
  
  public boolean isInstock()
  {
    return this.instock;
  }
  
  public boolean isNeedShipping()
  {
    return this.needShipping;
  }
  
  public boolean isNoFollow()
  {
    return this.noFollow;
  }
  
  public boolean isNoIndex()
  {
    return this.noIndex;
  }
  
  public boolean isPreorder()
  {
    return this.preorder;
  }
  
  public boolean isSequentialDimension()
  {
    return this.sequentialDimension;
  }
  
  public boolean isStatus()
  {
    return this.status;
  }
  
  public void setActionText(String paramString)
  {
    this.actionText = paramString;
  }
  
  public void setActualPrice(int paramInt)
  {
    this.actualPrice = paramInt;
  }
  
  public void setAddQtyUsingPrice(boolean paramBoolean)
  {
    this.addQtyUsingPrice = paramBoolean;
  }
  
  public void setAncestors(List<CJRGiftCardAncestor> paramList)
  {
    this.ancestors = paramList;
  }
  
  public void setAttributeChart(Object paramObject)
  {
    this.attributeChart = paramObject;
  }
  
  public void setAttributes(CJRGiftCardAttributes paramCJRGiftCardAttributes)
  {
    this.attributes = paramCJRGiftCardAttributes;
  }
  
  public void setAutocode(Object paramObject)
  {
    this.autocode = paramObject;
  }
  
  public void setBargainName(String paramString)
  {
    this.bargainName = paramString;
  }
  
  public void setBargainable(boolean paramBoolean)
  {
    this.bargainable = paramBoolean;
  }
  
  public void setBrand(String paramString)
  {
    this.brand = paramString;
  }
  
  public void setBrandId(int paramInt)
  {
    this.brandId = paramInt;
  }
  
  public void setBrandSellerUrl(String paramString)
  {
    this.brandSellerUrl = paramString;
  }
  
  public void setBulkPricing(boolean paramBoolean)
  {
    this.bulkPricing = paramBoolean;
  }
  
  public void setBulletPoints(Object paramObject)
  {
    this.bulletPoints = paramObject;
  }
  
  public void setButtonText(Object paramObject)
  {
    this.buttonText = paramObject;
  }
  
  public void setCategoryIds(List<Integer> paramList)
  {
    this.categoryIds = paramList;
  }
  
  public void setConvenienceFee(int paramInt)
  {
    this.convenienceFee = paramInt;
  }
  
  public void setDimensions(String paramString)
  {
    this.dimensions = paramString;
  }
  
  public void setDiscount(String paramString)
  {
    this.discount = paramString;
  }
  
  public void setEmiEnabled(boolean paramBoolean)
  {
    this.emiEnabled = paramBoolean;
  }
  
  public void setFilters(List<CJRGiftCardFilters> paramList)
  {
    this.filters = paramList;
  }
  
  public void setFulfilledByPaytm(int paramInt)
  {
    this.fulfilledByPaytm = paramInt;
  }
  
  public void setGaKey(String paramString)
  {
    this.gaKey = paramString;
  }
  
  public void setImageUrl(String paramString)
  {
    this.imageUrl = paramString;
  }
  
  public void setInputFields(List<CJRGiftCardUserInputField> paramList)
  {
    this.inputFields = paramList;
  }
  
  public void setInstock(boolean paramBoolean)
  {
    this.instock = paramBoolean;
  }
  
  public void setListingTag(List<Object> paramList)
  {
    this.listingTag = paramList;
  }
  
  public void setLongRichDesc(List<CJRGiftCardLongRichDesc> paramList)
  {
    this.longRichDesc = paramList;
  }
  
  public void setMerchant(CJRGiftCardMerchant paramCJRGiftCardMerchant)
  {
    this.merchant = paramCJRGiftCardMerchant;
  }
  
  public void setMerchantSku(String paramString)
  {
    this.merchantSku = paramString;
  }
  
  public void setMetaDescription(String paramString)
  {
    this.metaDescription = paramString;
  }
  
  public void setMetaKeyword(Object paramObject)
  {
    this.metaKeyword = paramObject;
  }
  
  public void setMetaTitle(String paramString)
  {
    this.metaTitle = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setNeedShipping(boolean paramBoolean)
  {
    this.needShipping = paramBoolean;
  }
  
  public void setNoFollow(boolean paramBoolean)
  {
    this.noFollow = paramBoolean;
  }
  
  public void setNoIndex(boolean paramBoolean)
  {
    this.noIndex = paramBoolean;
  }
  
  public void setOfferPrice(int paramInt)
  {
    this.offerPrice = paramInt;
  }
  
  public void setOfferUrl(String paramString)
  {
    this.offerUrl = paramString;
  }
  
  public void setOtherImages(List<Object> paramList)
  {
    this.otherImages = paramList;
  }
  
  public void setOtherRichDesc(Object paramObject)
  {
    this.otherRichDesc = paramObject;
  }
  
  public void setOtherSellers(CJRGiftCardOtherSellers paramCJRGiftCardOtherSellers)
  {
    this.otherSellers = paramCJRGiftCardOtherSellers;
  }
  
  public void setParentId(int paramInt)
  {
    this.parentId = paramInt;
  }
  
  public void setPayTypeSupported(CJRGiftCardPayTypeSupported paramCJRGiftCardPayTypeSupported)
  {
    this.payTypeSupported = paramCJRGiftCardPayTypeSupported;
  }
  
  public void setPincode(String paramString)
  {
    this.pincode = paramString;
  }
  
  public void setPreorder(boolean paramBoolean)
  {
    this.preorder = paramBoolean;
  }
  
  public void setProductCreatedDate(Object paramObject)
  {
    this.productCreatedDate = paramObject;
  }
  
  public void setProductEndDate(Object paramObject)
  {
    this.productEndDate = paramObject;
  }
  
  public void setProductId(int paramInt)
  {
    this.productId = paramInt;
  }
  
  public void setProductName(String paramString)
  {
    this.productName = paramString;
  }
  
  public void setProductRating(Object paramObject)
  {
    this.productRating = paramObject;
  }
  
  public void setProductType(String paramString)
  {
    this.productType = paramString;
  }
  
  public void setPromoText(Object paramObject)
  {
    this.promoText = paramObject;
  }
  
  public void setQuantity(int paramInt)
  {
    this.quantity = paramInt;
  }
  
  public void setSequentialDimension(boolean paramBoolean)
  {
    this.sequentialDimension = paramBoolean;
  }
  
  public void setShareurl(String paramString)
  {
    this.shareurl = paramString;
  }
  
  public void setShortDesc(String paramString)
  {
    this.shortDesc = paramString;
  }
  
  public void setStatus(boolean paramBoolean)
  {
    this.status = paramBoolean;
  }
  
  public void setTag(Object paramObject)
  {
    this.tag = paramObject;
  }
  
  public void setTermsConditions(CJRGiftCardTermsConditions paramCJRGiftCardTermsConditions)
  {
    this.termsConditions = paramCJRGiftCardTermsConditions;
  }
  
  public void setThumbnail(String paramString)
  {
    this.thumbnail = paramString;
  }
  
  public void setTotalScore(int paramInt)
  {
    this.totalScore = paramInt;
  }
  
  public void setUpcfields(List<Object> paramList)
  {
    this.upcfields = paramList;
  }
  
  public void setUrl(String paramString)
  {
    this.url = paramString;
  }
  
  public void setVerticalId(int paramInt)
  {
    this.verticalId = paramInt;
  }
  
  public void setVerticalLabel(String paramString)
  {
    this.verticalLabel = paramString;
  }
  
  public void setWeight(String paramString)
  {
    this.weight = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardPDPModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */