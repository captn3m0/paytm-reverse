package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftCardsStoreFrontModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="homepage_layout")
  private ArrayList<CJRGiftCardsFrontDetailModel> mStoreFrontItems = new ArrayList();
  
  public ArrayList<CJRGiftCardsFrontDetailModel> getmStoreFrontItems()
  {
    return this.mStoreFrontItems;
  }
  
  public void setmStoreFrontItems(ArrayList<CJRGiftCardsFrontDetailModel> paramArrayList)
  {
    this.mStoreFrontItems = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardsStoreFrontModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */