package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftCardCartVerifyRequestModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="cart_items")
  private List<CJRGiftCardCartItemModel> cart_items = new ArrayList();
  @b(a="promocode")
  private String promocode;
  
  public List<CJRGiftCardCartItemModel> getCart_items()
  {
    return this.cart_items;
  }
  
  public String getPromocode()
  {
    return this.promocode;
  }
  
  public void setCart_items(List<CJRGiftCardCartItemModel> paramList)
  {
    this.cart_items = paramList;
  }
  
  public void setPromocode(String paramString)
  {
    this.promocode = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardCartVerifyRequestModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */