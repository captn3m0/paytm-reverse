package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import java.io.Serializable;

public class CJRGiftCardLongRichDescAttributes
  implements Serializable
{
  @b(a="Brand")
  private String brand;
  @b(a="Conditions for Validity")
  private String conditionsForValidity;
  @b(a="Product Code")
  private String productCode;
  @b(a="Valid Till")
  private String validTill;
  
  public String getBrand()
  {
    return this.brand;
  }
  
  public String getConditionsForValidity()
  {
    return this.conditionsForValidity;
  }
  
  public String getProductCode()
  {
    return this.productCode;
  }
  
  public String getValidTill()
  {
    return this.validTill;
  }
  
  public void setBrand(String paramString)
  {
    this.brand = paramString;
  }
  
  public void setConditionsForValidity(String paramString)
  {
    this.conditionsForValidity = paramString;
  }
  
  public void setProductCode(String paramString)
  {
    this.productCode = paramString;
  }
  
  public void setValidTill(String paramString)
  {
    this.validTill = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardLongRichDescAttributes.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */