package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftCardExpressCartModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="aggregate_item_price")
  private Integer aggregate_item_price;
  @b(a="cart_items")
  private List<CJRGiftCardCartVerifyCartItemModel> cart_items = new ArrayList();
  @b(a="conv_fee")
  private Integer conv_fee;
  @b(a="count")
  private Integer count;
  @b(a="customer_id")
  private Integer customer_id;
  @b(a="final_price")
  private Integer final_price;
  @b(a="final_price_excl_shipping")
  private Integer final_price_excl_shipping;
  @b(a="need_shipping")
  private Boolean need_shipping;
  @b(a="order_total")
  private Integer order_total;
  @b(a="paytm_cashback")
  private Integer paytm_cashback;
  @b(a="paytm_discount")
  private Integer paytm_discount;
  @b(a="place_order_url")
  private String place_order_url;
  @b(a="promofailuretext")
  private String promofailuretext;
  @b(a="promostatus")
  private String promostatus;
  @b(a="promosuccesstext")
  private String promosuccesstext;
  @b(a="shipping_charges")
  private Integer shipping_charges;
  @b(a="site_id")
  private Integer site_id;
  @b(a="total_other_taxes")
  private List<Object> total_other_taxes = new ArrayList();
  
  public Integer getAggregate_item_price()
  {
    return this.aggregate_item_price;
  }
  
  public List<CJRGiftCardCartVerifyCartItemModel> getCart_items()
  {
    return this.cart_items;
  }
  
  public Integer getConv_fee()
  {
    return this.conv_fee;
  }
  
  public Integer getCount()
  {
    return this.count;
  }
  
  public Integer getCustomer_id()
  {
    return this.customer_id;
  }
  
  public Integer getFinal_price()
  {
    return this.final_price;
  }
  
  public Integer getFinal_price_excl_shipping()
  {
    return this.final_price_excl_shipping;
  }
  
  public Boolean getNeed_shipping()
  {
    return this.need_shipping;
  }
  
  public Integer getOrder_total()
  {
    return this.order_total;
  }
  
  public Integer getPaytm_cashback()
  {
    return this.paytm_cashback;
  }
  
  public Integer getPaytm_discount()
  {
    return this.paytm_discount;
  }
  
  public String getPlace_order_url()
  {
    return this.place_order_url;
  }
  
  public String getPromofailuretext()
  {
    return this.promofailuretext;
  }
  
  public String getPromostatus()
  {
    return this.promostatus;
  }
  
  public String getPromosuccesstext()
  {
    return this.promosuccesstext;
  }
  
  public Integer getShipping_charges()
  {
    return this.shipping_charges;
  }
  
  public Integer getSite_id()
  {
    return this.site_id;
  }
  
  public List<Object> getTotal_other_taxes()
  {
    return this.total_other_taxes;
  }
  
  public void setAggregate_item_price(Integer paramInteger)
  {
    this.aggregate_item_price = paramInteger;
  }
  
  public void setCart_items(List<CJRGiftCardCartVerifyCartItemModel> paramList)
  {
    this.cart_items = paramList;
  }
  
  public void setConv_fee(Integer paramInteger)
  {
    this.conv_fee = paramInteger;
  }
  
  public void setCount(Integer paramInteger)
  {
    this.count = paramInteger;
  }
  
  public void setCustomer_id(Integer paramInteger)
  {
    this.customer_id = paramInteger;
  }
  
  public void setFinal_price(Integer paramInteger)
  {
    this.final_price = paramInteger;
  }
  
  public void setFinal_price_excl_shipping(Integer paramInteger)
  {
    this.final_price_excl_shipping = paramInteger;
  }
  
  public void setNeed_shipping(Boolean paramBoolean)
  {
    this.need_shipping = paramBoolean;
  }
  
  public void setOrder_total(Integer paramInteger)
  {
    this.order_total = paramInteger;
  }
  
  public void setPaytm_cashback(Integer paramInteger)
  {
    this.paytm_cashback = paramInteger;
  }
  
  public void setPaytm_discount(Integer paramInteger)
  {
    this.paytm_discount = paramInteger;
  }
  
  public void setPlace_order_url(String paramString)
  {
    this.place_order_url = paramString;
  }
  
  public void setPromofailuretext(String paramString)
  {
    this.promofailuretext = paramString;
  }
  
  public void setPromostatus(String paramString)
  {
    this.promostatus = paramString;
  }
  
  public void setPromosuccesstext(String paramString)
  {
    this.promosuccesstext = paramString;
  }
  
  public void setShipping_charges(Integer paramInteger)
  {
    this.shipping_charges = paramInteger;
  }
  
  public void setSite_id(Integer paramInteger)
  {
    this.site_id = paramInteger;
  }
  
  public void setTotal_other_taxes(List<Object> paramList)
  {
    this.total_other_taxes = paramList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardExpressCartModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */