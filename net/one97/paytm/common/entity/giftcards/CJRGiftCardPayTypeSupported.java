package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftCardPayTypeSupported
  implements IJRDataModel
{
  @b(a="COD")
  private int cashOnDelivery;
  @b(a="CC")
  private int creditCard;
  @b(a="DC")
  private int debitCard;
  @b(a="EMI")
  private int eMI;
  @b(a="ESCROW")
  private int eSCROW;
  @b(a="NB")
  private int netBanking;
  @b(a="PPI")
  private int pPI;
  
  public int getCC()
  {
    return this.creditCard;
  }
  
  public int getCOD()
  {
    return this.cashOnDelivery;
  }
  
  public int getDC()
  {
    return this.debitCard;
  }
  
  public int getEMI()
  {
    return this.eMI;
  }
  
  public int getESCROW()
  {
    return this.eSCROW;
  }
  
  public int getNB()
  {
    return this.netBanking;
  }
  
  public int getPPI()
  {
    return this.pPI;
  }
  
  public void setCC(int paramInt)
  {
    this.creditCard = paramInt;
  }
  
  public void setCOD(int paramInt)
  {
    this.cashOnDelivery = paramInt;
  }
  
  public void setDC(int paramInt)
  {
    this.debitCard = paramInt;
  }
  
  public void setEMI(int paramInt)
  {
    this.eMI = paramInt;
  }
  
  public void setESCROW(int paramInt)
  {
    this.eSCROW = paramInt;
  }
  
  public void setNB(int paramInt)
  {
    this.netBanking = paramInt;
  }
  
  public void setPPI(int paramInt)
  {
    this.pPI = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardPayTypeSupported.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */