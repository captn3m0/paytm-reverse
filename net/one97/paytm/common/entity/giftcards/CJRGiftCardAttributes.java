package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftCardAttributes
  implements IJRDataModel
{
  @b(a="campaign")
  private String campaign;
  @b(a="cardtype")
  private String cardtype;
  @b(a="denomination_id")
  private String denominationId;
  @b(a="hash")
  private String hash;
  @b(a="occasion")
  private String occasion;
  @b(a="pricing")
  private String pricing;
  @b(a="redemption_type")
  private String redemptionType;
  @b(a="sku_id")
  private String skuId;
  
  public String getCampaign()
  {
    return this.campaign;
  }
  
  public String getCardtype()
  {
    return this.cardtype;
  }
  
  public String getDenominationId()
  {
    return this.denominationId;
  }
  
  public String getHash()
  {
    return this.hash;
  }
  
  public String getOccasion()
  {
    return this.occasion;
  }
  
  public String getPricing()
  {
    return this.pricing;
  }
  
  public String getRedemptionType()
  {
    return this.redemptionType;
  }
  
  public String getSkuId()
  {
    return this.skuId;
  }
  
  public void setCampaign(String paramString)
  {
    this.campaign = paramString;
  }
  
  public void setCardtype(String paramString)
  {
    this.cardtype = paramString;
  }
  
  public void setDenominationId(String paramString)
  {
    this.denominationId = paramString;
  }
  
  public void setHash(String paramString)
  {
    this.hash = paramString;
  }
  
  public void setOccasion(String paramString)
  {
    this.occasion = paramString;
  }
  
  public void setPricing(String paramString)
  {
    this.pricing = paramString;
  }
  
  public void setRedemptionType(String paramString)
  {
    this.redemptionType = paramString;
  }
  
  public void setSkuId(String paramString)
  {
    this.skuId = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardAttributes.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */