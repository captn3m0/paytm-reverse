package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftCardOtherSellers
  implements IJRDataModel
{
  @b(a="values")
  private List<Object> values = new ArrayList();
  
  public List<Object> getValues()
  {
    return this.values;
  }
  
  public void setValues(List<Object> paramList)
  {
    this.values = paramList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardOtherSellers.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */