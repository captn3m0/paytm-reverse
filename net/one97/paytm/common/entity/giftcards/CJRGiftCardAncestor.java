package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftCardAncestor
  implements IJRDataModel
{
  @b(a="id")
  private int id;
  @b(a="name")
  private String name;
  @b(a="seourl")
  private String seourl;
  @b(a="url_key")
  private String urlKey;
  @b(a="url_type")
  private String urlType;
  
  public int getId()
  {
    return this.id;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getSeourl()
  {
    return this.seourl;
  }
  
  public String getUrlKey()
  {
    return this.urlKey;
  }
  
  public String getUrlType()
  {
    return this.urlType;
  }
  
  public void setId(int paramInt)
  {
    this.id = paramInt;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setSeourl(String paramString)
  {
    this.seourl = paramString;
  }
  
  public void setUrlKey(String paramString)
  {
    this.urlKey = paramString;
  }
  
  public void setUrlType(String paramString)
  {
    this.urlType = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardAncestor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */