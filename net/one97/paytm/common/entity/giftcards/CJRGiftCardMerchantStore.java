package net.one97.paytm.common.entity.giftcards;

import com.google.c.a.a;
import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGiftCardMerchantStore
  implements IJRDataModel
{
  @a
  @b(a="store_image")
  private String storeImage;
  @a
  @b(a="store_name")
  private String storeName;
  
  public String getStoreImage()
  {
    return this.storeImage;
  }
  
  public String getStoreName()
  {
    return this.storeName;
  }
  
  public void setStoreImage(String paramString)
  {
    this.storeImage = paramString;
  }
  
  public void setStoreName(String paramString)
  {
    this.storeName = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/giftcards/CJRGiftCardMerchantStore.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */