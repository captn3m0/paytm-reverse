package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRLoginOTPOnCall
  implements IJRDataModel
{
  @b(a="message")
  private String mMessage;
  @b(a="state")
  private String mState;
  @b(a="status")
  private String mStatus;
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getState()
  {
    return this.mState;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRLoginOTPOnCall.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */