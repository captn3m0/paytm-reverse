package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class ErrorAdditionalInfo
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="confimation")
  private String confirmation;
  @b(a="confimation_flag")
  private String mConfirmationFlag;
  
  public String getConfirmation()
  {
    return this.confirmation;
  }
  
  public String getConfirmationFlag()
  {
    return this.mConfirmationFlag;
  }
  
  public void setConfirmation(String paramString)
  {
    this.confirmation = paramString;
  }
  
  public void setConfirmationFlag(String paramString)
  {
    this.mConfirmationFlag = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/ErrorAdditionalInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */