package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.HashMap;

public class CJRActionUrlRequest
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="ipAddress")
  private String mIpAddress;
  @b(a="metadata")
  private String mMetadata;
  @b(a="operationType")
  private String mOperationType;
  @b(a="platformName")
  private String mPlatformName;
  @b(a="request")
  private HashMap<Object, Object> mRequest;
  
  public String getIpAddress()
  {
    return this.mIpAddress;
  }
  
  public String getMetadata()
  {
    return this.mMetadata;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getOperationType()
  {
    return this.mOperationType;
  }
  
  public String getPlatformName()
  {
    return this.mPlatformName;
  }
  
  public HashMap<Object, Object> getRequest()
  {
    return this.mRequest;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRActionUrlRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */