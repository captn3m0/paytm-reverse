package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRBusComment
  implements IJRDataModel
{
  @b(a="age")
  private int age;
  @b(a="comment_edited")
  private String comment;
  @b(a="name")
  private String commenterName;
  @b(a="gender")
  private String gender;
  @b(a="travel_date")
  private String travelDate;
  
  public int getAge()
  {
    return this.age;
  }
  
  public String getComment()
  {
    return this.comment;
  }
  
  public String getCommenterName()
  {
    return this.commenterName;
  }
  
  public String getGender()
  {
    return this.gender;
  }
  
  public String getTravelDate()
  {
    return this.travelDate;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRBusComment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */