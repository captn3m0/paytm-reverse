package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRAuthCode
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="code")
  private String mCode;
  @b(a="displayMessage")
  private String mDisplayMessage;
  @b(a="email")
  private String mEmail;
  private int mHttpCode;
  @b(a="loginPassword")
  private String mLoginPassword;
  @b(a="mobileNumber")
  private String mMobileNo;
  @b(a="state")
  private String mState;
  @b(a="statusCode")
  private String mStatusCode;
  @b(a="statusMessage")
  private String mStatusMessage;
  
  public String getCode()
  {
    return this.mCode;
  }
  
  public String getDisplayMessage()
  {
    return this.mDisplayMessage;
  }
  
  public String getEmail()
  {
    return this.mEmail;
  }
  
  public int getHttpCode()
  {
    return this.mHttpCode;
  }
  
  public String getLoginPassword()
  {
    return this.mLoginPassword;
  }
  
  public String getMobileNo()
  {
    return this.mMobileNo;
  }
  
  public String getState()
  {
    return this.mState;
  }
  
  public String getStatusCode()
  {
    return this.mStatusCode;
  }
  
  public String getStatusMessage()
  {
    return this.mStatusMessage;
  }
  
  public void setHttpCode(int paramInt)
  {
    this.mHttpCode = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRAuthCode.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */