package net.one97.paytm.common.entity.auth;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class MergeChallenge
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="currentData")
  private String currentData;
  @b(a="code")
  private String mCode;
  @b(a="message")
  private String mMessage;
  @b(a="parkedState")
  private String mParkedState;
  @b(a="responseCode")
  private String mResponseCode;
  @b(a="state")
  private String mState;
  @b(a="status")
  private String mStatus;
  @b(a="valueList")
  private ArrayList<String> valueList;
  
  public String getCode()
  {
    return this.mCode;
  }
  
  public String getCurrentData()
  {
    return this.currentData;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getParkedState()
  {
    return this.mParkedState;
  }
  
  public String getResponseCode()
  {
    return this.mResponseCode;
  }
  
  public String getState()
  {
    return this.mState;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public ArrayList<String> getValueList()
  {
    return this.valueList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/auth/MergeChallenge.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */