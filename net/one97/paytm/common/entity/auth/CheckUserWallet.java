package net.one97.paytm.common.entity.auth;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CheckUserWallet
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="message")
  private String mMessage;
  @b(a="responseCode")
  private String mResponseCode;
  @b(a="state")
  private String mState;
  @b(a="status")
  private String mStatus;
  @b(a="walletType")
  private String mWalletType;
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getResponseCode()
  {
    return this.mResponseCode;
  }
  
  public String getState()
  {
    return this.mState;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public String getWalletType()
  {
    return this.mWalletType;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/auth/CheckUserWallet.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */