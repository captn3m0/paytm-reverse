package net.one97.paytm.common.entity.auth;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class KYCTncAccept
  implements IJRDataModel
{
  @b(a="message")
  private String message;
  @b(a="responseCode")
  private String responseCode;
  @b(a="status")
  private String status;
  
  public String getMessage()
  {
    return this.message;
  }
  
  public String getResponseCode()
  {
    return this.responseCode;
  }
  
  public String getStatus()
  {
    return this.status;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/auth/KYCTncAccept.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */