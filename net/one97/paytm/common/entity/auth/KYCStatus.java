package net.one97.paytm.common.entity.auth;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class KYCStatus
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="isKycUnderProcess")
  private String isKycUnderProcess;
  @b(a="is_verified_email")
  private int isVerifiedEmail;
  @b(a="is_verified_mobile")
  private int isVerifiedMobile;
  @b(a="message")
  private String message;
  @b(a="responseCode")
  private String responseCode;
  @b(a="status")
  private String status;
  @b(a="walletType")
  private String walletType;
  
  public String getIsKycUnderProcess()
  {
    return this.isKycUnderProcess;
  }
  
  public int getIsVerifiedEmail()
  {
    return this.isVerifiedEmail;
  }
  
  public int getIsVerifiedMobile()
  {
    return this.isVerifiedMobile;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public String getResponseCode()
  {
    return this.responseCode;
  }
  
  public String getStatus()
  {
    return this.status;
  }
  
  public String getWalletType()
  {
    return this.walletType;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/auth/KYCStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */