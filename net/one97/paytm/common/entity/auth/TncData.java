package net.one97.paytm.common.entity.auth;

import com.google.c.a.b;

public class TncData
{
  @b(a="code")
  private String code;
  @b(a="description")
  private String description;
  @b(a="status")
  private String status;
  @b(a="version")
  private int version;
  
  public String getCode()
  {
    return this.code;
  }
  
  public String getDescription()
  {
    return this.description;
  }
  
  public String getStatus()
  {
    return this.status;
  }
  
  public int getVersion()
  {
    return this.version;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/auth/TncData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */