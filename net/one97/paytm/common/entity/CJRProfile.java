package net.one97.paytm.common.entity;

import com.google.c.a.b;
import net.one97.paytm.common.entity.shopping.CJRCart;

public class CJRProfile
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="cart")
  private CJRCart mCart;
  @b(a="userinfo")
  private CJRUserInfo mUserInfo;
  @b(a="wallet")
  private CJRWallet mWallet;
  
  public CJRCart getCart()
  {
    return this.mCart;
  }
  
  public CJRUserInfo getUserInfo()
  {
    return this.mUserInfo;
  }
  
  public CJRWallet getWallet()
  {
    return this.mWallet;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRProfile.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */