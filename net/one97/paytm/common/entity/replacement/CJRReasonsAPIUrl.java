package net.one97.paytm.common.entity.replacement;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRReasonsAPIUrl
  implements IJRDataModel
{
  @b(a="file")
  private String file;
  @b(a="issue_id")
  private Integer issueId;
  @b(a="key")
  private String key;
  @b(a="value")
  private String value;
  
  public String getFile()
  {
    return this.file;
  }
  
  public Integer getIssueId()
  {
    return this.issueId;
  }
  
  public String getKey()
  {
    return this.key;
  }
  
  public String getValue()
  {
    return this.value;
  }
  
  public void setFile(String paramString)
  {
    this.file = paramString;
  }
  
  public void setIssueId(Integer paramInteger)
  {
    this.issueId = paramInteger;
  }
  
  public void setKey(String paramString)
  {
    this.key = paramString;
  }
  
  public void setValue(String paramString)
  {
    this.value = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/replacement/CJRReasonsAPIUrl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */