package net.one97.paytm.common.entity.replacement;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.Iterator;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRReplacementResponse
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="actions")
  private ArrayList<CJRReplacementAction> mActionList;
  @b(a="orderItemDetails")
  private CJROrderItemDetails mOrderItemDetails;
  @b(a="reasonObjs")
  private ArrayList<CJRReplacementReason> mReasonObjs;
  @b(a="replaceableOptions")
  private ArrayList<CJRReplaceableOptions> mReplaceableOptions;
  
  private void filterReasons(ArrayList<CJRReplacementReason> paramArrayList)
  {
    paramArrayList = paramArrayList.iterator();
    while (paramArrayList.hasNext())
    {
      CJRReplacementReason localCJRReplacementReason = (CJRReplacementReason)paramArrayList.next();
      if (localCJRReplacementReason.getIsReplacedOrder() == 0) {
        paramArrayList.remove();
      } else if ((localCJRReplacementReason.getChildList() != null) && (localCJRReplacementReason.getChildList().size() > 0)) {
        filterReasons(localCJRReplacementReason.getChildList());
      }
    }
  }
  
  public ArrayList<CJRReplacementAction> getActionList()
  {
    if (this.mActionList != null) {
      return this.mActionList;
    }
    return new ArrayList();
  }
  
  public ArrayList<CJRReplacementReason> getFilteredL1ReasonList()
  {
    if (this.mReasonObjs != null)
    {
      filterReasons(this.mReasonObjs);
      return this.mReasonObjs;
    }
    return new ArrayList();
  }
  
  public ArrayList<CJRReplacementReason> getL1ReasonList()
  {
    if (this.mReasonObjs != null) {
      return this.mReasonObjs;
    }
    return new ArrayList();
  }
  
  public CJROrderItemDetails getOrderItemDetails()
  {
    return this.mOrderItemDetails;
  }
  
  public ArrayList<CJRReplacementReason> getReasonObjs()
  {
    return this.mReasonObjs;
  }
  
  public ArrayList<CJRReplaceableOptions> getReplaceableOptions()
  {
    return this.mReplaceableOptions;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/replacement/CJRReplacementResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */