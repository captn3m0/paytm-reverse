package net.one97.paytm.common.entity.replacement;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;
import org.json.JSONException;
import org.json.JSONObject;

public class CJRCartInfo
  implements IJRDataModel
{
  private final String PRODUCT_ID = "product_id";
  @b(a="product_id")
  private String productId;
  
  public JSONObject getIssueJSON(long paramLong)
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("product_id", String.valueOf(paramLong));
      return localJSONObject;
    }
    catch (JSONException localJSONException)
    {
      localJSONException.printStackTrace();
    }
    return localJSONObject;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/replacement/CJRCartInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */