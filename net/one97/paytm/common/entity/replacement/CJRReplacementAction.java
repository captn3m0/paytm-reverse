package net.one97.paytm.common.entity.replacement;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRReplacementAction
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  private boolean isOverlayEnabled;
  private boolean isSelected;
  @b(a="label")
  private String mLabel;
  @b(a="name")
  private String mName;
  @b(a="redirectPG")
  private boolean mRedirectPG;
  @b(a="ui_control")
  private String mUiControl;
  @b(a="urlParams")
  private CJRReplacementURLParams mUrlParams;
  
  public String getActionName()
  {
    return this.mName;
  }
  
  public String getLabel()
  {
    return this.mLabel;
  }
  
  public String getUiControl()
  {
    return this.mUiControl;
  }
  
  public CJRReplacementURLParams getUrlParams()
  {
    return this.mUrlParams;
  }
  
  public boolean isOverlayEnabled()
  {
    return this.isOverlayEnabled;
  }
  
  public boolean isRedirectPG()
  {
    return this.mRedirectPG;
  }
  
  public boolean isSelected()
  {
    return this.isSelected;
  }
  
  public void setOverlayEnabled(boolean paramBoolean)
  {
    this.isOverlayEnabled = paramBoolean;
  }
  
  public void setSelected(boolean paramBoolean)
  {
    this.isSelected = paramBoolean;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/replacement/CJRReplacementAction.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */