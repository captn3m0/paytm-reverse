package net.one97.paytm.common.entity.replacement;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;
import org.json.JSONException;
import org.json.JSONObject;

public class CJRURLParamsBody
  implements IJRDataModel
{
  private final String CART_INFO = "cart_info";
  private final String CATEGORY_ID = "category_id";
  private final String COMMENT = "comment";
  private final String ISSUE_INFO = "issue_info";
  private final String REASON_ID = "reason_id";
  @b(a="cart_info")
  private CJRCartInfo cartInfo;
  @b(a="category_id")
  private String categoryId;
  @b(a="comment")
  private String comment;
  @b(a="issue_info")
  private CJRIssueInfo issueInfo;
  @b(a="reason_id")
  private String reasonId;
  
  public String prepareReplacePostBody(int paramInt, long paramLong)
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      if (this.issueInfo != null) {
        localJSONObject.put("issue_info", this.issueInfo.getIssueJSON(paramInt));
      }
      if ((this.cartInfo != null) && (paramLong >= 0L)) {
        localJSONObject.put("cart_info", this.cartInfo.getIssueJSON(paramLong));
      }
    }
    catch (JSONException localJSONException)
    {
      for (;;)
      {
        localJSONException.printStackTrace();
      }
    }
    return localJSONObject.toString();
  }
  
  public String prepareReturnPostBody(int paramInt1, int paramInt2, String paramString)
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("category_id", String.valueOf(paramInt1));
      localJSONObject.put("reason_id", String.valueOf(paramInt2));
      localJSONObject.put("comment", paramString);
      return localJSONObject.toString();
    }
    catch (JSONException paramString)
    {
      for (;;)
      {
        paramString.printStackTrace();
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/replacement/CJRURLParamsBody.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */