package net.one97.paytm.common.entity.replacement;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;
import org.json.JSONException;
import org.json.JSONObject;

public class CJRIssueInfo
  implements IJRDataModel
{
  private final String ISSUE_ID = "issue_id";
  @b(a="issue_id")
  private String issueId;
  
  public String getIssueId()
  {
    return this.issueId;
  }
  
  public JSONObject getIssueJSON(int paramInt)
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("issue_id", String.valueOf(paramInt));
      return localJSONObject;
    }
    catch (JSONException localJSONException)
    {
      localJSONException.printStackTrace();
    }
    return localJSONObject;
  }
  
  public void setIssueId(String paramString)
  {
    this.issueId = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/replacement/CJRIssueInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */