package net.one97.paytm.common.entity.replacement;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRReplaceableOptions
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="is_only_variant")
  private boolean isOnlyVariant;
  @b(a="filter_label")
  private String mFilterLabel;
  @b(a="filter_param")
  private String mFilterParam;
  @b(a="product_variants")
  private ArrayList<CJRProductVarient> mProductVarientList;
  
  public String getFilterLabel()
  {
    return this.mFilterLabel;
  }
  
  public String getFilterParam()
  {
    return this.mFilterParam;
  }
  
  public ArrayList<CJRProductVarient> getProductVarientList()
  {
    return this.mProductVarientList;
  }
  
  public boolean isOnlyVariant()
  {
    return this.isOnlyVariant;
  }
  
  public void setFilterLabel(String paramString)
  {
    this.mFilterLabel = paramString;
  }
  
  public void setFilterParam(String paramString)
  {
    this.mFilterParam = paramString;
  }
  
  public void setProductVarientList(ArrayList<CJRProductVarient> paramArrayList)
  {
    this.mProductVarientList = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/replacement/CJRReplaceableOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */