package net.one97.paytm.common.entity.replacement;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRReplacementURLParams
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="data")
  private CJRURLParamsBody mData;
  @b(a="method")
  private String mMethod;
  @b(a="url")
  private String mUrl;
  
  public CJRURLParamsBody getData()
  {
    return this.mData;
  }
  
  public String getMethod()
  {
    return this.mMethod;
  }
  
  public String getUrl()
  {
    return this.mUrl;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/replacement/CJRReplacementURLParams.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */