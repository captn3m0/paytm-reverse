package net.one97.paytm.common.entity.replacement;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CSTReasonsMetaData
  implements IJRDataModel
{
  @b(a="api_url_android")
  private CJRReasonsAPIUrl apiUrl;
  @b(a="our_suggestion")
  private CJRReasonsAPIUrl ourSuggestion;
  @b(a="api_url_android_recentorders_homepage")
  private CJRReasonsAPIUrl recentOrderUrl;
  
  public CJRReasonsAPIUrl getApiUrl()
  {
    return this.apiUrl;
  }
  
  public CJRReasonsAPIUrl getOurSuggestion()
  {
    return this.ourSuggestion;
  }
  
  public CJRReasonsAPIUrl getRecentOrderUrl()
  {
    return this.recentOrderUrl;
  }
  
  public void setApiUrl(CJRReasonsAPIUrl paramCJRReasonsAPIUrl)
  {
    this.apiUrl = paramCJRReasonsAPIUrl;
  }
  
  public void setOurSuggestion(CJRReasonsAPIUrl paramCJRReasonsAPIUrl)
  {
    this.ourSuggestion = paramCJRReasonsAPIUrl;
  }
  
  public void setRecentOrderUrl(CJRReasonsAPIUrl paramCJRReasonsAPIUrl)
  {
    this.recentOrderUrl = paramCJRReasonsAPIUrl;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/replacement/CSTReasonsMetaData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */