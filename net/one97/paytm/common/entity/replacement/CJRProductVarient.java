package net.one97.paytm.common.entity.replacement;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRProductVarient
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="product_variant_name")
  private String mName;
  @b(a="product_id")
  private long mProductId;
  
  public String getName()
  {
    return this.mName;
  }
  
  public long getProductId()
  {
    return this.mProductId;
  }
  
  public void setName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setProductId(long paramLong)
  {
    this.mProductId = paramLong;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/replacement/CJRProductVarient.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */