package net.one97.paytm.common.entity.replacement;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.CJRTransaction;
import net.one97.paytm.common.entity.IJRDataModel;
import net.one97.paytm.common.entity.shopping.CJROrderList;

public class CJRReplacementReason
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  private ArrayList<CJRTransaction> allTransaction;
  @b(a="cancellation_allowed")
  private int cancellationAllowed;
  @b(a="category_id")
  private int categoryId;
  @b(a="child")
  private ArrayList<CJRReplacementReason> childList;
  int childPos = -1;
  @b(a="comments")
  private String comments;
  @b(a="icon")
  private String icon;
  @b(a="id")
  private int id;
  @b(a="is_active")
  private int isActive;
  @b(a="is_admin_user")
  private int isAdminUser;
  @b(a="is_call_allowed")
  private int isCallAllowed;
  @b(a="is_customer_user")
  private int isCustomerUser;
  @b(a="is_email_allowed")
  private int isEmailAllowed;
  @b(a="is_fc_owner")
  private int isFcOwner;
  @b(a="is_merchant_owner")
  private int isMerchantOwner;
  @b(a="is_merchant_user")
  private int isMerchantUser;
  @b(a="is_paytm_owner")
  private int isPaytmOwner;
  @b(a="is_replaced_order")
  private int isReplacedOrder;
  boolean isSelected = false;
  @b(a="children")
  private ArrayList<CJRReplacementReason> issueReasonsList;
  @b(a="issue_text")
  private String issueText;
  @b(a="message")
  private String message;
  @b(a="meta_data")
  private CSTReasonsMetaData metaData;
  private ArrayList<CJROrderList> orderLists;
  @b(a="order_state")
  private int orderState;
  @b(a="out_of_stock")
  private int outOfStock;
  private ReplacementPageType pageType;
  @b(a="parent_issue_id")
  private int parentIssueId;
  int parentPos = -1;
  @b(a="raise_ticket_allowed")
  private int raiseTicketAllowed;
  @b(a="replacement_allowed")
  private int replacementAllowed;
  @b(a="return_allowed")
  private int returnAllowed;
  @b(a="vertical_id")
  private int verticalId;
  
  public ArrayList<CJRTransaction> getAllTransaction()
  {
    return this.allTransaction;
  }
  
  public int getCancellationAllowed()
  {
    return this.cancellationAllowed;
  }
  
  public int getCategoryId()
  {
    return this.categoryId;
  }
  
  public ArrayList<CJRReplacementReason> getChildList()
  {
    if (this.childList != null) {
      return this.childList;
    }
    if (this.issueReasonsList != null)
    {
      ArrayList localArrayList = this.issueReasonsList;
      this.childList = localArrayList;
      return localArrayList;
    }
    return new ArrayList();
  }
  
  public int getChildPos()
  {
    return this.childPos;
  }
  
  public String getComments()
  {
    return this.comments;
  }
  
  public String getIcon()
  {
    return this.icon;
  }
  
  public int getId()
  {
    return this.id;
  }
  
  public int getIsActive()
  {
    return this.isActive;
  }
  
  public int getIsAdminUser()
  {
    return this.isAdminUser;
  }
  
  public boolean getIsCallAllowed()
  {
    return this.isCallAllowed == 1;
  }
  
  public int getIsCustomerUser()
  {
    return this.isCustomerUser;
  }
  
  public boolean getIsEmailAllowed()
  {
    return this.isEmailAllowed == 1;
  }
  
  public int getIsFcOwner()
  {
    return this.isFcOwner;
  }
  
  public int getIsMerchantOwner()
  {
    return this.isMerchantOwner;
  }
  
  public int getIsMerchantUser()
  {
    return this.isMerchantUser;
  }
  
  public int getIsPaytmOwner()
  {
    return this.isPaytmOwner;
  }
  
  public int getIsReplacedOrder()
  {
    return this.isReplacedOrder;
  }
  
  public String getIssueText()
  {
    return this.issueText;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public CSTReasonsMetaData getMetaData()
  {
    return this.metaData;
  }
  
  public ArrayList<CJROrderList> getOrderLists()
  {
    return this.orderLists;
  }
  
  public int getOrderState()
  {
    return this.orderState;
  }
  
  public int getOutOfStock()
  {
    return this.outOfStock;
  }
  
  public ReplacementPageType getPageType()
  {
    return this.pageType;
  }
  
  public int getParentIssueId()
  {
    return this.parentIssueId;
  }
  
  public int getParentPos()
  {
    return this.parentPos;
  }
  
  public int getRaiseTicketAllowed()
  {
    return this.raiseTicketAllowed;
  }
  
  public int getReplacementAllowed()
  {
    return this.replacementAllowed;
  }
  
  public int getReturnAllowed()
  {
    return this.returnAllowed;
  }
  
  public int getVerticalId()
  {
    return this.verticalId;
  }
  
  public boolean isSelected()
  {
    return this.isSelected;
  }
  
  public void setAllTransaction(ArrayList<CJRTransaction> paramArrayList)
  {
    this.allTransaction = paramArrayList;
  }
  
  public void setChildPos(int paramInt)
  {
    this.childPos = paramInt;
  }
  
  public void setId(int paramInt)
  {
    this.id = paramInt;
  }
  
  public void setIsCallAllowed(boolean paramBoolean)
  {
    int i = 1;
    if (paramBoolean == true) {}
    for (;;)
    {
      this.isCallAllowed = i;
      return;
      i = 0;
    }
  }
  
  public void setIsEmailAllowed(boolean paramBoolean)
  {
    int i = 1;
    if (paramBoolean == true) {}
    for (;;)
    {
      this.isEmailAllowed = i;
      return;
      i = 0;
    }
  }
  
  public void setIssueText(String paramString)
  {
    this.issueText = paramString;
  }
  
  public void setMetaData(CSTReasonsMetaData paramCSTReasonsMetaData)
  {
    this.metaData = paramCSTReasonsMetaData;
  }
  
  public void setOrderLists(ArrayList<CJROrderList> paramArrayList)
  {
    this.orderLists = paramArrayList;
  }
  
  public void setPageType(ReplacementPageType paramReplacementPageType)
  {
    this.pageType = paramReplacementPageType;
  }
  
  public void setParentPos(int paramInt)
  {
    this.parentPos = paramInt;
  }
  
  public void setSelected(boolean paramBoolean)
  {
    this.isSelected = paramBoolean;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/replacement/CJRReplacementReason.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */