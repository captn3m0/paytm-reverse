package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRWallet
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="balance")
  private String mBalance;
  @b(a="otpRequired")
  private int mOtpRequired;
  @b(a="ownerGuid")
  private String mOwnerGuide;
  @b(a="PaytmcashBalance")
  private float mPaytmCashBalance;
  @b(a="txnPinRequired")
  private int mTxnPinRequired;
  
  public String getBalance()
  {
    return this.mBalance;
  }
  
  public int getOtpRequired()
  {
    return this.mOtpRequired;
  }
  
  public String getOwnerGuide()
  {
    return this.mOwnerGuide;
  }
  
  public float getPaytmCashBalance()
  {
    return this.mPaytmCashBalance;
  }
  
  public int getTxnPinRequired()
  {
    return this.mTxnPinRequired;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRWallet.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */