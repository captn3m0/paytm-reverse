package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRValidateEmailOtpStatus
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="error")
  private String mError;
  @b(a="message")
  private String mMessage;
  @b(a="responseCode")
  private String mResponseCode;
  @b(a="state")
  private String mState;
  @b(a="status")
  private String mStatus;
  @b(a="title")
  private String mTitle;
  
  public String getError()
  {
    return this.mError;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getState()
  {
    return this.mState;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
  
  public String getmResponseCode()
  {
    return this.mResponseCode;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRValidateEmailOtpStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */