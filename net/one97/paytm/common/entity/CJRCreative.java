package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRCreative
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="alt")
  private String mAlt;
  @b(a="height")
  private int mHeight;
  @b(a="media")
  private String mMedia;
  @b(a="tracking")
  private CJRTracking mTracking;
  @b(a="width")
  private int mWidth;
  
  public String getAlt()
  {
    return this.mAlt;
  }
  
  public int getHeight()
  {
    return this.mHeight;
  }
  
  public String getMedia()
  {
    return this.mMedia;
  }
  
  public String getName()
  {
    return null;
  }
  
  public CJRTracking getTracking()
  {
    return this.mTracking;
  }
  
  public int getWidth()
  {
    return this.mWidth;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRCreative.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */