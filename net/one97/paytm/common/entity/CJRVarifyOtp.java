package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRVarifyOtp
  implements IJRDataModel
{
  @b(a="code")
  private String mCode;
  @b(a="message")
  private String mMessage;
  @b(a="status")
  private String mStatus;
  @b(a="responseCode")
  private String responseCode;
  
  public String getCode()
  {
    return this.mCode;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getResponseCode()
  {
    return this.responseCode;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRVarifyOtp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */