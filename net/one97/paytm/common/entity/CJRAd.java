package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.ArrayList;

public class CJRAd
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="adid")
  private String mAdId;
  @b(a="adunitid")
  private String mAdUnitId;
  @b(a="creative")
  private ArrayList<CJRCreative> mCreativeList;
  @b(a="html")
  private String mHtml;
  @b(a="type")
  private String mType;
  
  protected Object clone()
    throws CloneNotSupportedException
  {
    return super.clone();
  }
  
  public String gemAdId()
  {
    return this.mAdId;
  }
  
  public String getAdUnitId()
  {
    return this.mAdUnitId;
  }
  
  public ArrayList<CJRCreative> getCreative()
  {
    return this.mCreativeList;
  }
  
  public String getHtml()
  {
    return this.mHtml;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getType()
  {
    return this.mType;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRAd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */