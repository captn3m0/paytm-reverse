package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRModalButton
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="modalactionPage")
  private String mModalActionPage;
  @b(a="modalactionUrl")
  private String mModalActionUrl;
  @b(a="modalactionUrlRequest")
  private CJRModalActionUrlRequest mModalActionUrlRequest;
  @b(a="modalbuttonAlignment")
  private String mModalButtonAlignment;
  @b(a="modalbuttonName")
  private String mModalButtonName;
  @b(a="modalrespAction")
  private String modalRespAction;
  
  public String getModalActionPage()
  {
    return this.mModalActionPage;
  }
  
  public String getModalActionUrl()
  {
    return this.mModalActionUrl;
  }
  
  public CJRModalActionUrlRequest getModalActionUrlRequest()
  {
    return this.mModalActionUrlRequest;
  }
  
  public String getModalButtonAlignment()
  {
    return this.mModalButtonAlignment;
  }
  
  public String getModalRespAction()
  {
    return this.modalRespAction;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getmModalButtonName()
  {
    return this.mModalButtonName;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRModalButton.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */