package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRConfiguration
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="circle")
  protected String mCircle;
  @b(a="error")
  private CJRError mError;
  @b(a="name")
  protected String mName;
  @b(a="product_id")
  protected String mProductId;
  @b(a="quantity")
  protected String mQuantity;
  @b(a="status")
  private String mStatus;
  @b(a="type")
  protected String mType;
  
  public String getCircle()
  {
    return this.mCircle;
  }
  
  public CJRError getError()
  {
    return this.mError;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public String getType()
  {
    return this.mType;
  }
  
  public String getmProductId()
  {
    return this.mProductId;
  }
  
  public String getmQuantity()
  {
    return this.mQuantity;
  }
  
  public void setCircle(String paramString)
  {
    this.mCircle = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRConfiguration.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */