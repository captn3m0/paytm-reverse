package net.one97.paytm.common.entity.SellerRating;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROrderSummaryRatingModel
  implements IJRDataModel
{
  @b(a="rating")
  private boolean mIsRated;
  @b(a="item_id")
  private long mItemId;
  @b(a="s3")
  private float mProductDeliveryRating;
  @b(a="s1")
  private float mProductDescRating;
  @b(a="s2")
  private float mProductPackagingRating;
  
  public boolean getIsRated()
  {
    return this.mIsRated;
  }
  
  public long getItemId()
  {
    return this.mItemId;
  }
  
  public float getProductDeliveryRating()
  {
    return this.mProductDeliveryRating;
  }
  
  public float getProductDescRating()
  {
    return this.mProductDescRating;
  }
  
  public float getProductPackagingRating()
  {
    return this.mProductPackagingRating;
  }
  
  public void setIsRated(boolean paramBoolean)
  {
    this.mIsRated = paramBoolean;
  }
  
  public void setItemId(long paramLong)
  {
    this.mItemId = paramLong;
  }
  
  public void setProductDeliveryRating(float paramFloat)
  {
    this.mProductDeliveryRating = paramFloat;
  }
  
  public void setProductDescRating(float paramFloat)
  {
    this.mProductDescRating = paramFloat;
  }
  
  public void setProductPackagingRating(float paramFloat)
  {
    this.mProductPackagingRating = paramFloat;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/SellerRating/CJROrderSummaryRatingModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */