package net.one97.paytm.common.entity.SellerRating;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class RatingData
  implements IJRDataModel
{
  @b(a="rating")
  private String mIRatingData;
  @b(a="text")
  private String mText;
  
  public String getmIRatingData()
  {
    return this.mIRatingData;
  }
  
  public String getmText()
  {
    return this.mText;
  }
  
  public void setmIRatingData(String paramString)
  {
    this.mIRatingData = paramString;
  }
  
  public void setmText(String paramString)
  {
    this.mText = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/SellerRating/RatingData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */