package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.ArrayList;

public class CJRQueryListTabContainer
  implements IJRDataModel
{
  @b(a="items")
  private ArrayList<CJRQueryListTab> items;
  
  public ArrayList<CJRQueryListTab> getQueryListTabs()
  {
    return this.items;
  }
  
  public void setQueryListTabs(ArrayList<CJRQueryListTab> paramArrayList)
  {
    this.items = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRQueryListTabContainer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */