package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRButtonRequest
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="description")
  private String mDescription;
  @b(a="label")
  private String mLabel;
  @b(a="requestActionDesc")
  private CJRRequestActionDesc mRequestActionDesc;
  @b(a="submitActionUrl")
  private String mSubmitActionUrl;
  
  public String getDescription()
  {
    return this.mDescription;
  }
  
  public String getLabel()
  {
    return this.mLabel;
  }
  
  public String getName()
  {
    return null;
  }
  
  public CJRRequestActionDesc getRequestActionDesc()
  {
    return this.mRequestActionDesc;
  }
  
  public String getSubmitActionUrl()
  {
    return this.mSubmitActionUrl;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRButtonRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */