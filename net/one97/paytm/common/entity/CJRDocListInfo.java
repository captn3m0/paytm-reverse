package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRDocListInfo
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="docCode")
  private String mDocCode;
  @b(a="docId")
  private String mDocId;
  
  public String getDocCode()
  {
    return this.mDocCode;
  }
  
  public String getDocId()
  {
    return this.mDocId;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRDocListInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */