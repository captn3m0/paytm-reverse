package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRMessage
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="last_message")
  private String mLastMessage;
  @b(a="merchant_id")
  private String mMerchantId;
  @b(a="merchant_name")
  private String mMerchantName;
  @b(a="merchant_plustxt_id")
  private String mMerchantPlustxtId;
  
  public String getLastMessage()
  {
    return this.mLastMessage;
  }
  
  public String getMerchantId()
  {
    return this.mMerchantId;
  }
  
  public String getMerchantName()
  {
    return this.mMerchantName;
  }
  
  public String getMerchantPlustxtId()
  {
    return this.mMerchantPlustxtId;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRMessage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */