package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRSimilarProduct
  implements IJRDataModel
{
  @b(a="id")
  private String mId;
  @b(a="mrp")
  private String mMrp;
  @b(a="name")
  private String mName;
  @b(a="price")
  private String mPrice;
  @b(a="tag")
  private String mTag;
  @b(a="thumbnail")
  private String mThumbnail;
  
  public String getId()
  {
    return this.mId;
  }
  
  public String getMrp()
  {
    return this.mMrp;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getPrice()
  {
    return this.mPrice;
  }
  
  public String getTag()
  {
    return this.mTag;
  }
  
  public String getThumbnail()
  {
    return this.mThumbnail;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRSimilarProduct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */