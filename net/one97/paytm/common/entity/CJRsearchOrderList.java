package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.ArrayList;

public class CJRsearchOrderList
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="error")
  private CJRError mError;
  @b(a="next")
  private String mNextUrl;
  @b(a="orders")
  private ArrayList<CJROrder> mOrderList;
  @b(a="status")
  private CJRStatusError mStatus;
  
  public void addNewItems(ArrayList<CJROrder> paramArrayList)
  {
    if (this.mOrderList == null)
    {
      this.mOrderList = paramArrayList;
      return;
    }
    this.mOrderList.addAll(paramArrayList);
  }
  
  public CJRError getError()
  {
    return this.mError;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getNextUrl()
  {
    return this.mNextUrl;
  }
  
  public ArrayList<CJROrder> getOrderList()
  {
    return this.mOrderList;
  }
  
  public CJRStatusError getStatus()
  {
    return this.mStatus;
  }
  
  public void setNextUrl(String paramString)
  {
    this.mNextUrl = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRsearchOrderList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */