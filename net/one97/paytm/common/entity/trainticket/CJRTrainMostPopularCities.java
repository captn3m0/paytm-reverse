package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;
import net.one97.paytm.common.entity.train.CJRTrainOriginCityItem;

public class CJRTrainMostPopularCities
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="body")
  private ArrayList<CJRTrainOriginCityItem> mTrainOriginCityItems;
  
  public ArrayList<CJRTrainOriginCityItem> getmTrainOriginCityItems()
  {
    return this.mTrainOriginCityItems;
  }
  
  public void setmTrainOriginCityItems(ArrayList<CJRTrainOriginCityItem> paramArrayList)
  {
    this.mTrainOriginCityItems = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRTrainMostPopularCities.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */