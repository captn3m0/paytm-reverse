package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainBoardingStation
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="day_count")
  private String mDayCount;
  @b(a="departure_time")
  private String mDepartureTime;
  @b(a="station_code")
  private String mStationCode;
  @b(a="station_name")
  private String mStationName;
  
  public String getmDayCount()
  {
    return this.mDayCount;
  }
  
  public String getmDepartureTime()
  {
    return this.mDepartureTime;
  }
  
  public String getmStationCode()
  {
    return this.mStationCode;
  }
  
  public String getmStationName()
  {
    return this.mStationName;
  }
  
  public void setmDayCount(String paramString)
  {
    this.mDayCount = paramString;
  }
  
  public void setmDepartureTime(String paramString)
  {
    this.mDepartureTime = paramString;
  }
  
  public void setmStationCode(String paramString)
  {
    this.mStationCode = paramString;
  }
  
  public void setmStationName(String paramString)
  {
    this.mStationName = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRTrainBoardingStation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */