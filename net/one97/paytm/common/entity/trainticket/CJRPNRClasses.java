package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPNRClasses
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="3A")
  private String mAC3Tier;
  @b(a="3E")
  private String mAC3TierEconomy;
  @b(a="CC")
  private String mACChairCar;
  @b(a="2A")
  private String mACSleeper;
  @b(a="1A")
  private String mFirstAcAndExecutive;
  @b(a="FC")
  private String mFirstClass;
  @b(a="2S")
  private String mSecondClassSeats;
  @b(a="SL")
  private String mSleeperClass;
  
  public String getmAC3Tier()
  {
    return this.mAC3Tier;
  }
  
  public String getmAC3TierEconomy()
  {
    return this.mAC3TierEconomy;
  }
  
  public String getmACChairCar()
  {
    return this.mACChairCar;
  }
  
  public String getmACSleeper()
  {
    return this.mACSleeper;
  }
  
  public String getmFirstAcAndExecutive()
  {
    return this.mFirstAcAndExecutive;
  }
  
  public String getmFirstClass()
  {
    return this.mFirstClass;
  }
  
  public String getmSecondClassSeats()
  {
    return this.mSecondClassSeats;
  }
  
  public String getmSleeperClass()
  {
    return this.mSleeperClass;
  }
  
  public void setmAC3Tier(String paramString)
  {
    this.mAC3Tier = paramString;
  }
  
  public void setmAC3TierEconomy(String paramString)
  {
    this.mAC3TierEconomy = paramString;
  }
  
  public void setmACChairCar(String paramString)
  {
    this.mACChairCar = paramString;
  }
  
  public void setmACSleeper(String paramString)
  {
    this.mACSleeper = paramString;
  }
  
  public void setmFirstAcAndExecutive(String paramString)
  {
    this.mFirstAcAndExecutive = paramString;
  }
  
  public void setmFirstClass(String paramString)
  {
    this.mFirstClass = paramString;
  }
  
  public void setmSecondClassSeats(String paramString)
  {
    this.mSecondClassSeats = paramString;
  }
  
  public void setmSleeperClass(String paramString)
  {
    this.mSleeperClass = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRPNRClasses.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */