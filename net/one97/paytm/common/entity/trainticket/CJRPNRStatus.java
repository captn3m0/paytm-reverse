package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPNRStatus
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="message")
  private CJRPNRMessage mMessagePNR;
  @b(a="result")
  private String mResult;
  
  public CJRPNRMessage getmMessagePNR()
  {
    return this.mMessagePNR;
  }
  
  public String getmResult()
  {
    return this.mResult;
  }
  
  public void setmMessagePNR(CJRPNRMessage paramCJRPNRMessage)
  {
    this.mMessagePNR = paramCJRPNRMessage;
  }
  
  public void setmResult(String paramString)
  {
    this.mResult = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRPNRStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */