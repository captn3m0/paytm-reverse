package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRIRCTCUserStatus
  implements IJRDataModel
{
  @b(a="status")
  private IRCTCUserStaus mStatus;
  @b(a="body")
  private IRCTCUserBody mUserBody;
  
  public IRCTCUserStaus getStatus()
  {
    return this.mStatus;
  }
  
  public IRCTCUserBody getUserBody()
  {
    return this.mUserBody;
  }
  
  public void setStatus(IRCTCUserStaus paramIRCTCUserStaus)
  {
    this.mStatus = paramIRCTCUserStaus;
  }
  
  public void setUserBody(IRCTCUserBody paramIRCTCUserBody)
  {
    this.mUserBody = paramIRCTCUserBody;
  }
  
  public class IRCTCUserBody
  {
    @b(a="email")
    private String mEmail;
    @b(a="enabled")
    private String mEnabled;
    @b(a="mobile")
    private String mMobile;
    @b(a="userId")
    private String mUserId;
    @b(a="verified")
    private String mVarified;
    
    public IRCTCUserBody() {}
    
    public String getEnabled()
    {
      return this.mEnabled;
    }
    
    public String getMobile()
    {
      return this.mMobile;
    }
    
    public String getUserId()
    {
      return this.mUserId;
    }
    
    public String getVarified()
    {
      return this.mVarified;
    }
    
    public String getmEmail()
    {
      return this.mEmail;
    }
    
    public void setEnabled(String paramString)
    {
      this.mEnabled = paramString;
    }
    
    public void setMobile(String paramString)
    {
      this.mMobile = paramString;
    }
    
    public void setUserId(String paramString)
    {
      this.mUserId = paramString;
    }
    
    public void setVarified(String paramString)
    {
      this.mVarified = paramString;
    }
    
    public void setmEmail(String paramString)
    {
      this.mEmail = paramString;
    }
  }
  
  public class IRCTCUserMessage
  {
    @b(a="message")
    private String mMessage;
    @b(a="title")
    private String mTittle;
    
    public IRCTCUserMessage() {}
    
    public String getMessage()
    {
      return this.mMessage;
    }
    
    public String getTittle()
    {
      return this.mTittle;
    }
    
    public void setMessage(String paramString)
    {
      this.mMessage = paramString;
    }
    
    public void setTittle(String paramString)
    {
      this.mTittle = paramString;
    }
  }
  
  public class IRCTCUserStaus
  {
    @b(a="result")
    private String mResult;
    @b(a="message")
    private CJRIRCTCUserStatus.IRCTCUserMessage mUserMessage;
    
    public IRCTCUserStaus() {}
    
    public String getResult()
    {
      return this.mResult;
    }
    
    public CJRIRCTCUserStatus.IRCTCUserMessage getUserMessage()
    {
      return this.mUserMessage;
    }
    
    public void setResult(String paramString)
    {
      this.mResult = paramString;
    }
    
    public void setUserMessage(CJRIRCTCUserStatus.IRCTCUserMessage paramIRCTCUserMessage)
    {
      this.mUserMessage = paramIRCTCUserMessage;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRIRCTCUserStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */