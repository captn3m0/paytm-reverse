package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRCountryCode
  implements IJRDataModel
{
  @b(a="Nationality code")
  private String mCountryCode;
  @b(a="Nationality name")
  private String mCountryName;
  
  public String getCountryCode()
  {
    return this.mCountryCode;
  }
  
  public String getCountryName()
  {
    return this.mCountryName;
  }
  
  public void setCountryCode(String paramString)
  {
    this.mCountryCode = paramString;
  }
  
  public void setCountryName(String paramString)
  {
    this.mCountryName = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRCountryCode.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */