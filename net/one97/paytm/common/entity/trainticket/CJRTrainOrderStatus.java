package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainOrderStatus
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="status")
  private CJROrderStatus mOrderStatus;
  @b(a="body")
  private CJROrderStatusBody mOrderStatusBody;
  
  public CJROrderStatus getOrderStatus()
  {
    return this.mOrderStatus;
  }
  
  public CJROrderStatusBody getOrderStatusBody()
  {
    return this.mOrderStatusBody;
  }
  
  public void setmOrderStatus(CJROrderStatus paramCJROrderStatus)
  {
    this.mOrderStatus = paramCJROrderStatus;
  }
  
  public void setmOrderStatusBody(CJROrderStatusBody paramCJROrderStatusBody)
  {
    this.mOrderStatusBody = paramCJROrderStatusBody;
  }
  
  public class CJROrderPaymentStatus
  {
    @b(a="status")
    private String mPaymentStatus;
    
    public CJROrderPaymentStatus() {}
    
    public String getPaymentStatus()
    {
      return this.mPaymentStatus;
    }
  }
  
  public class CJROrderStatus
  {
    @b(a="result")
    private String mResult;
    @b(a="message")
    private CJRTrainOrderStatus.CJROrderStatusMessage mVerifyOTPMessage;
    
    public CJROrderStatus() {}
    
    public String getResult()
    {
      return this.mResult;
    }
    
    public CJRTrainOrderStatus.CJROrderStatusMessage getVerifyOTPMessage()
    {
      return this.mVerifyOTPMessage;
    }
    
    public void setmResult(String paramString)
    {
      this.mResult = paramString;
    }
    
    public void setmVerifyOTPMessage(CJRTrainOrderStatus.CJROrderStatusMessage paramCJROrderStatusMessage)
    {
      this.mVerifyOTPMessage = paramCJROrderStatusMessage;
    }
  }
  
  public class CJROrderStatusBody
  {
    @b(a="irctc_redirect")
    private CJRIRCTCRedirect mIRCTCRedirect;
    @b(a="payment_status")
    private CJRTrainOrderStatus.CJROrderPaymentStatus mOrderPaymentStatus;
    
    public CJROrderStatusBody() {}
    
    public CJRIRCTCRedirect getIRCTCRedirect()
    {
      return this.mIRCTCRedirect;
    }
    
    public CJRTrainOrderStatus.CJROrderPaymentStatus getOrderPaymentStatus()
    {
      return this.mOrderPaymentStatus;
    }
  }
  
  public class CJROrderStatusMessage
  {
    @b(a="message")
    private String mMessage;
    @b(a="title")
    private String mTilte;
    
    public CJROrderStatusMessage() {}
    
    public String getMessage()
    {
      return this.mMessage;
    }
    
    public String getTilte()
    {
      return this.mTilte;
    }
    
    public void setmMessage(String paramString)
    {
      this.mMessage = paramString;
    }
    
    public void setmTilte(String paramString)
    {
      this.mTilte = paramString;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRTrainOrderStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */