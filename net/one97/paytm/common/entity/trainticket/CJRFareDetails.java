package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFareDetails
  implements IJRDataModel
{
  @b(a="irctc_service_fee")
  private String irctcServiceFee;
  @b(a="paytm_service_fee")
  private String paytmServiceFee;
  @b(a="pg_charge")
  private String pgCharge;
  @b(a="total_collectible")
  private String totalCollectible;
  @b(a="total_fare")
  private String totalFare;
  @b(a="grand_total")
  private String tranGrandTotal;
  
  public String getIrctcServiceFee()
  {
    return this.irctcServiceFee;
  }
  
  public String getPaytmServiceFee()
  {
    return this.paytmServiceFee;
  }
  
  public String getPgCharge()
  {
    return this.pgCharge;
  }
  
  public String getTotalCollectible()
  {
    return this.totalCollectible;
  }
  
  public String getTotalFare()
  {
    return this.totalFare;
  }
  
  public String getTranGrandTotal()
  {
    return this.tranGrandTotal;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRFareDetails.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */