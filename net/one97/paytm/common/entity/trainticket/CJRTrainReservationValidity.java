package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainReservationValidity
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="arrival_time")
  private String mArrivalTime;
  @b(a="day_count")
  private String mDayCount;
  @b(a="station_code")
  private String mStationCode;
  @b(a="station_name")
  private String mStationName;
  
  public String getmArrivalTime()
  {
    return this.mArrivalTime;
  }
  
  public String getmDayCount()
  {
    return this.mDayCount;
  }
  
  public String getmStationCode()
  {
    return this.mStationCode;
  }
  
  public String getmStationName()
  {
    return this.mStationName;
  }
  
  public void setmArrivalTime(String paramString)
  {
    this.mArrivalTime = paramString;
  }
  
  public void setmDayCount(String paramString)
  {
    this.mDayCount = paramString;
  }
  
  public void setmStationCode(String paramString)
  {
    this.mStationCode = paramString;
  }
  
  public void setmStationName(String paramString)
  {
    this.mStationName = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRTrainReservationValidity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */