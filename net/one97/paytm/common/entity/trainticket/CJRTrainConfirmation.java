package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;
import net.one97.paytm.common.entity.recharge.CJRRechargePayment;
import net.one97.paytm.common.entity.train.CJRTrainDetailsBody;

public class CJRTrainConfirmation
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  private CJRRechargePayment mRechargePayment;
  @b(a="train_details")
  private CJRTrainDetailsBody mTrainDetails;
  @b(a="transaction_id")
  private String mTransactionId;
  
  public CJRRechargePayment getPaymentInfo()
  {
    return this.mRechargePayment;
  }
  
  public CJRTrainDetailsBody getmTrainDetails()
  {
    return this.mTrainDetails;
  }
  
  public String getmTransactionId()
  {
    return this.mTransactionId;
  }
  
  public void setPaymentInfo(CJRRechargePayment paramCJRRechargePayment)
  {
    this.mRechargePayment = paramCJRRechargePayment;
  }
  
  public void setmTrainDetails(CJRTrainDetailsBody paramCJRTrainDetailsBody)
  {
    this.mTrainDetails = paramCJRTrainDetailsBody;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRTrainConfirmation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */