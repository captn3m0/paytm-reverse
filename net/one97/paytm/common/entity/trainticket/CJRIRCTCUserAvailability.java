package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRIRCTCUserAvailability
  implements IJRDataModel
{
  @b(a="error")
  private String mError;
  @b(a="body")
  private UserAvailabilityBody mUserAvailabilityBody;
  
  public String getError()
  {
    return this.mError;
  }
  
  public UserAvailabilityBody getUserAvailabilityBody()
  {
    return this.mUserAvailabilityBody;
  }
  
  public void setError(String paramString)
  {
    this.mError = paramString;
  }
  
  public void setUserAvailabilityBody(UserAvailabilityBody paramUserAvailabilityBody)
  {
    this.mUserAvailabilityBody = paramUserAvailabilityBody;
  }
  
  public class UserAvailabilityBody
  {
    @b(a="email")
    private String mMailId;
    @b(a="mobile")
    private String mPhoneNumber;
    @b(a="userId")
    private String mUserId;
    
    public UserAvailabilityBody() {}
    
    public String getMailId()
    {
      return this.mMailId;
    }
    
    public String getPhoneNumber()
    {
      return this.mPhoneNumber;
    }
    
    public String getUserId()
    {
      return this.mUserId;
    }
    
    public void semUserId(String paramString)
    {
      this.mUserId = paramString;
    }
    
    public void setMailId(String paramString)
    {
      this.mMailId = paramString;
    }
    
    public void setPhoneNumber(String paramString)
    {
      this.mPhoneNumber = paramString;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRIRCTCUserAvailability.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */