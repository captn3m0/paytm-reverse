package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPNRBookingStatus
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="CKWL")
  private String mCKWLStatus;
  @b(a="CNF")
  private String mCNFstatus;
  @b(a="PQWL")
  private String mPQWLStatus;
  @b(a="RAC")
  private String mRACStatus;
  @b(a="RLWL")
  private String mRLWLStatus;
  @b(a="RQWL")
  private String mRQWLStatus;
  
  public String getmCKWLStatus()
  {
    return this.mCKWLStatus;
  }
  
  public String getmCNFstatus()
  {
    return this.mCNFstatus;
  }
  
  public String getmPQWLStatus()
  {
    return this.mPQWLStatus;
  }
  
  public String getmRACStatus()
  {
    return this.mRACStatus;
  }
  
  public String getmRLWLStatus()
  {
    return this.mRLWLStatus;
  }
  
  public String getmRQWLStatus()
  {
    return this.mRQWLStatus;
  }
  
  public void setmCKWLStatus(String paramString)
  {
    this.mCKWLStatus = paramString;
  }
  
  public void setmCNFstatus(String paramString)
  {
    this.mCNFstatus = paramString;
  }
  
  public void setmPQWLStatus(String paramString)
  {
    this.mPQWLStatus = paramString;
  }
  
  public void setmRACStatus(String paramString)
  {
    this.mRACStatus = paramString;
  }
  
  public void setmRLWLStatus(String paramString)
  {
    this.mRLWLStatus = paramString;
  }
  
  public void setmRQWLStatus(String paramString)
  {
    this.mRQWLStatus = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRPNRBookingStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */