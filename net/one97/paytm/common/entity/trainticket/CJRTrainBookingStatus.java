package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainBookingStatus
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="homepage")
  private ArrayList<CJRNotification> mHomeNotification;
  @b(a="notification_status_homepage")
  private String mHomePageStatus;
  @b(a="search")
  private ArrayList<CJRNotification> mSearchNotification;
  @b(a="notification_status_search")
  private String mSearchPageStatus;
  
  public ArrayList<CJRNotification> getmHomeNotification()
  {
    return this.mHomeNotification;
  }
  
  public String getmHomePageStatus()
  {
    return this.mHomePageStatus;
  }
  
  public ArrayList<CJRNotification> getmSearchNotification()
  {
    return this.mSearchNotification;
  }
  
  public String getmSearchPageStatus()
  {
    return this.mSearchPageStatus;
  }
  
  public void setmHomeNotification(ArrayList<CJRNotification> paramArrayList)
  {
    this.mHomeNotification = paramArrayList;
  }
  
  public void setmHomePageStatus(String paramString)
  {
    this.mHomePageStatus = paramString;
  }
  
  public void setmSearchNotification(ArrayList<CJRNotification> paramArrayList)
  {
    this.mSearchNotification = paramArrayList;
  }
  
  public void setmSearchPageStatus(String paramString)
  {
    this.mSearchPageStatus = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRTrainBookingStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */