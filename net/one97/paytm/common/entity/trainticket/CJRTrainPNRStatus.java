package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainPNRStatus
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="code")
  private String mCode;
  @b(a="error")
  private String mError;
  @b(a="meta")
  private CJRPNRStatusMeta mMeta;
  @b(a="body")
  private CJRPNRStatusDetails mPNRStatusDetails;
  @b(a="status")
  private CJRPNRStatus mStatus;
  
  public CJRPNRStatusDetails getPNRStatusDetails()
  {
    return this.mPNRStatusDetails;
  }
  
  public String getmCode()
  {
    return this.mCode;
  }
  
  public String getmError()
  {
    return this.mError;
  }
  
  public CJRPNRStatusMeta getmMeta()
  {
    return this.mMeta;
  }
  
  public CJRPNRStatus getmStatus()
  {
    return this.mStatus;
  }
  
  public void setmCode(String paramString)
  {
    this.mCode = paramString;
  }
  
  public void setmError(String paramString)
  {
    this.mError = paramString;
  }
  
  public void setmMeta(CJRPNRStatusMeta paramCJRPNRStatusMeta)
  {
    this.mMeta = paramCJRPNRStatusMeta;
  }
  
  public void setmPNRStatusDetails(CJRPNRStatusDetails paramCJRPNRStatusDetails)
  {
    this.mPNRStatusDetails = paramCJRPNRStatusDetails;
  }
  
  public void setmStatus(CJRPNRStatus paramCJRPNRStatus)
  {
    this.mStatus = paramCJRPNRStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRTrainPNRStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */