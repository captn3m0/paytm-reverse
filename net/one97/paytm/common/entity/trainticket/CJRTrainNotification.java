package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainNotification
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="enabled")
  private String mNotificationEnabled;
  @b(a="message")
  private String mNotificationMessage;
  @b(a="priority")
  private String mNotificationPriority;
  
  public String getmNotificationEnabled()
  {
    return this.mNotificationEnabled;
  }
  
  public String getmNotificationMessage()
  {
    return this.mNotificationMessage;
  }
  
  public String getmNotificationPriority()
  {
    return this.mNotificationPriority;
  }
  
  public void setmNotificationEnabled(String paramString)
  {
    this.mNotificationEnabled = paramString;
  }
  
  public void setmNotificationMessage(String paramString)
  {
    this.mNotificationMessage = paramString;
  }
  
  public void setmNotificationPriority(String paramString)
  {
    this.mNotificationPriority = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRTrainNotification.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */