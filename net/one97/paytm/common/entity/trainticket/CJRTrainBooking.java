package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainBooking
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="body")
  private CJRTrainConfirmation mConfirmation;
  
  public CJRTrainConfirmation getmConfirmation()
  {
    return this.mConfirmation;
  }
  
  public void setmConfirmation(CJRTrainConfirmation paramCJRTrainConfirmation)
  {
    this.mConfirmation = paramCJRTrainConfirmation;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRTrainBooking.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */