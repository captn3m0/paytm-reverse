package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainOfferDetails
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="conditions")
  private String mConditions;
  @b(a="id")
  private int mId;
  @b(a="promo_code")
  private String mPromoCode;
  @b(a="promo_title")
  private String mPromoTitle;
  
  public String getmConditions()
  {
    return this.mConditions;
  }
  
  public int getmId()
  {
    return this.mId;
  }
  
  public String getmPromoCode()
  {
    return this.mPromoCode;
  }
  
  public String getmPromoTitle()
  {
    return this.mPromoTitle;
  }
  
  public void setmConditions(String paramString)
  {
    this.mConditions = paramString;
  }
  
  public void setmId(int paramInt)
  {
    this.mId = paramInt;
  }
  
  public void setmPromoCode(String paramString)
  {
    this.mPromoCode = paramString;
  }
  
  public void setmPromoTitle(String paramString)
  {
    this.mPromoTitle = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRTrainOfferDetails.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */