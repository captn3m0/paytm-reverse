package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRCountryCodeBody
  implements IJRDataModel
{
  @b(a="countryList")
  private ArrayList<CJRCountryCode> mCountryList;
  
  public ArrayList<CJRCountryCode> getCountryList()
  {
    return this.mCountryList;
  }
  
  public void setCountryList(ArrayList<CJRCountryCode> paramArrayList)
  {
    this.mCountryList = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRCountryCodeBody.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */