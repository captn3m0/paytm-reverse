package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRIRCTCRedirect
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  private String mirctcuserid;
  @b(a="url")
  private String murl;
  @b(a="wsReturnUrl")
  private String mwsReturnUrl;
  @b(a="wsTxnId")
  private String mwsTxnId;
  
  public String getMirctcuserid()
  {
    return this.mirctcuserid;
  }
  
  public String getMurl()
  {
    return this.murl;
  }
  
  public String getMwsReturnUrl()
  {
    return this.mwsReturnUrl;
  }
  
  public String getMwsTxnId()
  {
    return this.mwsTxnId;
  }
  
  public void setMirctcuserid(String paramString)
  {
    this.mirctcuserid = paramString;
  }
  
  public void setMurl(String paramString)
  {
    this.murl = paramString;
  }
  
  public void setMwsReturnUrl(String paramString)
  {
    this.mwsReturnUrl = paramString;
  }
  
  public void setMwsTxnId(String paramString)
  {
    this.mwsTxnId = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRIRCTCRedirect.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */