package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.a;
import com.google.c.a.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainSearchResult
  implements IJRDataModel
{
  @a
  @b(a="body")
  private Body body;
  @a
  @b(a="code")
  private Integer code;
  @a
  @b(a="error")
  private String error;
  public HashMap<String, Object> mTrainQuotaMap;
  @a
  @b(a="meta")
  private Meta meta;
  @a
  @b(a="status")
  private Status status;
  private LinkedHashMap<String, Object> trainClassesLinkedMap;
  private HashMap<String, Object> trainClassesMap;
  
  public Body getBody()
  {
    return this.body;
  }
  
  public Integer getCode()
  {
    return this.code;
  }
  
  public String getError()
  {
    return this.error;
  }
  
  public Meta getMeta()
  {
    return this.meta;
  }
  
  public Status getStatus()
  {
    return this.status;
  }
  
  public LinkedHashMap<String, Object> getTrainClassesLinkedMap()
  {
    return this.trainClassesLinkedMap;
  }
  
  public HashMap<String, Object> getTrainClassesMap()
  {
    return this.trainClassesMap;
  }
  
  public HashMap<String, Object> getTrainQuotaMap()
  {
    return this.mTrainQuotaMap;
  }
  
  public void setBody(Body paramBody)
  {
    this.body = paramBody;
  }
  
  public void setCode(Integer paramInteger)
  {
    this.code = paramInteger;
  }
  
  public void setError(String paramString)
  {
    this.error = paramString;
  }
  
  public void setMeta(Meta paramMeta)
  {
    this.meta = paramMeta;
  }
  
  public void setStatus(Status paramStatus)
  {
    this.status = paramStatus;
  }
  
  public void setTrainClassesLinkedMap(LinkedHashMap<String, Object> paramLinkedHashMap)
  {
    this.trainClassesLinkedMap = paramLinkedHashMap;
  }
  
  public void setTrainClassesMap(HashMap<String, Object> paramHashMap)
  {
    this.trainClassesMap = paramHashMap;
  }
  
  public void setTrainQuotaMap(HashMap<String, Object> paramHashMap)
  {
    this.mTrainQuotaMap = paramHashMap;
  }
  
  public class Body
    implements IJRDataModel
  {
    @a
    @b(a="quota")
    private List<String> quota = new ArrayList();
    @a
    @b(a="serverid")
    private String serverid;
    @a
    @b(a="trains")
    private List<CJRTrainSearchResult.Train> trains = new ArrayList();
    
    public Body() {}
    
    public List<String> getQuota()
    {
      return this.quota;
    }
    
    public String getServerid()
    {
      return this.serverid;
    }
    
    public List<CJRTrainSearchResult.Train> getTrains()
    {
      return this.trains;
    }
    
    public void setQuota(List<String> paramList)
    {
      this.quota = paramList;
    }
    
    public void setServerid(String paramString)
    {
      this.serverid = paramString;
    }
    
    public void setTrains(List<CJRTrainSearchResult.Train> paramList)
    {
      this.trains = paramList;
    }
  }
  
  public class Classes
    implements IJRDataModel
  {
    @a
    @b(a="CC")
    private String CC;
    @a
    @b(a="FC")
    private String FC;
    @a
    @b(a="SL")
    private String SL;
    @a
    @b(a="1A")
    private String _1A;
    @a
    @b(a="2A")
    private String _2A;
    @a
    @b(a="2S")
    private String _2S;
    @a
    @b(a="3A")
    private String _3A;
    @a
    @b(a="3E")
    private String _3E;
    
    public Classes() {}
    
    public String get1A()
    {
      return this._1A;
    }
    
    public String get2A()
    {
      return this._2A;
    }
    
    public String get2S()
    {
      return this._2S;
    }
    
    public String get3A()
    {
      return this._3A;
    }
    
    public String get3E()
    {
      return this._3E;
    }
    
    public String getCC()
    {
      return this.CC;
    }
    
    public String getFC()
    {
      return this.FC;
    }
    
    public String getSL()
    {
      return this.SL;
    }
    
    public void set1A(String paramString)
    {
      this._1A = paramString;
    }
    
    public void set2A(String paramString)
    {
      this._2A = paramString;
    }
    
    public void set2S(String paramString)
    {
      this._2S = paramString;
    }
    
    public void set3A(String paramString)
    {
      this._3A = paramString;
    }
    
    public void set3E(String paramString)
    {
      this._3E = paramString;
    }
    
    public void setCC(String paramString)
    {
      this.CC = paramString;
    }
    
    public void setFC(String paramString)
    {
      this.FC = paramString;
    }
    
    public void setSL(String paramString)
    {
      this.SL = paramString;
    }
  }
  
  public class Message
    implements IJRDataModel
  {
    @a
    @b(a="message")
    private String message;
    @a
    @b(a="title")
    private String title;
    
    public Message() {}
    
    public String getMessage()
    {
      return this.message;
    }
    
    public String getTitle()
    {
      return this.title;
    }
    
    public void setMessage(String paramString)
    {
      this.message = paramString;
    }
    
    public void setTitle(String paramString)
    {
      this.title = paramString;
    }
  }
  
  public class Meta
    implements IJRDataModel
  {
    @a
    @b(a="classes")
    private HashMap<String, Object> classes;
    @a
    @b(a="quota")
    private HashMap<String, String> quota;
    @a
    @b(a="requestid")
    private String requestid;
    
    public Meta() {}
    
    public HashMap<String, Object> getClasses()
    {
      return this.classes;
    }
    
    public HashMap<String, String> getQuota()
    {
      return this.quota;
    }
    
    public String getRequestid()
    {
      return this.requestid;
    }
    
    public void setClasses(HashMap<String, Object> paramHashMap)
    {
      this.classes = paramHashMap;
    }
    
    public void setQuota(HashMap<String, String> paramHashMap)
    {
      this.quota = paramHashMap;
    }
    
    public void setRequestid(String paramString)
    {
      this.requestid = paramString;
    }
  }
  
  public class Quota
    implements IJRDataModel
  {
    @a
    @b(a="CK")
    private String CK;
    @a
    @b(a="GN")
    private String GN;
    @a
    @b(a="LD")
    private String LD;
    @a
    @b(a="PT")
    private String PT;
    
    public Quota() {}
    
    public String getCK()
    {
      return this.CK;
    }
    
    public String getGN()
    {
      return this.GN;
    }
    
    public String getLD()
    {
      return this.LD;
    }
    
    public String getPT()
    {
      return this.PT;
    }
    
    public void setCK(String paramString)
    {
      this.CK = paramString;
    }
    
    public void setGN(String paramString)
    {
      this.GN = paramString;
    }
    
    public void setLD(String paramString)
    {
      this.LD = paramString;
    }
    
    public void setPT(String paramString)
    {
      this.PT = paramString;
    }
  }
  
  public class RunningOn
    implements IJRDataModel
  {
    @a
    @b(a="fri")
    private String fri;
    @a
    @b(a="mon")
    private String mon;
    @a
    @b(a="sat")
    private String sat;
    @a
    @b(a="sun")
    private String sun;
    @a
    @b(a="thu")
    private String thu;
    @a
    @b(a="tue")
    private String tue;
    @a
    @b(a="wed")
    private String wed;
    
    public RunningOn() {}
    
    public String getFri()
    {
      return this.fri;
    }
    
    public String getMon()
    {
      return this.mon;
    }
    
    public String getSat()
    {
      return this.sat;
    }
    
    public String getSun()
    {
      return this.sun;
    }
    
    public String getThu()
    {
      return this.thu;
    }
    
    public String getTue()
    {
      return this.tue;
    }
    
    public String getWed()
    {
      return this.wed;
    }
    
    public void setFri(String paramString)
    {
      this.fri = paramString;
    }
    
    public void setMon(String paramString)
    {
      this.mon = paramString;
    }
    
    public void setSat(String paramString)
    {
      this.sat = paramString;
    }
    
    public void setSun(String paramString)
    {
      this.sun = paramString;
    }
    
    public void setThu(String paramString)
    {
      this.thu = paramString;
    }
    
    public void setTue(String paramString)
    {
      this.tue = paramString;
    }
    
    public void setWed(String paramString)
    {
      this.wed = paramString;
    }
  }
  
  public class Status
    implements IJRDataModel
  {
    @a
    @b(a="message")
    private CJRTrainSearchResult.Message message;
    @a
    @b(a="result")
    private String result;
    
    public Status() {}
    
    public CJRTrainSearchResult.Message getMessage()
    {
      return this.message;
    }
    
    public String getResult()
    {
      return this.result;
    }
    
    public void setMessage(CJRTrainSearchResult.Message paramMessage)
    {
      this.message = paramMessage;
    }
    
    public void setResult(String paramString)
    {
      this.result = paramString;
    }
  }
  
  public class Train
    implements IJRDataModel
  {
    @a
    @b(a="arrival")
    private String arrival;
    @a
    @b(a="classes")
    private List<String> classes = new ArrayList();
    @a
    @b(a="departure")
    private String departure;
    @a
    @b(a="destination")
    private String destination;
    @b(a="destination_name")
    private String destinationName;
    private Boolean isChecked;
    @b(a="duration")
    private String mDuration;
    private boolean mIsCollapse = true;
    @b(a="message_text")
    private String mMessage;
    @b(a="message_enabled")
    private boolean mMessageEnable;
    private String mSearchedTrainArrivalTime;
    private String mSearchedTrainDepartureTime;
    @b(a="train_type")
    private String mTrainType;
    private String mirctcuserid;
    @a
    @b(a="runningOn")
    private CJRTrainSearchResult.RunningOn runningOn;
    @a
    @b(a="source")
    private String source;
    @b(a="source_name")
    private String sourceName;
    @a
    @b(a="trainName")
    private String trainName;
    @a
    @b(a="trainNumber")
    private String trainNumber;
    
    public Train() {}
    
    public String getArrival()
    {
      return this.arrival;
    }
    
    public Boolean getChecked()
    {
      return this.isChecked;
    }
    
    public List<String> getClasses()
    {
      return this.classes;
    }
    
    public String getDeparture()
    {
      return this.departure;
    }
    
    public String getDestination()
    {
      return this.destination;
    }
    
    public String getDestinationName()
    {
      return this.destinationName;
    }
    
    public String getDuration()
    {
      return this.mDuration;
    }
    
    public String getMessage()
    {
      return this.mMessage;
    }
    
    public String getMirctcuserid()
    {
      return this.mirctcuserid;
    }
    
    public CJRTrainSearchResult.RunningOn getRunningOn()
    {
      return this.runningOn;
    }
    
    public String getSearchedTrainArrivalTime()
    {
      return this.mSearchedTrainArrivalTime;
    }
    
    public String getSearchedTrainDepartureTime()
    {
      return this.mSearchedTrainDepartureTime;
    }
    
    public String getSource()
    {
      return this.source;
    }
    
    public String getSourceName()
    {
      return this.sourceName;
    }
    
    public String getTrainName()
    {
      return this.trainName;
    }
    
    public String getTrainNumber()
    {
      return this.trainNumber;
    }
    
    public String getTrainType()
    {
      return this.mTrainType;
    }
    
    public boolean isIsCollapse()
    {
      return this.mIsCollapse;
    }
    
    public boolean isMessageEnable()
    {
      return this.mMessageEnable;
    }
    
    public void setArrival(String paramString)
    {
      this.arrival = paramString;
    }
    
    public void setChecked(Boolean paramBoolean)
    {
      this.isChecked = paramBoolean;
    }
    
    public void setClasses(List<String> paramList)
    {
      this.classes = paramList;
    }
    
    public void setDeparture(String paramString)
    {
      this.departure = paramString;
    }
    
    public void setDestination(String paramString)
    {
      this.destination = paramString;
    }
    
    public void setDestinationName(String paramString)
    {
      this.destinationName = paramString;
    }
    
    public void setDuration(String paramString)
    {
      this.mDuration = paramString;
    }
    
    public void setIsCollapse(boolean paramBoolean)
    {
      this.mIsCollapse = paramBoolean;
    }
    
    public void setMessage(String paramString)
    {
      this.mMessage = paramString;
    }
    
    public void setMessageEnable(boolean paramBoolean)
    {
      this.mMessageEnable = paramBoolean;
    }
    
    public void setMirctcuserid(String paramString)
    {
      this.mirctcuserid = paramString;
    }
    
    public void setRunningOn(CJRTrainSearchResult.RunningOn paramRunningOn)
    {
      this.runningOn = paramRunningOn;
    }
    
    public void setSearchedTrainArrivalTime(String paramString)
    {
      this.mSearchedTrainArrivalTime = paramString;
    }
    
    public void setSearchedTrainDepartureTime(String paramString)
    {
      this.mSearchedTrainDepartureTime = paramString;
    }
    
    public void setSource(String paramString)
    {
      this.source = paramString;
    }
    
    public void setSourceName(String paramString)
    {
      this.sourceName = paramString;
    }
    
    public void setTrainName(String paramString)
    {
      this.trainName = paramString;
    }
    
    public void setTrainNumber(String paramString)
    {
      this.trainNumber = paramString;
    }
    
    public void setTrainType(String paramString)
    {
      this.mTrainType = paramString;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRTrainSearchResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */