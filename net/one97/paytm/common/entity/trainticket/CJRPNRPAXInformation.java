package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPNRPAXInformation
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="bookingBerthCode")
  private String mBookingBirthCode;
  @b(a="bookingBerthNo")
  private String mBookingBirthNo;
  @b(a="bookingCoachId")
  private String mBookingCoachId;
  @b(a="bookingStatus")
  private String mBookingStatus;
  @b(a="bookingStatusIndex")
  private String mBookingStatusIndex;
  @b(a="currentBerthNo")
  private String mCurrentBerthNo;
  @b(a="currentCoachId")
  private String mCurrentCoachId;
  @b(a="currentStatus")
  private String mCurrentStatus;
  @b(a="currentStatusIndex")
  private String mCurrentStatusIndex;
  @b(a="passengerBerthChoice")
  private String mPassengerBerthChoice;
  @b(a="passengerIcardFlag")
  private String mPassengerICardFlag;
  @b(a="passengerName")
  private String mPassengerName;
  @b(a="passengerSerialNumber")
  private String mPassengerSerialNo;
  
  public String getmBookingBirthCode()
  {
    return this.mBookingBirthCode;
  }
  
  public String getmBookingBirthNo()
  {
    return this.mBookingBirthNo;
  }
  
  public String getmBookingCoachId()
  {
    return this.mBookingCoachId;
  }
  
  public String getmBookingStatus()
  {
    return this.mBookingStatus;
  }
  
  public String getmBookingStatusIndex()
  {
    return this.mBookingStatusIndex;
  }
  
  public String getmCurrentBerthNo()
  {
    return this.mCurrentBerthNo;
  }
  
  public String getmCurrentCoachId()
  {
    return this.mCurrentCoachId;
  }
  
  public String getmCurrentStatus()
  {
    return this.mCurrentStatus;
  }
  
  public String getmCurrentStatusIndex()
  {
    return this.mCurrentStatusIndex;
  }
  
  public String getmPassengerBerthChoice()
  {
    return this.mPassengerBerthChoice;
  }
  
  public String getmPassengerICardFlag()
  {
    return this.mPassengerICardFlag;
  }
  
  public String getmPassengerName()
  {
    return this.mPassengerName;
  }
  
  public String getmPassengerSerialNo()
  {
    return this.mPassengerSerialNo;
  }
  
  public void setmBookingBirthCode(String paramString)
  {
    this.mBookingBirthCode = paramString;
  }
  
  public void setmBookingBirthNo(String paramString)
  {
    this.mBookingBirthNo = paramString;
  }
  
  public void setmBookingCoachId(String paramString)
  {
    this.mBookingCoachId = paramString;
  }
  
  public void setmBookingStatus(String paramString)
  {
    this.mBookingStatus = paramString;
  }
  
  public void setmBookingStatusIndex(String paramString)
  {
    this.mBookingStatusIndex = paramString;
  }
  
  public void setmCurrentBerthNo(String paramString)
  {
    this.mCurrentBerthNo = paramString;
  }
  
  public void setmCurrentCoachId(String paramString)
  {
    this.mCurrentCoachId = paramString;
  }
  
  public void setmCurrentStatus(String paramString)
  {
    this.mCurrentStatus = paramString;
  }
  
  public void setmCurrentStatusIndex(String paramString)
  {
    this.mCurrentStatusIndex = paramString;
  }
  
  public void setmPassengerBerthChoice(String paramString)
  {
    this.mPassengerBerthChoice = paramString;
  }
  
  public void setmPassengerICardFlag(String paramString)
  {
    this.mPassengerICardFlag = paramString;
  }
  
  public void setmPassengerName(String paramString)
  {
    this.mPassengerName = paramString;
  }
  
  public void setmPassengerSerialNo(String paramString)
  {
    this.mPassengerSerialNo = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRPNRPAXInformation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */