package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPNRBerths
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="CB")
  private String mCabin;
  @b(a="CP")
  private String mCouple;
  @b(a="LB")
  private String mLowerBerths;
  @b(a="MB")
  private String mMiddleBerths;
  @b(a="SL")
  private String mSideLowerBerths;
  @b(a="SM")
  private String mSideMiddle;
  @b(a="SU")
  private String mSideUpperBerths;
  @b(a="UB")
  private String mUpperBerths;
  @b(a="WS")
  private String mWindowSide;
  
  public String getmCabin()
  {
    return this.mCabin;
  }
  
  public String getmCouple()
  {
    return this.mCouple;
  }
  
  public String getmLowerBerths()
  {
    return this.mLowerBerths;
  }
  
  public String getmMiddleBerths()
  {
    return this.mMiddleBerths;
  }
  
  public String getmSideLowerBerths()
  {
    return this.mSideLowerBerths;
  }
  
  public String getmSideMiddle()
  {
    return this.mSideMiddle;
  }
  
  public String getmSideUpperBerths()
  {
    return this.mSideUpperBerths;
  }
  
  public String getmUpperBerths()
  {
    return this.mUpperBerths;
  }
  
  public String getmWindowSide()
  {
    return this.mWindowSide;
  }
  
  public void setmCabin(String paramString)
  {
    this.mCabin = paramString;
  }
  
  public void setmCouple(String paramString)
  {
    this.mCouple = paramString;
  }
  
  public void setmLowerBerths(String paramString)
  {
    this.mLowerBerths = paramString;
  }
  
  public void setmMiddleBerths(String paramString)
  {
    this.mMiddleBerths = paramString;
  }
  
  public void setmSideLowerBerths(String paramString)
  {
    this.mSideLowerBerths = paramString;
  }
  
  public void setmSideMiddle(String paramString)
  {
    this.mSideMiddle = paramString;
  }
  
  public void setmSideUpperBerths(String paramString)
  {
    this.mSideUpperBerths = paramString;
  }
  
  public void setmUpperBerths(String paramString)
  {
    this.mUpperBerths = paramString;
  }
  
  public void setmWindowSide(String paramString)
  {
    this.mWindowSide = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRPNRBerths.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */