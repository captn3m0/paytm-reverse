package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBookings
  implements IJRDataModel
{
  @b(a="boarding_date")
  private String mBoardingDate;
  @b(a="boarding")
  private String mBoardingStationCode;
  @b(a="boarding_name")
  private String mBoardingStationName;
  @b(a="booking_date")
  private String mBookingDate;
  @b(a="journey_quota")
  private String mJourneyQuota;
  @b(a="order_id")
  private String mOrderID;
  @b(a="pnr_number")
  private String mPNRNumber;
  @b(a="reservation_upto")
  private String mReservationUpToStationCode;
  @b(a="reservation_upto_name")
  private String mReservationUpToStationName;
  @b(a="source_departure_date")
  private String mSourceDepartureDate;
  @b(a="source_station_name")
  private String mSourceStationName;
  @b(a="train_name")
  private String mTrainName;
  @b(a="train_number")
  private String mTrainNumber;
  
  public String getTrainName()
  {
    return this.mTrainName;
  }
  
  public String getTrainNumber()
  {
    return this.mTrainNumber;
  }
  
  public String getmBoardingDate()
  {
    return this.mBoardingDate;
  }
  
  public String getmBoardingStationCode()
  {
    return this.mBoardingStationCode;
  }
  
  public String getmBoardingStationName()
  {
    return this.mBoardingStationName;
  }
  
  public String getmBookingDate()
  {
    return this.mBookingDate;
  }
  
  public String getmJourneyQuota()
  {
    return this.mJourneyQuota;
  }
  
  public String getmOrderID()
  {
    return this.mOrderID;
  }
  
  public String getmPNRNumber()
  {
    return this.mPNRNumber;
  }
  
  public String getmReservationUpToStationCode()
  {
    return this.mReservationUpToStationCode;
  }
  
  public String getmReservationUpToStationName()
  {
    return this.mReservationUpToStationName;
  }
  
  public String getmSourceDepartureDate()
  {
    return this.mSourceDepartureDate;
  }
  
  public String getmSourceStationName()
  {
    return this.mSourceStationName;
  }
  
  public void setTrainName(String paramString)
  {
    this.mTrainName = paramString;
  }
  
  public void setTrainNumber(String paramString)
  {
    this.mTrainNumber = paramString;
  }
  
  public void setmBoardingDate(String paramString)
  {
    this.mBoardingDate = paramString;
  }
  
  public void setmBoardingStationCode(String paramString)
  {
    this.mBoardingStationCode = paramString;
  }
  
  public void setmBoardingStationName(String paramString)
  {
    this.mBoardingStationName = paramString;
  }
  
  public void setmBookingDate(String paramString)
  {
    this.mBookingDate = paramString;
  }
  
  public void setmJourneyQuota(String paramString)
  {
    this.mJourneyQuota = paramString;
  }
  
  public void setmOrderID(String paramString)
  {
    this.mOrderID = paramString;
  }
  
  public void setmPNRNumber(String paramString)
  {
    this.mPNRNumber = paramString;
  }
  
  public void setmReservationUpToStationCode(String paramString)
  {
    this.mReservationUpToStationCode = paramString;
  }
  
  public void setmReservationUpToStationName(String paramString)
  {
    this.mReservationUpToStationName = paramString;
  }
  
  public void setmSourceDepartureDate(String paramString)
  {
    this.mSourceDepartureDate = paramString;
  }
  
  public void setmSourceStationName(String paramString)
  {
    this.mSourceStationName = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRBookings.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */