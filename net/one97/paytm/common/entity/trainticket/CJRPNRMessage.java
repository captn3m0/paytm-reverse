package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPNRMessage
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="message")
  private String mMessage;
  @b(a="title")
  private String mTitle;
  
  public String getmMessage()
  {
    return this.mMessage;
  }
  
  public String getmTitle()
  {
    return this.mTitle;
  }
  
  public void setmMessage(String paramString)
  {
    this.mMessage = paramString;
  }
  
  public void setmTitle(String paramString)
  {
    this.mTitle = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRPNRMessage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */