package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainBannersOffers
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="status")
  private CJRPNRStatus mStatus;
  @b(a="body")
  private CJRTrainBannersOffersContent mTrainBannerOfferBody;
  
  public CJRPNRStatus getmStatus()
  {
    return this.mStatus;
  }
  
  public CJRTrainBannersOffersContent getmTrainBannerOfferBody()
  {
    return this.mTrainBannerOfferBody;
  }
  
  public void setmStatus(CJRPNRStatus paramCJRPNRStatus)
  {
    this.mStatus = paramCJRPNRStatus;
  }
  
  public void setmTrainBannerOfferBody(CJRTrainBannersOffersContent paramCJRTrainBannersOffersContent)
  {
    this.mTrainBannerOfferBody = paramCJRTrainBannersOffersContent;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRTrainBannersOffers.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */