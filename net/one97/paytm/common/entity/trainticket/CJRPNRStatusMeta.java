package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import java.util.HashMap;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPNRStatusMeta
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="berths")
  private CJRPNRBerths mBerths;
  @b(a="booking_status")
  private HashMap<String, String> mBookingStatusMap;
  @b(a="classes")
  private CJRPNRClasses mClasses;
  
  public HashMap<String, String> getBookingStatusMap()
  {
    return this.mBookingStatusMap;
  }
  
  public CJRPNRBerths getmBerths()
  {
    return this.mBerths;
  }
  
  public CJRPNRClasses getmClasses()
  {
    return this.mClasses;
  }
  
  public void setBookingStatusMap(HashMap<String, String> paramHashMap)
  {
    this.mBookingStatusMap = paramHashMap;
  }
  
  public void setmBerths(CJRPNRBerths paramCJRPNRBerths)
  {
    this.mBerths = paramCJRPNRBerths;
  }
  
  public void setmClasses(CJRPNRClasses paramCJRPNRClasses)
  {
    this.mClasses = paramCJRPNRClasses;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRPNRStatusMeta.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */