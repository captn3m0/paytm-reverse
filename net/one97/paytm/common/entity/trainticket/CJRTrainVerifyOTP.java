package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainVerifyOTP
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="status")
  private CJRVerifyOTPStatus VerifyOTPStatus;
  @b(a="body")
  private CJRVerifyOTPDetails mOTPDetails;
  
  public CJRVerifyOTPDetails getOTPDetails()
  {
    return this.mOTPDetails;
  }
  
  public CJRVerifyOTPStatus getVerifyOTPStatus()
  {
    return this.VerifyOTPStatus;
  }
  
  public void setVerifyOTPStatus(CJRVerifyOTPStatus paramCJRVerifyOTPStatus)
  {
    this.VerifyOTPStatus = paramCJRVerifyOTPStatus;
  }
  
  public void setmOTPDetails(CJRVerifyOTPDetails paramCJRVerifyOTPDetails)
  {
    this.mOTPDetails = paramCJRVerifyOTPDetails;
  }
  
  public class CJRVerifyOTPDetails
  {
    @b(a="status")
    private String mStatus;
    @b(a="userId")
    private String mUserID;
    
    public CJRVerifyOTPDetails() {}
    
    public String getStatus()
    {
      return this.mStatus;
    }
    
    public String getUserID()
    {
      return this.mUserID;
    }
    
    public void setmStatus(String paramString)
    {
      this.mStatus = paramString;
    }
    
    public void setmUserID(String paramString)
    {
      this.mUserID = paramString;
    }
  }
  
  public class CJRVerifyOTPMessage
  {
    @b(a="message")
    private String mMessage;
    @b(a="title")
    private String mTilte;
    
    public CJRVerifyOTPMessage() {}
    
    public String getMessage()
    {
      return this.mMessage;
    }
    
    public String getTilte()
    {
      return this.mTilte;
    }
    
    public void setmMessage(String paramString)
    {
      this.mMessage = paramString;
    }
    
    public void setmTilte(String paramString)
    {
      this.mTilte = paramString;
    }
  }
  
  public class CJRVerifyOTPStatus
  {
    @b(a="result")
    private String mResult;
    @b(a="message")
    private CJRTrainVerifyOTP.CJRVerifyOTPMessage mVerifyOTPMessage;
    
    public CJRVerifyOTPStatus() {}
    
    public CJRTrainVerifyOTP.CJRVerifyOTPMessage getResendOTPMessage()
    {
      return this.mVerifyOTPMessage;
    }
    
    public String getResult()
    {
      return this.mResult;
    }
    
    public void setmResult(String paramString)
    {
      this.mResult = paramString;
    }
    
    public void setmVerifyOTPMessage(CJRTrainVerifyOTP.CJRVerifyOTPMessage paramCJRVerifyOTPMessage)
    {
      this.mVerifyOTPMessage = paramCJRVerifyOTPMessage;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRTrainVerifyOTP.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */