package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPGStatus
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="payment_status")
  private String mstatus;
  
  public String getMstatus()
  {
    return this.mstatus;
  }
  
  public void setMstatus(String paramString)
  {
    this.mstatus = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRPGStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */