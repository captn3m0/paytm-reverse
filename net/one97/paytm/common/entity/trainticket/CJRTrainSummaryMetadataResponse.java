package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainSummaryMetadataResponse
  implements IJRDataModel
{
  @b(a="arrival_date")
  private String arrivalDate;
  @b(a="arrival_time")
  private String arrivalTime;
  @b(a="boarding_station")
  private String boardingStation;
  @b(a="boarding_station_name")
  private String boardingStationName;
  @b(a="boarding_time")
  private String boardingTime;
  @b(a="class")
  private String classVAlue;
  @b(a="departure_date")
  private String departureDate;
  @b(a="source_departure_time")
  private String departureTime;
  @b(a="destination")
  private String destination;
  @b(a="destination_name")
  private String destinationStationName;
  @b(a="fare")
  private CJRFareDetails fareDetails;
  @b(a="source_departure_date")
  private String mSourceDepartureDate;
  @b(a="quota")
  private String quota;
  @b(a="source")
  private String source;
  @b(a="source_station_name")
  private String sourceStationName;
  @b(a="train_name")
  private String trainName;
  @b(a="train_number")
  private String trainNumber;
  @b(a="travellers")
  private String travellersCount;
  
  public String getArrivalDate()
  {
    return this.arrivalDate;
  }
  
  public String getArrivalTime()
  {
    return this.arrivalTime;
  }
  
  public String getBoardingStation()
  {
    return this.boardingStation;
  }
  
  public String getBoardingStationName()
  {
    return this.boardingStationName;
  }
  
  public String getBoardingTime()
  {
    return this.boardingTime;
  }
  
  public String getClassVAlue()
  {
    return this.classVAlue;
  }
  
  public String getDepartureDate()
  {
    return this.departureDate;
  }
  
  public String getDepartureTime()
  {
    return this.departureTime;
  }
  
  public String getDestination()
  {
    return this.destination;
  }
  
  public String getDestinationStationName()
  {
    return this.destinationStationName;
  }
  
  public CJRFareDetails getFareDetails()
  {
    return this.fareDetails;
  }
  
  public String getQuota()
  {
    return this.quota;
  }
  
  public String getSource()
  {
    return this.source;
  }
  
  public String getSourceDepartureDate()
  {
    return this.mSourceDepartureDate;
  }
  
  public String getSourceStationName()
  {
    return this.sourceStationName;
  }
  
  public String getTrainName()
  {
    return this.trainName;
  }
  
  public String getTrainNumber()
  {
    return this.trainNumber;
  }
  
  public String getTravellersCount()
  {
    return this.travellersCount;
  }
  
  public void setSourceDepartureDate(String paramString)
  {
    this.mSourceDepartureDate = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRTrainSummaryMetadataResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */