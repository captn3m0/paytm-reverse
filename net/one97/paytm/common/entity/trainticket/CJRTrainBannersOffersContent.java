package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainBannersOffersContent
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="banners")
  private ArrayList<CJRTrainBannerDetails> mBannerDetails;
  @b(a="offers")
  private ArrayList<CJRTrainOfferDetails> mOfferDetails;
  
  public ArrayList<CJRTrainBannerDetails> getmBannerDetails()
  {
    return this.mBannerDetails;
  }
  
  public ArrayList<CJRTrainOfferDetails> getmOfferDetails()
  {
    return this.mOfferDetails;
  }
  
  public void setmBannerDetails(ArrayList<CJRTrainBannerDetails> paramArrayList)
  {
    this.mBannerDetails = paramArrayList;
  }
  
  public void setmOfferDetails(ArrayList<CJRTrainOfferDetails> paramArrayList)
  {
    this.mOfferDetails = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRTrainBannersOffersContent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */