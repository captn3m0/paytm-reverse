package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainBannerItems
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="alt_image_url")
  private String mAltImageUrl;
  @b(a="delete_url")
  private String mDelete_url;
  @b(a="id")
  private int mId;
  @b(a="image_url")
  private String mImageUrl;
  @b(a="name")
  private String mName;
  @b(a="priority")
  private int mPriority;
  @b(a="status")
  private String mStatus;
  @b(a="url")
  private String mUrl;
  @b(a="url_info")
  private String mUrlInfo;
  @b(a="url_type")
  private String mUrlType;
  @b(a="seourl")
  private String mseourl;
  @b(a="url_key")
  private String murl_key;
  
  public String getMseourl()
  {
    return this.mseourl;
  }
  
  public String getMurl_key()
  {
    return this.murl_key;
  }
  
  public String getmAltImageUrl()
  {
    return this.mAltImageUrl;
  }
  
  public String getmDelete_url()
  {
    return this.mDelete_url;
  }
  
  public int getmId()
  {
    return this.mId;
  }
  
  public String getmImageUrl()
  {
    return this.mImageUrl;
  }
  
  public String getmName()
  {
    return this.mName;
  }
  
  public int getmPriority()
  {
    return this.mPriority;
  }
  
  public String getmStatus()
  {
    return this.mStatus;
  }
  
  public String getmUrl()
  {
    return this.mUrl;
  }
  
  public String getmUrlInfo()
  {
    return this.mUrlInfo;
  }
  
  public String getmUrlType()
  {
    return this.mUrlType;
  }
  
  public void setMseourl(String paramString)
  {
    this.mseourl = paramString;
  }
  
  public void setMurl_key(String paramString)
  {
    this.murl_key = paramString;
  }
  
  public void setmAltImageUrl(String paramString)
  {
    this.mAltImageUrl = paramString;
  }
  
  public void setmDelete_url(String paramString)
  {
    this.mDelete_url = paramString;
  }
  
  public void setmId(int paramInt)
  {
    this.mId = paramInt;
  }
  
  public void setmImageUrl(String paramString)
  {
    this.mImageUrl = paramString;
  }
  
  public void setmName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setmPriority(int paramInt)
  {
    this.mPriority = paramInt;
  }
  
  public void setmStatus(String paramString)
  {
    this.mStatus = paramString;
  }
  
  public void setmUrl(String paramString)
  {
    this.mUrl = paramString;
  }
  
  public void setmUrlInfo(String paramString)
  {
    this.mUrlInfo = paramString;
  }
  
  public void setmUrlType(String paramString)
  {
    this.mUrlType = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRTrainBannerItems.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */