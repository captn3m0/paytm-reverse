package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;
import net.one97.paytm.common.entity.flightticket.CJRStatus;
import net.one97.paytm.common.entity.shopping.CJRHomePageLayout;

public class CJROrderSummaryCarouselModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="body")
  private ArrayList<CJRHomePageLayout> mCarouselBody;
  @b(a="error")
  private String mError;
  @b(a="status")
  private CJRStatus mStatus;
  
  public ArrayList<CJRHomePageLayout> getCarouselBody()
  {
    return this.mCarouselBody;
  }
  
  public String getmError()
  {
    return this.mError;
  }
  
  public CJRStatus getmStatus()
  {
    return this.mStatus;
  }
  
  public void setCarouselBody(ArrayList<CJRHomePageLayout> paramArrayList)
  {
    this.mCarouselBody = paramArrayList;
  }
  
  public void setmError(String paramString)
  {
    this.mError = paramString;
  }
  
  public void setmStatus(CJRStatus paramCJRStatus)
  {
    this.mStatus = paramCJRStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJROrderSummaryCarouselModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */