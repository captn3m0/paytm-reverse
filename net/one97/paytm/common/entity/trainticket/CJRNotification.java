package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRNotification
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="enabled")
  private String mIsEnabled;
  @b(a="message")
  private String mMessage;
  @b(a="priority")
  private String mPriority;
  @b(a="type")
  private String mType;
  
  public String getmIsEnabled()
  {
    return this.mIsEnabled;
  }
  
  public String getmMessage()
  {
    return this.mMessage;
  }
  
  public String getmPriority()
  {
    return this.mPriority;
  }
  
  public String getmType()
  {
    return this.mType;
  }
  
  public void setmIsEnabled(String paramString)
  {
    this.mIsEnabled = paramString;
  }
  
  public void setmMessage(String paramString)
  {
    this.mMessage = paramString;
  }
  
  public void setmPriority(String paramString)
  {
    this.mPriority = paramString;
  }
  
  public void setmType(String paramString)
  {
    this.mType = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRNotification.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */