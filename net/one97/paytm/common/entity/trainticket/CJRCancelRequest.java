package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.CJRStatusError;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRCancelRequest
  implements IJRDataModel
{
  @b(a="error")
  private String mError;
  @b(a="status")
  private CJRStatusError mStatus;
  
  public String getmError()
  {
    return this.mError;
  }
  
  public CJRStatusError getmStatus()
  {
    return this.mStatus;
  }
  
  public void setmError(String paramString)
  {
    this.mError = paramString;
  }
  
  public void setmStatus(CJRStatusError paramCJRStatusError)
  {
    this.mStatus = paramCJRStatusError;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRCancelRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */