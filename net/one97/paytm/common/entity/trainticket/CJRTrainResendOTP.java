package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainResendOTP
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="status")
  private CJRResendOTPStatus ResendStatus;
  @b(a="body")
  private CJRResendOTPDetails mOTPDetails;
  
  public CJRResendOTPDetails getOTPDetails()
  {
    return this.mOTPDetails;
  }
  
  public CJRResendOTPStatus getResendStatus()
  {
    return this.ResendStatus;
  }
  
  public void setResendStatus(CJRResendOTPStatus paramCJRResendOTPStatus)
  {
    this.ResendStatus = paramCJRResendOTPStatus;
  }
  
  public void setmOTPDetails(CJRResendOTPDetails paramCJRResendOTPDetails)
  {
    this.mOTPDetails = paramCJRResendOTPDetails;
  }
  
  public class CJRResendOTPDetails
  {
    @b(a="status")
    private String mStatus;
    @b(a="userId")
    private String mUserID;
    
    public CJRResendOTPDetails() {}
    
    public String getStatus()
    {
      return this.mStatus;
    }
    
    public String getUserID()
    {
      return this.mUserID;
    }
    
    public void setmStatus(String paramString)
    {
      this.mStatus = paramString;
    }
    
    public void setmUserID(String paramString)
    {
      this.mUserID = paramString;
    }
  }
  
  public class CJRResendOTPMessage
  {
    @b(a="message")
    private String mMessage;
    @b(a="title")
    private String mTilte;
    
    public CJRResendOTPMessage() {}
    
    public String getMessage()
    {
      return this.mMessage;
    }
    
    public String getTilte()
    {
      return this.mTilte;
    }
    
    public void setmMessage(String paramString)
    {
      this.mMessage = paramString;
    }
    
    public void setmTilte(String paramString)
    {
      this.mTilte = paramString;
    }
  }
  
  public class CJRResendOTPStatus
  {
    @b(a="message")
    private CJRTrainResendOTP.CJRResendOTPMessage mResendOTPMessage;
    @b(a="result")
    private String mResult;
    
    public CJRResendOTPStatus() {}
    
    public CJRTrainResendOTP.CJRResendOTPMessage getResendOTPMessage()
    {
      return this.mResendOTPMessage;
    }
    
    public String getResult()
    {
      return this.mResult;
    }
    
    public void setmResendOTPMessage(CJRTrainResendOTP.CJRResendOTPMessage paramCJRResendOTPMessage)
    {
      this.mResendOTPMessage = paramCJRResendOTPMessage;
    }
    
    public void setmResult(String paramString)
    {
      this.mResult = paramString;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRTrainResendOTP.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */