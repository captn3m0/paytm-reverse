package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainSourceStation
  implements IJRDataModel
{
  @b(a="day_count")
  private int mDayCount;
  @b(a="departure_time")
  private String mDepartureTime;
  @b(a="station_code")
  private String mStationCode;
  @b(a="station_name")
  private String mStationName;
  
  public int getDayCount()
  {
    return this.mDayCount;
  }
  
  public String getDepartureTime()
  {
    return this.mDepartureTime;
  }
  
  public String getStationCode()
  {
    return this.mStationCode;
  }
  
  public String getStationName()
  {
    return this.mStationName;
  }
  
  public void setDayCount(int paramInt)
  {
    this.mDayCount = paramInt;
  }
  
  public void setDepartureTime(String paramString)
  {
    this.mDepartureTime = paramString;
  }
  
  public void setStationCode(String paramString)
  {
    this.mStationCode = paramString;
  }
  
  public void setStationName(String paramString)
  {
    this.mStationName = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRTrainSourceStation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */