package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainBookingInformation
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="body")
  private CJRTrainBookingStatus mTrainBookingStatus;
  
  public CJRTrainBookingStatus getmTrainBookingStatus()
  {
    return this.mTrainBookingStatus;
  }
  
  public void setmTrainBookingStatus(CJRTrainBookingStatus paramCJRTrainBookingStatus)
  {
    this.mTrainBookingStatus = paramCJRTrainBookingStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRTrainBookingInformation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */