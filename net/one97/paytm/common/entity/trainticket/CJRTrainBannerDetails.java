package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;
import net.one97.paytm.common.entity.flightticket.CJROfferItems;

public class CJRTrainBannerDetails
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="items")
  private List<CJROfferItems> mBannerItems;
  @b(a="id")
  private int mId;
  @b(a="layout")
  private String mLayout;
  @b(a="name")
  private String mName;
  @b(a="priority")
  private int mPriority;
  @b(a="status")
  private int mStatus;
  
  public List<CJROfferItems> getmBannerItems()
  {
    return this.mBannerItems;
  }
  
  public int getmId()
  {
    return this.mId;
  }
  
  public String getmLayout()
  {
    return this.mLayout;
  }
  
  public String getmName()
  {
    return this.mName;
  }
  
  public int getmPriority()
  {
    return this.mPriority;
  }
  
  public int getmStatus()
  {
    return this.mStatus;
  }
  
  public void setmBannerItems(List<CJROfferItems> paramList)
  {
    this.mBannerItems = paramList;
  }
  
  public void setmId(int paramInt)
  {
    this.mId = paramInt;
  }
  
  public void setmLayout(String paramString)
  {
    this.mLayout = paramString;
  }
  
  public void setmName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setmPriority(int paramInt)
  {
    this.mPriority = paramInt;
  }
  
  public void setmStatus(int paramInt)
  {
    this.mStatus = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRTrainBannerDetails.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */