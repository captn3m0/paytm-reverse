package net.one97.paytm.common.entity.trainticket;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPNRStatusDetails
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="boarding_station")
  private CJRTrainBoardingStation mBoardingStation;
  @b(a="class")
  private String mClass;
  @b(a="date")
  private String mDate;
  @b(a="no_of_passengers")
  private String mNoOfPassenger;
  @b(a="pax_info")
  private ArrayList<CJRPNRPAXInformation> mPAXInfoList;
  @b(a="pnr_number")
  private String mPNRNumber;
  @b(a="reservation_upto")
  private CJRTrainReservationValidity mReservationUpTo;
  @b(a="source_station")
  private CJRTrainSourceStation mSourceStation;
  @b(a="train_name")
  private String mTrainName;
  @b(a="train_number")
  private String mTrainNumber;
  
  public CJRTrainSourceStation getSourceStation()
  {
    return this.mSourceStation;
  }
  
  public CJRTrainBoardingStation getmBoardingStation()
  {
    return this.mBoardingStation;
  }
  
  public String getmClass()
  {
    return this.mClass;
  }
  
  public String getmDate()
  {
    return this.mDate;
  }
  
  public String getmNoOfPassenger()
  {
    return this.mNoOfPassenger;
  }
  
  public ArrayList<CJRPNRPAXInformation> getmPAXInfoList()
  {
    return this.mPAXInfoList;
  }
  
  public String getmPNRNumber()
  {
    return this.mPNRNumber;
  }
  
  public CJRTrainReservationValidity getmReservationUpTo()
  {
    return this.mReservationUpTo;
  }
  
  public String getmTrainName()
  {
    return this.mTrainName;
  }
  
  public String getmTrainNumber()
  {
    return this.mTrainNumber;
  }
  
  public void setSourceStation(CJRTrainSourceStation paramCJRTrainSourceStation)
  {
    this.mSourceStation = paramCJRTrainSourceStation;
  }
  
  public void setmBoardingStation(CJRTrainBoardingStation paramCJRTrainBoardingStation)
  {
    this.mBoardingStation = paramCJRTrainBoardingStation;
  }
  
  public void setmClass(String paramString)
  {
    this.mClass = paramString;
  }
  
  public void setmDate(String paramString)
  {
    this.mDate = paramString;
  }
  
  public void setmNoOfPassenger(String paramString)
  {
    this.mNoOfPassenger = paramString;
  }
  
  public void setmPAXInfoList(ArrayList<CJRPNRPAXInformation> paramArrayList)
  {
    this.mPAXInfoList = paramArrayList;
  }
  
  public void setmPNRNumber(String paramString)
  {
    this.mPNRNumber = paramString;
  }
  
  public void setmReservationUpTo(CJRTrainReservationValidity paramCJRTrainReservationValidity)
  {
    this.mReservationUpTo = paramCJRTrainReservationValidity;
  }
  
  public void setmTrainName(String paramString)
  {
    this.mTrainName = paramString;
  }
  
  public void setmTrainNumber(String paramString)
  {
    this.mTrainNumber = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/trainticket/CJRPNRStatusDetails.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */