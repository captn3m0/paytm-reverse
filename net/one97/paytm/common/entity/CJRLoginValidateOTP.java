package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRLoginValidateOTP
  implements IJRDataModel
{
  @b(a="code")
  private String mCode;
  @b(a="message")
  private String mMessage;
  @b(a="responseCode")
  private String mResponseCode;
  
  public String getCode()
  {
    return this.mCode;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getResponseCode()
  {
    return this.mResponseCode;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRLoginValidateOTP.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */