package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRPromocodeStatusModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="promocode")
  private String mPromoCode;
  @b(a="messageID")
  private String mPromoCodeMessageID;
  @b(a="status")
  private String mPromoCodeStatus;
  
  public String getPromoCode()
  {
    return this.mPromoCode;
  }
  
  public String getPromoCodeMessageID()
  {
    return this.mPromoCodeMessageID;
  }
  
  public String getPromoCodeStatus()
  {
    return this.mPromoCodeStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRPromocodeStatusModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */