package net.one97.paytm.common.entity.events;

import android.text.TextUtils;
import com.google.c.a.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAddressDetailModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="eventDateTime")
  private List<CJREventDateTimeModel> eventDateTimeList;
  @b(a="address")
  private String mAddress;
  @b(a="address_name")
  private String mAddressName;
  @b(a="contact")
  private String mContact;
  private Map<String, List<CJRSeatDetailsModel>> mDateTimeToSeatListMap;
  private Map<String, List<CJREventDateTimeModel.Time>> mDateToTimeListMap;
  @b(a="description")
  private String mDescription;
  @b(a="email")
  private String mEmail;
  @b(a="latitude")
  private Double mLatitude;
  @b(a="longitude")
  private Double mLongitude;
  @b(a="name")
  private String mName;
  @b(a="pincode")
  private String mPincode;
  @b(a="seatDetails")
  private List<CJRSeatDetailsModel> mSeatDetailsModel;
  private boolean mSoldOut;
  
  private void addCurrentSeatDetailListToSingleList(List<CJRSeatDetailsModel> paramList1, List<CJRSeatDetailsModel> paramList2)
  {
    if ((paramList1 == null) || (paramList1.size() <= 0)) {}
    for (;;)
    {
      return;
      if (paramList2 != null)
      {
        paramList1 = paramList1.iterator();
        while (paramList1.hasNext()) {
          paramList2.add((CJRSeatDetailsModel)paramList1.next());
        }
      }
    }
  }
  
  private void checkAndAddCurrentSeatDetailListToSingleList(List<CJRSeatDetailsModel> paramList1, List<CJRSeatDetailsModel> paramList2)
  {
    if ((paramList1 == null) || (paramList1.size() <= 0)) {}
    for (;;)
    {
      return;
      if ((paramList2 != null) && (paramList2.size() > 0))
      {
        paramList1 = paramList1.iterator();
        while (paramList1.hasNext())
        {
          CJRSeatDetailsModel localCJRSeatDetailsModel = (CJRSeatDetailsModel)paramList1.next();
          if (!hasSeatBeenAlreadyAdded(localCJRSeatDetailsModel, paramList2)) {
            paramList2.add(localCJRSeatDetailsModel);
          }
        }
      }
    }
  }
  
  private boolean checkIfTimeAlreadyExistsInList(CJREventDateTimeModel.Time paramTime, List<CJREventDateTimeModel.Time> paramList)
  {
    if ((paramList == null) || (paramList.size() <= 0) || (paramTime == null)) {}
    Object localObject;
    String str;
    do
    {
      while (!paramList.hasNext())
      {
        return false;
        paramList = paramList.iterator();
      }
      localObject = (CJREventDateTimeModel.Time)paramList.next();
      str = ((CJREventDateTimeModel.Time)localObject).getmStart();
      localObject = ((CJREventDateTimeModel.Time)localObject).getmTo();
    } while ((TextUtils.isEmpty(str)) || (TextUtils.isEmpty((CharSequence)localObject)) || (TextUtils.isEmpty(paramTime.getmStart())) || (TextUtils.isEmpty(paramTime.getmTo())) || (!str.equalsIgnoreCase(paramTime.getmStart())) || (!((String)localObject).equalsIgnoreCase(paramTime.getmTo())));
    return true;
  }
  
  private boolean hasSeatBeenAlreadyAdded(CJRSeatDetailsModel paramCJRSeatDetailsModel, List<CJRSeatDetailsModel> paramList)
  {
    if ((paramCJRSeatDetailsModel == null) || (paramList == null) || (paramList.size() <= 0)) {}
    do
    {
      while (!paramList.hasNext())
      {
        return false;
        paramList = paramList.iterator();
      }
    } while (((CJRSeatDetailsModel)paramList.next()).getSeatId() != paramCJRSeatDetailsModel.getSeatId());
    return true;
  }
  
  public HashMap<String, List<CJREventDateTimeModel.Time>> extractDateTimeMap(List<CJREventDateTimeModel> paramList)
  {
    if ((paramList == null) || (paramList.size() <= 0)) {
      return null;
    }
    HashMap localHashMap = new HashMap();
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      Object localObject1 = (CJREventDateTimeModel)paramList.next();
      Object localObject2 = ((CJREventDateTimeModel)localObject1).getmEventDateList();
      localObject1 = ((CJREventDateTimeModel)localObject1).getmTime();
      if ((localObject2 != null) && (((List)localObject2).size() > 0))
      {
        localObject2 = ((List)localObject2).iterator();
        while (((Iterator)localObject2).hasNext())
        {
          Object localObject3 = (String)((Iterator)localObject2).next();
          if (localObject3 != null) {
            if (localHashMap.containsKey(localObject3))
            {
              localObject3 = (List)localHashMap.get(localObject3);
              if (!checkIfTimeAlreadyExistsInList((CJREventDateTimeModel.Time)localObject1, (List)localObject3)) {
                ((List)localObject3).add(localObject1);
              }
            }
            else
            {
              ArrayList localArrayList = new ArrayList();
              localArrayList.add(localObject1);
              localHashMap.put(localObject3, localArrayList);
            }
          }
        }
      }
    }
    return localHashMap;
  }
  
  public Map<String, List<CJRSeatDetailsModel>> extractDateTimeToSeatListMap(List<CJREventDateTimeModel> paramList)
  {
    if ((paramList == null) || (paramList.size() <= 0)) {
      return null;
    }
    HashMap localHashMap = new HashMap();
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      Object localObject2 = (CJREventDateTimeModel)paramList.next();
      Object localObject3 = ((CJREventDateTimeModel)localObject2).getmEventDateList();
      Object localObject1 = ((CJREventDateTimeModel)localObject2).getmTime();
      localObject1 = ((CJREventDateTimeModel.Time)localObject1).getmStart() + ((CJREventDateTimeModel.Time)localObject1).getmTo();
      localObject2 = ((CJREventDateTimeModel)localObject2).getmSeatDetails();
      if ((localObject3 != null) && (((List)localObject3).size() > 0))
      {
        localObject3 = ((List)localObject3).iterator();
        while (((Iterator)localObject3).hasNext())
        {
          String str = (String)((Iterator)localObject3).next();
          if (str != null)
          {
            str = str + (String)localObject1;
            if (localHashMap.containsKey(str))
            {
              checkAndAddCurrentSeatDetailListToSingleList((List)localObject2, (List)localHashMap.get(str));
            }
            else
            {
              ArrayList localArrayList = new ArrayList();
              addCurrentSeatDetailListToSingleList((List)localObject2, localArrayList);
              localHashMap.put(str, localArrayList);
            }
          }
        }
      }
    }
    return localHashMap;
  }
  
  public boolean extractVenueSoldOutInfo(List<CJREventDateTimeModel> paramList)
  {
    if ((paramList == null) || (paramList.size() <= 0))
    {
      break label22;
      break label22;
      label13:
      return true;
    }
    label22:
    Object localObject;
    label64:
    do
    {
      paramList = paramList.iterator();
      break label64;
      if (!paramList.hasNext()) {
        break label13;
      }
      localObject = ((CJREventDateTimeModel)paramList.next()).getmSeatDetails();
      if ((localObject == null) || (((List)localObject).size() <= 0)) {
        break;
      }
      localObject = ((List)localObject).iterator();
      if (!((Iterator)localObject).hasNext()) {
        break;
      }
    } while (((CJRSeatDetailsModel)((Iterator)localObject).next()).getSeatsAvailable() <= 0);
    return false;
  }
  
  public String getAddress()
  {
    return this.mAddress;
  }
  
  public String getAddressName()
  {
    return this.mAddressName;
  }
  
  public Map<String, List<CJRSeatDetailsModel>> getDateTimeToSeatListMap()
  {
    if (this.mDateTimeToSeatListMap == null) {
      this.mDateTimeToSeatListMap = extractDateTimeToSeatListMap(this.eventDateTimeList);
    }
    return this.mDateTimeToSeatListMap;
  }
  
  public Map<String, List<CJREventDateTimeModel.Time>> getDateToTimeListMap()
  {
    if (this.mDateToTimeListMap == null) {
      this.mDateToTimeListMap = extractDateTimeMap(this.eventDateTimeList);
    }
    return this.mDateToTimeListMap;
  }
  
  public String getDescription()
  {
    return this.mDescription;
  }
  
  public String getEmail()
  {
    return this.mEmail;
  }
  
  public List<CJREventDateTimeModel> getEventDateTimeList()
  {
    return this.eventDateTimeList;
  }
  
  public Double getLatitude()
  {
    return this.mLatitude;
  }
  
  public Double getLongitude()
  {
    return this.mLongitude;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getPincode()
  {
    return this.mPincode;
  }
  
  public List<CJRSeatDetailsModel> getSeatDetailsModel()
  {
    return this.mSeatDetailsModel;
  }
  
  public boolean getSoldOutInfo()
  {
    return this.mSoldOut;
  }
  
  public String getmContact()
  {
    return this.mContact;
  }
  
  public void setAddress(String paramString)
  {
    this.mAddress = paramString;
  }
  
  public void setAddressName(String paramString)
  {
    this.mAddressName = paramString;
  }
  
  public void setDescription(String paramString)
  {
    this.mDescription = paramString;
  }
  
  public void setEmail(String paramString)
  {
    this.mEmail = paramString;
  }
  
  public void setEventDateTimeList(List<CJREventDateTimeModel> paramList)
  {
    this.eventDateTimeList = paramList;
  }
  
  public void setLatitude(Double paramDouble)
  {
    this.mLatitude = paramDouble;
  }
  
  public void setLongitude(Double paramDouble)
  {
    this.mLongitude = paramDouble;
  }
  
  public void setName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setPincode(String paramString)
  {
    this.mPincode = paramString;
  }
  
  public void setSeatDetailsModel(List<CJRSeatDetailsModel> paramList)
  {
    this.mSeatDetailsModel = paramList;
  }
  
  public void setSoldOutInfo(boolean paramBoolean)
  {
    this.mSoldOut = paramBoolean;
  }
  
  public void setmContact(String paramString)
  {
    this.mContact = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/events/CJRAddressDetailModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */