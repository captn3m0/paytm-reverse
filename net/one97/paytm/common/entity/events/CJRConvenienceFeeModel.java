package net.one97.paytm.common.entity.events;

import com.google.c.a.b;

public class CJRConvenienceFeeModel
{
  @b(a="convFee")
  private String mConvFee;
  @b(a="otherTax")
  private String mOtherTax;
  @b(a="paytmCommission")
  private String mPaytmCommission;
  @b(a="pgCharges")
  private String mPgCharges;
  @b(a="serviceTax")
  private String mServiceTax;
  @b(a="swachBharatCess")
  private String mSwachBharatCess;
  @b(a="totalCommision")
  private String mTotalCommision;
  @b(a="totalConvFee")
  private String mTotalConvFee;
  @b(a="totalPgCharges")
  private String mTotalPgCharges;
  @b(a="totalServiceTax")
  private String mTotalServiceTax;
  @b(a="totalSwachBharatCess")
  private String mTotalSwachBharatCess;
  
  public String getConvFee()
  {
    return this.mConvFee;
  }
  
  public String getOtherTax()
  {
    return this.mOtherTax;
  }
  
  public String getPaytmCommission()
  {
    return this.mPaytmCommission;
  }
  
  public String getPgCharges()
  {
    return this.mPgCharges;
  }
  
  public String getServiceTax()
  {
    return this.mServiceTax;
  }
  
  public String getSwachBharatCess()
  {
    return this.mSwachBharatCess;
  }
  
  public String getTotalCommision()
  {
    return this.mTotalCommision;
  }
  
  public String getTotalConvFee()
  {
    return this.mTotalConvFee;
  }
  
  public String getTotalPgCharges()
  {
    return this.mTotalPgCharges;
  }
  
  public String getTotalServiceTax()
  {
    return this.mTotalServiceTax;
  }
  
  public String getTotalSwachBharatCess()
  {
    return this.mTotalSwachBharatCess;
  }
  
  public void setConvFee(String paramString)
  {
    this.mConvFee = paramString;
  }
  
  public void setOtherTax(String paramString)
  {
    this.mOtherTax = paramString;
  }
  
  public void setPaytmCommission(String paramString)
  {
    this.mPaytmCommission = paramString;
  }
  
  public void setPgCharges(String paramString)
  {
    this.mPgCharges = paramString;
  }
  
  public void setServiceTax(String paramString)
  {
    this.mServiceTax = paramString;
  }
  
  public void setSwachBharatCess(String paramString)
  {
    this.mSwachBharatCess = paramString;
  }
  
  public void setTotalCommision(String paramString)
  {
    this.mTotalCommision = paramString;
  }
  
  public void setTotalConvFee(String paramString)
  {
    this.mTotalConvFee = paramString;
  }
  
  public void setTotalPgCharges(String paramString)
  {
    this.mTotalPgCharges = paramString;
  }
  
  public void setTotalServiceTax(String paramString)
  {
    this.mTotalServiceTax = paramString;
  }
  
  public void setTotalSwachBharatCess(String paramString)
  {
    this.mTotalSwachBharatCess = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/events/CJRConvenienceFeeModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */