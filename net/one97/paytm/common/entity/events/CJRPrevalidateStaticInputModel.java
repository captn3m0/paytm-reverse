package net.one97.paytm.common.entity.events;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPrevalidateStaticInputModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="cart_items")
  private ArrayList<CJRPrevalidateCartItemStaticModel> cart_items = new ArrayList();
  @b(a="error")
  private String error;
  @b(a="promocode")
  private String promocode;
  @b(a="promofailuretext")
  private String promofailuretext;
  @b(a="promostatus")
  private String promostatus;
  @b(a="promotext")
  private String promotext;
  
  public ArrayList<CJRPrevalidateCartItemStaticModel> getCart_items()
  {
    return this.cart_items;
  }
  
  public String getError()
  {
    return this.error;
  }
  
  public String getPromocode()
  {
    return this.promocode;
  }
  
  public String getPromofailuretext()
  {
    return this.promofailuretext;
  }
  
  public String getPromostatus()
  {
    return this.promostatus;
  }
  
  public String getPromotext()
  {
    return this.promotext;
  }
  
  public void setCart_items(ArrayList<CJRPrevalidateCartItemStaticModel> paramArrayList)
  {
    this.cart_items = paramArrayList;
  }
  
  public void setCart_items(CJRPrevalidateCartItemStaticModel paramCJRPrevalidateCartItemStaticModel)
  {
    this.cart_items.add(paramCJRPrevalidateCartItemStaticModel);
  }
  
  public void setError(String paramString)
  {
    this.error = paramString;
  }
  
  public void setPromocode(String paramString)
  {
    this.promocode = paramString;
  }
  
  public void setPromofailuretext(String paramString)
  {
    this.promofailuretext = paramString;
  }
  
  public void setPromostatus(String paramString)
  {
    this.promostatus = paramString;
  }
  
  public void setPromotext(String paramString)
  {
    this.promotext = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/events/CJRPrevalidateStaticInputModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */