package net.one97.paytm.common.entity.events;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJREventCityModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="isTopCity")
  private boolean mIsTopCity;
  @b(a="label")
  private String mLabel;
  @b(a="value")
  private String mValue;
  
  public String getLabel()
  {
    return this.mLabel;
  }
  
  public String getValue()
  {
    return this.mValue;
  }
  
  public boolean isTopCity()
  {
    return this.mIsTopCity;
  }
  
  public void setIsTopCity(boolean paramBoolean)
  {
    this.mIsTopCity = paramBoolean;
  }
  
  public void setValue(String paramString)
  {
    this.mValue = paramString;
  }
  
  public void setlabel(String paramString)
  {
    this.mLabel = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/events/CJREventCityModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */