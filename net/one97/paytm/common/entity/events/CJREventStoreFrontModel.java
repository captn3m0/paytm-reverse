package net.one97.paytm.common.entity.events;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJREventStoreFrontModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="homepage_layout")
  private ArrayList<CJREventStoreFrontDetailModel> mStoreFrontItems = new ArrayList();
  
  public ArrayList<CJREventStoreFrontDetailModel> getmStoreFrontItems()
  {
    return this.mStoreFrontItems;
  }
  
  public void setmStoreFrontItems(ArrayList<CJREventStoreFrontDetailModel> paramArrayList)
  {
    this.mStoreFrontItems = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/events/CJREventStoreFrontModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */