package net.one97.paytm.common.entity.events;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRResourceDetailModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="type")
  private String mType;
  @b(a="value1")
  private String mValue1;
  @b(a="value2")
  private String mValue2;
  
  public String getType()
  {
    return this.mType;
  }
  
  public String getValue1()
  {
    return this.mValue1;
  }
  
  public String getmValue2()
  {
    return this.mValue2;
  }
  
  public void setType(String paramString)
  {
    this.mType = paramString;
  }
  
  public void setValue1(String paramString)
  {
    this.mValue1 = paramString;
  }
  
  public void setmValue2(String paramString)
  {
    this.mValue2 = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/events/CJRResourceDetailModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */