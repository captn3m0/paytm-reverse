package net.one97.paytm.common.entity.events;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRSeatDetailsModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="inventory")
  private int mInventory;
  @b(a="price")
  private int mPrice;
  @b(a="provider_seat_id")
  private String mProviderSeatId;
  @b(a="seat_description")
  private String mSeatDescription;
  @b(a="seat_id")
  private int mSeatId;
  @b(a="seat_limit")
  private int mSeatLimit;
  @b(a="seat_min")
  private int mSeatMin;
  @b(a="seat_type")
  private String mSeatType;
  @b(a="seats_available")
  private int mSeatsAvailable;
  private int mSelectedQuantity;
  private int mSelectedQuantityAtStart;
  
  public void decrementQuantityAtStart()
  {
    this.mSelectedQuantityAtStart -= 1;
  }
  
  public int getInventory()
  {
    return this.mInventory;
  }
  
  public int getPrice()
  {
    return this.mPrice;
  }
  
  public String getProviderSeatId()
  {
    return this.mProviderSeatId;
  }
  
  public String getSeatDescription()
  {
    return this.mSeatDescription;
  }
  
  public int getSeatId()
  {
    return this.mSeatId;
  }
  
  public int getSeatLimit()
  {
    return this.mSeatLimit;
  }
  
  public int getSeatMin()
  {
    return this.mSeatMin;
  }
  
  public String getSeatType()
  {
    return this.mSeatType;
  }
  
  public int getSeatsAvailable()
  {
    return this.mSeatsAvailable;
  }
  
  public int getSelectedQuantity()
  {
    return this.mSelectedQuantity;
  }
  
  public int getSelectedQuantityAtStart()
  {
    return this.mSelectedQuantityAtStart;
  }
  
  public void incrementQuantityAtStart()
  {
    this.mSelectedQuantityAtStart += 1;
  }
  
  public void setInventory(int paramInt)
  {
    this.mInventory = paramInt;
  }
  
  public void setPrice(int paramInt)
  {
    this.mPrice = paramInt;
  }
  
  public void setProviderSeatId(String paramString)
  {
    this.mProviderSeatId = paramString;
  }
  
  public void setSeatDescription(String paramString)
  {
    this.mSeatDescription = paramString;
  }
  
  public void setSeatId(int paramInt)
  {
    this.mSeatId = this.mSeatId;
  }
  
  public void setSeatLimit(int paramInt)
  {
    this.mSeatLimit = paramInt;
  }
  
  public void setSeatMin(int paramInt)
  {
    this.mSeatMin = paramInt;
  }
  
  public void setSeatType(String paramString)
  {
    this.mSeatType = paramString;
  }
  
  public void setSeatsAvailable(int paramInt)
  {
    this.mSeatsAvailable = paramInt;
  }
  
  public void setSelectedQuantity(int paramInt)
  {
    this.mSelectedQuantity = paramInt;
  }
  
  public void setSelectedQuantityAtStart(int paramInt)
  {
    this.mSelectedQuantityAtStart = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/events/CJRSeatDetailsModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */