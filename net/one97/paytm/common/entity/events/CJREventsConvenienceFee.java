package net.one97.paytm.common.entity.events;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJREventsConvenienceFee
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="convFee")
  private double convFee;
  @b(a="otherTax")
  private double otherTax;
  @b(a="paytmCommission")
  private double paytmCommission;
  @b(a="pgCharges")
  private double pgCharges;
  @b(a="serviceTax")
  private double serviceTax;
  @b(a="swachBharatCess")
  private double swachBharatCess;
  @b(a="totalCommision")
  private double totalCommision;
  @b(a="totalConvFee")
  private double totalConvFee;
  @b(a="totalPgCharges")
  private double totalPgCharges;
  @b(a="totalServiceTax")
  private double totalServiceTax;
  @b(a="totalSwachBharatCess")
  private double totalSwachBharatCess;
  
  public double getConvFee()
  {
    return this.convFee;
  }
  
  public double getOtherTax()
  {
    return this.otherTax;
  }
  
  public double getPaytmCommission()
  {
    return this.paytmCommission;
  }
  
  public double getPgCharges()
  {
    return this.pgCharges;
  }
  
  public double getServiceTax()
  {
    return this.serviceTax;
  }
  
  public double getSwachBharatCess()
  {
    return this.swachBharatCess;
  }
  
  public double getTotalCommision()
  {
    return this.totalCommision;
  }
  
  public double getTotalConvFee()
  {
    return this.totalConvFee;
  }
  
  public double getTotalPgCharges()
  {
    return this.totalPgCharges;
  }
  
  public double getTotalServiceTax()
  {
    return this.totalServiceTax;
  }
  
  public double getTotalSwachBharatCess()
  {
    return this.totalSwachBharatCess;
  }
  
  public void setConvFee(double paramDouble)
  {
    this.convFee = paramDouble;
  }
  
  public void setOtherTax(double paramDouble)
  {
    this.otherTax = paramDouble;
  }
  
  public void setPaytmCommission(double paramDouble)
  {
    this.paytmCommission = paramDouble;
  }
  
  public void setPgCharges(double paramDouble)
  {
    this.pgCharges = paramDouble;
  }
  
  public void setServiceTax(double paramDouble)
  {
    this.serviceTax = paramDouble;
  }
  
  public void setSwachBharatCess(double paramDouble)
  {
    this.swachBharatCess = paramDouble;
  }
  
  public void setTotalCommision(double paramDouble)
  {
    this.totalCommision = paramDouble;
  }
  
  public void setTotalConvFee(double paramDouble)
  {
    this.totalConvFee = paramDouble;
  }
  
  public void setTotalPgCharges(double paramDouble)
  {
    this.totalPgCharges = paramDouble;
  }
  
  public void setTotalServiceTax(double paramDouble)
  {
    this.totalServiceTax = paramDouble;
  }
  
  public void setTotalSwachBharatCess(double paramDouble)
  {
    this.totalSwachBharatCess = paramDouble;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/events/CJREventsConvenienceFee.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */