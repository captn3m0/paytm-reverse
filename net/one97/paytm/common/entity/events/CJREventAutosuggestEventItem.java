package net.one97.paytm.common.entity.events;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJREventAutosuggestEventItem
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="category")
  private ArrayList<String> category = new ArrayList();
  @b(a="cities")
  private ArrayList<String> cities = new ArrayList();
  @b(a="event_id")
  private String eventId;
  @b(a="event_name")
  private String eventName;
  @b(a="provider_id")
  private String providerId;
  private int type = 1;
  
  public ArrayList<String> getCategory()
  {
    return this.category;
  }
  
  public ArrayList<String> getCities()
  {
    return this.cities;
  }
  
  public String getEventId()
  {
    return this.eventId;
  }
  
  public String getEventName()
  {
    return this.eventName;
  }
  
  public String getProviderId()
  {
    return this.providerId;
  }
  
  public int getType()
  {
    return this.type;
  }
  
  public void setCategory(ArrayList<String> paramArrayList)
  {
    this.category = paramArrayList;
  }
  
  public void setCities(ArrayList<String> paramArrayList)
  {
    this.cities = paramArrayList;
  }
  
  public void setEventId(String paramString)
  {
    this.eventId = paramString;
  }
  
  public void setEventName(String paramString)
  {
    this.eventName = paramString;
  }
  
  public void setProviderId(String paramString)
  {
    this.providerId = paramString;
  }
  
  public void setType(int paramInt)
  {
    this.type = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/events/CJREventAutosuggestEventItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */