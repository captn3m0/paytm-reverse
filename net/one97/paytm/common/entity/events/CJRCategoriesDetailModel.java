package net.one97.paytm.common.entity.events;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRCategoriesDetailModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="category")
  private String mCategory;
  @b(a="color")
  private String mColor;
  
  public String getCategory()
  {
    return this.mCategory;
  }
  
  public String getColor()
  {
    return this.mColor;
  }
  
  public void setCategory(String paramString)
  {
    this.mCategory = paramString;
  }
  
  public void setColor(String paramString)
  {
    this.mColor = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/events/CJRCategoriesDetailModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */