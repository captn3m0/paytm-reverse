package net.one97.paytm.common.entity.events;

import com.google.c.a.b;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJREventDateTimeModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="eventDates")
  private List<String> mEventDateList = new ArrayList();
  @b(a="seatDetails")
  private List<CJRSeatDetailsModel> mSeatDetails = new ArrayList();
  @b(a="time")
  private Time mTime;
  
  public List<String> getmEventDateList()
  {
    return this.mEventDateList;
  }
  
  public List<CJRSeatDetailsModel> getmSeatDetails()
  {
    return this.mSeatDetails;
  }
  
  public Time getmTime()
  {
    return this.mTime;
  }
  
  public void setmEventDateList(List<String> paramList)
  {
    this.mEventDateList = paramList;
  }
  
  public void setmSeatDetails(List<CJRSeatDetailsModel> paramList)
  {
    this.mSeatDetails = paramList;
  }
  
  public void setmTime(Time paramTime)
  {
    this.mTime = paramTime;
  }
  
  public class Time
    implements Serializable
  {
    private static final long serialVersionUID = 1L;
    @b(a="start")
    private String mStart;
    @b(a="to")
    private String mTo;
    
    public Time() {}
    
    public String getmStart()
    {
      return this.mStart;
    }
    
    public String getmTo()
    {
      return this.mTo;
    }
    
    public void setmStart(String paramString)
    {
      this.mStart = paramString;
    }
    
    public void setmTo(String paramString)
    {
      this.mTo = paramString;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/events/CJREventDateTimeModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */