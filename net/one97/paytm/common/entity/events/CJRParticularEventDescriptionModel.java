package net.one97.paytm.common.entity.events;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRParticularEventDescriptionModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  private String citySearched;
  private String entityCity;
  @b(a="active_from")
  private String mActiveFrom;
  @b(a="active_to")
  private String mActiveTo;
  @b(a="addressList")
  private ArrayList<CJRAddressDetailModel> mAddressList = new ArrayList();
  @b(a="categories")
  private List<CJRCategoriesDetailModel> mCategories = new ArrayList();
  @b(a="conv_fee")
  private String mConvFee;
  @b(a="description")
  private String mDescription;
  @b(a="form")
  private String mForm;
  @b(a="id")
  private String mId;
  @b(a="instructions")
  private String mInstructions;
  @b(a="inventory")
  private String mInventory;
  @b(a="name")
  private String mName;
  @b(a="paytm_commission")
  private String mPaytmCommission;
  @b(a="pg_charges")
  private String mPgCharges;
  @b(a="price")
  private String mPrice;
  @b(a="product_id")
  private String mProductId;
  @b(a="provider_event_id")
  private String mProviderEventId;
  @b(a="provider_id")
  private String mProviderId;
  @b(a="provider_name")
  private String mProviderName;
  @b(a="resources")
  private List<CJRResourceDetailModel> mResources = new ArrayList();
  @b(a="tags")
  private String mTags;
  @b(a="terms")
  private String mTerms;
  
  public String getActiveFrom()
  {
    return this.mActiveFrom;
  }
  
  public String getActiveTo()
  {
    return this.mActiveTo;
  }
  
  public List<CJRCategoriesDetailModel> getCategories()
  {
    return this.mCategories;
  }
  
  public String getCitySearched()
  {
    return this.citySearched;
  }
  
  public String getConvFee()
  {
    return this.mConvFee;
  }
  
  public String getDescription()
  {
    return this.mDescription;
  }
  
  public String getEntityCity()
  {
    return this.entityCity;
  }
  
  public String getForm()
  {
    return this.mForm;
  }
  
  public String getId()
  {
    return this.mId;
  }
  
  public String getInstructions()
  {
    return this.mInstructions;
  }
  
  public String getInventory()
  {
    return this.mInventory;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getPaytmCommission()
  {
    return this.mPaytmCommission;
  }
  
  public String getPgCharges()
  {
    return this.mPgCharges;
  }
  
  public String getPrice()
  {
    return this.mPrice;
  }
  
  public String getProductId()
  {
    return this.mProductId;
  }
  
  public String getProviderEventId()
  {
    return this.mProviderEventId;
  }
  
  public String getProviderId()
  {
    return this.mProviderId;
  }
  
  public String getProviderName()
  {
    return this.mProviderName;
  }
  
  public List<CJRResourceDetailModel> getResources()
  {
    return this.mResources;
  }
  
  public String getTags()
  {
    return this.mTags;
  }
  
  public String getTerms()
  {
    return this.mTerms;
  }
  
  public ArrayList<CJRAddressDetailModel> getmAddressList()
  {
    return this.mAddressList;
  }
  
  public void setActiveFrom(String paramString)
  {
    this.mActiveFrom = paramString;
  }
  
  public void setActiveTo(String paramString)
  {
    this.mActiveTo = paramString;
  }
  
  public void setCategories(List<CJRCategoriesDetailModel> paramList)
  {
    this.mCategories = paramList;
  }
  
  public void setCitySearched(String paramString)
  {
    this.citySearched = paramString;
  }
  
  public void setConvFee(String paramString)
  {
    this.mConvFee = paramString;
  }
  
  public void setDescription(String paramString)
  {
    this.mDescription = paramString;
  }
  
  public void setEntityCity(String paramString)
  {
    this.entityCity = paramString;
  }
  
  public void setForm(String paramString)
  {
    this.mForm = paramString;
  }
  
  public void setId(String paramString)
  {
    this.mId = paramString;
  }
  
  public void setInstructions(String paramString)
  {
    this.mInstructions = paramString;
  }
  
  public void setInventory(String paramString)
  {
    this.mInventory = paramString;
  }
  
  public void setName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setPaytmCommission(String paramString)
  {
    this.mPaytmCommission = paramString;
  }
  
  public void setPgCharges(String paramString)
  {
    this.mPgCharges = paramString;
  }
  
  public void setPrice(String paramString)
  {
    this.mPrice = paramString;
  }
  
  public void setProductId(String paramString)
  {
    this.mProductId = paramString;
  }
  
  public void setProviderEventId(String paramString)
  {
    this.mProviderEventId = paramString;
  }
  
  public void setProviderId(String paramString)
  {
    this.mProviderId = paramString;
  }
  
  public void setProviderName(String paramString)
  {
    this.mProviderName = paramString;
  }
  
  public void setResources(List<CJRResourceDetailModel> paramList)
  {
    this.mResources = paramList;
  }
  
  public void setTags(String paramString)
  {
    this.mTags = paramString;
  }
  
  public void setTerms(String paramString)
  {
    this.mTerms = paramString;
  }
  
  public void setmAddressList(ArrayList<CJRAddressDetailModel> paramArrayList)
  {
    this.mAddressList = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/events/CJRParticularEventDescriptionModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */