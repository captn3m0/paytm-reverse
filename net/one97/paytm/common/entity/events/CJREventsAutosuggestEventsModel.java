package net.one97.paytm.common.entity.events;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJREventsAutosuggestEventsModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="events")
  private CJREventAutosuggestModel events;
  
  public CJREventAutosuggestModel getEvents()
  {
    return this.events;
  }
  
  public void setEvents(CJREventAutosuggestModel paramCJREventAutosuggestModel)
  {
    this.events = paramCJREventAutosuggestModel;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/events/CJREventsAutosuggestEventsModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */