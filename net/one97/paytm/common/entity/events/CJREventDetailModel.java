package net.one97.paytm.common.entity.events;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJREventDetailModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="active_from")
  private String mActiveFrom;
  @b(a="active_to")
  private String mActiveTo;
  @b(a="address")
  private String mAddress;
  @b(a="categories")
  private ArrayList<CJRCategoriesDetailModel> mCategories = new ArrayList();
  @b(a="cities")
  private List<String> mCities = new ArrayList();
  @b(a="id")
  private String mId;
  @b(a="image")
  private String mImage;
  @b(a="name")
  private String mName;
  @b(a="price")
  private String mPrice;
  @b(a="provider_id")
  private String mProviderId;
  @b(a="provider_priority")
  private String mProviderPriority;
  @b(a="tags")
  private String mtags;
  
  public String getActiveFrom()
  {
    return this.mActiveFrom;
  }
  
  public String getActiveTo()
  {
    return this.mActiveTo;
  }
  
  public String getAddress()
  {
    return this.mAddress;
  }
  
  public ArrayList<CJRCategoriesDetailModel> getCategories()
  {
    return this.mCategories;
  }
  
  public List<String> getCities()
  {
    return this.mCities;
  }
  
  public String getId()
  {
    return this.mId;
  }
  
  public String getImageURL()
  {
    return this.mImage;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getPrice()
  {
    return this.mPrice;
  }
  
  public String getProviderId()
  {
    return this.mProviderId;
  }
  
  public String getProviderPriority()
  {
    return this.mProviderPriority;
  }
  
  public String gettags()
  {
    return this.mtags;
  }
  
  public void setActiveFrom(String paramString)
  {
    this.mActiveFrom = paramString;
  }
  
  public void setActiveTo(String paramString)
  {
    this.mActiveTo = paramString;
  }
  
  public void setAddress(String paramString)
  {
    this.mAddress = paramString;
  }
  
  public void setCategories(ArrayList<CJRCategoriesDetailModel> paramArrayList)
  {
    this.mCategories = paramArrayList;
  }
  
  public void setCities(List<String> paramList)
  {
    this.mCities = paramList;
  }
  
  public void setId(String paramString)
  {
    this.mId = paramString;
  }
  
  public void setImage(String paramString)
  {
    this.mImage = paramString;
  }
  
  public void setName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setPrice(String paramString)
  {
    this.mPrice = paramString;
  }
  
  public void setProviderId(String paramString)
  {
    this.mProviderId = paramString;
  }
  
  public void setProviderPriority(String paramString)
  {
    this.mProviderPriority = paramString;
  }
  
  public void settags(String paramString)
  {
    this.mtags = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/events/CJREventDetailModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */