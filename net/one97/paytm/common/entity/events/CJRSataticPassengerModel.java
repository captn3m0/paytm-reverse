package net.one97.paytm.common.entity.events;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRSataticPassengerModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="address")
  private String address;
  @b(a="addressError")
  private String addressError;
  @b(a="age")
  private String age;
  @b(a="ageError")
  private String ageError;
  @b(a="city")
  private String city;
  @b(a="cityError")
  private String cityError;
  @b(a="email")
  private String email;
  @b(a="emailError")
  private String emailError;
  @b(a="genderError")
  private String genderError;
  @b(a="mobileError")
  private String mobileError;
  @b(a="mobileNumber")
  private String mobileNumber;
  @b(a="name")
  private String name;
  @b(a="nameError")
  private String nameError;
  @b(a="pinCode")
  private String pinCode;
  @b(a="pinCodeError")
  private String pinCodeError;
  @b(a="state")
  private String state;
  @b(a="stateError")
  private String stateError;
  @b(a="title")
  private String title;
  
  public String getAddress()
  {
    return this.address;
  }
  
  public String getAddressError()
  {
    return this.addressError;
  }
  
  public String getAge()
  {
    return this.age;
  }
  
  public String getAgeError()
  {
    return this.ageError;
  }
  
  public String getCity()
  {
    return this.city;
  }
  
  public String getCityError()
  {
    return this.cityError;
  }
  
  public String getEmail()
  {
    return this.email;
  }
  
  public String getEmailError()
  {
    return this.emailError;
  }
  
  public String getGenderError()
  {
    return this.genderError;
  }
  
  public String getMobileError()
  {
    return this.mobileError;
  }
  
  public String getMobileNumber()
  {
    return this.mobileNumber;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getNameError()
  {
    return this.nameError;
  }
  
  public String getPinCode()
  {
    return this.pinCode;
  }
  
  public String getPinCodeError()
  {
    return this.pinCodeError;
  }
  
  public String getState()
  {
    return this.state;
  }
  
  public String getStateError()
  {
    return this.stateError;
  }
  
  public String getTitle()
  {
    return this.title;
  }
  
  public void setAddress(String paramString)
  {
    this.address = paramString;
  }
  
  public void setAddressError(String paramString)
  {
    this.addressError = paramString;
  }
  
  public void setAge(String paramString)
  {
    this.age = paramString;
  }
  
  public void setAgeError(String paramString)
  {
    this.ageError = paramString;
  }
  
  public void setCity(String paramString)
  {
    this.city = paramString;
  }
  
  public void setCityError(String paramString)
  {
    this.cityError = paramString;
  }
  
  public void setEmail(String paramString)
  {
    this.email = paramString;
  }
  
  public void setEmailError(String paramString)
  {
    this.emailError = paramString;
  }
  
  public void setGenderError(String paramString)
  {
    this.genderError = paramString;
  }
  
  public void setMobileError(String paramString)
  {
    this.mobileError = paramString;
  }
  
  public void setMobileNumber(String paramString)
  {
    this.mobileNumber = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setNameError(String paramString)
  {
    this.nameError = paramString;
  }
  
  public void setPinCode(String paramString)
  {
    this.pinCode = paramString;
  }
  
  public void setPinCodeError(String paramString)
  {
    this.pinCodeError = paramString;
  }
  
  public void setState(String paramString)
  {
    this.state = paramString;
  }
  
  public void setStateError(String paramString)
  {
    this.stateError = paramString;
  }
  
  public void setTitle(String paramString)
  {
    this.title = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/events/CJRSataticPassengerModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */