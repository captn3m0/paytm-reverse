package net.one97.paytm.common.entity.events;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRInputSeatInfo
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="count")
  private String count = "1";
  @b(a="description")
  private String description = "";
  @b(a="pricePerSeat")
  private String pricePerSeat = "600";
  @b(a="providerSeatId")
  private String providerSeatId = "97700";
  @b(a="seatId")
  private String seatId = "25929";
  @b(a="seatType")
  private String seatType = "fees";
  
  public String getCount()
  {
    return this.count;
  }
  
  public String getDescription()
  {
    return this.description;
  }
  
  public String getPricePerSeat()
  {
    return this.pricePerSeat;
  }
  
  public String getProviderSeatId()
  {
    return this.providerSeatId;
  }
  
  public String getSeatId()
  {
    return this.seatId;
  }
  
  public String getSeatType()
  {
    return this.seatType;
  }
  
  public void setCount(String paramString)
  {
    this.count = paramString;
  }
  
  public void setDescription(String paramString)
  {
    this.description = paramString;
  }
  
  public void setPricePerSeat(String paramString)
  {
    this.pricePerSeat = paramString;
  }
  
  public void setProviderSeatId(String paramString)
  {
    this.providerSeatId = paramString;
  }
  
  public void setSeatId(String paramString)
  {
    this.seatId = paramString;
  }
  
  public void setSeatType(String paramString)
  {
    this.seatType = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/events/CJRInputSeatInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */