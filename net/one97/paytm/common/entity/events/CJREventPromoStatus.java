package net.one97.paytm.common.entity.events;

import com.google.c.a.b;
import java.io.Serializable;

public class CJREventPromoStatus
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @b(a="code")
  private int code;
  @b(a="message")
  private PromoMessage message;
  @b(a="result")
  private String result;
  
  public int getCode()
  {
    return this.code;
  }
  
  public PromoMessage getMessage()
  {
    return this.message;
  }
  
  public String getResult()
  {
    return this.result;
  }
  
  private class PromoMessage
    implements Serializable
  {
    private static final long serialVersionUID = 1L;
    @b(a="message")
    private String message;
    @b(a="title")
    private String title;
    
    private PromoMessage() {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/events/CJREventPromoStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */