package net.one97.paytm.common.entity.events;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPromoValidateResponseModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="cart")
  private CJRPrevalidateInputModel cart;
  @b(a="status")
  private CJREventPromoStatus status;
  
  public CJRPrevalidateInputModel getCart()
  {
    return this.cart;
  }
  
  public CJREventPromoStatus getStatus()
  {
    return this.status;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/events/CJRPromoValidateResponseModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */