package net.one97.paytm.common.entity.events;

import android.text.TextUtils;
import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class EventsTravellerResponseModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="flag")
  private int flag = 0;
  @b(a="form")
  private ArrayList<ArrayList<EventsInputForm>> form;
  @b(a="providerOid")
  private String providerOid;
  
  private void populateValuesToForm(Object paramObject, EventsInputForm paramEventsInputForm)
    throws JSONException
  {
    if ((paramObject == null) || (paramEventsInputForm == null)) {}
    do
    {
      do
      {
        return;
        localObject = paramEventsInputForm.getType();
      } while (TextUtils.isEmpty((CharSequence)localObject));
      if (((String)localObject).equalsIgnoreCase("checkbox"))
      {
        paramObject = (JSONArray)paramObject;
        localObject = new ArrayList();
        i = 0;
        while (i < ((JSONArray)paramObject).length())
        {
          FormValuesData localFormValuesData = new FormValuesData();
          localFormValuesData.setValue(((JSONArray)paramObject).getJSONObject(i).getString("value"));
          ((List)localObject).add(localFormValuesData);
          i += 1;
        }
        paramEventsInputForm.setCheckBoxListValues((List)localObject);
        return;
      }
      if (((String)localObject).equalsIgnoreCase("dropdown"))
      {
        paramObject = (JSONArray)paramObject;
        localObject = new ArrayList();
        i = 0;
        while (i < ((JSONArray)paramObject).length())
        {
          ((List)localObject).add(((JSONArray)paramObject).getString(i));
          i += 1;
        }
        paramEventsInputForm.setDropDownListValues((List)localObject);
        return;
      }
    } while (!((String)localObject).equalsIgnoreCase("radio"));
    paramObject = (JSONArray)paramObject;
    Object localObject = new ArrayList();
    int i = 0;
    while (i < ((JSONArray)paramObject).length())
    {
      ((List)localObject).add(((JSONArray)paramObject).getString(i));
      i += 1;
    }
    paramEventsInputForm.setRadioListValues((List)localObject);
  }
  
  public int getFlag()
  {
    return this.flag;
  }
  
  public ArrayList<ArrayList<EventsInputForm>> getForm()
  {
    return this.form;
  }
  
  public String getProviderOid()
  {
    return this.providerOid;
  }
  
  public void parseJsonString(String paramString)
  {
    try
    {
      paramString = new JSONObject(paramString);
      this.flag = paramString.getInt("flag");
      if (this.flag == 0)
      {
        this.form = null;
        return;
      }
      if (this.flag == 1)
      {
        this.providerOid = paramString.getString("providerOid");
        paramString = paramString.getJSONArray("form");
        this.form = new ArrayList();
        int i = 0;
        while (i < paramString.length())
        {
          JSONArray localJSONArray = paramString.getJSONArray(i);
          ArrayList localArrayList = new ArrayList();
          int j = 0;
          while (j < localJSONArray.length())
          {
            JSONObject localJSONObject = localJSONArray.getJSONObject(j);
            EventsInputForm localEventsInputForm = new EventsInputForm();
            if (localJSONObject.has("title")) {
              localEventsInputForm.setTitle(localJSONObject.getString("title"));
            }
            if (localJSONObject.has("type")) {
              localEventsInputForm.setType(localJSONObject.getString("type"));
            }
            if (localJSONObject.has("applied")) {
              localEventsInputForm.setApplied(localJSONObject.getString("applied"));
            }
            if (localJSONObject.has("id")) {
              localEventsInputForm.setId(localJSONObject.getString("id"));
            }
            if (localJSONObject.has("regex")) {
              localEventsInputForm.setRegex(localJSONObject.getString("regex"));
            }
            if (localJSONObject.has("values")) {
              populateValuesToForm(localJSONObject.get("values"), localEventsInputForm);
            }
            if (localJSONObject.has("seat_id")) {
              localEventsInputForm.setSeatId(localJSONObject.getString("seat_id"));
            }
            if (localJSONObject.has("provider_seat_id")) {
              localEventsInputForm.setProviderSeatId(localJSONObject.getString("provider_seat_id"));
            }
            localArrayList.add(localEventsInputForm);
            j += 1;
          }
          this.form.add(localArrayList);
          i += 1;
        }
      }
      return;
    }
    catch (JSONException paramString)
    {
      paramString.printStackTrace();
    }
  }
  
  public void setFlag(int paramInt)
  {
    this.flag = paramInt;
  }
  
  public void setForm(ArrayList<ArrayList<EventsInputForm>> paramArrayList)
  {
    this.form = paramArrayList;
  }
  
  public void setProviderOid(String paramString)
  {
    this.providerOid = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/events/EventsTravellerResponseModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */