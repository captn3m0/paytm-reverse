package net.one97.paytm.common.entity.events;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJREventModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="activeCategory")
  private String mActiveCategory;
  @b(a="categories")
  private List<String> mCategoryList = new ArrayList();
  @b(a="ent_list")
  private ArrayList<CJREventDetailModel> mEventList = new ArrayList();
  @b(a="trending")
  private ArrayList<CJREventDetailModel> mTrendingEventList = new ArrayList();
  
  public String getmActiveCategory()
  {
    return this.mActiveCategory;
  }
  
  public List<String> getmCategoryList()
  {
    return this.mCategoryList;
  }
  
  public ArrayList<CJREventDetailModel> getmEventList()
  {
    return this.mEventList;
  }
  
  public ArrayList<CJREventDetailModel> getmTrendingEventList()
  {
    return this.mTrendingEventList;
  }
  
  public void setmActiveCategory(String paramString)
  {
    this.mActiveCategory = paramString;
  }
  
  public void setmCategoryList(List<String> paramList)
  {
    this.mCategoryList = paramList;
  }
  
  public void setmEventList(ArrayList<CJREventDetailModel> paramArrayList)
  {
    this.mEventList = paramArrayList;
  }
  
  public void setmTrendingEventList(ArrayList<CJREventDetailModel> paramArrayList)
  {
    this.mTrendingEventList = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/events/CJREventModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */