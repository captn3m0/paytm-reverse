package net.one97.paytm.common.entity.events;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJREventAutosuggestModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="restevents")
  private ArrayList<CJREventAutosuggestEventItem> restEvents = new ArrayList();
  @b(a="today")
  private ArrayList<CJREventAutosuggestEventItem> todayEvents = new ArrayList();
  @b(a="tomorrow")
  private ArrayList<CJREventAutosuggestEventItem> tomorrowEvents = new ArrayList();
  @b(a="weekend")
  private ArrayList<CJREventAutosuggestEventItem> weekendEvents = new ArrayList();
  
  public ArrayList<CJREventAutosuggestEventItem> getRestEvents()
  {
    return this.restEvents;
  }
  
  public ArrayList<CJREventAutosuggestEventItem> getTodayEvents()
  {
    return this.todayEvents;
  }
  
  public ArrayList<CJREventAutosuggestEventItem> getTomorrowEvents()
  {
    return this.tomorrowEvents;
  }
  
  public ArrayList<CJREventAutosuggestEventItem> getWeekendEvents()
  {
    return this.weekendEvents;
  }
  
  public void setRestEvents(ArrayList<CJREventAutosuggestEventItem> paramArrayList)
  {
    this.restEvents = paramArrayList;
  }
  
  public void setTodayEvents(ArrayList<CJREventAutosuggestEventItem> paramArrayList)
  {
    this.todayEvents = paramArrayList;
  }
  
  public void setTomorrowEvents(ArrayList<CJREventAutosuggestEventItem> paramArrayList)
  {
    this.tomorrowEvents = paramArrayList;
  }
  
  public void setWeekendEvents(ArrayList<CJREventAutosuggestEventItem> paramArrayList)
  {
    this.weekendEvents = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/events/CJREventAutosuggestModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */