package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.HashMap;

public class CJRRequestActionDesc
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="ipAddress")
  private String mIpAddress;
  @b(a="metadata")
  private String mMetadata;
  @b(a="operationType")
  private String mOperationType;
  @b(a="platformName")
  private String mPlatformName;
  @b(a="request")
  private HashMap<String, String> mRequest;
  
  public String getName()
  {
    return null;
  }
  
  public HashMap<String, String> getRequest()
  {
    return this.mRequest;
  }
  
  public String getmIpAddress()
  {
    return this.mIpAddress;
  }
  
  public String getmMetadata()
  {
    return this.mMetadata;
  }
  
  public String getmOperationType()
  {
    return this.mOperationType;
  }
  
  public String getmPlatformName()
  {
    return this.mPlatformName;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRRequestActionDesc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */