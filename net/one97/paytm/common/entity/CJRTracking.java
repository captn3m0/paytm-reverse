package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRTracking
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="click")
  private String mClick;
  @b(a="impression")
  private String mImpression;
  
  public String getClick()
  {
    return this.mClick;
  }
  
  public String getImpression()
  {
    return this.mImpression;
  }
  
  public String getName()
  {
    return null;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRTracking.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */