package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRLocalityDestinationList
  implements IJRDataModel
{
  @b(a="c")
  private String mLocalityLocation;
  @b(a="n")
  private String mLocalityName;
  
  public String getLocalityLocation()
  {
    return this.mLocalityLocation;
  }
  
  public String getLocalityName()
  {
    return this.mLocalityName;
  }
  
  public void setLocalityLocation(String paramString)
  {
    this.mLocalityLocation = paramString;
  }
  
  public void setLocalityName(String paramString)
  {
    this.mLocalityName = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRLocalityDestinationList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */