package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRProvisionalBookingData
  implements IJRDataModel
{
  @b(a="amount")
  private String mProvisionalBookingAmount;
  @b(a="email")
  private String mProvisionalBookingEmail;
  @b(a="firstname")
  private String mProvisionalBookingFirstName;
  @b(a="guest")
  private String mProvisionalBookingGuest;
  @b(a="gobookingid")
  private String mProvisionalBookingId;
  @b(a="udf1")
  private String mProvisionalBookingUdf1;
  @b(a="productinfo")
  private String mProvisionalBookingproductInfo;
  
  public String getProvisionalBookingAmount()
  {
    return this.mProvisionalBookingAmount;
  }
  
  public String getProvisionalBookingEmail()
  {
    return this.mProvisionalBookingEmail;
  }
  
  public String getProvisionalBookingFirstName()
  {
    return this.mProvisionalBookingFirstName;
  }
  
  public String getProvisionalBookingGuest()
  {
    return this.mProvisionalBookingGuest;
  }
  
  public String getProvisionalBookingId()
  {
    return this.mProvisionalBookingId;
  }
  
  public String getProvisionalBookingUdf1()
  {
    return this.mProvisionalBookingUdf1;
  }
  
  public String getProvisionalBookingproductInfo()
  {
    return this.mProvisionalBookingproductInfo;
  }
  
  public void setProvisionalBookingAmount(String paramString)
  {
    this.mProvisionalBookingAmount = paramString;
  }
  
  public void setProvisionalBookingEmail(String paramString)
  {
    this.mProvisionalBookingEmail = paramString;
  }
  
  public void setProvisionalBookingFirstName(String paramString)
  {
    this.mProvisionalBookingFirstName = paramString;
  }
  
  public void setProvisionalBookingGuest(String paramString)
  {
    this.mProvisionalBookingGuest = paramString;
  }
  
  public void setProvisionalBookingId(String paramString)
  {
    this.mProvisionalBookingId = paramString;
  }
  
  public void setProvisionalBookingUdf1(String paramString)
  {
    this.mProvisionalBookingUdf1 = paramString;
  }
  
  public void setProvisionalBookingproductInfo(String paramString)
  {
    this.mProvisionalBookingproductInfo = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRProvisionalBookingData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */