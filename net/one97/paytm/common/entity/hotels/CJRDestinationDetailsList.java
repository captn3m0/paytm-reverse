package net.one97.paytm.common.entity.hotels;

import android.text.TextUtils;
import com.google.c.a.b;
import java.util.ArrayList;
import java.util.Iterator;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRDestinationDetailsList
  implements IJRDataModel
{
  @b(a="c")
  private String mDestinationCount;
  @b(a="p")
  private ArrayList<String> mDestinationName;
  @b(a="i")
  private ArrayList<String> mDestinationValue;
  
  public String getCityName()
  {
    if ((this.mDestinationName == null) || (this.mDestinationName.size() < 0)) {
      return "";
    }
    return (String)this.mDestinationName.get(this.mDestinationName.size() - 1);
  }
  
  public String getDestinationCount()
  {
    return this.mDestinationCount;
  }
  
  public String getDestinationName()
  {
    String str1 = "";
    if ((this.mDestinationName == null) || (this.mDestinationName.size() < 0)) {
      return "";
    }
    Iterator localIterator = this.mDestinationName.iterator();
    while (localIterator.hasNext())
    {
      String str2 = (String)localIterator.next();
      if (TextUtils.isEmpty(str1)) {
        str1 = str1 + str2;
      } else {
        str1 = str1 + ", " + str2;
      }
    }
    return str1;
  }
  
  public ArrayList<String> getDestinationValue()
  {
    return this.mDestinationValue;
  }
  
  public String getHotelName()
  {
    if ((this.mDestinationName == null) || (this.mDestinationName.size() < 0)) {
      return "";
    }
    return (String)this.mDestinationName.get(0);
  }
  
  public void setDestinationCount(String paramString)
  {
    this.mDestinationCount = paramString;
  }
  
  public void setDestinationName(ArrayList<String> paramArrayList)
  {
    this.mDestinationName = paramArrayList;
  }
  
  public void setDestinationValue(ArrayList<String> paramArrayList)
  {
    this.mDestinationValue = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRDestinationDetailsList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */