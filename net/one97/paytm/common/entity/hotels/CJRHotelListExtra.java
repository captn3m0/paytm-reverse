package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.Iterator;
import net.one97.paytm.common.entity.CJRFilterItem;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRHotelListExtra
  implements IJRDataModel
{
  @b(a="filters")
  private ArrayList<CJRFilterItem> mFilterList = new ArrayList();
  @b(a="hotelCount")
  private String mHotelCount;
  @b(a="sort_keys")
  private ArrayList<CJRSortKeyItem> mSortKeys = new ArrayList();
  
  public void clearData()
  {
    if (this.mFilterList != null) {
      this.mFilterList.clear();
    }
  }
  
  public CJRFilterItem getFilterItemByTitle(String paramString1, String paramString2)
  {
    Object localObject2 = null;
    Iterator localIterator = this.mFilterList.iterator();
    Object localObject1;
    do
    {
      localObject1 = localObject2;
      if (!localIterator.hasNext()) {
        break;
      }
      localObject1 = (CJRFilterItem)localIterator.next();
    } while ((!((CJRFilterItem)localObject1).getTitle().equalsIgnoreCase(paramString1)) || (!((CJRFilterItem)localObject1).getType().equalsIgnoreCase(paramString2)));
    return (CJRFilterItem)localObject1;
  }
  
  public ArrayList<CJRFilterItem> getFilterList()
  {
    return this.mFilterList;
  }
  
  public String getHotelCount()
  {
    return this.mHotelCount;
  }
  
  public ArrayList<CJRSortKeyItem> getSortKeys()
  {
    return this.mSortKeys;
  }
  
  public void setFilterList(ArrayList<CJRFilterItem> paramArrayList)
  {
    this.mFilterList = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRHotelListExtra.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */