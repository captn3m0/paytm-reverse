package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRHotelRatings
  implements IJRDataModel
{
  @b(a="TAHotelId")
  private String mHotelId;
  @b(a="starRating")
  private String mHotelStarRating;
  @b(a="TARating")
  private String mHotelTARating;
  
  public String getHotelId()
  {
    return this.mHotelId;
  }
  
  public String getHotelStarRating()
  {
    return this.mHotelStarRating;
  }
  
  public String getHotelTARating()
  {
    return this.mHotelTARating;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRHotelRatings.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */