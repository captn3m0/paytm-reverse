package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRHotelDestinationSuggestionList
  implements IJRDataModel
{
  @b(a="d")
  private ArrayList<CJRDestinationDetailsList> mDestinationDetailsList;
  @b(a="t")
  private String mDestinationTitle;
  @b(a="k")
  private ArrayList<String> mKeyToBeSent;
  
  public ArrayList<CJRDestinationDetailsList> getDestinationDetailsList()
  {
    return this.mDestinationDetailsList;
  }
  
  public String getDestinationTitle()
  {
    return this.mDestinationTitle;
  }
  
  public ArrayList<String> getKeyToBeSent()
  {
    return this.mKeyToBeSent;
  }
  
  public void setDestinationDetailsList(ArrayList<CJRDestinationDetailsList> paramArrayList)
  {
    this.mDestinationDetailsList = paramArrayList;
  }
  
  public void setDestinationTitle(String paramString)
  {
    this.mDestinationTitle = paramString;
  }
  
  public void setKeyToBeSent(ArrayList<String> paramArrayList)
  {
    this.mKeyToBeSent = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRHotelDestinationSuggestionList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */