package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAvailableHotelList
  implements IJRDataModel
{
  @b(a="data")
  private ArrayList<CJRAvailableHotelListItem> mData;
  @b(a="extra")
  private CJRHotelListExtra mHotelListExtra;
  @b(a="message")
  private String mMessage;
  @b(a="status")
  private int mStatus;
  
  public ArrayList<CJRAvailableHotelListItem> getData()
  {
    return this.mData;
  }
  
  public CJRHotelListExtra getHotelListExtra()
  {
    return this.mHotelListExtra;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public int getStatus()
  {
    return this.mStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRAvailableHotelList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */