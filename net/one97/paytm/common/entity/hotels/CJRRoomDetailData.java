package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRRoomDetailData
  implements IJRDataModel
{
  @b(a="offer_url")
  private String mOfferUrl;
  @b(a="roomOptions")
  private ArrayList<CJRHotelRoomOptions> mRoomOptionsList;
  @b(a="tnc")
  private CJRHotelTNC mTNC;
  
  public String getOfferUrl()
  {
    return this.mOfferUrl;
  }
  
  public ArrayList<CJRHotelRoomOptions> getRoomOptionsList()
  {
    return this.mRoomOptionsList;
  }
  
  public CJRHotelTNC getTNC()
  {
    return this.mTNC;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRRoomDetailData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */