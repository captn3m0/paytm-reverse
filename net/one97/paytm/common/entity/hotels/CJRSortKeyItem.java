package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRSortKeyItem
  implements IJRDataModel
{
  @b(a="default")
  private String defaultValue;
  private boolean mIsSelectedSubitem;
  private boolean mSelected;
  private int mSortCount;
  private String mSortOrder;
  @b(a="subItems")
  private ArrayList<CJRSortKeyItem> mSubItemList = new ArrayList();
  @b(a="name")
  private String name;
  @b(a="urlParams")
  private String urlParams;
  
  public String getDefaultValue()
  {
    return this.defaultValue;
  }
  
  public boolean getIsSelectedSubItem()
  {
    return this.mIsSelectedSubitem;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public int getSortCount()
  {
    return this.mSortCount;
  }
  
  public String getSortOrder()
  {
    return this.mSortOrder;
  }
  
  public String getUrlParams()
  {
    return this.urlParams;
  }
  
  public ArrayList<CJRSortKeyItem> getmSubItemList()
  {
    return this.mSubItemList;
  }
  
  public boolean isSelected()
  {
    return this.mSelected;
  }
  
  public void setIsSelectedSubItem(boolean paramBoolean)
  {
    this.mIsSelectedSubitem = paramBoolean;
  }
  
  public void setSelected(boolean paramBoolean)
  {
    this.mSelected = paramBoolean;
  }
  
  public void setSortCount(int paramInt)
  {
    this.mSortCount = paramInt;
  }
  
  public void setSortOrder(String paramString)
  {
    this.mSortOrder = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRSortKeyItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */