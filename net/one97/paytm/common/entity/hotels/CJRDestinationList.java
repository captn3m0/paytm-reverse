package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRDestinationList
  implements IJRDataModel
{
  @b(a="suggestions")
  private ArrayList<CJRHotelDestinationSuggestionList> mCitySuggestionList;
  
  public ArrayList<CJRHotelDestinationSuggestionList> getCityDestinationList()
  {
    return this.mCitySuggestionList;
  }
  
  public void setCityDestinationList(ArrayList<CJRHotelDestinationSuggestionList> paramArrayList)
  {
    this.mCitySuggestionList = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRDestinationList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */