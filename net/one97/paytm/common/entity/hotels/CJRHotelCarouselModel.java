package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRHotelCarouselModel
  implements IJRDataModel
{
  @b(a="data")
  private CJRCarouselData mCarouselData;
  @b(a="code")
  private int mCode;
  @b(a="message")
  private String mMessage;
  
  public CJRCarouselData getCarouselData()
  {
    return this.mCarouselData;
  }
  
  public int getCode()
  {
    return this.mCode;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public void setCarouselData(CJRCarouselData paramCJRCarouselData)
  {
    this.mCarouselData = paramCJRCarouselData;
  }
  
  public void setCode(int paramInt)
  {
    this.mCode = paramInt;
  }
  
  public void setMessage(String paramString)
  {
    this.mMessage = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRHotelCarouselModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */