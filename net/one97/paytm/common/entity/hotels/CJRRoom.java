package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRRoom
  implements IJRDataModel
{
  @b(a="num_of_adults")
  private int mAdultCount;
  @b(a="child_ages")
  private ArrayList<Integer> mChildrenAges;
  @b(a="num_of_children")
  private int mChildrenCount;
  private int mGuestCount;
  private boolean mIsAdultDecreaseEnabled;
  private boolean mIsAdultIncreaseEnabled;
  private boolean mIsChildrenDecreaseEnabled;
  private boolean mIsChildrenIncreaseEnabled;
  
  public int getAdultCount()
  {
    return this.mAdultCount;
  }
  
  public ArrayList<Integer> getChildrenAges()
  {
    return this.mChildrenAges;
  }
  
  public int getChildrenCount()
  {
    return this.mChildrenCount;
  }
  
  public int getGuestCount()
  {
    return this.mGuestCount;
  }
  
  public boolean getIsAdultDecreaseEnabled()
  {
    return this.mIsAdultDecreaseEnabled;
  }
  
  public boolean getIsAdultIncreaseEnabled()
  {
    return this.mIsAdultIncreaseEnabled;
  }
  
  public boolean getIsChildrenDecreaseEnabled()
  {
    return this.mIsChildrenDecreaseEnabled;
  }
  
  public boolean getIsChildrenIncreaseEnabled()
  {
    return this.mIsChildrenIncreaseEnabled;
  }
  
  public void setAdultCount(int paramInt)
  {
    this.mAdultCount = paramInt;
  }
  
  public void setChildrenAges(ArrayList<Integer> paramArrayList)
  {
    this.mChildrenAges = paramArrayList;
  }
  
  public void setChildrenCount(int paramInt)
  {
    this.mChildrenCount = paramInt;
  }
  
  public void setGuestCount(int paramInt)
  {
    this.mGuestCount = paramInt;
  }
  
  public void setIsAdultDecreaseEnabled(boolean paramBoolean)
  {
    this.mIsAdultDecreaseEnabled = paramBoolean;
  }
  
  public void setIsAdultIncreaseEnabled(boolean paramBoolean)
  {
    this.mIsAdultIncreaseEnabled = paramBoolean;
  }
  
  public void setIsChildrenDecreaseEnabled(boolean paramBoolean)
  {
    this.mIsChildrenDecreaseEnabled = paramBoolean;
  }
  
  public void setIsChildrenIncreaseEnabled(boolean paramBoolean)
  {
    this.mIsChildrenIncreaseEnabled = paramBoolean;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRRoom.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */