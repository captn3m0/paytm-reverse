package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRCurrentLocationData
  implements IJRDataModel
{
  @b(a="area")
  private String mAreaOfLocation;
  @b(a="city")
  private String mCityOfLocation;
  @b(a="cityExists")
  private boolean mIsCityExists;
  @b(a="pincode")
  private String mPinCodeOfLocation;
  @b(a="state")
  private String mStateOfLocation;
  
  public String getAreaOfLocation()
  {
    return this.mAreaOfLocation;
  }
  
  public String getCityOfLocation()
  {
    return this.mCityOfLocation;
  }
  
  public String getPinCodeOfLocation()
  {
    return this.mPinCodeOfLocation;
  }
  
  public String getStateOfLocation()
  {
    return this.mStateOfLocation;
  }
  
  public boolean isIsCityExists()
  {
    return this.mIsCityExists;
  }
  
  public void setAreaOfLocation(String paramString)
  {
    this.mAreaOfLocation = paramString;
  }
  
  public void setCityOfLocation(String paramString)
  {
    this.mCityOfLocation = paramString;
  }
  
  public void setIsCityExists(boolean paramBoolean)
  {
    this.mIsCityExists = paramBoolean;
  }
  
  public void setPinCodeOfLocation(String paramString)
  {
    this.mPinCodeOfLocation = paramString;
  }
  
  public void setStateOfLocation(String paramString)
  {
    this.mStateOfLocation = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRCurrentLocationData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */