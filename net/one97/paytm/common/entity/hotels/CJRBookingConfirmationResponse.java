package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBookingConfirmationResponse
  implements IJRDataModel
{
  @b(a="data")
  private CJRBookingConfirmationData mBookingConfirmationData;
  @b(a="message")
  private String mMessage;
  @b(a="status")
  private int mStatus;
  
  public CJRBookingConfirmationData getBookingConfirmationData()
  {
    return this.mBookingConfirmationData;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public int getStatus()
  {
    return this.mStatus;
  }
  
  public void setBookingConfirmationData(CJRBookingConfirmationData paramCJRBookingConfirmationData)
  {
    this.mBookingConfirmationData = paramCJRBookingConfirmationData;
  }
  
  public void setMessage(String paramString)
  {
    this.mMessage = paramString;
  }
  
  public void setStatus(int paramInt)
  {
    this.mStatus = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRBookingConfirmationResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */