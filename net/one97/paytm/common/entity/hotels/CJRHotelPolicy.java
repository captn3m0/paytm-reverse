package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRHotelPolicy
  implements IJRDataModel
{
  @b(a="checkInTime")
  private String mHotelCheckInTime;
  @b(a="checkOutTime")
  private String mHotelCheckOutTime;
  @b(a="other")
  private ArrayList<String> mOtherHotelPolicies;
  
  public String getHotelCheckInTime()
  {
    return this.mHotelCheckInTime;
  }
  
  public String getHotelCheckOutTime()
  {
    return this.mHotelCheckOutTime;
  }
  
  public ArrayList<String> getOtherHotelPolicies()
  {
    return this.mOtherHotelPolicies;
  }
  
  public void setHotelCheckInTime(String paramString)
  {
    this.mHotelCheckInTime = paramString;
  }
  
  public void setHotelCheckOutTime(String paramString)
  {
    this.mHotelCheckOutTime = paramString;
  }
  
  public void setOtherHotelPolicies(ArrayList<String> paramArrayList)
  {
    this.mOtherHotelPolicies = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRHotelPolicy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */