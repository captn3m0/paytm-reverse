package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRHotelAutoSuggestListItem
  implements IJRDataModel
{
  @b(a="city")
  private String mAutoSuggestCity;
  @b(a="country_code")
  private String mAutoSuggestCountryCode;
  @b(a="hotel_id")
  private String mAutoSuggestHotelId;
  @b(a="name")
  private String mAutoSuggestName;
  @b(a="object_id")
  private String mAutoSuggestObjectId;
  @b(a="state")
  private String mAutoSuggestState;
  @b(a="state_country")
  private String mAutoSuggestStateCountry;
  @b(a="type")
  private String mAutoSuggestType;
  @b(a="z")
  private String mAutoSuggestz;
  
  public String getAutoSuggestCity()
  {
    return this.mAutoSuggestCity;
  }
  
  public String getAutoSuggestCountryCode()
  {
    return this.mAutoSuggestCountryCode;
  }
  
  public String getAutoSuggestHotelId()
  {
    return this.mAutoSuggestHotelId;
  }
  
  public String getAutoSuggestName()
  {
    return this.mAutoSuggestName;
  }
  
  public String getAutoSuggestObjectId()
  {
    return this.mAutoSuggestObjectId;
  }
  
  public String getAutoSuggestState()
  {
    return this.mAutoSuggestState;
  }
  
  public String getAutoSuggestStateCountry()
  {
    return this.mAutoSuggestStateCountry;
  }
  
  public String getAutoSuggestType()
  {
    return this.mAutoSuggestType;
  }
  
  public String getAutoSuggestz()
  {
    return this.mAutoSuggestz;
  }
  
  public void setAutoSuggestCity(String paramString)
  {
    this.mAutoSuggestCity = paramString;
  }
  
  public void setAutoSuggestCountryCode(String paramString)
  {
    this.mAutoSuggestCountryCode = paramString;
  }
  
  public void setAutoSuggestHotelId(String paramString)
  {
    this.mAutoSuggestHotelId = paramString;
  }
  
  public void setAutoSuggestName(String paramString)
  {
    this.mAutoSuggestName = paramString;
  }
  
  public void setAutoSuggestObjectId(String paramString)
  {
    this.mAutoSuggestObjectId = paramString;
  }
  
  public void setAutoSuggestState(String paramString)
  {
    this.mAutoSuggestState = paramString;
  }
  
  public void setAutoSuggestStateCountry(String paramString)
  {
    this.mAutoSuggestStateCountry = paramString;
  }
  
  public void setAutoSuggestType(String paramString)
  {
    this.mAutoSuggestType = paramString;
  }
  
  public void setAutoSuggestz(String paramString)
  {
    this.mAutoSuggestz = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRHotelAutoSuggestListItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */