package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBookingConfirmationData
  implements IJRDataModel
{
  @b(a="bookingid")
  private String mConfirmationBookingId;
  @b(a="email")
  private String mConfirmationEmail;
  @b(a="gobookingid")
  private String mConfirmationGoBookingId;
  @b(a="message")
  private String mConfirmationMessage;
  @b(a="status")
  private String mConfirmationStatus;
  @b(a="vendor")
  private String mConfirmationVendor;
  
  public String getConfirmationBookingId()
  {
    return this.mConfirmationBookingId;
  }
  
  public String getConfirmationEmail()
  {
    return this.mConfirmationEmail;
  }
  
  public String getConfirmationGoBookingId()
  {
    return this.mConfirmationGoBookingId;
  }
  
  public String getConfirmationMessage()
  {
    return this.mConfirmationMessage;
  }
  
  public String getConfirmationStatus()
  {
    return this.mConfirmationStatus;
  }
  
  public String getConfirmationVendor()
  {
    return this.mConfirmationVendor;
  }
  
  public void setConfirmationBookingId(String paramString)
  {
    this.mConfirmationBookingId = paramString;
  }
  
  public void setConfirmationEmail(String paramString)
  {
    this.mConfirmationEmail = paramString;
  }
  
  public void setConfirmationGoBookingId(String paramString)
  {
    this.mConfirmationGoBookingId = paramString;
  }
  
  public void setConfirmationMessage(String paramString)
  {
    this.mConfirmationMessage = paramString;
  }
  
  public void setConfirmationStatus(String paramString)
  {
    this.mConfirmationStatus = paramString;
  }
  
  public void setConfirmationVendor(String paramString)
  {
    this.mConfirmationVendor = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRBookingConfirmationData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */