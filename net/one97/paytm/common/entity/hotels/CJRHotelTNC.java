package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRHotelTNC
  implements IJRDataModel
{
  @b(a="conditions")
  private ArrayList<String> mCondition;
  @b(a="promo")
  private String mPromo;
  
  public ArrayList<String> getCondition()
  {
    return this.mCondition;
  }
  
  public String getPromo()
  {
    return this.mPromo;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRHotelTNC.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */