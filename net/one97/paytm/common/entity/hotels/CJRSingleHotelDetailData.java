package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRSingleHotelDetailData
  implements IJRDataModel
{
  @b(a="description")
  private String mDescription;
  @b(a="offer_url")
  private String mOfferUrl;
  @b(a="reviews")
  private List<CJRHotelReviews> mReviews;
  @b(a="address")
  private CJRHotelAddress mSingleHotelAddress;
  @b(a="facilities")
  private CJRHotelFacilities mSingleHotelFacilities;
  @b(a="_id")
  private String mSingleHotelId;
  @b(a="name")
  private String mSingleHotelName;
  @b(a="paytm_images")
  private CJRHotelImages mSingleHotelPaytmImages;
  @b(a="hotelPolicy")
  private CJRHotelPolicy mSingleHotelPolicy;
  @b(a="ratings")
  private CJRHotelRatings mSingleHotelRatings;
  
  public String getDescription()
  {
    return this.mDescription;
  }
  
  public String getOfferUrl()
  {
    return this.mOfferUrl;
  }
  
  public List<CJRHotelReviews> getReviews()
  {
    return this.mReviews;
  }
  
  public CJRHotelAddress getSingleHotelAddress()
  {
    return this.mSingleHotelAddress;
  }
  
  public CJRHotelFacilities getSingleHotelFacilities()
  {
    return this.mSingleHotelFacilities;
  }
  
  public String getSingleHotelId()
  {
    return this.mSingleHotelId;
  }
  
  public String getSingleHotelName()
  {
    return this.mSingleHotelName;
  }
  
  public CJRHotelImages getSingleHotelPaytmImages()
  {
    return this.mSingleHotelPaytmImages;
  }
  
  public CJRHotelPolicy getSingleHotelPolicy()
  {
    return this.mSingleHotelPolicy;
  }
  
  public CJRHotelRatings getSingleHotelRatings()
  {
    return this.mSingleHotelRatings;
  }
  
  public void setReviews(List<CJRHotelReviews> paramList)
  {
    this.mReviews = paramList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRSingleHotelDetailData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */