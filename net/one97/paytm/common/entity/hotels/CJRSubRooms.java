package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRSubRooms
  implements IJRDataModel
{
  @b(a="cancellationPolicy")
  private String mHotelCancellationPolicy;
  @b(a="priceData")
  private CJRHotelPriceData mHotelPriceData;
  @b(a="includes")
  private ArrayList<String> mSubRoomIncludes;
  @b(a="name")
  private String mSubRoomName;
  @b(a="supplierName")
  private String mSubRoomSupplierName;
  
  public String getCancellationPolicy()
  {
    return this.mHotelCancellationPolicy;
  }
  
  public CJRHotelPriceData getHotelPriceData()
  {
    return this.mHotelPriceData;
  }
  
  public ArrayList<String> getSubRoomIncludes()
  {
    return this.mSubRoomIncludes;
  }
  
  public String getSubRoomName()
  {
    return this.mSubRoomName;
  }
  
  public String getSubRoomSupplierName()
  {
    return this.mSubRoomSupplierName;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRSubRooms.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */