package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRHotelReviewImage
  implements IJRDataModel
{
  @b(a="url")
  private String url;
  
  public String getUrl()
  {
    return this.url;
  }
  
  public void setUrl(String paramString)
  {
    this.url = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRHotelReviewImage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */