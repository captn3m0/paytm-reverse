package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRHotelPriceData
  implements IJRDataModel
{
  @b(a="discount")
  private double mDiscount;
  @b(a="discountPercentage")
  private double mDiscountPercentage;
  @b(a="finalPrice")
  private double mFinalPrice;
  @b(a="finalPriceWithTax")
  private double mFinalPriceWithTax;
  @b(a="finalTax")
  private double mFinalTax;
  @b(a="originalPrice")
  private double mOriginalPrice;
  @b(a="originalPriceWithTax")
  private double mOriginalPriceWithTax;
  @b(a="originalTax")
  private double mOriginalTax;
  @b(a="totalDiscount")
  private double mTotalDiscount;
  @b(a="totalPrice")
  private double mTotalPrice;
  @b(a="totalPriceWithTax")
  private double mTotalPriceWithTax;
  @b(a="totalTax")
  private double mTotalTax;
  
  public double getDiscount()
  {
    return this.mDiscount;
  }
  
  public double getDiscountPercentage()
  {
    return this.mDiscountPercentage;
  }
  
  public double getFinalPrice()
  {
    return this.mFinalPrice;
  }
  
  public double getFinalPriceWithTax()
  {
    return this.mFinalPriceWithTax;
  }
  
  public double getFinalTax()
  {
    return this.mFinalTax;
  }
  
  public double getOriginalPrice()
  {
    return this.mOriginalPrice;
  }
  
  public double getOriginalPriceWithTax()
  {
    return this.mOriginalPriceWithTax;
  }
  
  public double getOriginalTax()
  {
    return this.mOriginalTax;
  }
  
  public double getTotalDiscount()
  {
    return this.mTotalDiscount;
  }
  
  public double getTotalPrice()
  {
    return this.mTotalPrice;
  }
  
  public double getTotalPriceWithTax()
  {
    return this.mTotalPriceWithTax;
  }
  
  public double getTotalTax()
  {
    return this.mTotalTax;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRHotelPriceData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */