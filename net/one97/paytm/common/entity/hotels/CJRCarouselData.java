package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;
import net.one97.paytm.common.entity.shopping.CJRHomePageLayout;

public class CJRCarouselData
  implements IJRDataModel
{
  @b(a="offers")
  private ArrayList<CJRHomePageLayout> mCarouselOffers;
  
  public ArrayList<CJRHomePageLayout> getCarouselOffers()
  {
    return this.mCarouselOffers;
  }
  
  public void setCarouselOffers(ArrayList<CJRHomePageLayout> paramArrayList)
  {
    this.mCarouselOffers = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRCarouselData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */