package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRHotelFacilities
  implements IJRDataModel
{
  @b(a="basic")
  private CJRBasicHotelFacilities mBasicHotelFacilities;
  @b(a="more")
  private ArrayList<String> mMoreHotelFacilities;
  
  public CJRBasicHotelFacilities getBasicHotelFacilities()
  {
    return this.mBasicHotelFacilities;
  }
  
  public ArrayList<String> getMoreHotelFacilities()
  {
    return this.mMoreHotelFacilities;
  }
  
  public void setBasicHotelFacilities(CJRBasicHotelFacilities paramCJRBasicHotelFacilities)
  {
    this.mBasicHotelFacilities = paramCJRBasicHotelFacilities;
  }
  
  public void setMoreHotelFacilities(ArrayList<String> paramArrayList)
  {
    this.mMoreHotelFacilities = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRHotelFacilities.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */