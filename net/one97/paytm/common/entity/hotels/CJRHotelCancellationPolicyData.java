package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRHotelCancellationPolicyData
  implements IJRDataModel
{
  @b(a="cancellationPolicy")
  private String mCancellationPolicy;
  
  public String getCancellationPolicy()
  {
    return this.mCancellationPolicy;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRHotelCancellationPolicyData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */