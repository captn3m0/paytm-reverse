package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRHotelAutoSuggestList
  implements IJRDataModel
{
  @b(a="data")
  private CJRDestinationList mHotelAutoSuggestListData;
  @b(a="message")
  private String mMessage;
  @b(a="status")
  private int mStatus;
  
  public CJRDestinationList getHotelAutoSuggestListData()
  {
    return this.mHotelAutoSuggestListData;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public int getStatus()
  {
    return this.mStatus;
  }
  
  public void setHotelAutoSuggestListData(CJRDestinationList paramCJRDestinationList)
  {
    this.mHotelAutoSuggestListData = paramCJRDestinationList;
  }
  
  public void setMessage(String paramString)
  {
    this.mMessage = paramString;
  }
  
  public void setStatus(int paramInt)
  {
    this.mStatus = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRHotelAutoSuggestList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */