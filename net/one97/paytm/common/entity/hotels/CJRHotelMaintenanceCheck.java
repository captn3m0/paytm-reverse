package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRHotelMaintenanceCheck
  implements IJRDataModel
{
  @b(a="extra")
  private CJRHotelMaintenanceExtra mExtra;
  private int mStatusCode;
  
  public CJRHotelMaintenanceExtra getExtra()
  {
    return this.mExtra;
  }
  
  public int getStatusCode()
  {
    return this.mStatusCode;
  }
  
  public void setStatusCode(int paramInt)
  {
    this.mStatusCode = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRHotelMaintenanceCheck.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */