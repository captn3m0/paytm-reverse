package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRHotelOtas
  implements IJRDataModel
{
  @b(a="hotelId")
  private String mHotelOtaId;
  @b(a="name")
  private String mHotelOtaName;
  
  public String getHotelOtaId()
  {
    return this.mHotelOtaId;
  }
  
  public String getHotelOtaName()
  {
    return this.mHotelOtaName;
  }
  
  public void setHotelOtaId(String paramString)
  {
    this.mHotelOtaId = paramString;
  }
  
  public void setHotelOtaName(String paramString)
  {
    this.mHotelOtaName = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRHotelOtas.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */