package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import com.google.c.l;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;
import net.one97.paytm.common.entity.shopping.CJROffers;

public class CJRHotelRoomOptions
  implements IJRDataModel
{
  @b(a="bedType")
  private String mBedType;
  @b(a="book_params")
  private l mBookParams;
  @b(a="images")
  private CJRHotelImages mHotelRoomImages;
  @b(a="roomsLeft")
  private int mLeftRoomCount;
  @b(a="occupancyMax")
  private String mOccupancyMax;
  @b(a="offer_url")
  private String mOfferUrl;
  @b(a="description")
  private String mRoomDescription;
  @b(a="facilities")
  private ArrayList<String> mRoomFacilities;
  @b(a="maxAllowed")
  private int mRoomMaxAllowed;
  @b(a="size")
  private String mRoomSize;
  @b(a="view")
  private ArrayList<String> mRoomView;
  @b(a="subRooms")
  private ArrayList<CJRSubRooms> mSubRooms;
  private CJROffers offers;
  
  public String getBedType()
  {
    return this.mBedType;
  }
  
  public l getBookParams()
  {
    return this.mBookParams;
  }
  
  public CJRHotelImages getHotelRoomImages()
  {
    return this.mHotelRoomImages;
  }
  
  public int getLeftRoomCount()
  {
    return this.mLeftRoomCount;
  }
  
  public String getOccupancyMax()
  {
    return this.mOccupancyMax;
  }
  
  public String getOfferUrl()
  {
    return this.mOfferUrl;
  }
  
  public CJROffers getOffers()
  {
    return this.offers;
  }
  
  public String getRoomDescription()
  {
    return this.mRoomDescription;
  }
  
  public ArrayList<String> getRoomFacilities()
  {
    return this.mRoomFacilities;
  }
  
  public String getRoomSize()
  {
    return this.mRoomSize;
  }
  
  public ArrayList<CJRSubRooms> getSubRooms()
  {
    return this.mSubRooms;
  }
  
  public ArrayList<String> getmRoomView()
  {
    return this.mRoomView;
  }
  
  public void setOffers(CJROffers paramCJROffers)
  {
    this.offers = paramCJROffers;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRHotelRoomOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */