package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRHotelCustomerReview
  implements IJRDataModel
{
  @b(a="fullComment")
  private String fullComment;
  @b(a="overAllRating")
  private String overAllRating;
  @b(a="photos")
  private List<CJRHotelReviewImage> photos;
  @b(a="reviewDate")
  private String reviewDate;
  @b(a="reviewer")
  private CJRReviewer reviewer;
  @b(a="title")
  private String title;
  
  public String getFullComment()
  {
    return this.fullComment;
  }
  
  public String getOverAllRating()
  {
    return this.overAllRating;
  }
  
  public List<CJRHotelReviewImage> getPhotos()
  {
    return this.photos;
  }
  
  public String getReviewDate()
  {
    return this.reviewDate;
  }
  
  public CJRReviewer getReviewer()
  {
    return this.reviewer;
  }
  
  public String getTitle()
  {
    return this.title;
  }
  
  public void setFullComment(String paramString)
  {
    this.fullComment = paramString;
  }
  
  public void setOverAllRating(String paramString)
  {
    this.overAllRating = paramString;
  }
  
  public void setPhotos(List<CJRHotelReviewImage> paramList)
  {
    this.photos = paramList;
  }
  
  public void setReviewDate(String paramString)
  {
    this.reviewDate = paramString;
  }
  
  public void setReviewer(CJRReviewer paramCJRReviewer)
  {
    this.reviewer = paramCJRReviewer;
  }
  
  public void setTitle(String paramString)
  {
    this.title = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRHotelCustomerReview.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */