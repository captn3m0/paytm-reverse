package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGuestDetails
  implements IJRDataModel
{
  @b(a="country_phone_code")
  private String country_phone_code;
  @b(a="email")
  private String email;
  @b(a="firstname")
  private String firstname;
  @b(a="lastname")
  private String lastname;
  @b(a="mobile")
  private String mobile;
  @b(a="title")
  private String title;
  
  public String getCountryPhoneCode()
  {
    return this.country_phone_code;
  }
  
  public String getEmail()
  {
    return this.email;
  }
  
  public String getFirstName()
  {
    return this.firstname;
  }
  
  public String getLastName()
  {
    return this.lastname;
  }
  
  public String getMobile()
  {
    return this.mobile;
  }
  
  public String getTitle()
  {
    return this.title;
  }
  
  public void setCountryPhoneCode(String paramString)
  {
    this.country_phone_code = paramString;
  }
  
  public void setEmail(String paramString)
  {
    this.email = paramString;
  }
  
  public void setFirstName(String paramString)
  {
    this.firstname = paramString;
  }
  
  public void setLastName(String paramString)
  {
    this.lastname = paramString;
  }
  
  public void setMobile(String paramString)
  {
    this.mobile = paramString;
  }
  
  public void setTitle(String paramString)
  {
    this.title = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRGuestDetails.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */