package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAverageRating
  implements IJRDataModel
{
  @b(a="name")
  private String name;
  @b(a="rate")
  private int rate;
  
  public String getName()
  {
    return this.name;
  }
  
  public int getRate()
  {
    return this.rate;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setRate(int paramInt)
  {
    this.rate = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRAverageRating.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */