package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBasicHotelFacilities
  implements IJRDataModel
{
  @b(a="airConditioning")
  private boolean mHasAirConditioning;
  @b(a="bar")
  private boolean mHasBar;
  @b(a="businessCentre")
  private boolean mHasBusinessCentre;
  @b(a="gym")
  private boolean mHasGym;
  @b(a="parking")
  private boolean mHasParking;
  @b(a="restaurant")
  private boolean mHasRestaurant;
  @b(a="roomService")
  private boolean mHasRoomService;
  @b(a="swimmingPool")
  private boolean mHasSwimmingPool;
  @b(a="twentyFourHrCheckIn")
  private boolean mHasTwentyFourHrCheckIn;
  @b(a="wifi")
  private boolean mHasWifi;
  
  public boolean isAirConditioningPresent()
  {
    return this.mHasAirConditioning;
  }
  
  public boolean isBarPresent()
  {
    return this.mHasBar;
  }
  
  public boolean isBusinessCentrePresent()
  {
    return this.mHasBusinessCentre;
  }
  
  public boolean isGymPresent()
  {
    return this.mHasGym;
  }
  
  public boolean isParkingPresent()
  {
    return this.mHasParking;
  }
  
  public boolean isRestaurantPresent()
  {
    return this.mHasRestaurant;
  }
  
  public boolean isRoomServicePresent()
  {
    return this.mHasRoomService;
  }
  
  public boolean isSwimmingPoolPresent()
  {
    return this.mHasSwimmingPool;
  }
  
  public boolean isTwentyFourHrCheckInPresent()
  {
    return this.mHasTwentyFourHrCheckIn;
  }
  
  public boolean isWifiPresent()
  {
    return this.mHasWifi;
  }
  
  public void setHasAirConditioning(boolean paramBoolean)
  {
    this.mHasAirConditioning = paramBoolean;
  }
  
  public void setHasBar(boolean paramBoolean)
  {
    this.mHasBar = paramBoolean;
  }
  
  public void setHasBusinessCentre(boolean paramBoolean)
  {
    this.mHasBusinessCentre = paramBoolean;
  }
  
  public void setHasGym(boolean paramBoolean)
  {
    this.mHasGym = paramBoolean;
  }
  
  public void setHasParking(boolean paramBoolean)
  {
    this.mHasParking = paramBoolean;
  }
  
  public void setHasRestaurant(boolean paramBoolean)
  {
    this.mHasRestaurant = paramBoolean;
  }
  
  public void setHasRoomService(boolean paramBoolean)
  {
    this.mHasRoomService = paramBoolean;
  }
  
  public void setHasSwimmingPool(boolean paramBoolean)
  {
    this.mHasSwimmingPool = paramBoolean;
  }
  
  public void setHasTwentyFourHrCheckIn(boolean paramBoolean)
  {
    this.mHasTwentyFourHrCheckIn = paramBoolean;
  }
  
  public void setHasWifi(boolean paramBoolean)
  {
    this.mHasWifi = paramBoolean;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRBasicHotelFacilities.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */