package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRProvisionalBookingResponse
  implements IJRDataModel
{
  @b(a="message")
  private String mMessage;
  @b(a="data")
  private CJRProvisionalBookingData mProvisionalBookingData;
  @b(a="status")
  private int mStatus;
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public CJRProvisionalBookingData getProvisionalBookingData()
  {
    return this.mProvisionalBookingData;
  }
  
  public int getStatus()
  {
    return this.mStatus;
  }
  
  public void setMessage(String paramString)
  {
    this.mMessage = paramString;
  }
  
  public void setProvisionalBookingData(CJRProvisionalBookingData paramCJRProvisionalBookingData)
  {
    this.mProvisionalBookingData = paramCJRProvisionalBookingData;
  }
  
  public void setStatus(int paramInt)
  {
    this.mStatus = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRProvisionalBookingResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */