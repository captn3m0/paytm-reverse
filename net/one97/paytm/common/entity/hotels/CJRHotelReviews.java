package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRHotelReviews
  implements IJRDataModel
{
  @b(a="averageRatings")
  private List<CJRAverageRating> averageRatings;
  @b(a="icon")
  private String icon;
  @b(a="name")
  private String name;
  @b(a="rating")
  private float rating;
  @b(a="ratings_icon")
  private String ratingsIcon;
  @b(a="reviews_text")
  private String reviewText;
  @b(a="customer_reviews")
  private List<CJRHotelCustomerReview> reviews;
  @b(a="reviewsCount")
  private int reviewsCount;
  @b(a="scale")
  private int scale;
  
  public String getIcon()
  {
    return this.icon;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public List<CJRAverageRating> getOverAllRatings()
  {
    return this.averageRatings;
  }
  
  public float getRating()
  {
    return this.rating;
  }
  
  public String getRatingsIcon()
  {
    return this.ratingsIcon;
  }
  
  public String getReviewText()
  {
    return this.reviewText;
  }
  
  public List<CJRHotelCustomerReview> getReviews()
  {
    return this.reviews;
  }
  
  public int getReviewsCount()
  {
    return this.reviewsCount;
  }
  
  public int getScale()
  {
    return this.scale;
  }
  
  public void setIcon(String paramString)
  {
    this.icon = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setOverAllRatings(List<CJRAverageRating> paramList)
  {
    this.averageRatings = paramList;
  }
  
  public void setRating(float paramFloat)
  {
    this.rating = paramFloat;
  }
  
  public void setRatingsIcon(String paramString)
  {
    this.ratingsIcon = paramString;
  }
  
  public void setReviewText(String paramString)
  {
    this.reviewText = paramString;
  }
  
  public void setReviews(List<CJRHotelCustomerReview> paramList)
  {
    this.reviews = paramList;
  }
  
  public void setReviewsCount(int paramInt)
  {
    this.reviewsCount = paramInt;
  }
  
  public void setScale(int paramInt)
  {
    this.scale = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRHotelReviews.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */