package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRHotelDestinationList
  implements IJRDataModel
{
  @b(a="i")
  private String mHotelId;
  @b(a="c")
  private String mHotelLocation;
  @b(a="n")
  private String mHotelName;
  
  public String getHotelId()
  {
    return this.mHotelId;
  }
  
  public String getHotelLocation()
  {
    return this.mHotelLocation;
  }
  
  public String getHotelName()
  {
    return this.mHotelName;
  }
  
  public void setHotelId(String paramString)
  {
    this.mHotelId = paramString;
  }
  
  public void setHotelLocation(String paramString)
  {
    this.mHotelLocation = paramString;
  }
  
  public void setHotelName(String paramString)
  {
    this.mHotelName = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRHotelDestinationList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */