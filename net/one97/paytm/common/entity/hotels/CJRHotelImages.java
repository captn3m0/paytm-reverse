package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRHotelImages
  implements IJRDataModel
{
  @b(a="full")
  private ArrayList<String> full;
  @b(a="thumb")
  private ArrayList<String> thumb;
  
  public ArrayList<String> getFullImages()
  {
    return this.full;
  }
  
  public ArrayList<String> getThumbImages()
  {
    return this.thumb;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRHotelImages.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */