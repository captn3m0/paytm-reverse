package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import com.google.c.l;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAvailableHotelListItem
  implements IJRDataModel
{
  @b(a="extras")
  private l mExtras;
  @b(a="address")
  private CJRHotelAddress mHotelAddress;
  @b(a="_id")
  private String mHotelId;
  @b(a="name")
  private String mHotelName;
  @b(a="priceData")
  private CJRHotelPriceData mHotelPriceData;
  @b(a="ratings")
  private CJRHotelRatings mHotelRatings;
  private String mListItemType = "";
  @b(a="paytm_images")
  private CJRHotelImages mPaytmImages;
  private CJRSingleHotelExtras mSingleExtras;
  @b(a="reviews")
  private List<CJRHotelReviews> reviews;
  
  public l getExtras()
  {
    return this.mExtras;
  }
  
  public CJRHotelAddress getHotelAddress()
  {
    return this.mHotelAddress;
  }
  
  public String getHotelId()
  {
    return this.mHotelId;
  }
  
  public String getHotelName()
  {
    return this.mHotelName;
  }
  
  public CJRHotelPriceData getHotelPriceData()
  {
    return this.mHotelPriceData;
  }
  
  public CJRHotelRatings getHotelRatings()
  {
    return this.mHotelRatings;
  }
  
  public String getListItemType()
  {
    return this.mListItemType;
  }
  
  public CJRHotelImages getPaytmImages()
  {
    return this.mPaytmImages;
  }
  
  public List<CJRHotelReviews> getReviews()
  {
    return this.reviews;
  }
  
  public CJRSingleHotelExtras getSingleExtras()
  {
    return this.mSingleExtras;
  }
  
  public void setListItemType(String paramString)
  {
    this.mListItemType = paramString;
  }
  
  public void setReviews(List<CJRHotelReviews> paramList)
  {
    this.reviews = paramList;
  }
  
  public void setSingleExtras(CJRSingleHotelExtras paramCJRSingleHotelExtras)
  {
    this.mSingleExtras = paramCJRSingleHotelExtras;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRAvailableHotelListItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */