package net.one97.paytm.common.entity.hotels;

import com.google.b.a.a;
import com.google.b.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRSingleHotelExtras
  implements IJRDataModel
{
  @a
  @b(a="city_id")
  private String cityId;
  @a
  @b(a="country_name")
  private String countryName;
  @a
  @b(a="freeCancellable")
  private boolean freeCancellable;
  @a
  @b(a="freeCancellableTill")
  private String freeCancellableTill;
  @a
  @b(a="hotelid")
  private String hotelid;
  @a
  @b(a="index")
  private Integer index;
  @a
  @b(a="otas")
  private String otas;
  @a
  @b(a="pid")
  private Integer pid;
  @a
  @b(a="searchType")
  private String searchType;
  @a
  @b(a="sortBy")
  private String sortBy;
  
  public String getCityId()
  {
    return this.cityId;
  }
  
  public String getCountryName()
  {
    return this.countryName;
  }
  
  public boolean getFreeCancellable()
  {
    return this.freeCancellable;
  }
  
  public String getFreeCancellableTill()
  {
    return this.freeCancellableTill;
  }
  
  public String getHotelid()
  {
    return this.hotelid;
  }
  
  public Integer getIndex()
  {
    return this.index;
  }
  
  public String getOtas()
  {
    return this.otas;
  }
  
  public Integer getPid()
  {
    return this.pid;
  }
  
  public String getSearchType()
  {
    return this.searchType;
  }
  
  public String getSortBy()
  {
    return this.sortBy;
  }
  
  public void setCityId(String paramString)
  {
    this.cityId = paramString;
  }
  
  public void setCountryName(String paramString)
  {
    this.countryName = paramString;
  }
  
  public void setFreeCancellable(boolean paramBoolean)
  {
    this.freeCancellable = paramBoolean;
  }
  
  public void setFreeCancellableTill(String paramString)
  {
    this.freeCancellableTill = paramString;
  }
  
  public void setHotelid(String paramString)
  {
    this.hotelid = paramString;
  }
  
  public void setIndex(Integer paramInteger)
  {
    this.index = paramInteger;
  }
  
  public void setOtas(String paramString)
  {
    this.otas = paramString;
  }
  
  public void setPid(Integer paramInteger)
  {
    this.pid = paramInteger;
  }
  
  public void setSearchType(String paramString)
  {
    this.searchType = paramString;
  }
  
  public void setSortBy(String paramString)
  {
    this.sortBy = paramString;
  }
  
  public String toString()
  {
    return "extras [city_id = " + this.cityId + ", otas = " + this.otas + ", index = " + this.index + ", freeCancellable = " + this.freeCancellable + ", sortBy = " + this.sortBy + ", hotelid = " + this.hotelid + ", pid = " + this.pid + ", searchType = " + this.searchType + ", country_name = " + this.countryName + ", freeCancellableTill = " + this.freeCancellableTill + "]";
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRSingleHotelExtras.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */