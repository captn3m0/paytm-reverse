package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRSingleHotelReviews
  implements IJRDataModel
{
  @b(a="message")
  private String mMessage;
  @b(a="data")
  private List<CJRHotelReviews> mSingleHotelReviews;
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public List<CJRHotelReviews> getReviewList()
  {
    return this.mSingleHotelReviews;
  }
  
  public void setMessage(String paramString)
  {
    this.mMessage = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRSingleHotelReviews.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */