package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRCurrentLocationApiResponse
  implements IJRDataModel
{
  @b(a="data")
  private CJRCurrentLocationData mLocationData;
  @b(a="message")
  private String message;
  
  public CJRCurrentLocationData getLocationData()
  {
    return this.mLocationData;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public void setLocationData(CJRCurrentLocationData paramCJRCurrentLocationData)
  {
    this.mLocationData = paramCJRCurrentLocationData;
  }
  
  public void setMessage(String paramString)
  {
    this.message = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRCurrentLocationApiResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */