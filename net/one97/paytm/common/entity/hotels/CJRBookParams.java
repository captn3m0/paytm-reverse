package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import java.util.HashMap;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBookParams
  implements IJRDataModel
{
  @b(a="fwdp")
  private HashMap<String, String> fwdp;
  @b(a="hc")
  private String hc;
  @b(a="hotel_id")
  private String hotel_id;
  @b(a="hotel_index")
  private String hotel_index;
  @b(a="hotel_name")
  private String hotel_name;
  @b(a="ibp")
  private String ibp;
  @b(a="isProvisionalRequired")
  private String isProvisionalRequired;
  @b(a="property_id")
  private String property_id;
  @b(a="property_name")
  private String property_name;
  @b(a="room_id")
  private String room_id;
  @b(a="room_type")
  private String room_type;
  @b(a="rpc")
  private String rpc;
  @b(a="rtc")
  private String rtc;
  @b(a="search_id")
  private String search_id;
  
  public HashMap<String, String> getFwdp()
  {
    return this.fwdp;
  }
  
  public String getHc()
  {
    return this.hc;
  }
  
  public String getHotel_id()
  {
    return this.hotel_id;
  }
  
  public String getHotel_index()
  {
    return this.hotel_index;
  }
  
  public String getHotel_name()
  {
    return this.hotel_name;
  }
  
  public String getIbp()
  {
    return this.ibp;
  }
  
  public String getIsProvisionalRequired()
  {
    return this.isProvisionalRequired;
  }
  
  public String getProperty_id()
  {
    return this.property_id;
  }
  
  public String getProperty_name()
  {
    return this.property_name;
  }
  
  public String getRoom_id()
  {
    return this.room_id;
  }
  
  public String getRoom_type()
  {
    return this.room_type;
  }
  
  public String getRpc()
  {
    return this.rpc;
  }
  
  public String getRtc()
  {
    return this.rtc;
  }
  
  public String getSearch_id()
  {
    return this.search_id;
  }
  
  public void setFwdp(HashMap<String, String> paramHashMap)
  {
    this.fwdp = paramHashMap;
  }
  
  public void setHc(String paramString)
  {
    this.hc = paramString;
  }
  
  public void setHotel_id(String paramString)
  {
    this.hotel_id = paramString;
  }
  
  public void setHotel_index(String paramString)
  {
    this.hotel_index = paramString;
  }
  
  public void setHotel_name(String paramString)
  {
    this.hotel_name = paramString;
  }
  
  public void setIbp(String paramString)
  {
    this.ibp = paramString;
  }
  
  public void setIsProvisionalRequired(String paramString)
  {
    this.isProvisionalRequired = paramString;
  }
  
  public void setProperty_id(String paramString)
  {
    this.property_id = paramString;
  }
  
  public void setProperty_name(String paramString)
  {
    this.property_name = paramString;
  }
  
  public void setRoom_id(String paramString)
  {
    this.room_id = paramString;
  }
  
  public void setRoom_type(String paramString)
  {
    this.room_type = paramString;
  }
  
  public void setRpc(String paramString)
  {
    this.rpc = paramString;
  }
  
  public void setRtc(String paramString)
  {
    this.rtc = paramString;
  }
  
  public void setSearch_id(String paramString)
  {
    this.search_id = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRBookParams.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */