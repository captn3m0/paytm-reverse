package net.one97.paytm.common.entity.hotels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRHotelAddress
  implements IJRDataModel
{
  @b(a="city")
  private String mHotelCity;
  @b(a="country")
  private String mHotelCountry;
  @b(a="lat")
  private double mHotelLatitude;
  @b(a="locality")
  private String mHotelLocality;
  @b(a="lng")
  private double mHotelLongitude;
  @b(a="streetAddress")
  private String mHotelStreetAddress;
  @b(a="zip")
  private String mHotelZip;
  
  public String getHotelCity()
  {
    return this.mHotelCity;
  }
  
  public String getHotelCountry()
  {
    return this.mHotelCountry;
  }
  
  public double getHotelLatitude()
  {
    return this.mHotelLatitude;
  }
  
  public String getHotelLocality()
  {
    return this.mHotelLocality;
  }
  
  public double getHotelLongitude()
  {
    return this.mHotelLongitude;
  }
  
  public String getHotelStreetAddress()
  {
    return this.mHotelStreetAddress;
  }
  
  public String getHotelZip()
  {
    return this.mHotelZip;
  }
  
  public void setHotelCity(String paramString)
  {
    this.mHotelCity = paramString;
  }
  
  public void setHotelCountry(String paramString)
  {
    this.mHotelCountry = paramString;
  }
  
  public void setHotelLatitude(double paramDouble)
  {
    this.mHotelLatitude = paramDouble;
  }
  
  public void setHotelLocality(String paramString)
  {
    this.mHotelLocality = paramString;
  }
  
  public void setHotelLongitude(double paramDouble)
  {
    this.mHotelLongitude = paramDouble;
  }
  
  public void setHotelStreetAddress(String paramString)
  {
    this.mHotelStreetAddress = paramString;
  }
  
  public void setHotelZip(String paramString)
  {
    this.mHotelZip = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/hotels/CJRHotelAddress.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */