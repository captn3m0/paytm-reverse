package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.ArrayList;

public class CJRButton
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="isModalBtnEnabled")
  private boolean isModalBtnEnabled;
  @b(a="action")
  private String mAction;
  @b(a="actionPage")
  private String mActionPage;
  @b(a="actionUrl")
  private String mActionUrl;
  @b(a="actionUrlRequest")
  private CJRActionUrlRequest mActionUrlRequest;
  @b(a="buttonAlignment")
  private String mButtonAlignment;
  @b(a="buttonName")
  private String mButtonName;
  private int mButtonTag;
  @b(a="modalbuttonHeader")
  private String mModalButtonHeader;
  @b(a="checkUserTxnHistoryWrapperButtonsModals")
  private ArrayList<CJRModalButton> mModalButtonList;
  @b(a="modalbuttonText")
  private String mModalButtonText;
  @b(a="respAction")
  private String mRespAction;
  
  public String getAction()
  {
    return this.mAction;
  }
  
  public String getActionPage()
  {
    return this.mActionPage;
  }
  
  public String getActionUrl()
  {
    return this.mActionUrl;
  }
  
  public CJRActionUrlRequest getActionUrlRequest()
  {
    return this.mActionUrlRequest;
  }
  
  public String getButtonAlignment()
  {
    return this.mButtonAlignment;
  }
  
  public String getButtonName()
  {
    return this.mButtonName;
  }
  
  public int getButtonTag()
  {
    return this.mButtonTag;
  }
  
  public String getModalButtonHeader()
  {
    return this.mModalButtonHeader;
  }
  
  public ArrayList<CJRModalButton> getModalButtonList()
  {
    return this.mModalButtonList;
  }
  
  public String getModalButtonText()
  {
    return this.mModalButtonText;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getRespAction()
  {
    return this.mRespAction;
  }
  
  public boolean isModalBtnEnabled()
  {
    return this.isModalBtnEnabled;
  }
  
  public void setButtonName(String paramString)
  {
    this.mButtonName = paramString;
  }
  
  public void setButtonTag(int paramInt)
  {
    this.mButtonTag = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRButton.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */