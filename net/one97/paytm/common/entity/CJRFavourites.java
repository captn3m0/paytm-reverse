package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.recharge.CJRFrequentOrder;

public class CJRFavourites
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  public final String TAG_PAY_TYPE_PREPAID = "prepaid";
  @b(a="code")
  private int mCode;
  @b(a="data")
  private ArrayList<CJRFrequentOrder> mFavoriteNumberList;
  @b(a="message")
  private String mMessage;
  
  public int getCode()
  {
    return this.mCode;
  }
  
  public ArrayList<CJRFrequentOrder> getFavoriteNumberList()
  {
    return this.mFavoriteNumberList;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRFavourites.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */