package net.one97.paytm.common.entity.tabbeddesign;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTabMenu
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="items")
  private ArrayList<CJRTabMenuItem> mTabMenuItems;
  
  public ArrayList<CJRTabMenuItem> getTabMenuItems()
  {
    return this.mTabMenuItems;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/tabbeddesign/CJRTabMenu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */