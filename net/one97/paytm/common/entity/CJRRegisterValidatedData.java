package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRRegisterValidatedData
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="code")
  private String mCode;
  @b(a="message")
  private String mMessage;
  @b(a="responseCode")
  private String mResponseCode;
  @b(a="state")
  private String mState;
  @b(a="status")
  private String mStatus;
  
  public String getCode()
  {
    return this.mCode;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getResponseCode()
  {
    return this.mResponseCode;
  }
  
  public String getState()
  {
    return this.mState;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRRegisterValidatedData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */