package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRDocPanupdate
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="data")
  private String mData;
  @b(a="message")
  private String mMessage;
  @b(a="responseCode")
  private String mResponseCode;
  @b(a="status")
  private String mStatus;
  @b(a="success")
  private boolean mSuccess;
  
  public String getData()
  {
    return this.mData;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getResponseCode()
  {
    return this.mResponseCode;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public boolean getSuccess()
  {
    return this.mSuccess;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRDocPanupdate.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */