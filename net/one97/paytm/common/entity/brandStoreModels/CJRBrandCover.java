package net.one97.paytm.common.entity.brandStoreModels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrandCover
  implements IJRDataModel
{
  @b(a="name")
  public String mname;
  @b(a="url")
  public String murl;
  
  public String getMname()
  {
    return this.mname;
  }
  
  public String getMurl()
  {
    return this.murl;
  }
  
  public void setMname(String paramString)
  {
    this.mname = paramString;
  }
  
  public void setMurl(String paramString)
  {
    this.murl = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/brandStoreModels/CJRBrandCover.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */