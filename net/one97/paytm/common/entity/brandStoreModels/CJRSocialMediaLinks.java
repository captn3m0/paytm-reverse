package net.one97.paytm.common.entity.brandStoreModels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRSocialMediaLinks
  implements IJRDataModel
{
  @b(a="facebook")
  CJRSocialAtrributes mfacebook;
  @b(a="instagram")
  CJRSocialAtrributes minstagram;
  @b(a="pinInterest")
  CJRSocialAtrributes mpintrest;
  @b(a="twitter")
  CJRSocialAtrributes mtwitter;
  @b(a="youtube")
  CJRSocialAtrributes myoutube;
  
  public CJRSocialAtrributes getMfacebook()
  {
    return this.mfacebook;
  }
  
  public CJRSocialAtrributes getMinstagram()
  {
    return this.minstagram;
  }
  
  public CJRSocialAtrributes getMpintrest()
  {
    return this.mpintrest;
  }
  
  public CJRSocialAtrributes getMtwitter()
  {
    return this.mtwitter;
  }
  
  public CJRSocialAtrributes getMyoutube()
  {
    return this.myoutube;
  }
  
  public void setMfacebook(CJRSocialAtrributes paramCJRSocialAtrributes)
  {
    this.mfacebook = paramCJRSocialAtrributes;
  }
  
  public void setMinstagram(CJRSocialAtrributes paramCJRSocialAtrributes)
  {
    this.minstagram = paramCJRSocialAtrributes;
  }
  
  public void setMpintrest(CJRSocialAtrributes paramCJRSocialAtrributes)
  {
    this.mpintrest = paramCJRSocialAtrributes;
  }
  
  public void setMtwitter(CJRSocialAtrributes paramCJRSocialAtrributes)
  {
    this.mtwitter = paramCJRSocialAtrributes;
  }
  
  public void setMyoutube(CJRSocialAtrributes paramCJRSocialAtrributes)
  {
    this.myoutube = paramCJRSocialAtrributes;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/brandStoreModels/CJRSocialMediaLinks.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */