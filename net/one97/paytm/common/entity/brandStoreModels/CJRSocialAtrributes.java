package net.one97.paytm.common.entity.brandStoreModels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRSocialAtrributes
  implements IJRDataModel
{
  @b(a="isEnabled")
  private String misEnabled;
  @b(a="url")
  private String murl;
  @b(a="url_type")
  private String murlType;
  
  public String getMisEnabled()
  {
    return this.misEnabled;
  }
  
  public String getMurl()
  {
    return this.murl;
  }
  
  public String getMurlType()
  {
    return this.murlType;
  }
  
  public void setMisEnabled(String paramString)
  {
    this.misEnabled = paramString;
  }
  
  public void setMurl(String paramString)
  {
    this.murl = paramString;
  }
  
  public void setMurlType(String paramString)
  {
    this.murlType = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/brandStoreModels/CJRSocialAtrributes.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */