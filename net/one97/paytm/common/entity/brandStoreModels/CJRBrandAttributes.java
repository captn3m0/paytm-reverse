package net.one97.paytm.common.entity.brandStoreModels;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrandAttributes
  implements IJRDataModel
{
  @b(a="social_media_links")
  private CJRSocialMediaLinks cjrSocialMediaLinks;
  @b(a="email_id")
  private String emailID;
  @b(a="brand_logo")
  private CJRBrandLogo mBrandLogo;
  @b(a="cover_image")
  private CJRBrandCover mCoverImage;
  @b(a="img_url")
  private String mImgUrl;
  @b(a="mobile_no1")
  private String mMobile1;
  @b(a="mobile_no2")
  private String mMobile2;
  @b(a="name")
  private String mName;
  @b(a="short_description")
  private String mTabTitleName;
  @b(a="url_type")
  private String mUrlType;
  @b(a="video_url")
  private String mVideoUrl;
  @b(a="address")
  private String maddress;
  @b(a="display_name")
  private String mdisplayName;
  @b(a="text_description")
  private String mtextDescp;
  @b(a="title")
  private String mtitle;
  @b(a="visitStoreUrl")
  private String mvisitStoreUrl;
  
  public CJRSocialMediaLinks getCjrSocialMediaLinks()
  {
    return this.cjrSocialMediaLinks;
  }
  
  public String getEmailID()
  {
    return this.emailID;
  }
  
  public String getMaddress()
  {
    return this.maddress;
  }
  
  public String getMdisplayName()
  {
    return this.mdisplayName;
  }
  
  public String getMtextDescp()
  {
    return this.mtextDescp;
  }
  
  public String getMtitle()
  {
    return this.mtitle;
  }
  
  public String getMvisitStoreUrl()
  {
    return this.mvisitStoreUrl;
  }
  
  public CJRBrandLogo getmBrandLogo()
  {
    return this.mBrandLogo;
  }
  
  public CJRBrandCover getmCoverImage()
  {
    return this.mCoverImage;
  }
  
  public String getmImgUrl()
  {
    return this.mImgUrl;
  }
  
  public String getmMobile1()
  {
    return this.mMobile1;
  }
  
  public String getmMobile2()
  {
    return this.mMobile2;
  }
  
  public String getmName()
  {
    return this.mName;
  }
  
  public String getmTabTitleName()
  {
    return this.mTabTitleName;
  }
  
  public String getmUrlType()
  {
    return this.mUrlType;
  }
  
  public String getmVideoUrl()
  {
    return this.mVideoUrl;
  }
  
  public void setCjrSocialMediaLinks(CJRSocialMediaLinks paramCJRSocialMediaLinks)
  {
    this.cjrSocialMediaLinks = paramCJRSocialMediaLinks;
  }
  
  public void setEmailID(String paramString)
  {
    this.emailID = paramString;
  }
  
  public void setMaddress(String paramString)
  {
    this.maddress = paramString;
  }
  
  public void setMdisplayName(String paramString)
  {
    this.mdisplayName = paramString;
  }
  
  public void setMtextDescp(String paramString)
  {
    this.mtextDescp = paramString;
  }
  
  public void setMtitle(String paramString)
  {
    this.mtitle = paramString;
  }
  
  public void setMvisitStoreUrl(String paramString)
  {
    this.mvisitStoreUrl = paramString;
  }
  
  public void setmBrandLogo(CJRBrandLogo paramCJRBrandLogo)
  {
    this.mBrandLogo = paramCJRBrandLogo;
  }
  
  public void setmCoverImage(CJRBrandCover paramCJRBrandCover)
  {
    this.mCoverImage = paramCJRBrandCover;
  }
  
  public void setmImgUrl(String paramString)
  {
    this.mImgUrl = paramString;
  }
  
  public void setmMobile1(String paramString)
  {
    this.mMobile1 = paramString;
  }
  
  public void setmMobile2(String paramString)
  {
    this.mMobile2 = paramString;
  }
  
  public void setmName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setmTabTitleName(String paramString)
  {
    this.mTabTitleName = paramString;
  }
  
  public void setmUrlType(String paramString)
  {
    this.mUrlType = paramString;
  }
  
  public void setmVideoUrl(String paramString)
  {
    this.mVideoUrl = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/brandStoreModels/CJRBrandAttributes.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */