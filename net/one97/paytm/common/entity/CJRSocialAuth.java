package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRSocialAuth
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="authState")
  private String mAuthState;
  @b(a="code")
  private String mCode;
  @b(a="socialId")
  private String mSocialId;
  @b(a="state")
  private String mState;
  @b(a="statusCode")
  private String mStatusCode;
  @b(a="statusMessage")
  private String mStatusMessage;
  @b(a="username")
  private String mUserName;
  
  public String getAuthState()
  {
    return this.mAuthState;
  }
  
  public String getCode()
  {
    return this.mCode;
  }
  
  public String getSocialId()
  {
    return this.mSocialId;
  }
  
  public String getState()
  {
    return this.mState;
  }
  
  public String getStatusCode()
  {
    return this.mStatusCode;
  }
  
  public String getStatusMessage()
  {
    return this.mStatusMessage;
  }
  
  public String getUserName()
  {
    return this.mUserName;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRSocialAuth.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */