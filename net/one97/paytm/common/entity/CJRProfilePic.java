package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRProfilePic
  implements IJRDataModel
{
  @b(a="message")
  private String mMessage;
  @b(a="responseCode")
  private String mResponseCode;
  @b(a="status")
  private String mStatus;
  @b(a="userId")
  private String mUserID;
  @b(a="userPicture")
  private String mUserPicture;
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getResponseCode()
  {
    return this.mResponseCode;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public String getUserID()
  {
    return this.mUserID;
  }
  
  public String getUserPicture()
  {
    return this.mUserPicture;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRProfilePic.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */