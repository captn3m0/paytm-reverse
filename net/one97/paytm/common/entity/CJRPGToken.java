package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRPGToken
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="expires")
  private long mExpires;
  @b(a="resourceOwnerId")
  private String mResourceOwnerId;
  @b(a="scope")
  private String mScopes;
  @b(a="access_token")
  private String mToken;
  
  public long getExpires()
  {
    return this.mExpires;
  }
  
  public String getResourceOwnerId()
  {
    return this.mResourceOwnerId;
  }
  
  public String getScopes()
  {
    return this.mScopes;
  }
  
  public String getToken()
  {
    return this.mToken;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRPGToken.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */