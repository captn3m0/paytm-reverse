package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRRechargeConfig
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="circle")
  private String mCircle;
  @b(a="operator")
  private String mOperator;
  @b(a="price")
  private String mPrice;
  @b(a="recharge_number")
  private String mRechargeNumber;
  
  public String getCircle()
  {
    return this.mCircle;
  }
  
  public String getOperator()
  {
    return this.mOperator;
  }
  
  public String getPrice()
  {
    return this.mPrice;
  }
  
  public String getProductId()
  {
    return this.mRechargeNumber;
  }
  
  public String getRechargeNumber()
  {
    return this.mRechargeNumber;
  }
  
  public void setCircle(String paramString)
  {
    this.mCircle = paramString;
  }
  
  public void setOperator(String paramString)
  {
    this.mOperator = paramString;
  }
  
  public void setPrice(String paramString)
  {
    this.mPrice = paramString;
  }
  
  public void setRechargeNumber(String paramString)
  {
    this.mRechargeNumber = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRRechargeConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */