package net.one97.paytm.common.entity;

import com.google.c.a.b;
import net.one97.paytm.common.entity.shopping.CJRCart;

public class CJRRechargeCartItem
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="brand")
  private CJRCart mBrand;
  @b(a="brand_id")
  private CJRCart mBrandId;
  @b(a="discounted_price")
  private CJRCart mDiscountedPrice;
  @b(a="merchant_id")
  private CJRCart mMerchantId;
  @b(a="merchant_name")
  private CJRCart mMerchantName;
  @b(a="name")
  private CJRCart mName;
  @b(a="product_id")
  private String mProductId;
  @b(a="qty")
  private int mQuantity;
  @b(a="readonly")
  private CJRCart mReadOnly;
  @b(a="configuration")
  private CJRRechargeConfig mRechargeConfig;
  @b(a="selling_price")
  private CJRCart mSellingPrice;
  @b(a="shipping_charges")
  private CJRCart mShippingCharges;
  
  public CJRCart getBrand()
  {
    return this.mBrand;
  }
  
  public CJRCart getBrandId()
  {
    return this.mBrandId;
  }
  
  public CJRCart getDiscountedPrice()
  {
    return this.mDiscountedPrice;
  }
  
  public CJRCart getMerchantId()
  {
    return this.mMerchantId;
  }
  
  public CJRCart getMerchantName()
  {
    return this.mMerchantName;
  }
  
  public CJRCart getName()
  {
    return this.mName;
  }
  
  public String getProductId()
  {
    return this.mProductId;
  }
  
  public int getQuantity()
  {
    return this.mQuantity;
  }
  
  public CJRCart getReadOnly()
  {
    return this.mReadOnly;
  }
  
  public CJRRechargeConfig getRechargeConfig()
  {
    return this.mRechargeConfig;
  }
  
  public CJRCart getSellingPrice()
  {
    return this.mSellingPrice;
  }
  
  public CJRCart getShippingCharges()
  {
    return this.mShippingCharges;
  }
  
  public void setProductId(String paramString)
  {
    this.mProductId = paramString;
  }
  
  public void setQuantity(int paramInt)
  {
    this.mQuantity = paramInt;
  }
  
  public void setRechargeConfig(CJRRechargeConfig paramCJRRechargeConfig)
  {
    this.mRechargeConfig = paramCJRRechargeConfig;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRRechargeCartItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */