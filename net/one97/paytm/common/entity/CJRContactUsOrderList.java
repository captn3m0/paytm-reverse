package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.ArrayList;

public class CJRContactUsOrderList
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="amount")
  private String mAmount;
  @b(a="date")
  private String mDate;
  @b(a="item_count")
  private int mItemCount;
  @b(a="order_id")
  private String mOrderID;
  @b(a="items")
  private ArrayList<CJROrderItems> mOrderItems;
  @b(a="order_name")
  private String mOrderName;
  @b(a="status")
  private String mStatus;
  @b(a="status_icon")
  private String mStatusIcon;
  
  public String getAmount()
  {
    return this.mAmount;
  }
  
  public String getDate()
  {
    return this.mDate;
  }
  
  public int getItemCount()
  {
    return this.mItemCount;
  }
  
  public String getOrderID()
  {
    return this.mOrderID;
  }
  
  public ArrayList<CJROrderItems> getOrderItems()
  {
    return this.mOrderItems;
  }
  
  public String getOrderName()
  {
    return this.mOrderName;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public String getStatusIcon()
  {
    return this.mStatusIcon;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRContactUsOrderList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */