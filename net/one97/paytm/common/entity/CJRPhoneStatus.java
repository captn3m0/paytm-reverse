package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRPhoneStatus
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="isAvailable")
  private boolean isAvailable;
  @b(a="error")
  private String mErrorMessge;
  @b(a="phoneNo")
  String mPhoneNo;
  
  public String getErrorMessge()
  {
    return this.mErrorMessge;
  }
  
  public String getPhoneNo()
  {
    return this.mPhoneNo;
  }
  
  public boolean isAvailable()
  {
    return this.isAvailable;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRPhoneStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */