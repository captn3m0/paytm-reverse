package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRTransferToBankResponse
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="heading")
  private String mHeading;
  @b(a="text")
  private String mText;
  
  public String getHeading()
  {
    return this.mHeading;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getText()
  {
    return this.mText;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRTransferToBankResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */