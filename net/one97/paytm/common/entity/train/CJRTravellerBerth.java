package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTravellerBerth
  implements IJRDataModel
{
  @b(a="BerthCode")
  private String mBerthCode;
  @b(a="BerthName")
  private String mBerthName;
  
  public String getBerthCode()
  {
    return this.mBerthCode;
  }
  
  public String getBerthName()
  {
    return this.mBerthName;
  }
  
  public void setBerthCode(String paramString)
  {
    this.mBerthCode = paramString;
  }
  
  public void setBerthName(String paramString)
  {
    this.mBerthName = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRTravellerBerth.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */