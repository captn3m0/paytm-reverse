package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import net.one97.paytm.common.entity.IJRDataModel;
import net.one97.paytm.common.entity.trainticket.CJRBookings;

public class CJRTrainRecentBooking
  implements IJRDataModel
{
  @b(a="body")
  private RecentBookingBody mRecentBookingBody;
  
  public RecentBookingBody getRecentBookingBody()
  {
    return this.mRecentBookingBody;
  }
  
  public void setRecentBookingBody(RecentBookingBody paramRecentBookingBody)
  {
    this.mRecentBookingBody = paramRecentBookingBody;
  }
  
  public class RecentBookingBody
    implements IJRDataModel
  {
    @b(a="bookings")
    private ArrayList<CJRBookings> mBookingsList;
    @b(a="pax_info")
    private HashMap<String, CJRPassengerInformation> mPassengerInformation;
    
    public RecentBookingBody() {}
    
    public HashMap<String, CJRPassengerInformation> getPassengerInformation()
    {
      return this.mPassengerInformation;
    }
    
    public ArrayList<String> getPassengerNameList()
    {
      ArrayList localArrayList = new ArrayList();
      if ((this.mPassengerInformation != null) && (this.mPassengerInformation.size() > 0))
      {
        Iterator localIterator = this.mPassengerInformation.entrySet().iterator();
        while (localIterator.hasNext()) {
          localArrayList.add(((Map.Entry)localIterator.next()).getKey());
        }
      }
      return localArrayList;
    }
    
    public ArrayList<CJRBookings> getmBookingsList()
    {
      return this.mBookingsList;
    }
    
    public void setPassengerInformation(HashMap<String, CJRPassengerInformation> paramHashMap)
    {
      this.mPassengerInformation = paramHashMap;
    }
    
    public void setmBookingsList(ArrayList<CJRBookings> paramArrayList)
    {
      this.mBookingsList = paramArrayList;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRTrainRecentBooking.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */