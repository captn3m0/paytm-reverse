package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainRouteBody
  implements IJRDataModel
{
  @b(a="stationList")
  private List<CJRStationList> mStationList;
  @b(a="trainName")
  private String mTrainName;
  @b(a="trainNumber")
  private String mTrainNumber;
  
  public List<CJRStationList> getmStationList()
  {
    return this.mStationList;
  }
  
  public String getmTrainName()
  {
    return this.mTrainName;
  }
  
  public String getmTrainNumber()
  {
    return this.mTrainNumber;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRTrainRouteBody.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */