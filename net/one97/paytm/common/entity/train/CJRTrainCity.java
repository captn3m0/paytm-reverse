package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;
import net.one97.paytm.common.entity.trainticket.CJRSearchedCityDetails;

public class CJRTrainCity
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="body")
  private ArrayList<CJRTrainOriginCityItem> mTrainOriginCityItems;
  private ArrayList<CJRSearchedCityDetails> mTrainSearchedCityDetails;
  
  public ArrayList<CJRTrainOriginCityItem> getmTrainOriginCityItems()
  {
    return this.mTrainOriginCityItems;
  }
  
  public ArrayList<CJRSearchedCityDetails> getmTrainSearchedCityDetails()
  {
    return this.mTrainSearchedCityDetails;
  }
  
  public void setmTrainOriginCityItems(ArrayList<CJRTrainOriginCityItem> paramArrayList)
  {
    this.mTrainOriginCityItems = paramArrayList;
  }
  
  public void setmTrainSearchedCityDetails(ArrayList<CJRSearchedCityDetails> paramArrayList)
  {
    this.mTrainSearchedCityDetails = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRTrainCity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */