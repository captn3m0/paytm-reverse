package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainForgotUserId
  implements IJRDataModel
{
  @b(a="body")
  private CJRForgotUserIdBody mForgotUserIdBody;
  @b(a="status")
  private CJRForgotUserIdStatus mForgotUserIsStatus;
  
  public CJRForgotUserIdBody getForgotUserIdBody()
  {
    return this.mForgotUserIdBody;
  }
  
  public CJRForgotUserIdStatus getForgotUserIsStatus()
  {
    return this.mForgotUserIsStatus;
  }
  
  public void setForgotUserIdBody(CJRForgotUserIdBody paramCJRForgotUserIdBody)
  {
    this.mForgotUserIdBody = paramCJRForgotUserIdBody;
  }
  
  public void setForgotUserIsStatus(CJRForgotUserIdStatus paramCJRForgotUserIdStatus)
  {
    this.mForgotUserIsStatus = paramCJRForgotUserIdStatus;
  }
  
  public class CJRForgotUserIdBody
  {
    @b(a="status")
    private String mStatus;
    
    public CJRForgotUserIdBody() {}
    
    public String getStatus()
    {
      return this.mStatus;
    }
    
    public void setStatus(String paramString)
    {
      this.mStatus = paramString;
    }
  }
  
  public class CJRForgotUserIdMessage
  {
    @b(a="message")
    private String mMessage;
    @b(a="title")
    private String mTilte;
    
    public CJRForgotUserIdMessage() {}
    
    public String getMessage()
    {
      return this.mMessage;
    }
    
    public String getTilte()
    {
      return this.mTilte;
    }
    
    public void setTilte(String paramString)
    {
      this.mTilte = paramString;
    }
    
    public void setmMessage(String paramString)
    {
      this.mMessage = paramString;
    }
  }
  
  public class CJRForgotUserIdStatus
  {
    @b(a="message")
    private CJRTrainForgotUserId.CJRForgotUserIdMessage mForgotUserIdMessage;
    @b(a="result")
    private String mResult;
    
    public CJRForgotUserIdStatus() {}
    
    public CJRTrainForgotUserId.CJRForgotUserIdMessage getForgotUserIdMessage()
    {
      return this.mForgotUserIdMessage;
    }
    
    public String getResult()
    {
      return this.mResult;
    }
    
    public void setForgotUserIdMessage(CJRTrainForgotUserId.CJRForgotUserIdMessage paramCJRForgotUserIdMessage)
    {
      this.mForgotUserIdMessage = paramCJRForgotUserIdMessage;
    }
    
    public void setResult(String paramString)
    {
      this.mResult = paramString;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRTrainForgotUserId.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */