package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBoardingStationDetails
  implements IJRDataModel
{
  @b(a="dayCount")
  private int mDayCount;
  @b(a="departureTime")
  private String mDepartureTime;
  
  public int getDayCount()
  {
    return this.mDayCount;
  }
  
  public String getDepartureTime()
  {
    return this.mDepartureTime;
  }
  
  public void setDayCount(int paramInt)
  {
    this.mDayCount = paramInt;
  }
  
  public void setDepartureTime(String paramString)
  {
    this.mDepartureTime = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRBoardingStationDetails.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */