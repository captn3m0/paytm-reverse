package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import java.io.Serializable;

public class CJRTrainOriginCityItem
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @b(a="code")
  private String cityCode;
  @b(a="name")
  private String cityName;
  private boolean isRecentObject = false;
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if ((paramObject == null) || (getClass() != paramObject.getClass())) {
        return false;
      }
      paramObject = (CJRTrainOriginCityItem)paramObject;
      if (this.cityName != null)
      {
        if (this.cityName.equals(((CJRTrainOriginCityItem)paramObject).cityName)) {}
      }
      else {
        while (((CJRTrainOriginCityItem)paramObject).cityName != null) {
          return false;
        }
      }
      if (this.cityCode != null) {
        return this.cityCode.equals(((CJRTrainOriginCityItem)paramObject).cityCode);
      }
    } while (((CJRTrainOriginCityItem)paramObject).cityCode == null);
    return false;
  }
  
  public String getCityCode()
  {
    return this.cityCode;
  }
  
  public String getCityName()
  {
    return this.cityName;
  }
  
  public int hashCode()
  {
    int j = 0;
    if (this.cityName != null) {}
    for (int i = this.cityName.hashCode();; i = 0)
    {
      if (this.cityCode != null) {
        j = this.cityCode.hashCode();
      }
      return i * 31 + j;
    }
  }
  
  public boolean isRecentObject()
  {
    return this.isRecentObject;
  }
  
  public void setCityCode(String paramString)
  {
    this.cityCode = paramString;
  }
  
  public void setCityName(String paramString)
  {
    this.cityName = paramString;
  }
  
  public void setRecentObject(boolean paramBoolean)
  {
    this.isRecentObject = paramBoolean;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRTrainOriginCityItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */