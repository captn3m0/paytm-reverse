package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRCountrys
  implements IJRDataModel
{
  @b(a="country")
  private String mCountry;
  @b(a="countryCode")
  private String mCountryCode;
  @b(a="countryId")
  private String mCountryId;
  
  public String getCountry()
  {
    return this.mCountry;
  }
  
  public String getCountryCode()
  {
    return this.mCountryCode;
  }
  
  public String getCountryId()
  {
    return this.mCountryId;
  }
  
  public void setCountry(String paramString)
  {
    this.mCountry = paramString;
  }
  
  public void setCountryCode(String paramString)
  {
    this.mCountryCode = paramString;
  }
  
  public void setCountryId(String paramString)
  {
    this.mCountryId = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRCountrys.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */