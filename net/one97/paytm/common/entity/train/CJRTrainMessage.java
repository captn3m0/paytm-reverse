package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainMessage
  implements IJRDataModel
{
  @b(a="enabled")
  private boolean mEnable;
  @b(a="message_text")
  private String mMessage;
  @b(a="traveller_message")
  private String mTravellerMessage;
  @b(a="traveller_message_enabled")
  private boolean mTravellerMessageEnable;
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getTravellerMessage()
  {
    return this.mTravellerMessage;
  }
  
  public boolean isEnable()
  {
    return this.mEnable;
  }
  
  public boolean isTravellerMessageEnable()
  {
    return this.mTravellerMessageEnable;
  }
  
  public void setEnable(boolean paramBoolean)
  {
    this.mEnable = paramBoolean;
  }
  
  public void setMessage(String paramString)
  {
    this.mMessage = paramString;
  }
  
  public void setTravellerMessage(String paramString)
  {
    this.mTravellerMessage = paramString;
  }
  
  public void setTravellerMessageEnable(boolean paramBoolean)
  {
    this.mTravellerMessageEnable = paramBoolean;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRTrainMessage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */