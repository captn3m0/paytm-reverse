package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainForgotPassword
  implements IJRDataModel
{
  @b(a="status")
  private CJRForgotPasswordStatus mForgotPasswordStatus;
  @b(a="body")
  private CJRForgotPasswordBody mPasswordBody;
  
  public CJRForgotPasswordStatus getForgotPasswordStatus()
  {
    return this.mForgotPasswordStatus;
  }
  
  public CJRForgotPasswordBody getPasswordBody()
  {
    return this.mPasswordBody;
  }
  
  public void setForgotPasswordStatus(CJRForgotPasswordStatus paramCJRForgotPasswordStatus)
  {
    this.mForgotPasswordStatus = paramCJRForgotPasswordStatus;
  }
  
  public void setPasswordBody(CJRForgotPasswordBody paramCJRForgotPasswordBody)
  {
    this.mPasswordBody = paramCJRForgotPasswordBody;
  }
  
  public class CJRForgotPasswordBody
  {
    @b(a="status")
    private String mStatus;
    
    public CJRForgotPasswordBody() {}
    
    public String getStatus()
    {
      return this.mStatus;
    }
    
    public void setStatus(String paramString)
    {
      this.mStatus = paramString;
    }
  }
  
  public class CJRForgotPasswordMessage
  {
    @b(a="message")
    private String mMessage;
    @b(a="title")
    private String mTilte;
    
    public CJRForgotPasswordMessage() {}
    
    public String getMessage()
    {
      return this.mMessage;
    }
    
    public String getTilte()
    {
      return this.mTilte;
    }
    
    public void setTilte(String paramString)
    {
      this.mTilte = paramString;
    }
    
    public void setmMessage(String paramString)
    {
      this.mMessage = paramString;
    }
  }
  
  public class CJRForgotPasswordStatus
  {
    @b(a="message")
    private CJRTrainForgotPassword.CJRForgotPasswordMessage mResendOTPMessage;
    @b(a="result")
    private String mResult;
    
    public CJRForgotPasswordStatus() {}
    
    public CJRTrainForgotPassword.CJRForgotPasswordMessage getResendOTPMessage()
    {
      return this.mResendOTPMessage;
    }
    
    public String getResult()
    {
      return this.mResult;
    }
    
    public void setResendOTPMessage(CJRTrainForgotPassword.CJRForgotPasswordMessage paramCJRForgotPasswordMessage)
    {
      this.mResendOTPMessage = paramCJRForgotPasswordMessage;
    }
    
    public void setResult(String paramString)
    {
      this.mResult = paramString;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRTrainForgotPassword.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */