package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import net.one97.paytm.common.entity.CJRStatusError;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainDetails
  implements IJRDataModel
{
  @b(a="error")
  private String mError;
  @b(a="status")
  private CJRStatusError mStatus;
  @b(a="body")
  private CJRTrainDetailsBody mTrainDetailsBody;
  
  public String getmError()
  {
    return this.mError;
  }
  
  public CJRStatusError getmStatus()
  {
    return this.mStatus;
  }
  
  public CJRTrainDetailsBody getmTrainDetailsBody()
  {
    return this.mTrainDetailsBody;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRTrainDetails.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */