package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import java.util.HashMap;
import java.util.Map;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainAvailability
  implements IJRDataModel
{
  @b(a="booking_allowed")
  private boolean mBookingAllowed;
  @b(a="date")
  private String mDate;
  private Map<String, String> mFare;
  @b(a="message")
  private String mMessage;
  @b(a="message_enabled")
  private boolean mMessageEnable;
  private String mQuota = "General";
  private String mQuotaCode = "GN";
  @b(a="seats")
  private int mSeats;
  @b(a="status")
  private String mSttaus;
  @b(a="type")
  private HashMap<String, String> mTypeMap;
  
  public Map<String, String> getFare()
  {
    return this.mFare;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getQuota()
  {
    return this.mQuota;
  }
  
  public HashMap<String, String> getTypeMap()
  {
    return this.mTypeMap;
  }
  
  public String getmDate()
  {
    return this.mDate;
  }
  
  public String getmQuotaCode()
  {
    return this.mQuotaCode;
  }
  
  public int getmSeats()
  {
    return this.mSeats;
  }
  
  public String getmSttaus()
  {
    return this.mSttaus;
  }
  
  public boolean isBookingAllowed()
  {
    return this.mBookingAllowed;
  }
  
  public boolean isMessageEnable()
  {
    return this.mMessageEnable;
  }
  
  public void setBookingAllowed(boolean paramBoolean)
  {
    this.mBookingAllowed = paramBoolean;
  }
  
  public void setFare(Map<String, String> paramMap)
  {
    this.mFare = paramMap;
  }
  
  public void setMessage(String paramString)
  {
    this.mMessage = paramString;
  }
  
  public void setMessageEnable(boolean paramBoolean)
  {
    this.mMessageEnable = paramBoolean;
  }
  
  public void setQuota(String paramString)
  {
    this.mQuota = paramString;
  }
  
  public void setTypeMap(HashMap<String, String> paramHashMap)
  {
    this.mTypeMap = paramHashMap;
  }
  
  public void setmQuotaCode(String paramString)
  {
    this.mQuotaCode = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRTrainAvailability.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */