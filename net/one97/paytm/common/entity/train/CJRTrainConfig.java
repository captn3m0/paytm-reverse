package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainConfig
  implements IJRDataModel
{
  @b(a="bedRollFlagEnabled")
  private boolean mBedRollFlagEnabled;
  @b(a="childBerthMandatory")
  private boolean mChildBerthMandatory;
  @b(a="foodChoiceEnabled")
  private boolean mFoodChoiceEnables;
  @b(a="foodDetails")
  private ArrayList<String> mFoodDetailList;
  @b(a="idRequired")
  private boolean mIdRequired;
  @b(a="maxChildAge")
  private int mMaxChildAge;
  @b(a="maxInfants")
  private int mMaxInfant;
  @b(a="maxNameLength")
  private int mMaxNameLength;
  @b(a="maxPassengerAge")
  private int mMaxPassengerAge;
  @b(a="maxPassengers")
  private int mMaxPassengers;
  @b(a="minNameLength")
  private int mMinNameLength;
  @b(a="minPassengerAge")
  private int mMinPassengerAge;
  @b(a="seniorCitizenApplicable")
  private boolean mSeniorCitizenApplicable;
  @b(a="srctznAge")
  private int mSeniorMenAge;
  @b(a="srctnwAge")
  private int mSeniorWomenAge;
  @b(a="validIdCardTypes")
  private ArrayList<String> mValidIdCardTypes;
  
  public ArrayList<String> getFoodDetailList()
  {
    return this.mFoodDetailList;
  }
  
  public int getMaxChildAge()
  {
    return this.mMaxChildAge;
  }
  
  public int getMaxInfant()
  {
    return this.mMaxInfant;
  }
  
  public int getMaxNameLength()
  {
    return this.mMaxNameLength;
  }
  
  public int getMaxPassengerAge()
  {
    return this.mMaxPassengerAge;
  }
  
  public int getMaxPassengers()
  {
    return this.mMaxPassengers;
  }
  
  public int getMinNameLength()
  {
    return this.mMinNameLength;
  }
  
  public int getMinPassengerAge()
  {
    return this.mMinPassengerAge;
  }
  
  public int getSeniorMenAge()
  {
    return this.mSeniorMenAge;
  }
  
  public int getSeniorWomenAge()
  {
    return this.mSeniorWomenAge;
  }
  
  public ArrayList<String> getValidIdCardTypes()
  {
    return this.mValidIdCardTypes;
  }
  
  public boolean isChildBerthMandatory()
  {
    return this.mChildBerthMandatory;
  }
  
  public boolean isSeniorCitizenApplicable()
  {
    return this.mSeniorCitizenApplicable;
  }
  
  public boolean ismBedRollFlagEnabled()
  {
    return this.mBedRollFlagEnabled;
  }
  
  public boolean ismFoodChoiceEnables()
  {
    return this.mFoodChoiceEnables;
  }
  
  public boolean ismIdRequired()
  {
    return this.mIdRequired;
  }
  
  public void setChildBerthMandatory(boolean paramBoolean)
  {
    this.mChildBerthMandatory = paramBoolean;
  }
  
  public void setFoodDetailList(ArrayList<String> paramArrayList)
  {
    this.mFoodDetailList = paramArrayList;
  }
  
  public void setSeniorCitizenApplicable(boolean paramBoolean)
  {
    this.mSeniorCitizenApplicable = paramBoolean;
  }
  
  public void setSeniorMenAge(int paramInt)
  {
    this.mSeniorMenAge = paramInt;
  }
  
  public void setSeniorWomenAge(int paramInt)
  {
    this.mSeniorWomenAge = paramInt;
  }
  
  public void setmBedRollFlagEnabled(boolean paramBoolean)
  {
    this.mBedRollFlagEnabled = paramBoolean;
  }
  
  public void setmFoodChoiceEnables(boolean paramBoolean)
  {
    this.mFoodChoiceEnables = paramBoolean;
  }
  
  public void setmIdRequired(boolean paramBoolean)
  {
    this.mIdRequired = paramBoolean;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRTrainConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */