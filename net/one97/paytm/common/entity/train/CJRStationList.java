package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRStationList
  implements IJRDataModel
{
  private boolean isDestination;
  private boolean isSource;
  @b(a="arrivalTime")
  private String mArrivalTime;
  @b(a="dayCount")
  private String mDayCount;
  @b(a="departureTime")
  private String mDepartureTime;
  @b(a="distance")
  private String mDistance;
  @b(a="haltTime")
  private String mHaltTime;
  @b(a="routeNumber")
  private String mRouteNumber;
  @b(a="stationCode")
  private String mStationCode;
  @b(a="stationName")
  private String mStationName;
  @b(a="stnSerialNumber")
  private String mStnSerialNumber;
  private String mTrainDay;
  
  public String getTrainDay()
  {
    return this.mTrainDay;
  }
  
  public String getmArrivalTime()
  {
    return this.mArrivalTime;
  }
  
  public String getmDayCount()
  {
    return this.mDayCount;
  }
  
  public String getmDepartureTime()
  {
    return this.mDepartureTime;
  }
  
  public String getmDistance()
  {
    return this.mDistance;
  }
  
  public String getmHaltTime()
  {
    return this.mHaltTime;
  }
  
  public String getmRouteNumber()
  {
    return this.mRouteNumber;
  }
  
  public String getmStationCode()
  {
    return this.mStationCode;
  }
  
  public String getmStationName()
  {
    return this.mStationName;
  }
  
  public String getmStnSerialNumber()
  {
    return this.mStnSerialNumber;
  }
  
  public boolean isDestination()
  {
    return this.isDestination;
  }
  
  public boolean isSource()
  {
    return this.isSource;
  }
  
  public void setIsDestination(boolean paramBoolean)
  {
    this.isDestination = paramBoolean;
  }
  
  public void setSource(boolean paramBoolean)
  {
    this.isSource = paramBoolean;
  }
  
  public void setTrainDay(String paramString)
  {
    this.mTrainDay = paramString;
  }
  
  public void setmArrivalTime(String paramString)
  {
    this.mArrivalTime = paramString;
  }
  
  public void setmHaltTime(String paramString)
  {
    this.mHaltTime = paramString;
  }
  
  public void setmStationCode(String paramString)
  {
    this.mStationCode = paramString;
  }
  
  public void setmStationName(String paramString)
  {
    this.mStationName = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRStationList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */