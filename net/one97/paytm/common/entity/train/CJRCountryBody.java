package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRCountryBody
  implements IJRDataModel
{
  @b(a="countryList")
  private ArrayList<CJRCountrys> mCountryList;
  
  public ArrayList<CJRCountrys> getCountryList()
  {
    return this.mCountryList;
  }
  
  public void setCountryList(ArrayList<CJRCountrys> paramArrayList)
  {
    this.mCountryList = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRCountryBody.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */