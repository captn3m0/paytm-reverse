package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPincodeResponse
  implements IJRDataModel
{
  @b(a="body")
  CJRPincodeBody mPincodeBody;
  
  public CJRPincodeBody getPincodeBody()
  {
    return this.mPincodeBody;
  }
  
  public class CJRCities
    implements IJRDataModel
  {
    @b(a="name")
    private String mCityName;
    @b(a="po_list")
    private ArrayList<String> mPostOfficeList;
    
    public CJRCities() {}
    
    public String getCityName()
    {
      return this.mCityName;
    }
    
    public ArrayList<String> getPostOfficeList()
    {
      return this.mPostOfficeList;
    }
  }
  
  public class CJRPincodeBody
    implements IJRDataModel
  {
    @b(a="cities")
    ArrayList<CJRPincodeResponse.CJRCities> mCityList;
    @b(a="state")
    private String mState;
    
    public CJRPincodeBody() {}
    
    public ArrayList<CJRPincodeResponse.CJRCities> getCityList()
    {
      return this.mCityList;
    }
    
    public String getState()
    {
      return this.mState;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRPincodeResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */