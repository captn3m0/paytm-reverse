package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPassengerInformation
  implements IJRDataModel
{
  @b(a="passenger_age")
  private String mPassengerAge;
  @b(a="passenger_gender")
  private String mPassengerGender;
  
  public String getPassengerAge()
  {
    return this.mPassengerAge;
  }
  
  public String getPassengerGender()
  {
    return this.mPassengerGender;
  }
  
  public void setPassengerAge(String paramString)
  {
    this.mPassengerAge = paramString;
  }
  
  public void setPassengerGender(String paramString)
  {
    this.mPassengerGender = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRPassengerInformation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */