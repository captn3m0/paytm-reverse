package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainPreferences
  implements IJRDataModel
{
  @b(a="enabled")
  private boolean mEnable;
  @b(a="reservation_choice")
  private HashMap<String, Integer> mPreferenceChoice;
  
  public HashMap<String, Integer> getPreferenceChoice()
  {
    return this.mPreferenceChoice;
  }
  
  public ArrayList<String> getPreferenceList()
  {
    ArrayList localArrayList = new ArrayList();
    if ((this.mPreferenceChoice != null) && (this.mPreferenceChoice.size() > 0))
    {
      Iterator localIterator = this.mPreferenceChoice.entrySet().iterator();
      while (localIterator.hasNext()) {
        localArrayList.add(((Map.Entry)localIterator.next()).getKey());
      }
    }
    return localArrayList;
  }
  
  public boolean isEnable()
  {
    return this.mEnable;
  }
  
  public void setEnable(boolean paramBoolean)
  {
    this.mEnable = paramBoolean;
  }
  
  public void setPreferenceChoice(HashMap<String, Integer> paramHashMap)
  {
    this.mPreferenceChoice = paramHashMap;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRTrainPreferences.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */