package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainTrackingInfo
  implements IJRDataModel
{
  @b(a="error")
  private String mError;
  @b(a="code")
  private int mResponseCode;
  
  public String getError()
  {
    return this.mError;
  }
  
  public int getResponseCode()
  {
    return this.mResponseCode;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRTrainTrackingInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */