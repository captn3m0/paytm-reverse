package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import java.util.HashMap;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainFare
  implements IJRDataModel
{
  @b(a="fare_breakup")
  private HashMap<String, String> mFareBreakUp;
  @b(a="grand_total")
  private String mGrandTotal;
  @b(a="irctc_service_fee")
  private String mIRCTC_Service_Fee;
  @b(a="paytm_service_fee")
  private String mPaytm_Service_Fee;
  @b(a="pg_charge")
  private String mPgCharge;
  @b(a="total_collectible")
  private String mTotalCollectable;
  @b(a="total_fare")
  private String mTotalFare;
  
  public HashMap<String, String> getFareBreakUp()
  {
    return this.mFareBreakUp;
  }
  
  public String getGrandTotal()
  {
    return this.mGrandTotal;
  }
  
  public String getPgCharge()
  {
    return this.mPgCharge;
  }
  
  public String getTotalCollectable()
  {
    return this.mTotalCollectable;
  }
  
  public String getTotalFare()
  {
    return this.mTotalFare;
  }
  
  public String getmIRCTC_Service_Fee()
  {
    return this.mIRCTC_Service_Fee;
  }
  
  public String getmPaytm_Service_Fee()
  {
    return this.mPaytm_Service_Fee;
  }
  
  public void setFareBreakUp(HashMap<String, String> paramHashMap)
  {
    this.mFareBreakUp = paramHashMap;
  }
  
  public void setTotalCollectable(String paramString)
  {
    this.mTotalCollectable = paramString;
  }
  
  public void setTotalFare(String paramString)
  {
    this.mTotalFare = paramString;
  }
  
  public void setmIRCTC_Service_Fee(String paramString)
  {
    this.mIRCTC_Service_Fee = paramString;
  }
  
  public void setmPaytm_Service_Fee(String paramString)
  {
    this.mPaytm_Service_Fee = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRTrainFare.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */