package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainDetailsBody
  implements IJRDataModel
{
  private String boardingDate;
  private String boardingTime;
  private boolean mAutoUpgrade;
  @b(a="berths")
  public ArrayList<CJRTravellerBerth> mBerth;
  @b(a="boarding_details")
  private ArrayList<CJRBoardingStationDetails> mBoardingDetail;
  @b(a="boarding_stations")
  public ArrayList<String> mBoardingStation;
  private String mCoachNumber;
  private String mEmailAddress;
  private String mFormattedBoardingStation;
  @b(a="message")
  private CJRTrainMessage mMessage;
  private ArrayList<CJRPassengerInfo> mPassengerInfo;
  private int mReservationCode;
  private String mSelectedBoardingStation;
  @b(a="availability")
  private ArrayList<CJRTrainAvailability> mTrainAvailability;
  @b(a="config")
  private CJRTrainConfig mTrainConfig;
  @b(a="fare")
  private CJRTrainFare mTrainFare;
  @b(a="trainName")
  private String mTrainName;
  @b(a="preferences")
  private CJRTrainPreferences mTrainPreferences;
  private String mUserMobileNumber;
  private int miSelectedPosition;
  
  public boolean getAutoUpgrade()
  {
    return this.mAutoUpgrade;
  }
  
  public ArrayList<CJRTravellerBerth> getBerth()
  {
    return this.mBerth;
  }
  
  public String getBoardingDate()
  {
    return this.boardingDate;
  }
  
  public ArrayList<CJRBoardingStationDetails> getBoardingDetail()
  {
    return this.mBoardingDetail;
  }
  
  public ArrayList<String> getBoardingStation()
  {
    return this.mBoardingStation;
  }
  
  public String getBoardingTime()
  {
    return this.boardingTime;
  }
  
  public String getCoachNumber()
  {
    return this.mCoachNumber;
  }
  
  public String getEmailAddress()
  {
    return this.mEmailAddress;
  }
  
  public String getFormattedBoardingStation()
  {
    return this.mFormattedBoardingStation;
  }
  
  public CJRTrainMessage getMessage()
  {
    return this.mMessage;
  }
  
  public int getMiSelectedPosition()
  {
    return this.miSelectedPosition;
  }
  
  public ArrayList<CJRPassengerInfo> getPassengerInfo()
  {
    return this.mPassengerInfo;
  }
  
  public int getReservationCode()
  {
    return this.mReservationCode;
  }
  
  public String getSelectedBoardingStation()
  {
    return this.mSelectedBoardingStation;
  }
  
  public CJRTrainConfig getTrainConfig()
  {
    return this.mTrainConfig;
  }
  
  public CJRTrainFare getTrainFare()
  {
    return this.mTrainFare;
  }
  
  public CJRTrainPreferences getTrainPreferences()
  {
    return this.mTrainPreferences;
  }
  
  public String getUserMobileNumber()
  {
    return this.mUserMobileNumber;
  }
  
  public ArrayList<CJRTrainAvailability> getmTrainAvailability()
  {
    return this.mTrainAvailability;
  }
  
  public String getmTrainName()
  {
    return this.mTrainName;
  }
  
  public void setAutoUpgrade(boolean paramBoolean)
  {
    this.mAutoUpgrade = paramBoolean;
  }
  
  public void setBoardingDate(String paramString)
  {
    this.boardingDate = paramString;
  }
  
  public void setBoardingDetail(ArrayList<CJRBoardingStationDetails> paramArrayList)
  {
    this.mBoardingDetail = paramArrayList;
  }
  
  public void setBoardingTime(String paramString)
  {
    this.boardingTime = paramString;
  }
  
  public void setCoachNumber(String paramString)
  {
    this.mCoachNumber = paramString;
  }
  
  public void setEmailAddress(String paramString)
  {
    this.mEmailAddress = paramString;
  }
  
  public void setFormattedBoardingStation(String paramString)
  {
    this.mFormattedBoardingStation = paramString;
  }
  
  public void setMessage(CJRTrainMessage paramCJRTrainMessage)
  {
    this.mMessage = paramCJRTrainMessage;
  }
  
  public void setMiSelectedPosition(int paramInt)
  {
    this.miSelectedPosition = paramInt;
  }
  
  public void setPassengerInfo(ArrayList<CJRPassengerInfo> paramArrayList)
  {
    this.mPassengerInfo = paramArrayList;
  }
  
  public void setReservationCode(int paramInt)
  {
    this.mReservationCode = paramInt;
  }
  
  public void setSelectedBoardingStation(String paramString)
  {
    this.mSelectedBoardingStation = paramString;
  }
  
  public void setTrainFare(CJRTrainFare paramCJRTrainFare)
  {
    this.mTrainFare = paramCJRTrainFare;
  }
  
  public void setTrainPreferences(CJRTrainPreferences paramCJRTrainPreferences)
  {
    this.mTrainPreferences = paramCJRTrainPreferences;
  }
  
  public void setUserMobileNumber(String paramString)
  {
    this.mUserMobileNumber = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRTrainDetailsBody.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */