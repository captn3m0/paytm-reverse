package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBoardingStation
  implements IJRDataModel
{
  @b(a="body")
  ArrayList<String> mBoardingStation;
  
  public ArrayList<String> getBoardingStation()
  {
    return this.mBoardingStation;
  }
  
  public void setBoardingStation(ArrayList<String> paramArrayList)
  {
    this.mBoardingStation = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRBoardingStation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */