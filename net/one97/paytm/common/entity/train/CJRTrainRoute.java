package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import net.one97.paytm.common.entity.CJRStatusError;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrainRoute
  implements IJRDataModel
{
  @b(a="error")
  private String mError;
  @b(a="body")
  private CJRTrainRouteBody mRouteDetails;
  @b(a="status")
  private CJRStatusError mStatus;
  
  public String getmError()
  {
    return this.mError;
  }
  
  public CJRTrainRouteBody getmRouteDetails()
  {
    return this.mRouteDetails;
  }
  
  public CJRStatusError getmStatus()
  {
    return this.mStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRTrainRoute.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */