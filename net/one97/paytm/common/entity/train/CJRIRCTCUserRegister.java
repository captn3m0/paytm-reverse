package net.one97.paytm.common.entity.train;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRIRCTCUserRegister
  implements IJRDataModel
{
  @b(a="error")
  private String mError;
  @b(a="body")
  private CJRResponseBody mResponseBody;
  @b(a="status")
  private CJRRegistrationStatus mStatus;
  
  public String getError()
  {
    return this.mError;
  }
  
  public CJRResponseBody getResponseBody()
  {
    return this.mResponseBody;
  }
  
  public CJRRegistrationStatus getStatus()
  {
    return this.mStatus;
  }
  
  public void setError(String paramString)
  {
    this.mError = paramString;
  }
  
  public void setResponseBody(CJRResponseBody paramCJRResponseBody)
  {
    this.mResponseBody = paramCJRResponseBody;
  }
  
  public void setStatus(CJRRegistrationStatus paramCJRRegistrationStatus)
  {
    this.mStatus = paramCJRRegistrationStatus;
  }
  
  public class CJRRegistrationStatus
  {
    @b(a="result")
    private String mResult;
    @b(a="message")
    private CJRIRCTCUserRegister.RegistrationMessage message;
    
    public CJRRegistrationStatus() {}
    
    public CJRIRCTCUserRegister.RegistrationMessage getMessage()
    {
      return this.message;
    }
    
    public String getResult()
    {
      return this.mResult;
    }
    
    public void setMessage(CJRIRCTCUserRegister.RegistrationMessage paramRegistrationMessage)
    {
      this.message = paramRegistrationMessage;
    }
    
    public void setResult(String paramString)
    {
      this.mResult = paramString;
    }
  }
  
  public class CJRResponseBody
  {
    @b(a="status")
    String mStatus;
    
    public CJRResponseBody() {}
    
    public String getStatus()
    {
      return this.mStatus;
    }
    
    public void setStatus(String paramString)
    {
      this.mStatus = paramString;
    }
  }
  
  public class RegistrationMessage
  {
    @b(a="message")
    private String mMessage;
    @b(a="title")
    private String mTittle;
    
    public RegistrationMessage() {}
    
    public String getMessage()
    {
      return this.mMessage;
    }
    
    public String getTittle()
    {
      return this.mTittle;
    }
    
    public void setMessage(String paramString)
    {
      this.mMessage = paramString;
    }
    
    public void setTittle(String paramString)
    {
      this.mTittle = paramString;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/train/CJRIRCTCUserRegister.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */