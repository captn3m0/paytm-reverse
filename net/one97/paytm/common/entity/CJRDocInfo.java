package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.List;

public class CJRDocInfo
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="data")
  private String mData;
  @b(a="docList")
  private List<CJRDocListInfo> mDocList;
  @b(a="message")
  private String mMessage;
  @b(a="responseCode")
  private String mResponseCode;
  @b(a="status")
  private String mStatus;
  @b(a="success")
  private boolean mSuccess;
  @b(a="userId")
  private String mUserId;
  
  public String getData()
  {
    return this.mData;
  }
  
  public List<CJRDocListInfo> getDocList()
  {
    return this.mDocList;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getResponseCode()
  {
    return this.mResponseCode;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public boolean getSuccess()
  {
    return this.mSuccess;
  }
  
  public String getUserId()
  {
    return this.mUserId;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRDocInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */