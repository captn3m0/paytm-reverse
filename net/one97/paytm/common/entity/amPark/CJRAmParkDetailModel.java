package net.one97.paytm.common.entity.amPark;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAmParkDetailModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="address")
  private String mAddress;
  @b(a="categories")
  private ArrayList<CJRCategoriesDetailModel> mCategories = new ArrayList();
  @b(a="city")
  private String mCity;
  @b(a="custom_info")
  private CJRAmParkCustomInfo mCustomInfo;
  @b(a="id")
  private String mId;
  @b(a="image")
  private String mImage;
  @b(a="image_app")
  private String mImageApp;
  @b(a="name")
  private String mName;
  @b(a="open_from")
  private String mOpenFrom;
  @b(a="open_till")
  private String mOpenTill;
  @b(a="paytm_id")
  private String mPaytmId;
  @b(a="price")
  private String mPrice;
  @b(a="priority")
  private String mPriority;
  @b(a="provider_id")
  private String mProviderId;
  @b(a="provider_priority")
  private String mProviderPriority;
  @b(a="resource_id")
  private String mResourceId;
  @b(a="status")
  private String mStatus;
  @b(a="tags")
  private String mtags;
  
  public String getAddress()
  {
    return this.mAddress;
  }
  
  public ArrayList<CJRCategoriesDetailModel> getCategories()
  {
    return this.mCategories;
  }
  
  public String getImageURL()
  {
    return this.mImage;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getPrice()
  {
    return this.mPrice;
  }
  
  public String getProviderId()
  {
    return this.mProviderId;
  }
  
  public String getmCity()
  {
    return this.mCity;
  }
  
  public CJRAmParkCustomInfo getmCustomInfo()
  {
    return this.mCustomInfo;
  }
  
  public String getmId()
  {
    return this.mId;
  }
  
  public String getmImageApp()
  {
    return this.mImageApp;
  }
  
  public String getmOpenFrom()
  {
    return this.mOpenFrom;
  }
  
  public String getmOpenTill()
  {
    return this.mOpenTill;
  }
  
  public String getmPaytmId()
  {
    return this.mPaytmId;
  }
  
  public String getmPriority()
  {
    return this.mPriority;
  }
  
  public String getmProviderPriority()
  {
    return this.mProviderPriority;
  }
  
  public String getmResourceId()
  {
    return this.mResourceId;
  }
  
  public String getmStatus()
  {
    return this.mStatus;
  }
  
  public String gettags()
  {
    return this.mtags;
  }
  
  public void setAddress(String paramString)
  {
    this.mAddress = paramString;
  }
  
  public void setName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setmCategories(ArrayList<CJRCategoriesDetailModel> paramArrayList)
  {
    this.mCategories = paramArrayList;
  }
  
  public void setmCity(String paramString)
  {
    this.mCity = paramString;
  }
  
  public void setmCustomInfo(CJRAmParkCustomInfo paramCJRAmParkCustomInfo)
  {
    this.mCustomInfo = paramCJRAmParkCustomInfo;
  }
  
  public void setmId(String paramString)
  {
    this.mId = paramString;
  }
  
  public void setmImage(String paramString)
  {
    this.mImage = paramString;
  }
  
  public void setmImageApp(String paramString)
  {
    this.mImageApp = paramString;
  }
  
  public void setmOpenFrom(String paramString)
  {
    this.mOpenFrom = paramString;
  }
  
  public void setmOpenTill(String paramString)
  {
    this.mOpenTill = paramString;
  }
  
  public void setmPaytmId(String paramString)
  {
    this.mPaytmId = paramString;
  }
  
  public void setmPrice(String paramString)
  {
    this.mPrice = paramString;
  }
  
  public void setmPriority(String paramString)
  {
    this.mPriority = paramString;
  }
  
  public void setmProviderId(String paramString)
  {
    this.mProviderId = paramString;
  }
  
  public void setmProviderPriority(String paramString)
  {
    this.mProviderPriority = paramString;
  }
  
  public void setmResourceId(String paramString)
  {
    this.mResourceId = paramString;
  }
  
  public void setmStatus(String paramString)
  {
    this.mStatus = paramString;
  }
  
  public void settags(String paramString)
  {
    this.mtags = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/CJRAmParkDetailModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */