package net.one97.paytm.common.entity.amPark;

import com.google.b.a.a;
import com.google.b.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRValidatePackage
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @a
  @b(a="allowFurther")
  private Boolean allowFurther;
  @a
  @b(a="message")
  private String message;
  @a
  @b(a="showMessage")
  private Boolean showMessage;
  @a
  @b(a="type")
  private String type;
  
  public Boolean getAllowFurther()
  {
    return this.allowFurther;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public Boolean getShowMessage()
  {
    return this.showMessage;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public void setAllowFurther(Boolean paramBoolean)
  {
    this.allowFurther = paramBoolean;
  }
  
  public void setMessage(String paramString)
  {
    this.message = paramString;
  }
  
  public void setShowMessage(Boolean paramBoolean)
  {
    this.showMessage = paramBoolean;
  }
  
  public void setType(String paramString)
  {
    this.type = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/CJRValidatePackage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */