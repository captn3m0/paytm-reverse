package net.one97.paytm.common.entity.amPark;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAmParkAutosuggestEventItem
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="category")
  private String category;
  @b(a="city")
  private String city;
  @b(a="id")
  private String parkId;
  @b(a="name")
  private String parkName;
  @b(a="provider_id")
  private String providerId;
  private int type = 1;
  
  public String getCategory()
  {
    return this.category;
  }
  
  public String getCity()
  {
    return this.city;
  }
  
  public String getParkId()
  {
    return this.parkId;
  }
  
  public String getParkName()
  {
    return this.parkName;
  }
  
  public String getProviderId()
  {
    return this.providerId;
  }
  
  public int getType()
  {
    return this.type;
  }
  
  public void setCategory(String paramString)
  {
    this.category = paramString;
  }
  
  public void setCity(String paramString)
  {
    this.city = paramString;
  }
  
  public void setParkId(String paramString)
  {
    this.parkId = paramString;
  }
  
  public void setParkName(String paramString)
  {
    this.parkName = paramString;
  }
  
  public void setProviderId(String paramString)
  {
    this.providerId = paramString;
  }
  
  public void setType(int paramInt)
  {
    this.type = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/CJRAmParkAutosuggestEventItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */