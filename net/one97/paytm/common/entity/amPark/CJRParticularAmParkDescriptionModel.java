package net.one97.paytm.common.entity.amPark;

import android.text.TextUtils;
import com.google.c.a.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRParticularAmParkDescriptionModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  private String citySearched;
  private String entityCity;
  @b(a="active_from")
  private String mActiveFrom;
  @b(a="active_to")
  private String mActiveTo;
  @b(a="address")
  private CJRAddressDetailModel mAddress;
  @b(a="categories")
  private List<CJRCategoriesDetailModel> mCategories = new ArrayList();
  @b(a="conv_fee")
  private String mConvFee;
  private Map<String, List<CJRSeatDetailsModel>> mDateTimeToSeatListMap;
  private Map<String, List<CJRAmParkDateTimeModel.Time>> mDateToTimeListMap;
  @b(a="description")
  private String mDescription;
  @b(a="form")
  private String mForm;
  @b(a="id")
  private String mId;
  @b(a="instructions")
  private String mInstructions;
  @b(a="inventory")
  private String mInventory;
  @b(a="park_rides")
  private ArrayList<CJRParkRidesDetailModel> mMajorAttractions = new ArrayList();
  @b(a="name")
  private String mName;
  @b(a="parkDateTime")
  private List<CJRAmParkDateTimeModel> mParkDateTimeList = new ArrayList();
  @b(a="paytm_commission")
  private String mPaytmCommission;
  @b(a="pg_charges")
  private String mPgCharges;
  @b(a="price")
  private String mPrice;
  @b(a="product_id")
  private String mProductId;
  @b(a="provider_event_id")
  private String mProviderEventId;
  @b(a="provider_id")
  private String mProviderId;
  @b(a="provider_name")
  private String mProviderName;
  @b(a="resources")
  private List<CJRResourceDetailModel> mResources = new ArrayList();
  @b(a="tags")
  private String mTags;
  @b(a="terms")
  private String mTerms;
  
  private void addCurrentSeatDetailListToSingleList(List<CJRParkPackageDetailsModel> paramList, List<CJRSeatDetailsModel> paramList1)
  {
    if ((paramList == null) || (paramList.size() <= 0)) {
      break label13;
    }
    label13:
    while (paramList1 == null) {
      return;
    }
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      CJRParkPackageDetailsModel localCJRParkPackageDetailsModel = (CJRParkPackageDetailsModel)paramList.next();
      List localList = localCJRParkPackageDetailsModel.getmParkSeatDetails();
      int i = 0;
      while (i < localList.size())
      {
        CJRSeatDetailsModel localCJRSeatDetailsModel = (CJRSeatDetailsModel)localList.get(i);
        if ((!hasSeatBeenAlreadyAdded(localCJRSeatDetailsModel, paramList1)) && (localCJRSeatDetailsModel.getPrice() != 0))
        {
          localCJRSeatDetailsModel.setmPackageType(localCJRParkPackageDetailsModel.getmParkPackageType());
          localCJRSeatDetailsModel.setmPackageDescription(localCJRParkPackageDetailsModel.getmParkPackageDescription());
          localCJRSeatDetailsModel.setmIndex(i);
          paramList1.add(localCJRSeatDetailsModel);
        }
        i += 1;
      }
    }
  }
  
  private void checkAndAddCurrentSeatDetailListToSingleList(List<CJRParkPackageDetailsModel> paramList, List<CJRSeatDetailsModel> paramList1)
  {
    if ((paramList == null) || (paramList.size() <= 0)) {
      break label13;
    }
    label13:
    while ((paramList1 == null) || (paramList1.size() <= 0)) {
      return;
    }
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      CJRParkPackageDetailsModel localCJRParkPackageDetailsModel = (CJRParkPackageDetailsModel)paramList.next();
      List localList = localCJRParkPackageDetailsModel.getmParkSeatDetails();
      int i = 0;
      while (i < localList.size())
      {
        CJRSeatDetailsModel localCJRSeatDetailsModel = (CJRSeatDetailsModel)localList.get(i);
        if ((!hasSeatBeenAlreadyAdded(localCJRSeatDetailsModel, paramList1)) && (localCJRSeatDetailsModel.getPrice() != 0))
        {
          localCJRSeatDetailsModel.setmPackageType(localCJRParkPackageDetailsModel.getmParkPackageType());
          localCJRSeatDetailsModel.setmPackageDescription(localCJRParkPackageDetailsModel.getmParkPackageDescription());
          localCJRSeatDetailsModel.setmIndex(i);
          paramList1.add(localCJRSeatDetailsModel);
        }
        i += 1;
      }
    }
  }
  
  private boolean checkIfTimeAlreadyExistsInList(CJRAmParkDateTimeModel.Time paramTime, List<CJRAmParkDateTimeModel.Time> paramList)
  {
    if ((paramList == null) || (paramList.size() <= 0) || (paramTime == null)) {}
    Object localObject;
    String str;
    do
    {
      while (!paramList.hasNext())
      {
        return false;
        paramList = paramList.iterator();
      }
      localObject = (CJRAmParkDateTimeModel.Time)paramList.next();
      str = ((CJRAmParkDateTimeModel.Time)localObject).getmStart();
      localObject = ((CJRAmParkDateTimeModel.Time)localObject).getmTo();
    } while ((TextUtils.isEmpty(str)) || (TextUtils.isEmpty((CharSequence)localObject)) || (TextUtils.isEmpty(paramTime.getmStart())) || (TextUtils.isEmpty(paramTime.getmTo())) || (!str.equalsIgnoreCase(paramTime.getmStart())) || (!((String)localObject).equalsIgnoreCase(paramTime.getmTo())));
    return true;
  }
  
  private boolean hasSeatBeenAlreadyAdded(CJRSeatDetailsModel paramCJRSeatDetailsModel, List<CJRSeatDetailsModel> paramList)
  {
    if ((paramCJRSeatDetailsModel == null) || (paramList == null) || (paramList.size() <= 0)) {}
    do
    {
      while (!paramList.hasNext())
      {
        return false;
        paramList = paramList.iterator();
      }
    } while (((CJRSeatDetailsModel)paramList.next()).getSeatId() != paramCJRSeatDetailsModel.getSeatId());
    return true;
  }
  
  public HashMap<String, List<CJRAmParkDateTimeModel.Time>> extractDateTimeMap(List<CJRAmParkDateTimeModel> paramList)
  {
    if ((paramList == null) || (paramList.size() <= 0)) {
      return null;
    }
    HashMap localHashMap = new HashMap();
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      Object localObject1 = (CJRAmParkDateTimeModel)paramList.next();
      Object localObject2 = ((CJRAmParkDateTimeModel)localObject1).getmParkDateList();
      localObject1 = ((CJRAmParkDateTimeModel)localObject1).getmTime();
      if ((localObject2 != null) && (((List)localObject2).size() > 0))
      {
        localObject2 = ((List)localObject2).iterator();
        while (((Iterator)localObject2).hasNext())
        {
          Object localObject3 = (String)((Iterator)localObject2).next();
          if (localObject3 != null) {
            if (localHashMap.containsKey(localObject3))
            {
              localObject3 = (List)localHashMap.get(localObject3);
              if (!checkIfTimeAlreadyExistsInList((CJRAmParkDateTimeModel.Time)localObject1, (List)localObject3)) {
                ((List)localObject3).add(localObject1);
              }
            }
            else
            {
              ArrayList localArrayList = new ArrayList();
              localArrayList.add(localObject1);
              localHashMap.put(localObject3, localArrayList);
            }
          }
        }
      }
    }
    return localHashMap;
  }
  
  public Map<String, List<CJRSeatDetailsModel>> extractDateTimeToSeatListMap(List<CJRAmParkDateTimeModel> paramList)
  {
    if ((paramList == null) || (paramList.size() <= 0)) {
      return null;
    }
    HashMap localHashMap = new HashMap();
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      Object localObject2 = (CJRAmParkDateTimeModel)paramList.next();
      Object localObject3 = ((CJRAmParkDateTimeModel)localObject2).getmParkDateList();
      Object localObject1 = ((CJRAmParkDateTimeModel)localObject2).getmTime();
      localObject1 = ((CJRAmParkDateTimeModel.Time)localObject1).getmStart() + ((CJRAmParkDateTimeModel.Time)localObject1).getmTo();
      localObject2 = ((CJRAmParkDateTimeModel)localObject2).getmPackageDetailsList();
      if ((localObject3 != null) && (((List)localObject3).size() > 0))
      {
        localObject3 = ((List)localObject3).iterator();
        while (((Iterator)localObject3).hasNext())
        {
          String str = (String)((Iterator)localObject3).next();
          if (str != null)
          {
            str = str + (String)localObject1;
            if (localHashMap.containsKey(str))
            {
              checkAndAddCurrentSeatDetailListToSingleList((List)localObject2, (List)localHashMap.get(str));
            }
            else
            {
              ArrayList localArrayList = new ArrayList();
              addCurrentSeatDetailListToSingleList((List)localObject2, localArrayList);
              localHashMap.put(str, localArrayList);
            }
          }
        }
      }
    }
    return localHashMap;
  }
  
  public String getActiveFrom()
  {
    return this.mActiveFrom;
  }
  
  public String getActiveTo()
  {
    return this.mActiveTo;
  }
  
  public List<CJRCategoriesDetailModel> getCategories()
  {
    return this.mCategories;
  }
  
  public String getCitySearched()
  {
    return this.citySearched;
  }
  
  public String getConvFee()
  {
    return this.mConvFee;
  }
  
  public Map<String, List<CJRSeatDetailsModel>> getDateTimeToSeatListMap()
  {
    if (this.mDateTimeToSeatListMap == null) {
      this.mDateTimeToSeatListMap = extractDateTimeToSeatListMap(this.mParkDateTimeList);
    }
    return this.mDateTimeToSeatListMap;
  }
  
  public Map<String, List<CJRAmParkDateTimeModel.Time>> getDateToTimeListMap()
  {
    if (this.mDateToTimeListMap == null) {
      this.mDateToTimeListMap = extractDateTimeMap(this.mParkDateTimeList);
    }
    return this.mDateToTimeListMap;
  }
  
  public String getDescription()
  {
    return this.mDescription;
  }
  
  public String getEntityCity()
  {
    return this.entityCity;
  }
  
  public String getForm()
  {
    return this.mForm;
  }
  
  public String getId()
  {
    return this.mId;
  }
  
  public String getInstructions()
  {
    return this.mInstructions;
  }
  
  public String getInventory()
  {
    return this.mInventory;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getPaytmCommission()
  {
    return this.mPaytmCommission;
  }
  
  public String getPgCharges()
  {
    return this.mPgCharges;
  }
  
  public String getPrice()
  {
    return this.mPrice;
  }
  
  public String getProductId()
  {
    return this.mProductId;
  }
  
  public String getProviderEventId()
  {
    return this.mProviderEventId;
  }
  
  public String getProviderId()
  {
    return this.mProviderId;
  }
  
  public String getProviderName()
  {
    return this.mProviderName;
  }
  
  public List<CJRResourceDetailModel> getResources()
  {
    return this.mResources;
  }
  
  public String getTags()
  {
    return this.mTags;
  }
  
  public String getTerms()
  {
    return this.mTerms;
  }
  
  public CJRAddressDetailModel getmAddress()
  {
    return this.mAddress;
  }
  
  public ArrayList<CJRParkRidesDetailModel> getmMajorAttractions()
  {
    return this.mMajorAttractions;
  }
  
  public List<CJRAmParkDateTimeModel> getmParkDateTimeList()
  {
    return this.mParkDateTimeList;
  }
  
  public void setActiveFrom(String paramString)
  {
    this.mActiveFrom = paramString;
  }
  
  public void setActiveTo(String paramString)
  {
    this.mActiveTo = paramString;
  }
  
  public void setCategories(List<CJRCategoriesDetailModel> paramList)
  {
    this.mCategories = paramList;
  }
  
  public void setCitySearched(String paramString)
  {
    this.citySearched = paramString;
  }
  
  public void setConvFee(String paramString)
  {
    this.mConvFee = paramString;
  }
  
  public void setDescription(String paramString)
  {
    this.mDescription = paramString;
  }
  
  public void setEntityCity(String paramString)
  {
    this.entityCity = paramString;
  }
  
  public void setForm(String paramString)
  {
    this.mForm = paramString;
  }
  
  public void setId(String paramString)
  {
    this.mId = paramString;
  }
  
  public void setInstructions(String paramString)
  {
    this.mInstructions = paramString;
  }
  
  public void setInventory(String paramString)
  {
    this.mInventory = paramString;
  }
  
  public void setName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setPaytmCommission(String paramString)
  {
    this.mPaytmCommission = paramString;
  }
  
  public void setPgCharges(String paramString)
  {
    this.mPgCharges = paramString;
  }
  
  public void setPrice(String paramString)
  {
    this.mPrice = paramString;
  }
  
  public void setProductId(String paramString)
  {
    this.mProductId = paramString;
  }
  
  public void setProviderEventId(String paramString)
  {
    this.mProviderEventId = paramString;
  }
  
  public void setProviderId(String paramString)
  {
    this.mProviderId = paramString;
  }
  
  public void setProviderName(String paramString)
  {
    this.mProviderName = paramString;
  }
  
  public void setResources(List<CJRResourceDetailModel> paramList)
  {
    this.mResources = paramList;
  }
  
  public void setTags(String paramString)
  {
    this.mTags = paramString;
  }
  
  public void setTerms(String paramString)
  {
    this.mTerms = paramString;
  }
  
  public void setmAddress(CJRAddressDetailModel paramCJRAddressDetailModel)
  {
    this.mAddress = paramCJRAddressDetailModel;
  }
  
  public void setmParkDateTimeList(List<CJRAmParkDateTimeModel> paramList)
  {
    this.mParkDateTimeList = paramList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/CJRParticularAmParkDescriptionModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */