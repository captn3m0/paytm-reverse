package net.one97.paytm.common.entity.amPark;

import com.google.b.a.a;
import com.google.b.a.b;
import java.util.List;

public class CJRValidatePackageRequest
{
  @a
  @b(a="channel")
  private String channel;
  @a
  @b(a="entId")
  private Integer entId;
  @a
  @b(a="page")
  private Integer page;
  @a
  @b(a="passenger")
  List<AmParkInputForm> passenger;
  @a
  @b(a="providerId")
  private Integer providerId;
  @a
  @b(a="seatInfo")
  List<CJRInputSeatInfo> seatInfo;
  @a
  @b(a="startTime")
  private String startTime;
  @a
  @b(a="ticketCount")
  private Integer ticketCount;
  @a
  @b(a="version")
  private Integer version;
  
  public String getChannel()
  {
    return this.channel;
  }
  
  public Integer getEntId()
  {
    return this.entId;
  }
  
  public Integer getPage()
  {
    return this.page;
  }
  
  public List<AmParkInputForm> getPassenger()
  {
    return this.passenger;
  }
  
  public Integer getProviderId()
  {
    return this.providerId;
  }
  
  public List<CJRInputSeatInfo> getSeatInfo()
  {
    return this.seatInfo;
  }
  
  public String getStartTime()
  {
    return this.startTime;
  }
  
  public Integer getTicketCount()
  {
    return this.ticketCount;
  }
  
  public Integer getVersion()
  {
    return this.version;
  }
  
  public void setChannel(String paramString)
  {
    this.channel = paramString;
  }
  
  public void setEntId(Integer paramInteger)
  {
    this.entId = paramInteger;
  }
  
  public void setPage(Integer paramInteger)
  {
    this.page = paramInteger;
  }
  
  public void setPassenger(List<AmParkInputForm> paramList)
  {
    this.passenger = paramList;
  }
  
  public void setProviderId(Integer paramInteger)
  {
    this.providerId = paramInteger;
  }
  
  public void setSeatInfo(List<CJRInputSeatInfo> paramList)
  {
    this.seatInfo = paramList;
  }
  
  public void setStartTime(String paramString)
  {
    this.startTime = paramString;
  }
  
  public void setTicketCount(Integer paramInteger)
  {
    this.ticketCount = paramInteger;
  }
  
  public void setVersion(Integer paramInteger)
  {
    this.version = paramInteger;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/CJRValidatePackageRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */