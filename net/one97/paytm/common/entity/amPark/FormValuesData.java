package net.one97.paytm.common.entity.amPark;

import com.google.c.a.b;
import java.io.Serializable;

public class FormValuesData
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @b(a="value")
  private String value;
  
  public String getValue()
  {
    return this.value;
  }
  
  public void setValue(String paramString)
  {
    this.value = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/FormValuesData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */