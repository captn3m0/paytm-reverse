package net.one97.paytm.common.entity.amPark;

import com.google.c.a.b;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class AmParkInputForm
  implements IJRDataModel
{
  public static final String FIELD_TYPE_CALENDAR = "calendar";
  public static final String FIELD_TYPE_CHECKBOX = "checkbox";
  public static final String FIELD_TYPE_DROPDOWN = "dropdown";
  public static final String FIELD_TYPE_RADIO = "radio";
  public static final String FIELD_TYPE_TEXTBOX = "textbox";
  public static final String FIELD_TYPE_TEXT_AREA = "textarea";
  private static final long serialVersionUID = 1L;
  @b(a="applied")
  private String applied;
  private List<FormValuesData> checkBoxListValues;
  private List<String> dropDownListValues;
  @b(a="hint")
  private String hint;
  @b(a="id")
  private String id;
  @b(a="label")
  private String label;
  @b(a="provider_seat_id")
  private String providerSeatId;
  private List<String> radioListValues;
  @b(a="regex")
  private String regex;
  @b(a="seat_id")
  private String seatId;
  @b(a="title")
  private String title;
  @b(a="type")
  private String type;
  @b(a="values")
  private Object values;
  
  public String getAppliedData()
  {
    return this.applied;
  }
  
  public List<FormValuesData> getCheckBoxListValues()
  {
    return this.checkBoxListValues;
  }
  
  public List<String> getDropDownListValues()
  {
    return this.dropDownListValues;
  }
  
  public String getHint()
  {
    return this.hint;
  }
  
  public String getId()
  {
    return this.id;
  }
  
  public String getLabel()
  {
    return this.label;
  }
  
  public String getProviderSeatId()
  {
    return this.providerSeatId;
  }
  
  public List<String> getRadioListValues()
  {
    return this.radioListValues;
  }
  
  public String getRegex()
  {
    return this.regex;
  }
  
  public String getSeatId()
  {
    return this.seatId;
  }
  
  public String getTitle()
  {
    return this.title;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public Object getValues()
  {
    return this.values;
  }
  
  public void setApplied(String paramString)
  {
    this.applied = paramString;
  }
  
  public void setCheckBoxListValues(List<FormValuesData> paramList)
  {
    this.checkBoxListValues = paramList;
  }
  
  public void setDropDownListValues(List<String> paramList)
  {
    this.dropDownListValues = paramList;
  }
  
  public void setHint(String paramString)
  {
    this.hint = paramString;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setLabel(String paramString)
  {
    this.label = paramString;
  }
  
  public void setProviderSeatId(String paramString)
  {
    this.providerSeatId = paramString;
  }
  
  public void setRadioListValues(List<String> paramList)
  {
    this.radioListValues = paramList;
  }
  
  public void setRegex(String paramString)
  {
    this.regex = paramString;
  }
  
  public void setSeatId(String paramString)
  {
    this.seatId = paramString;
  }
  
  public void setTitle(String paramString)
  {
    this.title = paramString;
  }
  
  public void setType(String paramString)
  {
    this.type = paramString;
  }
  
  public void setValues(Object paramObject)
  {
    this.values = paramObject;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/AmParkInputForm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */