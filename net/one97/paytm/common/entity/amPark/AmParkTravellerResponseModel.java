package net.one97.paytm.common.entity.amPark;

import android.text.TextUtils;
import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AmParkTravellerResponseModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="flag")
  private int flag = 0;
  @b(a="form")
  private ArrayList<ArrayList<AmParkInputForm>> form;
  @b(a="providerOid")
  private String providerOid;
  
  private void populateValuesToForm(Object paramObject, AmParkInputForm paramAmParkInputForm)
    throws JSONException
  {
    if ((paramObject == null) || (paramAmParkInputForm == null)) {}
    do
    {
      do
      {
        return;
        localObject = paramAmParkInputForm.getType();
      } while (TextUtils.isEmpty((CharSequence)localObject));
      if (((String)localObject).equalsIgnoreCase("checkbox"))
      {
        paramObject = (JSONArray)paramObject;
        localObject = new ArrayList();
        i = 0;
        while (i < ((JSONArray)paramObject).length())
        {
          FormValuesData localFormValuesData = new FormValuesData();
          localFormValuesData.setValue(((JSONArray)paramObject).getJSONObject(i).getString("value"));
          ((List)localObject).add(localFormValuesData);
          i += 1;
        }
        paramAmParkInputForm.setCheckBoxListValues((List)localObject);
        return;
      }
      if (((String)localObject).equalsIgnoreCase("dropdown"))
      {
        paramObject = (JSONArray)paramObject;
        localObject = new ArrayList();
        i = 0;
        while (i < ((JSONArray)paramObject).length())
        {
          ((List)localObject).add(((JSONArray)paramObject).getString(i));
          i += 1;
        }
        paramAmParkInputForm.setDropDownListValues((List)localObject);
        return;
      }
    } while (!((String)localObject).equalsIgnoreCase("radio"));
    paramObject = (JSONArray)paramObject;
    Object localObject = new ArrayList();
    int i = 0;
    while (i < ((JSONArray)paramObject).length())
    {
      ((List)localObject).add(((JSONArray)paramObject).getString(i));
      i += 1;
    }
    paramAmParkInputForm.setRadioListValues((List)localObject);
  }
  
  public int getFlag()
  {
    return this.flag;
  }
  
  public ArrayList<ArrayList<AmParkInputForm>> getForm()
  {
    return this.form;
  }
  
  public String getProviderOid()
  {
    return this.providerOid;
  }
  
  public void parseJsonString(String paramString)
  {
    try
    {
      paramString = new JSONObject(paramString);
      this.flag = paramString.getInt("flag");
      if (this.flag == 0)
      {
        this.form = null;
        return;
      }
      if (this.flag == 1)
      {
        if (paramString.has("providerOid")) {
          this.providerOid = paramString.getString("providerOid");
        }
        paramString = paramString.getJSONArray("form");
        this.form = new ArrayList();
        int i = 0;
        while (i < paramString.length())
        {
          JSONArray localJSONArray = paramString.getJSONArray(i);
          ArrayList localArrayList = new ArrayList();
          int j = 0;
          while (j < localJSONArray.length())
          {
            JSONObject localJSONObject = localJSONArray.getJSONObject(j);
            AmParkInputForm localAmParkInputForm = new AmParkInputForm();
            if (localJSONObject.has("title")) {
              localAmParkInputForm.setTitle(localJSONObject.getString("title"));
            }
            if (localJSONObject.has("type")) {
              localAmParkInputForm.setType(localJSONObject.getString("type"));
            }
            if (localJSONObject.has("applied")) {
              localAmParkInputForm.setApplied(localJSONObject.getString("applied"));
            }
            if (localJSONObject.has("id")) {
              localAmParkInputForm.setId(localJSONObject.getString("id"));
            }
            if (localJSONObject.has("regex")) {
              localAmParkInputForm.setRegex(localJSONObject.getString("regex"));
            }
            if (localJSONObject.has("hint")) {
              localAmParkInputForm.setHint(localJSONObject.getString("hint"));
            }
            if (localJSONObject.has("label")) {
              localAmParkInputForm.setLabel(localJSONObject.getString("label"));
            }
            if (localJSONObject.has("values")) {
              populateValuesToForm(localJSONObject.get("values"), localAmParkInputForm);
            }
            if (localJSONObject.has("seat_id")) {
              localAmParkInputForm.setSeatId(localJSONObject.getString("seat_id"));
            }
            if (localJSONObject.has("provider_seat_id")) {
              localAmParkInputForm.setProviderSeatId(localJSONObject.getString("provider_seat_id"));
            }
            localArrayList.add(localAmParkInputForm);
            j += 1;
          }
          this.form.add(localArrayList);
          i += 1;
        }
      }
      return;
    }
    catch (JSONException paramString)
    {
      paramString.printStackTrace();
    }
  }
  
  public void setFlag(int paramInt)
  {
    this.flag = paramInt;
  }
  
  public void setForm(ArrayList<ArrayList<AmParkInputForm>> paramArrayList)
  {
    this.form = paramArrayList;
  }
  
  public void setProviderOid(String paramString)
  {
    this.providerOid = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/AmParkTravellerResponseModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */