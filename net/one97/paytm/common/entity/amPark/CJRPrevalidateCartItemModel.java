package net.one97.paytm.common.entity.amPark;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPrevalidateCartItemModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="configuration")
  private CJRConfigurationModel configuration;
  @b(a="meta_data")
  private CJRInputMetaDataModel meta_data;
  @b(a="product_id")
  private String product_id;
  @b(a="promocode")
  private String promocode;
  @b(a="promotext")
  private String promotext;
  @b(a="qty")
  private String qty;
  
  public CJRConfigurationModel getConfiguration()
  {
    return this.configuration;
  }
  
  public CJRInputMetaDataModel getMetadata()
  {
    return this.meta_data;
  }
  
  public String getProduct_id()
  {
    return this.product_id;
  }
  
  public String getPromocode()
  {
    return this.promocode;
  }
  
  public String getPromotext()
  {
    return this.promotext;
  }
  
  public String getQty()
  {
    return this.qty;
  }
  
  public void setConfiguration(CJRConfigurationModel paramCJRConfigurationModel)
  {
    this.configuration = paramCJRConfigurationModel;
  }
  
  public void setMetadata(CJRInputMetaDataModel paramCJRInputMetaDataModel)
  {
    this.meta_data = paramCJRInputMetaDataModel;
  }
  
  public void setProduct_id(String paramString)
  {
    this.product_id = paramString;
  }
  
  public void setPromocode(String paramString)
  {
    this.promocode = paramString;
  }
  
  public void setPromotext(String paramString)
  {
    this.promotext = paramString;
  }
  
  public void setQty(String paramString)
  {
    this.qty = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/CJRPrevalidateCartItemModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */