package net.one97.paytm.common.entity.amPark;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAmParkStoreFrontModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="homepage_layout")
  private ArrayList<CJRAmParkStoreFrontDetailModel> mStoreFrontItems = new ArrayList();
  
  public ArrayList<CJRAmParkStoreFrontDetailModel> getmStoreFrontItems()
  {
    return this.mStoreFrontItems;
  }
  
  public void setmStoreFrontItems(ArrayList<CJRAmParkStoreFrontDetailModel> paramArrayList)
  {
    this.mStoreFrontItems = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/CJRAmParkStoreFrontModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */