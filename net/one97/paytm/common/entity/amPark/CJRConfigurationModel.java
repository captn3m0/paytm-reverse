package net.one97.paytm.common.entity.amPark;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRConfigurationModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="conv_fee")
  private String conv_fee;
  @b(a="price")
  private String price;
  
  public String getPrice()
  {
    return this.price;
  }
  
  public String getconv_fee()
  {
    return this.conv_fee;
  }
  
  public void setPrice(String paramString)
  {
    this.price = paramString;
  }
  
  public void setconv_fee(String paramString)
  {
    this.conv_fee = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/CJRConfigurationModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */