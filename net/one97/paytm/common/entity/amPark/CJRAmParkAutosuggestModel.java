package net.one97.paytm.common.entity.amPark;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAmParkAutosuggestModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="featured_park")
  private ArrayList<CJRAmParkAutosuggestEventItem> featuredParkList = new ArrayList();
  @b(a="theme_park")
  private ArrayList<CJRAmParkAutosuggestEventItem> themeParkList = new ArrayList();
  @b(a="water_park")
  private ArrayList<CJRAmParkAutosuggestEventItem> waterParkList = new ArrayList();
  
  public ArrayList<CJRAmParkAutosuggestEventItem> getFeaturedParkList()
  {
    return this.featuredParkList;
  }
  
  public ArrayList<CJRAmParkAutosuggestEventItem> getThemeParkList()
  {
    return this.themeParkList;
  }
  
  public ArrayList<CJRAmParkAutosuggestEventItem> getWaterParkList()
  {
    return this.waterParkList;
  }
  
  public void setFeaturedParkList(ArrayList<CJRAmParkAutosuggestEventItem> paramArrayList)
  {
    this.featuredParkList = paramArrayList;
  }
  
  public void setThemeParkList(ArrayList<CJRAmParkAutosuggestEventItem> paramArrayList)
  {
    this.themeParkList = paramArrayList;
  }
  
  public void setWaterParkList(ArrayList<CJRAmParkAutosuggestEventItem> paramArrayList)
  {
    this.waterParkList = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/CJRAmParkAutosuggestModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */