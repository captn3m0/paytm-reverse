package net.one97.paytm.common.entity.amPark;

import com.google.c.a.b;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAmParkDateTimeModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="packages")
  private List<CJRParkPackageDetailsModel> mPackageDetailsList = new ArrayList();
  @b(a="parkDates")
  private List<String> mParkDateList = new ArrayList();
  @b(a="time")
  private Time mTime;
  
  public List<CJRParkPackageDetailsModel> getmPackageDetailsList()
  {
    return this.mPackageDetailsList;
  }
  
  public List<String> getmParkDateList()
  {
    return this.mParkDateList;
  }
  
  public Time getmTime()
  {
    return this.mTime;
  }
  
  public void setmPackageDetailsList(List<CJRParkPackageDetailsModel> paramList)
  {
    this.mPackageDetailsList = paramList;
  }
  
  public void setmParkDateList(List<String> paramList)
  {
    this.mParkDateList = paramList;
  }
  
  public void setmTime(Time paramTime)
  {
    this.mTime = paramTime;
  }
  
  public class Time
    implements Serializable
  {
    private static final long serialVersionUID = 1L;
    @b(a="start")
    private String mStart;
    @b(a="to")
    private String mTo;
    
    public Time() {}
    
    public boolean equals(Object paramObject)
    {
      boolean bool2 = false;
      boolean bool1 = bool2;
      if ((paramObject instanceof Time))
      {
        paramObject = (Time)paramObject;
        bool1 = bool2;
        if (this.mStart.equals(((Time)paramObject).mStart))
        {
          bool1 = bool2;
          if (this.mTo.equals(((Time)paramObject).mTo)) {
            bool1 = true;
          }
        }
      }
      return bool1;
    }
    
    public String getmStart()
    {
      return this.mStart;
    }
    
    public String getmTo()
    {
      return this.mTo;
    }
    
    public void setmStart(String paramString)
    {
      this.mStart = paramString;
    }
    
    public void setmTo(String paramString)
    {
      this.mTo = paramString;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/CJRAmParkDateTimeModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */