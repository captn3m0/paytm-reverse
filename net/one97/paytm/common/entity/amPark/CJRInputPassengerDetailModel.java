package net.one97.paytm.common.entity.amPark;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRInputPassengerDetailModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="applied")
  private String applied;
  @b(a="error")
  private String error;
  @b(a="id")
  private String id;
  @b(a="regex")
  private String regex;
  @b(a="title")
  private String title;
  @b(a="type")
  private String type;
  
  public String getApplied()
  {
    return this.applied;
  }
  
  public String getError()
  {
    return this.error;
  }
  
  public String getId()
  {
    return this.id;
  }
  
  public ArrayList<CJRInputPassengerDetailModel> getPassengerDetail()
  {
    ArrayList localArrayList = new ArrayList();
    CJRInputPassengerDetailModel localCJRInputPassengerDetailModel = new CJRInputPassengerDetailModel();
    localCJRInputPassengerDetailModel.setTitle("Full Name");
    localCJRInputPassengerDetailModel.setType("textbox");
    localCJRInputPassengerDetailModel.setApplied("Krishna Kumar");
    localCJRInputPassengerDetailModel.setId("FullName1");
    localCJRInputPassengerDetailModel.setError("");
    localCJRInputPassengerDetailModel.setRegex("");
    localArrayList.add(localCJRInputPassengerDetailModel);
    localCJRInputPassengerDetailModel = new CJRInputPassengerDetailModel();
    localCJRInputPassengerDetailModel.setTitle("Email Id");
    localCJRInputPassengerDetailModel.setType("textbox");
    localCJRInputPassengerDetailModel.setApplied("kriss.nit@gmail.com");
    localCJRInputPassengerDetailModel.setId("EmailId1");
    localCJRInputPassengerDetailModel.setError("");
    localCJRInputPassengerDetailModel.setRegex("^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$");
    localArrayList.add(localCJRInputPassengerDetailModel);
    localCJRInputPassengerDetailModel = new CJRInputPassengerDetailModel();
    localCJRInputPassengerDetailModel.setTitle("Mobile No");
    localCJRInputPassengerDetailModel.setType("textbox");
    localCJRInputPassengerDetailModel.setApplied("9632172352");
    localCJRInputPassengerDetailModel.setId("MobileNo1");
    localCJRInputPassengerDetailModel.setError("");
    localCJRInputPassengerDetailModel.setRegex("^[7-9][0-9]{9}$");
    localArrayList.add(localCJRInputPassengerDetailModel);
    return localArrayList;
  }
  
  public String getRegex()
  {
    return this.regex;
  }
  
  public String getTitle()
  {
    return this.title;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public void setApplied(String paramString)
  {
    this.applied = paramString;
  }
  
  public void setError(String paramString)
  {
    this.error = paramString;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setRegex(String paramString)
  {
    this.regex = paramString;
  }
  
  public void setTitle(String paramString)
  {
    this.title = paramString;
  }
  
  public void setType(String paramString)
  {
    this.type = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/CJRInputPassengerDetailModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */