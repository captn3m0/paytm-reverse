package net.one97.paytm.common.entity.amPark;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAmParkCustomInfo
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="offer_color")
  private String mOfferColor;
  @b(a="offer_text")
  private String mOfferText;
  
  public String getOfferColor()
  {
    return this.mOfferColor;
  }
  
  public String getOfferText()
  {
    return this.mOfferText;
  }
  
  public void setOfferColor(String paramString)
  {
    this.mOfferColor = paramString;
  }
  
  public void setOfferText(String paramString)
  {
    this.mOfferText = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/CJRAmParkCustomInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */