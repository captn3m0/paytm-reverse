package net.one97.paytm.common.entity.amPark;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAmParkModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="activeCategory")
  private String mActiveCategory;
  @b(a="categories")
  private List<String> mCategoryList = new ArrayList();
  @b(a="ent_list")
  private ArrayList<CJRAmParkDetailModel> mParkDetailModelList = new ArrayList();
  
  public String getmActiveCategory()
  {
    return this.mActiveCategory;
  }
  
  public List<String> getmCategoryList()
  {
    return this.mCategoryList;
  }
  
  public ArrayList<CJRAmParkDetailModel> getmParkDetailModelList()
  {
    return this.mParkDetailModelList;
  }
  
  public void setmActiveCategory(String paramString)
  {
    this.mActiveCategory = paramString;
  }
  
  public void setmCategoryList(List<String> paramList)
  {
    this.mCategoryList = paramList;
  }
  
  public void setmParkDetailModelList(ArrayList<CJRAmParkDetailModel> paramArrayList)
  {
    this.mParkDetailModelList = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/CJRAmParkModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */