package net.one97.paytm.common.entity.amPark;

import com.google.c.a.b;
import java.io.Serializable;
import java.util.List;

public class CJRParkPackageDetailsModel
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @b(a="custom_info")
  private String mParkCustomInfo;
  @b(a="description")
  private String mParkPackageDescription;
  @b(a="type")
  private String mParkPackageType;
  @b(a="prices")
  private List<CJRSeatDetailsModel> mParkSeatDetails;
  
  public String getmParkCustomInfo()
  {
    return this.mParkCustomInfo;
  }
  
  public String getmParkPackageDescription()
  {
    return this.mParkPackageDescription;
  }
  
  public String getmParkPackageType()
  {
    return this.mParkPackageType;
  }
  
  public List<CJRSeatDetailsModel> getmParkSeatDetails()
  {
    return this.mParkSeatDetails;
  }
  
  public void setmParkCustomInfo(String paramString)
  {
    this.mParkCustomInfo = paramString;
  }
  
  public void setmParkPackageDescription(String paramString)
  {
    this.mParkPackageDescription = paramString;
  }
  
  public void setmParkPackageType(String paramString)
  {
    this.mParkPackageType = paramString;
  }
  
  public void setmParkSeatDetails(List<CJRSeatDetailsModel> paramList)
  {
    this.mParkSeatDetails = paramList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/CJRParkPackageDetailsModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */