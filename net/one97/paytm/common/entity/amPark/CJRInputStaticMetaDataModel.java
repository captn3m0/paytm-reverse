package net.one97.paytm.common.entity.amPark;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRInputStaticMetaDataModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="address")
  private String address;
  @b(a="addressName")
  private String addressName;
  @b(a="bookingId")
  private String bookingId;
  @b(a="category")
  private String category;
  @b(a="citySearched")
  private String citySearched;
  @b(a="contact")
  private String contact;
  @b(a="convFee")
  private String convFee;
  @b(a="email")
  private String email;
  @b(a="endTime")
  private String endTime;
  @b(a="entityCity")
  private String entityCity;
  @b(a="entityId")
  private String entityId;
  @b(a="entityName")
  private String entityName;
  @b(a="entityType")
  private String entityType;
  @b(a="imageUrl")
  private String imageUrl;
  @b(a="krishiKalyanCess")
  private String krishiKalyanCess;
  @b(a="latitude")
  private String latitude;
  @b(a="longitude")
  private String longitude;
  @b(a="otherTax")
  private String otherTax;
  @b(a="passenger")
  private CJRSataticPassengerModel passenger;
  @b(a="pgCharges")
  private String pgCharges;
  @b(a="pincode")
  private String pincode;
  @b(a="productId")
  private String productId;
  @b(a="providerId")
  private String providerId;
  @b(a="providerName")
  private String providerName;
  @b(a="seatInfo")
  private ArrayList<CJRInputSeatInfo> seatInfo = new ArrayList();
  @b(a="serviceTax")
  private String serviceTax;
  @b(a="startTime")
  private String startTime;
  @b(a="swachBharatCess")
  private String swachBharatCess;
  @b(a="ticketCount")
  private String ticketCount;
  @b(a="totalCommision")
  private String totalCommision;
  @b(a="totalConvFee")
  private String totalConvFee;
  @b(a="totalKrishiKalyanCess")
  private String totalKrishiKalyanCess;
  @b(a="totalPgCharges")
  private String totalPgCharges;
  @b(a="totalServiceTax")
  private String totalServiceTax;
  @b(a="totalSwachBharatCess")
  private String totalSwachBharatCess;
  @b(a="totalTicketPrice")
  private String totalTicketPrice;
  @b(a="vertical")
  private String vertical;
  
  public String getAddress()
  {
    return this.address;
  }
  
  public String getAddressName()
  {
    return this.addressName;
  }
  
  public String getBookingId()
  {
    return this.bookingId;
  }
  
  public String getCategory()
  {
    return this.category;
  }
  
  public String getCitySearched()
  {
    return this.citySearched;
  }
  
  public String getContact()
  {
    return this.contact;
  }
  
  public String getConvFee()
  {
    return this.convFee;
  }
  
  public String getEmail()
  {
    return this.email;
  }
  
  public String getEndTime()
  {
    return this.endTime;
  }
  
  public String getEntityCity()
  {
    return this.entityCity;
  }
  
  public String getEntityId()
  {
    return this.entityId;
  }
  
  public String getEntityName()
  {
    return this.entityName;
  }
  
  public String getEntityType()
  {
    return this.entityType;
  }
  
  public String getImageUrl()
  {
    return this.imageUrl;
  }
  
  public String getKrishiKalyanCess()
  {
    return this.krishiKalyanCess;
  }
  
  public String getLatitude()
  {
    return this.latitude;
  }
  
  public String getLongitude()
  {
    return this.longitude;
  }
  
  public String getOtherTax()
  {
    return this.otherTax;
  }
  
  public CJRSataticPassengerModel getPassenger()
  {
    return this.passenger;
  }
  
  public String getPgCharges()
  {
    return this.pgCharges;
  }
  
  public String getPincode()
  {
    return this.pincode;
  }
  
  public String getProductId()
  {
    return this.productId;
  }
  
  public String getProviderId()
  {
    return this.providerId;
  }
  
  public String getProviderName()
  {
    return this.providerName;
  }
  
  public String getServiceTax()
  {
    return this.serviceTax;
  }
  
  public String getStartTime()
  {
    return this.startTime;
  }
  
  public String getSwachBharatCess()
  {
    return this.swachBharatCess;
  }
  
  public String getTicketCount()
  {
    return this.ticketCount;
  }
  
  public String getTotalCommision()
  {
    return this.totalCommision;
  }
  
  public String getTotalConvFee()
  {
    return this.totalConvFee;
  }
  
  public String getTotalKrishiKalyanCess()
  {
    return this.totalKrishiKalyanCess;
  }
  
  public String getTotalPgCharges()
  {
    return this.totalPgCharges;
  }
  
  public String getTotalServiceTax()
  {
    return this.totalServiceTax;
  }
  
  public String getTotalSwachBharatCess()
  {
    return this.totalSwachBharatCess;
  }
  
  public String getTotalTicketPrice()
  {
    return this.totalTicketPrice;
  }
  
  public String getVertical()
  {
    return this.vertical;
  }
  
  public ArrayList<CJRInputSeatInfo> getseatInfo()
  {
    return this.seatInfo;
  }
  
  public void setAddress(String paramString)
  {
    this.address = paramString;
  }
  
  public void setAddressName(String paramString)
  {
    this.addressName = paramString;
  }
  
  public void setBookingId(String paramString)
  {
    this.bookingId = paramString;
  }
  
  public void setCategory(String paramString)
  {
    this.category = paramString;
  }
  
  public void setCitySearched(String paramString)
  {
    this.citySearched = paramString;
  }
  
  public void setContact(String paramString)
  {
    this.contact = paramString;
  }
  
  public void setConvFee(String paramString)
  {
    this.convFee = paramString;
  }
  
  public void setEmail(String paramString)
  {
    this.email = paramString;
  }
  
  public void setEndTime(String paramString)
  {
    this.endTime = paramString;
  }
  
  public void setEntityCity(String paramString)
  {
    this.entityCity = paramString;
  }
  
  public void setEntityId(String paramString)
  {
    this.entityId = paramString;
  }
  
  public void setEntityName(String paramString)
  {
    this.entityName = paramString;
  }
  
  public void setEntityType(String paramString)
  {
    this.entityType = paramString;
  }
  
  public void setImageUrl(String paramString)
  {
    this.imageUrl = paramString;
  }
  
  public void setKrishiKalyanCess(String paramString)
  {
    this.krishiKalyanCess = paramString;
  }
  
  public void setLatitude(String paramString)
  {
    this.latitude = paramString;
  }
  
  public void setLongitude(String paramString)
  {
    this.longitude = paramString;
  }
  
  public void setOtherTax(String paramString)
  {
    this.otherTax = paramString;
  }
  
  public void setPassenger(CJRSataticPassengerModel paramCJRSataticPassengerModel)
  {
    this.passenger = paramCJRSataticPassengerModel;
  }
  
  public void setPgCharges(String paramString)
  {
    this.pgCharges = paramString;
  }
  
  public void setPincode(String paramString)
  {
    this.pincode = paramString;
  }
  
  public void setProductId(String paramString)
  {
    this.productId = paramString;
  }
  
  public void setProviderId(String paramString)
  {
    this.providerId = paramString;
  }
  
  public void setProviderName(String paramString)
  {
    this.providerName = paramString;
  }
  
  public void setServiceTax(String paramString)
  {
    this.serviceTax = paramString;
  }
  
  public void setStartTime(String paramString)
  {
    this.startTime = paramString;
  }
  
  public void setSwachBharatCess(String paramString)
  {
    this.swachBharatCess = paramString;
  }
  
  public void setTicketCount(String paramString)
  {
    this.ticketCount = paramString;
  }
  
  public void setTotalCommision(String paramString)
  {
    this.totalCommision = paramString;
  }
  
  public void setTotalConvFee(String paramString)
  {
    this.totalConvFee = paramString;
  }
  
  public void setTotalKrishiKalyanCess(String paramString)
  {
    this.totalKrishiKalyanCess = paramString;
  }
  
  public void setTotalPgCharges(String paramString)
  {
    this.totalPgCharges = paramString;
  }
  
  public void setTotalServiceTax(String paramString)
  {
    this.totalServiceTax = paramString;
  }
  
  public void setTotalSwachBharatCess(String paramString)
  {
    this.totalSwachBharatCess = paramString;
  }
  
  public void setTotalTicketPrice(String paramString)
  {
    this.totalTicketPrice = paramString;
  }
  
  public void setVertical(String paramString)
  {
    this.vertical = paramString;
  }
  
  public void setseatInfo(ArrayList<CJRInputSeatInfo> paramArrayList)
  {
    this.seatInfo = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/CJRInputStaticMetaDataModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */