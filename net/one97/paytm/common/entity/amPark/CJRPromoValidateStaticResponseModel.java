package net.one97.paytm.common.entity.amPark;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPromoValidateStaticResponseModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="cart")
  private CJRPrevalidateStaticInputModel cart;
  @b(a="status")
  private CJRAmParkPromoStatus status;
  
  public CJRPrevalidateStaticInputModel getCart()
  {
    return this.cart;
  }
  
  public CJRAmParkPromoStatus getStatus()
  {
    return this.status;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/CJRPromoValidateStaticResponseModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */