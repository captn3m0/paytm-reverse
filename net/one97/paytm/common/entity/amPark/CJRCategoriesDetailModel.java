package net.one97.paytm.common.entity.amPark;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRCategoriesDetailModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="category")
  private String mCategory;
  @b(a="color")
  private String mColor;
  @b(a="id")
  private String mId;
  
  public String getCategory()
  {
    return this.mCategory;
  }
  
  public String getColor()
  {
    return this.mColor;
  }
  
  public String getId()
  {
    return this.mId;
  }
  
  public void setCategory(String paramString)
  {
    this.mCategory = paramString;
  }
  
  public void setColor(String paramString)
  {
    this.mColor = paramString;
  }
  
  public void setId(String paramString)
  {
    this.mId = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/CJRCategoriesDetailModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */