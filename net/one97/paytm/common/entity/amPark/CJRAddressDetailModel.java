package net.one97.paytm.common.entity.amPark;

import com.google.c.a.b;
import java.util.Iterator;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAddressDetailModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="address")
  private String mAddress;
  @b(a="address_name")
  private String mAddressName;
  @b(a="contact")
  private String mContact;
  @b(a="description")
  private String mDescription;
  @b(a="email")
  private String mEmail;
  @b(a="latitude")
  private Double mLatitude;
  @b(a="longitude")
  private Double mLongitude;
  @b(a="name")
  private String mName;
  @b(a="pincode")
  private String mPincode;
  private boolean mSoldOut;
  
  public boolean extractVenueSoldOutInfo(List<CJRAmParkDateTimeModel> paramList)
  {
    if ((paramList == null) || (paramList.size() <= 0)) {}
    for (;;)
    {
      return true;
      paramList = paramList.iterator();
      while (paramList.hasNext()) {
        ((CJRAmParkDateTimeModel)paramList.next()).getmPackageDetailsList();
      }
    }
  }
  
  public String getAddress()
  {
    return this.mAddress;
  }
  
  public String getAddressName()
  {
    return this.mAddressName;
  }
  
  public String getDescription()
  {
    return this.mDescription;
  }
  
  public String getEmail()
  {
    return this.mEmail;
  }
  
  public Double getLatitude()
  {
    return this.mLatitude;
  }
  
  public Double getLongitude()
  {
    return this.mLongitude;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getPincode()
  {
    return this.mPincode;
  }
  
  public boolean getSoldOutInfo()
  {
    return this.mSoldOut;
  }
  
  public String getmContact()
  {
    return this.mContact;
  }
  
  public void setAddress(String paramString)
  {
    this.mAddress = paramString;
  }
  
  public void setAddressName(String paramString)
  {
    this.mAddressName = paramString;
  }
  
  public void setDescription(String paramString)
  {
    this.mDescription = paramString;
  }
  
  public void setEmail(String paramString)
  {
    this.mEmail = paramString;
  }
  
  public void setLatitude(Double paramDouble)
  {
    this.mLatitude = paramDouble;
  }
  
  public void setLongitude(Double paramDouble)
  {
    this.mLongitude = paramDouble;
  }
  
  public void setName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setPincode(String paramString)
  {
    this.mPincode = paramString;
  }
  
  public void setSoldOutInfo(boolean paramBoolean)
  {
    this.mSoldOut = paramBoolean;
  }
  
  public void setmContact(String paramString)
  {
    this.mContact = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/CJRAddressDetailModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */