package net.one97.paytm.common.entity.amPark;

import com.google.c.a.b;
import java.io.Serializable;

public class CJRParkRidesDetailModel
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @b(a="ride_description")
  private String mRideDescription;
  @b(a="ride_image")
  private String mRideImage;
  @b(a="ride_image_app")
  private String mRideImageApp;
  @b(a="ride_name")
  private String mRideName;
  
  public String getmRideDescription()
  {
    return this.mRideDescription;
  }
  
  public String getmRideImage()
  {
    return this.mRideImage;
  }
  
  public String getmRideImageApp()
  {
    return this.mRideImageApp;
  }
  
  public String getmRideName()
  {
    return this.mRideName;
  }
  
  public void setmRideDescription(String paramString)
  {
    this.mRideDescription = paramString;
  }
  
  public void setmRideImage(String paramString)
  {
    this.mRideImage = paramString;
  }
  
  public void setmRideImageApp(String paramString)
  {
    this.mRideImageApp = paramString;
  }
  
  public void setmRideName(String paramString)
  {
    this.mRideName = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/amPark/CJRParkRidesDetailModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */