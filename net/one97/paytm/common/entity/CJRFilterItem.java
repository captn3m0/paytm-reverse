package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class CJRFilterItem
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  private boolean isFilterChart;
  private boolean isToggleChecked;
  @b(a="display_value")
  private String mDisplayValue;
  @b(a="applied")
  private ArrayList<CJRFilterValue> mFilterApplied = new ArrayList();
  @b(a="applied_range")
  private CJRFilterValue mFilterAppliedRange = new CJRFilterValue();
  private String mFilterChartlabel;
  @b(a="filter_param")
  private String mFilterParam;
  private String mFilterUrl;
  @b(a="values")
  private ArrayList<CJRFilterValue> mFilterValues = new ArrayList();
  @b(a="selected")
  private String mSelected;
  private ArrayList<String> mSelectedCounts = new ArrayList();
  private ArrayList<String> mSelectedValues = new ArrayList();
  @b(a="title")
  private String mTitle;
  @b(a="type")
  private String mType;
  private HashMap<CJRFilterValue, Boolean> mValuesMap = new HashMap();
  
  public void createValueMap()
  {
    Iterator localIterator = this.mFilterValues.iterator();
    while (localIterator.hasNext())
    {
      CJRFilterValue localCJRFilterValue = (CJRFilterValue)localIterator.next();
      this.mValuesMap.put(localCJRFilterValue, Boolean.valueOf(false));
    }
  }
  
  public boolean equals(Object paramObject)
  {
    try
    {
      if (this.mTitle.equalsIgnoreCase(((CJRFilterItem)paramObject).mTitle))
      {
        boolean bool = this.mType.equalsIgnoreCase(((CJRFilterItem)paramObject).mType);
        if (bool) {
          return true;
        }
      }
      return false;
    }
    catch (Exception paramObject) {}
    return false;
  }
  
  public String getDisplayValue()
  {
    return this.mDisplayValue;
  }
  
  public ArrayList<CJRFilterValue> getFilterApplied()
  {
    return this.mFilterApplied;
  }
  
  public CJRFilterValue getFilterAppliedRange()
  {
    return this.mFilterAppliedRange;
  }
  
  public String getFilterParam()
  {
    return this.mFilterParam;
  }
  
  public String getFilterUrl()
  {
    return this.mFilterUrl;
  }
  
  public CJRFilterValue getFilterValueForID(int paramInt)
  {
    CJRFilterValue localCJRFilterValue = null;
    if (this.mFilterValues != null) {
      localCJRFilterValue = (CJRFilterValue)this.mFilterValues.get(paramInt);
    }
    return localCJRFilterValue;
  }
  
  public CJRFilterValue getFilterValueForName(String paramString)
  {
    Object localObject2 = null;
    Iterator localIterator = this.mFilterValues.iterator();
    Object localObject1;
    do
    {
      localObject1 = localObject2;
      if (!localIterator.hasNext()) {
        break;
      }
      localObject1 = (CJRFilterValue)localIterator.next();
    } while ((((CJRFilterValue)localObject1).getName() == null) || (!((CJRFilterValue)localObject1).getName().equals(paramString)));
    return (CJRFilterValue)localObject1;
  }
  
  public ArrayList<CJRFilterValue> getFilterValues()
  {
    return this.mFilterValues;
  }
  
  public ArrayList<String> getKeyForCount(boolean paramBoolean)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mValuesMap.keySet().iterator();
    while (localIterator.hasNext())
    {
      CJRFilterValue localCJRFilterValue = (CJRFilterValue)localIterator.next();
      if (((Boolean)this.mValuesMap.get(localCJRFilterValue)).booleanValue()) {
        localArrayList.add(localCJRFilterValue.getCount());
      }
    }
    return localArrayList;
  }
  
  public ArrayList<String> getKeyForValue(boolean paramBoolean)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mValuesMap.keySet().iterator();
    while (localIterator.hasNext())
    {
      CJRFilterValue localCJRFilterValue = (CJRFilterValue)localIterator.next();
      if (((Boolean)this.mValuesMap.get(localCJRFilterValue)).booleanValue()) {
        localArrayList.add(localCJRFilterValue.getName());
      }
    }
    return localArrayList;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getSelected()
  {
    return this.mSelected;
  }
  
  public ArrayList<String> getSelectedCounts()
  {
    return this.mSelectedCounts;
  }
  
  public ArrayList<String> getSelectedValues()
  {
    return this.mSelectedValues;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
  
  public String getType()
  {
    return this.mType;
  }
  
  public HashMap<CJRFilterValue, Boolean> getValuesMap()
  {
    return this.mValuesMap;
  }
  
  public String getmFilterChartlabel()
  {
    return this.mFilterChartlabel;
  }
  
  public boolean isFilterChart()
  {
    return this.isFilterChart;
  }
  
  public boolean isToggleChecked()
  {
    return this.isToggleChecked;
  }
  
  public void setCounts(ArrayList<String> paramArrayList)
  {
    this.mSelectedCounts = paramArrayList;
  }
  
  public void setFilterApplied(ArrayList<CJRFilterValue> paramArrayList)
  {
    this.mFilterApplied = paramArrayList;
  }
  
  public void setFilterAppliedRange(CJRFilterValue paramCJRFilterValue)
  {
    this.mFilterAppliedRange = paramCJRFilterValue;
  }
  
  public void setFilterChart(boolean paramBoolean)
  {
    this.isFilterChart = paramBoolean;
  }
  
  public void setFilterParam(String paramString)
  {
    this.mFilterParam = paramString;
  }
  
  public void setFilterValues(ArrayList<CJRFilterValue> paramArrayList)
  {
    this.mFilterValues = paramArrayList;
  }
  
  public void setTitle(String paramString)
  {
    this.mTitle = paramString;
  }
  
  public void setToggleChecked(boolean paramBoolean)
  {
    this.isToggleChecked = paramBoolean;
  }
  
  public void setType(String paramString)
  {
    this.mType = paramString;
  }
  
  public void setValues(ArrayList<String> paramArrayList)
  {
    this.mSelectedValues = paramArrayList;
  }
  
  public void setValuesMap(HashMap<CJRFilterValue, Boolean> paramHashMap)
  {
    this.mValuesMap = paramHashMap;
  }
  
  public void setmFilterChartlabel(String paramString)
  {
    this.mFilterChartlabel = paramString;
  }
  
  public void setmFilterUrl(String paramString)
  {
    this.mFilterUrl = paramString;
  }
  
  public void updateValuesMap(CJRFilterValue paramCJRFilterValue, boolean paramBoolean)
  {
    this.mValuesMap.put(paramCJRFilterValue, Boolean.valueOf(paramBoolean));
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRFilterItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */