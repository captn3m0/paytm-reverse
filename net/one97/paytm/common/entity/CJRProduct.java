package net.one97.paytm.common.entity;

import com.google.c.a.b;

public abstract class CJRProduct
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="brand")
  protected String mBrand;
  @b(a="name")
  protected String mName;
  @b(a="product_type")
  protected String mProductType;
  
  public abstract String getBrand();
  
  public abstract String getProductType();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRProduct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */