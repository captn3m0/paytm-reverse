package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRStatusError
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="code")
  private int mCode;
  @b(a="message")
  private CJRError mMessage;
  @b(a="result")
  private String mResult;
  
  public int getmCode()
  {
    return this.mCode;
  }
  
  public CJRError getmMessage()
  {
    return this.mMessage;
  }
  
  public String getmResult()
  {
    return this.mResult;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRStatusError.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */