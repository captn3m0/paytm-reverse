package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRProductDetailsModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="price")
  private String mAmount;
  @b(a="description")
  private String mDescription;
  @b(a="image_url")
  private String mImageUrl;
  @b(a="name")
  private String mMerchantName;
  @b(a="product_url")
  private String mProductURL;
  @b(a="id")
  private int miId;
  
  public String getAmount()
  {
    return this.mAmount;
  }
  
  public String getDescription()
  {
    return this.mDescription;
  }
  
  public int getId()
  {
    return this.miId;
  }
  
  public String getImageURL()
  {
    return this.mImageUrl;
  }
  
  public String getMerchantName()
  {
    return this.mMerchantName;
  }
  
  public String getProductURL()
  {
    return this.mProductURL;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRProductDetailsModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */