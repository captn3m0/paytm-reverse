package net.one97.paytm.common.entity.inmobi;

import com.google.c.a.b;
import java.io.Serializable;

public class CJRInMobiAd
  implements Serializable
{
  boolean isClickedImpressionTracked;
  boolean isImpressionTracked;
  @b(a="contextCode")
  private String mContextCode;
  @b(a="namespace")
  private String mNamespace;
  @b(a="pubContent")
  private CJRPubContent mPubContent;
  
  public boolean equals(Object paramObject)
  {
    try
    {
      boolean bool = this.mPubContent.equals(((CJRInMobiAd)paramObject).getPubContent());
      return bool;
    }
    catch (Exception paramObject) {}
    return false;
  }
  
  public String getContextCode()
  {
    return this.mContextCode;
  }
  
  public String getNamespace()
  {
    return this.mNamespace;
  }
  
  public CJRPubContent getPubContent()
  {
    return this.mPubContent;
  }
  
  public boolean isClickedImpressionTracked()
  {
    return this.isClickedImpressionTracked;
  }
  
  public boolean isImpressionTracked()
  {
    return this.isImpressionTracked;
  }
  
  public void setClickedImpressionTracked(boolean paramBoolean)
  {
    this.isClickedImpressionTracked = paramBoolean;
  }
  
  public void setImpressionTracked(boolean paramBoolean)
  {
    this.isImpressionTracked = paramBoolean;
  }
  
  public void setPubContent(CJRPubContent paramCJRPubContent)
  {
    this.mPubContent = paramCJRPubContent;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/inmobi/CJRInMobiAd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */