package net.one97.paytm.common.entity.inmobi;

import com.google.c.a.b;
import java.io.Serializable;
import java.util.ArrayList;

public class CJRInMobiTotalAds
  implements Serializable
{
  @b(a="ads")
  ArrayList<CJRInMobiAd> mAds;
  @b(a="requestId")
  String mRequestId;
  
  public void addNewItems(ArrayList<CJRInMobiAd> paramArrayList)
  {
    if (this.mAds == null)
    {
      this.mAds = paramArrayList;
      return;
    }
    this.mAds.addAll(paramArrayList);
  }
  
  public ArrayList<CJRInMobiAd> getAds()
  {
    return this.mAds;
  }
  
  public String getRequestId()
  {
    return this.mRequestId;
  }
  
  public void setAds(ArrayList<CJRInMobiAd> paramArrayList)
  {
    this.mAds = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/inmobi/CJRInMobiTotalAds.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */