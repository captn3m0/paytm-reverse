package net.one97.paytm.common.entity.inmobi;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTPData
  implements IJRDataModel
{
  boolean isClickedImpressionTracked;
  boolean isImpressionTracked;
  @b(a="abClick")
  public String mAbClick;
  @b(a="abImpression")
  public String mAbImpression;
  @b(a="im_contextCode")
  public String mContextCode;
  @b(a="im_namespace")
  public String mNamespace;
  @b(a="im_pubContent")
  public String mPubContent;
  
  public String getAbClick()
  {
    return this.mAbClick;
  }
  
  public String getAbImpression()
  {
    return this.mAbImpression;
  }
  
  public String getContextCode()
  {
    return this.mContextCode;
  }
  
  public String getNamespace()
  {
    return this.mNamespace;
  }
  
  public String getPubContent()
  {
    return this.mPubContent;
  }
  
  public boolean isClickedImpressionTracked()
  {
    return this.isClickedImpressionTracked;
  }
  
  public boolean isImpressionTracked()
  {
    return this.isImpressionTracked;
  }
  
  public void setIsClickedImpressionTracked(boolean paramBoolean)
  {
    this.isClickedImpressionTracked = paramBoolean;
  }
  
  public void setIsImpressionTracked(boolean paramBoolean)
  {
    this.isImpressionTracked = paramBoolean;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/inmobi/CJRTPData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */