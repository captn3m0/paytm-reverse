package net.one97.paytm.common.entity.inmobi;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRInMobiItem
  implements IJRDataModel
{
  @b(a="catalog_InMobi_Item")
  CJRInMobiCatalogItem mCatalogItem;
  @b(a="hometab_InMobi_Item")
  CJRHomeTabItem mHomeTabItem;
  
  public CJRInMobiCatalogItem getCatalogItem()
  {
    return this.mCatalogItem;
  }
  
  public CJRHomeTabItem getHomeTabItem()
  {
    return this.mHomeTabItem;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/inmobi/CJRInMobiItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */