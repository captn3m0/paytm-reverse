package net.one97.paytm.common.entity.inmobi;

import com.google.c.a.b;
import java.io.Serializable;

public class CJRPubContent
  implements Serializable
{
  String bannerType = "inmobi_type";
  @b(a="icon")
  private CJRCarouselIcon mCarouselIcon;
  @b(a="category")
  private String mCategory;
  @b(a="cta")
  private String mCta;
  @b(a="description")
  private String mDescription;
  @b(a="screenshots")
  private CJRGridIcon mGridIcon;
  @b(a="landingURL")
  private String mLandingUrl;
  @b(a="package_name")
  private String mPackageName;
  @b(a="rating")
  private float mRating;
  @b(a="title")
  private String mTitle;
  
  public boolean equals(Object paramObject)
  {
    try
    {
      boolean bool = this.mPackageName.trim().equalsIgnoreCase(((CJRPubContent)paramObject).getPackageName().trim());
      return bool;
    }
    catch (Exception paramObject) {}
    return false;
  }
  
  public String getBannerType()
  {
    return this.bannerType;
  }
  
  public CJRCarouselIcon getCarouselIcon()
  {
    return this.mCarouselIcon;
  }
  
  public String getCategory()
  {
    return this.mCategory;
  }
  
  public String getCta()
  {
    return this.mCta;
  }
  
  public String getDescription()
  {
    return this.mDescription;
  }
  
  public CJRGridIcon getGridIcon()
  {
    return this.mGridIcon;
  }
  
  public String getLandingUrl()
  {
    return this.mLandingUrl;
  }
  
  public String getPackageName()
  {
    return this.mPackageName;
  }
  
  public float getRating()
  {
    return this.mRating;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
  
  public void setBannerType(String paramString)
  {
    this.bannerType = paramString;
  }
  
  public void setGridIcon(CJRGridIcon paramCJRGridIcon)
  {
    this.mGridIcon = paramCJRGridIcon;
  }
  
  public void setLandingUrl(String paramString)
  {
    this.mLandingUrl = paramString;
  }
  
  public void setPackageName(String paramString)
  {
    this.mPackageName = paramString;
  }
  
  public void setTitle(String paramString)
  {
    this.mTitle = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/inmobi/CJRPubContent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */