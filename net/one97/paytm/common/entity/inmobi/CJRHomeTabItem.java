package net.one97.paytm.common.entity.inmobi;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRHomeTabItem
  implements IJRDataModel
{
  @b(a="index")
  int mIndex;
  @b(a="name")
  String mName;
  @b(a="url_type")
  String mUrlType;
  @b(a="visibility")
  int mVisibility;
  
  public int getIndex()
  {
    return this.mIndex;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getUrlType()
  {
    return this.mUrlType;
  }
  
  public int getVisibility()
  {
    return this.mVisibility;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/inmobi/CJRHomeTabItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */