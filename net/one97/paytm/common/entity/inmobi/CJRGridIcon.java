package net.one97.paytm.common.entity.inmobi;

import com.google.c.a.b;
import java.io.Serializable;

public class CJRGridIcon
  implements Serializable
{
  @b(a="aspectRatio")
  private float mAspectRatio;
  @b(a="height")
  private int mHeight;
  @b(a="url")
  private String mUrl;
  @b(a="width")
  private int mWidth;
  
  public float getAspectRatio()
  {
    return this.mAspectRatio;
  }
  
  public int getHeight()
  {
    return this.mHeight;
  }
  
  public String getUrl()
  {
    return this.mUrl;
  }
  
  public int getWidth()
  {
    return this.mWidth;
  }
  
  public void setmUrl(String paramString)
  {
    this.mUrl = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/inmobi/CJRGridIcon.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */