package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRFeedbackReason
  implements IJRDataModel
{
  @b(a="id")
  private String id;
  @b(a="reason")
  private String reason;
  
  public String getId()
  {
    return this.id;
  }
  
  public String getReason()
  {
    return this.reason;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setReason(String paramString)
  {
    this.reason = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRFeedbackReason.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */