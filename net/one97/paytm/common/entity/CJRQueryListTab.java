package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRQueryListTab
  implements IJRDataModel
{
  @b(a="display_label")
  private String displayLabel;
  @b(a="searchKey")
  private String searchKey;
  @b(a="searchUrl")
  private String searchUrl;
  
  public String getDisplayLabel()
  {
    return this.displayLabel;
  }
  
  public String getSearchKey()
  {
    return this.searchKey;
  }
  
  public String getSearchUrl()
  {
    return this.searchUrl;
  }
  
  public void setDisplayLabel(String paramString)
  {
    this.displayLabel = paramString;
  }
  
  public void setSearchKey(String paramString)
  {
    this.searchKey = paramString;
  }
  
  public void setSearchUrl(String paramString)
  {
    this.searchUrl = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRQueryListTab.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */