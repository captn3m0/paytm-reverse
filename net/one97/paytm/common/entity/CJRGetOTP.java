package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRGetOTP
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="error")
  private String mErrorMsg;
  @b(a="status")
  private String mStatus;
  
  public String getErrorMsg()
  {
    return this.mErrorMsg;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRGetOTP.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */