package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.ArrayList;

public class CJRUserInfoV2
  extends CJRDataModelItem
{
  @b(a="message")
  private String mMessage;
  @b(a="responseCode")
  private int mResponseCode;
  @b(a="socialInfo")
  private ArrayList<CJRUserSocialInfoV2> mSocialInfoList;
  @b(a="status")
  private String mStatus;
  @b(a="defaultInfo")
  private CJRUserDefaultInfo mUserDefaultInfo;
  @b(a="userId")
  private String mUserId;
  @b(a="userTypes")
  private ArrayList<String> mUserTypes;
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getName()
  {
    return null;
  }
  
  public int getResponseCode()
  {
    return this.mResponseCode;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public CJRUserDefaultInfo getUserDefaultInfo()
  {
    return this.mUserDefaultInfo;
  }
  
  public String getUserId()
  {
    return this.mUserId;
  }
  
  public ArrayList<CJRUserSocialInfoV2> getUserSocialInfoList()
  {
    return this.mSocialInfoList;
  }
  
  public ArrayList<String> getUserTypes()
  {
    return this.mUserTypes;
  }
  
  public void setMessage(String paramString)
  {
    this.mMessage = paramString;
  }
  
  public void setResponseCode(int paramInt)
  {
    this.mResponseCode = paramInt;
  }
  
  public void setStatus(String paramString)
  {
    this.mStatus = paramString;
  }
  
  public void setUserDefaultInfo(CJRUserDefaultInfo paramCJRUserDefaultInfo)
  {
    this.mUserDefaultInfo = paramCJRUserDefaultInfo;
  }
  
  public void setUserId(String paramString)
  {
    this.mUserId = paramString;
  }
  
  public void setUserSocialInfoList(ArrayList<CJRUserSocialInfoV2> paramArrayList)
  {
    this.mSocialInfoList = paramArrayList;
  }
  
  public void setUserTypes(ArrayList<String> paramArrayList)
  {
    this.mUserTypes = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRUserInfoV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */