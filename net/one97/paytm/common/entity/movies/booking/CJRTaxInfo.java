package net.one97.paytm.common.entity.movies.booking;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTaxInfo
  implements IJRDataModel
{
  @b(a="key")
  private String key;
  @b(a="label")
  private String label;
  @b(a="value")
  private String value;
  
  public String getKey()
  {
    return this.key;
  }
  
  public String getLabel()
  {
    return this.label;
  }
  
  public String getValue()
  {
    return this.value;
  }
  
  public void setKey(String paramString)
  {
    this.key = paramString;
  }
  
  public void setLabel(String paramString)
  {
    this.label = paramString;
  }
  
  public void setValue(String paramString)
  {
    this.value = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/booking/CJRTaxInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */