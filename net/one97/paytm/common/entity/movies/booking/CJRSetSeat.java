package net.one97.paytm.common.entity.movies.booking;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRSetSeat
  implements IJRDataModel
{
  @b(a="seatCodes")
  private ArrayList<String> seatCodes = new ArrayList();
  @b(a="seatInfo")
  private String seatInfo;
  @b(a="tempTransId")
  private String tempTransId;
  
  public ArrayList<String> getSeatCodes()
  {
    return this.seatCodes;
  }
  
  public String getSeatInfo()
  {
    return this.seatInfo;
  }
  
  public String getTempTransId()
  {
    return this.tempTransId;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/booking/CJRSetSeat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */