package net.one97.paytm.common.entity.movies.booking;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAddSeat
  implements IJRDataModel
{
  @b(a="bookingId")
  private String bookingId;
  @b(a="bookingIndex")
  private String bookingIndex;
  @b(a="seatCodes")
  private ArrayList<String> seatCodes = new ArrayList();
  @b(a="seatInfo")
  private String seatInfo;
  @b(a="tempTransId")
  private String tempTransId;
  @b(a="totalAmount")
  private String totalAmount;
  @b(a="uniqueBookingId")
  private String uniqueBookingId;
  
  public String getBookingId()
  {
    return this.bookingId;
  }
  
  public String getBookingIndex()
  {
    return this.bookingIndex;
  }
  
  public ArrayList<String> getSeatCodes()
  {
    return this.seatCodes;
  }
  
  public String getSeatInfo()
  {
    return this.seatInfo;
  }
  
  public String getTempTransId()
  {
    return this.tempTransId;
  }
  
  public String getTotalAmount()
  {
    return this.totalAmount;
  }
  
  public String getUniqueBookingId()
  {
    return this.uniqueBookingId;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/booking/CJRAddSeat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */