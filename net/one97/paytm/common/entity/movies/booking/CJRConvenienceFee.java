package net.one97.paytm.common.entity.movies.booking;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRConvenienceFee
  implements IJRDataModel
{
  @b(a="tax")
  private ArrayList<CJRTaxInfo> taxList;
  @b(a="totalCommision")
  private String totalCommision;
  @b(a="totalTax")
  private ArrayList<CJRTaxInfo> totalTaxList;
  
  public ArrayList<CJRTaxInfo> getTaxList()
  {
    return this.taxList;
  }
  
  public String getTotalCommision()
  {
    return this.totalCommision;
  }
  
  public ArrayList<CJRTaxInfo> getTotalTaxList()
  {
    return this.totalTaxList;
  }
  
  public void setTaxList(ArrayList<CJRTaxInfo> paramArrayList)
  {
    this.taxList = paramArrayList;
  }
  
  public void setTotalCommision(String paramString)
  {
    this.totalCommision = paramString;
  }
  
  public void setTotalTaxList(ArrayList<CJRTaxInfo> paramArrayList)
  {
    this.totalTaxList = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/booking/CJRConvenienceFee.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */