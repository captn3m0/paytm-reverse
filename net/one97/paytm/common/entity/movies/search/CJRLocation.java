package net.one97.paytm.common.entity.movies.search;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRLocation
  implements IJRDataModel
{
  @b(a="label")
  private String mLabel;
  private String mLastSelectedValue;
  @b(a="searchKeys")
  private ArrayList<String> mSearchKey;
  @b(a="value")
  private String mValue;
  
  public String getLabel()
  {
    return this.mLabel;
  }
  
  public String getLastSelected()
  {
    return this.mLastSelectedValue;
  }
  
  public String getValue()
  {
    return this.mValue;
  }
  
  public ArrayList<String> getmSearchKey()
  {
    return this.mSearchKey;
  }
  
  public void setLabel(String paramString)
  {
    this.mLabel = paramString;
  }
  
  public void setLastSelected(String paramString)
  {
    this.mLastSelectedValue = paramString;
  }
  
  public void setValue(String paramString)
  {
    this.mValue = paramString;
  }
  
  public void setmSearchKey(ArrayList<String> paramArrayList)
  {
    this.mSearchKey = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/search/CJRLocation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */