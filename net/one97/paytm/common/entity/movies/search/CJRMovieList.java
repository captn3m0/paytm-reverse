package net.one97.paytm.common.entity.movies.search;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRMovieList
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="code")
  private String mCode;
  @b(a="shows")
  private String[] mMovieShows;
  @b(a="title")
  private String mTitle;
  
  public String getCode()
  {
    return this.mCode;
  }
  
  public String[] getMovieShows()
  {
    return this.mMovieShows;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/search/CJRMovieList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */