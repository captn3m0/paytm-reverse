package net.one97.paytm.common.entity.movies.search;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.HashMap;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRMovieDetails
  implements IJRDataModel
{
  private static HashMap<String, ArrayList<CJRCinema>> mCinemaListGroupedByProvider;
  private static final long serialVersionUID = 1L;
  @b(a="cinemas")
  private ArrayList<CJRCinema> mCinema;
  @b(a="comingsoon")
  private ArrayList<CJRComingSoon> mComingSoon;
  @b(a="movielist")
  private ArrayList<CJRMovieList> mMovieList;
  @b(a="movies")
  private ArrayList<CJRMovies> mMovies;
  private ArrayList<CJRMovies> mNearbyMovieList;
  private ArrayList<CJRMovies> mNewReleaseList;
  private ArrayList<CJRMovies> mTopMovieList;
  @b(a="message_map")
  private HashMap<String, String> messageMap;
  
  public static HashMap<String, ArrayList<CJRCinema>> getCinemaListGroupedByProvider()
  {
    return mCinemaListGroupedByProvider;
  }
  
  public static void setCinemaListGroupedByProvider(HashMap<String, ArrayList<CJRCinema>> paramHashMap)
  {
    mCinemaListGroupedByProvider = paramHashMap;
  }
  
  public ArrayList<CJRCinema> getCinema()
  {
    return this.mCinema;
  }
  
  public ArrayList<CJRComingSoon> getComingSoon()
  {
    return this.mComingSoon;
  }
  
  public HashMap<String, String> getMessageMap()
  {
    return this.messageMap;
  }
  
  public ArrayList<CJRMovieList> getMovieList()
  {
    return this.mMovieList;
  }
  
  public ArrayList<CJRMovies> getMovies()
  {
    return this.mMovies;
  }
  
  public ArrayList<CJRMovies> getNearbyMovieList()
  {
    return this.mNearbyMovieList;
  }
  
  public ArrayList<CJRMovies> getNewReleaseList()
  {
    return this.mNewReleaseList;
  }
  
  public ArrayList<CJRMovies> getTopMovieList()
  {
    return this.mTopMovieList;
  }
  
  public void setNearbyMovieList(ArrayList<CJRMovies> paramArrayList)
  {
    this.mNearbyMovieList = paramArrayList;
  }
  
  public void setNewReleaseList(ArrayList<CJRMovies> paramArrayList)
  {
    this.mNewReleaseList = paramArrayList;
  }
  
  public void setTopMovieList(ArrayList<CJRMovies> paramArrayList)
  {
    this.mTopMovieList = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/search/CJRMovieDetails.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */