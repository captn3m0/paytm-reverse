package net.one97.paytm.common.entity.movies.search;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRComingSoon
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="catName")
  private String mCatName;
  @b(a="censor")
  private String mCensor;
  @b(a="code")
  private String mCode;
  @b(a="content")
  private String mContent;
  @b(a="description")
  private String mDescription;
  @b(a="display")
  private String mDisplay;
  @b(a="duration")
  private int mDuration;
  @b(a="longDescription")
  private String mLongDescription;
  @b(a="openingDate")
  private String mOpeningDate;
  @b(a="providerId")
  private int mProviderID;
  @b(a="providerName")
  private String mProviderName;
  @b(a="shortnameAlt")
  private String mShortNameAlt;
  @b(a="title")
  private String mTitle;
  @b(a="urlForTrailer")
  private String mTrailerURL;
  
  public String getCatName()
  {
    return this.mCatName;
  }
  
  public String getCensor()
  {
    return this.mCensor;
  }
  
  public String getCode()
  {
    return this.mCode;
  }
  
  public String getContent()
  {
    return this.mContent;
  }
  
  public String getDescription()
  {
    return this.mDescription;
  }
  
  public String getDisplay()
  {
    return this.mDisplay;
  }
  
  public int getDuration()
  {
    return this.mDuration;
  }
  
  public String getLongDescription()
  {
    return this.mLongDescription;
  }
  
  public String getOpeningDate()
  {
    return this.mOpeningDate;
  }
  
  public int getProviderID()
  {
    return this.mProviderID;
  }
  
  public String getProviderName()
  {
    return this.mProviderName;
  }
  
  public String getShortNameAlt()
  {
    return this.mShortNameAlt;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
  
  public String getTrailerURL()
  {
    return this.mTrailerURL;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/search/CJRComingSoon.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */