package net.one97.paytm.common.entity.movies.search;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRMoviesPriceDetails
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="areaCatCode")
  private String mAreaCode;
  @b(a="cinemaId")
  private String mCinemaID;
  @b(a="pGroupCode")
  private String mPGroupCode;
  @b(a="price")
  private double mPrice = -1.0D;
  @b(a="typeCode")
  private String mTypeCode;
  @b(a="tTypeDescription")
  private String mTypeDescription;
  @b(a="voucher_price")
  private double mVoucherPrice;
  
  public String getAreaCode()
  {
    return this.mAreaCode;
  }
  
  public String getCinemaID()
  {
    return this.mCinemaID;
  }
  
  public String getPGroupCode()
  {
    return this.mPGroupCode;
  }
  
  public double getPrice()
  {
    return this.mPrice;
  }
  
  public String getTypeCode()
  {
    return this.mTypeCode;
  }
  
  public String getTypeDescription()
  {
    return this.mTypeDescription;
  }
  
  public double getVoucherPrice()
  {
    return this.mVoucherPrice;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/search/CJRMoviesPriceDetails.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */