package net.one97.paytm.common.entity.movies.search;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRMovies
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  private boolean gridItemViewed;
  private boolean itemViewed;
  @b(a="app_cover_url")
  private String mAppCoverUrl;
  @b(a="catName")
  private String mCatName;
  @b(a="censor")
  private String mCensor;
  @b(a="code")
  private String mCode;
  @b(a="content")
  private String mContent;
  @b(a="openingDate")
  private String mDate;
  @b(a="display")
  private String mDisplay;
  @b(a="duration")
  private int mDuration;
  @b(a="image_url")
  private String mImageURL;
  @b(a="language")
  private String mLanguage;
  @b(a="sessions")
  private ArrayList<CJRMoviesSession> mMoviesSession;
  @b(a="providerName")
  private String mProviderName;
  @b(a="title")
  private String mTitle;
  @b(a="urlForTrailer")
  private String mTrailerURL;
  @b(a="genre")
  private String mgenre;
  
  public String getAppCoverUrl()
  {
    return this.mAppCoverUrl;
  }
  
  public String getCatName()
  {
    return this.mCatName;
  }
  
  public String getCensor()
  {
    return this.mCensor;
  }
  
  public String getCode()
  {
    return this.mCode;
  }
  
  public String getContent()
  {
    return this.mContent;
  }
  
  public String getDate()
  {
    return this.mDate;
  }
  
  public String getDisplay()
  {
    return this.mDisplay;
  }
  
  public int getDuration()
  {
    return this.mDuration;
  }
  
  public String getImageURL()
  {
    return this.mImageURL;
  }
  
  public String getLanguage()
  {
    return this.mLanguage;
  }
  
  public String getMgenre()
  {
    return this.mgenre;
  }
  
  public ArrayList<CJRMoviesSession> getMoviesSession()
  {
    return this.mMoviesSession;
  }
  
  public String getProviderName()
  {
    return this.mProviderName;
  }
  
  public String getTitle()
  {
    if (this.mTitle != null) {
      return this.mTitle;
    }
    return "";
  }
  
  public String getTrailerURL()
  {
    return this.mTrailerURL;
  }
  
  public boolean isGridItemViewed()
  {
    return this.gridItemViewed;
  }
  
  public boolean isItemViewed()
  {
    return this.itemViewed;
  }
  
  public void setCensor(String paramString)
  {
    this.mCensor = paramString;
  }
  
  public void setDuration(int paramInt)
  {
    this.mDuration = paramInt;
  }
  
  public void setGridItemViewed()
  {
    this.gridItemViewed = true;
  }
  
  public void setImageURL(String paramString)
  {
    this.mImageURL = paramString;
  }
  
  public void setItemViewed()
  {
    this.itemViewed = true;
  }
  
  public void setLanguage(String paramString)
  {
    this.mLanguage = paramString;
  }
  
  public void setMgenre(String paramString)
  {
    this.mgenre = paramString;
  }
  
  public void setTitle(String paramString)
  {
    this.mTitle = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/search/CJRMovies.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */