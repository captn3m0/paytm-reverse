package net.one97.paytm.common.entity.movies.search;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRMoviesSessionDetails
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="areaCatCode")
  private String mAreaCatCode;
  @b(a="cinemaId")
  private String mCinemaID;
  @b(a="priceDetails")
  private CJRMoviesPriceDetails mMoviesPriceDetails;
  @b(a="priceCode")
  private String mPriceCode;
  @b(a="seatsAvail")
  private int mSeatsAvailable;
  @b(a="sessionId")
  private String mSessionID;
  @b(a="seatsTotal")
  private String mTotalSeats;
  
  public String getAreaCatCode()
  {
    return this.mAreaCatCode;
  }
  
  public String getCinemaID()
  {
    return this.mCinemaID;
  }
  
  public CJRMoviesPriceDetails getMoviesPriceDetails()
  {
    return this.mMoviesPriceDetails;
  }
  
  public String getPriceCode()
  {
    return this.mPriceCode;
  }
  
  public int getSeatsAvailable()
  {
    return this.mSeatsAvailable;
  }
  
  public String getSessionID()
  {
    return this.mSessionID;
  }
  
  public String getTotalSeats()
  {
    return this.mTotalSeats;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/search/CJRMoviesSessionDetails.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */