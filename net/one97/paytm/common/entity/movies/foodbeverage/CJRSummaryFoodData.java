package net.one97.paytm.common.entity.movies.foodbeverage;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRSummaryFoodData
  implements IJRDataModel
{
  @b(a="itemDescription")
  private String itemDescription;
  @b(a="itemName")
  private String itemName;
  @b(a="quantity")
  private String quantity;
  @b(a="totalChargedPrice")
  private String totalChargedPrice;
  
  public String getItemDescription()
  {
    return this.itemDescription;
  }
  
  public String getItemName()
  {
    return this.itemName;
  }
  
  public String getQuantity()
  {
    return this.quantity;
  }
  
  public String getTotalChargedPrice()
  {
    return this.totalChargedPrice;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/foodbeverage/CJRSummaryFoodData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */