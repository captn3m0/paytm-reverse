package net.one97.paytm.common.entity.movies.foodbeverage;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFoodBeverageResponseItem
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="itemDescription")
  private String mItemDescription;
  @b(a="itemImageUrl")
  private String mItemImageURL;
  @b(a="itemName")
  private String mItemName;
  @b(a="paytmFoodID")
  private int mPaytmFoodID;
  @b(a="providerItemId")
  private String mProviderItemID;
  @b(a="providerTotalChargedConvFee")
  private int mProviderTotalChargedConvFee;
  @b(a="providerTotalChargedPrice")
  private int mProviderTotalChargedPrice;
  @b(a="providerUnitChargedPrice")
  private int mProviderUnitChargedPrice;
  @b(a="quantity")
  private int mQuantity;
  @b(a="totalChargedConvFee")
  private int mTotalChargedConvFee;
  @b(a="totalChargedPrice")
  private int mTotalChargedPrice;
  @b(a="unitChargedPrice")
  private int mUnitChargedPrice;
  
  public String getmItemDescription()
  {
    return this.mItemDescription;
  }
  
  public String getmItemImageURL()
  {
    return this.mItemImageURL;
  }
  
  public String getmItemName()
  {
    return this.mItemName;
  }
  
  public int getmPaytmFoodID()
  {
    return this.mPaytmFoodID;
  }
  
  public String getmProviderItemID()
  {
    return this.mProviderItemID;
  }
  
  public int getmProviderTotalChargedConvFee()
  {
    return this.mProviderTotalChargedConvFee;
  }
  
  public int getmProviderTotalChargedPrice()
  {
    return this.mProviderTotalChargedPrice;
  }
  
  public int getmProviderUnitChargedPrice()
  {
    return this.mProviderUnitChargedPrice;
  }
  
  public int getmQuantity()
  {
    return this.mQuantity;
  }
  
  public int getmTotalChargedConvFee()
  {
    return this.mTotalChargedConvFee;
  }
  
  public int getmTotalChargedPrice()
  {
    return this.mTotalChargedPrice;
  }
  
  public int getmUnitChargedPrice()
  {
    return this.mUnitChargedPrice;
  }
  
  public void setmItemDescription(String paramString)
  {
    this.mItemDescription = paramString;
  }
  
  public void setmItemImageURL(String paramString)
  {
    this.mItemImageURL = paramString;
  }
  
  public void setmItemName(String paramString)
  {
    this.mItemName = paramString;
  }
  
  public void setmPaytmFoodID(int paramInt)
  {
    this.mPaytmFoodID = paramInt;
  }
  
  public void setmProviderItemID(String paramString)
  {
    this.mProviderItemID = paramString;
  }
  
  public void setmProviderTotalChargedConvFee(int paramInt)
  {
    this.mProviderTotalChargedConvFee = paramInt;
  }
  
  public void setmProviderTotalChargedPrice(int paramInt)
  {
    this.mProviderTotalChargedPrice = paramInt;
  }
  
  public void setmProviderUnitChargedPrice(int paramInt)
  {
    this.mProviderUnitChargedPrice = paramInt;
  }
  
  public void setmQuantity(int paramInt)
  {
    this.mQuantity = paramInt;
  }
  
  public void setmTotalChargedConvFee(int paramInt)
  {
    this.mTotalChargedConvFee = paramInt;
  }
  
  public void setmTotalChargedPrice(int paramInt)
  {
    this.mTotalChargedPrice = paramInt;
  }
  
  public void setmUnitChargedPrice(int paramInt)
  {
    this.mUnitChargedPrice = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/foodbeverage/CJRFoodBeverageResponseItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */