package net.one97.paytm.common.entity.movies.foodbeverage;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFoodBeverageItem
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="paytm_cinema_id")
  private String mCinemaID;
  @b(a="conv_fee")
  private String mConvinienceFee;
  @b(a="created_at")
  private String mCreatedAt;
  private int mFoodQauntitySelected;
  @b(a="id")
  private String mID;
  @b(a="item_description")
  private String mItemDescription;
  @b(a="item_image_url")
  private String mItemImageURL;
  @b(a="item_name")
  private String mItemName;
  @b(a="pg_charges")
  private String mPGCharges;
  @b(a="price")
  private String mPrice;
  @b(a="product_id")
  private String mProductID;
  @b(a="provider_id")
  private String mProviderID;
  @b(a="provider_item_id")
  private String mProviderItemID;
  @b(a="status")
  private String mStatus;
  @b(a="updated_at")
  private String mupdatedAt;
  
  public String getMupdatedAt()
  {
    return this.mupdatedAt;
  }
  
  public String getmCinemaID()
  {
    return this.mCinemaID;
  }
  
  public String getmConvinienceFee()
  {
    return this.mConvinienceFee;
  }
  
  public String getmCreatedAt()
  {
    return this.mCreatedAt;
  }
  
  public int getmFoodQauntitySelected()
  {
    return this.mFoodQauntitySelected;
  }
  
  public String getmID()
  {
    return this.mID;
  }
  
  public String getmItemDescription()
  {
    return this.mItemDescription;
  }
  
  public String getmItemImageURL()
  {
    return this.mItemImageURL;
  }
  
  public String getmItemName()
  {
    return this.mItemName;
  }
  
  public String getmPGCharges()
  {
    return this.mPGCharges;
  }
  
  public String getmPrice()
  {
    return this.mPrice;
  }
  
  public String getmProductID()
  {
    return this.mProductID;
  }
  
  public String getmProviderID()
  {
    return this.mProviderID;
  }
  
  public String getmProviderItemID()
  {
    return this.mProviderItemID;
  }
  
  public String getmStatus()
  {
    return this.mStatus;
  }
  
  public void setMupdatedAt(String paramString)
  {
    this.mupdatedAt = paramString;
  }
  
  public void setmCinemaID(String paramString)
  {
    this.mCinemaID = paramString;
  }
  
  public void setmConvinienceFee(String paramString)
  {
    this.mConvinienceFee = paramString;
  }
  
  public void setmCreatedAt(String paramString)
  {
    this.mCreatedAt = paramString;
  }
  
  public void setmFoodQauntitySelected(int paramInt)
  {
    this.mFoodQauntitySelected = paramInt;
  }
  
  public void setmID(String paramString)
  {
    this.mID = paramString;
  }
  
  public void setmItemDescription(String paramString)
  {
    this.mItemDescription = paramString;
  }
  
  public void setmItemImageURL(String paramString)
  {
    this.mItemImageURL = paramString;
  }
  
  public void setmItemName(String paramString)
  {
    this.mItemName = paramString;
  }
  
  public void setmPGCharges(String paramString)
  {
    this.mPGCharges = paramString;
  }
  
  public void setmPrice(String paramString)
  {
    this.mPrice = paramString;
  }
  
  public void setmProductID(String paramString)
  {
    this.mProductID = paramString;
  }
  
  public void setmProviderID(String paramString)
  {
    this.mProviderID = paramString;
  }
  
  public void setmProviderItemID(String paramString)
  {
    this.mProviderItemID = paramString;
  }
  
  public void setmStatus(String paramString)
  {
    this.mStatus = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/foodbeverage/CJRFoodBeverageItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */