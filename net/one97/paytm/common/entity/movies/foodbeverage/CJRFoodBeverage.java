package net.one97.paytm.common.entity.movies.foodbeverage;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFoodBeverage
  implements IJRDataModel
{
  @b(a="items")
  private ArrayList<CJRFoodBeverageItem> mFoodBeverageList;
  
  public ArrayList<CJRFoodBeverageItem> getFoodBeverageList()
  {
    return this.mFoodBeverageList;
  }
  
  public void setmFoodBeverageList(ArrayList<CJRFoodBeverageItem> paramArrayList)
  {
    this.mFoodBeverageList = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/foodbeverage/CJRFoodBeverage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */