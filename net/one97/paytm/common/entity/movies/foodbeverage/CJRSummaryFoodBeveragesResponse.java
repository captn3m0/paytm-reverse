package net.one97.paytm.common.entity.movies.foodbeverage;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRSummaryFoodBeveragesResponse
  implements IJRDataModel
{
  @b(a="data")
  private ArrayList<CJRSummaryFoodData> data;
  @b(a="totalChargedPrice")
  private String totalChargedPrice;
  
  public ArrayList<CJRSummaryFoodData> getData()
  {
    return this.data;
  }
  
  public String getTotalChargedPrice()
  {
    return this.totalChargedPrice;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/foodbeverage/CJRSummaryFoodBeveragesResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */