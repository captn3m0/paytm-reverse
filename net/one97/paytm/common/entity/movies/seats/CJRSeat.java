package net.one97.paytm.common.entity.movies.seats;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.Iterator;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRSeat
  implements IJRDataModel
{
  @b(a="seatLayout")
  private CJRSeatLayout CJRSeatLayout;
  @b(a="areas")
  private ArrayList<CJRAreas> areas;
  @b(a="groupedSeats")
  private ArrayList<ArrayList<CJRGroupedSeats>> groupedSeats;
  @b(a="product_id")
  private String productId;
  @b(a="tempTransId")
  private String tempTransId;
  
  public ArrayList<CJRAreas> getAreas()
  {
    return this.areas;
  }
  
  public ArrayList<ArrayList<CJRGroupedSeats>> getGroupedSeats()
  {
    return this.groupedSeats;
  }
  
  public String getProductId()
  {
    return this.productId;
  }
  
  public CJRSeatLayout getSeatLayout()
  {
    return this.CJRSeatLayout;
  }
  
  public String getTempTransId()
  {
    return this.tempTransId;
  }
  
  public void sortSeats()
  {
    if ((getSeatLayout() != null) && (getSeatLayout().getColAreas() != null) && (getSeatLayout().getColAreas().getObjArea() != null))
    {
      Iterator localIterator = getSeatLayout().getColAreas().getObjArea().iterator();
      while (localIterator.hasNext())
      {
        Object localObject = (CJRObjArea)localIterator.next();
        if ((localObject != null) && (((CJRObjArea)localObject).getObjRow() != null))
        {
          ((CJRObjArea)localObject).sortObjRow();
          localObject = ((CJRObjArea)localObject).getObjRow().iterator();
          while (((Iterator)localObject).hasNext())
          {
            CJRObjRow localCJRObjRow = (CJRObjRow)((Iterator)localObject).next();
            if ((localCJRObjRow != null) && (localCJRObjRow.getObjSeat() != null)) {
              localCJRObjRow.sortObjSeat();
            }
          }
        }
      }
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/seats/CJRSeat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */