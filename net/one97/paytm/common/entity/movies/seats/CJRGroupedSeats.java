package net.one97.paytm.common.entity.movies.seats;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGroupedSeats
  implements IJRDataModel
{
  @b(a="AreaNum")
  private String areaNum;
  @b(a="GridRowId")
  private int gridRowId;
  @b(a="GridSeatNum")
  private int gridSeatNum;
  
  public String getAreaNum()
  {
    return this.areaNum;
  }
  
  public int getGridRowId()
  {
    return this.gridRowId;
  }
  
  public int getGridSeatNum()
  {
    return this.gridSeatNum;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/seats/CJRGroupedSeats.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */