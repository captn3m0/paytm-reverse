package net.one97.paytm.common.entity.movies.seats;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAreas
  implements IJRDataModel
{
  @b(a="AreaCode")
  private String areaCode;
  @b(a="AreaDesc")
  private String areaDesc;
  @b(a="available")
  private int available;
  
  public String getAreaCode()
  {
    return this.areaCode;
  }
  
  public String getAreaDesc()
  {
    return this.areaDesc;
  }
  
  public int getAvailable()
  {
    return this.available;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/seats/CJRAreas.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */