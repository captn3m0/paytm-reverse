package net.one97.paytm.common.entity.movies.seats;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.Collections;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRObjRow
  implements IJRDataModel
{
  @b(a="GridRowId")
  private int GridRowId;
  @b(a="PhyRowId")
  private String PhyRowId;
  @b(a="objSeat")
  private ArrayList<CJRObjSeat> objSeatList;
  
  public int getGridRowId()
  {
    return this.GridRowId;
  }
  
  public ArrayList<CJRObjSeat> getObjSeat()
  {
    return this.objSeatList;
  }
  
  public String getPhyRowId()
  {
    return this.PhyRowId;
  }
  
  public void sortObjSeat()
  {
    if (this.objSeatList != null) {
      Collections.sort(this.objSeatList, new CJRObjRow.1(this));
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/seats/CJRObjRow.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */