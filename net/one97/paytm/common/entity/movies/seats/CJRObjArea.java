package net.one97.paytm.common.entity.movies.seats;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.Collections;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRObjArea
  implements IJRDataModel
{
  @b(a="AreaCode")
  private String AreaCode;
  @b(a="AreaDesc")
  private String AreaDesc;
  @b(a="AreaNum")
  private String AreaNum;
  @b(a="HasCurrentOrder")
  private boolean hasCurrentOrder;
  @b(a="objRow")
  private ArrayList<CJRObjRow> objRowList;
  
  public String getAreaCode()
  {
    return this.AreaCode;
  }
  
  public String getAreaDesc()
  {
    return this.AreaDesc;
  }
  
  public String getAreaNum()
  {
    return this.AreaNum;
  }
  
  public boolean getHasCurrentOrder()
  {
    return this.hasCurrentOrder;
  }
  
  public ArrayList<CJRObjRow> getObjRow()
  {
    return this.objRowList;
  }
  
  public void sortObjRow()
  {
    if ((this.objRowList != null) && (!this.hasCurrentOrder)) {
      Collections.reverse(this.objRowList);
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/seats/CJRObjArea.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */