package net.one97.paytm.common.entity.movies.seats;

import android.text.TextUtils;
import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRObjSeat
  implements IJRDataModel
{
  @b(a="GridSeatNum")
  private int GridSeatNum;
  @b(a="SeatStatus")
  private String SeatStatus;
  private String areaCircleName;
  private String areaCode;
  private String areaDesc;
  private String gridSeatAreaNum;
  private int gridSeatRowId;
  private int mAreaAvailableCount;
  private String phyRowId;
  private double price;
  @b(a="seatNumber")
  private int seatNumber;
  private String typeCode;
  private double voucherPrice;
  
  public int getAreaAvailableCount()
  {
    return this.mAreaAvailableCount;
  }
  
  public String getAreaCircleName()
  {
    return this.areaCircleName;
  }
  
  public String getAreaCode()
  {
    if (!TextUtils.isEmpty(this.areaCode)) {
      return this.areaCode;
    }
    return "";
  }
  
  public String getAreaDesc()
  {
    return this.areaDesc;
  }
  
  public String getGridSeatAreaNum()
  {
    if (!TextUtils.isEmpty(this.gridSeatAreaNum)) {
      return this.gridSeatAreaNum;
    }
    return "";
  }
  
  public int getGridSeatNum()
  {
    return this.GridSeatNum;
  }
  
  public int getGridSeatRowId()
  {
    return this.gridSeatRowId;
  }
  
  public String getPhyRowId()
  {
    return this.phyRowId;
  }
  
  public double getPrice()
  {
    return this.price;
  }
  
  public int getSeatNumber()
  {
    return this.seatNumber;
  }
  
  public String getSeatStatus()
  {
    return this.SeatStatus;
  }
  
  public String getTypeCode()
  {
    return this.typeCode;
  }
  
  public String getUniqueId()
  {
    return getAreaCode() + getGridSeatAreaNum() + getGridSeatRowId() + getGridSeatNum();
  }
  
  public double getVoucherPrice()
  {
    return this.voucherPrice;
  }
  
  public void setAreaAvailableCount(int paramInt)
  {
    this.mAreaAvailableCount = paramInt;
  }
  
  public void setAreaCirclename(String paramString)
  {
    this.areaCircleName = paramString;
  }
  
  public void setAreaCode(String paramString)
  {
    this.areaCode = paramString;
  }
  
  public void setAreaDesc(String paramString)
  {
    this.areaDesc = paramString;
  }
  
  public void setGridSeatAreaNum(String paramString)
  {
    this.gridSeatAreaNum = paramString;
  }
  
  public void setGridSeatRowId(int paramInt)
  {
    this.gridSeatRowId = paramInt;
  }
  
  public void setPhyRowId(String paramString)
  {
    this.phyRowId = paramString;
  }
  
  public void setPrice(double paramDouble)
  {
    this.price = paramDouble;
  }
  
  public void setTypeCode(String paramString)
  {
    this.typeCode = paramString;
  }
  
  public void setVoucherPrice(double paramDouble)
  {
    this.voucherPrice = paramDouble;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/movies/seats/CJRObjSeat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */