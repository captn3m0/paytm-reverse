package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.ArrayList;

public class CJRFrontEndFilter
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="filter_param")
  private String mFilterParam;
  @b(a="values")
  private CJRFrontEndFilterItem mFrontEndFilterRootItem;
  private ArrayList<CJRFrontEndFilterItem> mSelectedFilterOptionList = new ArrayList();
  @b(a="title")
  private String mTitle;
  @b(a="type")
  private String mType;
  
  public void addItemSelectedFilterOptionList(CJRFrontEndFilterItem paramCJRFrontEndFilterItem)
  {
    this.mSelectedFilterOptionList.add(paramCJRFrontEndFilterItem);
  }
  
  public String getFilterParam()
  {
    return this.mFilterParam;
  }
  
  public CJRFrontEndFilterItem getFrontEndRootFilterItem()
  {
    return this.mFrontEndFilterRootItem;
  }
  
  public String getName()
  {
    return null;
  }
  
  public ArrayList<CJRFrontEndFilterItem> getSelectedFilterOptionList()
  {
    return this.mSelectedFilterOptionList;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
  
  public String getType()
  {
    return this.mType;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRFrontEndFilter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */