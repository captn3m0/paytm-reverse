package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRPaytmUser
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="error")
  private String mErrorMsg;
  @b(a="status")
  private String mStatus;
  @b(a="token")
  private String mToken;
  @b(a="user")
  private CJRUserAccount mUserInfo;
  
  public String getErrorMsg()
  {
    return this.mErrorMsg;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getSessionId()
  {
    return this.mToken;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public CJRUserAccount getUserInfo()
  {
    return this.mUserInfo;
  }
  
  public void setUserInfo(CJRUserAccount paramCJRUserAccount)
  {
    this.mUserInfo = paramCJRUserAccount;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRPaytmUser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */