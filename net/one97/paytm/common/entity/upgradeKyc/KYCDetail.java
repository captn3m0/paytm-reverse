package net.one97.paytm.common.entity.upgradeKyc;

import com.google.c.a.b;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class KYCDetail
  implements Serializable
{
  @b(a="kycAddress")
  private List<KycAddDetail> add_details = new ArrayList();
  @b(a="appointmentDate")
  private String appointmentDate;
  @b(a="appointmentTimeSlot")
  private String appointmentTimeSlot;
  @b(a="isSync")
  private Boolean isSync;
  @b(a="leadSource")
  private String leadSource;
  @b(a="name")
  private String name;
  @b(a="nameAsPerPoi")
  private String nameAsPerPoi;
  @b(a="poiNumber")
  private String poiNumber;
  @b(a="poiType")
  private String poiType;
  @b(a="stage")
  private String stage;
  @b(a="status")
  private String status;
  @b(a="substage")
  private String substage;
  @b(a="userDetails")
  private List<KycUserdetail> userDetails = new ArrayList();
  
  public List<KycAddDetail> getAddDetails()
  {
    return this.add_details;
  }
  
  public String getAppointmentTimeSlot()
  {
    return this.appointmentTimeSlot;
  }
  
  public Boolean getIsSync()
  {
    return this.isSync;
  }
  
  public String getLeadsource()
  {
    return this.leadSource;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getNameAsPerPOI()
  {
    return this.nameAsPerPoi;
  }
  
  public String getPOIType()
  {
    return this.poiType;
  }
  
  public String getPoINumber()
  {
    return this.poiNumber;
  }
  
  public String getPreferredDate()
  {
    return this.appointmentDate;
  }
  
  public String getStage()
  {
    return this.stage;
  }
  
  public String getStatus()
  {
    return this.status;
  }
  
  public String getSubstage()
  {
    return this.substage;
  }
  
  public List<KycUserdetail> getUserdetails()
  {
    return this.userDetails;
  }
  
  public void setAddDetails(List<KycAddDetail> paramList)
  {
    this.add_details = paramList;
  }
  
  public void setIsSync(Boolean paramBoolean)
  {
    this.isSync = paramBoolean;
  }
  
  public void setLeadsource(String paramString)
  {
    this.leadSource = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setNameAsPerPOI(String paramString)
  {
    this.nameAsPerPoi = paramString;
  }
  
  public void setPOIType(String paramString)
  {
    this.poiType = paramString;
  }
  
  public void setPoINumber(String paramString)
  {
    this.poiNumber = paramString;
  }
  
  public void setPreferredDate(String paramString)
  {
    this.appointmentDate = paramString;
  }
  
  public void setStage(String paramString)
  {
    this.stage = paramString;
  }
  
  public void setStatus(String paramString)
  {
    this.status = paramString;
  }
  
  public void setSubstage(String paramString)
  {
    this.substage = paramString;
  }
  
  public void setUserdetails(List<KycUserdetail> paramList)
  {
    this.userDetails = paramList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/upgradeKyc/KYCDetail.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */