package net.one97.paytm.common.entity.upgradeKyc;

import com.google.c.a.b;
import java.io.Serializable;

public class KycAddDetail
  implements Serializable
{
  @b(a="addressLine1")
  private String addressLine1;
  @b(a="addressLine2")
  private String addressLine2;
  @b(a="addressType")
  private String addressType;
  @b(a="city")
  private String city;
  @b(a="pincode")
  private String pincode;
  @b(a="state")
  private String state;
  
  public String getAddressLine1()
  {
    return this.addressLine1;
  }
  
  public String getAddressLine2()
  {
    return this.addressLine2;
  }
  
  public String getAddressType()
  {
    return this.addressType;
  }
  
  public String getCity()
  {
    return this.city;
  }
  
  public String getPincodeC()
  {
    return this.pincode;
  }
  
  public String getState()
  {
    return this.state;
  }
  
  public void setAddressLine1(String paramString)
  {
    this.addressLine1 = paramString;
  }
  
  public void setAddressLine2(String paramString)
  {
    this.addressLine2 = paramString;
  }
  
  public void setAddressType(String paramString)
  {
    this.addressType = paramString;
  }
  
  public void setCity(String paramString)
  {
    this.city = paramString;
  }
  
  public void setPincodeC(String paramString)
  {
    this.pincode = paramString;
  }
  
  public void setState(String paramString)
  {
    this.state = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/upgradeKyc/KycAddDetail.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */