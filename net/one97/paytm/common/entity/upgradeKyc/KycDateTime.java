package net.one97.paytm.common.entity.upgradeKyc;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class KycDateTime
  implements IJRDataModel
{
  @b(a="dateTime")
  private ArrayList<DateTime> dateTime;
  @b(a="errorMsg")
  private String errorMessage;
  @b(a="statusCode")
  private int statusCode;
  
  public ArrayList<DateTime> getDateTime()
  {
    return this.dateTime;
  }
  
  public String getErrorMessage()
  {
    return this.errorMessage;
  }
  
  public int getStatusCode()
  {
    return this.statusCode;
  }
  
  public class DateTime
  {
    @b(a="date")
    private String date;
    @b(a="timeSlot")
    private ArrayList<String> timeSlot;
    
    public DateTime() {}
    
    public String getDate()
    {
      return this.date;
    }
    
    public ArrayList<String> getTimeSlot()
    {
      return this.timeSlot;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/upgradeKyc/KycDateTime.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */