package net.one97.paytm.common.entity.upgradeKyc;

import com.google.c.a.b;
import java.io.Serializable;

public class KycUserdetail
  implements Serializable
{
  @b(a="aadharName")
  private String aadharName;
  @b(a="aadharNumber")
  private String aadharNumber;
  @b(a="altEmail")
  private String altEmail;
  @b(a="altPhoneNumber")
  private String altPhoneNumber;
  @b(a="dob")
  private String dob;
  @b(a="fatherOrSpouseFirstName")
  private String fatherOrSpouseFirstName;
  @b(a="fatherOrSpouseLastName")
  private String fatherOrSpouseLastName;
  @b(a="gender")
  private String gender;
  @b(a="isCorrespondanceAddressSameAsAadhar")
  private Boolean isCorrespondanceAddressSameAsAadhar;
  @b(a="isFatherInformation")
  private Boolean isFatherInformation;
  @b(a="maritalStatus")
  private String maritalStatus;
  @b(a="motherFirstName")
  private String motherFirstName;
  @b(a="motherLastName")
  private String motherLastName;
  @b(a="panNumber")
  private String panNumber;
  @b(a="profession")
  private String profession;
  
  public String getAadharName()
  {
    return this.aadharName;
  }
  
  public String getAadharNumber()
  {
    return this.aadharNumber;
  }
  
  public String getAlternateContactNo()
  {
    return this.altPhoneNumber;
  }
  
  public String getAlternateEmail()
  {
    return this.altEmail;
  }
  
  public String getDOB()
  {
    return this.dob;
  }
  
  public String getFatherOrSpouseFirstName()
  {
    return this.fatherOrSpouseFirstName;
  }
  
  public String getFatherOrSpouseLastName()
  {
    return this.fatherOrSpouseLastName;
  }
  
  public String getGender()
  {
    return this.gender;
  }
  
  public Boolean getIsCorrespondanceAddressSameAsAadhar()
  {
    return this.isCorrespondanceAddressSameAsAadhar;
  }
  
  public Boolean getIsFatherInformation()
  {
    return this.isFatherInformation;
  }
  
  public String getMaritalStatus()
  {
    return this.maritalStatus;
  }
  
  public String getMotherFirstName()
  {
    return this.motherFirstName;
  }
  
  public String getMotherLastName()
  {
    return this.motherLastName;
  }
  
  public String getPANCard()
  {
    return this.panNumber;
  }
  
  public String getProfession()
  {
    return this.profession;
  }
  
  public void setAadharName(String paramString)
  {
    this.aadharName = paramString;
  }
  
  public void setAadharNumber(String paramString)
  {
    this.aadharNumber = paramString;
  }
  
  public void setAlternateContactNo(String paramString)
  {
    this.altPhoneNumber = paramString;
  }
  
  public void setAlternateEmail(String paramString)
  {
    this.altEmail = paramString;
  }
  
  public void setDOB(String paramString)
  {
    this.dob = paramString;
  }
  
  public void setFatherOrSpouseFirstName(String paramString)
  {
    this.fatherOrSpouseFirstName = paramString;
  }
  
  public void setFatherOrSpouseLastName(String paramString)
  {
    this.fatherOrSpouseLastName = paramString;
  }
  
  public void setGender(String paramString)
  {
    this.gender = paramString;
  }
  
  public void setIsCorrespondanceAddressSameAsAadhar(Boolean paramBoolean)
  {
    this.isCorrespondanceAddressSameAsAadhar = paramBoolean;
  }
  
  public void setIsFatherInformation(Boolean paramBoolean)
  {
    this.isFatherInformation = paramBoolean;
  }
  
  public void setMaritalStatus(String paramString)
  {
    this.maritalStatus = paramString;
  }
  
  public void setMotherFirstName(String paramString)
  {
    this.motherFirstName = paramString;
  }
  
  public void setMotherLastName(String paramString)
  {
    this.motherLastName = paramString;
  }
  
  public void setPANCard(String paramString)
  {
    this.panNumber = paramString;
  }
  
  public void setProfession(String paramString)
  {
    this.profession = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/upgradeKyc/KycUserdetail.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */