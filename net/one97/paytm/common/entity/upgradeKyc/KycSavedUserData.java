package net.one97.paytm.common.entity.upgradeKyc;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class KycSavedUserData
  implements IJRDataModel
{
  @b(a="errorMsg")
  private String errorMsg;
  @b(a="kycAppointmentDetail")
  private List<KYCDetail> kycAppointmentDetail = new ArrayList();
  @b(a="message")
  private String message;
  @b(a="name")
  private String name;
  @b(a="statusCode")
  private Integer statusCode;
  
  public List<KYCDetail> getKYCDetail()
  {
    return this.kycAppointmentDetail;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public Integer getStatusCode()
  {
    return this.statusCode;
  }
  
  public String geterrorMsg()
  {
    return this.errorMsg;
  }
  
  public void setKYCDetail(List<KYCDetail> paramList)
  {
    this.kycAppointmentDetail = paramList;
  }
  
  public void setMessage(String paramString)
  {
    this.message = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setStatusCode(Integer paramInteger)
  {
    this.statusCode = paramInteger;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/upgradeKyc/KycSavedUserData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */