package net.one97.paytm.common.entity.brts;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAmountPax
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="amount")
  private double mAmount;
  @b(a="pax")
  private String mPax;
  @b(a="quantity")
  private int mQuantity;
  
  public double getAmount()
  {
    return this.mAmount;
  }
  
  public String getPax()
  {
    return this.mPax;
  }
  
  public int getQuantity()
  {
    return this.mQuantity;
  }
  
  public void setmAmount(double paramDouble)
  {
    this.mAmount = paramDouble;
  }
  
  public void setmPax(String paramString)
  {
    this.mPax = paramString;
  }
  
  public void setmQuantity(int paramInt)
  {
    this.mQuantity = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/brts/CJRAmountPax.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */