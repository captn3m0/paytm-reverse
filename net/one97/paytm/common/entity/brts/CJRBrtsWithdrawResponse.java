package net.one97.paytm.common.entity.brts;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrtsWithdrawResponse
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="cashBackMessage")
  private String mCashBackMessage;
  @b(a="cashBackStatus")
  private String mCashBackStatus;
  @b(a="comment")
  private String mComment;
  @b(a="heading")
  private String mHeading;
  @b(a="pgTxnId")
  private String mPGTxnId;
  @b(a="posId")
  private String mPosId;
  @b(a="state")
  private String mState;
  @b(a="timestamp")
  private String mTimeStamp;
  @b(a="txnExtendedInfo")
  private CJRBrtsTxnInfo mTxnExtendedInfo;
  @b(a="uniqueReferenceLabel")
  private String mUniqueReferenceLabel;
  @b(a="uniqueReferenceValue")
  private String mUniqueReferenceValue;
  @b(a="userGuid")
  private String mUserId;
  @b(a="walletSysTransactionId")
  private String mWalletSysTransactionId;
  @b(a="walletSystemTxnId")
  private String mWalletSystemTxnId;
  
  public String getCashBackMessage()
  {
    return this.mCashBackMessage;
  }
  
  public String getCashBackStatus()
  {
    return this.mCashBackStatus;
  }
  
  public String getComment()
  {
    return this.mComment;
  }
  
  public String getHeading()
  {
    return this.mHeading;
  }
  
  public String getPGTxnId()
  {
    return this.mPGTxnId;
  }
  
  public String getPosId()
  {
    return this.mPosId;
  }
  
  public String getState()
  {
    return this.mState;
  }
  
  public String getTimeStamp()
  {
    return this.mTimeStamp;
  }
  
  public CJRBrtsTxnInfo getTxnExtendedInfo()
  {
    return this.mTxnExtendedInfo;
  }
  
  public String getUniqueReferenceLabel()
  {
    return this.mUniqueReferenceLabel;
  }
  
  public String getUniqueReferenceValue()
  {
    return this.mUniqueReferenceValue;
  }
  
  public String getUserId()
  {
    return this.mUserId;
  }
  
  public String getWalletSysTransactionId()
  {
    return this.mWalletSysTransactionId;
  }
  
  public String getWalletSystemTxnId()
  {
    return this.mWalletSystemTxnId;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/brts/CJRBrtsWithdrawResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */