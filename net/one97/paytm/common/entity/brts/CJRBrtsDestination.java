package net.one97.paytm.common.entity.brts;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrtsDestination
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="typeOfBus")
  private ArrayList<String> mBusTypes;
  @b(a="destinationId")
  private String mDestinationId;
  @b(a="destinationName")
  private String mDestinationName;
  
  public boolean equals(Object paramObject)
  {
    return this.mDestinationName.equals(((CJRBrtsDestination)paramObject).getDestinationName());
  }
  
  public ArrayList<String> getBusTypes()
  {
    return this.mBusTypes;
  }
  
  public String getDestinationId()
  {
    return this.mDestinationId;
  }
  
  public String getDestinationName()
  {
    return this.mDestinationName;
  }
  
  public int hashCode()
  {
    return this.mDestinationName.hashCode();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/brts/CJRBrtsDestination.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */