package net.one97.paytm.common.entity.brts;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrtsFareResponse
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="busType")
  private String mBusType;
  @b(a="destinationId")
  private String mDestinationId;
  @b(a="destinationName")
  private String mDestinationName;
  @b(a="industryType")
  private String mIndustryType;
  @b(a="merchantGuid")
  private String mMerchantGuid;
  @b(a="fareDetails")
  private ArrayList<CJRBrtsPaxInfo> mPaxInfo;
  @b(a="routeId")
  private String mRouteId;
  @b(a="routeName")
  private String mRouteName;
  @b(a="sourceId")
  private String mSourceId;
  @b(a="sourceName")
  private String mSourceName;
  
  public String getBusType()
  {
    return this.mBusType;
  }
  
  public String getDestinationId()
  {
    return this.mDestinationId;
  }
  
  public String getDestinationName()
  {
    return this.mDestinationName;
  }
  
  public String getIndustryType()
  {
    return this.mIndustryType;
  }
  
  public String getMerchantGuid()
  {
    return this.mMerchantGuid;
  }
  
  public ArrayList<CJRBrtsPaxInfo> getPaxInfo()
  {
    return this.mPaxInfo;
  }
  
  public String getRouteId()
  {
    return this.mRouteId;
  }
  
  public String getRouteName()
  {
    return this.mRouteName;
  }
  
  public String getSourceId()
  {
    return this.mSourceId;
  }
  
  public String getSourceName()
  {
    return this.mSourceName;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/brts/CJRBrtsFareResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */