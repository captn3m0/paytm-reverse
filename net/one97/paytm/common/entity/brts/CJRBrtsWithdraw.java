package net.one97.paytm.common.entity.brts;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrtsWithdraw
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="response")
  private CJRBrtsWithdrawResponse mResponse;
  @b(a="statusCode")
  private String mStatusCode;
  @b(a="statusMessage")
  private String mStatusMessage;
  
  public CJRBrtsWithdrawResponse getResponse()
  {
    return this.mResponse;
  }
  
  public String getStatusCode()
  {
    return this.mStatusCode;
  }
  
  public String getStatusMessage()
  {
    return this.mStatusMessage;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/brts/CJRBrtsWithdraw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */