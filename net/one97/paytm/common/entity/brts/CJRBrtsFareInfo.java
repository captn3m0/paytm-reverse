package net.one97.paytm.common.entity.brts;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrtsFareInfo
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  private int mAdultCount;
  private int mChildCount;
  private int mPHCount;
  @b(a="response")
  private CJRBrtsFareResponse mResponse;
  private int mSeniorCount;
  @b(a="statusCode")
  private String mStatusCode;
  @b(a="statusMessage")
  private String mStatusMessage;
  private int mStudentCount;
  
  public int getAdultCount()
  {
    return this.mAdultCount;
  }
  
  public int getChildCount()
  {
    return this.mChildCount;
  }
  
  public int getPHCount()
  {
    return this.mPHCount;
  }
  
  public CJRBrtsFareResponse getResponse()
  {
    return this.mResponse;
  }
  
  public int getSeniorCount()
  {
    return this.mSeniorCount;
  }
  
  public String getStatusCode()
  {
    return this.mStatusCode;
  }
  
  public String getStatusMessage()
  {
    return this.mStatusMessage;
  }
  
  public int getStudentCount()
  {
    return this.mStudentCount;
  }
  
  public void setAdultCount(int paramInt)
  {
    this.mAdultCount = paramInt;
  }
  
  public void setChildCount(int paramInt)
  {
    this.mChildCount = paramInt;
  }
  
  public void setPHCount(int paramInt)
  {
    this.mPHCount = paramInt;
  }
  
  public void setSeniorCount(int paramInt)
  {
    this.mSeniorCount = paramInt;
  }
  
  public void setStudentCount(int paramInt)
  {
    this.mStudentCount = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/brts/CJRBrtsFareInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */