package net.one97.paytm.common.entity.brts;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrtsRoute
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="destinations")
  private ArrayList<CJRBrtsDestination> mDestinations;
  @b(a="routeId")
  private String mRouteId;
  @b(a="routeName")
  private String mRouteName;
  
  public ArrayList<CJRBrtsDestination> getDestinations()
  {
    return this.mDestinations;
  }
  
  public String getRouteId()
  {
    return this.mRouteId;
  }
  
  public String getRouteName()
  {
    return this.mRouteName;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/brts/CJRBrtsRoute.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */