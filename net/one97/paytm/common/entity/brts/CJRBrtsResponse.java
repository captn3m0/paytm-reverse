package net.one97.paytm.common.entity.brts;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrtsResponse
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="service")
  private String mService;
  @b(a="uts")
  private CJRBrtsUTS mUTS;
  
  public String getService()
  {
    return this.mService;
  }
  
  public CJRBrtsUTS getUTS()
  {
    return this.mUTS;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/brts/CJRBrtsResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */