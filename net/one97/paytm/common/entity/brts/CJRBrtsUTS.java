package net.one97.paytm.common.entity.brts;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrtsUTS
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="industryType")
  private String mIndustryType;
  @b(a="merchantGuid")
  private String mMerchantGuid;
  @b(a="merchantLogo")
  private String mMerchantLogoURL;
  @b(a="merchantName")
  private String mMerchantName;
  @b(a="routes")
  private ArrayList<CJRBrtsRoute> mRoutes;
  @b(a="sourceId")
  private String mSourceId;
  @b(a="sourceName")
  private String mSourceName;
  
  public String getIndustryType()
  {
    return this.mIndustryType;
  }
  
  public String getMerchantGuid()
  {
    return this.mMerchantGuid;
  }
  
  public String getMerchantLogoURL()
  {
    return this.mMerchantLogoURL;
  }
  
  public String getMerchantName()
  {
    return this.mMerchantName;
  }
  
  public ArrayList<CJRBrtsRoute> getRoutes()
  {
    return this.mRoutes;
  }
  
  public String getSourceId()
  {
    return this.mSourceId;
  }
  
  public String getSourceName()
  {
    return this.mSourceName;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/brts/CJRBrtsUTS.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */