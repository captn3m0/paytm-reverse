package net.one97.paytm.common.entity.brts;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrtsPaxInfo
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="fare")
  private double mFare;
  @b(a="pax")
  private String mPaxName;
  
  public double getFare()
  {
    return this.mFare;
  }
  
  public String getPaxName()
  {
    return this.mPaxName;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/brts/CJRBrtsPaxInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */