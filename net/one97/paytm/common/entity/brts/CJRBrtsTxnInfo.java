package net.one97.paytm.common.entity.brts;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrtsTxnInfo
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="amountDetails")
  private ArrayList<CJRAmountPax> mAmountDetails;
  @b(a="destination")
  private String mDestination;
  @b(a="source")
  private String mSource;
  @b(a="validity")
  private int mValidity;
  
  public ArrayList<CJRAmountPax> getAmountDetails()
  {
    return this.mAmountDetails;
  }
  
  public String getDestination()
  {
    return this.mDestination;
  }
  
  public String getSource()
  {
    return this.mSource;
  }
  
  public int getValidity()
  {
    return this.mValidity;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/brts/CJRBrtsTxnInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */