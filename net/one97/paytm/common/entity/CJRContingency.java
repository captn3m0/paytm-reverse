package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRContingency
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="close")
  private boolean mClose;
  @b(a="message")
  private String mMessage;
  @b(a="status")
  private boolean mStatus;
  @b(a="url")
  private String mUrl;
  
  public boolean getClose()
  {
    return this.mClose;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public boolean getStatus()
  {
    return this.mStatus;
  }
  
  public String getUrl()
  {
    return this.mUrl;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRContingency.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */