package net.one97.paytm.common.entity;

public abstract class CJRDataModelItem
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  
  public abstract String getName();
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRDataModelItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */