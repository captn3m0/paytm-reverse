package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.List;
import net.one97.paytm.common.entity.shopping.CJRHomePageLayout;

public class CJRAddMoneyBanner
  implements IJRDataModel
{
  @b(a="homepage_layout")
  private List<CJRHomePageLayout> mAddMoneyBannerProduct;
  
  public CJRHomePageLayout getAddMoneyBannerProduct()
  {
    if ((this.mAddMoneyBannerProduct != null) && (this.mAddMoneyBannerProduct.size() > 0)) {
      return (CJRHomePageLayout)this.mAddMoneyBannerProduct.get(0);
    }
    return null;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRAddMoneyBanner.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */