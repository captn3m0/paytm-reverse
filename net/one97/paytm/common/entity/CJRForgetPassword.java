package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRForgetPassword
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="detail")
  private String mDetail;
  @b(a="error")
  private String mError;
  @b(a="errorCode")
  private String mErrorCode;
  @b(a="code")
  private String mRevivedCode;
  @b(a="status")
  private String mStatus;
  @b(a="type")
  private String mType;
  @b(a="username")
  private String mUserName;
  
  public String getCode()
  {
    return this.mRevivedCode;
  }
  
  public String getDetail()
  {
    return this.mDetail;
  }
  
  public String getError()
  {
    return this.mError;
  }
  
  public String getErrorCode()
  {
    return this.mErrorCode;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public String getType()
  {
    return this.mType;
  }
  
  public String getUserName()
  {
    return this.mUserName;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRForgetPassword.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */