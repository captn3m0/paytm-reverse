package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.Iterator;

public class CJRTransaction
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="buttonList")
  private ArrayList<CJRButton> mButtonList = new ArrayList();
  @b(a="currencyCode")
  private String mCurrencyCode;
  @b(a="imageUrl")
  private String mImageUrl;
  @b(a="isBtnEnabled")
  private boolean mIsBtnEnabled;
  @b(a="label")
  private String mLabel;
  @b(a="narration")
  private String mNarration;
  @b(a="newNarration")
  private String mNewNarration;
  @b(a="txnamount")
  private String mTxnAmount;
  @b(a="txndate")
  private String mTxnDate;
  @b(a="txnDesc1")
  private String mTxnDesc1;
  @b(a="txnDesc2")
  private String mTxnDesc2;
  @b(a="txnDesc3")
  private String mTxnDesc3;
  @b(a="txnDesc4")
  private String mTxnDesc4;
  @b(a="txnFrom")
  private String mTxnFrom;
  @b(a="txnStatus")
  private String mTxnStatus;
  @b(a="txnTo")
  private String mTxnTo;
  @b(a="txntype")
  private String mTxnType;
  @b(a="type")
  private String mType;
  @b(a="walletOrderId")
  private String mWalletOrderId;
  
  public ArrayList<CJRButton> getButtonList()
  {
    return this.mButtonList;
  }
  
  public CJRButton getButtonWithTag(int paramInt)
  {
    Iterator localIterator = this.mButtonList.iterator();
    while (localIterator.hasNext())
    {
      CJRButton localCJRButton = (CJRButton)localIterator.next();
      if (localCJRButton.getButtonTag() == paramInt) {
        return localCJRButton;
      }
    }
    return null;
  }
  
  public String getCurrencyCode()
  {
    return this.mCurrencyCode;
  }
  
  public String getImageUrl()
  {
    return this.mImageUrl;
  }
  
  public String getLabel()
  {
    return this.mLabel;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getNarration()
  {
    return this.mNarration;
  }
  
  public String getNewNarration()
  {
    return this.mNewNarration;
  }
  
  public String getTxnAmount()
  {
    return this.mTxnAmount;
  }
  
  public String getTxnDate()
  {
    return this.mTxnDate;
  }
  
  public String getTxnDesc1()
  {
    return this.mTxnDesc1;
  }
  
  public String getTxnDesc2()
  {
    return this.mTxnDesc2;
  }
  
  public String getTxnDesc3()
  {
    return this.mTxnDesc3;
  }
  
  public String getTxnDesc4()
  {
    return this.mTxnDesc4;
  }
  
  public String getTxnFrom()
  {
    return this.mTxnFrom;
  }
  
  public String getTxnStatus()
  {
    return this.mTxnStatus;
  }
  
  public String getTxnTo()
  {
    return this.mTxnTo;
  }
  
  public String getTxnType()
  {
    return this.mTxnType;
  }
  
  public String getType()
  {
    return this.mType;
  }
  
  public String getWalletOrderId()
  {
    return this.mWalletOrderId;
  }
  
  public boolean isBtnEnabled()
  {
    return this.mIsBtnEnabled;
  }
  
  public void setNewNarration(String paramString)
  {
    this.mNewNarration = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRTransaction.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */