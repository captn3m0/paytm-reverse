package net.one97.paytm.common.entity.gamesapps;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.Iterator;
import net.one97.paytm.common.entity.shopping.CJRHomePageLayoutV2;
import net.one97.paytm.common.entity.shopping.CJRHomePageV2;
import net.one97.paytm.common.entity.shopping.LayoutType;

public class CJRGamesHomePage
  extends CJRHomePageV2
{
  @b(a="gamesandapp_layout")
  public ArrayList<CJRHomePageLayoutV2> mGamesHomePageLayoutList = new ArrayList();
  
  public ArrayList<CJRHomePageLayoutV2> gamesAppsPageRowItems()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mGamesHomePageLayoutList.iterator();
    while (localIterator.hasNext())
    {
      CJRHomePageLayoutV2 localCJRHomePageLayoutV2 = (CJRHomePageLayoutV2)localIterator.next();
      LayoutType localLayoutType = LayoutType.fromName(localCJRHomePageLayoutV2.getLayout());
      switch (CJRGamesHomePage.1.$SwitchMap$net$one97$paytm$common$entity$shopping$LayoutType[localLayoutType.ordinal()])
      {
      default: 
        break;
      case 1: 
      case 2: 
      case 3: 
      case 4: 
      case 5: 
        if (localCJRHomePageLayoutV2.getHomePageItemList().size() > 0) {
          localArrayList.add(localCJRHomePageLayoutV2);
        }
        break;
      case 6: 
        localArrayList.add(localCJRHomePageLayoutV2);
      }
    }
    return localArrayList;
  }
  
  public ArrayList<CJRHomePageLayoutV2> getGamesHomePageLayoutList()
  {
    return this.mGamesHomePageLayoutList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/gamesapps/CJRGamesHomePage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */