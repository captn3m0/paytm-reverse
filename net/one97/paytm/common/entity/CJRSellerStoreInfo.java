package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRSellerStoreInfo
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="link")
  private String sellerStoreLink;
  @b(a="url_type")
  private String urlType;
  
  public String getSellerStoreLink()
  {
    return this.sellerStoreLink;
  }
  
  public String getUrlType()
  {
    return this.urlType;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRSellerStoreInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */