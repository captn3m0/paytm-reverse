package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.ArrayList;

public class CJRSmartContactLabel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="l3")
  private ArrayList<CJRSmartContactLabel> mL3Message;
  @b(a="message")
  private String mMessage;
  @b(a="title")
  private String mTitle;
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
  
  public ArrayList<CJRSmartContactLabel> getmL3Message()
  {
    return this.mL3Message;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRSmartContactLabel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */