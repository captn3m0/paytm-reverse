package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.HashMap;

public class CJRNotificationItem
  implements IJRDataModel
{
  @b(a="category")
  private String mCategory;
  @b(a="description1")
  private String mDescription1;
  @b(a="displayName")
  private String mDisplayName;
  @b(a="settings")
  private HashMap<String, String> mSettings;
  
  public String getCategory()
  {
    return this.mCategory;
  }
  
  public String getDescription1()
  {
    return this.mDescription1;
  }
  
  public String getDisplayName()
  {
    return this.mDisplayName;
  }
  
  public HashMap<String, String> getSettings()
  {
    return this.mSettings;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRNotificationItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */