package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRBadage
{
  @b(a="image")
  private String mImage;
  @b(a="key")
  private String mKey;
  @b(a="label")
  private String mLabel;
  
  public String getmImage()
  {
    return this.mImage;
  }
  
  public String getmKey()
  {
    return this.mKey;
  }
  
  public String getmLabel()
  {
    return this.mLabel;
  }
  
  public void setmImage(String paramString)
  {
    this.mImage = paramString;
  }
  
  public void setmKey(String paramString)
  {
    this.mKey = paramString;
  }
  
  public void setmLabel(String paramString)
  {
    this.mLabel = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRBadage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */