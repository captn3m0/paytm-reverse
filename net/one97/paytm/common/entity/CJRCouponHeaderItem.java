package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRCouponHeaderItem
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="image_url")
  private String mImageUrl;
  @b(a="name")
  private String mName;
  @b(a="url")
  private String mUrl;
  @b(a="url_type")
  private String mUrlType;
  
  public String getImageUrl()
  {
    return this.mImageUrl;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getUrl()
  {
    return this.mUrl;
  }
  
  public String getUrlType()
  {
    return this.mUrlType;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRCouponHeaderItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */