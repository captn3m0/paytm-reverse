package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRIllegalCodeError
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="additional_info")
  private ErrorAdditionalInfo additionalInfo;
  @b(a="code")
  private String code;
  @b(a="errorCode")
  private String errorCode;
  @b(a="error")
  private String mError;
  @b(a="error_title")
  private String mErrorTitle;
  @b(a="status")
  private CJRStatusError mStatusError;
  
  public ErrorAdditionalInfo getAdditionalInfo()
  {
    return this.additionalInfo;
  }
  
  public String getCode()
  {
    return this.code;
  }
  
  public String getError()
  {
    return this.mError;
  }
  
  public String getErrorCode()
  {
    return this.errorCode;
  }
  
  public CJRStatusError getStatusError()
  {
    return this.mStatusError;
  }
  
  public String getmErrorTitle()
  {
    return this.mErrorTitle;
  }
  
  public void setStatusError(CJRStatusError paramCJRStatusError)
  {
    this.mStatusError = paramCJRStatusError;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRIllegalCodeError.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */