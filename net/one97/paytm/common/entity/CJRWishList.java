package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.Map;
import net.one97.paytm.common.entity.shopping.CJROffers;

public class CJRWishList
  implements IJRDataModel
{
  @b(a="attributes_dim_values")
  private Map<String, String> mAttributes;
  @b(a="available_qty")
  private int mAvailableQty;
  @b(a="brand")
  private String mBrand;
  @b(a="category_id")
  private String mCategoryId;
  @b(a="count")
  private int mCount;
  @b(a="customer_id")
  private String mCustomerId;
  @b(a="discount")
  private String mDiscount;
  @b(a="image_url")
  private String mImageUrl;
  @b(a="need_shipping")
  private boolean mIsNeedShipping;
  @b(a="merchant_id")
  private String mMerchantId;
  @b(a="merchant_name")
  private String mMerchantName;
  @b(a="mrp")
  private double mMrp;
  @b(a="name")
  private String mName;
  @b(a="latest_offers")
  private CJROffers mOffers;
  @b(a="price")
  private double mPrice;
  @b(a="price_when_added")
  private double mPriceWhenAdded;
  @b(a="product_id")
  private String mProductID;
  @b(a="product_type")
  private int mProductType;
  @b(a="seourl")
  private String mSeoUrl;
  @b(a="url")
  private String mUrl;
  
  public Map<String, String> getmAttributes()
  {
    return this.mAttributes;
  }
  
  public int getmAvailableQty()
  {
    return this.mAvailableQty;
  }
  
  public String getmBrand()
  {
    return this.mBrand;
  }
  
  public String getmCategoryId()
  {
    return this.mCategoryId;
  }
  
  public int getmCount()
  {
    return this.mCount;
  }
  
  public String getmCustomerId()
  {
    return this.mCustomerId;
  }
  
  public String getmDiscount()
  {
    return this.mDiscount;
  }
  
  public String getmImageUrl()
  {
    return this.mImageUrl;
  }
  
  public String getmMerchantId()
  {
    return this.mMerchantId;
  }
  
  public String getmMerchantName()
  {
    return this.mMerchantName;
  }
  
  public double getmMrp()
  {
    return this.mMrp;
  }
  
  public String getmName()
  {
    return this.mName;
  }
  
  public CJROffers getmOffers()
  {
    return this.mOffers;
  }
  
  public double getmPrice()
  {
    return this.mPrice;
  }
  
  public double getmPriceWhenAdded()
  {
    return this.mPriceWhenAdded;
  }
  
  public String getmProductID()
  {
    return this.mProductID;
  }
  
  public int getmProductType()
  {
    return this.mProductType;
  }
  
  public String getmSeoUrl()
  {
    return this.mSeoUrl;
  }
  
  public String getmUrl()
  {
    return this.mUrl;
  }
  
  public boolean ismIsNeedShipping()
  {
    return this.mIsNeedShipping;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRWishList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */