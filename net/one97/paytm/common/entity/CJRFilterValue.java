package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRFilterValue
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  private boolean isToggleChecked;
  @b(a="max")
  private double mAbsoluteMax;
  @b(a="min")
  private double mAbsoluteMin;
  @b(a="count")
  private String mCount;
  @b(a="filter_value_prefix")
  private String mFilterValuePrefix;
  @b(a="filter_value_suffix")
  private String mFilterValueSufix;
  @b(a="id")
  private String mID;
  @b(a="applied")
  private boolean mIsApplied;
  @b(a="exist")
  private boolean mIsExist;
  @b(a="label")
  private String mLabel;
  @b(a="name")
  private String mName;
  private int mSelectedMax;
  private int mSelectedMin;
  @b(a="url")
  private String mUrl;
  @b(a="product_id")
  private String productId;
  
  public boolean equals(Object paramObject)
  {
    try
    {
      paramObject = (CJRFilterValue)paramObject;
      boolean bool = this.mName.equalsIgnoreCase(((CJRFilterValue)paramObject).mName);
      return bool;
    }
    catch (Exception paramObject) {}
    return false;
  }
  
  public int getAbsoluteMax()
  {
    return (int)this.mAbsoluteMax;
  }
  
  public int getAbsoluteMin()
  {
    return (int)this.mAbsoluteMin;
  }
  
  public String getCount()
  {
    return this.mCount;
  }
  
  public String getFilterValuePrefix()
  {
    return this.mFilterValuePrefix;
  }
  
  public String getFilterValueSufix()
  {
    return this.mFilterValueSufix;
  }
  
  public String getID()
  {
    return this.mID;
  }
  
  public boolean getIsExist()
  {
    return this.mIsExist;
  }
  
  public String getLabel()
  {
    return this.mLabel;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getProductId()
  {
    return this.productId;
  }
  
  public int getSelectedMax()
  {
    return this.mSelectedMax;
  }
  
  public int getSelectedMin()
  {
    return this.mSelectedMin;
  }
  
  public String getUrl()
  {
    return this.mUrl;
  }
  
  public boolean isApplied()
  {
    return this.mIsApplied;
  }
  
  public boolean isChecked()
  {
    return this.mIsApplied;
  }
  
  public void setAbsoluteMax(int paramInt)
  {
    this.mAbsoluteMax = paramInt;
  }
  
  public void setAbsoluteMin(int paramInt)
  {
    this.mAbsoluteMin = paramInt;
  }
  
  public void setChecked(boolean paramBoolean)
  {
    this.mIsApplied = paramBoolean;
  }
  
  public void setID(String paramString)
  {
    this.mID = paramString;
  }
  
  public void setLabel(String paramString)
  {
    this.mLabel = paramString;
  }
  
  public void setName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setSelectedMax(int paramInt)
  {
    this.mSelectedMax = paramInt;
  }
  
  public void setSelectedMin(int paramInt)
  {
    this.mSelectedMin = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRFilterValue.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */