package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRContactResponse
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="message ")
  private String mMessage;
  @b(a="message")
  private String mMsg;
  @b(a="messsage")
  private String mMsg2;
  @b(a="status")
  private int mStatus;
  @b(a="ticket_id")
  private String mTicketId;
  
  public String getMsg()
  {
    return this.mMsg;
  }
  
  public int getStatus()
  {
    return this.mStatus;
  }
  
  public String getmMessage()
  {
    return this.mMessage;
  }
  
  public String getmMsg2()
  {
    return this.mMsg2;
  }
  
  public String getmTicketId()
  {
    return this.mTicketId;
  }
  
  public void setMsg(String paramString)
  {
    this.mMsg = paramString;
  }
  
  public void setmMsg2(String paramString)
  {
    this.mMsg2 = paramString;
  }
  
  public void setmTicketId(String paramString)
  {
    this.mTicketId = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRContactResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */