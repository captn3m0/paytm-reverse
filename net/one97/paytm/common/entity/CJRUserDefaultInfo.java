package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRUserDefaultInfo
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="countryCode")
  private String mCountryCode;
  @b(a="customerCreationDate")
  private String mCustomerCreationDate;
  @b(a="customerStatus")
  private String mCustomerStatus;
  @b(a="dob")
  private String mDob;
  @b(a="email")
  private String mEmail;
  @b(a="emailNotificationEnabled")
  private boolean mEmailNotificationEnabled;
  @b(a="emailVerificationStatus")
  private boolean mEmailVerificationStatus;
  @b(a="firstName")
  private String mFirstName;
  @b(a="gender")
  private String mGender;
  @b(a="isKyc")
  private boolean mIsKyc;
  @b(a="lastName")
  private String mLastName;
  @b(a="phone")
  private String mPhone;
  @b(a="phoneVerificationStatus")
  private boolean mPhoneVerificationStatus;
  @b(a="userPicture")
  private String mUserPicture;
  
  public String getCountryCode()
  {
    return this.mCountryCode;
  }
  
  public String getCustomerCreationDate()
  {
    return this.mCustomerCreationDate;
  }
  
  public String getCustomerStatus()
  {
    return this.mCustomerStatus;
  }
  
  public String getDob()
  {
    return this.mDob;
  }
  
  public String getEmail()
  {
    return this.mEmail;
  }
  
  public String getFirstName()
  {
    return this.mFirstName;
  }
  
  public String getGender()
  {
    return this.mGender;
  }
  
  public String getLastName()
  {
    return this.mLastName;
  }
  
  public String getPhone()
  {
    return this.mPhone;
  }
  
  public String getUserPicture()
  {
    return this.mUserPicture;
  }
  
  public boolean isEmailNotificationEnabled()
  {
    return this.mEmailNotificationEnabled;
  }
  
  public boolean isEmailVerificationStatus()
  {
    return this.mEmailVerificationStatus;
  }
  
  public boolean isKyc()
  {
    return this.mIsKyc;
  }
  
  public boolean isPhoneVerificationStatus()
  {
    return this.mPhoneVerificationStatus;
  }
  
  public void setCountryCode(String paramString)
  {
    this.mCountryCode = paramString;
  }
  
  public void setCustomerCreationDate(String paramString)
  {
    this.mCustomerCreationDate = paramString;
  }
  
  public void setCustomerStatus(String paramString)
  {
    this.mCustomerStatus = paramString;
  }
  
  public void setDob(String paramString)
  {
    this.mDob = paramString;
  }
  
  public void setEmail(String paramString)
  {
    this.mEmail = paramString;
  }
  
  public void setEmailNotificationEnabled(boolean paramBoolean)
  {
    this.mEmailNotificationEnabled = paramBoolean;
  }
  
  public void setEmailVerificationStatus(boolean paramBoolean)
  {
    this.mEmailVerificationStatus = paramBoolean;
  }
  
  public void setFirstName(String paramString)
  {
    this.mFirstName = paramString;
  }
  
  public void setGender(String paramString)
  {
    this.mGender = paramString;
  }
  
  public void setIsKyc(boolean paramBoolean)
  {
    this.mIsKyc = paramBoolean;
  }
  
  public void setLastName(String paramString)
  {
    this.mLastName = paramString;
  }
  
  public void setPhone(String paramString)
  {
    this.mPhone = paramString;
  }
  
  public void setPhoneVerificationStatus(boolean paramBoolean)
  {
    this.mPhoneVerificationStatus = paramBoolean;
  }
  
  public void setUserPicture(String paramString)
  {
    this.mUserPicture = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRUserDefaultInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */