package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrowsePlanDescription
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="actual_price")
  private String mActualPrice;
  @b(a="brand")
  private String mBrand;
  @b(a="long_rich_desc")
  private ArrayList<CJRBrowsePlansRichDesc> mLongRichDesc;
  @b(a="merchant_name")
  private String mMerchantName;
  @b(a="name")
  private String mName;
  @b(a="offer_price")
  private String mOfferPrice;
  @b(a="")
  private String mProductId;
  @b(a="promo_text")
  private String mPromoText;
  @b(a="seourl")
  private String mSeoUrl;
  @b(a="short_desc")
  private String mShortDesc;
  @b(a="tag")
  private String mTag;
  @b(a="url")
  private String mUrl;
  
  public String getActualPrice()
  {
    return this.mActualPrice;
  }
  
  public String getBrand()
  {
    return this.mBrand;
  }
  
  public ArrayList<CJRBrowsePlansRichDesc> getLongRichDesc()
  {
    return this.mLongRichDesc;
  }
  
  public String getMerchantName()
  {
    return this.mMerchantName;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getOfferPrice()
  {
    return this.mOfferPrice;
  }
  
  public String getProductId()
  {
    return this.mProductId;
  }
  
  public String getPromoText()
  {
    return this.mPromoText;
  }
  
  public String getSeoUrl()
  {
    return this.mSeoUrl;
  }
  
  public String getShortDesc()
  {
    return this.mShortDesc;
  }
  
  public String getTag()
  {
    return this.mTag;
  }
  
  public String getUrl()
  {
    return this.mUrl;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRBrowsePlanDescription.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */