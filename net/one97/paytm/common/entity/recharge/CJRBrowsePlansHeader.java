package net.one97.paytm.common.entity.recharge;

import android.text.TextUtils;
import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrowsePlansHeader
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="items")
  private ArrayList<CJRBrowsePlanHeader> mBrowsePlans;
  private String mCircle;
  private String mOperator;
  private String mType;
  
  public boolean checkBrowsePlanResponse(String paramString1, String paramString2, String paramString3)
  {
    return (!TextUtils.isEmpty(paramString1)) && (!TextUtils.isEmpty(this.mType)) && (this.mType.equalsIgnoreCase(paramString1)) && (!TextUtils.isEmpty(paramString2)) && (!TextUtils.isEmpty(this.mOperator)) && (this.mOperator.equalsIgnoreCase(paramString2)) && ((this.mType.equalsIgnoreCase("dth")) || ((!TextUtils.isEmpty(paramString3)) && (!TextUtils.isEmpty(this.mCircle)) && (this.mCircle.equalsIgnoreCase(paramString3))));
  }
  
  public ArrayList<CJRBrowsePlanHeader> getBrowsePlans()
  {
    return this.mBrowsePlans;
  }
  
  public String getCircle()
  {
    return this.mCircle;
  }
  
  public String getOperator()
  {
    return this.mOperator;
  }
  
  public String getType()
  {
    return this.mType;
  }
  
  public void setCircle(String paramString)
  {
    this.mCircle = paramString;
  }
  
  public void setOperator(String paramString)
  {
    this.mOperator = paramString;
  }
  
  public void setType(String paramString)
  {
    this.mType = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRBrowsePlansHeader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */