package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRNpsTrackingResponse
  implements IJRDataModel
{
  @b(a="message")
  private String message;
  
  public String getMessage()
  {
    return this.message;
  }
  
  public void setMessage(String paramString)
  {
    this.message = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRNpsTrackingResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */