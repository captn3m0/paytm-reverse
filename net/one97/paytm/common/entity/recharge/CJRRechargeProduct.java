package net.one97.paytm.common.entity.recharge;

import android.util.Log;
import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.CJRError;
import net.one97.paytm.common.entity.CJRProduct;

public class CJRRechargeProduct
  extends CJRProduct
{
  private static final long serialVersionUID = 1L;
  @b(a="circle")
  protected String mCircle;
  private ArrayList<CJRCircle> mCircles;
  @b(a="configurations")
  private CJRRechargeConfiguration mConfiguration;
  @b(a="convenience_fee")
  private String mConvenientFee;
  @b(a="error")
  private CJRError mError;
  @b(a="fetch_amt")
  protected boolean mFetchAmt;
  @b(a="form_fields")
  private ArrayList<CJRInputFields> mFormFields;
  @b(a="input_fields")
  private ArrayList<CJRInputFields> mInputFields;
  @b(a="max_amount")
  private int mMaxAmount;
  @b(a="min_amount")
  private int mMinAmount;
  @b(a="image")
  private String mOperatorImageUrl;
  @b(a="operator_label")
  private String mOperatorLabel;
  @b(a="prefetch")
  private int mPrefetch;
  @b(a="product_id")
  protected String mProductId;
  @b(a="regEx")
  private String mRegExp;
  @b(a="short_desc")
  private String mShortDesc;
  @b(a="softblock")
  private boolean mSoftblock;
  @b(a="status")
  private String mStatus;
  @b(a="type")
  protected String mType;
  
  public String getBrand()
  {
    return this.mBrand;
  }
  
  public String getCircle()
  {
    return this.mCircle;
  }
  
  public ArrayList<CJRCircle> getCircles()
  {
    return this.mCircles;
  }
  
  public CJRRechargeConfiguration getConfiguration()
  {
    return this.mConfiguration;
  }
  
  public String getConvenientFee()
  {
    return this.mConvenientFee;
  }
  
  public CJRError getError()
  {
    return this.mError;
  }
  
  public ArrayList<CJRInputFields> getFormFields()
  {
    return this.mFormFields;
  }
  
  public ArrayList<CJRInputFields> getInputFields()
  {
    return this.mInputFields;
  }
  
  public int getMaxAmount()
  {
    return this.mMaxAmount;
  }
  
  public int getMinAmount()
  {
    return this.mMinAmount;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getOperatorLabel()
  {
    return this.mOperatorLabel;
  }
  
  public String getProductType()
  {
    return this.mProductType;
  }
  
  public String getRegExp()
  {
    return this.mRegExp;
  }
  
  public String getShortDesc()
  {
    return this.mShortDesc;
  }
  
  public boolean getSoftblock()
  {
    return this.mSoftblock;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public String getType()
  {
    Log.v("ratnakar type is ", "hello" + this.mType);
    return this.mType;
  }
  
  public String getmOperatorImageUrl()
  {
    return this.mOperatorImageUrl;
  }
  
  public int getmPrefetch()
  {
    return this.mPrefetch;
  }
  
  public String getmProductId()
  {
    return this.mProductId;
  }
  
  public boolean isFetchable()
  {
    return this.mFetchAmt;
  }
  
  public void setCircle(String paramString)
  {
    this.mCircle = paramString;
  }
  
  public void setCircles(ArrayList<CJRCircle> paramArrayList)
  {
    this.mCircles = paramArrayList;
  }
  
  public void setConfiguration(CJRRechargeConfiguration paramCJRRechargeConfiguration)
  {
    this.mConfiguration = paramCJRRechargeConfiguration;
  }
  
  public void setRegExp(String paramString)
  {
    this.mRegExp = paramString;
  }
  
  public void setType(String paramString)
  {
    this.mType = paramString;
  }
  
  public void setmOperatorImageUrl(String paramString)
  {
    this.mOperatorImageUrl = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRRechargeProduct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */