package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRIndicativePlan
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="category")
  private String mCategory;
  @b(a="circle")
  private String mCircle;
  @b(a="full_desc")
  private String mFullDescription;
  @b(a="operator")
  private String mOperator;
  @b(a="short_desc")
  private String mShortDescription;
  @b(a="talktime")
  private String mTalktime;
  @b(a="type")
  private String mType;
  @b(a="validity")
  private String mValidity;
  @b(a="value")
  private String mValue;
  
  public String getCategory()
  {
    return this.mCategory;
  }
  
  public String getCircle()
  {
    return this.mCircle;
  }
  
  public String getFullDescription()
  {
    return this.mFullDescription;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getOperator()
  {
    return this.mOperator;
  }
  
  public String getShortDescription()
  {
    return this.mShortDescription;
  }
  
  public String getTalktime()
  {
    return this.mTalktime;
  }
  
  public String getType()
  {
    return this.mType;
  }
  
  public String getValidity()
  {
    return this.mValidity;
  }
  
  public String getValue()
  {
    return this.mValue;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRIndicativePlan.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */