package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrowsePlansRichDesc
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="attributes")
  private CJRBrowsePlansDescAttributes mAttributes;
  @b(a="description")
  private String mDescription;
  @b(a="title")
  private String mTitle;
  
  public CJRBrowsePlansDescAttributes getAttributes()
  {
    return this.mAttributes;
  }
  
  public String getDescription()
  {
    return this.mDescription;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRBrowsePlansRichDesc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */