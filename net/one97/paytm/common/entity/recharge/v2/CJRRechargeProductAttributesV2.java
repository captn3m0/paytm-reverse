package net.one97.paytm.common.entity.recharge.v2;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRRechargeProductAttributesV2
  implements IJRDataModel
{
  private CJRRechargeProductAttributesAlertV2 alertV2;
  private String circleDisplay;
  private String circleFilter;
  private CJRRechargeProductAttributesErrorV2 errorV2;
  private int fetch_amount;
  private String max_amount;
  private String min_amount;
  private String operatorDisplay;
  private String operatorFilter;
  private int prefetch;
  private String rechargeTypeDisplayName;
  private String rechargeTypeFilterName;
  private String regEx;
  @b(a="status")
  private String status;
  
  public CJRRechargeProductAttributesAlertV2 getAlertV2()
  {
    return this.alertV2;
  }
  
  public String getCircleDisplay()
  {
    return this.circleDisplay;
  }
  
  public String getCircleFilter()
  {
    return this.circleFilter;
  }
  
  public CJRRechargeProductAttributesErrorV2 getErrorV2()
  {
    return this.errorV2;
  }
  
  public int getFetch_amount()
  {
    return this.fetch_amount;
  }
  
  public String getMax_amount()
  {
    return this.max_amount;
  }
  
  public String getMin_amount()
  {
    return this.min_amount;
  }
  
  public String getOperatorDisplay()
  {
    return this.operatorDisplay;
  }
  
  public String getOperatorFilter()
  {
    return this.operatorFilter;
  }
  
  public int getPrefetch()
  {
    return this.prefetch;
  }
  
  public String getRechargeTypeDisplayName()
  {
    return this.rechargeTypeDisplayName;
  }
  
  public String getRechargeTypeFilterName()
  {
    return this.rechargeTypeFilterName;
  }
  
  public String getRegEx()
  {
    return this.regEx;
  }
  
  public String getStatus()
  {
    return this.status;
  }
  
  public void setAlertV2(CJRRechargeProductAttributesAlertV2 paramCJRRechargeProductAttributesAlertV2)
  {
    this.alertV2 = paramCJRRechargeProductAttributesAlertV2;
  }
  
  public void setCircleDisplay(String paramString)
  {
    this.circleDisplay = paramString;
  }
  
  public void setCircleFilter(String paramString)
  {
    this.circleFilter = paramString;
  }
  
  public void setErrorV2(CJRRechargeProductAttributesErrorV2 paramCJRRechargeProductAttributesErrorV2)
  {
    this.errorV2 = paramCJRRechargeProductAttributesErrorV2;
  }
  
  public void setFetch_amount(int paramInt)
  {
    this.fetch_amount = paramInt;
  }
  
  public void setMax_amount(String paramString)
  {
    this.max_amount = paramString;
  }
  
  public void setMin_amount(String paramString)
  {
    this.min_amount = paramString;
  }
  
  public void setOperatorDisplay(String paramString)
  {
    this.operatorDisplay = paramString;
  }
  
  public void setOperatorFilter(String paramString)
  {
    this.operatorFilter = paramString;
  }
  
  public void setPrefetch(int paramInt)
  {
    this.prefetch = paramInt;
  }
  
  public void setRechargeTypeDisplayName(String paramString)
  {
    this.rechargeTypeDisplayName = paramString;
  }
  
  public void setRechargeTypeFilterName(String paramString)
  {
    this.rechargeTypeFilterName = paramString;
  }
  
  public void setRegEx(String paramString)
  {
    this.regEx = paramString;
  }
  
  public void setStatus(String paramString)
  {
    this.status = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/v2/CJRRechargeProductAttributesV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */