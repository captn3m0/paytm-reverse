package net.one97.paytm.common.entity.recharge.v2;

import android.text.TextUtils;
import com.google.c.a.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.one97.paytm.common.entity.IJRDataModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CJRRechargeProductListV2
  implements IJRDataModel
{
  @b(a="ff_text")
  private String ff_text;
  @b(a="group_arr")
  private List<List<String>> groupArr;
  @b(a="grouping")
  private CJRRechargeProductListGroupArrV2 groupingData;
  @b(a="heading")
  private String heading;
  private int id;
  @b(a="long_rich_desc")
  private String longRichDesc;
  private HashMap<RechargeProductListKeyV2, List<CJRRechargeProductV2>> mOperatorCircleToProductMap;
  private Map<String, LocalizedOperatorIcon> mOperatorIconMap;
  private HashMap<String, List<CJRRechargeProductV2>> mOperatorToProductMap;
  private Long mServerResponseTime;
  private String mUrlType;
  @b(a="message")
  private String message;
  @b(a="meta_description")
  private String metaDescription;
  @b(a="meta_keyword")
  private String metaKeyword;
  @b(a="meta_title")
  private String metaTitle;
  private String name;
  private List<CJRRechargeProductV2> productListV2;
  @b(a="showFastforward")
  private boolean showFastForward;
  @b(a="storefront_url")
  private String storeFrontUrl;
  
  private CJRRechargeProductV2 populateProductsFromJson(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, JSONObject paramJSONObject)
    throws JSONException
  {
    Object localObject = paramJSONObject.getJSONObject("products");
    paramJSONObject = new CJRRechargeProductV2();
    int i = ((JSONObject)localObject).optInt("id");
    if (i != 0) {
      paramJSONObject.setProductId(String.valueOf(i));
    }
    if (((JSONObject)localObject).has("attributes"))
    {
      JSONObject localJSONObject = ((JSONObject)localObject).getJSONObject("attributes");
      CJRRechargeProductAttributesV2 localCJRRechargeProductAttributesV2 = new CJRRechargeProductAttributesV2();
      localCJRRechargeProductAttributesV2.setRechargeTypeDisplayName(paramString6);
      localCJRRechargeProductAttributesV2.setRechargeTypeFilterName(paramString5);
      i = localJSONObject.optInt("prefetch");
      if (i != 0) {
        localCJRRechargeProductAttributesV2.setPrefetch(i);
      }
      i = localJSONObject.optInt("fetch_amount");
      if (i != 0) {
        localCJRRechargeProductAttributesV2.setFetch_amount(i);
      }
      paramString5 = localJSONObject.optJSONObject("error");
      if (paramString5 != null)
      {
        paramString6 = new CJRRechargeProductAttributesErrorV2();
        paramString6.setTitle(paramString5.optString("title"));
        paramString6.setProceed(paramString5.optInt("proceed"));
        paramString6.setMessage(paramString5.optString("message"));
        paramString6.setOkButton(paramString5.optString("okbutton"));
        paramString6.setCancelButton(paramString5.optString("cancelButton"));
        localCJRRechargeProductAttributesV2.setErrorV2(paramString6);
      }
      paramString5 = localJSONObject.optJSONObject("alert");
      if (paramString5 != null)
      {
        paramString6 = new CJRRechargeProductAttributesAlertV2();
        paramString6.setTitle(paramString5.optString("title"));
        paramString6.setHeading(paramString5.optString("heading"));
        paramString6.setMessage(paramString5.optString("message"));
        paramString6.setOkButton(paramString5.optString("okbutton"));
        paramString6.setCancelButton(paramString5.optString("cancelButton"));
        localCJRRechargeProductAttributesV2.setAlertV2(paramString6);
      }
      localCJRRechargeProductAttributesV2.setCircleFilter(paramString3);
      localCJRRechargeProductAttributesV2.setCircleDisplay(paramString4);
      localCJRRechargeProductAttributesV2.setOperatorFilter(paramString1);
      localCJRRechargeProductAttributesV2.setOperatorDisplay(paramString2);
      paramJSONObject.setRechargeProductAttributesV2(localCJRRechargeProductAttributesV2);
    }
    if (((JSONObject)localObject).has("input_fields"))
    {
      paramString2 = ((JSONObject)localObject).getJSONArray("input_fields");
      paramString4 = new ArrayList();
      i = 0;
      while (i < paramString2.length())
      {
        paramString5 = new CJRRechargeInputFieldsTypeV2();
        paramString6 = paramString2.getJSONObject(i);
        if (paramString6.has("config_key")) {
          paramString5.setConfigKey(paramString6.getString("config_key"));
        }
        if (paramString6.has("title")) {
          paramString5.setTitle(paramString6.getString("title"));
        }
        if (paramString6.has("regex"))
        {
          localObject = paramString6.getString("regex");
          paramString5.setRegex((String)localObject);
          if ((paramJSONObject.getRechargeProductAttributesV2() != null) && (TextUtils.isEmpty(paramJSONObject.getRechargeProductAttributesV2().getRegEx()))) {
            paramJSONObject.getRechargeProductAttributesV2().setRegEx((String)localObject);
          }
        }
        if (paramString6.has("type")) {
          paramString5.setType(paramString6.getString("type"));
        }
        if (paramString6.has("mandatory")) {
          paramString5.setMandatory(paramString6.getBoolean("mandatory"));
        }
        int j;
        if (paramString6.has("min"))
        {
          j = paramString6.getInt("min");
          paramString5.setMin(j);
          if ((paramJSONObject.getRechargeProductAttributesV2() != null) && (TextUtils.isEmpty(paramJSONObject.getRechargeProductAttributesV2().getMin_amount()))) {
            paramJSONObject.getRechargeProductAttributesV2().setMin_amount(String.valueOf(j));
          }
        }
        if (paramString6.has("max"))
        {
          j = paramString6.getInt("max");
          paramString5.setMax(j);
          if ((paramJSONObject.getRechargeProductAttributesV2() != null) && (TextUtils.isEmpty(paramJSONObject.getRechargeProductAttributesV2().getMax_amount()))) {
            paramJSONObject.getRechargeProductAttributesV2().setMax_amount(String.valueOf(j));
          }
        }
        paramString4.add(paramString5);
        i += 1;
      }
      paramJSONObject.setInputFieldsTypeV2List(paramString4);
    }
    if (paramJSONObject != null)
    {
      this.productListV2.add(paramJSONObject);
      if ((!TextUtils.isEmpty(paramString1)) && (!TextUtils.isEmpty(paramString3)))
      {
        paramString3 = new RechargeProductListKeyV2(paramString1, paramString3);
        if (!this.mOperatorCircleToProductMap.containsKey(paramString3)) {
          break label798;
        }
        paramString2 = (List)this.mOperatorCircleToProductMap.get(paramString3);
        paramString2.add(paramJSONObject);
        if (!this.mOperatorToProductMap.containsKey(paramString1)) {
          break label819;
        }
      }
    }
    for (paramString1 = (List)this.mOperatorToProductMap.get(paramString1);; paramString1 = paramString2)
    {
      paramString1.add(paramJSONObject);
      return paramJSONObject;
      label798:
      paramString2 = new ArrayList();
      this.mOperatorCircleToProductMap.put(paramString3, paramString2);
      break;
      label819:
      paramString2 = new ArrayList();
      this.mOperatorToProductMap.put(paramString1, paramString2);
    }
  }
  
  public String getFf_text()
  {
    return this.ff_text;
  }
  
  public List<List<String>> getGroupArr()
  {
    return this.groupArr;
  }
  
  public CJRRechargeProductListGroupArrV2 getGroupingData()
  {
    return this.groupingData;
  }
  
  public String getHeading()
  {
    return this.heading;
  }
  
  public int getId()
  {
    return this.id;
  }
  
  public String getLongRichDesc()
  {
    return this.longRichDesc;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public String getMetaDescription()
  {
    return this.metaDescription;
  }
  
  public String getMetaKeyword()
  {
    return this.metaKeyword;
  }
  
  public String getMetaTitle()
  {
    return this.metaTitle;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public Map<RechargeProductListKeyV2, List<CJRRechargeProductV2>> getOperatorCircleToProductMap()
  {
    return this.mOperatorCircleToProductMap;
  }
  
  public Map<String, List<CJRRechargeProductV2>> getOperatorToProductMap()
  {
    return this.mOperatorToProductMap;
  }
  
  public List<CJRRechargeProductV2> getProductListV2()
  {
    return this.productListV2;
  }
  
  public Long getServerResponseTime()
  {
    return this.mServerResponseTime;
  }
  
  public boolean getShowFastForward()
  {
    return this.showFastForward;
  }
  
  public String getStoreFrontUrl()
  {
    return this.storeFrontUrl;
  }
  
  public String getUrlType()
  {
    return this.mUrlType;
  }
  
  public Map<String, LocalizedOperatorIcon> getmOperatorIconMap()
  {
    return this.mOperatorIconMap;
  }
  
  public void parseJsonData(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {}
    for (;;)
    {
      return;
      try
      {
        paramString = new JSONObject(paramString);
        if (paramString.has("id")) {
          setId(paramString.getInt("id"));
        }
        if (paramString.has("name")) {
          setName(paramString.getString("name"));
        }
        if (paramString.has("meta_title")) {
          setMetaTitle(paramString.getString("meta_title"));
        }
        if (paramString.has("long_rich_desc")) {
          setLongRichDesc(paramString.getString("long_rich_desc"));
        }
        if (paramString.has("meta_description")) {
          setMetaDescription(paramString.getString("meta_description"));
        }
        if (paramString.has("heading")) {
          setHeading(paramString.getString("heading"));
        }
        if (paramString.has("showFastforward")) {
          setShowFastForward(paramString.getBoolean("showFastforward"));
        }
        if (paramString.has("ff_text")) {
          setFf_text(paramString.getString("ff_text"));
        }
        Object localObject2;
        Object localObject3;
        Object localObject4;
        int j;
        if (paramString.has("group_arr"))
        {
          localObject1 = paramString.getJSONArray("group_arr");
          localObject2 = new ArrayList();
          i = 0;
          while (i < ((JSONArray)localObject1).length())
          {
            localObject3 = ((JSONArray)localObject1).getJSONArray(i);
            localObject4 = new ArrayList();
            j = 0;
            while (j < ((JSONArray)localObject3).length())
            {
              ((ArrayList)localObject4).add(((JSONArray)localObject3).getString(j));
              j += 1;
            }
            ((ArrayList)localObject2).add(localObject4);
            i += 1;
          }
          setGroupArr((List)localObject2);
        }
        if (paramString.has("grouping"))
        {
          localObject1 = paramString.getJSONObject("grouping");
          this.groupingData = new CJRRechargeProductListGroupArrV2();
          if (((JSONObject)localObject1).has("operator"))
          {
            localObject2 = ((JSONObject)localObject1).getJSONObject("operator");
            localObject3 = new CJRRechargeProductListGroupArrItemV2();
            ((CJRRechargeProductListGroupArrItemV2)localObject3).setKey(((JSONObject)localObject2).optString("key"));
            ((CJRRechargeProductListGroupArrItemV2)localObject3).setBrandImage(((JSONObject)localObject2).optBoolean("brandImage"));
            ((CJRRechargeProductListGroupArrItemV2)localObject3).setLabel(((JSONObject)localObject2).optString("label"));
            ((CJRRechargeProductListGroupArrItemV2)localObject3).setType(((JSONObject)localObject2).optString("type"));
            this.groupingData.setOperatorItem((CJRRechargeProductListGroupArrItemV2)localObject3);
          }
          if (((JSONObject)localObject1).has("circle"))
          {
            localObject2 = ((JSONObject)localObject1).getJSONObject("circle");
            localObject3 = new CJRRechargeProductListGroupArrItemV2();
            ((CJRRechargeProductListGroupArrItemV2)localObject3).setKey(((JSONObject)localObject2).optString("key"));
            ((CJRRechargeProductListGroupArrItemV2)localObject3).setBrandImage(((JSONObject)localObject2).optBoolean("brandImage"));
            ((CJRRechargeProductListGroupArrItemV2)localObject3).setLabel(((JSONObject)localObject2).optString("label"));
            ((CJRRechargeProductListGroupArrItemV2)localObject3).setType(((JSONObject)localObject2).optString("type"));
            this.groupingData.setCircleItem((CJRRechargeProductListGroupArrItemV2)localObject3);
          }
          if (((JSONObject)localObject1).has("producttype"))
          {
            localObject1 = ((JSONObject)localObject1).getJSONObject("producttype");
            localObject2 = new CJRRechargeProductListGroupArrItemV2();
            ((CJRRechargeProductListGroupArrItemV2)localObject2).setKey(((JSONObject)localObject1).optString("key"));
            ((CJRRechargeProductListGroupArrItemV2)localObject2).setBrandImage(((JSONObject)localObject1).optBoolean("brandImage"));
            ((CJRRechargeProductListGroupArrItemV2)localObject2).setLabel(((JSONObject)localObject1).optString("label"));
            ((CJRRechargeProductListGroupArrItemV2)localObject2).setType(((JSONObject)localObject1).optString("type"));
            this.groupingData.setProductTypeItem((CJRRechargeProductListGroupArrItemV2)localObject2);
          }
        }
        if ((this.groupingData == null) || (!paramString.has("product_list"))) {
          continue;
        }
        Object localObject1 = paramString.getJSONArray("product_list");
        this.productListV2 = new ArrayList();
        this.mOperatorCircleToProductMap = new HashMap();
        this.mOperatorToProductMap = new HashMap();
        this.mOperatorIconMap = new HashMap();
        int i = 0;
        while (i < ((JSONArray)localObject1).length())
        {
          if (this.groupingData.getOperatorItem() != null)
          {
            localObject4 = ((JSONArray)localObject1).getJSONObject(i);
            localObject2 = ((JSONObject)localObject4).optString("filter_name");
            localObject3 = ((JSONObject)localObject4).optString("display_name");
            paramString = null;
            if (((JSONObject)localObject4).has("attributes")) {
              paramString = ((JSONObject)localObject4).optJSONObject("attributes").optString("image");
            }
            Object localObject5 = new LocalizedOperatorIcon();
            ((LocalizedOperatorIcon)localObject5).setOperatorDisplayName((String)localObject3);
            ((LocalizedOperatorIcon)localObject5).setOperatorIconUrl(paramString);
            this.mOperatorIconMap.put(localObject2, localObject5);
            paramString = ((JSONObject)localObject4).getJSONArray("variants");
            j = 0;
            while (j < paramString.length())
            {
              if (this.groupingData.getCircleItem() != null)
              {
                Object localObject6 = paramString.getJSONObject(j);
                localObject4 = ((JSONObject)localObject6).optString("filter_name");
                localObject5 = ((JSONObject)localObject6).optString("display_name");
                if (this.groupingData.getProductType() != null)
                {
                  localObject6 = ((JSONObject)localObject6).getJSONArray("variants");
                  int k = 0;
                  while (k < ((JSONArray)localObject6).length())
                  {
                    JSONObject localJSONObject = ((JSONArray)localObject6).getJSONObject(k);
                    populateProductsFromJson((String)localObject2, (String)localObject3, (String)localObject4, (String)localObject5, localJSONObject.optString("filter_name"), localJSONObject.optString("display_name"), localJSONObject);
                    k += 1;
                  }
                }
                populateProductsFromJson((String)localObject2, (String)localObject3, (String)localObject4, (String)localObject5, null, null, (JSONObject)localObject6);
              }
              j += 1;
            }
          }
          i += 1;
        }
        return;
      }
      catch (Exception paramString)
      {
        paramString.printStackTrace();
      }
    }
  }
  
  public void setFf_text(String paramString)
  {
    this.ff_text = paramString;
  }
  
  public void setGroupArr(List<List<String>> paramList)
  {
    this.groupArr = paramList;
  }
  
  public void setGroupingData(CJRRechargeProductListGroupArrV2 paramCJRRechargeProductListGroupArrV2)
  {
    this.groupingData = paramCJRRechargeProductListGroupArrV2;
  }
  
  public void setHeading(String paramString)
  {
    this.heading = paramString;
  }
  
  public void setId(int paramInt)
  {
    this.id = paramInt;
  }
  
  public void setLongRichDesc(String paramString)
  {
    this.longRichDesc = paramString;
  }
  
  public void setMetaDescription(String paramString)
  {
    this.metaDescription = paramString;
  }
  
  public void setMetaTitle(String paramString)
  {
    this.metaTitle = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setOperatorCircleToProductMap(Map<RechargeProductListKeyV2, List<CJRRechargeProductV2>> paramMap)
  {
    this.mOperatorCircleToProductMap = new HashMap(paramMap);
  }
  
  public void setOperatorToProductMap(HashMap<String, List<CJRRechargeProductV2>> paramHashMap)
  {
    this.mOperatorToProductMap = paramHashMap;
  }
  
  public void setProductListV2(List<CJRRechargeProductV2> paramList)
  {
    this.productListV2 = paramList;
  }
  
  public void setServerResponseTime(Long paramLong)
  {
    this.mServerResponseTime = paramLong;
  }
  
  public void setShowFastForward(boolean paramBoolean)
  {
    this.showFastForward = paramBoolean;
  }
  
  public void setUrlType(String paramString)
  {
    this.mUrlType = paramString;
  }
  
  public void setmOperatorIconMap(Map<String, LocalizedOperatorIcon> paramMap)
  {
    this.mOperatorIconMap = paramMap;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/v2/CJRRechargeProductListV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */