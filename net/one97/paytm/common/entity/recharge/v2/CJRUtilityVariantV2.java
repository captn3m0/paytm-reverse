package net.one97.paytm.common.entity.recharge.v2;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRUtilityVariantV2
  implements IJRDataModel
{
  @b(a="display_name")
  private String displayName;
  @b(a="filter_name")
  private String filterName;
  @b(a="products")
  private CJRUtilityProductV2 product;
  @b(a="variants")
  private List<CJRUtilityProductV2> variants = new ArrayList();
  
  public String getDisplayName()
  {
    return this.displayName;
  }
  
  public String getFilterName()
  {
    return this.filterName;
  }
  
  public CJRUtilityProductV2 getProduct()
  {
    return this.product;
  }
  
  public List<CJRUtilityProductV2> getVariants()
  {
    return this.variants;
  }
  
  public void setDisplayName(String paramString)
  {
    this.displayName = paramString;
  }
  
  public void setFilterName(String paramString)
  {
    this.filterName = paramString;
  }
  
  public void setProduct(CJRUtilityProductV2 paramCJRUtilityProductV2)
  {
    this.product = paramCJRUtilityProductV2;
  }
  
  public void setVariants(List<CJRUtilityProductV2> paramList)
  {
    this.variants = paramList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/v2/CJRUtilityVariantV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */