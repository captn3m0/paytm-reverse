package net.one97.paytm.common.entity.recharge.v2;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRUtilityGroupFieldV2
  implements IJRDataModel
{
  @b(a="brandImage")
  private Boolean brandImage;
  @b(a="key")
  private String key;
  @b(a="label")
  private String label;
  @b(a="multiple_selection")
  private Boolean multipleSelection;
  @b(a="showfield")
  private Boolean showfield;
  @b(a="type")
  private String type;
  
  public Boolean getBrandImage()
  {
    return this.showfield;
  }
  
  public String getKey()
  {
    return this.key;
  }
  
  public String getLabel()
  {
    return this.label;
  }
  
  public Boolean getMultipleSelection()
  {
    return this.multipleSelection;
  }
  
  public Boolean getShowfield()
  {
    return this.showfield;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public void setBrandImage(Boolean paramBoolean)
  {
    this.brandImage = paramBoolean;
  }
  
  public void setKey(String paramString)
  {
    this.key = paramString;
  }
  
  public void setLabel(String paramString)
  {
    this.label = paramString;
  }
  
  public void setMultipleSelection(Boolean paramBoolean)
  {
    this.multipleSelection = paramBoolean;
  }
  
  public void setShowfield(Boolean paramBoolean)
  {
    this.showfield = paramBoolean;
  }
  
  public void setType(String paramString)
  {
    this.type = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/v2/CJRUtilityGroupFieldV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */