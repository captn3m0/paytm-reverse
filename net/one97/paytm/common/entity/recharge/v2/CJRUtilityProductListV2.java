package net.one97.paytm.common.entity.recharge.v2;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRUtilityProductListV2
  implements IJRDataModel
{
  @b(a="button_obj")
  private CJRUtilityButtonObjectV2 buttonObj;
  @b(a="canonical_url")
  private String canonicalUrl;
  @b(a="disclaimer")
  private String disclaimer;
  @b(a="error_obj")
  private CJRUtilityErrorV2 errorObj;
  @b(a="extra_description")
  private Object extraDescription;
  @b(a="ff_text")
  private String ffText;
  @b(a="ga_key")
  private String gaKey;
  @b(a="grouping")
  private Map<String, CJRUtilityGroupFieldV2> groupFieldMap;
  @b(a="group_arr")
  private List<List<String>> grouping = new ArrayList();
  @b(a="heading")
  private String heading;
  @b(a="long_rich_desc")
  private String longRichDesc;
  @b(a="message")
  private String message;
  @b(a="meta_description")
  private String metaDescription;
  @b(a="meta_keyword")
  private String metaKeyword;
  @b(a="meta_title")
  private String metaTitle;
  @b(a="name")
  private String name;
  @b(a="showFastforward")
  private Boolean showFastforward;
  @b(a="storefront_url")
  private Object storefrontUrl;
  @b(a="product_list")
  private List<CJRUtilityVariantV2> variantList = new ArrayList();
  
  public CJRUtilityButtonObjectV2 getButtonObj()
  {
    return this.buttonObj;
  }
  
  public String getCanonicalUrl()
  {
    return this.canonicalUrl;
  }
  
  public String getDisclaimer()
  {
    return this.disclaimer;
  }
  
  public CJRUtilityErrorV2 getErrorObj()
  {
    return this.errorObj;
  }
  
  public Object getExtraDescription()
  {
    return this.extraDescription;
  }
  
  public String getFfText()
  {
    return this.ffText;
  }
  
  public String getGaKey()
  {
    return this.gaKey;
  }
  
  public Map<String, CJRUtilityGroupFieldV2> getGroupFieldMap()
  {
    return this.groupFieldMap;
  }
  
  public List<List<String>> getGrouping()
  {
    return this.grouping;
  }
  
  public String getHeading()
  {
    return this.heading;
  }
  
  public String getLongRichDesc()
  {
    return this.longRichDesc;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public String getMetaDescription()
  {
    return this.metaDescription;
  }
  
  public String getMetaKeyword()
  {
    return this.metaKeyword;
  }
  
  public String getMetaTitle()
  {
    return this.metaTitle;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public Boolean getShowFastforward()
  {
    return this.showFastforward;
  }
  
  public Object getStorefrontUrl()
  {
    return this.storefrontUrl;
  }
  
  public List<CJRUtilityVariantV2> getVariantList()
  {
    return this.variantList;
  }
  
  public void setButtonObj(CJRUtilityButtonObjectV2 paramCJRUtilityButtonObjectV2)
  {
    this.buttonObj = paramCJRUtilityButtonObjectV2;
  }
  
  public void setCanonicalUrl(String paramString)
  {
    this.canonicalUrl = paramString;
  }
  
  public void setDisclaimer(String paramString)
  {
    this.disclaimer = paramString;
  }
  
  public void setErrorObj(CJRUtilityErrorV2 paramCJRUtilityErrorV2)
  {
    this.errorObj = paramCJRUtilityErrorV2;
  }
  
  public void setExtraDescription(Object paramObject)
  {
    this.extraDescription = paramObject;
  }
  
  public void setFfText(String paramString)
  {
    this.ffText = paramString;
  }
  
  public void setGaKey(String paramString)
  {
    this.gaKey = paramString;
  }
  
  public void setGroupFieldMap(Map<String, CJRUtilityGroupFieldV2> paramMap)
  {
    this.groupFieldMap = paramMap;
  }
  
  public void setGrouping(List<List<String>> paramList)
  {
    this.grouping = paramList;
  }
  
  public void setHeading(String paramString)
  {
    this.heading = paramString;
  }
  
  public void setLongRichDesc(String paramString)
  {
    this.longRichDesc = paramString;
  }
  
  public void setMessage(String paramString)
  {
    this.message = paramString;
  }
  
  public void setMetaDescription(String paramString)
  {
    this.metaDescription = paramString;
  }
  
  public void setMetaKeyword(String paramString)
  {
    this.metaKeyword = paramString;
  }
  
  public void setMetaTitle(String paramString)
  {
    this.metaTitle = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setShowFastforward(Boolean paramBoolean)
  {
    this.showFastforward = paramBoolean;
  }
  
  public void setStorefrontUrl(Object paramObject)
  {
    this.storefrontUrl = paramObject;
  }
  
  public void setVariantList(List<CJRUtilityVariantV2> paramList)
  {
    this.variantList = paramList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/v2/CJRUtilityProductListV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */