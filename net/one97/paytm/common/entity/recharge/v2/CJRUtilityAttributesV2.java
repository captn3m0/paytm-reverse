package net.one97.paytm.common.entity.recharge.v2;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRUtilityAttributesV2
  implements IJRDataModel
{
  @b(a="error")
  private CJRUtilityErrorV2 error;
  @b(a="prefetch")
  private String prefetch;
  @b(a="softblock")
  private Boolean softblock;
  @b(a="status")
  private Boolean status;
  
  public CJRUtilityErrorV2 getError()
  {
    return this.error;
  }
  
  public String getPrefetch()
  {
    return this.prefetch;
  }
  
  public Boolean getSoftblock()
  {
    return this.softblock;
  }
  
  public Boolean getStatus()
  {
    return this.status;
  }
  
  public void setError(CJRUtilityErrorV2 paramCJRUtilityErrorV2)
  {
    this.error = paramCJRUtilityErrorV2;
  }
  
  public void setPrefetch(String paramString)
  {
    this.prefetch = paramString;
  }
  
  public void setSoftblock(Boolean paramBoolean)
  {
    this.softblock = paramBoolean;
  }
  
  public void setStatus(Boolean paramBoolean)
  {
    this.status = paramBoolean;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/v2/CJRUtilityAttributesV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */