package net.one97.paytm.common.entity.recharge.v2;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRUtilityProductV2
  implements IJRDataModel
{
  @b(a="attributes")
  private CJRUtilityAttributesV2 attributes;
  @b(a="id")
  private Integer id;
  @b(a="input_fields")
  private List<CJRUtilityInputFieldsV2> inputFields = new ArrayList();
  
  public CJRUtilityAttributesV2 getAttributes()
  {
    return this.attributes;
  }
  
  public Integer getId()
  {
    return this.id;
  }
  
  public List<CJRUtilityInputFieldsV2> getInputFields()
  {
    return this.inputFields;
  }
  
  public void setAttributes(CJRUtilityAttributesV2 paramCJRUtilityAttributesV2)
  {
    this.attributes = paramCJRUtilityAttributesV2;
  }
  
  public void setId(Integer paramInteger)
  {
    this.id = paramInteger;
  }
  
  public void setInputFields(List<CJRUtilityInputFieldsV2> paramList)
  {
    this.inputFields = paramList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/v2/CJRUtilityProductV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */