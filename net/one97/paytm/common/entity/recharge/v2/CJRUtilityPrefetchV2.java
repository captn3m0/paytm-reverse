package net.one97.paytm.common.entity.recharge.v2;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRUtilityPrefetchV2
  implements IJRDataModel
{
  @b(a="post")
  private String post;
  @b(a="pre")
  private String pre;
  @b(a="processing")
  private String processing;
  
  public String getPost()
  {
    return this.post;
  }
  
  public String getPre()
  {
    return this.pre;
  }
  
  public String getProcessing()
  {
    return this.processing;
  }
  
  public void setPost(String paramString)
  {
    this.post = paramString;
  }
  
  public void setPre(String paramString)
  {
    this.pre = paramString;
  }
  
  public void setProcessing(String paramString)
  {
    this.processing = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/v2/CJRUtilityPrefetchV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */