package net.one97.paytm.common.entity.recharge.v2;

import com.google.c.a.b;
import java.util.Map;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRUtilityButtonObjectV2
  implements IJRDataModel
{
  @b(a="prefetch")
  private Map<String, CJRUtilityPrefetchV2> prefetchMap;
  
  public Map<String, CJRUtilityPrefetchV2> getPrefetchMap()
  {
    return this.prefetchMap;
  }
  
  public void setPrefetchMap(Map<String, CJRUtilityPrefetchV2> paramMap)
  {
    this.prefetchMap = paramMap;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/v2/CJRUtilityButtonObjectV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */