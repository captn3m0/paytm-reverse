package net.one97.paytm.common.entity.recharge.v2;

import com.google.c.a.b;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRRechargeProductPerCircleV2
  implements IJRDataModel
{
  @b(a="attributes")
  private CJRRechargeProductPerStateAttributeV2 attributes;
  @b(a="filter_name")
  private String circleName;
  @b(a="variants")
  private List<CJRRechargeProductV2> variantsList;
  
  public CJRRechargeProductPerStateAttributeV2 getAttributes()
  {
    return this.attributes;
  }
  
  public String getCircleName()
  {
    return this.circleName;
  }
  
  public List<CJRRechargeProductV2> getVariantsList()
  {
    return this.variantsList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/v2/CJRRechargeProductPerCircleV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */