package net.one97.paytm.common.entity.recharge.v2;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRRechargeProductListGroupArrV2
  implements IJRDataModel
{
  @b(a="circle")
  private CJRRechargeProductListGroupArrItemV2 circleItem;
  @b(a="operator")
  private CJRRechargeProductListGroupArrItemV2 operatorItem;
  @b(a="producttype")
  private CJRRechargeProductListGroupArrItemV2 productType;
  
  public CJRRechargeProductListGroupArrItemV2 getCircleItem()
  {
    return this.circleItem;
  }
  
  public CJRRechargeProductListGroupArrItemV2 getOperatorItem()
  {
    return this.operatorItem;
  }
  
  public CJRRechargeProductListGroupArrItemV2 getProductType()
  {
    return this.productType;
  }
  
  public void setCircleItem(CJRRechargeProductListGroupArrItemV2 paramCJRRechargeProductListGroupArrItemV2)
  {
    this.circleItem = paramCJRRechargeProductListGroupArrItemV2;
  }
  
  public void setOperatorItem(CJRRechargeProductListGroupArrItemV2 paramCJRRechargeProductListGroupArrItemV2)
  {
    this.operatorItem = paramCJRRechargeProductListGroupArrItemV2;
  }
  
  public void setProductTypeItem(CJRRechargeProductListGroupArrItemV2 paramCJRRechargeProductListGroupArrItemV2)
  {
    this.productType = paramCJRRechargeProductListGroupArrItemV2;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/v2/CJRRechargeProductListGroupArrV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */