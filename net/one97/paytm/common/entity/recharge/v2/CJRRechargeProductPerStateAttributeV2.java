package net.one97.paytm.common.entity.recharge.v2;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRRechargeProductPerStateAttributeV2
  implements IJRDataModel
{
  @b(a="circle_label")
  private String circleLabel;
  @b(a="image")
  private String image;
  @b(a="operator_label")
  private String operatorLabel;
  
  public String getCircleLabel()
  {
    return this.circleLabel;
  }
  
  public String getImage()
  {
    return this.image;
  }
  
  public String getOperatorLabel()
  {
    return this.operatorLabel;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/v2/CJRRechargeProductPerStateAttributeV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */