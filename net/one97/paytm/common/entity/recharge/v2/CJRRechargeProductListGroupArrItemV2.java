package net.one97.paytm.common.entity.recharge.v2;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRRechargeProductListGroupArrItemV2
  implements IJRDataModel
{
  @b(a="brandImage")
  private boolean brandImage;
  @b(a="key")
  private String key;
  @b(a="label")
  private String label;
  @b(a="type")
  private String type;
  
  public String getKey()
  {
    return this.key;
  }
  
  public String getLabel()
  {
    return this.label;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public boolean isBrandImage()
  {
    return this.brandImage;
  }
  
  public void setBrandImage(boolean paramBoolean)
  {
    this.brandImage = paramBoolean;
  }
  
  public void setKey(String paramString)
  {
    this.key = paramString;
  }
  
  public void setLabel(String paramString)
  {
    this.label = paramString;
  }
  
  public void setType(String paramString)
  {
    this.type = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/v2/CJRRechargeProductListGroupArrItemV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */