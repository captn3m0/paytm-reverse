package net.one97.paytm.common.entity.recharge.v2;

import com.google.c.a.b;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRRechargeProductV2
  implements IJRDataModel
{
  @b(a="input_fields")
  private List<CJRRechargeInputFieldsTypeV2> inputFieldsTypeV2List;
  @b(a="id")
  private String productId;
  @b(a="attributes")
  private CJRRechargeProductAttributesV2 rechargeProductAttributesV2;
  
  public List<CJRRechargeInputFieldsTypeV2> getInputFieldsTypeV2List()
  {
    return this.inputFieldsTypeV2List;
  }
  
  public String getProductId()
  {
    return this.productId;
  }
  
  public CJRRechargeProductAttributesV2 getRechargeProductAttributesV2()
  {
    return this.rechargeProductAttributesV2;
  }
  
  public void setInputFieldsTypeV2List(List<CJRRechargeInputFieldsTypeV2> paramList)
  {
    this.inputFieldsTypeV2List = paramList;
  }
  
  public void setProductId(String paramString)
  {
    this.productId = paramString;
  }
  
  public void setRechargeProductAttributesV2(CJRRechargeProductAttributesV2 paramCJRRechargeProductAttributesV2)
  {
    this.rechargeProductAttributesV2 = paramCJRRechargeProductAttributesV2;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/v2/CJRRechargeProductV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */