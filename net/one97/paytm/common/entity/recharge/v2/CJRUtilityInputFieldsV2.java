package net.one97.paytm.common.entity.recharge.v2;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRUtilityInputFieldsV2
  implements IJRDataModel
{
  @b(a="config_key")
  private String configKey;
  @b(a="mandatory")
  private Boolean mandatory;
  @b(a="regex")
  private String regex;
  @b(a="title")
  private String title;
  @b(a="type")
  private String type;
  
  public String getConfigKey()
  {
    return this.configKey;
  }
  
  public Boolean getMandatory()
  {
    return this.mandatory;
  }
  
  public String getRegex()
  {
    return this.regex;
  }
  
  public String getTitle()
  {
    return this.title;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public void setConfigKey(String paramString)
  {
    this.configKey = paramString;
  }
  
  public void setMandatory(Boolean paramBoolean)
  {
    this.mandatory = paramBoolean;
  }
  
  public void setRegex(String paramString)
  {
    this.regex = paramString;
  }
  
  public void setTitle(String paramString)
  {
    this.title = paramString;
  }
  
  public void setType(String paramString)
  {
    this.type = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/v2/CJRUtilityInputFieldsV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */