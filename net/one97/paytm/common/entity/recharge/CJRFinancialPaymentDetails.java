package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import java.util.LinkedHashMap;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFinancialPaymentDetails
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="fullInterestRebate")
  private String mFullInterestRebate;
  @b(a="fullTotalRebate")
  private String mFullTotalRebate;
  @b(a="interestAmount")
  private String mInterestAmount;
  @b(a="interestDue")
  private String mInterestDue;
  @b(a="maxPartialAmount")
  private String mMaxPartialAmount;
  @b(a="minPartialAmount")
  private String mMinPartialAmount;
  @b(a="netTotal")
  private String mNetAmount;
  @b(a="otherCharges")
  private String mOtherCharges;
  public LinkedHashMap<String, String> mPaymentDetailMap;
  @b(a="principalAmount")
  private String mPrincipalAmount;
  @b(a="transaction_id")
  private String mTransactionId;
  @b(a="name")
  private String mUserName;
  
  public LinkedHashMap<String, String> getDetails()
  {
    return this.mPaymentDetailMap;
  }
  
  public String getFullInterestRebate()
  {
    return this.mFullInterestRebate;
  }
  
  public String getFullTotalRebate()
  {
    return this.mFullTotalRebate;
  }
  
  public String getInterestAmount()
  {
    return this.mInterestAmount;
  }
  
  public String getInterestDue()
  {
    return this.mInterestDue;
  }
  
  public String getMaxPartialAmount()
  {
    return this.mMaxPartialAmount;
  }
  
  public String getMinPartialAmount()
  {
    return this.mMinPartialAmount;
  }
  
  public String getNetAmount()
  {
    return this.mNetAmount;
  }
  
  public String getOtherCharges()
  {
    return this.mOtherCharges;
  }
  
  public String getPrincipalAmount()
  {
    return this.mPrincipalAmount;
  }
  
  public String getTransactionId()
  {
    return this.mTransactionId;
  }
  
  public String getUserName()
  {
    return this.mUserName;
  }
  
  public void setDetails(LinkedHashMap<String, String> paramLinkedHashMap)
  {
    this.mPaymentDetailMap = paramLinkedHashMap;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRFinancialPaymentDetails.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */