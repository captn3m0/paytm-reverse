package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRRechargeConfiguration
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="caption")
  private String mCaption;
  @b(a="products")
  private ArrayList<CJRRechargeProduct> mProductList;
  
  public String getCaption()
  {
    return this.mCaption;
  }
  
  public ArrayList<CJRRechargeProduct> getProductList()
  {
    return this.mProductList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRRechargeConfiguration.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */