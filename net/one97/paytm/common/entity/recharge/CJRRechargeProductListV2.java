package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRRechargeProductListV2
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="group_field")
  public ArrayList<CJRGroupField> groupField;
  private boolean isCached = false;
  @b(a="showFastforward")
  private boolean mIsFastForward;
  @b(a="meta_description_new")
  private String mMetaDescription;
  @b(a="ProductList")
  private ArrayList<CJRRechargeProductV2> mRechargeProduct;
  @b(a="removeProducts")
  private CJRRechargeUtilRemoveProduct mRemoveProducts;
  private Long mServerResponseTime;
  
  public boolean getFastForward()
  {
    return this.mIsFastForward;
  }
  
  public ArrayList<CJRGroupField> getGroupField()
  {
    return this.groupField;
  }
  
  public String getMetaDescription()
  {
    return this.mMetaDescription;
  }
  
  public ArrayList<CJRRechargeProductV2> getRechargeProduct()
  {
    return this.mRechargeProduct;
  }
  
  public CJRRechargeUtilRemoveProduct getRemoveProducts()
  {
    return this.mRemoveProducts;
  }
  
  public Long getServerResponseTime()
  {
    return this.mServerResponseTime;
  }
  
  public boolean isCached()
  {
    return this.isCached;
  }
  
  public void setIsCached(boolean paramBoolean)
  {
    this.isCached = paramBoolean;
  }
  
  public void setRechargeProduct(ArrayList<CJRRechargeProductV2> paramArrayList)
  {
    this.mRechargeProduct = paramArrayList;
  }
  
  public void setRemoveProducts(CJRRechargeUtilRemoveProduct paramCJRRechargeUtilRemoveProduct)
  {
    this.mRemoveProducts = paramCJRRechargeUtilRemoveProduct;
  }
  
  public void setServerResponseTime(Long paramLong)
  {
    this.mServerResponseTime = paramLong;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRRechargeProductListV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */