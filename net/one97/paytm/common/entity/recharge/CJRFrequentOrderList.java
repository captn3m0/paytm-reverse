package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFrequentOrderList
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  public final String TAG_PAY_TYPE_POSTPAID = "postpaid";
  public final String TAG_PAY_TYPE_PREPAID = "prepaid";
  public final String TAG_SERVICE_DATACARD = "DataCard";
  public final String TAG_SERVICE_DTH = "DTH";
  public final String TAG_SERVICE_ELECTRICITY = "Electricity";
  public final String TAG_SERVICE_GAS = "Gas";
  public final String TAG_SERVICE_LANDLINE = "Landline";
  public final String TAG_SERVICE_METRO = "Metro";
  public final String TAG_SERVICE_MOBILE = "mobile";
  @b(a="orders")
  private ArrayList<CJRFrequentOrder> mOrderList = new ArrayList();
  
  public void clear()
  {
    if (this.mOrderList != null) {
      this.mOrderList.clear();
    }
  }
  
  public ArrayList<CJRFrequentOrder> getDatacardOrdres()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mOrderList.iterator();
    while (localIterator.hasNext())
    {
      CJRFrequentOrder localCJRFrequentOrder = (CJRFrequentOrder)localIterator.next();
      if (localCJRFrequentOrder != null)
      {
        String str1 = localCJRFrequentOrder.getPayType();
        String str2 = localCJRFrequentOrder.getService();
        if ((localCJRFrequentOrder.getFrequentOrderProduct() != null) && ((str1.equalsIgnoreCase("prepaid")) || (str1.equalsIgnoreCase("postpaid"))) && (str2.equalsIgnoreCase("DataCard"))) {
          localArrayList.add(localCJRFrequentOrder);
        }
      }
    }
    if (localArrayList.size() > 1) {
      Collections.sort(localArrayList);
    }
    return localArrayList;
  }
  
  public ArrayList<CJRFrequentOrder> getMobileOrdres()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mOrderList.iterator();
    while (localIterator.hasNext())
    {
      CJRFrequentOrder localCJRFrequentOrder = (CJRFrequentOrder)localIterator.next();
      if (localCJRFrequentOrder != null)
      {
        String str1 = localCJRFrequentOrder.getPayType();
        String str2 = localCJRFrequentOrder.getService();
        if ((localCJRFrequentOrder.getFrequentOrderProduct() != null) && ((str1.equalsIgnoreCase("prepaid")) || (str1.equalsIgnoreCase("postpaid"))) && (str2.equalsIgnoreCase("mobile"))) {
          localArrayList.add(localCJRFrequentOrder);
        }
      }
    }
    if (localArrayList.size() > 1) {
      Collections.sort(localArrayList);
    }
    return localArrayList;
  }
  
  public ArrayList<CJRFrequentOrder> getOrderList()
  {
    return this.mOrderList;
  }
  
  public ArrayList<CJRFrequentOrder> getPostpaidDTHOrders()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mOrderList.iterator();
    while (localIterator.hasNext())
    {
      CJRFrequentOrder localCJRFrequentOrder = (CJRFrequentOrder)localIterator.next();
      if ((localCJRFrequentOrder != null) && (localCJRFrequentOrder.getFrequentOrderProduct() != null) && (localCJRFrequentOrder.getPayType().equalsIgnoreCase("postpaid")) && (localCJRFrequentOrder.getService().equalsIgnoreCase("DTH"))) {
        localArrayList.add(localCJRFrequentOrder);
      }
    }
    if (localArrayList.size() > 1) {
      Collections.sort(localArrayList);
    }
    return localArrayList;
  }
  
  public ArrayList<CJRFrequentOrder> getPostpaidDatacardOrders()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mOrderList.iterator();
    while (localIterator.hasNext())
    {
      CJRFrequentOrder localCJRFrequentOrder = (CJRFrequentOrder)localIterator.next();
      if ((localCJRFrequentOrder != null) && (localCJRFrequentOrder.getFrequentOrderProduct() != null) && (localCJRFrequentOrder.getPayType().equalsIgnoreCase("postpaid")) && (localCJRFrequentOrder.getService().equalsIgnoreCase("DataCard"))) {
        localArrayList.add(localCJRFrequentOrder);
      }
    }
    if (localArrayList.size() > 1) {
      Collections.sort(localArrayList);
    }
    return localArrayList;
  }
  
  public ArrayList<CJRFrequentOrder> getPostpaidElectricityOrders()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mOrderList.iterator();
    while (localIterator.hasNext())
    {
      CJRFrequentOrder localCJRFrequentOrder = (CJRFrequentOrder)localIterator.next();
      if ((localCJRFrequentOrder != null) && (localCJRFrequentOrder.getFrequentOrderProduct() != null) && (localCJRFrequentOrder.getPayType().equalsIgnoreCase("postpaid")) && (localCJRFrequentOrder.getService().equalsIgnoreCase("Electricity"))) {
        localArrayList.add(localCJRFrequentOrder);
      }
    }
    if (localArrayList.size() > 1) {
      Collections.sort(localArrayList);
    }
    return localArrayList;
  }
  
  public ArrayList<CJRFrequentOrder> getPostpaidGasOrders()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mOrderList.iterator();
    while (localIterator.hasNext())
    {
      CJRFrequentOrder localCJRFrequentOrder = (CJRFrequentOrder)localIterator.next();
      if ((localCJRFrequentOrder != null) && (localCJRFrequentOrder.getFrequentOrderProduct() != null) && (localCJRFrequentOrder.getPayType().equalsIgnoreCase("postpaid")) && (localCJRFrequentOrder.getService().equalsIgnoreCase("Gas"))) {
        localArrayList.add(localCJRFrequentOrder);
      }
    }
    if (localArrayList.size() > 1) {
      Collections.sort(localArrayList);
    }
    return localArrayList;
  }
  
  public ArrayList<CJRFrequentOrder> getPostpaidLandlineOrders()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mOrderList.iterator();
    while (localIterator.hasNext())
    {
      CJRFrequentOrder localCJRFrequentOrder = (CJRFrequentOrder)localIterator.next();
      if ((localCJRFrequentOrder != null) && (localCJRFrequentOrder.getFrequentOrderProduct() != null) && (localCJRFrequentOrder.getPayType().equalsIgnoreCase("postpaid")) && (localCJRFrequentOrder.getService().equalsIgnoreCase("Landline"))) {
        localArrayList.add(localCJRFrequentOrder);
      }
    }
    if (localArrayList.size() > 1) {
      Collections.sort(localArrayList);
    }
    return localArrayList;
  }
  
  public ArrayList<CJRFrequentOrder> getPostpaidMobileOrders()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mOrderList.iterator();
    while (localIterator.hasNext())
    {
      CJRFrequentOrder localCJRFrequentOrder = (CJRFrequentOrder)localIterator.next();
      if ((localCJRFrequentOrder != null) && (localCJRFrequentOrder.getFrequentOrderProduct() != null) && (localCJRFrequentOrder.getPayType().equalsIgnoreCase("postpaid")) && (localCJRFrequentOrder.getService().equalsIgnoreCase("mobile"))) {
        localArrayList.add(localCJRFrequentOrder);
      }
    }
    if (localArrayList.size() > 1) {
      Collections.sort(localArrayList);
    }
    return localArrayList;
  }
  
  public ArrayList<CJRFrequentOrder> getPrepaidDTHOrdres()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mOrderList.iterator();
    while (localIterator.hasNext())
    {
      CJRFrequentOrder localCJRFrequentOrder = (CJRFrequentOrder)localIterator.next();
      if ((localCJRFrequentOrder != null) && (localCJRFrequentOrder.getFrequentOrderProduct() != null) && (localCJRFrequentOrder.getPayType().equalsIgnoreCase("prepaid")) && (localCJRFrequentOrder.getService().equalsIgnoreCase("DTH"))) {
        localArrayList.add(localCJRFrequentOrder);
      }
    }
    if (localArrayList.size() > 1) {
      Collections.sort(localArrayList);
    }
    return localArrayList;
  }
  
  public ArrayList<CJRFrequentOrder> getPrepaidDatacardOrdres()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mOrderList.iterator();
    while (localIterator.hasNext())
    {
      CJRFrequentOrder localCJRFrequentOrder = (CJRFrequentOrder)localIterator.next();
      if ((localCJRFrequentOrder != null) && (localCJRFrequentOrder.getFrequentOrderProduct() != null) && (localCJRFrequentOrder.getPayType().equalsIgnoreCase("prepaid")) && (localCJRFrequentOrder.getService().equalsIgnoreCase("DataCard"))) {
        localArrayList.add(localCJRFrequentOrder);
      }
    }
    if (localArrayList.size() > 1) {
      Collections.sort(localArrayList);
    }
    return localArrayList;
  }
  
  public ArrayList<CJRFrequentOrder> getPrepaidElectricityOrdres()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mOrderList.iterator();
    while (localIterator.hasNext())
    {
      CJRFrequentOrder localCJRFrequentOrder = (CJRFrequentOrder)localIterator.next();
      if ((localCJRFrequentOrder != null) && (localCJRFrequentOrder.getFrequentOrderProduct() != null) && (localCJRFrequentOrder.getPayType().equalsIgnoreCase("prepaid")) && (localCJRFrequentOrder.getService().equalsIgnoreCase("Electricity"))) {
        localArrayList.add(localCJRFrequentOrder);
      }
    }
    if (localArrayList.size() > 1) {
      Collections.sort(localArrayList);
    }
    return localArrayList;
  }
  
  public ArrayList<CJRFrequentOrder> getPrepaidGasOrdres()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mOrderList.iterator();
    while (localIterator.hasNext())
    {
      CJRFrequentOrder localCJRFrequentOrder = (CJRFrequentOrder)localIterator.next();
      if ((localCJRFrequentOrder != null) && (localCJRFrequentOrder.getFrequentOrderProduct() != null) && (localCJRFrequentOrder.getPayType().equalsIgnoreCase("prepaid")) && (localCJRFrequentOrder.getService().equalsIgnoreCase("Gas"))) {
        localArrayList.add(localCJRFrequentOrder);
      }
    }
    if (localArrayList.size() > 1) {
      Collections.sort(localArrayList);
    }
    return localArrayList;
  }
  
  public ArrayList<CJRFrequentOrder> getPrepaidLandlineOrdres()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mOrderList.iterator();
    while (localIterator.hasNext())
    {
      CJRFrequentOrder localCJRFrequentOrder = (CJRFrequentOrder)localIterator.next();
      if ((localCJRFrequentOrder != null) && (localCJRFrequentOrder.getFrequentOrderProduct() != null) && (localCJRFrequentOrder.getPayType().equalsIgnoreCase("prepaid")) && (localCJRFrequentOrder.getService().equalsIgnoreCase("Landline"))) {
        localArrayList.add(localCJRFrequentOrder);
      }
    }
    if (localArrayList.size() > 1) {
      Collections.sort(localArrayList);
    }
    return localArrayList;
  }
  
  public ArrayList<CJRFrequentOrder> getPrepaidMetroCardOrders()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mOrderList.iterator();
    while (localIterator.hasNext())
    {
      CJRFrequentOrder localCJRFrequentOrder = (CJRFrequentOrder)localIterator.next();
      if ((localCJRFrequentOrder != null) && (localCJRFrequentOrder.getFrequentOrderProduct() != null) && (localCJRFrequentOrder.getPayType().equalsIgnoreCase("prepaid")) && (localCJRFrequentOrder.getService().equalsIgnoreCase("Metro"))) {
        localArrayList.add(localCJRFrequentOrder);
      }
    }
    if (localArrayList.size() > 1) {
      Collections.sort(localArrayList);
    }
    return localArrayList;
  }
  
  public ArrayList<CJRFrequentOrder> getPrepaidMobileOrdres()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mOrderList.iterator();
    while (localIterator.hasNext())
    {
      CJRFrequentOrder localCJRFrequentOrder = (CJRFrequentOrder)localIterator.next();
      if ((localCJRFrequentOrder != null) && (localCJRFrequentOrder.getFrequentOrderProduct() != null) && (localCJRFrequentOrder.getPayType().equalsIgnoreCase("prepaid")) && (localCJRFrequentOrder.getService().equalsIgnoreCase("mobile"))) {
        localArrayList.add(localCJRFrequentOrder);
      }
    }
    if (localArrayList.size() > 1) {
      Collections.sort(localArrayList);
    }
    return localArrayList;
  }
  
  public ArrayList<CJRFrequentOrder> getUtilityFrequentList(String paramString1, String paramString2)
  {
    try
    {
      ArrayList localArrayList = new ArrayList();
      Iterator localIterator = this.mOrderList.iterator();
      while (localIterator.hasNext())
      {
        CJRFrequentOrder localCJRFrequentOrder = (CJRFrequentOrder)localIterator.next();
        if (localCJRFrequentOrder != null) {
          if (paramString2 != null)
          {
            if ((localCJRFrequentOrder.getFrequentOrderProduct() != null) && (localCJRFrequentOrder.getPayType().equalsIgnoreCase(paramString2)) && (localCJRFrequentOrder.getFrequentOrderProduct().getService().equalsIgnoreCase(paramString1))) {
              localArrayList.add(localCJRFrequentOrder);
            }
          }
          else if ((localCJRFrequentOrder.getFrequentOrderProduct() != null) && (localCJRFrequentOrder.getFrequentOrderProduct().getService().equalsIgnoreCase(paramString1))) {
            localArrayList.add(localCJRFrequentOrder);
          }
        }
      }
      paramString1 = localArrayList;
      if (localArrayList.size() > 1)
      {
        Collections.sort(localArrayList);
        return localArrayList;
      }
    }
    catch (Exception paramString1)
    {
      paramString1 = null;
    }
    return paramString1;
  }
  
  public void setOrderList(ArrayList<CJRFrequentOrder> paramArrayList)
  {
    this.mOrderList = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRFrequentOrderList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */