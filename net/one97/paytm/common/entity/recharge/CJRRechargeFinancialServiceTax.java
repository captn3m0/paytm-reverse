package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRRechargeFinancialServiceTax
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="productId")
  private long mProductId;
  @b(a="totalAmount")
  private String mTotalAmount;
  @b(a="transactionNo")
  private long mTransactionNo;
  
  public long getProductId()
  {
    return this.mProductId;
  }
  
  public String getTotalAmount()
  {
    return this.mTotalAmount;
  }
  
  public long getTransactionNo()
  {
    return this.mTransactionNo;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRRechargeFinancialServiceTax.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */