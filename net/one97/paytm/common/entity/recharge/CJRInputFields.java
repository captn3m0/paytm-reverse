package net.one97.paytm.common.entity.recharge;

import android.text.TextUtils;
import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRInputFields
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="hide")
  private String hide;
  @b(a="isAlphanumeric")
  private String mAlphaNumeric;
  @b(a="config_key")
  private String mConfigKey;
  @b(a="date_format")
  private String mDateFormat;
  @b(a="display_title")
  private String mDisplayTitle;
  @b(a="mandatory")
  private boolean mIsMandatory;
  @b(a="max_date")
  private String mMaxDate;
  @b(a="min_date")
  private String mMinDate;
  @b(a="regex")
  private String mRegex;
  @b(a="show_phonebook")
  private String mShowContactPicker;
  @b(a="sub_title")
  private String mSubTitle;
  @b(a="title")
  private String mTitle;
  @b(a="type")
  private String mType;
  
  public String getAlphaNumeric()
  {
    return this.mAlphaNumeric;
  }
  
  public String getConfigKey()
  {
    return this.mConfigKey;
  }
  
  public String getDateFormat()
  {
    return this.mDateFormat;
  }
  
  public String getDisplayTitle()
  {
    return this.mDisplayTitle;
  }
  
  public boolean getHidden()
  {
    return (!TextUtils.isEmpty(this.hide)) && (this.hide.equalsIgnoreCase("TRUE"));
  }
  
  public String getMaxDate()
  {
    return this.mMaxDate;
  }
  
  public String getMinDate()
  {
    return this.mMinDate;
  }
  
  public String getRegex()
  {
    return this.mRegex;
  }
  
  public String getSubTitle()
  {
    return this.mSubTitle;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
  
  public String getType()
  {
    return this.mType;
  }
  
  public boolean isIsMandatory()
  {
    return this.mIsMandatory;
  }
  
  public boolean isShowContactPicker()
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    try
    {
      if (!TextUtils.isEmpty(this.mShowContactPicker))
      {
        boolean bool3 = this.mShowContactPicker.equalsIgnoreCase("1");
        bool1 = bool2;
        if (bool3) {
          bool1 = true;
        }
      }
      return bool1;
    }
    catch (Exception localException) {}
    return false;
  }
  
  public boolean ismIsMandatory()
  {
    return this.mIsMandatory;
  }
  
  public void setHide(String paramString)
  {
    this.hide = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRInputFields.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */