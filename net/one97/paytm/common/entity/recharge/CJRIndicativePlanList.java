package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRIndicativePlanList
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="code")
  private String mCode;
  @b(a="method")
  private String mMethod;
  @b(a="plans")
  private ArrayList<CJRIndicativePlan> mPlans;
  @b(a="status")
  private String mStatus;
  
  public String getCode()
  {
    return this.mCode;
  }
  
  public String getMethod()
  {
    return this.mMethod;
  }
  
  public String getName()
  {
    return null;
  }
  
  public ArrayList<CJRIndicativePlan> getPlans()
  {
    return this.mPlans;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRIndicativePlanList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */