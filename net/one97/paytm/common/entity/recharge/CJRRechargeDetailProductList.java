package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRRechargeDetailProductList
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="attributes")
  private CJRAttributes mAttributes;
  @b(a="configuration")
  private CJRRechargeConfigurationV2 mConfiguration;
  @b(a="input_fields")
  private ArrayList<CJRInputFields> mInputFields;
  @b(a="product_id")
  private String mProductId;
  
  public CJRAttributes getAttributes()
  {
    return this.mAttributes;
  }
  
  public CJRRechargeConfigurationV2 getConfiguration()
  {
    return this.mConfiguration;
  }
  
  public ArrayList<CJRInputFields> getInputFields()
  {
    return this.mInputFields;
  }
  
  public String getProductId()
  {
    return this.mProductId;
  }
  
  public void setAttributes(CJRAttributes paramCJRAttributes)
  {
    this.mAttributes = paramCJRAttributes;
  }
  
  public void setConfiguration(CJRRechargeConfigurationV2 paramCJRRechargeConfigurationV2)
  {
    this.mConfiguration = paramCJRRechargeConfigurationV2;
  }
  
  public void setInputFields(ArrayList<CJRInputFields> paramArrayList)
  {
    this.mInputFields = paramArrayList;
  }
  
  public void setProductId(String paramString)
  {
    this.mProductId = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRRechargeDetailProductList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */