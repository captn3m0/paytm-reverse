package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRVarients
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="course")
  private String mCourse;
  @b(a="district")
  private String mDistrict;
  @b(a="group1")
  private String mGroup1;
  @b(a="group2")
  private String mGroup2;
  @b(a="location")
  private String mLocation;
  @b(a="products")
  private ArrayList<CJRRechargeDetailProductList> mRechargeDetailProductList;
  @b(a="variants")
  private ArrayList<CJRVarients> mVarients;
  
  public String getCourse()
  {
    return this.mCourse;
  }
  
  public String getDistrict()
  {
    return this.mDistrict;
  }
  
  public String getGroup1()
  {
    return this.mGroup1;
  }
  
  public String getGroup2()
  {
    return this.mGroup2;
  }
  
  public String getLocation()
  {
    return this.mLocation;
  }
  
  public ArrayList<CJRRechargeDetailProductList> getVarients()
  {
    return this.mRechargeDetailProductList;
  }
  
  public ArrayList<CJRVarients> getmVarients()
  {
    if ((this.mRechargeDetailProductList != null) && (this.mVarients == null))
    {
      ArrayList localArrayList = new ArrayList();
      CJRVarients localCJRVarients = new CJRVarients();
      localCJRVarients.setCourse("NA");
      localCJRVarients.setGroup2("NA");
      localCJRVarients.setRechargeDetailProductList(this.mRechargeDetailProductList);
      localArrayList.add(localCJRVarients);
      this.mVarients = localArrayList;
    }
    return this.mVarients;
  }
  
  public void setCourse(String paramString)
  {
    this.mCourse = paramString;
  }
  
  public void setGroup2(String paramString)
  {
    this.mGroup2 = paramString;
  }
  
  public void setRechargeDetailProductList(ArrayList<CJRRechargeDetailProductList> paramArrayList)
  {
    this.mRechargeDetailProductList = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRVarients.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */