package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import java.util.Map;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFrequentOrderProduct
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  public final String TAG_CIRCLE = "circle";
  public final String TAG_OPERATOR_LABEL = "operator_label";
  public final String TAG_PAY_OPERATOR = "operator";
  public final String TAG_PAY_TYPE = "paytype";
  public final String TAG_PLANS = "plans";
  public final String TAG_SERVICE = "service";
  @b(a="attributes")
  private Map<String, String> mAttributes;
  @b(a="brand")
  private String mFrequentOrderBrand;
  
  public Map<String, String> getAttributes()
  {
    return this.mAttributes;
  }
  
  public String getCircle()
  {
    if (this.mAttributes.containsKey("circle")) {
      return (String)this.mAttributes.get("circle");
    }
    return new StringBuilder().toString();
  }
  
  public String getFrequentOrderBrand()
  {
    return this.mFrequentOrderBrand;
  }
  
  public String getOperator()
  {
    if (this.mAttributes.containsKey("operator")) {
      return (String)this.mAttributes.get("operator");
    }
    return new StringBuilder().toString();
  }
  
  public String getOperatorLabel()
  {
    if (this.mAttributes.containsKey("operator_label")) {
      return (String)this.mAttributes.get("operator_label");
    }
    return new StringBuilder().toString();
  }
  
  public String getPaytype()
  {
    if (this.mAttributes.containsKey("paytype")) {
      return (String)this.mAttributes.get("paytype");
    }
    return new StringBuilder().toString();
  }
  
  public String getPlans()
  {
    if (this.mAttributes.containsKey("plans")) {
      return (String)this.mAttributes.get("plans");
    }
    return new StringBuilder().toString();
  }
  
  public String getService()
  {
    if (this.mAttributes.containsKey("service")) {
      return (String)this.mAttributes.get("service");
    }
    return new StringBuilder().toString();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRFrequentOrderProduct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */