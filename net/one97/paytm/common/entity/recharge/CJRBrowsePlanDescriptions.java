package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrowsePlanDescriptions
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="grid_layout")
  private ArrayList<CJRBrowsePlanDescription> mDescription;
  @b(a="has_more")
  private boolean mHasMore;
  
  public ArrayList<CJRBrowsePlanDescription> getDescription()
  {
    return this.mDescription;
  }
  
  public boolean hasMore()
  {
    return this.mHasMore;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRBrowsePlanDescriptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */