package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAttributes
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="alert_heading")
  private String alert_heading;
  @b(a="alert_message")
  private String alert_message;
  @b(a="fee_structure")
  private String feeStructure;
  @b(a="amount")
  private String mAmount;
  @b(a="circle")
  private String mCircle;
  @b(a="circle_label")
  private String mCircleLabel;
  @b(a="info")
  private String mInfo;
  @b(a="max_amount")
  private String mMaxAmount;
  @b(a="message")
  private String mMessage;
  @b(a="min_amount")
  private String mMinAmount;
  @b(a="operator_label")
  private String mOperatorLabel;
  @b(a="operator")
  private String mOperatorName;
  @b(a="paytype_label")
  private String mPayTypeLabel;
  @b(a="paytype")
  private String mPaytype;
  @b(a="prefetch")
  private String mPrefetch;
  @b(a="producttype")
  private String mProductType;
  @b(a="producttype_label")
  private String mProductTypeLabel;
  @b(a="service")
  private String mService;
  @b(a="service_label")
  private String mServiceLabel;
  @b(a="status")
  private String mStatus;
  @b(a="updated_info")
  private String mUpdatedInfo;
  
  public String getAlert_heading()
  {
    return this.alert_heading;
  }
  
  public String getAlert_message()
  {
    return this.alert_message;
  }
  
  public String getAmount()
  {
    return this.mAmount;
  }
  
  public String getCircle()
  {
    return this.mCircle;
  }
  
  public String getCircleLabel()
  {
    return this.mCircleLabel;
  }
  
  public String getFeeStructure()
  {
    return this.feeStructure;
  }
  
  public String getInfo()
  {
    return this.mInfo;
  }
  
  public String getMaxAmount()
  {
    return this.mMaxAmount;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getMinAmount()
  {
    return this.mMinAmount;
  }
  
  public String getOperatorLabel()
  {
    return this.mOperatorLabel;
  }
  
  public String getOperatorName()
  {
    return this.mOperatorName;
  }
  
  public String getPayTypeLabel()
  {
    return this.mPayTypeLabel;
  }
  
  public String getPaytype()
  {
    return this.mPaytype;
  }
  
  public String getPrefetch()
  {
    return this.mPrefetch;
  }
  
  public String getProductType()
  {
    return this.mProductType;
  }
  
  public String getProductTypeLabel()
  {
    return this.mProductTypeLabel;
  }
  
  public String getService()
  {
    return this.mService;
  }
  
  public String getServiceLabel()
  {
    return this.mServiceLabel;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public String getUpdatedInfo()
  {
    return this.mUpdatedInfo;
  }
  
  public void setAmount(String paramString)
  {
    this.mAmount = paramString;
  }
  
  public void setFeeStructure(String paramString)
  {
    this.feeStructure = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRAttributes.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */