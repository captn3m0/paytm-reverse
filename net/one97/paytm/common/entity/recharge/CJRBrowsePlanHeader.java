package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrowsePlanHeader
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="category_id")
  private String mCategoryId;
  @b(a="name")
  private String mName;
  @b(a="seourl")
  private String mSeourl;
  @b(a="url")
  private String mUrl;
  
  public String getCategoryId()
  {
    return this.mCategoryId;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getSeourl()
  {
    return this.mSeourl;
  }
  
  public String getUrl()
  {
    return this.mUrl;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRBrowsePlanHeader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */