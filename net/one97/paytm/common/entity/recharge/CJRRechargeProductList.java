package net.one97.paytm.common.entity.recharge;

import android.text.TextUtils;
import com.google.c.a.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRRechargeProductList
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  private String mJsonPostpaidString;
  private String mJsonPrepaidString;
  private String mPostpaidUrlType;
  private String mPrepaidUrlType;
  Map<String, ArrayList<CJRRechargeProduct>> mProductsBasedonCategory;
  @b(a="operators")
  private ArrayList<CJRRechargeProduct> mRechargeProduct;
  private Long mServerResponseTime;
  private String mUrlType;
  
  public void filterCircleList()
  {
    if (this.mRechargeProduct == null) {
      this.mRechargeProduct = new ArrayList();
    }
    int k = this.mRechargeProduct.size();
    int i = 0;
    while (i < k)
    {
      CJRRechargeProduct localCJRRechargeProduct = (CJRRechargeProduct)this.mRechargeProduct.get(i);
      Object localObject1 = localCJRRechargeProduct.getConfiguration().getProductList();
      LinkedHashMap localLinkedHashMap = new LinkedHashMap();
      int m = ((ArrayList)localObject1).size();
      int j = 0;
      Object localObject3;
      if (j < m)
      {
        localObject2 = (CJRRechargeProduct)((ArrayList)localObject1).get(j);
        if (((CJRRechargeProduct)localObject2).getCircle() == null) {
          ((CJRRechargeProduct)localObject2).setCircle("all");
        }
        localObject3 = (ArrayList)localLinkedHashMap.get(((CJRRechargeProduct)localObject2).getCircle());
        if (localObject3 == null)
        {
          localObject3 = new ArrayList();
          ((ArrayList)localObject3).add(localObject2);
          localLinkedHashMap.put(((CJRRechargeProduct)localObject2).getCircle(), localObject3);
        }
        for (;;)
        {
          j += 1;
          break;
          ((ArrayList)localObject3).add(localObject2);
          localLinkedHashMap.put(((CJRRechargeProduct)localObject2).getCircle(), localObject3);
        }
      }
      localObject1 = localLinkedHashMap.keySet().iterator();
      Object localObject2 = new ArrayList();
      while (((Iterator)localObject1).hasNext())
      {
        localObject3 = (String)((Iterator)localObject1).next();
        CJRCircle localCJRCircle = new CJRCircle();
        localCJRCircle.setName((String)localObject3);
        localCJRCircle.setCongigList((ArrayList)localLinkedHashMap.get(localObject3));
        ((ArrayList)localObject2).add(localCJRCircle);
      }
      localCJRRechargeProduct.setCircles((ArrayList)localObject2);
      i += 1;
    }
  }
  
  public void filterOperatorList()
  {
    if (this.mRechargeProduct == null) {
      this.mRechargeProduct = new ArrayList();
    }
    int j = this.mRechargeProduct.size();
    this.mProductsBasedonCategory = new LinkedHashMap();
    int i = 0;
    if (i < j)
    {
      CJRRechargeProduct localCJRRechargeProduct = (CJRRechargeProduct)this.mRechargeProduct.get(i);
      Object localObject1 = localCJRRechargeProduct.getProductType();
      Object localObject2;
      if (!TextUtils.isEmpty((CharSequence)localObject1))
      {
        localObject2 = localObject1.split("-")[0];
        localObject1 = localObject2;
        if (localObject2 != null) {
          localObject1 = ((String)localObject2).toLowerCase().trim();
        }
        localObject2 = (ArrayList)this.mProductsBasedonCategory.get(localObject1);
        if (localObject2 != null) {
          break label154;
        }
        localObject2 = new ArrayList();
        ((ArrayList)localObject2).add(localCJRRechargeProduct);
        this.mProductsBasedonCategory.put(localObject1, localObject2);
      }
      for (;;)
      {
        i += 1;
        break;
        label154:
        ((ArrayList)localObject2).add(localCJRRechargeProduct);
        this.mProductsBasedonCategory.put(localObject1, localObject2);
      }
    }
  }
  
  public String getJsonPostpaidString()
  {
    return this.mJsonPostpaidString;
  }
  
  public String getJsonPrepaidString()
  {
    return this.mJsonPrepaidString;
  }
  
  public String getName()
  {
    return null;
  }
  
  public ArrayList<CJRRechargeProduct> getRechargeProduct()
  {
    if (this.mRechargeProduct == null) {
      this.mRechargeProduct = new ArrayList();
    }
    return this.mRechargeProduct;
  }
  
  public Long getServerResponseTime()
  {
    return this.mServerResponseTime;
  }
  
  public String getUrlType()
  {
    return this.mUrlType;
  }
  
  public String getmPrepaidUrlType()
  {
    return this.mPrepaidUrlType;
  }
  
  public void setJsonPostpaidString(String paramString)
  {
    this.mJsonPostpaidString = paramString;
  }
  
  public void setJsonPrepaidString(String paramString)
  {
    this.mJsonPrepaidString = paramString;
  }
  
  public void setRechargeProduct(ArrayList<CJRRechargeProduct> paramArrayList)
  {
    this.mRechargeProduct = paramArrayList;
  }
  
  public void setServerResponseTime(Long paramLong)
  {
    this.mServerResponseTime = paramLong;
  }
  
  public void setUrlType(String paramString)
  {
    this.mUrlType = paramString;
  }
  
  public void setmPostpaidUrlType(String paramString)
  {
    this.mPostpaidUrlType = paramString;
  }
  
  public void setmPrepaidUrlType(String paramString)
  {
    this.mPrepaidUrlType = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRRechargeProductList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */