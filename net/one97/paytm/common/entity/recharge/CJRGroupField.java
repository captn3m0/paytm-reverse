package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;
import org.json.JSONObject;

public class CJRGroupField
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  private ArrayList<CJRGroupField> groupFields;
  private ArrayList<CJRInputFields> inputFields;
  private ArrayList<String> mCheckedIds = new ArrayList();
  private ArrayList<ArrayList<CJRInputFields>> mGroupFieldsArray;
  private ArrayList<CJRRechargeVarientDetail> mGrouplist;
  private transient ArrayList<JSONObject> mJSONObject;
  @b(a="key")
  public String mKey;
  @b(a="label")
  public String mLabel;
  @b(a="name")
  public String mName;
  private CJRRechargeDetailProductList mProductList;
  private ArrayList<CJRRechargeDetailProductList> mRechargeProductList;
  @b(a="type")
  public String mType;
  private transient ArrayList<JSONObject> mVArientObject;
  @b(a="showfield")
  public boolean showfield = true;
  
  public ArrayList<CJRGroupField> getGroupFields()
  {
    return this.groupFields;
  }
  
  public ArrayList<ArrayList<CJRInputFields>> getGroupInputFieldsArray()
  {
    return this.mGroupFieldsArray;
  }
  
  public ArrayList<JSONObject> getJSONObject()
  {
    return this.mJSONObject;
  }
  
  public String getKey()
  {
    return this.mKey;
  }
  
  public ArrayList<CJRRechargeDetailProductList> getRechargeProductList()
  {
    return this.mRechargeProductList;
  }
  
  public ArrayList<JSONObject> getVArientObject()
  {
    return this.mVArientObject;
  }
  
  public ArrayList<String> getmCheckedIds()
  {
    return this.mCheckedIds;
  }
  
  public ArrayList<CJRRechargeVarientDetail> getmGrouplist()
  {
    return this.mGrouplist;
  }
  
  public String getmLabel()
  {
    return this.mLabel;
  }
  
  public String getmName()
  {
    return this.mName;
  }
  
  public CJRRechargeDetailProductList getmProductList()
  {
    return this.mProductList;
  }
  
  public String getmType()
  {
    return this.mType;
  }
  
  public boolean isShowfield()
  {
    return this.showfield;
  }
  
  public void setCJRGroupFields(ArrayList<CJRGroupField> paramArrayList)
  {
    this.groupFields = paramArrayList;
  }
  
  public void setGroupFieldData(ArrayList<CJRRechargeVarientDetail> paramArrayList)
  {
    this.mGrouplist = paramArrayList;
  }
  
  public void setGroupInputFieldsArray(ArrayList<ArrayList<CJRInputFields>> paramArrayList)
  {
    this.mGroupFieldsArray = paramArrayList;
  }
  
  public void setInputFields(ArrayList<CJRInputFields> paramArrayList)
  {
    this.inputFields = paramArrayList;
  }
  
  public void setJSONObject(ArrayList<JSONObject> paramArrayList)
  {
    this.mJSONObject = paramArrayList;
  }
  
  public void setProductData(CJRRechargeDetailProductList paramCJRRechargeDetailProductList)
  {
    this.mProductList = paramCJRRechargeDetailProductList;
  }
  
  public void setRechargeProductList(ArrayList<CJRRechargeDetailProductList> paramArrayList)
  {
    this.mRechargeProductList = paramArrayList;
  }
  
  public void setShowfield(boolean paramBoolean)
  {
    this.showfield = paramBoolean;
  }
  
  public void setType(String paramString)
  {
    this.mType = paramString;
  }
  
  public void setVariantObject(ArrayList<JSONObject> paramArrayList)
  {
    this.mVArientObject = paramArrayList;
  }
  
  public void setmCheckedIds(ArrayList<String> paramArrayList)
  {
    this.mCheckedIds = paramArrayList;
  }
  
  public void setmJSONObject(ArrayList<JSONObject> paramArrayList)
  {
    this.mJSONObject = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRGroupField.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */