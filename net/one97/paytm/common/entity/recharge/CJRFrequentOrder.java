package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import com.google.c.e;
import java.util.Map;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFrequentOrder
  implements Comparable<CJRFrequentOrder>, IJRDataModel
{
  private static final long serialVersionUID = 1L;
  public final String TAG_PAY_TYPE = "paytype";
  public final String TAG_RECHARGE_AMOUNT = "price";
  public final String TAG_RECHARGE_AMOUNT2 = "price_new";
  public final String TAG_RECHARGE_NUMBER = "recharge_number";
  public final String TAG_RECHARGE_NUMBER_2 = "recharge_number_2";
  public final String TAG_SERVICE = "service";
  private String mCircle;
  @b(a="configuration")
  private Map<String, String> mConfiguration;
  @b(a="favLabel")
  private String mFavLabel;
  @b(a="favLabelId")
  private String mFavOrderId;
  @b(a="updated_at")
  private String mPaidOn;
  @b(a="price_new")
  private String mPriceNew;
  @b(a="product_id")
  private String mProductID;
  @b(a="product")
  private CJRFrequentOrderProduct mProducts;
  @b(a="frequency")
  private int mRechargeFrequency;
  private String mService;
  private boolean maskAmount = false;
  
  public int compareTo(CJRFrequentOrder paramCJRFrequentOrder)
  {
    return paramCJRFrequentOrder.getRechargeFrequency() - this.mRechargeFrequency;
  }
  
  public String getCircle()
  {
    return this.mProducts.getCircle();
  }
  
  public String getConfigAsJSON()
  {
    return new e().a(this.mConfiguration);
  }
  
  public Map<String, String> getConfiguration()
  {
    return this.mConfiguration;
  }
  
  public String getFavLabel()
  {
    return this.mFavLabel;
  }
  
  public String getFavOrderId()
  {
    return this.mFavOrderId;
  }
  
  public CJRFrequentOrderProduct getFrequentOrderProduct()
  {
    return this.mProducts;
  }
  
  public String getNewRechargeAmount()
  {
    if (this.mConfiguration.containsKey("price_new")) {
      return (String)this.mConfiguration.get("price_new");
    }
    return new StringBuilder().toString();
  }
  
  public String getOperator()
  {
    return this.mProducts.getOperator();
  }
  
  public String getOperatorLabel()
  {
    return this.mProducts.getOperatorLabel();
  }
  
  public String getPaidOn()
  {
    return this.mPaidOn;
  }
  
  public String getPayType()
  {
    return this.mProducts.getPaytype();
  }
  
  public String getProductID()
  {
    return this.mProductID;
  }
  
  public String getRechargeAmount()
  {
    if (this.mConfiguration.containsKey("price")) {
      return (String)this.mConfiguration.get("price");
    }
    return new StringBuilder().toString();
  }
  
  public int getRechargeFrequency()
  {
    return this.mRechargeFrequency;
  }
  
  public String getRechargeNumber()
  {
    if (this.mConfiguration.containsKey("recharge_number")) {
      return (String)this.mConfiguration.get("recharge_number");
    }
    return new StringBuilder().toString();
  }
  
  public String getRechargeNumber2()
  {
    if (this.mConfiguration.containsKey("recharge_number_2")) {
      return (String)this.mConfiguration.get("recharge_number_2");
    }
    return new StringBuilder().toString();
  }
  
  public String getService()
  {
    return this.mProducts.getService();
  }
  
  public boolean isAmountMasked()
  {
    return this.maskAmount;
  }
  
  public void setMaskAmount(boolean paramBoolean)
  {
    this.maskAmount = paramBoolean;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRFrequentOrder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */