package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrowsePlansDescAttributes
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="category_name")
  private String mCategoryName;
  @b(a="Circle")
  private String mCircle;
  @b(a="Disclaimer")
  private String mDisclaimer;
  @b(a="Operator")
  private String mOperator;
  @b(a="Talktime")
  private String mTalktime;
  @b(a="Validity")
  private String mValidity;
  
  public String getCategoryName()
  {
    return this.mCategoryName;
  }
  
  public String getCircle()
  {
    return this.mCircle;
  }
  
  public String getDisclaimer()
  {
    return this.mDisclaimer;
  }
  
  public String getOperator()
  {
    return this.mOperator;
  }
  
  public String getTalktime()
  {
    return this.mTalktime;
  }
  
  public String getValidity()
  {
    return this.mValidity;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRBrowsePlansDescAttributes.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */