package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import net.one97.paytm.common.entity.CJRError;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRRechargeConfigurationV2
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="error")
  private CJRError mError;
  @b(a="status")
  private String mStatus;
  
  public CJRError getError()
  {
    return this.mError;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRRechargeConfigurationV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */