package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRInstantOperator
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="Circle")
  private String mCircle;
  @b(a="Operator")
  private String mOperator;
  @b(a="postpaid")
  private boolean mPostPaid;
  @b(a="product_id")
  private String mProductId;
  @b(a="status")
  private boolean mStatus;
  
  public String getCircle()
  {
    return this.mCircle;
  }
  
  public String getOperator()
  {
    return this.mOperator;
  }
  
  public boolean getPostPaid()
  {
    return this.mPostPaid;
  }
  
  public boolean getStatus()
  {
    return this.mStatus;
  }
  
  public String getmProductId()
  {
    return this.mProductId;
  }
  
  public void setPostPaid(boolean paramBoolean)
  {
    this.mPostPaid = paramBoolean;
  }
  
  public void setStatus(boolean paramBoolean)
  {
    this.mStatus = paramBoolean;
  }
  
  public void setmOperator(String paramString)
  {
    this.mOperator = paramString;
  }
  
  public void setmProductId(String paramString)
  {
    this.mProductId = paramString;
  }
  
  public void setmircle(String paramString)
  {
    this.mCircle = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRInstantOperator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */