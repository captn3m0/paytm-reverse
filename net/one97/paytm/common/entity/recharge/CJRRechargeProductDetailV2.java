package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CJRRechargeProductDetailV2
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="meta_description")
  private String mMetaDescription;
  @b(a="name")
  private String mName;
  @b(a="removeProducts")
  private CJRRechargeUtilRemoveProduct mRemoveProducts;
  @b(a="variants")
  private ArrayList<CJRVarients> mVarients;
  private ArrayList<JSONObject> object = new ArrayList();
  
  public String getName()
  {
    return this.mName;
  }
  
  public ArrayList<JSONObject> getObject()
  {
    return this.object;
  }
  
  public String getProductMetaDescription()
  {
    return this.mMetaDescription;
  }
  
  public CJRRechargeUtilRemoveProduct getRemoveProducts()
  {
    return this.mRemoveProducts;
  }
  
  public ArrayList<CJRVarients> getVarients()
  {
    return this.mVarients;
  }
  
  public void parseJSON(String paramString)
  {
    try
    {
      paramString = new JSONObject(paramString);
      int i;
      JSONObject localJSONObject;
      if (paramString.has("variants"))
      {
        paramString = paramString.getJSONArray("variants");
        i = 0;
        while (i < paramString.length())
        {
          localJSONObject = paramString.getJSONObject(i);
          this.object.add(localJSONObject);
          i += 1;
        }
      }
      if (paramString.has("ProductList"))
      {
        paramString = paramString.getJSONArray("ProductList");
        i = 0;
        while (i < paramString.length())
        {
          localJSONObject = paramString.getJSONObject(i);
          this.object.add(localJSONObject);
          i += 1;
        }
      }
      return;
    }
    catch (JSONException paramString)
    {
      paramString.printStackTrace();
    }
  }
  
  public void setRemoveProducts(CJRRechargeUtilRemoveProduct paramCJRRechargeUtilRemoveProduct)
  {
    this.mRemoveProducts = paramCJRRechargeUtilRemoveProduct;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRRechargeProductDetailV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */