package net.one97.paytm.common.entity.recharge;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRRechargeProductV2
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="brand_image")
  private String mBrandImage;
  @b(a="group_field")
  public ArrayList<CJRGroupField> mGroupFields;
  @b(a="id")
  private String mId;
  @b(a="meta_description")
  private String mMetaData;
  @b(a="name")
  private String mName;
  @b(a="operator_image")
  private String mOperatorImage;
  @b(a="url")
  private String mUrl;
  
  public String getBrandImage()
  {
    return this.mBrandImage;
  }
  
  public ArrayList<CJRGroupField> getGroupFields()
  {
    return this.mGroupFields;
  }
  
  public String getId()
  {
    return this.mId;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getOperatorImage()
  {
    return this.mOperatorImage;
  }
  
  public String getOperatorName()
  {
    return this.mName;
  }
  
  public String getUrl()
  {
    return this.mUrl;
  }
  
  public String getmMetaData()
  {
    return this.mMetaData;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/recharge/CJRRechargeProductV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */