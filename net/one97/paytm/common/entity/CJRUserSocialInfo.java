package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRUserSocialInfo
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="authToken")
  private String mAuthToken;
  @b(a="picture")
  private String mImageUrl;
  @b(a="type")
  private String mType;
  @b(a="userId")
  private String mUserId;
  @b(a="username")
  private String mUserName;
  
  public String getAuthToken()
  {
    return this.mAuthToken;
  }
  
  public String getImageUrl()
  {
    return this.mImageUrl;
  }
  
  public String getType()
  {
    return this.mType;
  }
  
  public String getUserId()
  {
    return this.mUserId;
  }
  
  public String getUserName()
  {
    return this.mUserName;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRUserSocialInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */