package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.Iterator;

public class CJRFrontEndFilterItem
  extends CJRFilterValue
{
  private static final long serialVersionUID = 1L;
  @b(a="cats")
  private ArrayList<CJRFrontEndFilterItem> mFrontEndFilterItemList = new ArrayList();
  private CJRFrontEndFilterItem mParentFrontEndFilterItem;
  @b(a="parent_id")
  private String mParentId = null;
  
  public CJRFrontEndFilterItem getFrontEndFilterItemById(String paramString)
  {
    if (this.mFrontEndFilterItemList != null)
    {
      Iterator localIterator = this.mFrontEndFilterItemList.iterator();
      while (localIterator.hasNext())
      {
        CJRFrontEndFilterItem localCJRFrontEndFilterItem = (CJRFrontEndFilterItem)localIterator.next();
        if (localCJRFrontEndFilterItem.getID().equalsIgnoreCase(paramString)) {
          return localCJRFrontEndFilterItem;
        }
      }
    }
    return null;
  }
  
  public CJRFrontEndFilterItem getFrontEndFilterItemByName(String paramString1, String paramString2)
  {
    if (this.mFrontEndFilterItemList != null)
    {
      Iterator localIterator = this.mFrontEndFilterItemList.iterator();
      while (localIterator.hasNext())
      {
        CJRFrontEndFilterItem localCJRFrontEndFilterItem = (CJRFrontEndFilterItem)localIterator.next();
        if ((localCJRFrontEndFilterItem.getID().equalsIgnoreCase(paramString1)) && (localCJRFrontEndFilterItem.getName().equalsIgnoreCase(paramString2))) {
          return localCJRFrontEndFilterItem;
        }
      }
    }
    return null;
  }
  
  public ArrayList<CJRFrontEndFilterItem> getFrontEndFilterItemList()
  {
    return this.mFrontEndFilterItemList;
  }
  
  public CJRFrontEndFilterItem getParentFrontEndFilterItem()
  {
    return this.mParentFrontEndFilterItem;
  }
  
  public String getParentId()
  {
    return this.mParentId;
  }
  
  public CJRFrontEndFilterItem searchFrontEndFilterItemById(CJRFrontEndFilterItem paramCJRFrontEndFilterItem1, String paramString1, String paramString2, CJRFrontEndFilterItem paramCJRFrontEndFilterItem2)
  {
    CJRFrontEndFilterItem localCJRFrontEndFilterItem = paramCJRFrontEndFilterItem2;
    if (paramCJRFrontEndFilterItem2 == null)
    {
      localCJRFrontEndFilterItem = paramCJRFrontEndFilterItem1.getFrontEndFilterItemByName(paramString1, paramString2);
      if (localCJRFrontEndFilterItem != null) {
        return localCJRFrontEndFilterItem;
      }
      localCJRFrontEndFilterItem = paramCJRFrontEndFilterItem2;
      if (paramCJRFrontEndFilterItem1.getFrontEndFilterItemList() != null)
      {
        paramCJRFrontEndFilterItem1 = paramCJRFrontEndFilterItem1.getFrontEndFilterItemList().iterator();
        for (;;)
        {
          localCJRFrontEndFilterItem = paramCJRFrontEndFilterItem2;
          if (!paramCJRFrontEndFilterItem1.hasNext()) {
            break;
          }
          paramCJRFrontEndFilterItem2 = searchFrontEndFilterItemById((CJRFrontEndFilterItem)paramCJRFrontEndFilterItem1.next(), paramString1, paramString2, paramCJRFrontEndFilterItem2);
        }
      }
    }
    return localCJRFrontEndFilterItem;
  }
  
  public void setParentFrontEndFilterItem(CJRFrontEndFilterItem paramCJRFrontEndFilterItem)
  {
    this.mParentFrontEndFilterItem = paramCJRFrontEndFilterItem;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRFrontEndFilterItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */