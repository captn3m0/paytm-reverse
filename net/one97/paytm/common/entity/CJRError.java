package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRError
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="cancelButton")
  private String mCancelButtonText;
  @b(a="message")
  private String mMessage;
  @b(a="okbutton")
  private String mOkButtonText;
  @b(a="title")
  private String mTitle;
  
  public String getCancelButtonText()
  {
    return this.mCancelButtonText;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getOkButtonText()
  {
    return this.mOkButtonText;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
  
  public void setCancelButton(String paramString)
  {
    this.mCancelButtonText = paramString;
  }
  
  public void setMessage(String paramString)
  {
    this.mMessage = paramString;
  }
  
  public void setOkButton(String paramString)
  {
    this.mOkButtonText = paramString;
  }
  
  public void setTitle(String paramString)
  {
    this.mTitle = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRError.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */