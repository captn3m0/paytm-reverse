package net.one97.paytm.common.entity.gold;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGoldCart
  implements IJRDataModel
{
  @b(a="aggregate_item_price")
  private Integer aggregateItemPrice;
  @b(a="cart_items")
  private List<CJRGoldCartItem> cartItems = new ArrayList();
  @b(a="conv_fee")
  private Integer convFee;
  @b(a="count")
  private Integer count;
  @b(a="error")
  private String error;
  @b(a="final_price")
  private Integer finalPrice;
  @b(a="final_price_excl_shipping")
  private Integer finalPriceExclShipping;
  @b(a="need_shipping")
  private Boolean needShipping;
  @b(a="order_total")
  private Integer orderTotal;
  @b(a="paytm_cashback")
  private Integer paytmCashback;
  @b(a="paytm_discount")
  private Integer paytmDiscount;
  @b(a="place_order_url")
  private Object placeOrderUrl;
  @b(a="promostatus")
  private String promostatus;
  @b(a="service_options")
  private Boolean serviceOptions;
  @b(a="shipping_charges")
  private Integer shippingCharges;
  @b(a="site_id")
  private Integer siteId;
  @b(a="total_other_taxes")
  private List<Object> totalOtherTaxes = new ArrayList();
  
  public Integer getAggregateItemPrice()
  {
    return this.aggregateItemPrice;
  }
  
  public List<CJRGoldCartItem> getCartItems()
  {
    return this.cartItems;
  }
  
  public Integer getConvFee()
  {
    return this.convFee;
  }
  
  public Integer getCount()
  {
    return this.count;
  }
  
  public String getError()
  {
    return this.error;
  }
  
  public Integer getFinalPrice()
  {
    return this.finalPrice;
  }
  
  public Integer getFinalPriceExclShipping()
  {
    return this.finalPriceExclShipping;
  }
  
  public Boolean getNeedShipping()
  {
    return this.needShipping;
  }
  
  public Integer getOrderTotal()
  {
    return this.orderTotal;
  }
  
  public Integer getPaytmCashback()
  {
    return this.paytmCashback;
  }
  
  public Integer getPaytmDiscount()
  {
    return this.paytmDiscount;
  }
  
  public Object getPlaceOrderUrl()
  {
    return this.placeOrderUrl;
  }
  
  public String getPromostatus()
  {
    return this.promostatus;
  }
  
  public Boolean getServiceOptions()
  {
    return this.serviceOptions;
  }
  
  public Integer getShippingCharges()
  {
    return this.shippingCharges;
  }
  
  public Integer getSiteId()
  {
    return this.siteId;
  }
  
  public List<Object> getTotalOtherTaxes()
  {
    return this.totalOtherTaxes;
  }
  
  public void setAggregateItemPrice(Integer paramInteger)
  {
    this.aggregateItemPrice = paramInteger;
  }
  
  public void setCartItems(List<CJRGoldCartItem> paramList)
  {
    this.cartItems = paramList;
  }
  
  public void setConvFee(Integer paramInteger)
  {
    this.convFee = paramInteger;
  }
  
  public void setCount(Integer paramInteger)
  {
    this.count = paramInteger;
  }
  
  public void setError(String paramString)
  {
    this.error = paramString;
  }
  
  public void setFinalPrice(Integer paramInteger)
  {
    this.finalPrice = paramInteger;
  }
  
  public void setFinalPriceExclShipping(Integer paramInteger)
  {
    this.finalPriceExclShipping = paramInteger;
  }
  
  public void setNeedShipping(Boolean paramBoolean)
  {
    this.needShipping = paramBoolean;
  }
  
  public void setOrderTotal(Integer paramInteger)
  {
    this.orderTotal = paramInteger;
  }
  
  public void setPaytmCashback(Integer paramInteger)
  {
    this.paytmCashback = paramInteger;
  }
  
  public void setPaytmDiscount(Integer paramInteger)
  {
    this.paytmDiscount = paramInteger;
  }
  
  public void setPlaceOrderUrl(Object paramObject)
  {
    this.placeOrderUrl = paramObject;
  }
  
  public void setPromostatus(String paramString)
  {
    this.promostatus = paramString;
  }
  
  public void setServiceOptions(Boolean paramBoolean)
  {
    this.serviceOptions = paramBoolean;
  }
  
  public void setShippingCharges(Integer paramInteger)
  {
    this.shippingCharges = paramInteger;
  }
  
  public void setSiteId(Integer paramInteger)
  {
    this.siteId = paramInteger;
  }
  
  public void setTotalOtherTaxes(List<Object> paramList)
  {
    this.totalOtherTaxes = paramList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/gold/CJRGoldCart.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */