package net.one97.paytm.common.entity.gold;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGoldStatus
  implements IJRDataModel
{
  @b(a="code")
  private Integer code;
  @b(a="message")
  private CJRGoldMessage message;
  @b(a="result")
  private String result;
  
  public Integer getCode()
  {
    return this.code;
  }
  
  public CJRGoldMessage getMessage()
  {
    return this.message;
  }
  
  public String getResult()
  {
    return this.result;
  }
  
  public void setCode(Integer paramInteger)
  {
    this.code = paramInteger;
  }
  
  public void setMessage(CJRGoldMessage paramCJRGoldMessage)
  {
    this.message = paramCJRGoldMessage;
  }
  
  public void setResult(String paramString)
  {
    this.result = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/gold/CJRGoldStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */