package net.one97.paytm.common.entity.gold;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.CJRItem;
import net.one97.paytm.common.entity.shopping.CJRRelatedCategory;

public class CJRGoldOfferItems
  extends CJRItem
{
  private static final long serialVersionUID = 1L;
  @b(a="alt_image_url")
  private String mAltImageUrl;
  @b(a="delete_url")
  private String mDelete_url;
  @b(a="id")
  private int mId;
  @b(a="image_url")
  private String mImageUrl;
  @b(a="name")
  private String mName;
  @b(a="priority")
  private int mPriority;
  @b(a="status")
  private String mStatus;
  @b(a="url")
  private String mUrl;
  @b(a="url_info")
  private String mUrlInfo;
  @b(a="url_type")
  private String mUrlType;
  @b(a="seourl")
  private String mseourl;
  @b(a="url_key")
  private String murl_key;
  @b(a="tnc")
  private List<String> tnc;
  
  public String getBrand()
  {
    return null;
  }
  
  public String getCategoryId()
  {
    return null;
  }
  
  public String getItemID()
  {
    return "" + this.mId;
  }
  
  public String getLabel()
  {
    return null;
  }
  
  public String getListId()
  {
    return "";
  }
  
  public String getListName()
  {
    return null;
  }
  
  public int getListPosition()
  {
    return 0;
  }
  
  public String getMurl_key()
  {
    return this.murl_key;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getParentID()
  {
    return "";
  }
  
  public ArrayList<CJRRelatedCategory> getRelatedCategories()
  {
    return null;
  }
  
  public String getSearchABValue()
  {
    return null;
  }
  
  public String getSearchCategory()
  {
    return null;
  }
  
  public String getSearchResultType()
  {
    return null;
  }
  
  public String getSearchTerm()
  {
    return null;
  }
  
  public String getSearchType()
  {
    return null;
  }
  
  public List<String> getTnc()
  {
    return this.tnc;
  }
  
  public String getURL()
  {
    return this.mUrl;
  }
  
  public String getURLType()
  {
    return this.mUrlType;
  }
  
  public String getmAltImageUrl()
  {
    return this.mAltImageUrl;
  }
  
  public String getmContainerInstanceID()
  {
    return "";
  }
  
  public String getmDelete_url()
  {
    return this.mDelete_url;
  }
  
  public int getmId()
  {
    return this.mId;
  }
  
  public String getmImageUrl()
  {
    return this.mImageUrl;
  }
  
  public String getmName()
  {
    return this.mName;
  }
  
  public int getmPriority()
  {
    return this.mPriority;
  }
  
  public String getmStatus()
  {
    return this.mStatus;
  }
  
  public String getmUrl()
  {
    return this.mUrl;
  }
  
  public String getmUrlInfo()
  {
    return this.mUrlInfo;
  }
  
  public String getmUrlType()
  {
    return this.mUrlType;
  }
  
  public void setMurl_key(String paramString)
  {
    this.murl_key = paramString;
  }
  
  public void setTnc(List<String> paramList)
  {
    this.tnc = paramList;
  }
  
  public void setmAltImageUrl(String paramString)
  {
    this.mAltImageUrl = paramString;
  }
  
  public void setmDelete_url(String paramString)
  {
    this.mDelete_url = paramString;
  }
  
  public void setmId(int paramInt)
  {
    this.mId = paramInt;
  }
  
  public void setmImageUrl(String paramString)
  {
    this.mImageUrl = paramString;
  }
  
  public void setmName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setmPriority(int paramInt)
  {
    this.mPriority = paramInt;
  }
  
  public void setmStatus(String paramString)
  {
    this.mStatus = paramString;
  }
  
  public void setmUrl(String paramString)
  {
    this.mUrl = paramString;
  }
  
  public void setmUrlInfo(String paramString)
  {
    this.mUrlInfo = paramString;
  }
  
  public void setmUrlType(String paramString)
  {
    this.mUrlType = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/gold/CJRGoldOfferItems.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */