package net.one97.paytm.common.entity.gold;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGoldConfiguration
  implements IJRDataModel
{
  @b(a="price")
  private Integer price;
  @b(a="recharge_number")
  private Integer rechargeNumber;
  @b(a="recharge_number_2")
  private String rechargeNumber2;
  @b(a="recharge_number_3")
  private String rechargeNumber3;
  @b(a="recharge_number_4")
  private Integer rechargeNumber4;
  @b(a="recharge_number_5")
  private String rechargeNumber5;
  @b(a="recharge_number_6")
  private String rechargeNumber6;
  
  public Integer getPrice()
  {
    return this.price;
  }
  
  public Integer getRechargeNumber()
  {
    return this.rechargeNumber;
  }
  
  public String getRechargeNumber2()
  {
    return this.rechargeNumber2;
  }
  
  public String getRechargeNumber3()
  {
    return this.rechargeNumber3;
  }
  
  public Integer getRechargeNumber4()
  {
    return this.rechargeNumber4;
  }
  
  public String getRechargeNumber5()
  {
    return this.rechargeNumber5;
  }
  
  public String getRechargeNumber6()
  {
    return this.rechargeNumber6;
  }
  
  public void setPrice(Integer paramInteger)
  {
    this.price = paramInteger;
  }
  
  public void setRechargeNumber(Integer paramInteger)
  {
    this.rechargeNumber = paramInteger;
  }
  
  public void setRechargeNumber2(String paramString)
  {
    this.rechargeNumber2 = paramString;
  }
  
  public void setRechargeNumber3(String paramString)
  {
    this.rechargeNumber3 = paramString;
  }
  
  public void setRechargeNumber4(Integer paramInteger)
  {
    this.rechargeNumber4 = paramInteger;
  }
  
  public void setRechargeNumber5(String paramString)
  {
    this.rechargeNumber5 = paramString;
  }
  
  public void setRechargeNumber6(String paramString)
  {
    this.rechargeNumber6 = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/gold/CJRGoldConfiguration.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */