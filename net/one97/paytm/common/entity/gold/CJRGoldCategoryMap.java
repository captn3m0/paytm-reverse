package net.one97.paytm.common.entity.gold;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGoldCategoryMap
  implements IJRDataModel
{
  @b(a="id")
  private Integer id;
  @b(a="name")
  private String name;
  @b(a="parent_id")
  private Integer parentId;
  @b(a="url_key")
  private String urlKey;
  @b(a="vat_restrict")
  private Integer vatRestrict;
  
  public Integer getId()
  {
    return this.id;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public Integer getParentId()
  {
    return this.parentId;
  }
  
  public String getUrlKey()
  {
    return this.urlKey;
  }
  
  public Integer getVatRestrict()
  {
    return this.vatRestrict;
  }
  
  public void setId(Integer paramInteger)
  {
    this.id = paramInteger;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setParentId(Integer paramInteger)
  {
    this.parentId = paramInteger;
  }
  
  public void setUrlKey(String paramString)
  {
    this.urlKey = paramString;
  }
  
  public void setVatRestrict(Integer paramInteger)
  {
    this.vatRestrict = paramInteger;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/gold/CJRGoldCategoryMap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */