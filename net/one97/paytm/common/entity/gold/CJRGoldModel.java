package net.one97.paytm.common.entity.gold;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGoldModel
  implements IJRDataModel
{
  @b(a="cart")
  private CJRGoldCart cart;
  @b(a="status")
  private CJRGoldStatus status;
  
  public CJRGoldCart getCart()
  {
    return this.cart;
  }
  
  public CJRGoldStatus getStatus()
  {
    return this.status;
  }
  
  public void setCart(CJRGoldCart paramCJRGoldCart)
  {
    this.cart = paramCJRGoldCart;
  }
  
  public void setStatus(CJRGoldStatus paramCJRGoldStatus)
  {
    this.status = paramCJRGoldStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/gold/CJRGoldModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */