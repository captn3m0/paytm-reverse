package net.one97.paytm.common.entity.gold;

import android.content.res.Configuration;
import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGoldCartItem
  implements IJRDataModel
{
  @b(a="aggregate_item_price")
  private Integer aggregateItemPrice;
  @b(a="attributes_dim_values")
  private CJRGoldAttributesDimValues attributesDimValues;
  @b(a="available_quantity")
  private Object availableQuantity;
  @b(a="brand")
  private String brand;
  @b(a="brand_id")
  private Integer brandId;
  @b(a="categoryMap")
  private List<CJRGoldCategoryMap> categoryMap = new ArrayList();
  @b(a="configuration")
  private Configuration configuration;
  @b(a="conv_fee")
  private Integer convFee;
  @b(a="conv_fee_map")
  private CJRGoldConvFeeMap convFeeMap;
  @b(a="discounted_price")
  private Integer discountedPrice;
  @b(a="display_sequence")
  private Integer displaySequence;
  @b(a="estimated_delivery")
  private Object estimatedDelivery;
  @b(a="ga_key")
  private String gaKey;
  @b(a="image_url")
  private String imageUrl;
  @b(a="item_id")
  private String itemId;
  @b(a="merchant_id")
  private Integer merchantId;
  @b(a="merchant_name")
  private String merchantName;
  @b(a="meta_data")
  private CJRGoldMetaData metaData;
  @b(a="mrp")
  private Integer mrp;
  @b(a="name")
  private String name;
  @b(a="need_shipping")
  private Boolean needShipping;
  @b(a="offer_url")
  private String offerUrl;
  @b(a="other_taxes")
  private List<Object> otherTaxes = new ArrayList();
  @b(a="parent_id")
  private Object parentId;
  @b(a="price")
  private Integer price;
  @b(a="price_incl_taxes")
  private List<Object> priceInclTaxes = new ArrayList();
  @b(a="product_id")
  private Integer productId;
  @b(a="promostatus")
  private String promostatus;
  @b(a="quantity")
  private Integer quantity;
  @b(a="readonly")
  private Boolean readonly;
  @b(a="selling_price")
  private Integer sellingPrice;
  @b(a="seourl")
  private String seourl;
  @b(a="service_options")
  private CJRGoldServiceOptions serviceOptions;
  @b(a="shipping_charges")
  private Integer shippingCharges;
  @b(a="short_description")
  private CJRGoldShortDescription shortDescription;
  @b(a="title")
  private String title;
  @b(a="total_price")
  private Integer totalPrice;
  @b(a="url")
  private String url;
  @b(a="vertical_id")
  private Integer verticalId;
  @b(a="vertical_label")
  private String verticalLabel;
  
  public Integer getAggregateItemPrice()
  {
    return this.aggregateItemPrice;
  }
  
  public CJRGoldAttributesDimValues getAttributesDimValues()
  {
    return this.attributesDimValues;
  }
  
  public Object getAvailableQuantity()
  {
    return this.availableQuantity;
  }
  
  public String getBrand()
  {
    return this.brand;
  }
  
  public Integer getBrandId()
  {
    return this.brandId;
  }
  
  public List<CJRGoldCategoryMap> getCategoryMap()
  {
    return this.categoryMap;
  }
  
  public Configuration getConfiguration()
  {
    return this.configuration;
  }
  
  public Integer getConvFee()
  {
    return this.convFee;
  }
  
  public CJRGoldConvFeeMap getConvFeeMap()
  {
    return this.convFeeMap;
  }
  
  public Integer getDiscountedPrice()
  {
    return this.discountedPrice;
  }
  
  public Integer getDisplaySequence()
  {
    return this.displaySequence;
  }
  
  public Object getEstimatedDelivery()
  {
    return this.estimatedDelivery;
  }
  
  public String getGaKey()
  {
    return this.gaKey;
  }
  
  public String getImageUrl()
  {
    return this.imageUrl;
  }
  
  public String getItemId()
  {
    return this.itemId;
  }
  
  public Integer getMerchantId()
  {
    return this.merchantId;
  }
  
  public String getMerchantName()
  {
    return this.merchantName;
  }
  
  public CJRGoldMetaData getMetaData()
  {
    return this.metaData;
  }
  
  public Integer getMrp()
  {
    return this.mrp;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public Boolean getNeedShipping()
  {
    return this.needShipping;
  }
  
  public String getOfferUrl()
  {
    return this.offerUrl;
  }
  
  public List<Object> getOtherTaxes()
  {
    return this.otherTaxes;
  }
  
  public Object getParentId()
  {
    return this.parentId;
  }
  
  public Integer getPrice()
  {
    return this.price;
  }
  
  public List<Object> getPriceInclTaxes()
  {
    return this.priceInclTaxes;
  }
  
  public Integer getProductId()
  {
    return this.productId;
  }
  
  public String getPromostatus()
  {
    return this.promostatus;
  }
  
  public Integer getQuantity()
  {
    return this.quantity;
  }
  
  public Boolean getReadonly()
  {
    return this.readonly;
  }
  
  public Integer getSellingPrice()
  {
    return this.sellingPrice;
  }
  
  public String getSeourl()
  {
    return this.seourl;
  }
  
  public CJRGoldServiceOptions getServiceOptions()
  {
    return this.serviceOptions;
  }
  
  public Integer getShippingCharges()
  {
    return this.shippingCharges;
  }
  
  public CJRGoldShortDescription getShortDescription()
  {
    return this.shortDescription;
  }
  
  public String getTitle()
  {
    return this.title;
  }
  
  public Integer getTotalPrice()
  {
    return this.totalPrice;
  }
  
  public String getUrl()
  {
    return this.url;
  }
  
  public Integer getVerticalId()
  {
    return this.verticalId;
  }
  
  public String getVerticalLabel()
  {
    return this.verticalLabel;
  }
  
  public void setAggregateItemPrice(Integer paramInteger)
  {
    this.aggregateItemPrice = paramInteger;
  }
  
  public void setAttributesDimValues(CJRGoldAttributesDimValues paramCJRGoldAttributesDimValues)
  {
    this.attributesDimValues = paramCJRGoldAttributesDimValues;
  }
  
  public void setAvailableQuantity(Object paramObject)
  {
    this.availableQuantity = paramObject;
  }
  
  public void setBrand(String paramString)
  {
    this.brand = paramString;
  }
  
  public void setBrandId(Integer paramInteger)
  {
    this.brandId = paramInteger;
  }
  
  public void setCategoryMap(List<CJRGoldCategoryMap> paramList)
  {
    this.categoryMap = paramList;
  }
  
  public void setConfiguration(Configuration paramConfiguration)
  {
    this.configuration = paramConfiguration;
  }
  
  public void setConvFee(Integer paramInteger)
  {
    this.convFee = paramInteger;
  }
  
  public void setConvFeeMap(CJRGoldConvFeeMap paramCJRGoldConvFeeMap)
  {
    this.convFeeMap = paramCJRGoldConvFeeMap;
  }
  
  public void setDiscountedPrice(Integer paramInteger)
  {
    this.discountedPrice = paramInteger;
  }
  
  public void setDisplaySequence(Integer paramInteger)
  {
    this.displaySequence = paramInteger;
  }
  
  public void setEstimatedDelivery(Object paramObject)
  {
    this.estimatedDelivery = paramObject;
  }
  
  public void setGaKey(String paramString)
  {
    this.gaKey = paramString;
  }
  
  public void setImageUrl(String paramString)
  {
    this.imageUrl = paramString;
  }
  
  public void setItemId(String paramString)
  {
    this.itemId = paramString;
  }
  
  public void setMerchantId(Integer paramInteger)
  {
    this.merchantId = paramInteger;
  }
  
  public void setMerchantName(String paramString)
  {
    this.merchantName = paramString;
  }
  
  public void setMetaData(CJRGoldMetaData paramCJRGoldMetaData)
  {
    this.metaData = paramCJRGoldMetaData;
  }
  
  public void setMrp(Integer paramInteger)
  {
    this.mrp = paramInteger;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setNeedShipping(Boolean paramBoolean)
  {
    this.needShipping = paramBoolean;
  }
  
  public void setOfferUrl(String paramString)
  {
    this.offerUrl = paramString;
  }
  
  public void setOtherTaxes(List<Object> paramList)
  {
    this.otherTaxes = paramList;
  }
  
  public void setParentId(Object paramObject)
  {
    this.parentId = paramObject;
  }
  
  public void setPrice(Integer paramInteger)
  {
    this.price = paramInteger;
  }
  
  public void setPriceInclTaxes(List<Object> paramList)
  {
    this.priceInclTaxes = paramList;
  }
  
  public void setProductId(Integer paramInteger)
  {
    this.productId = paramInteger;
  }
  
  public void setPromostatus(String paramString)
  {
    this.promostatus = paramString;
  }
  
  public void setQuantity(Integer paramInteger)
  {
    this.quantity = paramInteger;
  }
  
  public void setReadonly(Boolean paramBoolean)
  {
    this.readonly = paramBoolean;
  }
  
  public void setSellingPrice(Integer paramInteger)
  {
    this.sellingPrice = paramInteger;
  }
  
  public void setSeourl(String paramString)
  {
    this.seourl = paramString;
  }
  
  public void setServiceOptions(CJRGoldServiceOptions paramCJRGoldServiceOptions)
  {
    this.serviceOptions = paramCJRGoldServiceOptions;
  }
  
  public void setShippingCharges(Integer paramInteger)
  {
    this.shippingCharges = paramInteger;
  }
  
  public void setShortDescription(CJRGoldShortDescription paramCJRGoldShortDescription)
  {
    this.shortDescription = paramCJRGoldShortDescription;
  }
  
  public void setTitle(String paramString)
  {
    this.title = paramString;
  }
  
  public void setTotalPrice(Integer paramInteger)
  {
    this.totalPrice = paramInteger;
  }
  
  public void setUrl(String paramString)
  {
    this.url = paramString;
  }
  
  public void setVerticalId(Integer paramInteger)
  {
    this.verticalId = paramInteger;
  }
  
  public void setVerticalLabel(String paramString)
  {
    this.verticalLabel = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/gold/CJRGoldCartItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */