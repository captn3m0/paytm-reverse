package net.one97.paytm.common.entity.gold;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGoldServiceOptions
  implements IJRDataModel
{
  @b(a="actions")
  private List<CJRGoldAction> actions = new ArrayList();
  
  public List<CJRGoldAction> getActions()
  {
    return this.actions;
  }
  
  public void setActions(List<CJRGoldAction> paramList)
  {
    this.actions = paramList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/gold/CJRGoldServiceOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */