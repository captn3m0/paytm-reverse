package net.one97.paytm.common.entity.gold;

import com.google.c.a.b;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGoldOfferLayout
  implements IJRDataModel
{
  @b(a="id")
  private int mId;
  @b(a="layout")
  private String mLayout;
  @b(a="name")
  private String mName;
  @b(a="items")
  private List<CJRGoldOfferItems> mOfferItems;
  @b(a="priority")
  private int mPriority;
  @b(a="status")
  private int mStatus;
  
  public int getmId()
  {
    return this.mId;
  }
  
  public String getmLayout()
  {
    return this.mLayout;
  }
  
  public String getmName()
  {
    return this.mName;
  }
  
  public List<CJRGoldOfferItems> getmOfferItems()
  {
    return this.mOfferItems;
  }
  
  public int getmPriority()
  {
    return this.mPriority;
  }
  
  public int getmStatus()
  {
    return this.mStatus;
  }
  
  public void setmId(int paramInt)
  {
    this.mId = paramInt;
  }
  
  public void setmLayout(String paramString)
  {
    this.mLayout = paramString;
  }
  
  public void setmName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setmOfferItems(List<CJRGoldOfferItems> paramList)
  {
    this.mOfferItems = paramList;
  }
  
  public void setmPriority(int paramInt)
  {
    this.mPriority = paramInt;
  }
  
  public void setmStatus(int paramInt)
  {
    this.mStatus = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/gold/CJRGoldOfferLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */