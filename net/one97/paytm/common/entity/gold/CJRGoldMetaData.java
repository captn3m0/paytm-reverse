package net.one97.paytm.common.entity.gold;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGoldMetaData
  implements IJRDataModel
{
  @b(a="due_amount")
  private Double dueAmount;
  
  public Double getDueAmount()
  {
    return this.dueAmount;
  }
  
  public void setDueAmount(Double paramDouble)
  {
    this.dueAmount = paramDouble;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/gold/CJRGoldMetaData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */