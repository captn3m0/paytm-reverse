package net.one97.paytm.common.entity.gold;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPincodeModel
  implements IJRDataModel
{
  @b(a="cod_enabled")
  private Boolean codEnabled;
  @b(a="enabled")
  private Boolean enabled;
  @b(a="pincode")
  private String pincode;
  @b(a="shipping_charges")
  private Integer shippingCharges;
  
  public Boolean getCodEnabled()
  {
    return this.codEnabled;
  }
  
  public Boolean getEnabled()
  {
    return this.enabled;
  }
  
  public String getPincode()
  {
    return this.pincode;
  }
  
  public Integer getShippingCharges()
  {
    return this.shippingCharges;
  }
  
  public void setCodEnabled(Boolean paramBoolean)
  {
    this.codEnabled = paramBoolean;
  }
  
  public void setEnabled(Boolean paramBoolean)
  {
    this.enabled = paramBoolean;
  }
  
  public void setPincode(String paramString)
  {
    this.pincode = paramString;
  }
  
  public void setShippingCharges(Integer paramInteger)
  {
    this.shippingCharges = paramInteger;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/gold/CJRPincodeModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */