package net.one97.paytm.common.entity.gold;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRGoldAction
  implements IJRDataModel
{
  @b(a="billAmount")
  private Double billAmount;
  @b(a="bill_amount_max")
  private Object billAmountMax;
  @b(a="bill_amount_min")
  private Object billAmountMin;
  @b(a="billDueDate")
  private Object billDueDate;
  @b(a="billamount_editable")
  private Boolean billamountEditable;
  @b(a="caNumber")
  private Object caNumber;
  @b(a="course")
  private Object course;
  @b(a="customerBalance")
  private String customerBalance;
  @b(a="customerEmail")
  private Object customerEmail;
  @b(a="customerName")
  private Object customerName;
  @b(a="displayValues")
  private List<Object> displayValues = new ArrayList();
  @b(a="dob")
  private Object dob;
  @b(a="fatherName")
  private Object fatherName;
  @b(a="inputNumberType")
  private Object inputNumberType;
  @b(a="label")
  private String label;
  @b(a="operatorName")
  private Object operatorName;
  @b(a="section")
  private Object section;
  @b(a="studentClass")
  private Object studentClass;
  @b(a="year")
  private Object year;
  
  public Double getBillAmount()
  {
    return this.billAmount;
  }
  
  public Object getBillAmountMax()
  {
    return this.billAmountMax;
  }
  
  public Object getBillAmountMin()
  {
    return this.billAmountMin;
  }
  
  public Object getBillDueDate()
  {
    return this.billDueDate;
  }
  
  public Boolean getBillamountEditable()
  {
    return this.billamountEditable;
  }
  
  public Object getCaNumber()
  {
    return this.caNumber;
  }
  
  public Object getCourse()
  {
    return this.course;
  }
  
  public String getCustomerBalance()
  {
    return this.customerBalance;
  }
  
  public Object getCustomerEmail()
  {
    return this.customerEmail;
  }
  
  public Object getCustomerName()
  {
    return this.customerName;
  }
  
  public List<Object> getDisplayValues()
  {
    return this.displayValues;
  }
  
  public Object getDob()
  {
    return this.dob;
  }
  
  public Object getFatherName()
  {
    return this.fatherName;
  }
  
  public Object getInputNumberType()
  {
    return this.inputNumberType;
  }
  
  public String getLabel()
  {
    return this.label;
  }
  
  public Object getOperatorName()
  {
    return this.operatorName;
  }
  
  public Object getSection()
  {
    return this.section;
  }
  
  public Object getStudentClass()
  {
    return this.studentClass;
  }
  
  public Object getYear()
  {
    return this.year;
  }
  
  public void setBillAmount(Double paramDouble)
  {
    this.billAmount = paramDouble;
  }
  
  public void setBillAmountMax(Object paramObject)
  {
    this.billAmountMax = paramObject;
  }
  
  public void setBillAmountMin(Object paramObject)
  {
    this.billAmountMin = paramObject;
  }
  
  public void setBillDueDate(Object paramObject)
  {
    this.billDueDate = paramObject;
  }
  
  public void setBillamountEditable(Boolean paramBoolean)
  {
    this.billamountEditable = paramBoolean;
  }
  
  public void setCaNumber(Object paramObject)
  {
    this.caNumber = paramObject;
  }
  
  public void setCourse(Object paramObject)
  {
    this.course = paramObject;
  }
  
  public void setCustomerBalance(String paramString)
  {
    this.customerBalance = paramString;
  }
  
  public void setCustomerEmail(Object paramObject)
  {
    this.customerEmail = paramObject;
  }
  
  public void setCustomerName(Object paramObject)
  {
    this.customerName = paramObject;
  }
  
  public void setDisplayValues(List<Object> paramList)
  {
    this.displayValues = paramList;
  }
  
  public void setDob(Object paramObject)
  {
    this.dob = paramObject;
  }
  
  public void setFatherName(Object paramObject)
  {
    this.fatherName = paramObject;
  }
  
  public void setInputNumberType(Object paramObject)
  {
    this.inputNumberType = paramObject;
  }
  
  public void setLabel(String paramString)
  {
    this.label = paramString;
  }
  
  public void setOperatorName(Object paramObject)
  {
    this.operatorName = paramObject;
  }
  
  public void setSection(Object paramObject)
  {
    this.section = paramObject;
  }
  
  public void setStudentClass(Object paramObject)
  {
    this.studentClass = paramObject;
  }
  
  public void setYear(Object paramObject)
  {
    this.year = paramObject;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/gold/CJRGoldAction.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */