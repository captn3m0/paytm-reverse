package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRCashWalletResponse
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="amount")
  private double mAmount;
  
  public double getAmount()
  {
    return this.mAmount;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRCashWalletResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */