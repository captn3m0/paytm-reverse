package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRDeepLinkData
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="deepLink")
  private String mDeepLink;
  
  public String getDeepLink()
  {
    return this.mDeepLink;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRDeepLinkData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */