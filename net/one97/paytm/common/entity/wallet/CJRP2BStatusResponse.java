package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRP2BStatusResponse
  implements IJRDataModel
{
  @b(a="validForTxn")
  private boolean isValidForTxn;
  @b(a="txnWiseResponse")
  private CJRP2BStatusTxnWiseResponse mStatusTxnWiseResponse;
  @b(a="txnWiseMessages")
  private ArrayList<String> mTxnWiseMessages;
  
  public CJRP2BStatusTxnWiseResponse getStatusTxnWiseResponse()
  {
    return this.mStatusTxnWiseResponse;
  }
  
  public ArrayList<String> getTxnWiseMessages()
  {
    return this.mTxnWiseMessages;
  }
  
  public boolean isValidForTxn()
  {
    return this.isValidForTxn;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRP2BStatusResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */