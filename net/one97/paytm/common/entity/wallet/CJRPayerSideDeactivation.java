package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPayerSideDeactivation
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="statusCode")
  private String mStatusCode;
  @b(a="statusMessage")
  private String mStatusMessage;
  
  public String getmStatusCode()
  {
    return this.mStatusCode;
  }
  
  public String getmStatusMessage()
  {
    return this.mStatusMessage;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRPayerSideDeactivation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */