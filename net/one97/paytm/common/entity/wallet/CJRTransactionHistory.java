package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTransactionHistory
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="status")
  private String mStatus;
  @b(a="data")
  private ArrayList<CJRTransactionData> mTransactionList;
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public ArrayList<CJRTransactionData> getTransactionList()
  {
    return this.mTransactionList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRTransactionHistory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */