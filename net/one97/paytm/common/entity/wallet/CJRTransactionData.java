package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTransactionData
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="amount")
  private String mAmount;
  @b(a="info")
  private String mInfo;
  @b(a="refundToBank")
  private boolean mRefundToBank;
  @b(a="status")
  private String mStatus;
  @b(a="text")
  private String mText;
  @b(a="txnDate")
  private String mTxnDate;
  @b(a="txnGuid")
  private String mTxnGuid;
  @b(a="txntype")
  private String mTxntype;
  
  public String getAmount()
  {
    return this.mAmount;
  }
  
  public String getInfo()
  {
    return this.mInfo;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public String getText()
  {
    return this.mText;
  }
  
  public String getTxnDate()
  {
    return this.mTxnDate;
  }
  
  public String getTxnGuid()
  {
    return this.mTxnGuid;
  }
  
  public String getTxntype()
  {
    return this.mTxntype;
  }
  
  public boolean isRefundToBank()
  {
    return this.mRefundToBank;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRTransactionData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */