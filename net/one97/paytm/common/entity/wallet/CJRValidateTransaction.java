package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRValidateTransaction
  implements IJRDataModel
{
  @b(a="response")
  private CJRSendMoneyResponse mResponse;
  @b(a="status")
  private String mStatus;
  @b(a="statusCode")
  private String mStatusCode;
  @b(a="statusMessage")
  private String mStatusMessage;
  @b(a="orderId")
  private String orderId;
  @b(a="requestGuid")
  private String reuestGuid;
  
  public CJRSendMoneyResponse getResponse()
  {
    return this.mResponse;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public String getStatusCode()
  {
    return this.mStatusCode;
  }
  
  public String getStatusMessage()
  {
    return this.mStatusMessage;
  }
  
  public String getorderId()
  {
    return this.orderId;
  }
  
  public String getrequestGUID()
  {
    return this.reuestGuid;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRValidateTransaction.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */