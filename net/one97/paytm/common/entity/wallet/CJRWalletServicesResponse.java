package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRWalletServicesResponse
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="paytmWalletServices")
  private ArrayList<CJRPaytmWalletService> mPaytmWalletService;
  
  public String getName()
  {
    return null;
  }
  
  public ArrayList<CJRPaytmWalletService> getPaytmWalletService()
  {
    return this.mPaytmWalletService;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRWalletServicesResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */