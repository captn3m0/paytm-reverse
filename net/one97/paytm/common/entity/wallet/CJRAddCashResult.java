package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAddCashResult
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="data")
  private String mData;
  @b(a="status")
  private String mStatus;
  
  public String getData()
  {
    return this.mData;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRAddCashResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */