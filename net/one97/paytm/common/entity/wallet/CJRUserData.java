package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRUserData
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="balance")
  private String mBalance;
  @b(a="displaymessage")
  private String mDisplayMessage;
  @b(a="message")
  private String mMessage;
  @b(a="otpRequired")
  private String mOtpRequired;
  @b(a="ownerGuid")
  private String mOwnerGuid;
  @b(a="refundParams")
  private CJRRefundParams mRefundParams;
  @b(a="status")
  private String mStatus;
  @b(a="txnPinRequired")
  private String mTxnPinRequired;
  @b(a="walletType")
  private String mWalletType;
  
  public String getBalance()
  {
    return this.mBalance;
  }
  
  public String getDisplayMessage()
  {
    return this.mDisplayMessage;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getOtpRequired()
  {
    return this.mOtpRequired;
  }
  
  public String getOwnerGuid()
  {
    return this.mOwnerGuid;
  }
  
  public CJRRefundParams getRefundParams()
  {
    return this.mRefundParams;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public String getTxnPinRequired()
  {
    return this.mTxnPinRequired;
  }
  
  public String getWalletType()
  {
    return this.mWalletType;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRUserData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */