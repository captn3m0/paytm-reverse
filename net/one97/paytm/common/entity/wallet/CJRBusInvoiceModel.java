package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRBusInvoiceModel
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 2L;
  @b(a="response")
  private BusResponseModel busResponseModel;
  @b(a="orderId")
  private String mOrderId;
  @b(a="requestGuid")
  private String mRequestFGuid;
  @b(a="statusCode")
  private String mStatusCode;
  @b(a="statusMessage")
  private String mStatusMessage;
  @b(a="status")
  private String status;
  
  public BusResponseModel getBusResponseModel()
  {
    return this.busResponseModel;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getStatus()
  {
    return this.status;
  }
  
  public String getStatusCode()
  {
    return this.mStatusCode;
  }
  
  public String getmOrderId()
  {
    return this.mOrderId;
  }
  
  public String getmStatusMessage()
  {
    return this.mStatusMessage;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRBusInvoiceModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */