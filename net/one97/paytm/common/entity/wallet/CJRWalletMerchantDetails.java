package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRWalletMerchantDetails
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="image")
  private String mImage;
  @b(a="noOfColumns")
  private int mNoOfColumns;
  @b(a="noOfRows")
  private int mNoOfRows;
  @b(a="urls")
  private List<String> mUrlList;
  
  public String getImage()
  {
    return this.mImage;
  }
  
  public int getNoOfColumns()
  {
    return this.mNoOfColumns;
  }
  
  public int getNoOfRows()
  {
    return this.mNoOfRows;
  }
  
  public List<String> getUrlList()
  {
    return this.mUrlList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRWalletMerchantDetails.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */