package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRMerchantListResponse
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="response")
  private CJRMerchantListContainer mMerchantListContainer;
  
  public CJRMerchantListContainer getMerchantListContainer()
  {
    return this.mMerchantListContainer;
  }
  
  public String getName()
  {
    return null;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRMerchantListResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */