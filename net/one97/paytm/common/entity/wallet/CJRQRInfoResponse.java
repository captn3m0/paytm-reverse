package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;
import org.json.JSONException;
import org.json.JSONObject;

public class CJRQRInfoResponse
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="response")
  private JSONObject mResponse;
  @b(a="statusCode")
  private String mStatusCode;
  @b(a="statusMessage")
  private String mStatusMessage;
  
  public JSONObject getResponse()
  {
    return this.mResponse;
  }
  
  public String getStatusCode()
  {
    return this.mStatusCode;
  }
  
  public String getStatusMessage()
  {
    return this.mStatusMessage;
  }
  
  public void parseJsonString(String paramString)
    throws JSONException
  {
    paramString = new JSONObject(paramString);
    this.mStatusCode = paramString.optString("statusCode");
    this.mStatusMessage = paramString.optString("statusMessage");
    this.mResponse = paramString.optJSONObject("response");
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRQRInfoResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */