package net.one97.paytm.common.entity.wallet;

import com.google.b.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class AcceptPaymentMerchantStatusModal
  implements IJRDataModel
{
  @b(a="metadata")
  private Object metadata;
  @b(a="response")
  private Response response;
  @b(a="status")
  private String status;
  @b(a="statusCode")
  private String statusCode;
  @b(a="statusMessage")
  private String statusMessage;
  
  public Object getMetadata()
  {
    return this.metadata;
  }
  
  public Response getResponse()
  {
    return this.response;
  }
  
  public String getStatus()
  {
    return this.status;
  }
  
  public String getStatusCode()
  {
    return this.statusCode;
  }
  
  public String getStatusMessage()
  {
    return this.statusMessage;
  }
  
  public void setMetadata(Object paramObject)
  {
    this.metadata = paramObject;
  }
  
  public void setResponse(Response paramResponse)
  {
    this.response = paramResponse;
  }
  
  public void setStatus(String paramString)
  {
    this.status = paramString;
  }
  
  public void setStatusCode(String paramString)
  {
    this.statusCode = paramString;
  }
  
  public void setStatusMessage(String paramString)
  {
    this.statusMessage = paramString;
  }
  
  public class Response
  {
    @b(a="acceptPayment")
    private String acceptPayment;
    @b(a="message1")
    private String message1;
    @b(a="message2")
    private String message2;
    
    public Response() {}
    
    public String getAcceptPayment()
    {
      return this.acceptPayment;
    }
    
    public String getMessage1()
    {
      return this.message1;
    }
    
    public String getMessage2()
    {
      return this.message2;
    }
    
    public void setAcceptPayment(String paramString)
    {
      this.acceptPayment = paramString;
    }
    
    public void setMessage1(String paramString)
    {
      this.message1 = paramString;
    }
    
    public void setMessage2(String paramString)
    {
      this.message2 = paramString;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/AcceptPaymentMerchantStatusModal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */