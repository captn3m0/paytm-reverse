package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRP2BBeneficiaryItemRequest
  implements IJRDataModel
{
  @b(a="bankAccountNo")
  private String mBankAccountNo;
  @b(a="bankName")
  private String mBankName;
  @b(a="ifscCode")
  private String mIfscCode;
  @b(a="senderName")
  private String mSenderName;
  
  public String getBankAccountNo()
  {
    return this.mBankAccountNo;
  }
  
  public String getBankName()
  {
    return this.mBankName;
  }
  
  public String getIfscCode()
  {
    return this.mIfscCode;
  }
  
  public String getSenderName()
  {
    return this.mSenderName;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRP2BBeneficiaryItemRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */