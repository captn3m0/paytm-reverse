package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRP2BStatus
  implements IJRDataModel
{
  @b(a="response")
  private CJRP2BStatusResponse mResponse;
  @b(a="statusCode")
  private String mStatusCode;
  
  public CJRP2BStatusResponse getResponse()
  {
    return this.mResponse;
  }
  
  public String getStatusCode()
  {
    return this.mStatusCode;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRP2BStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */