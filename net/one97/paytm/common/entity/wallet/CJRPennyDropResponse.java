package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPennyDropResponse
  implements IJRDataModel
{
  @b(a="errorCode")
  private String mErrorCode;
  @b(a="errorMsg")
  private String mErrorMsg;
  @b(a="statusCode")
  private String mStatusCode;
  @b(a="successMsg")
  private String mSuccessMsg;
  
  public String getErrorCode()
  {
    return this.mErrorCode;
  }
  
  public String getErrorMsg()
  {
    return this.mErrorMsg;
  }
  
  public String getStatusCode()
  {
    return this.mStatusCode;
  }
  
  public String getSuccessMsg()
  {
    return this.mSuccessMsg;
  }
  
  public void setErrorCode(String paramString)
  {
    this.mErrorCode = paramString;
  }
  
  public void setErrorMsg(String paramString)
  {
    this.mErrorMsg = paramString;
  }
  
  public void setStatusCode(String paramString)
  {
    this.mStatusCode = paramString;
  }
  
  public void setSuccessMsg(String paramString)
  {
    this.mSuccessMsg = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRPennyDropResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */