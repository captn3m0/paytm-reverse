package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRLedgerResponse
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="displayMessage")
  private String mDisplayMessage;
  @b(a="txnGuid")
  private String mStatusCode;
  @b(a="statusMessage")
  private String mStatusMessage;
  @b(a="txnMessage")
  private String mTxnMessage;
  @b(a="txnStatus")
  private String mTxnStatus;
  
  public String getDisplayMessage()
  {
    return this.mDisplayMessage;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getStatusCode()
  {
    return this.mStatusCode;
  }
  
  public String getStatusMessage()
  {
    return this.mStatusMessage;
  }
  
  public String getTxnMessage()
  {
    return this.mTxnMessage;
  }
  
  public String getTxnStatus()
  {
    return this.mTxnStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRLedgerResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */