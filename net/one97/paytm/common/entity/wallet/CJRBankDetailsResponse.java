package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBankDetailsResponse
  implements IJRDataModel
{
  @b(a="bankName")
  private String mBankName;
  @b(a="branch")
  private String mBranch;
  @b(a="city")
  private String mCity;
  @b(a="ifscCode")
  private String mIfscCode;
  @b(a="state")
  private String mState;
  
  public String getBankName()
  {
    return this.mBankName;
  }
  
  public String getBranch()
  {
    return this.mBranch;
  }
  
  public String getCity()
  {
    return this.mCity;
  }
  
  public String getIfscCode()
  {
    return this.mIfscCode;
  }
  
  public String getState()
  {
    return this.mState;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRBankDetailsResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */