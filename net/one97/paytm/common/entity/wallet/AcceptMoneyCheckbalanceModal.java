package net.one97.paytm.common.entity.wallet;

import com.google.b.a.a;
import com.google.b.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class AcceptMoneyCheckbalanceModal
  implements IJRDataModel
{
  @b(a="response")
  private Response response;
  @b(a="status")
  private String status;
  @b(a="statusCode")
  private String statusCode;
  @b(a="statusMessage")
  private String statusMessage;
  
  public Response getResponse()
  {
    return this.response;
  }
  
  public String getStatus()
  {
    return this.status;
  }
  
  public String getStatusCode()
  {
    return this.statusCode;
  }
  
  public String getStatusMessage()
  {
    return this.statusMessage;
  }
  
  public void setResponse(Response paramResponse)
  {
    this.response = paramResponse;
  }
  
  public void setStatus(String paramString)
  {
    this.status = paramString;
  }
  
  public void setStatusCode(String paramString)
  {
    this.statusCode = paramString;
  }
  
  public void setStatusMessage(String paramString)
  {
    this.statusMessage = paramString;
  }
  
  public String toString()
  {
    return "AcceptMoneyCheckbalanceModal{status=" + this.status + ", statusCode='" + this.statusCode + '\'' + ", statusMessage='" + this.statusMessage + '\'' + ", response=" + this.response + '}';
  }
  
  public class Response
  {
    @a
    @b(a="lastUpdated")
    private String lastUpdated;
    @a
    @b(a="merchantWalletBalance")
    private String merchantWalletBalance;
    @a
    @b(a="pendingSettledAmount")
    private String pendingSettledAmount;
    @a
    @b(a="settledAmount")
    private String settledAmount;
    
    public Response() {}
    
    public String getLastUpdated()
    {
      return this.lastUpdated;
    }
    
    public String getMerchantWalletBalance()
    {
      return this.merchantWalletBalance;
    }
    
    public String getPendingSettledAmount()
    {
      return this.pendingSettledAmount;
    }
    
    public String getSettledAmount()
    {
      return this.settledAmount;
    }
    
    public void setLastUpdated(String paramString)
    {
      this.lastUpdated = paramString;
    }
    
    public void setMerchantWalletBalance(String paramString)
    {
      this.merchantWalletBalance = paramString;
    }
    
    public void setPendingSettledAmount(String paramString)
    {
      this.pendingSettledAmount = paramString;
    }
    
    public void setSettledAmount(String paramString)
    {
      this.settledAmount = paramString;
    }
    
    public String toString()
    {
      return "Response{settledAmount='" + this.settledAmount + '\'' + ", pendingSettledAmount='" + this.pendingSettledAmount + '\'' + ", merchantWalletBalance='" + this.merchantWalletBalance + '\'' + ", lastUpdated='" + this.lastUpdated + '\'' + '}';
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/AcceptMoneyCheckbalanceModal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */