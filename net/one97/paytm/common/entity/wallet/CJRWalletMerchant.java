package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRWalletMerchant
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="imageUrl")
  private String mLogo;
  @b(a="label")
  private String mName;
  @b(a="url")
  private String mUrl;
  
  public String getLogo()
  {
    return this.mLogo;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getUrl()
  {
    return this.mUrl;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRWalletMerchant.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */