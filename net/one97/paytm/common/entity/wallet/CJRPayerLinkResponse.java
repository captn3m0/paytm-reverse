package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPayerLinkResponse
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="orderId")
  private String mOrderId;
  @b(a="response")
  private CJRPayerResponse mResponse;
  @b(a="statusCode")
  private String mStatusCode;
  @b(a="statusMessage")
  private String mStatusMessage;
  @b(a="type")
  private String mType;
  
  public String getOrderId()
  {
    return this.mOrderId;
  }
  
  public CJRPayerResponse getResponse()
  {
    return this.mResponse;
  }
  
  public String getStatusCode()
  {
    return this.mStatusCode;
  }
  
  public String getStatusMessage()
  {
    return this.mStatusMessage;
  }
  
  public String getType()
  {
    return this.mType;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRPayerLinkResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */