package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFavAddMoneyResponse
  implements IJRDataModel
{
  @b(a="favouriteAmounts")
  private ArrayList<String> mFavouriteAmounts;
  
  public ArrayList<String> getFavouriteAmounts()
  {
    return this.mFavouriteAmounts;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRFavAddMoneyResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */