package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRObdResponse
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="orderId")
  private String mOrderId;
  @b(a="requestGuid")
  private String mRequestGuid;
  @b(a="status")
  private String mStatus;
  @b(a="statusCode")
  private String mStatusCode;
  @b(a="statusMessage")
  private String mStatusMessage;
  
  public String getOrderId()
  {
    return this.mOrderId;
  }
  
  public String getRequestGuid()
  {
    return this.mRequestGuid;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public String getStatusCode()
  {
    return this.mStatusCode;
  }
  
  public String getStatusMessage()
  {
    return this.mStatusMessage;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRObdResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */