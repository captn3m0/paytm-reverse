package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPayerData
  implements IJRDataModel
{
  @b(a="payerEmail")
  private String mPayerEmail;
  @b(a="payerMobile")
  private String mPayerMobile;
  @b(a="payerName")
  private String mPayerName;
  @b(a="payerSsoId")
  private String mPayerSsoId;
  
  public String getPayerEmail()
  {
    return this.mPayerEmail;
  }
  
  public String getPayerMobile()
  {
    return this.mPayerMobile;
  }
  
  public String getPayerName()
  {
    return this.mPayerName;
  }
  
  public String getPayerSsoId()
  {
    return this.mPayerSsoId;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRPayerData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */