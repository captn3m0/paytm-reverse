package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRP2BBeneficiaryItem
  implements IJRDataModel
{
  @b(a="requestActionUrl")
  private CJRP2BBeneficiaryItemRequestActionUrl mBeneficiaryItemRequestActionUrl;
  
  public String getBankAccountNo()
  {
    if ((getBeneficiaryItemRequestActionUrl() == null) || (getBeneficiaryItemRequestActionUrl().getBeneficiaryItemRequest() == null)) {
      return null;
    }
    return getBeneficiaryItemRequestActionUrl().getBeneficiaryItemRequest().getBankAccountNo();
  }
  
  public String getBankName()
  {
    if ((getBeneficiaryItemRequestActionUrl() == null) || (getBeneficiaryItemRequestActionUrl().getBeneficiaryItemRequest() == null)) {
      return null;
    }
    return getBeneficiaryItemRequestActionUrl().getBeneficiaryItemRequest().getBankName();
  }
  
  public CJRP2BBeneficiaryItemRequestActionUrl getBeneficiaryItemRequestActionUrl()
  {
    return this.mBeneficiaryItemRequestActionUrl;
  }
  
  public String getIfscCode()
  {
    if ((getBeneficiaryItemRequestActionUrl() == null) || (getBeneficiaryItemRequestActionUrl().getBeneficiaryItemRequest() == null)) {
      return null;
    }
    return getBeneficiaryItemRequestActionUrl().getBeneficiaryItemRequest().getIfscCode();
  }
  
  public String getSenderName()
  {
    if ((getBeneficiaryItemRequestActionUrl() == null) || (getBeneficiaryItemRequestActionUrl().getBeneficiaryItemRequest() == null)) {
      return null;
    }
    return getBeneficiaryItemRequestActionUrl().getBeneficiaryItemRequest().getSenderName();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRP2BBeneficiaryItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */