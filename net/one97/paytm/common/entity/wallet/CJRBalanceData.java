package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBalanceData
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="balance")
  private String mBalance;
  @b(a="message")
  private String mMessage;
  @b(a="otpRequired")
  private String mOtpRequired;
  @b(a="ownerGuid")
  private String mOwnerGuid;
  @b(a="status")
  private String mStatus;
  @b(a="txnPinRequired")
  private String mTxnPinRequired;
  @b(a="walletType")
  private String mWalletType;
  
  public String getBalance()
  {
    return this.mBalance;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getOtpRequired()
  {
    return this.mOtpRequired;
  }
  
  public String getOwnerGuid()
  {
    return this.mOwnerGuid;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public String getTxnPinRequired()
  {
    return this.mTxnPinRequired;
  }
  
  public String getWalletType()
  {
    return this.mWalletType;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRBalanceData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */