package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRMerchantListContainer
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="merchants")
  private ArrayList<CJRWalletMerchant> mMerchantList;
  
  public ArrayList<CJRWalletMerchant> getMerchantList()
  {
    return this.mMerchantList;
  }
  
  public String getName()
  {
    return null;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRMerchantListContainer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */