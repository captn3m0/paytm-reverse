package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRP2BBeneficiaryListResponse
  implements IJRDataModel
{
  @b(a="beneficiaryList")
  private ArrayList<CJRP2BBeneficiaryItem> mBeneficiaryList;
  
  public ArrayList<CJRP2BBeneficiaryItem> getBeneficiaryList()
  {
    return this.mBeneficiaryList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRP2BBeneficiaryListResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */