package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRSendMoneyResponse
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="heading")
  private String mHeading;
  @b(a="displayName")
  private String mName;
  @b(a="state")
  private String mState;
  @b(a="payeeSsoId")
  private String payeeSsoId;
  @b(a="walletSysTransactionId")
  private String walletSysTransactionID;
  
  public String getHeading()
  {
    return this.mHeading;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getState()
  {
    return this.mState;
  }
  
  public String getpayeeSsoId()
  {
    return this.payeeSsoId;
  }
  
  public String getwalletSysTransactionID()
  {
    return this.walletSysTransactionID;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRSendMoneyResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */