package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRSendMoneyToMerchantResponse
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="cashBackMessage")
  private String mCashBackMessage;
  @b(a="cashBackStatus")
  private String mCashBackStatus;
  @b(a="heading")
  private String mHeading;
  @b(a="state")
  private String mState;
  @b(a="timestamp")
  private String mTimestamp;
  @b(a="userGuid")
  private String mUserGuid;
  @b(a="walletSystemTxnId")
  private String mWalletSystemTxnId;
  @b(a="pccCode")
  private String pccCode;
  
  public String getCashBackMessage()
  {
    return this.mCashBackMessage;
  }
  
  public String getCashBackStatus()
  {
    return this.mCashBackStatus;
  }
  
  public String getHeading()
  {
    return this.mHeading;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getPccCode()
  {
    return this.pccCode;
  }
  
  public String getState()
  {
    return this.mState;
  }
  
  public String getTimestamp()
  {
    return this.mTimestamp;
  }
  
  public String getUserGuid()
  {
    return this.mUserGuid;
  }
  
  public String getWalletSystemTxnId()
  {
    return this.mWalletSystemTxnId;
  }
  
  public String toString()
  {
    return "CJRSendMoneyToMerchantResponse{mUserGuid='" + this.mUserGuid + '\'' + ", mWalletSystemTxnId='" + this.mWalletSystemTxnId + '\'' + ", mTimestamp='" + this.mTimestamp + '\'' + ", mCashBackStatus='" + this.mCashBackStatus + '\'' + ", mCashBackMessage='" + this.mCashBackMessage + '\'' + ", mState='" + this.mState + '\'' + ", pccCode='" + this.pccCode + '\'' + ", mHeading='" + this.mHeading + '\'' + '}';
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRSendMoneyToMerchantResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */