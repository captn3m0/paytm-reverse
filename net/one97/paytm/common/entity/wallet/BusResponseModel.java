package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class BusResponseModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 2L;
  @b(a="imageUrl")
  private String imageUrl;
  @b(a="invoiceDetails")
  private BusInvoiceDetailsModel invoiceDetails;
  @b(a="merchantName")
  private String merchantName;
  @b(a="merchantOrderId")
  private String merchantOrderId;
  @b(a="txnamount")
  private String txnamount;
  @b(a="txndate")
  private String txndate;
  @b(a="uniqueReferenceLabel")
  private String uniqueReferenceLabel;
  @b(a="uniqueReferenceValue")
  private String uniqueReferenceValue;
  @b(a="walletSysTransactionId")
  private String walletSysTransactionId;
  
  public String getImageUrl()
  {
    return this.imageUrl;
  }
  
  public BusInvoiceDetailsModel getInvoiceDetails()
  {
    return this.invoiceDetails;
  }
  
  public String getMerchantName()
  {
    return this.merchantName;
  }
  
  public String getMerchantOrderId()
  {
    return this.merchantOrderId;
  }
  
  public String getTxnamount()
  {
    return this.txnamount;
  }
  
  public String getTxndate()
  {
    return this.txndate;
  }
  
  public String getUniqueReferenceLabel()
  {
    return this.uniqueReferenceLabel;
  }
  
  public String getUniqueReferenceValue()
  {
    return this.uniqueReferenceValue;
  }
  
  public String getWalletSysTransactionId()
  {
    return this.walletSysTransactionId;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/BusResponseModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */