package net.one97.paytm.common.entity.wallet;

import com.google.b.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class Bank
  implements IJRDataModel
{
  @b(a="BANK_CODE")
  private String BANK_CODE;
  @b(a="BANK_NAME")
  private String BANK_NAME;
  @b(a="IS_ATM")
  private boolean IS_ATM;
  private boolean isChecked;
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    do
    {
      return true;
      if (paramObject == null) {
        return false;
      }
      paramObject = (Bank)paramObject;
    } while (this.BANK_CODE.equals(((Bank)paramObject).getBankCode()));
    return false;
  }
  
  public String getBankCode()
  {
    return this.BANK_CODE;
  }
  
  public String getBankName()
  {
    return this.BANK_NAME;
  }
  
  public int hashCode()
  {
    return this.BANK_CODE.hashCode();
  }
  
  public boolean isATM()
  {
    return this.IS_ATM;
  }
  
  public boolean isChecked()
  {
    return this.isChecked;
  }
  
  public void setBankName(String paramString)
  {
    this.BANK_NAME = paramString;
  }
  
  public void setChecked(boolean paramBoolean)
  {
    this.isChecked = paramBoolean;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/Bank.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */