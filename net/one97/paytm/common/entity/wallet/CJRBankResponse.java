package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBankResponse
  implements IJRDataModel
{
  @b(a="bankBranchList")
  private ArrayList<CJRBranchDetails> mBankBranchList;
  @b(a="list")
  private ArrayList<String> mList;
  
  public ArrayList<CJRBranchDetails> getBankBranchList()
  {
    return this.mBankBranchList;
  }
  
  public ArrayList<String> getList()
  {
    return this.mList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRBankResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */