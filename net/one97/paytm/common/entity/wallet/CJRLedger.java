package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.CJRDataModelItem;
import net.one97.paytm.common.entity.CJRTransaction;

public class CJRLedger
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  private String ledgerTag = "";
  private long mLastUpdateTime;
  @b(a="mMetadata")
  private String mMetadata;
  @b(a="orderId")
  private String mOrderId;
  @b(a="requestGuid")
  private String mRequestFGuid;
  @b(a="statusCode")
  private String mStatusCode;
  @b(a="statusMessage")
  private String mStatusMessage;
  @b(a="response")
  public ArrayList<CJRTransaction> mTransactionList;
  
  public long getLastUpdateTime()
  {
    return this.mLastUpdateTime;
  }
  
  public String getLedgerTag()
  {
    return this.ledgerTag;
  }
  
  public String getMetadata()
  {
    return this.mMetadata;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getOrderId()
  {
    return this.mOrderId;
  }
  
  public String getRequestFGuid()
  {
    return this.mRequestFGuid;
  }
  
  public String getStatusCode()
  {
    return this.mStatusCode;
  }
  
  public String getStatusMessage()
  {
    return this.mStatusMessage;
  }
  
  public ArrayList<CJRTransaction> getTransactionList()
  {
    return this.mTransactionList;
  }
  
  public void setLastUpdateTime(long paramLong)
  {
    this.mLastUpdateTime = paramLong;
  }
  
  public void setLedgerTag(String paramString)
  {
    this.ledgerTag = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRLedger.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */