package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPayerResponse
  implements IJRDataModel
{
  @b(a="requestMoneyNotifData")
  private ArrayList<CJRRequestMoneyNotifDataPayers> mRequestMoneyNotifDataPayers;
  @b(a="statusMessage")
  private String mStatusMessage;
  
  public ArrayList<CJRRequestMoneyNotifDataPayers> getRequestMoneyNotifDataPayers()
  {
    return this.mRequestMoneyNotifDataPayers;
  }
  
  public String getStatusMessage()
  {
    return this.mStatusMessage;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRPayerResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */