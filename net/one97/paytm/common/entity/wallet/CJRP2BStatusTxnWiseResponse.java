package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRP2BStatusTxnWiseResponse
  implements IJRDataModel
{
  @b(a="fixedCommission")
  private String mFixedCommission;
  @b(a="floatCommission")
  private String mFloatCommission;
  @b(a="maxTxnAmount")
  private String mMaxTxnAmount;
  @b(a="minTxnAmount")
  private String mMinTxnAmount;
  @b(a="responseCode")
  private String responseCode;
  
  public String getFixedCommission()
  {
    return this.mFixedCommission;
  }
  
  public String getFloatCommission()
  {
    return this.mFloatCommission;
  }
  
  public String getMaxTxnAmount()
  {
    return this.mMaxTxnAmount;
  }
  
  public String getMinTxnAmount()
  {
    return this.mMinTxnAmount;
  }
  
  public String getResponseCode()
  {
    return this.responseCode;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRP2BStatusTxnWiseResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */