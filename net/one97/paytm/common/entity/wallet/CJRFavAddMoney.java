package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFavAddMoney
  implements IJRDataModel
{
  @b(a="response")
  private CJRFavAddMoneyResponse mResponse;
  @b(a="statusCode")
  private String mStatusCode;
  
  public CJRFavAddMoneyResponse getResponse()
  {
    return this.mResponse;
  }
  
  public String getStatusCode()
  {
    return this.mStatusCode;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRFavAddMoney.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */