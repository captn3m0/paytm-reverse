package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class BusInvoiceDetailsModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 2L;
  @b(a="headerName")
  ArrayList<String> headerName;
  @b(a="itemDetails")
  ArrayList<ArrayList<String>> itemDetails;
  @b(a="otherDetails")
  private ArrayList<BusOtherDetailsModel> otherDetails;
  @b(a="taxDetails")
  private String taxDetails;
  
  public ArrayList<String> getHeaderName()
  {
    return this.headerName;
  }
  
  public ArrayList<ArrayList<String>> getItemDetails()
  {
    return this.itemDetails;
  }
  
  public ArrayList<BusOtherDetailsModel> getOtherDetails()
  {
    return this.otherDetails;
  }
  
  public String getTaxDetails()
  {
    return this.taxDetails;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/BusInvoiceDetailsModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */