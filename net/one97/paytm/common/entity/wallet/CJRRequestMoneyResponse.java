package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRRequestMoneyResponse
  implements IJRDataModel
{
  @b(a="heading")
  private String mHeading;
  @b(a="text")
  private String mText;
  
  public String getHeading()
  {
    return this.mHeading;
  }
  
  public String getText()
  {
    return this.mText;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRRequestMoneyResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */