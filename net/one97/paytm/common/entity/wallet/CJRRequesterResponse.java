package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRRequesterResponse
  implements IJRDataModel
{
  @b(a="requestMoneyNotifData")
  private ArrayList<CJRRequestMoneyNotifData> mRequestMoneyNotifData;
  @b(a="statusMessage")
  private String mStatusMessage;
  
  public ArrayList<CJRRequestMoneyNotifData> getRequestMoneyNotifData()
  {
    return this.mRequestMoneyNotifData;
  }
  
  public String getStatusMessage()
  {
    return this.mStatusMessage;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRRequesterResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */