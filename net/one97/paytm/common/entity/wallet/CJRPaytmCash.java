package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPaytmCash
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="max_allowed")
  private String mMaxAllowed;
  @b(a="PaytmcashBalance")
  private String mPaytmCashBalance;
  @b(a="trans_count")
  private String mTransCount;
  
  public String getMaxAllowed()
  {
    return this.mMaxAllowed;
  }
  
  public String getPaytmCashBalance()
  {
    return this.mPaytmCashBalance;
  }
  
  public String getTransCount()
  {
    return this.mTransCount;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRPaytmCash.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */