package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;

public class CJRRefundParams
{
  @b(a="amount")
  private String mAmount;
  @b(a="txnGuid")
  private String mTxnGuid;
  
  public String getAmount()
  {
    return this.mAmount;
  }
  
  public String getTxnGuid()
  {
    return this.mTxnGuid;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRRefundParams.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */