package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRLimitStatusResponse
  implements IJRDataModel
{
  @b(a="allowedLimit")
  private double mAllowedLimit;
  @b(a="allowedP2BForSdMerchant")
  private String mAllowedP2BForSdMerchant;
  @b(a="creditConsumed")
  private double mCreditConsumed;
  @b(a="thresholdAchieved")
  private String mThresholdAchieved;
  @b(a="walletRbiType")
  private String mWalletRbiType;
  
  public double getAllowedLimit()
  {
    return this.mAllowedLimit;
  }
  
  public String getAllowedP2BForSdMerchant()
  {
    return this.mAllowedP2BForSdMerchant;
  }
  
  public double getCreditConsumed()
  {
    return this.mCreditConsumed;
  }
  
  public String getThresholdAchieved()
  {
    return this.mThresholdAchieved;
  }
  
  public String getWalletRbiType()
  {
    return this.mWalletRbiType;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRLimitStatusResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */