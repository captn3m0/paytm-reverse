package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class BusOtherDetailsModel
  implements IJRDataModel
{
  public static final String ROW_NAME_BOKKING_TIME = "BookingTime";
  public static final String ROW_NAME_SERVER_TIME = "ServerTime";
  public static final String ROW_NAME_VALIDITY = "Validity";
  private static final long serialVersionUID = 2L;
  @b(a="rowname")
  private String rowname;
  @b(a="rowvalue")
  private String rowvalue;
  
  public String getRowname()
  {
    return this.rowname;
  }
  
  public String getRowvalue()
  {
    return this.rowvalue;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/BusOtherDetailsModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */