package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRWalletUserInfo
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="status")
  private String mStatus;
  @b(a="data")
  private CJRUserData mUserData;
  
  public String getmStatus()
  {
    return this.mStatus;
  }
  
  public CJRUserData getmUserData()
  {
    return this.mUserData;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRWalletUserInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */