package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRRequestMoneyNotifData
  implements IJRDataModel
{
  @b(a="comment")
  private String mComment;
  @b(a="createDate")
  private String mCreateDate;
  @b(a="payerData")
  private ArrayList<CJRPayerData> mPayerData;
  @b(a="requestCode")
  private String mRequestCode;
  @b(a="requestedAmount")
  private String mRequestedAmount;
  @b(a="status")
  private int mStatus;
  @b(a="type")
  private String mType;
  
  public String getComment()
  {
    return this.mComment;
  }
  
  public String getCreateDate()
  {
    return this.mCreateDate;
  }
  
  public ArrayList<CJRPayerData> getPayerData()
  {
    return this.mPayerData;
  }
  
  public String getRequestCode()
  {
    return this.mRequestCode;
  }
  
  public String getRequestedAmount()
  {
    return this.mRequestedAmount;
  }
  
  public int getStatus()
  {
    return this.mStatus;
  }
  
  public String getType()
  {
    return this.mType;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRRequestMoneyNotifData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */