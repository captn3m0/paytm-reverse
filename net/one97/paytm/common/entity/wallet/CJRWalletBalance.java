package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRWalletBalance
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="data")
  private CJRBalanceData mBalanceData;
  @b(a="error")
  private String mError;
  @b(a="result")
  private String mResult;
  @b(a="status")
  private String mStatus;
  
  public CJRBalanceData getBalanceData()
  {
    return this.mBalanceData;
  }
  
  public String getError()
  {
    return this.mError;
  }
  
  public String getResult()
  {
    return this.mResult;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRWalletBalance.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */