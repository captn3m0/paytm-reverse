package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRRequestMoneyNotifDataPayers
  implements IJRDataModel
{
  @b(a="comments")
  private String mComments;
  @b(a="createDate")
  private String mCreateDate;
  @b(a="email")
  private String mEmail;
  @b(a="mobile")
  private String mMobile;
  @b(a="payeeName")
  private String mPayeeName;
  @b(a="requestCode")
  private String mRequestCode;
  @b(a="requestedAmount")
  private String mRequestedAmount;
  @b(a="status")
  private boolean mStatus;
  
  public String getComments()
  {
    return this.mComments;
  }
  
  public String getCreateDate()
  {
    return this.mCreateDate;
  }
  
  public String getEmail()
  {
    return this.mEmail;
  }
  
  public boolean getIsStatus()
  {
    return this.mStatus;
  }
  
  public String getMobile()
  {
    return this.mMobile;
  }
  
  public String getPayeeName()
  {
    return this.mPayeeName;
  }
  
  public String getRequestCode()
  {
    return this.mRequestCode;
  }
  
  public String getRequestedAmount()
  {
    return this.mRequestedAmount;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRRequestMoneyNotifDataPayers.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */