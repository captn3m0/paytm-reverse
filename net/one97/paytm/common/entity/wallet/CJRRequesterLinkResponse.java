package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRRequesterLinkResponse
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="orderId")
  private String mOrderId;
  @b(a="response")
  private CJRRequesterResponse mResponse;
  @b(a="statusCode")
  private String mStatusCode;
  @b(a="statusMessage")
  private String mStatusMessage;
  
  public String getOrderId()
  {
    return this.mOrderId;
  }
  
  public CJRRequesterResponse getResponse()
  {
    return this.mResponse;
  }
  
  public String getStatusCode()
  {
    return this.mStatusCode;
  }
  
  public String getStatusMessage()
  {
    return this.mStatusMessage;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRRequesterLinkResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */