package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRPaytmWalletService
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="landingPage")
  private String mLandingPage;
  @b(a="name")
  private String mName;
  @b(a="url")
  private String mUrl;
  
  public String getLandingPage()
  {
    return this.mLandingPage;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getUrl()
  {
    return this.mUrl;
  }
  
  public void setLandingPage(String paramString)
  {
    this.mLandingPage = paramString;
  }
  
  public void setName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setUrl(String paramString)
  {
    this.mUrl = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRPaytmWalletService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */