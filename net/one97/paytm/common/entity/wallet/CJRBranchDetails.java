package net.one97.paytm.common.entity.wallet;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBranchDetails
  implements IJRDataModel
{
  @b(a="branchName")
  private String mBranchName;
  @b(a="displayName")
  private String mDisplayName;
  @b(a="ifscCode")
  private String mIfscCode;
  
  public String getDisplayName()
  {
    return this.mDisplayName;
  }
  
  public String getIfscCode()
  {
    return this.mIfscCode;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/wallet/CJRBranchDetails.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */