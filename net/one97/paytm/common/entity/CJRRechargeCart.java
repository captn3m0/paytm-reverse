package net.one97.paytm.common.entity;

import java.util.ArrayList;
import net.one97.paytm.common.entity.shopping.CJRCart;
import net.one97.paytm.common.entity.shopping.CJRCartProduct;
import net.one97.paytm.common.entity.shopping.CJRCartStatus;
import org.json.JSONArray;
import org.json.JSONObject;

public class CJRRechargeCart
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @com.google.c.a.b(a="cart")
  private CJRCart mCart;
  @com.google.c.a.b(a="status")
  private CJRCartStatus mCartStatus;
  
  public CJRCart getCart()
  {
    return this.mCart;
  }
  
  public CJRCartStatus getCartStatus()
  {
    return this.mCartStatus;
  }
  
  public void setCartJsonResponse(String paramString)
  {
    net.one97.paytm.common.utility.b.a("TAG", "JSON CART--" + paramString);
    try
    {
      paramString = new JSONObject(paramString).getJSONObject("cart").getJSONArray("cart_items");
      int i = 0;
      if (i < paramString.length())
      {
        Object localObject;
        int j;
        if (paramString.getJSONObject(i).has("service_options"))
        {
          localObject = paramString.getJSONObject(i).getJSONObject("service_options");
          if (((JSONObject)localObject).has("actions"))
          {
            localObject = ((JSONObject)localObject).getJSONArray("actions");
            j = 0;
          }
        }
        for (;;)
        {
          if (j < ((JSONArray)localObject).length())
          {
            if (((JSONArray)localObject).getJSONObject(j).has("displayValues")) {
              ((CJRCartProduct)this.mCart.getCartItems().get(i)).setDisplayItemIndex(j);
            }
          }
          else
          {
            i += 1;
            break;
          }
          j += 1;
        }
      }
      return;
    }
    catch (Exception paramString) {}
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRRechargeCart.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */