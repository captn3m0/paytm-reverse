package net.one97.paytm.common.entity.busticket;

import com.google.c.a.b;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBlockOneModel
  implements IJRDataModel
{
  @b(a="block_expiry_time")
  private int blockExpiryTime;
  @b(a="block_key")
  private String blockKey;
  @b(a="other_tax_details")
  private List<CJRTaxItem> otherTaxDetails;
  @b(a="requestid")
  private String requestid;
  @b(a="showIntermediate")
  private boolean showIntermediate;
  @b(a="tax_details")
  private List<CJRTaxItem> taxDetails;
  @b(a="ticket_id")
  private String ticketId;
  @b(a="total_base_fare")
  private double totalBaseFare;
  @b(a="total_tax")
  private double totalTax;
  @b(a="travelDate")
  private String travelDate;
  
  public int getBlockExpiryTime()
  {
    return this.blockExpiryTime;
  }
  
  public String getBlockKey()
  {
    return this.blockKey;
  }
  
  public List<CJRTaxItem> getOtherTaxDetails()
  {
    return this.otherTaxDetails;
  }
  
  public String getRequestid()
  {
    return this.requestid;
  }
  
  public List<CJRTaxItem> getTaxDetails()
  {
    return this.taxDetails;
  }
  
  public String getTicketId()
  {
    return this.ticketId;
  }
  
  public double getTotalBaseFare()
  {
    return this.totalBaseFare;
  }
  
  public double getTotalTax()
  {
    return this.totalTax;
  }
  
  public String getTravelDate()
  {
    return this.travelDate;
  }
  
  public boolean isShowIntermediate()
  {
    return this.showIntermediate;
  }
  
  public void setBlockExpiryTime(int paramInt)
  {
    this.blockExpiryTime = paramInt;
  }
  
  public void setBlockKey(String paramString)
  {
    this.blockKey = paramString;
  }
  
  public void setOtherTaxDetails(List<CJRTaxItem> paramList)
  {
    this.otherTaxDetails = paramList;
  }
  
  public void setRequestid(String paramString)
  {
    this.requestid = paramString;
  }
  
  public void setShowIntermediate(boolean paramBoolean)
  {
    this.showIntermediate = paramBoolean;
  }
  
  public void setTaxDetails(List<CJRTaxItem> paramList)
  {
    this.taxDetails = paramList;
  }
  
  public void setTicketId(String paramString)
  {
    this.ticketId = paramString;
  }
  
  public void setTotalBaseFare(double paramDouble)
  {
    this.totalBaseFare = paramDouble;
  }
  
  public void setTotalTax(double paramDouble)
  {
    this.totalTax = paramDouble;
  }
  
  public void setTravelDate(String paramString)
  {
    this.travelDate = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/CJRBlockOneModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */