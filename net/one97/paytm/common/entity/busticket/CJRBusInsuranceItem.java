package net.one97.paytm.common.entity.busticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBusInsuranceItem
  implements IJRDataModel
{
  @b(a="details")
  private String details;
  @b(a="id")
  private int id;
  @b(a="max_claim_amount")
  private String maxClaimAmount;
  @b(a="name")
  private String name;
  @b(a="price")
  private double price;
  @b(a="product_id")
  private int productId;
  @b(a="tc")
  private String tc;
  @b(a="text_to_display")
  private String text2Display;
  @b(a="type")
  private String type;
  @b(a="vertical_id")
  private int verticalId;
  
  public String getDetails()
  {
    return this.details;
  }
  
  public int getId()
  {
    return this.id;
  }
  
  public String getMaxClaimAmount()
  {
    return this.maxClaimAmount;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public double getPrice()
  {
    return this.price;
  }
  
  public int getProductId()
  {
    return this.productId;
  }
  
  public String getTc()
  {
    return this.tc;
  }
  
  public String getText2Display()
  {
    return this.text2Display;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public int getVerticalId()
  {
    return this.verticalId;
  }
  
  public void setDetails(String paramString)
  {
    this.details = paramString;
  }
  
  public void setId(int paramInt)
  {
    this.id = paramInt;
  }
  
  public void setMaxClaimAmount(String paramString)
  {
    this.maxClaimAmount = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setPrice(double paramDouble)
  {
    this.price = paramDouble;
  }
  
  public void setProductId(int paramInt)
  {
    this.productId = paramInt;
  }
  
  public void setTc(String paramString)
  {
    this.tc = paramString;
  }
  
  public void setText2Display(String paramString)
  {
    this.text2Display = paramString;
  }
  
  public void setType(String paramString)
  {
    this.type = paramString;
  }
  
  public void setVerticalId(int paramInt)
  {
    this.verticalId = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/CJRBusInsuranceItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */