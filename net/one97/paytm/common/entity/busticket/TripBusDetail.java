package net.one97.paytm.common.entity.busticket;

import com.google.c.a.a;
import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class TripBusDetail
  implements IJRDataModel
{
  @a
  @b(a="body")
  private ArrayList<TripBusDetailsItem> body = new ArrayList();
  @a
  @b(a="code")
  private Integer code;
  @a
  @b(a="error")
  private Object error;
  @a
  @b(a="meta")
  private TripDetailsMeta meta;
  @a
  @b(a="status")
  private TripBusDetailsStatus status;
  
  public ArrayList<TripBusDetailsItem> getBody()
  {
    return this.body;
  }
  
  public Integer getCode()
  {
    return this.code;
  }
  
  public Object getError()
  {
    return this.error;
  }
  
  public TripDetailsMeta getMeta()
  {
    return this.meta;
  }
  
  public TripBusDetailsStatus getStatus()
  {
    return this.status;
  }
  
  public void setBody(ArrayList<TripBusDetailsItem> paramArrayList)
  {
    this.body = paramArrayList;
  }
  
  public void setCode(Integer paramInteger)
  {
    this.code = paramInteger;
  }
  
  public void setError(Object paramObject)
  {
    this.error = paramObject;
  }
  
  public void setMeta(TripDetailsMeta paramTripDetailsMeta)
  {
    this.meta = paramTripDetailsMeta;
  }
  
  public void setStatus(TripBusDetailsStatus paramTripBusDetailsStatus)
  {
    this.status = paramTripBusDetailsStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/TripBusDetail.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */