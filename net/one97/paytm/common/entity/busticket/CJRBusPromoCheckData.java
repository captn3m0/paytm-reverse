package net.one97.paytm.common.entity.busticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBusPromoCheckData
  implements IJRDataModel
{
  @b(a="paytm_cashback")
  private String mPaytmCashback;
  @b(a="paytm_discount")
  private String mPaytmDiscount;
  @b(a="promocode")
  private String mPromocode;
  @b(a="promostatus")
  private String mPromostatus;
  @b(a="promotext")
  private String mPromotext;
  
  public String getPaytmCashback()
  {
    return this.mPaytmCashback;
  }
  
  public String getPaytmDiscount()
  {
    return this.mPaytmDiscount;
  }
  
  public String getPromocode()
  {
    return this.mPromocode;
  }
  
  public String getPromostatus()
  {
    return this.mPromostatus;
  }
  
  public String getPromotext()
  {
    return this.mPromotext;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/CJRBusPromoCheckData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */