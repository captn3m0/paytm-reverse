package net.one97.paytm.common.entity.busticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBusSearchLocation
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="boardingDate")
  private String boardingDate;
  @b(a="display_name")
  private String display_name;
  @b(a="locationName")
  private String locationName;
  @b(a="providerLocationId")
  private String providerLocationId;
  @b(a="reportingTime")
  private String reportingTime;
  @b(a="time")
  private String time;
  
  public String getBoardingDate()
  {
    return this.boardingDate;
  }
  
  public String getDisplay_name()
  {
    return this.display_name;
  }
  
  public String getLocationName()
  {
    return this.locationName;
  }
  
  public String getProviderLocationId()
  {
    return this.providerLocationId;
  }
  
  public String getReportingTime()
  {
    return this.reportingTime;
  }
  
  public String getTime()
  {
    return this.time;
  }
  
  public void setBoardingDate(String paramString)
  {
    this.boardingDate = paramString;
  }
  
  public void setDisplay_name(String paramString)
  {
    this.display_name = paramString;
  }
  
  public void setLocationName(String paramString)
  {
    this.locationName = paramString;
  }
  
  public void setProviderLocationId(String paramString)
  {
    this.providerLocationId = paramString;
  }
  
  public void setReportingTime(String paramString)
  {
    this.reportingTime = paramString;
  }
  
  public void setTime(String paramString)
  {
    this.time = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/CJRBusSearchLocation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */