package net.one97.paytm.common.entity.busticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBusUserInfo
  implements IJRDataModel
{
  @b(a="age")
  private int age;
  @b(a="email")
  private String email;
  @b(a="fullname")
  private String fullname;
  @b(a="mobile")
  private String mobile;
  @b(a="person_id")
  private String person_id;
  @b(a="title")
  private String title;
  
  public int getAge()
  {
    return this.age;
  }
  
  public String getEmail()
  {
    return this.email;
  }
  
  public String getFullname()
  {
    return this.fullname;
  }
  
  public String getMobile()
  {
    return this.mobile;
  }
  
  public String getPerson_id()
  {
    return this.person_id;
  }
  
  public String getTitle()
  {
    return this.title;
  }
  
  public void setAge(int paramInt)
  {
    this.age = paramInt;
  }
  
  public void setEmail(String paramString)
  {
    this.email = paramString;
  }
  
  public void setFullname(String paramString)
  {
    this.fullname = paramString;
  }
  
  public void setMobile(String paramString)
  {
    this.mobile = paramString;
  }
  
  public void setPerson_id(String paramString)
  {
    this.person_id = paramString;
  }
  
  public void setTitle(String paramString)
  {
    this.title = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/CJRBusUserInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */