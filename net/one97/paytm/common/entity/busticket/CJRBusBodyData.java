package net.one97.paytm.common.entity.busticket;

import com.google.c.a.b;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBusBodyData
  implements IJRDataModel
{
  @b(a="contacts")
  private List<CJRBusUserInfo> contacts;
  
  public List<CJRBusUserInfo> getContacts()
  {
    return this.contacts;
  }
  
  public void setContacts(List<CJRBusUserInfo> paramList)
  {
    this.contacts = paramList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/CJRBusBodyData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */