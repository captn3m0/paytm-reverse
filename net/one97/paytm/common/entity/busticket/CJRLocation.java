package net.one97.paytm.common.entity.busticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRLocation
  implements Comparable<CJRLocation>, IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="display_name")
  private String display_name;
  @b(a="boardingDate")
  private String mBoardingDate;
  @b(a="locationName")
  private String mLocationName;
  @b(a="providerLocationId")
  private String mProviderLocationId;
  @b(a="reportingTime")
  private String mReportingTime;
  @b(a="time")
  private String mTime;
  
  public int compareTo(CJRLocation paramCJRLocation)
  {
    try
    {
      int i = getLocationName().compareToIgnoreCase(paramCJRLocation.getLocationName());
      return i;
    }
    catch (Exception paramCJRLocation) {}
    return -1;
  }
  
  public boolean equals(Object paramObject)
  {
    try
    {
      boolean bool = this.mLocationName.trim().equalsIgnoreCase(((CJRLocation)paramObject).mLocationName.trim());
      return bool;
    }
    catch (Exception paramObject) {}
    return false;
  }
  
  public String getBoardingDate()
  {
    return this.mBoardingDate;
  }
  
  public String getDisplay_name()
  {
    return this.display_name;
  }
  
  public String getLocationName()
  {
    return this.mLocationName;
  }
  
  public String getProviderLocationId()
  {
    return this.mProviderLocationId;
  }
  
  public String getReportingTime()
  {
    return this.mReportingTime;
  }
  
  public String getTime()
  {
    return this.mTime;
  }
  
  public void setBoardingDate(String paramString)
  {
    this.mBoardingDate = paramString;
  }
  
  public void setDisplay_name(String paramString)
  {
    this.display_name = paramString;
  }
  
  public void setLocationName(String paramString)
  {
    this.mLocationName = paramString;
  }
  
  public void setProviderLocationId(String paramString)
  {
    this.mProviderLocationId = paramString;
  }
  
  public void setReportingTime(String paramString)
  {
    this.mReportingTime = paramString;
  }
  
  public void setTime(String paramString)
  {
    this.mTime = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/CJRLocation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */