package net.one97.paytm.common.entity.busticket;

import com.google.c.a.b;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBusInsurancePlans
  implements IJRDataModel
{
  @b(a="code")
  private int code;
  @b(a="data")
  private List<CJRBusInsuranceItem> insuranceItems;
  @b(a="status")
  private String status;
  
  public int getCode()
  {
    return this.code;
  }
  
  public CJRBusInsuranceItem getInsuranceAtIndex(int paramInt)
  {
    if (paramInt < getPlansCount()) {
      return (CJRBusInsuranceItem)this.insuranceItems.get(paramInt);
    }
    return null;
  }
  
  public List<CJRBusInsuranceItem> getInsuranceItems()
  {
    return this.insuranceItems;
  }
  
  public int getPlansCount()
  {
    if (this.insuranceItems != null) {
      return this.insuranceItems.size();
    }
    return 0;
  }
  
  public String getStatus()
  {
    return this.status;
  }
  
  public void setCode(int paramInt)
  {
    this.code = paramInt;
  }
  
  public void setInsuranceItems(List<CJRBusInsuranceItem> paramList)
  {
    this.insuranceItems = paramList;
  }
  
  public void setStatus(String paramString)
  {
    this.status = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/CJRBusInsurancePlans.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */