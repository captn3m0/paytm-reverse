package net.one97.paytm.common.entity.busticket;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;
import net.one97.paytm.common.entity.shopping.CJRHomePageLayout;

public class CJRBusCarouselModle
  implements IJRDataModel
{
  @b(a="homepage_layout")
  private ArrayList<CJRHomePageLayout> mBusCarouselOffer;
  @b(a="code")
  private int mCode;
  @b(a="status")
  private String mMessage;
  
  public ArrayList<CJRHomePageLayout> getBusCarouselOffer()
  {
    return this.mBusCarouselOffer;
  }
  
  public int getCode()
  {
    return this.mCode;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public void setBusCarouselOffer(ArrayList<CJRHomePageLayout> paramArrayList)
  {
    this.mBusCarouselOffer = paramArrayList;
  }
  
  public void setCode(int paramInt)
  {
    this.mCode = paramInt;
  }
  
  public void setMessage(String paramString)
  {
    this.mMessage = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/CJRBusCarouselModle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */