package net.one97.paytm.common.entity.busticket;

import com.google.c.a.a;
import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class TripBusDetailsItem
  implements IJRDataModel
{
  @a
  @b(a="column")
  private long column;
  @a
  @b(a="fare")
  private String fare;
  @a
  @b(a="isAvailable")
  private Boolean isAvailable;
  @a
  @b(a="isLadiesSeat")
  private Boolean isLadiesSeat;
  @a
  @b(a="isLowerBerth")
  private Boolean isLowerBerth;
  @a
  @b(a="isMenSeat")
  private Boolean isMenSeat;
  @a
  @b(a="length")
  private long length;
  @a
  @b(a="markupFareAbsolute")
  private String markupFareAbsolute;
  @a
  @b(a="markupFarePercentage")
  private String markupFarePercentage;
  @a
  @b(a="operatorServiceChargeAbsolute")
  private String operatorServiceChargeAbsolute;
  @a
  @b(a="operatorServiceChargePercent")
  private String operatorServiceChargePercent;
  @a
  @b(a="product_id")
  private String productId;
  @a
  @b(a="row")
  private long row;
  @b(a="seatName")
  private String seatName;
  @a
  @b(a="width")
  private long width;
  
  public Boolean getAvailable()
  {
    return this.isAvailable;
  }
  
  public long getColumn()
  {
    return this.column;
  }
  
  public String getFare()
  {
    return this.fare;
  }
  
  public Boolean getLadiesSeat()
  {
    return this.isLadiesSeat;
  }
  
  public long getLength()
  {
    return this.length;
  }
  
  public Boolean getLowerBerth()
  {
    return this.isLowerBerth;
  }
  
  public String getMarkupFareAbsolute()
  {
    return this.markupFareAbsolute;
  }
  
  public String getMarkupFarePercentage()
  {
    return this.markupFarePercentage;
  }
  
  public Boolean getMenSeat()
  {
    return this.isMenSeat;
  }
  
  public String getOperatorServiceChargeAbsolute()
  {
    return this.operatorServiceChargeAbsolute;
  }
  
  public String getOperatorServiceChargePercent()
  {
    return this.operatorServiceChargePercent;
  }
  
  public String getProductId()
  {
    return this.productId;
  }
  
  public long getRow()
  {
    return this.row;
  }
  
  public String getSeatName()
  {
    return this.seatName;
  }
  
  public long getWidth()
  {
    return this.width;
  }
  
  public void setAvailable(Boolean paramBoolean)
  {
    this.isAvailable = paramBoolean;
  }
  
  public void setColumn(long paramLong)
  {
    this.column = paramLong;
  }
  
  public void setFare(String paramString)
  {
    this.fare = paramString;
  }
  
  public void setLadiesSeat(Boolean paramBoolean)
  {
    this.isLadiesSeat = paramBoolean;
  }
  
  public void setLength(long paramLong)
  {
    this.length = paramLong;
  }
  
  public void setLowerBerth(Boolean paramBoolean)
  {
    this.isLowerBerth = paramBoolean;
  }
  
  public void setMarkupFareAbsolute(String paramString)
  {
    this.markupFareAbsolute = paramString;
  }
  
  public void setMarkupFarePercentage(String paramString)
  {
    this.markupFarePercentage = paramString;
  }
  
  public void setMenSeat(Boolean paramBoolean)
  {
    this.isMenSeat = paramBoolean;
  }
  
  public void setOperatorServiceChargeAbsolute(String paramString)
  {
    this.operatorServiceChargeAbsolute = paramString;
  }
  
  public void setOperatorServiceChargePercent(String paramString)
  {
    this.operatorServiceChargePercent = paramString;
  }
  
  public void setProductId(String paramString)
  {
    this.productId = paramString;
  }
  
  public void setRow(long paramLong)
  {
    this.row = paramLong;
  }
  
  public void setSeatName(String paramString)
  {
    this.seatName = paramString;
  }
  
  public void setWidth(long paramLong)
  {
    this.width = paramLong;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/TripBusDetailsItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */