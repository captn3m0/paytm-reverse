package net.one97.paytm.common.entity.busticket;

import com.google.c.a.a;
import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class TripDetailProvider
  implements IJRDataModel
{
  @a
  @b(a="address")
  private String address;
  @a
  @b(a="contact")
  private String contact;
  @a
  @b(a="email")
  private String email;
  @a
  @b(a="name")
  private String name;
  
  public String getAddress()
  {
    return this.address;
  }
  
  public String getContact()
  {
    return this.contact;
  }
  
  public String getEmail()
  {
    return this.email;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public void setAddress(String paramString)
  {
    this.address = paramString;
  }
  
  public void setContact(String paramString)
  {
    this.contact = paramString;
  }
  
  public void setEmail(String paramString)
  {
    this.email = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/TripDetailProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */