package net.one97.paytm.common.entity.busticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTaxItem
  implements IJRDataModel
{
  @b(a="isVisible")
  private boolean isVisible;
  @b(a="name")
  private String name;
  @b(a="type")
  private String type;
  @b(a="value")
  private double value;
  
  public String getName()
  {
    return this.name;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public double getValue()
  {
    return this.value;
  }
  
  public boolean isVisible()
  {
    return this.isVisible;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setType(String paramString)
  {
    this.type = paramString;
  }
  
  public void setValue(double paramDouble)
  {
    this.value = paramDouble;
  }
  
  public void setVisible(boolean paramBoolean)
  {
    this.isVisible = paramBoolean;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/CJRTaxItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */