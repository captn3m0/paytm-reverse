package net.one97.paytm.common.entity.busticket;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBusSearchItem
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="arrivalDate")
  private String arrivalDate;
  @b(a="arrivalTime")
  private String arrivalTime;
  @b(a="avalableSeats")
  private int avalableSeats;
  @b(a="boardingLocations")
  private ArrayList<CJRLocation> boardingLocations;
  @b(a="boardingTime")
  private String boardingTime;
  @b(a="busType")
  private String busType;
  @b(a="computedTravelsName")
  private String computedTravelsName;
  @b(a="departureDate")
  private String departureDate;
  @b(a="departureTime")
  private String departureTime;
  @b(a="destination")
  private String destination;
  @b(a="droppingLocations")
  private ArrayList<CJRLocation> droppingLocations;
  @b(a="duration")
  private String duration;
  @b(a="fare")
  private double[] fare;
  @b(a="idProofRequired")
  private boolean idProofRequired;
  private String info;
  @b(a="isAc")
  private boolean isAc;
  @b(a="isLuxury")
  private boolean isLuxury;
  @b(a="isMobileTicketAllowed")
  private boolean isMobileTicketAllowed;
  @b(a="isNonAc")
  private boolean isNonAc;
  @b(a="isNonSleeper")
  private boolean isNonSleeper;
  @b(a="isSleeper")
  private boolean isSleeper;
  @b(a="isSold")
  private CJRIsSoldBusSeat isSoldBusSeat;
  @b(a="is_service_charge_extra")
  private boolean is_service_charge_extra;
  @b(a="operatorId")
  private String operatorId;
  @b(a="partialCancellationAllowed")
  private boolean partialCancellationAllowed;
  @b(a="providerId")
  private String providerId;
  @b(a="ratings")
  private String ratings;
  @b(a="reportingTime")
  private String reportingTime;
  @b(a="review_data")
  private CJRBusSearchReviewData review_data;
  @b(a="service_tax_message")
  private String service_tax_message;
  @b(a="source")
  private String source;
  @b(a="travelDurationDays")
  private String travelDurationDays;
  @b(a="travelsName")
  private String travelsName;
  @b(a="tripId")
  private String tripId;
  
  public String getArrivalDate()
  {
    return this.arrivalDate;
  }
  
  public String getArrivalTime()
  {
    return this.arrivalTime;
  }
  
  public int getAvalableSeats()
  {
    return this.avalableSeats;
  }
  
  public ArrayList<CJRLocation> getBoardingLocations()
  {
    return this.boardingLocations;
  }
  
  public String getBoardingTime()
  {
    return this.boardingTime;
  }
  
  public String getBusType()
  {
    return this.busType;
  }
  
  public String getComputedTravelsName()
  {
    return this.computedTravelsName;
  }
  
  public String getDepartureDate()
  {
    return this.departureDate;
  }
  
  public String getDepartureTime()
  {
    return this.departureTime;
  }
  
  public String getDestination()
  {
    return this.destination;
  }
  
  public ArrayList<CJRLocation> getDroppingLocations()
  {
    return this.droppingLocations;
  }
  
  public String getDuration()
  {
    return this.duration;
  }
  
  public double[] getFare()
  {
    return this.fare;
  }
  
  public String getInfo()
  {
    return this.info;
  }
  
  public CJRIsSoldBusSeat getIsSoldBusSeat()
  {
    return this.isSoldBusSeat;
  }
  
  public boolean getIs_service_charge_extra()
  {
    return this.is_service_charge_extra;
  }
  
  public String getOperatorId()
  {
    return this.operatorId;
  }
  
  public String getProviderId()
  {
    return this.providerId;
  }
  
  public String getRatings()
  {
    return this.ratings;
  }
  
  public String getReportingTime()
  {
    return this.reportingTime;
  }
  
  public CJRBusSearchReviewData getReview_data()
  {
    return this.review_data;
  }
  
  public String getService_tax_message()
  {
    return this.service_tax_message;
  }
  
  public String getSource()
  {
    return this.source;
  }
  
  public String getTravelDurationDays()
  {
    return this.travelDurationDays;
  }
  
  public String getTravelsName()
  {
    return this.travelsName;
  }
  
  public String getTripId()
  {
    return this.tripId;
  }
  
  public boolean isAc()
  {
    return this.isAc;
  }
  
  public boolean isIdProofRequired()
  {
    return this.idProofRequired;
  }
  
  public boolean isLuxury()
  {
    return this.isLuxury;
  }
  
  public boolean isMobileTicketAllowed()
  {
    return this.isMobileTicketAllowed;
  }
  
  public boolean isNonAc()
  {
    return this.isNonAc;
  }
  
  public boolean isNonSleeper()
  {
    return this.isNonSleeper;
  }
  
  public boolean isPartialCancellationAllowed()
  {
    return this.partialCancellationAllowed;
  }
  
  public boolean isSleeper()
  {
    return this.isSleeper;
  }
  
  public boolean is_service_charge_extra()
  {
    return this.is_service_charge_extra;
  }
  
  public void setAc(boolean paramBoolean)
  {
    this.isAc = paramBoolean;
  }
  
  public void setArrivalDate(String paramString)
  {
    this.arrivalDate = paramString;
  }
  
  public void setArrivalTime(String paramString)
  {
    this.arrivalTime = paramString;
  }
  
  public void setAvalableSeats(int paramInt)
  {
    this.avalableSeats = paramInt;
  }
  
  public void setBoardingLocations(ArrayList<CJRLocation> paramArrayList)
  {
    this.boardingLocations = paramArrayList;
  }
  
  public void setBoardingTime(String paramString)
  {
    this.boardingTime = paramString;
  }
  
  public void setBusType(String paramString)
  {
    this.busType = paramString;
  }
  
  public void setComputedTravelsName(String paramString)
  {
    this.computedTravelsName = paramString;
  }
  
  public void setDepartureDate(String paramString)
  {
    this.departureDate = paramString;
  }
  
  public void setDepartureTime(String paramString)
  {
    this.departureTime = paramString;
  }
  
  public void setDestination(String paramString)
  {
    this.destination = paramString;
  }
  
  public void setDroppingLocations(ArrayList<CJRLocation> paramArrayList)
  {
    this.droppingLocations = paramArrayList;
  }
  
  public void setDuration(String paramString)
  {
    this.duration = paramString;
  }
  
  public void setFare(double[] paramArrayOfDouble)
  {
    this.fare = paramArrayOfDouble;
  }
  
  public void setIdProofRequired(boolean paramBoolean)
  {
    this.idProofRequired = paramBoolean;
  }
  
  public void setInfo(String paramString)
  {
    this.info = paramString;
  }
  
  public void setIsSoldBusSeat(CJRIsSoldBusSeat paramCJRIsSoldBusSeat)
  {
    this.isSoldBusSeat = paramCJRIsSoldBusSeat;
  }
  
  public void setIs_service_charge_extra(boolean paramBoolean)
  {
    this.is_service_charge_extra = paramBoolean;
  }
  
  public void setLuxury(boolean paramBoolean)
  {
    this.isLuxury = paramBoolean;
  }
  
  public void setMobileTicketAllowed(boolean paramBoolean)
  {
    this.isMobileTicketAllowed = paramBoolean;
  }
  
  public void setNonAc(boolean paramBoolean)
  {
    this.isNonAc = paramBoolean;
  }
  
  public void setNonSleeper(boolean paramBoolean)
  {
    this.isNonSleeper = paramBoolean;
  }
  
  public void setOperatorId(String paramString)
  {
    this.operatorId = paramString;
  }
  
  public void setPartialCancellationAllowed(boolean paramBoolean)
  {
    this.partialCancellationAllowed = paramBoolean;
  }
  
  public void setProviderId(String paramString)
  {
    this.providerId = paramString;
  }
  
  public void setRatings(String paramString)
  {
    this.ratings = paramString;
  }
  
  public void setReportingTime(String paramString)
  {
    this.reportingTime = paramString;
  }
  
  public void setReview_data(CJRBusSearchReviewData paramCJRBusSearchReviewData)
  {
    this.review_data = paramCJRBusSearchReviewData;
  }
  
  public void setService_tax_message(String paramString)
  {
    this.service_tax_message = paramString;
  }
  
  public void setSleeper(boolean paramBoolean)
  {
    this.isSleeper = paramBoolean;
  }
  
  public void setSource(String paramString)
  {
    this.source = paramString;
  }
  
  public void setTravelDurationDays(String paramString)
  {
    this.travelDurationDays = paramString;
  }
  
  public void setTravelsName(String paramString)
  {
    this.travelsName = paramString;
  }
  
  public void setTripId(String paramString)
  {
    this.tripId = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/CJRBusSearchItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */