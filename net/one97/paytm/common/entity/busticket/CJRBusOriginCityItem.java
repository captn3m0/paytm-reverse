package net.one97.paytm.common.entity.busticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBusOriginCityItem
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="id")
  private int cityId;
  @b(a="name")
  private String cityName;
  private int itemType = 0;
  @b(a="short_name")
  private String shortCityName;
  
  public int getCityId()
  {
    return this.cityId;
  }
  
  public String getCityName()
  {
    return this.cityName;
  }
  
  public int getItemType()
  {
    return this.itemType;
  }
  
  public String getShortCityName()
  {
    return this.shortCityName;
  }
  
  public void setCityName(String paramString)
  {
    this.cityName = paramString;
  }
  
  public void setItemType(int paramInt)
  {
    this.itemType = paramInt;
  }
  
  public void setShortCityName(String paramString)
  {
    this.shortCityName = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/CJRBusOriginCityItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */