package net.one97.paytm.common.entity.busticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBlockOneResponse
  implements IJRDataModel
{
  @b(a="body")
  private CJRBlockOneModel blockResponse;
  @b(a="code")
  private int code;
  @b(a="meta")
  private Meta meta;
  
  public CJRBlockOneModel getBlockResponse()
  {
    return this.blockResponse;
  }
  
  public Meta getMeta()
  {
    return this.meta;
  }
  
  public void setBlockResponse(CJRBlockOneModel paramCJRBlockOneModel)
  {
    this.blockResponse = paramCJRBlockOneModel;
  }
  
  public void setMeta(Meta paramMeta)
  {
    this.meta = paramMeta;
  }
  
  public class Meta
    implements IJRDataModel
  {
    @b(a="requestid")
    private String requestid;
    
    public Meta() {}
    
    public String getRequestid()
    {
      return this.requestid;
    }
    
    public void setRequestid(String paramString)
    {
      this.requestid = paramString;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/CJRBlockOneResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */