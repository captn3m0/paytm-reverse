package net.one97.paytm.common.entity.busticket;

import com.google.c.a.a;
import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class TripDetailsMeta
  implements IJRDataModel
{
  @a
  @b(a="boardingPoints")
  private ArrayList<TripBusDetailBoardingPoints> boardingPoints = new ArrayList();
  @a
  @b(a="product_id")
  private String productId;
  @a
  @b(a="provider")
  private TripDetailProvider provider;
  @a
  @b(a="requestid")
  private String requestid;
  @a
  @b(a="serverTime")
  private long serverTime;
  
  public ArrayList<TripBusDetailBoardingPoints> getBoardingPoints()
  {
    return this.boardingPoints;
  }
  
  public String getProductId()
  {
    return this.productId;
  }
  
  public TripDetailProvider getProvider()
  {
    return this.provider;
  }
  
  public String getRequestid()
  {
    return this.requestid;
  }
  
  public long getServerTime()
  {
    return this.serverTime;
  }
  
  public void setBoardingPoints(ArrayList<TripBusDetailBoardingPoints> paramArrayList)
  {
    this.boardingPoints = paramArrayList;
  }
  
  public void setProductId(String paramString)
  {
    this.productId = paramString;
  }
  
  public void setProvider(TripDetailProvider paramTripDetailProvider)
  {
    this.provider = paramTripDetailProvider;
  }
  
  public void setRequestid(String paramString)
  {
    this.requestid = paramString;
  }
  
  public void setServerTime(long paramLong)
  {
    this.serverTime = paramLong;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/TripDetailsMeta.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */