package net.one97.paytm.common.entity.busticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBusSearchMetaData
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="requestid")
  private String requestid;
  @b(a="xday_val")
  private String xday_val;
  
  public CJRBusSearchMetaData(String paramString1, String paramString2)
  {
    this.requestid = paramString1;
    this.xday_val = paramString2;
  }
  
  public String getRequestid()
  {
    return this.requestid;
  }
  
  public String getXday_val()
  {
    return this.xday_val;
  }
  
  public void setRequestid(String paramString)
  {
    this.requestid = paramString;
  }
  
  public void setXday_val(String paramString)
  {
    this.xday_val = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/CJRBusSearchMetaData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */