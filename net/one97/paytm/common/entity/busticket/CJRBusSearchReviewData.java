package net.one97.paytm.common.entity.busticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBusSearchReviewData
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="cancel_in_xday")
  private int cancel_in_xday;
  @b(a="reviewCount")
  private int mReviewCount;
  
  public CJRBusSearchReviewData(int paramInt1, int paramInt2)
  {
    this.cancel_in_xday = paramInt1;
    this.mReviewCount = paramInt2;
  }
  
  public int getCancel_in_xday()
  {
    return this.cancel_in_xday;
  }
  
  public int getReviewCount()
  {
    return this.mReviewCount;
  }
  
  public void setCancel_in_xday(int paramInt)
  {
    this.cancel_in_xday = paramInt;
  }
  
  public void setReviewCount(int paramInt)
  {
    this.mReviewCount = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/CJRBusSearchReviewData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */