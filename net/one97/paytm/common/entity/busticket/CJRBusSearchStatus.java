package net.one97.paytm.common.entity.busticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBusSearchStatus
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="result")
  private String result;
  
  public String getResult()
  {
    return this.result;
  }
  
  public void setResult(String paramString)
  {
    this.result = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/CJRBusSearchStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */