package net.one97.paytm.common.entity.busticket;

import com.google.c.a.a;
import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class TripBusDetailsStatus
  implements IJRDataModel
{
  @a
  @b(a="message")
  private TripBusDetailMessage message;
  @a
  @b(a="result")
  private String result;
  
  public TripBusDetailMessage getMessage()
  {
    return this.message;
  }
  
  public String getResult()
  {
    return this.result;
  }
  
  public void setMessage(TripBusDetailMessage paramTripBusDetailMessage)
  {
    this.message = paramTripBusDetailMessage;
  }
  
  public void setResult(String paramString)
  {
    this.result = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/TripBusDetailsStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */