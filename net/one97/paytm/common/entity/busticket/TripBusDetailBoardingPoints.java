package net.one97.paytm.common.entity.busticket;

import com.google.c.a.a;
import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class TripBusDetailBoardingPoints
  implements IJRDataModel
{
  @a
  @b(a="contactNumber")
  private String contactNumber;
  @a
  @b(a="landmark")
  private String landmark;
  @a
  @b(a="locationAddress")
  private String locationAddress;
  @a
  @b(a="locationName")
  private String locationName;
  @a
  @b(a="providerLocationId")
  private String providerLocationId;
  @a
  @b(a="time")
  private String time;
  
  public String getContactNumber()
  {
    return this.contactNumber;
  }
  
  public String getLandmark()
  {
    return this.landmark;
  }
  
  public String getLocationAddress()
  {
    return this.locationAddress;
  }
  
  public String getLocationName()
  {
    return this.locationName;
  }
  
  public String getProviderLocationId()
  {
    return this.providerLocationId;
  }
  
  public String getTime()
  {
    return this.time;
  }
  
  public void setContactNumber(String paramString)
  {
    this.contactNumber = paramString;
  }
  
  public void setLandmark(String paramString)
  {
    this.landmark = paramString;
  }
  
  public void setLocationAddress(String paramString)
  {
    this.locationAddress = paramString;
  }
  
  public void setLocationName(String paramString)
  {
    this.locationName = paramString;
  }
  
  public void setProviderLocationId(String paramString)
  {
    this.providerLocationId = paramString;
  }
  
  public void setTime(String paramString)
  {
    this.time = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/TripBusDetailBoardingPoints.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */