package net.one97.paytm.common.entity.busticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBusSearchCancellationPolicy
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="departure_heading")
  private String departure_heading;
  @b(a="policy_heading")
  private String policy_heading;
  
  public String getDeparture_heading()
  {
    return this.departure_heading;
  }
  
  public String getPolicy_heading()
  {
    return this.policy_heading;
  }
  
  public void setDeparture_heading(String paramString)
  {
    this.departure_heading = paramString;
  }
  
  public void setPolicy_heading(String paramString)
  {
    this.policy_heading = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/CJRBusSearchCancellationPolicy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */