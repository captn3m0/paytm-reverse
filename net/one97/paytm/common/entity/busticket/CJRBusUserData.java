package net.one97.paytm.common.entity.busticket;

import com.google.c.a.b;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBusUserData
  implements IJRDataModel
{
  @b(a="body")
  private List<CJRBusBodyData> body;
  @b(a="code")
  private int code;
  @b(a="message")
  private String message;
  
  public List<CJRBusBodyData> getBody()
  {
    return this.body;
  }
  
  public int getCode()
  {
    return this.code;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public void setBody(List<CJRBusBodyData> paramList)
  {
    this.body = paramList;
  }
  
  public void setCode(int paramInt)
  {
    this.code = paramInt;
  }
  
  public void setMessage(String paramString)
  {
    this.message = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/CJRBusUserData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */