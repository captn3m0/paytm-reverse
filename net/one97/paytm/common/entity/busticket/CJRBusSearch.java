package net.one97.paytm.common.entity.busticket;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CJRBusSearch
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="body")
  private ArrayList<CJRBusSearchItem> body;
  @b(a="meta")
  private CJRBusSearchMetaData meta;
  
  public ArrayList<CJRBusSearchItem> getBody()
  {
    return this.body;
  }
  
  public CJRBusSearchMetaData getMeta()
  {
    return this.meta;
  }
  
  public void parseJson(String paramString)
    throws JSONException
  {
    paramString = new JSONObject(paramString);
    JSONArray localJSONArray = paramString.getJSONArray("body");
    paramString = paramString.getJSONObject("meta");
    this.meta = new CJRBusSearchMetaData(paramString.getString("requestid"), paramString.getString("xday_val"));
    this.body = new ArrayList();
    if (localJSONArray != null)
    {
      int i = 0;
      for (;;)
      {
        if (i < localJSONArray.length())
        {
          CJRBusSearchItem localCJRBusSearchItem = new CJRBusSearchItem();
          paramString = localJSONArray.getJSONObject(i);
          Object localObject1;
          int j;
          if (paramString != null)
          {
            if (!paramString.isNull("tripId")) {
              localCJRBusSearchItem.setTripId(paramString.getString("tripId"));
            }
            if (!paramString.isNull("travelDurationDays")) {
              localCJRBusSearchItem.setTravelDurationDays(paramString.getString("travelDurationDays"));
            }
            if (!paramString.isNull("arrivalTime")) {
              localCJRBusSearchItem.setArrivalTime(paramString.getString("arrivalTime"));
            }
            if (!paramString.isNull("departureDate")) {
              localCJRBusSearchItem.setDepartureDate(paramString.getString("departureDate"));
            }
            if (!paramString.isNull("departureTime")) {
              localCJRBusSearchItem.setDepartureTime(paramString.getString("departureTime"));
            }
            if (!paramString.isNull("destination")) {
              localCJRBusSearchItem.setDestination(paramString.getString("destination"));
            }
            if (!paramString.isNull("source")) {
              localCJRBusSearchItem.setSource(paramString.getString("source"));
            }
            if (!paramString.isNull("avalableSeats")) {
              localCJRBusSearchItem.setAvalableSeats(paramString.getInt("avalableSeats"));
            }
            if (!paramString.isNull("busType")) {
              localCJRBusSearchItem.setBusType(paramString.getString("busType"));
            }
            Object localObject2;
            if (!paramString.isNull("fare"))
            {
              localObject1 = paramString.getJSONArray("fare");
              localObject2 = new double[((JSONArray)localObject1).length()];
              j = 0;
              while (j < ((JSONArray)localObject1).length())
              {
                localObject2[j] = ((JSONArray)localObject1).getDouble(j);
                j += 1;
              }
              localCJRBusSearchItem.setFare((double[])localObject2);
            }
            if (!paramString.isNull("travelsName")) {
              localCJRBusSearchItem.setTravelsName(paramString.getString("travelsName"));
            }
            if (!paramString.isNull("computedTravelsName")) {
              localCJRBusSearchItem.setComputedTravelsName(paramString.getString("computedTravelsName"));
            }
            if (!paramString.isNull("providerId")) {
              localCJRBusSearchItem.setProviderId(paramString.getString("providerId"));
            }
            JSONObject localJSONObject;
            CJRLocation localCJRLocation;
            if (!paramString.isNull("boardingLocations"))
            {
              localObject1 = paramString.getJSONArray("boardingLocations");
              localObject2 = new ArrayList();
              j = 0;
              while (j < ((JSONArray)localObject1).length())
              {
                localJSONObject = ((JSONArray)localObject1).getJSONObject(j);
                localCJRLocation = new CJRLocation();
                if (!localJSONObject.isNull("providerLocationId")) {
                  localCJRLocation.setProviderLocationId(localJSONObject.getString("providerLocationId"));
                }
                if (!localJSONObject.isNull("locationName")) {
                  localCJRLocation.setLocationName(localJSONObject.getString("locationName"));
                }
                if (!localJSONObject.isNull("time")) {
                  localCJRLocation.setTime(localJSONObject.getString("time"));
                }
                if (!localJSONObject.isNull("reportingTime")) {
                  localCJRLocation.setReportingTime(localJSONObject.getString("reportingTime"));
                }
                if (!localJSONObject.isNull("boardingDate")) {
                  localCJRLocation.setBoardingDate(localJSONObject.getString("boardingDate"));
                }
                ((ArrayList)localObject2).add(localCJRLocation);
                j += 1;
              }
              localCJRBusSearchItem.setBoardingLocations((ArrayList)localObject2);
            }
            if (!paramString.isNull("droppingLocations"))
            {
              localObject1 = paramString.getJSONArray("droppingLocations");
              localObject2 = new ArrayList();
              j = 0;
              while (j < ((JSONArray)localObject1).length())
              {
                localJSONObject = ((JSONArray)localObject1).getJSONObject(j);
                localCJRLocation = new CJRLocation();
                if (!localJSONObject.isNull("providerLocationId")) {
                  localCJRLocation.setProviderLocationId(localJSONObject.getString("providerLocationId"));
                }
                if (!localJSONObject.isNull("locationName")) {
                  localCJRLocation.setLocationName(localJSONObject.getString("locationName"));
                }
                if (!localJSONObject.isNull("time")) {
                  localCJRLocation.setTime(localJSONObject.getString("time"));
                }
                if (!localJSONObject.isNull("reportingTime")) {
                  localCJRLocation.setReportingTime(localJSONObject.getString("reportingTime"));
                }
                ((ArrayList)localObject2).add(localCJRLocation);
                j += 1;
              }
              localCJRBusSearchItem.setDroppingLocations((ArrayList)localObject2);
            }
            if (!paramString.isNull("boardingTime")) {
              localCJRBusSearchItem.setBoardingTime(paramString.getString("boardingTime"));
            }
            if (!paramString.isNull("idProofRequired")) {
              localCJRBusSearchItem.setIdProofRequired(paramString.getBoolean("idProofRequired"));
            }
            if (!paramString.isNull("arrivalDate")) {
              localCJRBusSearchItem.setArrivalDate(paramString.getString("arrivalDate"));
            }
            if (!paramString.isNull("duration")) {
              localCJRBusSearchItem.setDuration(paramString.getString("duration"));
            }
            if (!paramString.isNull("info")) {
              localCJRBusSearchItem.setInfo(paramString.getJSONObject("info").toString());
            }
            if ((!paramString.isNull("isAc")) && (paramString.has("isAc"))) {
              localCJRBusSearchItem.setAc(paramString.getBoolean("isAc"));
            }
            if ((!paramString.isNull("isSleeper")) && (paramString.has("isSleeper"))) {
              localCJRBusSearchItem.setSleeper(paramString.getBoolean("isSleeper"));
            }
            if ((!paramString.isNull("isLuxury")) && (paramString.has("isLuxury"))) {
              localCJRBusSearchItem.setLuxury(paramString.getBoolean("isLuxury"));
            }
            if ((!paramString.isNull("review_data")) && (paramString.has("review_data"))) {
              localObject1 = paramString.getJSONObject("review_data");
            }
          }
          try
          {
            j = ((JSONObject)localObject1).getInt("reviewCount");
            localCJRBusSearchItem.setReview_data(new CJRBusSearchReviewData(((JSONObject)localObject1).getInt("cancel_in_xday"), j));
            if ((!paramString.isNull("isSold")) && (paramString.has("isSold")))
            {
              localObject1 = paramString.getJSONObject("isSold");
              paramString = null;
            }
            try
            {
              localObject1 = ((JSONObject)localObject1).getString("msg");
              paramString = (String)localObject1;
            }
            catch (Exception localException)
            {
              for (;;) {}
            }
            localObject1 = new CJRIsSoldBusSeat();
            ((CJRIsSoldBusSeat)localObject1).setMsg(paramString);
            localCJRBusSearchItem.setIsSoldBusSeat((CJRIsSoldBusSeat)localObject1);
            this.body.add(localCJRBusSearchItem);
            i += 1;
          }
          catch (JSONException localJSONException)
          {
            for (;;)
            {
              j = 0;
            }
          }
        }
      }
    }
  }
  
  public void setBody(ArrayList<CJRBusSearchItem> paramArrayList)
  {
    this.body = paramArrayList;
  }
  
  public void setMeta(CJRBusSearchMetaData paramCJRBusSearchMetaData)
  {
    this.meta = paramCJRBusSearchMetaData;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/CJRBusSearch.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */