package net.one97.paytm.common.entity.busticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRStatusModel
  implements IJRDataModel
{
  @b(a="message")
  private String message;
  @b(a="result")
  private String result;
  
  public String getMessage()
  {
    return this.message;
  }
  
  public String getResult()
  {
    return this.result;
  }
  
  public void setMessage(String paramString)
  {
    this.message = paramString;
  }
  
  public void setResult(String paramString)
  {
    this.result = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/busticket/CJRStatusModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */