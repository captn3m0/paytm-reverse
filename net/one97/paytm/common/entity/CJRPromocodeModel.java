package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRPromocodeModel
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="message")
  private String mMessage;
  @b(a="minQuantity")
  private String mMinQuantity;
  @b(a="promocode")
  private String mPromoCode;
  @b(a="validity")
  private String mPromoCodeValidity;
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getMinQuantity()
  {
    return this.mMinQuantity;
  }
  
  public String getPromoCode()
  {
    return this.mPromoCode;
  }
  
  public String getPromoCodeValidity()
  {
    return this.mPromoCodeValidity;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRPromocodeModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */