package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRSocialSendOtp
  implements IJRDataModel
{
  @b(a="message")
  private String mMessage;
  @b(a="signupToken")
  private String mSignUpToken;
  @b(a="status")
  private String mStatus;
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getSignUpToken()
  {
    return this.mSignUpToken;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRSocialSendOtp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */