package net.one97.paytm.common.entity;

import java.io.Serializable;
import java.util.ArrayList;
import net.one97.paytm.common.entity.shopping.CJRCatalogItem;

public class CJRCatalogSavedState
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private ArrayList<CJRCatalogItem> catalogList;
  private CJRNode currentNode;
  private boolean isHome;
  private CJRCatalogItem mSelectedItem;
  
  public ArrayList<CJRCatalogItem> getCatalogList()
  {
    return this.catalogList;
  }
  
  public CJRNode getCurrentNode()
  {
    return this.currentNode;
  }
  
  public CJRCatalogItem getSelectedItem()
  {
    return this.mSelectedItem;
  }
  
  public boolean isHome()
  {
    return this.isHome;
  }
  
  public void setCatalogList(ArrayList<CJRCatalogItem> paramArrayList)
  {
    this.catalogList = paramArrayList;
  }
  
  public void setCurrentNode(CJRNode paramCJRNode)
  {
    this.currentNode = paramCJRNode;
  }
  
  public void setHome(boolean paramBoolean)
  {
    this.isHome = paramBoolean;
  }
  
  public void setSelectedItem(CJRCatalogItem paramCJRCatalogItem)
  {
    this.mSelectedItem = paramCJRCatalogItem;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRCatalogSavedState.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */