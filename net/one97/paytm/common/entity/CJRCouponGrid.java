package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.shopping.CJRGridProduct;

public class CJRCouponGrid
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="grid_layout")
  private ArrayList<CJRGridProduct> mGridLayout = new ArrayList();
  @b(a="has_more")
  private boolean mHasMore;
  
  public ArrayList<CJRGridProduct> getGridLayout()
  {
    return this.mGridLayout;
  }
  
  public boolean hasMore()
  {
    return this.mHasMore;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRCouponGrid.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */