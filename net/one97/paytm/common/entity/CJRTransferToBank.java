package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRTransferToBank
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="displayMessage")
  private String mDisplayMessage;
  @b(a="response")
  private CJRTransferToBankResponse mResponse;
  
  public String getDisplayMessage()
  {
    return this.mDisplayMessage;
  }
  
  public String getName()
  {
    return null;
  }
  
  public CJRTransferToBankResponse getResponse()
  {
    return this.mResponse;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRTransferToBank.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */