package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRRegisterResendOTP
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="message")
  private String mMessage;
  @b(a="responseCode")
  private String mResponseCode;
  @b(a="status")
  private String mStatus;
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getResponseCode()
  {
    return this.mResponseCode;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRRegisterResendOTP.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */