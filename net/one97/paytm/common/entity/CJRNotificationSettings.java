package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.ArrayList;

public class CJRNotificationSettings
  implements IJRDataModel
{
  @b(a="preferences")
  ArrayList<CJRNotificationItem> mNotificationItems;
  
  public ArrayList<CJRNotificationItem> getNotificationItems()
  {
    return this.mNotificationItems;
  }
  
  public void setNotificationItems(ArrayList<CJRNotificationItem> paramArrayList)
  {
    this.mNotificationItems = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRNotificationSettings.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */