package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class SearchFeedbackResponse
  implements IJRDataModel
{
  @b(a="message")
  private String mMessage;
  @b(a="status")
  private String mSatus;
  
  public String gemMessage()
  {
    return this.mMessage;
  }
  
  public String getSatus()
  {
    return this.mSatus;
  }
  
  public void setMessage(String paramString)
  {
    this.mMessage = paramString;
  }
  
  public void setSatus(String paramString)
  {
    this.mSatus = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/SearchFeedbackResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */