package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRTwitterConnectResponse
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  private String mCallType;
  private int mHttpResponseCode;
  @b(a="message")
  private String mMessage;
  @b(a="responseCode")
  private String mResponseCode;
  @b(a="status")
  private String mStatus;
  
  public String getCallType()
  {
    return this.mCallType;
  }
  
  public int getHttpResponseCode()
  {
    return this.mHttpResponseCode;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getResponseCode()
  {
    return this.mResponseCode;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public void setCallType(String paramString)
  {
    this.mCallType = paramString;
  }
  
  public void setHttpResponseCode(int paramInt)
  {
    this.mHttpResponseCode = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRTwitterConnectResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */