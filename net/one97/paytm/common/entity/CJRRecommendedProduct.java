package net.one97.paytm.common.entity;

import com.google.c.a.b;
import net.one97.paytm.common.entity.shopping.CJRHomePageLayoutV2;

public class CJRRecommendedProduct
  implements IJRDataModel
{
  @b(a="data")
  private CJRHomePageLayoutV2 mRecommendedProduct;
  @b(a="status")
  private String mStatus;
  
  public CJRHomePageLayoutV2 getRecommendedProduct()
  {
    return this.mRecommendedProduct;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRRecommendedProduct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */