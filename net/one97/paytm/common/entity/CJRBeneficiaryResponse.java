package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.ArrayList;

public class CJRBeneficiaryResponse
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="beneficiaryList")
  private ArrayList<CJRBeneficiary> mBeneficiaryList;
  
  public ArrayList<CJRBeneficiary> getBeneficiaryList()
  {
    return this.mBeneficiaryList;
  }
  
  public String getName()
  {
    return null;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRBeneficiaryResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */