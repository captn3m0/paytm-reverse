package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class ResetLoginPassword
  implements IJRDataModel
{
  @b(a="code")
  private String code;
  @b(a="displayMessage")
  private String displayMessage;
  @b(a="message")
  private String message;
  @b(a="responseCode")
  private String responseCode;
  @b(a="state")
  private String state;
  @b(a="status")
  private String status;
  @b(a="statusCode")
  private String statusCode;
  @b(a="statusMessage")
  private String statusMessage;
  
  public String getCode()
  {
    return this.code;
  }
  
  public String getDisplayMessage()
  {
    return this.displayMessage;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public String getResponseCode()
  {
    return this.responseCode;
  }
  
  public String getState()
  {
    return this.state;
  }
  
  public String getStatus()
  {
    return this.status;
  }
  
  public String getStatusCode()
  {
    return this.statusCode;
  }
  
  public String getStatusMessage()
  {
    return this.statusMessage;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/ResetLoginPassword.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */