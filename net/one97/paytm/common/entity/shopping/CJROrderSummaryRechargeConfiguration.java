package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROrderSummaryRechargeConfiguration
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="price")
  private String mPrice;
  @b(a="recharge_number")
  private String mRechargeNumber;
  
  public String getPrice()
  {
    return this.mPrice;
  }
  
  public String getRechargeNumber()
  {
    return this.mRechargeNumber;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROrderSummaryRechargeConfiguration.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */