package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.HashMap;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRProductStatistics
  implements IJRDataModel
{
  private final String KEY_ORDER_COUNT = "order_count";
  private final String KEY_WISH_LIST = "wishlist";
  private final String KEY_WISH_LIST_COUNT = "wl_count";
  @b(a="all")
  private HashMap<String, Integer> mAll;
  @b(a="user")
  private HashMap<String, Boolean> mUser;
  
  public boolean getIsWishListed()
  {
    if ((this.mUser != null) && (this.mUser.containsKey("wishlist"))) {
      return ((Boolean)this.mUser.get("wishlist")).booleanValue();
    }
    return false;
  }
  
  public int getOrderCount()
  {
    if ((this.mAll != null) && (this.mAll.containsKey("order_count"))) {
      return ((Integer)this.mAll.get("order_count")).intValue();
    }
    return 0;
  }
  
  public int getWishListCount()
  {
    if ((this.mAll != null) && (this.mAll.containsKey("wl_count"))) {
      return ((Integer)this.mAll.get("wl_count")).intValue();
    }
    return 0;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRProductStatistics.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */