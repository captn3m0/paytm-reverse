package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPincodeCheck
  implements IJRDataModel
{
  private static final long serialVersionUID = 6385089574864398425L;
  @b(a="enabled")
  private boolean isEnabled;
  @b(a="cod_enabled")
  private boolean mCodEnabled;
  @b(a="error")
  private String mError;
  @b(a="pincode")
  private String mPincode;
  @b(a="shipping_charges")
  private int mShippingCharges;
  @b(a="status")
  private CJRCartStatus mStatus;
  
  public CJRCartStatus getStatus()
  {
    return this.mStatus;
  }
  
  public String getmError()
  {
    return this.mError;
  }
  
  public String getmPincode()
  {
    return this.mPincode;
  }
  
  public int getmShippingCharges()
  {
    return this.mShippingCharges;
  }
  
  public boolean isEnabled()
  {
    return this.isEnabled;
  }
  
  public boolean ismCodEnabled()
  {
    return this.mCodEnabled;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRPincodeCheck.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */