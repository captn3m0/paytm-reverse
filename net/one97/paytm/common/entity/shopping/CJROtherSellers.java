package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROtherSellers
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  private boolean isAuthrised;
  @b(a="id")
  private String mId;
  private String mImageAuthUrl;
  @b(a="applied")
  private boolean mIsApplied;
  @b(a="exist")
  private boolean mIsExist;
  @b(a="name")
  private String mName;
  @b(a="offer_price")
  private String mOfferPrice;
  @b(a="offer_text")
  private String mOfferText;
  @b(a="product_id")
  private String mProductId;
  private CJRSellerRatings mRatings;
  private boolean mSelected = false;
  @b(a="totalScore")
  private float mTotalScore;
  @b(a="url")
  private String mUrl;
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getOfferPrice()
  {
    return this.mOfferPrice;
  }
  
  public String getOfferText()
  {
    return this.mOfferText;
  }
  
  public CJRSellerRatings getRatings()
  {
    return this.mRatings;
  }
  
  public float getTotalScore()
  {
    return this.mTotalScore;
  }
  
  public String getUrl()
  {
    return this.mUrl;
  }
  
  public String getmId()
  {
    return this.mId;
  }
  
  public String getmImageAuthUrl()
  {
    return this.mImageAuthUrl;
  }
  
  public String getmProductId()
  {
    return this.mProductId;
  }
  
  public boolean isAuthrised()
  {
    return this.isAuthrised;
  }
  
  public boolean isExist()
  {
    return this.mIsExist;
  }
  
  public boolean isSelected()
  {
    return this.mSelected;
  }
  
  public void setIsAuthrised(boolean paramBoolean)
  {
    this.isAuthrised = paramBoolean;
  }
  
  public void setRatings(CJRSellerRatings paramCJRSellerRatings)
  {
    this.mRatings = paramCJRSellerRatings;
  }
  
  public void setSelected(boolean paramBoolean)
  {
    this.mSelected = paramBoolean;
  }
  
  public void setTotalScore(float paramFloat)
  {
    this.mTotalScore = paramFloat;
  }
  
  public void setmImageAuthUrl(String paramString)
  {
    this.mImageAuthUrl = paramString;
  }
  
  public void setmProductId(String paramString)
  {
    this.mProductId = paramString;
  }
  
  public boolean ssApplied()
  {
    return this.mIsApplied;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROtherSellers.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */