package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.CJRDataModelItem;
import net.one97.paytm.common.entity.CJRFilterItem;
import net.one97.paytm.common.entity.CJRFrontEndFilter;

public class CJRGrid
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="ancestors")
  private ArrayList<CJRAncestor> mAncestors = new ArrayList();
  @b(a="app_url")
  private String mAppUrl;
  @b(a="datasources")
  private ArrayList<CJRDataSource> mDataSourceList = new ArrayList();
  @b(a="default_sorting_param")
  private String mDeafultSortParam;
  @b(a="filters")
  private ArrayList<CJRFilterItem> mFilterList = new ArrayList();
  @b(a="footer_html")
  private String mFooterHtml;
  @b(a="footer_image_url")
  private String mFooterImageUrl;
  @b(a="frontend_filters")
  private ArrayList<CJRFrontEndFilter> mFrontEndFilterList = new ArrayList();
  @b(a="ga_key")
  private String mGAKey;
  private String mGATitle;
  @b(a="grid_header")
  private CJRGridHeader mGridHeader;
  @b(a="grid_layout")
  private ArrayList<CJRGridProduct> mGridLayout = new ArrayList();
  @b(a="meta")
  private CJRGridMeta mGridMeta;
  @b(a="has_more")
  private boolean mHasMore;
  @b(a="id")
  private String mID;
  @b(a="offer_text")
  private String mOfferText;
  @b(a="result_type")
  private String mResultType;
  @b(a="total_count")
  private String mSearchCount;
  @b(a="search_id")
  private String mSearchId;
  @b(a="search_suggestion")
  private ArrayList<CJRSearchSuggestion> mSearchSuggestionList;
  @b(a="totalCount")
  private String mSearchTotalCount;
  @b(a="search_user_id")
  private String mSearchUserId;
  @b(a="sorting_keys")
  private ArrayList<CJRSortingKeys> mSortKeys = new ArrayList();
  @b(a="terms_conditions")
  private CJRTermsAndConditions mTermsConditions;
  
  public String getAncestorID()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if ((this.mAncestors != null) && (this.mAncestors.size() > 0))
    {
      int j = this.mAncestors.size();
      int i = 0;
      while (i < j)
      {
        localStringBuilder.append(((CJRAncestor)this.mAncestors.get(i)).getAncestorID());
        if ((j > 1) && (i < j - 1)) {
          localStringBuilder.append("/");
        }
        i += 1;
      }
    }
    return localStringBuilder.toString();
  }
  
  public ArrayList<CJRAncestor> getAncestors()
  {
    return this.mAncestors;
  }
  
  public String getAppUrl()
  {
    return this.mAppUrl;
  }
  
  public String getCategoryMapPath()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if ((this.mAncestors != null) && (this.mAncestors.size() > 0))
    {
      int j = this.mAncestors.size();
      int i = 0;
      while (i < j)
      {
        localStringBuilder.append(((CJRAncestor)this.mAncestors.get(i)).getName());
        if ((j > 1) && (i < j - 1)) {
          localStringBuilder.append("/");
        }
        i += 1;
      }
    }
    return localStringBuilder.toString();
  }
  
  public String getDefaultSortParam()
  {
    return this.mDeafultSortParam;
  }
  
  public ArrayList<CJRFilterItem> getFilterList()
  {
    return this.mFilterList;
  }
  
  public String getFooterHtml()
  {
    return this.mFooterHtml;
  }
  
  public String getFooterImageUrl()
  {
    return this.mFooterImageUrl;
  }
  
  public ArrayList<CJRFrontEndFilter> getFrontEndFilterList()
  {
    return this.mFrontEndFilterList;
  }
  
  public String getGAKey()
  {
    return this.mGAKey;
  }
  
  public String getGATitle()
  {
    return this.mGATitle;
  }
  
  public CJRGridHeader getGridHeader()
  {
    return this.mGridHeader;
  }
  
  public ArrayList<CJRGridProduct> getGridLayout()
  {
    int i = 0;
    while (i < this.mGridLayout.size())
    {
      ((CJRGridProduct)this.mGridLayout.get(i)).setListId(getmID());
      i += 1;
    }
    return this.mGridLayout;
  }
  
  public String getGridMeta()
  {
    return this.mGridMeta.getExperimentName();
  }
  
  public boolean getHasMore()
  {
    return this.mHasMore;
  }
  
  public String getLastAncestorID()
  {
    Object localObject2 = null;
    Object localObject1 = localObject2;
    if (this.mAncestors != null)
    {
      localObject1 = localObject2;
      if (this.mAncestors.size() > 0)
      {
        int i = this.mAncestors.size();
        localObject1 = ((CJRAncestor)this.mAncestors.get(i - 1)).getAncestorID();
      }
    }
    return (String)localObject1;
  }
  
  public String getLastAncestorName()
  {
    Object localObject2 = null;
    Object localObject1 = localObject2;
    if (this.mAncestors != null)
    {
      localObject1 = localObject2;
      if (this.mAncestors.size() > 0)
      {
        int i = this.mAncestors.size();
        localObject1 = ((CJRAncestor)this.mAncestors.get(i - 1)).getName();
      }
    }
    return (String)localObject1;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getOfferText()
  {
    return this.mOfferText;
  }
  
  public String getResultType()
  {
    return this.mResultType;
  }
  
  public String getSearchCount()
  {
    return this.mSearchCount;
  }
  
  public String getSearchId()
  {
    return this.mSearchId;
  }
  
  public ArrayList<CJRSearchSuggestion> getSearchSuggestionList()
  {
    return this.mSearchSuggestionList;
  }
  
  public String getSearchTotalCount()
  {
    return this.mSearchTotalCount;
  }
  
  public String getSearchUserId()
  {
    return this.mSearchUserId;
  }
  
  public ArrayList<CJRSortingKeys> getSortingKeys()
  {
    return this.mSortKeys;
  }
  
  public CJRTermsAndConditions getTermsConditions()
  {
    return this.mTermsConditions;
  }
  
  public ArrayList<CJRDataSource> getmDataSourceList()
  {
    return this.mDataSourceList;
  }
  
  public String getmID()
  {
    return this.mID;
  }
  
  public void setGATitle(String paramString)
  {
    this.mGATitle = paramString;
  }
  
  public void setSearchId(String paramString)
  {
    this.mSearchId = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRGrid.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */