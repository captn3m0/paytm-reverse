package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.CJRItem;

public class CJRRelatedCategory
  extends CJRItem
{
  private static final long serialVersionUID = 1L;
  private String listId = "";
  @b(a="label")
  private String mLabel;
  @b(a="url")
  public String mUrl;
  public String mUrlType;
  
  public String getBrand()
  {
    return null;
  }
  
  public String getCategoryId()
  {
    return null;
  }
  
  public String getItemID()
  {
    return null;
  }
  
  public String getLabel()
  {
    return this.mLabel;
  }
  
  public String getListId()
  {
    return this.listId;
  }
  
  public String getListName()
  {
    return null;
  }
  
  public int getListPosition()
  {
    return -1;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getParentID()
  {
    return "";
  }
  
  public ArrayList<CJRRelatedCategory> getRelatedCategories()
  {
    return null;
  }
  
  public String getSearchABValue()
  {
    return null;
  }
  
  public String getSearchCategory()
  {
    return null;
  }
  
  public String getSearchResultType()
  {
    return null;
  }
  
  public String getSearchTerm()
  {
    return null;
  }
  
  public String getSearchType()
  {
    return null;
  }
  
  public String getURL()
  {
    return this.mUrl;
  }
  
  public String getURLType()
  {
    return this.mUrlType;
  }
  
  public String getmContainerInstanceID()
  {
    return "";
  }
  
  public void setLabel(String paramString)
  {
    this.mLabel = paramString;
  }
  
  public void setUrl(String paramString)
  {
    this.mUrl = paramString;
  }
  
  public void setUrlType(String paramString)
  {
    this.mUrlType = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRRelatedCategory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */