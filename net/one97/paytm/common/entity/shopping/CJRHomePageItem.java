package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.CJRItem;
import net.one97.paytm.common.entity.brandStoreModels.CJRBrandAttributes;
import net.one97.paytm.common.entity.inmobi.CJRTPData;
import net.one97.paytm.common.utility.c;

public class CJRHomePageItem
  extends CJRItem
{
  private static final long serialVersionUID = 1L;
  private String aadhar_number;
  private String bankAccountNumber;
  private String bankAmount;
  private String bankcomment;
  private String ifsc;
  private boolean imageFromResource;
  private boolean isFromReqDelivery = false;
  private boolean isSellerStoreImpressionSent;
  private boolean itemViewed;
  private String kyc_name;
  private String landing_page;
  @b(a="layout")
  private CJRLayoutParams layoutParam;
  @b(a="actual_price")
  private float mActualPrice;
  @b(a="brand")
  private String mBrand;
  @b(a="category")
  private String mCategory;
  private String mCategoryId;
  public String mContainerInstanceID;
  private boolean mDeepLinking;
  @b(a="description")
  public String mDescription;
  @b(a="discount")
  public String mDiscount;
  private String mEventCategory = null;
  private String mEventCityName = null;
  private String mEventName = null;
  private String mEventProviderId = null;
  public String mGTMListName;
  public int mGTMPosition;
  public String mGiftCardUrl = null;
  @b(a="has_reward")
  public boolean mHasRewards;
  @b(a="image_url")
  public String mImageUrl;
  @b(a="img_height")
  public String mImgHeight;
  @b(a="img_width")
  public String mImgWidth;
  private boolean mIsFromSearch;
  @b(a="stock")
  public boolean mIsInStock;
  @b(a="id")
  private String mItemID;
  @b(a="label")
  private String mLabel;
  private String mListId = "";
  @b(a="login_required")
  public boolean mLoginRequired;
  @b(a="name")
  public String mName;
  @b(a="offer_price")
  private float mOfferPrice;
  @b(a="tag")
  private String mOfferTag;
  private String mOrigin;
  @b(a="parent_id")
  public String mParentId;
  private String mParentItem;
  private String mParkCategory = null;
  private String mParkCityName = null;
  private String mParkName = null;
  private String mParkProviderId = null;
  private String mParkcityLabel = null;
  private String mParkcityValue = null;
  @b(a="price")
  public String mPrice;
  @b(a="priority")
  private String mPripority;
  private String mPushAlertMessageBody = "";
  private String mPushCashAdd;
  private String mPushCheckInDate;
  private String mPushCheckOutDate;
  private String mPushCity;
  private String mPushCode;
  private String mPushComment;
  private String mPushDate;
  private String mPushDestinationCityName;
  private String mPushDestinationCityShortName;
  @b(a="featuretype")
  private String mPushFeatureType = "";
  private String mPushFlightClass;
  private String mPushFlightDepartDate;
  private String mPushFlightReturnDate;
  private String mPushFlightTripType;
  private String mPushHotelExtras;
  private String mPushHotelFinalPriceWithTax;
  private String mPushHotelId;
  private String mPushHotelName;
  private String mPushPassengerCount;
  private String mPushProductId;
  private String mPushPromoCode;
  private String mPushQuantity;
  private String mPushRecipient;
  private String mPushRoomDetailsJson;
  private boolean mPushShowPopup;
  private String mPushSourceCityName;
  private String mPushSourceCityShortName;
  private String mPushTitle;
  private String mPushType;
  private String mPushUtmSource;
  private String mPushWalletCode;
  private String mQueryString;
  @b(a="rating")
  public float mRating;
  private String mRechargeAmount;
  private String mRechargeNumber;
  private String mRechargePromoCode;
  public String mReferralNo;
  public String mReferralRechargeType;
  public String mReferralSource;
  public String mRefferralAmount;
  @b(a="related_category")
  private ArrayList<CJRRelatedCategory> mRelatedCategories = new ArrayList();
  @b(a="reward_text")
  public String mRewardText;
  private String mRoamingEnabled;
  private String mSearcKey;
  public String mSearchABValue;
  public String mSearchCategory;
  public String mSearchResultType;
  public String mSearchTerm;
  public String mSearchType;
  private String mSearchUrl;
  @b(a="source")
  private String mSource;
  @b(a="status")
  public String mStatus;
  private String mTitle = "";
  @b(a="tp_data")
  public CJRTPData mTpData;
  private String mTrainDepartureDate;
  private String mTrainDestinationCity;
  private String mTrainDestinationCityName;
  private String mTrainSourceCity;
  private String mTrainSourceCityName;
  @b(a="url")
  public String mUrl;
  @b(a="url_info")
  private String mUrlInfo;
  @b(a="url_type")
  public String mUrlType;
  private String mUtmCampaign;
  private String mUtmContent;
  private String mUtmMedium;
  private String mUtmTerm;
  @b(a="attributes")
  public CJRBrandAttributes mattributes;
  private String mode;
  @b(a="products")
  private ArrayList<CJRHomePageItem> mproducts = new ArrayList();
  @b(a="seourl")
  public String mseourl;
  private String overlay;
  private String p2pamount;
  private String p2pcomment;
  private String p2pmobilenumber;
  public String qrid;
  private String userName;
  
  public static long getSerialVersionUID()
  {
    return 1L;
  }
  
  public boolean equals(Object paramObject)
  {
    try
    {
      boolean bool = this.mItemID.equals(((CJRHomePageItem)paramObject).mItemID);
      return bool;
    }
    catch (Exception paramObject) {}
    return false;
  }
  
  public String getAadhar_number()
  {
    return this.aadhar_number;
  }
  
  public float getActualPrice()
  {
    return this.mActualPrice;
  }
  
  public String getBankAccountNumber()
  {
    return this.bankAccountNumber;
  }
  
  public String getBankAmount()
  {
    return this.bankAmount;
  }
  
  public String getBankComment()
  {
    return this.bankcomment;
  }
  
  public String getBankUserName()
  {
    return this.userName;
  }
  
  public String getBrand()
  {
    return this.mBrand;
  }
  
  public String getCategoryId()
  {
    return this.mCategoryId;
  }
  
  public String getDescription()
  {
    return this.mDescription;
  }
  
  public String getDiscountPercent()
  {
    return this.mDiscount;
  }
  
  public String getEventCategory()
  {
    return this.mEventCategory;
  }
  
  public String getEventCityName()
  {
    return this.mEventCityName;
  }
  
  public String getEventName()
  {
    return this.mEventName;
  }
  
  public String getEventProviderId()
  {
    return this.mEventProviderId;
  }
  
  public String getFormatedActualPrice()
  {
    if (this.mActualPrice > 0.0F) {
      return c.a(String.valueOf(this.mActualPrice));
    }
    return "Free";
  }
  
  public String getFormatedOfferPrice()
  {
    if (this.mOfferPrice > 0.0F) {
      return c.a(String.valueOf(this.mOfferPrice));
    }
    return "Free";
  }
  
  public String getGiftCardUrl()
  {
    return this.mGiftCardUrl;
  }
  
  public String getIfsc()
  {
    return this.ifsc;
  }
  
  public boolean getImageFromResource()
  {
    return this.imageFromResource;
  }
  
  public String getImageUrl()
  {
    return this.mImageUrl;
  }
  
  public boolean getIsInStock()
  {
    return this.mIsInStock;
  }
  
  public String getItemID()
  {
    return this.mItemID;
  }
  
  public String getKyc_name()
  {
    return this.kyc_name;
  }
  
  public String getLabel()
  {
    return this.mLabel;
  }
  
  public String getLanding_page()
  {
    return this.landing_page;
  }
  
  public CJRLayoutParams getLayoutParam()
  {
    return this.layoutParam;
  }
  
  public String getListId()
  {
    return this.mListId;
  }
  
  public String getListName()
  {
    return this.mGTMListName;
  }
  
  public int getListPosition()
  {
    return this.mGTMPosition;
  }
  
  public CJRBrandAttributes getMattributes()
  {
    return this.mattributes;
  }
  
  public String getMode()
  {
    return this.mode;
  }
  
  public ArrayList<CJRHomePageItem> getMproducts()
  {
    return this.mproducts;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public float getOfferPrice()
  {
    return this.mOfferPrice;
  }
  
  public String getOfferTag()
  {
    return this.mOfferTag;
  }
  
  public String getOrigin()
  {
    return this.mOrigin;
  }
  
  public String getOverlay()
  {
    return this.overlay;
  }
  
  public String getP2pamount()
  {
    return this.p2pamount;
  }
  
  public String getP2pcomment()
  {
    return this.p2pcomment;
  }
  
  public String getP2pmobilenumber()
  {
    return this.p2pmobilenumber;
  }
  
  public String getParentID()
  {
    return this.mParentId;
  }
  
  public String getParentId()
  {
    return this.mParentId;
  }
  
  public String getParentItem()
  {
    return this.mParentItem;
  }
  
  public String getParkcityLabel()
  {
    return this.mParkcityLabel;
  }
  
  public String getParkcityValue()
  {
    return this.mParkcityValue;
  }
  
  public String getPrice()
  {
    if ((this.mPrice != null) && (!this.mPrice.contains("Rs"))) {
      this.mPrice = ("Rs " + this.mPrice);
    }
    return this.mPrice;
  }
  
  public String getProductCategory()
  {
    return this.mCategory;
  }
  
  public String getProductImgHeight()
  {
    return this.mImgHeight;
  }
  
  public String getProductImgWidth()
  {
    return this.mImgWidth;
  }
  
  public String getPushCashAdd()
  {
    return this.mPushCashAdd;
  }
  
  public String getPushCheckInDate()
  {
    return this.mPushCheckInDate;
  }
  
  public String getPushCheckOutDate()
  {
    return this.mPushCheckOutDate;
  }
  
  public String getPushCity()
  {
    return this.mPushCity;
  }
  
  public String getPushCode()
  {
    return this.mPushCode;
  }
  
  public String getPushComment()
  {
    return this.mPushComment;
  }
  
  public String getPushDate()
  {
    return this.mPushDate;
  }
  
  public String getPushDestinationCityName()
  {
    return this.mPushDestinationCityName;
  }
  
  public String getPushDestinationCityShortName()
  {
    return this.mPushDestinationCityShortName;
  }
  
  public String getPushFeatureType()
  {
    return this.mPushFeatureType;
  }
  
  public String getPushFlightClass()
  {
    return this.mPushFlightClass;
  }
  
  public String getPushFlightDepartDate()
  {
    return this.mPushFlightDepartDate;
  }
  
  public String getPushFlightReturnDate()
  {
    return this.mPushFlightReturnDate;
  }
  
  public String getPushFlightTripType()
  {
    return this.mPushFlightTripType;
  }
  
  public String getPushHotelExtras()
  {
    return this.mPushHotelExtras;
  }
  
  public String getPushHotelFinalPriceWithTax()
  {
    return this.mPushHotelFinalPriceWithTax;
  }
  
  public String getPushHotelId()
  {
    return this.mPushHotelId;
  }
  
  public String getPushHotelName()
  {
    return this.mPushHotelName;
  }
  
  public String getPushMessageBody()
  {
    return this.mPushAlertMessageBody;
  }
  
  public String getPushPassengerCount()
  {
    return this.mPushPassengerCount;
  }
  
  public String getPushProductId()
  {
    return this.mPushProductId;
  }
  
  public String getPushPromoCode()
  {
    return this.mPushPromoCode;
  }
  
  public String getPushQuantity()
  {
    return this.mPushQuantity;
  }
  
  public String getPushRechargeAmount()
  {
    return this.mRechargeAmount;
  }
  
  public String getPushRechargeNumber()
  {
    return this.mRechargeNumber;
  }
  
  public String getPushRechargePromo()
  {
    return this.mRechargePromoCode;
  }
  
  public String getPushRechargeRoaming()
  {
    return this.mRoamingEnabled;
  }
  
  public String getPushRecipient()
  {
    return this.mPushRecipient;
  }
  
  public String getPushRoomDetailsJson()
  {
    return this.mPushRoomDetailsJson;
  }
  
  public String getPushSourceCityName()
  {
    return this.mPushSourceCityName;
  }
  
  public String getPushSourceCityShortName()
  {
    return this.mPushSourceCityShortName;
  }
  
  public String getPushTitle()
  {
    return this.mPushTitle;
  }
  
  public String getPushType()
  {
    return this.mPushType;
  }
  
  public String getPushWalletCode()
  {
    return this.mPushWalletCode;
  }
  
  public String getQrid()
  {
    return this.qrid;
  }
  
  public String getQueryString()
  {
    return this.mQueryString;
  }
  
  public float getRating()
  {
    return this.mRating;
  }
  
  public String getReferralAmount()
  {
    return this.mRefferralAmount;
  }
  
  public String getReferralNo()
  {
    return this.mReferralNo;
  }
  
  public String getReferralRechargeType()
  {
    return this.mReferralRechargeType;
  }
  
  public String getReferralSource()
  {
    return this.mReferralSource;
  }
  
  public ArrayList<CJRRelatedCategory> getRelatedCategories()
  {
    return this.mRelatedCategories;
  }
  
  public String getRewardText()
  {
    return this.mRewardText;
  }
  
  public String getSearchABValue()
  {
    return this.mSearchABValue;
  }
  
  public String getSearchCategory()
  {
    return this.mSearchCategory;
  }
  
  public String getSearchKey()
  {
    return this.mSearcKey;
  }
  
  public String getSearchResultType()
  {
    return this.mSearchResultType;
  }
  
  public String getSearchTerm()
  {
    return this.mSearchTerm;
  }
  
  public String getSearchType()
  {
    return this.mSearchType;
  }
  
  public String getSearchUrl()
  {
    return this.mSearchUrl;
  }
  
  public String getSource()
  {
    return this.mSource;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
  
  public CJRTPData getTpData()
  {
    return this.mTpData;
  }
  
  public String getTrainDepartureDate()
  {
    return this.mTrainDepartureDate;
  }
  
  public String getTrainDestinationCityCode()
  {
    return this.mTrainDestinationCity;
  }
  
  public String getTrainDestinationCityName()
  {
    return this.mTrainDestinationCityName;
  }
  
  public String getTrainSourceCityCode()
  {
    return this.mTrainSourceCity;
  }
  
  public String getTrainSourceCityName()
  {
    return this.mTrainSourceCityName;
  }
  
  public String getURL()
  {
    if (this.mUrl == null) {
      return this.mseourl;
    }
    return this.mUrl;
  }
  
  public String getURLType()
  {
    return this.mUrlType;
  }
  
  public String getUrlInfo()
  {
    return this.mUrlInfo;
  }
  
  public String getUtmCampaign()
  {
    return this.mUtmCampaign;
  }
  
  public String getUtmContent()
  {
    return this.mUtmContent;
  }
  
  public String getUtmMedium()
  {
    return this.mUtmMedium;
  }
  
  public String getUtmSource()
  {
    return this.mPushUtmSource;
  }
  
  public String getUtmTerm()
  {
    return this.mUtmTerm;
  }
  
  public String getmContainerInstanceID()
  {
    return this.mContainerInstanceID;
  }
  
  public String getmParkCategory()
  {
    return this.mParkCategory;
  }
  
  public String getmParkCityName()
  {
    return this.mParkCityName;
  }
  
  public String getmParkName()
  {
    return this.mParkName;
  }
  
  public String getmParkProviderId()
  {
    return this.mParkProviderId;
  }
  
  public String getmPripority()
  {
    return this.mPripority;
  }
  
  public boolean isDeepLinking()
  {
    return this.mDeepLinking;
  }
  
  public boolean isFromFromSearch()
  {
    return this.mIsFromSearch;
  }
  
  public boolean isFromReqDelivery()
  {
    return this.isFromReqDelivery;
  }
  
  public boolean isHasRewards()
  {
    return this.mHasRewards;
  }
  
  public boolean isItemViewed()
  {
    return this.itemViewed;
  }
  
  public boolean isLoginRequired()
  {
    return this.mLoginRequired;
  }
  
  public boolean isPushShowPopup()
  {
    return this.mPushShowPopup;
  }
  
  public boolean isSellerStoreImpressionSent()
  {
    return this.isSellerStoreImpressionSent;
  }
  
  public void setAadhar_number(String paramString)
  {
    this.aadhar_number = paramString;
  }
  
  public void setBankAccountNumber(String paramString)
  {
    this.bankAccountNumber = paramString;
  }
  
  public void setBankAmount(String paramString)
  {
    this.bankAmount = paramString;
  }
  
  public void setBankComment(String paramString)
  {
    this.bankcomment = paramString;
  }
  
  public void setBankUserName(String paramString)
  {
    this.userName = paramString;
  }
  
  public void setCategoryId(String paramString)
  {
    this.mCategoryId = paramString;
  }
  
  public void setDeepLinking(boolean paramBoolean)
  {
    this.mDeepLinking = paramBoolean;
  }
  
  public void setDiscountPercent(String paramString)
  {
    this.mDiscount = paramString;
  }
  
  public void setEventCategory(String paramString)
  {
    this.mEventCategory = paramString;
  }
  
  public void setEventCityName(String paramString)
  {
    this.mEventCityName = paramString;
  }
  
  public void setEventName(String paramString)
  {
    this.mEventName = paramString;
  }
  
  public void setEventProviderId(String paramString)
  {
    this.mEventProviderId = paramString;
  }
  
  public void setFromReqDelivery(boolean paramBoolean)
  {
    this.isFromReqDelivery = paramBoolean;
  }
  
  public void setGiftCardUrl(String paramString)
  {
    this.mGiftCardUrl = paramString;
  }
  
  public void setIfsc(String paramString)
  {
    this.ifsc = paramString;
  }
  
  public void setImageFromResource(boolean paramBoolean)
  {
    this.imageFromResource = paramBoolean;
  }
  
  public void setImageUrl(String paramString)
  {
    this.mImageUrl = paramString;
  }
  
  public void setIsFromSearch(boolean paramBoolean)
  {
    this.mIsFromSearch = paramBoolean;
  }
  
  public void setIsInStock(boolean paramBoolean)
  {
    this.mIsInStock = paramBoolean;
  }
  
  public void setItemID(String paramString)
  {
    this.mItemID = paramString;
  }
  
  public void setItemViewed()
  {
    this.itemViewed = true;
  }
  
  public void setKyc_name(String paramString)
  {
    this.kyc_name = paramString;
  }
  
  public void setLabel(String paramString)
  {
    this.mLabel = paramString;
  }
  
  public void setLanding_page(String paramString)
  {
    this.landing_page = paramString;
  }
  
  public void setListId(String paramString)
  {
    this.mListId = paramString;
  }
  
  public void setListName(String paramString)
  {
    this.mGTMListName = paramString;
  }
  
  public void setListPosition(int paramInt)
  {
    this.mGTMPosition = paramInt;
  }
  
  public void setMattributes(CJRBrandAttributes paramCJRBrandAttributes)
  {
    this.mattributes = paramCJRBrandAttributes;
  }
  
  public void setMode(String paramString)
  {
    this.mode = paramString;
  }
  
  public void setMproducts(ArrayList<CJRHomePageItem> paramArrayList)
  {
    this.mproducts = paramArrayList;
  }
  
  public void setName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setOrigin(String paramString)
  {
    this.mOrigin = paramString;
  }
  
  public void setOverlay(String paramString)
  {
    this.overlay = paramString;
  }
  
  public void setP2pamount(String paramString)
  {
    this.p2pamount = paramString;
  }
  
  public void setP2pcomment(String paramString)
  {
    this.p2pcomment = paramString;
  }
  
  public void setP2pmobilenumber(String paramString)
  {
    this.p2pmobilenumber = paramString;
  }
  
  public void setParentItem(String paramString)
  {
    this.mParentItem = paramString;
  }
  
  public void setParkcityValue(String paramString)
  {
    this.mParkcityValue = paramString;
  }
  
  public void setProductImgHeight(String paramString)
  {
    this.mImgHeight = paramString;
  }
  
  public void setProductImgWidth(String paramString)
  {
    this.mImgWidth = paramString;
  }
  
  public void setPushCashAdd(String paramString)
  {
    this.mPushCashAdd = paramString;
  }
  
  public void setPushCheckInDate(String paramString)
  {
    this.mPushCheckInDate = paramString;
  }
  
  public void setPushCheckOutDate(String paramString)
  {
    this.mPushCheckOutDate = paramString;
  }
  
  public void setPushCity(String paramString)
  {
    this.mPushCity = paramString;
  }
  
  public void setPushCode(String paramString)
  {
    this.mPushCode = paramString;
  }
  
  public void setPushComment(String paramString)
  {
    this.mPushComment = paramString;
  }
  
  public void setPushDate(String paramString)
  {
    this.mPushDate = paramString;
  }
  
  public void setPushDestinationCityName(String paramString)
  {
    this.mPushDestinationCityName = paramString;
  }
  
  public void setPushDestinationCityShortName(String paramString)
  {
    this.mPushDestinationCityShortName = paramString;
  }
  
  public void setPushFeatureType(String paramString)
  {
    this.mPushFeatureType = paramString;
  }
  
  public void setPushFlightClass(String paramString)
  {
    this.mPushFlightClass = paramString;
  }
  
  public void setPushFlightDepartDate(String paramString)
  {
    this.mPushFlightDepartDate = paramString;
  }
  
  public void setPushFlightReturnDate(String paramString)
  {
    this.mPushFlightReturnDate = paramString;
  }
  
  public void setPushFlightTripType(String paramString)
  {
    this.mPushFlightTripType = paramString;
  }
  
  public void setPushHotelExtras(String paramString)
  {
    this.mPushHotelExtras = paramString;
  }
  
  public void setPushHotelFinalPriceWithTax(String paramString)
  {
    this.mPushHotelFinalPriceWithTax = paramString;
  }
  
  public void setPushHotelId(String paramString)
  {
    this.mPushHotelId = paramString;
  }
  
  public void setPushHotelName(String paramString)
  {
    this.mPushHotelName = paramString;
  }
  
  public void setPushMessageBody(String paramString)
  {
    this.mPushAlertMessageBody = paramString;
  }
  
  public void setPushPassengerCount(String paramString)
  {
    this.mPushPassengerCount = paramString;
  }
  
  public void setPushProductId(String paramString)
  {
    this.mPushProductId = paramString;
  }
  
  public void setPushPromoCode(String paramString)
  {
    this.mPushPromoCode = paramString;
  }
  
  public void setPushQuantity(String paramString)
  {
    this.mPushQuantity = paramString;
  }
  
  public void setPushRechargeAmount(String paramString)
  {
    this.mRechargeAmount = paramString;
  }
  
  public void setPushRechargeNumber(String paramString)
  {
    this.mRechargeNumber = paramString;
  }
  
  public void setPushRechargePromo(String paramString)
  {
    this.mRechargePromoCode = paramString;
  }
  
  public void setPushRechargeRoaming(String paramString)
  {
    this.mRoamingEnabled = paramString;
  }
  
  public void setPushRecipient(String paramString)
  {
    this.mPushRecipient = paramString;
  }
  
  public void setPushRoomDetailsJson(String paramString)
  {
    this.mPushRoomDetailsJson = paramString;
  }
  
  public void setPushShowPopup(boolean paramBoolean)
  {
    this.mPushShowPopup = paramBoolean;
  }
  
  public void setPushSourceCityName(String paramString)
  {
    this.mPushSourceCityName = paramString;
  }
  
  public void setPushSourceCityShortName(String paramString)
  {
    this.mPushSourceCityShortName = paramString;
  }
  
  public void setPushTitle(String paramString)
  {
    this.mPushTitle = paramString;
  }
  
  public void setPushType(String paramString)
  {
    this.mPushType = paramString;
  }
  
  public void setPushUtmSource(String paramString)
  {
    this.mPushUtmSource = paramString;
  }
  
  public void setPushWalletCode(String paramString)
  {
    this.mPushWalletCode = paramString;
  }
  
  public void setQrid(String paramString)
  {
    this.qrid = paramString;
  }
  
  public void setQueryString(String paramString)
  {
    this.mQueryString = paramString;
  }
  
  public void setReferralAmount(String paramString)
  {
    this.mRefferralAmount = paramString;
  }
  
  public void setReferralNo(String paramString)
  {
    this.mReferralNo = paramString;
  }
  
  public void setReferralRechargeType(String paramString)
  {
    this.mReferralRechargeType = paramString;
  }
  
  public void setRelatedCategories(ArrayList<CJRRelatedCategory> paramArrayList)
  {
    this.mRelatedCategories = paramArrayList;
  }
  
  public void setSearcKey(String paramString)
  {
    this.mSearcKey = paramString;
  }
  
  public void setSearchABValue(String paramString)
  {
    this.mSearchABValue = paramString;
  }
  
  public void setSearchCategory(String paramString)
  {
    this.mSearchCategory = paramString;
  }
  
  public void setSearchResultType(String paramString)
  {
    this.mSearchResultType = paramString;
  }
  
  public void setSearchTerm(String paramString)
  {
    this.mSearchTerm = paramString;
  }
  
  public void setSearchType(String paramString)
  {
    this.mSearchType = paramString;
  }
  
  public void setSearchUrl(String paramString)
  {
    this.mSearchUrl = paramString;
  }
  
  public void setSellerStoreImpressionSent(boolean paramBoolean)
  {
    this.isSellerStoreImpressionSent = paramBoolean;
  }
  
  public void setSource(String paramString)
  {
    this.mSource = paramString;
  }
  
  public void setTitle(String paramString)
  {
    this.mTitle = paramString;
  }
  
  public void setTrainDepartureDate(String paramString)
  {
    this.mTrainDepartureDate = paramString;
  }
  
  public void setTrainDestinationCityCode(String paramString)
  {
    this.mTrainDestinationCity = paramString;
  }
  
  public void setTrainDestinationCityName(String paramString)
  {
    this.mTrainDestinationCityName = paramString;
  }
  
  public void setTrainSourceCityCode(String paramString)
  {
    this.mTrainSourceCity = paramString;
  }
  
  public void setTrainSourceCityName(String paramString)
  {
    this.mTrainSourceCityName = paramString;
  }
  
  public void setUrl(String paramString)
  {
    this.mUrl = paramString;
  }
  
  public void setUrlType(String paramString)
  {
    this.mUrlType = paramString;
  }
  
  public void setUtmCampaign(String paramString)
  {
    this.mUtmCampaign = paramString;
  }
  
  public void setUtmContent(String paramString)
  {
    this.mUtmContent = paramString;
  }
  
  public void setUtmMedium(String paramString)
  {
    this.mUtmMedium = paramString;
  }
  
  public void setUtmTerm(String paramString)
  {
    this.mUtmTerm = paramString;
  }
  
  public void setmContainerInstanceID(String paramString)
  {
    this.mContainerInstanceID = paramString;
  }
  
  public void setmParkCategory(String paramString)
  {
    this.mParkCategory = paramString;
  }
  
  public void setmParkCityName(String paramString)
  {
    this.mParkCityName = paramString;
  }
  
  public void setmParkName(String paramString)
  {
    this.mParkName = paramString;
  }
  
  public void setmParkProviderId(String paramString)
  {
    this.mParkProviderId = paramString;
  }
  
  public void setmParkcityLabel(String paramString)
  {
    this.mParkcityLabel = paramString;
  }
  
  public void setmReferralSource(String paramString)
  {
    this.mReferralSource = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRHomePageItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */