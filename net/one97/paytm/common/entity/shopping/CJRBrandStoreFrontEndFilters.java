package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrandStoreFrontEndFilters
  implements IJRDataModel
{
  @b(a="filter_param")
  private String mFilterParam;
  @b(a="title")
  private String mTitle;
  @b(a="type")
  private String mType;
  @b(a="values")
  private CJRBrandStoreCategoryValues mValues;
  
  public String getFilterParam()
  {
    return this.mFilterParam;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
  
  public String getType()
  {
    return this.mType;
  }
  
  public CJRBrandStoreCategoryValues getValues()
  {
    return this.mValues;
  }
  
  public void setFilterParam(String paramString)
  {
    this.mFilterParam = paramString;
  }
  
  public void setTitle(String paramString)
  {
    this.mTitle = paramString;
  }
  
  public void setType(String paramString)
  {
    this.mType = paramString;
  }
  
  public void setValues(CJRBrandStoreCategoryValues paramCJRBrandStoreCategoryValues)
  {
    this.mValues = paramCJRBrandStoreCategoryValues;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRBrandStoreFrontEndFilters.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */