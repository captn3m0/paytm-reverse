package net.one97.paytm.common.entity.shopping;

import android.text.TextUtils;
import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.CJRItem;

public class CJRCatalogItem
  extends CJRItem
{
  private static final long serialVersionUID = 1L;
  @b(a="brand")
  private String mBrand;
  @b(a="category_id")
  private String mCategoryId;
  @b(a="image_data")
  private String mImageData;
  @b(a="image_url")
  private String mImageIconUrl;
  private boolean mIsSelected;
  @b(a="label")
  private String mLabel;
  private String mListId = "";
  @b(a="name")
  private String mName;
  transient ArrayList<String> mParentName;
  @b(a="related_category")
  private ArrayList<CJRRelatedCategory> mRelatedCategories = new ArrayList();
  @b(a="seourl")
  private String mSeoUrl;
  @b(a="items")
  private ArrayList<CJRCatalogItem> mSubItems;
  @b(a="tag_label_color")
  private String mTagColor;
  @b(a="tag_label_name")
  private String mTagLabel;
  @b(a="url")
  private String mUrl;
  @b(a="url_type")
  private String mUrlType;
  
  public boolean equals(Object paramObject)
  {
    try
    {
      paramObject = (CJRCatalogItem)paramObject;
      boolean bool = this.mName.equalsIgnoreCase(((CJRCatalogItem)paramObject).getName());
      return bool;
    }
    catch (Exception paramObject) {}
    return false;
  }
  
  public String getBrand()
  {
    return this.mBrand;
  }
  
  public String getCategoryId()
  {
    return this.mCategoryId;
  }
  
  public String getImageData()
  {
    return this.mImageData;
  }
  
  public String getImageIconUrl()
  {
    return this.mImageIconUrl;
  }
  
  public String getItemID()
  {
    return null;
  }
  
  public String getLabel()
  {
    return this.mLabel;
  }
  
  public String getListId()
  {
    return this.mListId;
  }
  
  public String getListName()
  {
    return null;
  }
  
  public int getListPosition()
  {
    return -1;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getParentID()
  {
    return "";
  }
  
  public ArrayList<String> getParentName()
  {
    return this.mParentName;
  }
  
  public ArrayList<CJRRelatedCategory> getRelatedCategories()
  {
    return this.mRelatedCategories;
  }
  
  public String getSearchABValue()
  {
    return null;
  }
  
  public String getSearchCategory()
  {
    return null;
  }
  
  public String getSearchResultType()
  {
    return null;
  }
  
  public String getSearchTerm()
  {
    return null;
  }
  
  public String getSearchType()
  {
    return null;
  }
  
  public ArrayList<CJRCatalogItem> getSubItems()
  {
    return this.mSubItems;
  }
  
  public String getTagColor()
  {
    return this.mTagColor;
  }
  
  public String getTagLabel()
  {
    return this.mTagLabel;
  }
  
  public String getURL()
  {
    if ((this.mUrl != null) && (!TextUtils.isEmpty(this.mUrl))) {
      return this.mUrl;
    }
    return this.mSeoUrl;
  }
  
  public String getURLType()
  {
    return this.mUrlType;
  }
  
  public String getmContainerInstanceID()
  {
    return "";
  }
  
  public boolean isSelected()
  {
    return this.mIsSelected;
  }
  
  public void setBrand(String paramString)
  {
    this.mBrand = paramString;
  }
  
  public void setCategoryId(String paramString)
  {
    this.mCategoryId = paramString;
  }
  
  public void setImageData(String paramString)
  {
    this.mImageData = paramString;
  }
  
  public void setImageIconUrl(String paramString)
  {
    this.mImageIconUrl = paramString;
  }
  
  public void setLabel(String paramString)
  {
    this.mLabel = paramString;
  }
  
  public void setListId(String paramString)
  {
    this.mListId = paramString;
  }
  
  public void setName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setParentName(ArrayList<String> paramArrayList)
  {
    this.mParentName = paramArrayList;
  }
  
  public void setParentNameItems(ArrayList<String> paramArrayList)
  {
    this.mParentName = paramArrayList;
    if ((this.mSubItems != null) && (this.mSubItems.size() > 0))
    {
      int j = this.mSubItems.size();
      int i = 0;
      while (i < j)
      {
        paramArrayList = new ArrayList();
        paramArrayList.addAll(this.mParentName);
        paramArrayList.add(this.mName);
        ((CJRCatalogItem)this.mSubItems.get(i)).setParentNameItems(paramArrayList);
        i += 1;
      }
    }
  }
  
  public void setSelected(boolean paramBoolean)
  {
    this.mIsSelected = paramBoolean;
  }
  
  public void setTagLabel(String paramString)
  {
    this.mTagLabel = paramString;
  }
  
  public void setTagLabelColor(String paramString)
  {
    this.mTagColor = paramString;
  }
  
  public void setURLType(String paramString)
  {
    this.mUrlType = paramString;
  }
  
  public void setUrl(String paramString)
  {
    this.mUrl = paramString;
  }
  
  public void setmRelatedCategories(ArrayList<CJRRelatedCategory> paramArrayList)
  {
    this.mRelatedCategories = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRCatalogItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */