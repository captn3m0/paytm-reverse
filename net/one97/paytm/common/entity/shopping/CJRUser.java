package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;

public class CJRUser
{
  @b(a="experiment_id")
  private String experimentID;
  @b(a="ga_id")
  private int gaID;
  @b(a="user_id")
  private int userID;
  
  public String getExperimentID()
  {
    return this.experimentID;
  }
  
  public int getGaID()
  {
    return this.gaID;
  }
  
  public int getUserID()
  {
    return this.userID;
  }
  
  public void setExperimentID(String paramString)
  {
    this.experimentID = paramString;
  }
  
  public void setGaID(int paramInt)
  {
    this.gaID = paramInt;
  }
  
  public void setUserID(int paramInt)
  {
    this.userID = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRUser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */