package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.Iterator;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRCart
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="address")
  private CJRAddress mAddress;
  @b(a="aggregate_item_price")
  private String mAggregateItemPrice;
  @b(a="cart_id")
  private String mCartId;
  @b(a="cart_items")
  private ArrayList<CJRCartProduct> mCartItems;
  @b(a="conv_fee")
  private String mConvFee;
  @b(a="count")
  private String mCount;
  @b(a="customer_id")
  private String mCustomerId;
  private CJRAddress mDefaultAddress;
  @b(a="error")
  private String mError;
  @b(a="error_code")
  private String mErrorCode;
  @b(a="error_info")
  private String mErrorInfo;
  @b(a="error_title")
  private String mErrorTitle;
  @b(a="final_price")
  private String mFinalPrice;
  @b(a="final_price_excl_shipping")
  private String mFinalPriceExclShipping;
  @b(a="need_shipping")
  private boolean mNeedShipping;
  @b(a="order_total")
  private String mOrderTotal;
  @b(a="paytm_cashback")
  private String mPaytmCashBack;
  @b(a="paytm_discount")
  private String mPaytmDiscount;
  @b(a="paytm_promocode")
  private String mPaytmPromocode;
  @b(a="place_order_url")
  private String mPlaceOrderUrl;
  @b(a="promocode")
  private String mPromoCode;
  @b(a="promofailuretext")
  private String mPromoFailureText;
  @b(a="promostatus")
  private String mPromoStatus;
  @b(a="promotext")
  private String mPromoText;
  @b(a="service_options")
  private boolean mServiceOption;
  @b(a="shipping_charges")
  private String mShippingCharges;
  @b(a="tax_data")
  public CJRTaxData mTaxData;
  @b(a="total_other_taxes")
  ArrayList<CJROtherTaxes> totalOtherTaxes;
  
  public CJRAddress getAddress()
  {
    return this.mAddress;
  }
  
  public ArrayList<CJRCartProduct> getCartItems()
  {
    return this.mCartItems;
  }
  
  public ArrayList<CJRCartProduct> getCartItemsWithTrackingInfo()
  {
    Object localObject = null;
    if (this.mCartItems != null)
    {
      ArrayList localArrayList = new ArrayList();
      Iterator localIterator = this.mCartItems.iterator();
      for (;;)
      {
        localObject = localArrayList;
        if (!localIterator.hasNext()) {
          break;
        }
        localObject = (CJRCartProduct)localIterator.next();
        CJRCartProduct localCJRCartProduct = new CJRCartProduct();
        if (((CJRCartProduct)localObject).getProductId() != null) {
          localCJRCartProduct.setProductId(((CJRCartProduct)localObject).getProductId());
        }
        if (((CJRCartProduct)localObject).getTrackingInfo() != null) {
          localCJRCartProduct.setTrackingInfo(((CJRCartProduct)localObject).getTrackingInfo());
        }
        localArrayList.add(localCJRCartProduct);
      }
    }
    return (ArrayList<CJRCartProduct>)localObject;
  }
  
  public CJRCartProduct getCartProduct(String paramString)
  {
    Iterator localIterator = this.mCartItems.iterator();
    while (localIterator.hasNext())
    {
      CJRCartProduct localCJRCartProduct = (CJRCartProduct)localIterator.next();
      if ((localCJRCartProduct.getProductId() != null) && (localCJRCartProduct.getProductId().equals(paramString))) {
        return localCJRCartProduct;
      }
    }
    return null;
  }
  
  public String getConvFee()
  {
    return this.mConvFee;
  }
  
  public int getCount()
  {
    return Integer.parseInt(this.mCount);
  }
  
  public String getCustomerId()
  {
    return this.mCustomerId;
  }
  
  public String getError()
  {
    return this.mError;
  }
  
  public String getErrorCode()
  {
    return this.mErrorCode;
  }
  
  public String getErrorInfo()
  {
    return this.mErrorInfo;
  }
  
  public String getErrorTitle()
  {
    return this.mErrorTitle;
  }
  
  public String getFinalPrice()
  {
    return this.mFinalPrice;
  }
  
  public String getFinalPriceExclShipping()
  {
    return this.mFinalPriceExclShipping;
  }
  
  public boolean getNeedShipping()
  {
    return this.mNeedShipping;
  }
  
  public String getOrderTotal()
  {
    return this.mOrderTotal;
  }
  
  public String getPaytmCashBack()
  {
    return this.mPaytmCashBack;
  }
  
  public String getPaytmDiscount()
  {
    return this.mPaytmDiscount;
  }
  
  public String getPaytmPromocode()
  {
    return this.mPaytmPromocode;
  }
  
  public String getPlaceOrderUrl()
  {
    return this.mPlaceOrderUrl;
  }
  
  public String getPromoCode()
  {
    return this.mPromoCode;
  }
  
  public String getPromoFailureText()
  {
    return this.mPromoFailureText;
  }
  
  public String getPromoStatus()
  {
    return this.mPromoStatus;
  }
  
  public String getPromoText()
  {
    return this.mPromoText;
  }
  
  public String getShippingCharges()
  {
    return this.mShippingCharges;
  }
  
  public CJRTaxData getTaxData()
  {
    return this.mTaxData;
  }
  
  public ArrayList<CJROtherTaxes> getTotalOtherTaxes()
  {
    return this.totalOtherTaxes;
  }
  
  public String getmAggregateItemPrice()
  {
    return this.mAggregateItemPrice;
  }
  
  public String getmCartId()
  {
    return this.mCartId;
  }
  
  public CJRAddress getmDefaultAddress()
  {
    return this.mDefaultAddress;
  }
  
  public boolean isServiceOption()
  {
    return this.mServiceOption;
  }
  
  public void setErrorCode(String paramString)
  {
    this.mErrorCode = paramString;
  }
  
  public void setmDefaultAddress(CJRAddress paramCJRAddress)
  {
    this.mDefaultAddress = paramCJRAddress;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRCart.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */