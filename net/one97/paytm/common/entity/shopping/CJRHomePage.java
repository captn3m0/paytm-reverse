package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.Iterator;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRHomePage
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="alt_image_url")
  private String alt_image_url;
  @b(a="name")
  private String banner_name;
  @b(a="description")
  private String description;
  @b(a="entity_associated_with")
  private String entityAssociatedWith;
  @b(a="image_url")
  private String image_url;
  public boolean isOrderDetailsExpanded;
  @b(a="error")
  private String mErrorMsg;
  @b(a="footer_image_url")
  private String mFooterImage;
  @b(a="ga_key")
  public String mGAKey;
  private String mGATitle;
  @b(a="homepage_layout")
  public ArrayList<CJRHomePageLayout> mHomePageLayoutList = new ArrayList();
  @b(a="id")
  private String mID = "";
  @b(a="mobile_layout")
  public ArrayList<CJRHomePageLayout> mMobileLayout = new ArrayList();
  @b(a="old_category_id")
  private String mOldCategoryId;
  @b(a="placeholder_image_url")
  public String mPlaceHolderImageUrl;
  @b(a="url_key")
  private String urlKey;
  
  public String getAlt_image_url()
  {
    return this.alt_image_url;
  }
  
  public String getBannerName()
  {
    return this.banner_name;
  }
  
  public String getDescription()
  {
    return this.description;
  }
  
  public String getEntityAssociatedWith()
  {
    return this.entityAssociatedWith;
  }
  
  public String getErrorMsg()
  {
    return this.mErrorMsg;
  }
  
  public String getFooterImage()
  {
    return this.mFooterImage;
  }
  
  public String getGAKey()
  {
    return this.mGAKey;
  }
  
  public String getGATitle()
  {
    return this.mGATitle;
  }
  
  public ArrayList<CJRHomePageLayout> getHomePageLayoutList()
  {
    return this.mHomePageLayoutList;
  }
  
  public String getID()
  {
    return this.mID;
  }
  
  public String getImage_url()
  {
    return this.image_url;
  }
  
  public ArrayList<CJRHomePageItem> getItemsForLayout(String paramString)
  {
    Object localObject2 = null;
    Iterator localIterator = this.mHomePageLayoutList.iterator();
    do
    {
      localObject1 = localObject2;
      if (!localIterator.hasNext()) {
        break;
      }
      localObject1 = (CJRHomePageLayout)localIterator.next();
    } while (!((CJRHomePageLayout)localObject1).getLayout().equalsIgnoreCase(paramString));
    Object localObject1 = ((CJRHomePageLayout)localObject1).getHomePageItemList();
    return (ArrayList<CJRHomePageItem>)localObject1;
  }
  
  public ArrayList<CJRHomePageItem> getItemsForProduct(String paramString)
  {
    Object localObject2 = null;
    Iterator localIterator = this.mHomePageLayoutList.iterator();
    do
    {
      localObject1 = localObject2;
      if (!localIterator.hasNext()) {
        break;
      }
      localObject1 = (CJRHomePageLayout)localIterator.next();
    } while (!((CJRHomePageLayout)localObject1).getName().equalsIgnoreCase(paramString));
    Object localObject1 = ((CJRHomePageLayout)localObject1).getHomePageItemList();
    return (ArrayList<CJRHomePageItem>)localObject1;
  }
  
  public CJRHomePageLayout getLayoutFromItem(CJRHomePageItem paramCJRHomePageItem)
  {
    try
    {
      Iterator localIterator = this.mHomePageLayoutList.iterator();
      while (localIterator.hasNext())
      {
        CJRHomePageLayout localCJRHomePageLayout = (CJRHomePageLayout)localIterator.next();
        Object localObject = localCJRHomePageLayout.getHomePageItemList();
        if ((localObject != null) && (paramCJRHomePageItem != null) && (((ArrayList)localObject).contains(paramCJRHomePageItem)))
        {
          localObject = (CJRHomePageItem)((ArrayList)localObject).get(((ArrayList)localObject).indexOf(paramCJRHomePageItem));
          if ((localObject != null) && (((CJRHomePageItem)localObject).getURLType() != null) && (paramCJRHomePageItem.getURLType() != null))
          {
            boolean bool = ((CJRHomePageItem)localObject).getURLType().equalsIgnoreCase(paramCJRHomePageItem.getURLType());
            if (bool) {
              return localCJRHomePageLayout;
            }
          }
        }
      }
    }
    catch (Exception paramCJRHomePageItem)
    {
      paramCJRHomePageItem.printStackTrace();
    }
    return null;
  }
  
  public ArrayList<CJRHomePageLayout> getMobileLayoutList()
  {
    return this.mMobileLayout;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getOldCategoryId()
  {
    return this.mOldCategoryId;
  }
  
  public ArrayList<CJRHomePageItem> getParentListForItem(String paramString, CJRHomePageItem paramCJRHomePageItem)
  {
    Object localObject1 = null;
    Iterator localIterator = this.mHomePageLayoutList.iterator();
    Object localObject2;
    do
    {
      do
      {
        paramString = (String)localObject1;
        if (!localIterator.hasNext()) {
          break;
        }
        paramString = (CJRHomePageLayout)localIterator.next();
        localObject2 = paramString.getHomePageItemList();
      } while (!((ArrayList)localObject2).contains(paramCJRHomePageItem));
      localObject2 = (CJRHomePageItem)((ArrayList)localObject2).get(((ArrayList)localObject2).indexOf(paramCJRHomePageItem));
    } while ((localObject2 == null) || (((CJRHomePageItem)localObject2).getParentItem() == null) || (paramCJRHomePageItem.getParentItem() == null) || (!((CJRHomePageItem)localObject2).getParentItem().equalsIgnoreCase(paramCJRHomePageItem.getParentItem())));
    paramString = paramString.getHomePageItemList();
    return paramString;
  }
  
  public String getPlaceHolderImageUrl()
  {
    return this.mPlaceHolderImageUrl;
  }
  
  public ArrayList<CJRHomePageLayout> getPromotionImpression()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mHomePageLayoutList.iterator();
    while (localIterator.hasNext())
    {
      CJRHomePageLayout localCJRHomePageLayout = (CJRHomePageLayout)localIterator.next();
      LayoutType localLayoutType = LayoutType.fromName(localCJRHomePageLayout.getLayout());
      switch (CJRHomePage.1.$SwitchMap$net$one97$paytm$common$entity$shopping$LayoutType[localLayoutType.ordinal()])
      {
      case 7: 
      case 8: 
      case 9: 
      case 10: 
      case 11: 
      default: 
        break;
      case 3: 
      case 4: 
      case 5: 
      case 6: 
      case 12: 
        if (localCJRHomePageLayout.getHomePageItemList().size() > 0) {
          localArrayList.add(localCJRHomePageLayout);
        }
        break;
      }
    }
    return localArrayList;
  }
  
  public String getUrlKey()
  {
    return this.urlKey;
  }
  
  public boolean isOrderDetailsExpanded()
  {
    return this.isOrderDetailsExpanded;
  }
  
  public ArrayList<CJRHomePageLayout> pageRowItems(boolean paramBoolean)
  {
    ArrayList localArrayList = new ArrayList();
    CJRHomePageLayout localCJRHomePageLayout;
    if (paramBoolean)
    {
      localIterator = this.mMobileLayout.iterator();
      while (localIterator.hasNext())
      {
        localCJRHomePageLayout = (CJRHomePageLayout)localIterator.next();
        if ((localCJRHomePageLayout.getLayout().equalsIgnoreCase("tabs")) && (localCJRHomePageLayout.getHomePageItemList().size() > 0)) {
          localArrayList.add(localCJRHomePageLayout);
        }
      }
    }
    Iterator localIterator = this.mHomePageLayoutList.iterator();
    while (localIterator.hasNext())
    {
      localCJRHomePageLayout = (CJRHomePageLayout)localIterator.next();
      if (localCJRHomePageLayout.getLayout() != null)
      {
        LayoutType localLayoutType = LayoutType.fromName(localCJRHomePageLayout.getLayout());
        switch (CJRHomePage.1.$SwitchMap$net$one97$paytm$common$entity$shopping$LayoutType[localLayoutType.ordinal()])
        {
        default: 
          break;
        case 1: 
        case 2: 
        case 3: 
        case 4: 
        case 5: 
        case 6: 
        case 7: 
        case 8: 
        case 9: 
        case 10: 
        case 11: 
          if (localCJRHomePageLayout.getHomePageItemList().size() > 0) {
            localArrayList.add(localCJRHomePageLayout);
          }
          break;
        case 12: 
        case 13: 
        case 14: 
        case 15: 
        case 16: 
        case 17: 
        case 18: 
        case 19: 
        case 20: 
        case 21: 
        case 22: 
        case 23: 
        case 24: 
          localArrayList.add(localCJRHomePageLayout);
        }
      }
    }
    return localArrayList;
  }
  
  public void setEntityAssociatedWith(String paramString)
  {
    this.entityAssociatedWith = paramString;
  }
  
  public void setFooterImageUrl(String paramString)
  {
    this.mFooterImage = paramString;
  }
  
  public void setGATitle(String paramString)
  {
    this.mGATitle = paramString;
  }
  
  public void setHomePageLayoutList(ArrayList<CJRHomePageLayout> paramArrayList)
  {
    this.mHomePageLayoutList = paramArrayList;
  }
  
  public void setIsOrderDetailsExpanded(boolean paramBoolean)
  {
    this.isOrderDetailsExpanded = paramBoolean;
  }
  
  public void setmHomePageLayoutList(ArrayList<CJRHomePageLayout> paramArrayList)
  {
    this.mHomePageLayoutList = paramArrayList;
  }
  
  public void setmHomePageLayoutList(CJRHomePageLayout paramCJRHomePageLayout)
  {
    this.mHomePageLayoutList.add(paramCJRHomePageLayout);
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRHomePage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */