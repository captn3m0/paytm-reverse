package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.Map;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRLongRichDesc
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="attributes")
  private Map<String, String> mAttributes;
  @b(a="description")
  private String mDescription;
  @b(a="show_on_tap")
  private String mShowOnTap;
  @b(a="title")
  private String mTitle;
  
  public String getShowOnTap()
  {
    return this.mShowOnTap;
  }
  
  public Map<String, String> getmAttributes()
  {
    return this.mAttributes;
  }
  
  public String getmDescription()
  {
    return this.mDescription;
  }
  
  public String getmTitle()
  {
    return this.mTitle;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRLongRichDesc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */