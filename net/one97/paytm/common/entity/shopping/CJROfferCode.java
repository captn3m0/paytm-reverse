package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.Date;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROfferCode
  implements IJRDataModel
{
  private boolean isOfferApplied;
  @b(a="code")
  private String mCode;
  @b(a="effective_price")
  private double mEffectivePrice;
  @b(a="offerText")
  private String mOfferText;
  @b(a="savings")
  private String mSavings;
  @b(a="terms")
  private String mTerms;
  @b(a="terms_title")
  private String mTermsTitle;
  @b(a="valid_upto")
  private String mValidTill;
  private Date mValidUpTo;
  
  public String getCode()
  {
    return this.mCode;
  }
  
  public double getEffectivePrice()
  {
    return this.mEffectivePrice;
  }
  
  public String getOfferText()
  {
    return this.mOfferText;
  }
  
  public String getSavingsPrice()
  {
    return this.mSavings;
  }
  
  public String getTerms()
  {
    return this.mTerms;
  }
  
  public String getTermsTitle()
  {
    return this.mTermsTitle;
  }
  
  public Date getValidUpTo()
  {
    return this.mValidUpTo;
  }
  
  public String getmValidTill()
  {
    return this.mValidTill;
  }
  
  public void setIsOfferApplied(boolean paramBoolean)
  {
    this.isOfferApplied = paramBoolean;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROfferCode.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */