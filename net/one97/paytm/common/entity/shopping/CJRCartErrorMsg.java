package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRCartErrorMsg
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="content")
  private CJRCartErrorMsg mContent;
  @b(a="description")
  private CJRCartErrorMsg mDescription;
  @b(a="result")
  private CJRCartErrorMsg mResult;
  @b(a="type")
  private String mType;
  
  public CJRCartErrorMsg getmContent()
  {
    return this.mContent;
  }
  
  public CJRCartErrorMsg getmDescription()
  {
    return this.mDescription;
  }
  
  public CJRCartErrorMsg getmResult()
  {
    return this.mResult;
  }
  
  public String getmType()
  {
    return this.mType;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRCartErrorMsg.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */