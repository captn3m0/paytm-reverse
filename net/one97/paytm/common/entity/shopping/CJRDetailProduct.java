package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import net.one97.paytm.common.entity.CJRBadage;
import net.one97.paytm.common.entity.CJRFilterItem;
import net.one97.paytm.common.entity.CJRFilterValue;
import net.one97.paytm.common.entity.CJRProduct;
import net.one97.paytm.common.entity.CJRStatus;

public class CJRDetailProduct
  extends CJRProduct
{
  private static final String ESTIMATED_ARRIVAL = "Estimated Arrival";
  private static final String SHIPPING_DETAIL = "Shipping Details";
  private static final long serialVersionUID = 1L;
  @b(a="convenience_fee")
  private String convenienceFee;
  @b(a="bargainable")
  private boolean isBargainable;
  @b(a="bargained")
  private String isBargained;
  @b(a="action_id")
  private String mActionId;
  @b(a="action_text")
  private String mActionText;
  @b(a="actual_price")
  private String mActualPrice;
  @b(a="add_to_cart_url")
  private String mAddToCartUrl;
  @b(a="ancestors")
  private ArrayList<CJRAncestor> mAncestors;
  @b(a="attribute_chart")
  private Map<String, CJRFilterValue> mAttributeChart;
  @b(a="bargain_name")
  private String mBargainName;
  @b(a="brand_id")
  private String mBrandId;
  @b(a="brand_seller_url")
  private String mBrandSellerUrl;
  @b(a="brandstore_info")
  private CJRBrandStoreInfo mBrandStoreInfo;
  @b(a="bullet_points")
  private CJRBulletPoints mBulletPoints;
  @b(a="cart_price")
  private String mCartPrice;
  @b(a="cart_text")
  private String mCartText;
  @b(a="category")
  private String mCategory;
  @b(a="category_ids")
  private ArrayList<String> mCategoryIds;
  @b(a="configurations")
  private CJRConfigurationNew mConfiguration;
  @b(a="configuration_caption")
  private String mConfigurationCaption;
  @b(a="configuration_name")
  private String mConfigurationName;
  @b(a="delete_url")
  private String mDeleteUrl;
  @b(a="discount")
  private String mDiscount;
  @b(a="offer_price")
  private String mDiscountedPrice;
  @b(a="error")
  private String mError;
  @b(a="filters")
  private ArrayList<CJRFilterItem> mFilterList = new ArrayList();
  @b(a="footer_image_url")
  private String mFooterImageUrl;
  @b(a="ga_key")
  private String mGAKey;
  @b(a="image_data")
  private String mImageData;
  @b(a="image_url")
  private String mImageUrl;
  @b(a="instock")
  private boolean mInStock;
  @b(a="sequential_dimension")
  private boolean mIsSequential;
  @b(a="long_rich_desc")
  private ArrayList<CJRLongRichDesc> mLongRichDesc;
  @b(a="merchant")
  private CJRMerchant mMerchant;
  @b(a="need_shipping")
  private boolean mNeedShipping;
  @b(a="offer_url")
  private String mOfferUrl;
  @b(a="other_images")
  private ArrayList<String> mOtherMedia;
  @b(a="other_rich_desc")
  private String mOtherRichDesc;
  @b(a="other_sellers")
  private CJROtherSellersMain mOtherSellers;
  @b(a="parent_id")
  private String mParentID;
  @b(a="pincode")
  private String mPinCode;
  @b(a="product_id")
  private String mProductID;
  @b(a="product_rating")
  private String mProductRating;
  @b(a="product_sku_id")
  private String mProductSkuID;
  @b(a="product_url")
  private String mProductUrl;
  @b(a="promo_text")
  private String mPromoText;
  @b(a="promocode_url")
  private String mPromocodeUrl;
  @b(a="quantity")
  private int mQuantity;
  @b(a="selectable_attributes")
  private ArrayList<CJRSelectableAttributes> mSelectableAttributes;
  @b(a="selected_attributes")
  private ArrayList<CJRSelectedAttributes> mSelectedAttributes;
  @b(a="selected_configuration")
  private int mSelectedConfiguration;
  @b(a="shareurl")
  private String mShareUrl;
  @b(a="shipping_cost")
  private String mShippingCost;
  private String mShortDesc;
  @b(a="list")
  private ArrayList<CJRHomePageLayout> mSimilarProductList;
  @b(a="terms_conditions")
  private CJRStatus mTermsAndConditions;
  @b(a="title")
  private String mTitle;
  @b(a="update_quantity_url")
  private String mUpdateQuantityUrl;
  @b(a="url")
  private String mUrl;
  @b(a="vertical_id")
  private String mVerticalID;
  @b(a="vertical_label")
  private String mVerticalLabel;
  @b(a="listing_tag")
  private ArrayList<CJRBadage> mlisting_tag = new ArrayList();
  private boolean show;
  
  public String getAllAncestors()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if ((this.mAncestors != null) && (this.mAncestors.size() > 1))
    {
      int j = this.mAncestors.size();
      int i = 0;
      while (i < j - 1)
      {
        localStringBuilder.append(((CJRAncestor)this.mAncestors.get(i)).getName());
        if ((j > 2) && (i < j - 2)) {
          localStringBuilder.append("/");
        }
        i += 1;
      }
    }
    return localStringBuilder.toString();
  }
  
  public String getAllAncestorsId()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if ((this.mAncestors != null) && (this.mAncestors.size() > 1))
    {
      int j = this.mAncestors.size();
      int i = 0;
      while (i < j - 1)
      {
        localStringBuilder.append(((CJRAncestor)this.mAncestors.get(i)).getAncestorID());
        if ((j > 2) && (i < j - 2)) {
          localStringBuilder.append("/");
        }
        i += 1;
      }
    }
    return localStringBuilder.toString();
  }
  
  public ArrayList<CJRAncestor> getAncestors()
  {
    return this.mAncestors;
  }
  
  public Map<String, CJRFilterValue> getAttributeChart()
  {
    return this.mAttributeChart;
  }
  
  public String getBargainName()
  {
    return this.mBargainName;
  }
  
  public String getBrand()
  {
    return this.mBrand;
  }
  
  public String getBrandId()
  {
    return this.mBrandId;
  }
  
  public String getCategory()
  {
    return this.mCategory;
  }
  
  public ArrayList<String> getCategoryIds()
  {
    return this.mCategoryIds;
  }
  
  public String getConfigurationName()
  {
    return this.mConfigurationName;
  }
  
  public CJRConfigurationNew getConfigurations()
  {
    return this.mConfiguration;
  }
  
  public String getConvenienceFee()
  {
    return this.convenienceFee;
  }
  
  public String getDiscount()
  {
    return this.mDiscount;
  }
  
  public String getError()
  {
    return this.mError;
  }
  
  public ArrayList<CJRFilterItem> getFilterList()
  {
    return this.mFilterList;
  }
  
  public String getFooterImageUrl()
  {
    return this.mFooterImageUrl;
  }
  
  public String getGAKey()
  {
    return this.mGAKey;
  }
  
  public String getImageUrl()
  {
    return this.mImageUrl;
  }
  
  public String getImmidiateAncestor()
  {
    Object localObject2 = null;
    Object localObject1 = localObject2;
    if (this.mAncestors.size() - 2 >= 0)
    {
      CJRAncestor localCJRAncestor = (CJRAncestor)this.mAncestors.get(this.mAncestors.size() - 2);
      localObject1 = localObject2;
      if (localCJRAncestor != null) {
        localObject1 = localCJRAncestor.getName();
      }
    }
    return (String)localObject1;
  }
  
  public boolean getIsBargainable()
  {
    return this.isBargainable;
  }
  
  public String getIsBargained()
  {
    return this.isBargained;
  }
  
  public ArrayList<CJRBadage> getMlisting_tag()
  {
    return this.mlisting_tag;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getProductType()
  {
    return this.mProductType;
  }
  
  public int getSelectedConfiguration()
  {
    return this.mSelectedConfiguration;
  }
  
  public String getShareUrl()
  {
    return this.mShareUrl;
  }
  
  public String getShippingDetail()
  {
    Iterator localIterator = this.mLongRichDesc.iterator();
    while (localIterator.hasNext())
    {
      Object localObject = (CJRLongRichDesc)localIterator.next();
      if ("Shipping Details".equalsIgnoreCase(((CJRLongRichDesc)localObject).getmTitle()))
      {
        localObject = ((CJRLongRichDesc)localObject).getmAttributes();
        if (((Map)localObject).containsKey("Estimated Arrival")) {
          return (String)((Map)localObject).get("Estimated Arrival");
        }
      }
    }
    return null;
  }
  
  public ArrayList<CJRHomePageLayout> getSimilarProductList()
  {
    return this.mSimilarProductList;
  }
  
  public CJRStatus getTermsAndConditions()
  {
    return this.mTermsAndConditions;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
  
  public String getVerticalLabel()
  {
    return this.mVerticalLabel;
  }
  
  public String getmActionId()
  {
    return this.mActionId;
  }
  
  public String getmActionText()
  {
    return this.mActionText;
  }
  
  public String getmActualPrice()
  {
    return this.mActualPrice;
  }
  
  public String getmAddToCartUrl()
  {
    return this.mAddToCartUrl;
  }
  
  public String getmBrandSellerUrl()
  {
    return this.mBrandSellerUrl;
  }
  
  public CJRBrandStoreInfo getmBrandStoreInfo()
  {
    return this.mBrandStoreInfo;
  }
  
  public CJRBulletPoints getmBulletPoints()
  {
    return this.mBulletPoints;
  }
  
  public String getmCartPrice()
  {
    return this.mCartPrice;
  }
  
  public String getmCartText()
  {
    return this.mCartText;
  }
  
  public String getmConfigurationCaption()
  {
    return this.mConfigurationCaption;
  }
  
  public String getmDeleteUrl()
  {
    return this.mDeleteUrl;
  }
  
  public String getmDiscountedPrice()
  {
    return this.mDiscountedPrice;
  }
  
  public String getmImageData()
  {
    return this.mImageData;
  }
  
  public ArrayList<CJRLongRichDesc> getmLongRichDesc()
  {
    return this.mLongRichDesc;
  }
  
  public CJRMerchant getmMerchant()
  {
    return this.mMerchant;
  }
  
  public String getmOfferUrl()
  {
    return this.mOfferUrl;
  }
  
  public ArrayList<String> getmOtherMedia()
  {
    return this.mOtherMedia;
  }
  
  public String getmOtherRichDesc()
  {
    return this.mOtherRichDesc;
  }
  
  public CJROtherSellersMain getmOtherSellers()
  {
    return this.mOtherSellers;
  }
  
  public String getmPinCode()
  {
    return this.mPinCode;
  }
  
  public String getmProductID()
  {
    return this.mProductID;
  }
  
  public String getmProductRating()
  {
    return this.mProductRating;
  }
  
  public String getmProductSkuID()
  {
    return this.mProductSkuID;
  }
  
  public String getmProductUrl()
  {
    return this.mProductUrl;
  }
  
  public String getmPromoText()
  {
    return this.mPromoText;
  }
  
  public String getmPromocodeUrl()
  {
    return this.mPromocodeUrl;
  }
  
  public int getmQuantity()
  {
    return this.mQuantity;
  }
  
  public ArrayList<CJRSelectableAttributes> getmSelectableAttributes()
  {
    return this.mSelectableAttributes;
  }
  
  public ArrayList<CJRSelectedAttributes> getmSelectedAttributes()
  {
    return this.mSelectedAttributes;
  }
  
  public String getmShippingCost()
  {
    return this.mShippingCost;
  }
  
  public String getmShortDesc()
  {
    return this.mShortDesc;
  }
  
  public String getmUpdateQuantityUrl()
  {
    return this.mUpdateQuantityUrl;
  }
  
  public String getmUrl()
  {
    return this.mUrl;
  }
  
  public String getmVerticalID()
  {
    return this.mVerticalID;
  }
  
  public String getparentID()
  {
    return this.mParentID;
  }
  
  public boolean isInStock()
  {
    return this.mInStock;
  }
  
  public boolean isShippingNeeded()
  {
    return this.mNeedShipping;
  }
  
  public boolean ismIsSequential()
  {
    return this.mIsSequential;
  }
  
  public void setBrand(String paramString)
  {
    this.mBrand = paramString;
  }
  
  public void setCategoryIds(ArrayList<String> paramArrayList)
  {
    this.mCategoryIds = paramArrayList;
  }
  
  public void setConvenienceFee(String paramString)
  {
    this.convenienceFee = paramString;
  }
  
  public void setMlisting_tag(ArrayList<CJRBadage> paramArrayList)
  {
    this.mlisting_tag = paramArrayList;
  }
  
  public void setmBrandStoreInfo(CJRBrandStoreInfo paramCJRBrandStoreInfo)
  {
    this.mBrandStoreInfo = paramCJRBrandStoreInfo;
  }
  
  public void setmBulletPoints(CJRBulletPoints paramCJRBulletPoints)
  {
    this.mBulletPoints = paramCJRBulletPoints;
  }
  
  public void setmOtherSellers(CJROtherSellersMain paramCJROtherSellersMain)
  {
    this.mOtherSellers = paramCJROtherSellersMain;
  }
  
  public void setmVerticalID(String paramString)
  {
    this.mVerticalID = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRDetailProduct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */