package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROffers
  implements IJRDataModel
{
  @b(a="codes")
  private ArrayList<CJROfferCode> mOfferCodes;
  
  public ArrayList<CJROfferCode> getOfferCodes()
  {
    return this.mOfferCodes;
  }
  
  public void setOfferCodes(ArrayList<CJROfferCode> paramArrayList)
  {
    this.mOfferCodes = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROffers.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */