package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTermsAndConditions
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="cancelButton")
  private String mCancelButton;
  @b(a="message")
  private String mMessage;
  @b(a="okbutton")
  private String mOkButton;
  @b(a="title")
  private String mTitle;
  
  public String getCancelButton()
  {
    return this.mCancelButton;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getOkButton()
  {
    return this.mOkButton;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRTermsAndConditions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */