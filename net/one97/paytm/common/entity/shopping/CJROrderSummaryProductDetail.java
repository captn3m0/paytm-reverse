package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROrderSummaryProductDetail
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  public double discountedPrice;
  @b(a="brand")
  private String mBrandName;
  @b(a="category_id")
  private long mCategoryId;
  @b(a="categoryMap")
  private ArrayList<CJRCategoryMap> mCategoryMap;
  @b(a="category_path")
  private String mCategoryPath;
  @b(a="ga_key")
  private String mGAkey;
  @b(a="id")
  private long mId;
  @b(a="image")
  private String mImageUrl;
  @b(a="merchant")
  private CJROrderSummaryMerchant mMercahnt;
  @b(a="merchant_name")
  private String mMerchantName;
  @b(a="name")
  private String mName;
  @b(a="need_shipping")
  private String mNeedShipping;
  @b(a="parent_id")
  public String mParentId;
  @b(a="price")
  private double mPrice;
  @b(a="product_type")
  private String mProductType;
  @b(a="promo_text")
  private String mPromoText;
  @b(a="thumbnail")
  private String mThumnail;
  @b(a="thumbnail_base64")
  private String mThumnailBase64;
  @b(a="url")
  private String mURL;
  @b(a="vertical_label")
  private String mVertical;
  @b(a="vertical_id")
  private long mVerticalId;
  
  public String getBrandName()
  {
    return this.mBrandName;
  }
  
  public long getCategoryId()
  {
    return this.mCategoryId;
  }
  
  public String getCategoryIdMapPath()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if ((this.mCategoryMap != null) && (this.mCategoryMap.size() > 0))
    {
      int j = this.mCategoryMap.size();
      int i = 0;
      while (i < j)
      {
        localStringBuilder.append(((CJRCategoryMap)this.mCategoryMap.get(i)).getCategoryId());
        if ((j > 1) && (i < j - 1)) {
          localStringBuilder.append("/");
        }
        i += 1;
      }
    }
    return localStringBuilder.toString();
  }
  
  public ArrayList<CJRCategoryMap> getCategoryMap()
  {
    return this.mCategoryMap;
  }
  
  public String getCategoryMapPath()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if ((this.mCategoryMap != null) && (this.mCategoryMap.size() > 0))
    {
      int j = this.mCategoryMap.size();
      int i = 0;
      while (i < j)
      {
        localStringBuilder.append(((CJRCategoryMap)this.mCategoryMap.get(i)).getName());
        if ((j > 1) && (i < j - 1)) {
          localStringBuilder.append("/");
        }
        i += 1;
      }
    }
    return localStringBuilder.toString();
  }
  
  public String getCategoryPath()
  {
    return this.mCategoryPath;
  }
  
  public double getDiscountedPrice()
  {
    return this.discountedPrice;
  }
  
  public String getGAkey()
  {
    return this.mGAkey;
  }
  
  public long getId()
  {
    return this.mId;
  }
  
  public String getImageUrl()
  {
    return this.mImageUrl;
  }
  
  public CJROrderSummaryMerchant getMercahnt()
  {
    return this.mMercahnt;
  }
  
  public String getMerchantName()
  {
    return this.mMerchantName;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getNeedShipping()
  {
    return this.mNeedShipping;
  }
  
  public String getParentId()
  {
    return this.mParentId;
  }
  
  public double getPrice()
  {
    return this.mPrice;
  }
  
  public String getProductType()
  {
    return this.mProductType;
  }
  
  public String getPromoText()
  {
    return this.mPromoText;
  }
  
  public String getThumbnail()
  {
    return this.mThumnail;
  }
  
  public String getThumnailBase64()
  {
    return this.mThumnailBase64;
  }
  
  public String getURL()
  {
    return this.mURL;
  }
  
  public String getVertical()
  {
    return this.mVertical;
  }
  
  public long getVerticalId()
  {
    return this.mVerticalId;
  }
  
  public void setDiscountedPrice(double paramDouble)
  {
    this.discountedPrice = paramDouble;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROrderSummaryProductDetail.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */