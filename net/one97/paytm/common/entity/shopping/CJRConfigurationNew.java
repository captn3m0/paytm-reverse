package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRConfigurationNew
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="caption")
  private String mCaption;
  @b(a="products")
  private ArrayList<CJRDetailProduct> mProductList;
  
  public String getCaption()
  {
    return this.mCaption;
  }
  
  public ArrayList<CJRDetailProduct> getProductList()
  {
    return this.mProductList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRConfigurationNew.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */