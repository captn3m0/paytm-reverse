package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROrderItemProduct
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="need_shipping")
  private String mNeedShipping;
  @b(a="id")
  private long mProductId;
  @b(a="thumbnail")
  private String mThumbnail;
  @b(a="vertical_id")
  private long mVerticalId;
  @b(a="vertical_label")
  private String mVerticalLabel;
  
  public String getNeedShipping()
  {
    return this.mNeedShipping;
  }
  
  public long getProductId()
  {
    return this.mProductId;
  }
  
  public String getThumbnail()
  {
    return this.mThumbnail;
  }
  
  public long getVerticalId()
  {
    return this.mVerticalId;
  }
  
  public String getVerticalLabel()
  {
    return this.mVerticalLabel;
  }
  
  public void setNeedShipping(String paramString)
  {
    this.mNeedShipping = paramString;
  }
  
  public void setmVerticalLabel(String paramString)
  {
    this.mVerticalLabel = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROrderItemProduct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */