package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrandStoreResultArray
  implements IJRDataModel
{
  @b(a="auth_level")
  private String mAuthLevel;
  @b(a="brand_id")
  private String mBrandId;
  @b(a="category_id")
  private String mCategoryId;
  @b(a="merchant_id")
  private String mMerchantId;
  @b(a="status")
  private String mStatus;
  
  public String getAuthLevel()
  {
    return this.mAuthLevel;
  }
  
  public String getBrandId()
  {
    return this.mBrandId;
  }
  
  public String getCategoryId()
  {
    return this.mCategoryId;
  }
  
  public String getMerchantId()
  {
    return this.mMerchantId;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public void setAuthLevel(String paramString)
  {
    this.mAuthLevel = paramString;
  }
  
  public void setBrandId(String paramString)
  {
    this.mBrandId = paramString;
  }
  
  public void setCategoryId(String paramString)
  {
    this.mCategoryId = paramString;
  }
  
  public void setMerchantId(String paramString)
  {
    this.mMerchantId = paramString;
  }
  
  public void setStatus(String paramString)
  {
    this.mStatus = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRBrandStoreResultArray.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */