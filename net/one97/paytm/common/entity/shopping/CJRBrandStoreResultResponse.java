package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrandStoreResultResponse
  implements IJRDataModel
{
  @b(a="result")
  private ArrayList<CJRBrandStoreResultArray> mBrandResultResponse;
  
  public ArrayList<CJRBrandStoreResultArray> getBrandResultResponse()
  {
    return this.mBrandResultResponse;
  }
  
  public void setBrandResultResponse(ArrayList<CJRBrandStoreResultArray> paramArrayList)
  {
    this.mBrandResultResponse = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRBrandStoreResultResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */