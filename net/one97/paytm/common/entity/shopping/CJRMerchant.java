package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.HashMap;
import net.one97.paytm.common.entity.CJRSellerStoreInfo;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRMerchant
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="address")
  private HashMap<String, String> mMerchantAdress;
  @b(a="merchant_id")
  private String mMerchantID;
  @b(a="merchant_image")
  private String mMerchantImage;
  @b(a="merchant_name")
  private String mMerchantName;
  @b(a="merchant_rating")
  private MerchantRating mMerchantRating;
  @b(a="stores")
  private ArrayList<CJRStores> mStoreList;
  @b(a="sellerstore_info")
  private CJRSellerStoreInfo sellerStroreInfo;
  
  public String getMerchantID()
  {
    return this.mMerchantID;
  }
  
  public String getMerchantImage()
  {
    return this.mMerchantImage;
  }
  
  public String getMerchantName()
  {
    return this.mMerchantName;
  }
  
  public MerchantRating getMerchantRating()
  {
    return this.mMerchantRating;
  }
  
  public CJRSellerStoreInfo getSellerStroreInfo()
  {
    return this.sellerStroreInfo;
  }
  
  public ArrayList<CJRStores> getStoreList()
  {
    return this.mStoreList;
  }
  
  public HashMap<String, String> getmMerchantAdress()
  {
    return this.mMerchantAdress;
  }
  
  public ArrayList<CJRStores> getmStoreList()
  {
    return this.mStoreList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRMerchant.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */