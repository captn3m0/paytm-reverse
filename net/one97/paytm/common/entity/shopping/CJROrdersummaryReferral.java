package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROrdersummaryReferral
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="offerUrl")
  private String mOfferUrl;
  @b(a="title")
  private String mTitle;
  @b(a="uiControl")
  private String mUiControl;
  @b(a="url")
  private String mUrl;
  
  public String getOfferUrl()
  {
    return this.mOfferUrl;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
  
  public String getUiControl()
  {
    return this.mUiControl;
  }
  
  public String getUrl()
  {
    return this.mUrl;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROrdersummaryReferral.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */