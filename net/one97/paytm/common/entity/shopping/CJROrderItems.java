package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROrderItems
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  private boolean isOrderSelected;
  private boolean isSendToBankEnabled;
  @b(a="id")
  private long mId;
  @b(a="item_status")
  private String mItemStatus;
  @b(a="name")
  private String mName;
  @b(a="price")
  private String mPrice;
  @b(a="product")
  private CJROrderItemProduct mProduct;
  @b(a="quantity")
  private String mQuantity;
  @b(a="repeat")
  private boolean mRepeat;
  @b(a="retry")
  private boolean mRetry;
  @b(a="status")
  private String mStatus;
  @b(a="status_text")
  private String mStatusText;
  @b(a="subtotal")
  private String mSubTotal;
  @b(a="total_price")
  private String mTotalPrice;
  
  public long getId()
  {
    return this.mId;
  }
  
  public boolean getIsRepeat()
  {
    return this.mRepeat;
  }
  
  public boolean getIsRetry()
  {
    return this.mRetry;
  }
  
  public String getItemStatus()
  {
    return this.mItemStatus;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getPrice()
  {
    return this.mPrice;
  }
  
  public CJROrderItemProduct getProduct()
  {
    return this.mProduct;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public String getStatusText()
  {
    return this.mStatusText;
  }
  
  public String getSubTotal()
  {
    return this.mSubTotal;
  }
  
  public String getTotalPrice()
  {
    return this.mTotalPrice;
  }
  
  public boolean isOrderSelected()
  {
    return this.isOrderSelected;
  }
  
  public boolean isSendToBankEnabled()
  {
    return this.isSendToBankEnabled;
  }
  
  public void setId(long paramLong)
  {
    this.mId = paramLong;
  }
  
  public void setName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setOrderSelected(boolean paramBoolean)
  {
    this.isOrderSelected = paramBoolean;
  }
  
  public void setProduct(CJROrderItemProduct paramCJROrderItemProduct)
  {
    this.mProduct = paramCJROrderItemProduct;
  }
  
  public void setSendToBankEnabled(boolean paramBoolean)
  {
    this.isSendToBankEnabled = paramBoolean;
  }
  
  public void setStatusText(String paramString)
  {
    this.mStatusText = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROrderItems.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */