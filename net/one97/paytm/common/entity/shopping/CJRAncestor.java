package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRAncestor
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="id")
  private String mAncestorID;
  @b(a="name")
  private String mAncestorName;
  @b(a="url_key")
  private String mURLKey;
  
  public String getAncestorID()
  {
    return this.mAncestorID;
  }
  
  public String getName()
  {
    return this.mAncestorName;
  }
  
  public String getURLKey()
  {
    return this.mURLKey;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRAncestor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */