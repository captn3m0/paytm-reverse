package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.Iterator;

public class CJRHomePageSlotItemV2
{
  @b(a="views")
  public ArrayList<CJRHomePageLayoutV2> mHomePageLayoutList = new ArrayList();
  @b(a="slots")
  private ArrayList<CJRHomePageSlotItemV2> mHomePageSlotItemList = new ArrayList();
  @b(a="id")
  private String mId;
  @b(a="layout")
  private String mLayout;
  @b(a="layout_details")
  private CJRLayoutDetailV2 mLayoutDetails;
  @b(a="link")
  private String mLink;
  @b(a="mobile_layout")
  public ArrayList<CJRHomePageLayoutV2> mMobileLayout = new ArrayList();
  @b(a="name")
  private String mName;
  @b(a="attributes")
  private CJRLayoutDetailV2 mattribute;
  
  public ArrayList<CJRHomePageLayoutV2> getHomePageLayoutList()
  {
    return this.mHomePageLayoutList;
  }
  
  public ArrayList<CJRHomePageItem> getItemsForLayout(String paramString)
  {
    Object localObject2 = null;
    Iterator localIterator = this.mHomePageLayoutList.iterator();
    do
    {
      localObject1 = localObject2;
      if (!localIterator.hasNext()) {
        break;
      }
      localObject1 = (CJRHomePageLayoutV2)localIterator.next();
    } while (!((CJRHomePageLayoutV2)localObject1).getLayout().equalsIgnoreCase(paramString));
    Object localObject1 = ((CJRHomePageLayoutV2)localObject1).getHomePageItemList();
    return (ArrayList<CJRHomePageItem>)localObject1;
  }
  
  public ArrayList<CJRHomePageItem> getItemsForProduct(String paramString)
  {
    Object localObject2 = null;
    Iterator localIterator = this.mHomePageLayoutList.iterator();
    do
    {
      localObject1 = localObject2;
      if (!localIterator.hasNext()) {
        break;
      }
      localObject1 = (CJRHomePageLayoutV2)localIterator.next();
    } while (!((CJRHomePageLayoutV2)localObject1).getName().equalsIgnoreCase(paramString));
    Object localObject1 = ((CJRHomePageLayoutV2)localObject1).getHomePageItemList();
    return (ArrayList<CJRHomePageItem>)localObject1;
  }
  
  public CJRHomePageLayoutV2 getLayoutFromItem(CJRHomePageItem paramCJRHomePageItem)
  {
    try
    {
      Iterator localIterator = this.mHomePageLayoutList.iterator();
      while (localIterator.hasNext())
      {
        CJRHomePageLayoutV2 localCJRHomePageLayoutV2 = (CJRHomePageLayoutV2)localIterator.next();
        Object localObject = localCJRHomePageLayoutV2.getHomePageItemList();
        if ((localObject != null) && (paramCJRHomePageItem != null) && (((ArrayList)localObject).contains(paramCJRHomePageItem)))
        {
          localObject = (CJRHomePageItem)((ArrayList)localObject).get(((ArrayList)localObject).indexOf(paramCJRHomePageItem));
          if ((localObject != null) && (((CJRHomePageItem)localObject).getURLType() != null) && (paramCJRHomePageItem.getURLType() != null))
          {
            boolean bool = ((CJRHomePageItem)localObject).getURLType().equalsIgnoreCase(paramCJRHomePageItem.getURLType());
            if (bool) {
              return localCJRHomePageLayoutV2;
            }
          }
        }
      }
    }
    catch (Exception paramCJRHomePageItem)
    {
      paramCJRHomePageItem.printStackTrace();
    }
    return null;
  }
  
  public CJRLayoutDetailV2 getMattribute()
  {
    return this.mattribute;
  }
  
  public ArrayList<CJRHomePageItem> getParentListForItem(String paramString, CJRHomePageItem paramCJRHomePageItem)
  {
    Object localObject1 = null;
    Iterator localIterator = this.mHomePageLayoutList.iterator();
    Object localObject2;
    do
    {
      do
      {
        paramString = (String)localObject1;
        if (!localIterator.hasNext()) {
          break;
        }
        paramString = (CJRHomePageLayoutV2)localIterator.next();
        localObject2 = paramString.getHomePageItemList();
      } while (!((ArrayList)localObject2).contains(paramCJRHomePageItem));
      localObject2 = (CJRHomePageItem)((ArrayList)localObject2).get(((ArrayList)localObject2).indexOf(paramCJRHomePageItem));
    } while ((localObject2 == null) || (((CJRHomePageItem)localObject2).getParentItem() == null) || (paramCJRHomePageItem.getParentItem() == null) || (!((CJRHomePageItem)localObject2).getParentItem().equalsIgnoreCase(paramCJRHomePageItem.getParentItem())));
    paramString = paramString.getHomePageItemList();
    return paramString;
  }
  
  public ArrayList<CJRHomePageLayoutV2> getPromotionImpression()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mHomePageLayoutList.iterator();
    while (localIterator.hasNext())
    {
      CJRHomePageLayoutV2 localCJRHomePageLayoutV2 = (CJRHomePageLayoutV2)localIterator.next();
      LayoutType localLayoutType = LayoutType.fromName(localCJRHomePageLayoutV2.getLayout());
      switch (CJRHomePageSlotItemV2.1.$SwitchMap$net$one97$paytm$common$entity$shopping$LayoutType[localLayoutType.ordinal()])
      {
      case 9: 
      case 10: 
      case 11: 
      case 12: 
      case 14: 
      default: 
        break;
      case 4: 
      case 5: 
      case 6: 
      case 7: 
      case 8: 
      case 13: 
      case 15: 
        if (localCJRHomePageLayoutV2.getHomePageItemList().size() > 0) {
          localArrayList.add(localCJRHomePageLayoutV2);
        }
        break;
      }
    }
    return localArrayList;
  }
  
  public ArrayList<CJRHomePageSlotItemV2> getmHomePageSlotItemList()
  {
    return this.mHomePageSlotItemList;
  }
  
  public String getmId()
  {
    return this.mId;
  }
  
  public String getmLayout()
  {
    return this.mLayout;
  }
  
  public CJRLayoutDetailV2 getmLayoutDetails()
  {
    return this.mLayoutDetails;
  }
  
  public String getmLink()
  {
    return this.mLink;
  }
  
  public ArrayList<CJRHomePageLayoutV2> getmMobileLayout()
  {
    return this.mMobileLayout;
  }
  
  public String getmName()
  {
    return this.mName;
  }
  
  public ArrayList<CJRHomePageLayoutV2> pageRowItems(boolean paramBoolean)
  {
    ArrayList localArrayList = new ArrayList();
    CJRHomePageLayoutV2 localCJRHomePageLayoutV2;
    if (paramBoolean)
    {
      localIterator = this.mMobileLayout.iterator();
      while (localIterator.hasNext())
      {
        localCJRHomePageLayoutV2 = (CJRHomePageLayoutV2)localIterator.next();
        if ((localCJRHomePageLayoutV2.getLayout().equalsIgnoreCase("tabs")) && (localCJRHomePageLayoutV2.getHomePageItemList().size() > 0)) {
          localArrayList.add(localCJRHomePageLayoutV2);
        }
      }
    }
    Iterator localIterator = this.mHomePageLayoutList.iterator();
    while (localIterator.hasNext())
    {
      localCJRHomePageLayoutV2 = (CJRHomePageLayoutV2)localIterator.next();
      if (localCJRHomePageLayoutV2.getLayout() != null)
      {
        Object localObject = LayoutType.fromName(localCJRHomePageLayoutV2.getLayout());
        switch (CJRHomePageSlotItemV2.1.$SwitchMap$net$one97$paytm$common$entity$shopping$LayoutType[localObject.ordinal()])
        {
        default: 
          break;
        case 1: 
        case 2: 
          if (localCJRHomePageLayoutV2.getHomePageItemList().size() > 0)
          {
            localObject = (CJRHomePageItem)localCJRHomePageLayoutV2.getHomePageItemList().get(0);
            if ((localObject != null) && (((CJRHomePageItem)localObject).getMproducts() != null) && (((CJRHomePageItem)localObject).getMproducts().size() > 0)) {
              localArrayList.add(localCJRHomePageLayoutV2);
            }
          }
          break;
        case 3: 
        case 4: 
        case 5: 
        case 6: 
        case 7: 
        case 8: 
        case 9: 
        case 10: 
        case 11: 
        case 12: 
        case 13: 
        case 14: 
          if (localCJRHomePageLayoutV2.getHomePageItemList().size() > 0) {
            localArrayList.add(localCJRHomePageLayoutV2);
          }
          break;
        case 15: 
        case 16: 
        case 17: 
        case 18: 
        case 19: 
        case 20: 
        case 21: 
        case 22: 
        case 23: 
        case 24: 
        case 25: 
        case 26: 
        case 27: 
          localArrayList.add(localCJRHomePageLayoutV2);
        }
      }
    }
    return localArrayList;
  }
  
  public void setHomePageLayoutList(ArrayList<CJRHomePageLayoutV2> paramArrayList)
  {
    this.mHomePageLayoutList = paramArrayList;
  }
  
  public void setMattribute(CJRLayoutDetailV2 paramCJRLayoutDetailV2)
  {
    this.mattribute = paramCJRLayoutDetailV2;
  }
  
  public void setmHomePageLayoutList(ArrayList<CJRHomePageLayoutV2> paramArrayList)
  {
    this.mHomePageLayoutList = paramArrayList;
  }
  
  public void setmHomePageLayoutList(CJRHomePageLayoutV2 paramCJRHomePageLayoutV2)
  {
    this.mHomePageLayoutList.add(paramCJRHomePageLayoutV2);
  }
  
  public void setmHomePageSlotItemList(ArrayList<CJRHomePageSlotItemV2> paramArrayList)
  {
    this.mHomePageSlotItemList = paramArrayList;
  }
  
  public void setmId(String paramString)
  {
    this.mId = paramString;
  }
  
  public void setmLayout(String paramString)
  {
    this.mLayout = paramString;
  }
  
  public void setmLayoutDetails(CJRLayoutDetailV2 paramCJRLayoutDetailV2)
  {
    this.mLayoutDetails = paramCJRLayoutDetailV2;
  }
  
  public void setmLink(String paramString)
  {
    this.mLink = paramString;
  }
  
  public void setmMobileLayout(ArrayList<CJRHomePageLayoutV2> paramArrayList)
  {
    this.mMobileLayout = paramArrayList;
  }
  
  public void setmName(String paramString)
  {
    this.mName = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRHomePageSlotItemV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */