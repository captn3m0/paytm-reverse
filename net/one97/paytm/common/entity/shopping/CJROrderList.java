package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;
import net.one97.paytm.common.entity.wallet.CJRLedger;

public class CJROrderList
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="amount")
  private String mAmount;
  @b(a="date")
  private String mDate;
  @b(a="item_count")
  private int mItemCount;
  private CJRLedger mLedger;
  @b(a="order_detail_url")
  private String mOrderDetailUrl;
  @b(a="order_id")
  private String mOrderID;
  @b(a="items")
  private ArrayList<CJROrderItems> mOrderItems;
  @b(a="order_name")
  private String mOrderName;
  @b(a="payment_status")
  private String mPaymentStatus;
  @b(a="refund")
  private CJROrderSummaryRefundToBank mRefundToBank;
  @b(a="status")
  private String mStatus;
  @b(a="status_icon")
  private String mStatusIcon;
  private int orderItemSelected = 0;
  
  public String getAmount()
  {
    return this.mAmount;
  }
  
  public String getDate()
  {
    return this.mDate;
  }
  
  public int getItemCount()
  {
    return this.mItemCount;
  }
  
  public CJRLedger getLedger()
  {
    return this.mLedger;
  }
  
  public String getOrderID()
  {
    return this.mOrderID;
  }
  
  public int getOrderItemSelected()
  {
    return this.orderItemSelected;
  }
  
  public ArrayList<CJROrderItems> getOrderItems()
  {
    return this.mOrderItems;
  }
  
  public String getOrderName()
  {
    return this.mOrderName;
  }
  
  public String getPaymentStatus()
  {
    return this.mPaymentStatus;
  }
  
  public CJROrderSummaryRefundToBank getRefund()
  {
    return this.mRefundToBank;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public String getStatusIcon()
  {
    return this.mStatusIcon;
  }
  
  public String getmOrderDetailUrl()
  {
    return this.mOrderDetailUrl;
  }
  
  public void setAmount(String paramString)
  {
    this.mAmount = paramString;
  }
  
  public void setDate(String paramString)
  {
    this.mDate = paramString;
  }
  
  public void setLedger(CJRLedger paramCJRLedger)
  {
    this.mLedger = paramCJRLedger;
  }
  
  public void setOrderID(String paramString)
  {
    this.mOrderID = paramString;
  }
  
  public void setOrderItemSelected(int paramInt)
  {
    this.orderItemSelected = paramInt;
  }
  
  public void setOrderItems(ArrayList<CJROrderItems> paramArrayList)
  {
    this.mOrderItems = paramArrayList;
  }
  
  public void setOrderName(String paramString)
  {
    this.mOrderName = paramString;
  }
  
  public void setStatus(String paramString)
  {
    this.mStatus = paramString;
  }
  
  public void setmOrderDetailUrl(String paramString)
  {
    this.mOrderDetailUrl = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROrderList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */