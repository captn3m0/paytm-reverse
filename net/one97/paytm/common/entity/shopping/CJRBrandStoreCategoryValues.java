package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrandStoreCategoryValues
  implements IJRDataModel
{
  @b(a="count")
  private String mCategoryCount;
  @b(a="id")
  private String mCategoryId;
  @b(a="name")
  private String mCategoryName;
  private boolean mIsCategoryAndIsLeaf;
  private boolean mIsClicked;
  private boolean mIsDeleted;
  private boolean mIsExpanded;
  private boolean mIsLastLeafNode;
  private boolean mIsLastSelected;
  private boolean mIsLeafNode;
  private boolean mIsMainCategory;
  private boolean mIsSubCategory;
  private int mLevel;
  private CJRBrandStoreCategoryValues mParent;
  @b(a="cats")
  private ArrayList<CJRBrandStoreCategoryValues> mValues;
  
  public String getCategoryCount()
  {
    return this.mCategoryCount;
  }
  
  public String getCategoryId()
  {
    return this.mCategoryId;
  }
  
  public String getCategoryName()
  {
    return this.mCategoryName;
  }
  
  public int getLevel()
  {
    return this.mLevel;
  }
  
  public CJRBrandStoreCategoryValues getParent()
  {
    return this.mParent;
  }
  
  public ArrayList<CJRBrandStoreCategoryValues> getValues()
  {
    return this.mValues;
  }
  
  public boolean isCategoryAndIsLeaf()
  {
    return this.mIsCategoryAndIsLeaf;
  }
  
  public boolean isClicked()
  {
    return this.mIsClicked;
  }
  
  public boolean isDeleted()
  {
    return this.mIsDeleted;
  }
  
  public boolean isExpanded()
  {
    return this.mIsExpanded;
  }
  
  public boolean isLastLeafNode()
  {
    return this.mIsLastLeafNode;
  }
  
  public boolean isLastSelected()
  {
    return this.mIsLastSelected;
  }
  
  public boolean isLeafNode()
  {
    return this.mIsLeafNode;
  }
  
  public boolean isMainCategory()
  {
    return this.mIsMainCategory;
  }
  
  public boolean isSubCategory()
  {
    return this.mIsSubCategory;
  }
  
  public void setCategoryCount(String paramString)
  {
    this.mCategoryCount = paramString;
  }
  
  public void setCategoryId(String paramString)
  {
    this.mCategoryId = paramString;
  }
  
  public void setCategoryName(String paramString)
  {
    this.mCategoryName = paramString;
  }
  
  public void setIsCategoryAndIsLeaf(boolean paramBoolean)
  {
    this.mIsCategoryAndIsLeaf = paramBoolean;
  }
  
  public void setIsClicked(boolean paramBoolean)
  {
    this.mIsClicked = paramBoolean;
  }
  
  public void setIsDeleted(boolean paramBoolean)
  {
    this.mIsDeleted = paramBoolean;
  }
  
  public void setIsExpanded(boolean paramBoolean)
  {
    this.mIsExpanded = paramBoolean;
  }
  
  public void setIsLastLeafNode(boolean paramBoolean)
  {
    this.mIsLastLeafNode = paramBoolean;
  }
  
  public void setIsLastSelected(boolean paramBoolean)
  {
    this.mIsLastSelected = paramBoolean;
  }
  
  public void setIsLeafNode(boolean paramBoolean)
  {
    this.mIsLeafNode = paramBoolean;
  }
  
  public void setIsMainCategory(boolean paramBoolean)
  {
    this.mIsMainCategory = paramBoolean;
  }
  
  public void setIsSubCategory(boolean paramBoolean)
  {
    this.mIsSubCategory = paramBoolean;
  }
  
  public void setLevel(int paramInt)
  {
    this.mLevel = paramInt;
  }
  
  public void setParent(CJRBrandStoreCategoryValues paramCJRBrandStoreCategoryValues)
  {
    this.mParent = paramCJRBrandStoreCategoryValues;
  }
  
  public void setValues(ArrayList<CJRBrandStoreCategoryValues> paramArrayList)
  {
    this.mValues = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRBrandStoreCategoryValues.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */