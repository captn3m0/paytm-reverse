package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRSearchPageItem
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="id")
  public String mId;
  @b(a="searchUrl")
  public String mSearchUrl;
  @b(a="text")
  public String mText;
  @b(a="seourl")
  public String mUrl;
  
  public String getId()
  {
    return this.mId;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getSearchUrl()
  {
    return this.mSearchUrl;
  }
  
  public String getText()
  {
    return this.mText;
  }
  
  public String getUrl()
  {
    return this.mUrl;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRSearchPageItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */