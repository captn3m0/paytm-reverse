package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROtherSellersMain
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="values")
  private ArrayList<CJROtherSellers> mOtherSellersList = new ArrayList();
  
  public ArrayList<CJROtherSellers> getmOtherSellers()
  {
    return this.mOtherSellersList;
  }
  
  public void setmOtherSellers(ArrayList<CJROtherSellers> paramArrayList)
  {
    this.mOtherSellersList = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROtherSellersMain.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */