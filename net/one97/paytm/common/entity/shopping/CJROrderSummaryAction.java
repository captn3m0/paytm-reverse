package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROrderSummaryAction
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="confirmation")
  private String mConfirmation;
  @b(a="label")
  private String mLabel;
  @b(a="message")
  private String mMessage;
  @b(a="name")
  private String mName;
  @b(a="ui_control")
  private String mUiControl;
  @b(a="urlParams")
  private CJROrderSummaryActionURLParams mUrlParams;
  
  public String getActionName()
  {
    return this.mName;
  }
  
  public String getConfirmation()
  {
    return this.mConfirmation;
  }
  
  public String getLabel()
  {
    return this.mLabel;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getUiControl()
  {
    return this.mUiControl;
  }
  
  public CJROrderSummaryActionURLParams getUrlParams()
  {
    return this.mUrlParams;
  }
  
  public void setActionName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setLabel(String paramString)
  {
    this.mLabel = paramString;
  }
  
  public void setMessage(String paramString)
  {
    this.mMessage = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROrderSummaryAction.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */