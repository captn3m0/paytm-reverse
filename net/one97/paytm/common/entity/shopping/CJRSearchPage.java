package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRSearchPage
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="app_display_count")
  public int mDisplayCount;
  @b(a="keywords")
  public ArrayList<CJRSearchLayout> mKeyWordsLayoutList = new ArrayList();
  public ArrayList<CJRSearchLayout> mProductsLayout = new ArrayList();
  @b(a="s")
  public String mQueryString;
  @b(a="search_user_id")
  private String mSearchUserId;
  
  public int getDisplayCount()
  {
    return this.mDisplayCount;
  }
  
  public ArrayList<CJRSearchLayout> getKeyWordsLayoutList()
  {
    return this.mKeyWordsLayoutList;
  }
  
  public String getName()
  {
    return null;
  }
  
  public ArrayList<CJRSearchLayout> getProductsLayoutList()
  {
    return this.mProductsLayout;
  }
  
  public String getQueryString()
  {
    return this.mQueryString;
  }
  
  public String getSearchUserId()
  {
    return this.mSearchUserId;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRSearchPage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */