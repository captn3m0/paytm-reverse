package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBulletPoints
  implements IJRDataModel
{
  @b(a="salient_feature")
  private ArrayList<String> mSalientFeatures = new ArrayList();
  
  public ArrayList<String> getmSalientFeatures()
  {
    return this.mSalientFeatures;
  }
  
  public void setmSalientFeatures(ArrayList<String> paramArrayList)
  {
    this.mSalientFeatures = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRBulletPoints.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */