package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRCartError
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="message")
  private CJRCartErrorMsg mMessage;
  @b(a="title")
  private String mTitle;
  
  public CJRCartErrorMsg getmMessage()
  {
    return this.mMessage;
  }
  
  public String getmTitle()
  {
    return this.mTitle;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRCartError.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */