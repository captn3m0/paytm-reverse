package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPendingPushMessage
  implements IJRDataModel
{
  @b(a="industryType")
  private String industryType;
  @b(a="merchantGuid")
  private String merchantGuid;
  @b(a="merchantLogo")
  private String merchantLogo;
  @b(a="merchantName")
  private String merchantName;
  @b(a="merchantOrderId")
  private String merchantOrderId;
  @b(a="otpSent")
  private boolean otpSent;
  @b(a="posId")
  private String posId;
  @b(a="pushTimestamp")
  private String pushTimestamp;
  private boolean read;
  @b(a="state")
  private String state;
  @b(a="txnAmount")
  private double txnAmount;
  
  public String getIndustryType()
  {
    return this.industryType;
  }
  
  public String getMerchantGuid()
  {
    return this.merchantGuid;
  }
  
  public String getMerchantLogo()
  {
    return this.merchantLogo;
  }
  
  public String getMerchantName()
  {
    return this.merchantName;
  }
  
  public String getMerchantOrderId()
  {
    return this.merchantOrderId;
  }
  
  public String getPosId()
  {
    return this.posId;
  }
  
  public String getPushTimestamp()
  {
    return this.pushTimestamp;
  }
  
  public String getState()
  {
    return this.state;
  }
  
  public double getTxnAmount()
  {
    return this.txnAmount;
  }
  
  public boolean isOtpSent()
  {
    return this.otpSent;
  }
  
  public boolean isRead()
  {
    return this.read;
  }
  
  public void setRead()
  {
    this.read = true;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRPendingPushMessage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */