package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROrderSummaryPayment
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="bank_transaction_id")
  private String mBankTransactionId;
  @b(a="gateway")
  private String mGateway;
  @b(a="id")
  private String mId;
  @b(a="mid")
  private String mMID;
  @b(a="payment_bank")
  private String mPaymentBank;
  @b(a="payment_method")
  private String mPaymentMethod;
  @b(a="payment_option")
  private String mPaymentOption;
  @b(a="paytm_cash")
  private double mPaytmCash;
  @b(a="pg_amount")
  private double mPgAmount;
  @b(a="pg_response_code")
  private String mPgResponseCode;
  @b(a="provider")
  private String mProvider;
  @b(a="status")
  private String mStatus;
  @b(a="transaction_number")
  private String mTransactionNumber;
  @b(a="transaction_response")
  private String mTransactionResponse;
  @b(a="wallet_transaction_id")
  private String mWalletTranactionId;
  @b(a="wallet_type")
  private String mWalletType;
  
  public String getBankTransactionId()
  {
    return this.mBankTransactionId;
  }
  
  public String getGateway()
  {
    return this.mGateway;
  }
  
  public String getId()
  {
    return this.mId;
  }
  
  public String getMID()
  {
    return this.mMID;
  }
  
  public String getPaymentBank()
  {
    return this.mPaymentBank;
  }
  
  public String getPaymentMethod()
  {
    return this.mPaymentMethod;
  }
  
  public String getPaymentOption()
  {
    return this.mPaymentOption;
  }
  
  public double getPaytmCash()
  {
    return this.mPaytmCash;
  }
  
  public double getPgAmount()
  {
    return this.mPgAmount;
  }
  
  public String getPgResponseCode()
  {
    return this.mPgResponseCode;
  }
  
  public String getProvider()
  {
    return this.mProvider;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public String getTransactionNumber()
  {
    return this.mTransactionNumber;
  }
  
  public String getTransactionResponse()
  {
    return this.mTransactionResponse;
  }
  
  public String getWalletTranactionId()
  {
    return this.mWalletTranactionId;
  }
  
  public String getWalletType()
  {
    return this.mWalletType;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROrderSummaryPayment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */