package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class MerchantRatingNew
  implements IJRDataModel
{
  @b(a="data")
  private ArrayList<RatingData> mData;
  @b(a="display_rating")
  private String mDipRating;
  @b(a="merchant_id")
  private String mMerchantId;
  @b(a="rating")
  private String mRating;
  @b(a="sample_count")
  private String mSampleCount;
  
  public ArrayList<RatingData> getmData()
  {
    return this.mData;
  }
  
  public String getmDipRating()
  {
    return this.mDipRating;
  }
  
  public String getmMerchantId()
  {
    return this.mMerchantId;
  }
  
  public String getmRating()
  {
    return this.mRating;
  }
  
  public String getmSampleCount()
  {
    return this.mSampleCount;
  }
  
  public void setmData(ArrayList<RatingData> paramArrayList)
  {
    this.mData = paramArrayList;
  }
  
  public void setmDipRating(String paramString)
  {
    this.mDipRating = paramString;
  }
  
  public void setmMerchantId(String paramString)
  {
    this.mMerchantId = paramString;
  }
  
  public void setmRating(String paramString)
  {
    this.mRating = paramString;
  }
  
  public void setmSampleCount(String paramString)
  {
    this.mSampleCount = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/MerchantRatingNew.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */