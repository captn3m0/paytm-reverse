package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.CJRError;
import net.one97.paytm.common.entity.CJRStatusError;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROrdersNew
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="error")
  private CJRError mError;
  @b(a="next")
  private String mNextUrl;
  @b(a="orders")
  private ArrayList<CJROrderList> mOrders;
  @b(a="status")
  private CJRStatusError mStatus;
  
  public void addNewItems(ArrayList<CJROrderList> paramArrayList)
  {
    if (this.mOrders == null)
    {
      this.mOrders = paramArrayList;
      return;
    }
    this.mOrders.addAll(paramArrayList);
  }
  
  public CJRError getError()
  {
    return this.mError;
  }
  
  public String getNextUrl()
  {
    return this.mNextUrl;
  }
  
  public ArrayList<CJROrderList> getOrders()
  {
    return this.mOrders;
  }
  
  public CJRStatusError getStatus()
  {
    return this.mStatus;
  }
  
  public void setNextUrl(String paramString)
  {
    this.mNextUrl = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROrdersNew.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */