package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.CJRItem;

public class CJRGridProduct
  extends CJRItem
{
  private static final long serialVersionUID = 1L;
  private boolean itemViewed;
  @b(a="actual_price")
  private String mActualPrice;
  private String mAncestorID;
  @b(a="brand")
  private String mBrand;
  private String mContainerInstanceID;
  @b(a="discount")
  private String mDiscountPercent;
  @b(a="offer_price")
  private String mDiscountedPrice;
  private boolean mHasVisibleDetails;
  @b(a="image_data")
  private String mImageData;
  @b(a="image_url")
  private String mImageUrl;
  private boolean mIsAdded;
  private boolean mIsExpanded;
  @b(a="stock")
  private boolean mIsInStock;
  private String mItemBgColor = "#FFFFFFFF";
  private String mListId = "";
  private String mListName;
  private int mListPosition;
  @b(a="long_rich_desc")
  private ArrayList<CJRLongRichDesc> mLongRichDesc = new ArrayList();
  @b(a="merchant_name")
  private String mMerchantName;
  @b(a="message")
  private CJRTermsAndConditions mMessage;
  @b(a="name")
  private String mName;
  @b(a="tag")
  private String mOfferTag;
  @b(a="parent_id")
  private String mParentID;
  @b(a="product_id")
  private String mProductID;
  @b(a="img_height")
  private String mProductImgHeight;
  @b(a="img_width")
  private String mProductImgWidth;
  @b(a="product_type")
  private String mProductType;
  @b(a="promo_text")
  private String mPromoText;
  private String mSearchABValue;
  private String mSearchCategory;
  private String mSearchResultType;
  private String mSearchTerm;
  private String mSearchType;
  @b(a="short_desc")
  private String mShortDesc;
  @b(a="source")
  private String mSource;
  @b(a="url")
  private String mURL;
  @b(a="url_type")
  private String mURLType;
  
  public String getActualPrice()
  {
    return this.mActualPrice;
  }
  
  public String getAncestorID()
  {
    return this.mAncestorID;
  }
  
  public String getBrand()
  {
    return this.mBrand;
  }
  
  public String getCategoryId()
  {
    return null;
  }
  
  public String getDiscountPercent()
  {
    return this.mDiscountPercent;
  }
  
  public String getDiscountedPrice()
  {
    return this.mDiscountedPrice;
  }
  
  public boolean getHasVisibleDetails()
  {
    return this.mHasVisibleDetails;
  }
  
  public String getImageData()
  {
    return this.mImageData;
  }
  
  public String getImageUrl()
  {
    return this.mImageUrl;
  }
  
  public boolean getIsAdded()
  {
    return this.mIsAdded;
  }
  
  public boolean getIsExpanded()
  {
    return this.mIsExpanded;
  }
  
  public boolean getIsInStock()
  {
    return this.mIsInStock;
  }
  
  public String getItemBgColor()
  {
    return this.mItemBgColor;
  }
  
  public String getItemID()
  {
    return null;
  }
  
  public String getLabel()
  {
    return null;
  }
  
  public String getListId()
  {
    return this.mListId;
  }
  
  public String getListName()
  {
    return this.mListName;
  }
  
  public int getListPosition()
  {
    return this.mListPosition;
  }
  
  public ArrayList<CJRLongRichDesc> getLongRichDesc()
  {
    return this.mLongRichDesc;
  }
  
  public String getMerchantName()
  {
    return this.mMerchantName;
  }
  
  public CJRTermsAndConditions getMessage()
  {
    return this.mMessage;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getOfferTag()
  {
    return this.mOfferTag;
  }
  
  public String getParentID()
  {
    return this.mParentID;
  }
  
  public String getProductID()
  {
    return this.mProductID;
  }
  
  public String getProductImgHeight()
  {
    return this.mProductImgHeight;
  }
  
  public String getProductImgWidth()
  {
    return this.mProductImgWidth;
  }
  
  public String getProductType()
  {
    return this.mProductType;
  }
  
  public String getPromoText()
  {
    return this.mPromoText;
  }
  
  public ArrayList<CJRRelatedCategory> getRelatedCategories()
  {
    return null;
  }
  
  public String getSearchABValue()
  {
    return this.mSearchABValue;
  }
  
  public String getSearchCategory()
  {
    return this.mSearchCategory;
  }
  
  public String getSearchResultType()
  {
    return this.mSearchResultType;
  }
  
  public String getSearchTerm()
  {
    return this.mSearchTerm;
  }
  
  public String getSearchType()
  {
    return this.mSearchType;
  }
  
  public String getShortDesc()
  {
    return this.mShortDesc;
  }
  
  public String getURL()
  {
    return this.mURL;
  }
  
  public String getURLType()
  {
    return this.mURLType;
  }
  
  public String getmContainerInstanceID()
  {
    return this.mContainerInstanceID;
  }
  
  public String getmSource()
  {
    return this.mSource;
  }
  
  public boolean isItemViewed()
  {
    return this.itemViewed;
  }
  
  public void setAncestorID(String paramString)
  {
    this.mAncestorID = paramString;
  }
  
  public void setHasVisibleDetails(boolean paramBoolean)
  {
    this.mHasVisibleDetails = paramBoolean;
  }
  
  public void setIsAdded(boolean paramBoolean)
  {
    this.mIsAdded = paramBoolean;
  }
  
  public void setIsExpanded(boolean paramBoolean)
  {
    this.mIsExpanded = paramBoolean;
  }
  
  public void setItemViewed()
  {
    this.itemViewed = true;
  }
  
  public void setListId(String paramString)
  {
    this.mListId = paramString;
  }
  
  public void setListName(String paramString)
  {
    this.mListName = paramString;
  }
  
  public void setListPosition(int paramInt)
  {
    this.mListPosition = paramInt;
  }
  
  public void setSearchABValue(String paramString)
  {
    this.mSearchABValue = paramString;
  }
  
  public void setSearchCategory(String paramString)
  {
    this.mSearchCategory = paramString;
  }
  
  public void setSearchResultType(String paramString)
  {
    this.mSearchResultType = paramString;
  }
  
  public void setSearchTerm(String paramString)
  {
    this.mSearchTerm = paramString;
  }
  
  public void setSearchType(String paramString)
  {
    this.mSearchType = paramString;
  }
  
  public void setURL(String paramString)
  {
    this.mURL = paramString;
  }
  
  public void setmContainerInstanceID(String paramString)
  {
    this.mContainerInstanceID = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRGridProduct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */