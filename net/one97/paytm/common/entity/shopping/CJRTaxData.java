package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTaxData
  implements IJRDataModel
{
  @b(a="state_entry_tax")
  private String mStateEntryTax;
  @b(a="value_added_tax")
  private String mValueAddedTax;
  
  public String getStateEntryTax()
  {
    return this.mStateEntryTax;
  }
  
  public String getValueAddedTax()
  {
    return this.mValueAddedTax;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRTaxData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */