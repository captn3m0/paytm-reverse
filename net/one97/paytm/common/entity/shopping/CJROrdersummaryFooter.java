package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROrdersummaryFooter
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="description")
  private String mDescription;
  @b(a="header")
  private String mHeader;
  
  public String getDescription()
  {
    return this.mDescription;
  }
  
  public String getHeader()
  {
    return this.mHeader;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROrdersummaryFooter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */