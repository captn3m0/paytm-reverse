package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class WishList
  implements IJRDataModel
{
  @b(a="message")
  private String mMessage;
  @b(a="items")
  private ArrayList<WishListProduct> mProduct;
  
  public String getmMessage()
  {
    return this.mMessage;
  }
  
  public ArrayList<WishListProduct> getmProduct()
  {
    return this.mProduct;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/WishList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */