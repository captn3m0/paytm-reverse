package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRSearchLayout
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="count")
  public String mCount;
  @b(a="image_url")
  public String mImageUrl;
  @b(a="price")
  public String mPrice;
  @b(a="cats")
  private ArrayList<CJRSearchPageItem> mSearchPageItemList = new ArrayList();
  @b(a="seourl")
  public String mSeourl;
  @b(a="text")
  private String mText;
  
  public String getCount()
  {
    return this.mCount;
  }
  
  public String getImageUrl()
  {
    return this.mImageUrl;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getPrice()
  {
    if ((this.mPrice != null) && (!this.mPrice.contains("Rs"))) {
      this.mPrice = ("Rs " + this.mPrice);
    }
    return this.mPrice;
  }
  
  public ArrayList<CJRSearchPageItem> getSearchPageItemList()
  {
    return this.mSearchPageItemList;
  }
  
  public String getSeourl()
  {
    return this.mSeourl;
  }
  
  public String getText()
  {
    return this.mText;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRSearchLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */