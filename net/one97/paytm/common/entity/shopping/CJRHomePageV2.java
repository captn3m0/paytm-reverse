package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.Iterator;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRHomePageV2
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="alt_image_url")
  private String alt_image_url;
  @b(a="api_version")
  private String apiVersion;
  @b(a="name")
  private String banner_name;
  @b(a="context")
  public CJRContext cjrContext;
  @b(a="description")
  private String description;
  private String entityAssociatedWith;
  @b(a="image_url")
  private String image_url;
  public boolean isOrderDetailsExpanded;
  @b(a="error")
  private String mErrorMsg;
  @b(a="footer_image_url")
  private String mFooterImage;
  @b(a="ga_key")
  public String mGAKey;
  private String mGATitle;
  @b(a="page_id")
  private String mID = "";
  @b(a="mobile_layout")
  public ArrayList<CJRHomePageLayoutV2> mMobileLayout = new ArrayList();
  @b(a="old_category_id")
  private String mOldCategoryId;
  @b(a="page")
  public ArrayList<CJRHomePageDetailV2> mPage = new ArrayList();
  @b(a="pages")
  public ArrayList<CJRHomePageDetailV2> mPages = new ArrayList();
  @b(a="placeholder_image_url")
  public String mPlaceHolderImageUrl;
  @b(a="page_name")
  private String pageName;
  @b(a="url_key")
  private String urlKey;
  
  public String getAlt_image_url()
  {
    return this.alt_image_url;
  }
  
  public String getBannerName()
  {
    return this.banner_name;
  }
  
  public String getDescription()
  {
    return this.description;
  }
  
  public String getEntityAssociatedWith()
  {
    return this.mID;
  }
  
  public String getErrorMsg()
  {
    return this.mErrorMsg;
  }
  
  public String getFooterImage()
  {
    return this.mFooterImage;
  }
  
  public String getGAKey()
  {
    return this.mGAKey;
  }
  
  public String getGATitle()
  {
    return this.mGATitle;
  }
  
  public String getID()
  {
    return this.mID;
  }
  
  public String getImage_url()
  {
    return this.image_url;
  }
  
  public CJRHomePageLayoutV2 getLayoutFromItem(CJRHomePageItem paramCJRHomePageItem)
  {
    try
    {
      label34:
      CJRHomePageLayoutV2 localCJRHomePageLayoutV2;
      boolean bool;
      do
      {
        Object localObject;
        do
        {
          do
          {
            Iterator localIterator1 = this.mPage.iterator();
            break label34;
            Iterator localIterator2;
            while (!localIterator2.hasNext())
            {
              if (!localIterator1.hasNext()) {
                break;
              }
              localIterator2 = ((CJRHomePageDetailV2)localIterator1.next()).getHomePageLayoutList().iterator();
            }
            localCJRHomePageLayoutV2 = (CJRHomePageLayoutV2)localIterator2.next();
            localObject = localCJRHomePageLayoutV2.getHomePageItemList();
          } while ((localObject == null) || (paramCJRHomePageItem == null) || (!((ArrayList)localObject).contains(paramCJRHomePageItem)));
          localObject = (CJRHomePageItem)((ArrayList)localObject).get(((ArrayList)localObject).indexOf(paramCJRHomePageItem));
        } while ((localObject == null) || (((CJRHomePageItem)localObject).getURLType() == null) || (paramCJRHomePageItem.getURLType() == null));
        bool = ((CJRHomePageItem)localObject).getURLType().equalsIgnoreCase(paramCJRHomePageItem.getURLType());
      } while (!bool);
      return localCJRHomePageLayoutV2;
    }
    catch (Exception paramCJRHomePageItem)
    {
      paramCJRHomePageItem.printStackTrace();
    }
    return null;
  }
  
  public ArrayList<CJRHomePageLayoutV2> getMobileLayoutList()
  {
    return this.mMobileLayout;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getOldCategoryId()
  {
    return this.mOldCategoryId;
  }
  
  public ArrayList<CJRHomePageItem> getParentListForItem(String paramString, CJRHomePageItem paramCJRHomePageItem)
  {
    new ArrayList();
    CJRHomePageLayoutV2 localCJRHomePageLayoutV2;
    Object localObject;
    do
    {
      do
      {
        paramString = this.mPage.iterator();
        Iterator localIterator;
        while (!localIterator.hasNext())
        {
          if (!paramString.hasNext()) {
            break;
          }
          localIterator = ((CJRHomePageDetailV2)paramString.next()).getHomePageLayoutList().iterator();
        }
        localCJRHomePageLayoutV2 = (CJRHomePageLayoutV2)localIterator.next();
        localObject = localCJRHomePageLayoutV2.getHomePageItemList();
      } while (!((ArrayList)localObject).contains(paramCJRHomePageItem));
      localObject = (CJRHomePageItem)((ArrayList)localObject).get(((ArrayList)localObject).indexOf(paramCJRHomePageItem));
    } while ((localObject == null) || (((CJRHomePageItem)localObject).getParentItem() == null) || (paramCJRHomePageItem.getParentItem() == null) || (!((CJRHomePageItem)localObject).getParentItem().equalsIgnoreCase(paramCJRHomePageItem.getParentItem())));
    return localCJRHomePageLayoutV2.getHomePageItemList();
    return null;
  }
  
  public String getPlaceHolderImageUrl()
  {
    return this.mPlaceHolderImageUrl;
  }
  
  public String getUrlKey()
  {
    return this.urlKey;
  }
  
  public ArrayList<CJRHomePageDetailV2> getmPage()
  {
    return this.mPage;
  }
  
  public ArrayList<CJRHomePageDetailV2> getmPages()
  {
    return this.mPages;
  }
  
  public boolean isOrderDetailsExpanded()
  {
    return this.isOrderDetailsExpanded;
  }
  
  public void setEntityAssociatedWith(String paramString)
  {
    this.entityAssociatedWith = paramString;
  }
  
  public void setFooterImageUrl(String paramString)
  {
    this.mFooterImage = paramString;
  }
  
  public void setGATitle(String paramString)
  {
    this.mGATitle = paramString;
  }
  
  public void setIsOrderDetailsExpanded(boolean paramBoolean)
  {
    this.isOrderDetailsExpanded = paramBoolean;
  }
  
  public void setmPage(ArrayList<CJRHomePageDetailV2> paramArrayList)
  {
    this.mPage = paramArrayList;
  }
  
  public void setmPages(ArrayList<CJRHomePageDetailV2> paramArrayList)
  {
    this.mPages = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRHomePageV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */