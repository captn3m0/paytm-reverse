package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrandStoreInfo
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="display_tag")
  private String mBrandStoreDisplayTag;
  @b(a="link")
  private String mBrandStoreLink;
  @b(a="url_type")
  private String mBrandStoreURLType;
  
  public String getmBrandStoreDisplayTag()
  {
    return this.mBrandStoreDisplayTag;
  }
  
  public String getmBrandStoreLink()
  {
    return this.mBrandStoreLink;
  }
  
  public String getmBrandStoreURLType()
  {
    return this.mBrandStoreURLType;
  }
  
  public void setmBrandStoreDisplayTag(String paramString)
  {
    this.mBrandStoreDisplayTag = paramString;
  }
  
  public void setmBrandStoreLink(String paramString)
  {
    this.mBrandStoreLink = paramString;
  }
  
  public void setmBrandStoreURLType(String paramString)
  {
    this.mBrandStoreURLType = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRBrandStoreInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */