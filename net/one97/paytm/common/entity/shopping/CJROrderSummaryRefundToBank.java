package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROrderSummaryRefundToBank
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="bankRefund")
  private boolean mBankRefundButtons;
  @b(a="message")
  private String mMessage;
  @b(a="title")
  private String mTitle;
  @b(a="walletGuId")
  private String mWalletGuid;
  
  public boolean getBankRefundButtons()
  {
    return this.mBankRefundButtons;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
  
  public String getWalletGuid()
  {
    return this.mWalletGuid;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROrderSummaryRefundToBank.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */