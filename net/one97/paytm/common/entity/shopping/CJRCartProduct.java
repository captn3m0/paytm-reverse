package net.one97.paytm.common.entity.shopping;

import android.text.TextUtils;
import com.google.c.a.b;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import net.one97.paytm.common.entity.CJRProduct;
import net.one97.paytm.common.entity.SellerRating.MerchantRatingNew;

public class CJRCartProduct
  extends CJRProduct
{
  private static final long serialVersionUID = 1L;
  @b(a="attributes_dim_values")
  private Map<String, String> cartItemAttribute;
  @b(a="available_quantity")
  private String mAvlbleQuantity;
  @b(a="categoryMap")
  private ArrayList<CJRCategoryMap> mCategoryMap;
  public String mContainerInstanceID;
  @b(a="conv_fee")
  private String mConvFee;
  @b(a="estimated_delivery_range")
  private ArrayList<String> mDates;
  @b(a="paytm_discount")
  private String mDiscount;
  @b(a="discounted_price")
  private String mDiscountedPrice;
  private int mDisplayValueIndex;
  @b(a="error")
  private String mError;
  @b(a="error_title")
  private String mErrorTitle;
  @b(a="estimated_delivery")
  private String mEstimatedDelivery;
  @b(a="ga_key")
  private String mGAkey;
  @b(a="image_url")
  private String mImageUrl;
  @b(a="instock")
  private boolean mIsInstock;
  @b(a="more_sellers")
  private Boolean mIsMoreSellers = null;
  @b(a="aggregate_item_price")
  private String mItemAggregateItemPrice;
  @b(a="merchant_id")
  private String mMerchantId;
  @b(a="merchant_name")
  private String mMerchantName;
  private MerchantRatingNew mMerchantRating;
  @b(a="meta_data")
  public Object mMetaDataResponse;
  @b(a="mrp")
  private String mMrp;
  @b(a="need_shipping")
  private boolean mNeedShipping;
  @b(a="offer_text")
  private String mOfferText;
  @b(a="offer_url")
  private String mOffersUrl;
  private CJROtherSellersMain mOtherSellers;
  @b(a="parent_id")
  private String mParentID;
  @b(a="paytm_cashback")
  private String mPaytmCashBack;
  @b(a="price")
  private String mPrice;
  @b(a="product_config_name")
  private String mProductConfigName;
  @b(a="product_id")
  private String mProductId;
  @b(a="promocode")
  private String mPromoCode;
  @b(a="promostatus")
  private String mPromoStatus;
  @b(a="promotext")
  private String mPromoText;
  @b(a="quantity")
  private String mQuantity;
  @b(a="configuration")
  private Map<String, String> mRechargeConfigList;
  @b(a="selling_price")
  private String mSellingPrice;
  @b(a="service_options")
  private CJRServiceOptions mServiceOptions;
  @b(a="shipping_charges")
  private int mShippingCharges;
  @b(a="shipping_cost")
  private String mShippingCost;
  @b(a="title")
  private String mTitle;
  @b(a="total_price")
  private String mTotalPrice;
  @b(a="tracking_info")
  private CJRTrackingInfo mTrackingInfo;
  @b(a="url")
  private String mUrl;
  @b(a="vertical_id")
  private String mVerticalId;
  @b(a="vertical_label")
  private String mVerticalLabel;
  @b(a="other_taxes")
  private ArrayList<CJROtherTaxes> otherTaxes;
  
  public String getAvlbleQuantity()
  {
    return this.mAvlbleQuantity;
  }
  
  public String getBrand()
  {
    return this.mBrand;
  }
  
  public Map<String, String> getCartItemAttribute()
  {
    return this.cartItemAttribute;
  }
  
  public String getCategoryIdForGTM()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if ((this.mCategoryMap != null) && (this.mCategoryMap.size() > 0))
    {
      int j = this.mCategoryMap.size();
      int i = 0;
      while (i < j)
      {
        localStringBuilder.append(((CJRCategoryMap)this.mCategoryMap.get(i)).getCategoryId());
        if ((j > 1) && (i < j - 1)) {
          localStringBuilder.append("/");
        }
        i += 1;
      }
    }
    return localStringBuilder.toString();
  }
  
  public ArrayList<CJRCategoryMap> getCategoryMap()
  {
    return this.mCategoryMap;
  }
  
  public String getCategoryPathForGTM()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if ((this.mCategoryMap != null) && (this.mCategoryMap.size() > 0))
    {
      int j = this.mCategoryMap.size();
      int i = 0;
      while (i < j)
      {
        localStringBuilder.append(((CJRCategoryMap)this.mCategoryMap.get(i)).getName());
        if ((j > 1) && (i < j - 1)) {
          localStringBuilder.append("/");
        }
        i += 1;
      }
    }
    return localStringBuilder.toString();
  }
  
  public Map<String, String> getConfigurationList()
  {
    return this.mRechargeConfigList;
  }
  
  public ArrayList<String> getDates()
  {
    return this.mDates;
  }
  
  public String getDiscountedPrice()
  {
    if (TextUtils.isEmpty(this.mDiscountedPrice)) {
      return "0";
    }
    return this.mDiscountedPrice;
  }
  
  public int getDisplayArrayIndex()
  {
    return this.mDisplayValueIndex;
  }
  
  public String getError()
  {
    return this.mError;
  }
  
  public String getErrorTitle()
  {
    return this.mErrorTitle;
  }
  
  public String getGAkey()
  {
    return this.mGAkey;
  }
  
  public String getImageUrl()
  {
    return this.mImageUrl;
  }
  
  public boolean getIsInstock()
  {
    return this.mIsInstock;
  }
  
  public String getLastItemInCategoryMap()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    try
    {
      if (this.mCategoryMap.size() > 0) {
        localStringBuilder.append(((CJRCategoryMap)this.mCategoryMap.get(this.mCategoryMap.size() - 1)).getCategoryId());
      }
      return localStringBuilder.toString();
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  public String getMerchantId()
  {
    return this.mMerchantId;
  }
  
  public String getMerchantName()
  {
    return this.mMerchantName;
  }
  
  public Object getMetaDataResponse()
  {
    return this.mMetaDataResponse;
  }
  
  public String getMrp()
  {
    if (TextUtils.isEmpty(this.mMrp)) {
      return "0";
    }
    return this.mMrp;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public boolean getNeedShipping()
  {
    return this.mNeedShipping;
  }
  
  public String getOffersUrl()
  {
    return this.mOffersUrl;
  }
  
  public ArrayList<CJROtherTaxes> getOtherTaxes()
  {
    return this.otherTaxes;
  }
  
  public String getParentID()
  {
    return this.mParentID;
  }
  
  public String getPaytmCashBack()
  {
    return this.mPaytmCashBack;
  }
  
  public String getPrice()
  {
    if (TextUtils.isEmpty(this.mPrice)) {
      return "0";
    }
    return this.mPrice;
  }
  
  public String getProductConfigName()
  {
    return this.mProductConfigName;
  }
  
  public String getProductId()
  {
    return this.mProductId;
  }
  
  public String getProductType()
  {
    return this.mProductType;
  }
  
  public String getPromoCode()
  {
    return this.mPromoCode;
  }
  
  public String getPromoText()
  {
    return this.mPromoText;
  }
  
  public String getQuantity()
  {
    return this.mQuantity;
  }
  
  public String getSellingPrice()
  {
    if (TextUtils.isEmpty(this.mSellingPrice)) {
      return "0";
    }
    return this.mSellingPrice;
  }
  
  public CJRServiceOptions getServiceOptions()
  {
    return this.mServiceOptions;
  }
  
  public int getShippingCharges()
  {
    return this.mShippingCharges;
  }
  
  public String getShippingCost()
  {
    return this.mShippingCost;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
  
  public String getTotalPrice()
  {
    if (TextUtils.isEmpty(this.mTotalPrice)) {
      return "0";
    }
    return this.mTotalPrice;
  }
  
  public CJRTrackingInfo getTrackingInfo()
  {
    return this.mTrackingInfo;
  }
  
  public String getUrl()
  {
    return this.mUrl;
  }
  
  public String getVerticalId()
  {
    return this.mVerticalId;
  }
  
  public String getVerticalLabel()
  {
    return this.mVerticalLabel;
  }
  
  public String getmContainerInstanceID()
  {
    return this.mContainerInstanceID;
  }
  
  public String getmConvFee()
  {
    return this.mConvFee;
  }
  
  public String getmDiscount()
  {
    return this.mDiscount;
  }
  
  public String getmEstimatedDelivery()
  {
    Object localObject2;
    Object localObject1;
    String str1;
    Object localObject3;
    Object localObject4;
    if ((getDates() != null) && (getDates().size() > 0))
    {
      localObject2 = null;
      localObject1 = null;
      str1 = new String();
      if (getDates().get(1) != null)
      {
        localObject3 = (String)getDates().get(1);
        localObject1 = localObject3;
        if (((String)localObject3).contains("T")) {
          localObject1 = ((String)localObject3).substring(0, ((String)localObject3).indexOf("T"));
        }
      }
      if (getDates().get(0) != null)
      {
        localObject3 = (String)getDates().get(0);
        localObject2 = localObject3;
        if (((String)localObject3).contains("T")) {
          localObject2 = ((String)localObject3).substring(0, ((String)localObject3).indexOf("T"));
        }
      }
      localObject4 = new SimpleDateFormat("yyyy-MM-dd");
      localObject3 = new SimpleDateFormat("MMM");
      if ((localObject2 == null) || (localObject1 == null)) {}
    }
    else
    {
      try
      {
        localObject1 = ((DateFormat)localObject4).parse((String)localObject1);
        localObject4 = ((DateFormat)localObject4).parse((String)localObject2);
        localObject2 = Calendar.getInstance();
        ((Calendar)localObject2).setTime((Date)localObject4);
        localObject4 = Integer.valueOf(((Calendar)localObject2).get(5));
        String str2 = ((DateFormat)localObject3).format(((Calendar)localObject2).getTime());
        ((Calendar)localObject2).setTime((Date)localObject1);
        localObject1 = Integer.valueOf(((Calendar)localObject2).get(5));
        localObject2 = ((DateFormat)localObject3).format(((Calendar)localObject2).getTime());
        if (((String)localObject2).equals(str2)) {
          return str1 + localObject4 + "-" + localObject1 + " " + (String)localObject2;
        }
        localObject1 = str1 + localObject4 + " " + str2 + "-" + localObject1 + " " + (String)localObject2;
        return (String)localObject1;
      }
      catch (Exception localException) {}
      return null;
    }
    return str1;
  }
  
  public String getmItemAggregateItemPrice()
  {
    return this.mItemAggregateItemPrice;
  }
  
  public MerchantRatingNew getmMerchantRating()
  {
    return this.mMerchantRating;
  }
  
  public String getmOfferText()
  {
    return this.mOfferText;
  }
  
  public CJROtherSellersMain getmOtherSellers()
  {
    return this.mOtherSellers;
  }
  
  public String getmPromoStatus()
  {
    return this.mPromoStatus;
  }
  
  public Boolean ismIsMoreSellers()
  {
    return this.mIsMoreSellers;
  }
  
  public void setDisplayItemIndex(int paramInt)
  {
    this.mDisplayValueIndex = paramInt;
  }
  
  public void setMetaDataResponse(Object paramObject)
  {
    this.mMetaDataResponse = paramObject;
  }
  
  public void setProductId(String paramString)
  {
    this.mProductId = paramString;
  }
  
  public void setTrackingInfo(CJRTrackingInfo paramCJRTrackingInfo)
  {
    this.mTrackingInfo = paramCJRTrackingInfo;
  }
  
  public void setVerticalId(String paramString)
  {
    this.mVerticalId = paramString;
  }
  
  public void setmContainerInstanceID(String paramString)
  {
    this.mContainerInstanceID = paramString;
  }
  
  public void setmConvFee(String paramString)
  {
    this.mConvFee = paramString;
  }
  
  public void setmDisplayValueIndex(int paramInt)
  {
    this.mDisplayValueIndex = paramInt;
  }
  
  public void setmIsMoreSellers(Boolean paramBoolean)
  {
    this.mIsMoreSellers = paramBoolean;
  }
  
  public void setmMerchantRating(MerchantRatingNew paramMerchantRatingNew)
  {
    this.mMerchantRating = paramMerchantRatingNew;
  }
  
  public void setmOtherSellers(CJROtherSellersMain paramCJROtherSellersMain)
  {
    this.mOtherSellers = paramCJROtherSellersMain;
  }
  
  public void setmQuantity(String paramString)
  {
    this.mQuantity = paramString;
  }
  
  public void setmShippingCharges(int paramInt)
  {
    this.mShippingCharges = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRCartProduct.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */