package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROrderSummaryTransaction
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="title")
  private String mTitle;
  @b(a="type")
  private String mType;
  @b(a="value")
  private String mValue;
  
  public String getTitle()
  {
    return this.mTitle;
  }
  
  public String getType()
  {
    return this.mType;
  }
  
  public String getValue()
  {
    return this.mValue;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROrderSummaryTransaction.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */