package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.Iterator;
import net.one97.paytm.common.entity.CJRStatusError;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROrderSummary
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="aggregate_item_price")
  private String mAggregateItemPrice;
  @b(a="amount_bfr_tax")
  private double mAmountBfrTax;
  @b(a="banner")
  private CJROrdersummaryReferral mBanner;
  @b(a="billing_address")
  private CJROrderSummaryAddress mBillingAddress;
  @b(a="channel_id")
  private String mChannelId;
  @b(a="total_conv_fee")
  private double mConFee;
  @b(a="conv_fee")
  private double mConvFee;
  @b(a="created_at")
  private String mCreatedAt;
  @b(a="customer_email")
  private String mCustomerEmail;
  @b(a="customer_firstname")
  private String mCustomerFirstName;
  @b(a="customer_lastname")
  private String mCustomerLastName;
  @b(a="customer_middlename")
  private String mCustomerMiddleName;
  @b(a="customer_type")
  private String mCustomerType;
  @b(a="discount_amount")
  private double mDiscountAmount;
  @b(a="discount_surcharge_amount")
  private double mDiscountSurchargeAmount;
  @b(a="discount_tax_amount")
  private double mDiscountTaxAmount;
  @b(a="error")
  private CJRStatusError mError;
  @b(a="footer")
  private CJROrdersummaryFooter mFooter;
  @b(a="grandtotal")
  private double mGrandTotal;
  @b(a="id")
  private String mId;
  @b(a="label")
  private String mLabel;
  @b(a="need_shipping")
  private boolean mNeedShipping;
  @b(a="order_discount")
  private double mOrderDiscount;
  @b(a="items")
  private ArrayList<CJROrderedCart> mOrderedCartList;
  @b(a="payments")
  private ArrayList<CJROrderSummaryPayment> mPaymentInfo;
  @b(a="payment_status")
  private String mPaymentStatus;
  @b(a="payment_summary")
  private String mPaymentSummary;
  @b(a="payment_text")
  private String mPaymentText;
  @b(a="promo_code")
  private String mPromoCode;
  @b(a="promo_description")
  private String mPromoDescription;
  @b(a="refund")
  private CJROrderSummaryRefundToBank mRefundToBank;
  @b(a="remote_ip")
  private String mRemoteIP;
  @b(a="result")
  private String mResult;
  @b(a="shipping_address")
  private CJRAddress mShippingAddress;
  @b(a="shipping_amount")
  private double mShippingAmount;
  @b(a="shipping_description")
  private String mShippingDescription;
  @b(a="shipping_method")
  private String mShippingMethod;
  @b(a="shipping_tax_amount")
  private double mShippingTaxAmount;
  @b(a="status")
  private String mStatus;
  @b(a="subtotal")
  private double mSubTotal;
  @b(a="subtotal_incl_tax")
  private double mSubTotalInclTax;
  @b(a="surcharge_amount")
  private double mSurChargeAmount;
  @b(a="tax_amount")
  private double mTaxAmount;
  @b(a="transaction_details")
  private ArrayList<CJROrderSummaryTransaction> mTransactionDetails;
  private String movieImageText;
  @b(a="total_other_taxes")
  ArrayList<CJROtherTaxes> otherTaxes;
  private String qrBitmapText;
  
  public boolean checkPaymentMethod(String paramString)
  {
    boolean bool2 = false;
    Iterator localIterator = this.mPaymentInfo.iterator();
    do
    {
      bool1 = bool2;
      if (!localIterator.hasNext()) {
        break;
      }
    } while (!((CJROrderSummaryPayment)localIterator.next()).getPaymentMethod().equalsIgnoreCase(paramString));
    boolean bool1 = true;
    return bool1;
  }
  
  public String getAggregateItemPrice()
  {
    return this.mAggregateItemPrice;
  }
  
  public double getAmountBfrTax()
  {
    return this.mAmountBfrTax;
  }
  
  public CJROrdersummaryReferral getBanner()
  {
    return this.mBanner;
  }
  
  public CJROrderSummaryAddress getBillingAddress()
  {
    return this.mBillingAddress;
  }
  
  public String getChannelId()
  {
    return this.mChannelId;
  }
  
  public double getConvFee()
  {
    return this.mConvFee;
  }
  
  public String getCreatedAt()
  {
    return this.mCreatedAt;
  }
  
  public String getCustomerEmail()
  {
    return this.mCustomerEmail;
  }
  
  public String getCustomerFirstName()
  {
    return this.mCustomerFirstName;
  }
  
  public String getCustomerLastName()
  {
    return this.mCustomerLastName;
  }
  
  public String getCustomerMiddleName()
  {
    return this.mCustomerMiddleName;
  }
  
  public String getCustomerType()
  {
    return this.mCustomerType;
  }
  
  public double getDiscountAmount()
  {
    return this.mDiscountAmount;
  }
  
  public double getDiscountSurchargeAmount()
  {
    return this.mDiscountSurchargeAmount;
  }
  
  public double getDiscountTaxAmount()
  {
    return this.mDiscountTaxAmount;
  }
  
  public CJRStatusError getError()
  {
    return this.mError;
  }
  
  public CJROrdersummaryFooter getFooter()
  {
    return this.mFooter;
  }
  
  public double getGrandTotal()
  {
    return this.mGrandTotal;
  }
  
  public String getId()
  {
    return this.mId;
  }
  
  public String getLabel()
  {
    return this.mLabel;
  }
  
  public String getMovieImageText()
  {
    return this.movieImageText;
  }
  
  public boolean getNeedShipping()
  {
    return this.mNeedShipping;
  }
  
  public double getOrderDiscount()
  {
    return this.mOrderDiscount;
  }
  
  public ArrayList<CJROrderedCart> getOrderedCartList()
  {
    return this.mOrderedCartList;
  }
  
  public ArrayList<CJROtherTaxes> getOtherTaxes()
  {
    return this.otherTaxes;
  }
  
  public ArrayList<CJROrderSummaryPayment> getPaymentInfo()
  {
    return this.mPaymentInfo;
  }
  
  public String getPaymentMethodForGA()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if ((this.mPaymentInfo != null) && (this.mPaymentInfo.size() > 0))
    {
      Iterator localIterator = this.mPaymentInfo.iterator();
      while (localIterator.hasNext())
      {
        localStringBuilder.append(((CJROrderSummaryPayment)localIterator.next()).getPaymentMethod());
        if (localIterator.hasNext()) {
          localStringBuilder.append(" | ");
        }
      }
    }
    return localStringBuilder.toString();
  }
  
  public String getPaymentStatus()
  {
    return this.mPaymentStatus;
  }
  
  public String getPaymentSummary()
  {
    return this.mPaymentSummary;
  }
  
  public String getPaymentText()
  {
    return this.mPaymentText;
  }
  
  public String getPromoCode()
  {
    return this.mPromoCode;
  }
  
  public String getPromoDescription()
  {
    return this.mPromoDescription;
  }
  
  public String getQrBitmapText()
  {
    return this.qrBitmapText;
  }
  
  public CJROrderSummaryRefundToBank getRefund()
  {
    return this.mRefundToBank;
  }
  
  public String getRemoteIP()
  {
    return this.mRemoteIP;
  }
  
  public String getResult()
  {
    return this.mResult;
  }
  
  public CJRAddress getShippingAddress()
  {
    return this.mShippingAddress;
  }
  
  public double getShippingAmount()
  {
    return this.mShippingAmount;
  }
  
  public String getShippingAndBillingAddressForGA()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.mShippingAddress.getCity()).append(" | ").append(this.mBillingAddress.getCity());
    return localStringBuilder.toString();
  }
  
  public String getShippingDescription()
  {
    return this.mShippingDescription;
  }
  
  public String getShippingMethod()
  {
    return this.mShippingMethod;
  }
  
  public double getShippingTaxAmount()
  {
    return this.mShippingTaxAmount;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public double getSubTotal()
  {
    return this.mSubTotal;
  }
  
  public double getSubTotalInclTax()
  {
    return this.mSubTotalInclTax;
  }
  
  public double getSurChargeAmount()
  {
    return this.mSurChargeAmount;
  }
  
  public double getTaxAmount()
  {
    return this.mTaxAmount;
  }
  
  public ArrayList<CJROrderSummaryTransaction> getTransactionDetails()
  {
    return this.mTransactionDetails;
  }
  
  public double getmConFee()
  {
    return this.mConFee;
  }
  
  public void setMovieImageText(String paramString)
  {
    this.movieImageText = paramString;
  }
  
  public void setQrBitmapText(String paramString)
  {
    this.qrBitmapText = paramString;
  }
  
  public void setmConFee(double paramDouble)
  {
    this.mConFee = paramDouble;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROrderSummary.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */