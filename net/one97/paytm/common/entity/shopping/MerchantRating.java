package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class MerchantRating
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="communication")
  private String mCommunication;
  @b(a="item_as_described")
  private String mItemAsDescribed;
  @b(a="shipping_time")
  private String mShippingTime;
  @b(a="total")
  private String mTotal;
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/MerchantRating.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */