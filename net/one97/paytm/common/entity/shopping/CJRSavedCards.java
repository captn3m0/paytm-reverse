package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRSavedCards
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="BIN_DETAILS")
  private ArrayList<CJRSavedCard> mSavedCardList = new ArrayList();
  @b(a="SIZE")
  private int mSize;
  @b(a="STATUS")
  private String mStatus;
  
  public ArrayList<CJRSavedCard> getSavedCardList()
  {
    return this.mSavedCardList;
  }
  
  public int getSize()
  {
    return this.mSize;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRSavedCards.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */