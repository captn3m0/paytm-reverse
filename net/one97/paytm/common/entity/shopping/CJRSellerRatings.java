package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.List;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRSellerRatings
  extends CJRDataModelItem
{
  @b(a="data")
  private List<CJRSellerRatingProp> mData;
  @b(a="display_rating")
  private String mDisplayRating;
  @b(a="merchant_id")
  private String mMerchantId;
  @b(a="rating")
  private String mRating;
  @b(a="sample_count")
  private String mRatingCount;
  
  public List<CJRSellerRatingProp> getData()
  {
    return this.mData;
  }
  
  public String getDisplayRating()
  {
    return this.mDisplayRating;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getRating()
  {
    return this.mRating;
  }
  
  public String getRatingCount()
  {
    return this.mRatingCount;
  }
  
  public String getmMerchantId()
  {
    return this.mMerchantId;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRSellerRatings.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */