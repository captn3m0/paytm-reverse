package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.CJRDataModelItem;
import net.one97.paytm.common.entity.CJRError;
import net.one97.paytm.common.entity.CJRStatusError;

public class CJRRechargeOrderList
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="error")
  private CJRError mError;
  @b(a="next")
  private String mNextUrl;
  @b(a="orders")
  private ArrayList<CJROrderList> mOrderList;
  @b(a="status")
  private CJRStatusError mStatus;
  private String pageType = "";
  
  public void addNewItems(ArrayList<CJROrderList> paramArrayList)
  {
    if (this.mOrderList == null)
    {
      this.mOrderList = paramArrayList;
      return;
    }
    this.mOrderList.addAll(paramArrayList);
  }
  
  public CJRError getError()
  {
    return this.mError;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getNextUrl()
  {
    return this.mNextUrl;
  }
  
  public ArrayList<CJROrderList> getOrderList()
  {
    return this.mOrderList;
  }
  
  public String getPageType()
  {
    return this.pageType;
  }
  
  public CJRStatusError getStatus()
  {
    return this.mStatus;
  }
  
  public void setNextUrl(String paramString)
  {
    this.mNextUrl = paramString;
  }
  
  public void setOrderList(ArrayList<CJROrderList> paramArrayList)
  {
    this.mOrderList = paramArrayList;
  }
  
  public void setPageType(String paramString)
  {
    this.pageType = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRRechargeOrderList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */