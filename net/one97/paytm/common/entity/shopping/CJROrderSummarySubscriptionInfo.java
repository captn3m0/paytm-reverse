package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;

public class CJROrderSummarySubscriptionInfo
{
  @b(a="amount")
  private long mAmount;
  @b(a="expiryDate")
  private String mExpiryDate;
  @b(a="interval")
  private int mInterval;
  
  public String getExpiryDate()
  {
    return this.mExpiryDate;
  }
  
  public int getInterval()
  {
    return this.mInterval;
  }
  
  public long getSubscriptionAmount()
  {
    return this.mAmount;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROrderSummarySubscriptionInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */