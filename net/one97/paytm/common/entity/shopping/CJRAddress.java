package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAddress
  implements IJRDataModel
{
  public static final String RESPONSE_STATUS_FAILURE = "Failure";
  private static final long serialVersionUID = 1L;
  @b(a="address1")
  private String mAddress1;
  @b(a="address2")
  private String mAddress2;
  @b(a="id")
  private String mAddressId;
  @b(a="city")
  private String mCity;
  @b(a="country")
  private String mCountry;
  @b(a="ERROR")
  private String mError;
  @b(a="fulladdress")
  private String mFulladdress;
  private boolean mIsChecked;
  @b(a="isDeleted")
  private boolean mIsDeleted;
  @b(a="message")
  private String mMessage;
  @b(a="mobile")
  private String mMobile;
  @b(a="name")
  private String mName;
  @b(a="pin")
  private String mPin;
  @b(a="priority")
  private int mPriority;
  @b(a="responseCode")
  private String mResponseCode;
  @b(a="state")
  private String mState;
  @b(a="status")
  private String mStatus;
  @b(a="title")
  private String mTitle;
  
  public boolean equals(Object paramObject)
  {
    try
    {
      boolean bool = this.mAddressId.equals(((CJRAddress)paramObject).mAddressId);
      return bool;
    }
    catch (Exception paramObject) {}
    return false;
  }
  
  public String getAddress1()
  {
    return this.mAddress1;
  }
  
  public String getAddress2()
  {
    return this.mAddress2;
  }
  
  public String getCity()
  {
    return this.mCity;
  }
  
  public String getCountry()
  {
    return this.mCountry;
  }
  
  public String getError()
  {
    return this.mError;
  }
  
  public String getFulladdress()
  {
    return this.mFulladdress;
  }
  
  public String getId()
  {
    return this.mAddressId;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getMobile()
  {
    return this.mMobile;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getPin()
  {
    return this.mPin;
  }
  
  public int getPriority()
  {
    return this.mPriority;
  }
  
  public String getResponseCode()
  {
    return this.mResponseCode;
  }
  
  public String getState()
  {
    return this.mState;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
  
  public boolean isChecked()
  {
    return this.mIsChecked;
  }
  
  public boolean isDeleted()
  {
    return this.mIsDeleted;
  }
  
  public void setAddress1(String paramString)
  {
    this.mAddress1 = paramString;
  }
  
  public void setAddress2(String paramString)
  {
    this.mAddress2 = paramString;
  }
  
  public void setCity(String paramString)
  {
    this.mCity = paramString;
  }
  
  public void setCountry(String paramString)
  {
    this.mCountry = paramString;
  }
  
  public void setFulladdress(String paramString)
  {
    this.mFulladdress = paramString;
  }
  
  public void setId(String paramString)
  {
    this.mAddressId = paramString;
  }
  
  public void setIsChecked(boolean paramBoolean)
  {
    this.mIsChecked = paramBoolean;
  }
  
  public void setMobile(String paramString)
  {
    this.mMobile = paramString;
  }
  
  public void setName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setPin(String paramString)
  {
    this.mPin = paramString;
  }
  
  public void setPriority(int paramInt)
  {
    this.mPriority = paramInt;
  }
  
  public void setState(String paramString)
  {
    this.mState = paramString;
  }
  
  public void setTitle(String paramString)
  {
    this.mTitle = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRAddress.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */