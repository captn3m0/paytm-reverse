package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;

public class CJRGridHeader
{
  private static final long serialVersionUID = 1L;
  @b(a="image_url")
  private String mImageUrl;
  @b(a="text")
  private String mText;
  
  public String getImageUrl()
  {
    return this.mImageUrl;
  }
  
  public String getText()
  {
    return this.mText;
  }
  
  public void setHeaderText(String paramString)
  {
    this.mText = paramString;
  }
  
  public void setImageUrl(String paramString)
  {
    this.mImageUrl = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRGridHeader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */