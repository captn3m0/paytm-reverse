package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROrderSummaryMerchant
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="company_name")
  private String mCompanyName;
  @b(a="display_name")
  private String mDisplayName;
  @b(a="email_id")
  private String mEmailId;
  
  public String getCompanyName()
  {
    return this.mCompanyName;
  }
  
  public String getDisplayName()
  {
    return this.mDisplayName;
  }
  
  public String getEmailId()
  {
    return this.mEmailId;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROrderSummaryMerchant.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */