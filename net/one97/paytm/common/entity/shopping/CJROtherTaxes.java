package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROtherTaxes
  implements IJRDataModel
{
  @b(a="label")
  private String mLabel;
  @b(a="value")
  private String mValue;
  
  public String getLabel()
  {
    return this.mLabel;
  }
  
  public String getValue()
  {
    return this.mValue;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROtherTaxes.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */