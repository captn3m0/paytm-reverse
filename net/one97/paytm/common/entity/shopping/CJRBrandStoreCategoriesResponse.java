package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRBrandStoreCategoriesResponse
  implements IJRDataModel
{
  @b(a="frontend_filters")
  private ArrayList<CJRBrandStoreFrontEndFilters> mFrontEndFilters;
  @b(a="result_type")
  private String mResultType;
  
  public ArrayList<CJRBrandStoreFrontEndFilters> getFrontEndFilters()
  {
    return this.mFrontEndFilters;
  }
  
  public String getResultType()
  {
    return this.mResultType;
  }
  
  public void setFrontEndFilters(ArrayList<CJRBrandStoreFrontEndFilters> paramArrayList)
  {
    this.mFrontEndFilters = paramArrayList;
  }
  
  public void setResultType(String paramString)
  {
    this.mResultType = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRBrandStoreCategoriesResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */