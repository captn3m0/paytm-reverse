package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRSearchSuggestion
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="value")
  private String mValue;
  
  public CJRSearchSuggestion(String paramString)
  {
    this.mValue = paramString;
  }
  
  public String getValue()
  {
    return this.mValue;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRSearchSuggestion.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */