package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRActionResponse
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="actions")
  private ArrayList<CJROrderSummaryAction> mActions;
  @b(a="error")
  private String mError;
  @b(a="message")
  private String mMessage;
  @b(a="status")
  private int mStatus;
  @b(a="title ")
  private String mTitle;
  
  public ArrayList<CJROrderSummaryAction> getActions()
  {
    return this.mActions;
  }
  
  public String getError()
  {
    return this.mError;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public int getStatus()
  {
    return this.mStatus;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRActionResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */