package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPendingPushResponse
  implements IJRDataModel
{
  @b(a="orderId")
  private String orderId;
  @b(a="requestGuid")
  private String requestGuid;
  @b(a="response")
  private CJRPendingPushList response;
  @b(a="status")
  private String status;
  @b(a="statusCode")
  private String statusCode;
  @b(a="statusMessage")
  private String statusMessage;
  @b(a="type")
  private String type;
  
  public String getOrderId()
  {
    return this.orderId;
  }
  
  public String getRequestGuid()
  {
    return this.requestGuid;
  }
  
  public CJRPendingPushList getResponse()
  {
    return this.response;
  }
  
  public String getStatus()
  {
    return this.status;
  }
  
  public String getStatusCode()
  {
    return this.statusCode;
  }
  
  public String getStatusMessage()
  {
    return this.statusMessage;
  }
  
  public String getType()
  {
    return this.type;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRPendingPushResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */