package net.one97.paytm.common.entity.shopping;

import android.graphics.Bitmap;
import com.google.c.a.b;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROrderedCart
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  public final String TAG_RECHARGE_NUMBER = "recharge_number";
  public final String TAG_RECHARGE_PRICE = "price";
  @b(a="attributes")
  private CJRAttribute attributes;
  @b(a="child_item_id")
  private long childItemId;
  @b(a="child_order_id")
  private long childOrderId;
  @b(a="hasReplacement")
  private int hasReplacement;
  private boolean isNPSRequested;
  private boolean isNPSTracked;
  @b(a="isReplacement")
  private int isReplacement;
  private boolean isSellerRatingUpdatedFromCache;
  @b(a="actions")
  private ArrayList<CJROrderSummaryAction> mAction;
  @b(a="aggregate_item_price")
  private String mAggregateItemPrice;
  @b(a="conv_fee")
  private String mConvFee;
  @b(a="created_at")
  private String mCreatedAt;
  @b(a="estimated_delivery_range")
  private ArrayList<String> mDates;
  @b(a="discount_amount")
  private double mDiscountAmount;
  @b(a="discount_surcharge_amount")
  private double mDiscountSurChargeAmount;
  @b(a="discount_tax_amount")
  private double mDiscountTaxAmount;
  @b(a="discounted_price")
  private double mDiscountedPrice;
  @b(a="fs_name")
  private String mFSName;
  private String mFavLabelId;
  @b(a="fulfillment_id")
  private long mFulfillmentId;
  @b(a="fulfillment_info")
  private String mFulfillmentInfo;
  @b(a="fulfillment_response")
  private String mFulfillmentResponse;
  @b(a="fulfillment_sku")
  private String mFulfillmentSKU;
  @b(a="fulfillment_status")
  private String mFulfillmentStatus;
  @b(a="fulfillment")
  private CJRFullFillmentObject mFullFillmentOject;
  @b(a="id")
  private long mId;
  @b(a="info")
  private String mInfo;
  @b(a="item_status")
  private String mItemStatus;
  @b(a="mrp")
  private double mMRP;
  @b(a="merchant_address")
  private HashMap<String, String> mMerchantAdress;
  @b(a="merchant_id")
  private long mMerchantId;
  @b(a="message")
  private String mMessage;
  @b(a="meta_data")
  public Object mMetaDataResponse;
  private Bitmap mMovieMosterBitmap;
  @b(a="name")
  private String mName;
  @b(a="price")
  private double mPrice;
  @b(a="price_incl_tax")
  private double mPriceInclTax;
  @b(a="product")
  private CJROrderSummaryProductDetail mProductDetail;
  @b(a="product_id")
  private long mProductId;
  @b(a="promo_code")
  private String mPromoCode;
  @b(a="promo_description")
  private String mPromoDescription;
  @b(a="promo_text")
  private String mPromoText;
  @b(a="qty_canceled")
  private int mQtyCanceled;
  @b(a="qty_invoiced")
  private int mQtyInvoiced;
  @b(a="qty_ordered")
  private int mQtyOrdered;
  @b(a="qty_refunded")
  private int mQtyRefunded;
  @b(a="qty_shipped")
  private int mQtyShipped;
  @b(a="quantity")
  private int mQuantity;
  private String mRatingFromCache;
  @b(a="configuration")
  private Map<String, String> mRechargeConfiguration;
  @b(a="repeat")
  private boolean mRepeat;
  @b(a="retry")
  private boolean mRetry;
  @b(a="sku")
  private String mSKU;
  @b(a="selling_price")
  private double mSellingPrice;
  @b(a="shipping_charges")
  private String mShippingCharge;
  @b(a="status")
  private String mStatus;
  @b(a="status_flow")
  private ArrayList<CJROrderSummaryStatusFlow> mStatusFlow;
  @b(a="status_text")
  private String mStatusText;
  @b(a="status_text_image")
  private String mStatusTextImage;
  @b(a="subtotal_incl_tax")
  private double mSubInclTotal;
  @b(a="subtotal")
  private double mSubTotal;
  @b(a="subscription")
  private CJROrderSummarySubscription mSubscription;
  @b(a="tap_action")
  private CJROrderSummaryAction mTapAction;
  private String mTapActionLabel;
  private String mTapActionMessage;
  private ArrayList<CJROrderSummaryAction> mTapActions;
  @b(a="tax_amount")
  private double mTaxAmount;
  @b(a="tax_data")
  private CJRTaxData mTaxData;
  @b(a="title")
  private String mTitle;
  @b(a="total_price")
  private double mTotalPrice;
  @b(a="updated_at")
  private String mUpdatedAt;
  @b(a="vertical_id")
  private long mVerticalId;
  @b(a="other_taxes")
  private ArrayList<CJROtherTaxes> otherTaxes;
  @b(a="parent_item_id")
  private long parentItemId;
  @b(a="parent_order_id")
  private long parentOrderId;
  @b(a="statusCode")
  private String statusCode;
  private int version = 0;
  
  public boolean equals(Object paramObject)
  {
    try
    {
      paramObject = ((CJROrderedCart)paramObject).mName;
      boolean bool = this.mName.equalsIgnoreCase((String)paramObject);
      return bool;
    }
    catch (Exception paramObject) {}
    return false;
  }
  
  public ArrayList<CJROrderSummaryAction> getAction()
  {
    return this.mAction;
  }
  
  public String getAggregateItemPrice()
  {
    return this.mAggregateItemPrice;
  }
  
  public CJRAttribute getAttributes()
  {
    return this.attributes;
  }
  
  public long getChildItemId()
  {
    return this.childItemId;
  }
  
  public long getChildOrderId()
  {
    return this.childOrderId;
  }
  
  public String getConvFee()
  {
    return this.mConvFee;
  }
  
  public String getCreatedAt()
  {
    return this.mCreatedAt;
  }
  
  public ArrayList<String> getDates()
  {
    return this.mDates;
  }
  
  public double getDiscountAmount()
  {
    return this.mDiscountAmount;
  }
  
  public double getDiscountSurChargeAmount()
  {
    return this.mDiscountSurChargeAmount;
  }
  
  public double getDiscountTaxAmount()
  {
    return this.mDiscountTaxAmount;
  }
  
  public double getDiscountedPrice()
  {
    return this.mDiscountedPrice;
  }
  
  public String getFSName()
  {
    return this.mFSName;
  }
  
  public String getFavouriteLabelId()
  {
    return this.mFavLabelId;
  }
  
  public long getFulfillmentId()
  {
    return this.mFulfillmentId;
  }
  
  public String getFulfillmentInfo()
  {
    return this.mFulfillmentInfo;
  }
  
  public String getFulfillmentResponse()
  {
    return this.mFulfillmentResponse;
  }
  
  public String getFulfillmentSKU()
  {
    return this.mFulfillmentSKU;
  }
  
  public String getFulfillmentStatus()
  {
    return this.mFulfillmentStatus;
  }
  
  public CJRFullFillmentObject getFullFillmentOject()
  {
    return this.mFullFillmentOject;
  }
  
  public long getId()
  {
    return this.mId;
  }
  
  public String getInfo()
  {
    return this.mInfo;
  }
  
  public boolean getIsNPSRequested()
  {
    return this.isNPSRequested;
  }
  
  public boolean getIsRepeat()
  {
    return this.mRepeat;
  }
  
  public boolean getIsRetry()
  {
    return this.mRetry;
  }
  
  public boolean getIsSellerRatingUpdatedFromCache()
  {
    return this.isSellerRatingUpdatedFromCache;
  }
  
  public String getItemStatus()
  {
    return this.mItemStatus;
  }
  
  public double getMRP()
  {
    return this.mMRP;
  }
  
  public HashMap<String, String> getMerchantAdress()
  {
    return this.mMerchantAdress;
  }
  
  public long getMerchantId()
  {
    return this.mMerchantId;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public Object getMetaDataResponse()
  {
    return this.mMetaDataResponse;
  }
  
  public Bitmap getMovieMosterBitmap()
  {
    return this.mMovieMosterBitmap;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public ArrayList<CJROtherTaxes> getOtherTaxes()
  {
    return this.otherTaxes;
  }
  
  public long getParentItemId()
  {
    return this.parentItemId;
  }
  
  public long getParentOrderId()
  {
    return this.parentOrderId;
  }
  
  public double getPrice()
  {
    return this.mPrice;
  }
  
  public double getPriceInclTax()
  {
    return this.mPriceInclTax;
  }
  
  public CJROrderSummaryProductDetail getProductDetail()
  {
    return this.mProductDetail;
  }
  
  public long getProductId()
  {
    return this.mProductId;
  }
  
  public String getPromoCode()
  {
    return this.mPromoCode;
  }
  
  public String getPromoDescription()
  {
    return this.mPromoDescription;
  }
  
  public String getPromoText()
  {
    return this.mPromoText;
  }
  
  public double getQtyCanceled()
  {
    return this.mQtyCanceled;
  }
  
  public double getQtyInvoiced()
  {
    return this.mQtyInvoiced;
  }
  
  public double getQtyOrdered()
  {
    return this.mQtyOrdered;
  }
  
  public double getQtyRefunded()
  {
    return this.mQtyRefunded;
  }
  
  public double getQtyShipped()
  {
    return this.mQtyShipped;
  }
  
  public int getQuantity()
  {
    return this.mQuantity;
  }
  
  public Map<String, String> getRechargeConfiguration()
  {
    return this.mRechargeConfiguration;
  }
  
  public String getRechargeNumber()
  {
    if (this.mRechargeConfiguration.containsKey("recharge_number")) {
      return (String)this.mRechargeConfiguration.get("recharge_number");
    }
    return new StringBuilder().toString();
  }
  
  public String getRechargePrice()
  {
    if (this.mRechargeConfiguration.containsKey("price")) {
      return (String)this.mRechargeConfiguration.get("price");
    }
    return new StringBuilder().toString();
  }
  
  public String getSKU()
  {
    return this.mSKU;
  }
  
  public String getSellerRatingFromCache()
  {
    return this.mRatingFromCache;
  }
  
  public double getSellingPrice()
  {
    return this.mSellingPrice;
  }
  
  public String getShippingCharge()
  {
    return this.mShippingCharge;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public ArrayList<CJROrderSummaryStatusFlow> getStatusFlow()
  {
    return this.mStatusFlow;
  }
  
  public String getStatusText()
  {
    return this.mStatusText;
  }
  
  public String getStatusTextImage()
  {
    return this.mStatusTextImage;
  }
  
  public double getSubInclTotal()
  {
    return this.mSubInclTotal;
  }
  
  public double getSubTotal()
  {
    return this.mSubTotal;
  }
  
  public CJROrderSummarySubscription getSubscription()
  {
    return this.mSubscription;
  }
  
  public CJROrderSummaryAction getTapAction()
  {
    return this.mTapAction;
  }
  
  public String getTapActionLabel()
  {
    return this.mTapActionLabel;
  }
  
  public String getTapActionMessage()
  {
    return this.mTapActionMessage;
  }
  
  public ArrayList<CJROrderSummaryAction> getTapActions()
  {
    return this.mTapActions;
  }
  
  public double getTaxAmount()
  {
    return this.mTaxAmount;
  }
  
  public CJRTaxData getTaxData()
  {
    return this.mTaxData;
  }
  
  public String getTitle()
  {
    return this.mTitle;
  }
  
  public double getTotalPrice()
  {
    return this.mTotalPrice;
  }
  
  public String getUpdatedAt()
  {
    return this.mUpdatedAt;
  }
  
  public int getVersion()
  {
    return this.version;
  }
  
  public long getVerticalId()
  {
    return this.mVerticalId;
  }
  
  public String getmEstimatedDelivery()
  {
    Object localObject2;
    Object localObject1;
    String str1;
    Object localObject3;
    Object localObject4;
    if ((getDates() != null) && (getDates().size() > 0))
    {
      localObject2 = null;
      localObject1 = null;
      str1 = new String();
      if (getDates().get(1) != null)
      {
        localObject3 = (String)getDates().get(1);
        localObject1 = localObject3;
        if (((String)localObject3).contains("T")) {
          localObject1 = ((String)localObject3).substring(0, ((String)localObject3).indexOf("T"));
        }
      }
      if (getDates().get(0) != null)
      {
        localObject3 = (String)getDates().get(0);
        localObject2 = localObject3;
        if (((String)localObject3).contains("T")) {
          localObject2 = ((String)localObject3).substring(0, ((String)localObject3).indexOf("T"));
        }
      }
      localObject4 = new SimpleDateFormat("yyyy-MM-dd");
      localObject3 = new SimpleDateFormat("MMM");
      if ((localObject2 == null) || (localObject1 == null)) {}
    }
    else
    {
      try
      {
        localObject1 = ((DateFormat)localObject4).parse((String)localObject1);
        localObject4 = ((DateFormat)localObject4).parse((String)localObject2);
        localObject2 = Calendar.getInstance();
        ((Calendar)localObject2).setTime((Date)localObject4);
        localObject4 = Integer.valueOf(((Calendar)localObject2).get(5));
        String str2 = ((DateFormat)localObject3).format(((Calendar)localObject2).getTime());
        ((Calendar)localObject2).setTime((Date)localObject1);
        localObject1 = Integer.valueOf(((Calendar)localObject2).get(5));
        localObject2 = ((DateFormat)localObject3).format(((Calendar)localObject2).getTime());
        if (((String)localObject2).equals(str2)) {
          return str1 + localObject4 + "-" + localObject1 + " " + (String)localObject2;
        }
        localObject1 = str1 + localObject4 + " " + str2 + "-" + localObject1 + " " + (String)localObject2;
        return (String)localObject1;
      }
      catch (Exception localException) {}
      return null;
    }
    return str1;
  }
  
  public String getstatusCode()
  {
    return this.statusCode;
  }
  
  public boolean hasReplacement()
  {
    return this.hasReplacement == 1;
  }
  
  public boolean isNPSTracked()
  {
    return this.isNPSTracked;
  }
  
  public boolean isReplacement()
  {
    return this.isReplacement == 1;
  }
  
  public void setAction(ArrayList<CJROrderSummaryAction> paramArrayList)
  {
    this.mAction = paramArrayList;
  }
  
  public void setFavouriteLabelId(String paramString)
  {
    this.mFavLabelId = paramString;
  }
  
  public void setFullFillmentOject(CJRFullFillmentObject paramCJRFullFillmentObject)
  {
    this.mFullFillmentOject = paramCJRFullFillmentObject;
  }
  
  public void setIsNPSRequested(boolean paramBoolean)
  {
    this.isNPSRequested = paramBoolean;
  }
  
  public void setIsSellerRatingUpdatedFromCache(boolean paramBoolean)
  {
    this.isSellerRatingUpdatedFromCache = paramBoolean;
  }
  
  public void setMessage(String paramString)
  {
    this.mMessage = paramString;
  }
  
  public void setMetaDataResponse(Object paramObject)
  {
    this.mMetaDataResponse = paramObject;
  }
  
  public void setMovieMosterBitmap(Bitmap paramBitmap)
  {
    this.mMovieMosterBitmap = paramBitmap;
  }
  
  public void setNPSTracked(boolean paramBoolean)
  {
    this.isNPSTracked = paramBoolean;
  }
  
  public void setSellerRatingFromCache(String paramString)
  {
    this.mRatingFromCache = paramString;
  }
  
  public void setStatus(String paramString)
  {
    this.mStatus = paramString;
  }
  
  public void setTapActionLabel(String paramString)
  {
    this.mTapActionLabel = paramString;
  }
  
  public void setTapActionMessage(String paramString)
  {
    this.mTapActionMessage = paramString;
  }
  
  public void setTapActions(ArrayList<CJROrderSummaryAction> paramArrayList)
  {
    this.mTapActions = paramArrayList;
  }
  
  public void setTaxData(CJRTaxData paramCJRTaxData)
  {
    this.mTaxData = paramCJRTaxData;
  }
  
  public void setVersion(int paramInt)
  {
    this.version = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROrderedCart.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */