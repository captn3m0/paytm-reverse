package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRSavedCard
  implements IJRDataModel
{
  public static final String CARD_XXX = "XX XXXX ";
  public static final String CREDIT_CARD = "Credit Card";
  public static final String DEBIT_CARD = "Debit Card";
  private static final long serialVersionUID = 1L;
  private boolean isChecked = false;
  @b(a="BANK_NAME")
  private String mBankname;
  @b(a="SAVE_CARD_ID")
  private String mCardID;
  @b(a="CARDBIN")
  private String mCardbin;
  @b(a="CARDLASTDIGIT")
  private String mCardlastdigit;
  @b(a="CARD_TYPE")
  private String mCardtype;
  @b(a="PAYMENTMODE")
  private String mPaymentmode;
  
  public String getBankname()
  {
    return this.mBankname;
  }
  
  public String getCardBIN()
  {
    return this.mCardbin;
  }
  
  public String getCardID()
  {
    return this.mCardID;
  }
  
  public String getCardLastDigit()
  {
    return this.mCardlastdigit;
  }
  
  public String getCardType()
  {
    return this.mCardtype;
  }
  
  public String getPaymentmode()
  {
    return this.mPaymentmode;
  }
  
  public boolean isChecked()
  {
    return this.isChecked;
  }
  
  public void setChecked(boolean paramBoolean)
  {
    this.isChecked = paramBoolean;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRSavedCard.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */