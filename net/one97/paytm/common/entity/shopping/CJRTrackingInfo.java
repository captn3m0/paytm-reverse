package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRTrackingInfo
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="container_id")
  private String mContainerID;
  @b(a="list_id_type")
  private String mListId;
  @b(a="list_name")
  private String mListName;
  @b(a="list_position")
  private int mListPosition;
  @b(a="parent_id")
  private String mParentId;
  @b(a="search_ab_value")
  private String mSearchABValue;
  @b(a="search_category")
  private String mSearchCategory;
  @b(a="search_result_type")
  private String mSearchResultType;
  @b(a="search_term")
  private String mSearchTerm;
  @b(a="search_type")
  private String mSearchType;
  
  public String getContainerID()
  {
    return this.mContainerID;
  }
  
  public String getListId()
  {
    return this.mListId;
  }
  
  public String getListName()
  {
    return this.mListName;
  }
  
  public int getListPosition()
  {
    return this.mListPosition;
  }
  
  public String getParentId()
  {
    return this.mParentId;
  }
  
  public String getSearchABValue()
  {
    return this.mSearchABValue;
  }
  
  public String getSearchCategory()
  {
    return this.mSearchCategory;
  }
  
  public String getSearchResultType()
  {
    return this.mSearchResultType;
  }
  
  public String getSearchTerm()
  {
    return this.mSearchTerm;
  }
  
  public String getSearchType()
  {
    return this.mSearchType;
  }
  
  public void setListName(String paramString)
  {
    this.mListName = paramString;
  }
  
  public void setListPosition(int paramInt)
  {
    this.mListPosition = paramInt;
  }
  
  public void setSearchABValue(String paramString)
  {
    this.mSearchABValue = paramString;
  }
  
  public void setSearchCategory(String paramString)
  {
    this.mSearchCategory = paramString;
  }
  
  public void setSearchResultType(String paramString)
  {
    this.mSearchResultType = paramString;
  }
  
  public void setSearchTerm(String paramString)
  {
    this.mSearchTerm = paramString;
  }
  
  public void setSearchType(String paramString)
  {
    this.mSearchType = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRTrackingInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */