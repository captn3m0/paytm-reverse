package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;
import org.json.JSONException;
import org.json.JSONObject;

public class CJRFullFillmentObject
  implements IJRDataModel
{
  JSONObject jsonObject;
  @b(a="fulfillment_response")
  private String mFulfillmentResponse;
  
  public String getFulfillmentResponse()
  {
    return this.mFulfillmentResponse;
  }
  
  public JSONObject getFullFillment()
  {
    if (this.mFulfillmentResponse != null) {
      try
      {
        this.jsonObject = new JSONObject(this.mFulfillmentResponse);
        return this.jsonObject;
      }
      catch (JSONException localJSONException)
      {
        for (;;)
        {
          localJSONException.printStackTrace();
        }
      }
    }
    return null;
  }
  
  public void setFulfillmentResponse(String paramString)
  {
    this.mFulfillmentResponse = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRFullFillmentObject.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */