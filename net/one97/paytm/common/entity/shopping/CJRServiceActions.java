package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;
import net.one97.paytm.common.entity.recharge.CJRDisplayValues;

public class CJRServiceActions
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="billAmount")
  private String mBillAmount;
  @b(a="billamount_editable")
  private boolean mBillAmountEditable;
  @b(a="bill_amount_max")
  private String mBillAmountMax;
  @b(a="bill_amount_min")
  private String mBillAmountMin;
  @b(a="billDueDate")
  private String mBillDueDate;
  @b(a="caNumber")
  private String mCaNumber;
  @b(a="course")
  private String mCourse;
  @b(a="customerEmail")
  private String mCustomerEmail;
  @b(a="customerName")
  private String mCustomerName;
  @b(a="displayValues")
  private ArrayList<CJRDisplayValues> mDisplayValues;
  @b(a="dob")
  private String mDob;
  @b(a="fatherName")
  private String mFatherName;
  @b(a="label")
  private String mLabel;
  @b(a="message")
  private String mMessage;
  @b(a="section")
  private String mSection;
  @b(a="studentClass")
  private String mStudentClass;
  @b(a="year")
  private String mYear;
  
  public String getBillAmount()
  {
    return this.mBillAmount;
  }
  
  public String getBillAmountMax()
  {
    return this.mBillAmountMax;
  }
  
  public String getBillAmountMin()
  {
    return this.mBillAmountMin;
  }
  
  public String getCaNumber()
  {
    return this.mCaNumber;
  }
  
  public String getCourse()
  {
    return this.mCourse;
  }
  
  public String getCustomerEmail()
  {
    return this.mCustomerEmail;
  }
  
  public String getCustomerName()
  {
    return this.mCustomerName;
  }
  
  public ArrayList<CJRDisplayValues> getDisplayValues()
  {
    return this.mDisplayValues;
  }
  
  public String getDob()
  {
    return this.mDob;
  }
  
  public String getFatherName()
  {
    return this.mFatherName;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getSection()
  {
    return this.mSection;
  }
  
  public String getStudentClass()
  {
    return this.mStudentClass;
  }
  
  public String getYear()
  {
    return this.mYear;
  }
  
  public String getmBillDueDate()
  {
    return this.mBillDueDate;
  }
  
  public String getmLabel()
  {
    return this.mLabel;
  }
  
  public boolean isBillAmountEditable()
  {
    return this.mBillAmountEditable;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRServiceActions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */