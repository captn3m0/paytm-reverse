package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRProductDescAttr
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="care")
  private String mCare;
  @b(a="dimensions")
  private String mDimensions;
  @b(a="estimated_arrival")
  private String mEstimated_arrival;
  @b(a="material")
  private String mMaterial;
  @b(a="shipping_info")
  private String mShipping_info;
  
  public String getmCare()
  {
    return this.mCare;
  }
  
  public String getmDimensions()
  {
    return this.mDimensions;
  }
  
  public String getmEstimated_arrival()
  {
    return this.mEstimated_arrival;
  }
  
  public String getmMaterial()
  {
    return this.mMaterial;
  }
  
  public String getmShipping_info()
  {
    return this.mShipping_info;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRProductDescAttr.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */