package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRShoppingCart
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="address")
  private CJRAddressList mAddress;
  @b(a="cart")
  private CJRCart mCart;
  @b(a="status")
  private CJRCartStatus mStatus;
  @b(a="code")
  private int mStatusCode;
  
  public CJRAddressList getAddress()
  {
    return this.mAddress;
  }
  
  public CJRCart getCart()
  {
    return this.mCart;
  }
  
  public CJRCartStatus getStatus()
  {
    return this.mStatus;
  }
  
  public int getStatusCode()
  {
    return this.mStatusCode;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRShoppingCart.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */