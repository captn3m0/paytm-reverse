package net.one97.paytm.common.entity.shopping;

import android.graphics.Bitmap;
import com.google.c.a.b;
import java.util.HashMap;
import net.one97.paytm.common.entity.IJRDataModel;
import net.one97.paytm.common.entity.movies.foodbeverage.CJRSummaryFoodBeveragesResponse;

public class CJROrderSummaryMetadataResponse
  implements IJRDataModel
{
  @b(a="address")
  private Object address;
  @b(a="audi")
  private String audi;
  @b(a="bookingId")
  private String bookingId;
  @b(a="bookingIndex")
  private String bookingIndex;
  @b(a="branchCode")
  private String branchCode;
  @b(a="censor")
  private String censor;
  @b(a="cinema")
  private String cinema;
  @b(a="cinemaId")
  private String cinemaId;
  @b(a="circleName")
  private String circleName;
  @b(a="citySearched")
  private String citySearched;
  @b(a="convFee")
  private String convFee;
  @b(a="duration")
  private int duration;
  @b(a="food_beverages")
  private CJRSummaryFoodBeveragesResponse foodAndBeverages;
  @b(a="freeSeating")
  private boolean freeSeating;
  @b(a="language")
  private String language;
  @b(a="latitude")
  private String latitude;
  @b(a="longitude")
  private String longitude;
  @b(a="movie")
  private String movie;
  @b(a="movieImageUrl")
  private String movieImageUrl;
  private Bitmap moviePosterImg;
  @b(a="multipleEticket")
  private String multipleEticket;
  @b(a="multipleEticketSelected")
  private Boolean multipleEticketSelected;
  @b(a="providerId")
  private String providerId;
  @b(a="screenNum")
  private String screenNum;
  @b(a="seatIdsReturned")
  private String seatIdsReturned;
  @b(a="seatType")
  private String seatType;
  @b(a="serviceTax")
  private String serviceTax;
  @b(a="sessionId")
  private String sessionId;
  @b(a="showTime")
  private String showTime;
  @b(a="source")
  private String source;
  @b(a="swachBharatCess")
  private String swachBharatCess;
  @b(a="taxInfo")
  private HashMap<String, String> taxInfo;
  @b(a="ticketCount")
  private int ticketCount;
  @b(a="totalCommision")
  private String totalCommision;
  @b(a="totalConvFee")
  private String totalConvFee;
  @b(a="totalPgCharges")
  private String totalPgCharges;
  @b(a="totalServiceTax")
  private String totalServiceTax;
  @b(a="totalSwachBharatCess")
  private String totalSwachBharatCess;
  @b(a="totalTicketPrice")
  private double totalTicketPrice;
  @b(a="transId")
  private String transId;
  @b(a="uniqueBookingId")
  private String uniqueBookingId;
  
  public String getAudi()
  {
    return this.audi;
  }
  
  public String getBookingId()
  {
    return this.bookingId;
  }
  
  public String getBookingIndex()
  {
    return this.bookingIndex;
  }
  
  public String getBranchCode()
  {
    return this.branchCode;
  }
  
  public String getCensor()
  {
    return this.censor;
  }
  
  public String getCinema()
  {
    return this.cinema;
  }
  
  public String getCinemaId()
  {
    return this.cinemaId;
  }
  
  public String getCircleName()
  {
    return this.circleName;
  }
  
  public String getCitySearched()
  {
    return this.citySearched;
  }
  
  public String getConvFee()
  {
    return this.convFee;
  }
  
  public int getDuration()
  {
    return this.duration;
  }
  
  public CJRSummaryFoodBeveragesResponse getFoodAndBeverages()
  {
    return this.foodAndBeverages;
  }
  
  public String getLanguage()
  {
    return this.language;
  }
  
  public String getLatitude()
  {
    return this.latitude;
  }
  
  public String getLongitude()
  {
    return this.longitude;
  }
  
  public String getMovie()
  {
    return this.movie;
  }
  
  public String getMovieImageUrl()
  {
    return this.movieImageUrl;
  }
  
  public Bitmap getMoviePosterImg()
  {
    return this.moviePosterImg;
  }
  
  public String getMultipleEticket()
  {
    return this.multipleEticket;
  }
  
  public Boolean getMultipleEticketSelected()
  {
    return this.multipleEticketSelected;
  }
  
  public String getProviderId()
  {
    return this.providerId;
  }
  
  public String getScreenNum()
  {
    return this.screenNum;
  }
  
  public String getSeatIdsReturned()
  {
    return this.seatIdsReturned;
  }
  
  public String getSeatType()
  {
    return this.seatType;
  }
  
  public String getServiceTax()
  {
    return this.serviceTax;
  }
  
  public String getSessionId()
  {
    return this.sessionId;
  }
  
  public String getShowTime()
  {
    return this.showTime;
  }
  
  public String getSource()
  {
    return this.source;
  }
  
  public String getStringAddress()
  {
    if ((this.address != null) && (this.address.toString() != null)) {
      return this.address.toString();
    }
    return null;
  }
  
  public String getSwachBharatCess()
  {
    return this.swachBharatCess;
  }
  
  public HashMap<String, String> getTaxInfo()
  {
    return this.taxInfo;
  }
  
  public int getTicketCount()
  {
    return this.ticketCount;
  }
  
  public String getTotalCommision()
  {
    return this.totalCommision;
  }
  
  public String getTotalConvFee()
  {
    return this.totalConvFee;
  }
  
  public String getTotalPgCharges()
  {
    return this.totalPgCharges;
  }
  
  public String getTotalServiceTax()
  {
    return this.totalServiceTax;
  }
  
  public String getTotalSwachBharatCess()
  {
    return this.totalSwachBharatCess;
  }
  
  public double getTotalTicketPrice()
  {
    return this.totalTicketPrice;
  }
  
  public String getTransId()
  {
    return this.transId;
  }
  
  public String getUniqueBookingId()
  {
    return this.uniqueBookingId;
  }
  
  public boolean isFreeSeating()
  {
    return this.freeSeating;
  }
  
  public void setAudi(String paramString)
  {
    this.audi = paramString;
  }
  
  public void setBookingId(String paramString)
  {
    this.bookingId = paramString;
  }
  
  public void setBookingIndex(String paramString)
  {
    this.bookingIndex = paramString;
  }
  
  public void setBranchCode(String paramString)
  {
    this.branchCode = paramString;
  }
  
  public void setCensor(String paramString)
  {
    this.censor = paramString;
  }
  
  public void setCinema(String paramString)
  {
    this.cinema = paramString;
  }
  
  public void setCinemaId(String paramString)
  {
    this.cinemaId = paramString;
  }
  
  public void setCitySearched(String paramString)
  {
    this.citySearched = paramString;
  }
  
  public void setConvFee(String paramString)
  {
    this.convFee = paramString;
  }
  
  public void setFoodAndBeverages(CJRSummaryFoodBeveragesResponse paramCJRSummaryFoodBeveragesResponse)
  {
    this.foodAndBeverages = paramCJRSummaryFoodBeveragesResponse;
  }
  
  public void setLanguage(String paramString)
  {
    this.language = paramString;
  }
  
  public void setMovie(String paramString)
  {
    this.movie = paramString;
  }
  
  public void setMovieImageUrl(String paramString)
  {
    this.movieImageUrl = paramString;
  }
  
  public void setMoviePosterImg(Bitmap paramBitmap)
  {
    this.moviePosterImg = paramBitmap;
  }
  
  public void setMultipleEticket(String paramString)
  {
    this.multipleEticket = paramString;
  }
  
  public void setMultipleEticketSelected(Boolean paramBoolean)
  {
    this.multipleEticketSelected = paramBoolean;
  }
  
  public void setProviderId(String paramString)
  {
    this.providerId = paramString;
  }
  
  public void setScreenNum(String paramString)
  {
    this.screenNum = paramString;
  }
  
  public void setSeatIdsReturned(String paramString)
  {
    this.seatIdsReturned = paramString;
  }
  
  public void setSeatType(String paramString)
  {
    this.seatType = paramString;
  }
  
  public void setServiceTax(String paramString)
  {
    this.serviceTax = paramString;
  }
  
  public void setSessionId(String paramString)
  {
    this.sessionId = paramString;
  }
  
  public void setShowTime(String paramString)
  {
    this.showTime = paramString;
  }
  
  public void setSource(String paramString)
  {
    this.source = paramString;
  }
  
  public void setSwachBharatCess(String paramString)
  {
    this.swachBharatCess = paramString;
  }
  
  public void setTicketCount(int paramInt)
  {
    this.ticketCount = paramInt;
  }
  
  public void setTotalCommision(String paramString)
  {
    this.totalCommision = paramString;
  }
  
  public void setTotalConvFee(String paramString)
  {
    this.totalConvFee = paramString;
  }
  
  public void setTotalServiceTax(String paramString)
  {
    this.totalServiceTax = paramString;
  }
  
  public void setTotalSwachBharatCess(String paramString)
  {
    this.totalSwachBharatCess = paramString;
  }
  
  public void setTotalTicketPrice(int paramInt)
  {
    this.totalTicketPrice = paramInt;
  }
  
  public void setTransId(String paramString)
  {
    this.transId = paramString;
  }
  
  public void setUniqueBookingId(String paramString)
  {
    this.uniqueBookingId = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROrderSummaryMetadataResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */