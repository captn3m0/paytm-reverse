package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.Iterator;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRCatalog
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  @b(a="items")
  private ArrayList<CJRCatalogItem> mCatalogItems = new ArrayList();
  @b(a="search_query_url")
  private String mSearchQueryUrl;
  
  private ArrayList<CJRCatalogItem> filterCatalogList()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mCatalogItems.iterator();
    while (localIterator.hasNext())
    {
      CJRCatalogItem localCJRCatalogItem = (CJRCatalogItem)localIterator.next();
      if (((localCJRCatalogItem.getURLType() == null) || (!localCJRCatalogItem.getURLType().equalsIgnoreCase("mobile-postpaid"))) && ((localCJRCatalogItem.getURLType() == null) || (!localCJRCatalogItem.getURLType().equalsIgnoreCase("datacard-postpaid")))) {
        localArrayList.add(localCJRCatalogItem);
      }
    }
    localArrayList.trimToSize();
    this.mCatalogItems = localArrayList;
    return localArrayList;
  }
  
  private ArrayList<CJRCatalogItem> filterCatalogList(int paramInt)
  {
    ArrayList localArrayList = new ArrayList();
    while (paramInt < this.mCatalogItems.size())
    {
      localArrayList.add(this.mCatalogItems.get(paramInt));
      paramInt += 1;
    }
    return localArrayList;
  }
  
  public ArrayList<CJRCatalogItem> getAllCatalogList()
  {
    this.mCatalogItems.trimToSize();
    return this.mCatalogItems;
  }
  
  public ArrayList<CJRCatalogItem> getCatalogList()
  {
    return filterCatalogList();
  }
  
  public ArrayList<CJRCatalogItem> getCatalogList(int paramInt)
  {
    return filterCatalogList(paramInt);
  }
  
  public CJRCatalogItem getHomeItem()
  {
    Object localObject = null;
    Iterator localIterator = this.mCatalogItems.iterator();
    while (localIterator.hasNext())
    {
      CJRCatalogItem localCJRCatalogItem = (CJRCatalogItem)localIterator.next();
      if (localCJRCatalogItem.getName().equalsIgnoreCase("Paytm Home")) {
        localObject = localCJRCatalogItem;
      }
    }
    return (CJRCatalogItem)localObject;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getSearchQueryUrl()
  {
    return this.mSearchQueryUrl;
  }
  
  public void setAllCatalogList(ArrayList<CJRCatalogItem> paramArrayList)
  {
    this.mCatalogItems.clear();
    this.mCatalogItems = paramArrayList;
  }
  
  public void setParentNameItems(ArrayList<String> paramArrayList)
  {
    if ((this.mCatalogItems != null) && (this.mCatalogItems.size() > 0))
    {
      int j = this.mCatalogItems.size();
      int i = 0;
      while (i < j)
      {
        ((CJRCatalogItem)this.mCatalogItems.get(i)).setParentNameItems(paramArrayList);
        i += 1;
      }
    }
  }
  
  public void setSearchQueryUrl(String paramString)
  {
    this.mSearchQueryUrl = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRCatalog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */