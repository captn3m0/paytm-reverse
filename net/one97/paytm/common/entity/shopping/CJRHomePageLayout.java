package net.one97.paytm.common.entity.shopping;

import android.text.TextUtils;
import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.CJRItem;

public class CJRHomePageLayout
  extends CJRItem
{
  private static final long serialVersionUID = 1L;
  private boolean isSellerStoreImpressionSent;
  private String mFooterImageURL;
  private boolean mGDREventSent;
  @b(a="header_imageurl")
  private String mHeaderImageData;
  @b(a="items")
  private ArrayList<CJRHomePageItem> mHomePageItemList = new ArrayList();
  @b(a="html")
  private String mHtml;
  @b(a="id")
  private String mID;
  private boolean mIsRecommended;
  @b(a="layout")
  private String mLayout;
  private String mListId = "";
  @b(a="name")
  private String mName;
  @b(a="see_all_url")
  private String mSeeAllUrl;
  
  public String getBrand()
  {
    return null;
  }
  
  public String getCardState()
  {
    return "";
  }
  
  public String getCategoryId()
  {
    return null;
  }
  
  public String getFooterImageURL()
  {
    return this.mFooterImageURL;
  }
  
  public String getHeaderImage()
  {
    return this.mHeaderImageData;
  }
  
  public ArrayList<CJRHomePageItem> getHomePageItemList()
  {
    int i = 0;
    while (i < this.mHomePageItemList.size())
    {
      ((CJRHomePageItem)this.mHomePageItemList.get(i)).setListId(getmID());
      i += 1;
    }
    return this.mHomePageItemList;
  }
  
  public String getHtml()
  {
    return this.mHtml;
  }
  
  public String getItemID()
  {
    return null;
  }
  
  public String getLabel()
  {
    return null;
  }
  
  public String getLayout()
  {
    return this.mLayout;
  }
  
  public String getListId()
  {
    return this.mListId;
  }
  
  public String getListName()
  {
    return null;
  }
  
  public int getListPosition()
  {
    return -1;
  }
  
  public String getName()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if (!TextUtils.isEmpty(this.mName)) {
      localStringBuilder.append(this.mName);
    }
    return localStringBuilder.toString();
  }
  
  public String getParentID()
  {
    return "";
  }
  
  public String getPrice()
  {
    return "";
  }
  
  public String getPromoCode()
  {
    return "";
  }
  
  public String getPromoText()
  {
    return "";
  }
  
  public ArrayList<CJRRelatedCategory> getRelatedCategories()
  {
    return null;
  }
  
  public String getSearchABValue()
  {
    return null;
  }
  
  public String getSearchCategory()
  {
    return null;
  }
  
  public String getSearchResultType()
  {
    return null;
  }
  
  public String getSearchTerm()
  {
    return null;
  }
  
  public String getSearchType()
  {
    return null;
  }
  
  public String getSeeAllUrl()
  {
    return this.mSeeAllUrl;
  }
  
  public String getStatusSubText()
  {
    return "";
  }
  
  public String getStatusText()
  {
    return "";
  }
  
  public String getThumbnailUrl()
  {
    return "";
  }
  
  public String getURL()
  {
    return this.mSeeAllUrl;
  }
  
  public String getURLType()
  {
    return null;
  }
  
  public String getmContainerInstanceID()
  {
    return "";
  }
  
  public String getmID()
  {
    return this.mID;
  }
  
  public boolean isExpanded()
  {
    return false;
  }
  
  public boolean isGDREventSent()
  {
    return this.mGDREventSent;
  }
  
  public boolean isKeepTryingEnabled()
  {
    return false;
  }
  
  public boolean isPendingCancelEnabled()
  {
    return false;
  }
  
  public boolean isRetryEnabled()
  {
    return false;
  }
  
  public boolean isSellerStoreImpressionSent()
  {
    return this.isSellerStoreImpressionSent;
  }
  
  public boolean ismIsRecommended()
  {
    return this.mIsRecommended;
  }
  
  public void setFooterImageURL(String paramString)
  {
    this.mFooterImageURL = paramString;
  }
  
  public void setGDREventSent()
  {
    this.mGDREventSent = true;
  }
  
  public void setIsRecommended(boolean paramBoolean)
  {
    this.mIsRecommended = paramBoolean;
  }
  
  public void setLayout(String paramString)
  {
    this.mLayout = paramString;
  }
  
  public void setListId(String paramString)
  {
    this.mListId = paramString;
  }
  
  public void setName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setSellerStoreImpressionSent(boolean paramBoolean)
  {
    this.isSellerStoreImpressionSent = paramBoolean;
  }
  
  public void setUrlType(String paramString)
  {
    this.mLayout = paramString;
  }
  
  public void setmHomePageItemList(ArrayList<CJRHomePageItem> paramArrayList)
  {
    this.mHomePageItemList = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRHomePageLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */