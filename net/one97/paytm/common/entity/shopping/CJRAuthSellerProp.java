package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRAuthSellerProp
  extends CJRDataModelItem
{
  @b(a="auth_level")
  private String mAuthLevel;
  @b(a="brand_id")
  private String mBrandId;
  @b(a="image")
  private String mImage;
  @b(a="merchant_id")
  private String mMerchantId;
  @b(a="status")
  private String mStatus;
  
  public String getAuthLevel()
  {
    return this.mAuthLevel;
  }
  
  public String getBrandId()
  {
    return this.mBrandId;
  }
  
  public String getImage()
  {
    return this.mImage;
  }
  
  public String getMerchantId()
  {
    return this.mMerchantId;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public void setImage(String paramString)
  {
    this.mImage = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRAuthSellerProp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */