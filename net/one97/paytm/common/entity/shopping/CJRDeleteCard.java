package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRDeleteCard
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="ErrorMsg")
  private String errorMessage;
  @b(a="NUMBER_OF_RECORDS")
  private int numberOfRecords;
  @b(a="STATUS")
  private String status;
  
  public String getErrorMessage()
  {
    return this.errorMessage;
  }
  
  public int getNumberOfRecords()
  {
    return this.numberOfRecords;
  }
  
  public String getStatus()
  {
    return this.status;
  }
  
  public void setErrorMessage(String paramString)
  {
    this.errorMessage = paramString;
  }
  
  public void setNumberOfRecords(int paramInt)
  {
    this.numberOfRecords = paramInt;
  }
  
  public void setStatus(String paramString)
  {
    this.status = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRDeleteCard.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */