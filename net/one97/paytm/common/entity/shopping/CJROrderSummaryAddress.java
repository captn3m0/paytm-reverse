package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROrderSummaryAddress
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="address")
  private String mAddress;
  @b(a="city")
  private String mCity;
  @b(a="country")
  private String mCountry;
  @b(a="customer_id")
  private String mCustomerId;
  @b(a="firstname")
  private String mFirstName;
  @b(a="pincode")
  private String mPinCode;
  @b(a="state")
  private String mState;
  
  public String getAddress()
  {
    return this.mAddress;
  }
  
  public String getCity()
  {
    return this.mCity;
  }
  
  public String getCountry()
  {
    return this.mCountry;
  }
  
  public String getCustomerId()
  {
    return this.mCustomerId;
  }
  
  public String getFirstName()
  {
    return this.mFirstName;
  }
  
  public String getPinCode()
  {
    return this.mPinCode;
  }
  
  public String getState()
  {
    return this.mState;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROrderSummaryAddress.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */