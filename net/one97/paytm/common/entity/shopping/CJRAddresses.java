package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAddresses
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="addressess")
  private CJRAddressList mAddress = new CJRAddressList();
  @b(a="ERROR")
  private String mError;
  @b(a="message")
  private String mMessage;
  @b(a="responseCode")
  private String mResponseCode;
  @b(a="status")
  private String mStatus;
  
  public CJRAddressList getAddress()
  {
    return this.mAddress;
  }
  
  public String getError()
  {
    return this.mError;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getResponseCode()
  {
    return this.mResponseCode;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRAddresses.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */