package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRCategoryMap
  implements IJRDataModel
{
  @b(a="id")
  private String mId;
  @b(a="name")
  private String mName;
  
  public String getCategoryId()
  {
    return this.mId;
  }
  
  public String getName()
  {
    return this.mName;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRCategoryMap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */