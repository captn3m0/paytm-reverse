package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROrderSummaryStatusFlowHistory
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="date")
  private String mDate;
  @b(a="message")
  private String mMessage;
  
  public String getStatusHistoryDate()
  {
    return this.mDate;
  }
  
  public String getStatusHistoryMessage()
  {
    return this.mMessage;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROrderSummaryStatusFlowHistory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */