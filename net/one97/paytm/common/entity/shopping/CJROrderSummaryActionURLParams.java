package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import com.google.c.l;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROrderSummaryActionURLParams
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="body")
  private l mBody;
  @b(a="method")
  private String mMethod;
  @b(a="url")
  private String mUrl;
  
  public l getBody()
  {
    return this.mBody;
  }
  
  public String getMethod()
  {
    return this.mMethod;
  }
  
  public String getUrl()
  {
    return this.mUrl;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROrderSummaryActionURLParams.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */