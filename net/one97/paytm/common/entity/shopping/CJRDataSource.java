package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRDataSource
  implements IJRDataModel
{
  @b(a="container_id")
  private String mContainerID;
  @b(a="container_instance_id")
  private String mContainerInstanceID;
  @b(a="list_id")
  private String mListId;
  @b(a="type")
  private String mType;
  
  public String getmContainerID()
  {
    return this.mContainerID;
  }
  
  public String getmContainerInstanceID()
  {
    return this.mContainerInstanceID;
  }
  
  public String getmListId()
  {
    return this.mListId;
  }
  
  public String getmType()
  {
    return this.mType;
  }
  
  public void setmContainerID(String paramString)
  {
    this.mContainerID = paramString;
  }
  
  public void setmContainerInstanceID(String paramString)
  {
    this.mContainerInstanceID = paramString;
  }
  
  public void setmListId(String paramString)
  {
    this.mListId = paramString;
  }
  
  public void setmType(String paramString)
  {
    this.mType = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRDataSource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */