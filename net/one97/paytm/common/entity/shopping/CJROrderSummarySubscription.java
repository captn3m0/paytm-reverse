package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROrderSummarySubscription
  implements IJRDataModel
{
  @b(a="is_available")
  private boolean mIsAvailable;
  @b(a="is_subscribed")
  private boolean mIsSubscribed;
  @b(a="info")
  private CJROrderSummarySubscriptionInfo mSubscriptionInfo;
  
  public boolean getIsAvailable()
  {
    return this.mIsAvailable;
  }
  
  public boolean getIsSubscribed()
  {
    return this.mIsSubscribed;
  }
  
  public CJROrderSummarySubscriptionInfo getSubscriptionInfo()
  {
    return this.mSubscriptionInfo;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJROrderSummarySubscription.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */