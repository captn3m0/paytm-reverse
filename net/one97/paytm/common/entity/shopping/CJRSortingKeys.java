package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRSortingKeys
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  private boolean isSelected;
  @b(a="default")
  private String mDefault;
  @b(a="name")
  private String mName;
  @b(a="urlParams")
  private String mUrlParams;
  
  public String getDefault()
  {
    return this.mDefault;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getUrlParams()
  {
    return this.mUrlParams;
  }
  
  public boolean isSelected()
  {
    return this.isSelected;
  }
  
  public void setDefault(String paramString)
  {
    this.mDefault = paramString;
  }
  
  public void setName(String paramString)
  {
    this.mName = paramString;
  }
  
  public void setSelected(boolean paramBoolean)
  {
    this.isSelected = paramBoolean;
  }
  
  public void setUrlParams(String paramString)
  {
    this.mUrlParams = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRSortingKeys.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */