package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRLayoutDetailV2
  implements IJRDataModel
{
  @b(a="label")
  private String label;
  @b(a="label_bgcolor")
  private String labelBgColor;
  @b(a="label_text_color")
  private String labelColor;
  
  public String getLabel()
  {
    return this.label;
  }
  
  public String getLabelBgColor()
  {
    return this.labelBgColor;
  }
  
  public String getLabelColor()
  {
    return this.labelColor;
  }
  
  public void setLabel(String paramString)
  {
    this.label = paramString;
  }
  
  public void setLabelBgColor(String paramString)
  {
    this.labelBgColor = paramString;
  }
  
  public void setLabelColor(String paramString)
  {
    this.labelColor = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRLayoutDetailV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */