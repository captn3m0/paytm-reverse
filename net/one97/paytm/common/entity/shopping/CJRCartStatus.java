package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.CJRError;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRCartStatus
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="code")
  private String mCode;
  @b(a="message")
  private CJRError mMessage;
  @b(a="result")
  private String mResult;
  
  public String getCode()
  {
    return this.mCode;
  }
  
  public CJRError getMessage()
  {
    return this.mMessage;
  }
  
  public String getResult()
  {
    return this.mResult;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRCartStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */