package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.CJRDataModelItem;

public class CJRSellerRatingList
  extends CJRDataModelItem
{
  @b(a="orders")
  private ArrayList<CJRSellerRatings> mRatingList = new ArrayList();
  
  public String getName()
  {
    return null;
  }
  
  public ArrayList<CJRSellerRatings> getRatingList()
  {
    return this.mRatingList;
  }
  
  public void setRatingList(ArrayList<CJRSellerRatings> paramArrayList)
  {
    this.mRatingList = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRSellerRatingList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */