package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class WishListTag
  implements IJRDataModel
{
  @b(a="display_name")
  private String mDisplayName;
  @b(a="name")
  private String name;
  
  public String getName()
  {
    return this.name;
  }
  
  public String getmDisplayName()
  {
    return this.mDisplayName;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setmDisplayName(String paramString)
  {
    this.mDisplayName = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/WishListTag.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */