package net.one97.paytm.common.entity.shopping;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.Iterator;
import net.one97.paytm.common.entity.brandStoreModels.CJRCategoryTree;

public class CJRHomePageDetailV2
{
  @b(a="badge_image_url")
  private String mBadgeImageUrl;
  private String mFooterImageURL;
  @b(a="views")
  public ArrayList<CJRHomePageLayoutV2> mHomePageLayoutList = new ArrayList();
  @b(a="slots")
  private ArrayList<CJRHomePageSlotItemV2> mHomePageSlotItemList = new ArrayList();
  @b(a="id")
  private String mId;
  @b(a="layout")
  private String mLayout;
  @b(a="layout_details")
  private CJRLayoutDetailV2 mLayoutDetails;
  @b(a="mobile_layout")
  public ArrayList<CJRHomePageLayoutV2> mMobileLayout = new ArrayList();
  @b(a="attributes")
  private CJRLayoutDetailV2 mattribute;
  @b(a="category_tree")
  private CJRCategoryTree mcategorytree;
  
  public String getFooterImageURL()
  {
    return this.mFooterImageURL;
  }
  
  public ArrayList<CJRHomePageLayoutV2> getHomePageLayoutList()
  {
    return this.mHomePageLayoutList;
  }
  
  public ArrayList<CJRHomePageItem> getItemsForLayout(String paramString)
  {
    Object localObject2 = null;
    Iterator localIterator = this.mHomePageLayoutList.iterator();
    do
    {
      localObject1 = localObject2;
      if (!localIterator.hasNext()) {
        break;
      }
      localObject1 = (CJRHomePageLayoutV2)localIterator.next();
    } while (!((CJRHomePageLayoutV2)localObject1).getLayout().equalsIgnoreCase(paramString));
    Object localObject1 = ((CJRHomePageLayoutV2)localObject1).getHomePageItemList();
    return (ArrayList<CJRHomePageItem>)localObject1;
  }
  
  public ArrayList<CJRHomePageItem> getItemsForProduct(String paramString)
  {
    Object localObject2 = null;
    Iterator localIterator = this.mHomePageLayoutList.iterator();
    do
    {
      localObject1 = localObject2;
      if (!localIterator.hasNext()) {
        break;
      }
      localObject1 = (CJRHomePageLayoutV2)localIterator.next();
    } while (!((CJRHomePageLayoutV2)localObject1).getName().equalsIgnoreCase(paramString));
    Object localObject1 = ((CJRHomePageLayoutV2)localObject1).getHomePageItemList();
    return (ArrayList<CJRHomePageItem>)localObject1;
  }
  
  public CJRHomePageLayoutV2 getLayoutFromItem(CJRHomePageItem paramCJRHomePageItem)
  {
    try
    {
      Iterator localIterator = this.mHomePageLayoutList.iterator();
      while (localIterator.hasNext())
      {
        CJRHomePageLayoutV2 localCJRHomePageLayoutV2 = (CJRHomePageLayoutV2)localIterator.next();
        Object localObject = localCJRHomePageLayoutV2.getHomePageItemList();
        if ((localObject != null) && (paramCJRHomePageItem != null) && (((ArrayList)localObject).contains(paramCJRHomePageItem)))
        {
          localObject = (CJRHomePageItem)((ArrayList)localObject).get(((ArrayList)localObject).indexOf(paramCJRHomePageItem));
          if ((localObject != null) && (((CJRHomePageItem)localObject).getURLType() != null) && (paramCJRHomePageItem.getURLType() != null))
          {
            boolean bool = ((CJRHomePageItem)localObject).getURLType().equalsIgnoreCase(paramCJRHomePageItem.getURLType());
            if (bool) {
              return localCJRHomePageLayoutV2;
            }
          }
        }
      }
    }
    catch (Exception paramCJRHomePageItem)
    {
      paramCJRHomePageItem.printStackTrace();
    }
    return null;
  }
  
  public CJRCategoryTree getMcategorytree()
  {
    return this.mcategorytree;
  }
  
  public ArrayList<CJRHomePageLayoutV2> getPromotionImpression()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mHomePageLayoutList.iterator();
    while (localIterator.hasNext())
    {
      CJRHomePageLayoutV2 localCJRHomePageLayoutV2 = (CJRHomePageLayoutV2)localIterator.next();
      LayoutType localLayoutType = LayoutType.fromName(localCJRHomePageLayoutV2.getLayout());
      switch (CJRHomePageDetailV2.1.$SwitchMap$net$one97$paytm$common$entity$shopping$LayoutType[localLayoutType.ordinal()])
      {
      case 10: 
      case 11: 
      case 12: 
      case 13: 
      case 14: 
      default: 
        break;
      case 4: 
      case 5: 
      case 6: 
      case 7: 
      case 8: 
      case 9: 
      case 15: 
        if (localCJRHomePageLayoutV2.getHomePageItemList().size() > 0) {
          localArrayList.add(localCJRHomePageLayoutV2);
        }
        break;
      }
    }
    return localArrayList;
  }
  
  public String getmBadgeImageUrl()
  {
    return this.mBadgeImageUrl;
  }
  
  public ArrayList<CJRHomePageSlotItemV2> getmHomePageSlotItemList()
  {
    return this.mHomePageSlotItemList;
  }
  
  public String getmId()
  {
    return this.mId;
  }
  
  public String getmLayout()
  {
    return this.mLayout;
  }
  
  public ArrayList<CJRHomePageLayoutV2> pageRowItems(boolean paramBoolean)
  {
    ArrayList localArrayList = new ArrayList();
    CJRHomePageLayoutV2 localCJRHomePageLayoutV2;
    if (paramBoolean)
    {
      localIterator = this.mMobileLayout.iterator();
      while (localIterator.hasNext())
      {
        localCJRHomePageLayoutV2 = (CJRHomePageLayoutV2)localIterator.next();
        if ((localCJRHomePageLayoutV2.getLayout().equalsIgnoreCase("tabs")) && (localCJRHomePageLayoutV2.getHomePageItemList().size() > 0)) {
          localArrayList.add(localCJRHomePageLayoutV2);
        }
      }
    }
    Iterator localIterator = this.mHomePageLayoutList.iterator();
    while (localIterator.hasNext())
    {
      localCJRHomePageLayoutV2 = (CJRHomePageLayoutV2)localIterator.next();
      if (localCJRHomePageLayoutV2.getLayout() != null)
      {
        LayoutType localLayoutType = LayoutType.fromName(localCJRHomePageLayoutV2.getLayout());
        switch (CJRHomePageDetailV2.1.$SwitchMap$net$one97$paytm$common$entity$shopping$LayoutType[localLayoutType.ordinal()])
        {
        default: 
          break;
        case 1: 
        case 2: 
        case 3: 
        case 4: 
        case 5: 
        case 6: 
        case 7: 
        case 8: 
        case 9: 
        case 10: 
        case 11: 
        case 12: 
        case 13: 
        case 14: 
          if (localCJRHomePageLayoutV2.getHomePageItemList().size() > 0) {
            localArrayList.add(localCJRHomePageLayoutV2);
          }
          break;
        case 15: 
        case 16: 
        case 17: 
        case 18: 
        case 19: 
        case 20: 
        case 21: 
        case 22: 
        case 23: 
        case 24: 
        case 25: 
        case 26: 
        case 27: 
          localArrayList.add(localCJRHomePageLayoutV2);
        }
      }
    }
    return localArrayList;
  }
  
  public void setFooterImageURL(String paramString)
  {
    this.mFooterImageURL = paramString;
  }
  
  public void setHomePageLayoutList(ArrayList<CJRHomePageLayoutV2> paramArrayList)
  {
    this.mHomePageLayoutList = paramArrayList;
  }
  
  public void setLayout(String paramString)
  {
    this.mLayout = paramString;
  }
  
  public void setMcategorytree(CJRCategoryTree paramCJRCategoryTree)
  {
    this.mcategorytree = paramCJRCategoryTree;
  }
  
  public void setmBadgeImageUrl(String paramString)
  {
    this.mBadgeImageUrl = paramString;
  }
  
  public void setmHomePageLayoutList(CJRHomePageLayoutV2 paramCJRHomePageLayoutV2)
  {
    this.mHomePageLayoutList.add(paramCJRHomePageLayoutV2);
  }
  
  public void setmHomePageSlotItemList(ArrayList<CJRHomePageSlotItemV2> paramArrayList)
  {
    this.mHomePageSlotItemList = paramArrayList;
  }
  
  public void setmId(String paramString)
  {
    this.mId = paramString;
  }
  
  public void setmLayout(String paramString)
  {
    this.mLayout = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/shopping/CJRHomePageDetailV2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */