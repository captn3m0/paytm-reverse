package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRAccessToken
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="access_token")
  private String mAccessToken;
  @b(a="error")
  private String mError;
  @b(a="error_description")
  private String mErrorDescription;
  @b(a="scope")
  private String mScope;
  @b(a="tokenType")
  private String mTokenType;
  
  public String getAccessToken()
  {
    return this.mAccessToken;
  }
  
  public String getError()
  {
    return this.mError;
  }
  
  public String getErrorDescription()
  {
    return this.mErrorDescription;
  }
  
  public String getScope()
  {
    return this.mScope;
  }
  
  public String getTokenType()
  {
    return this.mTokenType;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRAccessToken.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */