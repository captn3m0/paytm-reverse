package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJRRegisteredData
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="code")
  private String mCode;
  @b(a="email")
  private String mEmail;
  @b(a="message")
  private String mMessage;
  @b(a="mobile")
  private String mMobile;
  @b(a="responseCode")
  private String mResponseCode;
  @b(a="signupToken")
  private String mSignupToken;
  @b(a="state")
  private String mState;
  @b(a="status")
  private String mStatus;
  
  public String getEmail()
  {
    return this.mEmail;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getMobile()
  {
    return this.mMobile;
  }
  
  public String getResponseCode()
  {
    return this.mResponseCode;
  }
  
  public String getSignupToken()
  {
    return this.mSignupToken;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRRegisteredData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */