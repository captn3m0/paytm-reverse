package net.one97.paytm.common.entity;

import com.google.c.a.b;

public class CJROrderItems
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="name")
  private String mName;
  @b(a="price")
  private String mPrice;
  @b(a="product")
  private CJROrderItemProduct mProduct;
  @b(a="quantity")
  private String mQuantity;
  @b(a="status")
  private String mStatus;
  @b(a="status_text")
  private String mStatusText;
  @b(a="subtotal")
  private String mSubTotal;
  @b(a="total_price")
  private String mTotalPrice;
  
  public String getName()
  {
    return this.mName;
  }
  
  public String getPrice()
  {
    return this.mPrice;
  }
  
  public CJROrderItemProduct getProduct()
  {
    return this.mProduct;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public String getStatusText()
  {
    return this.mStatusText;
  }
  
  public String getSubTotal()
  {
    return this.mSubTotal;
  }
  
  public String getTotalPrice()
  {
    return this.mTotalPrice;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJROrderItems.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */