package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFlightDetailsItem
  implements Cloneable, IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="airline")
  private String mAirLine;
  @b(a="airlineCode")
  private String mAirLineCode;
  @b(a="arrivalTimeLocal")
  private String mArrivalTime;
  @b(a="bookingClass")
  private String mBookingClass;
  @b(a="departureTimeLocal")
  private String mDepartureTime;
  @b(a="destination")
  private String mDestination;
  @b(a="discountedFlights")
  private ArrayList<CJRDiscountedFlights> mDiscountedFlights;
  @b(a="duration")
  private String mDuration;
  @b(a="flightid")
  private String mFlightId;
  @b(a="hops")
  private ArrayList<CJRFlightStops> mFlights;
  @b(a="insurance")
  private String mInsurance;
  @b(a="refundable")
  private boolean mIsRefundable;
  @b(a="origin")
  private String mOrigin;
  @b(a="otherinfo")
  private List<String> mOtherinfo;
  @b(a="price")
  private ArrayList<CJRFlightPrice> mPrice;
  private CJRFlightPrice mServiceProviderSelected;
  
  public CJRFlightDetailsItem clone()
    throws CloneNotSupportedException
  {
    return (CJRFlightDetailsItem)super.clone();
  }
  
  public String getmAirLine()
  {
    return this.mAirLine;
  }
  
  public String getmAirLineCode()
  {
    return this.mAirLineCode;
  }
  
  public String getmArrivalTime()
  {
    return this.mArrivalTime;
  }
  
  public String getmBookingClass()
  {
    return this.mBookingClass;
  }
  
  public String getmDepartureTime()
  {
    return this.mDepartureTime;
  }
  
  public String getmDestination()
  {
    return this.mDestination;
  }
  
  public ArrayList<CJRDiscountedFlights> getmDiscountedFlights()
  {
    return this.mDiscountedFlights;
  }
  
  public String getmDuration()
  {
    return this.mDuration;
  }
  
  public String getmFlightId()
  {
    return this.mFlightId;
  }
  
  public ArrayList<CJRFlightStops> getmFlights()
  {
    return this.mFlights;
  }
  
  public String getmInsurance()
  {
    return this.mInsurance;
  }
  
  public String getmOrigin()
  {
    return this.mOrigin;
  }
  
  public String getmOtherinfo()
  {
    String str1 = "";
    String str2 = str1;
    if (this.mOtherinfo != null)
    {
      int i = 0;
      for (;;)
      {
        str2 = str1;
        if (i >= this.mOtherinfo.size()) {
          break;
        }
        str1 = str1 + (String)this.mOtherinfo.get(i) + ". ";
        i += 1;
      }
    }
    return str2;
  }
  
  public ArrayList<CJRFlightPrice> getmPrice()
  {
    return this.mPrice;
  }
  
  public CJRFlightPrice getmServiceProviderSelected()
  {
    return this.mServiceProviderSelected;
  }
  
  public boolean ismIsRefundable()
  {
    return this.mIsRefundable;
  }
  
  public void setmAirLine(String paramString)
  {
    this.mAirLine = paramString;
  }
  
  public void setmAirLineCode(String paramString)
  {
    this.mAirLineCode = paramString;
  }
  
  public void setmArrivalTime(String paramString)
  {
    this.mArrivalTime = paramString;
  }
  
  public void setmBookingClass(String paramString)
  {
    this.mBookingClass = paramString;
  }
  
  public void setmDepartureTime(String paramString)
  {
    this.mDepartureTime = paramString;
  }
  
  public void setmDestination(String paramString)
  {
    this.mDestination = paramString;
  }
  
  public void setmDiscountedFlights(ArrayList<CJRDiscountedFlights> paramArrayList)
  {
    this.mDiscountedFlights = paramArrayList;
  }
  
  public void setmDuration(String paramString)
  {
    this.mDuration = paramString;
  }
  
  public void setmFlightId(String paramString)
  {
    this.mFlightId = paramString;
  }
  
  public void setmFlights(ArrayList<CJRFlightStops> paramArrayList)
  {
    this.mFlights = paramArrayList;
  }
  
  public void setmInsurance(String paramString)
  {
    this.mInsurance = paramString;
  }
  
  public void setmIsRefundable(boolean paramBoolean)
  {
    this.mIsRefundable = paramBoolean;
  }
  
  public void setmOrigin(String paramString)
  {
    this.mOrigin = paramString;
  }
  
  public void setmOtherinfo(List<String> paramList)
  {
    this.mOtherinfo = paramList;
  }
  
  public void setmPrice(ArrayList<CJRFlightPrice> paramArrayList)
  {
    this.mPrice = paramArrayList;
  }
  
  public void setmServiceProviderSelected(CJRFlightPrice paramCJRFlightPrice)
  {
    this.mServiceProviderSelected = paramCJRFlightPrice;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRFlightDetailsItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */