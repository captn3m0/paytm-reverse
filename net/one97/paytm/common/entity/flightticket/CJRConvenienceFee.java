package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.a;
import com.google.c.a.b;
import java.util.HashMap;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRConvenienceFee
  implements IJRDataModel
{
  @a
  @b(a="body")
  private HashMap<String, Object> body;
  private HashMap<String, Object> mConvenienceFeeMap;
  
  public HashMap<String, Object> getBody()
  {
    return this.body;
  }
  
  public HashMap<String, Object> getmConvenienceFeeMap()
  {
    return this.mConvenienceFeeMap;
  }
  
  public void setBody(HashMap<String, Object> paramHashMap)
  {
    this.body = paramHashMap;
  }
  
  public void setmConvenienceFeeMap(HashMap<String, Object> paramHashMap)
  {
    this.mConvenienceFeeMap = paramHashMap;
  }
  
  public class Body
    implements IJRDataModel
  {
    @a
    @b(a="ezeego")
    private CJRConvenienceFee.Provider ezeego;
    @a
    @b(a="goibibo")
    private CJRConvenienceFee.Provider goibibo;
    
    public Body() {}
    
    public CJRConvenienceFee.Provider getEzeego()
    {
      return this.ezeego;
    }
    
    public CJRConvenienceFee.Provider getGoibibo()
    {
      return this.goibibo;
    }
    
    public void setEzeego(CJRConvenienceFee.Provider paramProvider)
    {
      this.ezeego = paramProvider;
    }
    
    public void setGoibibo(CJRConvenienceFee.Provider paramProvider)
    {
      this.goibibo = paramProvider;
    }
  }
  
  public class Provider
    implements IJRDataModel
  {
    @a
    @b(a="oneway")
    private Integer oneway;
    @a
    @b(a="twoway")
    private Integer twoway;
    
    public Provider() {}
    
    public Integer getOneway()
    {
      return this.oneway;
    }
    
    public Integer getTwoway()
    {
      return this.twoway;
    }
    
    public void setOneway(Integer paramInteger)
    {
      this.oneway = paramInteger;
    }
    
    public void setTwoway(Integer paramInteger)
    {
      this.twoway = paramInteger;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRConvenienceFee.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */