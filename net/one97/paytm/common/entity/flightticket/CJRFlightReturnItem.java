package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFlightReturnItem
  implements IJRDataModel
{
  @b(a="flights")
  private List<CJRFlightItem> flights = new ArrayList();
  @b(a="refundable")
  private Boolean refundable;
  @b(a="seller")
  private String seller;
  @b(a="type")
  private String type;
  
  public List<CJRFlightItem> getFlights()
  {
    return this.flights;
  }
  
  public Boolean getRefundable()
  {
    return this.refundable;
  }
  
  public String getSeller()
  {
    return this.seller;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public void setFlights(List<CJRFlightItem> paramList)
  {
    this.flights = paramList;
  }
  
  public void setRefundable(Boolean paramBoolean)
  {
    this.refundable = paramBoolean;
  }
  
  public void setSeller(String paramString)
  {
    this.seller = paramString;
  }
  
  public void setType(String paramString)
  {
    this.type = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRFlightReturnItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */