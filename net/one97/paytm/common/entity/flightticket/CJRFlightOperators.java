package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFlightOperators
  implements IJRDataModel
{
  @b(a="imgUrl")
  private String imgUrl;
  @b(a="name")
  private String name;
  
  public CJRFlightOperators(String paramString)
  {
    this.name = paramString;
  }
  
  public String getImgUrl()
  {
    return this.imgUrl;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public void setImgUrl(String paramString)
  {
    this.imgUrl = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public String toString()
  {
    return "ClassPojo [imgUrl = " + this.imgUrl + ", name = " + this.name + "]";
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRFlightOperators.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */