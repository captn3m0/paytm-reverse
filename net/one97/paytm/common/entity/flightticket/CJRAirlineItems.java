package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAirlineItems
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  private boolean isSelected;
  @b(a="name")
  private String mAirlineName;
  @b(a="iata")
  private String mIATA;
  @b(a="website")
  private String mWebsite;
  
  public String getmAirlineName()
  {
    return this.mAirlineName;
  }
  
  public String getmIATA()
  {
    return this.mIATA;
  }
  
  public String getmWebsite()
  {
    return this.mWebsite;
  }
  
  public boolean isSelected()
  {
    return this.isSelected;
  }
  
  public void setSelected(boolean paramBoolean)
  {
    this.isSelected = paramBoolean;
  }
  
  public void setmAirlineName(String paramString)
  {
    this.mAirlineName = paramString;
  }
  
  public void setmIATA(String paramString)
  {
    this.mIATA = paramString;
  }
  
  public void setmWebsite(String paramString)
  {
    this.mWebsite = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRAirlineItems.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */