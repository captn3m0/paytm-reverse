package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFlightSearchResult
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="error")
  private String mError;
  private ArrayList<CJRIntlFlightList> mInternationalFlightList = new ArrayList();
  @b(a="meta")
  private CJRMetadetails mMetaDetails;
  @b(a="body")
  private CJROnwardReturnFlightInformation mOnwardReturnFlights;
  @b(a="status")
  private CJRStatus mStatus;
  
  private void addMapping(List<Integer> paramList1, List<Integer> paramList2, CJRIntlFlightPricing paramCJRIntlFlightPricing)
  {
    int k = paramList1.size();
    int m = paramList2.size();
    int i = 0;
    while (i < k)
    {
      int n = ((Integer)paramList1.get(i)).intValue();
      int j = 0;
      for (;;)
      {
        if (j >= m) {
          break label307;
        }
        int i1 = ((Integer)paramList2.get(j)).intValue();
        Object localObject1 = null;
        Object localObject2 = null;
        try
        {
          localObject3 = ((CJRFlightDetailsItem)this.mOnwardReturnFlights.getmOnwardFlights().getmFlightDetails().get(n)).clone();
          localObject1 = localObject3;
          localObject4 = ((CJRFlightDetailsItem)this.mOnwardReturnFlights.getmReturnFlights().getmFlightDetails().get(i1)).clone();
          localObject2 = localObject4;
          localObject1 = localObject3;
        }
        catch (CloneNotSupportedException localCloneNotSupportedException)
        {
          for (;;)
          {
            Object localObject3;
            Object localObject4;
            String str;
            ArrayList localArrayList;
            CJRFlightPrice localCJRFlightPrice;
            localCloneNotSupportedException.printStackTrace();
          }
        }
        localObject3 = new CJRIntlFlightList();
        if ((localObject1 != null) && (localObject2 != null))
        {
          localObject4 = paramCJRIntlFlightPricing.getTotalfare();
          str = paramCJRIntlFlightPricing.getProvider();
          localArrayList = new ArrayList();
          localCJRFlightPrice = new CJRFlightPrice();
          localCJRFlightPrice.setmFlightCost((String)localObject4);
          localCJRFlightPrice.setmServiceProvider(str);
          localArrayList.add(localCJRFlightPrice);
          ((CJRFlightDetailsItem)localObject1).setmPrice(localArrayList);
          localArrayList = new ArrayList();
          localCJRFlightPrice = new CJRFlightPrice();
          localCJRFlightPrice.setmFlightCost("0");
          localCJRFlightPrice.setmServiceProvider(str);
          localArrayList.add(localCJRFlightPrice);
          ((CJRFlightDetailsItem)localObject2).setmPrice(localArrayList);
          ((CJRIntlFlightList)localObject3).setmOnwardFlights((CJRFlightDetailsItem)localObject1);
          ((CJRIntlFlightList)localObject3).setmReturnFlights((CJRFlightDetailsItem)localObject2);
          ((CJRIntlFlightList)localObject3).setmTotalPrice((String)localObject4);
          this.mInternationalFlightList.add(localObject3);
        }
        j += 1;
      }
      label307:
      i += 1;
    }
  }
  
  public String getmError()
  {
    return this.mError;
  }
  
  public ArrayList<CJRIntlFlightList> getmInternationalFlightList()
  {
    if ((this.mInternationalFlightList == null) || (this.mInternationalFlightList.size() == 0)) {
      return new ArrayList();
    }
    return this.mInternationalFlightList;
  }
  
  public CJRMetadetails getmMetaDetails()
  {
    return this.mMetaDetails;
  }
  
  public CJROnwardReturnFlightInformation getmOnwardReturnFlights()
  {
    return this.mOnwardReturnFlights;
  }
  
  public CJRStatus getmStatus()
  {
    return this.mStatus;
  }
  
  public void setmError(String paramString)
  {
    this.mError = paramString;
  }
  
  public void setmInternationalFlightList()
  {
    this.mInternationalFlightList = new ArrayList();
    if ((this.mOnwardReturnFlights != null) && (this.mOnwardReturnFlights.getmMapping() != null) && (this.mOnwardReturnFlights.getmMapping().size() > 0))
    {
      int j = this.mOnwardReturnFlights.getmMapping().size();
      int i = 0;
      while (i < j)
      {
        Object localObject = (CJRIntlFlightMapping)this.mOnwardReturnFlights.getmMapping().get(i);
        if (localObject != null)
        {
          List localList1 = ((CJRIntlFlightMapping)localObject).getOnward();
          List localList2 = ((CJRIntlFlightMapping)localObject).getReturn();
          localObject = ((CJRIntlFlightMapping)localObject).getPricing();
          if ((localObject != null) && (localList1 != null) && (localList2 != null) && (localList1.size() > 0) && (localList2.size() > 0)) {
            addMapping(localList1, localList2, (CJRIntlFlightPricing)localObject);
          }
        }
        i += 1;
      }
    }
  }
  
  public void setmMetaDetails(CJRMetadetails paramCJRMetadetails)
  {
    this.mMetaDetails = paramCJRMetadetails;
  }
  
  public void setmOnwardReturnFlights(CJROnwardReturnFlightInformation paramCJROnwardReturnFlightInformation)
  {
    this.mOnwardReturnFlights = paramCJROnwardReturnFlightInformation;
  }
  
  public void setmStatus(CJRStatus paramCJRStatus)
  {
    this.mStatus = paramCJRStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRFlightSearchResult.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */