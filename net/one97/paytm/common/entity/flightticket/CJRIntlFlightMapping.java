package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;

public class CJRIntlFlightMapping
{
  @b(a="return")
  private List<Integer> _return = new ArrayList();
  @b(a="onward")
  private List<Integer> onward = new ArrayList();
  @b(a="pricing")
  private CJRIntlFlightPricing pricing;
  
  public List<Integer> getOnward()
  {
    return this.onward;
  }
  
  public CJRIntlFlightPricing getPricing()
  {
    return this.pricing;
  }
  
  public List<Integer> getReturn()
  {
    return this._return;
  }
  
  public void setOnward(List<Integer> paramList)
  {
    this.onward = paramList;
  }
  
  public void setPricing(CJRIntlFlightPricing paramCJRIntlFlightPricing)
  {
    this.pricing = paramCJRIntlFlightPricing;
  }
  
  public void setReturn(List<Integer> paramList)
  {
    this._return = paramList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRIntlFlightMapping.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */