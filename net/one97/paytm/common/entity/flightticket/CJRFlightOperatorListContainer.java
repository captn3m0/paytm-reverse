package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFlightOperatorListContainer
  implements IJRDataModel
{
  @b(a="code")
  private String code;
  @b(a="body")
  private CJRFlightOperatorList flightOperatorList;
  @b(a="status")
  private CJRStatus status;
  
  public String getCode()
  {
    return this.code;
  }
  
  public CJRFlightOperatorList getFlightOperatorList()
  {
    return this.flightOperatorList;
  }
  
  public CJRStatus getStatus()
  {
    return this.status;
  }
  
  public void setCode(String paramString)
  {
    this.code = paramString;
  }
  
  public void setFlightOperatorList(CJRFlightOperatorList paramCJRFlightOperatorList)
  {
    this.flightOperatorList = paramCJRFlightOperatorList;
  }
  
  public void setStatus(CJRStatus paramCJRStatus)
  {
    this.status = paramCJRStatus;
  }
  
  public String toString()
  {
    return "CJRFlightOperatorListContainer [flightOperatorList = " + this.flightOperatorList + ", status = " + this.status + ", code = " + this.code + "]";
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRFlightOperatorListContainer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */