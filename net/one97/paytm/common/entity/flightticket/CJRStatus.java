package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRStatus
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="message")
  private CJRFlightMessage mFlightMesssgae;
  @b(a="result")
  private String mResult;
  
  public static long getSerialVersionUID()
  {
    return 1L;
  }
  
  public CJRFlightMessage getmFlightMesssgae()
  {
    return this.mFlightMesssgae;
  }
  
  public String getmResult()
  {
    return this.mResult;
  }
  
  public void setmFlightMesssgae(CJRFlightMessage paramCJRFlightMessage)
  {
    this.mFlightMesssgae = paramCJRFlightMessage;
  }
  
  public void setmResult(String paramString)
  {
    this.mResult = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */