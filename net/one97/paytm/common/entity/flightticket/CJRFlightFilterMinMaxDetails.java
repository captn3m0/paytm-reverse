package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFlightFilterMinMaxDetails
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="airlines")
  private String[] mAirlines;
  @b(a="duration")
  private CJRFilterMinMaxItem mDuration;
  @b(a="price")
  private CJRFilterMinMaxItem mPrice;
  @b(a="provider_order")
  private ArrayList<String> mServiceProviderName;
  @b(a="stops")
  private CJRFilterMinMaxItem mStops;
  
  public String[] getmAirlines()
  {
    return this.mAirlines;
  }
  
  public CJRFilterMinMaxItem getmDuration()
  {
    return this.mDuration;
  }
  
  public CJRFilterMinMaxItem getmPrice()
  {
    return this.mPrice;
  }
  
  public ArrayList<String> getmServiceProviderName()
  {
    return this.mServiceProviderName;
  }
  
  public CJRFilterMinMaxItem getmStops()
  {
    return this.mStops;
  }
  
  public void setmAirlines(String[] paramArrayOfString)
  {
    this.mAirlines = paramArrayOfString;
  }
  
  public void setmDuration(CJRFilterMinMaxItem paramCJRFilterMinMaxItem)
  {
    this.mDuration = paramCJRFilterMinMaxItem;
  }
  
  public void setmPrice(CJRFilterMinMaxItem paramCJRFilterMinMaxItem)
  {
    this.mPrice = paramCJRFilterMinMaxItem;
  }
  
  public void setmServiceProviderName(ArrayList<String> paramArrayList)
  {
    this.mServiceProviderName = paramArrayList;
  }
  
  public void setmStops(CJRFilterMinMaxItem paramCJRFilterMinMaxItem)
  {
    this.mStops = paramCJRFilterMinMaxItem;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRFlightFilterMinMaxDetails.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */