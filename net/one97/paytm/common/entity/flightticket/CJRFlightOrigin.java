package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFlightOrigin
  implements IJRDataModel
{
  @b(a="airport")
  private String flightOriginAirport;
  @b(a="city")
  private String flightOriginCity;
  @b(a="iata")
  private String flightOriginCode;
  
  public String getFlightOriginAirport()
  {
    return this.flightOriginAirport;
  }
  
  public String getFlightOriginCity()
  {
    return this.flightOriginCity;
  }
  
  public String getFlightOriginCode()
  {
    return this.flightOriginCode;
  }
  
  public void setFlightOriginAirport(String paramString)
  {
    this.flightOriginAirport = paramString;
  }
  
  public void setFlightOriginCity(String paramString)
  {
    this.flightOriginCity = paramString;
  }
  
  public void setFlightOriginCode(String paramString)
  {
    this.flightOriginCode = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRFlightOrigin.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */