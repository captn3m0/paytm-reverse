package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFlightTaxBreakupDetail
  implements IJRDataModel
{
  @b(a="Fuel Surcharge")
  private double mFuelCharge;
  @b(a="Krishi Kalyan Cess")
  private double mKrishiKalyanCess;
  @b(a="Passenger Service Fare")
  private double mPassengerServiceFare;
  @b(a="SVCT")
  private double mSvct;
  @b(a="Swachh Bharat Cess")
  private double mSwachBharatCess;
  @b(a="User Development Fee")
  private double mUserDevFee;
  
  public double getmFuelCharge()
  {
    return this.mFuelCharge;
  }
  
  public double getmKrishiKalyanCess()
  {
    return this.mKrishiKalyanCess;
  }
  
  public double getmPassengerServiceFare()
  {
    return this.mPassengerServiceFare;
  }
  
  public double getmSvct()
  {
    return this.mSvct;
  }
  
  public double getmSwachBharatCess()
  {
    return this.mSwachBharatCess;
  }
  
  public double getmUserDevFee()
  {
    return this.mUserDevFee;
  }
  
  public void setmFuelCharge(double paramDouble)
  {
    this.mFuelCharge = paramDouble;
  }
  
  public void setmKrishiKalyanCess(double paramDouble)
  {
    this.mKrishiKalyanCess = paramDouble;
  }
  
  public void setmPassengerServiceFare(double paramDouble)
  {
    this.mPassengerServiceFare = paramDouble;
  }
  
  public void setmSvct(double paramDouble)
  {
    this.mSvct = paramDouble;
  }
  
  public void setmSwachBharatCess(double paramDouble)
  {
    this.mSwachBharatCess = paramDouble;
  }
  
  public void setmUserDevFee(double paramDouble)
  {
    this.mUserDevFee = paramDouble;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRFlightTaxBreakupDetail.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */