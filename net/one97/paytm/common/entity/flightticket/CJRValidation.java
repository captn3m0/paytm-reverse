package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRValidation
  implements IJRDataModel
{
  @b(a="after")
  private String after;
  @b(a="before")
  private String before;
  @b(a="key")
  private String key;
  private String mValue;
  @b(a="max_length")
  private Integer maxLength;
  @b(a="min_length")
  private Integer minLength;
  @b(a="name")
  private String name;
  @b(a="note")
  private String note;
  @b(a="type")
  private String type;
  @b(a="values")
  private List<CJRValue> values = new ArrayList();
  
  public String getAfter()
  {
    return this.after;
  }
  
  public String getBefore()
  {
    return this.before;
  }
  
  public String getKey()
  {
    return this.key;
  }
  
  public Integer getMaxLength()
  {
    return this.maxLength;
  }
  
  public Integer getMinLength()
  {
    return this.minLength;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getNote()
  {
    return this.note;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public List<CJRValue> getValues()
  {
    return this.values;
  }
  
  public String getmValue()
  {
    return this.mValue;
  }
  
  public void setAfter(String paramString)
  {
    this.after = paramString;
  }
  
  public void setBefore(String paramString)
  {
    this.before = paramString;
  }
  
  public void setKey(String paramString)
  {
    this.key = paramString;
  }
  
  public void setMaxLength(Integer paramInteger)
  {
    this.maxLength = paramInteger;
  }
  
  public void setMinLength(Integer paramInteger)
  {
    this.minLength = paramInteger;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setNote(String paramString)
  {
    this.note = paramString;
  }
  
  public void setType(String paramString)
  {
    this.type = paramString;
  }
  
  public void setValues(List<CJRValue> paramList)
  {
    this.values = paramList;
  }
  
  public void setmValue(String paramString)
  {
    this.mValue = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRValidation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */