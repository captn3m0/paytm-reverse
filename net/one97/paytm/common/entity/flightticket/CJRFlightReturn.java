package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFlightReturn
  implements IJRDataModel
{
  @b(a="airline")
  private String airline;
  @b(a="airline_code")
  private String airlineCode;
  
  public String getAirline()
  {
    return this.airline;
  }
  
  public String getAirlineCode()
  {
    return this.airlineCode;
  }
  
  public void setAirline(String paramString)
  {
    this.airline = paramString;
  }
  
  public void setAirlineCode(String paramString)
  {
    this.airlineCode = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRFlightReturn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */