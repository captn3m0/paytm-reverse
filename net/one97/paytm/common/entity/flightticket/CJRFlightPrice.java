package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFlightPrice
  implements Cloneable, IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="baggage")
  private String mBaggage;
  @b(a="price")
  private String mFlightCost;
  @b(a="provider")
  private String mServiceProvider;
  
  public CJRFlightPrice clone()
    throws CloneNotSupportedException
  {
    return (CJRFlightPrice)super.clone();
  }
  
  public String getmBaggage()
  {
    return this.mBaggage;
  }
  
  public String getmFlightCost()
  {
    return this.mFlightCost;
  }
  
  public String getmServiceProvider()
  {
    return this.mServiceProvider;
  }
  
  public void setmBaggage(String paramString)
  {
    this.mBaggage = paramString;
  }
  
  public void setmFlightCost(String paramString)
  {
    this.mFlightCost = paramString;
  }
  
  public void setmServiceProvider(String paramString)
  {
    this.mServiceProvider = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRFlightPrice.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */