package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPassenger
  implements IJRDataModel
{
  @b(a="tooltip")
  private String tooltip;
  @b(a="validations")
  private List<CJRValidation> validations = new ArrayList();
  
  public String getTooltip()
  {
    return this.tooltip;
  }
  
  public List<CJRValidation> getValidations()
  {
    return this.validations;
  }
  
  public void setTooltip(String paramString)
  {
    this.tooltip = paramString;
  }
  
  public void setValidations(List<CJRValidation> paramList)
  {
    this.validations = paramList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRPassenger.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */