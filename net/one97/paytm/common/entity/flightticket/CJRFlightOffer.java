package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFlightOffer
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="error")
  private String mError;
  @b(a="body")
  private CJROffers mOffers;
  @b(a="status")
  private CJRStatus mStatus;
  
  public String getmError()
  {
    return this.mError;
  }
  
  public CJROffers getmOffers()
  {
    return this.mOffers;
  }
  
  public CJRStatus getmStatus()
  {
    return this.mStatus;
  }
  
  public void setmError(String paramString)
  {
    this.mError = paramString;
  }
  
  public void setmOffers(CJROffers paramCJROffers)
  {
    this.mOffers = paramCJROffers;
  }
  
  public void setmStatus(CJRStatus paramCJRStatus)
  {
    this.mStatus = paramCJRStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRFlightOffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */