package net.one97.paytm.common.entity.flightticket.insurance;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRInsuranceModel
  implements IJRDataModel
{
  @b(a="body")
  private CJRInsuranceBody body;
  @b(a="code")
  private Integer code;
  @b(a="error")
  private Object error;
  @b(a="meta")
  private CJRInsuranceMeta meta;
  @b(a="status")
  private CJRInsuranceStatus status;
  
  public CJRInsuranceBody getBody()
  {
    return this.body;
  }
  
  public Integer getCode()
  {
    return this.code;
  }
  
  public Object getError()
  {
    return this.error;
  }
  
  public CJRInsuranceMeta getMeta()
  {
    return this.meta;
  }
  
  public CJRInsuranceStatus getStatus()
  {
    return this.status;
  }
  
  public void setBody(CJRInsuranceBody paramCJRInsuranceBody)
  {
    this.body = paramCJRInsuranceBody;
  }
  
  public void setCode(Integer paramInteger)
  {
    this.code = paramInteger;
  }
  
  public void setError(Object paramObject)
  {
    this.error = paramObject;
  }
  
  public void setMeta(CJRInsuranceMeta paramCJRInsuranceMeta)
  {
    this.meta = paramCJRInsuranceMeta;
  }
  
  public void setStatus(CJRInsuranceStatus paramCJRInsuranceStatus)
  {
    this.status = paramCJRInsuranceStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/insurance/CJRInsuranceModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */