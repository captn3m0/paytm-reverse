package net.one97.paytm.common.entity.flightticket.insurance;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRInsuranceBody
  implements IJRDataModel
{
  @b(a="items")
  private List<CJRInsuranceItem> items = new ArrayList();
  
  public List<CJRInsuranceItem> getItems()
  {
    return this.items;
  }
  
  public void setItems(List<CJRInsuranceItem> paramList)
  {
    this.items = paramList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/insurance/CJRInsuranceBody.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */