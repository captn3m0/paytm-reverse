package net.one97.paytm.common.entity.flightticket.insurance;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRInsuranceItem
  implements IJRDataModel
{
  @b(a="details")
  private String details;
  @b(a="id")
  private Integer id;
  @b(a="info")
  private Object info;
  @b(a="max_claim")
  private Integer maxClaim;
  @b(a="name")
  private String name;
  @b(a="price")
  private Integer price;
  @b(a="tc")
  private String tc;
  @b(a="text_to_display")
  private String textToDisplay;
  @b(a="totalPrice")
  private Integer totalPrice;
  @b(a="type")
  private String type;
  
  public String getDetails()
  {
    return this.details;
  }
  
  public Integer getId()
  {
    return this.id;
  }
  
  public Object getInfo()
  {
    return this.info;
  }
  
  public Integer getMaxClaim()
  {
    return this.maxClaim;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public Integer getPrice()
  {
    return this.price;
  }
  
  public String getTc()
  {
    return this.tc;
  }
  
  public String getTextToDisplay()
  {
    return this.textToDisplay;
  }
  
  public Integer getTotalPrice()
  {
    return this.totalPrice;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public void setDetails(String paramString)
  {
    this.details = paramString;
  }
  
  public void setId(Integer paramInteger)
  {
    this.id = paramInteger;
  }
  
  public void setInfo(Object paramObject)
  {
    this.info = paramObject;
  }
  
  public void setMaxClaim(Integer paramInteger)
  {
    this.maxClaim = paramInteger;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setPrice(Integer paramInteger)
  {
    this.price = paramInteger;
  }
  
  public void setTc(String paramString)
  {
    this.tc = paramString;
  }
  
  public void setTextToDisplay(String paramString)
  {
    this.textToDisplay = paramString;
  }
  
  public void setTotalPrice(Integer paramInteger)
  {
    this.totalPrice = paramInteger;
  }
  
  public void setType(String paramString)
  {
    this.type = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/insurance/CJRInsuranceItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */