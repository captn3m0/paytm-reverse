package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRPaxInfo
  implements IJRDataModel
{
  @b(a="adult")
  private CJRPassenger adult;
  @b(a="child")
  private CJRPassenger child;
  @b(a="infant")
  private CJRPassenger infant;
  
  public CJRPassenger getAdult()
  {
    return this.adult;
  }
  
  public CJRPassenger getChild()
  {
    return this.child;
  }
  
  public CJRPassenger getInfant()
  {
    return this.infant;
  }
  
  public void setAdult(CJRPassenger paramCJRPassenger)
  {
    this.adult = paramCJRPassenger;
  }
  
  public void setChild(CJRPassenger paramCJRPassenger)
  {
    this.child = paramCJRPassenger;
  }
  
  public void setInfant(CJRPassenger paramCJRPassenger)
  {
    this.infant = paramCJRPassenger;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRPaxInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */