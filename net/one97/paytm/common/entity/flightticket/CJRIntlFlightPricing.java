package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;

public class CJRIntlFlightPricing
{
  @b(a="provider")
  private String provider;
  @b(a="totalfare")
  private String totalfare;
  
  public String getProvider()
  {
    return this.provider;
  }
  
  public String getTotalfare()
  {
    return this.totalfare;
  }
  
  public void setProvider(String paramString)
  {
    this.provider = paramString;
  }
  
  public void setTotalfare(String paramString)
  {
    this.totalfare = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRIntlFlightPricing.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */