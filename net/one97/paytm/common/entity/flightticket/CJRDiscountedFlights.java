package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRDiscountedFlights
  implements Cloneable, IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="flightid")
  private String mDiscountedFlightID;
  @b(a="totalfare")
  private double mDiscountedFlightTotalFare;
  @b(a="totaltax")
  private double mDiscountedFlightTotalTax;
  
  public CJRDiscountedFlights clone()
    throws CloneNotSupportedException
  {
    return (CJRDiscountedFlights)super.clone();
  }
  
  public String getmDiscountedFlightID()
  {
    return this.mDiscountedFlightID;
  }
  
  public double getmDiscountedFlightTotalFare()
  {
    return this.mDiscountedFlightTotalFare;
  }
  
  public double getmDiscountedFlightTotalTax()
  {
    return this.mDiscountedFlightTotalTax;
  }
  
  public void setmDiscountedFlightID(String paramString)
  {
    this.mDiscountedFlightID = paramString;
  }
  
  public void setmDiscountedFlightTotalFare(double paramDouble)
  {
    this.mDiscountedFlightTotalFare = paramDouble;
  }
  
  public void setmDiscountedFlightTotalTax(double paramDouble)
  {
    this.mDiscountedFlightTotalTax = paramDouble;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRDiscountedFlights.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */