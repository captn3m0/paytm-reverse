package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFlightStops
  implements Cloneable, IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="aircraftType")
  private String mAirCraftType;
  @b(a="airline")
  private String mAirLine;
  @b(a="airlineCode")
  private String mAirLineCode;
  @b(a="amenities")
  private List<String> mAmenities;
  @b(a="arrivalTimeLocal")
  private String mArrivalTime;
  @b(a="baggage")
  private String mBaggage;
  @b(a="class")
  private String mClass;
  @b(a="departureTimeLocal")
  private String mDepartureTime;
  @b(a="destination")
  private String mDestination;
  @b(a="destination_airport")
  private String mDestinationAirport;
  @b(a="destination_city")
  private String mDestinationCity;
  @b(a="duration")
  private String mDuration;
  @b(a="flightNumber")
  private String mFlightNumber;
  @b(a="inclusions")
  private List<String> mInclusions;
  @b(a="layover")
  private String mLayover;
  @b(a="meal")
  private String mMeal;
  @b(a="notes")
  private List<String> mNotes;
  @b(a="origin")
  private String mOrigin;
  @b(a="origin_airport")
  private String mOriginAirport;
  @b(a="origin_city")
  private String mOriginCity;
  
  public CJRFlightStops clone()
    throws CloneNotSupportedException
  {
    return (CJRFlightStops)super.clone();
  }
  
  public String getDestinationAirport()
  {
    return this.mDestinationAirport;
  }
  
  public String getDestinationCity()
  {
    return this.mDestinationCity;
  }
  
  public String getOriginAirport()
  {
    return this.mOriginAirport;
  }
  
  public String getOriginCity()
  {
    return this.mOriginCity;
  }
  
  public String getmAirCraftType()
  {
    return this.mAirCraftType;
  }
  
  public String getmAirLine()
  {
    return this.mAirLine;
  }
  
  public String getmAirLineCode()
  {
    return this.mAirLineCode;
  }
  
  public String getmAmenities()
  {
    String str1 = "";
    String str2 = str1;
    if (this.mAmenities != null)
    {
      int i = 0;
      for (;;)
      {
        str2 = str1;
        if (i >= this.mAmenities.size()) {
          break;
        }
        str1 = str1 + (String)this.mAmenities.get(i) + ". ";
        i += 1;
      }
    }
    return str2;
  }
  
  public String getmArrivalTime()
  {
    return this.mArrivalTime;
  }
  
  public String getmClass()
  {
    return this.mClass;
  }
  
  public String getmDepartureTime()
  {
    return this.mDepartureTime;
  }
  
  public String getmDestination()
  {
    return this.mDestination;
  }
  
  public String getmDuration()
  {
    return this.mDuration;
  }
  
  public String getmFlightNumber()
  {
    return this.mFlightNumber;
  }
  
  public String getmInclusions()
  {
    String str1 = "";
    String str2 = str1;
    if (this.mInclusions != null)
    {
      int i = 0;
      for (;;)
      {
        str2 = str1;
        if (i >= this.mInclusions.size()) {
          break;
        }
        str1 = str1 + (String)this.mInclusions.get(i) + ". ";
        i += 1;
      }
    }
    return str2;
  }
  
  public String getmLayover()
  {
    return this.mLayover;
  }
  
  public String getmMeal()
  {
    return this.mMeal;
  }
  
  public String getmNotes()
  {
    String str1 = "";
    String str2 = str1;
    if (this.mNotes != null)
    {
      int i = 0;
      for (;;)
      {
        str2 = str1;
        if (i >= this.mNotes.size()) {
          break;
        }
        str1 = str1 + (String)this.mNotes.get(i) + ". ";
        i += 1;
      }
    }
    return str2.trim();
  }
  
  public String getmOrigin()
  {
    return this.mOrigin;
  }
  
  public void setmAirCraftType(String paramString)
  {
    this.mAirCraftType = paramString;
  }
  
  public void setmAirLine(String paramString)
  {
    this.mAirLine = paramString;
  }
  
  public void setmAirLineCode(String paramString)
  {
    this.mAirLineCode = paramString;
  }
  
  public void setmAmenities(List<String> paramList)
  {
    this.mAmenities = paramList;
  }
  
  public void setmArrivalTime(String paramString)
  {
    this.mArrivalTime = paramString;
  }
  
  public void setmClass(String paramString)
  {
    this.mClass = paramString;
  }
  
  public void setmDepartureTime(String paramString)
  {
    this.mDepartureTime = paramString;
  }
  
  public void setmDestination(String paramString)
  {
    this.mDestination = paramString;
  }
  
  public void setmDuration(String paramString)
  {
    this.mDuration = paramString;
  }
  
  public void setmFlightNumber(String paramString)
  {
    this.mFlightNumber = paramString;
  }
  
  public void setmInclusions(List<String> paramList)
  {
    this.mInclusions = paramList;
  }
  
  public void setmLayover(String paramString)
  {
    this.mLayover = paramString;
  }
  
  public void setmMeal(String paramString)
  {
    this.mMeal = paramString;
  }
  
  public void setmNotes(List<String> paramList)
  {
    this.mNotes = paramList;
  }
  
  public void setmOrigin(String paramString)
  {
    this.mOrigin = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRFlightStops.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */