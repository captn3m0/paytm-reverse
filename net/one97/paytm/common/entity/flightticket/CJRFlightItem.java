package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFlightItem
  implements IJRDataModel
{
  @b(a="class")
  private String _class;
  @b(a="airline")
  private String airline;
  @b(a="airline_code")
  private String airlineCode;
  @b(a="arrival_time")
  private String arrivalTime;
  @b(a="arrival_time_local")
  private String arrivalTimeLocal;
  @b(a="departure_time")
  private String departureTime;
  @b(a="departure_time_local")
  private String departureTimeLocal;
  @b(a="destination")
  private CJRFlightDestination destination;
  @b(a="duration")
  private String duration;
  @b(a="airline_color_code")
  private String flightColorCode;
  @b(a="flight_no")
  private String flightNo;
  @b(a="layover")
  private String layover;
  @b(a="origin")
  private CJRFlightOrigin origin;
  @b(a="terminal")
  private String terminal;
  
  public String getAirline()
  {
    return this.airline;
  }
  
  public String getAirlineCode()
  {
    return this.airlineCode;
  }
  
  public String getArrivalTime()
  {
    return this.arrivalTime;
  }
  
  public String getArrivalTimeLocal()
  {
    return this.arrivalTimeLocal;
  }
  
  public String getDepartureTime()
  {
    return this.departureTime;
  }
  
  public String getDepartureTimeLocal()
  {
    return this.departureTimeLocal;
  }
  
  public CJRFlightDestination getDestination()
  {
    return this.destination;
  }
  
  public String getDuration()
  {
    return this.duration;
  }
  
  public String getFlightColorCode()
  {
    return this.flightColorCode;
  }
  
  public String getFlightNo()
  {
    return this.flightNo;
  }
  
  public String getLayover()
  {
    return this.layover;
  }
  
  public CJRFlightOrigin getOrigin()
  {
    return this.origin;
  }
  
  public String getTerminal()
  {
    return this.terminal;
  }
  
  public String get_class()
  {
    return this._class;
  }
  
  public void setAirline(String paramString)
  {
    this.airline = paramString;
  }
  
  public void setAirlineCode(String paramString)
  {
    this.airlineCode = paramString;
  }
  
  public void setArrivalTime(String paramString)
  {
    this.arrivalTime = paramString;
  }
  
  public void setArrivalTimeLocal(String paramString)
  {
    this.arrivalTimeLocal = paramString;
  }
  
  public void setDepartureTime(String paramString)
  {
    this.departureTime = paramString;
  }
  
  public void setDepartureTimeLocal(String paramString)
  {
    this.departureTimeLocal = paramString;
  }
  
  public void setDestination(CJRFlightDestination paramCJRFlightDestination)
  {
    this.destination = paramCJRFlightDestination;
  }
  
  public void setDuration(String paramString)
  {
    this.duration = paramString;
  }
  
  public void setFlightColorCode(String paramString)
  {
    this.flightColorCode = paramString;
  }
  
  public void setFlightNo(String paramString)
  {
    this.flightNo = paramString;
  }
  
  public void setLayover(String paramString)
  {
    this.layover = paramString;
  }
  
  public void setOrigin(CJRFlightOrigin paramCJRFlightOrigin)
  {
    this.origin = paramCJRFlightOrigin;
  }
  
  public void setTerminal(String paramString)
  {
    this.terminal = paramString;
  }
  
  public void set_class(String paramString)
  {
    this._class = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRFlightItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */