package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROnwardFlights
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="flights")
  private ArrayList<CJRFlightDetailsItem> mFlightDetails;
  @b(a="length")
  private String mLength;
  @b(a="meta")
  private CJRFlightFilterMinMaxDetails mMeta;
  @b(a="timestamp")
  private String mOnwardFlightTimeStamp;
  @b(a="flight")
  private CJRFlightDetailsItem mRepriceFlight;
  @b(a="provider")
  private String mRepriceProvider;
  
  public ArrayList<CJRFlightDetailsItem> getmFlightDetails()
  {
    return this.mFlightDetails;
  }
  
  public String getmLength()
  {
    return this.mLength;
  }
  
  public CJRFlightFilterMinMaxDetails getmMeta()
  {
    return this.mMeta;
  }
  
  public String getmOnwardFlightTimeStamp()
  {
    return this.mOnwardFlightTimeStamp;
  }
  
  public CJRFlightDetailsItem getmRepriceFlight()
  {
    return this.mRepriceFlight;
  }
  
  public String getmRepriceProvider()
  {
    return this.mRepriceProvider;
  }
  
  public void setmFlightDetails(ArrayList<CJRFlightDetailsItem> paramArrayList)
  {
    this.mFlightDetails = paramArrayList;
  }
  
  public void setmLength(String paramString)
  {
    this.mLength = paramString;
  }
  
  public void setmMeta(CJRFlightFilterMinMaxDetails paramCJRFlightFilterMinMaxDetails)
  {
    this.mMeta = paramCJRFlightFilterMinMaxDetails;
  }
  
  public void setmOnwardFlightTimeStamp(String paramString)
  {
    this.mOnwardFlightTimeStamp = paramString;
  }
  
  public void setmRepriceFlight(CJRFlightDetailsItem paramCJRFlightDetailsItem)
  {
    this.mRepriceFlight = paramCJRFlightDetailsItem;
  }
  
  public void setmRepriceProvider(String paramString)
  {
    this.mRepriceProvider = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJROnwardFlights.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */