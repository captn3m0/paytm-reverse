package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAirlines
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="body")
  private List<CJRAirlineItems> mAirlineItems;
  @b(a="error")
  private String mError;
  @b(a="status")
  private CJRStatus mStatus;
  
  public List<CJRAirlineItems> getmAirlineItems()
  {
    return this.mAirlineItems;
  }
  
  public String getmError()
  {
    return this.mError;
  }
  
  public CJRStatus getmStatus()
  {
    return this.mStatus;
  }
  
  public void setmAirlineItems(List<CJRAirlineItems> paramList)
  {
    this.mAirlineItems = paramList;
  }
  
  public void setmError(String paramString)
  {
    this.mError = paramString;
  }
  
  public void setmStatus(CJRStatus paramCJRStatus)
  {
    this.mStatus = paramCJRStatus;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRAirlines.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */