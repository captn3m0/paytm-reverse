package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRMeta
  implements IJRDataModel
{
  @b(a="pax_info")
  private CJRPaxInfo CJRPaxInfo;
  @b(a="notes")
  private List<String> notes = new ArrayList();
  
  public CJRPaxInfo getCJRPaxInfo()
  {
    return this.CJRPaxInfo;
  }
  
  public List<String> getNotes()
  {
    return this.notes;
  }
  
  public void setCJRPaxInfo(CJRPaxInfo paramCJRPaxInfo)
  {
    this.CJRPaxInfo = paramCJRPaxInfo;
  }
  
  public void setNotes(List<String> paramList)
  {
    this.notes = paramList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRMeta.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */