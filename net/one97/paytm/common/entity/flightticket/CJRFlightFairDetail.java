package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFlightFairDetail
  implements IJRDataModel
{
  @b(a="convenience_fee")
  private int mConvenience;
  @b(a="tax")
  private int mTax;
  @b(a="price")
  private int mTotalPrice;
  @b(a="tax_break_up")
  private CJRFlightTaxBreakupDetail texBreakup;
  
  public CJRFlightTaxBreakupDetail getTexBreakup()
  {
    return this.texBreakup;
  }
  
  public int getmConvenience()
  {
    return this.mConvenience;
  }
  
  public int getmTax()
  {
    return this.mTax;
  }
  
  public int getmTotalPrice()
  {
    return this.mTotalPrice;
  }
  
  public void setTexBreakup(CJRFlightTaxBreakupDetail paramCJRFlightTaxBreakupDetail)
  {
    this.texBreakup = paramCJRFlightTaxBreakupDetail;
  }
  
  public void setmConvenience(int paramInt)
  {
    this.mConvenience = paramInt;
  }
  
  public void setmTax(int paramInt)
  {
    this.mTax = paramInt;
  }
  
  public void setmTotalPrice(int paramInt)
  {
    this.mTotalPrice = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRFlightFairDetail.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */