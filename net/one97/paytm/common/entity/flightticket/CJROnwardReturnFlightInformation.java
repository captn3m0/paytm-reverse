package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJROnwardReturnFlightInformation
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="discounted")
  private Object discountObject;
  @b(a="journey_type")
  private String journey_type;
  @b(a="meta")
  private CJRMeta mCJRMeta;
  @b(a="mapping")
  private List<CJRIntlFlightMapping> mMapping;
  @b(a="onwardflights")
  private CJROnwardFlights mOnwardFlights;
  @b(a="fare")
  private CJRRepriceFlightFare mRepriceFare;
  @b(a="onward_flight")
  private CJROnwardFlights mRepriceOnwardFlight;
  @b(a="return_flight")
  private CJROnwardFlights mRepriceReturnFlight;
  @b(a="returnflights")
  private CJROnwardFlights mReturnFlights;
  
  public Object getDiscountObject()
  {
    return this.discountObject;
  }
  
  public String getJourney_type()
  {
    return this.journey_type;
  }
  
  public CJRMeta getmCJRMeta()
  {
    return this.mCJRMeta;
  }
  
  public List<CJRIntlFlightMapping> getmMapping()
  {
    return this.mMapping;
  }
  
  public CJROnwardFlights getmOnwardFlights()
  {
    return this.mOnwardFlights;
  }
  
  public CJRRepriceFlightFare getmRepriceFare()
  {
    return this.mRepriceFare;
  }
  
  public CJROnwardFlights getmRepriceOnwardFlight()
  {
    return this.mRepriceOnwardFlight;
  }
  
  public CJROnwardFlights getmRepriceReturnFlight()
  {
    return this.mRepriceReturnFlight;
  }
  
  public CJROnwardFlights getmReturnFlights()
  {
    return this.mReturnFlights;
  }
  
  public void setDiscountObject(Object paramObject)
  {
    this.discountObject = paramObject;
  }
  
  public void setJourney_type(String paramString)
  {
    this.journey_type = paramString;
  }
  
  public void setmCJRMeta(CJRMeta paramCJRMeta)
  {
    this.mCJRMeta = paramCJRMeta;
  }
  
  public void setmMapping(List<CJRIntlFlightMapping> paramList)
  {
    this.mMapping = paramList;
  }
  
  public void setmOnwardFlights(CJROnwardFlights paramCJROnwardFlights)
  {
    this.mOnwardFlights = paramCJROnwardFlights;
  }
  
  public void setmRepriceFare(CJRRepriceFlightFare paramCJRRepriceFlightFare)
  {
    this.mRepriceFare = paramCJRRepriceFlightFare;
  }
  
  public void setmRepriceOnwardFlight(CJROnwardFlights paramCJROnwardFlights)
  {
    this.mRepriceOnwardFlight = paramCJROnwardFlights;
  }
  
  public void setmRepriceReturnFlight(CJROnwardFlights paramCJROnwardFlights)
  {
    this.mRepriceReturnFlight = paramCJROnwardFlights;
  }
  
  public void setmReturnFlights(CJROnwardFlights paramCJROnwardFlights)
  {
    this.mReturnFlights = paramCJROnwardFlights;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJROnwardReturnFlightInformation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */