package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import java.io.Serializable;

public class CJRHOPSItem
  implements Serializable
{
  @b(a="departureTime")
  private String departureTime;
  @b(a="aircraftType")
  private String mAircraftType;
  @b(a="airlineCode")
  private String mAirlineCode;
  @b(a="arrivalTime")
  private String mArrivalTime;
  @b(a="class")
  private String mClass;
  @b(a="destination")
  private String mDestination;
  @b(a="duration")
  private String mDuration;
  @b(a="flightNumber")
  private String mFlightNumber;
  @b(a="origin")
  private String mOrigin;
  
  public String getDepartureTime()
  {
    return this.departureTime;
  }
  
  public String getmAircraftType()
  {
    return this.mAircraftType;
  }
  
  public String getmAirlineCode()
  {
    return this.mAirlineCode;
  }
  
  public String getmArrivalTime()
  {
    return this.mArrivalTime;
  }
  
  public String getmClass()
  {
    return this.mClass;
  }
  
  public String getmDestination()
  {
    return this.mDestination;
  }
  
  public String getmDuration()
  {
    return this.mDuration;
  }
  
  public String getmFlightNumber()
  {
    return this.mFlightNumber;
  }
  
  public String getmOrigin()
  {
    return this.mOrigin;
  }
  
  public void setDepartureTime(String paramString)
  {
    this.departureTime = paramString;
  }
  
  public void setmAircraftType(String paramString)
  {
    this.mAircraftType = paramString;
  }
  
  public void setmAirlineCode(String paramString)
  {
    this.mAirlineCode = paramString;
  }
  
  public void setmArrivalTime(String paramString)
  {
    this.mArrivalTime = paramString;
  }
  
  public void setmClass(String paramString)
  {
    this.mClass = paramString;
  }
  
  public void setmDestination(String paramString)
  {
    this.mDestination = paramString;
  }
  
  public void setmDuration(String paramString)
  {
    this.mDuration = paramString;
  }
  
  public void setmFlightNumber(String paramString)
  {
    this.mFlightNumber = paramString;
  }
  
  public void setmOrigin(String paramString)
  {
    this.mOrigin = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRHOPSItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */