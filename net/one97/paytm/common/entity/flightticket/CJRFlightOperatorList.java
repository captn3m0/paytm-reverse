package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFlightOperatorList
  implements IJRDataModel
{
  @b(a="oneway")
  private ArrayList<CJRFlightOperators> oneway;
  @b(a="twoway")
  private ArrayList<CJRFlightOperators> twoway;
  
  public ArrayList<CJRFlightOperators> getOnewayOperators()
  {
    return this.oneway;
  }
  
  public ArrayList<CJRFlightOperators> getTwowayOperators()
  {
    return this.twoway;
  }
  
  public void setOneway(ArrayList<CJRFlightOperators> paramArrayList)
  {
    this.oneway = paramArrayList;
  }
  
  public void setTwoway(ArrayList<CJRFlightOperators> paramArrayList)
  {
    this.twoway = paramArrayList;
  }
  
  public String toString()
  {
    return "CJRFlightOperatorList [twoway = " + this.twoway + ", oneway = " + this.oneway + "]";
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRFlightOperatorList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */