package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import java.io.Serializable;

public class CJRAirportCityItem
  implements Serializable, Cloneable
{
  public static final int ITEM_TYPE_HEADER = 0;
  public static final int ITEM_TYPE_RECENT_CITY = 1;
  public static final int ITEM_TYPE_SEARCH_CITY = 2;
  private static final long serialVersionUID = 1L;
  @b(a="airport")
  private String airPortName;
  @b(a="city")
  private String cityName;
  @b(a="country")
  private String countryName;
  private int itemType = 2;
  @b(a="iata")
  private String shortCityName;
  
  public CJRAirportCityItem clone()
    throws CloneNotSupportedException
  {
    return (CJRAirportCityItem)super.clone();
  }
  
  public String getAirPortName()
  {
    return this.airPortName;
  }
  
  public String getCityName()
  {
    return this.cityName;
  }
  
  public String getCountryName()
  {
    return this.countryName;
  }
  
  public int getItemType()
  {
    return this.itemType;
  }
  
  public String getShortCityName()
  {
    return this.shortCityName;
  }
  
  public void setAirPortName(String paramString)
  {
    this.airPortName = paramString;
  }
  
  public void setCityName(String paramString)
  {
    this.cityName = paramString;
  }
  
  public void setCountryName(String paramString)
  {
    this.countryName = paramString;
  }
  
  public void setItemType(int paramInt)
  {
    this.itemType = paramInt;
  }
  
  public void setShortCityName(String paramString)
  {
    this.shortCityName = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRAirportCityItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */