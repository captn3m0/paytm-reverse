package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFlightDestination
  implements IJRDataModel
{
  @b(a="airport")
  private String flightDestinationAirport;
  @b(a="city")
  private String flightDestinationCity;
  @b(a="iata")
  private String flightDestinationCode;
  
  public String getFlightDestinationAirport()
  {
    return this.flightDestinationAirport;
  }
  
  public String getFlightDestinationCity()
  {
    return this.flightDestinationCity;
  }
  
  public String getFlightDestinationCode()
  {
    return this.flightDestinationCode;
  }
  
  public void setFlightDestinationAirport(String paramString)
  {
    this.flightDestinationAirport = paramString;
  }
  
  public void setFlightDestinationCity(String paramString)
  {
    this.flightDestinationCity = paramString;
  }
  
  public void setFlightDestinationCode(String paramString)
  {
    this.flightDestinationCode = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRFlightDestination.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */