package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRAirportCity
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  private int cityItemType;
  @b(a="body")
  private ArrayList<CJRAirportCityItem> mAirportCityItems;
  private Long mServerResponseTime;
  
  public int getCityItemType()
  {
    return this.cityItemType;
  }
  
  public ArrayList<CJRAirportCityItem> getmAirportCityItems()
  {
    return this.mAirportCityItems;
  }
  
  public Long getmServerResponseTime()
  {
    return this.mServerResponseTime;
  }
  
  public void setCityItemType(int paramInt)
  {
    this.cityItemType = paramInt;
  }
  
  public void setmAirportCityItems(ArrayList<CJRAirportCityItem> paramArrayList)
  {
    this.mAirportCityItems = paramArrayList;
  }
  
  public void setmServerResponseTime(Long paramLong)
  {
    this.mServerResponseTime = paramLong;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRAirportCity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */