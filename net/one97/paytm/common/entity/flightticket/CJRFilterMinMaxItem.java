package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFilterMinMaxItem
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="max")
  private Object mMax;
  @b(a="min")
  private Object mMin;
  
  public Object getmMax()
  {
    return this.mMax;
  }
  
  public Object getmMin()
  {
    return this.mMin;
  }
  
  public void setmMax(Object paramObject)
  {
    this.mMax = paramObject;
  }
  
  public void setmMin(Object paramObject)
  {
    this.mMin = paramObject;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRFilterMinMaxItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */