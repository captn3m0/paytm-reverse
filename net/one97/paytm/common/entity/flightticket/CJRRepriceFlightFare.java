package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRRepriceFlightFare
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="breakup")
  private List<CJRBreakup> breakup = new ArrayList();
  private String mConvenienceFee;
  @b(a="totaltax")
  private String mTax;
  @b(a="totalfare")
  private String mTotalFare;
  private String mTotalFee;
  
  public List<CJRBreakup> getBreakup()
  {
    return this.breakup;
  }
  
  public String getTotalFee()
  {
    return this.mTotalFee;
  }
  
  public String getmConvenienceFee()
  {
    return this.mConvenienceFee;
  }
  
  public String getmTax()
  {
    return this.mTax;
  }
  
  public String getmTotalFare()
  {
    return this.mTotalFare;
  }
  
  public void setBreakup(List<CJRBreakup> paramList)
  {
    this.breakup = paramList;
  }
  
  public void setTotalFee(String paramString)
  {
    this.mTotalFee = paramString;
  }
  
  public void setmConvenienceFee(String paramString)
  {
    this.mConvenienceFee = paramString;
  }
  
  public void setmTax(String paramString)
  {
    this.mTax = paramString;
  }
  
  public void setmTotalFare(String paramString)
  {
    this.mTotalFare = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRRepriceFlightFare.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */