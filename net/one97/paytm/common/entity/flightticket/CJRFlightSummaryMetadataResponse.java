package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import java.util.HashMap;
import java.util.Map;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFlightSummaryMetadataResponse
  implements IJRDataModel
{
  private Map<String, Object> additionalProperties = new HashMap();
  @b(a="adults")
  private String adults;
  @b(a="booking_class")
  private String bookingClass;
  @b(a="booking_type")
  private String bookingType;
  @b(a="children")
  private String children;
  @b(a="commissionable_value")
  private String commissionableValue;
  @b(a="contact_email")
  private String contactEmail;
  @b(a="contact_phone")
  private String contactPhone;
  @b(a="convenience_fee_of_all_items")
  private String convenienceFeeOfAllItems;
  @b(a="dDF")
  private String dDF;
  @b(a="departureDate")
  private String departureDate;
  @b(a="destination")
  private String destination;
  @b(a="destination_airport")
  private String destinationAirport;
  @b(a="destination_city")
  private String destinationCity;
  @b(a="destination_iata")
  private String destinationIata;
  @b(a="f_Dir")
  private String fDir;
  @b(a="fare_details")
  private CJRFlightFairDetail fairDetail;
  @b(a="fare_type")
  private String fareType;
  @b(a="frequent_flyer")
  private String frequentFlyer;
  @b(a="infants")
  private String infants;
  @b(a="item_type")
  private String itemType;
  @b(a="journey")
  private CJRFlightJourney journey;
  @b(a="name")
  private String name;
  @b(a="number_of_passengers")
  private int numberOfPassengers;
  @b(a="onward_airline_type")
  private String onwardAirlineType;
  @b(a="onward_product_id_1")
  private long onwardProductId;
  @b(a="origin_airport")
  private String originAirport;
  @b(a="origin_city")
  private String originCity;
  @b(a="origin_iata")
  private String originIata;
  @b(a="passenger_age")
  private String passengerAge;
  @b(a="passenger_dob")
  private String passengerDob;
  @b(a="passenger_firstname")
  private String passengerFirstname;
  @b(a="passenger_lastname")
  private String passengerLastname;
  @b(a="passenger_name")
  private String passengerName;
  @b(a="passenger_title")
  private String passengerTitle;
  @b(a="passenger_type")
  private String passengerType;
  @b(a="provider")
  private String provider;
  @b(a="return_airline_type")
  private String returnAirlineType;
  @b(a="return_product_id")
  private String returnProductId;
  @b(a="return_provider")
  private String returnProvider;
  @b(a="return_service_operator")
  private String returnServiceOperator;
  @b(a="route")
  private String route;
  @b(a="service_operator")
  private String serviceOperator;
  @b(a="source")
  private String source;
  @b(a="total_base_of_all_items")
  private String totalBaseOfAllItems;
  @b(a="total_price_of_all_items")
  private String totalPriceOfAllItems;
  @b(a="total_tax_of_all_items")
  private String totalTaxOfAllItems;
  @b(a="travel_date")
  private String travelDate;
  @b(a="trip_type")
  private String tripType;
  
  public Map<String, Object> getAdditionalProperties()
  {
    return this.additionalProperties;
  }
  
  public String getAdults()
  {
    return this.adults;
  }
  
  public String getBookingClass()
  {
    return this.bookingClass;
  }
  
  public String getBookingType()
  {
    return this.bookingType;
  }
  
  public String getChildren()
  {
    return this.children;
  }
  
  public String getCommissionableValue()
  {
    return this.commissionableValue;
  }
  
  public String getContactEmail()
  {
    return this.contactEmail;
  }
  
  public String getContactPhone()
  {
    return this.contactPhone;
  }
  
  public String getConvenienceFeeOfAllItems()
  {
    return this.convenienceFeeOfAllItems;
  }
  
  public String getDepartureDate()
  {
    return this.departureDate;
  }
  
  public String getDestination()
  {
    return this.destination;
  }
  
  public String getDestinationAirport()
  {
    return this.destinationAirport;
  }
  
  public String getDestinationCity()
  {
    return this.destinationCity;
  }
  
  public String getDestinationIata()
  {
    return this.destinationIata;
  }
  
  public CJRFlightFairDetail getFairDetail()
  {
    return this.fairDetail;
  }
  
  public String getFareType()
  {
    return this.fareType;
  }
  
  public String getFrequentFlyer()
  {
    return this.frequentFlyer;
  }
  
  public String getInfants()
  {
    return this.infants;
  }
  
  public String getItemType()
  {
    return this.itemType;
  }
  
  public CJRFlightJourney getJourney()
  {
    return this.journey;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public int getNumberOfPassengers()
  {
    return this.numberOfPassengers;
  }
  
  public String getOnwardAirlineType()
  {
    return this.onwardAirlineType;
  }
  
  public long getOnwardProductId()
  {
    return this.onwardProductId;
  }
  
  public String getOriginAirport()
  {
    return this.originAirport;
  }
  
  public String getOriginCity()
  {
    return this.originCity;
  }
  
  public String getOriginIata()
  {
    return this.originIata;
  }
  
  public String getPassengerAge()
  {
    return this.passengerAge;
  }
  
  public String getPassengerDob()
  {
    return this.passengerDob;
  }
  
  public String getPassengerFirstname()
  {
    return this.passengerFirstname;
  }
  
  public String getPassengerLastname()
  {
    return this.passengerLastname;
  }
  
  public String getPassengerName()
  {
    return this.passengerName;
  }
  
  public String getPassengerTitle()
  {
    return this.passengerTitle;
  }
  
  public String getPassengerType()
  {
    return this.passengerType;
  }
  
  public String getProvider()
  {
    return this.provider;
  }
  
  public String getReturnAirlineType()
  {
    return this.returnAirlineType;
  }
  
  public String getReturnProductId()
  {
    return this.returnProductId;
  }
  
  public String getReturnProvider()
  {
    return this.returnProvider;
  }
  
  public String getReturnServiceOperator()
  {
    return this.returnServiceOperator;
  }
  
  public String getRoute()
  {
    return this.route;
  }
  
  public String getServiceOperator()
  {
    return this.serviceOperator;
  }
  
  public String getSource()
  {
    return this.source;
  }
  
  public String getTotalBaseOfAllItems()
  {
    return this.totalBaseOfAllItems;
  }
  
  public String getTotalPriceOfAllItems()
  {
    return this.totalPriceOfAllItems;
  }
  
  public String getTotalTaxOfAllItems()
  {
    return this.totalTaxOfAllItems;
  }
  
  public String getTravelDate()
  {
    return this.travelDate;
  }
  
  public String getTripType()
  {
    return this.tripType;
  }
  
  public String getdDF()
  {
    return this.dDF;
  }
  
  public String getfDir()
  {
    return this.fDir;
  }
  
  public void setAdditionalProperties(Map<String, Object> paramMap)
  {
    this.additionalProperties = paramMap;
  }
  
  public void setAdults(String paramString)
  {
    this.adults = paramString;
  }
  
  public void setBookingClass(String paramString)
  {
    this.bookingClass = paramString;
  }
  
  public void setBookingType(String paramString)
  {
    this.bookingType = paramString;
  }
  
  public void setChildren(String paramString)
  {
    this.children = paramString;
  }
  
  public void setCommissionableValue(String paramString)
  {
    this.commissionableValue = paramString;
  }
  
  public void setContactEmail(String paramString)
  {
    this.contactEmail = paramString;
  }
  
  public void setContactPhone(String paramString)
  {
    this.contactPhone = paramString;
  }
  
  public void setConvenienceFeeOfAllItems(String paramString)
  {
    this.convenienceFeeOfAllItems = paramString;
  }
  
  public void setDepartureDate(String paramString)
  {
    this.departureDate = paramString;
  }
  
  public void setDestination(String paramString)
  {
    this.destination = paramString;
  }
  
  public void setDestinationAirport(String paramString)
  {
    this.destinationAirport = paramString;
  }
  
  public void setDestinationCity(String paramString)
  {
    this.destinationCity = paramString;
  }
  
  public void setDestinationIata(String paramString)
  {
    this.destinationIata = paramString;
  }
  
  public void setFairDetail(CJRFlightFairDetail paramCJRFlightFairDetail)
  {
    this.fairDetail = paramCJRFlightFairDetail;
  }
  
  public void setFareType(String paramString)
  {
    this.fareType = paramString;
  }
  
  public void setFrequentFlyer(String paramString)
  {
    this.frequentFlyer = paramString;
  }
  
  public void setInfants(String paramString)
  {
    this.infants = paramString;
  }
  
  public void setItemType(String paramString)
  {
    this.itemType = paramString;
  }
  
  public void setJourney(CJRFlightJourney paramCJRFlightJourney)
  {
    this.journey = paramCJRFlightJourney;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setNumberOfPassengers(int paramInt)
  {
    this.numberOfPassengers = paramInt;
  }
  
  public void setOnwardAirlineType(String paramString)
  {
    this.onwardAirlineType = paramString;
  }
  
  public void setOnwardProductId(long paramLong)
  {
    this.onwardProductId = paramLong;
  }
  
  public void setOriginAirport(String paramString)
  {
    this.originAirport = paramString;
  }
  
  public void setOriginCity(String paramString)
  {
    this.originCity = paramString;
  }
  
  public void setOriginIata(String paramString)
  {
    this.originIata = paramString;
  }
  
  public void setPassengerAge(String paramString)
  {
    this.passengerAge = paramString;
  }
  
  public void setPassengerDob(String paramString)
  {
    this.passengerDob = paramString;
  }
  
  public void setPassengerFirstname(String paramString)
  {
    this.passengerFirstname = paramString;
  }
  
  public void setPassengerLastname(String paramString)
  {
    this.passengerLastname = paramString;
  }
  
  public void setPassengerName(String paramString)
  {
    this.passengerName = paramString;
  }
  
  public void setPassengerTitle(String paramString)
  {
    this.passengerTitle = paramString;
  }
  
  public void setPassengerType(String paramString)
  {
    this.passengerType = paramString;
  }
  
  public void setProvider(String paramString)
  {
    this.provider = paramString;
  }
  
  public void setReturnAirlineType(String paramString)
  {
    this.returnAirlineType = paramString;
  }
  
  public void setReturnProductId(String paramString)
  {
    this.returnProductId = paramString;
  }
  
  public void setReturnProvider(String paramString)
  {
    this.returnProvider = paramString;
  }
  
  public void setReturnServiceOperator(String paramString)
  {
    this.returnServiceOperator = paramString;
  }
  
  public void setRoute(String paramString)
  {
    this.route = paramString;
  }
  
  public void setServiceOperator(String paramString)
  {
    this.serviceOperator = paramString;
  }
  
  public void setSource(String paramString)
  {
    this.source = paramString;
  }
  
  public void setTotalBaseOfAllItems(String paramString)
  {
    this.totalBaseOfAllItems = paramString;
  }
  
  public void setTotalPriceOfAllItems(String paramString)
  {
    this.totalPriceOfAllItems = paramString;
  }
  
  public void setTotalTaxOfAllItems(String paramString)
  {
    this.totalTaxOfAllItems = paramString;
  }
  
  public void setTravelDate(String paramString)
  {
    this.travelDate = paramString;
  }
  
  public void setTripType(String paramString)
  {
    this.tripType = paramString;
  }
  
  public void setdDF(String paramString)
  {
    this.dDF = paramString;
  }
  
  public void setfDir(String paramString)
  {
    this.fDir = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRFlightSummaryMetadataResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */