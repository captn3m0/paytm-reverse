package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.b;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;
import net.one97.paytm.common.entity.shopping.CJROfferCode;

public class CJROffers
  implements IJRDataModel
{
  private static final long serialVersionUID = 1L;
  @b(a="codes")
  private List<CJROfferCode> mOfferCodes;
  @b(a="offers")
  private List<CJROffersDetails> mOfferDetails;
  
  public List<CJROfferCode> getmOfferCodes()
  {
    return this.mOfferCodes;
  }
  
  public List<CJROffersDetails> getmOfferDetails()
  {
    return this.mOfferDetails;
  }
  
  public void setmOfferCodes(List<CJROfferCode> paramList)
  {
    this.mOfferCodes = paramList;
  }
  
  public void setmOfferDetails(List<CJROffersDetails> paramList)
  {
    this.mOfferDetails = paramList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJROffers.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */