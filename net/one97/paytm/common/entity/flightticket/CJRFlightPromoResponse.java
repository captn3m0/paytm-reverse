package net.one97.paytm.common.entity.flightticket;

import com.google.c.a.a;
import com.google.c.a.b;
import net.one97.paytm.common.entity.IJRDataModel;

public class CJRFlightPromoResponse
  implements IJRDataModel
{
  @a
  @b(a="body")
  private Body body;
  @a
  @b(a="code")
  private Integer code;
  @a
  @b(a="error")
  private Object error;
  @a
  @b(a="meta")
  private Meta meta;
  @a
  @b(a="status")
  private Status status;
  
  public Body getBody()
  {
    return this.body;
  }
  
  public Integer getCode()
  {
    return this.code;
  }
  
  public Object getError()
  {
    return this.error;
  }
  
  public Meta getMeta()
  {
    return this.meta;
  }
  
  public Status getStatus()
  {
    return this.status;
  }
  
  public void setBody(Body paramBody)
  {
    this.body = paramBody;
  }
  
  public void setCode(Integer paramInteger)
  {
    this.code = paramInteger;
  }
  
  public void setError(Object paramObject)
  {
    this.error = paramObject;
  }
  
  public void setMeta(Meta paramMeta)
  {
    this.meta = paramMeta;
  }
  
  public void setStatus(Status paramStatus)
  {
    this.status = paramStatus;
  }
  
  public class Body
    implements IJRDataModel
  {
    @a
    @b(a="conv_fee")
    private Integer convFee;
    @a
    @b(a="count")
    private Integer count;
    @a
    @b(a="customer_id")
    private Integer customerId;
    @a
    @b(a="final_price")
    private Integer finalPrice;
    @a
    @b(a="final_price_excl_shipping")
    private Integer finalPriceExclShipping;
    @a
    @b(a="need_shipping")
    private Boolean needShipping;
    @a
    @b(a="order_total")
    private Integer orderTotal;
    @a
    @b(a="paytm_cashback")
    private Integer paytmCashback;
    @a
    @b(a="paytm_discount")
    private Integer paytmDiscount;
    @a
    @b(a="paytm_promocode")
    private String paytmPromocode;
    @a
    @b(a="promofailuretext")
    private String promofailuretext;
    @a
    @b(a="promostatus")
    private String promostatus;
    @a
    @b(a="promotext")
    private String promotext;
    @a
    @b(a="shipping_charges")
    private Integer shippingCharges;
    
    public Body() {}
    
    public Integer getConvFee()
    {
      return this.convFee;
    }
    
    public Integer getCount()
    {
      return this.count;
    }
    
    public Integer getCustomerId()
    {
      return this.customerId;
    }
    
    public Integer getFinalPrice()
    {
      return this.finalPrice;
    }
    
    public Integer getFinalPriceExclShipping()
    {
      return this.finalPriceExclShipping;
    }
    
    public Boolean getNeedShipping()
    {
      return this.needShipping;
    }
    
    public Integer getOrderTotal()
    {
      return this.orderTotal;
    }
    
    public Integer getPaytmCashback()
    {
      return this.paytmCashback;
    }
    
    public Integer getPaytmDiscount()
    {
      return this.paytmDiscount;
    }
    
    public String getPaytmPromocode()
    {
      return this.paytmPromocode;
    }
    
    public String getPromofailuretext()
    {
      return this.promofailuretext;
    }
    
    public String getPromostatus()
    {
      return this.promostatus;
    }
    
    public String getPromotext()
    {
      return this.promotext;
    }
    
    public Integer getShippingCharges()
    {
      return this.shippingCharges;
    }
    
    public void setConvFee(Integer paramInteger)
    {
      this.convFee = paramInteger;
    }
    
    public void setCount(Integer paramInteger)
    {
      this.count = paramInteger;
    }
    
    public void setCustomerId(Integer paramInteger)
    {
      this.customerId = paramInteger;
    }
    
    public void setFinalPrice(Integer paramInteger)
    {
      this.finalPrice = paramInteger;
    }
    
    public void setFinalPriceExclShipping(Integer paramInteger)
    {
      this.finalPriceExclShipping = paramInteger;
    }
    
    public void setNeedShipping(Boolean paramBoolean)
    {
      this.needShipping = paramBoolean;
    }
    
    public void setOrderTotal(Integer paramInteger)
    {
      this.orderTotal = paramInteger;
    }
    
    public void setPaytmCashback(Integer paramInteger)
    {
      this.paytmCashback = paramInteger;
    }
    
    public void setPaytmDiscount(Integer paramInteger)
    {
      this.paytmDiscount = paramInteger;
    }
    
    public void setPaytmPromocode(String paramString)
    {
      this.paytmPromocode = paramString;
    }
    
    public void setPromofailuretext(String paramString)
    {
      this.promofailuretext = paramString;
    }
    
    public void setPromostatus(String paramString)
    {
      this.promostatus = paramString;
    }
    
    public void setPromotext(String paramString)
    {
      this.promotext = paramString;
    }
    
    public void setShippingCharges(Integer paramInteger)
    {
      this.shippingCharges = paramInteger;
    }
  }
  
  public class Message
    implements IJRDataModel
  {
    @a
    @b(a="message")
    private String message;
    @a
    @b(a="title")
    private String title;
    
    public Message() {}
    
    public String getMessage()
    {
      return this.message;
    }
    
    public String getTitle()
    {
      return this.title;
    }
    
    public void setMessage(String paramString)
    {
      this.message = paramString;
    }
    
    public void setTitle(String paramString)
    {
      this.title = paramString;
    }
  }
  
  public class Meta
    implements IJRDataModel
  {
    @a
    @b(a="requestid")
    private String requestid;
    
    public Meta() {}
    
    public String getRequestid()
    {
      return this.requestid;
    }
    
    public void setRequestid(String paramString)
    {
      this.requestid = paramString;
    }
  }
  
  public class Status
    implements IJRDataModel
  {
    @a
    @b(a="message")
    private CJRFlightPromoResponse.Message message;
    @a
    @b(a="result")
    private String result;
    
    public Status() {}
    
    public CJRFlightPromoResponse.Message getMessage()
    {
      return this.message;
    }
    
    public String getResult()
    {
      return this.result;
    }
    
    public void setMessage(CJRFlightPromoResponse.Message paramMessage)
    {
      this.message = paramMessage;
    }
    
    public void setResult(String paramString)
    {
      this.result = paramString;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/flightticket/CJRFlightPromoResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */