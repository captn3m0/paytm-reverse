package net.one97.paytm.common.entity;

import com.google.c.a.b;
import java.util.ArrayList;

public class CJRUserInfo
  extends CJRDataModelItem
{
  private static final long serialVersionUID = 1L;
  private boolean isFirstMobileNumber = false;
  @b(a="showVerificationPage")
  private boolean isShowVerificationPage;
  @b(a="isSkipAllowed")
  private boolean isSkipAllowed;
  @b(a="isUniqueEmail")
  private boolean isUniqueEmail;
  @b(a="isUniqueMobile")
  private boolean isUniqueMobile;
  @b(a="walletType")
  private String isUserPrime;
  @b(a="isValidEmail")
  private boolean isValidEmail;
  @b(a="isValidMobile")
  private boolean isValidMobile;
  @b(a="created_at")
  private String mCreatedAt;
  @b(a="customerAreacode")
  private String mCustomerAreaCode;
  @b(a="customerIp")
  private String mCustomerIpCode;
  @b(a="customerLastLoginDate")
  private String mCustomerLastLoginDate;
  @b(a="customerSessionid")
  private String mCustomerSessionId;
  @b(a="customerTermandConditon")
  private String mCustomerTermAndConditon;
  @b(a="dateOfBirth")
  private String mDOB;
  @b(a="email")
  private String mEmail;
  @b(a="firstName")
  private String mFirstName;
  @b(a="gender")
  private String mGender;
  private int mHttpCode;
  @b(a="id")
  private String mId;
  @b(a="isConsent")
  private boolean mIsConsent;
  @b(a="is_verified_email")
  private int mIsVerifiedEmail;
  @b(a="is_verified_mobile")
  private int mIsVerifiedMobile;
  @b(a="ivr_flag")
  private int mIvrFlag;
  @b(a="lastName")
  private String mLastName;
  @b(a="message")
  private String mMessage;
  @b(a="mobile")
  private String mMobile;
  @b(a="plustxt_id")
  private String mPlustxtId;
  @b(a="responseCode")
  private String mResponseCode;
  @b(a="sms_flag")
  private String mSmsFlag;
  @b(a="status")
  private String mStatus;
  @b(a="type")
  private String mType;
  @b(a="username")
  private String mUserName;
  @b(a="userPicture")
  private String mUserPicture;
  @b(a="userSocialInfoList")
  private ArrayList<CJRUserSocialInfo> mUserSocialInfoList;
  
  public String getCreatedAt()
  {
    return this.mCreatedAt;
  }
  
  public String getCustomerAreaCode()
  {
    return this.mCustomerAreaCode;
  }
  
  public String getCustomerIpCode()
  {
    return this.mCustomerIpCode;
  }
  
  public String getCustomerLastLoginDate()
  {
    return this.mCustomerLastLoginDate;
  }
  
  public String getCustomerSessionId()
  {
    return this.mCustomerSessionId;
  }
  
  public String getCustomerTermAndConditon()
  {
    return this.mCustomerTermAndConditon;
  }
  
  public String getDOB()
  {
    return this.mDOB;
  }
  
  public String getEmail()
  {
    return this.mEmail;
  }
  
  public String getFirstName()
  {
    return this.mFirstName;
  }
  
  public String getGender()
  {
    return this.mGender;
  }
  
  public int getHttpCode()
  {
    return this.mHttpCode;
  }
  
  public String getId()
  {
    return this.mId;
  }
  
  public boolean getIsConsent()
  {
    return this.mIsConsent;
  }
  
  public String getIsUserPrime()
  {
    return this.isUserPrime;
  }
  
  public int getIsVerifiedEmail()
  {
    return this.mIsVerifiedEmail;
  }
  
  public int getIsVerifiedMobile()
  {
    return this.mIsVerifiedMobile;
  }
  
  public int getIvrFlag()
  {
    return this.mIvrFlag;
  }
  
  public String getLastName()
  {
    return this.mLastName;
  }
  
  public String getMessage()
  {
    return this.mMessage;
  }
  
  public String getMobile()
  {
    return this.mMobile;
  }
  
  public String getName()
  {
    return null;
  }
  
  public String getSmsFlag()
  {
    return this.mSmsFlag;
  }
  
  public String getStatus()
  {
    return this.mStatus;
  }
  
  public String getType()
  {
    return this.mType;
  }
  
  public String getUserName()
  {
    return this.mUserName;
  }
  
  public String getUserPicture()
  {
    return this.mUserPicture;
  }
  
  public ArrayList<CJRUserSocialInfo> getUserSocialInfoList()
  {
    return this.mUserSocialInfoList;
  }
  
  public boolean isShowVerificationPage()
  {
    return this.isShowVerificationPage;
  }
  
  public boolean isSkipAllowed()
  {
    return this.isSkipAllowed;
  }
  
  public boolean isUniqueEmail()
  {
    return this.isUniqueEmail;
  }
  
  public boolean isUniqueMobile()
  {
    return this.isUniqueMobile;
  }
  
  public boolean isUserUpdatedMobileNumber()
  {
    return this.isFirstMobileNumber;
  }
  
  public boolean isValidEmail()
  {
    return this.isValidEmail;
  }
  
  public boolean isValidMobile()
  {
    return this.isValidMobile;
  }
  
  public void setCreatedAt(String paramString)
  {
    this.mCreatedAt = paramString;
  }
  
  public void setDOB(String paramString)
  {
    this.mDOB = paramString;
  }
  
  public void setEmail(String paramString)
  {
    this.mEmail = paramString;
  }
  
  public void setFirstName(String paramString)
  {
    this.mFirstName = paramString;
  }
  
  public void setGender(String paramString)
  {
    this.mGender = paramString;
  }
  
  public void setHttpCode(int paramInt)
  {
    this.mHttpCode = paramInt;
  }
  
  public void setId(String paramString)
  {
    this.mId = paramString;
  }
  
  public void setIsUserPrime(String paramString)
  {
    this.isUserPrime = paramString;
  }
  
  public void setIsVerifiedEmail(int paramInt)
  {
    this.mIsVerifiedEmail = paramInt;
  }
  
  public void setLastName(String paramString)
  {
    this.mLastName = paramString;
  }
  
  public void setMessage(String paramString)
  {
    this.mMessage = paramString;
  }
  
  public void setMobile(String paramString)
  {
    this.mMobile = paramString;
  }
  
  public void setSkipAllowed(boolean paramBoolean)
  {
    this.isSkipAllowed = paramBoolean;
  }
  
  public void setStatus(String paramString)
  {
    this.mStatus = paramString;
  }
  
  public void setUniqueEmail(boolean paramBoolean)
  {
    this.isUniqueEmail = paramBoolean;
  }
  
  public void setUniqueMobile(boolean paramBoolean)
  {
    this.isUniqueMobile = paramBoolean;
  }
  
  public void setUserPicture(String paramString)
  {
    this.mUserPicture = paramString;
  }
  
  public void setUserUpdatedMobileNumber(boolean paramBoolean)
  {
    this.isFirstMobileNumber = paramBoolean;
  }
  
  public void setValidEmail(boolean paramBoolean)
  {
    this.isValidEmail = paramBoolean;
  }
  
  public void setValidMobile(boolean paramBoolean)
  {
    this.isValidMobile = paramBoolean;
  }
  
  public void setmIsVerifiedMobile(int paramInt)
  {
    this.mIsVerifiedMobile = paramInt;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/common/entity/CJRUserInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */