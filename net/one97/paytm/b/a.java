package net.one97.paytm.b;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import com.google.android.gms.tagmanager.DataLayer;
import com.google.android.gms.tagmanager.TagManager;
import com.urbanairship.q;
import io.hansel.pebbletracesdk.HanselCrashReporter;
import io.hansel.pebbletracesdk.annotations.HanselInclude;
import io.hansel.pebbletracesdk.codepatch.Conversions;
import io.hansel.pebbletracesdk.codepatch.PatchJoinPoint.PatchJoinPointBuilder;
import io.hansel.pebbletracesdk.codepatch.patch.Patch;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.one97.paytm.common.entity.CJRItem;
import net.one97.paytm.common.entity.events.CJREventStoreFrontItemDetailModel;
import net.one97.paytm.common.entity.shopping.CJRCartProduct;
import net.one97.paytm.common.entity.shopping.CJRDetailProduct;
import net.one97.paytm.common.entity.shopping.CJRGridProduct;
import net.one97.paytm.common.entity.shopping.CJRHomePageItem;
import net.one97.paytm.common.entity.shopping.CJRHomePageLayout;
import net.one97.paytm.common.entity.shopping.CJRHomePageLayoutV2;
import net.one97.paytm.common.entity.shopping.CJRMerchant;
import net.one97.paytm.common.entity.shopping.CJROrderSummary;
import net.one97.paytm.common.entity.shopping.CJROrderSummaryProductDetail;
import net.one97.paytm.common.entity.shopping.CJROrderedCart;
import net.one97.paytm.common.entity.shopping.CJRParcelTrackingInfo;
import net.one97.paytm.common.entity.shopping.CJRTrackingInfo;
import net.one97.paytm.common.entity.shopping.WishListProduct;
import net.one97.paytm.common.utility.b;
import net.one97.paytm.common.utility.e;
import net.one97.paytm.common.utility.i;
import net.one97.paytm.common.utility.l;
import net.one97.paytm.fragment.w;
import net.one97.paytm.utils.d;
import org.json.JSONException;
import org.json.JSONObject;

@HanselInclude
public class a
{
  private static HandlerThread m = null;
  private static Handler n = null;
  private final String A = "remove";
  private final String B = "removeFromCart";
  private final String C = "addToCart";
  private final String D = "add";
  private final String E = "summaryPage";
  private final String F = "actionField";
  private final String G = "purchase";
  private final String H = "affiliation";
  private final String I = "revenue";
  private final String J = "shipping";
  private final String K = "tax";
  private final String L = "step";
  private final String M = "option";
  private final String N = "homePage";
  private final String O = "searchTerm";
  private final String P = "list";
  private final String Q = "LandingPage-";
  private final String R = "Landing Page";
  private final String S = "Order Summary";
  private final String T = "Paytm Store - Android";
  private final int U = 10;
  public final String a = "/recharge";
  public final String b = "/dth";
  public final String c = "/electricity";
  public final String d = "/landline/broadband";
  public final String e = "/gas";
  public final String f = "/datacard";
  public final String g = "proceed_to_recharge";
  public final String h = "pay_now_for_recharge";
  public final String i = "home_page";
  public final String j = "category_page";
  public final String k = "product_page";
  public final String l = "add_to_cart";
  private final String o = "id";
  private final String p = "name";
  private final String q = "category";
  private final String r = "brand";
  private final String s = "variant";
  private final String t = "price";
  private final String u = "quantity";
  private final String v = "coupon";
  private final String w = "products";
  private final String x = "detail";
  private final String y = "click";
  private final String z = "eventName";
  
  public static String a(Context paramContext)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class });
    if (localObject != null) {
      return (String)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext }).toPatchJoinPoint());
    }
    localObject = "";
    if (net.one97.paytm.common.utility.j.a(paramContext) != null) {
      localObject = d.r(paramContext);
    }
    return (String)localObject;
  }
  
  private String a(String paramString1, String paramString2, long paramLong)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { String.class, String.class, Long.TYPE });
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString1, paramString2, new Long(paramLong) }).toPatchJoinPoint());
    }
    if (paramString1.equalsIgnoreCase("Tickets")) {
      return "BusTicket";
    }
    if (paramString1.equalsIgnoreCase("Wallet")) {
      return "Wallet";
    }
    if (b(paramString2, paramString1))
    {
      paramString1 = b(paramString2, paramLong, paramString1);
      if (!TextUtils.isEmpty(paramString1)) {
        return paramString1;
      }
      return "Recharge";
    }
    if (paramString1.equalsIgnoreCase("Paytm Hotel")) {
      return "Hotel";
    }
    return "Marketplace";
  }
  
  private HashMap<String, Object> a(CJRItem paramCJRItem, String paramString)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJRItem.class, String.class });
    if (localObject != null) {
      return (HashMap)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJRItem, paramString }).toPatchJoinPoint());
    }
    String str2 = "";
    int i1 = -1;
    String str7 = null;
    String str4 = null;
    String str6 = null;
    String str5 = null;
    String str1 = null;
    localObject = null;
    String str3 = null;
    if (paramCJRItem != null)
    {
      str2 = paramCJRItem.getListName();
      i1 = paramCJRItem.getListPosition();
      str7 = paramCJRItem.getSearchType();
      str4 = paramCJRItem.getSearchCategory();
      str6 = paramCJRItem.getSearchTerm();
      str5 = paramCJRItem.getSearchResultType();
      str1 = paramCJRItem.getListId();
      localObject = paramCJRItem.getmContainerInstanceID();
      str3 = paramCJRItem.getSearchABValue();
    }
    paramCJRItem = new HashMap();
    if (!TextUtils.isEmpty(str2)) {
      paramCJRItem.put("dimension24", str2);
    }
    if (i1 != -1) {
      paramCJRItem.put("dimension25", "" + (i1 + 1));
    }
    if (!TextUtils.isEmpty(str7)) {
      paramCJRItem.put("dimension26", str7);
    }
    if (!TextUtils.isEmpty(str4)) {
      paramCJRItem.put("dimension27", str4);
    }
    if (!TextUtils.isEmpty(str6)) {
      paramCJRItem.put("dimension28", str6);
    }
    if (!TextUtils.isEmpty(str5)) {
      paramCJRItem.put("dimension29", str5);
    }
    if (!TextUtils.isEmpty(paramString)) {
      paramCJRItem.put("dimension31", paramString);
    }
    if (!TextUtils.isEmpty((CharSequence)localObject)) {
      paramCJRItem.put("dimension40", localObject);
    }
    if (!TextUtils.isEmpty(str3)) {
      paramCJRItem.put("dimension53", str3);
    }
    paramCJRItem.put("dimension38", str1);
    return paramCJRItem;
  }
  
  public static HashMap<String, CJRTrackingInfo> a(CJRParcelTrackingInfo paramCJRParcelTrackingInfo)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJRParcelTrackingInfo.class });
    if (localObject != null) {
      return (HashMap)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramCJRParcelTrackingInfo }).toPatchJoinPoint());
    }
    localObject = new HashMap();
    if (paramCJRParcelTrackingInfo != null)
    {
      paramCJRParcelTrackingInfo = paramCJRParcelTrackingInfo.getCartItemsForTrackingInfo().iterator();
      while (paramCJRParcelTrackingInfo.hasNext())
      {
        CJRCartProduct localCJRCartProduct = (CJRCartProduct)paramCJRParcelTrackingInfo.next();
        String str = localCJRCartProduct.getProductId();
        if ((CJRTrackingInfo)((HashMap)localObject).get(str) == null) {
          ((HashMap)localObject).put(str, localCJRCartProduct.getTrackingInfo());
        }
      }
    }
    return (HashMap<String, CJRTrackingInfo>)localObject;
  }
  
  private List<Object> a(ArrayList<CJRCartProduct> paramArrayList, String paramString, boolean paramBoolean)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "a", new Class[] { ArrayList.class, String.class, Boolean.TYPE });
    if (localObject != null) {
      return (List)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramArrayList, paramString, new Boolean(paramBoolean) }).toPatchJoinPoint());
    }
    localObject = new ArrayList();
    paramArrayList = paramArrayList.iterator();
    while (paramArrayList.hasNext()) {
      ((List)localObject).add(a((CJRCartProduct)paramArrayList.next(), paramString, paramBoolean));
    }
    return (List<Object>)localObject;
  }
  
  private List<Object> a(CJROrderSummary paramCJROrderSummary, CJRParcelTrackingInfo paramCJRParcelTrackingInfo)
  {
    Object localObject1 = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJROrderSummary.class, CJRParcelTrackingInfo.class });
    if (localObject1 != null) {
      return (List)((Patch)localObject1).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject1).getClassForPatch()).setMethod(((Patch)localObject1).getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJROrderSummary, paramCJRParcelTrackingInfo }).toPatchJoinPoint());
    }
    localObject1 = paramCJROrderSummary.getOrderedCartList();
    ArrayList localArrayList1 = new ArrayList();
    HashMap localHashMap1 = new HashMap();
    HashMap localHashMap2 = a(paramCJRParcelTrackingInfo);
    ArrayList localArrayList2 = new ArrayList();
    int i1 = 0;
    Object localObject2 = ((ArrayList)localObject1).iterator();
    Object localObject3;
    Object localObject4;
    while (((Iterator)localObject2).hasNext())
    {
      paramCJRParcelTrackingInfo = (CJROrderedCart)((Iterator)localObject2).next();
      if (paramCJRParcelTrackingInfo.getProductDetail() != null)
      {
        localObject3 = paramCJRParcelTrackingInfo.getProductDetail();
        localObject4 = String.valueOf(((CJROrderSummaryProductDetail)localObject3).getId());
        ((CJROrderSummaryProductDetail)localObject3).setDiscountedPrice(paramCJRParcelTrackingInfo.getDiscountedPrice());
        localArrayList2.add(String.valueOf(paramCJRParcelTrackingInfo.getMerchantId()));
        localObject1 = (ArrayList)localHashMap1.get(localObject4);
        paramCJRParcelTrackingInfo = (CJRParcelTrackingInfo)localObject1;
        if (localObject1 == null)
        {
          paramCJRParcelTrackingInfo = new ArrayList();
          localHashMap1.put(localObject4, paramCJRParcelTrackingInfo);
        }
        paramCJRParcelTrackingInfo.add(localObject3);
      }
    }
    Iterator localIterator = localHashMap1.keySet().iterator();
    if (localIterator.hasNext())
    {
      String str1 = (String)localIterator.next();
      Object localObject5 = (ArrayList)localHashMap1.get(str1);
      CJRTrackingInfo localCJRTrackingInfo = (CJRTrackingInfo)localHashMap2.get(str1);
      localObject4 = (CJROrderSummaryProductDetail)((ArrayList)localObject5).get(0);
      paramCJRParcelTrackingInfo = "";
      label340:
      label361:
      int i2;
      if (((CJROrderSummaryProductDetail)localObject4).getCategoryMapPath().trim().length() > 0)
      {
        localObject1 = ((CJROrderSummaryProductDetail)localObject4).getCategoryMapPath();
        if (((CJROrderSummaryProductDetail)localObject4).getCategoryIdMapPath().trim().length() <= 0) {
          break label938;
        }
        localObject2 = ((CJROrderSummaryProductDetail)localObject4).getCategoryIdMapPath();
        String str3 = ((CJROrderSummaryProductDetail)localObject4).getBrandName();
        String str2 = ((CJROrderSummaryProductDetail)localObject4).getName();
        i2 = i1;
        localObject3 = paramCJRParcelTrackingInfo;
        if (i1 < localArrayList2.size())
        {
          i2 = i1;
          localObject3 = paramCJRParcelTrackingInfo;
          if (localArrayList2.get(i1) != null)
          {
            localObject3 = (String)localArrayList2.get(i1);
            i2 = i1 + 1;
          }
        }
        if (!TextUtils.isEmpty(paramCJROrderSummary.getPromoCode())) {
          break label951;
        }
        paramCJRParcelTrackingInfo = "";
        label441:
        double d1 = ((CJROrderSummaryProductDetail)localObject4).getDiscountedPrice();
        localObject4 = str2;
        if (TextUtils.isEmpty(str2)) {
          localObject4 = "";
        }
        localObject2 = DataLayer.mapOf(new Object[] { "id", str1, "name", localObject4, "category", localObject1, "brand", str3, "variant", "", "price", String.valueOf(d1), "quantity", Integer.valueOf(((ArrayList)localObject5).size()), "dimension41", localObject3, "dimension43", localObject2 });
        if (localCJRTrackingInfo != null)
        {
          localObject1 = localCJRTrackingInfo.getListName();
          localObject3 = "" + (localCJRTrackingInfo.getListPosition() + 1);
          localObject4 = localCJRTrackingInfo.getSearchType();
          str2 = localCJRTrackingInfo.getSearchCategory();
          localObject5 = localCJRTrackingInfo.getSearchTerm();
          str3 = localCJRTrackingInfo.getSearchResultType();
          localCJRTrackingInfo.getSearchABValue();
          ((Map)localObject2).put("dimension24", localObject1);
          ((Map)localObject2).put("dimension25", localObject3);
          ((Map)localObject2).put("position", localObject3);
          if (!TextUtils.isEmpty((CharSequence)localObject4)) {
            ((Map)localObject2).put("dimension26", localObject4);
          }
          if (!TextUtils.isEmpty(str2)) {
            ((Map)localObject2).put("dimension27", str2);
          }
          if (!TextUtils.isEmpty((CharSequence)localObject5)) {
            ((Map)localObject2).put("dimension28", localObject5);
          }
          if (!TextUtils.isEmpty(str3)) {
            ((Map)localObject2).put("dimension29", str3);
          }
          if (!TextUtils.isEmpty(localCJRTrackingInfo.getListId())) {
            ((Map)localObject2).put("dimension38", localCJRTrackingInfo.getListId());
          }
          if (!TextUtils.isEmpty(localCJRTrackingInfo.getSearchABValue())) {
            ((Map)localObject2).put("dimension53", localCJRTrackingInfo.getSearchABValue());
          }
          if (!TextUtils.isEmpty(localCJRTrackingInfo.getContainerID())) {
            ((Map)localObject2).put("dimension40", localCJRTrackingInfo.getContainerID());
          }
          if (!TextUtils.isEmpty(localCJRTrackingInfo.getParentId())) {
            break label959;
          }
        }
      }
      label938:
      label951:
      label959:
      for (localObject1 = str1;; localObject1 = localCJRTrackingInfo.getParentId())
      {
        ((Map)localObject2).put("dimension31", localObject1);
        if (!TextUtils.isEmpty(paramCJRParcelTrackingInfo)) {
          ((Map)localObject2).put("coupon", paramCJRParcelTrackingInfo);
        }
        localArrayList1.add(localObject2);
        i1 = i2;
        break;
        localObject1 = ((CJROrderSummaryProductDetail)localObject4).getVertical();
        break label340;
        localObject2 = String.valueOf(((CJROrderSummaryProductDetail)localObject4).getVerticalId());
        break label361;
        paramCJRParcelTrackingInfo = paramCJROrderSummary.getPromoCode();
        break label441;
      }
    }
    return localArrayList1;
  }
  
  private Map<String, Object> a(int paramInt, ArrayList<CJRCartProduct> paramArrayList)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Integer.TYPE, ArrayList.class });
    if (localObject != null) {
      return (Map)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(this).setArguments(new Object[] { new Integer(paramInt), paramArrayList }).toPatchJoinPoint());
    }
    localObject = new HashMap();
    ((Map)localObject).put("step", Integer.valueOf(paramInt));
    if ((paramArrayList.size() == 1) && (((CJRCartProduct)paramArrayList.get(0)).getTrackingInfo() != null) && (((CJRCartProduct)paramArrayList.get(0)).getTrackingInfo().getListName() != null)) {
      ((Map)localObject).put("list", ((CJRCartProduct)paramArrayList.get(0)).getTrackingInfo().getListName());
    }
    return (Map<String, Object>)localObject;
  }
  
  private Map<String, Object> a(ArrayList<CJRCartProduct> paramArrayList, int paramInt, String paramString, boolean paramBoolean)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { ArrayList.class, Integer.TYPE, String.class, Boolean.TYPE });
    if (localPatch != null) {
      return (Map)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramArrayList, new Integer(paramInt), paramString, new Boolean(paramBoolean) }).toPatchJoinPoint());
    }
    return DataLayer.mapOf(new Object[] { "checkout", DataLayer.mapOf(new Object[] { "actionField", a(paramInt, paramArrayList), "products", a(paramArrayList, paramString, paramBoolean) }) });
  }
  
  private Map<String, Object> a(HashMap<String, Object> paramHashMap, CJRGridProduct paramCJRGridProduct, String paramString1, String paramString2, int paramInt, String paramString3)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "a", new Class[] { HashMap.class, CJRGridProduct.class, String.class, String.class, Integer.TYPE, String.class });
    if (localObject != null) {
      return (Map)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramHashMap, paramCJRGridProduct, paramString1, paramString2, new Integer(paramInt), paramString3 }).toPatchJoinPoint());
    }
    String str4 = null;
    String str1 = null;
    String str3 = null;
    String str2 = null;
    localObject = null;
    if (paramHashMap != null)
    {
      str4 = (String)paramHashMap.get("search_type");
      str1 = (String)paramHashMap.get("search_category");
      str3 = (String)paramHashMap.get("search_term");
      str2 = (String)paramHashMap.get("search_result_type");
      localObject = (String)paramHashMap.get("search_ab_value");
    }
    HashMap localHashMap = new HashMap();
    localHashMap.put("id", paramCJRGridProduct.getProductID());
    localHashMap.put("name", paramCJRGridProduct.getName());
    localHashMap.put("brand", paramCJRGridProduct.getBrand());
    localHashMap.put("variant", "");
    localHashMap.put("category", paramString2);
    localHashMap.put("price", paramCJRGridProduct.getDiscountedPrice());
    localHashMap.put("dimension24", paramString1);
    localHashMap.put("dimension25", Integer.valueOf(paramInt));
    localHashMap.put("position", Integer.valueOf(paramInt));
    if (str4 != null) {
      localHashMap.put("dimension26", str4);
    }
    if (str1 != null) {
      localHashMap.put("dimension27", str1);
    }
    if (str3 != null) {
      localHashMap.put("dimension28", str3);
    }
    if (str2 != null) {
      localHashMap.put("dimension29", str2);
    }
    if (localObject != null) {
      localHashMap.put("dimension53", localObject);
    }
    if (TextUtils.isEmpty(paramCJRGridProduct.getParentID())) {}
    for (paramHashMap = paramCJRGridProduct.getProductID();; paramHashMap = paramCJRGridProduct.getParentID())
    {
      localHashMap.put("dimension31", paramHashMap);
      localHashMap.put("dimension38", paramCJRGridProduct.getListId());
      localHashMap.put("dimension43", paramString3);
      return localHashMap;
    }
  }
  
  private Map<String, Object> a(CJRItem paramCJRItem)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJRItem.class });
    if (localObject != null) {
      return (Map)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJRItem }).toPatchJoinPoint());
    }
    localObject = new HashMap();
    if ((paramCJRItem != null) && (!paramCJRItem.getListName().isEmpty())) {
      ((HashMap)localObject).put("list", paramCJRItem.getListName());
    }
    return (Map<String, Object>)localObject;
  }
  
  private Map<String, Object> a(CJRCartProduct paramCJRCartProduct)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJRCartProduct.class });
    if (localObject != null) {
      return (Map)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJRCartProduct }).toPatchJoinPoint());
    }
    localObject = b(paramCJRCartProduct);
    paramCJRCartProduct = paramCJRCartProduct.getTrackingInfo();
    HashMap localHashMap = new HashMap();
    if ((paramCJRCartProduct != null) && (paramCJRCartProduct.getListName() != null) && (!paramCJRCartProduct.getListName().isEmpty())) {
      localHashMap.put("list", paramCJRCartProduct.getListName());
    }
    return DataLayer.mapOf(new Object[] { "actionField", localHashMap, "products", DataLayer.listOf(new Object[] { localObject }) });
  }
  
  private Map<String, Object> a(CJRCartProduct paramCJRCartProduct, String paramString)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJRCartProduct.class, String.class });
    if (localObject != null) {
      return (Map)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJRCartProduct, paramString }).toPatchJoinPoint());
    }
    localObject = paramString;
    if (TextUtils.isEmpty(paramString)) {
      localObject = paramCJRCartProduct.getVerticalLabel();
    }
    return DataLayer.mapOf(new Object[] { "products", DataLayer.listOf(new Object[] { DataLayer.mapOf(new Object[] { "id", paramCJRCartProduct.getProductId(), "name", paramCJRCartProduct.getName(), "category", localObject, "brand", paramCJRCartProduct.getBrand(), "variant", "", "price", paramCJRCartProduct.getDiscountedPrice(), "quantity", Integer.valueOf(1) }) }) });
  }
  
  private Map<String, Object> a(CJRCartProduct paramCJRCartProduct, String paramString, boolean paramBoolean)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJRCartProduct.class, String.class, Boolean.TYPE });
    if (localObject != null) {
      return (Map)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJRCartProduct, paramString, new Boolean(paramBoolean) }).toPatchJoinPoint());
    }
    localObject = paramCJRCartProduct.getCategoryPathForGTM();
    String str = paramCJRCartProduct.getCategoryIdForGTM();
    paramString = (String)localObject;
    if (TextUtils.isEmpty((CharSequence)localObject))
    {
      paramString = paramCJRCartProduct.getVerticalLabel();
      str = paramCJRCartProduct.getVerticalId();
    }
    HashMap localHashMap;
    if (TextUtils.isEmpty(paramCJRCartProduct.getPromoCode()))
    {
      localObject = "";
      localHashMap = new HashMap();
      localHashMap.put("id", paramCJRCartProduct.getProductId());
      localHashMap.put("name", paramCJRCartProduct.getName());
      localHashMap.put("category", paramString);
      localHashMap.put("brand", paramCJRCartProduct.getBrand());
      localHashMap.put("variant", "");
      localHashMap.put("price", paramCJRCartProduct.getSellingPrice());
      localHashMap.put("quantity", paramCJRCartProduct.getQuantity());
      localHashMap.put("dimension43", str);
      localHashMap.put("dimension41", paramCJRCartProduct.getMerchantId());
      if (paramCJRCartProduct.getTrackingInfo() != null)
      {
        paramString = paramCJRCartProduct.getTrackingInfo();
        if (!TextUtils.isEmpty(paramString.getListName())) {
          localHashMap.put("dimension24", paramString.getListName());
        }
        if (paramString.getListPosition() != -1)
        {
          localHashMap.put("dimension25", "" + (paramString.getListPosition() + 1));
          localHashMap.put("position", "" + (paramString.getListPosition() + 1));
        }
        if (!TextUtils.isEmpty(paramString.getSearchType())) {
          localHashMap.put("dimension26", paramString.getSearchType());
        }
        if (!TextUtils.isEmpty(paramString.getSearchType())) {
          localHashMap.put("dimension27", paramString.getSearchCategory());
        }
        if (!TextUtils.isEmpty(paramString.getSearchType())) {
          localHashMap.put("dimension28", paramString.getSearchTerm());
        }
        if (!TextUtils.isEmpty(paramString.getSearchType())) {
          localHashMap.put("dimension29", paramString.getSearchResultType());
        }
        if (!TextUtils.isEmpty(paramString.getListId())) {
          localHashMap.put("dimension38", paramString.getListId());
        }
        if (!TextUtils.isEmpty(paramString.getContainerID())) {
          localHashMap.put("dimension40", paramString.getContainerID());
        }
        if (!TextUtils.isEmpty(paramString.getSearchABValue())) {
          localHashMap.put("dimension53", paramString.getSearchABValue());
        }
        if (!TextUtils.isEmpty(paramString.getParentId())) {
          break label590;
        }
      }
    }
    label590:
    for (paramCJRCartProduct = paramCJRCartProduct.getProductId();; paramCJRCartProduct = paramString.getParentId())
    {
      localHashMap.put("dimension31", paramCJRCartProduct);
      if (!TextUtils.isEmpty((CharSequence)localObject)) {
        localHashMap.put("coupon", localObject);
      }
      return localHashMap;
      localObject = paramCJRCartProduct.getPromoCode();
      break;
    }
  }
  
  private Map<String, Object> a(CJRDetailProduct paramCJRDetailProduct, int paramInt, CJRItem paramCJRItem)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJRDetailProduct.class, Integer.TYPE, CJRItem.class });
    if (localPatch != null) {
      return (Map)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJRDetailProduct, new Integer(paramInt), paramCJRItem }).toPatchJoinPoint());
    }
    return DataLayer.mapOf(new Object[] { "actionField", a(paramCJRItem), "products", b(paramCJRDetailProduct, paramInt, paramCJRItem) });
  }
  
  private static Map<String, Object> a(CJRGridProduct paramCJRGridProduct, boolean paramBoolean, String paramString1, String paramString2, int paramInt, HashMap<String, Object> paramHashMap, String paramString3, String paramString4)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJRGridProduct.class, Boolean.TYPE, String.class, String.class, Integer.TYPE, HashMap.class, String.class, String.class });
    if (localObject != null) {
      return (Map)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramCJRGridProduct, new Boolean(paramBoolean), paramString1, paramString2, new Integer(paramInt), paramHashMap, paramString3, paramString4 }).toPatchJoinPoint());
    }
    String str3 = null;
    localObject = null;
    String str2 = null;
    String str1 = null;
    paramString3 = null;
    if (paramHashMap != null)
    {
      str3 = (String)paramHashMap.get("search_type");
      localObject = (String)paramHashMap.get("search_category");
      str2 = (String)paramHashMap.get("search_term");
      str1 = (String)paramHashMap.get("search_result_type");
      paramString3 = (String)paramHashMap.get("search_ab_value");
    }
    if (paramBoolean) {
      paramString1 = "search-" + str2;
    }
    HashMap localHashMap;
    for (;;)
    {
      paramHashMap = paramString1;
      if (paramCJRGridProduct != null)
      {
        paramHashMap = paramString1;
        if (paramCJRGridProduct.getmSource() != null)
        {
          paramHashMap = paramString1;
          if (!paramCJRGridProduct.getmSource().isEmpty()) {
            paramHashMap = paramString1 + "/" + paramCJRGridProduct.getmSource();
          }
        }
      }
      localHashMap = new HashMap();
      localHashMap.put("id", paramCJRGridProduct.getProductID());
      localHashMap.put("name", paramCJRGridProduct.getName());
      localHashMap.put("list", paramHashMap);
      localHashMap.put("category", paramString2);
      localHashMap.put("brand", paramCJRGridProduct.getBrand());
      localHashMap.put("price", String.valueOf(paramCJRGridProduct.getDiscountedPrice()));
      localHashMap.put("quantity", Integer.valueOf(1));
      localHashMap.put("position", "" + paramInt);
      localHashMap.put("dimension24", paramHashMap);
      localHashMap.put("dimension25", "" + paramInt);
      localHashMap.put("dimension43", paramCJRGridProduct.getAncestorID());
      localHashMap.put("dimension41", w.c);
      localHashMap.put("dimension40", paramString4);
      if (str3 != null) {
        localHashMap.put("dimension26", str3);
      }
      if (localObject != null) {
        localHashMap.put("dimension27", localObject);
      }
      if (str2 != null) {
        localHashMap.put("dimension28", str2);
      }
      if (str1 != null) {
        localHashMap.put("dimension29", str1);
      }
      if (paramString3 != null) {
        localHashMap.put("dimension53", paramString3);
      }
      try
      {
        if (TextUtils.isEmpty(paramCJRGridProduct.getParentID())) {}
        for (paramString1 = paramCJRGridProduct.getProductID();; paramString1 = paramCJRGridProduct.getParentID())
        {
          localHashMap.put("dimension31", paramString1);
          localHashMap.put("dimension38", paramCJRGridProduct.getListId());
          break;
        }
        return localHashMap;
      }
      catch (Exception paramCJRGridProduct)
      {
        paramCJRGridProduct.printStackTrace();
      }
    }
  }
  
  private static Map<String, Object> a(CJRHomePageItem paramCJRHomePageItem, String paramString1, String paramString2, String paramString3, HashMap<String, Object> paramHashMap, boolean paramBoolean)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJRHomePageItem.class, String.class, String.class, String.class, HashMap.class, Boolean.TYPE });
    if (localObject != null) {
      return (Map)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramCJRHomePageItem, paramString1, paramString2, paramString3, paramHashMap, new Boolean(paramBoolean) }).toPatchJoinPoint());
    }
    String str2 = null;
    paramString2 = null;
    String str1 = null;
    localObject = null;
    if (paramHashMap != null)
    {
      str2 = (String)paramHashMap.get("search_type");
      paramString2 = (String)paramHashMap.get("search_category");
      str1 = (String)paramHashMap.get("search_term");
      localObject = (String)paramHashMap.get("search_result_type");
    }
    paramHashMap = new HashMap();
    if (!TextUtils.isEmpty(paramCJRHomePageItem.getmContainerInstanceID())) {
      paramHashMap.put("dimension40", paramCJRHomePageItem.getmContainerInstanceID());
    }
    paramHashMap.put("id", paramCJRHomePageItem.getItemID());
    paramHashMap.put("name", paramCJRHomePageItem.getName());
    paramHashMap.put("list", paramString1);
    paramHashMap.put("brand", paramCJRHomePageItem.getBrand());
    paramHashMap.put("price", Double.valueOf(paramCJRHomePageItem.getOfferPrice()));
    paramHashMap.put("position", paramString3);
    paramHashMap.put("dimension24", paramString1);
    paramHashMap.put("dimension25", paramString3);
    if (str2 != null) {
      paramHashMap.put("dimension26", str2);
    }
    if (paramString2 != null) {
      paramHashMap.put("dimension27", paramString2);
    }
    if (str1 != null) {
      paramHashMap.put("dimension28", str1);
    }
    if (localObject != null) {
      paramHashMap.put("dimension29", localObject);
    }
    paramHashMap.put("dimension38", paramCJRHomePageItem.getListId());
    if (TextUtils.isEmpty(paramCJRHomePageItem.getParentId())) {}
    for (paramCJRHomePageItem = paramCJRHomePageItem.getItemID();; paramCJRHomePageItem = paramCJRHomePageItem.getParentId())
    {
      paramHashMap.put("dimension31", paramCJRHomePageItem);
      return paramHashMap;
    }
  }
  
  /* Error */
  private static Map<String, Object> a(CJRHomePageItem paramCJRHomePageItem, boolean paramBoolean, String paramString1, int paramInt, HashMap<String, Object> paramHashMap, String paramString2)
  {
    // Byte code:
    //   0: ldc 2
    //   2: ldc -11
    //   4: bipush 6
    //   6: anewarray 247	java/lang/Class
    //   9: dup
    //   10: iconst_0
    //   11: ldc_w 643
    //   14: aastore
    //   15: dup
    //   16: iconst_1
    //   17: getstatic 362	java/lang/Boolean:TYPE	Ljava/lang/Class;
    //   20: aastore
    //   21: dup
    //   22: iconst_2
    //   23: ldc_w 305
    //   26: aastore
    //   27: dup
    //   28: iconst_3
    //   29: getstatic 383	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   32: aastore
    //   33: dup
    //   34: iconst_4
    //   35: ldc_w 380
    //   38: aastore
    //   39: dup
    //   40: iconst_5
    //   41: ldc_w 305
    //   44: aastore
    //   45: invokestatic 255	io/hansel/pebbletracesdk/HanselCrashReporter:getPatch	(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Lio/hansel/pebbletracesdk/codepatch/patch/Patch;
    //   48: astore 6
    //   50: aload 6
    //   52: ifnull +91 -> 143
    //   55: aload 6
    //   57: new 257	io/hansel/pebbletracesdk/codepatch/PatchJoinPoint$PatchJoinPointBuilder
    //   60: dup
    //   61: invokespecial 258	io/hansel/pebbletracesdk/codepatch/PatchJoinPoint$PatchJoinPointBuilder:<init>	()V
    //   64: aload 6
    //   66: invokevirtual 264	io/hansel/pebbletracesdk/codepatch/patch/Patch:getClassForPatch	()Ljava/lang/Class;
    //   69: invokevirtual 268	io/hansel/pebbletracesdk/codepatch/PatchJoinPoint$PatchJoinPointBuilder:setClassOfMethod	(Ljava/lang/Class;)Lio/hansel/pebbletracesdk/codepatch/PatchJoinPoint$PatchJoinPointBuilder;
    //   72: aload 6
    //   74: invokevirtual 272	io/hansel/pebbletracesdk/codepatch/patch/Patch:getMethodForPatch	()Ljava/lang/reflect/Method;
    //   77: invokevirtual 276	io/hansel/pebbletracesdk/codepatch/PatchJoinPoint$PatchJoinPointBuilder:setMethod	(Ljava/lang/reflect/Method;)Lio/hansel/pebbletracesdk/codepatch/PatchJoinPoint$PatchJoinPointBuilder;
    //   80: ldc 2
    //   82: invokevirtual 280	io/hansel/pebbletracesdk/codepatch/PatchJoinPoint$PatchJoinPointBuilder:setTarget	(Ljava/lang/Object;)Lio/hansel/pebbletracesdk/codepatch/PatchJoinPoint$PatchJoinPointBuilder;
    //   85: bipush 6
    //   87: anewarray 4	java/lang/Object
    //   90: dup
    //   91: iconst_0
    //   92: aload_0
    //   93: aastore
    //   94: dup
    //   95: iconst_1
    //   96: new 361	java/lang/Boolean
    //   99: dup
    //   100: iload_1
    //   101: invokespecial 367	java/lang/Boolean:<init>	(Z)V
    //   104: aastore
    //   105: dup
    //   106: iconst_2
    //   107: aload_2
    //   108: aastore
    //   109: dup
    //   110: iconst_3
    //   111: new 382	java/lang/Integer
    //   114: dup
    //   115: iload_3
    //   116: invokespecial 386	java/lang/Integer:<init>	(I)V
    //   119: aastore
    //   120: dup
    //   121: iconst_4
    //   122: aload 4
    //   124: aastore
    //   125: dup
    //   126: iconst_5
    //   127: aload 5
    //   129: aastore
    //   130: invokevirtual 284	io/hansel/pebbletracesdk/codepatch/PatchJoinPoint$PatchJoinPointBuilder:setArguments	([Ljava/lang/Object;)Lio/hansel/pebbletracesdk/codepatch/PatchJoinPoint$PatchJoinPointBuilder;
    //   133: invokevirtual 288	io/hansel/pebbletracesdk/codepatch/PatchJoinPoint$PatchJoinPointBuilder:toPatchJoinPoint	()Lio/hansel/pebbletracesdk/codepatch/PatchJoinPoint;
    //   136: invokevirtual 292	io/hansel/pebbletracesdk/codepatch/patch/Patch:apply	(Lio/hansel/pebbletracesdk/codepatch/PatchJoinPoint;)Ljava/lang/Object;
    //   139: checkcast 613	java/util/Map
    //   142: areturn
    //   143: new 380	java/util/HashMap
    //   146: dup
    //   147: invokespecial 432	java/util/HashMap:<init>	()V
    //   150: astore 11
    //   152: aconst_null
    //   153: astore 9
    //   155: aconst_null
    //   156: astore 6
    //   158: aconst_null
    //   159: astore 8
    //   161: aconst_null
    //   162: astore 7
    //   164: ldc_w 307
    //   167: astore 10
    //   169: aload 4
    //   171: ifnull +55 -> 226
    //   174: aload 4
    //   176: ldc_w 653
    //   179: invokevirtual 501	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   182: checkcast 305	java/lang/String
    //   185: astore 9
    //   187: aload 4
    //   189: ldc_w 655
    //   192: invokevirtual 501	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   195: checkcast 305	java/lang/String
    //   198: astore 6
    //   200: aload 4
    //   202: ldc_w 657
    //   205: invokevirtual 501	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   208: checkcast 305	java/lang/String
    //   211: astore 8
    //   213: aload 4
    //   215: ldc_w 659
    //   218: invokevirtual 501	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   221: checkcast 305	java/lang/String
    //   224: astore 7
    //   226: aload 10
    //   228: astore_2
    //   229: aload_0
    //   230: ifnull +56 -> 286
    //   233: aload 10
    //   235: astore_2
    //   236: aload_0
    //   237: invokevirtual 774	net/one97/paytm/common/entity/shopping/CJRHomePageItem:getBrand	()Ljava/lang/String;
    //   240: ifnull +46 -> 286
    //   243: aload 10
    //   245: astore_2
    //   246: aload 5
    //   248: ifnull +38 -> 286
    //   251: new 442	java/lang/StringBuilder
    //   254: dup
    //   255: invokespecial 443	java/lang/StringBuilder:<init>	()V
    //   258: ldc_w 792
    //   261: invokevirtual 447	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   264: aload_0
    //   265: invokevirtual 774	net/one97/paytm/common/entity/shopping/CJRHomePageItem:getBrand	()Ljava/lang/String;
    //   268: invokevirtual 447	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   271: ldc_w 794
    //   274: invokevirtual 447	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   277: aload 5
    //   279: invokevirtual 447	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   282: invokevirtual 453	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   285: astore_2
    //   286: iload_1
    //   287: ifeq +320 -> 607
    //   290: new 442	java/lang/StringBuilder
    //   293: dup
    //   294: invokespecial 443	java/lang/StringBuilder:<init>	()V
    //   297: ldc_w 749
    //   300: invokevirtual 447	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   303: aload_2
    //   304: invokevirtual 447	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   307: invokevirtual 453	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   310: astore_2
    //   311: aload_0
    //   312: invokevirtual 769	net/one97/paytm/common/entity/shopping/CJRHomePageItem:getmContainerInstanceID	()Ljava/lang/String;
    //   315: invokestatic 350	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   318: ifne +16 -> 334
    //   321: aload 11
    //   323: ldc_w 465
    //   326: aload_0
    //   327: invokevirtual 769	net/one97/paytm/common/entity/shopping/CJRHomePageItem:getmContainerInstanceID	()Ljava/lang/String;
    //   330: invokevirtual 438	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   333: pop
    //   334: aload 11
    //   336: ldc 115
    //   338: aload_0
    //   339: invokevirtual 772	net/one97/paytm/common/entity/shopping/CJRHomePageItem:getItemID	()Ljava/lang/String;
    //   342: invokevirtual 438	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   345: pop
    //   346: aload 11
    //   348: ldc 119
    //   350: aload_0
    //   351: invokevirtual 773	net/one97/paytm/common/entity/shopping/CJRHomePageItem:getName	()Ljava/lang/String;
    //   354: invokevirtual 438	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   357: pop
    //   358: aload 11
    //   360: ldc -33
    //   362: aload_2
    //   363: invokevirtual 438	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   366: pop
    //   367: aload 11
    //   369: ldc 127
    //   371: aload_0
    //   372: invokevirtual 774	net/one97/paytm/common/entity/shopping/CJRHomePageItem:getBrand	()Ljava/lang/String;
    //   375: invokevirtual 438	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   378: pop
    //   379: aload 11
    //   381: ldc -121
    //   383: aload_0
    //   384: invokevirtual 778	net/one97/paytm/common/entity/shopping/CJRHomePageItem:getOfferPrice	()F
    //   387: invokestatic 797	java/lang/String:valueOf	(F)Ljava/lang/String;
    //   390: invokevirtual 438	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   393: pop
    //   394: aload 11
    //   396: ldc -117
    //   398: iconst_1
    //   399: invokestatic 594	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   402: invokevirtual 438	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   405: pop
    //   406: aload 11
    //   408: ldc_w 616
    //   411: new 442	java/lang/StringBuilder
    //   414: dup
    //   415: invokespecial 443	java/lang/StringBuilder:<init>	()V
    //   418: ldc_w 307
    //   421: invokevirtual 447	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   424: iload_3
    //   425: iconst_1
    //   426: iadd
    //   427: invokevirtual 450	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   430: invokevirtual 453	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   433: invokevirtual 438	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   436: pop
    //   437: aload 11
    //   439: ldc_w 434
    //   442: aload_2
    //   443: invokevirtual 438	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   446: pop
    //   447: aload 11
    //   449: ldc_w 440
    //   452: new 442	java/lang/StringBuilder
    //   455: dup
    //   456: invokespecial 443	java/lang/StringBuilder:<init>	()V
    //   459: ldc_w 307
    //   462: invokevirtual 447	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   465: iload_3
    //   466: iconst_1
    //   467: iadd
    //   468: invokevirtual 450	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   471: invokevirtual 453	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   474: invokevirtual 438	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   477: pop
    //   478: aload 9
    //   480: ifnull +14 -> 494
    //   483: aload 11
    //   485: ldc_w 455
    //   488: aload 9
    //   490: invokevirtual 438	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   493: pop
    //   494: aload 6
    //   496: ifnull +14 -> 510
    //   499: aload 11
    //   501: ldc_w 457
    //   504: aload 6
    //   506: invokevirtual 438	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   509: pop
    //   510: aload 8
    //   512: ifnull +14 -> 526
    //   515: aload 11
    //   517: ldc_w 459
    //   520: aload 8
    //   522: invokevirtual 438	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   525: pop
    //   526: aload 7
    //   528: ifnull +14 -> 542
    //   531: aload 11
    //   533: ldc_w 461
    //   536: aload 7
    //   538: invokevirtual 438	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   541: pop
    //   542: aload_0
    //   543: invokevirtual 785	net/one97/paytm/common/entity/shopping/CJRHomePageItem:getParentId	()Ljava/lang/String;
    //   546: invokestatic 350	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   549: ifeq +34 -> 583
    //   552: aload_0
    //   553: invokevirtual 772	net/one97/paytm/common/entity/shopping/CJRHomePageItem:getItemID	()Ljava/lang/String;
    //   556: astore_2
    //   557: aload 11
    //   559: ldc_w 463
    //   562: aload_2
    //   563: invokevirtual 438	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   566: pop
    //   567: aload 11
    //   569: ldc_w 469
    //   572: aload_0
    //   573: invokevirtual 784	net/one97/paytm/common/entity/shopping/CJRHomePageItem:getListId	()Ljava/lang/String;
    //   576: invokevirtual 438	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   579: pop
    //   580: goto +24 -> 604
    //   583: aload_0
    //   584: invokevirtual 785	net/one97/paytm/common/entity/shopping/CJRHomePageItem:getParentId	()Ljava/lang/String;
    //   587: astore_2
    //   588: goto -31 -> 557
    //   591: astore_0
    //   592: aload_0
    //   593: invokevirtual 766	java/lang/Exception:printStackTrace	()V
    //   596: goto +8 -> 604
    //   599: astore_0
    //   600: aload_0
    //   601: invokevirtual 766	java/lang/Exception:printStackTrace	()V
    //   604: aload 11
    //   606: areturn
    //   607: goto -296 -> 311
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	610	0	paramCJRHomePageItem	CJRHomePageItem
    //   0	610	1	paramBoolean	boolean
    //   0	610	2	paramString1	String
    //   0	610	3	paramInt	int
    //   0	610	4	paramHashMap	HashMap<String, Object>
    //   0	610	5	paramString2	String
    //   48	457	6	localObject	Object
    //   162	375	7	str1	String
    //   159	362	8	str2	String
    //   153	336	9	str3	String
    //   167	77	10	str4	String
    //   150	455	11	localHashMap	HashMap
    // Exception table:
    //   from	to	target	type
    //   542	557	591	java/lang/Exception
    //   557	580	591	java/lang/Exception
    //   583	588	591	java/lang/Exception
    //   174	226	599	java/lang/Exception
    //   236	243	599	java/lang/Exception
    //   251	286	599	java/lang/Exception
    //   290	311	599	java/lang/Exception
    //   311	334	599	java/lang/Exception
    //   334	478	599	java/lang/Exception
    //   483	494	599	java/lang/Exception
    //   499	510	599	java/lang/Exception
    //   515	526	599	java/lang/Exception
    //   531	542	599	java/lang/Exception
    //   592	596	599	java/lang/Exception
  }
  
  private Map<String, Object> a(CJROrderSummary paramCJROrderSummary, String paramString, CJRParcelTrackingInfo paramCJRParcelTrackingInfo)
  {
    Object localObject1 = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJROrderSummary.class, String.class, CJRParcelTrackingInfo.class });
    if (localObject1 != null) {
      return (Map)((Patch)localObject1).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject1).getClassForPatch()).setMethod(((Patch)localObject1).getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJROrderSummary, paramString, paramCJRParcelTrackingInfo }).toPatchJoinPoint());
    }
    Object localObject2 = "Paytm Store - Android";
    localObject1 = localObject2;
    if (!TextUtils.isEmpty(paramString))
    {
      localObject1 = localObject2;
      if (paramString.trim().length() > 0) {
        localObject1 = paramString;
      }
    }
    if (TextUtils.isEmpty(paramCJROrderSummary.getPromoCode())) {}
    for (paramString = "";; paramString = paramCJROrderSummary.getPromoCode())
    {
      localObject2 = new HashMap();
      ((Map)localObject2).put("id", paramCJROrderSummary.getId());
      ((Map)localObject2).put("affiliation", localObject1);
      ((Map)localObject2).put("revenue", String.valueOf(paramCJROrderSummary.getGrandTotal()));
      ((Map)localObject2).put("tax", "0");
      ((Map)localObject2).put("shipping", String.valueOf(paramCJROrderSummary.getShippingAmount()));
      if ((paramCJRParcelTrackingInfo != null) && (paramCJRParcelTrackingInfo.getCartItemsForTrackingInfo() != null) && (paramCJRParcelTrackingInfo.getCartItemsForTrackingInfo().size() == 1))
      {
        paramCJROrderSummary = (CJRCartProduct)paramCJRParcelTrackingInfo.getCartItemsForTrackingInfo().get(0);
        if (paramCJROrderSummary != null)
        {
          paramCJROrderSummary = paramCJROrderSummary.getTrackingInfo();
          if ((paramCJROrderSummary != null) && (paramCJROrderSummary.getListName() != null)) {
            ((Map)localObject2).put("list", String.valueOf(paramCJROrderSummary.getListName()));
          }
        }
      }
      if (!TextUtils.isEmpty(paramString)) {
        ((Map)localObject2).put("coupon", paramString);
      }
      return (Map<String, Object>)localObject2;
    }
  }
  
  private Map<String, Object> a(CJROrderSummary paramCJROrderSummary, CJRParcelTrackingInfo paramCJRParcelTrackingInfo, String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJROrderSummary.class, CJRParcelTrackingInfo.class, String.class });
    if (localPatch != null) {
      return (Map)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJROrderSummary, paramCJRParcelTrackingInfo, paramString }).toPatchJoinPoint());
    }
    return DataLayer.mapOf(new Object[] { "purchase", DataLayer.mapOf(new Object[] { "actionField", a(paramCJROrderSummary, paramString, paramCJRParcelTrackingInfo), "products", a(paramCJROrderSummary, paramCJRParcelTrackingInfo) }) });
  }
  
  public static void a()
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", null);
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[0]).toPatchJoinPoint());
    }
    for (;;)
    {
      return;
      try
      {
        if (m == null)
        {
          m = new HandlerThread("GTMHandlerThread");
          m.start();
          n = new Handler(m.getLooper());
          return;
        }
      }
      catch (Exception localException)
      {
        localException.printStackTrace();
      }
    }
  }
  
  public static void a(Context paramContext, String paramString1, String paramString2, ArrayList<CJRGridProduct> paramArrayList, String paramString3, boolean paramBoolean)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, String.class, String.class, ArrayList.class, String.class, Boolean.TYPE });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext, paramString1, paramString2, paramArrayList, paramString3, new Boolean(paramBoolean) }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.21(paramArrayList, paramString2, paramContext, paramString1, paramBoolean, paramString3));
  }
  
  public static void a(Context paramContext, String paramString, ArrayList<CJRCartProduct> paramArrayList)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, String.class, ArrayList.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext, paramString, paramArrayList }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.45(paramContext, paramString, paramArrayList));
  }
  
  public static void a(Context paramContext, String paramString1, boolean paramBoolean1, ArrayList<CJRHomePageLayout> paramArrayList, String paramString2, boolean paramBoolean2)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, String.class, Boolean.TYPE, ArrayList.class, String.class, Boolean.TYPE });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext, paramString1, new Boolean(paramBoolean1), paramArrayList, paramString2, new Boolean(paramBoolean2) }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.28(paramArrayList, paramString2, paramBoolean1, paramContext, paramString1, paramBoolean2));
  }
  
  private static void a(Context paramContext, HashMap paramHashMap, ArrayList<CJROrderedCart> paramArrayList)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, HashMap.class, ArrayList.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext, paramHashMap, paramArrayList }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.26(paramContext, paramHashMap, paramArrayList));
  }
  
  public static void a(Context paramContext, List<CJRHomePageItem> paramList, String paramString1, String paramString2, String paramString3)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, List.class, String.class, String.class, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext, paramList, paramString1, paramString2, paramString3 }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.4(paramContext, paramList, paramString2, paramString1, paramString3));
  }
  
  public static void a(Context paramContext, List<CJRGridProduct> paramList, String paramString1, String paramString2, String paramString3, int paramInt, String paramString4, String paramString5)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, List.class, String.class, String.class, String.class, Integer.TYPE, String.class, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext, paramList, paramString1, paramString2, paramString3, new Integer(paramInt), paramString4, paramString5 }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.2(paramContext, paramList, paramString3, paramString1, paramString2, paramInt, paramString4, paramString5));
  }
  
  public static void a(Context paramContext, List<CJRGridProduct> paramList, String paramString1, String paramString2, HashMap<String, Object> paramHashMap, int paramInt, String paramString3)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, List.class, String.class, String.class, HashMap.class, Integer.TYPE, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext, paramList, paramString1, paramString2, paramHashMap, new Integer(paramInt), paramString3 }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.59(paramContext, paramList, paramHashMap, paramString1, paramString2, paramInt, paramString3));
  }
  
  public static void a(Context paramContext, List<CJRHomePageItem> paramList, String paramString1, HashMap<String, Object> paramHashMap, String paramString2)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, List.class, String.class, HashMap.class, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext, paramList, paramString1, paramHashMap, paramString2 }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.60(paramContext, paramList, paramHashMap, paramString1, paramString2));
  }
  
  public static void a(Context paramContext, CJRHomePageItem paramCJRHomePageItem, int paramInt, String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, CJRHomePageItem.class, Integer.TYPE, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext, paramCJRHomePageItem, new Integer(paramInt), paramString }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.23(paramInt, paramCJRHomePageItem, paramString, paramContext));
  }
  
  public static void a(Context paramContext, CJROrderSummary paramCJROrderSummary)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, CJROrderSummary.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext, paramCJROrderSummary }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.11(paramCJROrderSummary, paramContext));
  }
  
  public static void a(Context paramContext, WishListProduct paramWishListProduct, int paramInt)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, WishListProduct.class, Integer.TYPE });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext, paramWishListProduct, new Integer(paramInt) }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.22(paramInt, paramContext, paramWishListProduct));
  }
  
  public static void a(Context paramContext, boolean paramBoolean, CJROrderSummary paramCJROrderSummary, String paramString1, String paramString2)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, Boolean.TYPE, CJROrderSummary.class, String.class, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext, new Boolean(paramBoolean), paramCJROrderSummary, paramString1, paramString2 }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.31(paramCJROrderSummary, paramContext, paramBoolean, paramString1, paramString2));
  }
  
  public static void a(String paramString, Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { String.class, Context.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString, paramContext }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.15(paramContext, paramString));
  }
  
  public static void a(String paramString1, Context paramContext, String paramString2)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { String.class, Context.class, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString1, paramContext, paramString2 }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.55(paramContext, paramString2, paramString1));
  }
  
  public static void a(String paramString1, String paramString2, Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { String.class, String.class, Context.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString1, paramString2, paramContext }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.8(paramContext, paramString1, paramString2));
  }
  
  public static void a(String paramString1, String paramString2, String paramString3, Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { String.class, String.class, String.class, Context.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString1, paramString2, paramString3, paramContext }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.5(paramContext, paramString1, paramString2, paramString3));
  }
  
  public static void a(String paramString1, String paramString2, String paramString3, String paramString4, Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { String.class, String.class, String.class, String.class, Context.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString1, paramString2, paramString3, paramString4, paramContext }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.6(paramContext, paramString1, paramString2, paramString3, paramString4));
  }
  
  public static void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { String.class, String.class, String.class, String.class, String.class, String.class, Context.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramContext }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.7(paramContext, paramString1, paramString2, paramString3, paramString4, paramString5, paramString6));
  }
  
  public static void a(String paramString1, String paramString2, boolean paramBoolean, String paramString3, String paramString4, Context paramContext, ArrayList<CJROrderedCart> paramArrayList, String paramString5)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "a", new Class[] { String.class, String.class, Boolean.TYPE, String.class, String.class, Context.class, ArrayList.class, String.class });
    if (localObject != null) {
      ((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString1, paramString2, new Boolean(paramBoolean), paramString3, paramString4, paramContext, paramArrayList, paramString5 }).toPatchJoinPoint());
    }
    for (;;)
    {
      return;
      try
      {
        localObject = new HashMap();
        ((HashMap)localObject).put("screenName", "/summary");
        ((HashMap)localObject).put("gdr_chkout_amount", paramString3);
        ((HashMap)localObject).put("gdr_referrer", paramString4);
        if (paramBoolean)
        {
          if (!TextUtils.isEmpty(paramString1)) {
            d("/summary", paramString1, paramContext);
          }
        }
        else
        {
          if ((paramString1 != null) && (paramString1.equalsIgnoreCase("Paytm Hotel")))
          {
            d("hotel_order_summary", "Hotel", paramContext);
            a("screen_loaded_order_summary", (Map)localObject, paramContext);
            return;
          }
          if (paramString1.equalsIgnoreCase("Tickets"))
          {
            d("/summary", "BusTicket", paramContext);
            return;
          }
          if (paramString1.equalsIgnoreCase("Flights"))
          {
            d("/summary", "Flights", paramContext);
            a("flt_ordersmmry_loaded", (Map)localObject, paramContext);
            return;
          }
          if (paramString1.equalsIgnoreCase("Movie Tickets"))
          {
            d("/summary", "Movies", paramContext);
            a("movie_order_screen_loaded", paramArrayList, paramContext, paramString3);
            return;
          }
          if (paramString1.equalsIgnoreCase("Trains"))
          {
            d("/summary", "Trains", paramContext);
            return;
          }
          if (paramString1.equalsIgnoreCase("Wallet"))
          {
            d("/summary", "Wallet", paramContext);
            a("wallet_screen_loaded_summary", (Map)localObject, paramContext);
            return;
          }
          if (b(paramString5, paramString1))
          {
            paramString1 = b(paramString5, Long.parseLong(paramString2), paramString1);
            if ((paramString1 != null) && (!TextUtils.isEmpty(paramString1)))
            {
              d("/summary", paramString1, paramContext);
              return;
            }
            d("/summary", "Recharge", paramContext);
            a("recharge_screen_loaded_summary", (Map)localObject, paramContext);
            return;
          }
          d("/summary", "Marketplace", paramContext);
          a("marketplace_screen_loaded_summary", (Map)localObject, paramContext);
          a(paramContext, (HashMap)localObject, paramArrayList);
          return;
        }
      }
      catch (Exception paramString1) {}
    }
  }
  
  public static void a(String paramString1, ArrayList<CJROrderedCart> paramArrayList, Context paramContext, String paramString2)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { String.class, ArrayList.class, Context.class, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString1, paramArrayList, paramContext, paramString2 }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.9(paramArrayList, paramContext, paramString1));
  }
  
  public static void a(String paramString, Map<String, Object> paramMap, Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { String.class, Map.class, Context.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString, paramMap, paramContext }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.10(paramContext, paramString, paramMap));
  }
  
  public static void a(String paramString1, CJRCartProduct paramCJRCartProduct, Context paramContext, String paramString2)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { String.class, CJRCartProduct.class, Context.class, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString1, paramCJRCartProduct, paramContext, paramString2 }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.52(paramCJRCartProduct, paramString2, paramString1, paramContext));
  }
  
  public static void a(String paramString, CJRHomePageItem paramCJRHomePageItem, Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { String.class, CJRHomePageItem.class, Context.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString, paramCJRHomePageItem, paramContext }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.30(paramContext, paramCJRHomePageItem, paramString));
  }
  
  public static void a(String paramString1, boolean paramBoolean, Context paramContext, String paramString2, String paramString3)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { String.class, Boolean.TYPE, Context.class, String.class, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString1, new Boolean(paramBoolean), paramContext, paramString2, paramString3 }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.27(paramBoolean, paramString1, paramContext, paramString2, paramString3));
  }
  
  public static void a(ArrayList<WishListProduct> paramArrayList, Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { ArrayList.class, Context.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramArrayList, paramContext }).toPatchJoinPoint());
      return;
    }
    TagManager.getInstance(paramContext).getDataLayer();
  }
  
  public static void a(ArrayList<CJRHomePageItem> paramArrayList, Context paramContext, int paramInt1, String paramString1, HashMap<String, Object> paramHashMap, boolean paramBoolean, int paramInt2, String paramString2)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { ArrayList.class, Context.class, Integer.TYPE, String.class, HashMap.class, Boolean.TYPE, Integer.TYPE, String.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramArrayList, paramContext, new Integer(paramInt1), paramString1, paramHashMap, new Boolean(paramBoolean), new Integer(paramInt2), paramString2 }).toPatchJoinPoint());
      return;
    }
    paramArrayList = new a.20(paramString1, TagManager.getInstance(paramContext).getDataLayer(), paramContext, paramArrayList, paramHashMap, paramInt1, paramBoolean, paramInt2, paramString2);
    l.a().a(paramArrayList);
  }
  
  public static void a(ArrayList<CJRHomePageItem> paramArrayList, Context paramContext, int paramInt1, String paramString1, HashMap<String, Object> paramHashMap, boolean paramBoolean, int paramInt2, String paramString2, String paramString3)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { ArrayList.class, Context.class, Integer.TYPE, String.class, HashMap.class, Boolean.TYPE, Integer.TYPE, String.class, String.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramArrayList, paramContext, new Integer(paramInt1), paramString1, paramHashMap, new Boolean(paramBoolean), new Integer(paramInt2), paramString2, paramString3 }).toPatchJoinPoint());
      return;
    }
    paramArrayList = new a.19(paramString1, TagManager.getInstance(paramContext).getDataLayer(), paramContext, paramArrayList, paramHashMap, paramInt1, paramBoolean, paramInt2, paramString2, paramString3);
    l.a().a(paramArrayList);
  }
  
  public static void a(List<Map<String, Object>> paramList, String paramString1, Context paramContext, String paramString2)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { List.class, String.class, Context.class, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramList, paramString1, paramContext, paramString2 }).toPatchJoinPoint());
    }
    while ((n == null) || (paramList.size() <= 0)) {
      return;
    }
    n.post(new a.53(paramContext, paramString2, paramString1, paramList));
  }
  
  public static void a(Map<String, Object> paramMap, Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Map.class, Context.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramMap, paramContext }).toPatchJoinPoint());
    }
    while (paramMap == null) {
      return;
    }
    try
    {
      paramMap.put("Device_Brand", "");
      paramMap.put("Device_Name", "");
      paramMap.put("Advertising_ID", "");
      paramMap.put("IMEI", "");
      paramMap.put("Custom_Device_ID", "");
      paramMap.put("Customer_Id", "");
      paramMap.put("Contact_Number", "");
      paramMap.put("user_email", "");
      paramMap.put("Channel_ID", "");
      return;
    }
    catch (Exception paramMap)
    {
      paramMap.printStackTrace();
    }
  }
  
  public static void a(CJREventStoreFrontItemDetailModel paramCJREventStoreFrontItemDetailModel, Context paramContext, int paramInt, String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJREventStoreFrontItemDetailModel.class, Context.class, Integer.TYPE, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramCJREventStoreFrontItemDetailModel, paramContext, new Integer(paramInt), paramString }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.54(paramContext, paramInt, paramString, paramCJREventStoreFrontItemDetailModel));
  }
  
  public static void a(CJRDetailProduct paramCJRDetailProduct, Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJRDetailProduct.class, Context.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramCJRDetailProduct, paramContext }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.1(paramCJRDetailProduct, paramContext));
  }
  
  public static void a(CJRHomePageItem paramCJRHomePageItem, Context paramContext, int paramInt, String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJRHomePageItem.class, Context.class, Integer.TYPE, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramCJRHomePageItem, paramContext, new Integer(paramInt), paramString }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.17(paramContext, paramInt, paramString, paramCJRHomePageItem));
  }
  
  public static void a(CJRHomePageItem paramCJRHomePageItem, Context paramContext, int paramInt, String paramString1, String paramString2, String paramString3)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJRHomePageItem.class, Context.class, Integer.TYPE, String.class, String.class, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramCJRHomePageItem, paramContext, new Integer(paramInt), paramString1, paramString2, paramString3 }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.18(paramContext, paramInt, paramString1, paramCJRHomePageItem, paramString2, paramString3));
  }
  
  public static void a(CJRHomePageItem paramCJRHomePageItem, String paramString1, Context paramContext, String paramString2, int paramInt, String paramString3)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJRHomePageItem.class, String.class, Context.class, String.class, Integer.TYPE, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramCJRHomePageItem, paramString1, paramContext, paramString2, new Integer(paramInt), paramString3 }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.58(paramString2, paramString3, paramContext, paramString1, paramCJRHomePageItem, paramInt));
  }
  
  private static int b(ArrayList<CJRHomePageItem> paramArrayList)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "b", new Class[] { ArrayList.class });
    int i1;
    if (localPatch != null) {
      i1 = Conversions.intValue(localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramArrayList }).toPatchJoinPoint()));
    }
    do
    {
      int i2;
      do
      {
        return i1;
        i2 = 0;
        i1 = i2;
      } while (paramArrayList == null);
      i1 = i2;
    } while (paramArrayList.size() <= 0);
    if (paramArrayList.size() > 3) {
      return 3;
    }
    return paramArrayList.size();
  }
  
  public static String b(Context paramContext)
  {
    Object localObject2 = null;
    Object localObject1 = HanselCrashReporter.getPatch(a.class, "b", new Class[] { Context.class });
    if (localObject1 != null) {
      paramContext = (String)((Patch)localObject1).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject1).getClassForPatch()).setMethod(((Patch)localObject1).getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext }).toPatchJoinPoint());
    }
    for (;;)
    {
      return paramContext;
      localObject1 = null;
      try
      {
        if (b.k(paramContext)) {
          localObject1 = b.h(paramContext);
        }
        paramContext = (Context)localObject2;
        if (localObject1 != null)
        {
          paramContext = (Context)localObject2;
          if (!TextUtils.isEmpty((CharSequence)localObject1))
          {
            paramContext = MessageDigest.getInstance("SHA-256");
            paramContext.update(((String)localObject1).getBytes());
            paramContext = Base64.encodeToString(paramContext.digest(), 11);
            return paramContext;
          }
        }
      }
      catch (NoSuchAlgorithmException paramContext) {}
    }
    return null;
  }
  
  private static String b(String paramString)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "b", new Class[] { String.class });
    if (localObject != null) {
      return (String)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString }).toPatchJoinPoint());
    }
    localObject = paramString;
    if (paramString.length() > 0)
    {
      localObject = paramString;
      if (paramString.charAt(paramString.length() - 1) == '-') {
        localObject = paramString.substring(0, paramString.length() - 1);
      }
    }
    return (String)localObject;
  }
  
  private static String b(String paramString1, long paramLong, String paramString2)
  {
    localObject = HanselCrashReporter.getPatch(a.class, "b", new Class[] { String.class, Long.TYPE, String.class });
    if (localObject != null) {
      return (String)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString1, new Long(paramLong), paramString2 }).toPatchJoinPoint());
    }
    localObject = "";
    try
    {
      JSONObject localJSONObject = new JSONObject(paramString1);
      paramString1 = (String)localObject;
      if (localJSONObject.has(paramString2.toLowerCase()))
      {
        paramString2 = localJSONObject.getJSONObject(paramString2.toLowerCase());
        paramString1 = (String)localObject;
        if (paramString2 != null)
        {
          paramString1 = (String)localObject;
          if (paramString2.has(String.valueOf(paramLong))) {
            paramString1 = paramString2.getJSONObject(String.valueOf(paramLong)).getString("n");
          }
        }
      }
    }
    catch (JSONException paramString1)
    {
      for (;;)
      {
        paramString1.printStackTrace();
        paramString1 = (String)localObject;
      }
    }
    return paramString1;
  }
  
  private static String b(String paramString, boolean paramBoolean, CJRHomePageLayout paramCJRHomePageLayout)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "b", new Class[] { String.class, Boolean.TYPE, CJRHomePageLayout.class });
    if (localObject != null) {
      return (String)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString, new Boolean(paramBoolean), paramCJRHomePageLayout }).toPatchJoinPoint());
    }
    localObject = "";
    if (paramString != null) {
      localObject = paramString;
    }
    paramString = (String)localObject;
    if (paramBoolean) {
      paramString = (String)localObject + "-search";
    }
    localObject = paramString;
    if (paramCJRHomePageLayout != null)
    {
      localObject = paramString;
      if (paramCJRHomePageLayout.getName() != null) {
        localObject = paramString + "-" + paramCJRHomePageLayout.getName();
      }
    }
    return (String)localObject;
  }
  
  private static String b(String paramString, boolean paramBoolean, CJRHomePageLayoutV2 paramCJRHomePageLayoutV2)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "b", new Class[] { String.class, Boolean.TYPE, CJRHomePageLayoutV2.class });
    if (localObject != null) {
      return (String)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString, new Boolean(paramBoolean), paramCJRHomePageLayoutV2 }).toPatchJoinPoint());
    }
    localObject = "";
    if (paramString != null) {
      localObject = paramString;
    }
    paramString = (String)localObject;
    if (paramBoolean) {
      paramString = (String)localObject + "-search";
    }
    localObject = paramString;
    if (paramCJRHomePageLayoutV2 != null)
    {
      localObject = paramString;
      if (paramCJRHomePageLayoutV2.getName() != null) {
        localObject = paramString + "-" + paramCJRHomePageLayoutV2.getName();
      }
    }
    return (String)localObject;
  }
  
  private static ArrayList<Map<String, Object>> b(ArrayList<CJRHomePageItem> paramArrayList, String paramString1, HashMap<String, Object> paramHashMap, int paramInt1, boolean paramBoolean, int paramInt2, String paramString2)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "b", new Class[] { ArrayList.class, String.class, HashMap.class, Integer.TYPE, Boolean.TYPE, Integer.TYPE, String.class });
    if (localObject != null) {
      return (ArrayList)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramArrayList, paramString1, paramHashMap, new Integer(paramInt1), new Boolean(paramBoolean), new Integer(paramInt2), paramString2 }).toPatchJoinPoint());
    }
    localObject = new ArrayList();
    int i1 = 0;
    while (i1 < paramArrayList.size())
    {
      ((ArrayList)localObject).add(a((CJRHomePageItem)paramArrayList.get(i1), "Similar Products", paramString1, String.valueOf(paramInt1 * 4 + (i1 + 1) + paramInt2), paramHashMap, paramBoolean));
      if (paramString2 != null)
      {
        HashMap localHashMap = new HashMap();
        localHashMap.put("dimension39", paramString2);
        ((ArrayList)localObject).add(localHashMap);
      }
      i1 += 1;
    }
    return (ArrayList<Map<String, Object>>)localObject;
  }
  
  private static ArrayList<Map<String, Object>> b(ArrayList<CJRHomePageItem> paramArrayList, String paramString1, HashMap<String, Object> paramHashMap, int paramInt1, boolean paramBoolean, int paramInt2, String paramString2, String paramString3)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "b", new Class[] { ArrayList.class, String.class, HashMap.class, Integer.TYPE, Boolean.TYPE, Integer.TYPE, String.class, String.class });
    if (localObject != null) {
      return (ArrayList)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramArrayList, paramString1, paramHashMap, new Integer(paramInt1), new Boolean(paramBoolean), new Integer(paramInt2), paramString2, paramString3 }).toPatchJoinPoint());
    }
    ArrayList localArrayList = new ArrayList();
    int i1 = 0;
    while (i1 < paramArrayList.size())
    {
      localObject = "";
      if (((CJRHomePageItem)paramArrayList.get(i1)).getSource() != null) {
        localObject = "/" + ((CJRHomePageItem)paramArrayList.get(i1)).getSource();
      }
      localObject = paramString1 + "-" + ((CJRHomePageItem)paramArrayList.get(i1)).getParentItem() + (String)localObject;
      localArrayList.add(a((CJRHomePageItem)paramArrayList.get(i1), (String)localObject, paramString1, String.valueOf(paramInt1 * 4 + (i1 + 1) + paramInt2), paramHashMap, paramBoolean));
      if (paramString2 != null)
      {
        localObject = new HashMap();
        ((Map)localObject).put("dimension39", paramString2);
        ((Map)localObject).put("dimension24", paramString3);
        localArrayList.add(localObject);
      }
      i1 += 1;
    }
    return localArrayList;
  }
  
  private static ArrayList<Map<String, Object>> b(List<CJRGridProduct> paramList, String paramString1, String paramString2, int paramInt, HashMap<String, Object> paramHashMap, boolean paramBoolean, String paramString3, String paramString4)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "b", new Class[] { List.class, String.class, String.class, Integer.TYPE, HashMap.class, Boolean.TYPE, String.class, String.class });
    if (localObject != null) {
      return (ArrayList)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramList, paramString1, paramString2, new Integer(paramInt), paramHashMap, new Boolean(paramBoolean), paramString3, paramString4 }).toPatchJoinPoint());
    }
    localObject = new ArrayList();
    ArrayList localArrayList = new ArrayList();
    localArrayList.addAll(paramList);
    int i1 = 0;
    while (i1 < localArrayList.size())
    {
      ((ArrayList)localObject).add(a((CJRGridProduct)paramList.get(i1), paramBoolean, paramString1, paramString2, paramInt + i1 + 1, paramHashMap, paramString3, paramString4));
      i1 += 1;
    }
    return (ArrayList<Map<String, Object>>)localObject;
  }
  
  private static ArrayList<Map<String, Object>> b(List<CJRHomePageItem> paramList, String paramString1, HashMap<String, Object> paramHashMap, boolean paramBoolean, String paramString2)
  {
    localObject = HanselCrashReporter.getPatch(a.class, "b", new Class[] { List.class, String.class, HashMap.class, Boolean.TYPE, String.class });
    if (localObject != null) {
      return (ArrayList)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramList, paramString1, paramHashMap, new Boolean(paramBoolean), paramString2 }).toPatchJoinPoint());
    }
    localObject = new ArrayList();
    int i1 = 0;
    try
    {
      while (i1 < paramList.size())
      {
        ((ArrayList)localObject).add(a((CJRHomePageItem)paramList.get(i1), paramBoolean, paramString1, i1, paramHashMap, paramString2));
        i1 += 1;
      }
      return (ArrayList<Map<String, Object>>)localObject;
    }
    catch (Exception paramList) {}
  }
  
  private List<Object> b(CJRDetailProduct paramCJRDetailProduct, int paramInt, CJRItem paramCJRItem)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "b", new Class[] { CJRDetailProduct.class, Integer.TYPE, CJRItem.class });
    if (localPatch != null) {
      return (List)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJRDetailProduct, new Integer(paramInt), paramCJRItem }).toPatchJoinPoint());
    }
    return DataLayer.listOf(new Object[] { c(paramCJRDetailProduct, paramInt, paramCJRItem) });
  }
  
  private List<Object> b(CJRDetailProduct paramCJRDetailProduct, CJRItem paramCJRItem)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "b", new Class[] { CJRDetailProduct.class, CJRItem.class });
    if (localPatch != null) {
      return (List)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJRDetailProduct, paramCJRItem }).toPatchJoinPoint());
    }
    return DataLayer.listOf(new Object[] { c(paramCJRDetailProduct, paramCJRItem) });
  }
  
  private static Map<String, Object> b(HashMap<String, Object> paramHashMap, String paramString1, CJRHomePageItem paramCJRHomePageItem, String paramString2, int paramInt)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "b", new Class[] { HashMap.class, String.class, CJRHomePageItem.class, String.class, Integer.TYPE });
    if (localObject != null) {
      return (Map)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramHashMap, paramString1, paramCJRHomePageItem, paramString2, new Integer(paramInt) }).toPatchJoinPoint());
    }
    String str3 = null;
    localObject = null;
    String str2 = null;
    String str1 = null;
    if (paramHashMap != null)
    {
      str3 = (String)paramHashMap.get("search_type");
      localObject = (String)paramHashMap.get("search_category");
      str2 = (String)paramHashMap.get("search_term");
      str1 = (String)paramHashMap.get("search_result_type");
    }
    if (TextUtils.isEmpty(paramCJRHomePageItem.getParentId())) {}
    for (paramHashMap = paramCJRHomePageItem.getItemID();; paramHashMap = paramCJRHomePageItem.getParentId())
    {
      HashMap localHashMap = new HashMap();
      if (!TextUtils.isEmpty(paramCJRHomePageItem.getmContainerInstanceID())) {
        localHashMap.put("dimension40", paramCJRHomePageItem.getmContainerInstanceID());
      }
      localHashMap.put("id", paramCJRHomePageItem.getItemID());
      localHashMap.put("name", paramCJRHomePageItem.getName());
      localHashMap.put("category", paramString1);
      localHashMap.put("brand", paramCJRHomePageItem.getBrand());
      localHashMap.put("variant", "");
      localHashMap.put("price", Float.toString(paramCJRHomePageItem.getOfferPrice()));
      localHashMap.put("dimension24", paramString2);
      localHashMap.put("dimension25", "" + paramInt);
      localHashMap.put("position", "" + paramInt);
      if (str3 != null) {
        localHashMap.put("dimension26", str3);
      }
      if (localObject != null) {
        localHashMap.put("dimension27", localObject);
      }
      if (str2 != null) {
        localHashMap.put("dimension28", str2);
      }
      if (str1 != null) {
        localHashMap.put("dimension29", str1);
      }
      localHashMap.put("dimension31", paramHashMap);
      localHashMap.put("dimension38", paramCJRHomePageItem.getListId());
      return localHashMap;
    }
  }
  
  private static Map<String, Object> b(HashMap<String, Object> paramHashMap, CJRCartProduct paramCJRCartProduct, String paramString, int paramInt)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "b", new Class[] { HashMap.class, CJRCartProduct.class, String.class, Integer.TYPE });
    if (localObject != null) {
      return (Map)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramHashMap, paramCJRCartProduct, paramString, new Integer(paramInt) }).toPatchJoinPoint());
    }
    String str3 = null;
    localObject = null;
    String str2 = null;
    String str1 = null;
    if (paramHashMap != null)
    {
      str3 = (String)paramHashMap.get("search_type");
      localObject = (String)paramHashMap.get("search_category");
      str2 = (String)paramHashMap.get("search_term");
      str1 = (String)paramHashMap.get("search_result_type");
    }
    if (TextUtils.isEmpty(paramCJRCartProduct.getParentID())) {
      paramCJRCartProduct.getProductId();
    }
    for (;;)
    {
      paramHashMap = new HashMap();
      paramHashMap.put("id", paramCJRCartProduct.getProductId());
      paramHashMap.put("name", paramCJRCartProduct.getName());
      paramHashMap.put("category", paramCJRCartProduct.getCategoryPathForGTM());
      paramHashMap.put("brand", paramCJRCartProduct.getBrand());
      paramHashMap.put("variant", "");
      paramHashMap.put("price", paramCJRCartProduct.getDiscountedPrice());
      paramHashMap.put("dimension24", paramString);
      paramHashMap.put("dimension25", Integer.valueOf(paramInt));
      paramHashMap.put("position", Integer.valueOf(paramInt));
      if (str3 != null) {
        paramHashMap.put("dimension26", str3);
      }
      if (localObject != null) {
        paramHashMap.put("dimension27", localObject);
      }
      if (str2 != null) {
        paramHashMap.put("dimension28", str2);
      }
      if (str1 != null) {
        paramHashMap.put("dimension29", str1);
      }
      paramString = paramCJRCartProduct.getTrackingInfo();
      if ((paramString != null) && (paramString.getListId() != null)) {
        paramHashMap.put("dimension38", paramString.getListId());
      }
      if (!TextUtils.isEmpty(paramString.getContainerID())) {
        paramHashMap.put("dimension40", paramString.getContainerID());
      }
      paramHashMap.put("dimension43", paramCJRCartProduct.getCategoryIdForGTM());
      if ((paramString != null) && (paramString.getParentId() != null)) {
        paramHashMap.put("dimension31", paramString.getParentId());
      }
      return paramHashMap;
      paramCJRCartProduct.getParentID();
    }
  }
  
  private static Map<String, Object> b(HashMap<String, Object> paramHashMap, CJRHomePageItem paramCJRHomePageItem, String paramString1, int paramInt, String paramString2, String paramString3)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "b", new Class[] { HashMap.class, CJRHomePageItem.class, String.class, Integer.TYPE, String.class, String.class });
    if (localObject != null) {
      return (Map)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramHashMap, paramCJRHomePageItem, paramString1, new Integer(paramInt), paramString2, paramString3 }).toPatchJoinPoint());
    }
    String str3 = null;
    localObject = null;
    String str2 = null;
    String str1 = null;
    if (paramHashMap != null)
    {
      str3 = (String)paramHashMap.get("search_type");
      localObject = (String)paramHashMap.get("search_category");
      str2 = (String)paramHashMap.get("search_term");
      str1 = (String)paramHashMap.get("search_result_type");
    }
    HashMap localHashMap = new HashMap();
    if (!TextUtils.isEmpty(paramCJRHomePageItem.getmContainerInstanceID())) {
      localHashMap.put("dimension40", paramCJRHomePageItem.getmContainerInstanceID());
    }
    localHashMap.put("id", paramCJRHomePageItem.getItemID());
    localHashMap.put("name", paramCJRHomePageItem.getName());
    localHashMap.put("brand", paramCJRHomePageItem.getBrand());
    localHashMap.put("price", Float.toString(paramCJRHomePageItem.getOfferPrice()));
    localHashMap.put("dimension24", paramString1);
    localHashMap.put("dimension25", Integer.valueOf(paramInt));
    localHashMap.put("position", Integer.valueOf(paramInt));
    if (str3 != null) {
      localHashMap.put("dimension26", str3);
    }
    if (localObject != null) {
      localHashMap.put("dimension27", localObject);
    }
    if (str2 != null) {
      localHashMap.put("dimension28", str2);
    }
    if (str1 != null) {
      localHashMap.put("dimension29", str1);
    }
    if (TextUtils.isEmpty(paramCJRHomePageItem.getParentId())) {}
    for (paramHashMap = paramCJRHomePageItem.getItemID();; paramHashMap = paramCJRHomePageItem.getParentId())
    {
      localHashMap.put("dimension31", paramHashMap);
      localHashMap.put("dimension38", paramCJRHomePageItem.getListId());
      if (paramString2 != null) {
        localHashMap.put("dimension39", paramString2);
      }
      if (paramString3 != null) {
        localHashMap.put("dimension24", paramString3);
      }
      return localHashMap;
    }
  }
  
  private Map<String, Object> b(CJRCartProduct paramCJRCartProduct)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "b", new Class[] { CJRCartProduct.class });
    if (localObject != null) {
      return (Map)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJRCartProduct }).toPatchJoinPoint());
    }
    String str2 = paramCJRCartProduct.getCategoryPathForGTM();
    String str1 = paramCJRCartProduct.getCategoryIdForGTM();
    localObject = str2;
    if (TextUtils.isEmpty(str2))
    {
      localObject = paramCJRCartProduct.getVerticalLabel();
      str1 = paramCJRCartProduct.getVerticalId();
    }
    if (TextUtils.isEmpty(paramCJRCartProduct.getParentID())) {}
    for (str2 = paramCJRCartProduct.getProductId();; str2 = paramCJRCartProduct.getParentID())
    {
      HashMap localHashMap = new HashMap();
      localHashMap.put("dimension24", paramCJRCartProduct.getTrackingInfo().getListName());
      localHashMap.put("dimension25", "" + paramCJRCartProduct.getTrackingInfo().getListPosition());
      localHashMap.put("dimension26", paramCJRCartProduct.getTrackingInfo().getSearchType());
      localHashMap.put("dimension27", paramCJRCartProduct.getTrackingInfo().getSearchCategory());
      localHashMap.put("dimension28", paramCJRCartProduct.getTrackingInfo().getSearchTerm());
      localHashMap.put("dimension29", paramCJRCartProduct.getTrackingInfo().getSearchResultType());
      localHashMap.put("dimension31", str2);
      localHashMap.put("dimension40", paramCJRCartProduct.getTrackingInfo().getContainerID());
      localHashMap.put("dimension53", paramCJRCartProduct.getTrackingInfo().getSearchABValue());
      localHashMap.put("dimension38", paramCJRCartProduct.getTrackingInfo().getListId());
      localHashMap.put("id", paramCJRCartProduct.getProductId());
      localHashMap.put("name", paramCJRCartProduct.getName());
      localHashMap.put("category", localObject);
      localHashMap.put("brand", paramCJRCartProduct.getBrand());
      localHashMap.put("price", paramCJRCartProduct.getSellingPrice());
      localHashMap.put("quantity", paramCJRCartProduct.getQuantity());
      localHashMap.put("dimension43", str1);
      return localHashMap;
    }
  }
  
  private static Map<String, Object> b(CJRHomePageItem paramCJRHomePageItem, CJROrderSummaryProductDetail paramCJROrderSummaryProductDetail)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "b", new Class[] { CJRHomePageItem.class, CJROrderSummaryProductDetail.class });
    if (localObject != null) {
      return (Map)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramCJRHomePageItem, paramCJROrderSummaryProductDetail }).toPatchJoinPoint());
    }
    String str4 = null;
    String str1 = null;
    String str3 = null;
    String str2 = null;
    localObject = null;
    int i1 = 0;
    if (paramCJRHomePageItem != null)
    {
      str4 = paramCJRHomePageItem.getSearchType();
      str1 = paramCJRHomePageItem.getSearchCategory();
      str3 = paramCJRHomePageItem.getSearchTerm();
      str2 = paramCJRHomePageItem.getSearchResultType();
      localObject = paramCJRHomePageItem.getListName();
      i1 = paramCJRHomePageItem.getListPosition() + 1;
    }
    if (TextUtils.isEmpty(paramCJROrderSummaryProductDetail.getParentId())) {}
    for (String str5 = String.valueOf(paramCJROrderSummaryProductDetail.getId());; str5 = paramCJROrderSummaryProductDetail.getParentId())
    {
      HashMap localHashMap = new HashMap();
      if (!TextUtils.isEmpty(paramCJRHomePageItem.getmContainerInstanceID())) {
        localHashMap.put("dimension40", paramCJRHomePageItem.getmContainerInstanceID());
      }
      localHashMap.put("id", String.valueOf(paramCJROrderSummaryProductDetail.getId()));
      localHashMap.put("name", paramCJROrderSummaryProductDetail.getName());
      localHashMap.put("brand", paramCJROrderSummaryProductDetail.getBrandName());
      localHashMap.put("price", Double.valueOf(paramCJROrderSummaryProductDetail.getDiscountedPrice()));
      localHashMap.put("position", "" + i1);
      if (localObject != null) {
        localHashMap.put("dimension24", localObject);
      }
      localHashMap.put("dimension25", "" + i1);
      localHashMap.put("position", Integer.valueOf(i1));
      if (str4 != null) {
        localHashMap.put("dimension26", str4);
      }
      if (str1 != null) {
        localHashMap.put("dimension27", str1);
      }
      if (str3 != null) {
        localHashMap.put("dimension28", str3);
      }
      if (str2 != null) {
        localHashMap.put("dimension29", str2);
      }
      localHashMap.put("dimension31", str5);
      localHashMap.put("dimension38", paramCJRHomePageItem.getListId());
      return localHashMap;
    }
  }
  
  public static void b()
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "b", null);
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[0]).toPatchJoinPoint());
    }
    for (;;)
    {
      return;
      try
      {
        if ((n != null) && (n.getLooper() != null))
        {
          n.getLooper().quit();
          m = null;
          n = null;
          return;
        }
      }
      catch (Exception localException)
      {
        localException.printStackTrace();
      }
    }
  }
  
  public static void b(Context paramContext, String paramString1, boolean paramBoolean1, ArrayList<CJRHomePageLayoutV2> paramArrayList, String paramString2, boolean paramBoolean2)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "b", new Class[] { Context.class, String.class, Boolean.TYPE, ArrayList.class, String.class, Boolean.TYPE });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext, paramString1, new Boolean(paramBoolean1), paramArrayList, paramString2, new Boolean(paramBoolean2) }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.3(paramArrayList, paramString2, paramBoolean1, paramContext, paramString1, paramBoolean2));
  }
  
  private static void b(Context paramContext, Map<String, Object> paramMap)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "b", new Class[] { Context.class, Map.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext, paramMap }).toPatchJoinPoint());
    }
    do
    {
      return;
      paramContext = new i(paramContext).getString("ADVERTISING_ID", null);
    } while (paramContext == null);
    paramMap.put("Advertising_ID", paramContext);
  }
  
  public static void b(Context paramContext, CJRHomePageItem paramCJRHomePageItem, int paramInt, String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "b", new Class[] { Context.class, CJRHomePageItem.class, Integer.TYPE, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext, paramCJRHomePageItem, new Integer(paramInt), paramString }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.24(paramInt, paramString, paramCJRHomePageItem, paramContext));
  }
  
  public static void b(String paramString, Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "b", new Class[] { String.class, Context.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString, paramContext }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.16(paramContext, paramString));
  }
  
  public static void b(String paramString1, String paramString2, Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "b", new Class[] { String.class, String.class, Context.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString1, paramString2, paramContext }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.12(paramContext, paramString1, paramString2));
  }
  
  public static void b(String paramString1, String paramString2, String paramString3, String paramString4, Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "b", new Class[] { String.class, String.class, String.class, String.class, Context.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString1, paramString2, paramString3, paramString4, paramContext }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.14(paramContext, paramString1, paramString2, paramString3, paramString4));
  }
  
  public static void b(String paramString, Map<String, Object> paramMap, Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "b", new Class[] { String.class, Map.class, Context.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString, paramMap, paramContext }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.33(paramContext, paramString, paramMap));
  }
  
  public static void b(Map<String, Object> paramMap, Context paramContext)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "b", new Class[] { Map.class, Context.class });
    if (localObject != null) {
      ((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramMap, paramContext }).toPatchJoinPoint());
    }
    for (;;)
    {
      return;
      if (paramMap != null) {
        try
        {
          paramMap.put("Device_Brand", b.a());
          paramMap.put("Device_Name", b.b());
          b(paramContext, paramMap);
          localObject = d(paramContext);
          if (localObject != null) {
            paramMap.put("IMEI", Base64.encodeToString(((String)localObject).getBytes(), 0));
          }
          localObject = e.a(paramContext);
          if (localObject != null) {
            paramMap.put("Custom_Device_ID", Base64.encodeToString(((String)localObject).getBytes(), 0));
          }
          if (net.one97.paytm.common.utility.j.a(paramContext) != null)
          {
            if (!TextUtils.isEmpty(b.h(paramContext))) {
              paramMap.put("Customer_Id", b.h(paramContext));
            }
            localObject = b.f(paramContext);
            if (localObject != null) {
              paramMap.put("Contact_Number", Base64.encodeToString(((String)localObject).getBytes(), 0));
            }
            paramContext = b.g(paramContext);
            if (paramContext != null) {
              paramMap.put("user_email", Base64.encodeToString(paramContext.getBytes(), 0));
            }
          }
          paramContext = q.a().m().v();
          if (paramContext != null)
          {
            paramMap.put("Channel_ID", paramContext);
            return;
          }
        }
        catch (Exception paramMap)
        {
          paramMap.printStackTrace();
        }
      }
    }
  }
  
  public static void b(CJREventStoreFrontItemDetailModel paramCJREventStoreFrontItemDetailModel, Context paramContext, int paramInt, String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "b", new Class[] { CJREventStoreFrontItemDetailModel.class, Context.class, Integer.TYPE, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramCJREventStoreFrontItemDetailModel, paramContext, new Integer(paramInt), paramString }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.57(paramInt, paramString, paramContext, paramCJREventStoreFrontItemDetailModel));
  }
  
  private static boolean b(String paramString1, String paramString2)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "b", new Class[] { String.class, String.class });
    boolean bool1;
    if (localPatch != null) {
      bool1 = Conversions.booleanValue(localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString1, paramString2 }).toPatchJoinPoint()));
    }
    for (;;)
    {
      return bool1;
      bool1 = false;
      try
      {
        boolean bool2 = new JSONObject(paramString1).has(paramString2.toLowerCase());
        if (bool2) {
          return true;
        }
      }
      catch (JSONException paramString1)
      {
        paramString1.printStackTrace();
      }
    }
    return false;
  }
  
  private Map<String, Object> c(CJRDetailProduct paramCJRDetailProduct, int paramInt, CJRItem paramCJRItem)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "c", new Class[] { CJRDetailProduct.class, Integer.TYPE, CJRItem.class });
    if (localObject != null) {
      return (Map)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJRDetailProduct, new Integer(paramInt), paramCJRItem }).toPatchJoinPoint());
    }
    String str = paramCJRDetailProduct.getAllAncestors();
    if (TextUtils.isEmpty(paramCJRDetailProduct.getparentID())) {}
    for (localObject = paramCJRDetailProduct.getmProductID();; localObject = paramCJRDetailProduct.getparentID())
    {
      localObject = a(paramCJRItem, (String)localObject);
      ((HashMap)localObject).put("id", paramCJRDetailProduct.getmProductID());
      ((HashMap)localObject).put("name", paramCJRDetailProduct.getName());
      ((HashMap)localObject).put("category", str);
      ((HashMap)localObject).put("brand", paramCJRDetailProduct.getBrand());
      ((HashMap)localObject).put("price", paramCJRDetailProduct.getmDiscountedPrice());
      ((HashMap)localObject).put("quantity", Integer.valueOf(paramInt));
      ((HashMap)localObject).put("dimension43", paramCJRDetailProduct.getAllAncestorsId());
      ((HashMap)localObject).put("dimension41", paramCJRDetailProduct.getmMerchant().getMerchantID());
      if (paramCJRItem != null) {
        ((HashMap)localObject).put("position", "" + (paramCJRItem.getListPosition() + 1));
      }
      return (Map<String, Object>)localObject;
    }
  }
  
  private Map<String, Object> c(CJRDetailProduct paramCJRDetailProduct, CJRItem paramCJRItem)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "c", new Class[] { CJRDetailProduct.class, CJRItem.class });
    if (localObject != null) {
      return (Map)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJRDetailProduct, paramCJRItem }).toPatchJoinPoint());
    }
    String str = paramCJRDetailProduct.getAllAncestors();
    if (TextUtils.isEmpty(paramCJRDetailProduct.getparentID())) {}
    for (localObject = paramCJRDetailProduct.getmProductID();; localObject = paramCJRDetailProduct.getparentID())
    {
      localObject = a(paramCJRItem, (String)localObject);
      ((HashMap)localObject).put("id", paramCJRDetailProduct.getmProductID());
      ((HashMap)localObject).put("name", paramCJRDetailProduct.getName());
      ((HashMap)localObject).put("category", str);
      ((HashMap)localObject).put("brand", paramCJRDetailProduct.getBrand());
      ((HashMap)localObject).put("price", paramCJRDetailProduct.getmDiscountedPrice());
      ((HashMap)localObject).put("quantity", Integer.valueOf(paramCJRDetailProduct.getmQuantity()));
      ((HashMap)localObject).put("dimension43", paramCJRDetailProduct.getAllAncestorsId());
      ((HashMap)localObject).put("dimension41", paramCJRDetailProduct.getmMerchant().getMerchantID());
      if (paramCJRItem != null) {
        ((HashMap)localObject).put("position", "" + (paramCJRItem.getListPosition() + 1));
      }
      return (Map<String, Object>)localObject;
    }
  }
  
  public static void c(Context paramContext, CJRHomePageItem paramCJRHomePageItem, int paramInt, String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "c", new Class[] { Context.class, CJRHomePageItem.class, Integer.TYPE, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext, paramCJRHomePageItem, new Integer(paramInt), paramString }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.25(paramInt, paramString, paramCJRHomePageItem, paramContext));
  }
  
  public static void c(String paramString1, String paramString2, Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "c", new Class[] { String.class, String.class, Context.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString1, paramString2, paramContext }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.13(paramContext, paramString1, paramString2));
  }
  
  public static void c(String paramString1, String paramString2, String paramString3, String paramString4, Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "c", new Class[] { String.class, String.class, String.class, String.class, Context.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString1, paramString2, paramString3, paramString4, paramContext }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.32(paramContext, paramString1, paramString2, paramString3, paramString4));
  }
  
  private static String d(Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "d", new Class[] { Context.class });
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext }).toPatchJoinPoint());
    }
    return b.a(paramContext, (TelephonyManager)paramContext.getSystemService("phone"));
  }
  
  public static void d(Context paramContext, CJRHomePageItem paramCJRHomePageItem, int paramInt, String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "d", new Class[] { Context.class, CJRHomePageItem.class, Integer.TYPE, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext, paramCJRHomePageItem, new Integer(paramInt), paramString }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.50(paramInt, paramString, paramCJRHomePageItem, paramContext));
  }
  
  public static void d(String paramString1, String paramString2, Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "d", new Class[] { String.class, String.class, Context.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString1, paramString2, paramContext }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.29(paramContext, paramString1, paramString2));
  }
  
  public static void e(Context paramContext, CJRHomePageItem paramCJRHomePageItem, int paramInt, String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "e", new Class[] { Context.class, CJRHomePageItem.class, Integer.TYPE, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext, paramCJRHomePageItem, new Integer(paramInt), paramString }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.51(paramInt, paramString, paramCJRHomePageItem, paramContext));
  }
  
  public Map<String, Object> a(CJRDetailProduct paramCJRDetailProduct, CJRItem paramCJRItem)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJRDetailProduct.class, CJRItem.class });
    if (localObject != null) {
      return (Map)((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJRDetailProduct, paramCJRItem }).toPatchJoinPoint());
    }
    localObject = "";
    if (paramCJRItem != null) {
      localObject = paramCJRItem.getListName();
    }
    return DataLayer.mapOf(new Object[] { "detail", DataLayer.mapOf(new Object[] { "actionField", DataLayer.mapOf(new Object[] { "list", localObject }), "products", b(paramCJRDetailProduct, paramCJRItem) }) });
  }
  
  public void a(Context paramContext, String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramContext, paramString }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.39(this, paramContext, paramString));
  }
  
  public void a(Context paramContext, CJRCartProduct paramCJRCartProduct, boolean paramBoolean, String paramString, int paramInt, HashMap<String, Object> paramHashMap)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, CJRCartProduct.class, Boolean.TYPE, String.class, Integer.TYPE, HashMap.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramContext, paramCJRCartProduct, new Boolean(paramBoolean), paramString, new Integer(paramInt), paramHashMap }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.46(this, paramContext, paramInt, paramString, paramHashMap, paramCJRCartProduct));
  }
  
  public void a(Context paramContext, CJRGridProduct paramCJRGridProduct, boolean paramBoolean, String paramString1, int paramInt, String paramString2, String paramString3, HashMap<String, Object> paramHashMap, String paramString4)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, CJRGridProduct.class, Boolean.TYPE, String.class, Integer.TYPE, String.class, String.class, HashMap.class, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramContext, paramCJRGridProduct, new Boolean(paramBoolean), paramString1, new Integer(paramInt), paramString2, paramString3, paramHashMap, paramString4 }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.43(this, paramInt, paramContext, paramHashMap, paramBoolean, paramString1, paramCJRGridProduct, paramString2, paramString4));
  }
  
  public void a(Context paramContext, CJRHomePageItem paramCJRHomePageItem, int paramInt, String paramString1, String paramString2, HashMap<String, Object> paramHashMap)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, CJRHomePageItem.class, Integer.TYPE, String.class, String.class, HashMap.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramContext, paramCJRHomePageItem, new Integer(paramInt), paramString1, paramString2, paramHashMap }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.40(this, paramInt, paramContext, paramString1, paramCJRHomePageItem, paramString2, paramHashMap));
  }
  
  public void a(Context paramContext, CJRHomePageItem paramCJRHomePageItem, int paramInt, String paramString1, String paramString2, HashMap<String, Object> paramHashMap, String paramString3)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, CJRHomePageItem.class, Integer.TYPE, String.class, String.class, HashMap.class, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramContext, paramCJRHomePageItem, new Integer(paramInt), paramString1, paramString2, paramHashMap, paramString3 }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.42(this, paramInt, paramContext, paramCJRHomePageItem, paramString2, paramHashMap, paramString3));
  }
  
  public void a(Context paramContext, CJRHomePageItem paramCJRHomePageItem, int paramInt, String paramString1, String paramString2, HashMap<String, Object> paramHashMap, String paramString3, String paramString4)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, CJRHomePageItem.class, Integer.TYPE, String.class, String.class, HashMap.class, String.class, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramContext, paramCJRHomePageItem, new Integer(paramInt), paramString1, paramString2, paramHashMap, paramString3, paramString4 }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.41(this, paramInt, paramContext, paramString1, paramCJRHomePageItem, paramString2, paramString4, paramHashMap, paramString3));
  }
  
  public void a(Context paramContext, CJRHomePageItem paramCJRHomePageItem, String paramString, CJROrderSummaryProductDetail paramCJROrderSummaryProductDetail)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, CJRHomePageItem.class, String.class, CJROrderSummaryProductDetail.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramContext, paramCJRHomePageItem, paramString, paramCJROrderSummaryProductDetail }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.47(this, paramContext, paramString, paramCJRHomePageItem, paramCJROrderSummaryProductDetail));
  }
  
  public void a(Context paramContext, CJRHomePageItem paramCJRHomePageItem, boolean paramBoolean, String paramString1, int paramInt, String paramString2, String paramString3, HashMap<String, Object> paramHashMap)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, CJRHomePageItem.class, Boolean.TYPE, String.class, Integer.TYPE, String.class, String.class, HashMap.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramContext, paramCJRHomePageItem, new Boolean(paramBoolean), paramString1, new Integer(paramInt), paramString2, paramString3, paramHashMap }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.44(this, paramInt, paramContext, paramBoolean, paramString1, paramHashMap, paramString2, paramCJRHomePageItem));
  }
  
  public void a(Context paramContext, CJROrderSummary paramCJROrderSummary, CJRParcelTrackingInfo paramCJRParcelTrackingInfo, String paramString1, boolean paramBoolean, String paramString2, String paramString3, long paramLong)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, CJROrderSummary.class, CJRParcelTrackingInfo.class, String.class, Boolean.TYPE, String.class, String.class, Long.TYPE });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramContext, paramCJROrderSummary, paramCJRParcelTrackingInfo, paramString1, new Boolean(paramBoolean), paramString2, paramString3, new Long(paramLong) }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.38(this, paramCJROrderSummary, paramBoolean, paramString2, paramString3, paramLong, paramContext, paramCJRParcelTrackingInfo, paramString1));
  }
  
  public void a(ArrayList<CJRCartProduct> paramArrayList, Context paramContext, int paramInt, String paramString, boolean paramBoolean)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { ArrayList.class, Context.class, Integer.TYPE, String.class, Boolean.TYPE });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramArrayList, paramContext, new Integer(paramInt), paramString, new Boolean(paramBoolean) }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.48(this, paramArrayList, paramContext, paramInt, paramString, paramBoolean));
  }
  
  public void a(CJRCartProduct paramCJRCartProduct, Context paramContext, String paramString1, String paramString2)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJRCartProduct.class, Context.class, String.class, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJRCartProduct, paramContext, paramString1, paramString2 }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.37(this, paramCJRCartProduct, paramContext, paramString2));
  }
  
  public void a(CJRCartProduct paramCJRCartProduct, String paramString, Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJRCartProduct.class, String.class, Context.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJRCartProduct, paramString, paramContext }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.35(this, paramCJRCartProduct, paramContext, paramString));
  }
  
  public void a(CJRDetailProduct paramCJRDetailProduct, int paramInt, Context paramContext, String paramString, CJRItem paramCJRItem)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJRDetailProduct.class, Integer.TYPE, Context.class, String.class, CJRItem.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJRDetailProduct, new Integer(paramInt), paramContext, paramString, paramCJRItem }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.36(this, paramCJRDetailProduct, paramContext, paramString, paramInt, paramCJRItem));
  }
  
  public void a(CJRDetailProduct paramCJRDetailProduct, Context paramContext, CJRItem paramCJRItem)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJRDetailProduct.class, Context.class, CJRItem.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJRDetailProduct, paramContext, paramCJRItem }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.34(this, paramCJRDetailProduct, paramContext, paramCJRItem));
  }
  
  public void a(CJRHomePageItem paramCJRHomePageItem, Context paramContext, int paramInt, String paramString1, String paramString2)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJRHomePageItem.class, Context.class, Integer.TYPE, String.class, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJRHomePageItem, paramContext, new Integer(paramInt), paramString1, paramString2 }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.49(this, paramInt, paramString2, paramCJRHomePageItem, paramContext));
  }
  
  public void a(CJRHomePageItem paramCJRHomePageItem, Context paramContext, int paramInt, String paramString1, String paramString2, String paramString3, String paramString4)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { CJRHomePageItem.class, Context.class, Integer.TYPE, String.class, String.class, String.class, String.class });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJRHomePageItem, paramContext, new Integer(paramInt), paramString1, paramString2, paramString3, paramString4 }).toPatchJoinPoint());
    }
    while (n == null) {
      return;
    }
    n.post(new a.56(this, paramInt, paramString2, paramCJRHomePageItem, paramContext, paramString3, paramString4));
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/b/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */