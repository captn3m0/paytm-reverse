package net.one97.paytm.movies.c;

import io.hansel.pebbletracesdk.HanselCrashReporter;
import io.hansel.pebbletracesdk.annotations.HanselInclude;
import io.hansel.pebbletracesdk.codepatch.PatchJoinPoint.PatchJoinPointBuilder;
import io.hansel.pebbletracesdk.codepatch.patch.Patch;
import java.io.Serializable;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

@HanselInclude
public class a
  implements Serializable, IJRDataModel
{
  private static final long serialVersionUID = -6377573678240024862L;
  @com.google.b.a.b(a="recipient_map")
  public ArrayList<b> a = new ArrayList();
  @com.google.b.a.b(a="order_id")
  private String b;
  
  public ArrayList<b> a()
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", null);
    if (localPatch != null) {
      return (ArrayList)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.a;
  }
  
  public void a(String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { String.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
      return;
    }
    this.b = paramString;
  }
  
  public void a(ArrayList<b> paramArrayList)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { ArrayList.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramArrayList }).toPatchJoinPoint());
      return;
    }
    this.a = paramArrayList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/movies/c/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */