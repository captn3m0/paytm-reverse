package net.one97.paytm.movies.c;

import io.hansel.pebbletracesdk.HanselCrashReporter;
import io.hansel.pebbletracesdk.annotations.HanselInclude;
import io.hansel.pebbletracesdk.codepatch.PatchJoinPoint.PatchJoinPointBuilder;
import io.hansel.pebbletracesdk.codepatch.patch.Patch;
import net.one97.paytm.common.entity.IJRDataModel;

@HanselInclude
public class b
  implements IJRDataModel
{
  @com.google.b.a.b(a="name")
  private String a;
  @com.google.b.a.b(a="contact")
  private String b;
  private String c;
  
  public String a()
  {
    Patch localPatch = HanselCrashReporter.getPatch(b.class, "a", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.c;
  }
  
  public void a(String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(b.class, "a", new Class[] { String.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
      return;
    }
    this.c = paramString;
  }
  
  public String b()
  {
    Patch localPatch = HanselCrashReporter.getPatch(b.class, "b", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.a;
  }
  
  public void b(String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(b.class, "b", new Class[] { String.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
      return;
    }
    this.a = paramString;
  }
  
  public String c()
  {
    Patch localPatch = HanselCrashReporter.getPatch(b.class, "c", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.b;
  }
  
  public void c(String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(b.class, "c", new Class[] { String.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
      return;
    }
    this.b = paramString;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/movies/c/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */