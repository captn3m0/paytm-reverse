package net.one97.paytm.wallet.newdesign.nearby.datamodals;

import com.google.b.a.a;
import com.google.b.a.b;
import io.hansel.pebbletracesdk.HanselCrashReporter;
import io.hansel.pebbletracesdk.annotations.HanselInclude;
import io.hansel.pebbletracesdk.codepatch.PatchJoinPoint.PatchJoinPointBuilder;
import io.hansel.pebbletracesdk.codepatch.patch.Patch;
import java.util.ArrayList;
import java.util.List;
import net.one97.paytm.common.entity.IJRDataModel;

@HanselInclude
public class NearbyCategorySubCategoryModal
  implements IJRDataModel
{
  @a
  @b(a="orderId")
  private Object orderId;
  @a
  @b(a="requestGuid")
  private Object requestGuid;
  @a
  @b(a="response")
  private Response response;
  @a
  @b(a="status")
  private String status;
  @a
  @b(a="statusCode")
  private String statusCode;
  @a
  @b(a="statusMessage")
  private String statusMessage;
  
  public Object getOrderId()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyCategorySubCategoryModal.class, "getOrderId", null);
    if (localPatch != null) {
      return (Object)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.orderId;
  }
  
  public Object getRequestGuid()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyCategorySubCategoryModal.class, "getRequestGuid", null);
    if (localPatch != null) {
      return (Object)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.requestGuid;
  }
  
  public Response getResponse()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyCategorySubCategoryModal.class, "getResponse", null);
    if (localPatch != null) {
      return (Response)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.response;
  }
  
  public String getStatus()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyCategorySubCategoryModal.class, "getStatus", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.status;
  }
  
  public String getStatusCode()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyCategorySubCategoryModal.class, "getStatusCode", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.statusCode;
  }
  
  public String getStatusMessage()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyCategorySubCategoryModal.class, "getStatusMessage", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.statusMessage;
  }
  
  public void setOrderId(Object paramObject)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyCategorySubCategoryModal.class, "setOrderId", new Class[] { Object.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramObject }).toPatchJoinPoint());
      return;
    }
    this.orderId = paramObject;
  }
  
  public void setRequestGuid(Object paramObject)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyCategorySubCategoryModal.class, "setRequestGuid", new Class[] { Object.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramObject }).toPatchJoinPoint());
      return;
    }
    this.requestGuid = paramObject;
  }
  
  public void setResponse(Response paramResponse)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyCategorySubCategoryModal.class, "setResponse", new Class[] { Response.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramResponse }).toPatchJoinPoint());
      return;
    }
    this.response = paramResponse;
  }
  
  public void setStatus(String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyCategorySubCategoryModal.class, "setStatus", new Class[] { String.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
      return;
    }
    this.status = paramString;
  }
  
  public void setStatusCode(String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyCategorySubCategoryModal.class, "setStatusCode", new Class[] { String.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
      return;
    }
    this.statusCode = paramString;
  }
  
  public void setStatusMessage(String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyCategorySubCategoryModal.class, "setStatusMessage", new Class[] { String.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
      return;
    }
    this.statusMessage = paramString;
  }
  
  @HanselInclude
  public class CategoryDetail
  {
    @a
    @b(a="categoryName")
    private String categoryName;
    @a
    @b(a="subCategoryDetails")
    private List<NearbyCategorySubCategoryModal.SubCategoryDetail> subCategoryDetails = new ArrayList();
    
    public CategoryDetail() {}
    
    public String getCategoryName()
    {
      Patch localPatch = HanselCrashReporter.getPatch(CategoryDetail.class, "getCategoryName", null);
      if (localPatch != null) {
        return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.categoryName;
    }
    
    public List<NearbyCategorySubCategoryModal.SubCategoryDetail> getSubCategoryDetails()
    {
      Patch localPatch = HanselCrashReporter.getPatch(CategoryDetail.class, "getSubCategoryDetails", null);
      if (localPatch != null) {
        return (List)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.subCategoryDetails;
    }
    
    public void setCategoryName(String paramString)
    {
      Patch localPatch = HanselCrashReporter.getPatch(CategoryDetail.class, "setCategoryName", new Class[] { String.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
        return;
      }
      this.categoryName = paramString;
    }
    
    public void setSubCategoryDetails(List<NearbyCategorySubCategoryModal.SubCategoryDetail> paramList)
    {
      Patch localPatch = HanselCrashReporter.getPatch(CategoryDetail.class, "setSubCategoryDetails", new Class[] { List.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramList }).toPatchJoinPoint());
        return;
      }
      this.subCategoryDetails = paramList;
    }
  }
  
  @HanselInclude
  public class Response
  {
    @a
    @b(a="categoryDetails")
    private ArrayList<NearbyCategorySubCategoryModal.CategoryDetail> categoryDetails = new ArrayList();
    
    public Response() {}
    
    public ArrayList<NearbyCategorySubCategoryModal.CategoryDetail> getCategoryDetails()
    {
      Patch localPatch = HanselCrashReporter.getPatch(Response.class, "getCategoryDetails", null);
      if (localPatch != null) {
        return (ArrayList)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.categoryDetails;
    }
    
    public void setCategoryDetails(ArrayList<NearbyCategorySubCategoryModal.CategoryDetail> paramArrayList)
    {
      Patch localPatch = HanselCrashReporter.getPatch(Response.class, "setCategoryDetails", new Class[] { ArrayList.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramArrayList }).toPatchJoinPoint());
        return;
      }
      this.categoryDetails = paramArrayList;
    }
  }
  
  @HanselInclude
  public class SubCategoryDetail
  {
    @a
    @b(a="logoUrl")
    private String logoUrl;
    @a
    @b(a="subCategoryName")
    private String subCategoryName;
    
    public SubCategoryDetail() {}
    
    public String getLogoUrl()
    {
      Patch localPatch = HanselCrashReporter.getPatch(SubCategoryDetail.class, "getLogoUrl", null);
      if (localPatch != null) {
        return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.logoUrl;
    }
    
    public String getSubCategoryName()
    {
      Patch localPatch = HanselCrashReporter.getPatch(SubCategoryDetail.class, "getSubCategoryName", null);
      if (localPatch != null) {
        return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.subCategoryName;
    }
    
    public void setLogoUrl(String paramString)
    {
      Patch localPatch = HanselCrashReporter.getPatch(SubCategoryDetail.class, "setLogoUrl", new Class[] { String.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
        return;
      }
      this.logoUrl = paramString;
    }
    
    public void setSubCategoryName(String paramString)
    {
      Patch localPatch = HanselCrashReporter.getPatch(SubCategoryDetail.class, "setSubCategoryName", new Class[] { String.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
        return;
      }
      this.subCategoryName = paramString;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/wallet/newdesign/nearby/datamodals/NearbyCategorySubCategoryModal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */