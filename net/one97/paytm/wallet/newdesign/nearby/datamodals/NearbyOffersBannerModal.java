package net.one97.paytm.wallet.newdesign.nearby.datamodals;

import com.google.b.a.b;
import io.hansel.pebbletracesdk.HanselCrashReporter;
import io.hansel.pebbletracesdk.annotations.HanselInclude;
import io.hansel.pebbletracesdk.codepatch.Conversions;
import io.hansel.pebbletracesdk.codepatch.PatchJoinPoint.PatchJoinPointBuilder;
import io.hansel.pebbletracesdk.codepatch.patch.Patch;
import java.util.ArrayList;
import net.one97.paytm.common.entity.CJRItem;
import net.one97.paytm.common.entity.IJRDataModel;
import net.one97.paytm.common.entity.shopping.CJRRelatedCategory;

@HanselInclude
public class NearbyOffersBannerModal
  implements IJRDataModel
{
  @b(a="alt_image_url")
  private Object altImageUrl;
  @b(a="ancestors")
  private ArrayList<Object> ancestors = new ArrayList();
  @b(a="display_name")
  private String displayName;
  @b(a="entity_associated_with")
  private Integer entityAssociatedWith;
  @b(a="entity_type")
  private String entityType;
  @b(a="footer_image_url")
  private String footerImageUrl;
  @b(a="ga_key")
  private String gaKey;
  @b(a="homepage_layout")
  private ArrayList<HomepageLayout> homepage_layout = new ArrayList();
  @b(a="id")
  private Integer id;
  @b(a="image_url")
  private Object image_url;
  @b(a="long_rich_desc")
  private String longRichDesc;
  @b(a="meta_description")
  private String metaDescription;
  @b(a="meta_keyword")
  private String metaKeyword;
  @b(a="meta_title")
  private String metaTitle;
  @b(a="mobile_layout")
  private ArrayList<Object> mobileLayout = new ArrayList();
  @b(a="old_category_id")
  private Object oldCategoryId;
  @b(a="placeholder_image_url")
  private String placeholderImageUrl;
  @b(a="storeName")
  private String storeName;
  @b(a="url_key")
  private String urlKey;
  
  public Object getAltImageUrl()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "getAltImageUrl", null);
    if (localPatch != null) {
      return (Object)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.altImageUrl;
  }
  
  public ArrayList<Object> getAncestors()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "getAncestors", null);
    if (localPatch != null) {
      return (ArrayList)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.ancestors;
  }
  
  public String getDisplayName()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "getDisplayName", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.displayName;
  }
  
  public Integer getEntityAssociatedWith()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "getEntityAssociatedWith", null);
    if (localPatch != null) {
      return (Integer)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.entityAssociatedWith;
  }
  
  public String getEntityType()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "getEntityType", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.entityType;
  }
  
  public String getFooterImageUrl()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "getFooterImageUrl", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.footerImageUrl;
  }
  
  public String getGaKey()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "getGaKey", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.gaKey;
  }
  
  public ArrayList<HomepageLayout> getHomepageLayout()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "getHomepageLayout", null);
    if (localPatch != null) {
      return (ArrayList)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.homepage_layout;
  }
  
  public Integer getId()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "getId", null);
    if (localPatch != null) {
      return (Integer)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.id;
  }
  
  public Object getImageUrl()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "getImageUrl", null);
    if (localPatch != null) {
      return (Object)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.image_url;
  }
  
  public String getLongRichDesc()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "getLongRichDesc", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.longRichDesc;
  }
  
  public String getMetaDescription()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "getMetaDescription", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.metaDescription;
  }
  
  public String getMetaKeyword()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "getMetaKeyword", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.metaKeyword;
  }
  
  public String getMetaTitle()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "getMetaTitle", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.metaTitle;
  }
  
  public ArrayList<Object> getMobileLayout()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "getMobileLayout", null);
    if (localPatch != null) {
      return (ArrayList)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.mobileLayout;
  }
  
  public Object getOldCategoryId()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "getOldCategoryId", null);
    if (localPatch != null) {
      return (Object)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.oldCategoryId;
  }
  
  public String getPlaceholderImageUrl()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "getPlaceholderImageUrl", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.placeholderImageUrl;
  }
  
  public String getStoreName()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "getStoreName", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.storeName;
  }
  
  public String getUrlKey()
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "getUrlKey", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.urlKey;
  }
  
  public void setAltImageUrl(Object paramObject)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "setAltImageUrl", new Class[] { Object.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramObject }).toPatchJoinPoint());
      return;
    }
    this.altImageUrl = paramObject;
  }
  
  public void setAncestors(ArrayList<Object> paramArrayList)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "setAncestors", new Class[] { ArrayList.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramArrayList }).toPatchJoinPoint());
      return;
    }
    this.ancestors = paramArrayList;
  }
  
  public void setDisplayName(String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "setDisplayName", new Class[] { String.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
      return;
    }
    this.displayName = paramString;
  }
  
  public void setEntityAssociatedWith(Integer paramInteger)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "setEntityAssociatedWith", new Class[] { Integer.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramInteger }).toPatchJoinPoint());
      return;
    }
    this.entityAssociatedWith = paramInteger;
  }
  
  public void setEntityType(String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "setEntityType", new Class[] { String.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
      return;
    }
    this.entityType = paramString;
  }
  
  public void setFooterImageUrl(String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "setFooterImageUrl", new Class[] { String.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
      return;
    }
    this.footerImageUrl = paramString;
  }
  
  public void setGaKey(String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "setGaKey", new Class[] { String.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
      return;
    }
    this.gaKey = paramString;
  }
  
  public void setHomepageLayout(ArrayList<HomepageLayout> paramArrayList)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "setHomepageLayout", new Class[] { ArrayList.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramArrayList }).toPatchJoinPoint());
      return;
    }
    this.homepage_layout = paramArrayList;
  }
  
  public void setId(Integer paramInteger)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "setId", new Class[] { Integer.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramInteger }).toPatchJoinPoint());
      return;
    }
    this.id = paramInteger;
  }
  
  public void setImageUrl(Object paramObject)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "setImageUrl", new Class[] { Object.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramObject }).toPatchJoinPoint());
      return;
    }
    this.image_url = paramObject;
  }
  
  public void setLongRichDesc(String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "setLongRichDesc", new Class[] { String.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
      return;
    }
    this.longRichDesc = paramString;
  }
  
  public void setMetaDescription(String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "setMetaDescription", new Class[] { String.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
      return;
    }
    this.metaDescription = paramString;
  }
  
  public void setMetaKeyword(String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "setMetaKeyword", new Class[] { String.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
      return;
    }
    this.metaKeyword = paramString;
  }
  
  public void setMetaTitle(String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "setMetaTitle", new Class[] { String.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
      return;
    }
    this.metaTitle = paramString;
  }
  
  public void setMobileLayout(ArrayList<Object> paramArrayList)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "setMobileLayout", new Class[] { ArrayList.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramArrayList }).toPatchJoinPoint());
      return;
    }
    this.mobileLayout = paramArrayList;
  }
  
  public void setOldCategoryId(Object paramObject)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "setOldCategoryId", new Class[] { Object.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramObject }).toPatchJoinPoint());
      return;
    }
    this.oldCategoryId = paramObject;
  }
  
  public void setPlaceholderImageUrl(String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "setPlaceholderImageUrl", new Class[] { String.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
      return;
    }
    this.placeholderImageUrl = paramString;
  }
  
  public void setStoreName(String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "setStoreName", new Class[] { String.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
      return;
    }
    this.storeName = paramString;
  }
  
  public void setUrlKey(String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(NearbyOffersBannerModal.class, "setUrlKey", new Class[] { String.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
      return;
    }
    this.urlKey = paramString;
  }
  
  @HanselInclude
  public class HomepageLayout
  {
    @b(a="id")
    private Integer id;
    @b(a="items")
    private ArrayList<NearbyOffersBannerModal.Item> items = new ArrayList();
    @b(a="layout")
    private String layout;
    @b(a="name")
    private String name;
    @b(a="priority")
    private Integer priority;
    @b(a="status")
    private Integer status;
    
    public HomepageLayout() {}
    
    public Integer getId()
    {
      Patch localPatch = HanselCrashReporter.getPatch(HomepageLayout.class, "getId", null);
      if (localPatch != null) {
        return (Integer)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.id;
    }
    
    public ArrayList<NearbyOffersBannerModal.Item> getItems()
    {
      Patch localPatch = HanselCrashReporter.getPatch(HomepageLayout.class, "getItems", null);
      if (localPatch != null) {
        return (ArrayList)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.items;
    }
    
    public String getLayout()
    {
      Patch localPatch = HanselCrashReporter.getPatch(HomepageLayout.class, "getLayout", null);
      if (localPatch != null) {
        return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.layout;
    }
    
    public String getName()
    {
      Patch localPatch = HanselCrashReporter.getPatch(HomepageLayout.class, "getName", null);
      if (localPatch != null) {
        return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.name;
    }
    
    public Integer getPriority()
    {
      Patch localPatch = HanselCrashReporter.getPatch(HomepageLayout.class, "getPriority", null);
      if (localPatch != null) {
        return (Integer)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.priority;
    }
    
    public Integer getStatus()
    {
      Patch localPatch = HanselCrashReporter.getPatch(HomepageLayout.class, "getStatus", null);
      if (localPatch != null) {
        return (Integer)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.status;
    }
    
    public void setId(Integer paramInteger)
    {
      Patch localPatch = HanselCrashReporter.getPatch(HomepageLayout.class, "setId", new Class[] { Integer.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramInteger }).toPatchJoinPoint());
        return;
      }
      this.id = paramInteger;
    }
    
    public void setItems(ArrayList<NearbyOffersBannerModal.Item> paramArrayList)
    {
      Patch localPatch = HanselCrashReporter.getPatch(HomepageLayout.class, "setItems", new Class[] { ArrayList.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramArrayList }).toPatchJoinPoint());
        return;
      }
      this.items = paramArrayList;
    }
    
    public void setLayout(String paramString)
    {
      Patch localPatch = HanselCrashReporter.getPatch(HomepageLayout.class, "setLayout", new Class[] { String.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
        return;
      }
      this.layout = paramString;
    }
    
    public void setName(String paramString)
    {
      Patch localPatch = HanselCrashReporter.getPatch(HomepageLayout.class, "setName", new Class[] { String.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
        return;
      }
      this.name = paramString;
    }
    
    public void setPriority(Integer paramInteger)
    {
      Patch localPatch = HanselCrashReporter.getPatch(HomepageLayout.class, "setPriority", new Class[] { Integer.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramInteger }).toPatchJoinPoint());
        return;
      }
      this.priority = paramInteger;
    }
    
    public void setStatus(Integer paramInteger)
    {
      Patch localPatch = HanselCrashReporter.getPatch(HomepageLayout.class, "setStatus", new Class[] { Integer.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramInteger }).toPatchJoinPoint());
        return;
      }
      this.status = paramInteger;
    }
  }
  
  @HanselInclude
  public class Item
    extends CJRItem
  {
    @b(a="alt_image_url")
    private Object altImageUrl;
    @b(a="id")
    private Integer id;
    @b(a="image_url")
    private String image_url;
    @b(a="name")
    private String name;
    @b(a="priority")
    private Integer priority;
    @b(a="seourl")
    private String seourl;
    @b(a="status")
    private Integer status;
    @b(a="subtitle")
    private String subtitle;
    @b(a="title")
    private String title;
    @b(a="url")
    private String url;
    @b(a="url_info")
    private String urlInfo;
    @b(a="url_type")
    private String url_type;
    
    public Item() {}
    
    public Object getAltImageUrl()
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getAltImageUrl", null);
      if (localPatch != null) {
        return (Object)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.altImageUrl;
    }
    
    public String getBrand()
    {
      String str = null;
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getBrand", null);
      if (localPatch != null) {
        str = (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return str;
    }
    
    public String getCategoryId()
    {
      String str = null;
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getCategoryId", null);
      if (localPatch != null) {
        str = (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return str;
    }
    
    public Integer getId()
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getId", null);
      if (localPatch != null) {
        return (Integer)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.id;
    }
    
    public String getImageUrl()
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getImageUrl", null);
      if (localPatch != null) {
        return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.image_url;
    }
    
    public String getItemID()
    {
      String str = null;
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getItemID", null);
      if (localPatch != null) {
        str = (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return str;
    }
    
    public String getLabel()
    {
      String str = null;
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getLabel", null);
      if (localPatch != null) {
        str = (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return str;
    }
    
    public String getListId()
    {
      String str = null;
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getListId", null);
      if (localPatch != null) {
        str = (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return str;
    }
    
    public String getListName()
    {
      String str = null;
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getListName", null);
      if (localPatch != null) {
        str = (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return str;
    }
    
    public int getListPosition()
    {
      int i = 0;
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getListPosition", null);
      if (localPatch != null) {
        i = Conversions.intValue(localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint()));
      }
      return i;
    }
    
    public String getName()
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getName", null);
      if (localPatch != null) {
        return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.name;
    }
    
    public String getParentID()
    {
      String str = null;
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getParentID", null);
      if (localPatch != null) {
        str = (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return str;
    }
    
    public Integer getPriority()
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getPriority", null);
      if (localPatch != null) {
        return (Integer)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.priority;
    }
    
    public ArrayList<CJRRelatedCategory> getRelatedCategories()
    {
      ArrayList localArrayList = null;
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getRelatedCategories", null);
      if (localPatch != null) {
        localArrayList = (ArrayList)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return localArrayList;
    }
    
    public String getSearchABValue()
    {
      String str = null;
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getSearchABValue", null);
      if (localPatch != null) {
        str = (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return str;
    }
    
    public String getSearchCategory()
    {
      String str = null;
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getSearchCategory", null);
      if (localPatch != null) {
        str = (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return str;
    }
    
    public String getSearchResultType()
    {
      String str = null;
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getSearchResultType", null);
      if (localPatch != null) {
        str = (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return str;
    }
    
    public String getSearchTerm()
    {
      String str = null;
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getSearchTerm", null);
      if (localPatch != null) {
        str = (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return str;
    }
    
    public String getSearchType()
    {
      String str = null;
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getSearchType", null);
      if (localPatch != null) {
        str = (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return str;
    }
    
    public String getSeourl()
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getSeourl", null);
      if (localPatch != null) {
        return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.seourl;
    }
    
    public Integer getStatus()
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getStatus", null);
      if (localPatch != null) {
        return (Integer)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.status;
    }
    
    public String getSubtitle()
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getSubtitle", null);
      if (localPatch != null) {
        return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.subtitle;
    }
    
    public String getTitle()
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getTitle", null);
      if (localPatch != null) {
        return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.title;
    }
    
    public String getURL()
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getURL", null);
      if (localPatch != null) {
        return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return getUrl();
    }
    
    public String getURLType()
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getURLType", null);
      if (localPatch != null) {
        return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return getUrlType();
    }
    
    public String getUrl()
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getUrl", null);
      if (localPatch != null) {
        return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.url;
    }
    
    public String getUrlInfo()
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getUrlInfo", null);
      if (localPatch != null) {
        return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.urlInfo;
    }
    
    public String getUrlType()
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getUrlType", null);
      if (localPatch != null) {
        return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.url_type;
    }
    
    public String getmContainerInstanceID()
    {
      String str = null;
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "getmContainerInstanceID", null);
      if (localPatch != null) {
        str = (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return str;
    }
    
    public void setAltImageUrl(Object paramObject)
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "setAltImageUrl", new Class[] { Object.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramObject }).toPatchJoinPoint());
        return;
      }
      this.altImageUrl = paramObject;
    }
    
    public void setId(Integer paramInteger)
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "setId", new Class[] { Integer.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramInteger }).toPatchJoinPoint());
        return;
      }
      this.id = paramInteger;
    }
    
    public void setImageUrl(String paramString)
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "setImageUrl", new Class[] { String.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
        return;
      }
      this.image_url = paramString;
    }
    
    public void setName(String paramString)
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "setName", new Class[] { String.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
        return;
      }
      this.name = paramString;
    }
    
    public void setPriority(Integer paramInteger)
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "setPriority", new Class[] { Integer.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramInteger }).toPatchJoinPoint());
        return;
      }
      this.priority = paramInteger;
    }
    
    public void setSeourl(String paramString)
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "setSeourl", new Class[] { String.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
        return;
      }
      this.seourl = paramString;
    }
    
    public void setStatus(Integer paramInteger)
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "setStatus", new Class[] { Integer.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramInteger }).toPatchJoinPoint());
        return;
      }
      this.status = paramInteger;
    }
    
    public void setSubtitle(String paramString)
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "setSubtitle", new Class[] { String.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
        return;
      }
      this.subtitle = paramString;
    }
    
    public void setTitle(String paramString)
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "setTitle", new Class[] { String.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
        return;
      }
      this.title = paramString;
    }
    
    public void setUrl(String paramString)
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "setUrl", new Class[] { String.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
        return;
      }
      this.url = paramString;
    }
    
    public void setUrlInfo(String paramString)
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "setUrlInfo", new Class[] { String.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
        return;
      }
      this.urlInfo = paramString;
    }
    
    public void setUrlType(String paramString)
    {
      Patch localPatch = HanselCrashReporter.getPatch(Item.class, "setUrlType", new Class[] { String.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
        return;
      }
      this.url_type = paramString;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/wallet/newdesign/nearby/datamodals/NearbyOffersBannerModal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */