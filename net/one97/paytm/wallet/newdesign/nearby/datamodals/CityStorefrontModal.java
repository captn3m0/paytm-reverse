package net.one97.paytm.wallet.newdesign.nearby.datamodals;

import com.google.c.a.b;
import io.hansel.pebbletracesdk.HanselCrashReporter;
import io.hansel.pebbletracesdk.annotations.HanselInclude;
import io.hansel.pebbletracesdk.codepatch.PatchJoinPoint.PatchJoinPointBuilder;
import io.hansel.pebbletracesdk.codepatch.patch.Patch;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

@HanselInclude
public class CityStorefrontModal
  implements IJRDataModel
{
  @b(a="default")
  private String defaultUrl;
  @b(a="response")
  private ArrayList<Response> response;
  
  public String getDefaultUrl()
  {
    Patch localPatch = HanselCrashReporter.getPatch(CityStorefrontModal.class, "getDefaultUrl", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.defaultUrl;
  }
  
  public ArrayList<Response> getResponse()
  {
    Patch localPatch = HanselCrashReporter.getPatch(CityStorefrontModal.class, "getResponse", null);
    if (localPatch != null) {
      return (ArrayList)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.response;
  }
  
  public void setDefaultUrl(String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(CityStorefrontModal.class, "setDefaultUrl", new Class[] { String.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
      return;
    }
    this.defaultUrl = paramString;
  }
  
  public void setResponse(ArrayList<Response> paramArrayList)
  {
    Patch localPatch = HanselCrashReporter.getPatch(CityStorefrontModal.class, "setResponse", new Class[] { ArrayList.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramArrayList }).toPatchJoinPoint());
      return;
    }
    this.response = paramArrayList;
  }
  
  @HanselInclude
  public class Response
  {
    @b(a="city")
    private String city;
    @b(a="url")
    private String url;
    
    public Response() {}
    
    public String getCity()
    {
      Patch localPatch = HanselCrashReporter.getPatch(Response.class, "getCity", null);
      if (localPatch != null) {
        return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.city;
    }
    
    public String getUrl()
    {
      Patch localPatch = HanselCrashReporter.getPatch(Response.class, "getUrl", null);
      if (localPatch != null) {
        return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      }
      return this.url;
    }
    
    public void setCity(String paramString)
    {
      Patch localPatch = HanselCrashReporter.getPatch(Response.class, "setCity", new Class[] { String.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
        return;
      }
      this.city = paramString;
    }
    
    public void setUrl(String paramString)
    {
      Patch localPatch = HanselCrashReporter.getPatch(Response.class, "setUrl", new Class[] { String.class });
      if (localPatch != null)
      {
        localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramString }).toPatchJoinPoint());
        return;
      }
      this.url = paramString;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/wallet/newdesign/nearby/datamodals/CityStorefrontModal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */