package net.one97.paytm.wallet.entity;

import com.google.c.a.b;
import io.hansel.pebbletracesdk.HanselCrashReporter;
import io.hansel.pebbletracesdk.annotations.HanselInclude;
import io.hansel.pebbletracesdk.codepatch.PatchJoinPoint.PatchJoinPointBuilder;
import io.hansel.pebbletracesdk.codepatch.patch.Patch;
import net.one97.paytm.common.entity.IJRDataModel;

@HanselInclude
public class CJRRolesList
  implements IJRDataModel
{
  @b(a="role")
  public String mRole;
  
  public String getRole()
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRRolesList.class, "getRole", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.mRole;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/wallet/entity/CJRRolesList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */