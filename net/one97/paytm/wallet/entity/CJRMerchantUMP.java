package net.one97.paytm.wallet.entity;

import com.google.c.a.b;
import io.hansel.pebbletracesdk.HanselCrashReporter;
import io.hansel.pebbletracesdk.annotations.HanselInclude;
import io.hansel.pebbletracesdk.codepatch.PatchJoinPoint.PatchJoinPointBuilder;
import io.hansel.pebbletracesdk.codepatch.patch.Patch;
import java.util.ArrayList;
import net.one97.paytm.common.entity.IJRDataModel;

@HanselInclude
public class CJRMerchantUMP
  implements IJRDataModel
{
  @b(a="merchants")
  public ArrayList<CJRMerchantEntityID> mMerchantEntityIDList;
  
  public ArrayList<CJRMerchantEntityID> getMerchantEntityIDList()
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRMerchantUMP.class, "getMerchantEntityIDList", null);
    if (localPatch != null) {
      return (ArrayList)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.mMerchantEntityIDList;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/wallet/entity/CJRMerchantUMP.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */