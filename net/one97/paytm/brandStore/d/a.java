package net.one97.paytm.brandStore.d;

import com.google.c.a.b;
import io.hansel.pebbletracesdk.HanselCrashReporter;
import io.hansel.pebbletracesdk.annotations.HanselInclude;
import io.hansel.pebbletracesdk.codepatch.PatchJoinPoint.PatchJoinPointBuilder;
import io.hansel.pebbletracesdk.codepatch.patch.Patch;
import net.one97.paytm.common.entity.IJRDataModel;

@HanselInclude
public class a
  implements IJRDataModel
{
  @b(a="site_id")
  private String a;
  @b(a="child_site_id")
  private String b;
  
  public String a()
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.a;
  }
  
  public String b()
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "b", null);
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.b;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/brandStore/d/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */