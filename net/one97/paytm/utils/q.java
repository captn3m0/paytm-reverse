package net.one97.paytm.utils;

import com.android.volley.toolbox.ImageLoader;
import io.hansel.pebbletracesdk.HanselCrashReporter;
import io.hansel.pebbletracesdk.annotations.HanselInclude;
import io.hansel.pebbletracesdk.codepatch.Conversions;
import io.hansel.pebbletracesdk.codepatch.PatchJoinPoint.PatchJoinPointBuilder;
import io.hansel.pebbletracesdk.codepatch.patch.Patch;
import net.one97.paytm.app.a;

@HanselInclude
public enum q
{
  private b b;
  private ImageLoader c;
  
  private q() {}
  
  private int d()
  {
    Patch localPatch = HanselCrashReporter.getPatch(q.class, "d", null);
    if (localPatch != null) {
      return Conversions.intValue(localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint()));
    }
    return (int)(Runtime.getRuntime().maxMemory() / 1024L) / 8;
  }
  
  public void a()
  {
    Patch localPatch = HanselCrashReporter.getPatch(q.class, "a", null);
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      return;
    }
    this.b = new b(d());
    this.c = new ImageLoader(a.a(), this.b);
  }
  
  public ImageLoader b()
  {
    Patch localPatch = HanselCrashReporter.getPatch(q.class, "b", null);
    if (localPatch != null) {
      return (ImageLoader)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.c;
  }
  
  public void c()
  {
    Patch localPatch = HanselCrashReporter.getPatch(q.class, "c", null);
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    while (this.b == null) {
      return;
    }
    this.b.a();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/utils/q.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */