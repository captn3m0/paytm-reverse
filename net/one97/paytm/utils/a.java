package net.one97.paytm.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import com.urbanairship.q;
import com.urbanairship.richpush.b;
import io.hansel.pebbletracesdk.HanselCrashReporter;
import io.hansel.pebbletracesdk.annotations.HanselInclude;
import io.hansel.pebbletracesdk.codepatch.Conversions;
import io.hansel.pebbletracesdk.codepatch.PatchJoinPoint.PatchJoinPointBuilder;
import io.hansel.pebbletracesdk.codepatch.patch.Patch;
import java.util.Iterator;
import java.util.List;

@HanselInclude
public class a
{
  public static int a(Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class });
    int i;
    if (localPatch != null) {
      i = Conversions.intValue(localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext }).toPatchJoinPoint()));
    }
    do
    {
      int j;
      do
      {
        do
        {
          return i;
          j = 0;
          i = j;
        } while (!d.v(paramContext));
        paramContext = q.a().n().g();
        i = j;
      } while (paramContext == null);
      i = j;
    } while (paramContext.size() <= 0);
    return paramContext.size();
  }
  
  public static void a(Context paramContext, boolean paramBoolean)
  {
    Object localObject = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class, Boolean.TYPE });
    if (localObject != null)
    {
      ((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext, new Boolean(paramBoolean) }).toPatchJoinPoint());
      return;
    }
    try
    {
      localObject = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
      if (paramBoolean) {
        ((Intent)localObject).putExtra("badge_count", 0);
      }
      for (;;)
      {
        ((Intent)localObject).putExtra("badge_count_package_name", paramContext.getPackageName());
        ((Intent)localObject).putExtra("badge_count_class_name", b(paramContext));
        paramContext.sendBroadcast((Intent)localObject);
        return;
        ((Intent)localObject).putExtra("badge_count", a(paramContext));
      }
      return;
    }
    catch (Exception paramContext) {}
  }
  
  public static String b(Context paramContext)
  {
    Object localObject1 = HanselCrashReporter.getPatch(a.class, "b", new Class[] { Context.class });
    if (localObject1 != null) {
      return (String)((Patch)localObject1).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject1).getClassForPatch()).setMethod(((Patch)localObject1).getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext }).toPatchJoinPoint());
    }
    localObject1 = paramContext.getPackageManager();
    Object localObject2 = new Intent("android.intent.action.MAIN");
    ((Intent)localObject2).addCategory("android.intent.category.LAUNCHER");
    localObject1 = ((PackageManager)localObject1).queryIntentActivities((Intent)localObject2, 0).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (ResolveInfo)((Iterator)localObject1).next();
      if (((ResolveInfo)localObject2).activityInfo.applicationInfo.packageName.equalsIgnoreCase(paramContext.getPackageName())) {
        return ((ResolveInfo)localObject2).activityInfo.name;
      }
    }
    return null;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/utils/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */