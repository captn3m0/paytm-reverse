package net.one97.paytm.app;

import android.content.Context;
import com.android.volley.Cache;
import com.android.volley.Cache.Entry;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.google.c.e;
import io.hansel.pebbletracesdk.HanselCrashReporter;
import io.hansel.pebbletracesdk.annotations.HanselInclude;
import io.hansel.pebbletracesdk.codepatch.PatchJoinPoint.PatchJoinPointBuilder;
import io.hansel.pebbletracesdk.codepatch.patch.Patch;
import java.io.File;
import java.util.Map;
import net.one97.paytm.common.entity.IJRDataModel;
import net.one97.paytm.common.utility.n;
import net.one97.paytm.landingpage.h.d;
import net.one97.paytm.utils.q;

@HanselInclude
public class a
{
  private static RequestQueue a;
  private static RequestQueue b;
  private static String c = a.class.getSimpleName();
  private static Context d;
  private static e e;
  
  public static RequestQueue a()
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", null);
    if (localPatch != null) {
      return (RequestQueue)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[0]).toPatchJoinPoint());
    }
    if (b != null) {
      return b;
    }
    throw new IllegalStateException("Image RequestQueue not initialized");
  }
  
  private static String a(String paramString)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { String.class });
    if (localPatch != null) {
      return (String)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString }).toPatchJoinPoint());
    }
    return paramString.replaceAll("[&?]lat.*?(?=&|\\?|$)", "").replaceAll("[&?]long.*?(?=&|\\?|$)", "");
  }
  
  public static IJRDataModel a(String paramString, Context paramContext, IJRDataModel paramIJRDataModel)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { String.class, Context.class, IJRDataModel.class });
    if (localPatch != null) {
      return (IJRDataModel)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramString, paramContext, paramIJRDataModel }).toPatchJoinPoint());
    }
    localPatch = null;
    try
    {
      paramContext = b(paramContext).getCache().get(a(paramString));
      paramString = localPatch;
      if (paramContext != null)
      {
        if (e == null) {
          e = new e();
        }
        String str = (String)paramContext.responseHeaders.get("Content-Encoding");
        paramString = localPatch;
        if (paramContext.data != null) {
          if ((str != null) && (str.equals("gzip")))
          {
            paramString = n.a(paramContext.data);
            paramString = (IJRDataModel)e.a(paramString, paramIJRDataModel.getClass());
          }
          else
          {
            paramString = new String(paramContext.data, "UTF-8");
            paramString = (IJRDataModel)e.a(paramString, paramIJRDataModel.getClass());
          }
        }
      }
    }
    catch (Exception paramString)
    {
      paramString.printStackTrace();
      paramString = localPatch;
    }
    return paramString;
  }
  
  public static void a(Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Context.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext }).toPatchJoinPoint());
      return;
    }
    e = new e();
    d = paramContext;
    b(paramContext);
    b = Volley.newRequestQueue(paramContext, true);
    q.a.a();
  }
  
  public static <T> void a(Request<T> paramRequest)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "a", new Class[] { Request.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramRequest }).toPatchJoinPoint());
      return;
    }
    paramRequest.setTag(c);
    paramRequest.setShouldCache(true);
    b(d).add(paramRequest);
  }
  
  public static <T> Request<T> b(Request<T> paramRequest)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "b", new Class[] { Request.class });
    if (localPatch != null) {
      return (Request)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramRequest }).toPatchJoinPoint());
    }
    return b(d).add(paramRequest);
  }
  
  public static RequestQueue b(Context paramContext)
  {
    Patch localPatch = HanselCrashReporter.getPatch(a.class, "b", new Class[] { Context.class });
    if (localPatch != null) {
      return (RequestQueue)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(a.class).setArguments(new Object[] { paramContext }).toPatchJoinPoint());
    }
    if (a == null)
    {
      paramContext = new File(paramContext.getCacheDir().getPath() + File.separator + "paytm-volley-cache");
      a = new RequestQueue(new DiskBasedCache(paramContext, (int)d.a(paramContext, 10485760L)), new BasicNetwork(new HurlStack()));
      a.start();
    }
    return a;
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/app/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */