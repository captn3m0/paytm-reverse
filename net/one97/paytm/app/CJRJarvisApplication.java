package net.one97.paytm.app;

import android.content.Context;
import android.content.res.Resources;
import android.support.multidex.MultiDexApplication;
import com.a.a.f;
import com.google.android.gms.tagmanager.Container;
import com.google.android.gms.tagmanager.ContainerHolder;
import com.squareup.a.t;
import com.squareup.a.t.a;
import com.urbanairship.push.a.e;
import com.urbanairship.push.j;
import com.urbanairship.q.a;
import io.a.a.a.c;
import io.a.a.a.h;
import io.hansel.pebbletracesdk.HanselCrashReporter;
import io.hansel.pebbletracesdk.annotations.HanselInclude;
import io.hansel.pebbletracesdk.codepatch.Conversions;
import io.hansel.pebbletracesdk.codepatch.PatchJoinPoint.PatchJoinPointBuilder;
import io.hansel.pebbletracesdk.codepatch.patch.Patch;
import java.util.HashMap;
import net.one97.paytm.bf;
import net.one97.paytm.common.entity.CJRCatalogSavedState;
import net.one97.paytm.common.entity.recharge.CJRFrequentOrderList;
import net.one97.paytm.common.entity.shopping.CJRCatalog;
import net.one97.paytm.common.entity.shopping.CJRShoppingCart;
import net.one97.paytm.common.utility.o;
import net.one97.paytm.wallet.newdesign.d.b;

@HanselInclude
public class CJRJarvisApplication
  extends MultiDexApplication
{
  public static ContainerHolder a;
  public static Container b;
  public static Container c;
  public static HashMap<String, String> d = new HashMap();
  public static HashMap<String, Integer> e = new HashMap();
  public static HashMap<String, Boolean> f = new HashMap();
  public static ContainerHolder g;
  public static HashMap<String, String> h = new HashMap();
  public static HashMap<String, Integer> i = new HashMap();
  public static HashMap<String, Boolean> j = new HashMap();
  private static Context k;
  private static CJRCatalog l;
  private static CJRShoppingCart m;
  private static Boolean p = Boolean.valueOf(true);
  private static Boolean q = Boolean.valueOf(true);
  private CJRFrequentOrderList n;
  private CJRCatalogSavedState o;
  
  public static Context a()
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "a", null);
    if (localPatch != null) {
      return (Context)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(CJRJarvisApplication.class).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return k;
  }
  
  public static void a(Boolean paramBoolean)
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "a", new Class[] { Boolean.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(CJRJarvisApplication.class).setArguments(new Object[] { paramBoolean }).toPatchJoinPoint());
      return;
    }
    p = paramBoolean;
  }
  
  public static void a(CJRShoppingCart paramCJRShoppingCart)
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "a", new Class[] { CJRShoppingCart.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(CJRJarvisApplication.class).setArguments(new Object[] { paramCJRShoppingCart }).toPatchJoinPoint());
      return;
    }
    m = paramCJRShoppingCart;
  }
  
  public static CJRShoppingCart b()
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "b", null);
    if (localPatch != null) {
      return (CJRShoppingCart)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(CJRJarvisApplication.class).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return m;
  }
  
  public static void b(Boolean paramBoolean)
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "b", new Class[] { Boolean.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(CJRJarvisApplication.class).setArguments(new Object[] { paramBoolean }).toPatchJoinPoint());
      return;
    }
    q = paramBoolean;
  }
  
  public static boolean c()
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "c", null);
    if (localPatch != null) {
      return Conversions.booleanValue(localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(CJRJarvisApplication.class).setArguments(new Object[0]).toPatchJoinPoint()));
    }
    return p.booleanValue();
  }
  
  public static boolean d()
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "d", null);
    if (localPatch != null) {
      return Conversions.booleanValue(localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(CJRJarvisApplication.class).setArguments(new Object[0]).toPatchJoinPoint()));
    }
    return q.booleanValue();
  }
  
  public static void e()
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "e", null);
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(CJRJarvisApplication.class).setArguments(new Object[0]).toPatchJoinPoint());
      return;
    }
    m = null;
  }
  
  private void i()
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "i", null);
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      return;
    }
    a.a(this);
    o();
    n();
    m();
    l();
    net.one97.paytm.b.a.a();
    k();
    j();
    com.evernote.android.job.d.a(this).a(new b());
  }
  
  private void j()
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "j", null);
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      return;
    }
    HanselCrashReporter.getInstance().initializeSDK(this, getResources().getString(2131231938), getResources().getString(2131231939));
  }
  
  private void k()
  {
    Object localObject = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "k", null);
    if (localObject != null)
    {
      ((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      return;
    }
    localObject = new t.a(k);
    ((t.a)localObject).a(new com.squareup.a.s(k, 2147483647L));
    t.a(((t.a)localObject).a());
  }
  
  private void l()
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "l", null);
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      return;
    }
    if (net.one97.paytm.utils.d.v(getApplicationContext()))
    {
      net.one97.paytm.utils.a.a(getApplicationContext(), false);
      return;
    }
    net.one97.paytm.utils.a.a(getApplicationContext(), true);
  }
  
  private void m()
  {
    Object localObject = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "m", null);
    if (localObject != null)
    {
      ((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      return;
    }
    localObject = o.a(getApplicationContext());
    o.b(getApplicationContext(), (String)localObject);
  }
  
  private void n()
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "n", null);
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      return;
    }
    try
    {
      c.a(this, new h[] { new f() });
      return;
    }
    catch (Exception localException) {}
  }
  
  private void o()
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "o", null);
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      return;
    }
    net.one97.paytm.utils.d.a("don", "initUrbanAirship");
    com.urbanairship.q.a(this, new q.a()
    {
      public void a(com.urbanairship.q paramAnonymousq)
      {
        Object localObject = HanselCrashReporter.getPatch(1.class, "a", new Class[] { com.urbanairship.q.class });
        if (localObject != null)
        {
          ((Patch)localObject).apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(((Patch)localObject).getClassForPatch()).setMethod(((Patch)localObject).getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramAnonymousq }).toPatchJoinPoint());
          return;
        }
        localObject = new net.one97.paytm.utils.s(CJRJarvisApplication.this.getApplicationContext());
        com.urbanairship.q.a().m().a((e)localObject);
        paramAnonymousq.m().a(true);
        com.urbanairship.q.a().m().b(false);
        net.one97.paytm.utils.d.a("don", "take off comeplted and enabled push service");
      }
    });
    bf.a();
  }
  
  public void a(CJRCatalogSavedState paramCJRCatalogSavedState)
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "a", new Class[] { CJRCatalogSavedState.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJRCatalogSavedState }).toPatchJoinPoint());
      return;
    }
    this.o = paramCJRCatalogSavedState;
  }
  
  public void a(CJRFrequentOrderList paramCJRFrequentOrderList)
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "a", new Class[] { CJRFrequentOrderList.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJRFrequentOrderList }).toPatchJoinPoint());
      return;
    }
    this.n = paramCJRFrequentOrderList;
  }
  
  public void a(CJRCatalog paramCJRCatalog)
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "a", new Class[] { CJRCatalog.class });
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { paramCJRCatalog }).toPatchJoinPoint());
      return;
    }
    l = paramCJRCatalog;
  }
  
  public CJRCatalogSavedState f()
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "f", null);
    if (localPatch != null) {
      return (CJRCatalogSavedState)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.o;
  }
  
  public CJRCatalog g()
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "g", null);
    if (localPatch != null) {
      return (CJRCatalog)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return l;
  }
  
  public CJRFrequentOrderList h()
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "h", null);
    if (localPatch != null) {
      return (CJRFrequentOrderList)localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
    }
    return this.n;
  }
  
  public void onCreate()
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "onCreate", null);
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      return;
    }
    super.onCreate();
    k = getApplicationContext();
    i();
  }
  
  public void onLowMemory()
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "onLowMemory", null);
    if (localPatch != null)
    {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[0]).toPatchJoinPoint());
      return;
    }
    super.onLowMemory();
    net.one97.paytm.utils.q.a.c();
  }
  
  public void onTrimMemory(int paramInt)
  {
    Patch localPatch = HanselCrashReporter.getPatch(CJRJarvisApplication.class, "onTrimMemory", new Class[] { Integer.TYPE });
    if (localPatch != null) {
      localPatch.apply(new PatchJoinPoint.PatchJoinPointBuilder().setClassOfMethod(localPatch.getClassForPatch()).setMethod(localPatch.getMethodForPatch()).setTarget(this).setArguments(new Object[] { new Integer(paramInt) }).toPatchJoinPoint());
    }
    do
    {
      return;
      super.onTrimMemory(paramInt);
    } while (paramInt < 15);
    net.one97.paytm.utils.q.a.c();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/net/one97/paytm/app/CJRJarvisApplication.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */