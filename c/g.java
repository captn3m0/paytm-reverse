package c;

import java.io.IOException;
import java.util.zip.Deflater;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;

public final class g
  implements s
{
  private final d a;
  private final Deflater b;
  private boolean c;
  
  g(d paramd, Deflater paramDeflater)
  {
    if (paramd == null) {
      throw new IllegalArgumentException("source == null");
    }
    if (paramDeflater == null) {
      throw new IllegalArgumentException("inflater == null");
    }
    this.a = paramd;
    this.b = paramDeflater;
  }
  
  public g(s params, Deflater paramDeflater)
  {
    this(m.a(params), paramDeflater);
  }
  
  @IgnoreJRERequirement
  private void a(boolean paramBoolean)
    throws IOException
  {
    c localc = this.a.b();
    p localp;
    label119:
    do
    {
      localp = localc.e(1);
      if (paramBoolean) {}
      for (int i = this.b.deflate(localp.a, localp.c, 2048 - localp.c, 2);; i = this.b.deflate(localp.a, localp.c, 2048 - localp.c))
      {
        if (i <= 0) {
          break label119;
        }
        localp.c += i;
        localc.b += i;
        this.a.w();
        break;
      }
    } while (!this.b.needsInput());
    if (localp.b == localp.c)
    {
      localc.a = localp.a();
      q.a(localp);
    }
  }
  
  void a()
    throws IOException
  {
    this.b.finish();
    a(false);
  }
  
  public void close()
    throws IOException
  {
    if (this.c) {}
    for (;;)
    {
      return;
      Object localObject2 = null;
      try
      {
        a();
        try
        {
          this.b.end();
          localObject1 = localObject2;
        }
        catch (Throwable localThrowable2)
        {
          for (;;)
          {
            label34:
            localObject1 = localThrowable1;
            if (localThrowable1 == null) {
              localObject1 = localThrowable2;
            }
          }
        }
        try
        {
          this.a.close();
          localObject2 = localObject1;
        }
        catch (Throwable localThrowable3)
        {
          Object localObject3 = localObject1;
          if (localObject1 != null) {
            break label34;
          }
          localObject3 = localThrowable3;
          break label34;
        }
        this.c = true;
        if (localObject2 == null) {
          continue;
        }
        v.a((Throwable)localObject2);
        return;
      }
      catch (Throwable localThrowable1)
      {
        for (;;) {}
      }
    }
  }
  
  public void flush()
    throws IOException
  {
    a(true);
    this.a.flush();
  }
  
  public u timeout()
  {
    return this.a.timeout();
  }
  
  public String toString()
  {
    return "DeflaterSink(" + this.a + ")";
  }
  
  public void write(c paramc, long paramLong)
    throws IOException
  {
    v.a(paramc.b, 0L, paramLong);
    while (paramLong > 0L)
    {
      p localp = paramc.a;
      int i = (int)Math.min(paramLong, localp.c - localp.b);
      this.b.setInput(localp.a, localp.b, i);
      a(false);
      paramc.b -= i;
      localp.b += i;
      if (localp.b == localp.c)
      {
        paramc.a = localp.a();
        q.a(localp);
      }
      paramLong -= i;
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/c/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */