package c;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class j
  extends u
{
  private u a;
  
  public j(u paramu)
  {
    if (paramu == null) {
      throw new IllegalArgumentException("delegate == null");
    }
    this.a = paramu;
  }
  
  public final j a(u paramu)
  {
    if (paramu == null) {
      throw new IllegalArgumentException("delegate == null");
    }
    this.a = paramu;
    return this;
  }
  
  public final u a()
  {
    return this.a;
  }
  
  public u clearDeadline()
  {
    return this.a.clearDeadline();
  }
  
  public u clearTimeout()
  {
    return this.a.clearTimeout();
  }
  
  public long deadlineNanoTime()
  {
    return this.a.deadlineNanoTime();
  }
  
  public u deadlineNanoTime(long paramLong)
  {
    return this.a.deadlineNanoTime(paramLong);
  }
  
  public boolean hasDeadline()
  {
    return this.a.hasDeadline();
  }
  
  public void throwIfReached()
    throws IOException
  {
    this.a.throwIfReached();
  }
  
  public u timeout(long paramLong, TimeUnit paramTimeUnit)
  {
    return this.a.timeout(paramLong, paramTimeUnit);
  }
  
  public long timeoutNanos()
  {
    return this.a.timeoutNanos();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/c/j.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */