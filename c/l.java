package c;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

public final class l
  implements t
{
  private final e a;
  private final Inflater b;
  private int c;
  private boolean d;
  
  l(e parame, Inflater paramInflater)
  {
    if (parame == null) {
      throw new IllegalArgumentException("source == null");
    }
    if (paramInflater == null) {
      throw new IllegalArgumentException("inflater == null");
    }
    this.a = parame;
    this.b = paramInflater;
  }
  
  public l(t paramt, Inflater paramInflater)
  {
    this(m.a(paramt), paramInflater);
  }
  
  private void b()
    throws IOException
  {
    if (this.c == 0) {
      return;
    }
    int i = this.c - this.b.getRemaining();
    this.c -= i;
    this.a.g(i);
  }
  
  public boolean a()
    throws IOException
  {
    if (!this.b.needsInput()) {
      return false;
    }
    b();
    if (this.b.getRemaining() != 0) {
      throw new IllegalStateException("?");
    }
    if (this.a.f()) {
      return true;
    }
    p localp = this.a.b().a;
    this.c = (localp.c - localp.b);
    this.b.setInput(localp.a, localp.b, this.c);
    return false;
  }
  
  public void close()
    throws IOException
  {
    if (this.d) {
      return;
    }
    this.b.end();
    this.d = true;
    this.a.close();
  }
  
  public long read(c paramc, long paramLong)
    throws IOException
  {
    if (paramLong < 0L) {
      throw new IllegalArgumentException("byteCount < 0: " + paramLong);
    }
    if (this.d) {
      throw new IllegalStateException("closed");
    }
    if (paramLong == 0L) {
      return 0L;
    }
    for (;;)
    {
      boolean bool = a();
      try
      {
        p localp = paramc.e(1);
        int i = this.b.inflate(localp.a, localp.c, 2048 - localp.c);
        if (i > 0)
        {
          localp.c += i;
          paramc.b += i;
          return i;
        }
        if ((this.b.finished()) || (this.b.needsDictionary()))
        {
          b();
          if (localp.b == localp.c)
          {
            paramc.a = localp.a();
            q.a(localp);
          }
        }
        else
        {
          if (!bool) {
            continue;
          }
          throw new EOFException("source exhausted prematurely");
        }
      }
      catch (DataFormatException paramc)
      {
        throw new IOException(paramc);
      }
    }
    return -1L;
  }
  
  public u timeout()
  {
    return this.a.timeout();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/c/l.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */