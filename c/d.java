package c;

import java.io.IOException;
import java.io.OutputStream;

public abstract interface d
  extends s
{
  public abstract long a(t paramt)
    throws IOException;
  
  public abstract c b();
  
  public abstract d b(f paramf)
    throws IOException;
  
  public abstract d b(String paramString)
    throws IOException;
  
  public abstract d c(byte[] paramArrayOfByte)
    throws IOException;
  
  public abstract d c(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException;
  
  public abstract OutputStream c();
  
  public abstract d e()
    throws IOException;
  
  public abstract d g(int paramInt)
    throws IOException;
  
  public abstract d h(int paramInt)
    throws IOException;
  
  public abstract d i(int paramInt)
    throws IOException;
  
  public abstract d j(long paramLong)
    throws IOException;
  
  public abstract d k(long paramLong)
    throws IOException;
  
  public abstract d w()
    throws IOException;
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/c/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */