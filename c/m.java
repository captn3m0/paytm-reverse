package c;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class m
{
  private static final Logger a = Logger.getLogger(m.class.getName());
  
  public static d a(s params)
  {
    if (params == null) {
      throw new IllegalArgumentException("sink == null");
    }
    return new n(params);
  }
  
  public static e a(t paramt)
  {
    if (paramt == null) {
      throw new IllegalArgumentException("source == null");
    }
    return new o(paramt);
  }
  
  public static s a(OutputStream paramOutputStream)
  {
    return a(paramOutputStream, new u());
  }
  
  private static s a(final OutputStream paramOutputStream, u paramu)
  {
    if (paramOutputStream == null) {
      throw new IllegalArgumentException("out == null");
    }
    if (paramu == null) {
      throw new IllegalArgumentException("timeout == null");
    }
    new s()
    {
      public void close()
        throws IOException
      {
        paramOutputStream.close();
      }
      
      public void flush()
        throws IOException
      {
        paramOutputStream.flush();
      }
      
      public u timeout()
      {
        return this.a;
      }
      
      public String toString()
      {
        return "sink(" + paramOutputStream + ")";
      }
      
      public void write(c paramAnonymousc, long paramAnonymousLong)
        throws IOException
      {
        v.a(paramAnonymousc.b, 0L, paramAnonymousLong);
        while (paramAnonymousLong > 0L)
        {
          this.a.throwIfReached();
          p localp = paramAnonymousc.a;
          int i = (int)Math.min(paramAnonymousLong, localp.c - localp.b);
          paramOutputStream.write(localp.a, localp.b, i);
          localp.b += i;
          long l = paramAnonymousLong - i;
          paramAnonymousc.b -= i;
          paramAnonymousLong = l;
          if (localp.b == localp.c)
          {
            paramAnonymousc.a = localp.a();
            q.a(localp);
            paramAnonymousLong = l;
          }
        }
      }
    };
  }
  
  public static s a(Socket paramSocket)
    throws IOException
  {
    if (paramSocket == null) {
      throw new IllegalArgumentException("socket == null");
    }
    a locala = c(paramSocket);
    return locala.sink(a(paramSocket.getOutputStream(), locala));
  }
  
  public static t a(File paramFile)
    throws FileNotFoundException
  {
    if (paramFile == null) {
      throw new IllegalArgumentException("file == null");
    }
    return a(new FileInputStream(paramFile));
  }
  
  public static t a(InputStream paramInputStream)
  {
    return a(paramInputStream, new u());
  }
  
  private static t a(final InputStream paramInputStream, u paramu)
  {
    if (paramInputStream == null) {
      throw new IllegalArgumentException("in == null");
    }
    if (paramu == null) {
      throw new IllegalArgumentException("timeout == null");
    }
    new t()
    {
      public void close()
        throws IOException
      {
        paramInputStream.close();
      }
      
      public long read(c paramAnonymousc, long paramAnonymousLong)
        throws IOException
      {
        if (paramAnonymousLong < 0L) {
          throw new IllegalArgumentException("byteCount < 0: " + paramAnonymousLong);
        }
        if (paramAnonymousLong == 0L) {
          return 0L;
        }
        this.a.throwIfReached();
        p localp = paramAnonymousc.e(1);
        int i = (int)Math.min(paramAnonymousLong, 2048 - localp.c);
        i = paramInputStream.read(localp.a, localp.c, i);
        if (i == -1) {
          return -1L;
        }
        localp.c += i;
        paramAnonymousc.b += i;
        return i;
      }
      
      public u timeout()
      {
        return this.a;
      }
      
      public String toString()
      {
        return "source(" + paramInputStream + ")";
      }
    };
  }
  
  public static s b(File paramFile)
    throws FileNotFoundException
  {
    if (paramFile == null) {
      throw new IllegalArgumentException("file == null");
    }
    return a(new FileOutputStream(paramFile));
  }
  
  public static t b(Socket paramSocket)
    throws IOException
  {
    if (paramSocket == null) {
      throw new IllegalArgumentException("socket == null");
    }
    a locala = c(paramSocket);
    return locala.source(a(paramSocket.getInputStream(), locala));
  }
  
  private static a c(Socket paramSocket)
  {
    new a()
    {
      protected IOException newTimeoutException(IOException paramAnonymousIOException)
      {
        SocketTimeoutException localSocketTimeoutException = new SocketTimeoutException("timeout");
        if (paramAnonymousIOException != null) {
          localSocketTimeoutException.initCause(paramAnonymousIOException);
        }
        return localSocketTimeoutException;
      }
      
      protected void timedOut()
      {
        try
        {
          this.a.close();
          return;
        }
        catch (Exception localException)
        {
          m.a().log(Level.WARNING, "Failed to close timed out socket " + this.a, localException);
          return;
        }
        catch (AssertionError localAssertionError)
        {
          if ((localAssertionError.getCause() != null) && (localAssertionError.getMessage() != null) && (localAssertionError.getMessage().contains("getsockname failed")))
          {
            m.a().log(Level.WARNING, "Failed to close timed out socket " + this.a, localAssertionError);
            return;
          }
          throw localAssertionError;
        }
      }
    };
  }
  
  public static s c(File paramFile)
    throws FileNotFoundException
  {
    if (paramFile == null) {
      throw new IllegalArgumentException("file == null");
    }
    return a(new FileOutputStream(paramFile, true));
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/c/m.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */