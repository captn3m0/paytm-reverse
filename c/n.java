package c;

import java.io.IOException;
import java.io.OutputStream;

final class n
  implements d
{
  public final c a;
  public final s b;
  private boolean c;
  
  public n(s params)
  {
    this(params, new c());
  }
  
  public n(s params, c paramc)
  {
    if (params == null) {
      throw new IllegalArgumentException("sink == null");
    }
    this.a = paramc;
    this.b = params;
  }
  
  public long a(t paramt)
    throws IOException
  {
    if (paramt == null) {
      throw new IllegalArgumentException("source == null");
    }
    long l1 = 0L;
    for (;;)
    {
      long l2 = paramt.read(this.a, 2048L);
      if (l2 == -1L) {
        break;
      }
      l1 += l2;
      w();
    }
    return l1;
  }
  
  public c b()
  {
    return this.a;
  }
  
  public d b(f paramf)
    throws IOException
  {
    if (this.c) {
      throw new IllegalStateException("closed");
    }
    this.a.a(paramf);
    return w();
  }
  
  public d b(String paramString)
    throws IOException
  {
    if (this.c) {
      throw new IllegalStateException("closed");
    }
    this.a.a(paramString);
    return w();
  }
  
  public d c(byte[] paramArrayOfByte)
    throws IOException
  {
    if (this.c) {
      throw new IllegalStateException("closed");
    }
    this.a.b(paramArrayOfByte);
    return w();
  }
  
  public d c(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if (this.c) {
      throw new IllegalStateException("closed");
    }
    this.a.b(paramArrayOfByte, paramInt1, paramInt2);
    return w();
  }
  
  public OutputStream c()
  {
    new OutputStream()
    {
      public void close()
        throws IOException
      {
        n.this.close();
      }
      
      public void flush()
        throws IOException
      {
        if (!n.a(n.this)) {
          n.this.flush();
        }
      }
      
      public String toString()
      {
        return n.this + ".outputStream()";
      }
      
      public void write(int paramAnonymousInt)
        throws IOException
      {
        if (n.a(n.this)) {
          throw new IOException("closed");
        }
        n.this.a.b((byte)paramAnonymousInt);
        n.this.w();
      }
      
      public void write(byte[] paramAnonymousArrayOfByte, int paramAnonymousInt1, int paramAnonymousInt2)
        throws IOException
      {
        if (n.a(n.this)) {
          throw new IOException("closed");
        }
        n.this.a.b(paramAnonymousArrayOfByte, paramAnonymousInt1, paramAnonymousInt2);
        n.this.w();
      }
    };
  }
  
  public void close()
    throws IOException
  {
    if (this.c) {}
    label57:
    do
    {
      return;
      localObject2 = null;
      Object localObject1 = localObject2;
      try
      {
        if (this.a.b > 0L)
        {
          this.b.write(this.a, this.a.b);
          localObject1 = localObject2;
        }
      }
      catch (Throwable localThrowable1)
      {
        for (;;) {}
      }
      try
      {
        this.b.close();
        localObject2 = localObject1;
      }
      catch (Throwable localThrowable2)
      {
        localObject2 = localThrowable1;
        if (localThrowable1 != null) {
          break label57;
        }
        localObject2 = localThrowable2;
        break label57;
      }
      this.c = true;
    } while (localObject2 == null);
    v.a((Throwable)localObject2);
  }
  
  public d e()
    throws IOException
  {
    if (this.c) {
      throw new IllegalStateException("closed");
    }
    long l = this.a.a();
    if (l > 0L) {
      this.b.write(this.a, l);
    }
    return this;
  }
  
  public void flush()
    throws IOException
  {
    if (this.c) {
      throw new IllegalStateException("closed");
    }
    if (this.a.b > 0L) {
      this.b.write(this.a, this.a.b);
    }
    this.b.flush();
  }
  
  public d g(int paramInt)
    throws IOException
  {
    if (this.c) {
      throw new IllegalStateException("closed");
    }
    this.a.d(paramInt);
    return w();
  }
  
  public d h(int paramInt)
    throws IOException
  {
    if (this.c) {
      throw new IllegalStateException("closed");
    }
    this.a.c(paramInt);
    return w();
  }
  
  public d i(int paramInt)
    throws IOException
  {
    if (this.c) {
      throw new IllegalStateException("closed");
    }
    this.a.b(paramInt);
    return w();
  }
  
  public d j(long paramLong)
    throws IOException
  {
    if (this.c) {
      throw new IllegalStateException("closed");
    }
    this.a.i(paramLong);
    return w();
  }
  
  public d k(long paramLong)
    throws IOException
  {
    if (this.c) {
      throw new IllegalStateException("closed");
    }
    this.a.h(paramLong);
    return w();
  }
  
  public u timeout()
  {
    return this.b.timeout();
  }
  
  public String toString()
  {
    return "buffer(" + this.b + ")";
  }
  
  public d w()
    throws IOException
  {
    if (this.c) {
      throw new IllegalStateException("closed");
    }
    long l = this.a.h();
    if (l > 0L) {
      this.b.write(this.a, l);
    }
    return this;
  }
  
  public void write(c paramc, long paramLong)
    throws IOException
  {
    if (this.c) {
      throw new IllegalStateException("closed");
    }
    this.a.write(paramc, paramLong);
    w();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/c/n.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */