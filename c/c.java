package c;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class c
  implements d, e, Cloneable
{
  private static final byte[] c = { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102 };
  p a;
  long b;
  
  public int a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    v.a(paramArrayOfByte.length, paramInt1, paramInt2);
    p localp = this.a;
    if (localp == null) {
      paramInt1 = -1;
    }
    do
    {
      return paramInt1;
      paramInt2 = Math.min(paramInt2, localp.c - localp.b);
      System.arraycopy(localp.a, localp.b, paramArrayOfByte, paramInt1, paramInt2);
      localp.b += paramInt2;
      this.b -= paramInt2;
      paramInt1 = paramInt2;
    } while (localp.b != localp.c);
    this.a = localp.a();
    q.a(localp);
    return paramInt2;
  }
  
  public long a()
  {
    return this.b;
  }
  
  public long a(byte paramByte)
  {
    return a(paramByte, 0L);
  }
  
  public long a(byte paramByte, long paramLong)
  {
    if (paramLong < 0L) {
      throw new IllegalArgumentException("fromIndex < 0");
    }
    Object localObject1 = this.a;
    if (localObject1 == null) {
      return -1L;
    }
    long l = 0L;
    int j = ((p)localObject1).c - ((p)localObject1).b;
    if (paramLong >= j) {}
    for (paramLong -= j;; paramLong = 0L)
    {
      l += j;
      Object localObject2 = ((p)localObject1).f;
      localObject1 = localObject2;
      if (localObject2 != this.a) {
        break;
      }
      return -1L;
      localObject2 = ((p)localObject1).a;
      int i = (int)(((p)localObject1).b + paramLong);
      int k = ((p)localObject1).c;
      while (i < k)
      {
        if (localObject2[i] == paramByte) {
          return i + l - ((p)localObject1).b;
        }
        i += 1;
      }
    }
  }
  
  public long a(t paramt)
    throws IOException
  {
    if (paramt == null) {
      throw new IllegalArgumentException("source == null");
    }
    long l2;
    for (long l1 = 0L;; l1 += l2)
    {
      l2 = paramt.read(this, 2048L);
      if (l2 == -1L) {
        break;
      }
    }
    return l1;
  }
  
  public c a(int paramInt)
  {
    if (paramInt < 128)
    {
      b(paramInt);
      return this;
    }
    if (paramInt < 2048)
    {
      b(paramInt >> 6 | 0xC0);
      b(paramInt & 0x3F | 0x80);
      return this;
    }
    if (paramInt < 65536)
    {
      if ((paramInt >= 55296) && (paramInt <= 57343)) {
        throw new IllegalArgumentException("Unexpected code point: " + Integer.toHexString(paramInt));
      }
      b(paramInt >> 12 | 0xE0);
      b(paramInt >> 6 & 0x3F | 0x80);
      b(paramInt & 0x3F | 0x80);
      return this;
    }
    if (paramInt <= 1114111)
    {
      b(paramInt >> 18 | 0xF0);
      b(paramInt >> 12 & 0x3F | 0x80);
      b(paramInt >> 6 & 0x3F | 0x80);
      b(paramInt & 0x3F | 0x80);
      return this;
    }
    throw new IllegalArgumentException("Unexpected code point: " + Integer.toHexString(paramInt));
  }
  
  public c a(c paramc, long paramLong1, long paramLong2)
  {
    if (paramc == null) {
      throw new IllegalArgumentException("out == null");
    }
    v.a(this.b, paramLong1, paramLong2);
    if (paramLong2 == 0L) {
      return this;
    }
    paramc.b += paramLong2;
    p localp2;
    long l1;
    long l2;
    for (p localp1 = this.a;; localp1 = localp1.f)
    {
      localp2 = localp1;
      l1 = paramLong1;
      l2 = paramLong2;
      if (paramLong1 < localp1.c - localp1.b) {
        break;
      }
      paramLong1 -= localp1.c - localp1.b;
    }
    label103:
    if (l2 > 0L)
    {
      localp1 = new p(localp2);
      localp1.b = ((int)(localp1.b + l1));
      localp1.c = Math.min(localp1.b + (int)l2, localp1.c);
      if (paramc.a != null) {
        break label215;
      }
      localp1.g = localp1;
      localp1.f = localp1;
      paramc.a = localp1;
    }
    for (;;)
    {
      l2 -= localp1.c - localp1.b;
      l1 = 0L;
      localp2 = localp2.f;
      break label103;
      break;
      label215:
      paramc.a.g.a(localp1);
    }
  }
  
  public c a(f paramf)
  {
    if (paramf == null) {
      throw new IllegalArgumentException("byteString == null");
    }
    paramf.a(this);
    return this;
  }
  
  public c a(String paramString)
  {
    return a(paramString, 0, paramString.length());
  }
  
  public c a(String paramString, int paramInt1, int paramInt2)
  {
    if (paramString == null) {
      throw new IllegalArgumentException("string == null");
    }
    if (paramInt1 < 0) {
      throw new IllegalAccessError("beginIndex < 0: " + paramInt1);
    }
    if (paramInt2 < paramInt1) {
      throw new IllegalArgumentException("endIndex < beginIndex: " + paramInt2 + " < " + paramInt1);
    }
    if (paramInt2 > paramString.length()) {
      throw new IllegalArgumentException("endIndex > string.length: " + paramInt2 + " > " + paramString.length());
    }
    if (paramInt1 < paramInt2)
    {
      int k = paramString.charAt(paramInt1);
      byte[] arrayOfByte;
      int i;
      if (k < 128)
      {
        p localp = e(1);
        arrayOfByte = localp.a;
        i = localp.c - paramInt1;
        int j = Math.min(paramInt2, 2048 - i);
        arrayOfByte[(i + paramInt1)] = ((byte)k);
        paramInt1 += 1;
        label202:
        if (paramInt1 < j)
        {
          k = paramString.charAt(paramInt1);
          if (k < 128) {}
        }
        else
        {
          i = paramInt1 + i - localp.c;
          localp.c += i;
          this.b += i;
        }
      }
      for (;;)
      {
        break;
        arrayOfByte[(i + paramInt1)] = ((byte)k);
        paramInt1 += 1;
        break label202;
        if (k < 2048)
        {
          b(k >> 6 | 0xC0);
          b(k & 0x3F | 0x80);
          paramInt1 += 1;
        }
        else if ((k < 55296) || (k > 57343))
        {
          b(k >> 12 | 0xE0);
          b(k >> 6 & 0x3F | 0x80);
          b(k & 0x3F | 0x80);
          paramInt1 += 1;
        }
        else
        {
          if (paramInt1 + 1 < paramInt2) {}
          for (i = paramString.charAt(paramInt1 + 1);; i = 0)
          {
            if ((k <= 56319) && (i >= 56320) && (i <= 57343)) {
              break label446;
            }
            b(63);
            paramInt1 += 1;
            break;
          }
          label446:
          i = 65536 + ((0xFFFF27FF & k) << 10 | 0xFFFF23FF & i);
          b(i >> 18 | 0xF0);
          b(i >> 12 & 0x3F | 0x80);
          b(i >> 6 & 0x3F | 0x80);
          b(i & 0x3F | 0x80);
          paramInt1 += 2;
        }
      }
    }
    return this;
  }
  
  public c a(String paramString, int paramInt1, int paramInt2, Charset paramCharset)
  {
    if (paramString == null) {
      throw new IllegalArgumentException("string == null");
    }
    if (paramInt1 < 0) {
      throw new IllegalAccessError("beginIndex < 0: " + paramInt1);
    }
    if (paramInt2 < paramInt1) {
      throw new IllegalArgumentException("endIndex < beginIndex: " + paramInt2 + " < " + paramInt1);
    }
    if (paramInt2 > paramString.length()) {
      throw new IllegalArgumentException("endIndex > string.length: " + paramInt2 + " > " + paramString.length());
    }
    if (paramCharset == null) {
      throw new IllegalArgumentException("charset == null");
    }
    if (paramCharset.equals(v.a)) {
      return a(paramString);
    }
    paramString = paramString.substring(paramInt1, paramInt2).getBytes(paramCharset);
    return b(paramString, 0, paramString.length);
  }
  
  public c a(String paramString, Charset paramCharset)
  {
    return a(paramString, 0, paramString.length(), paramCharset);
  }
  
  public String a(long paramLong, Charset paramCharset)
    throws EOFException
  {
    v.a(this.b, 0L, paramLong);
    if (paramCharset == null) {
      throw new IllegalArgumentException("charset == null");
    }
    if (paramLong > 2147483647L) {
      throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + paramLong);
    }
    if (paramLong == 0L) {
      paramCharset = "";
    }
    p localp;
    String str;
    do
    {
      return paramCharset;
      localp = this.a;
      if (localp.b + paramLong > localp.c) {
        return new String(f(paramLong), paramCharset);
      }
      str = new String(localp.a, localp.b, (int)paramLong, paramCharset);
      localp.b = ((int)(localp.b + paramLong));
      this.b -= paramLong;
      paramCharset = str;
    } while (localp.b != localp.c);
    this.a = localp.a();
    q.a(localp);
    return str;
  }
  
  public void a(long paramLong)
    throws EOFException
  {
    if (this.b < paramLong) {
      throw new EOFException();
    }
  }
  
  public void a(byte[] paramArrayOfByte)
    throws EOFException
  {
    int i = 0;
    while (i < paramArrayOfByte.length)
    {
      int j = a(paramArrayOfByte, i, paramArrayOfByte.length - i);
      if (j == -1) {
        throw new EOFException();
      }
      i += j;
    }
  }
  
  public byte b(long paramLong)
  {
    v.a(this.b, paramLong, 1L);
    for (p localp = this.a;; localp = localp.f)
    {
      int i = localp.c - localp.b;
      if (paramLong < i) {
        return localp.a[(localp.b + (int)paramLong)];
      }
      paramLong -= i;
    }
  }
  
  public c b()
  {
    return this;
  }
  
  public c b(int paramInt)
  {
    p localp = e(1);
    byte[] arrayOfByte = localp.a;
    int i = localp.c;
    localp.c = (i + 1);
    arrayOfByte[i] = ((byte)paramInt);
    this.b += 1L;
    return this;
  }
  
  public c b(byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte == null) {
      throw new IllegalArgumentException("source == null");
    }
    return b(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public c b(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    if (paramArrayOfByte == null) {
      throw new IllegalArgumentException("source == null");
    }
    v.a(paramArrayOfByte.length, paramInt1, paramInt2);
    int i = paramInt1 + paramInt2;
    while (paramInt1 < i)
    {
      p localp = e(1);
      int j = Math.min(i - paramInt1, 2048 - localp.c);
      System.arraycopy(paramArrayOfByte, paramInt1, localp.a, localp.c, j);
      paramInt1 += j;
      localp.c += j;
    }
    this.b += paramInt2;
    return this;
  }
  
  public c c(int paramInt)
  {
    p localp = e(2);
    byte[] arrayOfByte = localp.a;
    int i = localp.c;
    int j = i + 1;
    arrayOfByte[i] = ((byte)(paramInt >>> 8 & 0xFF));
    arrayOfByte[j] = ((byte)(paramInt & 0xFF));
    localp.c = (j + 1);
    this.b += 2L;
    return this;
  }
  
  public f c(long paramLong)
    throws EOFException
  {
    return new f(f(paramLong));
  }
  
  public OutputStream c()
  {
    new OutputStream()
    {
      public void close() {}
      
      public void flush() {}
      
      public String toString()
      {
        return this + ".outputStream()";
      }
      
      public void write(int paramAnonymousInt)
      {
        c.this.b((byte)paramAnonymousInt);
      }
      
      public void write(byte[] paramAnonymousArrayOfByte, int paramAnonymousInt1, int paramAnonymousInt2)
      {
        c.this.b(paramAnonymousArrayOfByte, paramAnonymousInt1, paramAnonymousInt2);
      }
    };
  }
  
  public void close() {}
  
  public c d()
  {
    return this;
  }
  
  public c d(int paramInt)
  {
    p localp = e(4);
    byte[] arrayOfByte = localp.a;
    int j = localp.c;
    int i = j + 1;
    arrayOfByte[j] = ((byte)(paramInt >>> 24 & 0xFF));
    j = i + 1;
    arrayOfByte[i] = ((byte)(paramInt >>> 16 & 0xFF));
    i = j + 1;
    arrayOfByte[j] = ((byte)(paramInt >>> 8 & 0xFF));
    arrayOfByte[i] = ((byte)(paramInt & 0xFF));
    localp.c = (i + 1);
    this.b += 4L;
    return this;
  }
  
  public String d(long paramLong)
    throws EOFException
  {
    return a(paramLong, v.a);
  }
  
  public d e()
  {
    return this;
  }
  
  p e(int paramInt)
  {
    if ((paramInt < 1) || (paramInt > 2048)) {
      throw new IllegalArgumentException();
    }
    p localp2;
    p localp1;
    if (this.a == null)
    {
      this.a = q.a();
      localp2 = this.a;
      p localp3 = this.a;
      localp1 = this.a;
      localp3.g = localp1;
      localp2.f = localp1;
    }
    do
    {
      return localp1;
      localp2 = this.a.g;
      if (localp2.c + paramInt > 2048) {
        break;
      }
      localp1 = localp2;
    } while (localp2.e);
    return localp2.a(q.a());
  }
  
  String e(long paramLong)
    throws EOFException
  {
    if ((paramLong > 0L) && (b(paramLong - 1L) == 13))
    {
      str = d(paramLong - 1L);
      g(2L);
      return str;
    }
    String str = d(paramLong);
    g(1L);
    return str;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof c)) {
      return false;
    }
    paramObject = (c)paramObject;
    if (this.b != ((c)paramObject).b) {
      return false;
    }
    if (this.b == 0L) {
      return true;
    }
    p localp = this.a;
    paramObject = ((c)paramObject).a;
    int i = localp.b;
    int j = ((p)paramObject).b;
    long l1 = 0L;
    long l2;
    int k;
    if (l1 < this.b)
    {
      l2 = Math.min(localp.c - i, ((p)paramObject).c - j);
      int m = 0;
      k = i;
      i = j;
      j = k;
      k = m;
      while (k < l2)
      {
        if (localp.a[j] != paramObject.a[i]) {
          return false;
        }
        k += 1;
        i += 1;
        j += 1;
      }
      if (j != localp.c) {
        break label223;
      }
      localp = localp.f;
      j = localp.b;
    }
    label223:
    for (;;)
    {
      if (i == ((p)paramObject).c) {
        paramObject = ((p)paramObject).f;
      }
      for (k = ((p)paramObject).b;; k = i)
      {
        l1 += l2;
        i = j;
        j = k;
        break;
        return true;
      }
    }
  }
  
  public f f(int paramInt)
  {
    if (paramInt == 0) {
      return f.b;
    }
    return new r(this, paramInt);
  }
  
  public boolean f()
  {
    return this.b == 0L;
  }
  
  public byte[] f(long paramLong)
    throws EOFException
  {
    v.a(this.b, 0L, paramLong);
    if (paramLong > 2147483647L) {
      throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + paramLong);
    }
    byte[] arrayOfByte = new byte[(int)paramLong];
    a(arrayOfByte);
    return arrayOfByte;
  }
  
  public void flush() {}
  
  public InputStream g()
  {
    new InputStream()
    {
      public int available()
      {
        return (int)Math.min(c.this.b, 2147483647L);
      }
      
      public void close() {}
      
      public int read()
      {
        if (c.this.b > 0L) {
          return c.this.i() & 0xFF;
        }
        return -1;
      }
      
      public int read(byte[] paramAnonymousArrayOfByte, int paramAnonymousInt1, int paramAnonymousInt2)
      {
        return c.this.a(paramAnonymousArrayOfByte, paramAnonymousInt1, paramAnonymousInt2);
      }
      
      public String toString()
      {
        return c.this + ".inputStream()";
      }
    };
  }
  
  public void g(long paramLong)
    throws EOFException
  {
    while (paramLong > 0L)
    {
      if (this.a == null) {
        throw new EOFException();
      }
      int i = (int)Math.min(paramLong, this.a.c - this.a.b);
      this.b -= i;
      long l = paramLong - i;
      p localp = this.a;
      localp.b += i;
      paramLong = l;
      if (this.a.b == this.a.c)
      {
        localp = this.a;
        this.a = localp.a();
        q.a(localp);
        paramLong = l;
      }
    }
  }
  
  public long h()
  {
    long l2 = this.b;
    if (l2 == 0L) {
      return 0L;
    }
    p localp = this.a.g;
    long l1 = l2;
    if (localp.c < 2048)
    {
      l1 = l2;
      if (localp.e) {
        l1 = l2 - (localp.c - localp.b);
      }
    }
    return l1;
  }
  
  public c h(long paramLong)
  {
    if (paramLong == 0L) {
      return b(48);
    }
    int j = 0;
    long l = paramLong;
    if (paramLong < 0L)
    {
      l = -paramLong;
      if (l < 0L) {
        return a("-9223372036854775808");
      }
      j = 1;
    }
    int i;
    if (l < 100000000L) {
      if (l < 10000L) {
        if (l < 100L) {
          if (l < 10L) {
            i = 1;
          }
        }
      }
    }
    int k;
    p localp;
    byte[] arrayOfByte;
    for (;;)
    {
      k = i;
      if (j != 0) {
        k = i + 1;
      }
      localp = e(k);
      arrayOfByte = localp.a;
      i = localp.c + k;
      while (l != 0L)
      {
        int m = (int)(l % 10L);
        i -= 1;
        arrayOfByte[i] = c[m];
        l /= 10L;
      }
      i = 2;
      continue;
      if (l < 1000L)
      {
        i = 3;
      }
      else
      {
        i = 4;
        continue;
        if (l < 1000000L)
        {
          if (l < 100000L) {
            i = 5;
          } else {
            i = 6;
          }
        }
        else if (l < 10000000L)
        {
          i = 7;
        }
        else
        {
          i = 8;
          continue;
          if (l < 1000000000000L)
          {
            if (l < 10000000000L)
            {
              if (l < 1000000000L) {
                i = 9;
              } else {
                i = 10;
              }
            }
            else if (l < 100000000000L) {
              i = 11;
            } else {
              i = 12;
            }
          }
          else if (l < 1000000000000000L)
          {
            if (l < 10000000000000L) {
              i = 13;
            } else if (l < 100000000000000L) {
              i = 14;
            } else {
              i = 15;
            }
          }
          else if (l < 100000000000000000L)
          {
            if (l < 10000000000000000L) {
              i = 16;
            } else {
              i = 17;
            }
          }
          else if (l < 1000000000000000000L) {
            i = 18;
          } else {
            i = 19;
          }
        }
      }
    }
    if (j != 0) {
      arrayOfByte[(i - 1)] = 45;
    }
    localp.c += k;
    this.b += k;
    return this;
  }
  
  public int hashCode()
  {
    Object localObject = this.a;
    if (localObject == null) {
      return 0;
    }
    int j = 1;
    int i;
    p localp;
    do
    {
      int k = ((p)localObject).b;
      int m = ((p)localObject).c;
      i = j;
      j = k;
      while (j < m)
      {
        i = i * 31 + localObject.a[j];
        j += 1;
      }
      localp = ((p)localObject).f;
      j = i;
      localObject = localp;
    } while (localp != this.a);
    return i;
  }
  
  public byte i()
  {
    if (this.b == 0L) {
      throw new IllegalStateException("size == 0");
    }
    p localp = this.a;
    int i = localp.b;
    int j = localp.c;
    byte[] arrayOfByte = localp.a;
    int k = i + 1;
    byte b1 = arrayOfByte[i];
    this.b -= 1L;
    if (k == j)
    {
      this.a = localp.a();
      q.a(localp);
      return b1;
    }
    localp.b = k;
    return b1;
  }
  
  public c i(long paramLong)
  {
    if (paramLong == 0L) {
      return b(48);
    }
    int j = Long.numberOfTrailingZeros(Long.highestOneBit(paramLong)) / 4 + 1;
    p localp = e(j);
    byte[] arrayOfByte = localp.a;
    int i = localp.c + j - 1;
    int k = localp.c;
    while (i >= k)
    {
      arrayOfByte[i] = c[((int)(0xF & paramLong))];
      paramLong >>>= 4;
      i -= 1;
    }
    localp.c += j;
    this.b += j;
    return this;
  }
  
  public short j()
  {
    if (this.b < 2L) {
      throw new IllegalStateException("size < 2: " + this.b);
    }
    p localp = this.a;
    int k = localp.b;
    int i = localp.c;
    if (i - k < 2) {
      return (short)((i() & 0xFF) << 8 | i() & 0xFF);
    }
    byte[] arrayOfByte = localp.a;
    int j = k + 1;
    k = arrayOfByte[k];
    int m = j + 1;
    j = arrayOfByte[j];
    this.b -= 2L;
    if (m == i)
    {
      this.a = localp.a();
      q.a(localp);
    }
    for (;;)
    {
      return (short)((k & 0xFF) << 8 | j & 0xFF);
      localp.b = m;
    }
  }
  
  public int k()
  {
    if (this.b < 4L) {
      throw new IllegalStateException("size < 4: " + this.b);
    }
    p localp = this.a;
    int j = localp.b;
    int i = localp.c;
    if (i - j < 4) {
      return (i() & 0xFF) << 24 | (i() & 0xFF) << 16 | (i() & 0xFF) << 8 | i() & 0xFF;
    }
    byte[] arrayOfByte = localp.a;
    int k = j + 1;
    j = arrayOfByte[j];
    int n = k + 1;
    k = arrayOfByte[k];
    int m = n + 1;
    int i1 = arrayOfByte[n];
    n = m + 1;
    j = (j & 0xFF) << 24 | (k & 0xFF) << 16 | (i1 & 0xFF) << 8 | arrayOfByte[m] & 0xFF;
    this.b -= 4L;
    if (n == i)
    {
      this.a = localp.a();
      q.a(localp);
      return j;
    }
    localp.b = n;
    return j;
  }
  
  public short l()
  {
    return v.a(j());
  }
  
  public int m()
  {
    return v.a(k());
  }
  
  public long n()
  {
    if (this.b == 0L) {
      throw new IllegalStateException("size == 0");
    }
    long l2 = 0L;
    int m = 0;
    int n = 0;
    int i = 0;
    long l3 = -7L;
    Object localObject = this.a;
    byte[] arrayOfByte = ((p)localObject).a;
    int k = ((p)localObject).b;
    int i1 = ((p)localObject).c;
    long l1 = l2;
    int j = m;
    l2 = l3;
    m = k;
    k = n;
    n = i;
    if (m < i1)
    {
      n = arrayOfByte[m];
      if ((n >= 48) && (n <= 57))
      {
        int i2 = 48 - n;
        if ((l1 < -922337203685477580L) || ((l1 == -922337203685477580L) && (i2 < l2)))
        {
          localObject = new c().h(l1).b(n);
          if (k == 0) {
            ((c)localObject).i();
          }
          throw new NumberFormatException("Number too large: " + ((c)localObject).q());
        }
        l1 = l1 * 10L + i2;
      }
      for (;;)
      {
        m += 1;
        j += 1;
        break;
        if ((n != 45) || (j != 0)) {
          break label252;
        }
        k = 1;
        l2 -= 1L;
      }
      label252:
      if (j == 0) {
        throw new NumberFormatException("Expected leading [0-9] or '-' character but was 0x" + Integer.toHexString(n));
      }
      n = 1;
    }
    if (m == i1)
    {
      this.a = ((p)localObject).a();
      q.a((p)localObject);
    }
    for (;;)
    {
      if (n == 0)
      {
        i = n;
        n = k;
        l3 = l2;
        m = j;
        l2 = l1;
        if (this.a != null) {
          break;
        }
      }
      this.b -= j;
      if (k == 0) {
        break label369;
      }
      return l1;
      ((p)localObject).b = m;
    }
    label369:
    return -l1;
  }
  
  public long o()
  {
    if (this.b == 0L) {
      throw new IllegalStateException("size == 0");
    }
    long l1 = 0L;
    int i = 0;
    int j = 0;
    Object localObject = this.a;
    byte[] arrayOfByte = ((p)localObject).a;
    int m = ((p)localObject).b;
    int n = ((p)localObject).c;
    long l2 = l1;
    int k = i;
    label60:
    i = j;
    if (m < n)
    {
      int i1 = arrayOfByte[m];
      if ((i1 >= 48) && (i1 <= 57)) {
        i = i1 - 48;
      }
      for (;;)
      {
        if ((0xF000000000000000 & l2) == 0L) {
          break label302;
        }
        localObject = new c().i(l2).b(i1);
        throw new NumberFormatException("Number too large: " + ((c)localObject).q());
        if ((i1 >= 97) && (i1 <= 102))
        {
          i = i1 - 97 + 10;
        }
        else
        {
          if ((i1 < 65) || (i1 > 70)) {
            break;
          }
          i = i1 - 65 + 10;
        }
      }
      if (k == 0) {
        throw new NumberFormatException("Expected leading [0-9a-fA-F] character but was 0x" + Integer.toHexString(i1));
      }
      i = 1;
    }
    if (m == n)
    {
      this.a = ((p)localObject).a();
      q.a((p)localObject);
    }
    for (;;)
    {
      if (i == 0)
      {
        j = i;
        i = k;
        l1 = l2;
        if (this.a != null) {
          break;
        }
      }
      this.b -= k;
      return l2;
      label302:
      l2 = l2 << 4 | i;
      m += 1;
      k += 1;
      break label60;
      ((p)localObject).b = m;
    }
  }
  
  public f p()
  {
    return new f(s());
  }
  
  public String q()
  {
    try
    {
      String str = a(this.b, v.a);
      return str;
    }
    catch (EOFException localEOFException)
    {
      throw new AssertionError(localEOFException);
    }
  }
  
  public String r()
    throws EOFException
  {
    long l = a((byte)10);
    if (l == -1L)
    {
      c localc = new c();
      a(localc, 0L, Math.min(32L, this.b));
      throw new EOFException("\\n not found: size=" + a() + " content=" + localc.p().d() + "...");
    }
    return e(l);
  }
  
  public long read(c paramc, long paramLong)
  {
    if (paramc == null) {
      throw new IllegalArgumentException("sink == null");
    }
    if (paramLong < 0L) {
      throw new IllegalArgumentException("byteCount < 0: " + paramLong);
    }
    if (this.b == 0L) {
      return -1L;
    }
    long l = paramLong;
    if (paramLong > this.b) {
      l = this.b;
    }
    paramc.write(this, l);
    return l;
  }
  
  public byte[] s()
  {
    try
    {
      byte[] arrayOfByte = f(this.b);
      return arrayOfByte;
    }
    catch (EOFException localEOFException)
    {
      throw new AssertionError(localEOFException);
    }
  }
  
  public void t()
  {
    try
    {
      g(this.b);
      return;
    }
    catch (EOFException localEOFException)
    {
      throw new AssertionError(localEOFException);
    }
  }
  
  public u timeout()
  {
    return u.NONE;
  }
  
  public String toString()
  {
    if (this.b == 0L) {
      return "Buffer[size=0]";
    }
    Object localObject;
    if (this.b <= 16L)
    {
      localObject = u().p();
      return String.format("Buffer[size=%s data=%s]", new Object[] { Long.valueOf(this.b), ((f)localObject).d() });
    }
    try
    {
      MessageDigest localMessageDigest = MessageDigest.getInstance("MD5");
      localMessageDigest.update(this.a.a, this.a.b, this.a.c - this.a.b);
      for (localObject = this.a.f; localObject != this.a; localObject = ((p)localObject).f) {
        localMessageDigest.update(((p)localObject).a, ((p)localObject).b, ((p)localObject).c - ((p)localObject).b);
      }
      localObject = String.format("Buffer[size=%s md5=%s]", new Object[] { Long.valueOf(this.b), f.a(localMessageDigest.digest()).d() });
      return (String)localObject;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      throw new AssertionError();
    }
  }
  
  public c u()
  {
    c localc = new c();
    if (this.b == 0L) {
      return localc;
    }
    localc.a = new p(this.a);
    p localp1 = localc.a;
    p localp2 = localc.a;
    p localp3 = localc.a;
    localp2.g = localp3;
    localp1.f = localp3;
    for (localp1 = this.a.f; localp1 != this.a; localp1 = localp1.f) {
      localc.a.g.a(new p(localp1));
    }
    localc.b = this.b;
    return localc;
  }
  
  public f v()
  {
    if (this.b > 2147483647L) {
      throw new IllegalArgumentException("size > Integer.MAX_VALUE: " + this.b);
    }
    return f((int)this.b);
  }
  
  public void write(c paramc, long paramLong)
  {
    if (paramc == null) {
      throw new IllegalArgumentException("source == null");
    }
    if (paramc == this) {
      throw new IllegalArgumentException("source == this");
    }
    v.a(paramc.b, 0L, paramLong);
    if (paramLong > 0L)
    {
      if (paramLong >= paramc.a.c - paramc.a.b) {
        break label190;
      }
      if (this.a == null) {
        break label161;
      }
      localp1 = this.a.g;
      if ((localp1 == null) || (!localp1.e)) {
        break label177;
      }
      l = localp1.c;
      if (!localp1.d) {
        break label167;
      }
    }
    label161:
    label167:
    for (int i = 0;; i = localp1.b)
    {
      if (paramLong + l - i > 2048L) {
        break label177;
      }
      paramc.a.a(localp1, (int)paramLong);
      paramc.b -= paramLong;
      this.b += paramLong;
      return;
      localp1 = null;
      break;
    }
    label177:
    paramc.a = paramc.a.a((int)paramLong);
    label190:
    p localp1 = paramc.a;
    long l = localp1.c - localp1.b;
    paramc.a = localp1.a();
    if (this.a == null)
    {
      this.a = localp1;
      localp1 = this.a;
      p localp2 = this.a;
      p localp3 = this.a;
      localp2.g = localp3;
      localp1.f = localp3;
    }
    for (;;)
    {
      paramc.b -= l;
      this.b += l;
      paramLong -= l;
      break;
      this.a.g.a(localp1).b();
    }
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/c/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */