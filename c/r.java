package c;

import java.util.Arrays;

final class r
  extends f
{
  final transient byte[][] f;
  final transient int[] g;
  
  r(c paramc, int paramInt)
  {
    super(null);
    v.a(paramc.b, 0L, paramInt);
    int j = 0;
    int i = 0;
    for (p localp = paramc.a; j < paramInt; localp = localp.f)
    {
      if (localp.c == localp.b) {
        throw new AssertionError("s.limit == s.pos");
      }
      j += localp.c - localp.b;
      i += 1;
    }
    this.f = new byte[i][];
    this.g = new int[i * 2];
    j = 0;
    i = 0;
    for (paramc = paramc.a; j < paramInt; paramc = paramc.f)
    {
      this.f[i] = paramc.a;
      j += paramc.c - paramc.b;
      this.g[i] = j;
      this.g[(this.f.length + i)] = paramc.b;
      paramc.d = true;
      i += 1;
    }
  }
  
  private int b(int paramInt)
  {
    paramInt = Arrays.binarySearch(this.g, 0, this.f.length, paramInt + 1);
    if (paramInt >= 0) {
      return paramInt;
    }
    return paramInt ^ 0xFFFFFFFF;
  }
  
  private f h()
  {
    return new f(g());
  }
  
  private Object writeReplace()
  {
    return h();
  }
  
  public byte a(int paramInt)
  {
    v.a(this.g[(this.f.length - 1)], paramInt, 1L);
    int j = b(paramInt);
    if (j == 0) {}
    for (int i = 0;; i = this.g[(j - 1)])
    {
      int k = this.g[(this.f.length + j)];
      return this.f[j][(paramInt - i + k)];
    }
  }
  
  public String a()
  {
    return h().a();
  }
  
  void a(c paramc)
  {
    int j = 0;
    int i = 0;
    int m = this.f.length;
    if (i < m)
    {
      int n = this.g[(m + i)];
      int k = this.g[i];
      p localp = new p(this.f[i], n, n + k - j);
      if (paramc.a == null)
      {
        localp.g = localp;
        localp.f = localp;
        paramc.a = localp;
      }
      for (;;)
      {
        j = k;
        i += 1;
        break;
        paramc.a.g.a(localp);
      }
    }
    paramc.b += j;
  }
  
  public boolean a(int paramInt1, f paramf, int paramInt2, int paramInt3)
  {
    if (paramInt1 > f() - paramInt3) {
      return false;
    }
    int j = b(paramInt1);
    int i = paramInt1;
    paramInt1 = j;
    label26:
    if (paramInt3 > 0)
    {
      if (paramInt1 == 0) {}
      for (j = 0;; j = this.g[(paramInt1 - 1)])
      {
        int k = Math.min(paramInt3, j + (this.g[paramInt1] - j) - i);
        int m = this.g[(this.f.length + paramInt1)];
        if (!paramf.a(paramInt2, this.f[paramInt1], i - j + m, k)) {
          break;
        }
        i += k;
        paramInt2 += k;
        paramInt3 -= k;
        paramInt1 += 1;
        break label26;
      }
    }
    return true;
  }
  
  public boolean a(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3)
  {
    if ((paramInt1 > f() - paramInt3) || (paramInt2 > paramArrayOfByte.length - paramInt3)) {
      return false;
    }
    int j = b(paramInt1);
    int i = paramInt1;
    paramInt1 = j;
    label35:
    if (paramInt3 > 0)
    {
      if (paramInt1 == 0) {}
      for (j = 0;; j = this.g[(paramInt1 - 1)])
      {
        int k = Math.min(paramInt3, j + (this.g[paramInt1] - j) - i);
        int m = this.g[(this.f.length + paramInt1)];
        if (!v.a(this.f[paramInt1], i - j + m, paramArrayOfByte, paramInt2, k)) {
          break;
        }
        i += k;
        paramInt2 += k;
        paramInt3 -= k;
        paramInt1 += 1;
        break label35;
      }
    }
    return true;
  }
  
  public String b()
  {
    return h().b();
  }
  
  public f c()
  {
    return h().c();
  }
  
  public String d()
  {
    return h().d();
  }
  
  public f e()
  {
    return h().e();
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {
      return true;
    }
    if (((paramObject instanceof f)) && (((f)paramObject).f() == f()) && (a(0, (f)paramObject, 0, f()))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public int f()
  {
    return this.g[(this.f.length - 1)];
  }
  
  public byte[] g()
  {
    byte[] arrayOfByte = new byte[this.g[(this.f.length - 1)]];
    int j = 0;
    int i = 0;
    int m = this.f.length;
    while (i < m)
    {
      int n = this.g[(m + i)];
      int k = this.g[i];
      System.arraycopy(this.f[i], n, arrayOfByte, j, k - j);
      j = k;
      i += 1;
    }
    return arrayOfByte;
  }
  
  public int hashCode()
  {
    int i = this.d;
    if (i != 0) {
      return i;
    }
    int k = 1;
    int j = 0;
    i = 0;
    int i2 = this.f.length;
    while (i < i2)
    {
      byte[] arrayOfByte = this.f[i];
      int n = this.g[(i2 + i)];
      int i1 = this.g[i];
      int m = n;
      while (m < n + (i1 - j))
      {
        k = k * 31 + arrayOfByte[m];
        m += 1;
      }
      j = i1;
      i += 1;
    }
    this.d = k;
    return k;
  }
  
  public String toString()
  {
    return h().toString();
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/c/r.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */