package c;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

final class o
  implements e
{
  public final c a;
  public final t b;
  private boolean c;
  
  public o(t paramt)
  {
    this(paramt, new c());
  }
  
  public o(t paramt, c paramc)
  {
    if (paramt == null) {
      throw new IllegalArgumentException("source == null");
    }
    this.a = paramc;
    this.b = paramt;
  }
  
  public long a(byte paramByte)
    throws IOException
  {
    return a(paramByte, 0L);
  }
  
  public long a(byte paramByte, long paramLong)
    throws IOException
  {
    if (this.c) {
      throw new IllegalStateException("closed");
    }
    long l;
    do
    {
      l = paramLong;
      if (paramLong < this.a.b) {
        break;
      }
    } while (this.b.read(this.a, 2048L) != -1L);
    paramLong = -1L;
    return paramLong;
    do
    {
      l = this.a.a(paramByte, l);
      paramLong = l;
      if (l != -1L) {
        break;
      }
      l = this.a.b;
    } while (this.b.read(this.a, 2048L) != -1L);
    return -1L;
  }
  
  public void a(long paramLong)
    throws IOException
  {
    if (!b(paramLong)) {
      throw new EOFException();
    }
  }
  
  public c b()
  {
    return this.a;
  }
  
  public boolean b(long paramLong)
    throws IOException
  {
    if (paramLong < 0L) {
      throw new IllegalArgumentException("byteCount < 0: " + paramLong);
    }
    if (this.c) {
      throw new IllegalStateException("closed");
    }
    while (this.a.b < paramLong) {
      if (this.b.read(this.a, 2048L) == -1L) {
        return false;
      }
    }
    return true;
  }
  
  public f c(long paramLong)
    throws IOException
  {
    a(paramLong);
    return this.a.c(paramLong);
  }
  
  public void close()
    throws IOException
  {
    if (this.c) {
      return;
    }
    this.c = true;
    this.b.close();
    this.a.t();
  }
  
  public boolean f()
    throws IOException
  {
    if (this.c) {
      throw new IllegalStateException("closed");
    }
    return (this.a.f()) && (this.b.read(this.a, 2048L) == -1L);
  }
  
  public byte[] f(long paramLong)
    throws IOException
  {
    a(paramLong);
    return this.a.f(paramLong);
  }
  
  public InputStream g()
  {
    new InputStream()
    {
      public int available()
        throws IOException
      {
        if (o.a(o.this)) {
          throw new IOException("closed");
        }
        return (int)Math.min(o.this.a.b, 2147483647L);
      }
      
      public void close()
        throws IOException
      {
        o.this.close();
      }
      
      public int read()
        throws IOException
      {
        if (o.a(o.this)) {
          throw new IOException("closed");
        }
        if ((o.this.a.b == 0L) && (o.this.b.read(o.this.a, 2048L) == -1L)) {
          return -1;
        }
        return o.this.a.i() & 0xFF;
      }
      
      public int read(byte[] paramAnonymousArrayOfByte, int paramAnonymousInt1, int paramAnonymousInt2)
        throws IOException
      {
        if (o.a(o.this)) {
          throw new IOException("closed");
        }
        v.a(paramAnonymousArrayOfByte.length, paramAnonymousInt1, paramAnonymousInt2);
        if ((o.this.a.b == 0L) && (o.this.b.read(o.this.a, 2048L) == -1L)) {
          return -1;
        }
        return o.this.a.a(paramAnonymousArrayOfByte, paramAnonymousInt1, paramAnonymousInt2);
      }
      
      public String toString()
      {
        return o.this + ".inputStream()";
      }
    };
  }
  
  public void g(long paramLong)
    throws IOException
  {
    if (this.c) {
      throw new IllegalStateException("closed");
    }
    do
    {
      long l = Math.min(paramLong, this.a.a());
      this.a.g(l);
      paramLong -= l;
      if (paramLong <= 0L) {
        break;
      }
    } while ((this.a.b != 0L) || (this.b.read(this.a, 2048L) != -1L));
    throw new EOFException();
  }
  
  public byte i()
    throws IOException
  {
    a(1L);
    return this.a.i();
  }
  
  public short j()
    throws IOException
  {
    a(2L);
    return this.a.j();
  }
  
  public int k()
    throws IOException
  {
    a(4L);
    return this.a.k();
  }
  
  public short l()
    throws IOException
  {
    a(2L);
    return this.a.l();
  }
  
  public int m()
    throws IOException
  {
    a(4L);
    return this.a.m();
  }
  
  public long n()
    throws IOException
  {
    a(1L);
    int i = 0;
    while (b(i + 1))
    {
      byte b1 = this.a.b(i);
      if (((b1 < 48) || (b1 > 57)) && ((i != 0) || (b1 != 45)))
      {
        if (i != 0) {
          break;
        }
        throw new NumberFormatException(String.format("Expected leading [0-9] or '-' character but was %#x", new Object[] { Byte.valueOf(b1) }));
      }
      i += 1;
    }
    return this.a.n();
  }
  
  public long o()
    throws IOException
  {
    a(1L);
    int i = 0;
    while (b(i + 1))
    {
      byte b1 = this.a.b(i);
      if (((b1 < 48) || (b1 > 57)) && ((b1 < 97) || (b1 > 102)) && ((b1 < 65) || (b1 > 70)))
      {
        if (i != 0) {
          break;
        }
        throw new NumberFormatException(String.format("Expected leading [0-9a-fA-F] character but was %#x", new Object[] { Byte.valueOf(b1) }));
      }
      i += 1;
    }
    return this.a.o();
  }
  
  public String r()
    throws IOException
  {
    long l = a((byte)10);
    if (l == -1L)
    {
      c localc = new c();
      this.a.a(localc, 0L, Math.min(32L, this.a.a()));
      throw new EOFException("\\n not found: size=" + this.a.a() + " content=" + localc.p().d() + "...");
    }
    return this.a.e(l);
  }
  
  public long read(c paramc, long paramLong)
    throws IOException
  {
    if (paramc == null) {
      throw new IllegalArgumentException("sink == null");
    }
    if (paramLong < 0L) {
      throw new IllegalArgumentException("byteCount < 0: " + paramLong);
    }
    if (this.c) {
      throw new IllegalStateException("closed");
    }
    if ((this.a.b == 0L) && (this.b.read(this.a, 2048L) == -1L)) {
      return -1L;
    }
    paramLong = Math.min(paramLong, this.a.b);
    return this.a.read(paramc, paramLong);
  }
  
  public byte[] s()
    throws IOException
  {
    this.a.a(this.b);
    return this.a.s();
  }
  
  public u timeout()
  {
    return this.b.timeout();
  }
  
  public String toString()
  {
    return "buffer(" + this.b + ")";
  }
}


/* Location:              /home/nemo/Downloads/mobile/Paytm-dex2jar.jar!/c/o.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */